namespace LendersOfficeApp.newlos
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Web.UI;

    using CommonProjectLib.Caching;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Conversions;
    using LendersOffice.HttpModule;
    using LendersOffice.Migration;
    using LendersOffice.Security;

    public class BaseLoanPage : LendersOffice.Common.BaseServicePage
    {
        private NameValueCollection m_customQueryString;
        private BrokerDB m_broker;
        private Dictionary<WorkflowOperation, bool> m_opResults = new Dictionary<WorkflowOperation, bool>();
        private Dictionary<WorkflowOperation, string> m_reasons = new Dictionary<WorkflowOperation, string>();

        private static string CopyRightFooter = string.Format("<hr><table><tr><td nowrap style='font-size:11px'>{0}</td></tr></table>", ConstAppDavid.CopyrightMessage);

        protected bool IsLeadPage
        {
            get { return RequestHelper.GetSafeQueryString("isLead") == "t"; }
        }

        protected virtual WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[] { };
        }

        protected virtual WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return new WorkflowOperation[] { };
        }

        public string GetReasonUserCannotPerformPrivilege(WorkflowOperation privilege)
        {
            return m_reasons[privilege];
        }

        private WorkflowOperation[] GetRequiredOpsToCheck()
        {
            var ops = new HashSet<WorkflowOperation>()
            {
                WorkflowOperations.ReadLoanOrTemplate,
                WorkflowOperations.WriteLoanOrTemplate,
                WorkflowOperations.RunPmlForAllLoans,
                WorkflowOperations.RunPmlForRegisteredLoans,
                WorkflowOperations.RunPmlToStep3,
                WorkflowOperations.WriteField,
                WorkflowOperations.ProtectField
            };

            foreach (WorkflowOperation op in GetExtraOpsToCheck())
            {
                ops.Add(op);
            }

            return ops.ToArray();
        }

        public bool UserHasWorkflowPrivilege(WorkflowOperation priv)
        {
            return m_opResults[priv];
        }

        /// <summary>
        /// This member need to set in the Init event.
        /// </summary>
        protected bool IsAlwaysSave = false;

        protected bool IsAppSpecific = true;

        /// <summary>
        /// Controls whether or not we add loanframework2.js and loanedit service.
        /// </summary>
        protected internal bool UseNewFramework { get; protected set; } = false; // 4/13/2004 dd - To be remove.

        protected bool IsDebug
        {
            get { return false; }
        }

        protected bool IsRefreshLoanSummaryInfo = true;

        /// <summary>
        /// Without setting this, the Print button won't select the correct form.
        /// </summary>
        protected Type PDFPrintClass = null;

        /// <summary>
        /// Returns true if the user cannot write.<para />
        /// Returns true if the user does not have the required write permissions.<para />
        /// *Only use it after init, as it is inaccurate during init.
        /// </summary>
		public virtual bool IsReadOnly
        {
            get { return !HasEditLoanPermission(); }
        }

        /// <summary>
        /// Alters the is readonly check to also check the user has the permissions returned by this method
        /// </summary>
        protected virtual Permission[] RequiredWritePermissions
        {
            get { return new Permission[] { }; }
        }

        /// <summary>
        /// REturns true if the user has ALL required write permissiosn
        /// false other wise.
        /// </summary>
        private bool UserHasPermissions(Permission[] perms)
        {
            foreach (var perm in perms)
            {
                if (!BrokerUser.HasPermission(perm))
                {
                    return false;
                }
            }

            return true;
        }

        protected virtual Permission[] RequiredReadPermissions
        {
            get { return new Permission[] { }; }
        }

        // 2/10/2004 dd - If the page is display within other frame, this property should set to false.
        protected bool DisplayCopyRight = true;

        #region Protected properties that shared among loan pages.
        public int sFileVersion
        {
            get
            {
                int version = 0;
                string s = Request["sFileVersion"];
                if (string.IsNullOrEmpty(s) == false)
                {
                    if (int.TryParse(s, out version) == false)
                    {
                        version = 0;
                    }
                }

                return version;
            }
            set
            {
                ClientScript.RegisterHiddenField("sFileVersion", value.ToString());
            }
        }

        protected Guid LoanID
        {
            get { return RequestHelper.LoanID; }
        }

        protected Guid ApplicationID
        {
            get { return RequestHelper.GetGuid("appid", Guid.Empty); }
        }

        protected BrokerUserPrincipal BrokerUser
        {
            get
            {
                BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
                if (null == principal)
                {
                    RequestHelper.Signout(); // 10/12/2005 dd - If unable to cast to BrokerUserPrincipal redirect to log out.
                }

                return principal;
            }
        }

        protected Guid BrokerID
        {
            get { return BrokerUser.BrokerId; }
        }

        protected Guid UserID
        {
            get { return BrokerUser.UserId; }
        }

        protected Guid EmployeeID
        {
            get { return BrokerUser.EmployeeId; }
        }

        protected BrokerDB Broker
        {
            get { return m_broker; }
        }
        #endregion

        protected void AddResponseQueryString(string key, string value)
        {
            m_customQueryString[key] = value;
        }

        protected void AddResponseQueryString(string key, int value)
        {
            m_customQueryString[key] = value.ToString();
        }

        private bool m_canRead = false;
        private bool m_canWrite = false;
        private bool m_canRunPml = false;
        private bool m_hasFieldWritePermmissions = false;

        override protected void OnInit(EventArgs e)
        {
            PerformanceMonitorItem monitorItem = PerformanceMonitorItem.Current;
            if (monitorItem != null)
            {
                monitorItem.SetCustomKeyValue("sLId", LoanID.ToString());
            }

            LogPerformanceMonitorItem("BaseLoanPage.OnInit - Begin");
            RegisterJsGlobalVariables("sLId", LoanID);
            RegisterJsGlobalVariables("aAppId", ApplicationID);
            ClientScript.RegisterHiddenField("loanid", LoanID.ToString());
            ClientScript.RegisterHiddenField("applicationid", ApplicationID.ToString());

            m_customQueryString = new NameValueCollection(Request.QueryString.Count);
            foreach (string key in Request.QueryString.Keys)
            {
                m_customQueryString[key] = RequestHelper.GetSafeQueryString(key);
            }

            this.RegisterVbsScript("common.vbs");
            this.RegisterJsScript("LQBPrintFix.js");
            this.RegisterJsScript("loanframework.js");
            // 4/13/2004 dd - Will always include loanframe2.js after migrate is completed.
            // 3/23/2006 dd - Still not ready to remove this variable yet.
            if (UseNewFramework)
            {
                this.RegisterJsScript("loanframework2.js");
                int l = VirtualRoot.Length;
                string url = Request.Path.Substring(l, Request.Path.Length - l - 5) + "Service.aspx";
                RegisterService("loanedit", url);

                ClientScript.RegisterHiddenField("_PerApp", IsAppSpecific.ToString());
            }

            // 10/7/2004 kb - We need the user's broker right away.
            m_broker = this.BrokerUser.BrokerDB;

            InitializeComponent();
            base.OnInit(e);
        }

        override protected void OnPreRender(EventArgs e)
        {
            if (DisplayCopyRight)
            {
                Control lastControl = Page.Controls[Page.Controls.Count - 1];

                if (lastControl is LiteralControl)
                {
                    // This LiteralControl is parsed out of the markup. Things
                    // like the doc type declaration and the closing </body>
                    // tag are parsed from the page as LiteralControls. I don't
                    // think we need to take any action here for XSS stuff.
                    // gf opm 464965
                    LiteralControl litControl = lastControl as LiteralControl;
                    string endHtml = litControl.Text;

                    int endingBodyTag = endHtml.IndexOf("</body>");
                    if (endingBodyTag > 0)
                    {
                        litControl.Text = endHtml.Insert(endingBodyTag, CopyRightFooter);
                        DisplayCopyRight = false;
                    }
                }
            }

            ClientScript.RegisterHiddenField("PrintID", null == PDFPrintClass ? "" : PDFPrintClass.Name);
            ClientScript.RegisterHiddenField("_ReadOnly", IsReadOnly.ToString());
            if (IsRefreshLoanSummaryInfo)
            {
                this.AddInitScriptFunction("f_refreshLoanSummary");
            }

            if (IsLeadPage)
            {
                this.AddInitScriptFunction("f_disableLeadOnlyLinksAndButtons");
            }

            if (!RequestHelper.GetBool("history"))
            {
                StringBuilder sb = new StringBuilder();
                foreach (string key in m_customQueryString.Keys)
                {
                    sb.AppendFormat("{0}={1}&", key, m_customQueryString[key]);
                }
                this.AddInitScriptFunctionWithArgs("f_addToHistory", string.Format(@"{0}, {1}, {2}",
                    AspxTools.JsString(Request.Path + "?" + sb.ToString()),
                    AspxTools.JsString(PageTitle),
                    AspxTools.JsString(PageID)));
            }
            else
            {
                // Page is load by history. Need to set borrowers drop downlist to the correct borrower.
                if (ApplicationID != Guid.Empty)
                {
                    this.AddInitScriptFunctionWithArgs("f_setCurrentApplicationID", "'" + ApplicationID + "', '" + PageID + "'");
                }
            }

            if (Broker.IsEnablePTMComplianceEaseIndicator)
            {
                this.AddInitScriptFunction("f_addsSpAddrValidatorForCE");
            }

            //OPM case 10447
            //Set the initial value for Range validation purposes once the value
            //has been read from the DB.
            foreach (Control c in Controls)
            {
                setValidationRangeInitialValue(c);
            }

            base.OnPreRender(e);
        }

        private void setValidationRangeInitialValue(Control c)
        {
            System.Web.UI.WebControls.TextBox tBox = c as System.Web.UI.WebControls.TextBox;
            if (tBox != null && tBox.Attributes["_initial"] != null)
            {
                tBox.Attributes["_initial"] = AspxTools.JsStringUnquoted(tBox.Text);
            }

            foreach (Control c1 in c.Controls)
            {
                setValidationRangeInitialValue(c1);
            }
        }

        protected override void Render(HtmlTextWriter output)
        {
            base.Render(output);
            if (DisplayCopyRight)
            {
                output.Write(CopyRightFooter);
            }
        }

        private void InitializeTextBox(System.Web.UI.WebControls.TextBox o, CFieldInfoTable fieldInfoTable)
        {
            o.EnableViewState = false;

            // 08/24/06 mf. If server control has AlwaysEnable attribute, we ignore read-only status.
            if (o.Attributes["AlwaysEnable"] == null)
            {
                o.ReadOnly = o.ReadOnly || IsReadOnly;
            }

            // 3/22/2004 dd - Skip all control inherit from MeridianLinkCommonControls because those already has MaxLength buildin.
            // 7/11/2006 dd - See if there any side effect comment out MoneyTextBox PercentTextBox
            if (//o is MeridianLink.CommonControls.MoneyTextBox ||
                //o is MeridianLink.CommonControls.PercentTextBox ||
                o is MeridianLink.CommonControls.DateTextBox ||
                o is MeridianLink.CommonControls.SSNTextBox ||
                o is MeridianLink.CommonControls.ZipcodeTextBox)
                return; // Skip;

            FieldProperties p = fieldInfoTable[o.ID];
            if (null != p)
            {
                if (null != p.DbInfo && p.DbInfo.m_charMaxLen > 0)
                {
                    o.MaxLength = p.DbInfo.m_charMaxLen;
                }

                if (null != p.DbInfo && (p.DbInfo.m_minRange != 0 || p.DbInfo.m_maxRange != 0))
                {
                    decimal min = p.DbInfo.m_minRange;
                    decimal max = p.DbInfo.m_maxRange;
                    decimal def = p.DbInfo.m_defaultValue;
                    //decimal
                    // set min/max
                    // Need to see if field need min/max
                    o.Attributes.Add("min", min.ToString());
                    o.Attributes.Add("max", max.ToString());
                    o.Attributes.Add("def", def.ToString());
                    o.Attributes.Add("clu", p.DbInfo.m_clusive);
                    o.Attributes.Add("_initial", AspxTools.JsStringUnquoted(o.Text));
                    //OPM case 10447
                    //Allow the given min-max range for the field AND also its initial value
                    //however, at this point o.Text = "", therefore '_initial' is not set.
                    //It has to be set afterwards at a later stage.
                }

                if (null != p.DbInfo && p.DbInfo.m_sInputs != null)
                {
                    o.Attributes.Add("sList", p.DbInfo.m_sInputs.ToString());
                }
            }
        }

        private void InitializeDropDownList(System.Web.UI.WebControls.DropDownList ddl)
        {
            // 08/24/06 mf. If server control has AlwaysEnable attribute, we ignore read-only status.
            if (ddl.Attributes["AlwaysEnable"] == null)
            {
                ddl.Enabled = ddl.Enabled && !IsReadOnly;
            }
        }

        private void InitializeCheckBox(System.Web.UI.WebControls.CheckBox cb)
        {
            cb.Enabled = cb.Enabled && !IsReadOnly;
        }

        private void InitializeStateDropDownList(MeridianLink.CommonControls.StateDropDownList sddl)
        {
            sddl.Enabled = sddl.Enabled && !IsReadOnly;
        }

        private void InitializeHtmlInputCheckBox(System.Web.UI.HtmlControls.HtmlInputCheckBox hicb)
        {
            hicb.Disabled = hicb.Disabled || IsReadOnly;
        }

        private void InitializeHtmlInputRadioButton(System.Web.UI.HtmlControls.HtmlInputRadioButton rb)
        {
            rb.Disabled = rb.Disabled || IsReadOnly;
        }

        private void InitializeRadioButtonList(System.Web.UI.WebControls.RadioButtonList rbl)
        {
            rbl.Enabled = rbl.Enabled && !IsReadOnly;
        }

        private void InitializeHtmlTextArea(System.Web.UI.HtmlControls.HtmlTextArea ta)
        {
            if (ta.Disabled || IsReadOnly)
            {
                ta.Attributes["readOnly"] = "";
            }
            else
            {
                ta.Attributes.Remove("readOnly");
            }
        }

        private void SetTextboxMaximumLength(Control c, CFieldInfoTable fieldInfoTable)
        {
            System.Web.UI.WebControls.TextBox tb = c as System.Web.UI.WebControls.TextBox;
            if (null != tb)
            {
                InitializeTextBox(tb, fieldInfoTable);
                return;
            }

            System.Web.UI.WebControls.DropDownList ddl = c as System.Web.UI.WebControls.DropDownList;
            if (null != ddl)
            {
                InitializeDropDownList(ddl);
                return;
            }

            System.Web.UI.WebControls.CheckBox cb = c as System.Web.UI.WebControls.CheckBox;
            if (null != cb)
            {
                InitializeCheckBox(cb);
                return;
            }

            MeridianLink.CommonControls.StateDropDownList sddl = c as MeridianLink.CommonControls.StateDropDownList;
            if (null != sddl)
            {
                InitializeStateDropDownList(sddl);
                return;
            }

            System.Web.UI.HtmlControls.HtmlInputCheckBox hicb = c as System.Web.UI.HtmlControls.HtmlInputCheckBox;
            if (null != hicb)
            {
                InitializeHtmlInputCheckBox(hicb);
                return;
            }

            System.Web.UI.HtmlControls.HtmlInputRadioButton rb = c as System.Web.UI.HtmlControls.HtmlInputRadioButton;
            if (null != rb)
            {
                InitializeHtmlInputRadioButton(rb);
                return;
            }

            System.Web.UI.WebControls.RadioButtonList rbl = c as System.Web.UI.WebControls.RadioButtonList;
            if (null != rbl)
            {
                InitializeRadioButtonList(rbl);
                return;
            }

            var ta = c as System.Web.UI.HtmlControls.HtmlTextArea;
            if (null != ta)
            {
                InitializeHtmlTextArea(ta);
                return;
            }

            foreach (Control c1 in c.Controls)
            {
                SetTextboxMaximumLength(c1, fieldInfoTable);
            }
        }

        private void SetTextboxMaximumLength()
        {
            CFieldInfoTable fieldInfoTable = CFieldInfoTable.GetInstance();
            foreach (Control c in Controls)
            {
                SetTextboxMaximumLength(c, fieldInfoTable);
            }
        }

        private void PageInit(object sender, System.EventArgs a)
        {
            // 11/3/2004 kb - In an effort to consolidate access control,
            // we now direct all load edit queries to a central helper.
            // This implementation will shift, soon, to one that checks
            // both native and override permissions.
            //
            // 11/4/2004 kb - We need to load the page's access binding
            // and cache it for this postback.  The resolved permission
            // bits will tell us if the user can load the loan and
            // view/edit it.

            // 2/2/2012 dd - To avoid additional call to CPageData.InitLoad() to get sFileVersion,
            // I will piggy back the sFileVersion in valueEvaluator call.
            WorkflowOperation[] ops = GetRequiredOpsToCheck();
            IEnumerable<string> dependencyFieldList = LendingQBExecutingEngine.GetDependencyFieldsByOperation(BrokerUser.BrokerId, ops);

            List<string> newFieldList = new List<string>(dependencyFieldList);
            newFieldList.Add("sFileVersion"); // 2/2/2012 dd - Add sFileVersion to avoid pulling sFileVersion.
            #region Required additional fields to display in Loan Summary Frame.
            newFieldList.Add("sStatusT");
            newFieldList.Add("sLNm");
            newFieldList.Add("sEmployeeLoanRepName");
            newFieldList.Add("sQualTopR");
            newFieldList.Add("sQualBottomR");
            newFieldList.Add("sLtvR");
            newFieldList.Add("sCltvR");
            newFieldList.Add("sHcltvR");
            newFieldList.Add("sRateLockStatusT");
            newFieldList.Add("sLT");
            newFieldList.Add("sNoteIR");
            newFieldList.Add("sFinalLAmt");
            newFieldList.Add("sDocMagicPlanCodeId");
            newFieldList.Add("sApr");
            newFieldList.Add("sLastDiscAPR");
            newFieldList.Add("sFinMethT");
            newFieldList.Add("sIsLineOfCredit");
            newFieldList.Add("sIsAprOutOfTolerence");
            newFieldList.Add("sLoanVersionT");

            // 10/26/2015 - dd - The field sClosingCostFeeVersionT is currently not part of LOAN_FILE_CACHE. Therefore,
            //                   when include this field it will require to load from LOAN_FILE_X table. This field requires a lot
            //                   field expansion and make it a very expensive operation to include on each load.
            //                   Until we add this to LOAN_FILE_CACHE, I will have to cache this value in memory.
            //newFieldList.Add("sClosingCostFeeVersionT");
            #endregion
            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(BrokerUser.ConnectionInfo, newFieldList, LoanID);

            if (!Page.IsPostBack)
            {
                sFileVersion = valueEvaluator.GetIntValues("sFileVersion")[0];
            }

            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(BrokerUser));

            this.CacheLoanValueEvaluator(valueEvaluator);

            m_canRunPml = LendingQBExecutingEngine.CanPerform(WorkflowOperations.RunPmlForAllLoans, valueEvaluator) ||
                LendingQBExecutingEngine.CanPerform(WorkflowOperations.RunPmlForRegisteredLoans, valueEvaluator) ||
                LendingQBExecutingEngine.CanPerform(WorkflowOperations.RunPmlToStep3, valueEvaluator);

            m_canRead = LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator);
            m_canWrite = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteLoanOrTemplate, valueEvaluator);
            m_hasFieldWritePermmissions = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteField, valueEvaluator);

            foreach (WorkflowOperation op in ops)
            {
                m_opResults.Add(op, LendingQBExecutingEngine.CanPerform(op, valueEvaluator));
            }

            foreach (WorkflowOperation op in GetReasonsForMissingPriv())
            {
                bool canPerformOp = m_opResults[op];
                string reason = "";

                if (canPerformOp == false)
                {
                    reason = LendingQBExecutingEngine.GetUserFriendlyMessage(op, valueEvaluator);
                }

                m_reasons.Add(op, reason);
            }

            PerformanceMonitorItem monitorItem = PerformanceMonitorItem.Current;
            if (monitorItem != null)
            {
                // 1/16/2012 dd - Record the current status of loan.
                var list = valueEvaluator.GetIntValues("sStatusT");
                if (null != list)
                {
                    if (list.Count > 0)
                    {
                        monitorItem.SetCustomKeyValue("sStatusT", list[0].ToString());
                    }
                }
            }

            if (m_canRead)
            {
                SetupLoanSummaryInformation(valueEvaluator);
            }
        }

        private void CacheLoanValueEvaluator(LoanValueEvaluator valueEvaluator)
        {
            int currentFileVersion = valueEvaluator.GetIntValues("sFileVersion")[0];
            string key = LoanValueEvaluator.GetCacheKey(this.LoanID, currentFileVersion);
            // 10/15/2015 - dd - Cache the LoanValueEvaluator so subsequence CPageData.InitLoad will use this.
            this.Context.Items[key] = valueEvaluator;
        }
        private void SetupLoanSummaryInformation(LoanValueEvaluator valueEvaluator)
        {
            int intValue = -1;
            string stringValue = "";

            stringValue = valueEvaluator.GetSafeScalarString("sLNm", "N/A");
            RegisterJsGlobalVariables("sLNm", stringValue);

            stringValue = valueEvaluator.GetSafeScalarString("sEmployeeLoanRepName", "");
            RegisterJsGlobalVariables("sEmployeeLoanRepName", stringValue);

            stringValue = valueEvaluator.GetSafeScalarString("sQualTopR", "N/A");
            if (stringValue != "N/A")
            {
                stringValue += "%";
            }
            RegisterJsGlobalVariables("sQualTopR", stringValue);

            stringValue = valueEvaluator.GetSafeScalarString("sQualBottomR", "N/A");
            if (stringValue != "N/A")
            {
                stringValue += "%";
            }
            RegisterJsGlobalVariables("sQualBottomR", stringValue);

            stringValue = valueEvaluator.GetSafeScalarString("sLtvR", "N/A");
            if (stringValue != "N/A")
            {
                stringValue += "%";
            }
            RegisterJsGlobalVariables("sLtvR", stringValue);

            stringValue = valueEvaluator.GetSafeScalarString("sCltvR", "N/A");
            if (stringValue != "N/A")
            {
                stringValue += "%";
            }
            RegisterJsGlobalVariables("sCltvR", stringValue);

            stringValue = valueEvaluator.GetSafeScalarString("sHcltvR", "N/A");

            RegisterJsGlobalVariables("sHcltvR", stringValue);

            stringValue = valueEvaluator.GetSafeScalarString("sNoteIR", "N/A");
            if (stringValue != "N/A")
            {
                stringValue += "%";
            }
            RegisterJsGlobalVariables("sNoteIR", stringValue);

            stringValue = valueEvaluator.GetSafeScalarString("sFinalLAmt", "N/A");
            if (stringValue != "N/A")
            {
                decimal decimalValue;
                if (decimal.TryParse(stringValue, out decimalValue) == true)
                {
                    LosConvert losConvert = new LosConvert();
                    stringValue = losConvert.ToMoneyString(decimalValue, FormatDirection.ToRep);
                }
            }
            RegisterJsGlobalVariables("sFinalLAmt", stringValue);

            intValue = valueEvaluator.GetSafeScalarInt("sStatusT", -1);
            stringValue = "";
            if (intValue >= 0)
            {
                stringValue = CPageBase.sStatusT_map_rep((E_sStatusT)intValue);
            }
            RegisterJsGlobalVariables("sStatusT", stringValue);

            intValue = valueEvaluator.GetSafeScalarInt("sRateLockStatusT", -1);
            stringValue = "";
            if (intValue >= 0)
            {
                stringValue = CPageBase.sRateLockStatusT_map_rep((E_sRateLockStatusT)intValue);
            }
            RegisterJsGlobalVariables("sRateLockStatusT", stringValue);

            intValue = valueEvaluator.GetSafeScalarInt("sLT", -1);
            stringValue = "";
            if (intValue >= 0)
            {
                stringValue = CPageBase.sLT_map_rep((E_sLT)intValue);
            }
            RegisterJsGlobalVariables("sLT", stringValue);

            stringValue = valueEvaluator.GetSafeScalarString("sDocMagicPlanCodeId", "");
            RegisterJsGlobalVariables("sDocMagicPlanCodeId", stringValue);

            // 10/26/2015 - dd - Until we include sClosingCostFeeVersionT in LOAN_FILE_CACHE this will
            //                   be cache in memory.
            stringValue = GetCacheValue_sClosingCostFeeVersionT(valueEvaluator);
            RegisterJsGlobalVariables("sClosingCostFeeVersionT", stringValue);

            // OPM 236812 - TRID changes to Disclosure Pipeline Trigger for APR.
            if (valueEvaluator.HasValues("sApr", "sLastDiscAPR"))
            {
                bool sIsAprOutOfTolerence = valueEvaluator.GetBoolValues("sIsAprOutOfTolerence")[0];
                float? sLastDiscAPR = valueEvaluator.GetSafeFloatValue("sLastDiscAPR");
                float? sApr = valueEvaluator.GetSafeFloatValue("sApr");

                if (sLastDiscAPR.HasValue && sApr.HasValue && sIsAprOutOfTolerence)
                {
                    float APRDelta = Math.Abs(sApr.Value - sLastDiscAPR.Value);
                    RegisterJsGlobalVariables("APRDelta", APRDelta.ToString());
                    RegisterJsGlobalVariables("NeedsAPRAlert", "1");
                }
            }

            LoanVersionT currentVersion = (LoanVersionT)valueEvaluator.GetSafeScalarInt("sLoanVersionT", -1);
            LoanVersionT latestVersion = LoanDataMigrationUtils.GetLatestVersion();
            bool isLoanVersionTUpToDate = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(currentVersion, latestVersion);
            RegisterJsGlobalVariables("LoanVersionTUpToDate", isLoanVersionTUpToDate.ToString());
            RegisterJsGlobalVariables("LoanVersionTCurrent", (int)currentVersion);
            RegisterJsGlobalVariables("LoanVersionTLatest", (int)latestVersion);
        }

        private string GetCacheValue_sClosingCostFeeVersionT(LoanValueEvaluator valueEvaluator)
        {
            int currentFileVersion = valueEvaluator.GetIntValues("sFileVersion")[0];
            string key = this.LoanID + "_" + currentFileVersion + "_BLP_sClosingCostFeeVersionT";

            string value = MemoryCacheUtilities.GetOrAddExisting<string>(key, () =>
            {
                CPageData dataLoan = new NotEnforceAccessControlPageData(this.LoanID, new string[] { "sClosingCostFeeVersionT" });
                dataLoan.InitLoad();
                return dataLoan.sClosingCostFeeVersionT.ToString();
            },
            DateTimeOffset.Now.AddHours(6));

            return value;
        }

        private void PageLoad(object sender, System.EventArgs e)
        {
            if (m_canRead == false) //if (!m_accessBinding.CanRead)
            {
                ErrorUtilities.DisplayErrorPage(ErrorMessages.GenericAccessDenied, false, BrokerUser.BrokerId, BrokerUser.EmployeeId);
            }

            if (!UserHasPermissions(RequiredReadPermissions))
            {
                if (Broker.IsEnableBigLoanPage)
                {
                    Response.Redirect("~/newlos/BigLoanInfo.aspx?loanid=" + LoanID + "&appid=" + ApplicationID, true);
                }
                else
                {
                    Response.Redirect("~/newlos/LoanInfo.aspx?loanid=" + LoanID + "&appid=" + ApplicationID, true);
                }
                return;
            }

            // Perform permission check at this point.
            //
            // 11/2/2004 kb - We add the view loan permission check to
            // allow employees to view loans within the editor.  We need
            // to check how this editor is accessed, now that users can
            // click 'view' and get in here.  To view and be denied and
            // be told you can't edit is a little misleading.  We can
            // resolve this if customers complain.

            if (!HasEditLoanPermission() && !HasViewLoanPermission())
            {
                string errorMessage = ErrorMessages.InsufficientPermissionToEditLoan;

                this.AddInitScriptFunctionWithArgs("f_accessDenied", AspxTools.JsString(errorMessage));
                return;
            }

            // 3/22/2004 dd - Set the maximum length for a textbox.

            SetTextboxMaximumLength();

            if (!Page.IsPostBack)
            {
                try
                {
                    LoadData();
                }
                catch (PageDataAccessDenied exc)
                {
                    this.AddInitScriptFunctionWithArgs("f_accessDenied", AspxTools.JsString(exc.UserMessage));
                    return;
                }
                catch (CBaseException exc)
                {
                    ErrorUtilities.DisplayErrorPage(exc, true, BrokerID, EmployeeID);
                }
                catch (ThreadAbortException)
                {
                    // 1/16/2011 dd - Reason for this exception to occur is when the LoadData perform redirect.
                    return;
                }
                catch (Exception exc)
                {
                    CBaseException loexc = new CBaseException(ErrorMessages.LoadLoanFailed, exc);
                    ErrorUtilities.DisplayErrorPage(loexc, true, BrokerID, EmployeeID); // Log error and send email
                }
            }
            else
            {
                // 9/30/2004 kb - Need to extend the framework a tad bit so
                // that we can access loaded form variables and use them prior
                // to saving but before events are raised.

                try
                {
                    PostData();
                }
                catch (LoanFieldWritePermissionDenied exc)
                {
                    AddInitScriptFunctionWithArgs("f_displayFieldWriteDenied", AspxTools.JsString(exc.UserMessage)); return;
                }
                catch (PageDataAccessDenied exc)
                {
                    this.AddInitScriptFunctionWithArgs("f_accessDenied", AspxTools.JsString(exc.UserMessage));
                    return;
                }
                catch (CBaseException exc)
                {
                    ErrorUtilities.DisplayErrorPage(exc, true, BrokerID, EmployeeID);
                }
                catch (ThreadAbortException)
                {
                    // 1/16/2011 dd - Reason for this exception to occur is when the PostData perform redirect.
                    return;
                }
                catch (Exception exc)
                {
                    CBaseException loexc = new CBaseException(ErrorMessages.PostDataFailed, exc);
                    ErrorUtilities.DisplayErrorPage(loexc, true, BrokerID, EmployeeID);
                }

                if (!IsReadOnly && (IsUpdate || IsAlwaysSave))
                {
                    try
                    {
                        Page.Validate();
                        if (Page.IsValid)
                        {
                            SaveData();
                        }
                    }
                    catch (LoanFieldWritePermissionDenied exc)
                    {
                        AddInitScriptFunctionWithArgs("f_displayFieldWriteDenied", Server.HtmlEncode(exc.UserMessage)); return;
                    }
                    catch (CBaseException exc)
                    {
                        ErrorUtilities.DisplayErrorPage(exc, true, BrokerID, EmployeeID);
                    }
                    catch (Exception exc)
                    {
                        CBaseException loexc = new CBaseException(ErrorMessages.SaveLoanFailed, exc);
                        ErrorUtilities.DisplayErrorPage(loexc, true, BrokerID, EmployeeID);
                    }
                }

                ProcessSpecialCommand();
            }
        }

        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
            this.Load += new System.EventHandler(this.PageLoad);
            ClientScript.GetPostBackEventReference(this, ""); // DO NOT REMOVE THIS LINE.
        }

        private bool HasEditLoanPermission()
        {
            // 11/3/2004 kb - In an effort to consolidate access control,
            // we now direct all load edit queries to a central helper.
            // This implementation will shift, soon, to one that checks
            // both native and override permissions.   
            if (UserHasPermissions(RequiredWritePermissions) == false)
            {
                return false;
            }
            else
            {
                return (m_canWrite && UserHasPermissions(RequiredWritePermissions)) || m_hasFieldWritePermmissions;
            }
        }

        /// <summary>
        /// Provide test to see if user can view a loan.  We don't
        /// worry about how fresh the permission set is because we
        /// assume the permissions are refreshed with each posting
        /// of a page.  NOTE: If this ceases to be the case, then
        /// we need to get latests permissions for this test.
        /// </summary>
        private bool HasViewLoanPermission()
        {
            // 11/2/2004 kb - Check the user's permission rights.
            // If allowed to view in the editor, then return true.
            // We may limit this to just loan officers, but for
            // now, anyone with the permission can view.
            //
            // 11/4/2004 kb - We previously assumed that a user
            // who can launch the editor has read privelages.
            // No more.  We now check using the access control
            // lookup we perform at initialization.

            //av 20101 march - add page specific permission checking for read

            //in opm 48044 was asked to not check  CanViewLoanInEditor  for read only access so I removed the call to this method from the load/init method.
            // 3/19/2010 dd - Only take Permission.CanViewLoanInEditor in account if and only if user does not have write permission to whole loan file.
            // 12/6/2010 dd - OPM 60360 - Allow user to view loan in editor if they have RUN PML priviledge.
            if (m_canRead == true &&
                (m_canWrite == true || BrokerUser.HasPermission(Permission.CanViewLoanInEditor) == true || m_canRunPml) &&
                UserHasPermissions(RequiredReadPermissions))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Process command argument in __EVENTARGUMENT, 
        /// Should only handle command with target (__EVENTTARGET) = "".
        /// Why is this method needed? Because there is no way to intercept this command
        /// in current ASP.NET framework, not as I know. dd 6/6/2003
        /// The client script command to invoke this method is
        /// __doPostback('', '[cmd]');
        /// 
        /// As the name implied, after the page is finish processing save, it will redirect to new location.
        /// </summary>
        private void ProcessSpecialCommand()
        {
            if (Request.Form["__EVENTTARGET"] == "")
            {
                string cmd = Request.Form["__EVENTARGUMENT"];

                if (cmd != "")
                {
                    if (cmd == "gotodesktop")
                    {
                        this.AddInitScriptFunction("f_goBackDesktop");
                    }
                    else if (cmd == "logout")
                    {
                        this.AddInitScriptFunction("f_logout");
                    }
                    else if (cmd == "closewindow")
                    {
                        this.AddInitScriptFunction("closeWindow", "function closeWindow() { window.parent.close(); }");
                    }
                    else if (cmd == "duplicateloan")
                    {
                        DuplicateLoanFile(false);
                    }
                    else if (cmd == "createloantemplate")
                    {
                        CreateTemplateFromLoanFile();
                    }
                    else if (cmd == "exportloan")
                    {
                        this.AddInitScriptFunctionWithArgs("f_exportLoan", "'" + LoanID + "'");
                    }
                    else if (cmd == "createsandbox")
                    {
                        DuplicateLoanFile(true);
                    }
                    else if (cmd == "createtest")
                    {
                        CreateTestLoanFile();
                    }
                }
            }
        }

        private void DuplicateLoanFile(bool isSandbox)
        {
            if (PrincipalFactory.CurrentPrincipal.HasPermission(Permission.CanDuplicateLoans) == false && !isSandbox)
            {
                throw new AccessDenied("Cannot duplicate loan because CanDuplicateLoans is false for user");
            }
            try
            {
                Guid newLoanId;
                string loanName;
                if (isSandbox)
                {
                    CLoanFileCreator creator = CLoanFileCreator.GetCreator(BrokerUser, LendersOffice.Audit.E_LoanCreationSource.Sandbox);
                    newLoanId = creator.CreateSandboxFile(LoanID);
                    loanName = creator.LoanName;

                    if (Broker.SandboxScratchLOXml.Length > 0)
                    {
                        //we should probably do somethign with errors.
                        LOFormatImporter.Import(newLoanId, PrincipalFactory.CurrentPrincipal, Broker.SandboxScratchLOXml, true);
                    }
                }
                else
                {
                    // 9/28/2004 kb - Added check for template source so we can
                    // properly duplicate a template file.  Until the rules change,
                    // we shall make new templates when duplicating existing ones.

                    CLoanFileCreator creator = CLoanFileCreator.GetCreator(BrokerUser, LendersOffice.Audit.E_LoanCreationSource.FileDuplication);
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@LoanID", LoanID)
                                                };
                    bool isTemplate = Convert.ToBoolean(StoredProcedureHelper.ExecuteScalar(BrokerUser.BrokerId, "IsLoanTemplate", parameters));

                    if (isTemplate)
                    {
                        newLoanId = creator.DuplicateLoanTemplate(this.LoanID);
                    }
                    else
                    {
                        newLoanId = creator.DuplicateLoanFile(this.LoanID);
                    }

                    loanName = creator.LoanName;
                }

                this.AddInitScriptFunctionWithArgs("f_openLoan", AspxTools.JsString(newLoanId) + "," + AspxTools.JsString(loanName));
            }
            catch (CBaseException exc)
            {
                ErrorUtilities.DisplayErrorPage(exc, true, BrokerID, EmployeeID);
            }
            catch (Exception exc)
            {
                CBaseException loexc = new CBaseException(ErrorMessages.LoanCopyFailed, exc);
                ErrorUtilities.DisplayErrorPage(loexc, true, BrokerID, EmployeeID);
            }

            LoadData();
        }

        private void CreateTestLoanFile()
        {
            if (PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCreatingTestLoans) == false)
            {
                throw new AccessDenied("Cannot create test loan because AllowCreatingTestLoans is false for user");
            }
            try
            {
                Guid loanID;
                string loanName;
                CLoanFileCreator creator = CLoanFileCreator.GetCreator(BrokerUser, LendersOffice.Audit.E_LoanCreationSource.CreateTest);
                loanID = creator.DuplicateLoanIntoTestFile(this.LoanID);
                loanName = creator.LoanName;

                this.AddInitScriptFunctionWithArgs("f_openLoan", AspxTools.JsString(loanID) + "," + AspxTools.JsString(loanName));
            }
            catch (CBaseException exc)
            {
                ErrorUtilities.DisplayErrorPage(exc, true, BrokerID, EmployeeID);
            }

            LoadData();
        }

        private void CreateTemplateFromLoanFile()
        {
            try
            {
                CLoanFileCreator creator = CLoanFileCreator.GetCreator(BrokerUser, LendersOffice.Audit.E_LoanCreationSource.FromTemplate);
                Guid sLId = creator.CreateTemplateFromLoan(this.LoanID);
                string loanName = creator.LoanName;
                this.AddInitScriptFunctionWithArgs("f_openLoan", AspxTools.JsString(sLId) + "," + AspxTools.JsString(loanName));
            }
            catch (CBaseException exc)
            {
                ErrorUtilities.DisplayErrorPage(exc, true, BrokerID, EmployeeID);
            }
            catch (Exception exc)
            {
                CBaseException loexc = new CBaseException(ErrorMessages.CreateTemplateFailed, exc);
                ErrorUtilities.DisplayErrorPage(loexc, true, BrokerID, EmployeeID);
            }

            LoadData();
        }

        protected virtual void LoadData()
        {
        }

        protected virtual void PostData()
        {
        }

        protected virtual void SaveData()
        {
        }
    }
}
