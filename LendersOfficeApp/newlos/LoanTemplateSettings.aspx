<%@ Page language="c#" Codebehind="LoanTemplateSettings.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.LoanTemplateSettings" enableViewState="False"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>LoanTemplateSettings</title>
  </HEAD>
  <body MS_POSITIONING="FlowLayout" class="EditBackground">
	
    <form id="LoanTemplateSettings" method="post" runat="server">
    <table cellpadding="0" cellspacing="0" border="0" width="400">
      <tr>
        <td class="MainRightHeader">Loan Template Settings</td>
      </tr>
      <tr>
        <td style="padding-left: 5px">
          <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
              <td nowrap></td>
              <td nowrap></td>
            </tr>
            <tr>
              <td nowrap class="FieldLabel">Assign new loans created from this template to this branch&nbsp;&nbsp;</td>
              <td nowrap>
                <asp:DropDownList ID="sBranchIdForNewFileFromTemplate" runat="server" />
              </td>
            </tr>
            <tr>
              <td colspan="2" nowrap style="padding-left: 20px"><a href="/help/faq/HowisaloansOriginationBra.aspx" target="_blank">How does the default option work?</a></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    </form>
	
  </body>
</HTML>
