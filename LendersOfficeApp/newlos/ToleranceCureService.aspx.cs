﻿#region Generated Code
namespace LendersOfficeApp.newlos
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Service Item for Tolerance Cure Page.
    /// </summary>
    public partial class ToleranceCureServiceItem : AbstractBackgroundServiceItem
    {
        /// <summary>
        /// Constructs CPageData object.
        /// </summary>
        /// <param name="loanId">The Loan Id.</param>
        /// <returns>CPageData object.</returns>
        protected override DataAccess.CPageData ConstructPageDataClass(Guid loanId)
        {
            return CPageData.CreateUsingSmartDependency(loanId, typeof(ToleranceCureServiceItem));
        }

        /// <summary>
        /// Load data from loan to page.
        /// </summary>
        /// <param name="dataLoan">The Loan data.</param>
        /// <param name="dataApp">The App data.</param>
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            this.SetResult("sToleranceZeroPercentCureLckd", dataLoan.sToleranceZeroPercentCureLckd);
            this.SetResult("sToleranceZeroPercentCure", dataLoan.sToleranceZeroPercentCure_rep);

            this.SetResult("sToleranceTenPercentCureLckd", dataLoan.sToleranceTenPercentCureLckd);
            this.SetResult("sToleranceTenPercentCure", dataLoan.sToleranceTenPercentCure_rep);

            this.SetResult("sToleranceCure", dataLoan.sToleranceCure_rep);
        }

        /// <summary>
        /// Bind data from page to loan.
        /// </summary>
        /// <param name="dataLoan">The Loan data.</param>
        /// <param name="dataApp">The App data.</param>
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sToleranceZeroPercentCureLckd = this.GetBool("sToleranceZeroPercentCureLckd");
            dataLoan.sToleranceZeroPercentCure_rep = this.GetString("sToleranceZeroPercentCure");

            dataLoan.sToleranceTenPercentCureLckd = this.GetBool("sToleranceTenPercentCureLckd");
            dataLoan.sToleranceTenPercentCure_rep = this.GetString("sToleranceTenPercentCure");
        }
    }

    /// <summary>
    /// Tolerance Cure Service Page.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed.")]
    public partial class ToleranceCureService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Initialize function for ToleranceCureService.
        /// </summary>
        protected override void Initialize()
        {
            this.AddBackgroundItem(string.Empty, new ToleranceCureServiceItem());
        }
    }
}