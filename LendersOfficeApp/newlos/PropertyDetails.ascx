﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="PropertyDetails.ascx.cs" Inherits="LendersOfficeApp.newlos.PropertyDetailsUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="DataAccess"%>

<style type="text/css">
    .address {
        width: 359px;
    }

    .city {
        width: 258px;
    }

    .Width200px {

    }
</style>

<script type="text/javascript">
    function _init() {
        var valuationMethodDDL = <%= AspxTools.JsGetElementById(sSpValuationMethodT) %>;
        var valuationMethod = valuationMethodDDL.options[valuationMethodDDL.selectedIndex].value;
      
        if (valuationMethod != <%= AspxTools.JsNumeric(Convert.ToInt32(E_sSpValuationMethodT.AutomatedValuationModel)) %>) {
            avmModelRow.style.display = 'none';
        }
        else {
            avmModelRow.style.display = '';
        }
      
        if (valuationMethod == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sSpValuationMethodT.None)) %>) {
            $('.appraisalIdRow').hide();
            valuationDateRow.style.display = 'none';
            gseProgramIdRow.style.display = '';
        }
        else {
            $('.appraisalIdRow').show();
            valuationDateRow.style.display = '';
            gseProgramIdRow.style.display = 'none';
        }
        
        var units = parseInt(<%= AspxTools.JsGetElementById(sUnitsNum) %>.value);
        <%= AspxTools.JsGetElementById(sSpUnit1BedroomsCount) %>.readOnly = units < 1;
        <%= AspxTools.JsGetElementById(sSpUnit1EligibleRent) %>.readOnly = units < 1;
        <%= AspxTools.JsGetElementById(sSpUnit2BedroomsCount) %>.readOnly = units < 2;
        <%= AspxTools.JsGetElementById(sSpUnit2EligibleRent) %>.readOnly = units < 2;
        <%= AspxTools.JsGetElementById(sSpUnit3BedroomsCount) %>.readOnly = units < 3;
        <%= AspxTools.JsGetElementById(sSpUnit3EligibleRent) %>.readOnly = units < 3;
        <%= AspxTools.JsGetElementById(sSpUnit4BedroomsCount) %>.readOnly = units < 4;
        <%= AspxTools.JsGetElementById(sSpUnit4EligibleRent) %>.readOnly = units < 4;
        resetInvestorDDLByRefinanceProgram(true);
        onPropertyTypeChange();
    }
    function resetInvestorDDLByRefinanceProgram(isInit)
    {
        var investorDdl = <%= AspxTools.JsGetElementById(sSpInvestorCurrentLoanT) %>;
        var refiProgDdl = <%= AspxTools.JsGetElementById(sSpGseRefiProgramT) %>;
        
        if(refiProgDdl.value == <%=AspxTools.JsString(E_sSpGseRefiProgramT.DURefiPlus) %> ||
            refiProgDdl.value == <%=AspxTools.JsString(E_sSpGseRefiProgramT.RefiPlus) %>)
        {
            investorDdl.selectedIndex = <%=AspxTools.JsNumeric((int)E_sSpInvestorCurrentLoanT.FannieMae) %>;
            investorDdl.disabled = true;
        }
        else if(refiProgDdl.value ==  <%=AspxTools.JsString(E_sSpGseRefiProgramT.ReliefRefiOpenAccess) %> ||
            refiProgDdl.value == <%=AspxTools.JsString(E_sSpGseRefiProgramT.ReliefRefiSameServicer) %>)
        {
            investorDdl.selectedIndex = <%=AspxTools.JsNumeric((int)E_sSpInvestorCurrentLoanT.FreddieMac) %>;
            investorDdl.disabled = true;
        }
        else{
            if(isInit)  
            {
            }else{
            investorDdl.disabled = false;
            investorDdl.selectedIndex = <%= AspxTools.JsNumeric((int)E_sSpInvestorCurrentLoanT.Unknown) %>;
            }
        }
    }

    function onPropertyTypeChange() {
        var manufacturedTypes = [
            <%=AspxTools.JsString(E_sGseSpT.ManufacturedHomeCondominium)%>,
            <%=AspxTools.JsString(E_sGseSpT.ManufacturedHomeMultiwide)%>,
            <%=AspxTools.JsString(E_sGseSpT.ManufacturedHousing)%>,
            <%=AspxTools.JsString(E_sGseSpT.ManufacturedHousingSingleWide)%>
        ];

        var propertyType = <%=AspxTools.JQuery(this.sGseSpT)%>.val();
        $('#HomeIsMhAdvantageRow').toggle(manufacturedTypes.indexOf(propertyType) !== -1);
    }

    function doAfterDateFormat(e)
    {
        if(e.id.indexOf("sSpValuationEffectiveD") !== -1)
        {
            refreshCalculation();
        }
    }
</script> 
<table class="InsetBorder FieldLabel" cellSpacing="0" cellPadding="0" border="0">
    <tr>
        <td class="FormTableSubHeader" colspan="6">
            Property Information
        </td>
    </tr>
    <tr>
        <td class="FieldLabel" nowrap>
            Property Address
        </td>
        <td nowrap>
            <asp:TextBox ID="sSpAddr" CssClass="address" MaxLength="60" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td nowrap>
        </td>
        <td nowrap>
            <asp:TextBox ID="sSpCity" CssClass="city" MaxLength="72" runat="server"></asp:TextBox><ml:StateDropDownList ID="sSpState" runat="server" IncludeTerritories="false"></ml:StateDropDownList>
            <ml:ZipcodeTextBox ID="sSpZip" runat="server" preset="zipcode"></ml:ZipcodeTextBox>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel" nowrap>
            Property Type
        </td>
        <td>
            <asp:DropDownList ID="sGseSpT" runat="server" onchange="refreshCalculation();" />
        </td>
    </tr>
    <tr id="HomeIsMhAdvantageRow">
        <td class="FieldLabel">Is home MH Advantage?</td>
        <td>
            <asp:DropDownList runat="server" ID="sHomeIsMhAdvantageTri"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel" nowrap>
            New Construction
        </td>
        <td>
            <input type="checkbox" id="sIsNewConstruction" runat="server"/>
        </td>
    </tr>
    <tr>
        <td class="FormTableSubHeader" colspan="6">
            Living Unit Details (2-4 unit and investment properties)
        </td>
    </tr>
    <tr>
        <td>
            Number of Units
            <asp:TextBox ID="sUnitsNum" runat="server" Width="40px" onchange="refreshCalculation();"></asp:TextBox>
        </td>
        <td></td>
        <td>
            Bedrooms
        </td>
        <td>
            Eligible Rent
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            Unit #1
        </td>
        <td>
            <asp:TextBox ID="sSpUnit1BedroomsCount" runat="server" Width="40px" onchange="refreshCalculation();"></asp:TextBox>
        </td>
        <td>
            <ml:moneytextbox id="sSpUnit1EligibleRent" runat="server" onchange="refreshCalculation();" preset="money" width="76px"></ml:moneytextbox>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            Unit #2
        </td>
        <td>
            <asp:TextBox ID="sSpUnit2BedroomsCount" runat="server" Width="40px" onchange="refreshCalculation();"></asp:TextBox>
        </td>
        <td>
            <ml:moneytextbox id="sSpUnit2EligibleRent" runat="server" onchange="refreshCalculation();" preset="money" width="76px"></ml:moneytextbox>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            Unit #3
        </td>
        <td>
            <asp:TextBox ID="sSpUnit3BedroomsCount" runat="server" Width="40px" onchange="refreshCalculation();"></asp:TextBox>
        </td>
        <td>
            <ml:moneytextbox id="sSpUnit3EligibleRent" runat="server" onchange="refreshCalculation();" preset="money" width="76px"></ml:moneytextbox>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            Unit #4
        </td>
        <td>
            <asp:TextBox ID="sSpUnit4BedroomsCount" runat="server" Width="40px" onchange="refreshCalculation();"></asp:TextBox>
        </td>
        <td>
            <ml:moneytextbox id="sSpUnit4EligibleRent" runat="server" onchange="refreshCalculation();" preset="money" width="76px"></ml:moneytextbox>
        </td>
    </tr>
    <tr>
        <td class="FormTableSubHeader" colspan="6">
            Project Details
        </td>
    </tr>
    <tr>
        <td>
            Project Name
        </td>
        <td colspan="3">
            <asp:TextBox ID="sProjNm" runat="server" onchange="refreshCalculation();"></asp:TextBox>
        </td>
        <td>
            Project Status
        </td>
        <td>
            <asp:DropDownList ID="sSpProjectStatusT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            CPM Project ID# (if any)
        </td>
        <td colspan="3">
            <asp:TextBox ID="sCpmProjectId" runat="server" onchange="refreshCalculation();"></asp:TextBox>
        </td>
        <td>
            Project Attachment Type
        </td>
        <td>
            <asp:DropDownList ID="sSpProjectAttachmentT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
        <td colspan="3">
            &nbsp;
        </td>
        <td>
            Project Design Type
        </td>
        <td>
            <asp:DropDownList ID="sSpProjectDesignT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            Freddie Mac Project Classification
        </td>
        <td colspan="3">
            <asp:DropDownList ID="sSpProjectClassFreddieT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
        </td>
        <td>
            Project Dwelling Unit Count
        </td>
        <td>
            <asp:TextBox ID="sSpProjectDwellingUnitCount" runat="server" onchange="refreshCalculation();"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Fannie Mae Project Classification
        </td>
        <td colspan="3">
            <asp:DropDownList ID="sSpProjectClassFannieT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
        </td>
        <td>
            Project Dwelling Units Sold Count
        </td>
        <td>
            <asp:TextBox ID="sSpProjectDwellingUnitSoldCount" runat="server" onchange="refreshCalculation();"></asp:TextBox>
        </td>
    </tr>
    <tbody id="AppraisalSection" class="appraisal-section" runat="server">
        <tr>
            <td class="FormTableSubHeader" colspan="6">Appraisal
            </td>
        </tr>
        <tr>
            <td>Property valuation method
            </td>
            <td colspan="3">
                <asp:DropDownList ID="sSpValuationMethodT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
            </td>
            <td class="fha-version">Property valuation amount
            </td>
            <td class="fha-version">
                <ml:moneytextbox id="sApprVal" runat="server" onchange="refreshCalculation();" preset="money" width="76px"></ml:moneytextbox>
            </td>
        </tr>
        <tr id="avmModelRow">
            <td>AVM Model
            </td>
            <td colspan="3">
                <asp:DropDownList ID="sSpAvmModelT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
            </td>
        </tr>
        <tr id="valuationDateRow">
            <td>Property valuation effective date
            </td>
            <td colspan="3">
                <ml:DateTextBox ID="sSpValuationEffectiveD" runat="server" Width="75" onchange="date_onblur(null, this);" />
            </td>
        </tr>
        <tr class="fha-version appraisalIdRow">
            <td>
                <span >FHA Doc File ID</span>
            </td>
            <td colspan="3">
                <asp:TextBox ID="sSpFhaDocId" runat="server" onchange="refreshCalculation();"></asp:TextBox>
            </td>
            <td>Date submitted to FHA
            </td>
            <td>
                <span >
                    <ml:DateTextBox ID="sSpSubmittedToFhaD" runat="server" preset="date"></ml:DateTextBox>
                </span>
            </td>
        </tr>
        <tr class="appraisalIdRow">
            <td>
                <span >UCDP Appraisal ID</span>
            </td>
            <td colspan="3">
                <asp:TextBox ID="sSpAppraisalId" runat="server" onchange="refreshCalculation();"></asp:TextBox>
            </td>
            <td>Date submitted to UCDP
            </td>
            <td>
                <span >
                    <ml:DateTextBox ID="sSpSubmittedToUcdpD" runat="server" preset="date"></ml:DateTextBox>
                </span>
            </td>
        </tr>
        <tr>
            <td>Appraisal Form Type
            </td>
            <td colspan="4">
                <asp:DropDownList ID="sSpAppraisalFormT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
            </td>
        </tr>
    </tbody>
    <tbody id="CollateralUnderwriterSection" class="collateral-underwriter-section" runat="server">
        <tr>
            <td class="FormTableSubHeader" colspan="6">Collateral Underwriter</td>
        </tr>
        <tr>
            <td>CU Score</td>
            <td colspan="4">
                <asp:TextBox ID="sSpCuScore" runat="server" Width="40px" preset="curiskscore"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Overvaluation Risk</td>
            <td>
                <asp:DropDownList ID="sSpOvervaluationRiskT" runat="server"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Property Eligibility Risk</td>
            <td>
                <asp:DropDownList ID="sSpPropertyEligibilityRiskT" runat="server"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Appraisal Quality Risk</td>
            <td>
                <asp:DropDownList ID="sSpAppraisalQualityRiskT" runat="server"></asp:DropDownList>
            </td>
        </tr>
    </tbody>
    <tr>
        <td class="FormTableSubHeader" colspan="6">
            Existing Subject Property Loan
        </td>
    </tr>
    <tr>
        <td>
            Existing Loan: FHA Case Number
        </td>
        <td colspan="3">
            <asp:TextBox ID="sSpFhaCaseNumCurrentLoan" runat="server" onchange="refreshCalculation();"></asp:TextBox>
            <asp:TextBox ID="sFHAPreviousCaseNum" runat="server" onchange="refreshCalculation();"></asp:TextBox>
        </td>
        <td>
            Existing Loan: FHA Endorsement Date
        </td>
        <td>
            <ml:DateTextBox ID="sFhaEndorsementD" runat="server" preset="date"></ml:DateTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Existing Loan: FHA UFMIP Amount
        </td>
        <td colspan="3">
            <ml:moneytextbox id="sFhaUfmipAmt" runat="server" preset="money" width="86px"></ml:moneytextbox>
        </td>
        <td>
            Existing Loan: Investor
        </td>
        <td>
            <asp:DropDownList ID="sSpInvestorCurrentLoanT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
        </td>
    </tr>
     <tr>
        <td>Existing Loan: Amortization Type</td>
        <td colspan ="3">
            <asp:DropDownList runat="server" ID="sLoanBeingRefinancedAmortizationT"></asp:DropDownList>
        </td>
        <td>Existing Loan: Lien Position</td>
        <td>
            <asp:DropDownList runat="server" ID="sLoanBeingRefinancedLienPosT"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>Existing Loan: Remaining Term (mths)</td>
        <td colspan="3">
            <asp:TextBox runat="server" ID="sLoanBeingRefinancedRemainingTerm" CssClass="minicount" onchange="refreshCalculation();"></asp:TextBox>
        </td>
        <td>Existing Loan: Interest Rate</td>
        <td>
            <ml:PercentTextBox runat="server" ID="sLoanBeingRefinancedInterestRate" preset="percent"></ml:PercentTextBox>
        </td>
    </tr>
    <tr>
        <td>Existing Loan: LTV</td>
        <td colspan="3">
            <ml:PercentTextBox runat="server" ID="sLoanBeingRefinancedLtvR" preset="percent"></ml:PercentTextBox>
        </td>
        <td>Existing Loan: Total of Payments</td>
        <td>
            <input type="text" runat="server" id="sLoanBeingRefinancedTotalOfPayments" preset="money" title="The total the borrower will have paid after making all payments of principal, interest, and mortgage or guaranty insurance (if applicable), as scheduled" />
        </td>
    </tr>
    <tr>
        <td>
            Existing Loan: FNMA/FHLMC Loan Number
        </td>
        <td colspan="3">
            <asp:TextBox ID="sSpGseLoanNumCurrentLoan" runat="server" onchange="refreshCalculation();"></asp:TextBox>
        </td>
        <td class="width-200">Existing Loan: Was used to construct, alter, or repair the home</td>
        <td>
            <asp:CheckBox runat="server" ID="sLoanBeingRefinancedUsedToConstructAlterRepair" />
        </td>
    </tr>
    <tr>
        <td>
            FNMA/FHLMC/FHLB Refinance Program
        </td>
        <td colspan="3">
            <asp:DropDownList ID="sSpGseRefiProgramT" runat="server" onchange="resetInvestorDDLByRefinanceProgram(); refreshCalculation();"></asp:DropDownList>
        </td>
    </tr>
    <tr id="gseProgramIdRow">
        <td>
            FNMA/FHLMC Collateral Program ID
        </td>
        <td colspan="3">
            <asp:DropDownList ID="sSpGseCollateralProgramT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
        </td>
    </tr>
    <tbody ID="ManufacturedHousingPanel" runat="server" enableviewstate="false" style="display:none">
    <tr>
        <td class="FormTableSubHeader" colspan="6">
            Manufactured Housing
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">Make</td>
        <td colspan="3">
            <asp:TextBox ID='sManufacturedHomeMake' runat='server'></asp:TextBox>
        </td>
        <td class="FieldLabel">Model</td>
        <td>
            <asp:TextBox ID='sManufacturedHomeModel' runat='server'></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">Year</td>
        <td colspan="3">
            <asp:TextBox ID='sManufacturedHomeYear' runat='server'></asp:TextBox>
        </td>
        <td class="FieldLabel">New/Used</td>
        <td>
            <asp:DropDownList ID='sManufacturedHomeConditionT' runat='server'></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">Length</td>
        <td colspan="3">
            <asp:TextBox ID='sManufacturedHomeLengthFeet' Width='8em' runat='server'></asp:TextBox>
            &nbsp;
            feet
        </td>
        <td class="FieldLabel">Width</td>
        <td>
            <asp:TextBox ID='sManufacturedHomeWidthFeet' Width='8em' runat='server'></asp:TextBox>
            &nbsp;
            feet
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">Manufacturer</td>
        <td colspan="3">
            <asp:TextBox ID='sManufacturedHomeManufacturer' runat='server'></asp:TextBox>
        </td>
        <td class="FieldLabel">Serial #</td>
        <td>
            <asp:TextBox ID='sManufacturedHomeSerialNumber' runat='server'></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">HUD Label #</td>
        <td colspan="3">
            <asp:TextBox ID='sManufacturedHomeHUDLabelNumber' runat='server'></asp:TextBox>
        </td>
        <td class="FieldLabel">Anchored</td>
        <td>
            <asp:DropDownList ID='sManufacturedHomeAttachedToFoundation' runat="server"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">Certificate of Title #</td>
        <td colspan="3">
            <asp:TextBox ID='sManufacturedHomeCertificateofTitle' runat='server'></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">Certificate of Title Type</td>
        <td colspan="5">
            <asp:DropDownList ID='sManufacturedHomeCertificateofTitleT' runat='server' Width="25em"></asp:DropDownList>
        </td>
    </tr>
    </tbody>
    
    <tbody ID="CooperativePanel" runat="server" enableviewstate="false" style="display: none">
    <tr>
        <td class="FormTableSubHeader" colspan="6">
            Cooperative
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">Apartment Number</td>
        <td colspan="3">
            <asp:TextBox ID='sCooperativeApartmentNumber' runat='server'></asp:TextBox>
        </td>
        <td class="FieldLabel">Number of Shares</td>
        <td>
            <asp:TextBox ID='sCooperativeNumberOfShares' runat='server'></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">Lease Date</td>
        <td colspan="3">
            <ml:DateTextBox ID='sCooperativeLeaseD' runat='server'></ml:DateTextBox>
        </td>
        <td class="FieldLabel">Lease Assign Date</td>
        <td>
            <ml:DateTextBox ID='sCooperativeLeaseAssignD' runat='server'></ml:DateTextBox>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">Lien Search Date</td>
        <td colspan="3">
            <ml:DateTextBox ID='sCooperativeLienSearchD' runat='server'></ml:DateTextBox>
        </td>
        <td class="FieldLabel">Lien Search Number</td>
        <td>
            <asp:TextBox ID='sCooperativeLienSearchNumber' runat='server'></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">Form Of Ownership</td>
        <td colspan="3">
            <asp:DropDownList ID='sCooperativeFormOfOwnershipT' runat='server'></asp:DropDownList>
        </td>
        <td class="FieldLabel">Stock Certificate Number</td>
        <td>
            <asp:TextBox ID='sCooperativeStockCertNumber' runat='server'></asp:TextBox>
        </td>
    </tr>
    </tbody>
    
    <tbody ID="LeaseholdPanel" runat="server" enableviewstate="false">
    <tr>
        <td class="FormTableSubHeader" colspan="6">
            Leasehold
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">Lease Date</td>
        <td colspan="3">
            <ml:DateTextBox ID='sLeaseholdLeaseD' runat='server'></ml:DateTextBox>
        </td>
        <td>Lease Expiration Date</td>
        <td>
            <ml:DateTextBox ID='sLeaseHoldExpireD' runat='server' disableYearLimit='true'></ml:DateTextBox>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">Record Date</td>
        <td colspan="3">
            <ml:DateTextBox ID='sLeaseholdRecordD' runat='server'></ml:DateTextBox>
        </td>
        <td class="FieldLabel">Instrument Number</td>
        <td>
            <asp:TextBox ID='sLeaseHoldInstrumentNumber' runat='server'></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">Holder Name</td>
        <td colspan="5">
            <asp:TextBox ID='sLeaseholdHolderName' runat='server' Width="25em"></asp:TextBox>
        </td>
    </tr>
    </tbody>
    
    <tbody ID="PurchaseContractPanel" runat="server" enableviewstate="false">
    <tr>
        <td class="FormTableSubHeader" colspan="6">
            Purchase Contract Dates
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">Purchase Contract Date</td>
        <td colspan="5">
            <ml:DateTextBox ID='sPurchaseContractDate' runat='server'></ml:DateTextBox>
        </td>
    </tr>
     <tr class="FieldLabel">
         <td>
             Financing Contingency Exp. Date
         </td>
         <td colspan="3">
             <ml:DateTextBox ID="sFinancingContingencyExpD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
         </td>
         <td>
             Extension Date
         </td>
         <td>
             <ml:DateTextBox ID="sFinancingContingencyExtensionExpD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
         </td>
     </tr>
    </tbody>
    
    <tbody ID="DeedPanel" runat="server" enableviewstate="false">
    <tr>
        <td class="FormTableSubHeader" colspan="6">
            Deed
        </td>
    </tr>
    <tr>
        <td>
            Deed Generated
        </td>
        <td>
            <ml:DateTextBox ID='sDeedGeneratedD' runat="server"></ml:DateTextBox>
        </td>
    </tr>
    </tbody>
</table>