using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.ConfigSystem;
using LendersOffice.ConfigSystem.Operations;
using LendersOffice.Constants;
using LendersOffice.Conversions;
using LendersOffice.Integration.GenericFramework;
using LendersOffice.Integration.OCR;
using LendersOffice.Rolodex;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos
{
    public partial class LoanUtilitiesService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "BatchDelete":
                    BatchDelete();
                    break;
                case "DuplicateLoanFile":
                    DuplicateLoanFile();
                    break;
                case "CreateSandbox":
                    CreateSandbox();
                    break;
                case "GetReasonsUserCannotEdit":
                    GetReasonsUserCannotEdit();
                    break;
                case "GetRolesOfDisabledOfficialContacts":
                    GetRolesOfDisabledOfficialContacts();
                    break;
                case "GetRateLockStatus":
                    GetRateLockStatus();
                    break;
                case "GetGenericFrameworkVendors":
                    GetGenericFrameworkVendors();
                    break;
                case "LoadGenericFrameworkVendor":
                    LoadGenericFrameworkVendor();
                    break;
                case "GetOCRPortalReviewLink":
                    GetOCRPortalReviewLink();
                    break;
            }
        }

        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private void GetOCRPortalReviewLink()
        {
            if (!BrokerUser.BrokerDB.IsOCREnabled || !BrokerUser.BrokerDB.OCRHasConfiguredAccount)
            {
                SetResult("Error", "OCR is not configured.");
                return;
            }

            OCRService service = new OCRService(BrokerUser);
            SetResult("URL", service.GetDocumentReviewPortalUrl());
        }

        private void BatchDelete()
        {
            bool hasError = false;
            string userMsg = string.Empty;
            List<string> errorLoanList = new List<string>();
            try
            {
                for (int i = 0, n = GetInt("Count"); i < n; i++)
                {
                    Guid loanID = GetGuid("LoanID_" + i);
                    try
                    {
                        Tools.DeclareLoanFileInvalid(BrokerUser, loanID, true, false);
                    }
                    catch (LoanDeletedPermissionDenied exc)
                    {
                        hasError = true;
                        errorLoanList.Add(exc.sLNm);
                    }
                }
            }
            catch (CBaseException)
            {
                hasError = true;
                userMsg = "Failed. Unable to delete specified loans.";
            }

            SetResult("Error", hasError);
            if (errorLoanList.Count > 0)
            {
                SetResult("UserMsg", "You do not have permission to delete the following loan files: " + Environment.NewLine + string.Join(Environment.NewLine, errorLoanList.ToArray()));
            }
            else
            {
                SetResult("UserMsg", userMsg);
            }
        }

        private void GetRolesOfDisabledOfficialContacts()
        {
            var loanID = GetGuid("sLId");

            var dataLoan = new CPageData(loanID, new string[]{"sDisabledOfficialContacts"});
            dataLoan.InitLoad();

            var disabledOfficialContacts = dataLoan.sDisabledOfficialContacts;
            var rolesOfDisabledContacts = from a in disabledOfficialContacts
                                          select RolodexDB.GetAgentType(a.AgentRoleT, a.OtherAgentRoleTDesc);

            SetResult("result", string.Join(", ", rolesOfDisabledContacts.ToArray()));
        }

        private void DuplicateLoanFile()
        {
            if (PrincipalFactory.CurrentPrincipal.HasPermission(Permission.CanDuplicateLoans) == false)
            {
                SetResult("Error", "True");
                return;
            }

            try
            {
                Guid loanID = GetGuid("LoanID");
                BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

                CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.FileDuplication);
                Guid newLoanID = creator.DuplicateLoanFile(sourceLoanId: loanID);

                SetResult("Error", "False");
                SetResult("NewLoanID", newLoanID);
                SetResult("LoanName", creator.LoanName);
            }
            catch (CBaseException exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                SetResult("Error", "True");
            }
        }

        private void CreateSandbox()
        {
            if (PrincipalFactory.CurrentPrincipal.HasPermission(Permission.CanDuplicateLoans) == false)
            {
                SetResult("Error", "True");
                return;
            }

            try
            {
                Guid loanID = GetGuid("LoanID");
                BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

                CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.Sandbox);
                Guid newLoanID = creator.CreateSandboxFile(loanID);

                BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);
                if (string.IsNullOrEmpty(brokerDB.SandboxScratchLOXml) == false)
                {
                    LOFormatImporter.Import(loanID, principal, brokerDB.SandboxScratchLOXml, true);
                }

                SetResult("Error", "False");
                SetResult("NewLoanID", newLoanID);
                SetResult("LoanName", creator.LoanName);
            }
            catch (CBaseException exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                SetResult("Error", "True");
            }
        }

        private void GetReasonsUserCannotEdit()
        {
            Guid loanID = GetGuid("LoanID");
            LoanValueEvaluator eval = new LoanValueEvaluator(BrokerUserPrincipal.CurrentPrincipal.ConnectionInfo, BrokerUserPrincipal.CurrentPrincipal.BrokerId, loanID, WorkflowOperations.WriteLoanOrTemplate);
            eval.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(BrokerUserPrincipal.CurrentPrincipal));
            IReadOnlyCollection<string> items = LendingQBExecutingEngine.GetUserFriendlyMessageList(WorkflowOperations.WriteLoanOrTemplate, eval);
            SetResult("Result", ObsoleteSerializationHelper.JsonSerialize(items));
        }

        private void GetRateLockStatus()
        {
            Guid loanId = GetGuid("loanid");
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(LoanUtilitiesService));
            dataLoan.InitLoad();
            dataLoan.ByPassFieldSecurityCheck = true;

            SetResult("extend", dataLoan.sIsRateLockExtentionAllowed);
            SetResult("relock", dataLoan.sIsRateReLockAllowed);
            SetResult("floatdown", dataLoan.sIsRateLockFloatDownAllowed);
        }

        private void GetGenericFrameworkVendors()
        {
            Guid loanId = GetGuid("loanid");
            string[] providerIDs = ObsoleteSerializationHelper.JavascriptJsonDeserializer<string[]>(GetString("ProviderIDs"));
            var mappings = GenericFrameworkVendor.EvaluatePermissions(loanId, providerIDs, LinkLocation.LQBPipeline);

            SetResult("ProviderIDPermissions", ObsoleteSerializationHelper.JavascriptJsonSerialize<KeyValuePair<string, bool>[]>(mappings.ToArray()));
        }

        private void LoadGenericFrameworkVendor()
        {
            Guid brokerID = PrincipalFactory.CurrentPrincipal.BrokerId;
            string providerID = GetString("ProviderID");
            Guid loanID = GetGuid("sLId");

            var response = GenericFrameworkVendorServer.SubmitRequest(brokerID, providerID, loanID, LinkLocation.LQBPipeline);

            bool hasError = true;
            string errorMessage = null;
            if (response != null && response.Window != null && response.Window.Url != null)
            {
                SetResult("PopupURL", response.Window.Url);
                SetResult("PopupHeight", response.Window.Height);
                SetResult("PopupWidth", response.Window.Width);
                SetResult("PopupIsModal", response.Window.IsModal);
                hasError = false;
            }
            else if (response != null && !string.IsNullOrEmpty(response.ErrorMessage))
            {
                errorMessage = response.ErrorMessage;
            }
            else
            {
                errorMessage = ErrorMessages.Generic;
            }

            SetResult("ErrorMessage", errorMessage);
            SetResult("HasError", hasError);
        }
    }

}
