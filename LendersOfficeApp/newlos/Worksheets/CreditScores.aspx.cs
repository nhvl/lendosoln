namespace LendersOfficeApp.newlos.Worksheets
{
    using System;
    using DataAccess;
    using LendersOffice.Migration;
    using LendersOffice.Common;

    public partial class CreditScores : BaseLoanPage
    {
        /// <summary>
        /// Saves data to the loan from this page's controls.
        /// </summary>
        /// <param name="dataLoan">The loan to bind to.</param>
        /// <param name="dataApp">The app to bind to.</param>
        /// <param name="serviceItem">The service item calling this method.</param>
        internal static void BindDataFromControls(CPageData dataLoan, CAppData dataApp, AbstractBackgroundServiceItem serviceItem)
        {
            HmdaReliedOn.BindHmdaReliedOnData(dataLoan, dataApp, serviceItem, $"{nameof(HmdaReliedOn)}_");
        }

        /// <summary>
        /// Loads the data from the loan to this page's controls.
        /// </summary>
        /// <param name="dataLoan">The loan to load from.</param>
        /// <param name="dataApp">The app to load from.</param>
        /// <param name="serviceItem">The service item.</param>
        internal static void LoadDataForControls(CPageData dataLoan, CAppData dataApp, AbstractBackgroundServiceItem serviceItem)
        {
            HmdaReliedOn.LoadHmdaReliedOnData(dataLoan, dataApp, serviceItem, $"{nameof(HmdaReliedOn)}_");
        }

        protected override void LoadData()
        {
            this.DataBind();
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(CreditScores));
            dataLoan.InitLoad();
            BindDataObject(dataLoan);
        }

        private void BindDataObject(CPageData dataLoan)
        {            
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            sCreditScoreType1.Text = dataLoan.sCreditScoreType1_rep;
            sCreditScoreType2.Text = dataLoan.sCreditScoreType2_rep;
            sCreditScoreType3.Text = dataLoan.sCreditScoreType3_rep;
            sCreditScoreType2Soft.Text = dataLoan.sCreditScoreType2Soft_rep;

            aBExperianScore.Text = dataApp.aBExperianScore_rep;
            aBTransUnionScore.Text = dataApp.aBTransUnionScore_rep;
            aBEquifaxScore.Text = dataApp.aBEquifaxScore_rep;
            aCExperianScore.Text = dataApp.aCExperianScore_rep;
            aCTransUnionScore.Text = dataApp.aCTransUnionScore_rep;
            aCEquifaxScore.Text = dataApp.aCEquifaxScore_rep;

            var replaceModelNameComboBoxes = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums);
            if (replaceModelNameComboBoxes)
            {
                this.aBExperianModelName.Visible = false;
                this.aBTransUnionModelName.Visible = false;
                this.aBEquifaxModelName.Visible = false;

                Tools.SetDropDownListValue(this.aBExperianModelT, dataApp.aBExperianModelT);
                this.aBExperianModelTOtherDescription.Text = dataApp.aBExperianModelTOtherDescription;

                Tools.SetDropDownListValue(this.aBTransUnionModelT, dataApp.aBTransUnionModelT);
                this.aBTransUnionModelTOtherDescription.Text = dataApp.aBTransUnionModelTOtherDescription;

                Tools.SetDropDownListValue(this.aBEquifaxModelT, dataApp.aBEquifaxModelT);
                this.aBEquifaxModelTOtherDescription.Text = dataApp.aBEquifaxModelTOtherDescription;

                this.aCExperianModelName.Visible = false;
                this.aCTransUnionModelName.Visible = false;
                this.aCEquifaxModelName.Visible = false;

                Tools.SetDropDownListValue(this.aCExperianModelT, dataApp.aCExperianModelT);
                this.aCExperianModelTOtherDescription.Text = dataApp.aCExperianModelTOtherDescription;

                Tools.SetDropDownListValue(this.aCTransUnionModelT, dataApp.aCTransUnionModelT);
                this.aCTransUnionModelTOtherDescription.Text = dataApp.aCTransUnionModelTOtherDescription;

                Tools.SetDropDownListValue(this.aCEquifaxModelT, dataApp.aCEquifaxModelT);
                this.aCEquifaxModelTOtherDescription.Text = dataApp.aCEquifaxModelTOtherDescription;
            }
            else
            {
                var isAppLevelCreditMigrated = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V19_PopulateApplicationLevelCreditModelNames);

                this.aBExperianLoanVersion26Section.Visible = false;
                this.aBTransUnionLoanVersion26Section.Visible = false;
                this.aBEquifaxLoanVersion26Section.Visible = false;

                this.aBExperianModelName.Text = dataApp.aBExperianModelName;
                this.aBExperianModelName.ReadOnly = !isAppLevelCreditMigrated;

                this.aBTransUnionModelName.Text = dataApp.aBTransUnionModelName;
                this.aBTransUnionModelName.ReadOnly = !isAppLevelCreditMigrated;

                this.aBEquifaxModelName.Text = dataApp.aBEquifaxModelName;
                this.aBEquifaxModelName.ReadOnly = !isAppLevelCreditMigrated;

                this.aCExperianLoanVersion26Section.Visible = false;
                this.aCTransUnionLoanVersion26Section.Visible = false;
                this.aCEquifaxLoanVersion26Section.Visible = false;

                this.aCExperianModelName.Text = dataApp.aCExperianModelName;
                this.aCExperianModelName.ReadOnly = !isAppLevelCreditMigrated;

                this.aCTransUnionModelName.Text = dataApp.aCTransUnionModelName;
                this.aCTransUnionModelName.ReadOnly = !isAppLevelCreditMigrated;

                this.aCEquifaxModelName.Text = dataApp.aCEquifaxModelName;
                this.aCEquifaxModelName.ReadOnly = !isAppLevelCreditMigrated;
            }

            aCreditReportId.Text = dataApp.aCreditReportId;
            aCreditReportIdLckd.Checked = dataApp.aCreditReportIdLckd;

            Tools.SetDropDownListValue(aBDecisionCreditSourceT, dataApp.aBDecisionCreditSourceT);
            Tools.SetDropDownListValue(aCDecisionCreditSourceT, dataApp.aCDecisionCreditSourceT);

            aBDecisionCreditSourceTLckd.Checked = dataApp.aBDecisionCreditSourceTLckd;
            aCDecisionCreditSourceTLckd.Checked = dataApp.aCDecisionCreditSourceTLckd;

            aBDecisionCreditScore.Text = dataApp.aBDecisionCreditScore_rep;
            aCDecisionCreditScore.Text = dataApp.aCDecisionCreditScore_rep;

            this.aBDecisionCreditModelName.Value = dataApp.aBDecisionCreditModelName;
            this.aCDecisionCreditModelName.Value = dataApp.aCDecisionCreditModelName;

            this.aBIsDecisionCreditModelAValidHmdaModel.Checked = dataApp.aBIsDecisionCreditModelAValidHmdaModel;
            this.aCIsDecisionCreditModelAValidHmdaModel.Checked = dataApp.aCIsDecisionCreditModelAValidHmdaModel;

            this.aBOtherCreditModelName.Text = dataApp.aBOtherCreditModelName;
            this.aCOtherCreditModelName.Text = dataApp.aCOtherCreditModelName;
        }

        protected void PageInit(object sender, EventArgs e)
        {
            this.PageTitle = "Credit Scores";
            this.PageID = "CreditScores";
            this.EnableJqueryMigrate = false;
            this.RegisterJsGlobalVariables("isCreditScores", true);

            Tools.Bind_CreditModelT(this.aBExperianModelT);
            Tools.Bind_CreditModelT(this.aBTransUnionModelT);
            Tools.Bind_CreditModelT(this.aBEquifaxModelT);

            Tools.Bind_CreditModelT(this.aCExperianModelT);
            Tools.Bind_CreditModelT(this.aCTransUnionModelT);
            Tools.Bind_CreditModelT(this.aCEquifaxModelT);

            Tools.BindComboBox_CreditModelName(this.aBExperianModelName);
            Tools.BindComboBox_CreditModelName(this.aBTransUnionModelName);
            Tools.BindComboBox_CreditModelName(this.aBEquifaxModelName);

            Tools.BindComboBox_CreditModelName(this.aCExperianModelName);
            Tools.BindComboBox_CreditModelName(this.aCTransUnionModelName);
            Tools.BindComboBox_CreditModelName(this.aCEquifaxModelName);

            Tools.BindComboBox_CreditModelName(this.aBOtherCreditModelName);
            Tools.BindComboBox_CreditModelName(this.aCOtherCreditModelName);

            Tools.Bind_aDecisionCreditSourceT(aBDecisionCreditSourceT);
            Tools.Bind_aDecisionCreditSourceT(aCDecisionCreditSourceT);
        }
        
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new EventHandler(this.PageInit);

        }
        #endregion
    }
}
