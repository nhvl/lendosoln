<%@ Page Language="c#" CodeBehind="CreditScores.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Worksheets.CreditScores" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="RELIED" TagName="HmdaReliedOn" Src="~/newlos/HmdaReliedOn.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<!DOCTYPE HTML>
<html>
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Credit Scores</title>
    <style type="text/css">
        table.Float {
            float: left;
        }

        table.Clear {
            clear: both;
        }

        table.Header {
            width: 100%;
        }

        table.Main {
            width: 750px;
        }

        .model-name {
            width: 200px;
        }

        #DecisionScoreTable {
            border-collapse: collapse;
        }

        #DecisionScoreTable td {
            white-space: nowrap;
        }

        #aBOtherCreditModelName, #aCOtherCreditModelName {
            width: 180px;
            max-width: 180px;
        }

        .CreditModelOtherDescription {
            margin-left: 43px;
            width: 252px;
        }
    </style>
    <script type="text/javascript">
        function _init() {            
            lockField(document.getElementById('aCreditReportIdLckd'), 'aCreditReportId');

            var borrowerDecisionSourceDropdown = document.getElementById('aBDecisionCreditSourceT');
            var coborrowerDecisionSourceDropdown = document.getElementById('aCDecisionCreditSourceT');

            disableDDL(borrowerDecisionSourceDropdown, !document.getElementById('aBDecisionCreditSourceTLckd').checked);
            disableDDL(coborrowerDecisionSourceDropdown, !document.getElementById('aCDecisionCreditSourceTLckd').checked);

            setOtherCreditVisibility(borrowerDecisionSourceDropdown, coborrowerDecisionSourceDropdown);

            if (ML.LoanVersionTCurrent >= 26) {
                $('.CreditModelT').each(function(index, value) {
                    toggleCreditModelOtherDescription(value);
                });
            }
        }

        function onCreditModelTypeChanged(modelTypeDropdown) {
            toggleCreditModelOtherDescription(modelTypeDropdown);
            refreshCalculation();
        }

        function toggleCreditModelOtherDescription(modelTypeDropdown) {
            var $modelType = $(modelTypeDropdown)
            var creditScoreModelT_Other = <%# AspxTools.JsString(E_CreditScoreModelT.Other.ToString("D")) %>;
            var creditModelIsOther = $modelType.val() === creditScoreModelT_Other;
            var $otherDesc = $modelType.siblings('.CreditModelOtherDescription');
            $otherDesc.toggle(creditModelIsOther);
        }

        function setOtherCreditVisibility(borrowerDecisionSourceDropdown, coborrowerDecisionSourceDropdown) {
            var $borrowerDecisionSource = $(borrowerDecisionSourceDropdown);
            var $coborrowerDecisionSource = $(coborrowerDecisionSourceDropdown);            

            var borrowerIsOther = $borrowerDecisionSource.val() === ML.OtherCreditSource;
            var coborrowerIsOther = $coborrowerDecisionSource.val() === ML.OtherCreditSource;            

            setDecisionScoreVisibility($borrowerDecisionSource, 'aBOtherCreditModelName', borrowerIsOther);
            setDecisionScoreVisibility($coborrowerDecisionSource, 'aCOtherCreditModelName', coborrowerIsOther);

            $('tr.other-credit-row').toggle(borrowerIsOther || coborrowerIsOther);
        }
        
        function setDecisionScoreVisibility(dropdown, otherModelNameId, isOther) {
            dropdown.next('input[type="text"]').prop('readonly', isOther ? '' : 'readonly');
            $('#' + otherModelNameId).parent().toggle(isOther);
        }
    </script>
</head>
<body class="RightBackground" ms_positioning="FlowLayout">
    <form id="CreditScores" method="post" runat="server">
        <table class="FormTable Header" id="Table2" cellspacing="0" cellpadding="3" border="0">
            <tr>
                <td class="MainRightHeader">Credit Scores</td>
            </tr>
        </table>
        <table>
            <tr>
                <td class="FieldLabel">Report Identifier</td>
                <td><asp:TextBox ID="aCreditReportId" runat="server" Width="120px" MaxLength="21"></asp:TextBox></td>
                <td><asp:CheckBox ID="aCreditReportIdLckd" runat="server" Text="Locked" CssClass="FieldLabel" onclick="refreshCalculation();" /></td>
            </tr>
        </table>
        <table class="InsetBorder Float Main" cellspacing="1" cellpadding="1" border="0">
            <tr>
                <td></td>
                <td class="FieldLabel">Borrower</td>
                <td class="FieldLabel">&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
                <td class="FieldLabel">Coborrower</td>
            </tr>
            <tr>
                <td class="FieldLabel">Experian</td>
                <td>
                    <asp:TextBox ID="aBExperianScore" runat="server" Width="40px" onchange="refreshCalculation();" MaxLength="4" TabIndex="5"></asp:TextBox>
                    <ml:ComboBox ID="aBExperianModelName" runat="server" MaxLength="100" CssClass="model-name"></ml:ComboBox>
                    <span id="aBExperianLoanVersion26Section" runat="server">
                        <asp:DropDownList ID="aBExperianModelT" class="CreditModelT" runat="server" onchange="onCreditModelTypeChanged(this);"></asp:DropDownList>
                        <br />
                        <asp:TextBox ID="aBExperianModelTOtherDescription" class="CreditModelOtherDescription" runat="server" onchange="refreshCalculation();"></asp:TextBox>
                    </span>
                </td>
                <td></td>
                <td>
                    <asp:TextBox ID="aCExperianScore" runat="server" Width="40px" onchange="refreshCalculation();" MaxLength="4" TabIndex="20"></asp:TextBox>
                    <ml:ComboBox ID="aCExperianModelName" runat="server" MaxLength="100" CssClass="model-name"></ml:ComboBox>
                    <span id="aCExperianLoanVersion26Section" runat="server">
                        <asp:DropDownList ID="aCExperianModelT" class="CreditModelT" runat="server" onchange="onCreditModelTypeChanged(this);"></asp:DropDownList>
                        <br />
                        <asp:TextBox ID="aCExperianModelTOtherDescription" class="CreditModelOtherDescription" runat="server" onchange="refreshCalculation();"></asp:TextBox>
                    </span>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">TransUnion</td>
                <td>
                    <asp:TextBox ID="aBTransUnionScore" runat="server" Width="40px" onchange="refreshCalculation();" MaxLength="4" TabIndex="10"></asp:TextBox>
                    <ml:ComboBox ID="aBTransUnionModelName" runat="server" MaxLength="100" CssClass="model-name"></ml:ComboBox>
                    <span id="aBTransUnionLoanVersion26Section" runat="server">
                        <asp:DropDownList ID="aBTransUnionModelT" class="CreditModelT" runat="server" onchange="onCreditModelTypeChanged(this);"></asp:DropDownList>
                        <br />
                        <asp:TextBox ID="aBTransUnionModelTOtherDescription" class="CreditModelOtherDescription" runat="server" onchange="refreshCalculation();"></asp:TextBox>
                    </span>
                </td>
                <td></td>
                <td>
                    <asp:TextBox ID="aCTransUnionScore" runat="server" Width="40px" onchange="refreshCalculation();" MaxLength="4" TabIndex="25"></asp:TextBox>
                    <ml:ComboBox ID="aCTransUnionModelName" runat="server" MaxLength="100" CssClass="model-name"></ml:ComboBox>
                    <span id="aCTransUnionLoanVersion26Section" runat="server">
                        <asp:DropDownList ID="aCTransUnionModelT" class="CreditModelT" runat="server" onchange="onCreditModelTypeChanged(this);"></asp:DropDownList>
                        <br />
                        <asp:TextBox ID="aCTransUnionModelTOtherDescription" class="CreditModelOtherDescription" runat="server" onchange="refreshCalculation();"></asp:TextBox>
                    </span>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">Equifax</td>
                <td>
                    <asp:TextBox ID="aBEquifaxScore" runat="server" Width="40px" onchange="refreshCalculation();" MaxLength="4" TabIndex="15"></asp:TextBox>
                    <ml:ComboBox ID="aBEquifaxModelName" runat="server" MaxLength="100" CssClass="model-name"></ml:ComboBox>
                    <span id="aBEquifaxLoanVersion26Section" runat="server">
                        <asp:DropDownList ID="aBEquifaxModelT" class="CreditModelT" runat="server" onchange="onCreditModelTypeChanged(this);"></asp:DropDownList>
                        <br />
                        <asp:TextBox ID="aBEquifaxModelTOtherDescription" class="CreditModelOtherDescription" runat="server" onchange="refreshCalculation();"></asp:TextBox>
                    </span>
                </td>
                <td></td>
                <td>
                    <asp:TextBox ID="aCEquifaxScore" runat="server" Width="40px" onchange="refreshCalculation();" MaxLength="4" TabIndex="30"></asp:TextBox>
                    <ml:ComboBox ID="aCEquifaxModelName" runat="server" MaxLength="100" CssClass="model-name"></ml:ComboBox>
                    <span id="aCEquifaxLoanVersion26Section" runat="server">
                        <asp:DropDownList ID="aCEquifaxModelT" class="CreditModelT" runat="server" onchange="onCreditModelTypeChanged(this);"></asp:DropDownList>
                        <br />
                        <asp:TextBox ID="aCEquifaxModelTOtherDescription" class="CreditModelOtherDescription" runat="server" onchange="refreshCalculation();"></asp:TextBox>
                    </span>
                </td>
            </tr>
        </table>
        <table id="DecisionScoreTable" class="InsetBorder Clear Main">
            <tr>
                <td class="FieldLabel">Borrower's Decision Score</td>
                <td>
                    <asp:CheckBox ID="aBDecisionCreditSourceTLckd" runat="server" onclick="refreshCalculation();" />
                    <asp:DropDownList ID="aBDecisionCreditSourceT" runat="server" onchange="refreshCalculation();" />
                    <asp:TextBox ID="aBDecisionCreditScore" runat="server" Width="50px" onchange="refreshCalculation();"/>
                    <asp:HiddenField ID="aBDecisionCreditModelName" runat="server" />
                    <asp:CheckBox ID="aBIsDecisionCreditModelAValidHmdaModel" class="hidden" runat="server" />
                </td>
                <td>&nbsp;</td>
                <td class="FieldLabel">Coborrower's Decision Score</td>
                <td>
                    <asp:CheckBox ID="aCDecisionCreditSourceTLckd" runat="server" onclick="refreshCalculation();" />
                    <asp:DropDownList ID="aCDecisionCreditSourceT" runat="server" onchange="refreshCalculation();" />
                    <asp:TextBox ID="aCDecisionCreditScore" runat="server" Width="50px" onchange="refreshCalculation();"/>
                    <asp:HiddenField ID="aCDecisionCreditModelName" runat="server" />
                    <asp:CheckBox ID="aCIsDecisionCreditModelAValidHmdaModel" class="hidden" runat="server"/>
                </td>
            </tr>
            <tr class="other-credit-row">
                <td>&nbsp;</td>
                <td>
                    <span>
                        <ml:ComboBox ID="aBOtherCreditModelName" runat="server" MaxLength="100" CssClass="model-name" onchange="refreshCalculation();"></ml:ComboBox>
                    </span>
                </td>
                <td colspan="2">&nbsp;</td>
                <td>
                    <span>
                        <ml:ComboBox ID="aCOtherCreditModelName" runat="server" MaxLength="100" CssClass="model-name" onchange="refreshCalculation();"></ml:ComboBox>
                    </span>
                </td>
            </tr>
        </table>
        <table class="Clear Main">
            <RELIED:HmdaReliedOn runat="server" id="HmdaReliedOn"></RELIED:HmdaReliedOn>
        </table>
        <table class="InsetBorder Clear">
            <tr>
                <td nowrap colspan="3"></td>
            </tr>
            <tr>
                <td class="FieldLabel" nowrap colspan="3">Calculated Credit Scores Used In Prequal Engine</td>
            </tr>
            <tr>
                <td class="FieldLabel" valign="top" nowrap align="left">Type #</td>
                <td class="FieldLabel">Description</td>
                <td class="FieldLabel">Result Score</td>
            </tr>
            <tr>
                <td valign="top" align="middle">1</td>
                <td valign="top" nowrap>Middle of 3 or lower of 2 scores for the primary wage earner.&nbsp;</td>
                <td valign="top"><asp:TextBox ID="sCreditScoreType1" runat="server" Width="70px" ReadOnly="True"></asp:TextBox></td>
            </tr>
            <tr>
                <td valign="top" align="middle">2</td>
                <td valign="top" nowrap>Lowest borrower using middle of 3 or lower of 2 for each borrower.&nbsp;<br>Co-borrowers with ssn are also included in this calculation.</td>
                <td valign="top"><asp:TextBox ID="sCreditScoreType2" runat="server" Width="70px" ReadOnly="True"></asp:TextBox></td>
            </tr>
            <tr>
                <td valign="top" align="middle">2 soft</td>
                <td valign="top" nowrap><p>Similar to score type #2 but co-borrowers without credit score&nbsp;<br>are not included in this calculation.</p></td>
                <td valign="top"><asp:TextBox ID="sCreditScoreType2Soft" runat="server" Width="70px" ReadOnly="True"></asp:TextBox></td>
            </tr>
            <tr>
                <td valign="top" align="middle">3</td>
                <td valign="top">Average score of the primary borrower.</td>
                <td valign="top"><asp:TextBox ID="sCreditScoreType3" runat="server" Width="70px" ReadOnly="True"></asp:TextBox></td>
            </tr>
        </table>
    </form>
</body>
</html>
