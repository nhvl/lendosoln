namespace LendersOfficeApp.newlos.Worksheets
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using Status;

    public class CreditScoresServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(CreditScoresServiceItem));
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sCreditScoreType1", dataLoan.sCreditScoreType1_rep);
            SetResult("sCreditScoreType2", dataLoan.sCreditScoreType2_rep);
            SetResult("sCreditScoreType3", dataLoan.sCreditScoreType3_rep);
            SetResult("sCreditScoreType2Soft", dataLoan.sCreditScoreType2Soft_rep);
            SetResult("aBExperianScore", dataApp.aBExperianScore_rep);
            SetResult("aBTransUnionScore", dataApp.aBTransUnionScore_rep);
            SetResult("aBEquifaxScore", dataApp.aBEquifaxScore_rep);
            SetResult("aCExperianScore", dataApp.aCExperianScore_rep);
            SetResult("aCTransUnionScore", dataApp.aCTransUnionScore_rep);
            SetResult("aCEquifaxScore", dataApp.aCEquifaxScore_rep);
            this.SetResult("aCreditReportId", dataApp.aCreditReportId);
            this.SetResult("aCreditReportIdLckd", dataApp.aCreditReportIdLckd);

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums))
            {
                this.SetResult(nameof(CAppData.aBExperianModelT), dataApp.aBExperianModelT);
                this.SetResult(nameof(CAppData.aBExperianModelTOtherDescription), dataApp.aBExperianModelTOtherDescription);
                this.SetResult(nameof(CAppData.aBTransUnionModelT), dataApp.aBTransUnionModelT);
                this.SetResult(nameof(CAppData.aBTransUnionModelTOtherDescription), dataApp.aBTransUnionModelTOtherDescription);
                this.SetResult(nameof(CAppData.aBEquifaxModelT), dataApp.aBEquifaxModelT);
                this.SetResult(nameof(CAppData.aBEquifaxModelTOtherDescription), dataApp.aBEquifaxModelTOtherDescription);

                this.SetResult(nameof(CAppData.aCExperianModelT), dataApp.aCExperianModelT);
                this.SetResult(nameof(CAppData.aCExperianModelTOtherDescription), dataApp.aCExperianModelTOtherDescription);
                this.SetResult(nameof(CAppData.aCTransUnionModelT), dataApp.aCTransUnionModelT);
                this.SetResult(nameof(CAppData.aCTransUnionModelTOtherDescription), dataApp.aCTransUnionModelTOtherDescription);
                this.SetResult(nameof(CAppData.aCEquifaxModelT), dataApp.aCEquifaxModelT);
                this.SetResult(nameof(CAppData.aCEquifaxModelTOtherDescription), dataApp.aCEquifaxModelTOtherDescription);
            }
            else
            {
                this.SetResult(nameof(CAppData.aBExperianModelName), dataApp.aBExperianModelName);
                this.SetResult(nameof(CAppData.aBTransUnionModelName), dataApp.aBTransUnionModelName);
                this.SetResult(nameof(CAppData.aBEquifaxModelName), dataApp.aBEquifaxModelName);

                this.SetResult(nameof(CAppData.aCExperianModelName), dataApp.aCExperianModelName);
                this.SetResult(nameof(CAppData.aCTransUnionModelName), dataApp.aCTransUnionModelName);
                this.SetResult(nameof(CAppData.aCEquifaxModelName), dataApp.aCEquifaxModelName);
            }

            this.SetResult("aBDecisionCreditScore", dataApp.aBDecisionCreditScore_rep);
            this.SetResult("aCDecisionCreditScore", dataApp.aCDecisionCreditScore_rep);

            this.SetResult("aBDecisionCreditSourceT", dataApp.aBDecisionCreditSourceT);
            this.SetResult("aCDecisionCreditSourceT", dataApp.aCDecisionCreditSourceT);

            this.SetResult("aBDecisionCreditSourceTLckd", dataApp.aBDecisionCreditSourceTLckd);
            this.SetResult("aCDecisionCreditSourceTLckd", dataApp.aCDecisionCreditSourceTLckd);

            this.SetResult(nameof(dataApp.aBDecisionCreditModelName), dataApp.aBDecisionCreditModelName);
            this.SetResult(nameof(dataApp.aCDecisionCreditModelName), dataApp.aCDecisionCreditModelName);

            this.SetResult(nameof(dataApp.aBIsDecisionCreditModelAValidHmdaModel), dataApp.aBIsDecisionCreditModelAValidHmdaModel);
            this.SetResult(nameof(dataApp.aCIsDecisionCreditModelAValidHmdaModel), dataApp.aCIsDecisionCreditModelAValidHmdaModel);

            this.SetResult(nameof(dataApp.aBOtherCreditModelName), dataApp.aBOtherCreditModelName);
            this.SetResult(nameof(dataApp.aCOtherCreditModelName), dataApp.aCOtherCreditModelName);

            CreditScores.LoadDataForControls(dataLoan, dataApp, this);
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.aBExperianScore_rep = GetString("aBExperianScore");
            dataApp.aBTransUnionScore_rep = GetString("aBTransUnionScore");
            dataApp.aBEquifaxScore_rep = GetString("aBEquifaxScore");
            dataApp.aCExperianScore_rep = GetString("aCExperianScore");
            dataApp.aCTransUnionScore_rep = GetString("aCTransUnionScore");
            dataApp.aCEquifaxScore_rep = GetString("aCEquifaxScore");
            dataApp.aCreditReportId = this.GetString("aCreditReportId");
            dataApp.aCreditReportIdLckd = this.GetBool("aCreditReportIdLckd");

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums))
            {
                dataApp.aBExperianModelT = (E_CreditScoreModelT)this.GetInt(nameof(CAppData.aBExperianModelT));
                dataApp.aBExperianModelTOtherDescription = this.GetString(nameof(CAppData.aBExperianModelTOtherDescription));
                dataApp.aBTransUnionModelT = (E_CreditScoreModelT)this.GetInt(nameof(CAppData.aBTransUnionModelT));
                dataApp.aBTransUnionModelTOtherDescription = this.GetString(nameof(CAppData.aBTransUnionModelTOtherDescription));
                dataApp.aBEquifaxModelT = (E_CreditScoreModelT)this.GetInt(nameof(CAppData.aBEquifaxModelT));
                dataApp.aBEquifaxModelTOtherDescription = this.GetString(nameof(CAppData.aBEquifaxModelTOtherDescription));

                dataApp.aCExperianModelT = (E_CreditScoreModelT)this.GetInt(nameof(CAppData.aCExperianModelT));
                dataApp.aCExperianModelTOtherDescription = this.GetString(nameof(CAppData.aCExperianModelTOtherDescription));
                dataApp.aCTransUnionModelT = (E_CreditScoreModelT)this.GetInt(nameof(CAppData.aCTransUnionModelT));
                dataApp.aCTransUnionModelTOtherDescription = this.GetString(nameof(CAppData.aCTransUnionModelTOtherDescription));
                dataApp.aCEquifaxModelT = (E_CreditScoreModelT)this.GetInt(nameof(CAppData.aCEquifaxModelT));
                dataApp.aCEquifaxModelTOtherDescription = this.GetString(nameof(CAppData.aCEquifaxModelTOtherDescription));
            }
            else if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V19_PopulateApplicationLevelCreditModelNames))
            {
                dataApp.aBExperianModelName = this.GetString(nameof(CAppData.aBExperianModelName));
                dataApp.aBTransUnionModelName = this.GetString(nameof(CAppData.aBTransUnionModelName));
                dataApp.aBEquifaxModelName = this.GetString(nameof(CAppData.aBEquifaxModelName));

                dataApp.aCExperianModelName = this.GetString(nameof(CAppData.aCExperianModelName));
                dataApp.aCTransUnionModelName = this.GetString(nameof(CAppData.aCTransUnionModelName));
                dataApp.aCEquifaxModelName = this.GetString(nameof(CAppData.aCEquifaxModelName));
            }

            dataApp.aBDecisionCreditSourceT = (E_aDecisionCreditSourceT)GetInt("aBDecisionCreditSourceT");
            dataApp.aCDecisionCreditSourceT = (E_aDecisionCreditSourceT)GetInt("aCDecisionCreditSourceT");

            dataApp.aBDecisionCreditSourceTLckd = GetBool("aBDecisionCreditSourceTLckd");
            dataApp.aCDecisionCreditSourceTLckd = GetBool("aCDecisionCreditSourceTLckd");

            dataApp.aBDecisionCreditScore_rep = this.GetString("aBDecisionCreditScore");
            dataApp.aCDecisionCreditScore_rep = this.GetString("aCDecisionCreditScore");

            dataApp.aBOtherCreditModelName = this.GetString(nameof(dataApp.aBOtherCreditModelName));
            dataApp.aCOtherCreditModelName = this.GetString(nameof(dataApp.aCOtherCreditModelName));

            CreditScores.BindDataFromControls(dataLoan, dataApp, this);
        }
    }

    public partial class CreditScoresService : BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new CreditScoresServiceItem());
        }
    }
}
