/// Author: David Dao

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using LendersOffice.DU;
using DataAccess;
namespace LendersOfficeApp.newlos.Services
{
	public partial class ViewDUFinding : LendersOffice.Common.BasePage
	{
		protected void PageLoad(object sender, System.EventArgs e)
		{
            Guid sLId = RequestHelper.GetGuid("loanid");

            CPageData dataLoan = new CDuFindingsHtmlData(sLId);
            dataLoan.InitLoad();
            string html = dataLoan.sDuFindingsHtml;
            
            if ("" != html) 
            {
                Response.Clear();
                byte[] buffer = System.Text.UTF8Encoding.UTF8.GetBytes(html);
                Response.OutputStream.Write(buffer, 0, buffer.Length);
                Response.Flush();
                Response.End();
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
