namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.CreditReport;
    using Status;
    using LendersOffice.ObjLib.ServiceCredential;


    public class TemporaryCRAInfo 
    {
        public string ProtocolId;
        public string VendorName;
        public string ProtocolUrl;
        public CreditReportProtocol ProtocolType;
        public bool IsPdfViewNeeded;
        public string UserName;
        public string Password;
        public string AccountID;
        public bool IsExperianPulled;
        public bool IsEquifaxPulled;
        public bool IsTransUnionPulled;
    }
    public class CRABureauInfo
    {
        public bool IsEquifaxPulled;
        public bool IsExperianPulled;
        public bool IsTransUnionPulled;
        public bool IsFanniePulled;
    }
    public partial class OrderCredit : BaseLoanPage
    {
        #region "Member variables"
        protected System.Web.UI.WebControls.DropDownList aBSuffixDDL;
        protected System.Web.UI.WebControls.DropDownList aCSuffixDDL;
        protected System.Web.UI.WebControls.TextBox m_presentStreetNumTF;
        protected System.Web.UI.WebControls.DropDownList m_presentStreetDirDDL;
        protected System.Web.UI.WebControls.TextBox m_presentStreetNameTF;
        protected System.Web.UI.WebControls.DropDownList m_presentStreetTypeDDL;
        protected System.Web.UI.WebControls.TextBox m_presentAptTF;
        protected System.Web.UI.WebControls.TextBox m_previousStreetNumTF;
        protected System.Web.UI.WebControls.DropDownList m_previousStreetDirDDL;
        protected System.Web.UI.WebControls.TextBox m_previousStreetNameTF;
        protected System.Web.UI.WebControls.DropDownList m_previousStreetTypeDDL;
        protected System.Web.UI.WebControls.TextBox m_previousAptTF;
		protected System.Web.UI.WebControls.Label												m_spacehere;
        #endregion

        protected string FileId { get; private set; } = string.Empty;

        protected string LqiFileId { get; private set; } = string.Empty;

        private static string[,] STREET_TYPE = {
                                        {" ", ""}, {"AV", "AVENUE"}, {"AL", "ALLEY"}, {"BV", "BOULEVARD"}, {"CI", "CIRCLE"},
                                        {"CN", "CENTER"}, {"CR", "CRESCENT"}, {"CT", "COURT"}, {"CX", "CROSSING"}, {"DR", "DRIVE"},
                                        {"DA", "DALE"}, {"EX", "EXPRESSWAY"}, {"FY", "FREEWAY"}, {"GA", "GARDEN"}, {"GR", "GROVE"},
                                        {"HL", "HILL"}, {"HT", "HEIGHTS"}, {"HY", "HIGHWAY"}, {"KN", "KNOLL"}, {"LN", "LANE"},
                                        {"LP", "LOOP"}, {"MA", "MALL"}, {"PA", "PATH"}, {"PI", "PIKE"}, {"PK", "PARK"}, {"PL", "PLACE"},
                                        {"PT", "POINT"}, {"PY", "PARKWAY"}, {"PZ", "PLAZA"}, {"RD", "ROAD"}, {"RN", "RUN"},
                                        {"RO", "ROW"}, {"RT", "ROUTE"}, {"SQ", "SQUARE"}, {"ST", "STREET"}, {"TE", "TERRACE"},
                                        {"TP", "TURNPIKE"}, {"TR", "TRAIL"}, {"VI", "VIADUCT"}, {"WK", "WALK"}, {"WY", "WAY"}
                                               };
        private static string[,] STREET_DIR = {
                                        {"0", " "}, {"1", "N"}, {"2", "S"}, {"3", "E"}, {"4", "W"}, {"5", "NW"}, {"6", "SW"}, {"7", "NE"}, {"8", "SE"},
                                        };


        protected bool m_hasLiabilities = true;
        protected bool m_hasBrokerDUInfo = false;
        private string m_brokerDUUserID = "";
        private string m_brokerDUPassword = "";

        private List<TemporaryCRAInfo> m_craList;
		
		//OPM 15995
		protected string m_LoanName
		{
			get { 
				if(ViewState["loanname"] != null)
					return (string) ViewState["loanname"]; 
				else
					return "";
			}
			set { ViewState["loanname"] = value; }
		}
		
		protected Guid m_dbFileKey 
        {
            get { return (Guid) ViewState["dbFileKey"]; }
            set { ViewState["dbFileKey"] = value; }
        }

        private string m_lastProtocolId 
        {
            get { return (string) ViewState["lastprotocolid"]; }
            set { ViewState["lastprotocolid"] = value; }
        }

        private string m_lastprotocolUrl 
        {
            get { return (string) ViewState["lastprotocolurl"]; }
            set { ViewState["lastprotocolurl"] = value; }

        }
        protected string m_lastVendorName 
        {
            get { return (string) ViewState["lastvendorname"]; }
            set { ViewState["lastvendorname"] = value; }
        }
        protected string m_lastProtocolType 
        {
            get { return (string) ViewState["lastprotocoltype"]; }
            set { ViewState["lastprotocoltype"] = value; }
        }
        private void BindDropDownList(DropDownList ddl, string[,] items) 
        {
            for (int i = 0; i < items.Length / 2; i++) 
                ddl.Items.Add(new ListItem(items[i, 1], items[i, 0]));
        }

        private void LoadBrokerInformation() 
        {
            
            BrokerDB broker = this.Broker;

            Page.ClientScript.RegisterHiddenField("RequestingPartyName", broker.Name);
            Page.ClientScript.RegisterHiddenField("RequestingPartyStreetAddress", broker.Address.StreetAddress);
            Page.ClientScript.RegisterHiddenField("RequestingPartyCity", broker.Address.City);
            Page.ClientScript.RegisterHiddenField("RequestingPartyState", broker.Address.State);
            Page.ClientScript.RegisterHiddenField("RequestingPartyZipcode", broker.Address.Zipcode);

        }
        private void DetermineBrokerDULoginInfo() 
        {
            m_brokerDUUserID = this.Broker.CreditMornetPlusUserID;
            m_brokerDUPassword = this.Broker.CreditMornetPlusPassword.Value;
            m_hasBrokerDUInfo = m_brokerDUUserID != "" && m_brokerDUPassword != "";
        }

        internal static void BindDataFromControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem serviceItem)
        {
            CreditReportAuthorization.BindDataToLoan(loan, application, serviceItem, nameof(CreditReportAuthorizationCtrl) + "_");
        }

        internal static void LoadDataForControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem serviceItem)
        {
            CreditReportAuthorization.LoadDataFromLoan(loan, application, serviceItem, nameof(CreditReportAuthorizationCtrl) + "_");
        }

        protected override void LoadData() 
        {

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(OrderCredit));
            dataLoan.InitLoad();

            // OPM 450477. If there is any credit login at the branch or broker level,
            // pre-populate the first one in the list. Otherwise, use old method.
            var branchId = dataLoan.sBranchId;
            Page.ClientScript.RegisterHiddenField("sBranchId", branchId.ToString());
            var savedCredentials = ServiceCredential.ListAvailableServiceCredentials(BrokerUser, branchId, ServiceCredentialService.CreditReports);

            List<string> savedCredentialIds = new List<string>();
            foreach (var credential in savedCredentials)
            {
                savedCredentialIds.Add(credential.ServiceProviderId.ToString());
            }

            RegisterJsObject("savedCredentials", savedCredentialIds);

            var firstCra = savedCredentials.FirstOrDefault();
            if (firstCra != null)
            {
                Tools.SetDropDownListValue(CreditAgencyList, firstCra.ServiceProviderId.ToString());
            }
            else
            {
                Guid lastUsedCra = BrokerUser.LastUsedCreditProtocolId;
                string lastUsedCreditLoginNm = BrokerUser.LastUsedCreditLoginNm;

                m_loginName.Text = lastUsedCreditLoginNm;
                if (lastUsedCreditLoginNm.TrimWhitespaceAndBOM() != "")
                {
                    m_rememberLoginCB.Checked = true;
                    m_accountIdentifier.Text = BrokerUser.LastUsedCreditAccountId;
                }

                if (lastUsedCra != Guid.Empty)
                {
                    Tools.SetDropDownListValue(CreditAgencyList, lastUsedCra.ToString());
                }
                else
                {
                    if (CreditAgencyList.Items.Count > 1)
                        CreditAgencyList.SelectedIndex = 1; // 5/11/2004 dd - Select first valid CRA by default.
                }
            }

            m_LoanName = dataLoan.sLNm;

            CAppData dataApp = null;
            if (ApplicationID == Guid.Empty) 
            {
                dataApp = dataLoan.GetAppData(0);
            } 
            else 
            {
                dataApp = dataLoan.GetAppData(ApplicationID);
            }

            m_hasLiabilities = dataApp.aLiaCollection.CountRegular != 0;
            aBFirstNm.Text = dataApp.aBFirstNm;
            aBLastNm.Text = dataApp.aBLastNm;
            aBMidNm.Text = dataApp.aBMidNm;
            aBSuffix.Text = dataApp.aBSuffix;
            aBDob.Text = dataApp.aBDob_rep;
            aBSsn.Text = dataApp.aBSsn.TrimWhitespaceAndBOM();
            Tools.SetDropDownListValue(aBMaritalStatT, dataApp.aBMaritalStatT);

            aBAddr.Text = dataApp.aBAddr;
            aBCity.Text = dataApp.aBCity;
            aBState.Value = dataApp.aBState;
            aBZip.Text = dataApp.aBZip;
            aBAddrYrs.Text = dataApp.aBAddrYrs;

            aBPrev1Addr.Text = dataApp.aBPrev1Addr;
            aBPrev1City.Text = dataApp.aBPrev1City;
            aBPrev1State.Value = dataApp.aBPrev1State;
            aBPrev1Zip.Text = dataApp.aBPrev1Zip;
            aBPrev1AddrYrs.Text = dataApp.aBPrev1AddrYrs;

            aCFirstNm.Text = dataApp.aCFirstNm;
            aCMidNm.Text = dataApp.aCMidNm;
            aCLastNm.Text = dataApp.aCLastNm;
            aCSuffix.Text = dataApp.aCSuffix;
            aCSsn.Text = dataApp.aCSsn.TrimWhitespaceAndBOM();
            aCDob.Text = dataApp.aCDob_rep;

            this.CreditReportAuthorizationCtrl.BindDataToControl(dataLoan, dataApp);

            SqlParameter[] parameters = { new SqlParameter("@ApplicationID", this.ApplicationID) };
            this.m_dbFileKey = Guid.Empty;

            string lastCra = null;

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerUser.BrokerId, "RetrieveCreditReport", parameters))
            {
                if (reader.Read())
                {
                    this.FileId = (string)reader["ExternalFileID"];
                    this.m_reportIDTF.Text = FileId;
                    this.m_upgradeReportIDTF.Text = FileId;
                    this.orderLqiReportId.Text = FileId;

                    lastCra = DetermineLastCra(reader["CrAccProxyId"], reader["ComId"]);
                }
            }

            parameters = new[] { new SqlParameter("@ApplicationID", this.ApplicationID) };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerUser.BrokerId, "RetrieveLqiCreditReport", parameters))
            {
                if (reader.Read())
                {
                    this.LqiFileId = (string)reader["ExternalFileID"];
                    this.reissueLqiReportId.Text = this.LqiFileId;
                    lastCra = DetermineLastCra(reader["CrAccProxyId"], reader["ComId"]) ?? lastCra;
                }
            }

            if (lastCra != null)
            {
                Tools.SetDropDownListValue(CreditAgencyList, lastCra);
            }
        }

        private string DetermineLastCra(object crAccProxyId, object comId)
        {
            string lastCra = (crAccProxyId is DBNull ? comId : crAccProxyId).ToString();

            // 10/24/2005 dd - if LastCra == POINT Dummy then set lastCra = null
            return new Guid(lastCra) == ConstAppDavid.DummyPointServiceCompany ? null : lastCra;

        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            LoadBrokerInformation();
            DetermineBrokerDULoginInfo();

            var masterCraList = MasterCRAList.RetrieveAvailableCras(this.Broker);

            #region Bind all available CRA to drop down list
            // This drop down list will have 3 sections: Broker frequent use CRA (in broker settings), CRA Lender Mapping (in LOAdmin),
            //                                           all available CRAs

            m_craList = new List<TemporaryCRAInfo>();
            CreditAgencyList.Items.Clear();
            CreditAgencyList.Items.Add(new ListItem("<-- Select Credit Agency -->", Guid.Empty.ToString()));

            #region Group 1 - Broker frequent use CRA

            bool hasBrokerCra = false;
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", BrokerID)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerID, "ListBrokerCra", parameters)) 
            {
                while (reader.Read()) 
                {
                    Guid serviceComId = (Guid)reader["ServiceComId"];

                    CRA cra = masterCraList.FirstOrDefault(o => o.ID == serviceComId);

                    if (null != cra)
                    {
                        CreditAgencyList.Items.Add(new ListItem(cra.VendorName, cra.ID.ToString()));
                        hasBrokerCra = true;
                    }
                }
            }

            if (hasBrokerCra) 
                CreditAgencyList.Items.Add(new ListItem("---------------------------", Guid.Empty.ToString()));

            #endregion
            
            #region Group 2 - CRA Lender Mapping
            bool hasLenderMappingCRA = false;
            var lenderMappedCraDict = new Dictionary<string, CRABureauInfo>();
            foreach (var proxy in CreditReportAccountProxy.Helper.ListCreditProxyByBrokerID(BrokerID))
            {
                TemporaryCRAInfo cra = new TemporaryCRAInfo();
                cra.ProtocolId = proxy.CrAccProxyId.ToString();
                cra.VendorName = proxy.CrAccProxyDesc;
                cra.ProtocolType = CreditReportProtocol.LenderMappingCRA;
                cra.AccountID = proxy.CraAccId;
                cra.IsEquifaxPulled = proxy.IsEquifaxPulled;
                cra.IsExperianPulled = proxy.IsExperianPulled;
                cra.IsTransUnionPulled = proxy.IsTransUnionPulled;

                m_craList.Add(cra);
                CreditAgencyList.Items.Add(new ListItem(cra.VendorName, cra.ProtocolId));
                hasLenderMappingCRA = true;
                lenderMappedCraDict.Add(
                    cra.ProtocolId,
                     new CRABureauInfo()
                     {
                         IsEquifaxPulled = cra.IsEquifaxPulled,
                         IsExperianPulled = cra.IsExperianPulled,
                         IsTransUnionPulled = cra.IsTransUnionPulled,
                         IsFanniePulled = proxy.IsFannieMaePulled,
                     });
            }

            if (hasLenderMappingCRA)
            {
                CreditAgencyList.Items.Add(new ListItem("---------------------------", Guid.Empty.ToString()));

                RegisterJsStruct("lenderMappedCRAs", lenderMappedCraDict);
            }

            #endregion

            #region Group 3 - All Available CRA that we supported


            foreach (CRA o in masterCraList) 
            {
                TemporaryCRAInfo cra = new TemporaryCRAInfo();
                cra.VendorName = o.VendorName;
                cra.ProtocolId = o.ID.ToString();
                cra.ProtocolType = o.Protocol;
                cra.ProtocolUrl = o.Url;
                cra.IsPdfViewNeeded = false;
                m_craList.Add(cra);

                CreditAgencyList.Items.Add(new ListItem(o.VendorName,  o.ID.ToString()));
            }
            #endregion

            #endregion
            Dictionary<string, int> creditHash = m_craList.Where(cra => cra.ProtocolType != CreditReportProtocol.Mcl).ToDictionary(cra => cra.ProtocolId, cra => (int)cra.ProtocolType);
            this.RegisterJsObjectWithJsonNetSerializer("creditHash", creditHash);

            Tools.Bind_MclCreditReportType(mclReportTypeDropdown);
            Tools.Bind_aMaritalStatT(aBMaritalStatT);
            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);

            aBZip.SmartZipcode(aBCity, aBState);
            aBPrev1Zip.SmartZipcode(aBPrev1City, aBPrev1State);

            this.PageTitle = "Order Credit";
            this.PageID = "OrderCredit";

            Page.ClientScript.RegisterHiddenField("DeleteLiabilities", "");
            Page.ClientScript.RegisterHiddenField("StatusQuery", ""); // For InfoNetwork
            Page.ClientScript.RegisterHiddenField("BillingFirstName", "");
            Page.ClientScript.RegisterHiddenField("BillingLastName", "");
            Page.ClientScript.RegisterHiddenField("BillingStreetAddress", "");
            Page.ClientScript.RegisterHiddenField("BillingCity", "");
            Page.ClientScript.RegisterHiddenField("BillingState", "");
            Page.ClientScript.RegisterHiddenField("BillingZipcode", "");
            Page.ClientScript.RegisterHiddenField("BillingCardNumber", "");
            Page.ClientScript.RegisterHiddenField("BillingExpirationMonth", "");
            Page.ClientScript.RegisterHiddenField("BillingExpirationYear", "");
            Page.ClientScript.RegisterHiddenField("BillingCVV", "");
            

            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("SimplePopups.js");
            this.RegisterJsGlobalVariables("DisableCreditOrderingViaBJP", ConstStage.DisableCreditOrderingViaBJP);
        }

        override protected void OnInit(EventArgs e)
        {
            this.IsAppSpecific = true;
            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;

            this.Load += new EventHandler(this.PageLoad);
            this.Init += new EventHandler(this.PageInit);
            base.OnInit(e);
        }

        protected void PageLoad(object sender, EventArgs e)
        {
        }
    }
}
