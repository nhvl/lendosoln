using System;
using System.Linq;
using System.Xml;
using DataAccess;
using LendersOffice.Audit;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.CreditReport;
using LendersOffice.CreditReport.FannieMae;
using LendersOffice.DU;
using LendersOffice.ObjLib.ServiceCredential;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Services
{
    public partial class DuSubmissionService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "SubmitDU":
                    SubmitDU();
                    break;
                case "Logout":
                    Logout();
                    break;
                case "SaveCaseExport":
                    SaveCaseExport();
                    break;
            }
        }

        private void SaveCaseExport() 
        {
            Guid sLId = GetGuid("LoanID");
            FnmaXisCasefileExportResponse response = new FnmaXisCasefileExportResponse(sLId);
            if (response.IsValid) 
            {
                SaveCasefileExportResponse(response);
            }
        }
        private void Logout() 
        {
            Guid sLId = Guid.Empty;
            bool isDo = GetBool("IsDo");
            string fannieMaeMornetUserId = GetString("FannieMaeMORNETUserID");
            string fannieMaeMornetPassword = GetString("FannieMaeMORNETPassword");

            FnmaDwebLogoutRequest request = new FnmaDwebLogoutRequest(isDo, sLId);
            request.FannieMaeMORNETUserID = fannieMaeMornetUserId;
            request.FannieMaeMORNETPassword = fannieMaeMornetPassword;
            DUServer.Submit(request);
        }
        private AbstractFnmaXisRequest CreateDuUnderwriteRequest() 
        {
            Guid sLId = GetGuid("LoanID");
            string fannieMaeMornetUserId = GetString("FannieMaeMORNETUserID", "");
            string fannieMaeMornetPassword = GetString("FannieMaeMORNETPassword", "");
            string globallyUniqueIdentifier = GetString("GloballyUniqueIdentifier");
            string craProviderName = GetString("CraProviderName","");
            string craLoginName = GetString("CraLoginName", "");
            string craPassword = GetString("CraPassword", "");

            CPageData dataLoan = null;
            BrokerUserPrincipal brokerUser = BrokerUserPrincipal.CurrentPrincipal;
            if (brokerUser.BrokerDB.UsingLegacyDUCredentials)
            {
                if (brokerUser.HasDuAutoLogin)
                {
                    if (string.IsNullOrEmpty(brokerUser.DuAutoLoginNm) == false)
                    {
                        fannieMaeMornetUserId = brokerUser.DuAutoLoginNm;
                    }
                    if (string.IsNullOrEmpty(brokerUser.DuAutoPassword) == false)
                    {
                        fannieMaeMornetPassword = brokerUser.DuAutoPassword;
                    }
                }
            }
            else
            {
                dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(DuSubmissionService));
                dataLoan.InitLoad();

                var savedCredential = ServiceCredential.ListAvailableServiceCredentials(brokerUser, dataLoan.sBranchId, ServiceCredentialService.AusSubmission)
                                            .FirstOrDefault(credential => credential.ChosenAusOption.HasValue && credential.ChosenAusOption.Value == AusOption.DesktopUnderwriter);
                if (savedCredential != null)
                {
                    fannieMaeMornetUserId = savedCredential.UserName;
                    fannieMaeMornetPassword = savedCredential.UserPassword.Value;
                }
            }

            if (brokerUser.HasDuAutoLogin)
            {
                if (string.IsNullOrEmpty(brokerUser.DuAutoCraId) == false) 
                {
                    craProviderName = brokerUser.DuAutoCraId;
                }
                if (string.IsNullOrEmpty(brokerUser.DuAutoCraLogin) == false)
                {
                    craLoginName = brokerUser.DuAutoCraLogin;
                }
                if (string.IsNullOrEmpty(brokerUser.DuAutoCraPassword) == false)
                {
                    craPassword = brokerUser.DuAutoCraPassword;
                }
            }

            bool importLiabitiesFromCreditReport = GetBool("ImportLiabitiesFromCreditReport");
            string creditOption = GetString("CreditOption");
            FnmaXisDuUnderwriteRequest request = new FnmaXisDuUnderwriteRequest(sLId);
            request.CopyLiabilitiesIndicator = importLiabitiesFromCreditReport;

            if (creditOption.Equals("UseExisting", StringComparison.OrdinalIgnoreCase))
            {
                request.SkipCreditSection = true;
            }
            request.FannieMaeMORNETUserID = fannieMaeMornetUserId;
            request.FannieMaeMORNETPassword = fannieMaeMornetPassword;
            request.GloballyUniqueIdentifier = globallyUniqueIdentifier;
            request.CraProviderNumber = craProviderName;
            request.CraLoginName = craLoginName;
            request.CraPassword = craPassword;

            if (string.IsNullOrEmpty(globallyUniqueIdentifier) == true)
            {
                if (dataLoan == null)
                {
                    dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(DuSubmissionService));
                    dataLoan.InitLoad();
                }

                dataLoan.SetFormatTarget(FormatTarget.FannieMaeMornetPlus);
                for (int i = 0; i < dataLoan.nApps; i++)
                {
                    CAppData dataApp = dataLoan.GetAppData(i);
                    E_XisCreditInformationRequestType creditRequestType = dataApp.aHasSpouse ? E_XisCreditInformationRequestType.Joint : E_XisCreditInformationRequestType.Individual;
                    string creditReference = GetString("CreditReference" + i, string.Empty);

                    XisCreditInformation creditInformation = new XisCreditInformation(dataApp.aBSsn, creditReference, creditRequestType);
                    request.AddCreditInformation(creditInformation);
                }
            }
            

            return request;

        }
        private void SubmitDU() 
        {

            Guid sLId = GetGuid("LoanID");
            string globallyUniqueIdentifier = GetString("GloballyUniqueIdentifier");
            string status = "ERROR";
            string errorMessage = "";
            string sDuCaseId = GetString("sDuCaseId");
            string sDuLenderInstitutionId = GetString("sDuLenderInstitutionId");
            string creditOption = GetString("CreditOption");
            if (globallyUniqueIdentifier == "") 
            {
                // 9/4/2007 dd - Only save load data if not a polling request.
                SaveData();
            }

            try 
            {
                AbstractFnmaXisRequest request = CreateDuUnderwriteRequest();

                if (null != request) 
                {
                    AbstractFnmaXisResponse response = DUServer.Submit(request);

                    if (response.IsReady) 
                    {
                        if (response.HasAnyError) 
                        {
                            status = "ERROR";
                            errorMessage = response.ErrorInHtml;
                        }
                        else 
                        {
                            status = "DONE";
                            sDuCaseId = response.MornetPlusCasefileIdentifier;
                            sDuLenderInstitutionId = response.LenderInstitutionIdentifier;
                        }
                        SaveDuUnderwriteResponse((FnmaXisDuUnderwriteResponse)response);
                    } 
                    else 
                    {
                        status = "PROCESSING";
                    }
                    globallyUniqueIdentifier = response.GloballyUniqueIdentifier;
                }
            } 
            catch (CBaseException exc) 
            {
                status= "ERROR";
                errorMessage = exc.UserMessage;
                Tools.LogErrorWithCriticalTracking("Unable to submit to DU", exc);
                
            }


            SetResult("Status", status);
            SetResult("ErrorMessage", errorMessage);
            SetResult("GloballyUniqueIdentifier", globallyUniqueIdentifier);
            SetResult("sDuCaseId", sDuCaseId);
            SetResult("sDuLenderInstitutionId", sDuLenderInstitutionId);

            
        }

        private int sFileVersion
        {
            get { return GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck); }
        }
        private void SaveData() 
        {
            Guid sLId = GetGuid("LoanID");
            string sDuCaseId = GetString("sDuCaseId");
            string sDuLenderInstitutionId = GetString("sDuLenderInstitutionId");

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(DuSubmissionService));
            dataLoan.InitSave(sFileVersion);

            if (sDuCaseId != dataLoan.sDuCaseId || sDuLenderInstitutionId != dataLoan.sDuLenderInstitutionId)
            {
                dataLoan.sDuLenderInstitutionId = sDuLenderInstitutionId;
                dataLoan.sDuCaseId = sDuCaseId;
                dataLoan.Save();
            }
            SetResult("sFileVersion", dataLoan.sFileVersion);
        }

        private void SaveDuUnderwriteResponse(FnmaXisDuUnderwriteResponse response) 
        {
            Guid sLId = GetGuid("LoanID");
            bool importLiabitiesFromCreditReport = GetBool("ImportLiabitiesFromCreditReport");
            response.ImportToLoanFile(sLId, BrokerUserPrincipal.CurrentPrincipal, true, true, importLiabitiesFromCreditReport);
        }

        private void SaveCasefileExportResponse(FnmaXisCasefileExportResponse response) 
        {
            Guid sLId = GetGuid("LoanID");
            int version = 0;
            response.SaveToFileDB(sLId);
            try 
            {
                LendersOffice.Conversions.FannieMae32Importer fnma32Importer = new LendersOffice.Conversions.FannieMae32Importer();
                fnma32Importer.ImportIntoExisting(response.Fnma32Content, sLId);

                CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(DuSubmissionService));
                dataLoan.InitSave(sFileVersion);
                dataLoan.SaveXisCasefileExportResponse(response, true);
                dataLoan.Save();
                version = dataLoan.sFileVersion;

                for (int i = 0; i < dataLoan.nApps; i++) 
                {
                    CAppData dataApp = dataLoan.GetAppData(i);
                    XmlDocument doc = response.RetrieveMismoCreditReportBySsn(dataApp.aBSsn);

                    FannieMae_CreditReportResponse fannieMaeCreditResponse = new FannieMae_CreditReportResponse(doc);


                    CreditReportUtilities.SaveXmlCreditReport(fannieMaeCreditResponse, BrokerUserPrincipal.CurrentPrincipal, dataLoan.sLId, dataApp.aAppId, ConstAppDavid.DummyFannieMaeServiceCompany, Guid.Empty, E_CreditReportAuditSourceT.ImportFromDoDu);
                                        
                }

            } 
            catch (Exception exc) 
            {
                Tools.LogError(exc);
                throw new CBaseException("Unable to import data to loan file.", exc);
            }
            SetResult("sFileVersion", version);

        }
    }
}
