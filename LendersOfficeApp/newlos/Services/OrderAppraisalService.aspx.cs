﻿namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Web.UI.WebControls;
    using AuthTicket;
    using DataAccess;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Integration.Appraisals;
    using LendersOffice.ObjLib.Appraisal;
    using LendersOffice.ObjLib.Conversions;
    using LendersOffice.ObjLib.Conversions.GlobalDMS;
    using LendersOffice.ObjLib.Conversions.LQBAppraisal;
    using LendersOffice.Security;
    using LQBAppraisal;
    using LQBAppraisal.LQBAppraisalRequest;
    using LQBAppraisal.LQBAppraisalResponse;

    public partial class OrderAppraisalService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        private const int DDL_SENTINEL_VALUE = -1;

        protected override void Process(string methodName)
        {
            switch (methodName) 
            {
                case "AttachGDMSDocuments":
                    AttachGDMSDocuments();
                    break;
                case "AttachFrameworkDocuments":
                    AttachFrameworkDocuments();
                    break;
                case "LoadDocumentTypeOptions":
                    LoadDocumentTypeOptions();
                    break;
                case "LoadOptions":
                    LoadOptions();    
                    break;
                case "SubmitOrder":
                    SubmitOrder();
                    break;
                case "SendEmail":
                    SendEmail();
                    break;
            }
        }

        private void AttachGDMSDocuments()
        {
            Guid vendorId = GetGuid("VendorId");
            AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(vendorId);
            GDMSCredentials credentials;
            bool credentialsBuilt = GetGDMSCredentials(vendor, out credentials);
            int fileNumber = GetInt("AppraisalOrderNumber");
            if (credentialsBuilt && AttachGDMSDocumentsToOrder(credentials, fileNumber))
                SetResult("Success", "True");
            else
                SetResult("Success", "False");
        }

        private void AttachFrameworkDocuments()
        {
            string orderNumber = GetString("AppraisalOrderNumber");
            Guid LoanID = GetGuid("LoanId");
            LQBAppraisalOrder order = LQBAppraisalOrder.Load(LoanID.ToString(), orderNumber);
            if (order == null)
                throw new CBaseException("Order Number not found", "Failed to load order number " + orderNumber);

            AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(GetGuid("VendorId"));
            var cred = new AppraisalVendorCredentials(PrincipalFactory.CurrentPrincipal.BrokerId, PrincipalFactory.CurrentPrincipal.EmployeeId, vendor.VendorId);

            var docs = new List<EmbeddedDoc>();
            List<Guid> docIds = null;
            var ids = ObsoleteSerializationHelper.JavascriptJsonDeserializer<string[]>(GetString("AttachedDocumentsIds"));
            order.AppraisalOrder.EmbeddedDocList.Clear();
            if (ids != null)
            {
                docIds = ids.Select(id => new Guid(id)).ToList();
                EDocumentRepository repo = EDocumentRepository.GetUserRepository();
                foreach (Guid docId in docIds)
                {
                    EDocument edoc = repo.GetDocumentById(docId);
                    EmbeddedDoc embeddedDoc = new EmbeddedDoc();
                    embeddedDoc.DocumentFormat = E_EmbeddedDocDocumentFormat.PDF;
                    embeddedDoc.DocumentName = edoc.Folder.FolderNm + " : " + edoc.DocType.DocTypeName;

                    byte[] docBytes = BinaryFileHelper.ReadAllBytes(edoc.GetPDFTempFile_Current());
                    embeddedDoc.Content = Convert.ToBase64String(docBytes);
                    docs.Add(embeddedDoc);
                }
                order.AppraisalOrder.EmbeddedDocList.AddRange(docs);
            }

            order.AppraisalOrder.OrderType = E_AppraisalOrderOrderType.UpdateOrder;
            order.AppraisalOrder.OrderNumber = orderNumber;
            order.AppraisalOrder.OrderInfo.TransactionID = Guid.NewGuid().ToString();

            var request = LQBAppraisalExporter.CreateAppraisalOrderRequest(cred, order.AppraisalOrder);
            var response = AppraisalVendorServer.Submit(request, vendor.PrimaryExportPath);

            SetResult("ErrorMessage", CreateErrorMessage(response.ServerResponse));
            if (response.ServerResponse.RequestStatus == E_ServerResponseRequestStatus.Success)
            {
                if (docIds != null)
                {
                    order.EmbeddedDocumentIds.AddRange(docIds);
                }
                order.OrderedDate = CDateTime.Create(DateTime.Now);
                order.Save();
                SetResult("Success", "True");
            }
            else
                SetResult("Success", "False");
        }

        private void LoadDocumentTypeOptions()
        {
            Guid vendorId = GetGuid("VendorId");
            AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(vendorId);
            SetResult("IsGdms", vendor.UsesGlobalDMS);
            if (vendor.UsesGlobalDMS)
            {
                GDMSCredentials credentials;
                if (!GetGDMSCredentials(vendor, out credentials))
                    return;
                using (var lookupMethodsClient = new LookupMethodsClient(credentials))
                {
                    try
                    {
                        var orderFileUploadFileTypes = lookupMethodsClient.GetOrderFileUploadFileTypes();
                        orderFileUploadFileTypes = Array.FindAll(orderFileUploadFileTypes, o => o.ViewableByClient);

                        IEnumerable<string[]> options; // string[] = DDL option: ["Value", "Text"]
                        options = orderFileUploadFileTypes.Select<LendersOffice.GDMS.LookupMethods.OrderFileUploadFileType, string[]>(o => new string[] { o.FileTypesId.ToString(), o.FileTypeDescription });
                        SetResult("GDMSOrderFileUploadFileType", ObsoleteSerializationHelper.JsonSerialize(options));
                    }
                    catch (GDMSErrorResponseException exc)
                    {
                        SetResult("ErrorMessage", CreateErrorMessage(exc));
                        Tools.LogError(exc);
                        return;
                    }
                }
            }
            SetResult("IsValid", "True");
        }

        private void LoadOptions()
        {
            Guid vendorId = GetGuid("SelectedAMC");
            AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(vendorId);
            SetResult("IsGdms", vendor.UsesGlobalDMS);
            AppraisalVendorCredentials cred = new AppraisalVendorCredentials(PrincipalFactory.CurrentPrincipal.BrokerId, PrincipalFactory.CurrentPrincipal.EmployeeId, vendorId);
            if (!vendor.UsesGlobalDMS)
            {
                LQBAppraisalRequest request = LQBAppraisalExporter.CreateAccountInfoRequest(cred, GetGuid("LoanID"), vendor.VendorName);
                LQBAppraisalResponse response = AppraisalVendorServer.Submit(request, vendor.PrimaryExportPath);
                if (response.ServerResponse.RequestStatus == E_ServerResponseRequestStatus.Success)
                {
                    List<AppraisalOrderProduct> products = new List<AppraisalOrderProduct>();
                    foreach (AvailableProduct prod in response.AccountInfoResponse.AvailableProductList)
                    {
                        products.Add(new AppraisalOrderProduct(prod.ProductName, ""));
                        foreach (AvailableSecondaryProduct prod2 in prod.AvailableSecondaryProductList)
                        {
                            products.Add(new AppraisalOrderProduct(prod2.ProductName, prod.ProductName));
                        }
                    }
                    var billing = response.AccountInfoResponse.BillingMethodList.Select(method => new BillingMethodDetails(method));
                    SetResult("ProductsList", ObsoleteSerializationHelper.JsonSerialize(products.ToArray()));
                    SetResult("BillingList", SerializationHelper.JsonNetAnonymousSerialize(billing)); 
                    SetResult("IsValid", "True");
                }
                SetResult("ErrorMessage", CreateErrorMessage(response.ServerResponse));
            }
            else
            {
                GDMSCredentials m_credentials;
                if (!GetGDMSCredentials(vendor, out m_credentials))
                    return; //the credentials were not able to be properly created
                using (var lookupMethodsClient = new LookupMethodsClient(m_credentials))
                {
                    try
                    {
                        var propertyTypes = lookupMethodsClient.GetPropertyTypes();
                        var products = lookupMethodsClient.GetProducts();
                        var intendedUses = lookupMethodsClient.GetIntendedUses();
                        var loanTypes = lookupMethodsClient.GetLoanTypes();
                        var occupancyTypes = lookupMethodsClient.GetOccupancyTypes();

                        var client2Users = lookupMethodsClient.GetClient2Users();
                        var clientUsers = lookupMethodsClient.GetClientUsers(0);
                        
                        var billingMethods = lookupMethodsClient.GetBillingMethods();

                        var orderFileUploadFileTypes = lookupMethodsClient.GetOrderFileUploadFileTypes();
                        orderFileUploadFileTypes = Array.FindAll(orderFileUploadFileTypes, o => o.ViewableByClient);

                        IEnumerable<string[]> options; // string[] = DDL option: ["Value", "Text"]
                        options = propertyTypes.Select<LendersOffice.GDMS.LookupMethods.PropertyType, string[]>(o => new string[] { o.PropertyTypeId.ToString(), o.PropertyTypeName });
                        SetResult("GDMSPropertyType", ObsoleteSerializationHelper.JsonSerialize(options));

                        options = products.Select<LendersOffice.GDMS.LookupMethods.Product, string[]>(o => new string[] { o.ProductID.ToString(), o.ProductFriendlyName });
                        SetResult("GDMSReportType", ObsoleteSerializationHelper.JsonSerialize(options));

                        options = intendedUses.Select<LendersOffice.GDMS.LookupMethods.LoanType, string[]>(o => new string[] { o.LoanTypeId.ToString(), o.LoanTypeName });
                        SetResult("GDMSIntendedUse", ObsoleteSerializationHelper.JsonSerialize(options));

                        options = loanTypes.Select<LendersOffice.GDMS.LookupMethods.LoanType, string[]>(o => new string[] { o.LoanTypeId.ToString(), o.LoanTypeName });
                        SetResult("GDMSLoanType", ObsoleteSerializationHelper.JsonSerialize(options));

                        options = occupancyTypes.Select<LendersOffice.GDMS.LookupMethods.OccupancyType, string[]>(o => new string[] { o.OccupancyID.ToString(), o.OccupancyName });
                        SetResult("GDMSOccupancyType", ObsoleteSerializationHelper.JsonSerialize(options));

                        var billingMethodViews = billingMethods.Select(o => new BillingMethodDetails(o));
                        SetResult("GDMSBillingMethod", SerializationHelper.JsonNetAnonymousSerialize(billingMethodViews));

                        options = client2Users.Select<LendersOffice.GDMS.LookupMethods.clsClientForDropDown, string[]>(o => new string[] { o.ClientID.ToString(), o.ClientName });
                        SetResult("GDMSClient2", ObsoleteSerializationHelper.JsonSerialize(options));

                        options = clientUsers.Select<LendersOffice.GDMS.LookupMethods.clsClientUser, string[]>(o => new string[] { o.ClientUserID.ToString(), o.UserFullName });
                        SetResult("GDMSProcessor", ObsoleteSerializationHelper.JsonSerialize(options));

                        options = clientUsers.Select<LendersOffice.GDMS.LookupMethods.clsClientUser, string[]>(o => new string[] { o.ClientUserID.ToString(), o.UserFullName });
                        SetResult("GDMSProcessor2", ObsoleteSerializationHelper.JsonSerialize(options));

                        options = orderFileUploadFileTypes.Select<LendersOffice.GDMS.LookupMethods.OrderFileUploadFileType, string[]>(o => new string[] { o.FileTypesId.ToString(), o.FileTypeDescription });
                        SetResult("GDMSOrderFileUploadFileType", ObsoleteSerializationHelper.JsonSerialize(options));
                    }

                    catch (GDMSErrorResponseException exc)
                    {
                        SetResult("ErrorMessage", CreateErrorMessage(exc));
                        Tools.LogError(exc);
                        return;
                    }
                }
                SetResult("IsValid", "True");
            }
        }

        [DataContract]
        private class AppraisalOrderProduct
        {
			//used for Javascript serialization for product dependencies - OPM 125002
            public AppraisalOrderProduct(string n, string r)
            {
                name = n;
                requires = r;
            }
            [DataMember]
            public string name { get; set; }
            [DataMember]
            public string requires { get; set; }
        }

        private string CreateErrorMessage(GDMSErrorResponseException exc)
        {
            StringBuilder errorMsg = new StringBuilder();
            errorMsg.AppendLine("There was an error processing your request.");
            errorMsg.AppendFormat(" {0}.", exc.ErrorMessage);
            foreach (string error in exc.Errors)
            {
                errorMsg.AppendFormat(" {0}.", error);
            }

            return errorMsg.ToString();
        }

        private string CreateErrorMessage(ServerResponse exc)
        {
            if (exc.ServerMessageList.Count == 0)
                return "";
            StringBuilder errorMsg = new StringBuilder();

            if (exc.RequestStatus != E_ServerResponseRequestStatus.Success)
                errorMsg.AppendLine("There was an error processing your request.\n");

            var errors = exc.ServerMessageList;
            errors.Sort(ErrorMessageSortComparer.Instance);
            foreach (ServerMessage error in errors)
            {
                string errorType;
                switch(error.Type)
                {
                    case E_ServerMessageType.DataError:
                        errorType = "Data Error";
                        break;
                    case E_ServerMessageType.LoginError:
                        errorType = "Login Error";
                        break;
                    case E_ServerMessageType.ServerError:
                        errorType = "Server Error";
                        break;
                    case E_ServerMessageType.Other:
                    case E_ServerMessageType.Undefined:
                        errorType="Warning";
                        break;
                    default:
                        throw new UnhandledEnumException(error.Type);
                }
                errorMsg.AppendFormat("({0}) {1}\n", errorType, error.Description);
            }

            return errorMsg.ToString();
        }
        private class ErrorMessageSortComparer : IComparer<ServerMessage>
        {
            public static readonly ErrorMessageSortComparer Instance = new ErrorMessageSortComparer();
            private ErrorMessageSortComparer() { }
            public int Compare(ServerMessage x, ServerMessage y)
            {
                var sortOrder = new List<E_ServerMessageType> { E_ServerMessageType.ServerError, E_ServerMessageType.LoginError, E_ServerMessageType.DataError, E_ServerMessageType.Other, E_ServerMessageType.Undefined };
                int xIndex = sortOrder.IndexOf(x.Type);
                int yIndex = sortOrder.IndexOf(y.Type);
                if (xIndex == -1) xIndex = sortOrder.Count;
                if (yIndex == -1) yIndex = sortOrder.Count;
                return xIndex.CompareTo(yIndex);
            }
        }

        public GdmsOrderRequestDetails CreateGdmsRequestDetails()
        {
            GdmsOrderRequestDetails details = new GdmsOrderRequestDetails();
            details.Property = new LendersOffice.Integration.Appraisals.Property();
            details.Property.Address = this.GetString("PropertyAddress");
            details.Property.City = this.GetString("PropertyCity");
            details.Property.State = this.GetString("PropertyState");
            details.Property.Zip = this.GetString("PropertyZip");
            Guid reoId = this.GetGuid("ReoId");
            details.LinkedReoId = reoId == Guid.Empty ? (Guid?)null : reoId;

            details.CaseNumber = this.GetString("GDMSCaseNum");
            details.ContactName = this.GetString("GDMSContactName");
            details.ContactPhone = this.GetString("GDMSContactPhone");
            details.ContactPhoneWork = this.GetString("GDMSContactWork");
            details.ContactPhoneOther = this.GetString("GDMSContactOther");

            details.PropertyTypeId = this.GetInt("GDMSPropertyTypeId");
            details.PropertyTypeName = this.GetString("GDMSPropertyTypeName");
            
            details.ReportTypeName = this.GetString("GDMSReportTypeName");
            details.ReportType1Id = this.GetInt("GDMSReportTypeId");
            details.ReportType2Id = this.GetInt("GDMSReportType2Id");
            details.ReportType3Id = this.GetInt("GDMSReportType3Id");
            details.ReportType4Id = this.GetInt("GDMSReportType4Id");
            details.ReportType5Id = this.GetInt("GDMSReportType5Id");

            details.AppraisalNeededBy = GetString("GDMSAppraisalNeeded", string.Empty).ToNullable<DateTime>(DateTime.TryParse);

            details.Client2Id = GetInt("GDMSClient2Id");
            details.ProcessorId = GetInt("GDMSProcessorId");
            details.Processor2Id = GetInt("GDMSProcessor2Id");
            details.BillingMethodId = GetInt("GDMSBillingMethodId");

            details.IntendedUseName = this.GetString("GDMSIntendedUseName");
            details.IntendedUseId = this.GetInt("GDMSIntendedUseId");

            details.LoanTypeName = this.GetString("GDMSLoanTypeName");
            details.LoanTypeId = this.GetInt("GDMSLoanTypeId");

            details.OccupancyTypeName = this.GetString("GDMSOccupancyTypeName");
            details.OccupancyTypeId = this.GetInt("GDMSOccupancyTypeId");

            details.LenderName = this.GetString("GDMSLenderName");
            details.Notes = GetString("Notes");

            details.BillingMethodDescription = GetString("GDMSBillingMethodName");

            details.Billing = new BillingDetails();
            details.Billing.Name = this.GetString("CCBillName", string.Empty);
            details.Billing.Street = this.GetString("CCAddress1", string.Empty);
            details.Billing.City = this.GetString("CCCity", string.Empty);
            details.Billing.State = this.GetString("CCState", string.Empty);
            details.Billing.Zip = this.GetString("CCZip", string.Empty);
            details.Billing.CreditCardType = GetString("CCType", string.Empty);
            details.Billing.CreditCardNumber = this.GetString("CCNumber", string.Empty);
            details.Billing.CreditCardExpirationMonth = this.GetInt("CCExpMonth", -1);
            details.Billing.CreditCardExpirationYear = this.GetInt("CCExpYear", -1);
            details.Billing.CreditCardSecurityCode = this.GetString("CCCode", string.Empty);

            details.OrderFileUploadTypeId = this.GetInt("AttachmentType");
            var ids = ObsoleteSerializationHelper.JavascriptJsonDeserializer<string[]>(GetString("AttachedDocumentsIds"));
            if (ids != null)
            {
                details.DocumentIds = ids.Select(id => Guid.Parse(id)).ToList();
            }

            return details;
        }

        private LqbOrderRequestDetails CreateLqbOrderRequestDetails()
        {
            LqbOrderRequestDetails details = new LqbOrderRequestDetails();
            details.ContactName = this.GetString("GDMSContactName");
            details.ContactPhone = this.GetString("GDMSContactPhone");
            details.ContactPhoneWork = this.GetString("GDMSContactWork");
            details.ContactPhoneOther = this.GetString("GDMSContactOther");
            details.ContactEmail = this.GetString("GDMSContactEmail");
            details.ContactAditionalEmail = this.GetString("GDMSAdditionalEmail");

            details.Property = new LendersOffice.Integration.Appraisals.Property();
            details.Property.Address = this.GetString("PropertyAddress");
            details.Property.City = this.GetString("PropertyCity");
            details.Property.State = this.GetString("PropertyState");
            details.Property.Zip = this.GetString("PropertyZip");
            Guid reoId = this.GetGuid("ReoId");
            details.LinkedReoId = reoId == Guid.Empty ? (Guid?)null : reoId;

            details.AppraisalNeededBy = GetString("LQBAppraisalNeeded", string.Empty).ToNullable<DateTime>(DateTime.TryParse);

            details.IsRushOrder = this.GetBool("RushOrder");
            details.Notes = this.GetString("Notes");
            details.Products = new List<string>();
            for (int i = 1; i <= 5; i++)
            {
                var product = GetString("OrderReportType" + i);
                if (string.IsNullOrEmpty(product))
                {
                    continue;
                }

                details.Products.Add(product);
            }

            details.BillingMethod = this.GetString("GDMSBillingMethodName");
            details.CreditCardIndicator = GetEnum<E_TriState>("CreditCardIndicator");

            details.Billing = new BillingDetails();
            details.Billing.Name = GetString("CCBillName", string.Empty);
            details.Billing.Street = GetString("CCAddress1", string.Empty);
            details.Billing.City = GetString("CCCity", string.Empty);
            details.Billing.State = GetString("CCState", string.Empty);
            details.Billing.Zip = GetString("CCZip", string.Empty);

            details.Billing.CreditCardType = GetString("CCType", string.Empty);
            details.Billing.CreditCardNumber = GetString("CCNumber", string.Empty);
            details.Billing.CreditCardExpirationMonth = GetInt("CCExpMonth", -1);
            details.Billing.CreditCardExpirationYear = GetInt("CCExpYear", -1);
            details.Billing.CreditCardSecurityCode = GetString("CCCode", string.Empty);

            var ids = ObsoleteSerializationHelper.JavascriptJsonDeserializer<string[]>(GetString("AttachedDocumentsIds"));
            if (ids != null)
            {
                details.DocumentIds = ids.Select(id => Guid.Parse(id)).ToList();
            }

            details.OrderNumber = this.GetString("OrderNumber");

            return details;
        }

        private void SubmitOrder()
        {
            Guid LoanID = GetGuid("LoanID");

            AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(GetGuid("SelectedAMC"));
            AbstractOrderRequestDetails details;
            if (vendor.UsesGlobalDMS)
            {
                details = this.CreateGdmsRequestDetails();
            }
            else 
            {
                //not GDMS
                details = this.CreateLqbOrderRequestDetails();
            }

            AppraisalOrderSubmissionResponse result = AppraisalVendorFactory.SubmitOrder(PrincipalFactory.CurrentPrincipal, LoanID, details, vendor);
            if (result.Status == LendersOffice.ObjLib.Webservices.ServiceResultStatus.Error)
            {
                var errorList = new List<string>();
                errorList.Add("There was an error processing your request.");
                errorList.AddRange(result.GetErrorList());
                this.SetResult("ErrorMessage", string.Join(Environment.NewLine, errorList));
                return;
            }

            SetResult("Success", "True");
        }

        private void SendEmail()
        {
            Guid vendorId = GetGuid("VendorId");
            AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(vendorId);
            GDMSCredentials credentials;
            if (!GetGDMSCredentials(vendor, out credentials))
            {
                SetResult("ErrorMessage", "Unable to send email: Invalid Credentials");
                return;
            }

            var emailToSend = new LendersOffice.GDMS.Orders.SendEmailMessageObject();
            emailToSend.Subject = GetString("SubjectField");
            emailToSend.EmailMessage = GetString("MessageField");
            emailToSend.SendEmailToUserType = OrdersClient.sendEmailToWho(GetString("ToField"));

            var orderIdentification = new LendersOffice.GDMS.Orders.OrderIdentifier();
            orderIdentification.FileNumber = GetInt("AppraisalOrderNumber");

            string reply = "";
            using (var ordersClient = new OrdersClient(credentials))
            {
                try
                {
                    reply = ordersClient.SendEmailCommunication(emailToSend, orderIdentification);
                    SetResult("Success", "True");
                }
                catch (GDMSErrorResponseException exc)
                {
                    string errorMessage = CreateErrorMessage(exc);
                    foreach (string error in exc.Errors)
                    {
                        if (error == "Unable to send email: The message did not have a 'To' email address")
                        {
                            errorMessage = "Unable to send email: Appraiser not yet assigned to this order";
                            break;
                        }
                    }
                    SetResult("ErrorMessage", errorMessage);
                }
            }
        }

        private bool AttachGDMSDocumentsToOrder(GDMSCredentials credentials, int fileNumber)
        {
            List<Guid> docIds = null;
            var ids = ObsoleteSerializationHelper.JavascriptJsonDeserializer<string[]>(GetString("AttachedDocumentsIds"));
            if (ids != null)
            {
                docIds = ids.Select(id => new Guid(id)).ToList();
                EDocumentRepository repo = EDocumentRepository.GetUserRepository();
                StringBuilder errorList = new StringBuilder();
                foreach (Guid docId in docIds)
                {
                    EDocument edoc = repo.GetDocumentById(docId);
                    string fileName = edoc.FolderAndDocTypeName + ".pdf";
                    //need to make sure fileName has no invalid characters for a file name; otherwise GDMS rejects the file
                    fileName = fileName.Replace('/', '-').Replace('\\', '-').Replace('?', '-').Replace('%', '-').Replace('*', '-')
                            .Replace(':', '-').Replace('|', '-').Replace('"', '-').Replace('<', '-').Replace('>', '-');
                    string filePath = edoc.GetPDFTempFile_Current();
                    using (System.IO.MemoryStream fileData = new System.IO.MemoryStream(BinaryFileHelper.ReadAllBytes(filePath)))
                    {
                        using (var fileUploadClient = new FileUploadClient(credentials, fileName, fileNumber, GetInt("AttachmentType"), fileData))
                        {
                            try
                            {
                                var uploadResults = fileUploadClient.SubmitReport();
                                if (!uploadResults.success)
                                {
                                    if (errorList.Length != 0) errorList.AppendLine();
                                    errorList.AppendFormat("{0}:\n\t{1} did not upload properly.", uploadResults.message, fileName);
                                }
                            }
                            catch (Exception exc)
                            {
                                Tools.LogError(string.Format("Unable to upload file: {0} to ETrac.\n", fileName), exc);
                                if (errorList.Length != 0) errorList.AppendLine();
                                errorList.AppendFormat("Unable to upload file {0}. Please contact support if this happens again.", fileName);
                            }
                        }
                    }
                }
                if (errorList.Length != 0)
                {
                    SetResult("FileErrorMessage", errorList.ToString());
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return true;
        }

        private bool GetGDMSCredentials(AppraisalVendorConfig vendor, out GDMSCredentials credentials)
        {
            string errorMessage;
            credentials = vendor.GetGDMSCredentials(
                PrincipalFactory.CurrentPrincipal.BrokerId,
                PrincipalFactory.CurrentPrincipal.EmployeeId,
                out errorMessage);
            if (credentials == null)
            {
                SetResult("ErrorMessage", errorMessage);
                return false;
            }

            return true;
        }
    }
}