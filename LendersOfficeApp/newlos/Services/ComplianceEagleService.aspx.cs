﻿// <copyright file="ComplianceEagleService.aspx.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Timothy Jewell, Brian Beery
//    Date:   7/28/2014 12:09 PM 
// </summary>

namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::ComplianceEagle;
    using LendersOffice.AntiXss;
    using LendersOffice.Constants;
    using LendersOffice.Conversions.ComplianceEagleIntegration;
    using LendersOffice.Security;

    public partial class ComplianceEagleService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName) 
            {
                case "Export":
                    Export();
                    break;
            }
        }

        private void Export()
        {
            string errorMessage = string.Empty;
            string fileDbKey = string.Empty;
            Guid loanId = this.GetGuid("LoanID");
            Guid applicationId = this.GetGuid("ApplicationID");

            string userName = this.GetString("UserName");
            string password = this.GetString("Password");
            bool rememberCredentials = this.GetBool("RememberMe");
            bool isReview = string.Equals(this.GetString("OrderType"), "Review");

            var savedCredentials = ComplianceEagleUserCredentials.Retrieve(PrincipalFactory.CurrentPrincipal);
            if (password == ConstAppDavid.FakePasswordDisplay && !string.IsNullOrEmpty(savedCredentials.Password))
            {
                password = savedCredentials.Password;
            }

            string companyID = savedCredentials.CompanyID;

            savedCredentials.UserName = rememberCredentials ? userName : string.Empty;
            savedCredentials.Password = rememberCredentials ? password : string.Empty;
            savedCredentials.Save();

            // Export to ComplianceEagle using the local variables (saved authentication may no longer be trusted)
            this.SubmitCEagleOrder(userName, password, companyID, loanId, applicationId, isReview);
        }

        /// <summary>
        /// Submits a request to QuestSoft ComplianceEagle.
        /// </summary>
        /// <param name="username">The ComplianceEagle account of the user.</param>
        /// <param name="password">The ComplianceEagle account password.</param>
        /// <param name="customerID">The ComplianceEagle customer ID of the lender.</param>
        /// <param name="loanID">The sLId of the loan.</param>
        /// <param name="applicationID">The aAppId of the application.</param>
        /// <param name="isReview">Indicates whether this is a review order.</param>
        private void SubmitCEagleOrder(string username, string password, string customerID, Guid loanID, Guid applicationID, bool isReview)
        {
            var orderedProducts = this.CollectOrderedProducts();
            var requestProvider = new ComplianceEagleRequestProvider(username, password, customerID, loanID, orderedProducts);

            string request = requestProvider.CreateComplianceEagleRequest(false, isReview, PrincipalFactory.CurrentPrincipal);
            var principal = PrincipalFactory.CurrentPrincipal;

            string successSummary;
            string transmissionErrors;
            bool success = ComplianceEagleServer.SubmitRequest(
                principal.BrokerId,
                loanID,
                applicationID,
                requestProvider.TransactionId,
                request,
                orderedProducts,
                out successSummary,
                out transmissionErrors,
                principal);

            SetResult("SuccessSummary", successSummary);
            SetResult("TransmissionErrors", transmissionErrors);
            SetResult("Success", success);
        }

        /// <summary>
        /// Collects all ordered products into a single list.
        /// </summary>
        /// <returns>A list of the products ordered.</returns>
        private List<ComplianceEagleProductT> CollectOrderedProducts()
        {
            var products = new List<ComplianceEagleProductT>();

            this.CheckForProduct("MaventReview", ComplianceEagleProductT.MaventReview, products);
            this.CheckForProduct("HighCost", ComplianceEagleProductT.HighCost, products);
            this.CheckForProduct("MaventNmls", ComplianceEagleProductT.MaventNmls, products);
            this.CheckForProduct("HmdaCheck", ComplianceEagleProductT.HmdaCheck, products);
            this.CheckForProduct("Geocode", ComplianceEagleProductT.Geocode, products);
            this.CheckForProduct("RateSpreadCheck", ComplianceEagleProductT.RateSpreadCheck, products);
            this.CheckForProduct("CraReview", ComplianceEagleProductT.CraReview, products);
            this.CheckForProduct("FraudComplete", ComplianceEagleProductT.FraudComplete, products);
            this.CheckForProduct("FraudBorrower", ComplianceEagleProductT.FraudBorrower, products);
            this.CheckForProduct("FraudCollateral", ComplianceEagleProductT.FraudCollateral, products);
            this.CheckForProduct("ExclusionaryList", ComplianceEagleProductT.ExclusionaryList, products);
            this.CheckForProduct("ServiceProviderSearch", ComplianceEagleProductT.ServiceProviderSearch, products);
            this.CheckForProduct("FloodQuickCheck", ComplianceEagleProductT.FloodQuickCheck, products);
            this.CheckForProduct("Flood", ComplianceEagleProductT.Flood, products);
            this.CheckForProduct("RecordingFee", ComplianceEagleProductT.RecordingFee, products);
            this.CheckForProduct("LefCheck", ComplianceEagleProductT.LefCheck, products);
            this.CheckForProduct("IncomeReview", ComplianceEagleProductT.IncomeReview, products);
            this.CheckForProduct("SsnReview", ComplianceEagleProductT.SsnReview, products);
            this.CheckForProduct("QmReview", ComplianceEagleProductT.QmReview, products);
            this.CheckForProduct("AvmReview", ComplianceEagleProductT.AvmReview, products);
            this.CheckForProduct("AusReview", ComplianceEagleProductT.AusReview, products);
            this.CheckForProduct("NmlsMcrReview", ComplianceEagleProductT.NmlsMcrReview, products);

            return products;
        }

        /// <summary>
        /// Determines whether a single ComplianceEagle product was selected.
        /// </summary>
        /// <param name="checkboxName">The ID of the checkbox element to check.</param>
        /// <param name="product">The corresponding product enum.</param>
        /// <param name="orderedProducts">A complete list of ordered products.</param>
        private void CheckForProduct(string checkboxName, ComplianceEagleProductT product, List<ComplianceEagleProductT> orderedProducts)
        {
            if (this.GetBool(checkboxName))
            {
                orderedProducts.Add(product);
            }
        }
    }
}
