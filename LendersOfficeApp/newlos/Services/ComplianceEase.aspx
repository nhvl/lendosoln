﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComplianceEase.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.ComplianceEase" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ComplianceEase</title>
    <style>
    .ErrorMessage
    {
      color:red;
      font-weight:bold;
    }
    .notice
    {
        margin: 4px;
        width: 90%;
        background-color: Yellow;
        border: solid 1px black;
        color: Black;
        font-family: Arial, Helvetica, Sans-Serif;
        padding: 4px;
        font-size: 12px;
        font-weight: bold;
    }
    img[alt=required] {
        padding-left: 5px;
    }
    </style>
</head>
<body bgcolor="gainsboro">
  <script type="text/javascript">
    function _init()
    {
      $('#btnSubmit').click(btnSubmit_Click);
      $('#lnkViewPdf').click(viewComplianceEaseReport);
      $('.compliance-report-data').toggle(ML.HasReport);

      if ($('#m_rememberMe').prop('checked'))
      {
        $('#m_password').val(<%= AspxTools.JsString(ConstAppDavid.FakePasswordDisplay) %>);
      }
    }

    function btnSubmit_Click()
    {
      $('#ErrorMessageDiv').empty();
      var args = getAllFormValues();
      gService.loanedit.callAsyncSimple('Export', args,
        function exportSuccessCallback(result) {
            clearDirty(); // first thing service call does is save
            $('#sComplianceEaseId').val(result.value['sComplianceEaseId']);
            $('#sComplianceEaseStatus').val(result.value['sComplianceEaseStatus']);
            if (result.value.HasError !== 'False') {
                var errorMessage = result.value.ErrorMessageHtml;
                $('#ErrorMessageDiv').append(errorMessage);
                return;
            }

            ML.HasReport = true;
            $('.compliance-report-data').show();
            viewComplianceEaseReport();
        },
        function exportErrorCallback(result) {
            var errMsg = result.UserMessage || 'Unable to export data. Please try again.';
            alert(errMsg);
        });
      return false;
    }
    function viewComplianceEaseReport()
    {
      window.open('ComplianceEaseReport.aspx?loanid=' + ML.sLId + "&rand=" + new Date(), "_parent", "toolbar=no,menubar=no,location=no,status=no");
      return false;
    }
  </script>
    <form id="form1" runat="server">
    <table cellpadding="0" cellspacing="0" width="100%" border="0">
      <tbody>
        <tr><td class="MainRightHeader">ComplianceEase Integration</td></tr>
      </tbody>
    </table>
    <asp:Panel runat="server" ID="TestLoanPanel" Visible="false" CssClass="notice">
    <div>Please note that ComplianceEase UAT credentials are required for test files.</div>
    </asp:Panel>
    <table cellspacing="3" cellpadding="3" border="0" style="padding-left:5px">
      <tr>
        <td class="FieldLabel">ComplianceEase ID</td>
        <td><asp:TextBox ID="sComplianceEaseId" runat="server"/></td>
      </tr>
      <tr>
        <td class="FieldLabel">Audit Type</td>
        <td><asp:DropDownList ID="m_auditType" runat="server" NotForEdit="True"/></td>
      </tr>
      <tr>
        <td class="FieldLabel">User Name</td>
        <td><asp:TextBox ID="m_userName" runat="server" NotForEdit="True" Width="200px"/><img src="~/images/require_icon.gif" alt="required" runat="server" /></td>
      </tr>
      <tr>
        <td class="FieldLabel">Password</td>
        <td><asp:TextBox ID="m_password" runat="server" TextMode="Password" NotForEdit="True" Width="200px"/><img src="~/images/require_icon.gif" alt="required" runat="server" /></td>
      </tr>
      <tr>
        <td class="FieldLabel" colspan="2"><input type="checkbox" id="m_rememberMe" runat="server" NotForEdit="True"/><label for="m_rememberMe">Remember logon credentials</label></td>
      </tr>
      <tr>
        <td colspan="2"><asp:Button runat="server" ID="btnSubmit" Text="Perform Loan Audit" CausesValidation="false"/>&nbsp;&nbsp;</td>
      </tr>
      <tr class="compliance-report-data">
        <td>Compliance Risk: </td>
        <td><asp:TextBox ID="sComplianceEaseStatus" runat="server" ReadOnly="true"/></td>
      </tr>
      <tr class="compliance-report-data">
        <td><a id="lnkViewPdf" href="#">View Recent Audit Report</a></td>
      </tr>
    </table>
    <div id="ErrorMessageDiv" class="ErrorMessage" runat="server"></div>
    </form>
</body>
</html>
