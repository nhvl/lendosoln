﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ComplianceEase;
using DataAccess;
using LendersOffice.Conversions.ComplianceEase;
using LendersOffice.Admin;
using LendersOffice.Security;
using LendersOffice.Constants;
using System.Web.Services;
using LendersOffice.ObjLib.Conversions.ComplianceEase;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Services
{
    public partial class ComplianceEaseErrorSummary : BaseLoanPage
    {
        protected string GetClass(object error)
        {
            var err = (FriendlyError)error;
            if (err.HasField) return "has-field";
            return "";
        }


        protected override void OnInit(EventArgs args)
        {
            DisplayCopyRight = false;
            this.EnableJqueryMigrate = false;
            base.OnInit(args);
        }

        protected void Page_Load(object sender, EventArgs eventargs)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, this.GetType().BaseType);
            dataLoan.InitLoad();

            if (dataLoan.sIsInitialLoanEstimateDateAssociatedWithInvalidArchive)
            {
                this.customError.InnerText = ErrorMessages.ArchiveError.InitialLoanEstimateAssociatedWithInvalidArchiveAsyncCompliance;
                this.Rerun.Visible = false;
            }
            else
            {
                this.customError.Visible = false;
            }

            string faultXml = dataLoan.sComplianceEaseErrorMessage;

            if (string.IsNullOrEmpty(faultXml))
            {
                Errors.DataSource = new[] { new FriendlyError("No errors found") };
                Errors.DataBind();
                return;
            }

            ServiceFault sf = new ServiceFault(faultXml);

            if (sf.FaultCause.ErrorList.Count == 0)
            {
                Errors.DataSource = new[] { new FriendlyError("No errors found") };
                Errors.DataBind();
                return;
            }

            var FriendlyErrors = sf.FaultCause.ErrorList.Select((e) => ComplianceEaseExporter.GetFriendlyErrorMessage(e));

            Errors.DataSource = FriendlyErrors;
            Errors.DataBind();
        }

        [WebMethod]
        public static bool RerunAudit(Guid LoanID)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ComplianceEaseErrorSummary));
            dataLoan.InitLoad();

            var broker = BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            bool isPaidAccount = !string.IsNullOrEmpty(broker.ComplianceEaseUserName);

            string username;
            string password;
            if (isPaidAccount)
            {
                username = broker.ComplianceEaseUserName;
                password = broker.ComplianceEasePassword.Value;
            }
            else
            {
                username = ConstStage.ComplianceEasePTMTestLogin;
                password = ConstStage.ComplianceEasePTMTestPassword;
            }

            ServiceFault sf;
            dataLoan.sComplianceEaseStatus = "Updating";
            dataLoan.sComplianceEaseErrorMessage = "";
            bool didUpdate = false;
            return ComplianceEaseExporter.Export(
                sLId: dataLoan.sLId,
                userName: username,
                password: password,
                auditType: E_TransmittalDataComplianceAuditType.Undefined,
                serviceFault: out sf,
                asynchronous: true,
                expectedLoanVersion: dataLoan.sFileVersion,
                ignorePreviousChecksum: true,
                didUpdate: out didUpdate
                );
        }

        protected void RerunAudit_Click(object sender, System.EventArgs e)
        {
            RerunAudit(LoanID);
            Response.Redirect(Request.Url.ToString());
        }

    }
}
