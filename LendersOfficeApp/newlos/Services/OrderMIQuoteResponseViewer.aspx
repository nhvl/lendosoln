﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderMIQuoteResponseViewer.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.OrderMIQuoteResponseViewer" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MI Quote Response Viewer</title>
    <link href="../../css/stylesheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #applyQuoteButton, #orderPolicyDelegatedButton, #orderPolicyNonDelegatedButton
        {
            height: 50px;
            width: 200px;
            white-space: normal;
            display: flex;
            justify-content: space-around;
            align-items: center;
        }
        #closeButton
        {
            height: 2.25em;
            width: 8em;
        }
        .bold {
            font-weight: bold;
        }
        .width-200 {
            width: 200px;
        }
    </style>
</head>
<body class="EditBackground">
    <script type="text/javascript">
        $(window).on("load", function() {
            $('#_ReadOnly').val("True"); // Disable saving on this page.

            if (!ML.HasCredentials) {
                $('#orderPolicyDelegatedButton').hide();
                $('#orderPolicyNonDelegatedButton').hide();
                $('#applyQuoteButton').hide();
            }
            else {
                $('#applyQuoteButton').click(f_onApplyQuoteButton_Click)

                if (!ML.CanOrderPolicyNonDelegated) {
                    $('#orderPolicyNonDelegatedButton')
                        .prop({
                            disabled: true,
                            title: ML.PolicyNonDelegatedDenialReason
                        })
                        .find('.WorkflowWarningIcon').toggle(true);
                }
                else {
                    $('#orderPolicyNonDelegatedButton').click(f_onOrderPolicyNonDelegated_Click)
                        .find('.WorkflowWarningIcon').toggle(false);;
                }

                if (!ML.CanRequestBeDelegated) {
                    $('#orderPolicyDelegatedButton').hide();
                }
                else if (!ML.CanOrderPolicyDelegated) {
                    $('#orderPolicyDelegatedButton')
                        .prop({
                            disabled: true,
                            title: ML.PolicyDelegatedDenialReason
                        })
                        .find('.WorkflowWarningIcon').toggle(true);
                }
                else {
                    $('#orderPolicyDelegatedButton').click(f_onOrderPolicyDelegated_Click)
                        .find('.WorkflowWarningIcon').toggle(false);
                }
            }

            $('#closeButton').click(function(){
                window.open('', '_self', '');
                onClosePopup();
            }).prop('disabled', false);
        });

        function f_onOrderPolicyDelegated_Click() {
            ApplyQuoteAndOrderPolicyClick(true);
        }

        function f_onOrderPolicyNonDelegated_Click() {
            ApplyQuoteAndOrderPolicyClick(false);
        }

        function f_onApplyQuoteButton_Click() {
            ApplyQuoteClick();
        }
        
        function CreateServiceArguments(isDelegated) {
            var args = {
                LoanID: ML.sLId,
                AppID: ML.aAppId,
                BranchID: ML.BranchId,
                TransactionID: <%= AspxTools.JsString(TransactionID) %>,
                OrderNumber: <%= AspxTools.JsString(OrderNumber) %>,
                IsDelegated: isDelegated
            };

            return args;
        }

        function ApplyQuoteClick() {
            DisableOrderButtonsForSubmit()

            var args = CreateServiceArguments();
            gService.OrderMIPolicy.callAsyncSimple("ApplyQuote", args, 
                function(result) {
                    HandleServiceResult(result);
                });
        }

        function DisableOrderButtonsForSubmit() {
            $('#applyQuoteButton, #orderPolicyDelegatedButton, #orderPolicyNonDelegatedButton')
                .prop('disabled', true);
        }

        function ReEnableOrderButtons() {
            $('#applyQuoteButton').prop('disabled', !ML.CanOrderQuote);
            $('#orderPolicyDelegatedButton').prop('disabled', !ML.CanOrderPolicyDelegated);
            $('#orderPolicyNonDelegatedButton').prop('disabled', !ML.CanOrderPolicyNonDelegated);
        }

        function ApplyQuoteAndOrderPolicyClick(isDelegated) {
            DisableOrderButtonsForSubmit();

            var args = CreateServiceArguments(isDelegated);

            var loadingPopup = SimplePopups.CreateLoadingPopup("Processing MI Order...", "Order Policy");
            LQBPopup.ShowElement($('<div>').append(loadingPopup), {
                width: 350,
                height: 200,
                elementClasses: 'FullWidthHeight',
                hideCloseButton: true
            });
            gService.OrderMIPolicy.callAsyncSimple("OrderPolicy", args,
                function (result) {
                    if (result.value.Status === 'Processing') {
                        var pollingIntervalInMilliseconds = parseInt(result.value.PollingIntervalInSeconds) * 1000;
                        var publicJobId = result.value.PublicJobId;
                        window.setTimeout(function () { PollForRequestResults(publicJobId, pollingIntervalInMilliseconds, 1); }, pollingIntervalInMilliseconds);
                    }
                    else {
                        LQBPopup.Return(null);
                        if(typeof window.opener.refreshOnOrder === "function") {
                            window.opener.refreshOnOrder();
                        }

                        HandleServiceResult(result);
                    }
                },
                function (result) {
                    LQBPopup.Return(null);
                    if(typeof window.opener.refreshOnOrder === "function") {
                        window.opener.refreshOnOrder();
                    }

                    HandleServiceResult(result);
                });
        }

        function HandleServiceResult(result) {
            if(result.error) {
                ReEnableOrderButtons();
                alert('Error: System error. Please retry in a few minutes and contact your LendingQB system administrator if the error recurs.');
            }
            else if(result.value.Status === "Error") {
                ReEnableOrderButtons();
                alert('Error: ' + result.value.Message);
            }
            else {
                alert(result.value.Message);
                location.href = location.href;
            }
        }

        function PollForRequestResults(publicJobId, pollingIntervalInMilliseconds, attemptCount) {
            var args = {
                PublicJobId: publicJobId,
                AttemptCount: attemptCount
            };

            gService.OrderMIPolicy.callAsyncBypassOverlay("PollForResults", args,
                function (result) {
                    if (result.value.Status === "Processing") {
                        window.setTimeout(function () { PollForRequestResults(publicJobId, pollingIntervalInMilliseconds, ++attemptCount) }, pollingIntervalInMilliseconds);
                    }
                    else {
                        LQBPopup.Return(null);
                        if(typeof window.opener.refreshOnOrder === "function") {
                            window.opener.refreshOnOrder();
                        }

                        HandleServiceResult(result);
                    }
                },
                function (result) {
                    LQBPopup.Return(null);
                    if(typeof window.opener.refreshOnOrder === "function") {
                        window.opener.refreshOnOrder();
                    }

                    HandleServiceResult(result);
                });
        }
    </script>
    <form id="form1" runat="server">
    <div>
    <div class="MainRightHeader">
        MI Quote
    </div>
    <table id="QuoteStatusTable" cellSpacing="2" cellPadding="0" width="100%" border="0">
        <tr>
            <td style="width: 400px">
            <table>
                <tr>
                    <td class="FieldLabel" style="width: 100px">
                        Quote Date:
                    </td>
                    <td><ml:EncodedLiteral ID="quoteDate" runat="server" ></ml:EncodedLiteral></td>
                </tr>
                <tr>
                    <td class="FieldLabel" style="width: 100px">
                        Quote ID:
                    </td>
                    <td><ml:EncodedLiteral ID="QuoteId" runat="server" ></ml:EncodedLiteral></td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Quote Expiration:
                    </td>
                    <td><ml:EncodedLiteral ID="quoteExpiration" runat="server" ></ml:EncodedLiteral></td>
                </tr>
            </table>
            </td>
            <td align="right" class="width-200">
                <button type="button" id="applyQuoteButton"><div>Apply Quote to Loan File</div></button>
            </td>
            <td align="right" class="width-200">
                <button type="button" id="orderPolicyDelegatedButton"><div class="Hidden WorkflowWarningIcon"><img src="../../images/warning25x25.png"/> </div><div>Apply Quote and Order Policy - <span class="bold">Delegated</span></div></button>
            </td>
            <td align="right" class="width-200">
                <button type="button" id="orderPolicyNonDelegatedButton"><div class="Hidden WorkflowWarningIcon"><img src="../../images/warning25x25.png"/> </div><div>Apply Quote and Order Policy - <span class="bold">Non&#8209;Delegated</span></div></button>
            </td>
        </tr>
        <tr>
            <td colspan="4">
            <table>
                <tr>
                    <td class="FieldLabel" style="width: 100px">
                        Quote Status:
                    </td>
                    <td><ml:EncodedLiteral ID="quoteStatus" runat="server" ></ml:EncodedLiteral>&nbsp; - &nbsp;<ml:EncodedLiteral ID="vendorMessage" runat="server" ></ml:EncodedLiteral></td>
                </tr>
            </table>
            </td>
        </tr>
   </table>
   <div>
        <hr />
   </div>
   <table id="PolicyInfoTable" cellSpacing="4" cellPadding="0" width="100%" border="0">
        <tr>
            <td class="FieldLabel" style="width: 140px">
                Policy Information:
            </td>
            <td><ml:EncodedLiteral ID="policyInformation" runat="server" ></ml:EncodedLiteral></td>
        </tr>
        <tr>
            <td colspan="2">
                <ml:CommonDataGrid ID="Premiums" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="DataGrid">
                    <alternatingitemstyle cssclass="GridAlternatingItem" />
                    <itemstyle cssclass="GridItem" />
                    <headerstyle cssclass="GridHeader" />
                    <Columns>
                        <asp:BoundColumn DataField="Description" HeaderText="Premium/Tax" ItemStyle-Width="200px" />
                        <asp:BoundColumn DataField="Factor" HeaderText="Factor" DataFormatString="{0:F4}%" ItemStyle-Width="80px" />
                        <asp:BoundColumn DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                    </Columns>
                </ml:CommonDataGrid>
            </td>
        </tr>
        <tr id="intialPremiumRow" runat="server">
            <td class="FieldLabel">
                Initial Premium Term
            </td>
            <td>
                <ml:EncodedLiteral ID="initialPremiumTerm" runat="server"></ml:EncodedLiteral>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Monthly Premium Term:
            </td>
            <td><ml:EncodedLiteral ID="premiumTerm" runat="server" ></ml:EncodedLiteral></td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Renewal Premium Term:
            </td>
            <td><ml:EncodedLiteral ID="renewalPremiumTerm" runat="server" ></ml:EncodedLiteral></td>
        </tr>
   </table>
   <div>
        <hr />
   </div>
   <table id="DisclaimerTable" cellSpacing="4" cellPadding="2" width="100%" border="0">
        <tr>
            <td class="FieldLabel">
                Rate Quote Disclaimer
            </td>
        </tr>
        <tr>
            <td>
                <div class="InsetBorder"><ml:EncodedLiteral ID="Disclaimer" runat="server" ></ml:EncodedLiteral></div>
            </td>
        </tr>
        <tr>
            <td align="right">
                <input type="button" id="closeButton" value="Close" />
            </td>
        </tr>
   </table>
    </div>
    </form>
</body>
</html>
