using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using LendersOfficeApp.los.credit;
using DataAccess;

using LendersOffice.CreditReport;


namespace LendersOfficeApp.newlos.Services
{

    public partial class ViewCredit : LendersOffice.Common.BasePage
    {
        protected System.Web.UI.WebControls.Literal HtmlReport;

        protected void PageLoad(object sender, System.EventArgs e)
        {
            ICreditReportView creditReportView = null;

            CreditReportTypes type = CreditReportTypes.Primary;
            Enum.TryParse(RequestHelper.GetSafeQueryString("type"), true, out type);

            Guid dbFileKey = RequestHelper.GetGuid("dbfilekey", Guid.Empty);
            Guid applicationID = RequestHelper.GetGuid("applicationid", Guid.Empty);
            Guid loanID = RequestHelper.GetGuid("loanid", Guid.Empty);

            if (Guid.Empty != loanID) 
            {
                CPageData dataLoan = new CCreditReportViewData(loanID);
                dataLoan.InitLoad();

                CAppData dataApp = dataLoan.GetAppData(applicationID);

                switch (type)
                {
                    case CreditReportTypes.Primary:
                        creditReportView = dataApp.CreditReportView.Value;
                        break;
                    case CreditReportTypes.Lqi:
                        creditReportView = dataApp.LqiCreditReportView.Value;
                        break;
                    default:
                        throw new UnhandledEnumException(type);
                }
            } else if (dbFileKey != Guid.Empty) 
            {
                creditReportView = CAppBase.GetCreditReportViewByFileDbKey(dbFileKey).Value;
            }
            RenderCreditReport(creditReportView, type);
        }

        private void RenderCreditReport(ICreditReportView creditReportView, CreditReportTypes type) 
        {
            if (null != creditReportView )
            {             

                if (creditReportView.HasHtml)
                {
                    Response.Clear();

                    byte[] buffer;
                    // Enable the "View PDF" button.
      
                    buffer = System.Text.UTF8Encoding.UTF8.GetBytes(creditReportView.HtmlContent);
                    Response.OutputStream.Write(buffer, 0, buffer.Length);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    PdfScript.Visible = creditReportView.HasPdf;
                    if (creditReportView.HasPdf == false)
                    {
                        if (creditReportView.RawXml != null && creditReportView.RawXml.Length > 0 && RequestHelper.GetGuid("loanid", Guid.Empty) != Guid.Empty)
                        {
                            Response.Redirect(string.Format("ViewCreditReportData.aspx?loanid={0}&appid={1}", RequestHelper.GetGuid("loanid", Guid.Empty), RequestHelper.GetGuid("applicationid", Guid.Empty)));
                        }
                        else
                        {
                            NoReportLabel.Text = ErrorMessages.CreditReportMissingXml;
                        }
                    }
                }
            }
            else
            {
                switch (type)
                {
                    case CreditReportTypes.Primary:
                        NoReportLabel.Text = ErrorMessages.CreditReportNotOrderedMessage;
                        break;
                    case CreditReportTypes.Lqi:
                        NoReportLabel.Text = ErrorMessages.LqiCreditReportNotOrderedMessage;
                        break;
                    default:
                        throw new UnhandledEnumException(type);
                }
            }
        }
        /*
        private void LoadCreditReport(string dbFileKey) 
        {

            Guid sLId = RequestHelper.GetGuid("loanid", Guid.Empty);
            string loginNm = ((LendersOffice.Security.BrokerUserPrincipal) User).LoginNm;

            CreditReportDebugInfo debugInfo = new CreditReportDebugInfo(sLId, loginNm, dbFileKey);
            ICreditReport creditReport = CreditReportServer.LoadFromFileDB(dbFileKey, debugInfo);

            if (null != creditReport) 
            {
                Response.Clear();


                string htmlContent = creditReport.RetrieveRawFormat("HTML");
                string pdfContent = creditReport.RetrieveRawFormat("PDF-BASE64");

                if ("" != pdfContent) 
                {
                    string script = @"<script>
                                parent.menu.f_showPdfButton();
                            </script>";
                    byte[] buffer = System.Text.UTF8Encoding.UTF8.GetBytes(script);

                    Response.OutputStream.Write(buffer, 0, buffer.Length);

                }

                if (htmlContent != "") 
                {

                    byte[] buffer = System.Text.UTF8Encoding.UTF8.GetBytes(htmlContent);
                    Response.OutputStream.Write(buffer, 0, buffer.Length);
                } 
                else if (pdfContent != "")
                {
                    // 8/10/2004 dd - If credit report doesn't have HTML/text version and there is PDF version
                    // then open PDF version.

                    string script = @"<script>if (typeof(parent.menu.viewPDF) == 'function') parent.menu.viewPDF();</script>";
                    byte[] buffer = System.Text.UTF8Encoding.UTF8.GetBytes(script);
                    Response.OutputStream.Write(buffer, 0, buffer.Length);

                }
                Response.End();

            }

        }
*/
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
    }
}
