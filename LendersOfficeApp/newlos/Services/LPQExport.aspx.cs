﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.ObjLib.Conversions.LoansPQ.Exporter;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Services
{
    public partial class LPQExport : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData lpqData = CPageData.CreateUsingSmartDependency(LoanID, typeof(LPQExport));
            lpqData.InitLoad();

            List<Tuple<Guid, string>> apps = new List<Tuple<Guid, string>>();
            for (int i = 0; i < lpqData.nApps; i++)
            {
                CAppData app = lpqData.GetAppData(i);
                apps.Add(Tuple.Create(app.aAppId, app.aAppNm));
            }

            MemberNumbers.DataSource = apps;
            MemberNumbers.DataBind();


            bool hasLpqNumber = lpqData.sLpqLoanNumber > 0;
            lpqnum.Text = lpqData.sLpqLoanNumber.ToString();
            CannotExportPanel.Visible = hasLpqNumber;
            FormLogin.Visible = !hasLpqNumber;
            EnableJqueryMigrate = false;

            SetRequiredValidatorMessage(UsernameIsRequired);
            SetRequiredValidatorMessage(PasswordIsRequired);

            LoadData();


        }


        protected void MemberNumbers_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            Tuple<Guid, string> entry = (Tuple<Guid, string>)args.Item.DataItem;

            MeridianLink.CommonControls.EncodedLiteral borrowerNm = (MeridianLink.CommonControls.EncodedLiteral)args.Item.FindControl("BorrowerNm");
            HtmlInputHidden aAppId = (HtmlInputHidden)args.Item.FindControl("AppId");

            borrowerNm.Text = entry.Item2;
            aAppId.Value = entry.Item1.ToString(); 
        }

        protected override void LoadData()
        {
            // get the user info.
            var parameters = new SqlParameter[]{ new SqlParameter("@UserId", BrokerUser.UserId) };
            string spName = "LPQ_RetrieveSavedAuthenticationByUserId";
            using(var reader = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, spName, parameters))
            {
                if(false == reader.Read())
                {
                    throw CBaseException.GenericException(
                        string.Format("User {0} was not found in the Broker_User table when executing {1}",
                        BrokerUser.UserId, spName));
                }

                Username.Text = (string)reader["LPQUsername"];
                string encryptedPassword = (string)reader["LPQPassword"];
                byte[] encryptedPasswordBytes = (byte[])reader["EncryptedLPQPassword"];
                if (encryptedPasswordBytes.Length != 0 || encryptedPassword != string.Empty)
                {
                    Password.Attributes.Add("value", ConstAppDavid.FakePasswordDisplay);
                }
            }
        }

        protected void SetRequiredValidatorMessage(System.Web.UI.WebControls.BaseValidator v)
        {
            HtmlImage img = new HtmlImage();
            img.Src = ResolveUrl("~/images/require_icon_red.gif");
            v.Controls.Add(img);
        }

        protected void ClearLPQId_OnClick(object sender, EventArgs args)
        {
            CPageData lpqData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(LoanID, typeof(LPQExport));
            lpqData.InitSave(ConstAppDavid.SkipVersionCheck);
            lpqData.sLpqLoanNumber = -100;
            lpqData.Save();
            string url = HttpContext.Current.Request.RawUrl + HttpContext.Current.Request.QueryString.ToString();
            Response.Redirect(url);
        }

        [WebMethod]
        public static object Export(string username, string password, bool updatePassword, Guid loanid, List<AppMemberMap> memberAppInfo)
        {
            try
            {
                return ExportImpl(username, password, updatePassword, loanid, memberAppInfo);
            }
            catch (ArgumentException e) // There was an error in input. One of the enums must be unsupported.
            {
                Tools.LogErrorWithCriticalTracking(e);
                return new { Error = e.Message };
            }
            catch (Exception e)
            {
                Tools.LogErrorWithCriticalTracking(e);
                throw;
            }
        }
        public static object ExportImpl(string username, string password, bool updatePassword, Guid loanid, List<AppMemberMap> memberAppInfo)
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            Guid brokerId = principal.BrokerId;

            if (password == ConstAppDavid.FakePasswordDisplay)
            {
                string encryptedPassword = null;
                byte[] encryptedPasswordBytes = null;
                LqbGrammar.DataTypes.EncryptionKeyIdentifier? maybeEncryptionKeyId = null;
                SqlParameter[] parameters = {
                                                new SqlParameter("@UserId", PrincipalFactory.CurrentPrincipal.UserId)
                                            };
                using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "LPQ_RetrieveSavedAuthenticationByUserId", parameters))
                {
                    if (reader.Read())
                    {
                        maybeEncryptionKeyId = LqbGrammar.DataTypes.EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]);
                        if (maybeEncryptionKeyId.HasValue)
                        {
                            encryptedPasswordBytes = (byte[])reader["EncryptedLPQPassword"];
                        }
                        else
                        {
                            encryptedPassword = (string)reader["LPQPassword"];
                        }
                    }
                }

                if (maybeEncryptionKeyId.HasValue && encryptedPasswordBytes.Length != 0)
                {
                    password = EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, encryptedPasswordBytes);
                }
                else if (!string.IsNullOrEmpty(encryptedPassword))
                {
                    password = EncryptionHelper.Decrypt(encryptedPassword);
                }
            }
            else if (updatePassword)
            {
                var maybeEncryptionKeyId = BrokerUserDB.RetrieveEncryptionKey(brokerId, principal.UserId);
                byte[] encryptedPasswordBytes = null;
                if (maybeEncryptionKeyId.HasValue)
                {
                    encryptedPasswordBytes = EncryptionHelper.EncryptString(maybeEncryptionKeyId.Value, password);
                }

                var parameters = new SqlParameter[] { 
                    new SqlParameter("@UserId", PrincipalFactory.CurrentPrincipal.UserId),
                    new SqlParameter("@LPQUsername", username),
                    new SqlParameter("@LPQPassword", EncryptionHelper.Encrypt(password)),
                    new SqlParameter("@EncryptedLPQPassword", encryptedPasswordBytes ?? new byte[0]),
                };

                int result = StoredProcedureHelper.ExecuteNonQuery(brokerId, "LPQ_UpdateSavedAuthentication", 0, parameters);

                if (result != -1)
                {
                    Tools.LogErrorWithCriticalTracking(
                        string.Format("Result was not -1 when executing {1} for user LPQ_UpdateSavedAuthentication",
                            PrincipalFactory.CurrentPrincipal.UserId));
                }
            }

            Dictionary<Guid, string> memberInfo = new Dictionary<Guid, string>();
            foreach (AppMemberMap application in memberAppInfo)
            {
                memberInfo.Add(new Guid(application.aAppId), application.appMemberNumber);
            }
            
            CPageData loandata = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanid, typeof(LPQExport));
            loandata.InitSave(ConstAppDavid.SkipVersionCheck);
            // 6/2/2014 gf - opm 181635, also create a HELOC LPQ loan when the loan is marked as a line of credit.
            bool isHomeEquity = loandata.sLpTemplateNm.Equals("HELOC", StringComparison.OrdinalIgnoreCase) || loandata.sIsLineOfCredit || loandata.sLPurposeT == E_sLPurposeT.HomeEquity;

            string relativeUrl = "/services/submitloan/SubmitLoan.aspx";
            CLFExporter clfExporter = new CLFExporter();
            string data = clfExporter.Export(loanid, memberInfo); 
            BrokerDB brokerDb = principal.BrokerDB;
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(brokerDb.LPQBaseUrl + relativeUrl);
           // webRequest.Timeout = TimeSpan.FromMinutes(3).Milliseconds;

            XDocument doc = new XDocument(
                new XDeclaration("1.0", "utf-8", "no"),
                new XElement("INPUT",
                    new XElement("LOGIN", new XAttribute("login", username), new XAttribute("password", "******")),
                    new XElement("REQUEST", new XAttribute("action_type", "NEW"),
                        new XElement("LOAN_DATA", new XAttribute("loan_type", isHomeEquity ? "HE" : "ML" ), new XCData(data)))));


            Tools.LogInfo("LPQExport " + doc.ToString(SaveOptions.None));

            //update pw
            doc.Element("INPUT").Element("LOGIN").Attribute("password").Value = password;

            webRequest.Method = "POST";
            webRequest.ContentType = "text/xml";


            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = false;
            settings.Indent = false;

            Encoding ut = new UTF8Encoding(false);
            settings.Encoding = ut;

            using( Stream requestStream = webRequest.GetRequestStream())
            using (XmlWriter writer = XmlWriter.Create(requestStream, settings))
            {
                doc.WriteTo(writer);
            }

            string responseOutput;
            using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
            using( Stream responseStream = webResponse.GetResponseStream())
            using (StreamReader sw = new StreamReader(responseStream))
            {
                responseOutput = sw.ReadToEnd();
                Tools.LogInfo("LPQ REsponse : " + responseOutput);
            }

            XDocument responseDoc = XDocument.Parse(responseOutput);
            XElement output = responseDoc.Document.Element("OUTPUT");
            if (output != null)
            {
                if (output.Element("ERROR") != null)
                {
                    return new { Error = output.Element("ERROR").Value };
                }
                var response = output.Element("RESPONSE");
                string loanNumber = response.Attribute("loan_number").Value;

                loandata.sLpqLoanNumber = long.Parse(loanNumber);
                loandata.Save();
            }

            return new { Success = "True" };
        }

        public class AppMemberMap
        {
            public string aAppId { get; set; }
            public string appMemberNumber { get; set; }
        }
    }


}
