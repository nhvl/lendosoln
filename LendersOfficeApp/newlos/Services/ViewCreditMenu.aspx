<%@ Page language="c#" Codebehind="ViewCreditMenu.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Services.ViewCreditMenu" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
  <head runat="server">
    <title>ViewCreditMenu</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="FlowLayout"  bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
    <form id="ViewCreditMenu" method="post" runat="server">
<table border="0" cellspacing="0" cellpadding="5" width="100%">
<tr>
    <td id="tdReportTypes" runat="server" align="left" valign="top">
        Report Type:
        <label>
            <input type="radio" class="reportType" id="primaryCreditReport" name="reportType" value="0" runat="server" /> 
            Primary credit report
        </label>
        <label>
            <input type="radio" class="reportType" id="lqiCreditReport" name="reportType" value="1" runat="server" />
            LQI credit report
        </label>
    </td>
	<td align="right" valign="top">
  	<input type=button id="btnViewPdf" class="viewPdf" onclick="viewPDF();" value="View PDF" runat="server" >
		<input type="button" onclick="f_print();" value="Print">
                    <input type="button" onclick="parent.onClosePopup();" value="Close">
	</td>
</tr>
</table>
     </form>
	<script language=javascript>
	    function viewPDF() {
	        var url;
	        if ($('.reportType:checked').val() == '1'){
                url = <%= AspxTools.JsString(VirtualRoot) %> + '/pdf/LqiCreditReport.aspx'
	        } else {
                url = <%= AspxTools.JsString(VirtualRoot) %> + '/pdf/CreditReport.aspx'
                
	        }

            url += '?loanid=' + <%= AspxTools.JsString(RequestHelper.GetGuid("loanid").ToString()) %> + '&applicationid=' + <%= AspxTools.JsString(RequestHelper.GetGuid("applicationid").ToString()) %> + '&crack=' + new Date();
            LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url, parent.body);
        }
        function f_print() {
            //iOPM: 456090. Fix cut off text when printing. 
            var style = $("<style id='printStyle'>body{size: 8.5in 11in;margin-top:0.5in;margin-left:0.45in;margin-right:0.45in;margin-bottom:0.5in;}</style>");
            var container = parent.document.head || parent.document.getElementsByTagName("head")[0];
            container.appendChild(style[0]);
         
            parent.document.body.rows = "0,*";
            window.onafterprint = function(){
                parent.document.body.rows = "50,*";
                container.removeChild(style[0]);
            };

            parent.body.focus();
            parent.body.print();
        }

        $('.reportType').click(function () {
            toggleViewPdf();
            parent.setBody(this.value);
        });

        function toggleViewPdf()
        {
            var type = $('.reportType:checked').val();
            $(".viewPdf").toggle(ML.HasDbReportPdf || (type == "0" && ML.HasPrimaryReportPdf) || (type == "1" && ML.HasLqiReportPdf));
        }

        toggleViewPdf();
    </script>
  </body>
</html>
