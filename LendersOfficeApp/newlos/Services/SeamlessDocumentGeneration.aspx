﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SeamlessDocumentGeneration.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.SeamlessDocumentGeneration" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Document Generation</title>
    <BASE target="_self">
</head>
<body bgcolor="gainsboro" style="padding-left:3px">
    <form id="form1" runat="server">
    <asp:HiddenField ID="m_AlertMessage" runat="server" />
    <asp:HiddenField ID="m_PageUrl" runat="server" />
    <script language="javascript" type="text/javascript">
        <!--
        function _init() {
            if (!$('#Disable').val()) {
                updateUIForNewPackageSelection();
            }
        }

        var promptUserForTemporaryArchiveSource = false;
        var temporaryArchiveSourcePrompt = '';
        var packageHasClosingDisclosure = false;
        var forbidGeneratingPackage = false;
        var packageGenerationForbiddenMessage = '';

        function updateUIForNewPackageSelection() {
            var selectedPackage = $('#m_ddlPackageType').val();
            var packageParts = selectedPackage.split(':');
            var tempArchiveSource = $('#TemporaryArchiveSourceForAudit').val();
            var args = {
                sLId: ML.sLId,
                VendorId: <%= AspxTools.JsString(VendorId.ToString()) %>,
                PackageType: packageParts[0]
            };

            var result = gService.utils.call('GetDocumentPackageInfo', args);

            setPackageSpecificWarning(result.value.Warning);

            promptUserForTemporaryArchiveSource = result.value.PromptForTemporaryArchiveSource === 'True';
            temporaryArchiveSourcePrompt = result.value.TemporaryArchiveSourcePrompt;
            packageHasClosingDisclosure = result.value.PackageHasClosingDisclosure === 'True';
            forbidGeneratingPackage = result.value.ForbidGeneratingPackage === 'True';
            packageGenerationForbiddenMessage = result.value.PackageGenerationForbiddenMessage;
        }

        function setPackageSpecificWarning(message) {
            $('#PackageSpecificWarning').text(message);
            $('#PackageSpecificWarning').show();

            var messageSpan = document.getElementById('TestEnvironmentDisclaimerSpan');
            if (message != '' && messageSpan && messageSpan.style.display != "none")
            {
                $('#TestEnvironmentDisclaimerDivider').show();
            }
        }

        function setSummary(message) {
            $('#Summary').text(message);
            $('#Summary').show();

            var messageSpan = document.getElementById('TestEnvironmentDisclaimerSpan');
            if (message != '' && messageSpan && messageSpan.style.display != "none")
            {
                $('#TestEnvironmentDisclaimerDivider').show();
            }
        }
            
        function disableControls(disabled) {
            document.getElementById('m_PerformAudit').disabled = disabled;
            document.getElementById('m_btnNext').disabled = disabled;
        }

        function AuditPostSave(idx) {
            jQuery('#AuditResults ul li').eq(idx).css({ 'text-decoration': 'line-through' });
        }

        function displayBusyUI() {
            jQuery('#AuditResults').empty().append('<div style="text-align:center"><br /><br />Running Data Entry Audit, Please Wait...</div>');
            disableControls(true);
            setSummary('');
        }
        
        function alertOnError() {
            var message = document.getElementById('m_AlertMessage').value;
            if (!message) return;
            LQBPopup.ShowString(message, {
                width: 420,
                height: 180,
                modifyOverflow: false,
                popupClasses: "LQBPopupAlert",
                onClose: function () {
                    setSummary(message);
                }
            });
        }

        function nextPage() {
            var selectedPackage = jQuery('#m_ddlPackageType option:selected').text();
            var selectedProgram = jQuery('#m_ddlPlanCode option:selected').text();
            if(selectedPackage == 'Form List')
            {
                var thePage ="/newlos/Services/SeamlessDocumentFormsList.aspx";
                var url = thePage+ '?loanid='+<%= AspxTools.JsString(LoanID) %> + 
                    "&PackageID=" + encodeURIComponent(selectedPackage) + 
                    "&ProgramID=" + encodeURIComponent(selectedProgram) + 
                    "&vendorId=" + <%= AspxTools.JsString(VendorId.ToString()) %>;
                showModal(url,  null, 'dialogHeight:500px;dialogWidth:500px;' , null, function(result){ 
                    if (result && result.selectedForms) {
                        jQuery('#m_PageUrl').val(jQuery('#m_PageUrl').val()+"&selectedForms="+encodeURIComponent(result.selectedForms));
                    }
                    else {
                        return;
                    }
                },{hideCloseButton:true});
            }

            //This roundabout method is necessary to change the window location when you're in a modal dialog.
            window.name = "SeamlessDocumentGeneration"
            window.open(document.getElementById('m_PageUrl').value, "SeamlessDocumentGeneration");
        }
        
        var childArgs = null;
        function getChildArgs(){
            return childArgs;
        }
        
        jQuery(function($) {
            if (ML && ML.sIsInitialLoanEstimateDateAssociatedWithInvalidArchive) {
                alert(<%= AspxTools.JsString(ErrorMessages.ArchiveError.InitialLoanEstimateAssociatedWithInvalidArchiveDocs) %>);
                onClosePopup();
            } else if (ML && ML.sHasArchiveInUnknownStatus) {
                var confirmInvalidationDialog =  ML.sHasLoanEstimateArchiveInUnknownStatus ? 'InvalidateUnknownArchive' : null;
                var confirmInvalidationCheckboxId = ML.sHasLoanEstimateArchiveInUnknownStatus ? 'userAgreesToInvalidate' : null;                
                var mode;

                if (ML.HasBothCDAndLEArchiveInUnknownStatus) {
                    mode = ArchiveStatusPopupMode.HasBothCDAndLEArchiveInUnknownStatus;
                }
                else if (ML.sHasLoanEstimateArchiveInUnknownStatus && ML.UnknownLoanEstimateWasSourcedFromCoC) {
                    mode = ArchiveStatusPopupMode.UnknownFromCoC;
                }
                else {
                    mode = ArchiveStatusPopupMode.Unknown;
                }
                    
                ArchiveStatusPopup.create('ArchiveStatusUnknown', ML.sLId, false, mode, ArchiveStatusPopupSource.SeamlessDocumentGeneration, null, true, confirmInvalidationDialog, confirmInvalidationCheckboxId);
                ArchiveStatusPopup.displayPopup();
            }

            var planCodeLabel = $('#PlanCodeLabel').val();
            if(planCodeLabel){
                $('#PlanCode span.Label').text(planCodeLabel);
            }
        
            if($('#Disable').val() == 'True'){
                disableControls(true);  
            }
            alertOnError();
            
            var defaultOptions = {
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            };

            $('#AuditResults').on("click", '.EditLink', function() {
                var url = $(this).next().val();
                var myIndex = $(this).parent().index();
                var currentQuery;
                var dlgArg = getModalArgs();
                if(dlgArg) currentQuery = new RegExp('&AuditPostSaveId=([^&#]*)').exec(dlgArg.body.location.search)
                if(!currentQuery) currentQuery = [0, 0];
                var current = currentQuery[1];
                if(window.dialogArguments && dialogArguments.body.f_smartConfirm(null, null, null, function(){AuditPostSave(current)}))
                {
                    dialogArguments.body.location = url + "&AuditPostSaveId=" + myIndex;
                }
            }).on("click", '.detail-showhide', function(){
                var $this = $(this);
                var $message = $this.siblings('.details-text');
                $message.toggleClass('Hidden');
                
                if($message.is(".Hidden")){
                    $this.text('show details');
                }
                else{
                    $this.text('hide details');
                }
            });

            $('#m_PerformAudit').click(function() {
                if($(this).is(':disabled')) return;
                if(!preAuditCheck()) return;
                
                displayBusyUI();
                //Need to put this gap in here so that the busy UI will actually show up.
                window.setTimeout(performAuditContinuation, 1);
            });
            
            $('#m_ddlPackageType').change(function() {
                document.getElementById('m_btnNext').disabled = true;
                updateUIForNewPackageSelection();

                if ($(this).val() !== "")
                {
                    // Once the user selects a package type, the blank option
                    // and required indicator should be removed.
                    validatePlanCodeAndPackageType();

                    $('#PackageTypeRequiredIndicator').hide();

                    if (this.options.length > 0 && this.options[0].value === "")
                    {
                        this.remove(0);
                    }
                }
            });
            
            $('#m_ddlPlanCode').change(function(event){
                document.getElementById('m_btnNext').disabled = true;
                var val = $(this).val();
                var current =  $(':selected', this);
                while(!isValidPlanCode(val))
                {
                    current = current.next();
                    val = current.val();
                }
                $(this).val(val);
                updateTransferTo(val, true);
                event.preventDefault();
                validatePlanCodeAndPackageType();
            });

            validatePlanCodeAndPackageType();
            
            $('#m_dialogLink').click(function() {
                var queryString = "?code=" + $('#m_ddlPlanCode').val() + '&vendorId=' + <%= AspxTools.JsString(VendorId.ToString()) %>;
                showModal('/newlos/Services/TransferToPicker.aspx' + queryString, null, null, null, function(result){
                    if (result && result.description && result.code) {
                        $('#m_transferToDesc').val(result.description);
                        $('#m_transferToCode').val(result.code);
                        $('#m_transferToDescLabel').text(result.description);
                    }
                },{hideCloseButton:true});
            });

            function validatePlanCodeAndPackageType()
            {
                var auditButton = $('#m_PerformAudit');
                var planCode = $('#m_ddlPlanCode');
                var packageType = $('#m_ddlPackageType');

                var badPlanCode = 'invalid-plan-code';
                var badPackage = 'invalid-package';

                if (!isValidPlanCode(planCode.val()))
                {
                    auditButton.addClass(badPlanCode).prop('disabled', true);
                }
                else
                {
                    auditButton.removeClass(badPlanCode);
                }

                if (packageType.val() === "")
                {
                    auditButton.addClass(badPackage).prop('disabled', true);
                }
                else
                {
                    auditButton.removeClass(badPackage);
                }

                auditButton.prop("disabled", !((!auditButton.hasClass(badPlanCode) && !auditButton.hasClass(badPackage))));
            }
            
            function isValidPlanCode(name)
            {
                return $.trim(name) != '';
            }
            
            function performAuditContinuation() {
                if (needsPlanCodePicked())
                {
                    forcePlanCodeSelection(function(args) {
                        if (args.Action != 'Failure') {
                            runAudit();
                        }
                        else{
                            jQuery('#AuditResults').empty().append('<div style="text-align:center"><br /><br />Please correct loan errors before auditing</div>');
                        }
                    });
                    
                    return;
                }
                
                runAudit();
            }

            function pickPlanCode() {
                var $planCodeDDL = $('#m_ddlPlanCode');
                var $validPlanCodes = $planCodeDDL.find('option[value!=""]');
                if ($validPlanCodes.length === 1) {
                    $planCodeDDL.change(); // Select the only valid entry.
                    $planCodeDDL.prop('disabled', false).css('background-color', '');
                }
                else if (needsPlanCodePicked()) {
                    forcePlanCodeSelection(function(args) {
                        if (args.Action == 'Failure') {
                            jQuery('#AuditResults').empty().append('<div style="text-align:center"><br /><br />Please correct loan errors before auditing</div>');
                        }
                        else{
                            jQuery('#AuditResults').empty().append('<div style="text-align:center"><br /><br />Please wait...</div>');                            
                            window.name = "SeamlessDocumentGeneration"
                            window.open(window.location, "SeamlessDocumentGeneration");
                        }
                    });
                }

                return true;
            }

            function needsPlanCodePicked() {
                var ret = false;
                var DTO = { 
                    sLId: ML.sLId,
                    VendorId: <%= AspxTools.JsString(VendorId.ToString()) %>
                };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: 'SeamlessDocumentGeneration.aspx/NeedsPlanCode',
                    data: JSON.stringify(DTO),
                    dataType: 'json',
                    async: false
                }).then(
                    function(msg) {
                        ret = msg.d;
                    },
                    function(XMLHttpRequest, textStatus, errorThrown) { 
                        var message = textStatus + ": " + errorThrown;
                        alert(message); 
                        setSummary(message);
                    }
                );

                return ret;
            }

            function forcePlanCodeSelection(forceCallback) {
                forceCallback = forceCallback || function() { };
                var args = {};
                args.callback = function(args) { forceCallback(args); };
                var url = '/newlos/SelectDocMagicPlan.aspx?loanid=' + document.getElementById('loanid').value
                try
                {
                    var _childWindow = showModeless(url, null, null, args, true);
                }
                catch(e)
                {
                    var args =  getModalArgs();
                    if (args){
                        var _childWindow = args.open('/newlos/SelectDocMagicPlan.aspx?loanid=' + document.getElementById('loanid').value, "PlanCodeSelection", "close=yes,titlebar=yes");
                    }
                }
            }

            function runAudit() {
                var selectedPlanCode = $('#m_ddlPlanCode').val();
                
                var selectedPackage = $('#m_ddlPackageType').val();
                var packageParts = selectedPackage.split(':');

                var DTO = {
                    sLId: ML.sLId,
                    AppId: ML.aAppId,
                    PackageType: packageParts[0],
                    AllowEPortal: packageParts[1],
                    DisableESign: packageParts[2],
                    AllowEClose: packageParts[3],
                    AllowManualFulfillment: packageParts[4],
                    ReqPlanCode: selectedPlanCode,
                    TransferToDescription: $('#m_transferToDesc').val(),
                    TransferToCode: $('#m_transferToCode').val(),
                    VendorId: <%= AspxTools.JsString(VendorId.ToString()) %>,
                    TemporaryArchiveSource: $('#TemporaryArchiveSourceForAudit').val()
                };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: 'SeamlessDocumentGeneration.aspx/PerformDocMagicAudit',
                    data: JSON.stringify(DTO),
                    dataType: 'json',
                    async: false
                }).then(
                    function(msg) {
                        setSummary(msg.d.SummaryText);
                        var url = msg.d.NextPageUrl.replace("=Y", "=1");
                        
                        $('#m_PageUrl').val(MakeNextPageUrl(url, DTO));
                        
                        $('#AuditResults').empty().append(msg.d.AuditHtml);

                        $('#m_PerformAudit').css({ 'background-color': '' })[0].disabled = false;

                        if (!msg.d.Errors) $('#m_btnNext')[0].disabled = false;
                        if(msg.d.IntegrationError)
                        {
                            alert(msg.d.IntegrationErrorMessage);
                        }
                    },
                    function(XMLHttpRequest, textStatus, errorThrown) { 
                        alert(textStatus + errorThrown);
                    }
                );
            }
            
            function MakeNextPageUrl(baseUrl, DTO)
            {
                return [baseUrl,
                    "&allowEPortalService=", YNToBool(DTO.AllowEPortal),
                    "&disableESign=", YNToBool(DTO.DisableESign),
                    "&allowEClose=", YNToBool(DTO.AllowEClose),
                    "&allowManualFulfillment=", YNToBool(DTO.AllowManualFulfillment),
                    "&vendorId=", <%= AspxTools.JsString(VendorId.ToString()) %>].join('');
            }
            
            function YNToBool(yn){
                if(yn == 'Y') return 'T';
                return 'F';
            }
            
            function preAuditCheck()
            {
                if (getModalArgs())
                {
                    var data = getModalArgs();
                    if (data && data.body) {
                        if (data.body.isDirty()) {
                            var ret = ConfirmSaveWithMessage("Do you want to save the changes to the loan file before running the audit?");
                            if (ret == vbConfirmYes)
                                data.info.f_save();
                            else if (ret == vbConfirmCancel)
                                return;
                        }
                    }
                }
                var selectedPackage = $('#m_ddlPackageType').val();
                if(!selectedPackage || selectedPackage.indexOf(":") == -1)
                {
                    preAuditFailure("Please select a package before running the audit.");
                    return false;
                }
                
                var selectedPlanCode = $('#m_ddlPlanCode').val();
                if(!selectedPlanCode || $.trim(selectedPlanCode) == '')
                {
                    var name = $("#PlanCode span.Label").text().toLowerCase();
                    var message = "Please select a " + name + " before running the audit.";
                    preAuditFailure(message);
                    return false;
                }

                if (promptUserForTemporaryArchiveSource) {
                    $('#TemporaryArchiveSourcePrompt').text(temporaryArchiveSourcePrompt);
                    ArchiveStatusPopup.create(
                        'TemporaryArchiveSourcePrompt',
                        ML.sLId,
                        false,
                        packageHasClosingDisclosure ? ArchiveStatusPopupMode.PendingLoanEstimateForClosingDisclosure : ArchiveStatusPopupMode.PendingWithoutLastDisclosed,
                        ArchiveStatusPopupSource.SeamlessDocumentGeneration,
                        function(event, ui) {
                            var selectedValue = $('#TemporaryArchiveSourcePrompt').data('tempArchiveSource');
                            if (selectedValue && selectedValue !== 'pending' && selectedValue !== 'live') {
                                throw 'Unhandled value.';
                            } else if (selectedValue) {
                                $('#TemporaryArchiveSourceForAudit').val(selectedValue);
                                // So we don't prompt unless they've changed the package.
                                promptUserForTemporaryArchiveSource = false;
                                $('#m_PerformAudit').click();
                             }
                       });
                    ArchiveStatusPopup.displayPopup();
                    return false;
                }

                if (forbidGeneratingPackage) {
                    alert(packageGenerationForbiddenMessage);
                    return false;
                }
                
                return true;
            }
            
            function preAuditFailure(msg)
            {
                alert(msg);
                setSummary(msg);
                $('#AuditResults').empty();
                var $auditButton = $('#m_PerformAudit');
                if(!$auditButton.is('.invalid-plan-code'))
                {
                    $auditButton.css({ 'background-color': '' })[0].disabled = false;
                }
            }
            
            function updateTransferTo(code, clearOldValues) {
                if (!<%= AspxTools.JsBool(SupportsTransferToInvestors) %>) {
                    return;
                }
                
                if (clearOldValues) {
                    $('#m_transferToDesc').val('');
                    $('#m_transferToCode').val('');
                    $('#m_transferToDescLabel').html('Loading...');
                }
                
                setDisabledAttr($('#m_dialogLink'), true);
                
                var arg = { 
                    PlanCode: code,
                    VendorId: <%= AspxTools.JsString(VendorId.ToString()) %>
                };
                var dash = '&#8211;';

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: 'SeamlessDocumentGeneration.aspx/InvestorsExistForPlanCode',
                    data: JSON.stringify(arg),
                    dataType: 'json',
                    async: true
                }).then(
                    function(msg) {
                        if (clearOldValues) {
                            $('#m_transferToDescLabel').html(dash + ' None ' + dash);
                        }
                        if (msg.d.Error) {
                            alert('An error has occurred while downloading data from DocMagic. '
                                + 'Please try again or contact support if this issue continues '
                                + 'to occur');
                            return
                        }
                        else if (msg.d.InvestorsExist === true) {
                            setDisabledAttr($('#m_dialogLink'), false);
                        }
                    },
                    function(XMLHttpRequest, textStatus, errorThrown) { 
                        alert(textStatus + errorThrown);
                        $('#m_transferToDescLabel').html(dash + ' None ' + dash);
                    }
                );
            }

            pickPlanCode()
            updateTransferTo($('#m_ddlPlanCode').val(), false);
        });
        //-->
    </script>
    
    <style type="text/css">
    .EditLink
    {
        cursor: pointer;
    }
    #AuditList
    {
        list-style-type: none;
        position:relative;
        right: 30px;
        bottom: 10px;
    }

    span.AuditEntry.FATAL
    {
        color: Red;
    }
    
    span.AuditEntry.CRITICAL
    {
        color: Red;
    }
    
    span.AuditEntry.WARNING
    {
        color: Orange;
    }
    
    #m_RunningAuditMessage
    {
        display:none;
        text-align:center;
    }
    #AuditResults
    {
        width: 600px;
        height: 400px;
        overflow-y:scroll; 
        border:inset
    }
    
    .Relative
    {
        position: relative;
    }
    
    #PlanCode
    {
        float:right;
        display: inline-block;
    }

    .Highlighted
    {
        background-color: #FFFF00;
    }
    
    .Hidden
    {
        display: none;
    }
    
    .Action
    {
        cursor: pointer;
        color: Blue;
        text-decoration: underline;
    }
    
    .Action:hover
    {
        color: Orange;
    }
    
    .details-text
    {
        font-weight: normal;
    }

    .Divider
    {
        padding: 1px;
        margin: 1px;
    }

    #ArchiveStatusUnknown, #TemporaryArchiveSourcePrompt, #InvalidateUnknownArchive
    {
        display: none;
    }

    .no-close .ui-dialog-titlebar-close
    {
        display: none;
    }
    </style>
    <div id="ArchiveStatusUnknown">
        <ml:EncodedLiteral runat="server" ID="ArchiveStatusUnknownMessage"></ml:EncodedLiteral>
    </div>
    <div id="InvalidateUnknownArchive">
        <div>
            <ml:EncodedLiteral runat="server" ID="InvalidateUnknownArchiveMessage"></ml:EncodedLiteral>
        </div>
        <div>
            <input type="checkbox" id="userAgreesToInvalidate" /><label for="userAgreesToInvalidate">I have read the above.</label>
        </div>
    </div>
    <div id="TemporaryArchiveSourcePrompt">
    </div>
    <table id="MainTable" cellspacing="0" cellpadding="0" width="98%" class="FieldLabel">
        <tr>
            <td class="MainRightHeader">
                <ml:EncodedLiteral id="TitleLabel" runat="server">Generate Documents</ml:EncodedLiteral>
            </td>
        </tr>
        <tr>
            <td>
                <span class="Settings" id="PlanCode">
                    <span class="Label">Plan Code</span>
                    <asp:DropDownList ID="m_ddlPlanCode" runat="server" Width="240"></asp:DropDownList>
                </span>
                <span class="Settings" id="PackageType">
                    <span class="Label">Package Type</span>
                    <asp:DropDownList ID="m_ddlPackageType" runat="server" Width="150"></asp:DropDownList>
                    <img id="PackageTypeRequiredIndicator" alt="required" src="../../images/require_icon.gif" />
                </span>
            </td>
        </tr>
        <tr id="TransferToRow" runat="server">
            <td>
                <span class="Settings" id="TransferTo">
                    <span class="Label">Transfer To</span>
                    <span id="m_transferToDescLabel" runat="server">&#8211; None &#8211;</span>
                    [<a href="#" runat="server" id="m_dialogLink">select transfer to</a>]
                    <input type="hidden" runat="server" id="m_transferToDesc" />
                    <input type="hidden" runat="server" id="m_transferToCode" />
                </span>
            </td>
        </tr>
        <tr>
            <td>
                <div class="Relative" id="AuditResults" runat="server">

                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="middle">
                <span runat="server" id="TestEnvironmentDisclaimerSpan">Document request will be sent to <span runat="server" id="TestEnvironmentDisclaimerVendorName"></span>'s <span class="Highlighted">test environment</span> (<span runat="server" id="TestEnvironmentDisclaimerReason"></span>)</span>
                <hr runat="server" id="TestEnvironmentDisclaimerDivider" class="Divider Hidden" />
                <span id="PackageSpecificWarning" class="AuditEntry CRITICAL Hidden"></span>
                <span id="Summary" runat="server" class="Hidden"></span>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="button" value="Cancel" onclick="onClosePopup();" />
                <input type="button" id="m_PerformAudit" value="Run Audit" disabled="disabled" />
                <input type="button" value="Next" onclick="nextPage();" id="m_btnNext"  disabled="disabled" runat="server" />
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="Disable" runat="server" />
    <asp:HiddenField ID="TemporaryArchiveSourceForAudit" runat="server" />
    <asp:Button runat="server" ID="RunAuditBtnHidden" OnClick="FallbackPost_OnClick"  style="display:none;" />
    </form>
</body>
</html>
