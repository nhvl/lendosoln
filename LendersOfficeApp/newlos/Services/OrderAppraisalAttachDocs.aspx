﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderAppraisalAttachDocs.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.OrderAppraisalAttachDocs" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%-- This page is essentially a reduced copy of OrderAppraisal.aspx --%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Attach Documents to Existing Order</title>
    <link href="../../css/stylesheet.css" rel="stylesheet" type="text/css" />
<style type="text/css">
input
{
    border: default;
}
h2
{
    margin: 0 0 2px 0;
    padding: 2px;
}
input
{
    padding: 0;
    height: 1.5em;
}
img
{
    vertical-align: top;
}
.row
{
    width: 100%;
    margin: 0 auto;
    overflow: hidden;
    font-size: 11px;
}
#AttachmentType
{
    width: 380px;
}
#AttachedDocuments
{
    width: 390px;
    resize: both;
}
#AttachDocsBtnContainer
{
    text-align: center;
}
#AttachDocsBtn, #CloseWindowBtn
{
    height: 2em;
    width: 5em;
}
</style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField id="VendorId" runat="server" />
    <asp:HiddenField id="AppraisalOrderNumber" runat="server" />
    <asp:HiddenField id="ErrorMessage" runat="server" />
    <asp:HiddenField id="FileErrorMessage" runat="server" />
    <div>
        <h2 class="MainRightHeader">
            Attach Documents
        </h2>
        <div class="row" id="AttachmentTypeSection" runat="server">
            <asp:DropDownList runat="server" ID="AttachmentType">
                <asp:ListItem Value="-1" Text="<-- Select Document Type -->"></asp:ListItem>
            </asp:DropDownList>
            <img id="AttachmentTypeReq" src="../../images/require_icon.gif" alt="Required" />
        </div>
        <div class="row">
            <asp:HiddenField runat="server" ID="AttachedDocumentsIds" />
            <asp:TextBox runat="server" ID="AttachedDocuments" cssclass="fillRow" TextMode="MultiLine" Height="150px" ReadOnly="true"></asp:TextBox><br />
            <span style="float:right;">
                <a href="#" onclick="if (!hasDisabledAttr(this)) selectDocumentsClick();" id="SelectDocumentsLink">select documents</a>
            </span>
        </div>
    </div>
    <div id="AttachDocsBtnContainer">
        <input id="AttachDocsBtn" runat="server" value="Attach" type="button" onclick="attachDocsClick();" />
        <input id="CloseWindowBtn" runat="server" value="Cancel" type="button" onclick="onClosePopup();" />
        <span id="ProcessingOrder" style="display:none;">Attaching Documents...</span>
    </div>
    </form>
</body>
<script type="text/javascript">
    $(window).on("load", function() {
        loadDocumentTypeOptions();
        $('#AttachmentType').change(function() {
            if ($(this).val() === '-1') {
                setDisabledAttr($('#SelectDocumentsLink'), true);
                $('#AttachDocsBtn').prop('disabled', true);
            } else {
                setDisabledAttr($('#SelectDocumentsLink'), false);
                if ($('#AttachedDocuments').val() !== '') {
                    $('#AttachDocsBtn').prop('disabled', false);
                }
            }
        });
        $('#AttachDocsBtn').prop('disabled', true);
    });
    
    function loadDocumentTypeOptions() {
        var args = { VendorId : $('#VendorId').val() }, result;
        result = gService.OrderAppraisal.call("LoadDocumentTypeOptions", args);
        if(result && result.value && result.value.IsValid == "True")
        {
            if (result.value.IsGdms === "True")
            {
                var orderFileUploadT = JSON.parse(result.value.GDMSOrderFileUploadFileType);
                $('#AttachmentType').empty().append('<option value="-1"><-- Select Document Type --></option>');
                for (var i = 0; i < orderFileUploadT.length; i++) {
                    $('#AttachmentType').append('<option value="' + orderFileUploadT[i][0] + '">' + orderFileUploadT[i][1] + '</option>');
                }
                setDisabledAttr($('#SelectDocumentsLink'), true);
                $('img').css('display', 'inline');
            }
            else
            {
                $('#AttachmentTypeSection').hide();
            }
            return true;
        }
        if(result)
        {
            var errorMsg = 'Error loading Document Types';
            if(result.value && result.value.ErrorMessage)
                errorMsg = result.value.ErrorMessage;
            alert(errorMsg);
        }
        return false;
    }
    function selectDocumentsClick() {
        var args = {};
        try {
            var ids = JSON.parse(document.getElementById('AttachedDocumentsIds').value);
            args['SelectedDocsIds'] = ids;
        }
        catch (e) { }

        showModal('/newlos/Services/OrderAppraisalDocumentPicker.aspx?loanid=<%= AspxTools.HtmlString(LoanID.ToString()) %>', args, null, null, function(result){ 
            if (result.DocNames && result.DocIds) {
                var text = '';
                for (var i = 0; i < result.DocNames.length; i++) {
                    text += result.DocNames[i] + '\n';
                }
                document.getElementById('AttachedDocuments').value = text;
                document.getElementById('AttachedDocumentsIds').value = JSON.stringify(result.DocIds);
                if (text != '') {
                    $('#AttachDocsBtn').prop('disabled', false);
                }
            }
        });
    }
    function attachDocsClick() {
        $('#AttachDocsBtn').hide();
        $('#CloseWindowBtn').hide();
        $('#ProcessingOrder').show();
        window.setTimeout(function() {
            var args = {
                LoanId : <%= AspxTools.JsString(LoanID) %>,
                VendorId : $('#VendorId').val(),
                AppraisalOrderNumber : $('#AppraisalOrderNumber').val(),
                AttachedDocumentsIds : $('#AttachedDocumentsIds').val(),
                AttachmentType : $('#AttachmentType').val()
            };
            var result;
            if (usingGlobalDMS == "True") {
                result = gService.OrderAppraisal.call("AttachGDMSDocuments", args);
            } else {
                result = gService.OrderAppraisal.call("AttachFrameworkDocuments", args);
            }
            
            var noMessage = true;
            if(result && result.value)
            {
                if (result.value.FileErrorMessage) {
                    alert(result.value.FileErrorMessage);
                    noMessage = false;
                }
                else if (result.value.ErrorMessage) {
                    alert(result.value.ErrorMessage);
                    noMessage = false;
                }
            }
            if(result && result.value && result.value.Success == "True")
            {
                alert("Document(s) successfully attached");
                $('#AttachedDocumentsIds').val("");
                $('#AttachedDocuments').val("");
                $('#AttachDocsBtn').prop('disabled', true);
            }
            else if(noMessage)
            {
                alert("Error adding documents to appraisal.");
            }
            $('#ProcessingOrder').hide();
            $('#AttachDocsBtn').show();
            $('#CloseWindowBtn').show();
        }, 0);
    }
</script>
</html>
