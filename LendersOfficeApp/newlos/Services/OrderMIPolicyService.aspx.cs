﻿// <copyright file="OrderMIPolicyService.aspx.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/17/2014 11:49:10 AM
// </summary>
namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.MortgageInsurance;
    using LendersOffice.Security;
    using Mismo231.MI;
    using Mismo231.MI.MortgageInsuranceResponse;

    /// <summary>
    /// Implements services for the Order MI Policy page of the loan file.
    /// </summary>
    public partial class OrderMIPolicyService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Processes the service method.
        /// </summary>
        /// <param name="methodName">The name of the service method.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "LoadMasterPolicyNumber":
                    this.LoadMasterPolicyNumber();
                    break;
                case "OrderQuote":
                    this.PlaceMIOrder(CreateQuoteOrder());
                    break;
                case "OrderPolicy":
                    this.PlaceMIOrder(CreatePolicyOrder(false));
                    break;
                case "ApplyQuote":
                    this.ApplyQuote();
                    break;
                case "GetQuoteViewerURL":
                    this.GetQuoteViewerURL();
                    break;
                case "GetOrderXML":
                    this.GetOrderXml(GetBool("Quote") ? CreateQuoteOrder() : CreatePolicyOrder(true));
                    return;
                case "PollForResults":
                    this.PollForResults();
                    return;
            }
        }

        private void GetOrderXml(MIRequestData requestData)
        {
            MIRequestHandler requestHandler = new MIRequestHandler(requestData);
            List<string> errors;
            var requestXml = requestHandler.GetRequestXml(checkWorkflow: true, errors: out errors);

            if (errors.Any())
            {
                this.SetResult("Error", string.Join(", ", errors));
            }
            else
            {
                string formattedXml = XDocument.Parse(requestXml).ToString();
                this.SetResult("OrderXML", formattedXml);
            }
        }

        /// <summary>
        /// Loads the master policy number assigned to the branch/lender by the MI vendor.
        /// </summary>
        private void LoadMasterPolicyNumber()
        {
            Guid brokerID = PrincipalFactory.CurrentPrincipal.BrokerId;
            Guid vendorID = GetGuid("VendorID");
            Guid branchID = GetGuid("BranchID");
            string policy = string.Empty;

            try
            {
                policy = MortgageInsuranceVendorBranchSettings.ListCredentialsByBranchId(brokerID, branchID).First(vendor => vendor.VendorId == vendorID).PolicyId;
            }
            catch (ArgumentNullException)
            {
            }
            catch (InvalidOperationException)
            {
            }

            if (string.IsNullOrEmpty(policy))
            {
                try
                {
                    policy = MortgageInsuranceVendorBrokerSettings.ListActiveVendorByBrokerId(brokerID).First(vendor => vendor.VendorId == vendorID).PolicyId;
                }
                catch (ArgumentNullException)
                {
                }
                catch (InvalidOperationException)
                {
                }
            }
            
            this.SetResult("Policy", policy);
        }

        private void PollForResults()
        {
            Guid publicJobId = this.GetGuid("PublicJobId");
            int attemptCount = this.GetInt("AttemptCount");

            var result = MIRequestHandler.CheckForRequestResults(publicJobId);
            if (result.Status == MIRequestResultStatus.Failure)
            {
                this.SetError(string.Join(", ", result.Errors));
            }
            else if (result.Status == MIRequestResultStatus.Processing)
            {
                if (attemptCount >= LendersOffice.Constants.ConstStage.MIFrameworkRequestJobPollingMaxAttempts)
                {
                    this.SetError("MI Order request has timed out.");
                    Tools.LogError($"[MIFrameworkRequestJob] PublicJobId: {publicJobId}. UI has timed out.");
                    return;
                }

                this.SetResult("Status", "Processing");
            }
            else
            {
                this.SetResult("Status", "OK");
                this.SetResult("Message", result.Message);
                this.SetResult("OrderNumber", result.OrderNumber);
                this.SetResult("TransactionID", result.TransactionId);
                this.SetResult("DisplayEdocMessage", result.EdocUploadFailed);
            }
        }

        /// <summary>
        /// Places the MI quote or policy order.
        /// </summary>
        private void PlaceMIOrder(MIRequestData requestData)
        {
            if (requestData == null)
            {
                return;
            }

            MIRequestHandler requestHandler = new MIRequestHandler(requestData);
            MIRequestResult result;
            if (LendersOffice.Constants.ConstStage.DisableMIFrameworkViaBJP)
            {
                result = requestHandler.SubmitSynchronously(checkWorkflow: true);
            }
            else
            {
                result = requestHandler.SubmitToProcessor(checkWorkflow: true);
            }

            if (result.Status == MIRequestResultStatus.Failure)
            {
                this.SetError(string.Join(", ", result.Errors));
            }
            else if (result.Status == MIRequestResultStatus.Processing)
            {
                this.SetResult("Status", "Processing");
                this.SetResult("PollingIntervalInSeconds", requestData.PollingIntervalInSeconds);
                this.SetResult("PublicJobId", result.PublicJobId.ToString());
            }
            else
            {
                this.SetResult("Status", "OK");
                this.SetResult("Message", result.Message);
                this.SetResult("OrderNumber", result.OrderNumber);
                this.SetResult("TransactionID", result.TransactionId);
                this.SetResult("DisplayEdocMessage", result.EdocUploadFailed);
            }
        }

        /// <summary>
        /// Places an MI quote request.
        /// </summary>
        /// <remarks>
        /// Note that <see cref="newlos.Test.TestViewXisResponse.MortgageInsuranceMismo231Export(bool, Guid)"/> 
        /// replicates some of the quote order creation logic found here. The logic in both places should be kept consistent.
        /// </remarks>
        private MIRequestData CreateQuoteOrder()
        {
            Guid loanID = GetGuid("LoanID");
            Guid applicationID = GetGuid("AppID");
            Guid vendorID = GetGuid("VendorID");
            Guid branchID = GetGuid("BranchID");
            E_sProdConvMIOptionT premiumType = (E_sProdConvMIOptionT)GetInt("MIPremiumType");
            string splitPremiumMismoValue = GetString("SplitPremiumMismoValue");
            MI_MIPremiumRefundableTypeEnumerated refundability = (MI_MIPremiumRefundableTypeEnumerated)GetInt("PremiumRefundability");
            MI_MIRenewalCalculationTypeEnumerated renewalType = (MI_MIRenewalCalculationTypeEnumerated)GetInt("RenewalOption");
            MI_MIInitialPremiumAtClosingTypeEnumerated premiumAtClosing = (MI_MIInitialPremiumAtClosingTypeEnumerated)GetInt("PremiumAtClosing");
            string masterPolicyNumber = GetString("MasterPolicyNumber");
            decimal coveragePercent = this.GetRate("MICoveragePercentage", -1);
            bool ufmipFinanced = GetBool("UFMIPFinanced");
            bool relocation = GetBool("RelocationLoan");

            MIRequestData requestData = MIRequestData.CreateQuoteRequestData(
                PrincipalFactory.CurrentPrincipal,
                loanID,
                applicationID,
                vendorID,
                branchID,
                premiumType,
                splitPremiumMismoValue,
                refundability,
                ufmipFinanced,
                masterPolicyNumber,
                coveragePercent,
                renewalType,
                premiumAtClosing,
                relocation,
                LendersOffice.Constants.ConstStage.MIFrameworkRequestJobPollingInterval, E_MiRequestType.User);
            return requestData;
        }

        /// <summary>
        /// Places an MI policy request.
        /// </summary>>
        /// <remarks>
        /// Note that <see cref="newlos.Test.TestViewXisResponse.MortgageInsuranceMismo231Export(bool, Guid)"/> 
        /// replicates some of the policy order creation logic found here. The logic in both places should be kept consistent.
        /// </remarks>
        private MIRequestData CreatePolicyOrder(bool debug)
        {
            var user = PrincipalFactory.CurrentPrincipal;
            Guid brokerID = user.BrokerId;
            Guid userID = user.UserId;
            Guid loanID = GetGuid("LoanID");

            Guid applicationID = GetGuid("AppID");
            Guid transactionID = GetGuid("TransactionID");
            string orderNumber = GetString("OrderNumber");
            Guid branchID = GetGuid("BranchID");
            bool? isDelegated = GetNullableBool("IsDelegated");
            
            MIOrderInfo quote = MIOrderInfo.RetrieveTransaction(transactionID, orderNumber, loanID, brokerID, applicationID);
            if (quote == null || !quote.HasResponse)
            {
                this.SetError(ErrorMessages.MIFramework.QuoteRetrievalFailure());
                return null;
            }

            if (isDelegated.HasValue && isDelegated.Value && MIUtil.CanRequestBeDelegated(user, quote.VendorID))
            {
                quote.DelegationType = DelegationType.Delegated;
            }
            else
            {
                quote.DelegationType = DelegationType.NonDelegated;
            }

            if (!debug)
            {
                this.SaveQuoteDataToLoan(loanID, quote.ResponseProvider.ResponseInfo, quote.VendorID, quote.ResponseProvider.SplitPremium, quote.UfmipFinanced, quote.IsQuoteRequest);
            }

            MIRequestData requestData = MIRequestData.CreatePolicyRequestData(user, loanID, applicationID, branchID, quote, LendersOffice.Constants.ConstStage.MIFrameworkRequestJobPollingInterval);
            return requestData;
        }

        /// <summary>
        /// Loads up the MI quote and saves the quote data to the loan file.
        /// </summary>
        private void ApplyQuote()
        {
            Guid brokerID = PrincipalFactory.CurrentPrincipal.BrokerId;
            Guid userID = PrincipalFactory.CurrentPrincipal.UserId;
            Guid loanID = GetGuid("LoanID");
            Guid applicationID = GetGuid("AppID");
            Guid transactionID = GetGuid("TransactionID");
            string orderNumber = GetString("OrderNumber");

            MIOrderInfo quote = MIOrderInfo.RetrieveTransaction(transactionID, orderNumber, loanID, brokerID, applicationID);

            if (quote == null || !quote.HasResponse)
            {
                this.SetError(ErrorMessages.MIFramework.QuoteRetrievalFailure());
                return;
            }

            this.SaveQuoteDataToLoan(loanID, quote.ResponseProvider.ResponseInfo, quote.VendorID, quote.ResponseProvider.SplitPremium, quote.UfmipFinanced, quote.IsQuoteRequest);

            this.SetResult("Status", "OK");
            this.SetResult("Message", "Save operation complete.");
        }

        /// <summary>
        /// Adds a URL for the MI quote viewer to the result.
        /// </summary>
        private void GetQuoteViewerURL()
        {
            Guid loanID = GetGuid("LoanID");
            Guid applicationID = GetGuid("AppID");
            Guid transactionID = GetGuid("TransactionID");
            string orderNumber = GetString("OrderNumber");

            this.SetResult("Status", "OK");

            string url = MIUtil.QuoteResponseViewerURL(loanID, applicationID, orderNumber, transactionID);
            this.SetResult("URL", LendersOffice.AntiXss.AspxTools.SafeUrl(url).Trim('"'));
        }

        /// <summary>
        /// Saves the give MI quote data to the loan file.
        /// </summary>
        /// <param name="loanID">The sLId of the LQB loan file.</param>
        /// <param name="data">An MIResponse object with the data from the MI vendor.</param>
        /// <param name="vendorID">The guid assigned to the MI vendor by LQB.</param>
        /// <param name="splitPremium">True if the MI response included a Split Premium indicator. Otherwise false.</param>
        /// <param name="financedUFMIP">True if the upfront MI premium is financed.</param>
        /// <param name="isQuote">Indicates whether the loan is being updated with a quote or a policy order.</param>
        private void SaveQuoteDataToLoan(Guid loanID, MIResponse data, Guid vendorID, bool splitPremium, bool financedUFMIP, bool isQuote)
        {
            MILoanSetter setter = new MILoanSetter(loanID, data, vendorID, splitPremium, financedUFMIP, isQuote);
            setter.UpdateLoanWithQuoteInfo();
        }

        /// <summary>
        /// Creates an MI order ready to be serialized into XML.
        /// </summary>
        /// <param name="quote">Whether the order is for a quote(true) or policy(false).</param>
        /// <returns>An MI order info object to be serialized.</returns>
        private MIOrderInfo GetSerializableMIOrder(MIOrderInfo baseOrder)
        {
            Guid branchID = GetGuid("BranchID");

            MortgageInsuranceVendorBranchSettings branchSettings = null;

            try
            {
                branchSettings = MortgageInsuranceVendorBranchSettings.ListCredentialsByBranchId(baseOrder.BrokerID, branchID).First(vendor => vendor.VendorId == baseOrder.VendorID);
            }
            catch (ArgumentNullException)
            {
            }
            catch (InvalidOperationException)
            {
            }

            MortgageInsuranceVendorBrokerSettings lenderSettings = RetrieveLenderSettings(baseOrder);

            bool useLenderConfig = branchSettings == null;

            if (useLenderConfig && lenderSettings == null)
            {
                this.SetError(ErrorMessages.MIFramework.NoCredentials());
                return null;
            }

            MIRequestProvider requestProvider = new MIRequestProvider(baseOrder.LoanID, PrincipalFactory.CurrentPrincipal);

            MIOrderInfo order;

            if (useLenderConfig)
            {
                order = requestProvider.CreateMortgageInsuranceRequest(baseOrder, lenderSettings);
            }
            else
            {
                order = requestProvider.CreateMortgageInsuranceRequest(baseOrder, branchSettings);
            }

            return order;
        }

        private static MortgageInsuranceVendorBrokerSettings RetrieveLenderSettings(MIOrderInfo order)
        {
            //// The lender config is required either way because that's where the production/test environment bit is located.
            try
            {
                return MortgageInsuranceVendorBrokerSettings.ListActiveVendorByBrokerId(order.BrokerID).FirstOrDefault(vendor => vendor.VendorId == order.VendorID);
            }
            catch (ArgumentNullException)
            {
            }
            catch (InvalidOperationException)
            {
            }

            return null;
        }

        /// <summary>
        /// Sets an error result and message via the base service.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        private void SetError(string errorMessage)
        {
            this.SetResult("Status", "Error");
            this.SetResult("Message", errorMessage);
        }

        /// <summary>
        /// Validates the inputs for a quote request.
        /// </summary>
        /// <param name="premiumAtClosing">The premium at closing type selected.</param>
        /// <param name="masterPolicyNumber">The master policy number entered.</param>
        /// <param name="coveragePercent">The coverage percentage entered.</param>
        /// <returns>True if the inputs are valid. False otherwise.</returns>
        private bool ValidateInputs(MI_MIInitialPremiumAtClosingTypeEnumerated premiumAtClosing, string masterPolicyNumber, out decimal coveragePercent)
        {
            bool valid = true;
            coveragePercent = 0.00M;

            try
            {
                coveragePercent = this.GetRate("MICoveragePercentage");
            }
            catch (GenericUserErrorMessageException)
            {
                this.SetError("Please enter a positive percentage for the MI Coverage %.");
                valid = false;
            }

            if (valid && coveragePercent <= 0.00M)
            {
                this.SetError("Please enter a positive percentage for the MI Coverage %.");
                valid = false;
            }

            if (valid && premiumAtClosing == MI_MIInitialPremiumAtClosingTypeEnumerated.None)
            {
                this.SetError("Please select a Premium at Closing option.");
                valid = false;
            }

            if (valid && string.IsNullOrEmpty(masterPolicyNumber))
            {
                this.SetError("Please enter a Master Policy Number");
                valid = false;
            }

            return valid;
        }
    }
}
