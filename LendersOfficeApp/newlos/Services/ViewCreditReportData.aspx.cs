using System;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Xml;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.CreditReport;

namespace LendersOfficeApp.newlos.Services
{
    public partial class ViewCreditReportData : LendersOfficeApp.newlos.BaseLoanPage
	{
        private CPageData m_dataLoan = null;
        private CAppData m_dataApp = null;
        private CreditReportProxy m_credProxy = null;

        private CreditReportProxy CreditProxy
        {
            get
            {
                if (m_credProxy == null)
                {
                    m_credProxy = m_dataApp.CreditReportData.Value as CreditReportProxy;
                    if (m_credProxy != null)
                    {
                        m_credProxy.IsDebug = true;
                        m_dataApp.SetLoanDataOn(m_credProxy);
                    }
                }
                return m_credProxy;
            }
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            m_dataLoan = new CPriceEngineData(LoanID);
            m_dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            m_dataLoan.InitLoad();
            m_dataApp = m_dataLoan.GetAppData(0);

            if (RequestHelper.GetSafeQueryString("cmd") == "liabilities")
            {
                ViewLiabilities();
            }
            else if (RequestHelper.GetSafeQueryString("cmd") == "publicrecords")
            {
                ViewPublicRecords();
            }
            else
            {
                if (!Page.IsPostBack)
                {
                    DisplayInitialReport();
                }
            }
            pnlMsg.InnerText = ErrorMessages.CreditReportViewableDataMissing;
        }

        protected void DisplayInitialReport()
        {
            m_dataApp.SetLoanDataOn(CreditProxy);

            CreditProxy.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Both;
            m_dataApp.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Both;

            XmlDocument xmlDocEval = new XmlDocument();
            XmlElement root = xmlDocEval.CreateElement("PricePolicy");

            if (null == CreditProxy)
            {
                MainHtml.Text = "NO CREDIT REPORT.";
                return;
            }
            if (null != CreditProxy)
            {
                GenerateHtml(CreditProxy);
            }
        }

        private void ViewPublicRecords()
        {
            if (null == CreditProxy)
            {
                return;
            }

            MainHtml.Text = CreditReportUtilities.GeneratePublicRecordsHtml(CreditProxy);
            radioPubRecords.Checked = true;
        }

        private void ViewLiabilities()
        {
            if (null == CreditProxy)
            {
                return;
            }

            MainHtml.Text = CreditReportUtilities.GenerateLiabilitiesHtml(CreditProxy);
            radioLiabilities.Checked = true;
        }

        /// <summary>
        /// Removes name, ssn alias address from the mismo report.
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        private XmlDocument ScrubSensitiveDataFromMismoReport(XmlDocument doc)
        {

            XmlNode residence = doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/BORROWER/_RESIDENCE");
            residence.Attributes["_StreetAddress"].Value = "123 Main St";

            XmlNodeList embeddedFiles = doc.SelectNodes("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/EMBEDDED_FILE");
            foreach (XmlNode embeddedFile in embeddedFiles)
            {
                embeddedFile.ParentNode.RemoveChild(embeddedFile);
            }

            string xml = doc.OuterXml;

            xml = Regex.Replace(xml, "_FirstName=\"[^\"]+\"+?", "_FirstName=\"JOHN\"");
            xml = Regex.Replace(xml, "_LastName=\"[^\"]+\"+?", "_LastName=\"DOE\"");
            xml = Regex.Replace(xml, "_SSN=\"\\d{9}\"", "_SSN=\"100000000\"");
            xml = Regex.Replace(xml, "_UnparsedName=\"[^\"]+\"+?", "_UnparsedName=\"JOHN\"");
            xml = Regex.Replace(xml, "alias=\"[^\"]+\"+?", "alias=\"Cleared\"");
            XmlDocument cleanedMismo = new XmlDocument();
            cleanedMismo.LoadXml(xml);
            return cleanedMismo;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion

        private void GenerateHtml(ICreditReport creditReport)
        {
            MainHtml.Text = "";
            if (null != creditReport)
            {
                MainHtml.Text = CreditReportUtilities.GenerateSummaryHtml(creditReport);
                radioScores.Checked = true;
            }
        }
	}
}
