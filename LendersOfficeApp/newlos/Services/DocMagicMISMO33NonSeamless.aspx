﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocMagicMISMO33NonSeamless.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.DocMagicMISMO33NonSeamless" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import namespace="LendersOffice.AntiXss" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Document Generation</title>
    <BASE target="_self">
</head>
<body bgcolor="gainsboro" style="padding-left:3px">
    <form id="form1" runat="server">
    <asp:HiddenField ID="m_AlertMessage" runat="server" />
    <asp:HiddenField ID="m_PageUrl" runat="server" />
    <script language="javascript" type="text/javascript">
        <!--
            
    function alertOnError() {
        if (document.getElementById('m_AlertMessage').value != '') {
            LQBPopup.ShowString(document.getElementById('m_AlertMessage').value, {
                width: 420,
                height: 180,
                modifyOverflow: false,
                popupClasses: "LQBPopupAlert",
                onClose: function () {
                    jQuery('#Summary').text(document.getElementById('m_AlertMessage').value);
                }
            });
        }
    }

    function nextPage() {
        var selectedPackage = jQuery('#m_ddlPackageType option:selected').text();
        var selectedProgram = jQuery('#m_ddlPlanCode option:selected').text();

        if(selectedPackage.trim() === '' || selectedProgram.trim() === '')
        {
            document.getElementById('m_AlertMessage').value = "You must select a valid \"Package Type\" and a valid \"Loan Program\".";
            alertOnError();
            return;
        }

        document.getElementById('m_btnSubmit').disabled = true;

        <%=AspxTools.JsGetElementById(RunBtnHidden)%>.click();
    }
        
        var childArgs = null;
        function getChildArgs(){
            return childArgs;
        }
        
        jQuery(function($) {
            window.resizeTo(625, 580);
            var planCodeLabel = $('#PlanCodeLabel').val();
            if(planCodeLabel){
                $('#PlanCode span.Label').text(planCodeLabel);
            }
        
            if($('#Disable').val() == 'True'){
                document.getElementById('m_btnSubmit').disabled = true;
            }
            else {
                document.getElementById('m_btnSubmit').disabled = false;
            }
            alertOnError();
            
            $('#m_ddlPlanCode').change(function(event){
                var val = $(this).val();
                var current =  $(':selected', this);
                while(!isValidPlanCode(val))
                {
                    current = current.next();
                    val = current.val();
                }
                $(this).val(val);
                event.preventDefault();
            });
            
            function isValidPlanCode(name)
            {
                return $.trim(name) != '';
            }

            function pickPlanCode() {
                var $planCodeDDL = $('#m_ddlPlanCode');
                var $validPlanCodes = $planCodeDDL.find('option[value!=""]');
                if ($validPlanCodes.length === 1) {
                    $planCodeDDL.change(); // Select the only valid entry.
                    $planCodeDDL.prop('disabled', false).css('background-color', '');
                }
                else if (needsPlanCodePicked()) {
                    forcePlanCodeSelection(function(args) {
                        if (args.Action == 'Failure') {
                            jQuery('#Results').empty().append('<div style="text-align:center"><br /><br />Please correct loan errors before auditing</div>');
                        }
                        else{
                            jQuery('#Results').empty().append('<div style="text-align:center"><br /><br />Please wait...</div>');
                            window.name = "DocMagicMISMO33NonSeamless"
                            window.open(window.location, "DocMagicMISMO33NonSeamless");
                        }
                    });
                }

                return true;
            }

            function needsPlanCodePicked() {
                var ret = false;
                var DTO = { 
                    sLId: ML.sLId,
                    VendorId: <%= AspxTools.JsString(VendorId) %>
                };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: 'DocMagicMISMO33NonSeamless.aspx/NeedsPlanCode',
                    data: JSON.stringify(DTO),
                    dataType: 'json',
                    async: false
                }).then(
                    function(msg) {
                        ret = msg.d;
                    },
                    function(XMLHttpRequest, textStatus, errorThrown) { 
                        var message = textStatus + ": " + errorThrown;
                        alert(message); 
                        $('#Summary').text(message);
                    }
                );

                return ret;
            }

            function forcePlanCodeSelection(forceCallback) {
                forceCallback = forceCallback || function() { };
                var args = {};
                args.callback = function(args) { forceCallback(args); };
                try
                {
                    var _childWindow = showModeless('/newlos/SelectDocMagicPlan.aspx?loanid=' + document.getElementById('loanid').value, null, null, args, true);
                }
                catch(e)
                {
                    var _childWindow = dialogArguments.open('/newlos/SelectDocMagicPlan.aspx?loanid=' + document.getElementById('loanid').value, "PlanCodeSelection", "close=yes,titlebar=yes");
                }
            }

            pickPlanCode()
        });
        //-->
    </script>
    
    <style type="text/css">
    #Results
    {
        width: 600px;
        height: 400px;
        overflow-y:scroll; 
        border:inset
    }
    
    .Relative
    {
        position: relative;
    }
    
    #PlanCode
    {
        float:right;
        display: inline-block;
    }
    
    .Hidden
    {
        display: none;
    }
    
    .Action
    {
        cursor: pointer;
        color: Blue;
        text-decoration: underline;
    }
    
    .Action:hover
    {
        color: Orange;
    }
    
    .details-text
    {
        font-weight: normal;
    }
    
    </style>
    <table id="MainTable" cellspacing="0" cellpadding="0" width="98%" class="FieldLabel">
        <tr>
            <td class="MainRightHeader">
                Generate Documents
            </td>
        </tr>
        <tr>
            <td>
                <span class="Settings" id="PlanCode">
                    <span class="Label">Plan Code</span>
                    <asp:DropDownList ID="m_ddlPlanCode" runat="server" Width="240"></asp:DropDownList>
                </span>
                <span class="Settings" id="PackageType">
                    <span class="Label">Package Type</span>
                    <asp:DropDownList ID="m_ddlPackageType" runat="server" Width="150"></asp:DropDownList>
                </span>
            </td>
        </tr>
        <tr>
            <td>
                <div class="Relative" id="Results" runat="server">

                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height:30px" valign="middle">
                <span id="Summary" runat="server"></span>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="button" value="Cancel" onclick="onClosePopup();" />
                <input type="button" value="Submit" onclick="nextPage();" id="m_btnSubmit" runat="server" />
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="Disable" runat="server" />
    <asp:Button runat="server" ID="RunBtnHidden" OnClick="SubmitBtn_Click"  style="display:none;" />
    </form>
</body>
</html>