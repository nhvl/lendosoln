﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Text;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Integration.DataRetrievalFramework;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Services
{
    public partial class SubmitToThirdParty : LendersOfficeApp.newlos.BaseLoanPage
    {
        #region Variables
        protected string m_sResultMessage = string.Empty;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Guid loanID = RequestHelper.GetGuid("loanid", Guid.Empty);

            if (DataRetrievalPartner.CountPartnersByBrokerId(BrokerID) == 0)
            {
                DisplayError(ErrorMessages.SubmitToThirdParty.PageDisabled);
            }
            else if (loanID == Guid.Empty)
            {
                DisplayError(ErrorMessages.InvalidLoanID);
            }
            else if (!BrokerUser.IsInEmployeeGroup(ConstAppDavid.ThirdPartyExportEmployeeGroup))
            {
                DisplayError(ErrorMessages.SubmitToThirdParty.PagePermissionRequired);
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.PageID = "SubmitToThirdParty";
            this.PageTitle = "Submit to 3rd Party";

            BindPartnerList();

            this.m_submitBtn.Click += new EventHandler(m_submitBtn_Click);
        }

        void m_submitBtn_Click(object sender, EventArgs e)
        {
            if (BrokerUser.IsInEmployeeGroup(ConstAppDavid.ThirdPartyExportEmployeeGroup) == false)
            {
                m_sResultMessage = ErrorMessages.SubmitToThirdParty.SubmitPermissionRequired;
                return;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SubmitToThirdParty));
            dataLoan.InitLoad();

            try
            {
                Guid parterId = new Guid(m_PartnerList.SelectedValue);

                DataRetrievalPartner partner = DataRetrievalPartner.RetrieveById(parterId);
                if (partner.IsXsltExport)
                {
                    DataRetrievalFrameworkServer.SubmitUsingExportXslt(BrokerUser.UserId, LoanID, partner.XsltExportMapName, PrincipalFactory.CurrentPrincipal);
                    // 1/21/2014 dd - Until we have a better way to perform XSLT XML response to friendly user message we will
                    // just return generic message.
                    m_sResultMessage = "Submission complete.";
                }
                else
                {
                    DataRetrievalFrameworkServer.Submit(dataLoan.sLNm, BrokerUser.BrokerId, new Guid(m_PartnerList.SelectedValue));
                    m_sResultMessage = "Submission complete.";
                }
            }
            catch (CBaseException exc)
            {
                m_sResultMessage = exc.UserMessage;
            }
        }

        private void BindPartnerList()
        {
            m_PartnerList.DataSource = DataRetrievalPartner.ListPartnersByBrokerId(BrokerID);
            m_PartnerList.DataValueField = "PartnerID";
            m_PartnerList.DataTextField = "Name";
            m_PartnerList.DataBind();

            if (m_PartnerList.Items.Count == 1)
            {
                m_PartnerList.Enabled = false;
            }
        }

        private void DisplayError(string sError)
        {
            ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.GenericAccessDenied, sError), false, BrokerUser.BrokerId, BrokerUser.EmployeeId);
        }
    }
}