<%@ Page language="c#" Codebehind="ExistingLiabilitiesConfirmation.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Services.ExistingLiabilitiesConfirmation" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>ExistingLiabilitiesConfirmation</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
		<link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">    
  </HEAD>
  <body MS_POSITIONING="FlowLayout" bgcolor=gainsboro>
		<script language=javascript>
<!--
function _init() {
  resize(550, 280);
}
function f_choice(choice) {
  var args = window.dialogArguments || {};
  args.choice = choice;
  onClosePopup(args);
  
}
//-->
</script>
    <h4 class="page-header">Existing Liabilities</h4>
    <form id="ExistingLiabilitiesConfirmation" method="post" runat="server">
      <table width="100%">
        <tr><td style="FONT-WEIGHT:bold;COLOR:black">There are existing liabilities.&nbsp; How should we proceed?</td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td style="FONT-WEIGHT:bold;COLOR:black"><ul><li>Delete the existing liabilities and import new 
        liabilities from the credit report</li>
		<br>- or -        
		<li>Update the existing liabilities and import new liabilities from the credit report</li>
        </ul>
        </td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td align=center><input type=button value="Delete existing and import new" class="buttonstyle" onclick="f_choice(0);" style="WIDTH: 215px">&nbsp;<input type=button value="Update existing and import new" class="buttonstyle" onclick="f_choice(1);" style="WIDTH: 215px">&nbsp;<input type=button value="Cancel" class="buttonstyle" onclick="f_choice(2);"></td></tr>
      </table>
     </form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
	
  </body>
</HTML>
