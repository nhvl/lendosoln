using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using DataAccess;
using LendersOfficeApp.newlos.Services;
using LendersOfficeApp.los.admin;
using LendersOffice.Common;
using LendersOffice.CreditReport;
using CommonLib;

namespace LendersOfficeApp.los.credit
{
	public partial class LoginScreen : LendersOffice.Common.BaseServicePage
	{
        protected bool m_hasInstantViewID = false;

        protected bool m_useCreditCardPaymentQueryString = false;
    
        protected void PageInit(object sender, System.EventArgs e) 
        {
            BillingZipcode.SmartZipcode(BillingCity, BillingState);
            
            m_useCreditCardPaymentQueryString = true;
    
            UseCreditCardPayment.Checked = m_useCreditCardPaymentQueryString;

            this.RegisterService("card", "/newlos/Services/LoginScreenService.aspx");
        }
		protected void PageLoad(object sender, System.EventArgs e) 
        {
            if (!Page.IsPostBack)
            {
                BillingExpirationYear.Items.Clear();
                BillingExpirationYear.Items.Add(new ListItem("-1", "-1", true));
                int year = DateTime.Now.Year;
                for (int i = 0; i < 7; i++)
                {
                    BillingExpirationYear.Items.Add(new ListItem(year.ToString(), year.ToString()));
                    year += 1;
                }

                this.IsLqbPopup.Value = RequestHelper.GetBool("lqbp").ToString();
                this.MiddleNameRow.Visible = RequestHelper.GetBool("mn");
            }
		}

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
