﻿namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.TotalScorecard;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Reminders;
    using LendersOffice.Security;
    using TotalScoreCard;

    public partial class FHATotalSubmitService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "Save" :
                    Save();
                    break;
                case "RefreshRentalIncome":
                    RefreshRentalIncome();
                    break;
                case "SaveAndSubmit" :
                    CPageData data = Save();
                    ConflictCheck(data);
                    break; 
                case "Submit":
                    Submit(GetGuid("loanid"));
                    break;
                case "ImportConditions":
                    BrokerDB db = BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                    if (db.IsUseNewTaskSystem)
                    {
                        ImportConditionsNew();
                    }
                    else
                    {
                        ImportConditions();
                    }
                    break;
                case "UpdateAssetInfo":
                    UpdateAssetInfo();
                    break;
                default:

                    break;
            }

        }

        private void UpdateAssetInfo()
        {
            Guid loanId = GetGuid("loanid");

            CPageData fhaData = CPageData.CreateUsingSmartDependency(loanId, typeof(FHATotalSubmitService));
            fhaData.InitLoad();
            int numberOfAssets = GetInt("AssetTot", 0);
            

            for (int i = 0; i < numberOfAssets; i++)
            {
                Guid assetId = GetGuid("AssetId_" + i);
                int source = GetInt("AssetSource_" + i);
                Guid appId = GetGuid("AssetAppId_" + i);

                CAppData app = fhaData.GetAppData(appId);
                var asset = app.aAssetCollection.GetRegRecordOf(assetId);
                asset.GiftSource = (E_GiftFundSourceT)source;
            }

            for (int i = 0; i< fhaData.nApps; i++)
            {
                CAppData app = fhaData.GetAppData(i);
                app.aAssetCollection.Flush();
            }


            SetResult("sTotGiftFundAsset", fhaData.sTotGiftFundAsset_rep);
            SetResult("sPrimaryGiftFundSource", fhaData.sPrimaryGiftFundSource.ToString());
        }
        private void ImportConditions()
        {
            Guid loanId = GetGuid("loanid");

            CPageData data = new CFHATotalAuditData(GetGuid("loanid"));
            data.InitLoad();

            XDocument doc = XDocument.Parse(data.sTotalScoreCertificateXmlContent.Value);

            Regex anchor = new Regex("<a href='([^']*)' target='[_]blank' >[^<]*</a>", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex br = new Regex("<br[/]*>", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            LoanConditionSetObsolete ldSet = new LoanConditionSetObsolete();
            ldSet.RetrieveAll(loanId, false);
            var savedConditions = ldSet.Digest;

            var conditionsToImport = new List<CLoanConditionObsolete>();

            foreach (XElement element in doc.Element("Findings").Element("ApprovalConditions").Elements("Category"))
            {
                foreach (XElement condition in element.Elements("condition"))
                {
                    if (condition.Attribute("doNotImport") != null)
                    {
                        continue;
                    }
                    CLoanConditionObsolete cdItem = new CLoanConditionObsolete(data.sBrokerId);
                    cdItem.CondCategoryDesc = "TOTAL-" + element.Attribute("name").Value;
                    cdItem.LoanId = loanId;
                    cdItem.CondDesc = br.Replace(anchor.Replace(condition.Value, "$1"), System.Environment.NewLine);

                    //dont save if there is a condition like this one already. Yes this is slow, but I don't think there should be too many conditions. 
                    if (savedConditions.Any(p =>
                        String.Equals(p.Category, cdItem.CondCategoryDesc, StringComparison.OrdinalIgnoreCase) &&
                        String.Equals(p.Description, cdItem.CondDesc, StringComparison.OrdinalIgnoreCase)))
                    {
                        continue;
                    }

                    conditionsToImport.Add(cdItem);
                }
            }

            if (conditionsToImport.Count > 0)
            {
                using (CStoredProcedureExec exec = new CStoredProcedureExec(data.sBrokerId))
                {
                    try
                    {
                        exec.BeginTransactionForWrite();
                        conditionsToImport.ForEach(p => p.Save(exec));
                        exec.CommitTransaction();
                    }

                    catch (Exception e)
                    {
                        Tools.LogError("Importing FHA Total Submissions", e);
                        exec.RollbackTransaction();
                        throw new CBaseException(ErrorMessages.Generic, "Could not save conditions.");
                    }
                }
            }

            SetResult("Success", true);
        }

        private void ImportConditionsNew()
        {
            Guid loanId = GetGuid("loanid");

            CPageData data = new CFHATotalAuditData(GetGuid("loanid"));
            data.InitSave(ConstAppDavid.SkipVersionCheck);

            XDocument doc = XDocument.Parse(data.sTotalScoreCertificateXmlContent.Value);

            Regex anchor = new Regex("<a href='([^']*)' target='[_]blank' >[^<]*</a>", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex br = new Regex("<br[/]*>", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            IConditionImporter importer = data.GetConditionImporter("FHA TOTAL SCORECARD");
            importer.IsSkipCategoryCheck = true; // 9/7/2011 dd - OPM 70845 don't check category for condition uniqueness.

            var conditionsToImport = new List<CLoanConditionObsolete>();

            foreach (XElement element in doc.Element("Findings").Element("ApprovalConditions").Elements("Category"))
            {
                foreach (XElement condition in element.Elements("condition"))
                {
                    if (condition.Attribute("doNotImport") != null)
                    {
                        continue;
                    }
                    string category = element.Attribute("name").Value;
                    string desc = br.Replace(anchor.Replace(condition.Value, "$1"), System.Environment.NewLine);
                    importer.AddUnique(category, desc);
                }
            }

            data.Save();

            SetResult("Success", true);
        }

            

    
        private void ConflictCheck(CPageData data)
        {
            bool hasConflict = data.sTotalScoreHasConflictBetweenFhaTransmittalAnd1003;

            SetResult("hasConflict", hasConflict);
            if (hasConflict)
            {
                return;
            }

            Submit(data.sLId);
        }

        private void Submit(Guid slid)
        {
         
            E_DataSourcePage src = E_DataSourcePage.Form_1003; 
            switch ( GetString("Choice", "1003") ) {
                case "1003" :
                    src = E_DataSourcePage.Form_1003;
                    break;
                case "TransmittalSummary" :
                    src = E_DataSourcePage.FHA_Transmittal_Summary;
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Invalid User Choice in FHA Total Submit");

            }
            CreditRiskEvaluation eval = new CreditRiskEvaluation();
            string errorKey = "";
            if (eval.Evaluate(slid, E_CalcModeT.LendersOfficePageEditing, src, out errorKey))
            {
                SetResult("Success", "True");
            }
            else
            {
                SetResult("Success", "False");
                SetResult("ErrorKey", errorKey);
            }
            
        }
        private void RefreshRentalIncome()
        {
            Guid loanId = GetGuid("loanid");
            CPageData pageData = new CFHATotalAuditData(loanId);
            pageData.InitLoad();                                                                                //in the future we may want to remove this DB dependency...
            pageData.sTotalScoreAppraisedFairMarketRent_rep = GetString("sTotalScoreAppraisedFairMarketRent");
            pageData.sTotalScoreVacancyFactor_rep = GetString("sTotalScoreVacancyFactor");
            SetResult("sTotalScoreNetRentalIncome", pageData.sTotalScoreNetRentalIncome_rep);
        }

        private CPageData Save()
        {
            Guid loanId = GetGuid("loanid");
            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
            CPageData pageData = new CFHATotalAuditData(loanId);
            pageData.InitSave(sFileVersion);

            E_sLPurposeT[] refinancePurposes = new E_sLPurposeT[] { E_sLPurposeT.Refin, E_sLPurposeT.RefinCashout, E_sLPurposeT.FhaStreamlinedRefinance };



            if (refinancePurposes.Contains(pageData.sLPurposeT))
            {

                pageData.sTotalScoreRefiT = (E_sTotalScoreRefiT)GetInt("sTotalScoreRefiT"); 
                bool found = false;
                List<ILiabilityRegular> mortgageLiabilities = pageData.sMortgageLiaList;
                string clientId = GetString("MortgageLiabilitiesId");
                int count = GetInt("MortgageLiabilitiesCount");
                for (int i = 0; i < count; i++)
                {
                    int rowNum = i + 1;
                    string rowName = string.Format(
                        "{0}_ctl{1}_",
                        clientId,
                        rowNum.ToString("D2"));
                    Guid recordId = GetGuid(rowName + "MortLiabIdentifier");
                    ILiabilityRegular current = mortgageLiabilities.Where(p => p.RecordId == recordId).First();
                    if (current == null)
                    {
                        throw new CBaseException("Please click on back and refresh the page. One of the liabilities could not be found.", "Liab missing from total submit save.");
                    }

                    string firstSubProp = GetString(rowName + "SubjProp1stMort", "");
                    current.IsSubjectProperty1stMortgage = firstSubProp == "SubjProp1stMort";
                    if (current.IsSubjectProperty1stMortgage)
                    {
                        if (found)
                        {
                            throw new CBaseException(ErrorMessages.Generic, "1stSubjectAlreadyFound");
                        }
                        found = true;
                        current.IsSubjectPropertyMortgage = true;
                    }
                    else
                    {

                        string regSubProp = GetString(rowName + "PropStatus");
                        switch (regSubProp)
                        {
                            case "SubjectProperty":
                                current.IsSubjectPropertyMortgage = true;
                                break;
                            case "OtherProperty":
                                current.IsSubjectPropertyMortgage = false;
                                break;
                            default:
                                throw new CBaseException(ErrorMessages.Generic, "Invalid iSsubjectproperty found.");
                        }
                    }

                    current.Update();
                }
            }
            if (pageData.sUnitsNum >= 3)
            {
                pageData.sTotalScoreAppraisedFairMarketRent_rep = GetString("sTotalScoreAppraisedFairMarketRent");
                pageData.sTotalScoreVacancyFactor_rep = GetString("sTotalScoreVacancyFactor");
            }

            pageData.sTotalScoreFhaProductT =  (E_sTotalScoreFhaProductT)GetInt("sTotalScoreFhaProductT");
            pageData.sTotalScoreIsIdentityOfInterest = GetBool("sTotalScoreIsIdentityOfInterest");
            if (pageData.sTotalScoreIsIdentityOfInterest)
            {
                pageData.sTotalScoreIsIdentityOfInterestException = GetBool("sTotalScoreIsTransactionException");
            }
            else
            {
                pageData.sTotalScoreIsIdentityOfInterestException = false;
            }

            pageData.sNoteIR_rep = GetString("sNoteIR");
            pageData.sTerm_rep = GetString("sTerm");
            pageData.sFinMethT = (E_sFinMethT)GetInt("sFinMethT");

            pageData.sTotalScoreUploadDataFHATransmittal = GetBool("sTotalScoreUploadDataFHATransmittal");


            if (pageData.sConcurSubFin > 0 || pageData.sSubFin > 0)
            {
                pageData.sTotalScoreSecondaryFinancingSrc = (E_TotalScorecardSecondaryFinancingSrc)GetInt(nameof(pageData.sTotalScoreSecondaryFinancingSrc));
                pageData.sTotalScoreSecondaryFinancingSrcLckd = GetBool(nameof(pageData.sTotalScoreSecondaryFinancingSrcLckd));
            }

            if (pageData.sFinMethT == E_sFinMethT.ARM)
            {
                pageData.sRAdj1stCapMon_rep = GetString("sRAdj1stCapMon");
            }
            else
            {
                pageData.sRAdj1stCapMon_rep = "0";
            }

      

            int numberOfApps = GetInt("NumberOfApplications");

            for (int i = 0; i < numberOfApps; i++)
            {
                int controlNum = 2 * i;
                string preid = string.Format(
                    "{0}_ctl{1}_",
                    GetString("BorrowersClientId"),
                    controlNum.ToString("D2"));
                Guid appId = GetGuid(preid + "BorrowerApplicationId");
                CAppData app = pageData.GetAppData(appId);

                app.aBTotalScoreIsCAIVRSAuthClear = GetBool(preid + "aBTotalScoreIsCAIVRSAuthClear");
                app.aFHABCaivrsNum = GetString(preid + "aFHABCaivrsNum");
                app.aBTotalScoreIsFthb = GetBool(preid + "aBTotalScoreIsFthb");
                app.aBTotalScoreFhtbCounselingT = (E_aTotalScoreFhtbCounselingT)GetInt(preid + "aBTotalScoreFhtbCounselingT");
                app.aFHABLdpGsaTri = (E_TriState)GetInt(preid + "aFHABLdpGsaTri",0);

                if (GetBool(preid+"HasCoborrower"))
                {
                    app.aCTotalScoreIsCAIVRSAuthClear = GetBool(preid + "aCTotalScoreIsCAIVRSAuthClear");
                    app.aFHACCaivrsNum = GetString(preid + "aFHACCaivrsNum");
                    app.aCTotalScoreIsFthb = GetBool(preid + "aCTotalScoreIsFthb");
                    app.aCTotalScoreFhtbCounselingT = (E_aTotalScoreFhtbCounselingT)GetInt(preid + "aCTotalScoreFhtbCounselingT");
                    app.aFHACLdpGsaTri = (E_TriState)GetInt(preid + "aFHACLdpGsaTri",0);
                }

            }

            int assetCount = GetInt("AssetTot", 0);

            if (assetCount > 0)
            {
                for (int x = 0; x < assetCount; x++)
                {
                    CAppData app = pageData.GetAppData(GetGuid("AssetAppId_" + x));
                    var giftAsset = app.aAssetCollection.GetRegRecordOf(GetGuid("AssetId_" + x));
                    giftAsset.GiftSource = (E_GiftFundSourceT)GetInt("AssetSource_" + x);
                }
            }

            // OPM 48103.
            BrokerDB broker = BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            int sFhaLenderIdT = GetInt("sFhaLenderIdT", -1);

            if (broker.IsHideFhaLenderIdInTotalScorecard == false && sFhaLenderIdT > -1 )
            {
                pageData.sFhaLenderIdT = (E_sFhaLenderIdT)sFhaLenderIdT;
                if (broker.FhaLenderId == string.Empty)
                {
                    pageData.sFHALenderIdCode = GetString("sFHALenderIdCode");
                }
                
                pageData.sFHASponsorAgentIdCode = GetString("sFHASponsorAgentIdCode");
                pageData.sFHASponsoredOriginatorEIN = GetString("sFHASponsoredOriginatorEIN");

            }
            
            pageData.sAgencyCaseNum = GetString("sAgencyCaseNum");

            pageData.Save();

            return pageData;
        }
    }
}
