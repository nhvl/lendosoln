<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="LoginScreen.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.credit.LoginScreen" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>LoginScreen</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </HEAD>
<body style="PADDING-LEFT: 5px" bgColor=gainsboro 
MS_POSITIONING="FlowLayout">
<script language=javascript>
  <!--
  function _init() {
   resizeForIE6And7(650, 430);
    f_enableCreditCardValidators(true);
    f_displayCreditCardPanel();

  /*
    <% if (m_hasInstantViewID) { %>
      f_displayInstantIDPanel();
    <% } else { %>
      document.getElementById("m_loginTF").focus();
    <% } %>
  */
  }
  function f_displayInstantIDPanel() {
    document.getElementById("MclInstantIDPanel").style.display = "";
    document.getElementById("RememberLoginPanel").style.display = "none";
    document.getElementById("RegularLoginPanel").style.display = "none";
    document.getElementById("LoginValidator").enabled = false;
    document.getElementById("PasswordValidator").enabled = false;
  }
  function TriggerYearValidation() {
            ValidatorValidate( document.getElementById(<%= AspxTools.JsGetClientIdString( ExpirationValidator ) %>) );
  }
  
  function f_displayCreditCardPanel() {
    <%= AspxTools.JsGetElementById(m_okBtn) %>.disabled = !<%= AspxTools.JsGetElementById(ICertify) %>.checked;
  
  /*
    var bChecked = <%= AspxTools.JsGetElementById(UseCreditCardPayment) %>.checked;
    document.getElementById("CreditCardPanel").style.display = bChecked ? "" : "none";
    if (bChecked) {
      f_enableCreditCardValidators(true);
      resize(640, 540);
    } else {
      f_enableCreditCardValidators(false);
      resize(300, 240);
    }
    */
  }
  function f_enableCreditCardValidators(bEnabled) {
    <%= AspxTools.JsGetElementById(BillingFirstNameValidator) %>.enabled = bEnabled;
    <%= AspxTools.JsGetElementById(BillingLastNameValidator) %>.enabled = bEnabled;
    <%= AspxTools.JsGetElementById(BillingStreetAddressValidator) %>.enabled = bEnabled;
    <%= AspxTools.JsGetElementById(BillingCardNumberValidator) %>.enabled = bEnabled;
    <%= AspxTools.JsGetElementById(BillingZipcodeValidator) %>.enabled = bEnabled;
    <%= AspxTools.JsGetElementById(ExpirationValidator) %>.enabled = bEnabled;
  }
  function f_validateExpiration(src, args) {

    var billingExpirationMonth = <%= AspxTools.JsGetElementById(BillingExpirationMonth) %>.value;
    var billingExpirationYear = <%= AspxTools.JsGetElementById(BillingExpirationYear) %>.value;
    
    args.IsValid = billingExpirationMonth != "-1" && billingExpirationYear != "-1";
  }
  function f_displayCVVHelp() {
    window.open('CVVHelp.htm', 'CVVHelp', 'width=400,height=320,menu=no,status=yes,location=no');
  }

  function OkClick() {
      var expirationMonthSelect = <%=AspxTools.JsGetElementById(BillingExpirationMonth)%>;
      var expirationMonth = expirationMonthSelect.options[expirationMonthSelect.selectedIndex].value;
      var yearSelect = <%=AspxTools.JsGetElementById(BillingExpirationYear)%>;
      var year = yearSelect.options[yearSelect.selectedIndex].value;
      var middleNameElement = document.getElementById("BillingMiddleName");
      var middleName = '';
      if(middleNameElement != null) {
          middleName = middleNameElement.value;
      }
      var data = {
          BillingFirstName: <%= AspxTools.JsGetElementById(BillingFirstName) %>.value,
          BillingLastName: <%= AspxTools.JsGetElementById(BillingLastName) %>.value,
          BillingCardNumber: <%= AspxTools.JsGetElementById(BillingCardNumber) %>.value,
          BillingCVV: <%= AspxTools.JsGetElementById(BillingCVV) %>.value,
          BillingStreetAddress: <%= AspxTools.JsGetElementById(BillingStreetAddress) %>.value,
          BillingCity: <%= AspxTools.JsGetElementById(BillingCity) %>.value,
          BillingState: <%= AspxTools.JsGetElementById(BillingState) %>.value,
          BillingZipcode: <%= AspxTools.JsGetElementById(BillingZipcode) %>.value,
          BillingExpirationMonth: expirationMonth,
          BillingExpirationYear: year,
          BillingMiddleName: middleName
      }

      var results = gService.card.call('ValidateCreditCardInfo', data);
      if(!results.error) {
          if (results.value["Success"].toLowerCase() === 'true') {
              var args = {};
              if(<%= AspxTools.JsGetElementById(IsLqbPopup) %>.value.toLowerCase() !== 'true') {
                  args = window.dialogArguments || {};
              }

              args.OK = true;
              args.BillingFirstName = results.value["BillingFirstName"];
              args.BillingLastName = results.value["BillingLastName"];
              args.BillingStreetAddress = results.value["BillingStreetAddress"];
              args.BillingCity = results.value["BillingCity"];
              args.BillingState = results.value["BillingState"];
              args.BillingZipcode = results.value["BillingZipcode"];
              args.BillingCardNumber = results.value["BillingCardNumber"];
              args.BillingExpirationMonth = results.value["BillingExpirationMonth"];
              args.BillingExpirationYear = results.value["BillingExpirationYear"];
              args.BillingCVV = results.value["BillingCVV"];
              args.BillingMiddleName = results.value["BillingMiddleName"];

              if(<%= AspxTools.JsGetElementById(IsLqbPopup) %>.value.toLowerCase() === 'true') {
                  parent.LQBPopup.Return(args);
              }
              else {
                  onClosePopup();
              }
          }
          else {
              <%= AspxTools.JsGetElementById(ErrorMessege) %>.innerText = results.value["Errors"];
          }
      }
      else {
          <%= AspxTools.JsGetElementById(ErrorMessege) %>.innerText = results.UserMessage; 
      }
  }

    function CloseClick() {
        if(<%= AspxTools.JsGetElementById(IsLqbPopup) %>.value.toLowerCase() !== 'true') {
            onClosePopup();
        }
        else {
            parent.LQBPopup.Return(null);
        }
    }
  //-->
  </script>
<h4 class="page-header">Login</h4>
<form id=LoginScreen method=post runat="server">
    <asp:HiddenField runat="server" ID="IsLqbPopup" />
<table class=FormTable id=Table1 cellSpacing=0 cellPadding=0 width="100%" border=0>
  <TBODY>

  <TR>
    <TD class=FieldLabel noWrap>&nbsp;</TD>
    <TD noWrap>&nbsp;</TD></TR>
  <tbody id="RegularLoginPanel" style="DISPLAY:none">
  <tr>
    <td class=FieldLabel noWrap>Account ID</td>
    <td noWrap><asp:textbox id=m_accountIdentifierTF runat="server"></asp:textbox></td></tr>
  <tr>
    <td class=FieldLabel noWrap>Login Name</td>
    <td noWrap><asp:textbox id=m_loginTF runat="server"></asp:textbox><IMG 
      src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/require_icon.gif")%>>
        <asp:requiredfieldvalidator id=LoginValidator runat="server" ControlToValidate="m_loginTF" Enabled="False">
            <img runat="server" src="../../images/error_icon.gif">
        </asp:requiredfieldvalidator></td></tr>
  <tr>
    <td class=FieldLabel noWrap>Password</td>
    <td noWrap><asp:textbox id=m_passwordTF runat="server" TextMode="Password"></asp:textbox><IMG 
      src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/require_icon.gif")%>>
        <asp:requiredfieldvalidator id=PasswordValidator runat="server" ControlToValidate="m_passwordTF" Enabled="False">
            <img runat="server" src="../../images/error_icon.gif">
        </asp:requiredfieldvalidator></td></tr>
  </tbody>
  <tbody id=MclInstantIDPanel style="DISPLAY: none">
  <tr>
    <td class=FieldLabel noWrap>Report ID</td>
    <td noWrap><asp:textbox id=m_reportIDTF runat="server"></asp:textbox></td></tr>
  <tr>
    <td class=FieldLabel noWrap>Instant View ID</td>
    <td noWrap><asp:textbox id=m_iviewIDTF runat="server"></asp:textbox></td></tr>
  </tbody>
  <tbody id=RememberLoginPanel style="DISPLAY:none">
  <tr>
    <td class=FieldLabel noWrap colSpan=2><asp:checkbox id=m_rememberLoginCB runat="server" Text="Remember login name"></asp:checkbox></td></tr>
  <tr>
    <td class=FieldLabel noWrap colSpan=2><asp:checkbox id=UseCreditCardPayment onclick=f_displayCreditCardPanel(); runat="server" text="Pay with credit card"></asp:checkbox></td></tr>
</tbody>    
  <tbody id=CreditCardPanel>
  <tr>
    <td class=FieldLabel noWrap colSpan=2>
      <table class=InsetBorder id=Table2 cellSpacing=0 cellPadding=0 border=0>
         <tr>
          <td class=FieldLabel noWrap colSpan=2 style="padding-bottom:1em">Credit 
            Card Billing Information</td></tr>           
          <tr>
          <td class=FieldLabel noWrap>First Name</td>
          <td noWrap><asp:textbox id=BillingFirstName runat="server"></asp:textbox><img 
            src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/require_icon.gif")%>>
              <asp:requiredfieldvalidator id=BillingFirstNameValidator runat="server" ControlToValidate="BillingFirstName">
                  <img runat="server" src="../../images/error_icon.gif">
              </asp:requiredfieldvalidator></td></tr>
          <tr id="MiddleNameRow" runat="server">
              <td class="FieldLabel" noWrap>Middle Name</td>
              <td noWrap>
                  <asp:TextBox ID="BillingMiddleName" runat="server"></asp:TextBox>
              </td>
          </tr>
        <tr>
          <td class=FieldLabel noWrap>Last Name</td>
          <td noWrap><asp:textbox id=BillingLastName runat="server"></asp:textbox><img 
            src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/require_icon.gif")%>>
              <asp:requiredfieldvalidator id=BillingLastNameValidator runat="server" ControlToValidate="BillingFirstName">
                  <img runat="server" src="../../images/error_icon.gif">
              </asp:requiredfieldvalidator></td></tr>
        <tr>
          <td class=FieldLabel noWrap>Street Address</td>
          <td noWrap><asp:textbox id=BillingStreetAddress runat="server" Width="230px"></asp:textbox><img 
            src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/require_icon.gif")%>>
              <asp:requiredfieldvalidator id=BillingStreetAddressValidator runat="server" ControlToValidate="BillingStreetAddress">
                  <img runat="server" src="../../images/error_icon.gif">
              </asp:requiredfieldvalidator></td></tr>
        <tr>
          <td noWrap></td>
          <td noWrap><asp:textbox id=BillingCity runat="server"></asp:textbox><ml:statedropdownlist id=BillingState runat="server"></ml:statedropdownlist><ml:zipcodetextbox id=BillingZipcode runat="server" preset="zipcode" width="50"></ml:zipcodetextbox>
              <asp:requiredfieldvalidator id=BillingZipcodeValidator runat="server" ControlToValidate="BillingZipcode">
                  <img runat="server" src="../../images/error_icon.gif">
              </asp:requiredfieldvalidator></td></tr>
        <tr>
          <td class=FieldLabel noWrap>Card Information</td>
          <td noWrap></td></tr>
        <tr>
          <td class=FieldLabel noWrap>Card Number</td>
          <td noWrap><asp:textbox id=BillingCardNumber runat="server"></asp:textbox><img 
            src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/require_icon.gif")%>>
              <asp:requiredfieldvalidator id=BillingCardNumberValidator runat="server" ControlToValidate="BillingCardNumber">
                  <img runat="server" src="../../images/error_icon.gif">
              </asp:requiredfieldvalidator></td></tr>
        <tr>
          <td class=FieldLabel noWrap>Expiration Date</td>
          <td noWrap><asp:dropdownlist id=BillingExpirationMonth runat="server" onchange="TriggerYearValidation()" onkeyup="TriggerYearValidation()">
<asp:ListItem Value="-1">&nbsp;</asp:ListItem>
<asp:ListItem Value="01">01</asp:ListItem>
<asp:ListItem Value="02">02</asp:ListItem>
<asp:ListItem Value="03">03</asp:ListItem>
<asp:ListItem Value="04">04</asp:ListItem>
<asp:ListItem Value="05">05</asp:ListItem>
<asp:ListItem Value="06">06</asp:ListItem>
<asp:ListItem Value="07">07</asp:ListItem>
<asp:ListItem Value="08">08</asp:ListItem>
<asp:ListItem Value="09">09</asp:ListItem>
<asp:ListItem Value="10">10</asp:ListItem>
<asp:ListItem Value="11">11</asp:ListItem>
<asp:ListItem Value="12">12</asp:ListItem>
</asp:dropdownlist>&nbsp;/ <asp:dropdownlist id=BillingExpirationYear runat="server">

</asp:dropdownlist><asp:customvalidator id=ExpirationValidator runat="server" ControlToValidate="BillingExpirationYear" ErrorMessage="Expiration Required" font-bold="True" ClientValidationFunction="f_validateExpiration"></asp:customvalidator></td></tr>
        <tr>
          <td class=FieldLabel noWrap>Card Verification 
            Value</td>
          <td noWrap><asp:textbox id=BillingCVV runat="server"></asp:textbox>(<A onclick=f_displayCVVHelp(); href="#" >What is this?</A>)</td></tr>
        <tr>
          <td class=FieldLabel colSpan=2><asp:checkbox id=ICertify onclick=f_displayCreditCardPanel(); runat="server" Text="I certify that the cardholder has authorized any related charges for the next 90 days to be applied to this card."></asp:checkbox></td></tr>
                          <tr >
                            <td align="center" colspan="2">
                                <ml:EncodedLabel runat="server" ID="ErrorMessege" Font-Bold="true" ForeColor="Red"  EnableViewState="false"></ml:EncodedLabel>
                            </td>
                        </tr>
          </table></td></tr></tbody>
  <tr>
    <td noWrap align=center colSpan=2>&nbsp;</td></tr>
  <tr>
    <td noWrap align=center colSpan=2><input type="button" id="m_okBtn" runat="server" value="  OK  " onclick="OkClick();" /><INPUT onclick="CloseClick();" type=button value=Cancel></td></tr></table></form><uc1:cmodaldlg id=CModalDlg1 runat="server"></uc1:cmodaldlg>
	
  </body>
</HTML>
