﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Conversions.Drive;
using System.Net;
using LendersOffice.Constants;
using System.Web.Services;
using LendersOffice.Security;
using System.IO;
using System.Xml.XPath;
using System.Xml.Xsl;
using LendersOffice.Conversions.Drive.Request;
using LendersOffice.Conversions.Drive.Response;
using System.Xml;
using System.Text;
using LendersOffice.Common;
using LendersOffice.Audit;
using System.Xml.Linq;
using DataAccess.FannieMae;

namespace LendersOfficeApp.newlos.Services
{
    public partial class DataVerifyDRIVE : LendersOfficeApp.newlos.BaseLoanPage
    {
        private E_InternalPhase? m_internalPhase;
        private DRIVEResponse m_response;
        private string m_responseXML;
        private string m_responseHTMLContent;
        protected CPageData m_dataLoan;
        private DriveSavedAuthentication m_auth;

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            
            this.PageID = "DataVerifyDRIVE";
            this.PageTitle = "DataVerify DRIVE";
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            GetLoginInfoFromDB();
            MergeUIAndDBLoginInfo();
            ShowOldResponse();
            m_internalPhase = E_InternalPhase.PreSubmit;
            timeout.Text = Math.Ceiling(ConstStage.DataVerifyDRIVETimeOutInMilliseconds / (double)60000).ToString();
        }
        private void ShowOldResponse()
        {
            m_dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(DataVerifyDRIVE));
            m_dataLoan.InitLoad();
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(m_dataLoan.sDriveXmlContent))
                {
                    m_responseHTMLContent = GetResponseHTMLFromResponseXML(m_dataLoan.sDriveXmlContent);
                    DisplayResult();
                }
                else
                {
                    m_phNoResponseOnFile.Visible = true;
                }
            }
        }


        protected void OnSubmitClick(object sender, EventArgs e)
        {
            SubmitToDrive();
            SaveLoginInfoToDB();
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            switch (m_internalPhase)
            {
                case E_InternalPhase.PreSubmit:
                    break;
                case E_InternalPhase.NullResponse:
                    HandleNullResponse();
                    break;
                case E_InternalPhase.NoResponse:
                    HandleNoResponse();
                    break;
                case E_InternalPhase.NoLoanList:
                    HandleNoLoanList();
                    break;
                case E_InternalPhase.Errors:
                    HandleErrors();
                    break;
                case E_InternalPhase.PickDoctype:
                    HandlePickDocType();
                    break;
                case E_InternalPhase.AllDoneNoUpload:
                    break;
                case E_InternalPhase.UploadToEDocs:
                    HandleUploadToEdocs();
                    break;
                default:
                    throw new UnhandledEnumException(m_internalPhase);
            }
            base.OnLoadComplete(e);
        }

        private void HandleUploadToEdocs()
        {
            DriveImporter.UploadReportToEDocs(EncodedEDoc.Value, (int)Broker.DriveDocTypeID, m_dataLoan.GetAppData(0).aAppId, LoanID);
            m_finishUploadEDoc.Visible = true;
        }

        private void HandlePickDocType()
        {
            m_phPickDocType.Visible = true;
        }

        private void HandleErrors()
        {
            m_phErrors.Visible = true;

            var errors = new List<Error>();
            errors.AddRange(m_response.Errors.ErrorsList);
            errors.AddRange(from l in m_response.LoanList
                            from e in l.Errors.ErrorList
                            select e);
            m_rErrors.DataSource = errors;
            m_rErrors.DataBind();
        }

        private void HandleNullResponse()
        {
            m_phNullResponse.Visible = true;
        }
        private void HandleNoResponse()
        {
            m_phNoResponse.Visible = true;
        }
        private void HandleNoLoanList()
        {
            m_phNoLoanList.Visible = true;
        }


        private void GetLoginInfoFromDB()
        {
            m_auth = DriveSavedAuthentication.Retrieve(BrokerUser);
        }

        private void MergeUIAndDBLoginInfo()
        {
            var logonBlank = string.IsNullOrEmpty(m_auth.UserName);
            var rememberedLogon = !logonBlank;

            if (!IsPostBack)
            {
                m_tbUserName.Text = m_auth.UserName;
                m_cbRememberLogon.Checked = rememberedLogon;
            }
            else
            {
                m_auth.UserName = m_tbUserName.Text;
                if (m_tbPassword.Text != ConstAppDavid.FakePasswordDisplay)
                {
                    m_auth.Password = m_tbPassword.Text;
                }
            }

            if (m_cbRememberLogon.Checked)
            {
                m_tbPassword.Attributes.Add("value", ConstAppDavid.FakePasswordDisplay);
            }
        }

        private void SaveLoginInfoToDB()
        {
            if (IsPostBack)
            {
                if (!m_cbRememberLogon.Checked)
                {
                    m_auth.UserName = "";
                    m_auth.Password = "";
                    m_tbUserName.Text = "";
                }
            }

            m_auth.Save();
        }
        private void DisplayResult()
        {
            m_phResults.Visible = true;
            m_phNoResponseOnFile.Visible = false;

            m_phResults.SetRenderMethodDelegate(new RenderMethod(delegate(HtmlTextWriter tw, Control container)
            {
                tw.Write(m_responseHTMLContent);
            }));
        }

        private void SubmitToDrive()
        {
            DRIVERequest request = DriveServer.CreateRequest(m_auth.UserName, m_auth.Password, LoanID);

            try
            {
                m_response = DriveServer.Submit(request, out m_responseXML);
            }
            catch (WebException e)
            {
                Tools.LogErrorWithCriticalTracking("DRIVE - Problem connecting to DRIVE server.", e);
                m_internalPhase = E_InternalPhase.NoResponse;
                return;
            }
            if (m_response == null)
            {
                m_internalPhase = E_InternalPhase.NullResponse;
                return;
            }

            if (m_response.Errors.ErrorsList.Count > 0 || m_response.LoanList.Any((l) => l.Errors.ErrorList.Count > 0))
            {
                m_internalPhase = E_InternalPhase.Errors;
                return;
            }
            if (m_response.LoanList.Count == 0)
            {
                m_internalPhase = E_InternalPhase.NoLoanList;
                return;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(LoanID, typeof(DataVerifyDRIVE));
            dataLoan.InitSave(sFileVersion);
            var responseLoan = m_response.LoanList.First();
            EncodedEDoc.Value = responseLoan.EncodedEDoc;

            DriveImporter.ImportConditions(responseLoan.Conditions.ConditionList, LoanID, BrokerID);

            dataLoan.ImportDVPinCodes(responseLoan.DVPinCodes.DVPinCode);

            m_responseHTMLContent = GetResponseHTMLFromResponseXML(m_responseXML);

            DisplayResult();

            // Audit log
            AbstractAuditItem auditItem = new DriveSubmissionAuditItem(PrincipalFactory.CurrentPrincipal, m_responseHTMLContent);
            AuditManager.RecordAudit(LoanID, auditItem);

            AbstractAuditItem xmlAuditItem = new DriveSubmissionXmlAuditItem(PrincipalFactory.CurrentPrincipal, m_responseXML);
            AuditManager.RecordAudit(LoanID, xmlAuditItem);

            dataLoan.sDriveXmlContent = m_responseXML;
            dataLoan.Save();

            if (false == BrokerUser.HasPermission(Permission.CanViewEDocs) || false == Broker.IsEDocsEnabled)
            {
                m_internalPhase = E_InternalPhase.AllDoneNoUpload;
                return;
            }

            if (null == Broker.DriveDocTypeID)
            {
                m_internalPhase = E_InternalPhase.PickDoctype;
                return;
            }

            m_internalPhase = E_InternalPhase.UploadToEDocs;
        }

        private string GetResponseHTMLFromResponseXML(string m_responseXML)
        {
            StringBuilder htmlStringBuilder = new StringBuilder();
            using (TextReader inputReader = new StringReader(m_responseXML))
            {
                using (TextWriter outputWriter = new StringWriter(htmlStringBuilder))
                {
                    XslTransformHelper.TransformFromEmbeddedResource("LendersOffice.ObjLib.Conversions.DRIVE.DriveSummaryHtml.xslt", inputReader, outputWriter, null);
                }
            }
            return htmlStringBuilder.ToString();
        }

        [WebMethod]
        public static void TriggerUpload(string EncodedEDoc, int docTypeID, string appID, string loanID, string brokerID)
        {
            var broker = LendersOffice.Admin.BrokerDB.RetrieveById(new Guid(brokerID));
            broker.DriveDocTypeID = docTypeID;
            broker.Save();
            DriveImporter.UploadReportToEDocs(EncodedEDoc, docTypeID, new Guid(appID), new Guid(loanID));
        }

        private enum E_InternalPhase
        {
            PreSubmit,
            NullResponse,
            NoResponse,
            NoLoanList,
            Errors,
            PickDoctype,
            AllDoneNoUpload,
            UploadToEDocs
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion
    }
}
