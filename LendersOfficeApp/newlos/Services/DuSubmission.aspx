<%@ Page language="c#" Codebehind="DuSubmission.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Services.DuSubmission" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <head runat="server">
    <title>DuSubmission</title>
<style>
.blockMask { Z-INDEX: 200; FILTER: alpha(opacity=30); BACKGROUND-COLOR: #333333; opacity: .3 }
.ErrorWindow { BORDER-RIGHT: #335ea8 5px inset; PADDING-RIGHT: 15px; BORDER-TOP: #335ea8 5px inset; PADDING-LEFT: 15px; Z-INDEX: 205; PADDING-BOTTOM: 15px; BORDER-LEFT: #335ea8 5px inset; PADDING-TOP: 15px; BORDER-BOTTOM: #335ea8 5px inset; BACKGROUND-COLOR: white; TEXT-ALIGN: center }
</style>
</head>
<body class=RightBackground style="MARGIN-LEFT: 0px" >
<script type="text/javascript">
<!--
var g_globallyUniqueIdentifier = "";
var g_pollingInterval = 10000; // 10 seconds
var g_nApps = <%= AspxTools.JsNumeric(m_nApps) %>;
var g_hasDuAutoLogin = <%= AspxTools.JsBool(m_hasDuAutoLogin) %>;
function f_submit() {
  f_displayWait(true);
  g_globallyUniqueIdentifier = "";
  f_submitImpl();
}

function f_submitImpl() {
  var args = new Object();
  args["LoanID"] = ML.sLId;
  args["FannieMaeMORNETUserID"] = <%= AspxTools.JsGetElementById(FannieMaeMORNETUserID) %>.value;
  args["FannieMaeMORNETPassword"] = <%= AspxTools.JsGetElementById(FannieMaeMORNETPassword) %>.value;
  args["GloballyUniqueIdentifier"] = g_globallyUniqueIdentifier;
    args["sDuLenderInstitutionId"] = <%= AspxTools.JsGetElementById(sDuLenderInstitutionId) %>.value;
  args["CraProviderName"] = <%= AspxTools.JsGetElementById(CraProviderName) %>.value;
  args["CraLoginName"] = <%= AspxTools.JsGetElementById(CraLoginName) %>.value;
  args["CraPassword"] = <%= AspxTools.JsGetElementById(CraPassword) %>.value;
  args["sDuCaseId"] = <%= AspxTools.JsGetElementById(sDuCaseId) %>.value;
  args["ImportLiabitiesFromCreditReport"] = <%=AspxTools.JsGetElementById(ImportLiabitiesFromCreditReport) %>.checked ? 'True' : 'False';
  args["CreditOption"] = <%= AspxTools.JsGetElementById(OrderNew) %>.checked ? "OrderNew" : "UseExisting";
  
  if (g_hasDuAutoLogin == false && (args["FannieMaeMORNETUserID"] == '' || args["FannieMaeMORNETPassword"] == ''))
  {
    f_displayWait(false);
  alert('User ID and password is required.');
  return false;
  }
  for (var i = 0; i < g_nApps; i++)
  {
    args["CreditReference" + i] = document.getElementById("CreditReference" + i).value;
  }
  
  var result = gService.loanedit.call("SubmitDU", args);
  if (!result.error) {
    switch (result.value["Status"]) {
      case "ERROR":
        f_displayErrorMessage(result.value["ErrorMessage"]);
        break;
      case "PROCESSING":
        g_globallyUniqueIdentifier = result.value["GloballyUniqueIdentifier"];
        setTimeout(f_submitImpl, g_pollingInterval);
        break;
      case "DONE":
      
        f_displayWait(false);
        <%= AspxTools.JsGetElementById(sDuCaseId) %>.value = result.value["sDuCaseId"];
        <%= AspxTools.JsGetElementById(sDuLenderInstitutionId) %>.value = result.value["sDuLenderInstitutionId"];
        break;
    }
  } else {
    var errMsg = 'Error.';
    if (null != result.UserMessage)
      errMsg = result.UserMessage;
      
      f_displayErrorMessage(errMsg);
      return false;
  }
  
}

function f_saveCaseExport() {
  var args = new Object();
  args["LoanID"] = ML.sLId;
  var result = gService.loanedit.call("SaveCaseExport", args);
  alert('Done');
}
function f_displayErrorMessage(errMsg) {
  f_displayWait(false);

		var panel = document.getElementById('DUErrorPanel');
		panel.style.position = "absolute";
		panel.style.left = "100px";
		panel.style.top = "100px";
		panel.style.width = "400px";
		panel.style.height = "150px";
		panel.style.display = "";
	      
		document.getElementById('ErrorMsg').innerHTML = errMsg;
		f_hideAllDropDown(true);

}
function f_closeErrorMsg() 
{
	document.getElementById('DUErrorPanel').style.display = 'none';
	f_hideAllDropDown(false);
}
function f_hideAllDropDown(bHide) 
{
  var coll = <%= AspxTools.JsGetElementById(CraProviderName) %>;
  coll.style.display = bHide ? 'none' : '';

}
function f_displayWait(bDisplay) 
{
f_hideAllDropDown(bDisplay);
var maskDiv = document.getElementById('MaskDiv');
maskDiv.style.display = bDisplay ? '' : 'none';
maskDiv.style.width = '100%';
maskDiv.style.height = '100%';
maskDiv.style.position = 'absolute';
maskDiv.style.left = '0px';
maskDiv.style.top = '0px';
    
var waitMessage = document.getElementById('WaitMessage');
waitMessage.style.display = bDisplay ? '' : 'none';
waitMessage.style.width = '100%';
waitMessage.style.height = '100%';
waitMessage.style.position = 'absolute';
waitMessage.style.left = '0px';
waitMessage.style.top = '0px';    
}
function f_displayDUFindings() {
  var report_url = <%= AspxTools.JsString(VirtualRoot) %> + '/newlos/services/ViewDUFindingFrame.aspx?loanid=' + ML.sLId;
	var win = openWindowWithArguments(report_url, "report", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=780,height=600") ;
	win.focus() ;
	
	return false;

}
var g_duWindow = null;
function f_launchDoDu(sDuCaseId) {
  g_duWindow = null;
	var report_url = <%= AspxTools.JsString(VirtualRoot) %> + '/newlos/services/LaunchDoDu.aspx?loanid=' + ML.sLId + '&sDuCaseId=' + sDuCaseId;
	g_duWindow = openWindowWithArguments(report_url, "report", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=850,height=600,scrollbars=yes") ;
	g_duWindow.focus() ;

  setTimeout(f_duWinUnload, 1000);
	return false;

}
function f_duWinUnload() {
  
  if (null != g_duWindow && g_duWindow.closed) {
    f_logoutDoDu();
    alert('DU Windows Close');
    return;
  }
    setTimeout(f_duWinUnload, 1000);
}
function f_logoutDoDu() {
  var args = new Object();
  args["IsDo"] = "False";
    args["FannieMaeMORNETUserID"] = <%= AspxTools.JsGetElementById(FannieMaeMORNETUserID) %>.value;
  args["FannieMaeMORNETPassword"] = <%= AspxTools.JsGetElementById(FannieMaeMORNETPassword) %>.value;
  args["sDuLenderInstitutionId"] = <%= AspxTools.JsGetElementById(sDuLenderInstitutionId) %>.value;
  var result = gService.loanedit.call("Logout", args);
}
//-->
</script>

<form id=DuSubmission method=post runat="server">
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  <tr>
    <td class=MainRightHeader>DU Submission</td></tr>
  <tr>
    <td>
            <table class="InsetBorder">
              <tr>
                <td class="FieldLabel">DU User Id</td>
                <td><asp:TextBox id=FannieMaeMORNETUserID runat="server" /></td>
              </tr>
              <tr>
                <td class="FieldLabel">DU Password</td>
                <td><asp:TextBox id=FannieMaeMORNETPassword runat="server" TextMode="Password" /></td>
              </tr>
              <tr>
                <td class="FieldLabel">DU Institution ID</td>
                <td><asp:TextBox ID="sDuLenderInstitutionId" runat="server" /></td>
              </tr>
        <TR>
          <TD class=FieldLabel>DU Casefile Id</TD>
          <TD><asp:TextBox id=sDuCaseId runat="server"></asp:TextBox></TD></TR>
          </table>
          <table class="InsetBorder">
        <TR>
          <TD class=FieldLabel colspan=2><asp:RadioButton id="OrderNew" runat="server" Text="Order new or reissue credit from credit provider" GroupName="CreditOption" Checked=true /><asp:RadioButton id="UseExisting" runat="server" Text="Use credit report from previous submission" GroupName="CreditOption"></asp:RadioButton></TD></TR>
              <tr>
                <td class="FieldLabel">Credit Provider</td>
                <td><asp:DropDownList id=CraProviderName runat="server" /></td>
              </tr>
              <tr>
                <td class="FieldLabel">CRA User Id</td>
                <td><asp:TextBox id=CraLoginName runat="server"/></td>
              </tr>
              <tr>
                <td class="FieldLabel">CRA Password</td>
                <td><asp:TextBox id=CraPassword runat="server" TextMode="Password"/></td>
              </tr>
                            <tr>
                <td colspan=2 class="FieldLabel"><asp:CheckBox ID="ImportLiabitiesFromCreditReport" runat="server" Text="Auto-populate the liabilities from the credit report to the 1003?" checked="true"/></td>
              </tr>

              <tr><td colspan=2><hr></td></tr>
        <TR>
          <TD colSpan=2 class=FieldLabel>Resubmit Credit 
            Report Id (Leave credit identifier blank to order new&nbsp; credit report)</TD></TR>
        <TR>
          <TD colSpan=2><ml:PassthroughLiteral id=ResubmitCreditHtml runat="server"></ml:PassthroughLiteral></TD></TR>
          </table><table>
              <tr>
              <td colspan=2><input type=button value="Submit" onclick="f_submit();" id=btnSubmit></td>
              </tr>
            </table></td></tr></table>
<div class=ErrorWindow id=DUErrorPanel style="DISPLAY: none">
<div id=ErrorMsg style="FONT-WEIGHT: bold; COLOR: red"></div><br>[<A onclick=f_closeErrorMsg(); href="#" >Close</A>] </div>            
<div class=blockMask id=MaskDiv style="DISPLAY: none"></div>
<div id=WaitMessage style="DISPLAY: none; Z-INDEX: 205">
<table height="100%" width="100%">
  <tr>
    <td vAlign=middle align=center>
      <div style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; FONT-WEIGHT: bold; Z-INDEX: 205; PADDING-BOTTOM: 30px; WIDTH: 250px; COLOR: black; PADDING-TOP: 30px; HEIGHT: 60px; BACKGROUND-COLOR: white">
      Submit to DU. Please wait ... <br><IMG height=10 src="../../images/status.gif" ></div></td></tr></table>
</div>            
            </form>
	
  </body>
</HTML>
