﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderMIPolicy.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.OrderMIPolicy" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Order MI Policy</title>
    <link href="../../css/stylesheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #orderQuoteButton {
            height: 2.25em;
            width: 18em;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        #orderQuoteButton > #orderQuoteWorkflowImage {
            margin: 0px 4px;
        }

        .FormTableSubheader {
            padding: 5px;
        }

        .width-60 {
            width: 60px;
        }

        .hidden {
            display: none;
        }
    </style>
</head>
<body class="EditBackground">
    <script type="text/javascript" language="javascript">
        $(window).on("load", function () {
            $('#_ReadOnly').val("True"); // Disable saving on this page.

            loadMasterPolicyNumber();

            $('body').on('change', '#MIProvider', function () {
                loadMasterPolicyNumber();
            });

            // The MI vendor only accepts whole percentages for coverage amounts.
            $('#MICoveragePercentage').keypress(function (event) {
                var percentRegex = /^[0-9%]$/i;
                return percentRegex.test(String.fromCharCode(event.which));
            });
        });

        function loadMasterPolicyNumber() {
            var miProvider = $('#MIProvider').val();
            if (miProvider === '') {
                return;
            }
            var args = {
                        VendorID: miProvider,
                        BranchID: $('#BranchId').val()
                       };
                       
            var result = gService.OrderMIPolicy.call("LoadMasterPolicyNumber", args);
            if (result && result.value) {
                $('#MasterPolicyNumber').val(result.value.Policy);
                $('#MasterPolicyNumber').trigger("change");
            }
        }

        function f_onOrderQuoteButton_Click() {
            var args = {
                LoanID: ML.sLId,
                AppID: ML.aAppId,
                VendorID: $('#MIProvider').val(),
                BranchID: $('#BranchId').val(),
                MIPremiumType: $('#MIPremiumType').val(),
                PremiumRefundability: $('#PremiumRefundability').val(),
                SplitPremiumMismoValue: $('#MISplitPremiumUpfrontPercentage').val(),
                RenewalOption: $('#RenewalOption').val(),
                PremiumAtClosing: $('#PremiumAtClosing').val(),
                MasterPolicyNumber: $('#MasterPolicyNumber').val(),
                MICoveragePercentage: $('#MICoveragePercentage').val(),
                UFMIPFinanced: $('#UFMIPFinanced').prop('checked'),
                RelocationLoan: $('#RelocationLoan').prop('checked')
            };

            var loadingPopup = SimplePopups.CreateLoadingPopup("Processing MI Order...", "Ordering Quote");
            LQBPopup.ShowElement($('<div>').append(loadingPopup), {
                width: 350,
                height: 200,
                elementClasses: 'FullWidthHeight',
                hideCloseButton: true
            });

            gService.OrderMIPolicy.callAsyncSimple("OrderQuote", args, 
                function (result) {
                    if (result.value.Status === 'Processing') {
                        var pollingIntervalInMilliseconds = parseInt(result.value.PollingIntervalInSeconds) * 1000;
                        var publicJobId = result.value.PublicJobId;
                        window.setTimeout(function () { PollForRequestResults(publicJobId, pollingIntervalInMilliseconds, 1); }, pollingIntervalInMilliseconds);
                    }
                    else {
                        HandleResults(result, null, null, null);
                    }
                },
                function (result) {
                    LQBPopup.Return(null);
                    alert('Error: System error. Please retry in a few minutes and contact your LendingQB system administrator if the error recurs.');
                });
        }

        function PollForRequestResults(publicJobId, pollingIntervalInMilliseconds, attemptCount) {
            var args = {
                PublicJobId: publicJobId,
                AttemptCount: attemptCount
            };

            gService.OrderMIPolicy.callAsyncBypassOverlay("PollForResults", args,
                function (result) {
                    HandleResults(result, publicJobId, pollingIntervalInMilliseconds, attemptCount);
                },
                function (result) {
                    LQBPopup.Return(null);
                    alert('Error: System error. Please retry in a few minutes and contact your LendingQB system administrator if the error recurs.');
                });
        }

        function HandleResults(result, publicJobId, pollingIntervalInMilliseconds, attemptCount) {
            if (result.value.Status === "Processing") {
                window.setTimeout(function () { PollForRequestResults(publicJobId, pollingIntervalInMilliseconds, ++attemptCount) }, pollingIntervalInMilliseconds);
            }
            else if (result.value.Status === "Error") {
                LQBPopup.Return(null);
                alert('Error: ' + result.value.Message);
            }
            else {
                LQBPopup.Return(null);
                if (result.value.DisplayEdocMessage === "True") {
                    alert(result.value.Message);
                }

                launchQuoteViewer(result.value.OrderNumber, result.value.TransactionID);
                location.href = location.href;
            }
        }

        if (!String.prototype.encodeHTML) {
            String.prototype.encodeHTML = function () {
                return this.replace(/&/g, '&amp;')
                           .replace(/</g, '&lt;')
                           .replace(/>/g, '&gt;')
                           .replace(/"/g, '&quot;')
                           .replace(/'/g, '&apos;');
            };
        }

        function getOrderXml(quote, orderNumber, transactionId) {
            var args = {
                LoanID: ML.sLId,
                AppID: ML.aAppId,
                VendorID: $('#MIProvider').val(),
                BranchID: $('#BranchId').val(),
                MIPremiumType: $('#MIPremiumType').val(),
                SplitPremiumMismoValue: $('#MISplitPremiumUpfrontPercentage').val(),
                PremiumRefundability: $('#PremiumRefundability').val(),
                RenewalOption: $('#RenewalOption').val(),
                PremiumAtClosing: $('#PremiumAtClosing').val(),
                MasterPolicyNumber: $('#MasterPolicyNumber').val(),
                MICoveragePercentage: $('#MICoveragePercentage').val(),
                UFMIPFinanced: $('#UFMIPFinanced').prop('checked'),
                RelocationLoan: $('#RelocationLoan').prop('checked'),
                Quote: quote,
                OrderNumber: orderNumber,
                TransactionID: transactionId
            };

            var result = gService.OrderMIPolicy.call("GetOrderXML", args);

            if (result.error) {
                alert("Error: " + result.error);
            }
            else if (result.value.Error) {
                alert(result.value.Error);
            }
            else {
                win = window.open("", "MI Order XML", "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes");
                win.document.body.innerHTML = "<pre>" + result.value.OrderXML.encodeHTML() + "</pre>";
                win.focus();
            }
        }

        function launchQuoteViewer(orderNumber, transactionID) {
            var args = {
                LoanID: ML.sLId,
                AppID: ML.aAppId,
                OrderNumber: orderNumber,
                TransactionID: transactionID
            };

            var result = gService.OrderMIPolicy.call("GetQuoteViewerURL", args);
            
            if (!result.error) {
                if (result.value.Status === "OK") {
                    window.open(result.value.URL, '_blank', 'width=800px, height=650px, top=100px, left=100px, resizable, scrollbars=yes');
                }
            }
        }
        
        function f_display_message(message) {
            alert(message);
        }

        function refreshOnOrder() {
            location.href = location.href;
        }
    </script>
    <form id="form1" runat="server">
        <asp:HiddenField ID="BranchId" runat="server" />
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div>
    <div class="MainRightHeader">
        Order MI Policy
    </div>
    <table cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td valign="top">
                <table id="OrderOptions" class="InsetBorder" cellspacing="0" cellpadding="4" width="100%" border="0" style="padding: 5px">
                    <tr>
                        <td class="FormTableSubheader">Mortgage Insurance Order Options</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel runat="server">
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="MIProvider" />
                                    <asp:AsyncPostBackTrigger ControlID="MasterPolicyNumber" />
                                    <asp:AsyncPostBackTrigger ControlID="MIPremiumType" />
                                    <asp:AsyncPostBackTrigger ControlID="MISplitPremiumUpfrontPercentage" />
                                    <asp:AsyncPostBackTrigger ControlID="MICoveragePercentage" />
                                    <asp:AsyncPostBackTrigger ControlID="PremiumRefundability" />
                                    <asp:AsyncPostBackTrigger ControlID="RenewalOption" />
                                    <asp:AsyncPostBackTrigger ControlID="PremiumAtClosing" />
                                </Triggers>
                                <ContentTemplate>
                                    <table id="OrderOptionsMenu" cellspacing="6" cellpadding="0" border="0">
                                    <tr>
                                        <td class="FieldLabel">MI Provider</td>
                                        <td>
                                            <asp:DropDownList ID="MIProvider" runat="server" OnSelectedIndexChanged="MIProvider_OnSelectedIndexChanged" AutoPostBack="true" />
                                            <img id="MIProviderRequired" src="../../images/require_icon.gif" alt="Required" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">Master Policy Number</td>
                                        <td>
                                            <asp:TextBox ID="MasterPolicyNumber" runat="server" Width="180px" OnTextChanged="RunValidation" AutoPostBack="true" />
                                            <img id="MasterPolicyNumberRequired" src="../../images/require_icon.gif" alt="Required" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">MI Premium Type</td>
                                        <td>
                                            <asp:DropDownList ID="MIPremiumType" runat="server" class="PremiumType" OnSelectedIndexChanged="MIPremiumType_OnSelectedIndexChanged" AutoPostBack="true" />
                                            <img id="MIPremiumTypeRequired" src="../../images/require_icon.gif" alt="Required" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel" id="UpfrontLabelCell" runat="server">Upfront Premium %</td>
                                        <td id="UpfrontPercentageCell" runat="server">
                                            <asp:DropDownList ID="MISplitPremiumUpfrontPercentage" CssClass="width-60" runat="server" OnSelectedIndexChanged="RunValidation" AutoPostBack="true" />
                                            <img id="MISplitPremiumUpfrontPercentageRequired" runat="server" src="../../images/require_icon.gif" alt="Required" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">MI Coverage %</td>
                                        <td>
                                            <asp:TextBox ID="MICoveragePercentage" runat="server" Width="56px" OnTextChanged="RunValidation" AutoPostBack="true" />
                                            <img id="MICoveragePercentageRequired" src="../../images/require_icon.gif" alt="Required" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">Premium Refundability</td>
                                        <td>
                                            <asp:DropDownList ID="PremiumRefundability" runat="server" Style="width: 140px;" OnSelectedIndexChanged="RunValidation" AutoPostBack="true" />
                                            <img id="PremiumRefundabilityRequired" runat="server" src="../../images/require_icon.gif" alt="Required" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">Renewal Option</td>
                                        <td>
                                            <asp:DropDownList ID="RenewalOption" runat="server" OnSelectedIndexChanged="RunValidation" AutoPostBack="true" />
                                            <img id="RenewalOptionRequired" runat="server" src="../../images/require_icon.gif" alt="Required" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">Premium at Closing</td>
                                        <td>
                                            <asp:DropDownList ID="PremiumAtClosing" runat="server" OnSelectedIndexChanged="PremiumAtClosing_OnSelectedIndexChanged" AutoPostBack="true" />
                                            <img id="PremiumAtClosingRequired" runat="server" src="../../images/require_icon.gif" alt="Required" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">UFMIP Financed</td>
                                        <td>
                                            <asp:CheckBox ID="UFMIPFinanced" runat="server" Text="Yes" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">Relocation Loan</td>
                                        <td>
                                            <asp:CheckBox ID="RelocationLoan" runat="server" Text="Yes" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <button ID="orderQuoteButton" runat="server" onclick="f_onOrderQuoteButton_Click();"><div id="orderQuoteWorkflowImage" runat="server"><img src="../../images/warning25x25.png"/> </div>Get MI Quote</button>
                                        </td>
                                    </tr>
                                    <tr id="InternalTools" runat="server">
                                        <td></td>
                                        <td>
                                            <div style="display: inline-block">
                                                <asp:LinkButton ID="viewQuoteXml" runat="server" OnClientClick="getOrderXml(true, null, null); return false;">view quote request xml</asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
                    <td width="6px"></td>
            <td valign="top">
                        <table id="PolicyInformation" class="InsetBorder" cellspacing="0" cellpadding="4" border="0" style="padding: 5px">
                    <tr>
                        <td class="FormTableSubheader">Mortgage Insurance Policy Information</td>
                    </tr>
                    <tr>
                        <td>
                                    <table id="PolicyInfoTable" cellspacing="6" cellpadding="0" border="0">
                                <tr>
                                            <td class="FieldLabel">MI Provider
                                    </td>
                                    <td>
                                        <asp:TextBox ID="sMiCompanyNmT" runat="server" Width="180px" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                            <td class="FieldLabel">MI Certificate ID
                                    </td>
                                    <td>
                                        <asp:TextBox ID="sMiCertId" runat="server" Width="180px" ReadOnly="true"></asp:TextBox>                        
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel">Conv Loan PMI Type</td>
                                    <td>
                                        <asp:TextBox ID="sProdConvMIOptionT" runat="server" Width="180px" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="PolicySplitPremiumUpfrontPercentageSection">
                                    <td class="FieldLabel">Upfront Premium %</td>
                                    <td>
                                        <asp:TextBox ID="sFfUfmipR" runat="server" CssClass="width-60" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel">MI Coverage %
                                    </td>
                                    <td>
                                        <asp:TextBox ID="sMiLenderPaidCoverage" runat="server" CssClass="width-60" ReadOnly="true"></asp:TextBox>                        
                                    </td>
                                </tr>
                                <tr>
                                            <td class="FieldLabel">Loan Type
                                    </td>
                                    <td>
                                        <asp:TextBox ID="sLT" runat="server" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                            <td class="FieldLabel">LTV / CLTV
                                    </td>
                                    <td>
                                        <asp:TextBox ID="sLtvR" runat="server" Width="56px" ReadOnly="true"></asp:TextBox>&nbsp;/&nbsp;
                                        <asp:TextBox ID="sCltvR" runat="server" Width="56px" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                            <td class="FieldLabel">Employee Loan
                                    </td>
                                    <td>
                                                <label><asp:CheckBox ID="sIsEmployeeLoan" runat="server" Enabled="false" />Yes</label>
                                    </td>
                                </tr>
                                <tr>
                                            <td class="FieldLabel">UFMIP Financed
                                    </td>
                                    <td>
                                                <label><asp:CheckBox ID="sFfUfMipIsBeingFinanced" runat="server" Enabled="false" />Yes</label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <div class="FormTableSubheader">MI Policy Orders on File</div>
    <ml:CommonDataGrid ID="PolicyOrders" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="DataGrid">
                <AlternatingItemStyle CssClass="GridAlternatingItem" />
                <ItemStyle CssClass="GridItem" />
                <HeaderStyle CssClass="GridHeader" />
        <Columns>
            <asp:TemplateColumn HeaderText="MI Provider">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="120px"></ItemStyle>
                <ItemTemplate>
                    <%# AspxTools.HtmlString(GetVendorName(DataBinder.Eval(Container.DataItem, "VendorID")))%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="MasterPolicyNumber" HeaderText="Master Policy #" />
            <asp:BoundColumn DataField="CertificateNumber" HeaderText="Certificate #" />
            <asp:BoundColumn DataField="OrderedDate" HeaderText="Ordered Date" DataFormatString="{0:MM/dd/yyyy h:mm tt}" />
            <asp:BoundColumn DataField="CertExpirationDate" HeaderText="Expiration Date" DataFormatString="{0:d}" />
            <asp:TemplateColumn HeaderText="Request Type">
                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
                <ItemTemplate>
                    <ml:EncodedLiteral ID="DelegationType" runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Decision Type">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
                <ItemTemplate>
                    <%# AspxTools.HtmlString(GetDecision(DataBinder.Eval(Container.DataItem, "Decision")))%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="Status" HeaderText="Policy Status" />
            <asp:BoundColumn DataField="VendorMessage" HeaderText="Message" />
        </Columns>
    </ml:CommonDataGrid>
            <div style="width: 100px; margin-left: auto; margin-right: auto; font-weight: bold; white-space: nowrap;" id="NoPolicyOrdersLabel" runat="server">No policy orders on file.</div>
    </div>
        <br />
        <br />
    <div class="FormTableSubheader">MI Quotes on File</div>
    <ml:CommonDataGrid ID="Quotes" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="DataGrid">
            <AlternatingItemStyle CssClass="GridAlternatingItem" />
            <ItemStyle CssClass="GridItem" />
            <HeaderStyle CssClass="GridHeader" />
        <Columns>
            <asp:TemplateColumn HeaderText="">
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="80px"></ItemStyle>
                <ItemTemplate>
                        <div>
                            <ml:PassthroughLiteral ID="QuoteResponseViewerLink" runat="server" /></div>
                        <div>
                            <asp:LinkButton ID="PolicyRequestXmlLink" runat="server" Text="policy request xml" /></div>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="MI Provider">
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="120px"></ItemStyle>
                <ItemTemplate>
                    <%# AspxTools.HtmlString(GetVendorName(DataBinder.Eval(Container.DataItem, "VendorID")))%>
                </ItemTemplate>
            </asp:TemplateColumn>
                <asp:BoundColumn DataField="QuoteNumber" HeaderText="Quote ID" ItemStyle-Width="80px" />
            <asp:TemplateColumn HeaderText="MI Premium Type">
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="200px"></ItemStyle>
                <ItemTemplate>
                    <%# AspxTools.HtmlString(GetPremiumType(DataBinder.Eval(Container.DataItem, "PremiumType")))%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Split Premium Upfront %">
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="60px"></ItemStyle>
                <ItemTemplate>
                    <%# AspxTools.HtmlString(GetUpfrontPercentage(DataBinder.Eval(Container.DataItem, "SplitPremiumUpfrontPercent"), DataBinder.Eval(Container.DataItem, "PremiumType")))%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="CoveragePercent" HeaderText="MI Coverage %" DataFormatString="{0:F4}%" ItemStyle-Width="80px" />
            <asp:TemplateColumn HeaderText="Premium Refundability">
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="120px"></ItemStyle>
                <ItemTemplate>
                    <%# AspxTools.HtmlString(GetPremiumRefundability(DataBinder.Eval(Container.DataItem, "Refundability")))%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Renewal Option">
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="100px"></ItemStyle>
                <ItemTemplate>
                    <%# AspxTools.HtmlString(GetRenewalType(DataBinder.Eval(Container.DataItem, "RenewalType")))%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Premium At Closing">
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="120px"></ItemStyle>
                <ItemTemplate>
                    <%# AspxTools.HtmlString(GetPremiumAtClosing(DataBinder.Eval(Container.DataItem, "PremiumAtClosing")))%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="UFMIP Financed">
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="100px"></ItemStyle>
                <ItemTemplate>
                    <%# AspxTools.HtmlString(DisplayYesNo(DataBinder.Eval(Container.DataItem, "UfmipFinanced")))%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="OrderedDate" HeaderText="Quote Date" DataFormatString="{0:MM/dd/yyyy h:mm tt}" />
        </Columns>
    </ml:CommonDataGrid>
        <div style="width: 100px; margin-left: auto; margin-right: auto; font-weight: bold; white-space: nowrap;" id="NoQuotesLabel" runat="server">No quotes on file.</div>
    <br />
    </form>
</body>
</html>
