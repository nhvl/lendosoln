﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderAppraisalEmailView.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.OrderAppraisalEmailView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>View Email</title>
    <link href="../../css/stylesheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
    #EmailContainer
    {
        text-align: center;
    }
    textarea
    {
        text-align: center;
    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h2 class="MainRightHeader">
            View Email
        </h2>
        <div id="EmailContainer">
            <div>
                <h5>From:<asp:TextBox id="from" runat="server" ReadOnly="true" BorderStyle="None"></asp:TextBox></h5>
                <h5>To:<asp:TextBox id="to" runat="server" ReadOnly="true" BorderStyle="None"></asp:TextBox></h5>
            </div>
            <div>
                <h5>Subject:</h5>
            </div>
            <div>
                <h5>Message:</h5>
                <asp:TextBox id="message" runat="server" ReadOnly="true" TextMode="MultiLine" BorderStyle="None"></asp:TextBox>
                
            </div>
            <input type="button" value="Close" onclick="onClosePopup()" />
        </div>
    </div>
    </form>
</body>
<script type="text/javascript">
    var args = getModalArgs();
    alert(window.dialogArguments);
    if (args && args.EmailFrom) {
        document.getElementById("from").nodeValue = args.EmailFrom;
    }
    if (args && args.EmailTo) {
        document.getElementById("to").nodeValue = args.EmailFrom;
    }
    if (args && args.EmailSubject) {
        document.getElementById("subject").nodeValue = args.EmailFrom;
    }
    if (args && args.EmailMessage) {
        document.getElementById("message").nodeValue = args.EmailFrom;
    }
</script>
</html>