﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="SeamlessDocumentGenerationOptions.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.SeamlessDocumentGenerationOptions" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
 <head id="Head1" runat="server">
    <title>Document Generation Options</title>
    <style type="text/css">
      .ErrorMessage {
          padding-left: 5px;
          font-weight: bold;
          padding-bottom: 5px;
          color: red;
          padding-top: 5px;
          overflow: auto;
          width: 380px;
          height: 201px;
      }

      .ErrorMessage pre {
          font-family: Arial, Helvetica, sans-serif;
      }

      .WaitLabel {
          font-weight: bold;
          font-size: 18pt;
          cursor: wait;
      }

      .LeftPadding {
          padding-left: 5px;
      }

      .DoubleLeftPadding {
          padding-left: 10px;
      }

      .PickFormat {
          display: none;
      }

      .NoWrap {
          white-space: nowrap;
      }

      .Indent-Level-1 {
          margin-left: 10px;
      }

      .Indent-Level-2 {
          margin-left: 20px;
      }

      .Hidden {
          display: none;
      }

      div.inset {
          width:600px;
          border: inset;
      }

      span.Action {
          color: blue;
          text-decoration: underline;
          cursor: pointer;
      }

      #SelectForms {
          display: inline-block;
          margin-left: 5px;
      }

      #creatingDocumentsSpan {
          display: none;
      }

      #divDocMasterToolTip {
          z-index: 900; 
          border: black 3px solid;
          padding: 5px;
          display: none; 
          width: 300px; 
          position: absolute; 
          height: 50px; 
          background-color: whitesmoke;
      }

      #NotifyOtherAdditionalAgentsTextBox {
          width: 300px;
      }
    </style>
    <base target="_self" />
</head>
<body class="RightBackground" style="MARGIN-LEFT:0px">
    <form id="SeamlessDocumentGenerationOptions" method="post" runat="server">
    <table id="MainTable" cellspacing="2" cellpadding="0" border="0" width="100%" class="FieldLabel">
        <tbody>
            <tr class="MainRightHeader">
              <td class="NoWrap" >Generate Documents</td>
            </tr>
        </tbody>
    </table>
        <div id="divDocMasterToolTip">
        DocMaster documents will need to be opened using DocMagic's DocMaster software.  Note: DocMaster documents do not save to EDocs automatically.
        <br />[ <a href="#" onclick="Modal.Hide(); return false">Close</a> ]
    </div>
        <div class="inset">
            <table width="100%" id="Options">      
            <tbody>
                <tr class="ElectronicDelivery">
                    <td class="FormTableSubheader" colspan="4">Electronic Delivery</td>
                </tr>
                <tr class="ElectronicDelivery">
                  <td class="LeftPadding NoWrap" width="150" ><asp:CheckBox ID="m_cbSendEDisclosures" runat="server" Text="Send eDisclosures" OnClick="onDeliveryOptionsClicked(); onElectronicDeliveryOptionClicked(); return true" /></td>
                  <td class="NoWrap"></td>
                </tr>
                <asp:PlaceHolder ID="WillECloseSection" runat="server">
                    <tr class="ElectronicDelivery">
                        <td class="LeftPadding NoWrap" width="150">
                            <asp:CheckBox ID="WillEClose" runat="server" Text="Will eClose" OnClick="onDeliveryOptionsClicked(); onElectronicDeliveryOptionClicked(); return true" />
                        </td>
                    </tr>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="m_phAllowOnlineSigning" runat="server">
                    <tr class="ElectronicDelivery" id="AllowOnlineSigning">
                      <td class="LeftPadding NoWrap" width="150">
                          <asp:CheckBox ID="m_cbAllowOnlineSigning" runat="server" Text="Allow Online Signing"/>
                      </td>
                      <td class="NoWrap"></td>
                    </tr>
                </asp:PlaceHolder>
                <tr class="ElectronicDelivery">
                    <td class="LeftPadding NoWrap" colspan="4">
                        <asp:CheckBox ID="cbNotifyAdditionalAgents" runat="server" CssClass="Indent-Level-1" Text="Notify Additional Agents"/>
                        <div id="NotifyAdditionalAgentsSelection" class="Hidden">
                            <asp:Repeater ID="AdditionalAgentsRepeater" runat="server" OnItemDataBound="AdditionalAgentsRepeater_ItemDataBound">
                                <ItemTemplate>
                                    <div class="LeftPadding NoWrap Indent-Level-2">
                                        <asp:CheckBox runat="server" ID="NotifyAdditionalAgentsCheckbox"/>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <div class="LeftPadding NoWrap Indent-Level-2">
                                <asp:CheckBox runat="server" ID="NotifyOtherAdditionalAgentsCheckbox" Text="Other"/>
                                <asp:TextBox runat="server" ID="NotifyOtherAdditionalAgentsTextBox"></asp:TextBox> (Separate by semicolons)
                            </div>
                        </div>
                    </td>
                    <td class="NoWrap"></td>
                </tr>
                <tr class="EmailOptions">
                    <td class="FormTableSubheader" colspan="4">Email Options</td>
                </tr>
                <tr class="EmailOptions">
                    <td width="150"><asp:CheckBox ID="m_cbEmailDocuments" runat="server" Text="Email Documents To" OnClick="onDeliveryOptionsClicked(); return true" /></td>
                    <td colspan="2" width="300"><asp:TextBox ID="m_tbEmailTo" runat="server" Width="300"></asp:TextBox></td>
                    <td>
                        <uc:CFM ID="CFM" runat="server" IsAllowLockableFeature="false" DisableAddTo="true" />
                    </td>
                    <td>
                        <asp:CustomValidator ID="m_rfvEmailTo" runat="server" ClientValidationFunction="isEmailValid"><IMG runat="server" src="../../images/require_icon.gif" > </asp:CustomValidator>
                    </td>
                </tr>
                <tr class="EmailOptions" id="RequirePassword">
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="m_cbRequirePassword" runat="server" Text="Require Password"/></td>
                    <td width="150"><asp:DropDownList ID="m_ddlRequirePasswordType" runat="server" Width="140"></asp:DropDownList></td>
                    <td width="150" align="right"><asp:TextBox ID="m_tbPassword" runat="server" Width="140"></asp:TextBox></td>
                    <td><asp:RequiredFieldValidator ID="m_rfvPassword" runat="server" ControlToValidate="m_tbPassword"><IMG runat="server" src="../../images/require_icon.gif" > </asp:RequiredFieldValidator></td>
                </tr>
                <tr class="EmailOptions" id="NotifyOnRetrieval">
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="m_cbNotifyOnRetrieval" runat="server" Text="Notify on Retrieval"/></td>
                </tr>
                <tr class="PickFormat">
                    <td class="FormTableSubheader" colspan="4">Document Format</td>
                </tr>
                <tr class="PickFormat">
                    <td colspan="3">
                        <asp:RadioButton ID="m_rbDocumentFormatPDF" Text="PDF" GroupName="DocumentFormatRBGroup" runat="server" Checked="True"/>
                        <asp:RadioButton ID="m_rbDocumentFormatDocMaster" Text="DocMaster" GroupName="DocumentFormatRBGroup" runat="server" />
                        <a href="#" onclick="Modal.ShowPopup('divDocMasterToolTip', null, event); return false">?</a>
                    </td>
                </tr>
                <tr class="PickForms">
                    <td class="FormTableSubheader" colspan="4">Select Individual Documents</td>
                </tr>
                <tr class="PickForms">
                    <td colspan="3">
                        <input id="CompletePackage" type="radio" name="forms" value="complete" checked="checked" />Generate Complete Package <br />
                        <input id="PartialPackage" type="radio" name="forms" value="specific" />Generate Specific Forms <span id="SelectForms">select forms</span>
                    </td>
                </tr>
                <tr class="AdditionalServices">
                    <td class="FormTableSubheader" colspan="4">Additional Services</td>
                </tr>
                <tr class="AdditionalServices" id="ManualFulfillment">
                    <td colspan=3><asp:CheckBox ID="m_cbDSIPrintAndDeliver" runat="server" Text="Delivery" OnClick="onDeliveryOptionsClicked(); return true" /><span style="color:Red" class="ExtraChargeWarning">*</span>&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="m_bDeliveryInfo" runat="server" Text="Delivery Information"  Width="200" OnClientClick="onDeliveryInfoClick(); return false;"/></td>
                </tr>
                <tr class="AdditionalServices" id="MERSRegistration">
                    <td><asp:CheckBox ID="m_cbMERSRegistration" runat="server" Text="MERS Registration"  OnClick="onDeliveryOptionsClicked(); return true" /></td>
                </tr>
            
            </tbody>          
            </table>
        </div>

        <table>
            <tbody>
            <tr class="ExtraChargeWarning">
                <td colspan=3>
                    <span style="color:Red">* This feature will incur an additional charge</span>
                </td>
            </tr>
                <tr id="buttonsRow">
                    <td width="50%"></td>
                    <td width="50">
                        <input type="button" value="Cancel" onclick="onClosePopup();" /></td>
                    <td>
                        <asp:Button runat="server" ID="m_bCreateDocuments" Text="Create Documents" OnClientClick="onBCreateDocumentsClicked('m_bCreateDocumentsPostbackButton'); return false;" />
                        <asp:Button runat="server" ID="m_bCreateDocumentsPostbackButton" CssClass="Hidden" OnClick="bCreateDocumentsClickHandler" />
                        <input type="button" value="Create Documents WITH WARNING" onclick="generateDocWithValidationWarning();" runat="server" id="btnCreateDocumentsWithWarning" visible="false" />
                    </td>
                    <td>
                        <asp:Button runat="server" ID="PreviewDocumentsButton" Text="Preview Documents" OnClientClick="onBCreateDocumentsClicked('PreviewDocumentsPostbackButton'); return false;"  />
                        <asp:Button runat="server" ID="PreviewDocumentsPostbackButton" CssClass="Hidden" OnClick="bCreateDocumentsClickHandler" />
                        <span id="creatingDocumentsSpan" style="color: Red" class="NoWrap">Please wait while creating...</span>
                    </td>
                    <td width="50%"></td>
                </tr>
            </tbody>
        </table>        
        
        <span id="Error" class="ErrorMessage">
        <asp:PlaceHolder runat="server" ID="FatalErrors" Visible="false">
            <asp:Repeater runat="server" ID="FatalErrorRepeeater">
                <HeaderTemplate>
                    <ul>
                </HeaderTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
                <ItemTemplate>
                    <li><%# AspxTools.HtmlString(Container.DataItem.ToString()) %></li>
                </ItemTemplate>    
            </asp:Repeater>
        </asp:PlaceHolder>
        
        <asp:PlaceHolder runat="server" ID="ESignConfirmation" Visible="false">
            <br />
            <br />
            Your <%=AspxTools.HtmlString(Package.Value) %> Package has been sent to the borrower for ESigning.
            When the documents are completed they will be returned to EDocs and a notification email will be sent to you.
        </asp:PlaceHolder>
        </span>
        
        <asp:HiddenField ID="PollingId" runat="server"/>
        <asp:HiddenField ID="Attention" runat="server"/>
        <asp:HiddenField ID="Name" runat="server" />
        <asp:HiddenField ID="Street" runat="server"/>
        <asp:HiddenField ID="City" runat="server" />
        <asp:HiddenField ID="State" runat="server" />
        <asp:HiddenField ID="Zip" runat="server" />
        <asp:HiddenField ID="Phone" runat="server" />
        <asp:HiddenField ID="DSINotes" runat="server" />
        
        <!-- // SK  broker specific options. -->
        <asp:HiddenField ID="BName" runat="server" />
        <asp:HiddenField ID="BStreet" runat="server"/>
        <asp:HiddenField ID="BCity" runat="server" />
        <asp:HiddenField ID="BState" runat="server" />
        <asp:HiddenField ID="BZip" runat="server" />
        <asp:HiddenField ID="BPhone" runat="server" /> 
        
        <asp:HiddenField ID="Package" runat="server" />
        <asp:HiddenField ID="Program" runat="server" />
        
        <asp:HiddenField ID="AllowEPortalService" runat="server"/>
        <asp:HiddenField ID="DisableESign" runat="server" />
        
        <asp:HiddenField ID="Pappid" runat="server" />
        <asp:HiddenField ID="sLNm" runat="server" />
        <asp:HiddenField ID="Websheet" runat="server" />
        
        <asp:HiddenField ID="AlertMessage" runat="server" />
        <asp:HiddenField ID="HideButtons" runat="server" Value="false" />
        <asp:HiddenField ID="SelectedForms" runat="server"/>
        <asp:HiddenField ID="DownloadDocs" runat="server" />
    </form>

    <script type="text/javascript">
        
        jQuery(function($){
            var args = getModalArgs();
            if(args)
            {
                function refreshTreeview() { 
                    try  {  
                        args.treeview.location.reload();
                    }
                    catch (e) {}
                }

                if($('#RefreshParent').length)
                {   
                    if(args.body && args.body.updateDisclosedAPR) 
                    {   
                        args.body.updateDisclosedAPR();
                    }

                    if (args.treeview)
                    {
                        window.onbeforeunload = refreshTreeview;
                    }    
                }
                
                if($('#RefreshTreeview').length)
                {   
                    if(args.treeview) 
                    {
                        refreshTreeview();
                    }
                }
            }
            
            if($('#Autoclose').length)
            {
                //Give the treeview some time to reload, then close our window.
                window.setTimeout(function()
                { 
                    try
                    {
                        onClosePopup();
                    } 
                    catch(e){}
                }, 1000);
            }

            $('#cbNotifyAdditionalAgents').change(function() {
                if ($(this).prop('checked'))
                {
                    $('#NotifyAdditionalAgentsSelection').slideDown();
                }
                else
                {
                    $('#NotifyAdditionalAgentsSelection').slideUp();
                }
            });

            $('#NotifyOtherAdditionalAgentsCheckbox').change(function() {
                $('#NotifyOtherAdditionalAgentsTextBox').prop('readOnly', !$(this).prop('checked'));
            });
           
            //Settings and Permissions get snuck in by the codebehind.
            window.Settings = window.Settings || null;
            window.Permissions = window.Permissions || null;
            applySettings(Settings);
            applyPermissions(Permissions);
            if (<%= AspxTools.JsBool(this.VendorIsLegacyDocMagic) %>) {
                if (<%= AspxTools.JsBool(!this.Broker.EnableDsiPrintAndDeliverOption) %>) {
                    $('#ManualFulfillment').hide();
                    $('.ExtraChargeWarning').hide();
                }

                $('#WillECloseSection').hide();
            }
           
            function applyPermissions(perms){
                //If for some reason we didn't get a permissions object, set this up anyway so it's still usable
                if(!perms || perms.EnableIndividualDocumentGeneration){
                    var selectedPackage = $('#Package').val();
                    var selectedProgram = $('#Program').val();
                   
                    $('#SelectForms').addClass('Action').click(function(){
                        if (hasDisabledAttr(this)) { 
                            return; 
                        }

                        var thePage ="/newlos/Services/SeamlessDocumentFormsList.aspx";
                        var url = thePage+ '?loanid='+ ML.sLId + 
                            "&PackageID=" + encodeURIComponent(selectedPackage) + 
                            "&ProgramID=" + encodeURIComponent(selectedProgram) + 
                            "&vendorId=" + <%= AspxTools.JsString(VendorId.ToString()) %>;
                        showModal(url,  null, 'dialogHeight:500px;dialogWidth:500px;' , null, function(result){ 
                            if (result && result.selectedForms) {
                                $('#SelectedForms').val(result.selectedForms);
                                $("#PartialPackage").prop('checked', true);
                            }
                            else {
                                $('#SelectedForms').val('');
                                $("#CompletePackage").prop('checked', true);
                                return;
                            }
                        },{hideCloseButton:true});
                    });
                    
                    $("#PartialPackage").click(function(){
                        $('#SelectForms').click();
                    });
                    
                    $("#CompletePackage").click(function(){
                        $('#SelectedForms').val('');                    
                    });
                }
            
                var elements = { 
                    ElectronicDelivery: { 
                        $all: $('.ElectronicDelivery'),
                        $OnlineSigning: $('#AllowOnlineSigning')
                    },
                    EmailOptions: {
                        $all: $('.EmailOptions'),
                        $CfmLink: $(document.getElementsByName("linkPickFromContact")[0]),
                        $NotifyOnRetrieval: $('#NotifyOnRetrieval'),
                        $RequirePassword: $('#RequirePassword')
                    },
                    PickFormat: { 
                        $all: $('.PickFormat')
                    },
                    PickForms: { 
                        $all: $('.PickForms')
                    },
                    AdditionalServices: { 
                        $all: $('.AdditionalServices'),
                        $ManualFulfillment: $('#ManualFulfillment'),
                        $MERSRegistration: $('#MERSRegistration')
                    }
                };

                if(!perms){
                    alert("Could not load your permissions, documents may not generate.");
                    $('#Error').text("Could not load your permissions, documents may not generate.");
                    elements.PickForms.$all.hide();
                    return;
                }
                
                var off;
                if (perms.HideDisabledOptions){
                    off = function($e, b) { if(!b) {$e.hide();} };
                }
                else{
                    off = function($e, b) { setDisabledAttr($e.find('input, select, textarea'), !b); };
                }
                
                if(!perms.EnableEDisclose){
                    off(elements.ElectronicDelivery.$all);
                }
                else{
                    off(elements.ElectronicDelivery.$OnlineSigning, perms.EnableESign);
                }
                
                if(!perms.EnableEmailDocuments){
                    off(elements.EmailOptions.$all);
                    off(elements.EmailOptions.$CfmLink);
                }
                else{
                    off(elements.EmailOptions.$NotifyOnRetrieval, perms.EnableEmailRetrievalNotification);
                    off(elements.EmailOptions.$RequirePassword, perms.EnableEmailPasswords);
                }
                
                if(!perms.EnableIndividualDocumentGeneration){
                    //Always hide this one if it's not allowed
                    elements.PickForms.$all.hide();
                }
                
                if(!(perms.EnableMERSRegistration || perms.EnableManualFulfillment)){
                    off(elements.AdditionalServices.$all);
                }
                else {
                    off(elements.AdditionalServices.$ManualFulfillment, perms.EnableManualFulfillment);
                    off(elements.AdditionalServices.$MERSRegistration, perms.EnableMERSRegistration);
                }
                
                var packageResults = /[\?&]packageType=([^&#]*)/.exec(window.location.search);
                var packageType = decodeURIComponent(packageResults[1].replace(/\+/g, " "));
                
                // 10/8/2015 BS - Case 226714. Comment it out per Joyce's request
                //if(packageType == "Redisclosure" || packageType == "Predisclosure") elements.PickForms.$all.prop('disabled', true);
                if(!perms.HideDisabledOptions) elements.ElectronicDelivery.$all.show();
                
                if($('#Options tbody tr:visible').length == 0){
                    $('#Options tbody').append('<tr><td class="FormTableSubheader">No document generation options available</td></tr>');
                }
            }
            
            function applySettings(settings){
                if(!settings){
                    alert("Could not load vendor-specific settings; options may be labelled incorrectly.");
                    $('#Error').text("Could not load vendor-specific settings; options may be labelled incorrectly.");                    
                    return;
                }
                $('.ExtraChargeWarning').toggle(settings.ChargeForManualFulfillment);
                $('.PickFormat').toggle(settings.PickDocumentFormat);
                $('#m_cbDSIPrintAndDeliver').siblings('label').text(settings.NameForManualFulfillment);
            }
        });
        function f_showDocMasterToolTip(event)
        {
            var tooltipDiv = document.getElementById("divDocMasterToolTip");
            
            tooltipDiv.style.display = "";
            tooltipDiv.style.visibility = 'visible';
            tooltipDiv.style.top = (event.clientY + document.body.scrollTop - 10)+ "px";
            tooltipDiv.style.left = (event.clientX + 10 ) + "px";
                
        }
        function f_closeDocMasterToolTip()
        {
            
            var tooltipDiv = document.getElementById("divDocMasterToolTip");
            tooltipDiv.style.display = 'none';
            tooltipDiv.style.visibility='hidden';
            
        }
        
        var cbSendEDisclosures= <%=AspxTools.JsGetElementById(m_cbSendEDisclosures) %>;
        <% if(m_phAllowOnlineSigning.Visible){ %>
            var cbAllowOnlineSigning = <%=AspxTools.JsGetElementById(m_cbAllowOnlineSigning) %>;    
        <%} %>
        
        var cbEmailDocuments = <%=AspxTools.JsGetElementById(m_cbEmailDocuments) %>;
            var tbEmailTo = <%=AspxTools.JsGetElementById(m_tbEmailTo) %>;
            var cbRequirePassword = <%=AspxTools.JsGetElementById(m_cbRequirePassword) %>;
                var ddlRequirePasswordType = <%=AspxTools.JsGetElementById(m_ddlRequirePasswordType) %>;
                    var tbPassword = <%=AspxTools.JsGetElementById(m_tbPassword) %>;
            var cbNotifyOnRetrieval = <%=AspxTools.JsGetElementById(m_cbNotifyOnRetrieval) %>;
        
        var cbDSIPrintAndDeliver = <%=AspxTools.JsGetElementById(m_cbDSIPrintAndDeliver) %>;
            var bDeliveryInfo = <%=AspxTools.JsGetElementById(m_bDeliveryInfo) %>;
            
        var creatingDocumentsSpan = document.getElementById('creatingDocumentsSpan');
        var bCreateDocuments = <%=AspxTools.JsGetElementById(m_bCreateDocuments) %>;
        
        var allowEPortalService = <%=AspxTools.JsGetElementById(AllowEPortalService) %>.value;
        var disableESign = <%= AspxTools.JsGetElementById(DisableESign) %>.value;
        
        var rbDocMasterFormat = <%=AspxTools.JsGetElementById(m_rbDocumentFormatDocMaster) %>;
        var rbPDFFormat = <%=AspxTools.JsGetElementById(m_rbDocumentFormatPDF) %>;
        
        function isDeliveryInfoOK(callback)
        {
            if(false == cbDSIPrintAndDeliver.checked || !bDeliveryInfo)
            {
                callback(true);
            }
            else
            {
                var openDI = false;

                if(<%=AspxTools.JsGetElementById(Name) %>.value == '')
                {
                    openDI = true;
                }
                else if(<%=AspxTools.JsGetElementById(Street) %>.value == '')
                {
                    openDI = true;
                }
                else if(<%=AspxTools.JsGetElementById(City) %>.value == '')
                {
                    openDI = true;
                }
                else if(<%=AspxTools.JsGetElementById(State) %>.value  == '')
                {
                    openDI = true;
                }
                else if(<%=AspxTools.JsGetElementById(Zip) %>.value  == '')
                {
                    openDI = true;
                }
                else if(<%=AspxTools.JsGetElementById(Phone) %>.value == '')
                {
                    openDI = true;
                }
                
                if(openDI)
                {
                    openDeliveryInfo(function(arg) {
                        if(!arg.OK) {
                            callback(false);
                        }
                        else{
                            isDeliveryInfoOK(callback);
                        }
                    });
                    
                }
                else{
                    callback(true);
                }
            }
        }
        
        function onDeliveryOptionsClicked()
        {
            var sendEDisclosures = document.getElementById('m_cbSendEDisclosures');
            var willEClose = document.getElementById('WillEClose');
            var emailDocuments = document.getElementById('m_cbEmailDocuments');
            var printDocuments = document.getElementById('m_cbDSIPrintAndDeliver');
            var mersRegistration = document.getElementById('m_cbMERSRegistration');
            var previewButton = document.getElementById('PreviewDocumentsButton');

            if (previewButton != null)
            {
                if ((sendEDisclosures != null && sendEDisclosures.checked) ||
                    (willEClose != null && willEClose.checked) ||
                    (emailDocuments != null && emailDocuments.checked) ||
                    (printDocuments != null && printDocuments.checked) ||
                    (mersRegistration != null && mersRegistration.checked))
                {
                    document.getElementById('PreviewDocumentsButton').disabled = true;
                }
                else
                {
                    document.getElementById('PreviewDocumentsButton').disabled = false;
                }
            }
        }

        function onElectronicDeliveryOptionClicked() {
            var sendEDisclosuresChecked = $('#m_cbSendEDisclosures').is(':checked');
            var willECloseChecked = $('#WillEClose').is(':checked') || false;
            var disableElectronicDeliverySubOptions = !sendEDisclosuresChecked && !willECloseChecked;

            var $allowOnlineSigning = $('#m_cbAllowOnlineSigning');
            if ($allowOnlineSigning.length) {
                $allowOnlineSigning.prop('disabled', willECloseChecked || disableElectronicDeliverySubOptions);
                $allowOnlineSigning.prop('checked', willECloseChecked);

                onAllowOnlineSigningClicked();
            }

            var $notifyAdditionalAgents = $('#cbNotifyAdditionalAgents');
            $notifyAdditionalAgents.prop('disabled', disableElectronicDeliverySubOptions)
            if (disableElectronicDeliverySubOptions)
            {
                $notifyAdditionalAgents.prop('checked', false).change();
            }

            var $printAndDeliver = $('#m_cbDSIPrintAndDeliver');
            $printAndDeliver.prop('disabled', sendEDisclosuresChecked);
            if (sendEDisclosuresChecked) {
                $printAndDeliver.prop('checked', false);
            }
        }

        function onDocMasterFormatClicked()
        {
            <% if(m_phAllowOnlineSigning.Visible){ %>     
                if(cbAllowOnlineSigning.checked)
                {
                    rbDocMasterFormat.checked = false;
                    rbPDFFormat.checked = true;
                }                
            <%} %>
        
        }
        <% if(m_phAllowOnlineSigning.Visible){ %>     
        function onAllowOnlineSigningClicked()
        {
            if(cbAllowOnlineSigning.checked)
            {
                rbDocMasterFormat.disabled = true;
                
                rbDocMasterFormat.checked = false;
                rbPDFFormat.checked = true;
            }
            else
            {
                rbDocMasterFormat.disabled = false;
            }
        }
        <%} %>

        function onEmailDocumentsClicked()
        {
            var disable = !cbEmailDocuments.checked;
            if(disable)
            {
                tbEmailTo.value = '';
                cbRequirePassword.checked = false;
                cbNotifyOnRetrieval.checked = false;
                ddlRequirePasswordType.disabled = true;
                ddlRequirePasswordType.style.backgroundColor = 'lightgray';
                tbPassword.value = '';
                tbPassword.readOnly = true;
                ValidatorEnable(<%=AspxTools.JsGetElementById(m_rfvPassword) %>, false);
            }
                       
            
            tbEmailTo.readOnly = disable;
            cbRequirePassword.disabled = disable;
            cbNotifyOnRetrieval.disabled = disable;
            ValidatorEnable(<%=AspxTools.JsGetElementById(m_rfvEmailTo) %>, !disable);
            enableDisableCFM();
            enableDisableCreateButton();
        }
        function enableDisableCFM()
        {
            var disable = !cbEmailDocuments.checked;
            setDisabledAttr(document.getElementsByName("linkPickFromContact")[0], disable);
        }
        function onDSIPrintAndDeliverClicked()
        {
            var disable = !cbDSIPrintAndDeliver.checked;
            if(bDeliveryInfo)
                bDeliveryInfo.disabled = disable;
        }
        function onCBRequirePasswordClicked()
        {
            var disable = !cbRequirePassword.checked;
            if(disable)
            {
                tbPassword.value = '';
                tbPassword.readOnly = true;
                ddlRequirePasswordType.style.backgroundColor='lightgray';
                ValidatorEnable(<%=AspxTools.JsGetElementById(m_rfvPassword) %>, false);
            }
            else{
                onDDLRequirePasswordChanged();
                ddlRequirePasswordType.style.backgroundColor='white';
            }
            
            ddlRequirePasswordType.disabled = disable;           
            enableDisableCreateButton();
        }
        function onDDLRequirePasswordChanged()
        {
            var option = ddlRequirePasswordType.options[ddlRequirePasswordType.selectedIndex];
            if(!option) return;
            
            if(option.text == "Other")
            {
                tbPassword.value = '';
                tbPassword.readOnly = false;
                ValidatorEnable(<%=AspxTools.JsGetElementById(m_rfvPassword) %>, true);
            }
            else{
                tbPassword.value =  option.value;
                tbPassword.readOnly = true;
                ValidatorEnable(<%=AspxTools.JsGetElementById(m_rfvPassword) %>, false);
            }
            enableDisableCreateButton();
        }
        function onDeliveryInfoClick()
        {           
            openDeliveryInfo();            
        }
        function openDeliveryInfo(callback)
        {
            var packageT = <%=AspxTools.JsGetElementById(Package) %>;
            var dsiArgs = "?loanid="+<%=AspxTools.JsString(RequestHelper.LoanID) %> ;
            if(packageT.value == "Predisclosure" || packageT.value == "Redisclosure") // Initial Disclosure / Redisclosure
            {
                dsiArgs += '' +
                '&name='+ <%=AspxTools.JsGetElementById(BName) %>.value +
                '&street='+ <%=AspxTools.JsGetElementById(BStreet) %>.value +
                '&city='+ <%=AspxTools.JsGetElementById(BCity) %>.value +
                '&state='+ <%=AspxTools.JsGetElementById(BState) %>.value +
                '&zip='+ <%=AspxTools.JsGetElementById(BZip) %>.value +
                '&phone='+ <%=AspxTools.JsGetElementById(BPhone) %>.value;
            }
            else{
                dsiArgs += '' +
                '&name='+ <%=AspxTools.JsGetElementById(Name) %>.value +
                '&street='+ <%=AspxTools.JsGetElementById(Street) %>.value +
                '&city='+ <%=AspxTools.JsGetElementById(City) %>.value +
                '&state='+ <%=AspxTools.JsGetElementById(State) %>.value +
                '&zip='+ <%=AspxTools.JsGetElementById(Zip) %>.value +
                '&phone='+ <%=AspxTools.JsGetElementById(Phone) %>.value;
            }
            dsiArgs += '' +
                '&dsinotes='+<%=AspxTools.JsGetElementById(DSINotes) %>.value +
                '&attention='+<%=AspxTools.JsGetElementById(Attention) %>.value;
                
                    
            showModal('/newlos/Services/DocMagicDeliveryInformation.aspx'+ dsiArgs, null, null, null, function(arg){
                if (arg.OK) {
                    
                    <%=AspxTools.JsGetElementById(Attention) %>.value = arg.attention;
                    <%=AspxTools.JsGetElementById(Name) %>.value = arg.name;
                    <%=AspxTools.JsGetElementById(Street) %>.value = arg.street;
                    <%=AspxTools.JsGetElementById(City) %>.value = arg.city;
                    <%=AspxTools.JsGetElementById(State) %>.value = arg.state;
                    <%=AspxTools.JsGetElementById(Zip) %>.value = arg.zip;
                    <%=AspxTools.JsGetElementById(Phone) %>.value = arg.phone;
                    <%=AspxTools.JsGetElementById(DSINotes) %>.value = arg.dsiNotes;
                }
                else{ // window was closed/ cancelled.
                    cbDSIPrintAndDeliver.checked = false;
                    onDSIPrintAndDeliverClicked();
                }

                if(typeof(callback) === 'function') {
                    callback(arg);
                }
            },{hideCloseButton:true});
        }
        function isValid(callback)
        {
            Page_ClientValidateForIE9();
            if(!Page_IsValid)
            {
                callback(false);
            }
            else{
                isDeliveryInfoOK(callback);
            }
        }
        function Page_ClientValidateForIE9()
        {
            Page_ClientValidate();
            if(Page_IsValid)
            {
                if(cbEmailDocuments.checked)
                {
                    if(!isEmailValid(null,{}))
                    {
                        Page_IsValid = false;
                        document.getElementById("m_rfvEmailTo").style.visibility = 'visible';
                    }
                    else{
                    }
                    if(cbRequirePassword.checked)
                    {
                        if(ddlRequirePasswordType.options[ddlRequirePasswordType.selectedIndex].text == "Other")
                        {
                            if(tbPassword.value.replace(" ", "")  == "")
                            {
                                document.getElementById("m_rfvPassword").style.visibility = 'visible';
                                Page_IsValid = false;        
                            }                            
                        }
                        
                    }
                }
            }
        }
        
        function onBCreateDocumentsClicked(postbackButtonId)
        {
            isValid(function(isPageValid) {
                if(isPageValid)
                {
                    bCreateDocuments.style.display = 'none';
                
                    var previewButton = document.getElementById('PreviewDocumentsButton');
                    if (previewButton != null)
                    {
                        previewButton.style.display = 'none';
                    }

                    // Don't allow the users to try to select forms after the request
                    // has been submitted. That page will perform a loan export, which
                    // may hit issues with the temporary archive.
                    jQuery('tr.PickForms input').prop('disabled', true); 
                    setDisabledAttr(jQuery('#SelectForms'), true);
                
                    creatingDocumentsSpan.style.display = 'block';

                    document.getElementById(postbackButtonId).click();
                }
            });
        }
        
        function openPortal(url)
        {
            var opts = [
                'height=' + Math.min(screen.height, 800),
                'width=' + Math.min(screen.width, 1000),
                'resizable'
            ].join(',');
            var portal;
            try{
                portal = window.open(url, "Portal", opts);
            }
            catch(e){
                //If the previous attempt didn't work, try again without the options.
                portal = window.open(url, "Portal");
            }
                
            if(portal)
            {
                try{
                    //Sometimes this might give an Access Denied error
                    portal.moveTo(10, 10);
                }
                catch(e){}
            }
        }
        
        function init()
        {
            var downloadDocs = <%=AspxTools.JsGetElementById(DownloadDocs) %>.value;
            if(downloadDocs == 'true')
            {
                var u = <%=AspxTools.SafeUrl(DownloadDBKURL)%>;
                var opts = "dialogWidth: 100px; dialogHeight: 100px; center: yes; resizable: yes; scroll: no; status: yes; toolbar:no; menubar:no; help: no;"
                showModal(u, 'SeamlessDocumentGeneration', opts);
            }
            if(<%=AspxTools.JsGetElementById(HideButtons) %>.value == 'true')
            {
                document.getElementById('buttonsRow').style.display = 'none';
                if (dialogArguments.treeview)
                {
                    window.onbeforeunload = function() 
                    { 
                        try 
                        {
                            dialogArguments.treeview.location.reload();
                        }
                        catch (e) {}
                    };
                }
            }
            if(allowEPortalService == "N")
            {
                cbSendEDisclosures.disabled = true;
            }
            
            ValidatorEnable(<%=AspxTools.JsGetElementById(m_rfvPassword) %>, false);
            ValidatorEnable(<%=AspxTools.JsGetElementById(m_rfvEmailTo) %>, true);
            
            attach(cbEmailDocuments, 'click', onEmailDocumentsClicked);
            attach(rbDocMasterFormat, 'click', onDocMasterFormatClicked);
            <% if(m_phAllowOnlineSigning.Visible){ %>     
            attach(cbAllowOnlineSigning, 'click', onAllowOnlineSigningClicked);
            <%} %>
            attach(cbDSIPrintAndDeliver, 'click', onDSIPrintAndDeliverClicked);
            attach(ddlRequirePasswordType, 'change', onDDLRequirePasswordChanged);
            attach(cbRequirePassword, 'click', onCBRequirePasswordClicked);
            
            attach(tbEmailTo, 'blur', enableDisableCreateButton);
            attach(tbPassword, 'blur', enableDisableCreateButton);
            
            var pickLinkList = document.getElementsByName("linkPickFromContact")
            attach(pickLinkList[0], 'click', enableDisableCreateButton);

            onElectronicDeliveryOptionClicked();
            onEmailDocumentsClicked();
            onDSIPrintAndDeliverClicked();
            onDDLRequirePasswordChanged();
            onCBRequirePasswordClicked();
            creatingDocumentsSpan.style.display = 'none';
            enableDisableCreateButton();
            
            var alertMessage = <%=AspxTools.JsGetElementById(AlertMessage) %>.value;
            if (alertMessage != '') {
                LQBPopup.ShowString(alertMessage, {
                    width: 420,
                    height: 180,
                    modifyOverflow: false,
                    popupClasses: "LQBPopupAlert",
                    onClose: function () {
                        var close = document.getElementById('ClosePage');
                        if (close) {
                            onClosePopup();
                            return;
                        } else {
                            <% if (IsDisplayDocumentValidationWarning) { %>
                                displayDocValidationWarning();
                            <% } %>
                        }
                    }
                });
            } else {
                <% if (IsDisplayDocumentValidationWarning) { %>
                    displayDocValidationWarning();
                <% } %>
            }

        }
        
        //<%-- From OPM 128950, we had issues because init would try to attach to an element that didn't exist. --%>
        function attach(elem, event, handler){
            if(!elem || (!elem.attachEvent && !elem.addEventListener) || !event || !handler) return;
                addEventHandler(elem, event, handler, false);
               
        }
        
        function enableDisableCreateButton()
        {
            Page_ClientValidateForIE9();
            if (bCreateDocuments) {
                bCreateDocuments.disabled = !Page_IsValid;
            }
        }
        function isEmailValid(source, args)
        {
            args.IsValid = true;
            if(cbEmailDocuments.checked)
            {
                var emailTxt = tbEmailTo.value;
                var emails = emailTxt.split(';');
                var epat = new RegExp(<%=AspxTools.JsString(ConstApp.EmailValidationExpression) %>);
                for(var count = 0; count < emails.length; count++)
                {
                    var email = emails[count].replace(/^\s+|\s+$/g,"");
                    if(false == epat.test(email))
                    {
                       args.IsValid = false;
                       return false;
                    }
                }
            }
            return true;
        }
        function displayDocValidationWarning()
        {
            var url = '/newlos/Services/DocumentValidationWarning.aspx?loanid=' + ML.sLId + '&cacheid=' + ML.WarningCacheId;
            showModal(url,  null, 'dialogHeight:500px;dialogWidth:500px;' , null, function(result){ 
                if (result.ContinueProcessing)
                {
                // Perform post back and save the e-doc.
                    generateDocWithValidationWarning();
                }
                else
                {
                    return;
                }
            },{hideCloseButton:true});
        }

        function generateDocWithValidationWarning()
        {
                        document.getElementById('ValidationWarningId').value = ML.WarningCacheId;
                <%= AspxTools.JsGetElementById(btnCreateDocumentsWithWarning) %>.style.display = 'none';
                // Prevent user from double click
                bCreateDocuments.style.display = 'none';
                creatingDocumentsSpan.style.display = 'block';
                __doPostBack('', '');
        }

        init();
    </script>
</body>
</html>
