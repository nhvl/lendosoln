﻿#region Auto Generated Code
namespace LendersOfficeApp.newlos.Services
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Integration.UcdDelivery;
    using LendersOffice.Integration.UcdDelivery.DocMagic;
    using LendersOffice.Integration.UcdDelivery.FannieMae;
    using LendersOffice.Integration.UcdDelivery.FreddieMac;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;

    /// <summary>
    /// Service class for UCD generation.
    /// </summary>
    public partial class GenerateUcdService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Chooses the method to run.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(this.CheckDmCredentials):
                    this.CheckDmCredentials();
                    break;
                case nameof(this.CheckGseCredentials):
                    this.CheckGseCredentials();
                    break;
                case nameof(this.DeliverUcd):
                    this.DeliverUcd(this.GetEnum<UcdDeliveryAction>("DeliveryAction"));
                    break;
                case nameof(this.SetDocCode):
                    this.SetDocCode();
                    break;
                default:
                    throw new UnhandledCaseException(methodName);
            }
        }

        /// <summary>
        /// Checks for DM credentials.
        /// </summary>
        private void CheckDmCredentials()
        {
            var cdId = this.GetGuid("ClosingDisclosureId");
            var loanId = this.GetGuid("LoanId");

            var dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(GenerateUcdService));
            dataLoan.InitLoad();

            var closingDisclosure = dataLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.FirstOrDefault((date) => date.UniqueId == cdId);
            if (closingDisclosure == null)
            {
                this.SetResult("Success", false);
                this.SetResult("Error", "Unable to find closing disclosure.");
                return;
            }

            Guid? vendorId = closingDisclosure.VendorId;
            if (vendorId.HasValue)
            {
                var principal = PrincipalFactory.CurrentPrincipal;
                var userId = principal.UserId;
                var brokerId = principal.BrokerId;
                var branchId = dataLoan.sBranchId;
                VendorCredentials credentials = new VendorCredentials(userId, branchId, brokerId, vendorId.Value);

                var hasValidCredentials = !string.IsNullOrEmpty(credentials.UserName) && !string.IsNullOrEmpty(credentials.Password) && !string.IsNullOrEmpty(credentials.CustomerId);
                if (hasValidCredentials)
                {
                    this.SetResult("Success", true);
                    this.SetResult("HasCredentials", true);
                    return;
                }
                else if (!string.IsNullOrEmpty(credentials.UserName) || !string.IsNullOrEmpty(credentials.Password) || !string.IsNullOrEmpty(credentials.CustomerId))
                {
                    // Incomplete credentials result in an error.
                    this.SetResult("Success", false);
                    this.SetResult("Errors", "Auto Doc Magic credentials are invalid.");
                    return;
                }
            }

            this.SetResult("Success", true);
            this.SetResult("HasCredentials", false);
        }

        /// <summary>
        /// Checks for GSE credentials.
        /// </summary>
        private void CheckGseCredentials()
        {
            Guid loanId = this.GetGuid("LoanId");
            UcdDeliveredToEntity deliveredTo = this.GetEnum<UcdDeliveredToEntity>("DeliveredTo");

            var dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(GenerateUcdService));
            dataLoan.InitLoad();

            var serviceCredentials = ServiceCredential.ListAvailableServiceCredentials(PrincipalFactory.CurrentPrincipal, dataLoan.sBranchId, ServiceCredentialService.UcdDelivery);

            bool needsSellerId = deliveredTo == UcdDeliveredToEntity.FreddieMac;
            bool hasSavedSellerId = false;
            bool hasSavedCredentials = false;
            if (deliveredTo == UcdDeliveredToEntity.FannieMae)
            {
                hasSavedCredentials = serviceCredentials.Any(cred => cred.UcdDeliveryTarget == UcdDeliveryTargetOption.FannieMaeUcdCollectionSolution);
            }
            else if (deliveredTo == UcdDeliveredToEntity.FreddieMac)
            {
                hasSavedSellerId = serviceCredentials.Any(cred => cred.UcdDeliveryTarget == UcdDeliveryTargetOption.LcaSellerCredential);
                hasSavedCredentials = serviceCredentials.Any(cred => cred.UcdDeliveryTarget == UcdDeliveryTargetOption.LcaUserLogin);
            }

            this.SetResult("Success", true);
            this.SetResult("NeedsSellerId", needsSellerId);
            this.SetResult("HasSavedSellerId", hasSavedSellerId);
            this.SetResult("HasSavedCredentials", hasSavedCredentials);
        }

        /// <summary>
        /// Validates a request to deliver a UCD file.
        /// </summary>
        /// <param name="closingDisclosure">The selected closing disclosure record.</param>
        /// <param name="deliveryAction">The selected delivery action to perform.</param>
        /// <param name="deliveryTarget">The selected end recipient of the UCD file.</param>
        /// <param name="principal">The user making the request.</param>
        /// <returns>A list of errors encountered in validation.</returns>
        private List<string> ValidateDeliverRequest(ClosingDisclosureDates closingDisclosure, UcdDeliveryAction deliveryAction, UcdDeliveredToEntity? deliveryTarget, AbstractUserPrincipal principal)
        {
            List<string> errors = new List<string>();
            if (deliveryAction != UcdDeliveryAction.GenerateOnly && deliveryTarget == null)
            {
                errors.Add("Cannot deliver if no delivery target is specified.");
            }

            if (deliveryAction == UcdDeliveryAction.DeliverExisting && (deliveryTarget != UcdDeliveredToEntity.FannieMae && deliveryTarget != UcdDeliveredToEntity.FreddieMac))
            {
                errors.Add("The selected action is \"Deliver Existing\", but the selected GSE is not supported for that action.");
            }

            UcdLenderService ucdConfig = principal.BrokerDB.GetUcdConfig();
            bool deliverExistingDisabled = 
                (deliveryTarget == UcdDeliveredToEntity.FannieMae && !ucdConfig.AllowLqbDeliveryToFannieMae)
                || (deliveryTarget == UcdDeliveredToEntity.FreddieMac && !ucdConfig.AllowLqbDeliveryToFreddieMac);
            bool generateAndDeliverDisabled = 
                (deliveryTarget == UcdDeliveredToEntity.FannieMae && !ucdConfig.AllowDocMagicDeliveryToFannieMae)
                || (deliveryTarget == UcdDeliveredToEntity.FreddieMac && !ucdConfig.AllowDocMagicDeliveryToFreddieMac);

            if ((deliveryAction == UcdDeliveryAction.DeliverExisting && deliverExistingDisabled)
                || (deliveryAction == UcdDeliveryAction.GenerateAndDeliver && generateAndDeliverDisabled))
            {
                errors.Add("The selected action is disabled for the selected GSE.");
            }

            if (closingDisclosure == null)
            {
                errors.Add("Unable to find closing disclosure.");
            }

            return errors;
        }

        /// <summary>
        /// Generates a UCD through the DocMagic UCD service, optionally delivering to the GSE.
        /// </summary>
        /// <param name="deliveryAction">Specifies the requested delivery action through the UI.</param>
        private void DeliverUcd(UcdDeliveryAction deliveryAction)
        {
            Guid loanId = this.GetGuid("LoanId");
            Guid appId = this.GetGuid("AppId");
            var closingDisclosureId = this.GetGuid("ClosingDisclosureId");
            UcdDeliveredToEntity? deliveryTarget = deliveryAction == UcdDeliveryAction.GenerateOnly ? (UcdDeliveredToEntity?)null : this.GetEnum<UcdDeliveredToEntity>("GseDeliveryTarget");

            var dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(GenerateUcdService));
            dataLoan.InitLoad();

            var principal = PrincipalFactory.CurrentPrincipal;
            var userId = principal.UserId;
            var brokerId = principal.BrokerId;
            var branchId = dataLoan.sBranchId;

            var closingDisclosure = dataLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.FirstOrDefault((date) => date.UniqueId == closingDisclosureId);
            var errors = ValidateDeliverRequest(closingDisclosure, deliveryAction, deliveryTarget, principal);

            if (errors.Any())
            {
                this.SetResult("Success", false);
                this.SetResult("Error", SerializationHelper.JsonNetSerialize(errors));
                return;
            }

            string gseCustomerId = this.GetString("GseSellerId", null);
            string gseUsername = this.GetString("GseUsername", null);
            string gsePassword = this.GetString("GsePassword", null);

            if (deliveryTarget == UcdDeliveredToEntity.FannieMae || deliveryTarget == UcdDeliveredToEntity.FreddieMac)
            {
                var ucdDeliveryCredentials = ServiceCredential.ListAvailableServiceCredentials(principal, branchId, ServiceCredentialService.UcdDelivery);
                if (deliveryTarget == UcdDeliveredToEntity.FannieMae)
                {
                    var serviceCredential = ucdDeliveryCredentials.FirstOrDefault(cred => cred.UcdDeliveryTarget == UcdDeliveryTargetOption.FannieMaeUcdCollectionSolution);

                    // No customer id in this case.
                    gseCustomerId = string.Empty;
                    gseUsername = serviceCredential?.UserName ?? gseUsername;
                    gsePassword = serviceCredential?.UserPassword.Value ?? gsePassword;
                }
                else if (deliveryTarget == UcdDeliveredToEntity.FreddieMac)
                {
                    var sellerCredential = ucdDeliveryCredentials.FirstOrDefault(cred => cred.UcdDeliveryTarget == UcdDeliveryTargetOption.LcaSellerCredential);
                    var userCredential = ucdDeliveryCredentials.FirstOrDefault(cred => cred.UcdDeliveryTarget == UcdDeliveryTargetOption.LcaUserLogin);

                    gseCustomerId = sellerCredential?.UserName ?? gseCustomerId;
                    gseUsername = userCredential?.UserName ?? gseUsername;
                    gsePassword = userCredential?.UserPassword.Value ?? gsePassword;
                }
            }

            AbstractUcdDeliveryRequestData requestData = null;

            if (deliveryAction == UcdDeliveryAction.GenerateOnly || deliveryAction == UcdDeliveryAction.GenerateAndDeliver)
            {
                string docCode = closingDisclosure.DocCode;

                string docMagicCustomerId = this.GetString("DmCustomerId", null);
                string docMagicUsername = this.GetString("DmUsername", null);
                string docMagicPassword = this.GetString("DmPassword", null);

                Guid? vendorId = closingDisclosure.VendorId;
                if (vendorId.HasValue)
                {
                    VendorCredentials credentials = new VendorCredentials(userId, branchId, brokerId, vendorId.Value);
                    var hasValidCredentials = !string.IsNullOrEmpty(credentials.UserName) && !string.IsNullOrEmpty(credentials.Password) && !string.IsNullOrEmpty(credentials.CustomerId);
                    if (hasValidCredentials)
                    {
                        docMagicCustomerId = credentials.CustomerId;
                        docMagicUsername = credentials.UserName;
                        docMagicPassword = credentials.Password;
                    }
                }
                else
                {
                    errors = new List<string>() { "Closing Disclosure was not associated with a Vendor." };
                    this.SetResult("Success", false);
                    this.SetResult("Error", SerializationHelper.JsonNetSerialize(errors));
                    return;
                }

                var docMagicRequestData = new DocMagicUcdDeliveryRequestData(docCode, loanId, appId, closingDisclosureId, vendorId.Value, deliveryAction, principal);
                docMagicRequestData.DeliveryTarget = deliveryTarget;
                docMagicRequestData.DmCustomerId = docMagicCustomerId;
                docMagicRequestData.DmUsername = docMagicUsername;
                docMagicRequestData.DmPassword = docMagicPassword;
                requestData = docMagicRequestData;
            }
            else if (deliveryAction == UcdDeliveryAction.DeliverExisting && deliveryTarget == UcdDeliveredToEntity.FannieMae)
            {
                requestData = FannieMaeUcdDeliveryRequestData.CreateFannieMaePostRequestData(loanId, appId, closingDisclosure, principal);
            }
            else if (deliveryAction == UcdDeliveryAction.DeliverExisting && deliveryTarget == UcdDeliveredToEntity.FreddieMac)
            {
                requestData = FreddieMacUcdDeliveryRequestData.CreateFreddieMacRequestData(loanId, appId, closingDisclosure, principal);
            }
            else
            {
                throw new NotSupportedException("No defined requestData for deliveryAction = " + deliveryAction + ", deliveryTarget = " + deliveryTarget);
            }

            requestData.GseCustomerId = gseCustomerId;
            requestData.GseUserName = gseUsername;
            requestData.GsePassword = gsePassword;

            string error;
            if (!requestData.Validate(out error))
            {
                errors = new List<string>() { error };
                this.SetResult("Success", false);
                this.SetResult("Errors", SerializationHelper.JsonNetSerialize(errors));
                return;
            }

            PlaceRequest(requestData, errors);
        }

        /// <summary>
        /// Place the given request and handle any errors.
        /// </summary>
        /// <param name="requestData">The request data object to use for placing the request.</param>
        /// <param name="errors">A list of existing errors to serialize in case of an error.</param>
        private void PlaceRequest(AbstractUcdDeliveryRequestData requestData, List<string> errors)
        {
            AbstractUcdDeliveryRequestHandler requestHandler = AbstractUcdDeliveryRequestHandler.GenerateRequestHandler(requestData);
            UcdDelivery deliveryOrder;
            var resultStatus = requestHandler.SubmitRequest(out deliveryOrder, out errors);
            if (resultStatus == UcdDeliveryResultStatus.Error)
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", SerializationHelper.JsonNetSerialize(errors));
                return;
            }

            this.SetResult("Success", true);
            this.SetResult("ResultStatus", resultStatus);
            if (resultStatus == UcdDeliveryResultStatus.Generated)
            {
                this.SetResult("Message", "UCD file has been uploaded to EDocs and associated with the Closing Disclosure record.");
            }
            else if (resultStatus == UcdDeliveryResultStatus.GeneratedButNotDelivered)
            {
                this.SetResult("Message", "UCD file has been uploaded to EDocs and associated with the Closing Disclosure record but delivery failed.");
                this.SetResult("Errors", SerializationHelper.JsonNetSerialize(errors));
            }
            else if (resultStatus == UcdDeliveryResultStatus.GeneratedAndDelivered)
            {
                UcdDeliveryView view = deliveryOrder.ToView();
                this.SetResult("DeliveryItem", SerializationHelper.JsonNetAnonymousSerialize(view));
                this.SetResult("Message", $"UCD file has been uploaded to EDocs and associated with the Closing Disclosure record and delivered to {view.DeliveredTo_rep}.");
            }
            else if (resultStatus == UcdDeliveryResultStatus.Delivered)
            {
                UcdDeliveryView view = deliveryOrder.ToView();
                this.SetResult("DeliveryItem", SerializationHelper.JsonNetAnonymousSerialize(view));
                this.SetResult("Message", $"UCD file has been delivered to {view.DeliveredTo_rep}.");
            }
        }

        /// <summary>
        /// Sets the doc code for a chosen closing disclosure dates.
        /// </summary>
        private void SetDocCode()
        {
            Guid closingDisclosureId = this.GetGuid("ClosingDisclosureId");
            string docCode = this.GetString("DocCode");
            Guid loanId = this.GetGuid("LoanId");

            CPageData loanData = CPageData.CreateUsingSmartDependency(loanId, typeof(GenerateUcdService));
            loanData.InitSave(ConstAppDavid.SkipVersionCheck);

            var chosenCd = loanData.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.FirstOrDefault((cd) => cd.UniqueId == closingDisclosureId);
            if (chosenCd == null)
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", "Unable to find closing disclosure.");
                return;
            }

            chosenCd.DocCode = docCode;
            loanData.SaveLEandCDDates();
            loanData.Save();

            this.SetResult("Success", true);
        }
    }
}