﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderAppraisalEmailLog.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.OrderAppraisalEmailLog" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Email Log</title>
    <link href="../../css/stylesheet.css" rel="stylesheet" type="text/css" />
<style type="text/css">
input
{
    border: default;
    padding: 0;
    height: 1.5em;
}
h2
{
    margin: 0 0 2px 0;
    padding: 2px;
}
img
{
    vertical-align: top;
}
.row
{
    width: 100%;
    margin: 0 auto;
    overflow: hidden;
    font-size: 11px;
}
input[type="button"]
{
    float: left;
    height: 2em;
}
#CloseWindowBtn
{
    float: right;
}
#TableContainer
{
    height: 480px;
    overflow-y: scroll;
    overflow-x: hidden;
}
.GridHeader
{
    height: 4px;
}
.EmailInfoRow
{
    height: 2em;
}
#footer
{
    height:10px;
}
</style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField id="VendorId" runat="server" />
    <asp:HiddenField id="AppraisalOrderNumber" runat="server" />
    <asp:HiddenField id="ErrorMessage" runat="server" />
    <div>
        <h2 class="MainRightHeader">
            Email Log
        </h2>
        <div id="logBody">
            <div id="TableContainer" class="InsetBorder">
            <asp:Repeater id="EmailLog" runat="server" OnItemDataBound="EmailLog_OnItemDataBound">
                <HeaderTemplate>
                    <table id="EmailLogTable" class="FormTable" cellpadding="2" cellspacing="1" style="width:100%;">
                    <thead>
                        <tr class="GridHeader">
                            <th style="width:50%" runat="server" id="SubjectColumn">Subject</th>
                            <th style="width:15%" runat="server" id="StatusColumn">From</th>
                            <th style="width:25%">Date</th>
                            <th style="width:10%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="GridItem EmailInfoRow" runat="server" id="Row">
                        <td><ml:EncodedLiteral id="EmailSubject" runat="server"></ml:EncodedLiteral></td>
                        <td><ml:EncodedLiteral id="EmailFrom" runat="server"></ml:EncodedLiteral></td>
                        <td><ml:EncodedLiteral id="EmailDate" runat="server"></ml:EncodedLiteral></td>
                        <td>
                            <a href="#" id="ViewLink" title="View Email Message" runat="server">view</a>
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="AlternatingGridItem EmailInfoRow" runat="server" id="Row">
                        <td><ml:EncodedLiteral id="EmailSubject" runat="server"></ml:EncodedLiteral></td>
                        <td><ml:EncodedLiteral id="EmailFrom" runat="server"></ml:EncodedLiteral></td>
                        <td><ml:EncodedLiteral id="EmailDate" runat="server"></ml:EncodedLiteral></td>
                        <td>
                            <a href="#" id="ViewLink" title="View Email Message" runat="server">view</a>
                        </td>
                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            </div>
            <div id="BtnContainer">
                <input type="button" id="SendEmailToOfficeBtn" value="Email Office" onclick="sendEmail('Office');" />
                <input type="button" id="SendEmailToAppraiserBtn" value="Email Appraiser" onclick="sendEmail('Appraiser');" />
                <input type="button" id="CloseWindowBtn" value="Close" onclick="onClosePopup();" />
            </div>
        </div>
    </div>
    </form>
    <div id="footer"></div>
</body>
<script type="text/javascript">
    $(function() {
        if ($.trim($('#ErrorMessage').val()) != '') {
            alert($('#ErrorMessage').val());
        }
    });
    $(window).on("load", function() {
        $('.EmailMessageRow').hide();
    });
    var options = "dialogHeight:600px; dialogWidth:700px; center:yes; resizable:yes; scroll:no; status=no; help=no;";
    function viewEmail(target)
    {
        showModal(target,null,options);
    }
    function sendEmail(recipient)
    {
        var url = '/newlos/Services/OrderAppraisalEmailMessage.aspx'
                    + '?loanid='+ <%= AspxTools.JsString( LoanID ) %>
                    + '&mode=' + 'send'
                    + '&vendorID='+ VendorID
                    + '&orderNumber='+ OrderNumber
                    + '&sendTo='+ recipient;
        showModal(url,null,options, null, null,{hideCloseButton:true});
    }
</script>
</html>
