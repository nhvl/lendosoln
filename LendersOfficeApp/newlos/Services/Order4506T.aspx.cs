﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LendersOffice.Integration.Irs4506T;
using DataAccess;
using System.Web.UI.HtmlControls;
using LendersOffice.AntiXss;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.newlos.Services
{
    public partial class Order4506T : BaseLoanPage
    {
        protected Guid TransactionID
        {
            get { return LendersOffice.Common.RequestHelper.GetGuid("TransactionId"); }
        }
        protected string RequestedDocKey
        {
            get { return LendersOffice.Common.RequestHelper.GetSafeQueryString("docKey"); }
        }
        protected bool IsDocRequest
        {
            get { return !string.IsNullOrEmpty(RequestedDocKey); }
        }
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            base.OnInit(e);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            this.PageTitle = "Order 4506-T";
            this.PageID = "Order4506T";

            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("LQBPopup.js");
            m_dg.ItemDataBound += new DataGridItemEventHandler(m_dg_ItemDataBound);
            m_vendorDDL.DataSource = Irs4506TVendorConfiguration.ListActiveVendorByBrokerId(BrokerID);
            m_vendorDDL.DataTextField = "VendorName";
            m_vendorDDL.DataValueField = "VendorId";
            m_vendorDDL.DataBind();
        }

        void m_dg_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer)
            {
                return;
            }
            Irs4506TOrderInfo orderInfo = (Irs4506TOrderInfo)e.Item.DataItem;

            PassthroughLiteral c = e.Item.FindControl("UploadDocumentsLinksHtml") as PassthroughLiteral;
            if (c != null)
            {
                c.Text = orderInfo.UploadDocumentsLinksHtml;
            }

            Label statusDescriptionLabel = e.Item.FindControl("StatusDescriptionLabel") as Label;
            statusDescriptionLabel.Text = orderInfo.StatusT == Irs4506TOrderStatus.Unknown ? orderInfo.StatusDescription : orderInfo.StatusT.ToString("G");

            HtmlAnchor statusDetailsLink = e.Item.FindControl("StatusDetailsLink") as HtmlAnchor;
            if (string.IsNullOrEmpty(orderInfo.StatusDescription) || orderInfo.StatusDescription.Equals(orderInfo.StatusT.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                statusDetailsLink.Visible = false;
            }
            else
            {
                statusDetailsLink.Title = orderInfo.StatusDescription;
            }

            Button checkStatusButton = e.Item.FindControl("CheckStatusButton") as Button;
            checkStatusButton.Visible =
                Irs4506TVendorConfiguration.RetrieveById(orderInfo.VendorId).CommunicationModel == Irs4506TCommunicationModel.EquifaxVerificationServices 
                && (orderInfo.StatusT == Irs4506TOrderStatus.Pending 
                    || (orderInfo.StatusT == Irs4506TOrderStatus.Completed && !orderInfo.UploadedFiles.Any()));
            checkStatusButton.OnClientClick = $"f_checkStatus(this, {AspxTools.JsString(orderInfo.OrderNumber)}, {AspxTools.JsString(orderInfo.VendorId)}); return false;";
            if (orderInfo.StatusT == Irs4506TOrderStatus.Completed)
            {
                checkStatusButton.Text = "Retrieve Document";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsDocRequest)
            {
                if (WriteDocument(RequestedDocKey, LoanID, TransactionID)) return;
            }
        }

        private bool WriteDocument(string requestedDocKey, Guid loanId, Guid transactionId)
        {
            //Although we could just grab the doc from the filedb key, 
            //we need to make sure the requested filedb key actually belongs to a document on this loan.
            var Irs4506TOrder = Irs4506TOrderInfo.RetrieveByLoanId(loanId).FirstOrDefault(order => order.TransactionId == transactionId);
            var requestedDoc = Irs4506TOrder.UploadedFiles.FirstOrDefault(
                                        file => file.FileDBKey.Equals(RequestedDocKey, StringComparison.CurrentCultureIgnoreCase));
            if (requestedDoc != default(Irs4506TOrderInfo.Irs4506TDocument))
            {
                WriteDoc(requestedDoc);
                return true;
            }
            return false;
        }
        private void WriteDoc(Irs4506TOrderInfo.Irs4506TDocument doc)
        {
            doc.UseFile((fileInfo) =>
            {
                Response.Clear();
                Response.ClearHeaders();
                //Need to remove any compression filtering, otherwise it comes out garbled.
                Response.Filter = null;
                Response.ContentType = "application/pdf";
                Response.AppendHeader("Content-Disposition", $"attachment; filename=\"{doc.DocumentName}\"");
                Response.AppendHeader("Content-Length", fileInfo.Length.ToString());
                Response.WriteFile(fileInfo.FullName);
                Response.Flush();
                Response.Close();
                Response.End();
            });
        }

        protected override void LoadData()
        {
            IEnumerable<Irs4506TOrderInfo> orderList = Irs4506TOrderInfo.RetrieveByLoanId(LoanID);
            m_dg.DataSource = orderList;
            m_dg.DataBind();
            m_noRecordLabel.Visible = orderList.Count() == 0;

            BindApplicationList();
        }

        private void BindApplicationList()
        {
            List<ListItem> list = new List<ListItem>();

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Order4506T));
            dataLoan.InitLoad();

            int nApps = dataLoan.nApps;

            for (int i = 0; i < nApps; i++)
            {
                CAppData dataApp = dataLoan.GetAppData(i);

                if (dataApp.aHasSpouse == false)
                {
                    list.Add(new ListItem(dataApp.aBNm, dataApp.aAppId + ":B:" + dataApp.aBNm));
                }
                else
                {
                    if (dataApp.aIs4506TFiledTaxesSeparately)
                    {
                        list.Add(new ListItem(dataApp.aBNm, dataApp.aAppId + ":B:" + dataApp.aBNm));
                        list.Add(new ListItem(dataApp.aCNm, dataApp.aAppId + ":C:" + dataApp.aCNm));
                    }
                    else
                    {
                        // Borrower & Spouse file jointly
                        list.Add(new ListItem(dataApp.aBNm_aCNm, dataApp.aAppId + ":B:" + dataApp.aBNm_aCNm));
                    }
                }
            }

            m_applicationDDL.Items.AddRange(list.ToArray());
        }
    }
}
