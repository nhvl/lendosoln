﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubmitToThirdParty.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.SubmitToThirdParty" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Submit to 3rd Party</title>
</head>
<body class="RightBackground">
    <form id="form1" runat="server">
        <script type="text/javascript">
		function _init()
		{
			if(<%= AspxTools.JsString(m_sResultMessage) %>.length > 0)
			{
				alert(<%= AspxTools.JsString(m_sResultMessage) %>);
			}
		}
	    </script>
        <div>
            <table cellspacing="0" cellpadding="3" width="100%" border="0">
              <tr>
                <td class="MainRightHeader">Submit to 3rd Party</td>
              </tr>
              <tr>
                <td><asp:DropDownList ID="m_PartnerList" runat="server" width="300"></asp:DropDownList></td>
              </tr>
              <tr>
                <td><asp:Button ID="m_submitBtn" Text="Submit" runat="server" /></td>
              </tr>
            </table>
        </div>
    </form>
</body>
</html>
