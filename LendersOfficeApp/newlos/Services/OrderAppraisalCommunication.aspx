﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderAppraisalCommunication.aspx.cs" Inherits="LendersOfficeApp.Newlos.Services.OrderAppraisalCommunication" EnableEventValidation="false" MaintainScrollPositionOnPostBack = "true"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<head runat="server">
    <title>Message</title>
    <link href="../../css/stylesheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        input
        {
            border: default;
            padding: 0;
            height: 1.5em;
        }
        h2
        {
            margin: 0 0 2px 0;
            padding: 2px;
        }
        img
        {
            vertical-align: top;
        }
        .row
        {
            width: 100%;
            margin: 0 auto;
            overflow: hidden;
            font-size: 11px;
        }
        .tableStyle{
            border-color: gainsboro;
            width: 100%;
            border-collapse: collapse;
        }
        .headerSortUp a:nth-child(2){
            display:inline !important;
        }
	    .headerSortDown a:nth-child(2){
            display:inline !important;
        }
        .selectedItem{
            background-color:rgb(8, 83, 147);
            color:white;
        }

        .wrap-container {
            padding-left: 5px;
        }
        .header {
            display: block;
            margin-top: 1em;
            margin-bottom: 1.5em;
        }
        .container {
            margin-left: 1.5em;
        }
        .container-table {
            height: 120px; margin-right: 1.5em;
        }
        .container-message {
            margin-right: 1.5em; margin-bottom: 1em; border: 1px solid #636363;
        }
        .container-message legend { background: gainsboro; }
        .wrap-btn {
            margin-top: 1em;
        }
        .text-center {
            text-align: center;
        }
        #WriteMsgBtn, #SendBtn, #CancelBtn, #ReplyBtn, input[type=button] {
            height: 2em;
            padding-left: 1em;
            padding-right: 1em;
        }
        .no-wrap{
            white-space: nowrap;
        }
        .subject{
            width: 625px;
        }
        .message {
            width: 669px; height: 222px;
        }
        .tableStyle td:first-child {
            padding-left: 4px;
            padding-right: 1em;
        }
        .tableStyle td:first-child span {
            display: inline-block;
            background: transparent;
            width: 3px;
            height: 18px;
            float: left;
            margin-top: -1px;
            margin-bottom: -1px;
            margin-right: 3px;
        }
        .tableStyle .unread td:first-child span{
            background: red;
            margin-left:-1px;
        }
        .table-bi-directional {
            table-layout: fixed; border: 1px solid #636363;
        }
        .table-bi-directional thead, .table-bi-directional tbody {
            display: block;
        }
        .table-bi-directional thead td { border: 0; border-right: 1px solid #636363; }
        .table-bi-directional tbody td { border: 1px solid #636363; }
        .table-bi-directional tbody td:first-child {
            border-left: 0;
        }
        .table-bi-directional tbody {
            overflow: auto;
            height: 84px;
        }
        .table-bi-directional thead td, .table-bi-directional tbody td { padding-left: 4px; }
        .table-bi-directional td:nth-child(1) { width: 115px; }
        .table-bi-directional td:nth-child(2) { width: 70px; }
        .table-bi-directional td:nth-child(3) { width: 70px; }
        .table-bi-directional td:nth-child(4) { width: 404px; border-right: 0; }

        .table-bi-directional[disabled] td {
            opacity: .65;
            cursor: not-allowed !important;
            pointer-events: none;
            color: grey;
        }
        .SortArrow{
            color:white !important;
            text-decoration:none !important;
            display:none;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField id="VendorId" runat="server" />
        <asp:HiddenField id="AppraisalOrderNumber" runat="server" />
        <asp:HiddenField ID="SortExpression" runat="server" />
        <asp:HiddenField ID="SortDirection" runat="server" />
        <asp:HiddenField ID="WriteMode" runat="server" />
        <asp:HiddenField ID="SelectedID" runat="server" />
        <asp:HiddenField ID="UnreadSelectedID" runat="server" />
        <asp:HiddenField id="ScrollPos" runat="server"/>
        <h2 class="MainRightHeader">
            Message
        </h2>
        <div class="wrap-container">
            <label class="header">History</label>
            <div class="container container-table">
                <asp:DataGrid id="history_grid" runat="server" DataKeyField="MessageId" AutoGenerateColumns="False" enableviewstate="False" AllowSorting="true" CssClass="tableStyle table-bi-directional">
                    <AlternatingItemStyle cssclass="GridItem no-wrap"/>
                    <ItemStyle cssclass="GridItem no-wrap"/>
                    <SelectedItemStyle CssClass="selectedItem no-wrap" />
                    <HeaderStyle cssclass="GridHeader no-wrap"/>
                    <FooterStyle cssclass="GridFooter"/>

                    <Columns>
                        <asp:TemplateColumn SortExpression="Date">
                            <HeaderTemplate>
                                <asp:LinkButton runat="server" CommandName="Sort" CommandArgument="Date">Date/Time</asp:LinkButton>
                                <asp:LinkButton runat="server" CommandName="Sort" CommandArgument="Date" CssClass="SortArrow"><%# AspxTools.HtmlString(DisplaySortDirection())%></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="linkButton" CommandName="Select"/>
                                <span></span>
                                <%# AspxTools.HtmlString(DisplayReceivedDate(DataBinder.Eval(Container.DataItem, "Date")))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>

                        <asp:TemplateColumn SortExpression="SendingParty">
                            <HeaderTemplate>
                                <asp:LinkButton runat="server" CommandName="Sort" CommandArgument="SendingParty">From</asp:LinkButton>
                                <asp:LinkButton runat="server" CommandName="Sort" CommandArgument="SendingParty" CssClass="SortArrow"><%# AspxTools.HtmlString(DisplaySortDirection())%></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                            <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "SendingParty"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>

                        <asp:TemplateColumn SortExpression="ReceivingParty">
                            <HeaderTemplate>
                                <asp:LinkButton runat="server" CommandName="Sort" CommandArgument="ReceivingParty">To</asp:LinkButton>
                                <asp:LinkButton runat="server" CommandName="Sort" CommandArgument="ReceivingParty" CssClass="SortArrow"><%# AspxTools.HtmlString(DisplaySortDirection())%></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                            <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ReceivingParty"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>

                        <asp:TemplateColumn SortExpression="Subject">
                            <HeaderTemplate>
                                <asp:LinkButton runat="server" CommandName="Sort" CommandArgument="Subject">Subject</asp:LinkButton>
                                <asp:LinkButton runat="server" CommandName="Sort" CommandArgument="Subject" CssClass="SortArrow"><%# AspxTools.HtmlString(DisplaySortDirection())%></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                            <%# AspxTools.HtmlString(DisplaySubject(DataBinder.Eval(Container.DataItem, "Subject")))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
        <hr/>
        <div class="wrap-container">
            <label class="header" id="titleLb" runat="server"></label>
            <fieldset class="container container-message" id="panel" runat="server">
                <legend id="dateLb" runat="server"></legend>
                <table>
                    <tr>
                        <td><label>From: </label></td>
                        <td><label id="fromLb" runat="server"></label></td>
                    </tr>
                    <tr>
                        <td><label>To:</label></td>
                        <td>
                            <label id="toLb" runat="server"></label>
                            <asp:DropDownList id="appraisalSelector" runat="server">
                                <asp:ListItem>Appraiser</asp:ListItem>
                                <asp:ListItem>AMC</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Subject:</label>
                        </td>
                        <td>
                            <asp:TextBox runat="server" id="subjectTxt" MaxLength="200" CssClass="subject"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <asp:TextBox runat="server" id="contentTxt" TextMode="MultiLine" Columns="85" Rows="18" MaxLength="5000" CssClass="message"></asp:TextBox>
                <div class="wrap-btn text-center">
                    <input id="ReplyBtn" runat="server" value="Reply" type="button"/>
                </div>
                <div class="wrap-btn">
                    <input id="SendBtn" runat="server" value="Send" type="button"/>
                    <input id="CancelBtn" runat="server" value="Cancel" type="button"/>
                </div>
            </fieldset>
        </div>
        <div class="wrap-container">
            <div class="container">
                <input id="WriteMsgBtn" runat="server" value="Write New" type="button"/>
            </div>
        </div>
    </form>
</body>
<script type="text/javascript">
    var hdnScroll = document.getElementById('<%= AspxTools.ClientId(ScrollPos)%>');
    var bigDiv = document.getElementById('<%= AspxTools.ClientId(history_grid)%>').getElementsByTagName('tbody')[0];
    bigDiv.onscroll = function() {
         hdnScroll.value = bigDiv.scrollTop;
    }

    window.onload = function () {
        bigDiv.scrollTop = hdnScroll.value;
        //http://jsfiddle.net/cJjRH/5/
        // scrolling magic
        var table = $('#<%= AspxTools.ClientId(history_grid)%>')[0];
        var selectedRow = $('.selectedItem', table).first();
        if (selectedRow.length) {
            var tableTop = $('tbody',table).position().top;
            var rowTop = selectedRow.position().top;
            var rowBottom = rowTop + selectedRow.height();
            var tableHeight = tableTop + $('tbody', table).height();
            var currentScroll = bigDiv.scrollTop;
            if (rowTop < tableTop) {
                // scroll up
                bigDiv.scrollTop = currentScroll + (rowTop-tableTop);
            }
            else if (rowBottom > tableHeight) {
                // scroll down
                var scrollAmount = rowBottom - tableHeight;
                bigDiv.scrollTop = currentScroll + scrollAmount;
            }
        }
    }

    $(function () {
        $("tr[preset='unread']").each(function () {
            $(this).toggleClass("unread",true);
        });
        var arrowClicked = false;
        $('.tableStyle').keydown(function (evt) {
            if (arrowClicked) {
                evt.preventDefault();
                return false;
            }
            var selectedInside = null;
            if (evt.keyCode == 38) { // up
                selectedInside = $('tbody tr:not(:first-child).selectedItem', this);
                if (selectedInside.length == 0) {
                    arrowClicked = false;
                }
                else {
                    arrowClicked = true;
                    selectedInside.prev().click();
                }
            }
            if (evt.keyCode == 40) { // down
                selectedInside = $('tbody tr:not(:last-child).selectedItem', this);
                if (selectedInside.length == 0) {
                    arrowClicked = false;
                }
                else {
                    arrowClicked = true;
                    selectedInside.next().click();
                }
            }
        });
        var table = $('#<%= AspxTools.ClientId(history_grid)%>')[0];
        <%-- Datagrids dont render the header in a thead but i need it  there  to keep static header --%>
        var head = document.createElement("THEAD");
        //head.appendChild(table.rows[0]);
        head.appendChild($('tr', table)[0]);
        table.insertBefore(head, table.childNodes[0]);
        var writeMode = $('#<%= AspxTools.ClientId(WriteMode)%>').first().value;
        if (writeMode == null || writeMode == "none") {
            table.focus();
        }
    });
</script>
</html>
