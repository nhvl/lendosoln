﻿namespace LendersOfficeApp.newlos.Services
{
    using System;
    using DataAccess;
    using System.Net;
    using System.Text;
    using System.IO;
    using LendersOffice.Constants;

    public partial class SubmitToOSI : LendersOfficeApp.newlos.BaseLoanPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.PageID = "SubmitToOSI";
            this.PageTitle = "Submit To Core";
            this.m_submitBtn.Click += new EventHandler(m_submitBtn_Click);
        }

        void m_submitBtn_Click(object sender, EventArgs e)
        {
            if (BrokerUser.IsInEmployeeGroup(ConstAppDavid.CoreExportEmployeeGroup) == false)
            {
                // 7/5/2013 dd - User does not have permission to perform this export.
                return;
            }

            CPageData dataLoan = this.InitDataLoan();

            // Test Environment - https://testlqb.firsttechfed.com/dropbox/<sLNm>/ 
            // Production Environment - https://lendingqb.firsttechfed.com/dropbox/<sLNm>/

            string testUrl = "https://testlqb.firsttechfed.com/dropbox/" + dataLoan.sLNm;
            string productionUrl = "https://lendingqb.firsttechfed.com/dropbox/" + dataLoan.sLNm;

            // Hardcode to test environment right now.
            string url = productionUrl;
            if (dataLoan.BranchCode.Equals("Test", StringComparison.OrdinalIgnoreCase))
            {
                url = testUrl;
            }

            StringBuilder debug = new StringBuilder();
            debug.AppendLine("[SubmitToOSI] - url=" + url);
            try
            {
                WebRequest webRequest = WebRequest.Create(url);

                webRequest.Method = "GET";
                using (WebResponse webResponse = webRequest.GetResponse())
                {
                    using (Stream stream = webResponse.GetResponseStream())
                    {
                        string content = GetString(stream);
                        debug.AppendLine().AppendLine(content);
                    }
                }

                this.SetFundedDate(dataLoan);
            }
            catch (Exception exc)
            {
                debug.AppendLine().AppendLine("Error: " + exc.ToString());
                throw;
            }
            finally
            {
                Tools.LogInfo(debug.ToString());
            }
        }

        private string GetString(Stream stream)
        {
            const int BUFFER_SIZE = 50000;
            byte[] buffer = new byte[BUFFER_SIZE];
            int n = 0;

            StringBuilder sb = new StringBuilder();
            while ((n = stream.Read(buffer, 0, BUFFER_SIZE)) > 0)
            {
                sb.Append(System.Text.ASCIIEncoding.ASCII.GetString(buffer, 0, n));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Initializes a <see cref="CPageData"/> object.
        /// </summary>
        private CPageData InitDataLoan()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(LoanID, typeof(SubmitToOSI));
            dataLoan.InitLoad();

            return dataLoan;
        }

        /// <summary>
        /// Sets the funded date to the current date.
        /// </summary>
        private void SetFundedDate(CPageData dataLoan)
        {
            if (Broker.SetFundedDateOnOSISubmission && string.IsNullOrEmpty(dataLoan.sFundD_rep))
            {
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.sFundD_rep = DateTime.Now.ToString();
                dataLoan.Save();
            }
        }
    }
}
