﻿namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using global::DocuTech;
    using LendersOffice.Common;
    using LendersOffice.Security;

    public partial class DocuTech : LendersOfficeApp.newlos.BaseLoanPage
    {
        private void Bind_DocuTechPackageType(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("Closing", E_DocumentsPackageType.Closing.ToString("D")));
            ddl.Items.Add(new ListItem("Initial Disclosure", E_DocumentsPackageType.InitialDisclosure.ToString("D")));
        }

        private void Bind_sSettlementChargesExportSource(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("GFE", "0"));
            ddl.Items.Add(new ListItem("Settlement Charges", "1"));
        }
        
        protected void Page_Init(object sender, EventArgs e)
        {
            this.PageID = "DocuTech";
            this.PageTitle = "DocuTech";

            Bind_DocuTechPackageType(ddlDocuTechPackageType);
            Bind_sSettlementChargesExportSource(sSettlementChargesExportSource);
            
            this.EnableJqueryMigrate = false;
            this.RegisterService("loanedit", "/newlos/Services/DocuTechService.aspx");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("ArchiveStatusPopup.js");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DocuTechSavedAuthentication savedAuthentication = DocuTechSavedAuthentication.Retrieve(BrokerUser);

            if (savedAuthentication.UserName == "")
            {
                cbRememberPassword.Checked = false;
            }
            else
            {
                cbRememberPassword.Checked = true;
                tbUsername.Text = savedAuthentication.UserName;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(DocuTech));
            dataLoan.InitLoad();

            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                string errorMessage = null;

                if (dataLoan.sHasLoanEstimateArchiveInPendingStatus)
                {
                    this.RegisterJsGlobalVariables("sHasLoanEstimateArchiveInPendingStatus", true);
                    var archive = dataLoan.sLoanEstimateArchiveInPendingStatus;
                    errorMessage = ErrorMessages.ArchiveError.UnableToGenerateDocumentsBecauseOfPendingArchive(
                        archive.DateArchived,
                        archive.Status,
                        userCanDetermineStatus: true);
                }
                else if (dataLoan.sHasClosingCostArchiveInUnknownStatus)
                {
                    this.RegisterJsGlobalVariables("sHasClosingCostArchiveInUnknownStatus", true);
                    this.RegisterJsGlobalVariables("HasBothCDAndLEArchiveInUnknownStatus", dataLoan.sHasBothCDAndLEArchiveInUnknownStatus);
                    var archive = dataLoan.sClosingCostArchiveInUnknownStatus;
                    errorMessage = ErrorMessages.ArchiveError.UnableToGenerateDocumentsBecuaseOfUnknownArchive(
                        archive.DateArchived,
                        archive.ClosingCostArchiveType,
                        userCanDetermineStatus: true);
                }

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    this.ArchiveStatusPopupMessage.InnerText = errorMessage;
                }
            }

            Tools.SetDropDownListValue(sSettlementChargesExportSource, dataLoan.sSettlementChargesExportSource);
            sIncludeGfeDataForDocMagicComparison.Checked = dataLoan.sIncludeGfeDataForDocMagicComparison;

            bool allowCloserRead = BrokerUser.HasPermission(Permission.AllowCloserRead);
            bool allowCloserWrite = BrokerUser.HasPermission(Permission.AllowCloserWrite);

            if (allowCloserWrite == false)
            {
                sIncludeGfeDataForDocMagicComparison.Enabled = false;
                sSettlementChargesExportSource.Enabled = false;
            }

            RegisterJsGlobalVariables("AllowCloserRead", allowCloserRead);
            RegisterJsGlobalVariables("AllowCloserWrite", allowCloserWrite);

            bool use2015DataLayer = dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy;

            if (use2015DataLayer)
            {
                exportType.Visible = false;
            }
        }
    }
}
