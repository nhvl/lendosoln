﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocuTech.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.DocuTech" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>DocuTech</title>
</head>
<body bgcolor="gainsboro">
  <script type="text/javascript">
    function _init()
    {
      $('#btnExport').click(btnExport_Click);
      $('#btnDownload').click(btnDownload_Click);

      if ($('#cbRememberPassword').prop('checked'))
      {
        $('#tbPassword').val(<%= AspxTools.JsString(ConstAppDavid.FakePasswordDisplay) %>);
      }
      
      if ($('#exportType').is(":visible"))
      {
          $('#sSettlementChargesExportSource').change(sSettlementChargesExportSource_Change);
          
          var sSettlementChargesExportSource = $("#sSettlementChargesExportSource").val();
          if (sSettlementChargesExportSource === "1") {
            $("#divIncludeGfe").show();
          } else {
            $("#divIncludeGfe").hide();
          }
      }
      
      checkCloserPermissions();

      function closeHandler () {
          window.location.href = window.location.href;
      }

      if (ML && ML.sHasLoanEstimateArchiveInPendingStatus) {
          ArchiveStatusPopup.create(
              'ArchiveStatusPopupMessage',
              ML.sLId,
              false,
              ArchiveStatusPopupMode.PendingNoCancel,
              ArchiveStatusPopupSource.NonSeamlessDocumentGeneration,
              closeHandler);
          ArchiveStatusPopup.displayPopup();
      } else if (ML && ML.sHasClosingCostArchiveInUnknownStatus) {
          ArchiveStatusPopup.create(
              'ArchiveStatusPopupMessage',
              ML.sLId,
              false,
              ML.HasBothCDAndLEArchiveInUnknownStatus ? ArchiveStatusPopupMode.HasBothCDAndLEArchiveInUnknownStatus : ArchiveStatusPopupMode.UnknownNoCancel,
              ArchiveStatusPopupSource.NonSeamlessDocumentGeneration,
              closeHandler);
          ArchiveStatusPopup.displayPopup();
      }
  }

    function sSettlementChargesExportSource_Change(evt) {
      var v = $(this).val();
      if (v === "1") {
        $("#divIncludeGfe").show();
      } else {
        $("#divIncludeGfe").hide();
      }
      checkCloserPermissions();
    }
    
    function checkCloserPermissions() {
      var sSettlementChargesExportSource = $("#sSettlementChargesExportSource").val();
      
      if (sSettlementChargesExportSource == "1" && ML.AllowCloserRead == false) 
      {
        document.getElementById("btnExport").disabled = true;
        document.getElementById("btnDownload").disabled = true;
        $("#CloserWarning").show();
      } else {
        $("#CloserWarning").hide();
      }
    }
    function btnDownload_Click() 
    {
      var packageType = $('#ddlDocuTechPackageType').val();
      var url = 'DocuTechDownload.aspx?loanid=' + ML.sLId + '&packagetype=' + packageType + '&rand=' + new Date();
      document.getElementById('DownloadFrame').src = url;
    }
    function btnExport_Click()
    {
      var args = getAllFormValues();

      var result = gService.loanedit.call("Export", args);

      if (!result.error)
      {
        document.getElementById("sFileVersion").value = result.value.sFileVersion;
        if (result.value.HasError === 'False')
        {
          var w = window.open(result.value.Url);
          w.focus();
        } 
        else
        {
          alert(result.value.ErrorMessage);
        }
      } 
      else
      {
        var errMsg = result.UserMessage || 'Unable to export data. Please try again.';
        alert(errMsg);
      }
    }
  </script>
    <form id="form1" runat="server">
    <div>
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
    <tr>
      <td class="MainRightHeader">DocuTech</td>
    </tr>
    </table>
    <table cellspacing="3" cellpadding="0" border="0" style="padding-left:5px">
    <tr>
      <td class="FieldLabel">Package Type</td>
      <td><asp:DropDownList ID="ddlDocuTechPackageType" runat="server" NotForEdit="true" /></td>
    </tr>
    <tr>
      <div id="exportType" runat="server">
          <td class="FieldLabel" colspan="2">Export settlement charges from <asp:DropDownList ID="sSettlementChargesExportSource" runat="server" NotForEdit="true" />
          <div id="divIncludeGfe"><asp:CheckBox runat="server" ID="sIncludeGfeDataForDocMagicComparison" Text="Include GFE data from LendingQB for GFE comparison on HUD-1" NotForEdit="true" /></div>
          </td>
      </div>
    </tr>
    <tr>
      <td class="FieldLabel">User name</td>
      <td><asp:TextBox ID="tbUsername" runat="server" NotForEdit="true" width="150px"/></td>
    </tr>    
    <tr>
      <td class="FieldLabel">Password</td>
      <td><asp:TextBox ID="tbPassword" runat="server" TextMode="Password" NotForEdit="true" width="150px"/></td>
    </tr>
    <tr>
      <td class="FieldLabel" colspan="2"><asp:CheckBox runat="server" ID="cbRememberPassword" Text="Remember logon credentials" NotForEdit="true"/></td>
    </tr>
    <tr>
      <td colspan="2"><input type="button" value="Export to DocuTech ConformX" id="btnExport" />
      <input type="button" value="Download Xml File" id="btnDownload" />
      </td>
    </tr>
    <tr>
      <td colspan="2"><span id="CloserWarning" style="color:Red">"Allow viewing pages in Closer Folder" permission is required to export data from the Settlement Charges page to DocuTech</span></td>
    </tr>
    </table>
    </div>
        <div id="ArchiveStatusPopupMessage" runat="server" style="display: none;"></div>
    </form>
    <iframe id="DownloadFrame" style="display: none;"></iframe>
</body>
</html>
