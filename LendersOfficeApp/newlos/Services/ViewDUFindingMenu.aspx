<%@ Page language="c#" Codebehind="ViewDUFindingMenu.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Services.ViewDUFindingMenu" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
  <head>
    <title>ViewDUFindingMenu</title>
  </head>
  <body class="body-viewDU-finding-menu" MS_POSITIONING="FlowLayout">
  	<script type="text/javascript">
      function f_print() {
        parent.body.focus();
        parent.body.print();
      }
      <% if (m_isDisplaySampleTotalScorePreview) { %>
      function f_viewTotalScore() {
        parent.body.location = 'TemporaryFHATotalFindings.aspx?loanid=' + <%=AspxTools.JsString(LoanID) %>;
      }
      <% } %>
    </script>

    <form id="ViewDUFindingMenu" method="post" runat="server">
      <table border=0 cellspacing=0 cellpadding=5 width="100%">
        <tr>
          <td align="left" valign="top">
            <input type="button" onclick="f_viewTotalScore();" value="Sample TotalScore Certificate Preview" runat="server" id="m_btnViewTotalScore"/>
          </td>
          <td align=right valign=top>
            <input type=button onclick="f_print();" value="Print" AlwaysEnable="true">
            <input type=button onclick="parent.onClosePopup()" value="Close" AlwaysEnable="true">
          </td>
        </tr>
      </table>
     </form>
     <script>
        function subcribeKeys(_window, keyEventMatchedFunc, handler){
          $(_window.document).bind("keydown keyup", function(e){
            if (keyEventMatchedFunc(e)){
              handler();
              return false;
            }
          });
        } 

        $(function() {
          var ctrlAndP = function(keyEvent){
              return keyEvent.ctrlKey && keyEvent.keyCode== 80;
          }

          var windowList =  [window, parent, parent.body];
          $.each(windowList, function(index, entry) {
            subcribeKeys(entry, ctrlAndP, f_print);
          });
        });
     </script>
  </body>
</html>
