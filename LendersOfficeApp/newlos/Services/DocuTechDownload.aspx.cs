﻿using System;
using LendersOffice.Common;
using LendersOffice.Conversions.DocuTech;
using DocuTech;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Services
{
    public partial class DocuTechDownload : MinimalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Guid sLId = RequestHelper.LoanID;
            int v = RequestHelper.GetInt("packagetype", 0);
            
            E_DocumentsPackageType packageType = E_DocumentsPackageType.InitialDisclosure;
            if (v == 1) 
            {
                packageType = E_DocumentsPackageType.Closing;
            } else if (v == 2) {
                packageType = E_DocumentsPackageType.InitialDisclosure;
            }

            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                sLId,
                typeof(DocuTechDownload));
            dataLoan.InitLoad();

            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                string errorMessage = null;

                if (dataLoan.sHasLoanEstimateArchiveInPendingStatus)
                {
                    var archive = dataLoan.sLoanEstimateArchiveInPendingStatus;
                    errorMessage = ErrorMessages.ArchiveError.UnableToGenerateDocumentsBecauseOfPendingArchive(
                        archive.DateArchived,
                        archive.Status,
                        userCanDetermineStatus: false);
                }
                else if (dataLoan.sHasClosingCostArchiveInUnknownStatus)
                {
                    var archive = dataLoan.sClosingCostArchiveInUnknownStatus;
                    errorMessage = ErrorMessages.ArchiveError.UnableToGenerateDocumentsBecuaseOfUnknownArchive(
                        archive.DateArchived,
                        archive.ClosingCostArchiveType,
                        userCanDetermineStatus: false);
                }

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    ErrorMessage.Value = errorMessage;
                    return;
                }
            }

            DocuTechExporter exporter = new DocuTechExporter(sLId, packageType);
            string xml = "";
            try
            {
                xml = exporter.Export("", "", packageType, PrincipalFactory.CurrentPrincipal);
            }
            catch (PermissionException exc)
            {
                ErrorMessage.Value = exc.UserMessage;
                return;
            }
            catch (LendersOffice.Conversions.FeeDiscrepancyException exc)
            {
                ErrorMessage.Value = exc.UserMessage;
                return;
            }
            

            Response.Clear();
            Response.ContentType = "application/text";
            Response.AddHeader("Content-Disposition", $"attachment; filename=\"{exporter.sLNm}.xml\"");

            Response.Write(xml);
            Response.Flush();
            Response.End();
        }
    }
}
