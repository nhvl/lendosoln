﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubmitToMISER.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.SubmitToMISER" EnableViewState="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Submit to MISER</title>
    <style>
    .tn { font-weight:bold; padding-right:10px;}
    .Status_A { color:Green; font-weight:bold;}
    
    .Status_R, .Status_M, .Status_B {color:Red;font-weight:bold;}
    .fnbr { padding-left:10px;padding-right:3px;}
    
    </style>
</head>
<body class="RightBackground">
  <script type="text/javascript">
    function f_download(map,output)
    {
      window.open('SubmitToMISER.aspx?loanid=' + ML.sLId + '&cmd=download&map=' + map + '&output=' + output, 'miser_download');
  }
  function f_submit(btn, map) {
      btn.disabled = true;
      document.getElementById("viewBtn").disabled = true;
        location.href = 'SubmitToMISER.aspx?loanid=' + ML.sLId + '&cmd=submit&map=' + encodeURIComponent(map);
        
        return false;
    }
  </script>
    <form id="form1" runat="server">
    <div>
          <table cellspacing="0" cellpadding="0" width="100%" border="0">
      <tr><td class="MainRightHeader">Submit to MISER</td></tr>
      </table>
      <button onclick="f_submit(this, 'Miser_PML0215'); return false;" id="m_submitBtn" runat="server">Submit To MISER</button>
      <button onclick="f_submit(this, 'Miser_PML0215_Test'); return false;" id="m_submitTestBtn" runat="server">Submit To TEST MISER</button>

      <button onclick="return f_download('Miser_PML0215','html'); return false;" id="viewBtn">View MISER Request</button>
      <ml:PassthroughLiteral ID="m_errorMessageLiteral" runat="server" />
    </div>
    </form>
</body>
</html>
