﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using LendersOffice.GDMS.LookupMethods;
using LendersOffice.ObjLib.Conversions.GlobalDMS;
using LendersOffice.Integration.Appraisals;
using LendersOfficeApp.los.admin;
using System.Text.RegularExpressions;
using DataAccess;
using MeridianLink.CommonControls;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos.Services
{
    public partial class OrderAppraisalEmailLog : BaseLoanPage
    {
        private clsEmailLog[] m_emailLog;

        protected string VendorID
        {
            get { return RequestHelper.GetSafeQueryString("vendorID"); }
        }
        protected string OrderNumber
        {
            get { return RequestHelper.GetSafeQueryString("orderNumber"); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(new Guid(VendorID));

            string errorMessage;
            var credentials = vendor.GetGDMSCredentials(BrokerID, EmployeeID, out errorMessage);
            if (credentials == null)
            {
                ErrorMessage.Value = "Error: " + errorMessage;
                return;
            }

            int orderNumber;
            if (!int.TryParse(OrderNumber, out orderNumber))
            {
                ErrorMessage.Value = "Error: Order Number must be an integer";
                return;
            }

            using (var ordersClient = new OrdersClient(credentials))
            {
                try
                {
                    m_emailLog = ordersClient.GetOrderEmailLog(orderNumber);
                }
                catch (GDMSErrorResponseException exc)
                {
                    StringBuilder errorMsg = new StringBuilder();
                    errorMsg.AppendLine("There was an error processing your request.");
                    errorMsg.AppendFormat(" {0}.", exc.ErrorMessage);
                    foreach (string error in exc.Errors)
                    {
                        errorMsg.AppendFormat(" {0}.", error);
                    }

                    ErrorMessage.Value = errorMsg.ToString();
                    string logMsg = String.Format("GDMS::Error retrieving order info for file #{0}. LoanID: {1}, GDMS Company Id: {2}, GDMS Username: {3}. Error message: {4}",
                                        orderNumber, LoanID, credentials.CompanyId, credentials.UserName, errorMsg.ToString());
                    Tools.LogWarning(logMsg, exc);

                    return;
                }
            }
            EmailLog.DataSource = m_emailLog;
            EmailLog.DataBind();

            string script = string.Format("var VendorID = '{0}'; var OrderNumber = '{1}';", vendor.VendorId, OrderNumber);
            ClientScript.RegisterStartupScript(typeof(OrderAppraisalEmailLog), "UISettings", script, true);
        }
        protected void EmailLog_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            //HtmlAnchor viewLink = args.Item.FindControl("ViewEmailLink") as HtmlAnchor;
            EncodedLiteral emailSubject = (EncodedLiteral)args.Item.FindControl("EmailSubject");
            EncodedLiteral emailFrom = (EncodedLiteral)args.Item.FindControl("EmailFrom");
            EncodedLiteral emailDate = (EncodedLiteral)args.Item.FindControl("EmailDate");
            HtmlAnchor viewLink = args.Item.FindControl("ViewLink") as HtmlAnchor;

            clsEmailLog emailLog = args.Item.DataItem as clsEmailLog;
            if (emailLog != null)
            {
                emailSubject.Text = emailLog.Subject;
                emailFrom.Text = emailLog.FromUserType;
                emailDate.Text = emailLog.DateTimeStamp_String;
                ////string formattedMessage = emailLog.Message
                ////                            .Replace("&amp;", "&")
                ////                            .Replace("&lt;", "<")
                ////                            .Replace("&gt;", ">")
                ////                            .Replace("&quot;", "\"")
                ////                            .Replace("&apos;", "'")
                ////                            .Replace("&#xD;", "\r")
                ////                            .Replace("&#xA;", "\n");
                //emailMessage.Text = emailLog.Message;
                ////emailMessage.Text = formattedMessage;
                //emailLogID.Value = emailLog.EmailLogID.ToString();
                //emailInfoFrom.Value = emailLog.FromUserType;
                //emailInfoTo.Value = emailLog.ToUserType;
                //emailInfoSubject.Value = emailLog.Subject;
                //emailInfoMessage.Value = emailLog.Message;
                viewLink.HRef = string.Format(
                    "javascript:viewEmail('/newlos/Services/OrderAppraisalEmailMessage.aspx?loanid={0}&mode={1}&vendorID={2}&orderNumber={3}&emailLogID={4}');",
                    AspxTools.JsStringUnquoted(LoanID.ToString()),
                    "view",
                    AspxTools.JsStringUnquoted(VendorID),
                    AspxTools.JsStringUnquoted(OrderNumber),
                    AspxTools.JsStringUnquoted(emailLog.EmailLogID.ToString())
                );
            }

        }
        protected void Page_Init(object sender, EventArgs e)
        {
            DisplayCopyRight = false;
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }
    }
}
