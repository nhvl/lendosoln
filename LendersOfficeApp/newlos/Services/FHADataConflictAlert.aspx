﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FHADataConflictAlert.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.FHADataConflictAlert" %>
<%@ Register TagPrefix="ML" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
		<title>FHA Data Conflict</title>
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet"/>
        <style type="text/css">
            #DataConflictTable
            {
                width: 100%;
                border-collapse: collapse;
            }
            #DataConflictTable td, #DataConflictTable th
            {
                padding: 3px;
            }
            #DataConflictTable th
            {
                background-color: #999999;
                font-weight: bold;
                color: White;
            }
        </style>
	</head>
    <body MS_POSITIONING="FlowLayout" scroll="no" onload="onInit();">
        <script>
            function onInit()
            {
                resizeForIE6And7(600, 430);
            }

            function EnableSubmitButton()
            {
                var sbtn = document.getElementById('SubmitBtn');
                sbtn.disabled = false;
            }

            function onClick(btn) {
                btn.disabled = true;
                var cancel = document.getElementById('<%= AspxTools.ClientId(m_cancelButton) %>');
                cancel.disabled = true;

                window.setTimeout(function() {

                    var radio = document.getElementsByName('m_conflictChoices');
                    var value;
                    for (var i = 0; i < radio.length; i++) {
                        if (radio[i].checked) { value = radio[i].value; }
                    }

                    var arg = window.dialogArguments || {};
                    arg.usercommand='submit';
                    arg.choice= value === '0' ? '1003' : 'TransmittalSummary';
                    onClosePopup(arg);

                }, 100 );
            }
        </script>
    <h4 class="page-header">FHA TOTAL Data Conflict</h4>
    <form id="form1" runat="server">
        <table width="100%" border="0" cellspacing="2" cellpadding="3">
            <tr>
                <td class="FieldLabel" style="padding-top:10px;padding-bottom:10px;padding-left:20px">
                    The following fields are inconsistent between the forms you are uploading to FHA Connection:
                </td>
            </tr>
            <tr>
                <td style="padding-left:20px;padding-right:20px">
                    <div style="HEIGHT: 200px;">
                        <table id="DataConflictTable" border="2">
                            <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>1003</th>
                                    <th>FHA Transmittal Summary</th>
                                </tr>
                            </thead>
                            <tbody>
                        <asp:Repeater ID="DataConflictRows" runat="server">
                            <ItemTemplate>
                                <tr class="Grid<%# AspxTools.HtmlString(Container.ItemIndex % 2 == 0 ? "Alternating" : null) %>Item">
                                    <td><%# AspxTools.HtmlString(GetItem(Container).Description) %></td>
                                    <td><%# AspxTools.HtmlString(GetItem(Container).Value1003) %></td>
                                    <td><%# AspxTools.HtmlString(GetItem(Container).ValueFhaTransmittalSummary) %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel" style="padding-top:10px;padding-left:20px">
                    For the conflicting data:
                </td>
            </tr>
            <tr>
                <td style="padding-left:20px">
                    <asp:RadioButtonList ID="m_conflictChoices" RepeatDirection="Vertical" runat="server" onclick="EnableSubmitButton();">
                        <asp:ListItem Text="Submit the 1003 data to FHA Connection" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Submit the FHA Transmittal Summary data to FHA Connection" Value="1"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr >
                <td colspan="3" align="center" style="padding-top:20px">
                    <input type="button" id="SubmitBtn" style="width:70px" value="Submit"  disabled="disabled" onclick="onClick(this)" />
                     &nbsp;
                    <asp:Button ID="m_cancelButton" Width="70px" Text="Cancel" OnClick="OnCancelClick" runat="server" />
                </td>
            </tr>
        </table>
    <ML:CModalDlg id="m_ModalDlg" runat="server"></ML:CModalDlg>
    </form>
</body>
</html>
