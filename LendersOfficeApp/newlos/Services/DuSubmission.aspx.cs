using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

using LendersOffice.Common;
using DataAccess;
using System.Data.SqlClient;
using LendersOffice.DU;
using LendersOffice.AntiXss;
using LendersOffice.ObjLib.ServiceCredential;
using System.Linq;

namespace LendersOfficeApp.newlos.Services
{
	public partial class DuSubmission : LendersOfficeApp.newlos.BaseLoanPage
	{
        protected int m_nApps = 1;
        protected bool m_hasDuAutoLogin = false;
        protected void PageLoad(object sender, System.EventArgs e)
        {

        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(DuSubmission));
            dataLoan.InitLoad();

            sDuCaseId.Text = dataLoan.sDuCaseId;
            sDuLenderInstitutionId.Text = dataLoan.sDuLenderInstitutionId;

            FnmaXisDuUnderwriteResponse underwriteResponse = new FnmaXisDuUnderwriteResponse(LoanID);
            if (underwriteResponse.IsValid)
            {
                CraProviderName.SelectedValue = underwriteResponse.CreditProviderAgency;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("<table><tr><td class='FieldLabel'>Borrower(s)</td><td class='FieldLabel'>Credit Report ID</td></tr>");
            m_nApps = dataLoan.nApps;
            for (int i = 0; i < m_nApps; i++)
            {
                CAppData dataApp = dataLoan.GetAppData(i);
                string creditReference = GetExistingCreditReference(dataApp.aAppId);

                string separator = null;
                if (!string.IsNullOrEmpty(dataApp.aCNm))
                {
                    separator = " & ";
                }

                sb.Append(
                    $@"<tr><td class=""fieldlabel"">{AspxTools.HtmlString(dataApp.aBNm + separator + dataApp.aCNm)
                    }</td><td><input type=""text"" name=""CreditReference{i}"" id=""CreditReference{i}"" value=""{AspxTools.HtmlString(creditReference)}""></td></tr>");

                if (!string.IsNullOrEmpty(creditReference))
                {
                    UseExisting.Checked = true;
                    ImportLiabitiesFromCreditReport.Checked = false;
                }
            }

            sb.Append("</table>");
            ResubmitCreditHtml.Text = sb.ToString();
            m_hasDuAutoLogin = false;
            if (BrokerUser.BrokerDB.UsingLegacyDUCredentials)
            {
                if (BrokerUser.HasDuAutoLogin)
                {
                    if (string.IsNullOrEmpty(BrokerUser.DuAutoLoginNm) == false)
                    {
                        FannieMaeMORNETUserID.Text = BrokerUser.DuAutoLoginNm;
                        FannieMaeMORNETUserID.Enabled = false;
                        FannieMaeMORNETUserID.ReadOnly = true;
                    }
                    if (string.IsNullOrEmpty(BrokerUser.DuAutoPassword) == false)
                    {
                        FannieMaeMORNETPassword.Enabled = false;
                        FannieMaeMORNETPassword.ReadOnly = true;
                    }

                    m_hasDuAutoLogin = true;
                }
            }
            else
            {
                var savedCredential = ServiceCredential.ListAvailableServiceCredentials(BrokerUser, dataLoan.sBranchId, ServiceCredentialService.AusSubmission)
                                            .FirstOrDefault(credential => credential.ChosenAusOption.HasValue && credential.ChosenAusOption.Value == AusOption.DesktopUnderwriter);
                if (savedCredential != null)
                {
                    FannieMaeMORNETUserID.Text = savedCredential.UserName;
                    FannieMaeMORNETUserID.Enabled = false;
                    FannieMaeMORNETUserID.ReadOnly = true;

                    FannieMaeMORNETPassword.Enabled = false;
                    FannieMaeMORNETPassword.ReadOnly = true;

                    m_hasDuAutoLogin = true;
                }
            }

            if (BrokerUser.HasDuAutoLogin)
            {
                if (string.IsNullOrEmpty(BrokerUser.DuAutoCraId) == false)
                {
                    CraProviderName.SelectedValue = BrokerUser.DuAutoCraId;
                    CraProviderName.Enabled = false;
                }
                if (string.IsNullOrEmpty(BrokerUser.DuAutoCraLogin) == false)
                {
                    CraLoginName.Enabled = false;
                    CraLoginName.ReadOnly = true;
                }
                if (string.IsNullOrEmpty(BrokerUser.DuAutoCraPassword) == false)
                {
                    CraPassword.Enabled = false;
                    CraPassword.ReadOnly = true;
                }
            }
        }

        private string GetExistingCreditReference(Guid aAppId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@ApplicationID", aAppId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerID, "RetrieveCreditReport", parameters))
            {
                if (reader.Read())
                {
                    return (string)reader["ExternalFileID"];
                }
            }
            return string.Empty;
        }
        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageID = "DuSubmission";
            this.PageTitle = "DU Submission";

            CraProviderName.DataSource = DUServer.GetCreditProviderList();
            CraProviderName.DataValueField = "Key";
            CraProviderName.DataTextField = "Value";
            CraProviderName.DataBind();
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
