﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using System.Xml;
using System.Text;
using System.IO;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Services
{
    public partial class FHATotalFindings : LendersOfficeApp.newlos.BaseLoanPage
    {
#if LQB_NET45
        protected global::System.Web.UI.HtmlControls.HtmlIframe Certificate;
#else
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl Certificate;
#endif

        protected void Page_Load(object sender, EventArgs e)
        {
            DisplayCopyRight = false;
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHATotalFindings));
            dataLoan.InitLoad();
            if (RequestHelper.GetInt("viewcert", -1) != -1)
            {
    
                Response.Clear();
                Response.Output.WriteLine(dataLoan.sTotalScoreCertificateHtml.Value);
                Response.Flush();
                Response.End();
            }

            if (string.IsNullOrEmpty(dataLoan.sTotalScoreCertificateXmlContent.Value))
            {
                conditionImport.Visible = false;
            }

        
            this.RegisterService("FHATotal", "/newlos/services/FHATotalSubmitService.aspx");
            this.RegisterJsScript("LQBPrintFix.js");
            Certificate.Attributes.Add("src", "FHATotalFindings.aspx?viewcert=1&loanid=" + LoanID);
            Certificate.Attributes.Add("onload", "f_resizeIframe('Certificate',10);");
        }
    }
}
