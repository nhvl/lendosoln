﻿// <copyright file="OrderMIQuoteResponseViewer.aspx.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/17/2014 3:49:10 PM
// </summary>
namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.MortgageInsurance;
    using LendersOffice.Security;

    /// <summary>
    /// Implements the MI Quote Response dialog.
    /// </summary>
    public partial class OrderMIQuoteResponseViewer : BaseLoanPage
    {
        /// <summary>
        /// Gets the transaction ID assigned to the quote by LQB from the query string.
        /// </summary>
        /// <value>The transaction ID associated with the MI quote.</value>
        protected Guid TransactionID
        {
            get
            {
                return RequestHelper.GetGuid("TransactionId");
            }
        }

        /// <summary>
        /// Gets the order number assigned to the quote by LQB from the query string.
        /// </summary>
        /// <value>The order number associated with the MI quote.</value>
        protected string OrderNumber
        {
            get
            {
                return RequestHelper.GetSafeQueryString("OrderNumber");
            }
        }

        /// <summary>
        /// Override compatibility mode.
        /// </summary>
        /// <returns>Edge mode.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;
            base.OnInit(e);
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.PageTitle = "MI Quote Response Viewer";
            this.PageID = "MIQuoteResponseViewer";

            this.EnableJqueryMigrate = false;
            this.RegisterService("OrderMIPolicy", "/newlos/Services/OrderMIPolicyService.aspx");
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("SimplePopups.js");

            this.Premiums.ItemDataBound += new DataGridItemEventHandler(this.PremiumsGrid_ItemDataBound);
        }

        /// <summary>
        /// Loads data for the page.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            MIOrderInfo order = MIOrderInfo.RetrieveTransaction(this.TransactionID, this.OrderNumber, LoanID, BrokerID, ApplicationID);

            if (order == null)
            {
                this.quoteStatus.Text = "Error";
                this.vendorMessage.Text = ErrorMessages.MIFramework.QuoteRetrievalFailure();
                return;
            }

            this.quoteDate.Text = (order.ResponseProvider.ResponseDateTime == DateTime.MinValue ? order.OrderedDate : order.ResponseProvider.ResponseDateTime).ToString("G");
            this.QuoteId.Text = (order.ResponseProvider.QuoteNumber == null ? string.Empty : order.ResponseProvider.QuoteNumber);

            DateTime quoteExpDate = DateTime.MinValue;
            if (order.ResponseProvider.HasResponseInfo)
            {
                quoteExpDate = MIUtil.SafeDateTime(order.ResponseProvider.ResponseInfo.MIRateQuoteExpirationDate);
            }

            this.quoteExpiration.Text = quoteExpDate == DateTime.MinValue ? string.Empty : quoteExpDate.ToString("d");
            this.quoteStatus.Text = order.ResponseProvider.Status;
            this.vendorMessage.Text = order.VendorMessage;

            if (string.IsNullOrEmpty(this.vendorMessage.Text))
            {
                this.vendorMessage.Text = "View quote details below";
            }

            this.Disclaimer.Text = order.ResponseProvider.QuoteDisclaimer;

            var vendorType = E_sMiCompanyNmT.LeaveBlank;
            string policyInfo = string.Empty;
            try
            {
                // For populating the UI, we can read invalid configs,
                // since they may have been removed after orders were placed.
                var vendorConfig = MortgageInsuranceVendorConfig.RetrieveById(order.VendorID, allowInvalid: true);
                vendorType = vendorConfig.VendorType;
                policyInfo = vendorConfig.VendorTypeFriendlyDisplay;
            }
            catch (NotFoundException)
            {
            }

            policyInfo = string.Format(
                "{0}, {1}, {2}% Coverage, {3}, {4}",
                policyInfo,
                Tools.Get_sProdConvMIOptionTFriendlyDisplay(order.PremiumType),
                order.CoveragePercent.ToString("F4"),
                MIUtil.GetMIPremiumRefundableTypeDisplay(order.Refundability),
                MIUtil.GetMIRenewalCalculationTypeDisplay(order.RenewalType));

            this.policyInformation.Text = policyInfo;
            this.premiumTerm.Text = this.GetPremiumTerm(order, vendorType);
            this.renewalPremiumTerm.Text = order.ResponseProvider.RenewalPremiumTerm == 0 ? "NA" : order.ResponseProvider.RenewalPremiumTerm.ToString() + " months";

            if (order.ResponseProvider.PremiumType == E_sProdConvMIOptionT.BorrPaidMonPrem)
            {
                if (order.ResponseProvider.InitialPremiumTerm == 0)
                {
                    initialPremiumTerm.Text = "N/A";
                }
                else if (order.ResponseProvider.InitialPremiumTerm == 1)
                {
                    initialPremiumTerm.Text = "1 month";
                }
                else
                {
                    initialPremiumTerm.Text = order.ResponseProvider.InitialPremiumTerm + " months";
                }
            }
            else
            {
                intialPremiumRow.Visible = false;
            }

            IEnumerable<MortgageInsurancePremium> premiumList = order.ResponseProvider.Premiums;
            this.Premiums.DataSource = premiumList;
            this.Premiums.DataBind();

            var user = PrincipalFactory.CurrentPrincipal;
            bool canRequestBeDelegated;
            bool hasCredentials;

            try
            {
                canRequestBeDelegated = MIUtil.CanRequestBeDelegated(user, order.VendorID);
                hasCredentials = true;
            }
            catch(CBaseException exc) when (exc.UserMessage == ErrorMessages.MIFramework.NoCredentials())
            {
                // We were unable to load credentials, they may have been deleted since this order.
                // The UI should still render, but disable the related controls per opm 471956.
                canRequestBeDelegated = false;
                hasCredentials = false;
            }

            Tuple<bool, string> policyDelegatedResult = Tools.IsWorkflowOperationAuthorized(user, this.LoanID, LendersOffice.ConfigSystem.Operations.WorkflowOperations.OrderMIPolicyDelegated);
            bool canOrderPolicyDelegated = policyDelegatedResult.Item1;
            string policyDelegatedDenialMessage = string.IsNullOrEmpty(policyDelegatedResult.Item2) ? ErrorMessages.GenericAccessDenied : policyDelegatedResult.Item2;

            Tuple<bool, string> policyNonDelegatedResult = Tools.IsWorkflowOperationAuthorized(user, this.LoanID, LendersOffice.ConfigSystem.Operations.WorkflowOperations.OrderMIPolicyNonDelegated);
            bool canOrderPolicyNonDelegated = policyNonDelegatedResult.Item1;
            string policyNonDelegatedDenialMessage = string.IsNullOrEmpty(policyNonDelegatedResult.Item2) ? ErrorMessages.GenericAccessDenied : policyNonDelegatedResult.Item2;

            RegisterJsGlobalVariables("CanOrderPolicyNonDelegated", canOrderPolicyNonDelegated);
            RegisterJsGlobalVariables("PolicyNonDelegatedDenialReason", policyNonDelegatedDenialMessage);
            RegisterJsGlobalVariables("CanOrderPolicyDelegated", canOrderPolicyDelegated);
            RegisterJsGlobalVariables("PolicyDelegatedDenialReason", policyDelegatedDenialMessage);
            RegisterJsGlobalVariables("CanRequestBeDelegated", canRequestBeDelegated);
            RegisterJsGlobalVariables("HasCredentials", hasCredentials);
            RegisterJsGlobalVariables("BranchId", BrokerUser.BranchId);
        }

        /// <summary>
        /// Obtains the premium term for the specified <paramref name="order"/>
        /// based on the type of premium and the specified <paramref name="vendorType"/>.
        /// </summary>
        /// <param name="order">
        /// The <see cref="MIOrderInfo"/> to obtain the premium term.
        /// </param>
        /// <param name="vendorType">
        /// The vendor for the MI order.
        /// </param>
        /// <returns>
        /// The number of months for the premium term or "NA" if the premium is 
        /// single payment.
        /// </returns>
        private string GetPremiumTerm(MIOrderInfo order, E_sMiCompanyNmT vendorType)
        {
            var term = order.ResponseProvider.MonthlyPremiumTerm;

            if (order.ResponseProvider.SplitPremium && vendorType != E_sMiCompanyNmT.MGIC)
            {
                var rateDurationMonths = 0;

                int.TryParse(order.ResponseProvider.ResponseInfo.MIInitialPremiumRateDurationMonths, out rateDurationMonths);

                term += rateDurationMonths;
            }

            return term == 0 ? "NA" : term.ToString() + " months";
        }

        /// <summary>
        /// Binds a data grid item to a list of MI premiums and/or taxes.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event arguments.</param>
        private void PremiumsGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer)
            {
                return;
            }

            MortgageInsurancePremium premium = (MortgageInsurancePremium)e.Item.DataItem;
        }
    }
}
