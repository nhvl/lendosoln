﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Services
{
    public partial class AutoDisclosureAuditResults : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.DisplayCopyRight = false;

            var loan = CPageData.CreateUsingSmartDependency(
                this.LoanID,
                typeof(AutoDisclosureAuditResults));

            loan.InitLoad();

            this.LoanNumber.Text = loan.sLNm;

            this.AuditResultHtml.Text = loan.sAutoDisclosureAuditHtml;
        }
    }
}
