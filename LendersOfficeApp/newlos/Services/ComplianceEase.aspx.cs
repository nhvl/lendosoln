﻿using System;
using System.Web.UI.WebControls;
using ComplianceEase;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Services
{
    public partial class ComplianceEase : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.m_auditType.Items.Add(new ListItem("Pre-Close Mortgage Loan", E_TransmittalDataComplianceAuditType.PreClose.ToString("D")));
            this.m_auditType.Items.Add(new ListItem("Post-Close Mortgage Loan", E_TransmittalDataComplianceAuditType.PostClose.ToString("D")));

            string fileDbKey = LoanID.ToString("N") + "_COMPLIANCE_EASE_REPORT_PDF";
            bool hasAuditReport = FileDBTools.DoesFileExist(E_FileDB.Normal, fileDbKey);

            RegisterJsGlobalVariables("HasReport", hasAuditReport);

            ComplianceEaseSavedAuthentication savedAuthentication = ComplianceEaseSavedAuthentication.Retrieve(BrokerUser);

            if (savedAuthentication.UserName == "")
            {
                m_rememberMe.Checked = false;
            }
            else
            {
                m_rememberMe.Checked = true;
                m_userName.Text = savedAuthentication.UserName;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(ComplianceEase));
            dataLoan.InitLoad();
            this.sComplianceEaseId.Text = dataLoan.sComplianceEaseId;
            this.sComplianceEaseStatus.Text = dataLoan.sComplianceEaseStatus;
            if (dataLoan.sLoanFileT == E_sLoanFileT.Test)
            {
                TestLoanPanel.Visible = true;
            }

            if (dataLoan.sIsInitialLoanEstimateDateAssociatedWithInvalidArchive)
            {
                this.ErrorMessageDiv.InnerText = ErrorMessages.ArchiveError.InitialLoanEstimateAssociatedWithInvalidArchiveCompliance;
                this.btnSubmit.Visible = false;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            this.PageID = "ComplianceEase";
            this.PageTitle = "ComplianceEase Integration";
            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;

            base.OnInit(e);
        }
    }
}