﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.XsltExportReport;
using LendersOffice.Integration.Cohesion;
using System.Text;
using LendersOffice.Constants;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Services
{
    public partial class SubmitToMISER : LendersOfficeApp.newlos.BaseLoanPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (BrokerUser.IsInEmployeeGroup(ConstAppDavid.CoreExportEmployeeGroup) == false)
            {
                // 7/5/2013 dd - User does not have permission to perform this export.
                return;
            }
            // 10/13/2014 dd - OPM 189320 - Don't perform additional field protection and workflow check here. As long as user 
            // has permission to export to Core.
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.LoanID, typeof(SubmitToMISER));

            dataLoan.InitLoad();

            if (dataLoan.BranchNm == "MISER TESTING")
            {
                m_submitTestBtn.Visible = true;
            }
            else
            {
                m_submitTestBtn.Visible = false;
            }

            List<string> denialReasonList = new List<string>();
            if (IsAllowSubmit(dataLoan, denialReasonList) == false)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("<div style='color:red'>Cannot submit to MISER.");
                sb.AppendLine("<ul>");
                foreach (var reason in denialReasonList)
                {
                    sb.AppendLine("<li>" + reason + "</li>");
                }
                sb.AppendLine("</ul></div>");
                m_errorMessageLiteral.Text = sb.ToString();

                m_submitBtn.Disabled = true;
                m_submitTestBtn.Disabled = true;
                return;
            }

            if (Page.IsPostBack == false)
            {
                if (Request.QueryString["cmd"] == "download")
                {
                    if (Request.QueryString["output"] == "html")
                    {
                        ViewFriendlyDebugHtml();
                    }
                    else
                    {
                        DownloadXml();
                    }
                }
                else if (Request.QueryString["cmd"] == "submit")
                {
                    SubmitMiser();
                }
            }
        }

        private bool IsAllowSubmit(CPageData dataLoan, List<string> denialReasonList)
        {

            bool isAllow = true;


            if (dataLoan.sRateLockStatusT != E_sRateLockStatusT.Locked)
            {
                isAllow = false;
                denialReasonList.Add("Rate Lock Status is not locked.");
            }
            else if (dataLoan.sFundD_rep == "")
            {
                // 10/13/2014 dd - Booking a hard stop.
                isAllow = false;
                denialReasonList.Add("Funding Date is required.");
            }

            return isAllow;
        }
        private bool VerifyMapName(string mapName)
        {
            try
            {
                XsltMapItem mapItem = XsltMap.Get(mapName);
                return mapItem.IsAllow(Broker);
            }
            catch (KeyNotFoundException)
            {
                
            }
            return false;
        }
        private void SubmitMiser()
        {
            string mapName = Request.QueryString["map"];

            if (VerifyMapName(mapName) == false)
            {
                m_errorMessageLiteral.Text = "<div style='color:red'>Invalid map=[" + mapName + "]. Contact Support.</div>";
                return;
            }
            CohesionRequest request = new CohesionRequest(this.BrokerUser.UserId, this.LoanID, mapName, PrincipalFactory.CurrentPrincipal);
            CohesionResponse response = CohesionServer.Submit(request);

            StringBuilder sb = new StringBuilder();
            int totalCount = response.ReplyList.Count();
            int acceptCount = 0;
            sb.Append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
            foreach (var o in response.ReplyList)
            {
                CohesionXmlReply reply = o.Value;
                if (reply.Status == "A")
                {
                    acceptCount++;
                }
                //sb.AppendLine("Transaction: " + o.Key);
                //sb.AppendLine("   Mode: " + reply.Mode);
                //sb.AppendLine("   Status: " + reply.StatusDescription);
                sb.AppendFormat("<tr><td class=\"tn\">Transaction: {0}</td><td class=\"Status_{1}\">{2}</td></tr>", o.Key, 
                    reply.Status, reply.StatusDescription);
                sb.AppendLine();
                sb.AppendFormat("<tr><td colspan=\"2\">&nbsp;</td></tr>");
                foreach (var field in reply.FieldList)
                {
                    sb.AppendFormat("<tr><td class=\"fnbr\">FNbr:{0}</td><td class=\"fdata\">{1}</td></tr>", field.FieldNbr, field.FieldData);
                    sb.AppendLine();
                    //sb.AppendLine("        FieldNbr:" + field.FieldNbr + ", FieldData:" + field.FieldData);
                }
                sb.AppendFormat("<tr><td colspan=\"2\">&nbsp;</td></tr>");

                sb.AppendLine();
                //sb.AppendLine();
            }
            sb.Append("</table>");

            string divStatus = string.Format("<div>Total Transactions:{0}  Accepted Transactions:{1}  Error Transactions:{2}</div>",
                totalCount, acceptCount, totalCount - acceptCount);
            m_errorMessageLiteral.Text = divStatus + sb.ToString();

        }

        private void DownloadXml()
        {
            string mapName = Request.QueryString["map"];

            if (VerifyMapName(mapName) == false)
            {
                m_errorMessageLiteral.Text = "<div style='color:red'>Invalid map=[" + mapName + "]. Contact Support.</div>";
                return;
            }

            CohesionRequest request = new CohesionRequest(this.BrokerUser.UserId, this.LoanID, mapName, PrincipalFactory.CurrentPrincipal);

            Response.Clear();
            Response.ContentType = "text/xml";
            Response.AddHeader("Content-Disposition", "inline; filename=\"miser.xml\"");

            Response.Write(request.ToXmlString());
            Response.Flush();
            Response.End();
        }

        private void ViewFriendlyDebugHtml()
        {
            string mapName = Request.QueryString["map"];

            if (VerifyMapName(mapName) == false)
            {
                m_errorMessageLiteral.Text = "<div style='color:red'>Invalid map=[" + mapName + "]. Contact Support.</div>";
                return;
            }
            CohesionRequest request = new CohesionRequest(this.BrokerUser.UserId, this.LoanID, mapName, PrincipalFactory.CurrentPrincipal);

            Response.Clear();
            Response.ContentType = "text/html";

            XslTransformHelper.TransformFromEmbeddedResource("LendersOffice.Integration.Cohesion.CohesionDebugRequest.xsl", request.ToXmlString(), Response.OutputStream, null);
            Response.Flush();
            Response.End();

        }
    }
}
