﻿// <copyright file="ComplianceEagleResultSummary.aspx.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Brian Beery
//    Date:   8/13/2014 05:00 PM 
// </summary>
namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Web.UI;
    using LendersOffice.Common;
    using LendersOffice.Conversions.ComplianceEagleIntegration;

    /// <summary>
    /// The result summary page for the ComplianceEagle integration. Used primarily to display results of automated requests but will display the latest results regardless of whether the request was automated or manual.
    /// </summary>
    public partial class ComplianceEagleResultSummary : BaseLoanPage
    {
        /// <summary>
        /// The response from ComplianceEagle in HTML format.
        /// </summary>
        private string responseHTMLContent;

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="args">The event arguments.</param>
        protected override void OnInit(EventArgs args)
        {
            DisplayCopyRight = false;
            base.OnInit(args);
            this.EnableJqueryMigrate = false;
        }

        /// <summary>
        /// Loads data for the page from the user account and loan file.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.ShowPriorResponse();
        }

        /// <summary>
        /// Render the XML response from ComplianceEagle as HTML.
        /// This is copied from the ComplianceEagle page in the services folder.
        /// </summary>
        private void DisplayResult()
        {
            resultsPlaceholder.Visible = true;
            noResponsePlaceholder.Visible = false;

            resultsPlaceholder.SetRenderMethodDelegate(new RenderMethod(delegate(HtmlTextWriter tw, Control container)
            {
                tw.Write(this.responseHTMLContent);
            }));
        }

        /// <summary>
        /// Load the latest prior response on file and render as HTML.
        /// This is copied from the ComplianceEagle page in the services folder.
        /// </summary>
        private void ShowPriorResponse()
        {
            if (!Page.IsPostBack)
            {
                ComplianceEagleStatusHelper responseConvertor = new ComplianceEagleStatusHelper(LoanID);
                this.responseHTMLContent = responseConvertor.GetResponseHTMLFromResponseXML();

                if (!string.IsNullOrEmpty(this.responseHTMLContent))
                {
                    this.DisplayResult();
                }
                else
                {
                    noResponsePlaceholder.Visible = true;
                }

                if (responseConvertor.IsLoanEstimateDateDataCurrentlyInvalid)
                {
                    this.warningCell.InnerText = ErrorMessages.ArchiveError.InitialLoanEstimateAssociatedWithInvalidArchiveAsyncCompliance;
                }
                else
                {
                    this.warningRow.Visible = false;
                }
            }
        }
    }
}
