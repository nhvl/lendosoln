﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComplianceEaseErrorSummary.aspx.cs"
    Inherits="LendersOfficeApp.newlos.Services.ComplianceEaseErrorSummary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="LendersOffice.AntiXss"%>
<%@ Import Namespace="ComplianceEase" %>
<%@ Import Namespace="LendersOffice.Conversions.ComplianceEase" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function AuditPostSave(idx) {
            jQuery('#ComplianceErrors ul li').eq(idx).addClass('complete');
        }
        jQuery(function($) {
            $('#Close').click(function() { onClosePopup() });
            $('#ComplianceErrors').on('click', '.edit-field', function () {
                var page = $(this).siblings('.page').val();
                var field = $(this).siblings('.field').val();
                var url = buildUrl(page, field);

                var myIndex = $(this).parent().index();

                var currentQuery;
                var dlgArgs = getModalArgs();
                if (dlgArgs) currentQuery = new RegExp('&AuditPostSaveId=([^&#]*)').exec(dlgArgs.body.location.search)
                if (!currentQuery) currentQuery = [0, 0];
                var current = currentQuery[1];

                if (dlgArgs && dlgArgs.body.f_smartConfirm(null, null, null, function() { AuditPostSave(current) })) {
                    dialogArguments.body.location = url + "&AuditPostSaveId=" + myIndex;
                }
            });
            $('#Rerun').click(function() {
                $('#RerunAudit').val("Please wait...").prop('disabled', true);
                var DTO = {
                    LoanID: ML.sLId
                };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: 'ComplianceEaseErrorSummary.aspx/RerunAudit',
                    data: JSON.stringify(DTO),
                    dataType: 'json',
                    async: true
                }).then(
                    function(msg) {
                        //This is what you have to go through to refresh a modeless window
                        window.name = "CEAudit"
                        window.open(window.location, "CEAudit");
                    },
                    function(XMLHttpRequest, textStatus, errorThrown) {
                        var message = textStatus + ": " + errorThrown;
                        alert(message);
                    }
                );
            });

            function buildUrl(page, field) {
                var delim = '&';
                if (page.indexOf('?') == -1) delim = '?';
                return '../' + page + delim + "highlightId=" + encodeURIComponent(field) + "&loanid=" + ML.sLId + "&appid=" + ML.aAppId;
            }
        });
    </script>
    <style type="text/css">
    #Controls
    {
        margin-left: 35%;
    }
    body
    {
        background-color: gainsboro;
    }
    #ComplianceErrors
    {
        height:300px;
        width:94%;
        margin-left: 2%;
        margin-top: 5px;
        margin-bottom: 5px;
        overflow-y: scroll;
        border: solid 1px black;
    }
    #ErrorList
    {
        list-style-type: none;
        padding: 2px;
    }
    #ErrorList li
    {
        margin:0px;
        margin-bottom: 5px;
    }
    .edit-field
    {
        display: none;
        text-decoration: underline;
        color: Blue;
        cursor: pointer;
    }
    .has-field .edit-field
    {
        display: inline;
    }
    .complete
    { 
        text-decoration: line-through;
    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="FormTableHeader">
            ComplianceEase Errors
        </div>
        <div runat="server" id="customError" style="color:red;">
        </div>
        <div id="ComplianceErrors">
            <asp:Repeater runat="server" ID="Errors">
                <HeaderTemplate>
                    <ul id="ErrorList">
                </HeaderTemplate>
                <ItemTemplate>
                    <li class="<%#AspxTools.HtmlString(GetClass(Container.DataItem))%>">
                        <%#AspxTools.HtmlString(((LendersOffice.ObjLib.Conversions.ComplianceEase.FriendlyError)Container.DataItem).Message)%>
                        <span class="edit-field">edit</span>
                        <input class="page" type="hidden" value="<%#AspxTools.HtmlString(((LendersOffice.ObjLib.Conversions.ComplianceEase.FriendlyError)Container.DataItem).Page)%>" />
                        <input class="field" type="hidden" value="<%#AspxTools.HtmlString(((LendersOffice.ObjLib.Conversions.ComplianceEase.FriendlyError)Container.DataItem).Field)%>" />
                    </li>
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <div id="Controls">
            <input type="button" value="Re-run Audit" id="Rerun" runat="server"/>
            <input type="button" id="Close" value="Close" />
        </div>
    </div>
    </form>
</body>
</html>
