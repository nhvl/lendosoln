﻿// <copyright file="OrderMIPolicy.aspx.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/17/2014 11:49:10 AM
// </summary>
namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.AntiXss;
    using LendersOffice.Integration.MortgageInsurance;
    using Mismo231.MI;

    /// <summary>
    /// Implements the Order MI Policy page of the loan file.
    /// </summary>
    public partial class OrderMIPolicy : BaseLoanPage
    {
        /// <summary>
        /// A list of all active Mortgage Insurance Vendors.
        /// </summary>
        private IEnumerable<MortgageInsuranceVendorConfig> activeVendors;

        /// <summary>
        /// A list of all Mortgage Insurance Vendors.
        /// </summary>
        private IEnumerable<MortgageInsuranceVendorConfig> allVendors;

        /// <summary>
        /// A list of Mortgage Insurance Vendors.
        /// </summary>
        private IEnumerable<MortgageInsuranceVendorBrokerSettings> enabledVendors;

        /// <summary>
        /// Gets a list of all Mortgage Insurance Vendors.
        /// </summary>
        /// <value>An IEnumerable of active Mortgage Insurance Vendors included in the MI framework.</value>
        private IEnumerable<MortgageInsuranceVendorConfig> ActiveVendors
        {
            get
            {
                if (this.activeVendors == null)
                {
                    this.activeVendors = MortgageInsuranceVendorConfig.ListActiveVendors();
                }

                return this.activeVendors;
            }
        }

        /// <summary>
        /// Gets a list of all Mortgage Insurance Vendors.
        /// </summary>
        /// <value>An IEnumerable of all Mortgage Insurance Vendors included in the MI framework.</value>
        private IEnumerable<MortgageInsuranceVendorConfig> AllVendors
        {
            get
            {
                if (this.allVendors == null)
                {
                    this.allVendors = MortgageInsuranceVendorConfig.ListAllVendors();
                }

                return this.allVendors;
            }
        }

        /// <summary>
        /// Gets a list of enabled Mortgage Insurance Vendors for the current broker.
        /// </summary>
        /// <value>An IEnumerable of enabled Mortgage Insurance Vendors for the current broker.</value>
        private IEnumerable<MortgageInsuranceVendorBrokerSettings> EnabledVendors
        {
            get
            {
                if (this.enabledVendors == null)
                {
                    this.enabledVendors = MortgageInsuranceVendorBrokerSettings.ListActiveVendorByBrokerId(this.BrokerID);
                }

                return this.enabledVendors;
            }
        }

        /// <summary>
        /// Indicates whether MIP is refundable on a pro-rata basis.
        /// </summary>
        private bool IsMipRefundableOnProRataBasis
        {
            get;
            set;
        }

        /// <summary>
        /// Indicates whether MIP is prepaid.
        /// </summary>
        private bool IsMipPrepaid
        {
            get;
            set;
        }

        /// <summary>
        /// Indicates whether MIP is being financed.
        /// </summary>
        private bool IsMipBeingFinanced
        {
            get;
            set;
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;
            base.OnInit(e);
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.PageTitle = "Order MI Policy";
            this.PageID = "OrderMIPolicy";

            this.EnableJqueryMigrate = false;
            this.RegisterService("OrderMIPolicy", "/newlos/Services/OrderMIPolicyService.aspx");
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("SimplePopups.js");

            this.Quotes.ItemDataBound += new DataGridItemEventHandler(this.OrderInfoGrid_ItemDataBound);
            this.PolicyOrders.ItemDataBound += new DataGridItemEventHandler(this.OrderInfoGrid_ItemDataBound);

            InternalTools.Visible = BrokerUser.HasPermission(LendersOffice.Security.Permission.CanModifyLoanPrograms);
        }

        /// <summary>
        /// Loads data for the page.
        /// </summary>
        /// <remarks>
        /// Note that <see cref="newlos.Test.TestViewXisResponse.MortgageInsuranceMismo231Export(bool, Guid)"/> 
        /// replicates some of the loading logic here. The logic in both places should be kept consistent.
        /// </remarks>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(OrderMIPolicy));
            dataLoan.InitLoad();

            this.BranchId.Value = dataLoan.sBranchId.ToString("D");

            if (!IsPostBack)
            {
                MortgageInsuranceVendorConfig currentVendor = null;
                MIUtil.Bind_MIProvider(this.MIProvider, this.EnabledVendors, this.ActiveVendors);
                var providerItem = this.MIProvider.Items.FindByText(dataLoan.sMiCompanyNmT_rep);
                if (providerItem != null)
                {
                    Guid vendorId;
                    if (Guid.TryParse(providerItem.Value, out vendorId))
                    {
                        currentVendor = MortgageInsuranceVendorConfig.RetrieveById(vendorId);
                    }
                    
                    Tools.SetDropDownListValue(this.MIProvider, providerItem.Value);
                    SetPremiumOptions();
                }
                
                MIUtil.Bind_MIPremiumType(this.MIPremiumType, Broker.EnabledBPMISinglePremium_Case115836, currentVendor?.UsesSplitPremiumPlans ?? false);
                var optionItem = this.MIPremiumType.Items.FindByValue(dataLoan.sProdConvMIOptionT.ToString("D"));
                if (optionItem != null)
                {
                    Tools.SetDropDownListValue(this.MIPremiumType, optionItem.Value);
                }

                this.SetUpfrontPercentageVisibility();

                LosConvert valueConvertor = new LosConvert(FormatTarget.Webform);
                this.MICoveragePercentage.Text = valueConvertor.ToCount(dataLoan.sMiLenderPaidCoverage_rep) + "%";

                this.IsMipRefundableOnProRataBasis = dataLoan.sUfmipIsRefundableOnProRataBasis;
                this.SetRefundabilityOptions();

                this.SetRenewalOptions();

                this.IsMipPrepaid = dataLoan.sIsMipPrepaid;
                this.SetPremiumAtClosingOptions();

                this.IsMipBeingFinanced = dataLoan.sFfUfMipIsBeingFinanced;
                this.SetFinancedIndicator();

                this.ValidateInput();
            }

            this.sMiCompanyNmT.Text = dataLoan.sMiCompanyNmT_rep;
            this.sMiCertId.Text = dataLoan.sMiCertId;
            this.sProdConvMIOptionT.Text = dataLoan.sProdConvMIOptionT_rep;
            this.sFfUfmipR.Text = dataLoan.sFfUfmipR_rep;

            if (dataLoan.sProdConvMIOptionT != E_sProdConvMIOptionT.BorrPaidSplitPrem)
            {
                this.PolicySplitPremiumUpfrontPercentageSection.Visible = false;
            }

            this.sMiLenderPaidCoverage.Text = dataLoan.sMiLenderPaidCoverage_rep;
            this.sLT.Text = dataLoan.sLT_rep;
            this.sLtvR.Text = dataLoan.sLtvR_rep;
            this.sCltvR.Text = dataLoan.sCltvR_rep;
            this.sIsEmployeeLoan.Checked = dataLoan.sIsEmployeeLoan;
            this.sFfUfMipIsBeingFinanced.Checked = dataLoan.sFfUfMipIsBeingFinanced;

            this.LoadOrders();
        }

        /// <summary>
        /// Looks up the vendor name from the list of MI vendors based on the given vendor ID.
        /// </summary>
        /// <param name="vendorID">The ID assigned to the MI vendor by LQB.</param>
        /// <returns>The name of the MI vendor. An empty string if the vendor was not found.</returns>
        protected string GetVendorName(object vendorID)
        {
            try
            {
                if (vendorID != null)
                {
                    return this.AllVendors.First(vendor => vendor.VendorId == new Guid(vendorID.ToString())).VendorTypeFriendlyDisplay;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (InvalidOperationException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the string representation of the given premium type.
        /// </summary>
        /// <param name="premiumType">The MI premium type, e.g. BPMI - Monthly.</param>
        /// <returns>The premium type as a string.</returns>
        protected string GetPremiumType(object premiumType)
        {
            try
            {
                return Tools.Get_sProdConvMIOptionTFriendlyDisplay((E_sProdConvMIOptionT)Convert.ToInt32(premiumType));
            }
            catch (SystemException exc) when (exc is FormatException || exc is InvalidCastException || exc is OverflowException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the string representation of the upfront premium percentage for a split premium order.
        /// </summary>
        /// <param name="splitPremiumUpfrontPercent">The upfront percentage.</param>
        /// <param name="premiumType">The premium type used for the order.</param>
        /// <returns>The upfront percentage as a string.</returns>
        protected string GetUpfrontPercentage(object splitPremiumUpfrontPercent, object premiumType)
        {
            try
            {
                var isSplitPremium = (E_sProdConvMIOptionT)Convert.ToInt32(premiumType) == E_sProdConvMIOptionT.BorrPaidSplitPrem;
                decimal upfrontPercentage = Convert.ToDecimal(splitPremiumUpfrontPercent);
                if (isSplitPremium && upfrontPercentage > 0m)
                {
                    return $"{upfrontPercentage.ToString("0.00")}%";
                }

                return string.Empty;
            }
            catch (SystemException exc) when (exc is FormatException || exc is InvalidCastException || exc is OverflowException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the string representation of the given $$refundability$$ type.
        /// </summary>
        /// <param name="refundability">The $$refundability$$ type.</param>
        /// <returns>The $$refundability$$ as a string.</returns>
        protected string GetPremiumRefundability(object refundability)
        {
            try
            {
                return MIUtil.GetMIPremiumRefundableTypeDisplay((MI_MIPremiumRefundableTypeEnumerated)Convert.ToInt32(refundability));
            }
            catch (SystemException exc) when (exc is FormatException || exc is InvalidCastException || exc is OverflowException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the string representation of the given renewal type.
        /// </summary>
        /// <param name="renewal">The MI premium renewal type.</param>
        /// <returns>The renewal type as a string.</returns>
        protected string GetRenewalType(object renewal)
        {
            try
            {
                return MIUtil.GetMIRenewalCalculationTypeDisplay((MI_MIRenewalCalculationTypeEnumerated)Convert.ToInt32(renewal));
            }
            catch (SystemException exc) when (exc is FormatException || exc is InvalidCastException || exc is OverflowException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the string representation of the MI premium at closing type.
        /// </summary>
        /// <param name="premiumAtClosing">The MI premium at closing type.</param>
        /// <returns>The premium at closing type as a string.</returns>
        protected string GetPremiumAtClosing(object premiumAtClosing)
        {
            try
            {
                return MIUtil.GetMIInitialPremiumAtClosingTypeDisplay((MI_MIInitialPremiumAtClosingTypeEnumerated)Convert.ToInt32(premiumAtClosing));
            }
            catch (SystemException exc) when (exc is FormatException || exc is InvalidCastException || exc is OverflowException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the string representation of the MI decision type.
        /// </summary>
        /// <param name="decision">The MI decision type.</param>
        /// <returns>The decision as a string.</returns>
        protected string GetDecision(object decision)
        {
            try
            {
                return MIUtil.GetMIDecisionTypeDisplay((MI_MIDecisionTypeEnumerated)Convert.ToInt32(decision));
            }
            catch (SystemException exc) when (exc is FormatException || exc is InvalidCastException || exc is OverflowException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Converts the given boolean to a Yes/No string.
        /// </summary>
        /// <param name="boolValue">The boolean to convert.</param>
        /// <returns>Yes if the boolean value is True. Otherwise No.</returns>
        protected string DisplayYesNo(object boolValue)
        {
            string value = "No";

            if (boolValue != null)
            {
                value = boolValue.ToString().Equals("True", StringComparison.OrdinalIgnoreCase) ? "Yes" : "No";
            }

            return value;
        }

        /// <summary>
        /// Resets dependent datapoints when the MI provider changes.
        /// </summary>
        /// <param name="sender">The MI provider dropdown.</param>
        /// <param name="e">A set of event arguments.</param>
        protected void MIProvider_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetPremiumOptions();
            this.MIPremiumType_OnSelectedIndexChanged(sender, e);
        }

        /// <summary>
        /// Resets dependent datapoints when the MI premium type changes.
        /// </summary>
        /// <param name="sender">The MI premium type dropdown.</param>
        /// <param name="e">A set of event arguments.</param>
        protected void MIPremiumType_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetUpfrontPercentageVisibility();
            this.SetRefundabilityOptions();
            this.SetRenewalOptions();
            this.SetPremiumAtClosingOptions();
            this.SetFinancedIndicator();
            this.RunValidation(sender, e);
        }

        /// <summary>
        /// Resets dependent datapoints when the premium at closing changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PremiumAtClosing_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetFinancedIndicator();
            this.RunValidation(sender, e);
        }

        /// <summary>
        /// Runs validation of the input data.
        /// </summary>
        /// <param name="sender">The sending control.</param>
        /// <param name="e">A set of event arguments.</param>
        protected void RunValidation(object sender, EventArgs e)
        {
            this.ValidateInput();
        }

        /// <summary>
        /// Validates the input data and toggles the Order Quote button if all data has been input.
        /// </summary>
        private void ValidateInput()
        {
            bool enableQuoteButton = ShouldEnableQuoteButton();

            this.orderQuoteButton.Disabled = !enableQuoteButton;
            this.viewQuoteXml.Enabled = enableQuoteButton;
        }

        private bool ShouldEnableQuoteButton()
        {
            var workflowResult = Tools.IsWorkflowOperationAuthorized(LendersOffice.Security.PrincipalFactory.CurrentPrincipal, this.LoanID, LendersOffice.ConfigSystem.Operations.WorkflowOperations.OrderMIQuote);
            if (!workflowResult.Item1)
            {
                orderQuoteButton.Attributes["title"] = workflowResult.Item2;
                orderQuoteWorkflowImage.Visible = true;
                return false;
            }
            else
            {
                orderQuoteButton.Attributes["title"] = workflowResult.Item2;
                orderQuoteWorkflowImage.Visible = false;
            }

            if (this.MIProvider.SelectedValue == string.Empty)
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(this.MasterPolicyNumber.Text))
            {
                return false;
            }

            if (this.MIPremiumType.SelectedValue == string.Empty)
            {
                return false;
            }

            if (this.MIPremiumType.SelectedValue == E_sProdConvMIOptionT.BorrPaidSplitPrem.ToString("D") && this.MISplitPremiumUpfrontPercentage.SelectedValue == string.Empty)
            {
                return false;
            }

            decimal coverage;
            bool coverageInputIsANumber = decimal.TryParse(this.MICoveragePercentage.Text.Replace("%", ""), out coverage);
            if (!coverageInputIsANumber || coverage == 0.0m)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Rebinds the MI premium dropdown based on the selected vendor.
        /// </summary>
        private void SetPremiumOptions()
        {
            MortgageInsuranceVendorConfig currentVendor = null;
            if (!string.IsNullOrEmpty(this.MIProvider.SelectedValue))
            {
                currentVendor = MortgageInsuranceVendorConfig.RetrieveById(Guid.Parse(this.MIProvider.SelectedValue));
            }

            MIUtil.Bind_MIPremiumType(this.MIPremiumType, Broker.EnabledBPMISinglePremium_Case115836, currentVendor?.UsesSplitPremiumPlans ?? false);
            MIUtil.Bind_MISplitPremiumUpfrontPercentage(this.MISplitPremiumUpfrontPercentage, currentVendor);

            this.MISplitPremiumUpfrontPercentage.Enabled = currentVendor != null
                && currentVendor.UsesSplitPremiumPlans
                && currentVendor.SplitPremiumOptions.Any();
        }

        /// <summary>
        /// Sets the visibility of the split premium upfront percentage option.
        /// </summary>
        private void SetUpfrontPercentageVisibility()
        {
            var isSplitPremium = this.MIPremiumType.SelectedValue == E_sProdConvMIOptionT.BorrPaidSplitPrem.ToString("D");
            this.UpfrontLabelCell.Style["display"] = isSplitPremium ? string.Empty : "none";
            this.UpfrontPercentageCell.Style["display"] = isSplitPremium ? string.Empty : "none";
        }

        /// <summary>
        /// Binds the premium refundability dropdown list based on the currently selected premium
        /// type. If the premium is refundable, an appropriate default is selected.
        /// </summary>
        private void SetRefundabilityOptions()
        {
            bool isRefundablePremiumType = this.MIPremiumType.SelectedValue != E_sProdConvMIOptionT.LendPaidSinglePrem.ToString("D");
            MIUtil.Bind_PremiumRefundability(this.PremiumRefundability, isRefundablePremiumType);
            this.PremiumRefundability.Enabled = isRefundablePremiumType;
            this.PremiumRefundabilityRequired.Visible = isRefundablePremiumType;

            if (isRefundablePremiumType)
            {
                Tools.SetDropDownListValue(this.PremiumRefundability, this.IsMipRefundableOnProRataBasis ? MI_MIPremiumRefundableTypeEnumerated.Refundable : MI_MIPremiumRefundableTypeEnumerated.NotRefundable);
            }
            else
            {
                Tools.SetDropDownListValue(this.PremiumRefundability, MI_MIPremiumRefundableTypeEnumerated.NotRefundable);
            }
        }

        /// <summary>
        /// Binds the renewal option dropdown list based on the currently selected premium type.
        /// </summary>
        private void SetRenewalOptions()
        {
            bool hasRenewal = this.MIPremiumType.SelectedValue == string.Empty
                || this.MIPremiumType.SelectedValue == E_sProdConvMIOptionT.BorrPaidMonPrem.ToString("D")
                || this.MIPremiumType.SelectedValue == E_sProdConvMIOptionT.BorrPaidSplitPrem.ToString("D");
            MIUtil.Bind_RenewalOption(this.RenewalOption, hasRenewal);
            this.RenewalOption.Enabled = hasRenewal;
            this.RenewalOptionRequired.Visible = hasRenewal;

            if (hasRenewal)
            {
                Tools.SetDropDownListValue(this.RenewalOption, MI_MIRenewalCalculationTypeEnumerated.Constant);
            }
            else
            {
                Tools.SetDropDownListValue(this.RenewalOption, MI_MIRenewalCalculationTypeEnumerated.NoRenewals);
            }
        }

        /// <summary>
        /// Binds the premium at closing based on the currently selected premium type.
        /// </summary>
        private void SetPremiumAtClosingOptions()
        {
            bool allowDeferred = this.MIPremiumType.SelectedValue == string.Empty
                || this.MIPremiumType.SelectedValue == E_sProdConvMIOptionT.BorrPaidMonPrem.ToString("D");
            MIUtil.Bind_PremiumAtClosing(this.PremiumAtClosing, allowDeferred);
            this.PremiumAtClosing.Enabled = allowDeferred;
            this.PremiumAtClosingRequired.Visible = allowDeferred;

            if (allowDeferred)
            {
                var defaultOption = this.IsMipPrepaid
                    ? MI_MIInitialPremiumAtClosingTypeEnumerated.Prepaid
                    : MI_MIInitialPremiumAtClosingTypeEnumerated.Deferred;
                Tools.SetDropDownListValue(this.PremiumAtClosing, defaultOption);
            }
            else
            {
                Tools.SetDropDownListValue(this.PremiumAtClosing, MI_MIInitialPremiumAtClosingTypeEnumerated.Prepaid);
            }
        }

        /// <summary>
        /// Toggles the financed indicator based on whether the premium is deferred.
        /// </summary>
        private void SetFinancedIndicator()
        {
            bool premiumDeferred = this.PremiumAtClosing.SelectedValue == MI_MIInitialPremiumAtClosingTypeEnumerated.Deferred.ToString("D");
            this.UFMIPFinanced.Enabled = !premiumDeferred;

            if (premiumDeferred)
            {
                this.UFMIPFinanced.Checked = false;
            }
            else
            {
                this.UFMIPFinanced.Checked = this.IsMipBeingFinanced;
            }
        }

        /// <summary>
        /// Loads the quote and policy orders from the LQB database and populates the associated grids.
        /// </summary>
        private void LoadOrders()
        {
            IEnumerable<MIOrderInfo> orderList = MIOrderInfo.RetrieveOrdersByLoanId(LoanID, BrokerID, ApplicationID, UserID);
            IEnumerable<MIOrderInfo> quoteList = orderList.Where(order => order.IsQuoteRequest == true);
            this.Quotes.DataSource = quoteList;
            this.Quotes.DataBind();
            this.NoQuotesLabel.Visible = orderList.Count() == 0;

            IEnumerable<MIOrderInfo> policyList = orderList.Where(order => order.IsQuoteRequest == false);
            this.PolicyOrders.DataSource = policyList;
            this.PolicyOrders.DataBind();
            this.NoPolicyOrdersLabel.Visible = policyList.Count() == 0;
        }

        /// <summary>
        /// Binds a data grid item to an <see cref="MIOrderInfo"/> object.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event arguments.</param>
        private void OrderInfoGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer)
            {
                return;
            }

            MIOrderInfo orderInfo = (MIOrderInfo)e.Item.DataItem;

            if (orderInfo.IsQuoteRequest)
            {
                MeridianLink.CommonControls.PassthroughLiteral quoteViewerLink = (MeridianLink.CommonControls.PassthroughLiteral)e.Item.FindControl("QuoteResponseViewerLink");

                if (quoteViewerLink != null)
                {
                    quoteViewerLink.Text = orderInfo.QuoteResponseViewerLink;
                }

                LinkButton policyRequestXmlLink = e.Item.FindControl("PolicyRequestXmlLink") as LinkButton;
                policyRequestXmlLink.Visible = BrokerUser.HasPermission(LendersOffice.Security.Permission.CanModifyLoanPrograms);
                if (policyRequestXmlLink.Visible)
                {
                    policyRequestXmlLink.OnClientClick = $"getOrderXml(false, {AspxTools.JsString(orderInfo.OrderNumber)}, {AspxTools.JsString(orderInfo.TransactionID)}); return false;";
                    Quotes.Columns[0].ItemStyle.Width = Unit.Pixel(100);
                }
            }
            else
            {
                MeridianLink.CommonControls.EncodedLiteral delegationTypeLiteral = (MeridianLink.CommonControls.EncodedLiteral)e.Item.FindControl("DelegationType");
                delegationTypeLiteral.Text = Tools.GetDescription(orderInfo.DelegationType);
            }
        }
    }
}
