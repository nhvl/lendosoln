﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using CommonLib;
using LendersOffice.Integration.TotalScorecard;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos.Services
{
    public partial class FHAErrorResults : LendersOfficeApp.newlos.BaseLoanPage
    {
        #region Variables
        private string m_borrName0 = "";
        private string m_borrName1 = "";
        private string m_borrName2 = "";
        private string m_borrName3 = "";
        private string m_borrName4 = "";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            m_okButton.Attributes.Add("onClick", "javascript:onClosePopup();");
            LoadUIData();
            this.DisplayCopyRight = false;
        }

        private void LoadUIData()
        {
            CFHATotalAuditData dataLoan = new CFHATotalAuditData(this.LoanID);
            dataLoan.InitLoad();
            try
            {
                string cacheId = Request["ErrorKey"];
                htmlList.Text = GenerateTableString(cacheId, dataLoan);
            }
            catch (Exception e)
            {
                Tools.LogError("Unable to render DataConflictAlert window: " + e.ToString()); //TODO - generate something to the user
            }
        }

        private string GenerateTableString(string cacheID, CFHATotalAuditData dataLoan)
        {
            string errorList = AutoExpiredTextCache.GetFromCache(cacheID);
            if (string.IsNullOrEmpty(errorList))
            {
                return "No Errors reported.";
            }

            SetVariables(dataLoan);

            HtmlTableWriter table = new HtmlTableWriter(50, 2);
            table.TableStyle = "border-collapse:collapse;";
            table.TableBorder = "2";
            table.TableCellpadding = "3";
            table.TableWidth = "100%";
            table.OddRowStyle = "BACKGROUND-COLOR: #cccccc;";
            table.EvenRowStyle = "BACKGROUND-COLOR: white";
            table.FirstRowStyle = "BACKGROUND-COLOR:#999999;font-weight:bold;color:White";

            table.WriteData("Error code");
            table.WriteData("Description");
            table.EndLine();

            string[] errorCodes = errorList.Split(',');
            foreach (string errorCode in errorCodes)
            {
                table.WriteData(AspxTools.HtmlString(errorCode));
                try
                {
                    string errorMsg = string.Format(ErrorCodeHandler.GetMessage(errorCode), m_borrName0, m_borrName1, m_borrName2, m_borrName3, m_borrName4);
                    table.WriteData(AspxTools.HtmlString(errorMsg));
                }
                catch (CBaseException cb)
                {
                    Tools.LogError(cb);
                    table.WriteData(AspxTools.HtmlString(cb.UserMessage));
                }
                table.EndLine();
            }

            return table.ToString();
        }

        private void SetVariables(CFHATotalAuditData dataLoan)
        {
            int borrsUsed = 0;

            for (int nApps = 0; nApps < dataLoan.nApps; nApps++)
            {
                CAppData dataApp = dataLoan.GetAppData(nApps);

                if (dataApp.aBIsValidNameSsn)
                {
                    switch (borrsUsed)
                    {
                        case 0:
                            m_borrName0 = dataApp.aBNm;
                            break;
                        case 1:
                            m_borrName1 = dataApp.aBNm;
                            break;
                        case 2:
                            m_borrName2 = dataApp.aBNm;
                            break;
                        case 3:
                            m_borrName3 = dataApp.aBNm;
                            break;
                        case 4:
                            m_borrName4 = dataApp.aBNm;
                            break;
                        default:
                            --borrsUsed;
                            break;
                    }
                    ++borrsUsed;
                }

                if (dataApp.aCIsValidNameSsn)
                {
                    switch (borrsUsed)
                    {
                        case 0:
                            m_borrName0 = dataApp.aCNm;
                            break;
                        case 1:
                            m_borrName1 = dataApp.aCNm;
                            break;
                        case 2:
                            m_borrName2 = dataApp.aCNm;
                            break;
                        case 3:
                            m_borrName3 = dataApp.aCNm;
                            break;
                        case 4:
                            m_borrName4 = dataApp.aCNm;
                            break;
                        default:
                            --borrsUsed;
                            break;
                    }
                    ++borrsUsed;
                }
            }
        }
    }
}
