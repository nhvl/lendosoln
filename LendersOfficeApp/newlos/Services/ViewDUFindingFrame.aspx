<!DOCTYPE html><%@ Page language="c#" Codebehind="ViewDUFindingFrame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Services.ViewDUFindingFrame" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import namespace="LendersOffice.AntiXss"%>
<html lang="en">
  <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <title>DO / DU Findings</title>
  </head>
  <body class="body-iframe">
    <h4 class="page-header">DO / DU Findings</h4>
    <form runat="server">
      <div class="top">
          <iframe name="menu" id="menu" src=<%=AspxTools.SafeUrl("ViewDUFindingMenu.aspx?loanid=" + RequestHelper.GetGuid("loanid"))%> scrolling=no></iframe>
      </div>
      <div class="bottom">
        <iframe name="body" id="body" src=<%=AspxTools.SafeUrl("ViewDUFinding.aspx?loanid=" + RequestHelper.GetGuid("loanid"))%>></iframe>
      </div>
    </form>
  </body>
</html>
