﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenerateUcd.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.GenerateUcd" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Integration.UcdDelivery" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Generate UCD</title>
    <style type="text/css">
        body  {
            background-color: gainsboro; 
        }
        a[data-sort] {
            color: white;
        }
        .LeftPadding20 {
            padding-left: 20px;
        }
        .LabelLength {
            width: 110px;
        }
        img[data-docmetadata] {
            float: right;
        }
        .ucd-file-column {
            min-width: 60px;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        jQuery(function ($) {
            $('img[data-docmetadata]').docmetadatatooltip();

            $('a[data-sort="true"]').click(function () {
                var link = $(this);
                var table = $(this).closest('table').eq(0);
                var rows = table.find('tr:gt(0)').toArray().sort(RowComparer(link.data('sorttarget')));
                table.find('.SortImg').hide();

                link.data('asc', !link.data('asc'));
                if (!link.data('asc')) {
                    rows = rows.reverse();
                    link.siblings('img').prop('src', '../../images/Tri_Desc.gif').show();
                }
                else {
                    link.siblings('img').prop('src', '../../images/Tri_Asc.gif').show();
                }

                for (var i = 0; i < rows.length; i++) {
                    table.append(rows[i]);
                }
            });

            $('#CancelBtn').click(function () {
                parent.LQBPopup.Return(null);
            });

            $('.RequiredInput').change(function () {
                $(this).siblings('.RequiredImg').toggle(this.value === '');
                ToggleGenerateBtn();
            });

            $('.UcdFileLink').click(function () {
                var docId = $(this).siblings('.UcdFileId').val();
                window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?docid=" + docId, '_self');
            });

            $('.UcdXmlLink').click(function () {
                var docId = $(this).siblings('.UcdFileId').val();
                window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?cmd=xml&docid=" + docId, 'xmldoc');
            });

            
            $('.CDPicker').click(function () {
                <% if (this.IsDocMagic) { %>
                var closingDisclosureId = $(this).siblings('.ClosingDisclosureDatesId').val();
                var data = {
                    ClosingDisclosureId: closingDisclosureId,
                    LoanId: ML.sLId
                };

                var result = gService.ucd.call("CheckDmCredentials", data);
                if (!result.error) {
                    if (result.value["Success"].toLowerCase() === 'true') {
                        var hasCredentials = result.value["HasCredentials"].toLowerCase() === 'true';
                        $('#DmCredentialsSection').toggle(!hasCredentials);
                        ToggleGenerateBtn();

                        var docCode = $(this).closest('tr').find('.DocCode').text();
                        $('#ManualDocCode').val(docCode);
                    }
                    else {
                        alert(result.value["Errors"]);
                    }
                }
                else {
                    alert(result.UserMessage);
                }
                <% } else { %>
                ToggleGenerateBtn();
                <% } %>
            });

            $('#GseDeliveryTarget').change(function () {
                var deliveryTargetValue = $(this).val();
                $('#GseDeliveryTargetImg').toggle(deliveryTargetValue === <%= AspxTools.JsString(UcdDeliveredToEntity.Blank) %>);
                $('#GseUserNameLabel').text(deliveryTargetValue === <%= AspxTools.JsString(UcdDeliveredToEntity.FreddieMac) %> ? 'User ID' : 'Username');
                $('#GseCredentialsSection').hide();
                
                if (deliveryTargetValue === <%= AspxTools.JsString(UcdDeliveredToEntity.Blank) %>) {
                    ToggleGenerateBtn();
                } else {
                    var data = {
                        LoanId: ML.sLId,
                        DeliveredTo: deliveryTargetValue
                    };
                    var result = gService.ucd.callAsyncSimple("CheckGseCredentials", data,
                    function successCallback(result) {
                        if (result.value["Success"] !== 'True') {
                            alert(result.value["Errors"]);
                            return;
                        }

                        var needsSellerId = result.value["NeedsSellerId"] === 'True';
                        var hasSavedSellerId = result.value["HasSavedSellerId"] === 'True';
                        var hasSavedCredentials = result.value["HasSavedCredentials"] === 'True';
                        var showSellerIdRow = needsSellerId && !hasSavedSellerId;
                        $('#GseSellerIdRow').toggle(showSellerIdRow);
                        $('.GseUserCredentialRow').toggle(!hasSavedCredentials);
                        $('#GseCredentialsSection').toggle(showSellerIdRow || !hasSavedCredentials);
                        ToggleGenerateBtn();
                    },
                    function errorCallback(result) {
                        alert(result.UserMessage);
                        ToggleGenerateBtn();
                    });
                }

            });

            $('#GenerateBtn').click(function () {
                var isDocMagic = <%= AspxTools.JsBool(this.IsDocMagic) %>;
                var chosenCd = $('.CDPicker:checked').siblings('.ClosingDisclosureDatesId').val();
                var deliveryTarget = $('#GseDeliveryTarget').val();

                var gseSellerId = $('#GseSellerId:visible').val() || '';
                var gseUsername = $('#GseUsername:visible').val() || '';
                var gsePassword = $('#GsePassword:visible').val() || '';

                var data = {
                    LoanId: ML.sLId,
                    AppId: ML.aAppId,
                    ClosingDisclosureId: chosenCd,
                    GseDeliveryTarget: deliveryTarget,
                    GseSellerId: gseSellerId,
                    GseUsername: gseUsername,
                    GsePassword: gsePassword,
                    DeliveryAction: ML.DeliveryAction
                }

                var loadingPopup = SimplePopups.CreateLoadingPopup("Performing Request...");
                LQBPopup.ShowElement($('<div>').append(loadingPopup), {
                    width: 350,
                    height: 200,
                    hideCloseButton: true,
                    elementClasses: 'FullWidthHeight'
                });

                if (isDocMagic)
                {
                    data.DmCustomerId = $('#DmCustomerId').is(':visible') ? $('#DmCustomerId').val() : '';
                    data.DmUsername = $('#DmUsername').is(':visible') ? $('#DmUsername').val() : '';
                    data.DmPassword = $('#DmPassword').is(':visible') ? $('#DmPassword').val() : '';
                }

                var result = gService.ucd.callAsyncSimple("DeliverUcd", data,
                    function(result) {
                        LQBPopup.Return(null);
                        HandleRequest(result);
                    },
                    function(result) {
                        LQBPopup.Return(null);
                        alert(result.UserMessage);
                    });
            });

            $('#ManualDocCodeBtn').click(function () {
                var checkedCdRow = $('#ClosingDisclosureTable').find('.CDPicker:checked').closest('tr');
                if (checkedCdRow.length <= 0) {
                    alert("Please select a closing disclosure.");
                    return;
                }

                var cdId = checkedCdRow.find('.ClosingDisclosureDatesId').val();
                var docCode = $('#ManualDocCode').val();

                var data = {
                    ClosingDisclosureId: cdId,
                    DocCode: docCode,
                    LoanId: ML.sLId
                };

                var result = gService.ucd.call("SetDocCode", data);
                if(!result.error) {
                    if(result.value["Success"].toLowerCase() === 'true') {
                        checkedCdRow.find('.DocCode').text(docCode);
                    }
                    else {
                        alert(result.value["Errors"]);
                    }
                }
                else {
                    alert(result.UserMessage);
                }
            });

            function HandleRequest(result) {
                if (!result.error) {
                    if (result.value["Success"].toLowerCase() === 'true') {
                        var resultStatus = result.value["ResultStatus"];
                        var popup;
                        var deliveryItem = null;
                        if(resultStatus === <%= AspxTools.JsString(UcdDeliveryResultStatus.Generated) %>) { 
                            popup = SimplePopups.CreateAlertPopup(result.value["Message"], '', ML.VirtualRoot + '/images/success.png');
                        }
                        else if(resultStatus === <%= AspxTools.JsString(UcdDeliveryResultStatus.GeneratedButNotDelivered) %>) { 
                            var errors = JSON.parse(result.value["Errors"]);
                            popup = SimplePopups.CreateErrorPopup(result.value["Message"], errors);
                        }
                        else if(resultStatus === <%= AspxTools.JsString(UcdDeliveryResultStatus.GeneratedAndDelivered) %> || resultStatus === <%=AspxTools.JsString(UcdDeliveryResultStatus.Delivered)%>) {
                            popup = SimplePopups.CreateAlertPopup(result.value["Message"], '', ML.VirtualRoot + '/images/success.png');
                            deliveryItem = result.value["DeliveryItem"];
                        }

                        LQBPopup.ShowElement($('<div>').append(popup), {
                            width: 350,
                            height: 200,
                            hideCloseButton: true,
                            elementClasses: 'FullWidthHeight',
                            onReturn: function () {
                                var returnArgs = null;
                                if(deliveryItem) {
                                    returnArgs = {
                                        ViewModelJSON: deliveryItem
                                    };
                                }

                                parent.LQBPopup.Return(returnArgs);
                            }
                        });
                    }
                    else {
                        var errors = JSON.parse(result.value["Errors"]);
                        var errorPopup = SimplePopups.CreateErrorPopup("Something went wrong", errors);
                        LQBPopup.ShowElement($('<div>').append(errorPopup), {
                            width: 350,
                            height: 200,
                            hideCloseButton: true,
                            elementClasses: 'FullWidthHeight'
                        });
                    }
                }
                else {
                    alert(result.UserMessage);
                }
            }

            function RowComparer(sortTargetClass) {
                return function (a, b) {
                    var sortTargetA = $(a).find('.' + sortTargetClass);
                    var sortTargetB = $(b).find('.' + sortTargetClass);

                    var sortValueA = sortTargetA.text();
                    var sortValueB = sortTargetB.text();

                    if (sortValueA < sortValueB) {
                        return -1;
                    }
                    else if (sortValueA > sortValueB) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                }
            }

            function ToggleGenerateBtn() {
                var cdChosen = $('.CDPicker:checked').length > 0;
                var targetChosen = ML.DeliveryAction === <%= AspxTools.JsNumeric(UcdDeliveryAction.GenerateOnly) %> 
                    || $('#GseDeliveryTarget').val() !== <%= AspxTools.JsString(UcdDeliveredToEntity.Blank) %>;

                var inputsOk = $('.RequiredInput:visible').filter(function () {
                    return $(this).val() === ''
                }).length === 0;

                $('#GenerateBtn').prop('disabled', !(cdChosen && inputsOk && targetChosen && ML.CanGenerateUCD));
            }

            $('.WarningIcon').toggle(!ML.CanGenerateUCD).attr('title', ML.CanGenerateUCDDenialReason);
            ToggleGenerateBtn();
            $('#GseCredentialsSection').hide();
            $('#DmCredentialsSection').hide();
        });
    </script>
    <form id="form1" runat="server">
        <div>
            <h4 class="page-header">
                <ml:EncodedLiteral runat="server" ID="GenUcdHeader"></ml:EncodedLiteral>
            </h4>
            <div class="Padding5">
                <div>
                    <h3>Select a Closing Disclosure</h3>
                </div>
                <div>
                    <table id="ClosingDisclosureTable" class="Table">
                        <thead>
                            <tr class="LoanFormHeader">
                                <td></td>
                                <td><a data-sort="true" data-sorttarget="AssociatedVendor" data-asc="false">Associated Vendor</a><img alt="Sort Image" class="SortImage align-middle Hidden" /></td>
                                <td><a data-sort="true" data-sorttarget="IssuedDate" data-asc="false">Issued Date</a><img alt="Sort Image" class="SortImg align-middle Hidden" /></td>
                                <td><a data-sort="true" data-sorttarget="ReceivedDate" data-asc="false">Received Date</a><img alt="Sort Image" class="SortImg align-middle Hidden" /></td>
                                <td><a data-sort="true" data-sorttarget="SignedDate" data-asc="false">Signed Date</a><img alt="Sort Image" class="SortImg align-middle Hidden" /></td>
                                <td><a data-sort="true" data-sorttarget="ArchiveDate" data-asc="false">Archive Date</a><img alt="Sort Image" class="SortImg align-middle Hidden" /></td>
                                <td><span>Initial</span></td>
                                <td><span>Preview</span></td>
                                <td><span>Final</span></td>
                                <td class="ucd-file-column"><span>UCD File</span></td>
                                <td><span>Post-Closing</span></td>
                                <td><a data-sort="true" data-sorttarget="PostConsummationRedisclosureReasonDate" data-asc="false">Event Requiring<br /> Redisclosure Date</a><img alt="Sort Image" class="SortImg align-middle Hidden" /></td>
                                <td><a data-sort="true" data-sorttarget="PostConsummationKnowledgeOfEventDate" data-asc="false">Date Creditor<br /> Received Knowledge</a><img alt="Sort Image" class="SortImg align-middle Hidden" /></td>
                                <% if (IsDocMagic) { %>
                                <td><a data-sort="true" data-sorttarget="DocCode" data-asc="false">Document Code</a><img alt="Sort Image" class="SortImg align-middle Hidden" /></td>
                                <% } %>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater runat="server" ID="CDRepeater" OnItemDataBound="CDRepeater_ItemDataBound">
                                <ItemTemplate>
                                    <tr class="ReverseGridAutoItem">
                                        <td>
                                            <input type="radio" name="CDPicker" class="CDPicker" />
                                            <input type="hidden" class="ClosingDisclosureDatesId" id="ClosingDisclosureDatesId" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ClosingDisclosureDatesId").ToString()) %>" />
                                        </td>
                                        <td>
                                            <ml:PassthroughLabel runat="server" class="AssociatedVendor" id="AssociatedVendor"></ml:PassthroughLabel>
                                        </td>
                                        <td>
                                            <span class="IssuedDate" id="IssuedDate"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "IssuedDate")) %></span>
                                        </td>
                                        <td>
                                            <span class="ReceivedDate" id="ReceivedDate"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ReceivedDate")) %></span>
                                        </td>
                                        <td>
                                            <span class="SignedDate" id="SignedDate"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "SignedDate")) %></span>
                                        </td>
                                        <td>
                                            <span class="ArchiveDate" id="ArchiveDate"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ArchiveDate")) %></span>
                                        </td>
                                        <td>
                                            <asp:CheckBox runat="server" ID="IsInitial" CssClass="IsInitial" Enabled="false" />
                                        </td>
                                        <td>
                                            <asp:CheckBox runat="server" ID="IsPreview" CssClass="IsPreview" Enabled="false" />
                                        </td>
                                        <td>
                                            <asp:CheckBox runat="server" ID="IsFinal" CssClass="IsFinal" Enabled="false" />
                                        </td>
                                        <td class="ucd-file-column">
                                            <img runat="server" id="UcdFileInfo" visible="false" />
                                            <a runat="server" id="UcdFileLink" class="UcdFileLink"></a>
                                            <br>
                                            <a runat="server" id="UcdXmlLink" class="UcdXmlLink"></a>
                                            <input type="hidden" id="UcdFileId" class="UcdFileId" runat="server" />
                                        </td>
                                        <td>
                                            <asp:CheckBox runat="server" ID="IsPostClosing" CssClass="IsPostClosing" Enabled="false" />
                                        </td>
                                        <td>
                                            <span class="PostConsummationRedisclosureReasonDate" id="PostConsummationRedisclosureReasonDate" runat="server"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "PostConsummationRedisclosureReasonDate")) %></span>
                                        </td>
                                        <td>
                                            <span class="PostConsummationKnowledgeOfEventDate" id="PostConsummationKnowledgeOfEventDate" runat="server"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "PostConsummationKnowledgeOfEventDate")) %></span>
                                        </td>
                                        <% if (IsDocMagic) { %>
                                        <td>
                                            <span class="DocCode" id="DocCode"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DocCode")) %></span>
                                        </td>
                                        <% } %>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>

                <hr />
                <div>
                    <div id="DocCodeSection" runat="server">
                        <span class="FieldLabel InlineBlock LabelLength">Assign Doc Code</span>
                        <input type="text" id="ManualDocCode" />
                        <input type="button" id="ManualDocCodeBtn" value="Assign" />
                    </div>
                    <div id="GseDeliveryTargetSection" class="PaddingTop" runat="server">
                        <span class="FieldLabel InlineBlock LabelLength">GSE Delivery Target</span>
                        <asp:DropDownList runat="server" ID="GseDeliveryTarget"></asp:DropDownList>
                        <img alt="Required" class="RequiredImg align-middle" id="GseDeliveryTargetImg" runat="server" src="~/images/require_icon_red.gif" />
                    </div>
                    <hr />
                </div>
                <div class="CssTable FullWidthHeight">
                    <div id="GseCredentialsSection" class="CssTableCell Padding5 Width50pc RightBorder" runat="server">
                        <h3>GSE Credentials</h3>
                        <div class="LeftPadding20">
                            <table>
                                <tr id="GseSellerIdRow">
                                    <td class="FieldLabel">Seller/Servicer Number</td>
                                    <td>
                                        <input type="text" class="RequiredInput InputLarge GseCredential" id="GseSellerId" />
                                        <img alt="Required" class="RequiredImg" runat="server" src="~/images/require_icon_red.gif" />
                                    </td>
                                </tr>
                                <tr class="GseUserCredentialRow">
                                    <td class="FieldLabel" id="GseUserNameLabel">Username</td>
                                    <td>
                                        <input type="text" class="RequiredInput InputLarge GseCredential" id="GseUsername" />
                                        <img alt="Required" class="RequiredImg" runat="server" src="~/images/require_icon_red.gif" />
                                    </td>
                                </tr>
                                <tr class="GseUserCredentialRow">
                                    <td class="FieldLabel">Password</td>
                                    <td>
                                        <input type="password" class="RequiredInput InputLarge GseCredential" id="GsePassword" />
                                        <img alt="Required" class="RequiredImg" runat="server" src="~/images/require_icon_red.gif" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="DmCredentialsSection" class="CssTableCell Padding5 Width50pc">
                        <h3>DocMagic Credentials</h3>
                        <table>
                            <tr id="DmCustomerIdRow">
                                <td class="FieldLabel ">Customer ID</td>
                                <td>
                                    <input type="text" class="RequiredInput InputLarge" id="DmCustomerId" />
                                    <img alt="Required" class="RequiredImg" runat="server" src="~/images/require_icon_red.gif" />
                                </td>
                            </tr>
                            <tr id="DmUsernameRow">
                                <td class="FieldLabel">Username</td>
                                <td>
                                    <input type="text" class="RequiredInput InputLarge" id="DmUsername" />
                                    <img alt="Required" class="RequiredImg" runat="server" src="~/images/require_icon_red.gif" />
                                </td>
                            </tr>
                            <tr id="DmPasswordRow">
                                <td class="FieldLabel">Password</td>
                                <td>
                                    <input type="password" class="RequiredInput InputLarge" id="DmPassword" />
                                    <img alt="Required" class="RequiredImg" runat="server" src="~/images/require_icon_red.gif" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
        <hr />
        <div class="align-center">
            <input type="button" id="GenerateBtn" runat="server" />
            <img src="~/images/warning25x25.png" runat="server" class="WarningIcon align-middle" alt="Warning" />
            <input type="button" id="CancelBtn" value="Cancel" />
        </div>
    </form>
</body>
</html>
