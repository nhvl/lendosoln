﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Web.UI.HtmlControls;
using LendersOffice.AntiXss;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Services
{
    public partial class FHATotalDataAudit : LendersOfficeApp.newlos.BaseLoanPage
    {
        class ExtraValidationHelper
        {
            public string LinkName { get; set; }
            public int AppNumber { get; set; }
            public Guid AppId { get; set; }
            public string PageUrl { get; set; }
            public string PageUrlArgs { get; set; }
            public string Error { get; set; }
        }

        protected bool IsEnableBigLoanPage
        {
            get
            {
                return Broker.IsEnableBigLoanPage;
            }
        }

        public bool HideTable { get; set; } 
        protected void Page_Load(object sender, EventArgs e)
        {

            HideTable = true;
            this.PageID = "FHATotalAudit";
                                                                                  
            CPageData data = new CFHATotalAuditData(LoanID);
            data.InitLoad();

            this.RegisterJsGlobalVariables("IsTargeting2019Ulad", data.sIsTargeting2019Ulad);

            List<CAppData> apps = new List<CAppData>(data.nApps );
            List<ExtraValidationHelper> otherItems = new List<ExtraValidationHelper>();

            for( int i = 0; i < data.nApps; i++ ) 
            {
                CAppData currentApp = data.GetAppData(i);

                apps.Add( currentApp );

                otherItems.Add(new ExtraValidationHelper()
                {
                    LinkName = "Borrower Employment History",
                    AppNumber = i+1,
                    AppId = currentApp.aAppId,
                    Error = currentApp.aBEmpCollectionValid ? string.Empty :  ErrorMessages.TotalScorecardWarning_InvalidEmploymentDate ,
                    PageUrl = "BorrowerInfo.aspx",
                    PageUrlArgs = "&pg=1"
                });

                if (currentApp.aCIsValidNameSsn)
                {
                    otherItems.Add(new ExtraValidationHelper()
                    {
                        LinkName = "Co-borrower Employment History",
                        AppNumber = i + 1,
                        AppId = currentApp.aAppId,
                        Error = currentApp.aCEmpCollectionValid ? string.Empty : ErrorMessages.TotalScorecardWarning_InvalidEmploymentDate,
                        PageUrl = "BorrowerInfo.aspx",
                        PageUrlArgs = "&pg=2"
                    });
                }

                // OPM 106244 - Check for credit report
                otherItems.Add(new ExtraValidationHelper()
                {
                    LinkName = "Credit Report",
                    AppNumber = i + 1,
                    AppId = currentApp.aAppId,
                    Error = currentApp.CreditReportView.Value != null ? string.Empty : ErrorMessages.CreditReportNotOrderedMessage,
                    PageUrl = "Services/OrderCredit.aspx",
                    PageUrlArgs = ""
                });

                if (data.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
                {
                    // OPM 184017
                    otherItems.Add(new ExtraValidationHelper()
                    {
                        LinkName = "Borrower Type",
                        AppNumber = i + 1,
                        AppId = currentApp.aAppId,
                        Error = currentApp.aBTypeT.EqualsOneOf(E_aTypeT.NonTitleSpouse, E_aTypeT.TitleOnly, E_aTypeT.CurrentTitleOnly)
                            ? (i == 0 ? ErrorMessages.TotalScorecardWarning_NonPurchasingPrimaryBorrower : ErrorMessages.TotalScorecardWarning_NonPurchasingOtherAppBorrower) : string.Empty,
                        PageUrl = "BorrowerInfo.aspx",
                        PageUrlArgs = ""
                    });
                }

                if (false == currentApp.aBEmpCollectionValid )
                {
                    HideTable = false;
                }
                if (currentApp.aCIsValidNameSsn && false == currentApp.aCEmpCollectionValid)
                {
                    HideTable = false;
                }
                if (currentApp.CreditReportView.Value == null)
                {
                    HideTable = false;
                }
                if (data.BrokerDB.IsUseNewNonPurchaseSpouseFeature && currentApp.aBTypeT.EqualsOneOf(E_aTypeT.NonTitleSpouse, E_aTypeT.TitleOnly, E_aTypeT.CurrentTitleOnly))
                {
                    HideTable = false;
                }
            }        

            Applications.DataSource = apps;
            Applications.DataBind();

            ls_PrimaryBorrower.Text = data.sPrimaryNm;
            ls_AppraisedValue.Text = data.sApprVal_rep;
            ls_CashftBorrower.Text = data.sTransNetCash_rep;
            ls_LoanAmount.Text = data.sLAmtCalc_rep;
            ls_LoanPurpose.Text = data.sLPurposeT_rep;
            ls_MortgageLiabilities.Text = data.sTotalMortgageTradelineCount_rep;
            ls_NumberOfTotalApps.Text = data.nApps.ToString();
            ls_PurchasePrice.Text = data.sPurchPrice_rep;
            ls_ReoPropertiesCount.Text = data.sReCollectionCount_rep;
            ls_RepresentativeScore.Text = data.sCreditScoreType2Soft_rep;
            ls_SellerClosingCostPaid.Text = data.sTotCcPbs_rep;
            ls_TotalCosts.Text = data.sTotTransC_rep;
            ls_TotalLiquidAssets.Text = data.sAsstLiqTotal_rep;
            ls_TotalLoanAmount.Text = data.sFinalLAmt_rep;
            ls_TotalMonthlyIncome.Text = data.sLTotI_rep;
            ls_TotalNonMortgagePayments.Text = data.sLiaMonLTot_rep;
            ls_UpfrontMip.Text = data.sFfUfmip1003_rep;
            ls_UpfrontMipFinanced.Text = data.sFfUfmipFinanced_rep;
            ls_ltvcltv.Text = string.Format("{0} / {1}", data.sLtvR_rep, data.sCltvR_rep);

            sLTotITotalScorecardWarning.Text = data.sLTotITotalScorecardWarning;
            sApprValTotalScorecardWarning.Text = data.sApprValTotalScorecardWarning;
            sFfUfmip1003TotalScorecardWarning.Text = data.sFfUfmip1003TotalScorecardWarning;
            sBorrCountTotalScorecardWarning.Text = data.sBorrCountTotalScorecardWarning;
            sLPurposeTTotalScorecardWarning.Text = data.sLPurposeTTotalScorecardWarning;
            ExtraEmploymentValidation.DataSource = otherItems;
            ExtraEmploymentValidation.DataBind(); 

            sPurchPriceTotalScorecardWarning.Text = data.sPurchPriceTotalScorecardWarning;
            sGseSpTFriendlyDisplay.Text = data.sGseSpTFriendlyDisplay;
            sTotEstCcNoDiscnt1003.Text = data.sTotEstCcNoDiscnt1003_rep;
            sTotalScorecardBorrPaidCc.Text = data.sTotalScorecardBorrPaidCc_rep;

            ContinueButton.Enabled = !data.sHasTotalScorecardWarning;
            sLAmtCalcTotalScorecardWarning.Text = data.sLAmtCalcTotalScorecardWarning;

            if (HideTable)
            {
                AdditioanlValidationError.Text = "";
                AdditionalValidationStatus.Text = "All passed";
                TableAnchor.InnerText = "Show validation messages";

            }
            else
            {

                AdditionalValidationStatus.Text = "";
                AdditioanlValidationError.Text = "Errors detected";
                TableAnchor.InnerText = "Hide validation messages";
            }
        }

        protected void Continue_OnClick(object sender, EventArgs args)
        {
            Response.Redirect("FHATotalSubmit.aspx?loanid=" + LoanID);
        }

        protected void ExtraValidation_DataBound( object sender, RepeaterItemEventArgs args  ) 
        {
            ExtraValidationHelper currentItem = args.Item.DataItem as ExtraValidationHelper;
            Literal appNumber = (Literal)args.Item.FindControl("AppNumber");
            HtmlAnchor anchor = (HtmlAnchor)args.Item.FindControl("ValidationFixLink");
            Literal error = (Literal)args.Item.FindControl("ValidationError");
            Literal status = (Literal)args.Item.FindControl("ValidationStatus");

            appNumber.Text = currentItem.AppNumber.ToString();
            anchor.InnerText = currentItem.LinkName;
            anchor.HRef = string.Format("javascript:FHATotalDataAudit.JumpTo('{0}', '{1}', '{2}');", AspxTools.JsStringUnquoted(currentItem.PageUrl), AspxTools.JsStringUnquoted(currentItem.AppId.ToString()), AspxTools.JsStringUnquoted(currentItem.PageUrlArgs));

            if (string.IsNullOrEmpty(currentItem.Error))
            {
                status.Text = "Passed";
            }
            else
            {
                error.Text = currentItem.Error;
            }

        }
        protected void Applications_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            CAppData data = (CAppData)args.Item.DataItem;
            var isTargetingUlad2019 = data.LoanData.sIsTargeting2019Ulad;

            Literal aBEmplrJobTitleTotalScorecardWarning = (Literal)args.Item.FindControl("aBEmplrJobTitleTotalScorecardWarning");
            Literal aCEmplrJobTitleTotalScorecardWarning = (Literal)args.Item.FindControl("aCEmplrJobTitleTotalScorecardWarning");
            Literal aBSsnTotalScorecardWarning = (Literal)args.Item.FindControl("aBSsnTotalScorecardWarning");
            Literal aCSsnTotalScorecardWarning = (Literal)args.Item.FindControl("aCSsnTotalScorecardWarning");
            Literal aBDecResidencyTotalScorecardWarning = (Literal)args.Item.FindControl("aBDecResidencyTotalScorecardWarning");
            Literal aCDecResidencyTotalScorecardWarning = (Literal)args.Item.FindControl("aCDecResidencyTotalScorecardWarning");
            Literal aBDecOccTotalScorecardWarning = (Literal)args.Item.FindControl("aBDecOccTotalScorecardWarning");
            Literal aCDecOccTotalScorecardWarning = (Literal)args.Item.FindControl("aCDecOccTotalScorecardWarning"); 

            Literal borrowerFullName = (Literal)args.Item.FindControl("BorrowerFullName");
            Literal borrowerSsn = (Literal)args.Item.FindControl("BorrowerSsn");
            Literal borrowerCititzenshipStatus = (Literal)args.Item.FindControl("BorrowerCititzenshipStatus");
            Literal borrowerTitle = (Literal)args.Item.FindControl("BorrowerTitle");
            Literal borrowerCreditScores = (Literal)args.Item.FindControl("BorrowerCreditScores");
            Literal borrowerOccupancy = (Literal)args.Item.FindControl("BorrowerOccupancy");
            Literal coborrowerName = (Literal)args.Item.FindControl("CoborrowerName");
            Literal coborrowerSsn = (Literal)args.Item.FindControl("CoborrowerSsn");
            Literal coborrowerCitizenshipStatus = (Literal)args.Item.FindControl("CoborrowerCitizenshipStatus");
            Literal coborrowerTitle = (Literal)args.Item.FindControl("CoborrowerTitle");
            Literal coborrowerScores = (Literal)args.Item.FindControl("CoborrowerScores");
            Literal coborrowerOccupancy = (Literal)args.Item.FindControl("CoborrowerOccupancy");
            Literal monthlyIncome = (Literal)args.Item.FindControl("MonthlyIncome");
            Literal liquidAssets = (Literal)args.Item.FindControl("LiquidAssets");
            Literal nonMortgagePayment = (Literal)args.Item.FindControl("NonMortgagePayment");
            Literal appNumber = (Literal)args.Item.FindControl("AppNumber");
            Literal giftFunds = (Literal)args.Item.FindControl("GiftFunds");
            PlaceHolder placeHolder = (PlaceHolder)args.Item.FindControl("CoborrowerFields");
            PlaceHolder nonPurchaseCoborrowerRow = (PlaceHolder)args.Item.FindControl("NonPurchaseCoborrowerFields");
            Literal nonPurchaseCoborrowerName = (Literal)args.Item.FindControl("NonPurchaseCoborrowerName");
            Literal nonPurchaseCoborrowerType = (Literal)args.Item.FindControl("NonPurchaseCoborrowerType");

            appNumber.Text = (args.Item.ItemIndex + 1).ToString();
            borrowerFullName.Text = data.aBNm;
            borrowerSsn.Text = data.aBSsnMasked;
            borrowerCititzenshipStatus.Text = data.aBCitizenDescription; 
            borrowerTitle.Text = data.aBEmplrJobTitle;
            borrowerCreditScores.Text = string.Format("XP: {0} TU: {1} EF: {2}", data.aBExperianScore_rep, data.aBTransUnionScore_rep, data.aBEquifaxScore_rep);
            borrowerOccupancy.Text = isTargetingUlad2019 ? data.aBDecIsPrimaryResidenceUlad.ToYN() : data.aBDecOcc;
            aBEmplrJobTitleTotalScorecardWarning.Text = data.aBEmplrJobTitleTotalScorecardWarning;
            aBSsnTotalScorecardWarning.Text = data.aBSsnTotalScorecardWarning;
            aBDecResidencyTotalScorecardWarning.Text = data.aBDecResidencyTotalScorecardWarning;
            aBDecOccTotalScorecardWarning.Text = data.aBDecOccTotalScorecardWarning;


            // OPM 184017.
            bool nonPurchaseCoborr = data.aCTypeT.EqualsOneOf(E_aTypeT.NonTitleSpouse, E_aTypeT.TitleOnly, E_aTypeT.CurrentTitleOnly);

            if (nonPurchaseCoborr && data.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
            {
                nonPurchaseCoborrowerRow.Visible = true;
                placeHolder.Visible = false;

                nonPurchaseCoborrowerName.Text = data.aCNm;

                switch (data.aCTypeT)
                {
                    case E_aTypeT.TitleOnly:
                        nonPurchaseCoborrowerType.Text = "Title Only";
                        break;
                    case E_aTypeT.NonTitleSpouse:
                        nonPurchaseCoborrowerType.Text = "Non Title Spouse";
                        break;
                    case E_aTypeT.CurrentTitleOnly:
                        nonPurchaseCoborrowerType.Text = "Current Title Only";
                        break;
                    default:
                        throw new UnhandledEnumException(data.aCTypeT);
                }
            }
            else
            {
                nonPurchaseCoborrowerRow.Visible = false;
                placeHolder.Visible = data.aCIsValidNameSsn;

                if (data.aCIsValidNameSsn)
                {
                    coborrowerCitizenshipStatus.Text = data.aCCitizenDescription;
                    coborrowerName.Text = data.aCNm;
                    coborrowerOccupancy.Text = isTargetingUlad2019 ? data.aCDecIsPrimaryResidenceUlad.ToYN() : data.aCDecOcc;
                    coborrowerScores.Text = string.Format("XP: {0} TU: {1} EF: {2}", data.aCExperianScore_rep, data.aCTransUnionScore_rep, data.aCEquifaxScore_rep);
                    coborrowerSsn.Text = data.aCSsnMasked;
                    coborrowerTitle.Text = data.aCEmplrJobTitle;

                    aCEmplrJobTitleTotalScorecardWarning.Text = data.aCEmplrJobTitleTotalScorecardWarning;
                    aCSsnTotalScorecardWarning.Text = data.aCSsnTotalScorecardWarning;
                    aCDecResidencyTotalScorecardWarning.Text = data.aCDecResidencyTotalScorecardWarning;
                    aCDecOccTotalScorecardWarning.Text = data.aCDecOccTotalScorecardWarning;
                }
            }

            monthlyIncome.Text = data.aTotI_rep;
            liquidAssets.Text = data.aAsstLiqTot_rep;
            nonMortgagePayment.Text = data.aLiaMonTot_rep;
            giftFunds.Text = data.aGiftFundAssets_rep;
        }
    }

}


