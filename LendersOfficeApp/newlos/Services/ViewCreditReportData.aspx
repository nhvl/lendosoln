<%@ Page language="c#" Codebehind="ViewCreditReportData.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Services.ViewCreditReportData" EnableSessionState="False" enableViewState="False" validateRequest="False"%>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<head runat="server"><title>TestCreditReport</title>
	<style type="text/css">
	div.recordgrid
	{
	    margin:1em;
	    BORDER-RIGHT:1px inset; 
	    BORDER-TOP:1px inset; 
	    BORDER-LEFT:1px inset; 
	    BORDER-BOTTOM:1px inset;
	}
	table.recordgrid
	{	    
	    border:1px;
	    margin-top:1em;
	}
	td.fieldlabel
	{
	    padding-left: 0.5em;
	    padding-right: 0.5em;
	    font-weight:bold;
	}
	
	</style>
	
	</head>
	
	<body class="RightBackground" onresize="f_onresize();">
		<script type="text/javascript">
<!-- 
    function _init() {
        if(window.top.location != window.location)
        {
            window.top.location = window.location;    
            window.resizeTo(900,800);
        }
    } 
    function f_onresize() {
        var h = document.body.clientHeight;
        document.getElementById("MainDiv").style.height=(h - 150) + "px";    

        var w = document.body.clientWidth;
        document.getElementById("MainDiv").style.width=(w - 150) + "px";    
    }  
    function f_constructUrl() {
        var sLId = <%= AspxTools.JsString(LoanID) %>;
        var aAppId = <%= AspxTools.JsString(ApplicationID) %>;
        var url =  'ViewCreditReportData.aspx?appid=' + aAppId + '&loanid=' + sLId;
        return url;
    }    
    function f_viewScores() {
        var url = f_constructUrl() + '&cmd=scores';
        window.top.location = url; 
    }
    function f_viewLiabilities() {
        var url = f_constructUrl() + '&cmd=liabilities';
        window.top.location = url;
    }    
    function f_viewPublicRecords() {
        var url = f_constructUrl() + '&cmd=publicrecords';
        window.top.location = url;
    }
//-->
		</script>
		<form id="TestCreditReport" method="post" runat="server" enctype="multipart/form-data">
		<br /><br />
		    <div style="color:Red" runat="server" id="divTopMessage"><span id="pnlMsg" runat="server"></span> <b>You can still view raw data below.</b></div>		    
			<hr />
			<input type = "radio" id="radioScores" runat="server" onclick="f_viewScores();"> <b>View Scores </b></input>
			<input type = "radio" id="radioLiabilities" runat="server" onclick="f_viewLiabilities();" > <b>View Liabilities </b></input>
			<input type = "radio" id="radioPubRecords" runat="server" onclick="f_viewPublicRecords();" > <b>View Public Records </b></input>
			
			<hr>
			<div id="MainDiv" style="width:750px; height:700px;  OVERFLOW:scroll;">
				<ml:PassthroughLiteral id="MainHtml" runat="server"></ml:PassthroughLiteral>
			</div>
			<hr />
		</form>
	</body>
</HTML>