﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.DU;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Services
{
    public partial class FannieMaeEarlyCheckService : BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName) 
            {
                case "SubmitEarlyCheck":
                    SubmitEarlyCheck();
                    break;
            }

        }

        private void SubmitEarlyCheck()
        {

            Guid sLId = GetGuid("LoanID");

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            BrokerDB broker = BrokerDB.RetrieveById(principal.BrokerId);

            string fannieMaeMornetUserId = string.Empty;
            string fannieMaeMornetPassword = string.Empty;
            string fannieMaeInstitutionId = string.Empty;

            if (string.IsNullOrEmpty(broker.FnmaEarlyCheckUserName) == false &&
                string.IsNullOrEmpty(broker.FnmaEarlyCheckInstutionId) == false)
            {
                // 10/2/2013 dd - Use login at broker level.
                fannieMaeMornetUserId = broker.FnmaEarlyCheckUserName;
                fannieMaeMornetPassword = broker.FnmaEarlyCheckDecryptedPassword.Value;
                fannieMaeInstitutionId = broker.FnmaEarlyCheckInstutionId;
            }
            else
            {
                fannieMaeMornetUserId = GetString("FannieMaeMORNETUserID");
                fannieMaeMornetPassword = GetString("FannieMaeMORNETPassword");
                fannieMaeInstitutionId = GetString("FannieMaeMORNETInstitutionId");

            }
            string globallyUniqueIdentifier = GetString("GloballyUniqueIdentifier");

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(FannieMaeEarlyCheckService));
            dataLoan.InitLoad();

            FnmaXisEarlyCheckRequest request = new FnmaXisEarlyCheckRequest(sLId);
            request.FannieMaeMORNETUserID = fannieMaeMornetUserId;
            request.FannieMaeMORNETPassword = fannieMaeMornetPassword;
            request.LenderInstitutionId = fannieMaeInstitutionId;
            request.LenderCaseIdentifier = dataLoan.sLenderCaseNum; // Require for MISMO 2.3 or 1003 format.
            request.GloballyUniqueIdentifier = globallyUniqueIdentifier;

            string status = "ERROR";
            string errorMessage = "";

            AbstractFnmaXisResponse response = DUServer.Submit(request);
            if (response.IsReady)
            {
                if (response.HasAnyError)
                {
                    status = "ERROR";
                    errorMessage = response.ErrorInHtml;
                }
                else
                {
                    status = "DONE";

                    ((FnmaXisEarlyCheckResponse)response).ImportToLoanFile(sLId, PrincipalFactory.CurrentPrincipal); // Save to FileDB on successful response.
                }
            }
            else
            {
                status = "PROCESSING";
            }
            globallyUniqueIdentifier = response.GloballyUniqueIdentifier;

            SetResult("Status", status);
            SetResult("ErrorMessage", errorMessage);
            SetResult("GloballyUniqueIdentifier", globallyUniqueIdentifier);

        }
    }
}
