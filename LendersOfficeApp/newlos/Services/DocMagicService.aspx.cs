namespace LendersOfficeApp.newlos.Services
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using global::DocMagic.DsiDocRequest;

    public class DocMagicServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(DocMagicServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("CanReadCloser", BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowCloserRead));
            SetResult("CanWriteCloser", BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowCloserWrite));

            Guid loanId = GetGuid("loanid");

            if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
            {
                if (false == BrokerUserPrincipal.CurrentPrincipal.HasPermission(LendersOffice.Security.Permission.AllowCloserWrite))
                {
                    //if they cannot write then they cannot make any changes ot the export settings.
                    return;
                }

                dataLoan.sIncludeGfeDataForDocMagicComparison = GetBool("sIncludeGfeDataForDocMagicComparison");
                int sSettlementChargesExportSource = GetInt("sSettlementChargesExportSource");

                if (sSettlementChargesExportSource == 0)
                {
                    dataLoan.sSettlementChargesExportSource = E_SettlementChargesExportSource.GFE;
                    SetResult("sIncludeGfeDataForDocMagicComparison", false);
                    dataLoan.sIncludeGfeDataForDocMagicComparison = false;
                }
                else if (sSettlementChargesExportSource == 1)
                {
                    dataLoan.sSettlementChargesExportSource = E_SettlementChargesExportSource.SETTLEMENT;
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Invalid   sSettlementChargesExportSource value " + sSettlementChargesExportSource);
                }
            }
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
           
        }
    }

	public partial class DocMagicService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize()
        {
            AddBackgroundItem("", new DocMagicServiceItem());
        }
	}
}
