﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FHATotalSubmit.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.FHATotalSubmit" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ML" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <script type="text/javascript" src="../../inc/utilities.js" ></script>

    <style type="text/css" >
    p, a,img, ol, ul, li,
     form, label, 
    table, caption, tbody, tfoot, thead, tr, th, td {
        margin: 0;
        padding: 0;
        border: 0;
        outline: 0;
        font-size: 100%;
    
    }
        .clear { clear: both; }
        .nav { width:100%;  text-align: center; margin-top: 10px;}
        .nav input { margin: 0 auto;   }
        li { padding-bottom: 3px; display: block; overflow: auto;  clear: both; line-height: 20px;}
        li input { display: block; }
        body { background-color: gainsboro; font-weight: bold; width: 100%;}
        li label { line-height: 20px; }

        h1, h2, h3 { margin: 0; padding: 0; }
        h2,h3 { font-size: 10px; }
        h2 { padding-left: 10px;}
        h3 { padding-left: 15px; }
        ul{ list-style : none; overflow: auto; display: block; } 
        li label,  li .label { display: block;   float:left; clear:left; text-align: left; }
        #refinanceinfo li label,  #refinanceinfo li .label  { width: 170px; }
        #unit34 li label,  #unit34 li .label  { width: 310px; }
        #RateInfoList li label,  #RateInfoList  li .label  { width: 120px; text-align: left;}
        .lefty  { float: left; display: block; }
        .paddingbottom { padding-bottom: 5px; overflow: auto; }
        img { margin-top: 4px; padding-left: 2px; }
        .Validation { display: none; }
        .alabel { display: block; float: left; width: 120px; }
        select { _margin-left: -3px; }
        fieldset { margin: 6px;}
        span.span-legend { padding: 4px;display: block;color: white;background-color: gray; font-weight: normal;}
        .lar label, .lar input, .lar span { display: block; float: left; margin: 0;}
        .lar input { border:0px; height:14px;   padding: 0 0 0 5px;}
        .lar { overflow: hidden; margin: 0; padding: 0;}
        .chbfix, .chbfix input {  padding: 0; margin: 0 3px 0 0;  width:15px;}
        table#mortgageLiabilities  { width: 80%; table-layout: fixed; border-color: #000; border-width: 0 0 1px 1px; border-style: solid;  }
        table#mortgageLiabilities thead tr th { background-color:#a6a6a6; }
        table#mortgageLiabilities  td.col1, table#mortgageLiabilities  th.col1 { width: 100px; text-align:center; }
        table#mortgageLiabilities  td, table#mortgageLiabilities  th {  padding: 4px; padding-left: 10px; padding-right: 10px; border-color: #000;  border-width: 1px 1px 0 0; border-style: solid;  }
        table#mortgageLiabilities input { padding: 0; margin: 0; margin-right: 4px; }
        table#mortgageLiabilities label { position: relative; top: -2px;}
        .rightAlign { text-align: right; }
        #GiftFundAssets {  border-collapse:collapse; border:1px solid #eee; table-layout: fixed;}
        #GiftFundAssets tr td {  border:1px solid #eee; border-right:1px solid #ddd; padding:1px; }
        .FthbExplanation { font-weight: normal; }
        .Error { color: #FF0000; }
        
    </style>
    <title>FHA Total Scorecard Submit Form</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField runat="server" ID="NumberOfApplications" />
    <asp:HiddenField runat="server" ID="BorrowersClientId" />
    <h1 class="MainRightHeader">Submit to FHA TOTAL Scorecard</h1>
    <div style="width:740px;">

        <asp:PlaceHolder runat="server" ID="RefinanceInformation">
        <fieldset onselectstart="return false;">
            
            <span class="span-legend">Refinance Information</span>
            <div style="padding: 10px">
                <ml:EncodedLabel runat="server" ID="RefinanceTypeLabel" AssociatedControlID="sTotalScoreRefiT" CssClass="alabel">Refinance Type </ml:EncodedLabel>
                <asp:DropDownList CssClass="lefty" onchange="FHASubmitForm.ValidatePage();" runat="server" ID="sTotalScoreRefiT"></asp:DropDownList> 
                <img alt="Required" class="Validation lefty" src="../../images/error_icon.gif" id="RefinanceTypeR" />
                <span id="RefinanceTypeInvalidMessage" class="Validation lefty" style="color: red">This refinance type is not accepted by TOTAL Scorecard, please select an appropriate type.</span>
                <br style="clear:both;" />
                <br />
                <ml:EncodedLabel runat="server" CssClass="alabel paddingbottom" Text="Mortgage Liabilities"></ml:EncodedLabel>
                <asp:Repeater runat="server" ID="MortgageLiabilitiesRepeater" OnItemDataBound="MortgageLiabilitiesRepeater_OnItemDataBound">
                    <HeaderTemplate> 
                        <table class="clear" id="mortgageLiabilities" cellpadding="0" cellspacing="0" border="0" >
                            <thead>
                                <tr>
                                    <th class="col1">Subject prop <br />1st mortgage</th>
                                    <th>Creditor name</th>
                                    <th>Balance</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr runat="server" id="row">
                            <td class="col1">
                           
                                <input type="radio" runat="server"  id="SubjProp1stMort" name="SubjProp1stMort" />
                                <asp:HiddenField runat="server" ID="MortLiabIdentifier" />
                            </td>
                            <td>
                                <ml:EncodedLabel runat="server" ID="sCredNm"></ml:EncodedLabel> &nbsp;
                            </td>
                            <td class="rightAlign">
                                <ml:EncodedLabel runat="server" ID="balance"></ml:EncodedLabel>
                            </td>
                            <td>
                                <asp:RadioButtonList runat="server" ID="PropStatus"  RepeatLayout="Flow">
                                    <asp:ListItem  Value="SubjectProperty" Text="Subject Property"></asp:ListItem>
                                    <asp:ListItem  Value="OtherProperty" Text="Other Property"></asp:ListItem>
                                    
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                            </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                
            </div>
        </fieldset>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="ThreeFourUnitProperties" >
            <fieldset>
                <span class="span-legend">3-4 Unit Properties</span>
                <div style="padding: 10px" >
                <ul id="unit34">
                    <li>
                        <ml:EncodedLabel runat="server"  AssociatedControlID="sTotalScoreAppraisedFairMarketRent"> Appraiser's estimate of fair market rent from all units?</ml:EncodedLabel>
                        <ml:MoneyTextBox runat="server" onchange="FHASubmitForm.ValidatePage(); FHASubmitForm.RefreshRentalIncome();" onkeyup="FHASubmitForm.ValidatePage();"   Width="75px" CssClass="lefty" ID="sTotalScoreAppraisedFairMarketRent" preset="money" ></ml:MoneyTextBox>
                        <img alt="Require" class="lefty Validation" src="../../images/error_icon.gif" id="AppraiserEstimateRequire" />
                    </li>
                    <li>
                        <ml:EncodedLabel runat="server" AssociatedControlID="sTotalScoreVacancyFactor">  Occupancy Rate - lower of appraisal or jurisdictional HOC </ml:EncodedLabel>
                        <ml:PercentTextBox runat="server" onchange="FHASubmitForm.ValidatePage(); FHASubmitForm.RefreshRentalIncome();" onkeyup="FHASubmitForm.ValidatePage();" ID="sTotalScoreVacancyFactor"  Width="75px" style="text-align: right;"  CssClass="lefty" preset="percent" ></ml:PercentTextBox></asp:TextBox>
                        <img alt="Required" class=" Validation lefty" src="../../images/require_icon.gif" id="VacancyR" />
                        <img alt="Error" class="lefty Validation" id="VacancyE"  src="../../images/error_icon.gif"/>
                    </li>
                    <li>
                        <ml:EncodedLabel runat="server" AssociatedControlID="sTotalScoreNetRentalIncome">  Net rental income</ml:EncodedLabel>
                        <ml:MoneyTextBox runat="server" ReadOnly="true"  Width="75px" CssClass="lefty" ID="sTotalScoreNetRentalIncome" preset="money" ></ml:MoneyTextBox>
                    </li>
                </ul>
                </div>
            </fieldset>
          </asp:PlaceHolder>
        <fieldset>
            <span class="span-legend">Loan Information </span>
            <div style="padding: 10px 10px 0 10px; overflow:auto" >
                  <div style="clear: both; overflow: auto; " class="lar">
                <ml:EncodedLabel runat="server" AssociatedControlID="sTotalScoreFhaProductT"   CssClass="alabel lefty"> FHA Program </ml:EncodedLabel>
                <asp:DropDownList style="margin-left: 0px" onchange="FHASubmitForm.ValidatePage();" runat="server" ID="sTotalScoreFhaProductT" CssClass="lefty">
                </asp:DropDownList>
                <img alt="Required" class=" lefty Validation" src="../../images/error_icon.gif" id="FhaProgramR" />
                </div>
                <div style="clear: both; overflow: auto; margin-top: 6px;" class="lar">
                    <table>
                        <tr>
                            <td style="width: 300px;">
                                <span class="lefty">Is loan an identity of interest transaction?&nbsp;&nbsp&nbsp;&nbsp</span>
                            </td>
                            <td style="width: 110px;">
                     
                                <asp:RadioButtonList onclick="FHASubmitForm.ValidatePage();" runat="server" RepeatLayout="Flow"
                                    CssClass="lefty" RepeatDirection="Horizontal" ID="sTotalScoreIsIdentityOfInterest">
                                    <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="false"></asp:ListItem>
                                </asp:RadioButtonList>
                                <img alt="Required" src="../../images/error_icon.gif" id="LoanIdentityR" class="lefty" />
                            </td>
                            <td>
                                <span class="lefty" >(<a href="javascript:void(0);" onclick="Modal.ShowPopup('IdentityTransactionExplaination', null, event);">Explain</a>)</span>
                            </td>
                        </tr>
                        <tr id="IdentityOfInterestExceptionPanel" style="">
                            <td style="padding-left: 10px; padding-top: 6px;">
                                <span class="lefty">Does the transaction fall under an exception?&nbsp;&nbsp&nbsp;&nbsp</span>
                            </td>
                            <td style="padding-top: 6px;">
                                <asp:RadioButtonList onclick="FHASubmitForm.ValidatePage();" runat="server" RepeatLayout="Flow"
                                    CssClass="lefty" RepeatDirection="Horizontal" ID="sTotalScoreIsTransactionException">
                                    <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="false"></asp:ListItem>
                                </asp:RadioButtonList>
                                <img alt="Required" src="../../images/error_icon.gif" style="margin-top: 0px;" id="TransExceptionR"
                                    class="lefty" />
                            </td>
                            <td style="padding-top: 6px;">
                                <span class="lefty">(<a href="javascript:void(0);" onclick="Modal.ShowPopup('IdentityExceptionExplanation', null, event);">Explain</a>)</span>
                            </td>
                        </tr>
                        </table>
                </div>
                <br style="clear: both;" />
              
            <div id="RateInfo" style="display: block; float: left;  width: 250px; margin-top: 5px; padding-right:60px; ">
                <ul id="RateInfoList">
                    <li>
                        <ml:EncodedLabel runat="server" ID="RateLabel" AssociatedControlID="sNoteIR">Rate</ml:EncodedLabel>
                        <ml:PercentTextBox onchange="FHASubmitForm.ValidatePage();" onkeyup="FHASubmitForm.ValidatePage();" runat="server" ID="sNoteIR"   Width="60px" style="text-align: left;"  CssClass="lefty" preset="percent" > </ml:PercentTextBox>
                        <img class="lefty Validation" alt="Required"  src="../../images/error_icon.gif" id="RateR" />
                    </li>
                    <li>
                        <ml:EncodedLabel runat="server" ID="TermLabel" AssociatedControlID="sTerm">Term</ml:EncodedLabel>
                        <asp:TextBox  onchange="FHASubmitForm.ValidatePage();" onkeyup="FHASubmitForm.ValidatePage();" runat="server" Width="35px" CssClass="lefty" ID="sTerm"></asp:TextBox>
                       <img  alt="Error" class="lefty Validation" src="../../images/error_icon.gif" id="TermE" />
                    </li>
                    <li>
                        <ml:EncodedLabel runat="server" ID="ArmortLabel" AssociatedControlID="sFinMethT">Amortization type</ml:EncodedLabel>
                        <asp:DropDownList onchange="FHASubmitForm.CheckFinMeth();FHASubmitForm.ValidatePage();"  runat="server" ID="sFinMethT" >
                        </asp:DropDownList>
                    </li>
                    <li id="ArmSetting">
                        <ml:EncodedLabel runat="server" ID="FirstChangeLabel" AssociatedControlID="sRAdj1stCapMon">1st Change</ml:EncodedLabel>
                        <asp:TextBox onchange="FHASubmitForm.ValidatePage();" onkeyup="FHASubmitForm.ValidatePage();" style="float:left; " runat="server" ID="sRAdj1stCapMon" Width="50px"></asp:TextBox> <span style="display: block; float:left" >mths</span>
   
                        <img  alt="Error" class="lefty Validation" src="../../images/error_icon.gif" id="ArmError" />
                    </li>
                </ul>
            </div>
            <div id="Seconfin" style="display: block; float: left;  margin-top: 5px " runat="server" >
                <ml:EncodedLabel runat="server" ID="SecondaryFinancingSourceLabel" CssClass="lefty" AssociatedControlID="sTotalScoreSecondaryFinancingSrc">Secondary financing source</ml:EncodedLabel>
                <br />
                <asp:DropDownList runat="server" class="lefty" ID="sTotalScoreSecondaryFinancingSrc" onchange="FHASubmitForm.ValidatePage();" />
                <img alt="Error" class="lefty Validation" id="sTotalScoreSecondaryFinancingSrcError" src="../../images/error_icon.gif"/>
                <asp:CheckBox runat="server" ID="sTotalScoreSecondaryFinancingSrcLckd" onchange="onSecondaryFinancingLocked();" />
                <span>Lock</span>
                <br />
                <span class="Error Validation" runat="server" id="sTotalScoreSecondaryFinancingSrcErrorMessage">This subfinancing source is not accepted by TOTAL Scorecard, <br />please select an appropriate source.</span>
            </div>
        </div>
        <br style="clear: both; "/>
        </fieldset>
        <fieldset id="BorrowerInfoFieldSet"  >
            <span class="span-legend">Borrower Information</span>
            <asp:Repeater runat="server" ID="Borrowers" OnItemDataBound="Borrowers_OnItemDataBound">
                <ItemTemplate>
                    <asp:HiddenField runat="server" ID="BorrowerApplicationId" />
                    <asp:HiddenField runat="server" ID="HasCoborrower" />
                    <div style="display: block; float: left; width: 150px; padding: 10px;">
                        <%# AspxTools.HtmlString(((DataAccess.CAppData)Container.DataItem).aBLastFirstNm) %>
                    </div>
                    <div style="display: block; float: left;">
                        <table cellspacing="5" cellpadding="0">
                            <tr>
                                <td>

                                    <asp:CheckBox runat="server" ID="aBTotalScoreIsCAIVRSAuthClear" CssClass="chbfix" Text="CAIVRS authorization is clear" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ml:EncodedLabel runat="server" AssociatedControlID="aFHABCaivrsNum"> CAIVRS # </ml:EncodedLabel><asp:TextBox
                                        runat="server" ID="aFHABCaivrsNum"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="lar">
                                <span class="lefty">
                                    <a href="javascript:void(0);"   onclick="return Dialog.openUrl('https://www5.hud.gov/ecpcis/main/ECPCIS_List.jsp', 'LDP', 800, 600 );">LDP</a>/<a href="javascript:void(0)"  onclick="return Dialog.openUrl('https://www.sam.gov/', 'LDP', 400, 500 );" >GSA</a>
                                    </span>
                                    <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"
                                        ID="aFHABLdpGsaTri">
                                        <asp:ListItem Text="Yes" Value="1"> </asp:ListItem>
                                        <asp:ListItem Text="No" Value="2"> </asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </div> 
                    <div style="display: block; float: left; margin-left: 10px;">
                        <table cellspacing="5" cellpadding="0">
                            <tr>
                                <td>
                                    <input type="checkbox" class="chbfix"  style="margin-right: 0" runat="server" ID="aBTotalScoreIsFthb" onclick="FHASubmitForm.ValidatePage();"  onchange="FHASubmitForm.ValidatePage();" /> 
                                    <ml:EncodedLabel  runat="server" AssociatedControlID="aBTotalScoreIsFthb">Is first time home buyer?</ml:EncodedLabel>
                                    <asp:PlaceHolder runat="server" ID="BorrFthbExplanation">
                                        <span class="FthbExplanation">
                                            see <a href="#" runat="server" id="BorrowerDeclarationsLink">declarations</a> item <span class="first-time-home-buyer-declarations-item-number"></span>
                                        </span>
                                    </asp:PlaceHolder>
                                </td>
                            </tr>
                            <tr>
                                <td height="20">
                                    <ml:EncodedLabel ID="aBTotalScoreFhtbCounselingTLabel" runat="server" AssociatedControlID="aBTotalScoreFhtbCounselingT">FTHB counseling completed </ml:EncodedLabel>
                                    <asp:DropDownList style="margin-left: 0px" runat="server" ID="aBTotalScoreFhtbCounselingT" onchange="FHASubmitForm.ValidatePage();" >

                                    </asp:DropDownList>
                                    <asp:Image  style="margin-top: 0px; margin-left: -3px" runat="server" id="BFTHBR" alt="Required" src="../../images/error_icon.gif" />
                                </td>
                            </tr>
                        </table>
                    </div>
                       <asp:PlaceHolder runat="server" ID="CoborrowerFields">
                    <br style="clear: both;">
                    <br />
                  
                    <div style="display: block; float: left; width: 150px; padding: 10px;">
                        <%# AspxTools.HtmlString(((DataAccess.CAppData)Container.DataItem).aCLastFirstNm) %>
                    </div>
                    <div style="display: block; float: left;">
                        <table cellspacing="5" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:CheckBox runat="server" ID="aCTotalScoreIsCAIVRSAuthClear" CssClass="chbfix " Text="CAIVRS authorization is clear" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ml:EncodedLabel ID="Label2" runat="server" AssociatedControlID="aFHACCaivrsNum"> CAIVRS # </ml:EncodedLabel>
                                    <asp:TextBox runat="server" ID="aFHACCaivrsNum"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td  class="lar">
                                <span class="lefty">
                                    <a href="javascript:void(0);" onclick="return Dialog.openUrl('https://www5.hud.gov/ecpcis/main/ECPCIS_List.jsp', 'LDP', 800, 600 );">LDP</a>/<a href="javascript:void(0);"  onclick="return Dialog.openUrl('https://www.sam.gov/', 'LDP', 400, 500 );" >GSA</a>
</span>
                                    <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"
                                        ID="aFHACLdpGsaTri">
                                        <asp:ListItem Text="Yes" Value="1"> </asp:ListItem>
                                        <asp:ListItem Text="No" Value="2"> </asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="display: block; float: left; margin-left: 10px;">
                        <table cellspacing="5" cellpadding="0">
                            <tr>
                                <td>
                                    <input type="checkbox" runat="server" ID="aCTotalScoreIsFthb" onclick="FHASubmitForm.ValidatePage();" class="chbfix" onchange="FHASubmitForm.ValidatePage();" />
                                    <ml:EncodedLabel runat="server"  AssociatedControlID="aCTotalScoreIsFthb">Is first time home buyer? </ml:EncodedLabel>
                                    <asp:PlaceHolder runat="server" ID="CoborrFthbExplanation">
                                        <span class="FthbExplanation">
                                            see <a href="#" runat="server" ID="CoborrowerDeclarationsLink">declarations</a> item <span class="first-time-home-buyer-declarations-item-number"></span>
                                        </span>
                                    </asp:PlaceHolder>
                                </td>
                            </tr>
                            <tr>
                                <td height="20">
                                    <ml:EncodedLabel ID="aCTotalScoreFhtbCounselingTLabel" runat="server" AssociatedControlID="aCTotalScoreFhtbCounselingT" > FTHB counseling completed </ml:EncodedLabel>
                                    <asp:DropDownList runat="server" style="margin-left: 0px"  ID="aCTotalScoreFhtbCounselingT"  onchange="FHASubmitForm.ValidatePage();" ></asp:DropDownList>
                                   <asp:Image   style="margin-top: 0px; " runat="server" id="CFTHBR" src="../../images/error_icon.gif" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    </asp:PlaceHolder>
                </ItemTemplate>
                <SeparatorTemplate>
                    <hr style="clear: both;" />
                </SeparatorTemplate>
            </asp:Repeater>
        </fieldset>
        
        <fieldset runat="server" id="GiftFundPanel" visible="false" >
           <span class="span-legend">Gift Funds</span>
            <asp:Repeater runat="server" ID="GiftFundAssets" OnItemDataBound="GiftFundAssets_OnItemDataBound">
                <HeaderTemplate>
                    <table cellpadding="0" id="GiftFundAssets" cellspacing="0" width="99%" >
                        <thead>
                            <tr class="GridHeader">
                                <th>Borrower</th>
                                <th>Description</th>
                                <th style="width: 100px">Amount</th>
                                <th style="width: 120px">Source</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
                <ItemTemplate>
                    <tr class="GridItem">
                        <td><ml:EncodedLiteral runat="server" ID="BorrowerName"></ml:EncodedLiteral></td>
                        <td><ml:EncodedLiteral runat="server" id="Description"></ml:EncodedLiteral></td>
                        <td align="right"><ml:EncodedLiteral runat="server" ID="Amount"></ml:EncodedLiteral></td>
                        <td><asp:DropDownList runat="server" ID="Source"></asp:DropDownList>
                         <asp:RegularExpressionValidator ValidationGroup="Assets"  ValidationExpression="[1-9]" runat="server" ControlToValidate="Source" ID="Validator"/>
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="AlternatingGridItem">
                        <td><ml:EncodedLiteral runat="server" ID="BorrowerName"></ml:EncodedLiteral></td>
                        <td><ml:EncodedLiteral runat="server" ID="Description"></ml:EncodedLiteral></td>
                        <td align="right"><ml:EncodedLiteral runat="server" ID="Amount"></ml:EncodedLiteral></td>
                        <td>
                            <asp:DropDownList runat="server" ID="Source"></asp:DropDownList>
                            <asp:RegularExpressionValidator  ValidationGroup="Assets" ValidationExpression="[1-9]" runat="server" ControlToValidate="Source" ID="Validator"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                </AlternatingItemTemplate>
            </asp:Repeater>
            <div style="width: 382px; float: left; margin: 5px 0px; line-height: 2">
            Primary Gift Funds Source: <ml:EncodedLabel runat="server" ID="sPrimaryGiftFundSource"></ml:EncodedLabel>  
            </div>
            <div style="float: left; margin: 5px; 0px; line-height: 2">
            Total from all sources:  <asp:TextBox runat="server" ReadOnly="true"  style="text-align: right; font-weight: bold;"   ID="sTotGiftFundAsset" Width="100px"></asp:TextBox>
            </div>
            <br style="clear:both;" />
        </fieldset>

        <fieldset style="clear:both;">
            <span class="span-legend">Submission Information</span>
            <div style="padding: 10px">
                <asp:PlaceHolder runat="server" ID="SubmissionIdSection">
                <div id="SubmissionIdDiv" runat="server" style="margin-bottom: 5px" >
                    <div runat="server" id="LenderIdDiv" style=" overflow: auto; line-height:3;" >
                        <input type="radio" group="SChoice" id="sFhaLenderIdT_RegularFhaLender" onclick="FHASubmitForm.ValidatePage();" name="sFhaLenderIdT" runat="server"  value="0" checked="true" />
                        <ml:EncodedLabel runat="server"  AssociatedControlID="sFHALenderIdCode">Lender ID</ml:EncodedLabel>

                        <asp:TextBox runat="server" style="margin-left: 80px"  ID="sFHALenderIdCode" onchange="FHASubmitForm.ValidatePage();" onkeyup="FHASubmitForm.ValidatePage();"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" Display="Static" ID="sFHALenderIdCodeRequired" ControlToValidate="sFHALenderIdCode" ValidationGroup="LenderId"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator runat="server"  ID="sFHALenderIdIs10DigitNumber" ControlToValidate="sFHALenderIdCode" ValidationGroup="LenderId" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                    </div>
                    <div style="overflow: auto; line-height:3">
                        <input type="radio" group="SChoice" id="sFhaLenderIdT_SponsoredOriginatorEIN" name="sFhaLenderIdT" value="1" onclick="FHASubmitForm.ValidatePage();" runat="server" />
                        <ml:EncodedLabel runat="server" AssociatedControlID="sFHASponsoredOriginatorEIN" >Sponsored Originator EIN</ml:EncodedLabel>
                        <asp:TextBox runat="server" ID="sFHASponsoredOriginatorEIN"></asp:TextBox>
                        <asp:RequiredFieldValidator  runat="server" ID="sFHASponsoredOriginatorEINRequired" ControlToValidate="sFHASponsoredOriginatorEIN" ValidationGroup="sFHASponsoredOriginatorEIN" />
                        <asp:RegularExpressionValidator  runat="server" ID="sFHASponsoredOriginatorEINIs9Digits" ControlToValidate="sFHASponsoredOriginatorEIN"  ValidationGroup="sFHASponsoredOriginatorEIN"></asp:RegularExpressionValidator>

                        <ml:EncodedLabel runat="server" AssociatedControlID="sFHASponsorAgentIdCode">Sponsor ID</ml:EncodedLabel>
                        <asp:TextBox runat="server" ID="sFHASponsorAgentIdCode" onchange="FHASubmitForm.ValidatePage();" onkeyup="FHASubmitForm.ValidatePage();"> </asp:TextBox>
                        <asp:RegularExpressionValidator runat="server"  ID="sFHASponsorAgentIdCodeIs10Digits" ValidationExpression="[0-9]{10}" ControlToValidate="sFHASponsorAgentIdCode" ValidationGroup="SponsorId" ErrorMessage="">Sponsor id must be 10 digits.</asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator runat="server" ID="sFHASponsorAgentIdCodeIsRequired"   ValidationGroup="SponsorId" ControlToValidate="sFHASponsorAgentIdCode" ></asp:RequiredFieldValidator>
                    </div>
                    <div style=" overflow: auto; line-height:3">
                        <input type="radio" runat="server" id="sFhaLenderIdT_NoOriginatorId" name="sFhaLenderIdT" value="2" onclick="FHASubmitForm.ValidatePage();" /> No Originator ID
                    </div>
                </div>
                </asp:PlaceHolder>
                <div style="padding-bottom: 5px; display: block; overflow: auto; clear: both; line-height: 2">
                    <ml:EncodedLabel runat="server" Style="display: block; width: 120px; float: left;" AssociatedControlID="sAgencyCaseNum">FHA Case Number</ml:EncodedLabel>
                    <asp:TextBox runat="server" Width="200px" onchange="FHASubmitForm.ValidatePage();"
                        onkeyup="FHASubmitForm.ValidatePage();" ID="sAgencyCaseNum" CssClass="lefty"></asp:TextBox>
                    <img class="lefty Validation" alt="Error" src="../../images/error_icon.gif" id="AgencyE" />
                </div>

                <div style="clear:both; display: block; margin-top: 5px; overflow:auto;  ">
                    <span style="padding-bottom: 5px;">Upload data from the following forms into CHUMS/FHA Connection</span>
                    <asp:CheckBox runat="server" Style="padding-left: 10px; padding-top:5px; margin-top: 1px; display: block; " ID="sTotalScoreUploadData1003"
                        Text="Loan 1003 (URLA)" Enabled="false" />
                    <asp:CheckBox runat="server" Style="padding-left: 10px; display: block; margin-top: 1px;" ID="sTotalScoreUploadDataFHATransmittal"
                        Text="FHA Transmittal Summary (HUD-92900-LT)" />
                </div>
            </div>
        </fieldset>
        <div class="nav">
        <input type="button" id="Submit" value="Submit to TOTAL Scorecard" NoHighlight onclick="FHASubmitForm.SubmitUI(this)" />
        <input type="button"  value="Cancel"  onclick="FHASubmitForm.cancel()" style="margin-left: 10px"/>
        </div>
    </div>
    <div id="IdentityTransactionExplaination" style=" z-index: 900; font-weight:normal; BORDER-RIGHT: black 3px solid; PADDING-RIGHT: 5px; BORDER-TOP: black 3px solid; DISPLAY: none; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: black 3px solid; WIDTH: 420px; PADDING-TOP: 5px; BORDER-BOTTOM: black 3px solid; POSITION: absolute; HEIGHT: 120px; BACKGROUND-COLOR: whitesmoke">

        An identity-of-interest transaction is a sales transaction between parties with a family, business relationship, or other business affiliates, for the purchase of a principal residence.
        <br />
        <br />
        Note: An identify-of-interest transaction does not include an employer/employee transaction when purchasing the seller's principal residence. (HUD 4155.1 9.1.a)
        <br />
        <div class="nav" >
        [<a href="javascript:void(0);" onclick="Modal.Hide();" >close</a>]
        </div>
    </div>
       <div id="IdentityExceptionExplanation" style="z-index:900;width: 520px; height: 360px; font-weight:normal; BORDER-RIGHT: black 3px solid; PADDING-RIGHT: 5px; BORDER-TOP: black 3px solid; DISPLAY: none; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: black 3px solid; PADDING-TOP: 5px; BORDER-BOTTOM: black 3px solid; POSITION: absolute;  BACKGROUND-COLOR: whitesmoke">
        Identity-of-interest transactions on principal residences are restricted to a maximum LTV ratio of <i>85 percent</i>. However, maximum financing above 85 percent LTV is permissible under the following circumstances: 
        <br />
        <br />
        <b>Family Member Purchase</b><br />
        A family member purchases another family member’s home as a principal residence. For a definition of the term “family member,” see HUD 4155.1 9. If the property is sold from one family member to another and is the seller’s investment property, this exception does not apply (the Tenant Purchase exception may apply, however. See below).<br />
        <br />
        <b>Builder’s Employee Purchase</b><br />
        An employee of a builder purchases one of the builder’s new homes or models as a principal residence.<br />
        <br />
        <b>Tenant Purchase</b><br />
        A current tenant, including a family member tenant, purchases the property where he/she has rented for at least six months immediately predating the sales contract.
        <i>Note</i>: A lease or other written evidence to verify occupancy is required.<br />
        The maximum mortgage calculation is not affected by a sales transaction between a tenant and a landlord with no identity of interest relationship.<br />
        <br />
        <b>Corporate Transfer</b><br />
        A corporation<br />
        <span style="margin-left:15px;">- transfers an employee to another location</span><br />
        <span style="margin-left:15px;">- purchases the employee’s home, and</span><br />
        <span style="margin-left:15px;">- sells the home to another employee.</span><br />
        <div class="nav" >
        [<a href="javascript:void(0);" onclick="Modal.Hide();" >close</a>]
        </div>
    </div>

    <div id="WaitMsg" style="position:absolute;  z-index: 900; top: 45%; left: 30%; font-weight:normal; BORDER-RIGHT: black 3px solid; PADDING-RIGHT: 5px; BORDER-TOP: black 3px solid; DISPLAY: none; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: black 3px solid; WIDTH: 190px; PADDING-TOP: 5px; BORDER-BOTTOM: black 3px solid; POSITION: absolute; HEIGHT: 40px; BACKGROUND-COLOR: whitesmoke"> 
        <b>Please wait...</b>
        <br />
        <img src="../../images/status.gif" alt="loading" />
    </div>
    <script type="text/javascript">
        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = retrieveEventTarget(evt);
            if ((evt.keyCode == 13) && (node.type=="text"))  {return false;} <%-- Stop Return/Enter Key --%>
        }

        document.onkeypress = stopRKey; 

        var FHASubmitForm = {   
            cancel : function()  {
                document.location = VRoot + '/newlos/services/FHATotalDataAudit.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %> + '&appid=' + encodeURIComponent(parent.info.f_getCurrentApplicationID());
            },
            init : function() {
                FHASubmitForm.CheckFinMeth();
                FHASubmitForm.ValidatePage();
                if (typeof(Page_ClientValidate) == 'function') {                
                    Page_ClientValidate();
                }
            },
            CheckFinMeth : function() {
                var finMeth = document.getElementById('<%=AspxTools.ClientId(sFinMethT)%>');
                var firstChange = document.getElementById('ArmSetting');
                if ( finMeth.options[finMeth.selectedIndex].value !== "1" ) {
                   
                    firstChange.style.display = 'none';  
                    return true;
                } 
                else {
                 firstChange.style.display = '';     
                  return firstChange.value != '0'; 
                }
            },

            UpdateLenderIdSection : function(){
                
                var sFhaLenderIdT_RegularFhaLender = document.getElementById('sFhaLenderIdT_RegularFhaLender'),
                    sFhaLenderIdT_SponsoredOriginatorEIN = document.getElementById('sFhaLenderIdT_SponsoredOriginatorEIN'),
                    sFhaLenderIdT_NoOriginatorId = document.getElementById('sFhaLenderIdT_NoOriginatorId'),
                    sFHALenderIdCode = document.getElementById('sFHALenderIdCode'),
                    SubmissionIdDiv = document.getElementById('SubmissionIdDiv'),
                    sFHALenderIdCode = document.getElementById('sFHALenderIdCode'),
                    sFHALenderIdCodeRequired = document.getElementById('sFHALenderIdCodeRequired'),
                    sFHALenderIdIs10DigitNumber = document.getElementById('sFHALenderIdIs10DigitNumber'),
                    sFHASponsoredOriginatorEINIs9Digits = document.getElementById('sFHASponsoredOriginatorEINIs9Digits'),
                    sFHASponsorAgentIdCodeIs10Digits = document.getElementById('sFHASponsorAgentIdCodeIs10Digits'),
                    sFHASponsorAgentIdCodeIsRequired = document.getElementById('sFHASponsorAgentIdCodeIsRequired'),
                    sFHASponsoredOriginatorEINRequired = document.getElementById('sFHASponsoredOriginatorEINRequired'),
                    sFHASponsoredOriginatorEIN = document.getElementById('sFHASponsoredOriginatorEIN');
                    
                    
                if(!SubmissionIdDiv ){
                    return; //nothing to do
                }
                
                
                if(sFhaLenderIdT_RegularFhaLender.checked){
               
                    sFHALenderIdCode.readOnly = false;
                    sFHASponsoredOriginatorEIN.readOnly = true;
                    ValidatorEnable(sFHALenderIdCodeRequired, true);
                    ValidatorEnable(sFHALenderIdIs10DigitNumber, true);
                    ValidatorEnable(sFHASponsorAgentIdCodeIs10Digits, true); 
                    ValidatorEnable(sFHASponsorAgentIdCodeIsRequired, false); 
                    ValidatorEnable(sFHASponsoredOriginatorEINIs9Digits, false);
                    ValidatorEnable(sFHASponsoredOriginatorEINRequired, false);
                    
                }else if( sFhaLenderIdT_SponsoredOriginatorEIN.checked ){
                    sFHALenderIdCode.readOnly = true;   
                    sFHASponsoredOriginatorEIN.readOnly = false;
                    ValidatorEnable(sFHASponsoredOriginatorEINIs9Digits, true);
                    ValidatorEnable(sFHASponsorAgentIdCodeIsRequired, true); 
                    ValidatorEnable(sFHASponsoredOriginatorEINRequired, true);
                    ValidatorEnable(sFHALenderIdCodeRequired, false);
                    ValidatorEnable(sFHALenderIdIs10DigitNumber, false);
                }
                else{
                    sFHALenderIdCode.readOnly = true;
                    sFHASponsoredOriginatorEIN.readOnly = true;
                    sFHALenderIdCode.value = '';
                    sFHASponsoredOriginatorEIN = '';
                    sFHASponsoredOriginatorEIN.value = '';
                    ValidatorEnable(sFHALenderIdCodeRequired, false);
                    ValidatorEnable(sFHALenderIdIs10DigitNumber, false);
                    ValidatorEnable(sFHASponsorAgentIdCodeIs10Digits, true); 
                    ValidatorEnable(sFHASponsorAgentIdCodeIsRequired, true); 
                    ValidatorEnable(sFHASponsoredOriginatorEINIs9Digits, false);
                    ValidatorEnable(sFHASponsoredOriginatorEINRequired, false);
                }
           
            },
            
       
            ValidatePage : function() { 
                
                FHASubmitForm.UpdateLenderIdSection();
                
                
                var isPageValid = FHASubmitForm.ValidateRefinanceType(); 
                if (typeof(Page_ClientValidate) == 'function') {                
                    isPageValid &= Page_ClientValidate();
                }
                isPageValid &= FHASubmitForm.Validate34UnitInfo();
                isPageValid &= FHASubmitForm.ValidateLoanInfo();
                isPageValid &= FHASubmitForm.ValidateSubmisionInfo();
                isPageValid &= FHASubmitForm.ValidateBorrowerInfo();
                var sbtn = document.getElementById('Submit');
                sbtn.disabled = !isPageValid;
            }, 
            
            
            
            ValidateRefinanceType : function() { 
                var refinanceType = <%= AspxTools.JsGetElementById(sTotalScoreRefiT) %>; 
                if ( refinanceType == null ) { return true; }
                var selectedRefiType = refinanceType.options[refinanceType.selectedIndex].text;
                var typeIsBlank = selectedRefiType === '';
                var typeIsInvalid = selectedRefiType === 'Unknown';

                var req = document.getElementById('RefinanceTypeR'); 
                var invalidMsg = document.getElementById("RefinanceTypeInvalidMessage");
                req.style.display = typeIsBlank ? 'block' : 'none';
                invalidMsg.style.display = typeIsInvalid ? 'block' : 'none';
                return !(typeIsBlank || typeIsInvalid);
            },
            Validate34UnitInfo : function() { 
                var unit34Info = document.getElementById('unit34');
                if ( unit34Info == null ) { return true; }
                var isValid = true;
                var appraiserEstimateField = document.getElementById('sTotalScoreAppraisedFairMarketRent');
                var appraiserEstimate = parseFloat(appraiserEstimateField.value.replace(/[$]/, '')); 
                var appraiserEstimateR = document.getElementById('AppraiserEstimateRequire');
   
                
                var vacancyFactorField = document.getElementById('<%= AspxTools.ClientId(sTotalScoreVacancyFactor) %>');
                var vacancyValue = parseFloat(vacancyFactorField.value.replace(/[%]/, ''));  
                var vacancyR = document.getElementById('VacancyR');   
                var vacancyE = document.getElementById('VacancyE');
                     
                if ( appraiserEstimateField.value === '' ) {
                      isValid = false;
                    appraiserEstimateR.style.display  = 'block'; 
                }  
                else if (isNaN(appraiserEstimate)  || appraiserEstimate <= 0.00) {
                    isValid = false;
                    appraiserEstimateR.style.display  = 'block'; 
                }
                else {
                     appraiserEstimateR.style.display  = 'none'; 
                }
                if ( vacancyFactorField.value === '' ) {
                    isValid = false;
                    vacancyR.style.display  = 'block'; 
                    vacancyE.style.display = 'none';  
                } 
                else if (isNaN(vacancyValue)  || vacancyValue <= 0.00) {
                    isValid = false;
                    vacancyR.style.display  = 'none'; 
                    vacancyE.style.display = 'block';
                } 
                else {
                    vacancyR.style.display  = 'none'; 
                    vacancyE.style.display = 'none';
                }
    
                return isValid;

            }, 
            
            ValidateLoanInfo : function() { 
               var fhaDdl = document.getElementById('<%= AspxTools.ClientId(sTotalScoreFhaProductT) %>');
               var fhaR = document.getElementById('FhaProgramR');
               var isValid = true;
               if ( fhaDdl.options[fhaDdl.selectedIndex].text === '' )  {
                    fhaR.style.display = 'block';
                    isValid = false;
               } 
               else {
                    fhaR.style.display = 'none';
               }
               
               var loanIdentity = document.getElementById('<%= AspxTools.ClientId(sTotalScoreIsIdentityOfInterest) %>');
               var loanIdentityR = document.getElementById('LoanIdentityR'); 
              
               
               var isIdentChecked = false;
               var showTransactionException = false;
               for(var i = 0; i < loanIdentity.childNodes.length; i++ ) {
                if ( typeof( loanIdentity.childNodes[i].type ) === 'undefined' )  {
                    continue;
                }
                if ( loanIdentity.childNodes[i].checked ) {
                    isIdentChecked = true;
                    showTransactionException = (loanIdentity.childNodes[i].value == "true");
                    break;
                }
               }
              if ( isIdentChecked )  {
                loanIdentityR.style.display = 'none';
              } 
              else {
                loanIdentityR.style.display = ''; 
                isValid = false;
              }
               
                             
              var transException = document.getElementById('<%= AspxTools.ClientId(sTotalScoreIsTransactionException) %>');
              var transExceptionR = document.getElementById('TransExceptionR'); 
              var transExceptionPanel = document.getElementById('IdentityOfInterestExceptionPanel');
              
              transExceptionPanel.style.display = showTransactionException ? '' : 'none';
              
              if (showTransactionException){
                   var isTransChecked = false;
                   for(var i = 0; i < transException.childNodes.length; i++ ) {
                    if ( typeof( transException.childNodes[i].type ) === 'undefined' )  {
                        continue;
                    }
                    if ( transException.childNodes[i].checked ) {
                        isTransChecked = true;
                        break;
                    }
                   }
                  if ( isTransChecked )  {
                    transExceptionR.style.display = 'none';
                  } 
                  else {
                    transExceptionR.style.display = ''; 
                    isValid = false;
                  }
               }
               else{
                    transExceptionR.style.display = 'none';
               }
               
               var rateField = document.getElementById('<%= AspxTools.ClientId(sNoteIR) %>');
               var rateValue = parseFloat(rateField.value.replace(/[%]/, ''));
               var rateR = document.getElementById('RateR');
               
               if ( rateField.value === '' ) {
                    rateR.style.display = 'block';
                    isValid = false;
               }
               
               else if ( isNaN(rateValue) || rateValue <= 0.000  ) {
                    rateR.style.display = 'block';
                    isValid = false;
               }
               else { 
                    rateR.style.display = 'none';
               }
               
               var termField = document.getElementById('<%= AspxTools.ClientId(sTerm) %>');
               var termE = document.getElementById('TermE'); 
               var termV = parseInt(termField.value);
               
               if ( termField.value === '' ) {
                    termE.style.display = 'block'; 
                    isValid = false;
               } 
               else if ( !termField.value.match(/^[0-9]+$/) || termV <= 0) {
                  
                    termE.style.display = 'block'; 
                    isValid = false;
               }
               else  {
                 termE.style.display = 'none';
                 
               }
               
               var armsetting = document.getElementById('ArmSetting');
              
               if ( armsetting.style.display !== 'none' ) {
                    var firstChange = document.getElementById('<%= AspxTools.ClientId(sRAdj1stCapMon) %>');
                    var firstChangeV = parseInt(firstChange.value);
                    var firstE = document.getElementById('ArmError');
                    if ( firstChange.value === '' ) {
                        isValid = false;
                        firstE.style.display = 'block';
                    } 
                    else if ( !firstChange.value.match(/^[0-9]+$/) || firstChangeV <= 0   ) {
                       
                        isValid = false;
                        firstE.style.display = 'block';
                    }
                    else {
                        firstE.style.display = 'none'; 
                    }
               }
               
                var financingSource = document.getElementById('<%= AspxTools.ClientId(sTotalScoreSecondaryFinancingSrc) %>');               

                if (financingSource != null) {
                   var undefinedValue = <%= AspxTools.JsString(TotalScoreCard.E_TotalScorecardSecondaryFinancingSrc.Undefined.ToString("D")) %>;
                   var governmentNonprofitInstrumentalityValue = <%= AspxTools.JsString(TotalScoreCard.E_TotalScorecardSecondaryFinancingSrc.GovernmentNonprofitInstrumentality.ToString("D")) %>;
                   var financingSourceSelectedValue = financingSource.options[financingSource.selectedIndex].value;

                   var errorIcon = document.getElementById('sTotalScoreSecondaryFinancingSrcError');
                   var errorMessage = document.getElementById('sTotalScoreSecondaryFinancingSrcErrorMessage');
                   if (financingSourceSelectedValue === undefinedValue || financingSourceSelectedValue === governmentNonprofitInstrumentalityValue) {
                       errorIcon.style.display = 'block';
                       errorMessage.style.display = 'block';
                       isValid = false;
                   }
                   else {
                       errorIcon.style.display = 'none';
                       errorMessage.style.display = 'none';
                   }
                }

                return isValid;
            },
            
            ValidateSubmisionInfo : function() { 
                var isValid = true;
               var lenderId = document.getElementById('<%= AspxTools.ClientId(sFHALenderIdCode) %>');
               
               var isShowLenderId = ( lenderId != null );
               
               var sponsor = document.getElementById('<%=AspxTools.ClientId(sFHASponsorAgentIdCode)%>');
               
               var agencyCaseN = document.getElementById('<%= AspxTools.ClientId(sAgencyCaseNum) %>');
               var agencyCaseR = document.getElementById('AgencyE');
               
               var chb = document.getElementById('<%= AspxTools.ClientId(sTotalScoreUploadData1003) %>');
               var chb2 = document.getElementById('<%=AspxTools.ClientId(sTotalScoreUploadDataFHATransmittal)%>');
               var chb2span = chb2.parentNode;
              
//               if ( isShowLenderId )
//               {
//                   if ( lenderId.value.trim().length === 0 || !lenderId.value.match(/^[0-9]{10}$/) ) {
//                        lenderR.style.display = "block";
//                        isValid = false;
//                   }  
//                   else {
//                    lenderR.style.display = "none";
//                   }
//               }
               if ( agencyCaseN.value.trim().length === 0 ) {
                 agencyCaseR.style.display = 'none';    
                 chb.checked = false;
                 chb2.checked = false;
                 chb2.disabled = true;
                 chb2span.disabled = true;
               }
               else if ( agencyCaseN.value.match(/^[0-9]{3}[-][0-9]{7}(-.*)*$/)  )  {
                    agencyCaseR.style.display = 'none';
                    chb.checked = true;
                    chb2.disabled = false;
                    chb2.checked = false;
                    chb2span.disabled = false;
               } 
               else {               
                    agencyCaseR.style.display = 'block';
                    isValid = false;
                    chb.checked = false;    
                    chb2.checked = false;
                    chb2.disabled = true;    
                    chb2span.disabled = true;
               }
               
//               if ( isShowLenderId )
//               {
//                   if ( sponsor.value === '' ) {
//                        sponsorE.style.display = 'none';
//                   }
//                   else if ( !sponsor.value.match(/^[0-9]{10}$/) ) {
//                        sponsorE.style.display = 'block';
//                
//                        isValid = false;
//                      
//                   }
//                   else{
//                        sponsorE.style.display = 'none';
//                   }
//                }
               return isValid;
            },
            

            JumpTo: function(page, params) {
                if ( !params ) { params = ''; }
                var appid = encodeURIComponent(parent.info.f_getCurrentApplicationID());
                var url =<%= AspxTools.JsString( VirtualRoot ) %> +'/newlos/' + page + '?loanid='+<%= AspxTools.JsString( LoanID ) %>+params+'&appid=' + appid ;
                document.location.href = url;
            } ,
            
            ValidateBorrowerInfo : function() {
                var isValid = true;
                var doc = document.getElementById('BorrowerInfoFieldSet');
                var ddls = doc.getElementsByTagName('select');
                for(var i = 0; i< ddls.length; i++) {
                    var ddl = ddls[i];

                    var cb = document.getElementById(ddl.getAttribute('CB'));
                    var img = document.getElementById(ddl.getAttribute('RI'));
                    FHASubmitForm.CheckFTHB(cb, ddl);
                    if ( ddl.options[ddl.selectedIndex].text === '' ) {
                        img.style.display = '';
                        isValid = false;
                    } 
                    else {
                        img.style.display = 'none';
                    }
                    
                    
                }
                
                return isValid;
                
            },
            
            CheckFTHB : function(chk, ddl) {  
                if (!chk.checked && ML.useLegacyCounselingDefinition) {
                    if ( ddl.options.length  < 4 ) {
                        var option = new Option("Not Applicable", "3",true);
                        ddl.options.add(option);
                    }
                    
                    ddl.options[3].selected = true; 
                   ddl.style.backgroundColor = 'gainsboro';
                    ddl.disabled = true;
                }
                else  {
                    if ( ddl.options.length == 4 ) {
                        ddl.remove(3);
                         ddl.disabled = false;
                         ddl.style.backgroundColor = 'white';
                    }
                    
                }
            },
            
            RefreshRentalIncome : function() { 
                var marketRent = document.getElementById('<%= AspxTools.ClientId(sTotalScoreAppraisedFairMarketRent) %>');
                var vancyFactor = document.getElementById('<%= AspxTools.ClientId(sTotalScoreVacancyFactor) %>');
                var resultField = document.getElementById('<%= AspxTools.ClientId(sTotalScoreNetRentalIncome) %>');
                
                var args = { 
                    loanid : <%= AspxTools.JsString(LoanID) %>,
                    sTotalScoreAppraisedFairMarketRent : marketRent.value, 
                    sTotalScoreVacancyFactor : vancyFactor.value
                };
                
                var y = gService.FHATotal.call("RefreshRentalIncome", args );
                
                if ( y.error ) {
                    alert( y.UserMessage );
                }
                else {
                    resultField.value = y.value.sTotalScoreNetRentalIncome;
                }

            },
            SubmitUI : function(btn) {
                // OPM 106224: Check for credit report
                if (<%= AspxTools.JsBool(!hasCreditReport) %> )
                {
                    alert("Cannot submit because there is no credit report on this application.");
                    return;
                }
            
                btn.disabled = true;
                Modal.Show('WaitMsg');
                btn.blur();
                window.setTimeout( function() {
                    FHASubmitForm.SubmitInfo(btn);
                }, 500);
            },
            
            UpdateAssetData : function(){
                var data = FHASubmitForm.GetAssetSources();
                data.loanid =  <%=AspxTools.JsString(LoanID) %>;  
                var results = gService.FHATotal.call("UpdateAssetInfo", data);
                if( results.error ){
                    alert(results.UserMessage);
                }
                else {
                    var source = document.getElementById('sPrimaryGiftFundSource');
                    source.innerText  = results.value["sPrimaryGiftFundSource"];
                }
                
                FHASubmitForm.ValidatePage();
            },
            
            GetAssetSources : function() {
                
                var table = document.getElementById('GiftFundAssets');
                if(!table){
                    return { 'AssetTot' : 0 };
                }
                var sources = table.getElementsByTagName("select"), 
                    count = sources.length,
                    select,
                    data = { 'AssetTot' : count };

                for( var i = 0; i < count; i++ ){
                     select = sources[i];
                     data["AssetId_" + i] = select.getAttribute('htmldataassetid');
                     data["AssetSource_" + i] = select.value;
                     data["AssetAppId_" + i] = select.getAttribute('htmldataappid');
                }   
                
                return data;
            },
            
            SubmitInfo : function(btn) {
                
                var args = getAllFormValues(),
                    assetData = FHASubmitForm.GetAssetSources(),
                    propName;
                    
                args["loanid"] = <%=AspxTools.JsString(LoanID) %>;  
                
       
                for( propName in assetData ){
                    if( assetData.hasOwnProperty(propName) ){
                        args[propName] = assetData[propName];
                    }
                }
                
                var results = gService.FHATotal.call("SaveAndSubmit", args);
                if( results.error ){
                  if (results.ErrorType == 'VersionMismatchException') {
                  f_displayVersionMismatch();
                  }
                  else if (results.ErrorType === 'LoanFieldWritePermissionDenied') 
                  {
                    f_displayFieldWriteDenied(results.UserMessage);
                  }
                  else {
                    alert( results.UserMessage );               
                  }
                } 
                
                
                else if ( results.value['hasConflict'] === "True" ) {
                     clearDirty();
                     showModal('/newlos/services/FHADataConflictAlert.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %>, null, null, null, function(modalArgs){ 
                         if ( modalArgs.usercommand === 'submit' ) {
                            var bypassArgs = { 
                                Choice : modalArgs.choice,
                                loanId : <%=AspxTools.JsString(LoanID) %>,
                                Success : 'True'
                            };
                            
                            var bypassResults = gService.FHATotal.call("Submit", bypassArgs); 
                            if ( bypassResults.error ) {
                                alert ( bypassResults.UserMessage );
                                
                            }
                            else if ( bypassResults.value["Success"] === "True" ) {
                               showModal("/newlos/services/FHATotalFindings.aspx?fv=1&loanid="+<%=AspxTools.JsString(LoanID) %>, null, null, null, null,{hideCloseButton:true});
                            } 
                            else {
                               showModal("/newlos/services/FHAErrorResults.aspx?loanid="+<%=AspxTools.JsString(LoanID) %> + "&errorkey=" + bypassResults.value["ErrorKey"], null, null, null, null,{hideCloseButton:true});
                            }
                         }
                     },{hideCloseButton:true});
              
                }
                else if (results.value['Success'] === "True" ) {
                         clearDirty();
                      showModal( "/newlos/services/FHATotalFindings.aspx?fv=1&loanid="+<%=AspxTools.JsString(LoanID) %>, null, null, null, null,{hideCloseButton:true});
                }
                else {
                     clearDirty();
                   showModal("/newlos/services/FHAErrorResults.aspx?loanid="+<%=AspxTools.JsString(LoanID) %> + "&errorkey=" + results.value["ErrorKey"], null, null, null, null,{hideCloseButton:true});
                }
             
              btn.disabled = false;
              Modal.Hide();
            }
          
        };
        
        
        function debug( obj )  {
           
                var result = " "; 
                for (curProperty in obj) {
                    result += curProperty + " ";
                }
                alert( result );
                
        }
        
        function _init() {
            FHASubmitForm.init(); 
            if (typeof(Page_ClientValidate) == 'function') {
                Page_ClientValidate();
            }

            $('.first-time-home-buyer-declarations-item-number').text(ML.IsTargeting2019Ulad ? 'a' : 'm');

            onSecondaryFinancingLocked();

            var mortgageLiabilities = $('#mortgageLiabilities').find('span');
            mortgageLiabilities.map(function(){
                // find the span that is disabled, then disable all child input (radio buttons)
                if (hasDisabledAttr(this)) {
                    var inputs = $(this).find('input');
                    setDisabledAttr(inputs, true);
                }
            });            
        }
        
        function f_saveMe() {
            var args = getAllFormValues(),
                assetData = FHASubmitForm.GetAssetSources(),
                propName;
            
            args["loanid"] = <%=AspxTools.JsString(LoanID) %>;
            for( propName in assetData ){
                if( assetData.hasOwnProperty(propName) ){
                    args[propName] = assetData[propName];
                }
            }
            
            var res = gService.FHATotal.call("Save", args);
            if( res.error ){
                alert( res.UserMessage ); 
                return false;
            }
            else{
                clearDirty();
                return true; 
            }
        }
        
        String.prototype.trim = function() {
            return this.replace(/^\s+|\s+$/g, '');
        };
        
        function SetUniqueRadioButton(nameregex, current) {
                var re = new RegExp(nameregex);  
    
                for(i = 0; i < document.forms[0].elements.length; i++) {
                        var elm = document.forms[0].elements[i];
                        if ( elm.CName === 'undefined' || elm.type !== 'radio') { continue; }
                        if (re.test(elm.name))
                        {
                            elm.checked = false;
                            EnableCNames(elm.getAttribute('cname'), true);
                        }
                }
                
                current.checked = true;
                EnableCNames(current.getAttribute('cname'), false);
        }
        
        function EnableCNames(cname, enable) {
            var radioButtons = document.getElementById(cname).childNodes;

            for ( var i = 0; i < radioButtons.length; i++ ) {
                var control = radioButtons[i];
                if ( control.type !== 'radio' ) { continue; }
                if ( !enable ) {
                    if ( control.value === 'SubjectProperty' ) { control.checked = true; }
                    else { control.checked = false; }
                }
                setDisabledAttr(document.getElementById(cname), !enable);
            }
        }

        function goToDeclarations(appId) {

            if (parent && parent.info && 
                    parent.info.f_getCurrentApplicationID && typeof parent.info.f_getCurrentApplicationID === 'function' &&
                    parent.info.f_setCurrentApplicationID && typeof parent.info.f_setCurrentApplicationID === 'function' &&
                    parent.info.f_getCurrentApplicationID() != appId) {
                parent.info.f_setCurrentApplicationID(appId);
            }

            if (parent &&
                parent.treeview &&
                parent.treeview.load &&
                typeof parent.treeview.load === 'function') {
                if (ML.IsTargeting2019Ulad) {
                    parent.treeview.load('Ulad/Declarations.aspx');
                }
                else {
                    parent.treeview.load('Declarations.aspx');
                }
            }
        }

        function onSecondaryFinancingLocked() {
            var financingSource = document.getElementById('sTotalScoreSecondaryFinancingSrc');
            if (financingSource) {
                var sourceLocked = document.getElementById('sTotalScoreSecondaryFinancingSrcLckd');

                if (sourceLocked.checked) {
                    financingSource.disabled = false;
                }
                else {
                    financingSource.disabled = true;
                }
            }
        }
    </script>
     <ML:CModalDlg id="m_ModalDlg" runat="server"></ML:CModalDlg>
    </form>
        <!--[if lte IE 6.5]><iframe id="selectmask" style="display:none; position:absolute;top:0px;left:0px;filter:mask()"  src="javascript:void(0)" ></iframe><![endif]-->

</body>

</html>
