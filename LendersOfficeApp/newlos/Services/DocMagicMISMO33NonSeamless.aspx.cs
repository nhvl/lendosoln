﻿namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Services;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Conversions;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Integration.DocumentVendor.LQBDocumentResponse;
    using LendersOffice.ObjLib.DocMagicLib;
    using LendersOffice.Security;
    using DsiRequest = global::DocMagic.DsiDocRequest;
    using DsiResponse = global::DocMagic.DsiDocResponse;

    public partial class DocMagicMISMO33NonSeamless : BaseLoanPage
    {
        protected IDocumentVendor Vendor;
        private DocumentVendorServer server;
        private VendorCredentials creds;

        protected Guid VendorId
        {
            get
            {
                var enabledVendors = DocumentVendorBrokerSettings.ListAllForBroker(BrokerID);
                var legacyDocMagicId = Guid.Empty;

                if (enabledVendors.Any(v => v.VendorId == ConstStage.DocMagicProductionVendorId))
                {
                    return ConstStage.DocMagicProductionVendorId;
                }
                else if (enabledVendors.Any(v => v.VendorId == ConstStage.DocMagicStageVendorId))
                {
                    return ConstStage.DocMagicStageVendorId;
                }
                else if (enabledVendors.Any(v => v.VendorId == legacyDocMagicId))
                {
                    return legacyDocMagicId;
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Client does not have a DocMagic vendor configured.");
                }
            }
        }

        private CPageData loanData = null;

        protected override void LoadData()
        {
            //OPM 115489: Users will need to be able to read these fields even if they don't have access to them normally.
            loanData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(LoanID, typeof(DocMagicMISMO33NonSeamless));
            loanData.InitLoad();

            if (loanData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                string errorMessage = null;

                if (loanData.sHasLoanEstimateArchiveInPendingStatus)
                {
                    var archive = loanData.sLoanEstimateArchiveInPendingStatus;
                    errorMessage = ErrorMessages.ArchiveError.UnableToGenerateDocumentsBecauseOfPendingArchive(
                        archive.DateArchived,
                        archive.Status,
                        userCanDetermineStatus: false);
                }
                else if (loanData.sHasClosingCostArchiveInUnknownStatus)
                {
                    var archive = loanData.sClosingCostArchiveInUnknownStatus;
                    errorMessage = ErrorMessages.ArchiveError.UnableToGenerateDocumentsBecuaseOfUnknownArchive(
                        archive.DateArchived,
                        archive.ClosingCostArchiveType,
                        userCanDetermineStatus: false);
                }

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    throw new CBaseException(errorMessage, "Cannot generate non-seamless docs with Pending/Unknown archive.");
                }
            }

            var packages = Vendor.GetAvailableDocumentPackages(LoanID, PrincipalFactory.CurrentPrincipal);
            if (packages.HasError)
            {
                m_AlertMessage.Value = packages.ErrorMessage;
                Disable.Value = true.ToString();
                return;
            }

            foreach (var p in packages.Result)
            {
                m_ddlPackageType.Items.Add(new ListItem(p.Description.ToString(), GetPackageString(p)));
            }

            BindPlanCodePicker(LoanID);

            if (!string.IsNullOrEmpty(loanData.sDocMagicPlanCodeNm)) m_ddlPlanCode.SelectedValue = loanData.sDocMagicPlanCodeNm;
            else if (Vendor.Skin.AutomaticPlanCodes) m_ddlPlanCode.Attributes["disabled"] = "disabled";

            ClientScript.RegisterHiddenField("PlanCodeLabel", Vendor.Skin.NameForPlanCodes);
        }

        private string GetPackageString(DocumentPackage p)
        {
            return p.Type + ":" + BoolToYN(p.AllowEPortal) + ":" + BoolToYN(p.DisableESign) + ":" + BoolToYN(p.AllowManualFulfillment);
        }

        private static string BoolToYN(bool input)
        {
            return input ? "Y" : "N";
        }

        private void BindPlanCodePicker(Guid LoanId)
        {
            var planCodes = Vendor.GetAvailablePlanCodes(LoanId, PrincipalFactory.CurrentPrincipal);
            if (planCodes.HasError)
            {
                m_AlertMessage.Value = planCodes.ErrorMessage;
                Disable.Value = true.ToString();
                return;
            }

            BindPlanCodeDictionary(m_ddlPlanCode, planCodes.Result);
        }

        private void BindPlanCodeDictionary(DropDownList planCodes, Dictionary<string, IEnumerable<PlanCode>> codes)
        {
            foreach (var type in codes.Keys)
            {
                planCodes.Items.Add("");
                string header = type.ToUpper() + " " + Vendor.Skin.NameForPlanCodes.ToUpper() + "S";

                planCodes.Items.Add(new ListItem("<----- " + header + " ----->", ""));

                if (type.Equals("Investor", StringComparison.OrdinalIgnoreCase))
                {
                    foreach (var group in codes[type].ToLookup(p => p.Investor))
                    {
                        planCodes.Items.Add(new ListItem("<----- " + group.Key + " ----->", ""));
                        foreach (var planCode in group.OrderBy(a => a.Description))
                        {
                            planCodes.Items.Add(new ListItem(planCode.Description, planCode.Code));
                        }
                    }
                }
                else
                {
                    foreach (var planCode in codes[type].OrderBy(a => a.Description))
                    {
                        planCodes.Items.Add(new ListItem(planCode.Description, planCode.Code));
                    }
                }
            }
        }

        [WebMethod]
        public static bool NeedsPlanCode(Guid sLId, Guid VendorId)
        {
            HttpContext.Current.Response.AddHeader("Connection", "close");
            if (!DocumentVendorFactory.Create(PrincipalFactory.CurrentPrincipal.BrokerId, VendorId).Skin.AutomaticPlanCodes) return false;

            var dataLoan = new CPageData(sLId, new[] { "sDocMagicPlanCodeId", "sDocMagicPlanCodeNm" });
            dataLoan.InitLoad();

            //Need to use the plan code name, because it gets cleared out if the plan code doesn't exist (but the id doesn't).
            if (string.IsNullOrEmpty(dataLoan.sDocMagicPlanCodeNm))
            {
                Guid result = DocMagicPlanCodeEvaluator.CalculateDocMagicPlanCode(sLId);
                if (result == DocMagicPlanCode.PendingUserChoice) return true;

                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.sDocMagicPlanCodeId = result;
                dataLoan.Save();
            }

            return false;
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "DocMagicDocGeneration";
            this.PageID = "DocMagicDocGeneration";
        }

        private void PageLoad(object sender, System.EventArgs e)
        {
            Vendor = DocumentVendorFactory.Create(this.BrokerID, VendorId);
            if (Vendor == null)
            {
                m_AlertMessage.Value = "Unable to load Vendor";
                return;
            }
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.IE9;
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = false; // There is no service page, so set this to false so that the smoke test won't choke on this page
            DisplayCopyRight = false;
            RegisterJsScript("ModelessDlg.js");
            this.EnableJqueryMigrate = false;

            this.Init += new System.EventHandler(this.PageInit);
            this.Load += new System.EventHandler(this.PageLoad);
            base.OnInit(e);
        }

        protected void SubmitBtn_Click(object sender, EventArgs e)
        {
            m_btnSubmit.Disabled = true;
            // This will hold the plan code number. For example, 30 YEAR FIXED RATE CONFORMING
            // has plan code BB_101
            Results.InnerText = string.Empty;
            
            var loanProgram = m_ddlPlanCode.SelectedValue;

            // This variable will have the following format. The ":" is a separator, see line
            // 79 above:
            // 
            // PackageName:AllowEPortal:DisableESign:AllowManualFulfillment
            //
            // PackageName is a string
            // AllowEPortal is a boolean Y or N
            // DisableESign is a boolean Y or N
            // AllowManualFulfillment is a boolean Y or N
            var packageType = m_ddlPackageType.SelectedValue;
            string[] parts = packageType.Split(':');

            // OPM 235850 - Check that user has workflow privileges before allowing audit to continue.
            string failureReasons;
            if (!this.Vendor.CanGenerateDocuments(PrincipalFactory.CurrentPrincipal, this.LoanID, parts[0], out failureReasons))
            {
                Results.InnerText = failureReasons;
                m_btnSubmit.Disabled = false;
                return;
            }

            creds = GetCredentials(PrincipalFactory.CurrentPrincipal, VendorId);
            server = new DocumentVendorServer(DocMagicDocumentVendor.DocMagicVendorConfig, BrokerID, creds);

            var auditResult = Vendor.AuditLoan(LoanID, ApplicationID, parts[0], loanProgram, PrincipalFactory.CurrentPrincipal, preventUsingTemporaryArchive: true);
            if (auditResult.HasError)
            {
                Results.InnerText = auditResult.ErrorMessage;
                m_btnSubmit.Disabled = false;
            }
            else
            {
                LaunchApplet();
            }
        }

        private static VendorCredentials GetCredentials(AbstractUserPrincipal user, Guid VendorId)
        {
            if (user == null || user is InternalUserPrincipal || user is SystemUserPrincipal)
            {
                //Since we don't have a valid user or branch, we can't really get credentials
                return new VendorCredentials(Guid.Empty, Guid.Empty, Guid.Empty, Guid.Empty);
            }
            else
            {
                return new VendorCredentials(user.UserId, user.BranchId, user.BrokerId, VendorId);
            }
        }

        private static AuditLine MakeAuditLine(AuditMessage audit, Guid LoanId, Guid AppId)
        {
            string message = audit.Description;
            AuditSeverity severity = ConvertFromDocRequest(audit.Severity);
            string editPage;
            string fieldId;

            if (string.IsNullOrEmpty(audit.FieldID))
            {
                var mapping = DocMagicXpathMapping.Retrieve(audit.MISMOPath);
                if (mapping == null)
                {
                    editPage = null;
                    fieldId = null;
                }
                else
                {
                    editPage = mapping.DefaultURL;
                    fieldId = mapping.FieldId;
                }
            }
            else
            {
                fieldId = audit.FieldID;
                editPage = null;
            }

            return new AuditLine(message, severity, editPage, fieldId, audit.AuditDetails ?? "", LoanId, AppId);
        }

        private static AuditSeverity ConvertFromDocRequest(AuditMessageSeverity auditMessageSeverity)
        {
            switch (auditMessageSeverity)
            {
                case (AuditMessageSeverity.FATAL):
                    return AuditSeverity.Fatal;
                case (AuditMessageSeverity.WARNING):
                    return AuditSeverity.Warning;
                case (AuditMessageSeverity.MISC):
                    return AuditSeverity.Misc;
                case (AuditMessageSeverity.INFO):
                    return AuditSeverity.Info;
                case (AuditMessageSeverity.CRITICAL):
                    return AuditSeverity.Critical;
                default:
                    Tools.LogWarning("Unknown audit message severity: " + auditMessageSeverity);
                    return AuditSeverity.Undefined;
            }
        }

        private void LaunchApplet()
        {
            loanData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(LoanID, typeof(DocMagicMISMO33NonSeamless));
            loanData.InitLoad();

            Regex pattern = new Regex("[^a-zA-Z0-9-]+");
            string websheetNumber = pattern.Replace(loanData.sLNm, "-");

            DsiRequest.DsiDocumentServerRequest request = DocMagicMismoRequest.CreateLaunchAppletRequest(creds.CustomerId, creds.UserName, creds.Password, RequestHelper.LoanID, websheetNumber);
            DsiResponse.DsiDocumentServerResponse response = DocMagicServer2.Submit(request);

            if (null != response)
            {
                switch (response.Status)
                {
                    case DsiResponse.E_DsiDocumentServerResponseStatus.Success:
                        WriteAppletHtml(response.AppletResponse.AppletHtml);
                        break;
                    case DsiResponse.E_DsiDocumentServerResponseStatus.Failure:
                        BuildErrorMessage(response);
                        break;
                }
            }
        }

        private void BuildErrorMessage(DsiResponse.DsiDocumentServerResponse dsiServerResponse)
        {
            StringBuilder sb = new StringBuilder();
            foreach (DsiResponse.Message msg in dsiServerResponse.MessageList)
            {
                if (msg.Type == global::DocMagic.DsiDocResponse.E_MessageType.Fatal )
                    sb.Append(msg.InnerText).Append(Environment.NewLine);
            }
            Results.InnerText = sb.ToString();
        }

        private void WriteAppletHtml(string appletHtml)
        {
            Response.ClearContent();
            Response.Output.Write(appletHtml);
            Response.Flush();
            Response.End();
        }
    }
}