﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GlobalDMSOrderAppraisalList.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.GlobalDMSOrderAppraisalList" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess"%>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title>View Appraisal Orders</title>
<style type="text/css">
    td {
        vertical-align: top;
    }
    .hidden
    {
        display: none;
    }
    .ActionLink
    {
        margin-right: 4em;
        text-align: left;
    }
    .MenuLinks
    {
        padding-left: 1.5em;
        display: block;
    }
    .wrap-order-ctl01-message {
        padding-left: 1em;
    }
    .order-ctl01-message {
        background: red;
        color: white;
        width: 10px;
        height: 10px;
        line-height: 10px;
        font-size: 9px;
        text-align: center;
        margin-top: 3px;
        margin-left: 4px;
        display: inline-block;
        text-decoration: none;
    }
    .order-ctl01-message:hover{
        cursor: default;
    }
    .order-ctl01-message-sub {
        display: inline-block;
        margin-left: -15px;
        margin-right: 4px;
    }
</style>
</head>
<body bgcolor="gainsboro" style="padding: 5px;">
    <input type="hidden" id="ErrorMessage" runat="server" value="" />
    <form id="form1" runat="server">
    <div class="MainRightHeader">
        View Appraisal Orders
    </div>
    <asp:HiddenField id="CurrentActionLink" runat="server" />
    <div id="TablePanel" runat="server">
        <asp:Repeater runat="server" ID="Orders" OnItemDataBound="Orders_OnItemDataBound">
            <HeaderTemplate>
                <table id="OrdersTable" class="FormTable">
                    <thead>
                        <tr class="GridHeader">
                            <th style="width: 50px;" runat="server" id="ViewColumn"></th>
                            <th style="width: 140px;" runat="server" id="FileNumColumn">File #</th>
                            <th style="width: 90px;" runat="server" id="OrderedDColumn">Ordered Date</th>
                            <th style="width: 260px;" runat="server" id="StatusColumn">Status</th>
                            <th style="width: 90px;" runat="server" id="StatusDColumn">Status Date</th>
                            <th style="width: 300px;" runat="server" id="UploadedDocsColumn">Uploaded Documents</th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="GridItem" runat="server" id="Row" style="height: 50px;">
                    <td width="100px">
                        <a href="#" class="ActionLink" id="ActionLink" runat="server">actions <ml:EncodedLabel runat="server" class="order-ctl01-message" id="NewMessage" Text="2" onclick='return false;'></ml:EncodedLabel></a>
                        <div class="hidden wrap-order-ctl01-message">
                            <a runat="server" href="#" class="MenuLinks" id="ViewLink">view order</a>
                            <a runat="server" href="#" class="MenuLinks" id="EditLink">edit order</a>
                            <a runat="server" href="#" class="MenuLinks" id="AttachDocsLink">attach docs</a>
                            <a runat="server" href="#" class="MenuLinks" id="SendEmailLink">send email</a>
                            <a runat="server" href="#" class="MenuLinks" id="MessageLink"><ml:EncodedLabel runat="server" class="order-ctl01-message order-ctl01-message-sub" id="NewMessageSub" Text="2"></ml:EncodedLabel>message</a>
                        </div>
                    </td>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="FileNumber"></ml:EncodedLiteral><br />
                        <ml:EncodedLiteral runat="server" ID="VendorName"></ml:EncodedLiteral>
                    </td>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="OrderedD"></ml:EncodedLiteral>
                    </td>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="Status"></ml:EncodedLiteral>
                    </td>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="StatusD"></ml:EncodedLiteral>
                    </td>
                    <td>
                        <div style="height: 50px; overflow-y: scroll; overflow-x: hidden;">
                        <asp:Repeater ID="UploadedDocuments" runat="server" OnItemDataBound="UploadedDocuments_OnItemDataBound">
                            <ItemTemplate>
                                <a runat="server" href="#" id="UploadedDocLink"></a><br />
                            </ItemTemplate>
                        </asp:Repeater>
                        </div>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div id="ErrorMessageDiv" runat="server">
        <span class="FieldLabel" id="ErrorSummary" style="color:Red;" runat="server"></span>
    </div>
    <div>
        <input type="button" id="addOrderBtn" value="Add Order" />
        <asp:HiddenField ID="Disable" runat="server" />
    </div>
    <uc1:cModalDlg ID="CModalDlg1" runat="server" DESIGNTIMEDRAGDROP="5482"></uc1:cModalDlg>
    </form>

</body>
<script type="text/javascript">
$(function() {
    if ($('#Disable').val() == 'True') {
        $('#addOrderBtn').prop('disabled', true);
    }

    if ($.trim($('#ErrorMessage').val()) != '') {
        alert($('#ErrorMessage').val());
        jQuery('#ErrorSummary').text($('#ErrorMessage').val());
    }

    $('#addOrderBtn').click(function() {
        var url =<%= AspxTools.JsString( VirtualRoot ) %> +'/newlos/services/OrderAppraisal.aspx' + '?loanid='+<%= AspxTools.JsString( LoanID ) %>+'&appid=' + <%= AspxTools.JsString( ApplicationID ) %>;
        document.location.href = url;
    });

    //Handle Message link
    $("span[id$='NewMessage']").each(function(){
        $(this).toggle($(this).text()!="0");
    });
    $("span[id$='NewMessageSub']").each(function(){
        $(this).toggle($(this).text()!="0");
    });
    var actionLink=$("input[id$='CurrentActionLink']").val();
    if(actionLink!=null && actionLink!=""){
        $("#"+actionLink).click();
    }
});
    $('.ActionLink').click(function() {
        $(this).next().toggle();

        $("input[id$='CurrentActionLink']").val("");
        var messageNotifStyle = $("a[id$='MessageLink'] span",$(this).next()).attr("style");
        var isShowMessageNotification = (messageNotifStyle == null) || (messageNotifStyle.toString().indexOf("display: none;")==-1);
        if(isShowMessageNotification){
            if($("span",this).text()!="0")
                $("span",this).toggle();
        }
        else{
            $("span",this).toggle(false);
        }
    });

    $("a[id$='MessageLink']").click(function(){
        // $("span",this).toggle(false);
        $("input[id$='CurrentActionLink']").val($(this).parent().prev().attr("id"));
        return true;
    });

function launch(target, source)
{
    var forceEdge = false;
    var windowOptions = "dialogHeight:375px; dialogWidth:400px; center:yes; scroll:no; status=no; help=no;";
    if (source === 'AttachDocs') {
        windowOptions = "dialogHeight:310px; dialogWidth:400px; center:yes; resizable:yes; scroll:no; status=no; help=no;";
    } else if (source === 'SendEmail') {
        windowOptions = "dialogHeight:600px; dialogWidth:700px; center:yes; resizable:yes; scroll:no; status=no; help=no;";
    } else if (source === 'Message') {
        windowOptions = "dialogHeight:680px; dialogWidth:732px; center:yes; resizable:no; scroll:no; status=no; help=no;";
        // Bi-Directional Communication must run in edge
        forceEdge = true;
    }
    showModal(target, null, windowOptions, forceEdge, function(result){
        if (source === 'Message') {
            // Handle unread message notification
            __doPostBack();
        }
    });

}

function openXmlDoc(docid) {
    window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?cmd=xml&docid=' + docid, 'xmldoc');
}
function openDoc(docid) {
    window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + docid, 'pdfdoc');
}
</script>
</html>
