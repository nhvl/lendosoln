﻿using System;
using System.Text;
using ComplianceEase;
using DataAccess;
using LendersOffice.AntiXss;
using LendersOffice.Constants;
using LendersOffice.Conversions.ComplianceEase;
using LendersOffice.Security;
using LendersOffice.ObjLib.Conversions.ComplianceEase;

namespace LendersOfficeApp.newlos.Services
{
    public partial class ComplianceEaseService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new ComplianceEaseServiceItem());
        }

        public class ComplianceEaseServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
        {
            protected override void Process(string methodName)
            {
                switch (methodName)
                {
                    case "Export":
                        Export();
                        break;
                }
            }

            protected override CPageData ConstructPageDataClass(Guid sLId)
            {
                return CPageData.CreateUsingSmartDependency(sLId, typeof(ComplianceEaseServiceItem));
            }

            protected override void BindData(CPageData dataLoan, CAppData dataApp)
            {
                dataLoan.sComplianceEaseId = this.GetString("sComplianceEaseId");
            }

            protected override void LoadData(CPageData dataLoan, CAppData dataApp)
            {
            }

            private void Export()
            {
                this.SaveData();
                Guid sLId = this.sLId;
                Guid aAppId = this.aAppId;

                string userName = this.GetString("m_userName");
                string password = this.GetString("m_password");
                bool rememberUserName = this.GetBool("m_rememberMe");

                E_TransmittalDataComplianceAuditType auditType = (E_TransmittalDataComplianceAuditType)this.GetInt("m_auditType");

                ComplianceEaseAuthentication savedAuthentication = ComplianceEaseSavedAuthentication.Retrieve(BrokerUserPrincipal.CurrentPrincipal);
                if (password == ConstAppDavid.FakePasswordDisplay)
                {
                    if (savedAuthentication.Password != "")
                    {
                        password = savedAuthentication.Password;
                    }
                }

                if (rememberUserName)
                {
                    savedAuthentication.UserName = userName;
                    savedAuthentication.Password = password;
                }
                else
                {
                    savedAuthentication.UserName = "";
                    savedAuthentication.Password = "";
                }

                savedAuthentication.Save();

                ServiceFault serviceFault = null;
                bool hasError = ComplianceEaseExporter.Export(sLId, userName, password, auditType, out serviceFault, false, -1);

                CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(ComplianceEaseServiceItem));
                dataLoan.InitLoad();

                string errorMessage = string.Empty;
                if (hasError && serviceFault != null)
                {
                    errorMessage = BuildErrorMessage(serviceFault, dataLoan.sClosingCostFeeVersionT, dataLoan.sDisclosureRegulationT, sLId, aAppId);
                }

                this.SetResult("HasError", hasError);
                this.SetResult("ErrorMessageHtml", errorMessage);
                this.SetResult("sComplianceEaseId", dataLoan.sComplianceEaseId);
                this.SetResult("sComplianceEaseStatus", dataLoan.sComplianceEaseStatus);
            }

            private static string BuildErrorMessage(ServiceFault serviceFault, E_sClosingCostFeeVersionT closingCostFeeVersionT, E_sDisclosureRegulationT disclosureRegulationT, Guid sLId, Guid aAppId)
            {
                StringBuilder sb = new StringBuilder();

                sb.Append("<div>");
                sb.Append(AspxTools.HtmlString(serviceFault.Description));
                sb.Append("<ul>");
                foreach (var o in serviceFault.FaultCause.ErrorList)
                {
                    FriendlyError friendlyError = ComplianceEaseExporter.GetFriendlyErrorMessage(o);

                    sb.Append("<li>");
                    sb.Append(friendlyError.Message);

                    string page = "";

                    if (closingCostFeeVersionT.Equals(E_sClosingCostFeeVersionT.Legacy) || (string.IsNullOrEmpty(friendlyError.PageGfe2010) && string.IsNullOrEmpty(friendlyError.PageTrid2015)))
                    {
                        page = friendlyError.Page;
                    }
                else if (!disclosureRegulationT.Equals(E_sDisclosureRegulationT.TRID))
                    {
                        page = friendlyError.PageGfe2010;
                    }
                    else
                    {
                        page = friendlyError.PageTrid2015;
                    }

                    // OPM 133090 - Add Error link
                    if (!string.IsNullOrEmpty(page))
                    {
                        string delim = page.IndexOf("?") == -1 ? "?" : "&";
                        string url = "../" + page + delim + "highlightId=" + friendlyError.Field + "&loanid=" + sLId + "&appid=" + aAppId;

                        sb.Append(" <a href='" + url + "'>edit</a>");
                    }

                    sb.Append("</li>");
                }
                sb.Append("</ul>");
                sb.Append("</div>");
                return sb.ToString();
            }
        }
    }
}
