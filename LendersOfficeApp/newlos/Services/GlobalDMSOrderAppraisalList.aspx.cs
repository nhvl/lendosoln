﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CommonProjectLib.Common.Lib;
using DataAccess;
using EDocs;
using LendersOffice.Common;
using LendersOffice.ConfigSystem.Operations;
using LendersOffice.GDMS.LookupMethods;
using LendersOffice.Integration.Appraisals;
using LendersOffice.Integration.VendorCommunication.Appraisal;
using LendersOffice.ObjLib.Appraisal;
using LendersOffice.ObjLib.Conversions.GlobalDMS;
using LendersOffice.ObjLib.Conversions.LQBAppraisal;
using LendersOffice.Security;
using MeridianLink.CommonControls;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos.Services
{
    public partial class GlobalDMSOrderAppraisalList : BaseLoanPage
    {
        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[] { WorkflowOperations.OrderAppraisal };
        }
        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return new WorkflowOperation[] { WorkflowOperations.OrderAppraisal };
        }
        protected string RequestedDocKey
        {
            get { return RequestHelper.GetSafeQueryString("docKey"); }
        }
        protected bool IsDocRequest
        {
            get { return !string.IsNullOrEmpty(this.RequestedDocKey); }
        }

        protected override void LoadData()
        {
            if (false == UserHasWorkflowPrivilege(WorkflowOperations.OrderAppraisal))
            {
                ErrorMessage.Value = GetReasonUserCannotPerformPrivilege(WorkflowOperations.OrderAppraisal);
                Disable.Value = true.ToString();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsDocRequest && WriteDocument(RequestedDocKey, LoanID))
            {
                return;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(GlobalDMSOrderAppraisalList));
            dataLoan.InitLoad();


            // Load orders for vendors using GlobalDMS/Etrac - hits their webservices to load newest data
            List<string> errorMessages = new List<string>();
            var gdmsLoader = new GlobalDMSOrderLoader(LoanID, BrokerUser, loadDocAttachmentTypes: false, shouldQueueNewFiles: true);
            var orderContainers = gdmsLoader.Retrieve();
            ErrorMessage.Value += string.Join(Environment.NewLine, errorMessages);

            // Load orders for vendors using the Appraisal Framework - data from DB
            var frameworkOrders = LQBAppraisalOrder.GetOrdersForLoan(LoanID);
            orderContainers.AddRange(frameworkOrders.Select(o => 
                new OrderContainer() { VendorID = o.VendorId, LQBAppraisalOrder = o, VendorName = AppraisalVendorConfig.Retrieve(o.VendorId).VendorName }));

            if (orderContainers.Count > 0)
            {
                Orders.DataSource = orderContainers;
                Orders.DataBind();

                Guid? vendorIdForDocs = GetLoanAppraisalVendorId(orderContainers);
                if (vendorIdForDocs.HasValue && vendorIdForDocs.Value != dataLoan.sAppraisalVendorId)
                {
                    // Because GlobalDMS won't consistently push to us, we have to check this and do a background save as a part of page load.
                    // This whole integration is a great target once we have the ability to easily throw operations to a background queue.
                    CPageData loanToSaveVendorId = new CFullAccessPageData(this.LoanID, new string[] { nameof(CPageData.sAppraisalVendorId) });
                    loanToSaveVendorId.InitSave(sFileVersion);
                    loanToSaveVendorId.sAppraisalVendorId = vendorIdForDocs.Value;
                    loanToSaveVendorId.Save();
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);
        }

        private bool WriteDocument(string RequestedDocKey, Guid LoanID)
        {
            //Although we could just grab the doc from the filedb key, 
            //we need to make sure the requested filedb key actually belongs to a document on this loan.
            var orders = LQBAppraisalOrder.GetOrdersForLoan(LoanID);

            foreach (var order in orders)
            {
                var requested = order.UploadedFiles.FirstOrDefault(
                    a => a.FileDBKey.Equals(RequestedDocKey, StringComparison.OrdinalIgnoreCase));
                if (requested != default(LQBAppraisalOrder.AppraisalDocument))
                {
                    WriteDoc(requested);
                    return true;
                }
            }

            return false;
        }

        private void WriteDoc(LQBAppraisalOrder.AppraisalDocument doc)
        {
            doc.UseFile((fileInfo) => {
                Response.Clear();
                Response.ClearHeaders();
                //Need to remove any compression filtering, otherwise it comes out garbled.
                Response.Filter = null;
                Response.ContentType = "application/pdf";
                Response.AppendHeader("Content-Disposition", $"attachment; filename=\"{doc.DocumentName}\"");
                Response.WriteFile(fileInfo.FullName);
                Response.Flush();
                Response.Close();
                Response.End();
            });
        }

        protected void Orders_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            HtmlAnchor viewLink = args.Item.FindControl("ViewLink") as HtmlAnchor;
            HtmlAnchor editLink = args.Item.FindControl("EditLink") as HtmlAnchor;
            HtmlAnchor attachDocsLink = args.Item.FindControl("AttachDocsLink") as HtmlAnchor;
            HtmlAnchor sendEmailLink = args.Item.FindControl("SendEmailLink") as HtmlAnchor;
            HtmlAnchor messageLink = args.Item.FindControl("MessageLink") as HtmlAnchor;
            Label newMessage = args.Item.FindControl("NewMessage") as Label;
            Label newMessageSub = args.Item.FindControl("NewMessageSub") as Label;
            EncodedLiteral fileNumber = (EncodedLiteral)args.Item.FindControl("FileNumber");
            EncodedLiteral vendorName = (EncodedLiteral)args.Item.FindControl("VendorName");
            EncodedLiteral orderedD = (EncodedLiteral)args.Item.FindControl("OrderedD");
            EncodedLiteral status = (EncodedLiteral)args.Item.FindControl("Status");
            EncodedLiteral statusD = (EncodedLiteral)args.Item.FindControl("StatusD");
            Repeater docsRepeater = args.Item.FindControl("UploadedDocuments") as Repeater;
            HtmlTableRow row = args.Item.FindControl("Row") as HtmlTableRow;

            OrderContainer orderInfo = args.Item.DataItem as OrderContainer;
            if (orderInfo == null) return;

            row.Attributes.Add("class", args.Item.ItemType == ListItemType.Item ? "GridItem" : "GridAlternatingItem");
            editLink.Visible = false;
            attachDocsLink.Visible = false;
            sendEmailLink.Visible = false;
            messageLink.Visible = false;
            newMessage.Visible = false;
            newMessageSub.Visible = false;

            var allowBiDirectionalCommunication = AppraisalVendorConfig.Retrieve(orderInfo.VendorID).EnableBiDirectionalCommunication;

            if (newMessage.Text != "0" && allowBiDirectionalCommunication)
            {
                newMessage.Visible = newMessageSub.Visible = true;
            }

            if (orderInfo.ClientOrder != null) //GDMS Appraisal
            {
                if (orderInfo.ClientOrder.StatusName == "Error Accessing Order")
                {
                    viewLink.Visible = false;
                    HtmlAnchor actionLink = args.Item.FindControl("ActionLink") as HtmlAnchor;
                    if (actionLink != null)
                        actionLink.Disabled = true;
                }
                else
                {
                    var pdf = new LendersOffice.Pdf.CAppraisalOrderPDF();
                    string link = string.Format(
                        "{0}/pdf/{1}.aspx?loanid={2}&filenumber={3}&vendorID={4}",
                        VirtualRoot,
                        Uri.EscapeDataString(pdf.Name),
                        Uri.EscapeDataString(LoanID.ToString()),
                        Uri.EscapeDataString(orderInfo.ClientOrder.FileNumber.ToString()),
                        Uri.EscapeDataString(orderInfo.VendorID.ToString()));
                    viewLink.Attributes.Add("onclick", $"LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload('{link}');");

                    string url = string.Format(
                        "'/newlos/Services/OrderAppraisalAttachDocs.aspx?loanid={0}&vendorID={1}&orderNumber={2}'",
                        Uri.EscapeDataString(LoanID.ToString()),
                        Uri.EscapeDataString(orderInfo.VendorID.ToString()),
                        Uri.EscapeDataString(orderInfo.ClientOrder.FileNumber.ToString()));
                    link = string.Format(
                        "javascript:launch({0},'AttachDocs');",
                        url);
                    attachDocsLink.HRef = link;
                    attachDocsLink.Visible = true;

                    url = string.Format(
                        "'/newlos/Services/OrderAppraisalEmailLog.aspx?loanid={0}&vendorID={1}&orderNumber={2}'",
                        Uri.EscapeDataString(LoanID.ToString()),
                        Uri.EscapeDataString(orderInfo.VendorID.ToString()),
                        Uri.EscapeDataString(orderInfo.ClientOrder.FileNumber.ToString()));
                    link = string.Format(
                        "javascript:launch({0},'SendEmail');",
                        url);
                    sendEmailLink.HRef = link;
                    sendEmailLink.Visible = true;

                    if (allowBiDirectionalCommunication)
                    {
                        url = string.Format(
                            "'/newlos/Services/OrderAppraisalCommunication.aspx?loanid={0}&vendorID={1}&orderNumber={2}'",
                            Uri.EscapeDataString(LoanID.ToString()),
                            Uri.EscapeDataString(orderInfo.VendorID.ToString()),
                            Uri.EscapeDataString(orderInfo.ClientOrder.FileNumber.ToString()));
                        link = string.Format(
                            "javascript:launch({0},'Message');",
                            url);
                        messageLink.HRef = link;
                        messageLink.Visible = true;
                        newMessage.Text = newMessageSub.Text = GetUnReadMessages(LoanID, orderInfo.VendorID, orderInfo.ClientOrder.FileNumber.ToString()).ToString();
                    }
                }

                fileNumber.Text = orderInfo.ClientOrder.FileNumber.ToString();
                vendorName.Text = orderInfo.VendorName;
                orderedD.Text = orderInfo.ClientOrder.CreationDate_String.Split(' ')[0]; // Get rid of the time
                status.Text = orderInfo.ClientOrder.StatusName;
                statusD.Text = orderInfo.ClientOrder.AppraiserLastModifiedDate_String.Split(' ')[0];

                docsRepeater.DataSource = orderInfo.UploadedFiles;
                docsRepeater.DataBind();
            }
            else if (orderInfo.LQBAppraisalOrder != null) //Framework Appraisal
            {
                var order = orderInfo.LQBAppraisalOrder;
                fileNumber.Text = order.OrderNumber;
                vendorName.Text = orderInfo.VendorName;
                orderedD.Text = order.OrderedDate_rep;
                status.Text = string.IsNullOrEmpty(order.Status)?"Unassigned":order.Status;
                statusD.Text = order.StatusDate_rep;

                string link;
                if (string.IsNullOrEmpty(order.Status)) //only allow edit if no status updates yet
                {
                    link = string.Format("{0}/newlos/Services/OrderAppraisal.aspx?loanid={1}&orderNumber={2}",
                        VirtualRoot,
                        Uri.EscapeDataString(LoanID.ToString()),
                        HttpUtility.UrlEncode(order.OrderNumber));
                    editLink.HRef = link;
                    editLink.Visible = true;
                }

                var pdf = new LendersOffice.Pdf.CLQBAppraisalOrderPDF();
                link = string.Format(
                    "{0}/pdf/{1}.aspx?loanid={2}&ordernumber={3}",
                    VirtualRoot,
                    Uri.EscapeDataString(pdf.Name),
                    Uri.EscapeDataString(LoanID.ToString()),
                    HttpUtility.UrlEncode(order.OrderNumber));
                viewLink.Attributes.Add("onclick", $"LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload('{link}');");

                string url = string.Format(
                    "'/newlos/Services/OrderAppraisalAttachDocs.aspx?loanid={0}&vendorID={1}&orderNumber={2}'",
                    Uri.EscapeDataString(LoanID.ToString()),
                    Uri.EscapeDataString(order.VendorId.ToString()),
                    HttpUtility.UrlEncode(order.OrderNumber));
                link = string.Format(
                    "javascript:launch({0},'AttachDocs');",
                    url);
                attachDocsLink.HRef = link;
                attachDocsLink.Visible = true;

                // 10/18/13 TimothyJ:
                //add EDocs that have been sent to the appraiser to the uploaded documents field.  This will match the way it is with GDMS
                var frameworkDocs = new List<FrameworkDocumentItem>();
                foreach (var appraisalDoc in order.UploadedFiles)
                {
                    frameworkDocs.Add(new FrameworkDocumentItem(appraisalDoc));
                }
                if (order.EmbeddedDocumentIds != null)
                {
                    foreach (Guid docId in order.EmbeddedDocumentIds)
                    {
                        frameworkDocs.Add(new FrameworkDocumentItem(docId));
                    }
                }

                docsRepeater.DataSource = frameworkDocs;
                docsRepeater.DataBind();

                if (allowBiDirectionalCommunication)
                {
                    url = string.Format(
                        "'/newlos/Services/OrderAppraisalCommunication.aspx?loanid={0}&vendorID={1}&orderNumber={2}'",
                        Uri.EscapeDataString(LoanID.ToString()),
                        Uri.EscapeDataString(orderInfo.VendorID.ToString()),
                        HttpUtility.UrlEncode(order.OrderNumber));
                    link = string.Format(
                        "javascript:launch({0},'Message');",
                        url);
                    messageLink.HRef = link;
                    messageLink.Visible = true;

                    newMessage.Text = newMessageSub.Text = GetUnReadMessages(LoanID, orderInfo.VendorID, order.OrderNumber).ToString();
                }
            }
        }

        protected void UploadedDocuments_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            HtmlAnchor viewLink = args.Item.FindControl("UploadedDocLink") as HtmlAnchor;

            OrderFileObject dataItem = args.Item.DataItem as OrderFileObject;

            if (dataItem == null || !dataItem.viewableByClient)
            {
                FrameworkDocumentItem generalDoc = args.Item.DataItem as FrameworkDocumentItem;
                if (generalDoc.AppraisalDoc != null)
                {
                    viewLink.InnerText = generalDoc.AppraisalDoc.DocumentName;
                    viewLink.HRef = "GlobalDMSOrderAppraisalList.aspx?loanid=" + AspxTools.HtmlString(LoanID) + "&docKey=" + AspxTools.HtmlString(generalDoc.AppraisalDoc.FileDBKey);
                }
                else if (generalDoc.EDocID != Guid.Empty)
                {
                    EDocumentRepository repo = EDocumentRepository.GetUserRepository();
                    EDocument edoc;
                    try
                    {
                        edoc = repo.GetDocumentById(generalDoc.EDocID);
                        viewLink.InnerText = edoc.FolderAndDocTypeName + ".pdf";
                        viewLink.HRef = string.Format("javascript:openDoc('{0}');", AspxTools.JsStringUnquoted(generalDoc.EDocID.ToString()));
                    }
                    catch (NotFoundException)
                    {
                        edoc = repo.GetDeletedDocumentById(generalDoc.EDocID);
                        viewLink.InnerText = "[Deleted] " + edoc.FolderAndDocTypeName + ".pdf";
                        viewLink.Title = "To restore, go to EDocs\\Document List and click \"Restore deleted docs...\"";
                    }
                    catch (AccessDenied)
                    {
                        viewLink.Visible = false; // OPM 457416 - Hide Docs the user does not have permission to view.
                    }
                }
            }
            else
            {
                viewLink.HRef = AspxTools.HtmlString(dataItem.url);
                viewLink.InnerText = dataItem.fileName;
                viewLink.Target = "_blank";
            }
        }

        private int GetUnReadMessages(Guid LoanId, Guid VendorId, string OrderNumber)
        {
            var messageHandler = new AppraisalMessageHandler(LoanId);
            var unreadMessages = messageHandler.GetUnreadMessages();
            if (unreadMessages.Count() == 0) return 0;
            return messageHandler.GetAllMessages().Where(m => unreadMessages.Contains(m.MessageId) && m.VendorId == VendorId && m.OrderNumber == OrderNumber).Count();
        }

        private class FrameworkDocumentItem
        {
            private LQBAppraisalOrder.AppraisalDocument appraisalDoc;
            private Guid edocID;

            public LQBAppraisalOrder.AppraisalDocument AppraisalDoc
            {
                get { return appraisalDoc; }
                set
                {
                    appraisalDoc = value;
                    if (appraisalDoc != null)
                        edocID = Guid.Empty;
                }
            }
            public Guid EDocID
            {
                get { return edocID; }
                set
                {
                    edocID = value;
                    if (edocID != Guid.Empty)
                        appraisalDoc = null;
                }
            }

            public FrameworkDocumentItem(LQBAppraisalOrder.AppraisalDocument appraisalDocument)
            {
                appraisalDoc = appraisalDocument;
            }
            public FrameworkDocumentItem(Guid eDocIdAttachedToOrder)
            {
                edocID = eDocIdAttachedToOrder;
            }
        }

        private static Guid? GetLoanAppraisalVendorId(List<OrderContainer> orderContainers)
        {
            if (orderContainers.Select(o => o.VendorID).Distinct().Count() == 1)
            {
                return orderContainers.First().VendorID;
            }

            //deal with orderContainers empty case externally
            //This leaves us with the case of at least 2 orders for at least 2 vendors
            List<Tuple<Guid, DateTime>> ordersData = CollectVendorIdDatePairs(orderContainers);
            DateTime mostRecentStatus = ordersData.First().Item2;
            foreach (var order in ordersData)
            {
                if (order.Item2 > mostRecentStatus)
                {
                    mostRecentStatus = order.Item2;
                }
            }
            var results = ordersData.FindAll(order => order.Item2 == mostRecentStatus);
            if (results.Select(o => o.Item1).Distinct().Count() == 1)
            {
                return results.First().Item1;
            }

            return null;
        }

        private static List<Tuple<Guid, DateTime>> CollectVendorIdDatePairs(List<OrderContainer> orderContainers)
        {
            var ret = new List<Tuple<Guid, DateTime>>();
            foreach (var order in orderContainers)
            {
                Guid vendorId = order.VendorID;
                if (order.ClientOrder != null && !string.IsNullOrEmpty(order.ClientOrder.AppraiserLastModifiedDate_String))
                {
                    ret.Add(new Tuple<Guid, DateTime>(vendorId, DateTime.Parse(order.ClientOrder.AppraiserLastModifiedDate_String))); // GlobalDMS
                }
                else if (order.LQBAppraisalOrder != null && order.LQBAppraisalOrder.StatusDate.HasValue)
                {
                    ret.Add(new Tuple<Guid, DateTime>(vendorId, order.LQBAppraisalOrder.StatusDate.Value)); // Appraisal Framework
                }
                else
                {
                    ret.Add(new Tuple<Guid, DateTime>(vendorId, new DateTime(1900, 1, 1))); // No current date
                }
            }

            return ret;
        }
    }
}
