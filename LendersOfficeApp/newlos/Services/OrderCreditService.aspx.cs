using System;
using System.Data.SqlClient;
using CommonLib;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Audit;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.CreditReport;
using LendersOffice.CreditReport.Mcl;
using LendersOffice.CreditReport.Mismo;
using LendersOffice.Security;
using LendersOffice.ObjLib.ServiceCredential;
using System.Linq;
using System.Collections.Generic;

namespace LendersOfficeApp.newlos.Services
{
	public partial class OrderCreditService : BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem(string.Empty, new OrderCreditServiceItem());
        }

        protected override bool PerformResultOptimization
        {
            get
            {
                return false;
            }
        }

        private class OrderCreditServiceItem : AbstractBackgroundServiceItem
        {
            private CAppData appData;
            private int m_version = -1;
            private BrokerUserPrincipal BrokerUser 
            {
                get { return BrokerUserPrincipal.CurrentPrincipal; }
            }

            protected override CPageData ConstructPageDataClass(Guid sLId)
            {
                return CPageData.CreateUsingSmartDependency(sLId, typeof(OrderCreditServiceItem));
            }

            protected override void Process(string methodName)
            {
                switch (methodName)
                {
                    case "OrderCredit":
                        OrderCredit();
                        break;
                    case "ImportLiabilities":
                        ImportLiabilities();
                        break;
                    case "CheckCRAWarningMessage":
                        CheckCRAWarningMessage();
                        break;
                    case "GetOrderCreditPermission":
                        GetOrderCreditPermission();
                        break;
                    case "GetOrderSupplementUrl":
                        GetOrderSupplementUrl();
                        break;
                    case "PollForCreditReportRequestResults":
                        PollForCreditReportRequestResults();
                        break;
                    case "OrderCreditSynchronously":
                        OrderCreditSynchronously();
                        break;
                    default:
                        throw CBaseException.GenericException($"Unhandled methodName [{methodName}]");
                }

                if (m_version != -1)
                {
                    SetResult("sFileVersion", m_version);
                }
            }

            protected override void BindData(CPageData dataLoan, CAppData dataApp)
            {
                dataApp.aBFirstNm = GetString("aBFirstNm");
                dataApp.aBMidNm = GetString("aBMidNm");
                dataApp.aBLastNm = GetString("aBLastNm");
                dataApp.aBSuffix = GetString("aBSuffix");
                dataApp.aBSsn = GetString("aBSsn");
                dataApp.aBDob_rep = GetString("aBDob");
                dataApp.aBMaritalStatT = (E_aBMaritalStatT)GetInt("aBMaritalStatT");

                dataApp.aBAddr = GetString("aBAddr");
                dataApp.aBCity = GetString("aBCity");
                dataApp.aBState = GetString("aBState");
                dataApp.aBZip = GetString("aBZip");
                dataApp.aBAddrYrs = GetString("aBAddrYrs");

                dataApp.aBPrev1Addr = GetString("aBPrev1Addr");
                dataApp.aBPrev1City = GetString("aBPrev1City");
                dataApp.aBPrev1State = GetString("aBPrev1State");
                dataApp.aBPrev1Zip = GetString("aBPrev1Zip");
                dataApp.aBPrev1AddrYrs = GetString("aBPrev1AddrYrs");

                dataApp.aCFirstNm = GetString("aCFirstNm");
                dataApp.aCLastNm = GetString("aCLastNm");
                dataApp.aCMidNm = GetString("aCMidNm");
                dataApp.aCSuffix = GetString("aCSuffix");
                dataApp.aCDob_rep = GetString("aCDob");
                dataApp.aCSsn = GetString("aCSsn");

                Services.OrderCredit.BindDataFromControls(dataLoan, dataApp, this);

                bool isLqiReport = this.GetBool("OrderLqiReport", false);
                if (this.CurrentMethodName == nameof(OrderCredit))
                {
                    if (isLqiReport)
                    {
                        dataApp.aLqiCrOd = CDateTime.Create(DateTime.Now);
                    }
                    else
                    {
                        dataApp.aCrOd = CDateTime.Create(DateTime.Now);
                    }

                    if (StringComparer.OrdinalIgnoreCase.Equals(this.GetString("ActionType"), "REMOVE_BORROWER"))
                    {
                        dataApp.SwapMarriedBorAndCobor();
                    }
                }
            }

            protected override void LoadData(CPageData dataLoan, CAppData dataApp)
            {
                this.appData = dataApp; // Used in OrderCredit()
                Services.OrderCredit.LoadDataForControls(dataLoan, dataApp, this);

                // Now that borrower data might get swapped during save, it's necessary to load this data
                this.SetResult("aBFirstNm", dataApp.aBFirstNm);
                this.SetResult("aBMidNm", dataApp.aBMidNm);
                this.SetResult("aBLastNm", dataApp.aBLastNm);
                this.SetResult("aBSuffix", dataApp.aBSuffix);
                this.SetResult("aBSsn", dataApp.aBSsn);
                this.SetResult("aBDob", dataApp.aBDob_rep);
                this.SetResult("aBMaritalStatT", dataApp.aBMaritalStatT);

                this.SetResult("aBAddr", dataApp.aBAddr);
                this.SetResult("aBCity", dataApp.aBCity);
                this.SetResult("aBState", dataApp.aBState);
                this.SetResult("aBZip", dataApp.aBZip);
                this.SetResult("aBAddrYrs", dataApp.aBAddrYrs);

                this.SetResult("aBPrev1Addr", dataApp.aBPrev1Addr);
                this.SetResult("aBPrev1City", dataApp.aBPrev1City);
                this.SetResult("aBPrev1State", dataApp.aBPrev1State);
                this.SetResult("aBPrev1Zip", dataApp.aBPrev1Zip);
                this.SetResult("aBPrev1AddrYrs", dataApp.aBPrev1AddrYrs);

                this.SetResult("aCFirstNm", dataApp.aCFirstNm);
                this.SetResult("aCLastNm", dataApp.aCLastNm);
                this.SetResult("aCMidNm", dataApp.aCMidNm);
                this.SetResult("aCSuffix", dataApp.aCSuffix);
                this.SetResult("aCDob", dataApp.aCDob_rep);
                this.SetResult("aCSsn", dataApp.aCSsn);
            }

            private CreditRequestData CreateRequestData(Guid loanId, CAppData dataApp)
            {
                Guid comId = GetGuid("comId", Guid.Empty);

                if (comId == Guid.Empty) 
                {
                    throw new CBaseException("Require a valid credit report agency.", "comId is Guid.Empty");
                }

                CreditRequestData data = CreditRequestData.Create(loanId, dataApp.aAppId);
                data.IncludeEquifax = GetBool("IsEquifax");
                data.IncludeExperian = GetBool("IsExperian");
                data.IncludeTransUnion = GetBool("IsTransunion");
                data.ReportID = GetString("ReportId");
                data.InstantViewID = GetString("InstantViewId");
                data.ReferenceNumber = GetString("loanname", ""); //OPM 15995
                data.UseCreditCardPayment = GetBool("UseCreditCardPayment");
                data.BillingFirstName = GetString("BillingFirstName");
                data.BillingLastName = GetString("BillingLastName");
                data.BillingStreetAddress = GetString("BillingStreetAddress");
                data.BillingCity = GetString("BillingCity");
                data.BillingState = GetString("BillingState");
                data.BillingZipcode = GetString("BillingZipcode");
                data.BillingCardNumber = GetString("BillingCardNumber");
                data.BillingExpirationMonth = GetInt("BillingExpirationMonth", 0);
                data.BillingExpirationYear = GetInt("BillingExpirationYear", 0);
                data.BillingCVV = GetString("BillingCvv");
                data.RequestingPartyName = GetString("RequestingPartyName");
                data.RequestingPartyStreetAddress = GetString("RequestingPartyStreetAddress");
                data.RequestingPartyCity = GetString("RequestingPartyCity");
                data.RequestingPartyState = GetString("RequestingPartyState");
                data.RequestingPartyZipcode = GetString("RequestingPartyZipcode");
                data.IsMortgageOnly = GetBool("IsMortgageOnly");
                data.MclCreditReportType = (MclCreditReportType)GetInt("MclReportType", 0);
                data.CreditReportType = this.GetInt("CreditReportType", 0);
                data.IsRequestLqiReport = GetBool("OrderLqiReport");
                data.IsRequestReissueLqiReport = GetBool("ReissueLqiReport");

                switch (GetString("ActionType").ToUpper())
                {
                    case "UPGRADE":
                        data.MclRequestType = LendersOffice.CreditReport.Mcl.MclRequestType.Upgrade;
                        //av not sure why this is needed but it is in the previous version. 
                        data.RequestActionType = E_CreditReportRequestActionType.Upgrade;
                        break;
                    case "NEW":
                        data.MclRequestType = LendersOffice.CreditReport.Mcl.MclRequestType.New;
                        data.RequestActionType = E_CreditReportRequestActionType.Submit;
                        break;
                    case "GET":
                        data.MclRequestType = LendersOffice.CreditReport.Mcl.MclRequestType.Get;
                        data.RequestActionType = E_CreditReportRequestActionType.Reissue;
                        break;
                    case "RETRIEVE":
                        data.MclRequestType = LendersOffice.CreditReport.Mcl.MclRequestType.Get;
                        data.RequestActionType = E_CreditReportRequestActionType.Retrieve;
                        break;
                    case "NEW_LQI":
                        data.MclRequestType = LendersOffice.CreditReport.Mcl.MclRequestType.Refresh;
                        data.RequestActionType = E_CreditReportRequestActionType.Other;
                        data.RequestActionTypeOtherDetail = CreditReportRequestActionTypeOtherDetail.LqiNew;
                        break;
                    case "REISSUE_LQI":
                        data.RequestActionType = E_CreditReportRequestActionType.Other;
                        data.RequestActionTypeOtherDetail = CreditReportRequestActionTypeOtherDetail.LqiReissue;
                        break;
                    case "REMOVE_BORROWER":
                        data.RequestActionType = E_CreditReportRequestActionType.Other;
                        data.RequestActionTypeOtherDetail = CreditReportRequestActionTypeOtherDetail.RemoveBorrower;
                        break;
                    case "REMOVE_COBORROWER":
                        data.RequestActionType = E_CreditReportRequestActionType.Other;
                        data.RequestActionTypeOtherDetail = CreditReportRequestActionTypeOtherDetail.RemoveCoBorrower;
                        break;
                    case "RETRIEVE_SUPPLEMENT":
                        data.RequestActionType = E_CreditReportRequestActionType.Other;
                        data.RequestActionTypeOtherDetail = CreditReportRequestActionTypeOtherDetail.RetrieveSupplement;
                        break;
                    case "RETRIEVE_BILLING_REPORT":
                        data.RequestActionType = E_CreditReportRequestActionType.Other;
                        data.RequestActionTypeOtherDetail = CreditReportRequestActionTypeOtherDetail.RetrieveBilling;
                        break;
                    default:
                        throw new CBaseException(ErrorMessages.Generic, "Unsupported ActionType=" + GetString("ActionType"));
                }

                if (data.WasLenderMapping) //clear out instant view incase it was lender mapping 
                {
                    data.InstantViewID = ""; 
                }
                //was in else statement where if is Is LenderMapping. along with bureaus, but now they will be set regardless and then lendermapping line below the following will set them from the proxy overwriting UI values. 

                // OPM 450477. If there is any stored credentials that match use them.
                var storedCredential = ServiceCredential.ListAvailableServiceCredentials(BrokerUser, GetGuid("sBranchId"), ServiceCredentialService.CreditReports)
                    .FirstOrDefault(credential => credential.ServiceProviderId.HasValue && credential.ServiceProviderId == comId);

                if (storedCredential != null)
                {
                    data.LoginInfo.UserName = storedCredential.UserName;
                    data.LoginInfo.Password = storedCredential.UserPassword.Value;
                    data.LoginInfo.AccountIdentifier = storedCredential.AccountId;
                }
                else
                {
                    data.LoginInfo.UserName = GetString("LoginName");
                    data.LoginInfo.Password = GetString("Password");
                    data.LoginInfo.AccountIdentifier = GetString("AccountIdentifier");
                }

                //The folllowing two lines were in the else of the if inside the fannie mae if statement, but this way we will just overwrite the values if they apply
                data.LoginInfo.FannieMaeMORNETUserID = GetString("DUUserID", "");
                data.LoginInfo.FannieMaeMORNETPassword = GetString("DUPassword", "");

                data.UpdateCRAInfoFrom(comId, BrokerUser.BrokerId, true);
                CreditReportUtilities.UpdateLastUsedCra(BrokerUser.BrokerId, BrokerUser.LastUsedCreditLoginNm, GetBool("RememberLogin") ? GetString("LoginName") : null,
                    BrokerUser.LastUsedCreditAccountId, GetBool("RememberLogin") ? GetString("AccountIdentifier") : null,
                    BrokerUser.LastUsedCreditProtocolId, comId,
                    BrokerUser.UserId);

            
                SetBorrowerInfo(data.Borrower, dataApp, data.CreditProtocol);
                SetBorrowerInfo(data.Coborrower, dataApp, data.CreditProtocol);

                if (data.CreditProtocol == CreditReportProtocol.FannieMae)
                {
                    data.LoginInfo.FannieMaeCreditProvider = data.Url; 

                    BrokerDB bdb = BrokerDB.RetrieveById(BrokerUser.BrokerId);
                    if (!string.IsNullOrEmpty(bdb.CreditMornetPlusUserID) &&
                        !string.IsNullOrEmpty(bdb.CreditMornetPlusPassword.Value))
                    {
                        data.LoginInfo.FannieMaeMORNETUserID = bdb.CreditMornetPlusUserID;
                        data.LoginInfo.FannieMaeMORNETPassword = bdb.CreditMornetPlusPassword.Value;
                    }
                }
            
                data.IsPdfViewNeeded = CreditReportUtilities.IsCraPdfEnabled(data.CreditProtocol, BrokerUser.BrokerId, data.ActualCraID); 

                if (GetString("StatusQuery") == "T")
                {
                    data.RequestActionType = E_CreditReportRequestActionType.StatusQuery;
                    // 8/11/2005 dd - Need to set ReportID just in case LenderMap Proxy wipe it out.
                    data.ReportID = GetString("ReportId");
                }


                // LandSafe is actually fannie mae type this guid belongs to landsafe literally. This fix also is needed for direct connection
                //it may not be on the list though but just in case in the future we support direct.            
                //if(  data.ActualCraID.ToString() == "07dc117a-f9e8-4ddf-8f65-9ed4ae9a7a60" || data.CreditProtocol == CreditReportProtocol.Landsafe)
                //Upper casing fails to authenticate with Landsafe Mismo 2.3.1 direct interface. So removing it.  OPM 51530. AM
                if(  data.ActualCraID.ToString() == "07dc117a-f9e8-4ddf-8f65-9ed4ae9a7a60")
                {
                    data.LoginInfo.UserName = data.LoginInfo.UserName.ToUpper();
                    data.LoginInfo.Password = data.LoginInfo.Password.ToUpper();
                    data.ReportID = data.ReportID.ToUpper();
                }
                return data;
            }
            private E_CreditReportAuditSourceT ComputeAuditType(CreditRequestData requestData)
            {
                E_CreditReportAuditSourceT requestType = E_CreditReportAuditSourceT.Reissue;

                if (GetBool("OrderUpgradeReport"))
                {
                    requestType = E_CreditReportAuditSourceT.Upgrade;
                }
                else if (GetBool("OrderNewReport"))
                {
                    requestType = E_CreditReportAuditSourceT.OrderNew;
                }
                else if (GetBool("OrderLqiReport"))
                {
                    requestType = E_CreditReportAuditSourceT.OrderLqiReport;
                }
                else if (this.GetBool("ReissueLqiReport"))
                {
                    requestType = E_CreditReportAuditSourceT.ReissueLqiReport;
                }

                if (requestData.RequestActionType == E_CreditReportRequestActionType.Other)
                {
                    switch (requestData.RequestActionTypeOtherDetail)
                    {
                        case CreditReportRequestActionTypeOtherDetail.LqiNew: return E_CreditReportAuditSourceT.OrderLqiReport;
                        case CreditReportRequestActionTypeOtherDetail.LqiReissue: return E_CreditReportAuditSourceT.ReissueLqiReport;
                        case CreditReportRequestActionTypeOtherDetail.RemoveBorrower: return E_CreditReportAuditSourceT.RemoveBorrower;
                        case CreditReportRequestActionTypeOtherDetail.RemoveCoBorrower: return E_CreditReportAuditSourceT.RemoveCoBorrower;
                        case CreditReportRequestActionTypeOtherDetail.RetrieveSupplement: return E_CreditReportAuditSourceT.RetrieveSupplement;
                        case CreditReportRequestActionTypeOtherDetail.RetrieveBilling: return E_CreditReportAuditSourceT.RetrieveBillingReport;
                    }
                }

                return requestType;
            }
            /// <summary>
            /// Helper function that will set basic user information to borrower object.
            /// </summary>
            /// <param name="borrower"></param>
            /// <param name="args"></param>
            /// <param name="isBorrower"></param>
            private void SetBorrowerInfo(LendersOffice.CreditReport.BorrowerInfo borrower, CAppData dataApp, CreditReportProtocol protocol) 
            {
                bool isCoborrower = borrower.IsCoborrower;

            
                borrower.FirstName = isCoborrower ? dataApp.aCFirstNm : dataApp.aBFirstNm;
                borrower.LastName = isCoborrower ? dataApp.aCLastNm : dataApp.aBLastNm;
                borrower.MiddleName = isCoborrower ? dataApp.aCMidNm : dataApp.aBMidNm;
                borrower.Suffix = isCoborrower ? dataApp.aCSuffix : dataApp.aBSuffix;
                borrower.Ssn = isCoborrower ? dataApp.aCSsn : dataApp.aBSsn;
                borrower.DOB = isCoborrower ? dataApp.aCDob_rep : dataApp.aBDob_rep;
                borrower.MaritalStatus = isCoborrower ? (E_aBMaritalStatT) (int) dataApp.aCMaritalStatT : dataApp.aBMaritalStatT;
                
                if (!borrower.IsCoborrower)
                {
                    try
                    {
                        Address address = new Address();
                        address.ParseStreetAddress(dataApp.aBAddr);
                        address.City = dataApp.aBCity;
                        address.State = dataApp.aBState;
                        address.Zipcode = dataApp.aBZip;

                        borrower.CurrentAddress = address;
                        borrower.YearsAtCurrentAddress = dataApp.aBAddrYrs;
                    }
                    catch
                    {
                        Address address = new Address();
                        address.StreetAddress = dataApp.aBAddr;
                        address.City = dataApp.aBCity;
                        address.State = dataApp.aBState;
                        address.Zipcode = dataApp.aBZip;
                        borrower.CurrentAddress = address;

                        Tools.LogWarning("Unable to parse borrower address: " + dataApp.aBAddr);
                    }

                    try
                    {
                        string aBPrev1Addr = dataApp.aBPrev1Addr;
                        string aBPrev1City = dataApp.aBPrev1City;
                        string aBPrev1State = dataApp.aBPrev1State;
                        string aBPrev1Zip = dataApp.aBPrev1Zip;
                        string aBPrev1AddrYrs = dataApp.aBPrev1AddrYrs;

                        // 1/3/2005 dd - If Previous Address section is all blank then skip.
                        if (aBPrev1Addr != "" || aBPrev1City != "" || aBPrev1State != "" || aBPrev1Zip != "" || aBPrev1AddrYrs != "")
                        {
                            try
                            {
                                Address address = new Address();
                                address.ParseStreetAddress(aBPrev1Addr);
                                address.City = aBPrev1City;
                                address.State = aBPrev1State;
                                address.Zipcode = aBPrev1Zip;
                                borrower.PreviousAddress = address;
                                borrower.YearsAtPreviousAddress = aBPrev1AddrYrs;
                            }
                            catch
                            {
                                Address address = new Address();
                                address.StreetAddress = aBPrev1Addr;
                                address.City = aBPrev1City;
                                address.State = aBPrev1State;
                                address.Zipcode = aBPrev1Zip;
                                borrower.PreviousAddress = address;

                                Tools.LogWarning("Unable to parse borrower previous address: " + aBPrev1Addr);
                            }
                        }
                    }
                    catch
                    {

                    }

                    Address mailingAddress = new Address();
                    mailingAddress.StreetAddress = dataApp.aAddrMail;
                    mailingAddress.City = dataApp.aCityMail;
                    mailingAddress.State = dataApp.aStateMail;
                    mailingAddress.Zipcode = dataApp.aZipMail;
                    borrower.MailingAddress = mailingAddress;
                }
                else if (protocol == CreditReportProtocol.CSD || protocol == CreditReportProtocol.InformativeResearch)
                {
                    if (!string.IsNullOrEmpty(dataApp.aCAddrMail) || !string.IsNullOrEmpty(dataApp.aCCityMail)
                        || !string.IsNullOrEmpty(dataApp.aCStateMail) || !string.IsNullOrEmpty(dataApp.aCZipMail))
                    {
                        Address mailingAddress = new Address();
                        mailingAddress.StreetAddress = dataApp.aCAddrMail;
                        mailingAddress.City = dataApp.aCCityMail;
                        mailingAddress.State = dataApp.aCStateMail;
                        mailingAddress.Zipcode = dataApp.aCZipMail;
                        borrower.MailingAddress = mailingAddress;
                    }
                }
            }

            private void GetOrderCreditPermission()
            {
                var craIdStr = GetString("craId");
                if (craIdStr.Length == 0 || craIdStr == Guid.Empty.ToString())
                {
                    SetResult("Error", "No valid CRA.");
                    return;
                }

                var craId = Guid.Parse(craIdStr);
                Guid loanId = this.sLId;

                string reasons;
                var canOrderCredit = Tools.CanOrderCredit(BrokerUser, loanId, craId, out reasons);

                SetResult("CanOrderCredit", canOrderCredit ? "1" : "0");
                SetResult("CanOrderCreditReason", reasons);
            }

            private void GetOrderSupplementUrl()
            {
                Guid craId = this.GetGuid("CraId");
                string reportId = this.GetString("ReportId");
                if (string.IsNullOrEmpty(reportId))
                {
                    this.SetResult("Success", false);
                    this.SetResult("ErrorMessage", "Please specify a valid report id");
                    return;
                }

                var cra = MasterCRAList.FindById(craId, searchProxyTable: true, brokerId: this.BrokerUser.BrokerId, includeInvalidCRAs: false);
                if (cra == null
                    || cra.Protocol != CreditReportProtocol.InformativeResearch
                    || !LqbGrammar.DataTypes.LqbAbsoluteUri.Create(cra.Url).HasValue)
                {
                    this.SetResult("Success", false);
                    this.SetResult("ErrorMessage", "Please specify a valid CRA");
                    return;
                }

                LqbGrammar.DataTypes.LqbAbsoluteUri baseUrl;
                if (LendersOffice.CreditReport.InformativeResearch.IR_CreditReportRequest.IsTestUrl(cra.Url))
                {
                    baseUrl = LqbGrammar.DataTypes.LqbAbsoluteUri.Create("https://uat.informativeresearch.com").ForceValue();
                }
                else
                {
                    baseUrl = LqbGrammar.DataTypes.LqbAbsoluteUri.Create("https://order.informativeresearch.com").ForceValue();
                }

                var uriBuilder = new UriBuilder(baseUrl.GetUri());
                uriBuilder.Path = "/WCS/Supplement/AddSupplements.aspx";
                uriBuilder.Query = "OrderID=" + Uri.EscapeDataString(reportId);
                uriBuilder.Fragment = null;
                this.SetResult("Success", true);
                this.SetResult("url", uriBuilder.ToString());
            }

            private void OrderCredit() 
            {
                Guid loanId = this.sLId;
                Guid comId = GetGuid("comId");

                if (comId == Guid.Empty)
                {
                    throw new CBaseException("Require a valid credit report agency.", "comId is Guid.Empty");
                }

                AuthServiceHelper.CheckOrderCreditAuthorization(loanId, comId);

                string key = null;

                if (GetString("ActionType") == "NEW")
                {
                    // 12/9/2011 dd - OPM 75259 - Prevent user from trigger double click while waiting for new credit.
                    key = "OrderCreditService_" + GetGuid("loanid") + "_" + GetGuid("applicationid") + "_" + GetString("ActionType");

                    object data = LOCache.Get(key);
                    if (data != null)
                    {
                        SetResult("Status", "NOOP");
                        return;
                    }

                    LOCache.Set(key, DateTime.Now, DateTime.Now.AddMinutes(1));
                }

                Guid applicationId = this.aAppId;
                string errorMessage = ValidateCreditReportOrder();
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    this.SetResult("Status", "ERROR");
                    this.SetResult("ErrorMessage", errorMessage);
                    return;
                }

                this.SaveData();
                CAppData dataApp = this.appData; // Set in LoadData(), which is called from the end of SaveData()
                if (applicationId == Guid.Empty)
                {
                    applicationId = dataApp.aAppId;
                }

                CreditRequestData requestData = CreateRequestData(loanId, dataApp);
                var pollingIntervalInSeconds = 10;
                CreditReportRequestHandler requestHandler = new CreditReportRequestHandler(requestData, principal: this.BrokerUser, shouldReissueOnResponseNotReady: false, retryIntervalInSeconds: pollingIntervalInSeconds, maxRetryCount: null, loanId: this.sLId);
                var initialResult = requestHandler.SubmitToProcessor();
                if (initialResult.Status == CreditReportRequestResultStatus.Failure)
                {
                    this.SetResult("Status", "ERROR");
                    this.SetResult("ErrorMessage", string.Join(", ", initialResult.ErrorMessages));
                }
                else
                {
                    this.SetResult("Status", "PROCESSING");
                    this.SetResult("PollingIntervalInSeconds", pollingIntervalInSeconds);
                    this.SetResult("PublicJobId", initialResult.PublicJobId.ToString());
                    this.SetResult("ChosenAppId", applicationId.ToString());
                    this.SetResult("CacheKey", key);
                }
            }

            private void PollForCreditReportRequestResults()
            {
                Guid publicJobId = this.GetGuid("PublicJobId");
                int attemptCount = this.GetInt("AttemptCount");
                int maxRetryCount = 20;

                var result = CreditReportRequestHandler.CheckForRequestResults(publicJobId);
                if (result.Status == CreditReportRequestResultStatus.Failure)
                {
                    this.SetResult("Status", "ERROR");
                    this.SetResult("ErrorMessage", string.Join(", ", result.ErrorMessages));
                    return;
                }
                else if (result.Status == CreditReportRequestResultStatus.Processing)
                {
                    if (attemptCount >= maxRetryCount)
                    {
                        this.SetResult("Status", "ERROR");
                        this.SetResult("ErrorMessage", "Credit Report request has timed out.");
                        Tools.LogError($"[CreditReportRequestResult] PublicJobId: {publicJobId}. UI has timed out.");
                        return;
                    }

                    this.SetResult("Status", "PROCESSING");
                }
                else
                {
                    string status = "";
                    bool hasError = false;
                    ICreditReportResponse response = result.CreditReportResponse;
                    CreditRequestData requestData = result.CreditRequestData;

                    var loanId = this.sLId;
                    var applicationId = GetGuid("ChosenAppId");
                    var cacheKey = GetString("CacheKey", null);
                    try
                    {
                        SetResult("IsReady", response.IsReady);
                        SetResult("ReportID", response.ReportID); //notice ReportID D is capitalize not the same in get. 
                        status = response.Status;
                        hasError = response.HasError;

                        var dataLoan = this.ConstructPageDataClass(loanId);
                        dataLoan.InitLoad();
                        var dataApp = dataLoan.GetAppData(applicationId);

                        if (response.HasError)
                        {
                            // 12/15/2006 nw - OPM 8360 - Provide a more descriptive message for FNMA "User not authenticated" error
                            if (response.ErrorMessage.ToLower() == "user not authenticated")
                                SetResult("ErrorMessage", "The FannieMae DU login information is incorrect. Please confirm your FannieMae DU login and password. To verify your account, contact FannieMae Customer Care at (877)722-6757.");
                            else
                                SetResult("ErrorMessage", response.ErrorMessage);
                        }

                        if (response.IsReady)
                        {
                            E_CreditReportAuditSourceT auditType = ComputeAuditType(requestData);
                            if (CreditReportUtilities.IsNonCreditReport(auditType))
                            {
                                string documentsUploadedStatusDescription = CreditReportUtilities.SaveNonCreditReport(response, BrokerUser, loanId, applicationId, requestData, auditType);
                                this.SetResult("IsNonCreditReport", true);
                                this.SetResult("NonCreditReportStatusDescription", CreditReportUtilities.GetNonCreditReportStatusDescription(response));
                                this.SetResult("NonCreditReportDocumentUploadStatus", documentsUploadedStatusDescription);
                            }
                            else if (CreditReportUtilities.IsBorrowerInformationMatch(loanId, applicationId, requestData, response)
                                || (auditType == E_CreditReportAuditSourceT.Reissue && string.IsNullOrEmpty(dataApp.aBNm)))
                            {
                                // 12/18/2006 nw - post credit order/reissue checkings have been moved to CreditReportUtilities.cs

                                CreditReportUtilities.SaveXmlCreditReport(response, BrokerUser, loanId, applicationId, requestData.ActualCraID, requestData.ProxyId, auditType);
                                if (!requestData.IsRequestLqiReport && !requestData.IsRequestReissueLqiReport)
                                {
                                    bool hasLiabilities = dataApp.aLiaCollection.CountRegular != 0;

                                    if (hasLiabilities)
                                    {
                                        SetResult("HasExistingLiabilities", true);

                                        var dataLoan2 = new NotEnforceAccessControlPageData(loanId, new string[] { "sfImportCreditScoresFromCreditReport" });
                                        dataLoan2.InitSave(ConstAppDavid.SkipVersionCheck);
                                        var dataApp2 = dataLoan2.GetAppData(dataApp.aAppId);
                                        dataApp2.ImportCreditScoresFromCreditReport();
                                        dataLoan2.Save();
                                    }
                                    else
                                    {
                                        m_version = CreditReportUtilities.ImportLiabilities(loanId, applicationId, !GetBool("IsImport0Balance"), GetBool("DeleteExistingLiabilities", false), false);
                                    }
                                }
                            }
                            else
                            {
                                // 04/08/10 vm - OPM 43616 we want to check the information on credit report and the loan file matches.

                                status = "ERROR";
                                hasError = true;
                                SetResult("ErrorMessage", ErrorMessages.BorrowerInfoMismatchBetweenLoanAndCredit);
                            }
                        }
                        else
                        {
                            if (requestData.CreditProtocol == CreditReportProtocol.InfoNetwork || requestData.CreditProtocol == CreditReportProtocol.Mcl)
                                SetResult("StatusQuery", "T");
                        }

                        if (cacheKey != null)
                        {
                            if (status == "ERROR" || status == "LOGIN_FAILED")
                            {
                                // 12/9/2011 dd - Clear the key.
                                LOCache.Set(cacheKey, null, DateTime.Now);
                            }
                        }
                    }
                    catch (CBaseException exc)
                    {
                        status = "ERROR";
                        hasError = true;
                        if (exc.DeveloperMessage.StartsWith("Credit response missing"))
                        {
                            SetResult("ErrorMessage", exc.UserMessage);
                            Tools.LogErrorWithCriticalTracking("Credit response returned with missing XML or viewable data.", exc);
                        }
                        else
                        {
                            SetResult("ErrorMessage", ErrorMessages.CreditReport_GenericError);
                            Tools.LogErrorWithCriticalTracking("Unable to order credit report", exc);
                        }
                    }
                    catch (Exception exc)
                    {
                        status = "ERROR";
                        hasError = true;
                        SetResult("ErrorMessage", ErrorMessages.CreditReport_GenericError);
                        Tools.LogErrorWithCriticalTracking("Unable to order credit report", exc);
                    }

                    SetResult("Status", status);
                    SetResult("HasError", hasError);
                }
            }

            private void OrderCreditSynchronously()
            {
                // TODO CREDIT_BJP: Temporary code. Remove once we have confirmed credit is working via the BJP.
                Guid loanId = this.sLId;
                Guid comId = GetGuid("comId");

                if (comId == Guid.Empty)
                {
                    throw new CBaseException("Require a valid credit report agency.", "comId is Guid.Empty");
                }

                AuthServiceHelper.CheckOrderCreditAuthorization(loanId, comId);

                string key = null;

                if (GetString("ActionType") == "NEW")
                {
                    // 12/9/2011 dd - OPM 75259 - Prevent user from trigger double click while waiting for new credit.
                    key = "OrderCreditService_" + GetGuid("loanid") + "_" + GetGuid("applicationid") + "_" + GetString("ActionType");

                    object data = LOCache.Get(key);
                    if (data != null)
                    {
                        SetResult("Status", "NOOP");
                        return;
                    }

                    LOCache.Set(key, DateTime.Now, DateTime.Now.AddMinutes(1));
                }

                Guid applicationId = this.aAppId;
                string errorMessage = ValidateCreditReportOrder();
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    this.SetResult("Status", "ERROR");
                    this.SetResult("ErrorMessage", errorMessage);
                    return;
                }

                this.SaveData();
                CAppData dataApp = this.appData; // Set in LoadData(), which is called from the end of SaveData()
                if (applicationId == Guid.Empty)
                {
                    applicationId = dataApp.aAppId;
                }

                string status = "";
                bool hasError = false;

                try
                {
                    CreditRequestData requestData = CreateRequestData(loanId, dataApp);
                    CreditReportRequestHandler requestHandler = new CreditReportRequestHandler(requestData, PrincipalFactory.CurrentPrincipal);
                    var result = requestHandler.SubmitSynchronously();
                    if (result.Status == CreditReportRequestResultStatus.Failure)
                    {
                        this.SetResult("Status", "ERROR");
                        this.SetResult("ErrorMessage", string.Join(", ", result.ErrorMessages));
                        return;
                    }
                    else
                    {
                        var response = result.CreditReportResponse;
                        SetResult("IsReady", response.IsReady);
                        SetResult("ReportID", response.ReportID); //notice ReportID D is capitalize not the same in get. 
                        status = response.Status;
                        hasError = response.HasError;


                        if (response.HasError)
                        {
                            // 12/15/2006 nw - OPM 8360 - Provide a more descriptive message for FNMA "User not authenticated" error
                            if (response.ErrorMessage.ToLower() == "user not authenticated")
                                SetResult("ErrorMessage", "The FannieMae DU login information is incorrect. Please confirm your FannieMae DU login and password. To verify your account, contact FannieMae Customer Care at (877)722-6757.");
                            else
                                SetResult("ErrorMessage", response.ErrorMessage);
                        }

                        if (response.IsReady)
                        {
                            E_CreditReportAuditSourceT auditType = ComputeAuditType(requestData);
                            if (CreditReportUtilities.IsNonCreditReport(auditType))
                            {
                                string documentsUploadedStatusDescription = CreditReportUtilities.SaveNonCreditReport(response, BrokerUser, loanId, applicationId, requestData, auditType);
                                this.SetResult("IsNonCreditReport", true);
                                this.SetResult("NonCreditReportStatusDescription", CreditReportUtilities.GetNonCreditReportStatusDescription(response));
                                this.SetResult("NonCreditReportDocumentUploadStatus", documentsUploadedStatusDescription);
                            }
                            else if (CreditReportUtilities.IsBorrowerInformationMatch(loanId, applicationId, requestData, response)
                                || (auditType == E_CreditReportAuditSourceT.Reissue && string.IsNullOrEmpty(dataApp.aBNm)))
                            {
                                // 12/18/2006 nw - post credit order/reissue checkings have been moved to CreditReportUtilities.cs

                                CreditReportUtilities.SaveXmlCreditReport(response, BrokerUser, loanId, applicationId, requestData.ActualCraID, requestData.ProxyId, auditType);
                                if (!requestData.IsRequestLqiReport && !requestData.IsRequestReissueLqiReport)
                                {
                                    bool hasLiabilities = dataApp.aLiaCollection.CountRegular != 0;

                                    if (hasLiabilities)
                                    {
                                        SetResult("HasExistingLiabilities", true);

                                        var dataLoan = new NotEnforceAccessControlPageData(loanId, new string[] { "sfImportCreditScoresFromCreditReport" });
                                        dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                                        var dataApp2 = dataLoan.GetAppData(dataApp.aAppId);
                                        dataApp2.ImportCreditScoresFromCreditReport();
                                        dataLoan.Save();
                                    }
                                    else
                                    {
                                        m_version = CreditReportUtilities.ImportLiabilities(loanId, applicationId, !GetBool("IsImport0Balance"), GetBool("DeleteExistingLiabilities", false), false);
                                    }
                                }
                            }
                            else
                            {
                                // 04/08/10 vm - OPM 43616 we want to check the information on credit report and the loan file matches.

                                status = "ERROR";
                                hasError = true;
                                SetResult("ErrorMessage", ErrorMessages.BorrowerInfoMismatchBetweenLoanAndCredit);
                            }
                        }
                        else
                        {
                            if (requestData.CreditProtocol == CreditReportProtocol.InfoNetwork || requestData.CreditProtocol == CreditReportProtocol.Mcl)
                                SetResult("StatusQuery", "T");
                        }
                    }
                }
                catch (CBaseException exc)
                {
                    status = "ERROR";
                    hasError = true;
                    if (exc.DeveloperMessage.StartsWith("Credit response missing"))
                    {
                        SetResult("ErrorMessage", exc.UserMessage);
                        Tools.LogErrorWithCriticalTracking("Credit response returned with missing XML or viewable data.", exc);
                    }
                    else
                    {
                        SetResult("ErrorMessage", ErrorMessages.CreditReport_GenericError);
                        Tools.LogErrorWithCriticalTracking("Unable to order credit report", exc);
                    }
                }
                catch (Exception exc)
                {
                    status = "ERROR";
                    hasError = true;
                    SetResult("ErrorMessage", ErrorMessages.CreditReport_GenericError);
                    Tools.LogErrorWithCriticalTracking("Unable to order credit report", exc);
                }
                if (key != null)
                {
                    if (status == "ERROR" || status == "LOGIN_FAILED")
                    {
                        // 12/9/2011 dd - Clear the key.
                        LOCache.Set(key, null, DateTime.Now);
                    }
                }
                SetResult("Status", status);
                SetResult("HasError", hasError);
            }

            /// <summary>
            /// Validates a requested order for a credit report to make sure it doesn't hit particular edge cases.
            /// </summary>
            /// <returns>The error message from the validation, or null if there were no issues.</returns>
            private string ValidateCreditReportOrder()
            {
                bool isRemoveBorrower = StringComparer.OrdinalIgnoreCase.Equals(this.GetString("ActionType"), "REMOVE_BORROWER");
                bool isRemoveCoborrower = StringComparer.OrdinalIgnoreCase.Equals(this.GetString("ActionType"), "REMOVE_COBORROWER");

                if (!isRemoveBorrower && !isRemoveCoborrower)
                {
                    return null;
                }

                // 2018-05 tj - We don't have a way to wrap the page save up with the order credit, and we also don't have audit functionality in the
                // credit framework.  Thus, my best option at this point is to manually load the loan, apply the data, then check the data that would
                // be loaded for the corresponding credit report.
                var loan = this.ConstructPageDataClass(this.sLId);
                loan.InitLoad();
                var application = loan.GetAppData(this.aAppId);
                this.BindData(loan, application);
                if (string.IsNullOrEmpty(application.aBFirstNm) || string.IsNullOrEmpty(application.aBLastNm) || string.IsNullOrEmpty(application.aBSsn))
                {
                    return "The applicant to be removed is the only one on this application. Please ensure that the applicant to be retained has been filled out, and retry if necessary.";
                }
                else if (isRemoveCoborrower &&
                    (string.IsNullOrEmpty(application.aCFirstNm) || string.IsNullOrEmpty(application.aCLastNm) || string.IsNullOrEmpty(application.aCSsn)))
                {
                    return "There is no co-borrower on this application to remove.";
                }
                else if (isRemoveBorrower
                    && string.IsNullOrEmpty(application.aBAddr)
                    && string.IsNullOrEmpty(application.aBCity)
                    && string.IsNullOrEmpty(application.aBState)
                    && string.IsNullOrEmpty(application.aBZip))
                {
                    // Note: aCAddr (and like) became aBAddr during this.BindData, since the borrower data was swapped
                    return "Please fill out the co-borrower's present address prior to removing the borrower, and then retry.";
                }

                return null;
            }

            private void ImportLiabilities()
            {
                m_version = CreditReportUtilities.ImportLiabilities(GetGuid("loanid"), GetGuid("applicationid") , !GetBool("IsImport0Balance"), GetBool("DeleteExistingLiabilities", false), false);
            }

		    // 3/28/2007 nw - OPM 10105 - let users know the CRA they want to pull or reissue credit is having technical difficulty
		    private void CheckCRAWarningMessage()
		    {
                Guid comId = GetGuid("comId");

                if (comId == Guid.Empty)
                {
                    throw new CBaseException("Require a valid credit report agency.", "comId is Guid.Empty");
                }

                var cra = Tools.GetCraById(comId, this.BrokerUser.BrokerId);

			    if (cra.IsWarningOn == true)
			    {
				    SetResult("IsWarningOn", true);
				    SetResult("WarningMsg", cra.WarningMsg);
				    SetResult("WarningStartD", string.Format("{0} {1} {2}", cra.WarningStartD.ToShortDateString(), cra.WarningStartD.ToShortTimeString(), TimeZone.CurrentTimeZone.IsDaylightSavingTime(cra.WarningStartD) ? "PDT" : "PST"));
			    }
            }
        }
    }
}
