﻿namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Linq;
    using System.Web;
    using System.Web.UI.WebControls;
    using LendersOffice.Common;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Security;

    public partial class SeamlessDocumentFormsList : BaseLoanPage
    {
        protected string ProgramID
        {
            get
            {
                return HttpContext.Current.Request.QueryString["ProgramID"] ?? "";
            }
        }
        protected string PackageID
        {
            get
            {
                return HttpContext.Current.Request.QueryString["PackageID"] ?? "";
            }
        }
        protected Guid VendorId
        {
            get
            {
                var values = Request.QueryString.GetValues("VendorId");
                if (values.Length > 0)
                    return new Guid(values[0]);

                throw new NotFoundException("Document Vendor not found", "VendorId invalid Guid");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            IDocumentVendor vendor = DocumentVendorFactory.Create(PrincipalFactory.CurrentPrincipal.BrokerId, VendorId);
            var forms = vendor.GetFormList(LoanID, ProgramID, PackageID, PrincipalFactory.CurrentPrincipal);
            if (forms.HasError)
            {
                m_alertMessage.Value = forms.ErrorMessage;
                return;
            }

            m_FormsList.Items.AddRange((from format in forms.Result
                                       select new ListItem(
                                           format.Name,
                                           format.BorrowerNumber + ":" + format.ID
                                       )).ToArray());
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "DocMagicDocGeneration";
            this.PageID = "DocMagicDocGeneration";
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = false; // There is no service page, so set this to false so that the smoke test won't choke on this page
            DisplayCopyRight = false;
            RegisterJsScript("ModelessDlg.js");
            EnableJqueryMigrate = false;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
