﻿

namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Security;
    using LendersOffice.Services;

    public partial class TransferToPicker : BaseServicePage
    {
        // { VendorId: Guid, code: string }
        protected Guid VendorId
        {
            get
            {
                var values = Request.QueryString.GetValues("VendorId");
                if (values.Length > 0)
                    return new Guid(values[0]);

                throw new NotFoundException("Document Vendor not found", "VendorId invalid Guid");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string code = RequestHelper.GetSafeQueryString("code");

            var result = DocumentVendorService.GetTransferToVendor(PrincipalFactory.CurrentPrincipal.BrokerId, VendorId, code);
            if (result.HasError)
            {
                ErrorMessage.Value = "An error has occurred while downloading data from DocMagic. "
                + "Please try again or contact support of this issue continues to occur.";
                return;
            }

            var data = result.Result;

            var hasData = data.Count > 0;
            m_transferToGrid.Visible = hasData;
            if (hasData)
            {
                m_transferToGrid.DataSource = data;
                m_transferToGrid.DataBind();
            }
            else
            {
                NotificationMessage.InnerText = "No Transfer To Options exist for this Plan Code.";
            }
        }

        protected void TransferTo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem == null)
            {
                return;
            }

            InvestorInfo info = (InvestorInfo)e.Row.DataItem;
            HtmlAnchor selectLink = e.Row.FindControl("Link") as HtmlAnchor;
            selectLink.InnerText = $"{info.Description} - ({info.Code})";
            selectLink.Attributes.Add("onclick", $"f_selectTransferToInvestor({AspxTools.JsString(info.Description)}, {AspxTools.JsString(info.Code)});");
        }
    }
}
