﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos.Services
{
    public partial class DocMagicDeliveryInformation : LendersOffice.Common.BaseServicePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Zip.SmartZipcode(City, State);

            if (!Page.IsPostBack)
            {
                Attention.Text = RequestHelper.GetSafeQueryString("attention");
                Zip.Text = RequestHelper.GetSafeQueryString("zip");
                Name.Text = RequestHelper.GetSafeQueryString("name");
                Street.Text = RequestHelper.GetSafeQueryString("street");
                City.Text = RequestHelper.GetSafeQueryString("city");
                State.Value = RequestHelper.GetSafeQueryString("state");
                Phone.Text = RequestHelper.GetSafeQueryString("phone");
                DSINotes.Text = RequestHelper.GetSafeQueryString("dsiNotes");
            }
        }
        protected void OK_OnClick(object sender, EventArgs e)
        {
            CloseDialog();
        }
        private void CloseDialog()
        {
            LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this, new string[] {
                "attention=" + AspxTools.JsString(Attention.Text),
                "name=" + AspxTools.JsString(Name.Text),
                "street="+ AspxTools.JsString(Street.Text),
                "city="+ AspxTools.JsString(City.Text),
                "state="+ AspxTools.JsString(State.Value),
                "zip="+ AspxTools.JsString(Zip.Text),
                "phone="+ AspxTools.JsString(Phone.Text),
                "dsiNotes="+ AspxTools.JsString(DSINotes.Text),
                "OK=true"
            });
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            //this.Init += new EventHandler(this.Page_Init);


        }
        #endregion
    }
}
