﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SeamlessDocumentFormsList.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.SeamlessDocumentFormsList" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>

<body bgcolor="gainsboro" style="padding-left:3px">
    <form id="form1" runat="server">
    <div>
    Select the document(s) to generate <br />
    <div align="center" style="padding-top:3px;padding-bottom:3px">
        <asp:ListBox SelectionMode="Multiple" ID="m_FormsList" runat="server" Width="95%" Height="450">
        </asp:ListBox>
    </div>
    <div align="center">
    <asp:Button ID="m_OK" runat="server" Text="OK" OnClientClick="f_okClicked(); return false;"/>
    <asp:Button ID="m_Cancel" runat="server" Text="Cancel" OnClientClick="f_cancelClicked(); return false;" />
    </div>
    
    </div>
    <asp:HiddenField ID="m_alertMessage" runat="server" />
    </form>
</body>

<script>
    function f_okClicked() {
        var formlist = <%=AspxTools.JsGetElementById(m_FormsList) %>;
        var selectedoptions = [];
        for(var i = 0; i <  formlist.options.length; i++)
        {
            var option = formlist.options[i];
            if(option.selected)
            {
                selectedoptions.push(option.value);
            }
        }        
        
        var args = {};
        args.selectedForms = selectedoptions.join(";");
        onClosePopup(args);        
    }
    function f_cancelClicked() {
        onClosePopup();
    }
    function alertAndCloseIfError()
    {
        var alertMessage = <%=AspxTools.JsGetElementById(m_alertMessage) %>.value;
        if(alertMessage)
        {
            alert(alertMessage);
            onClosePopup();
        }
    }
    function init()
    {
        alertAndCloseIfError();
    }
    init();
</script>
</html>
