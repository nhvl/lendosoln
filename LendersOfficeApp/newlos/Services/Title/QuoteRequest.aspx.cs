﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.ObjLib.TitleProvider.Audit;
using LendersOffice.ObjLib.TitleProvider;
using LendersOffice.AntiXss;
using DataAccess;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.newlos.Services.Title
{
    public partial class QuoteRequest : BaseLoanPage
    {

        protected override LendersOffice.Security.Permission[] RequiredReadPermissions
        {
            get
            {
                return new LendersOffice.Security.Permission[] {
                    LendersOffice.Security.Permission.AllowOrderingTitleServices
                };
            }
        }

        private CPageData m_loanData;

        protected override void LoadData()
        {
            m_loanData = CPageData.CreateUsingSmartDependency(LoanID, typeof(QuoteRequest));
            m_loanData.InitLoad();

            TitleAuditor auditor = new TitleAuditor(LoanID);
            AuditResults.DataSource = auditor.Rules;
            AuditResults.DataBind();

            AuditResult.Value = auditor.Success.ToString();
            NumApps.Value = m_loanData.nApps.ToString();


            List<AppRelationshipData> apps = new List<AppRelationshipData>(m_loanData.nApps);
            for (int i = 0; i < m_loanData.nApps; i++)
            {
                CAppData currentApp = m_loanData.GetAppData(i);
                AppRelationshipData appForBind = new AppRelationshipData();
                appForBind.Id = currentApp.aAppId;
                appForBind.aBNm = currentApp.aBNm;
                appForBind.aCNm = currentApp.aCNm;
                appForBind.aBRelationshipTitleT = currentApp.aBRelationshipTitleT;
                appForBind.aCRelationshipTitleT = currentApp.aCRelationshipTitleT;
                appForBind.hasCoborrower = currentApp.aCIsValidNameSsn;
                appForBind.aBRelationshipTitleOtherDesc = currentApp.aBRelationshipTitleOtherDesc;
                appForBind.aCRelationshipTitleOtherDesc = currentApp.aCRelationshipTitleOtherDesc;
                appForBind.aBTypeT = currentApp.aBTypeT;
                appForBind.aCTypeT = currentApp.aCTypeT;
                apps.Add(appForBind);
            }

            var titleOnlyBorrowers = m_loanData.sTitleBorrowers;
            NumTitleBorrowers.Value = titleOnlyBorrowers.Count.ToString();
            foreach (TitleBorrower titleBorrower in titleOnlyBorrowers )
            {
                AppRelationshipData rd = new AppRelationshipData();
                rd.aBNm = titleBorrower.FullName;
                rd.aBRelationshipTitleOtherDesc = titleBorrower.RelationshipTitleTOtherDesc;
                rd.aBRelationshipTitleT = titleBorrower.RelationshipTitleT;
                rd.hasCoborrower = false;
                rd.IsTitleOnly = true;
                rd.Id = titleBorrower.Id;
                apps.Add(rd);
            }

            Relationships.DataSource = apps;
            Relationships.DataBind();
            ClientScript.RegisterHiddenField("RelationshipsId", Relationships.ClientID);


            var log = TitleService.GetQuoteOrderLog(Broker.BrokerID, LoanID);
            if (log == null || !log.PolicyOrderedD.HasValue )
            {
                CurrentPolicyInfo.Visible = false;
            }
            else
            {
                RequestedD.Text = Tools.GetDateTimeDescription(log.PolicyOrderedD.Value);
                PolicyProvider.Text = log.PolicyProviderName;
                PolicyId.Text = log.PolicyID;
                DocsReceived.DataSource = log.DocumentReceipts;
                DocsReceived.DataBind();

                if (log.DocumentReceipts.Count == 0)
                {
                    DocsReceived.Visible = false;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            EnableJqueryMigrate = false;
            PageID = "Page_TitleQuoteRequest";
            RegisterService("Quote", "/newlos/Services/Title/QuoteServices.aspx");

            //we use this in the js to determine if the page should refresh or redirect when a quote is requested.
            if (RequestHelper.GetSafeQueryString("l") == "1")
            {
                ClientScript.RegisterHiddenField("l", "1");
            }
        }

        protected void DocsReceived_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DocumentReceipt receipt = args.Item.DataItem as DocumentReceipt;

            HtmlAnchor anchor = args.Item.FindControl("DownloadLink") as HtmlAnchor;
            anchor.HRef = Tools.GetEDocsLink(ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx") + "?docid=" + AspxTools.HtmlString(receipt.EDocumentId));
            anchor.InnerText = string.Format("{0}-{1}", receipt.MismoClass, receipt.DocumentName);
            anchor.Target = "_blank";
        }

        protected void AuditResults_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                EncodedLiteral desc = (EncodedLiteral)args.Item.FindControl("Description");
                Repeater data = (Repeater)args.Item.FindControl("DataRepeater");
                EncodedLiteral ruleResult = (EncodedLiteral)args.Item.FindControl("RuleResult");

                AuditRule rule = (AuditRule)args.Item.DataItem;

                desc.Text = rule.Description;
                data.DataSource = rule.Data.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                data.DataBind();
                ruleResult.Text = rule.GetRuleResult();
            }
        }

        protected void Relationships_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType == ListItemType.Footer)
            {
                DropDownList vendors = args.Item.FindControl("TitleVendor") as DropDownList;
                DropDownList policies = args.Item.FindControl("PolicyType") as DropDownList;

                var assoc = TitleProvider.GetAssociations(BrokerID);
                vendors.DataSource = assoc;
                vendors.DataTextField = "VendorName";
                vendors.DataValueField = "VendorId";
                vendors.DataBind();

                if (vendors.Items.Count > 1)
                {
                    vendors.Items.Insert(0, new ListItem() { Text = string.Empty, Value = "" });
                }
                else if (vendors.Items.Count == 1)
                {
                    var service = TitleProvider.GetService(BrokerID, assoc.FirstOrDefault().VendorId);
                    policies.DataSource = service.GetAvailablePolicyTypes(m_loanData.sLPurposeT, m_loanData.sSpState);
                    policies.DataBind();
                    vendors.Enabled = false;
                }
            }
            else if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                AppRelationshipData data = args.Item.DataItem as AppRelationshipData;
                EncodedLabel aBNm = args.Item.FindControl("aBNm") as EncodedLabel;
                EncodedLabel aCNm = args.Item.FindControl("aCNm") as EncodedLabel;
                EncodedLabel aBTypeT_isTitle = args.Item.FindControl("aBTypeT_isTitle") as EncodedLabel;
                EncodedLabel aCTypeT_isTitle = args.Item.FindControl("aCTypeT_isTitle") as EncodedLabel;
                TextBox aBRelationshipTitleOtherDesc = args.Item.FindControl("aBRelationshipTitleOtherDesc") as TextBox;
                TextBox aCRelationshipTitleOtherDesc = args.Item.FindControl("aCRelationshipTitleOtherDesc") as TextBox;
                DropDownList aBRelationshipTitleT = args.Item.FindControl("aBRelationshipTitleT") as DropDownList;
                DropDownList aCRelationshipTitleT = args.Item.FindControl("aCRelationshipTitleT") as DropDownList;
                HtmlTableRow coborrowerRow = args.Item.FindControl("coborrowerRow") as HtmlTableRow;
                HtmlTableCell aBRelationshipTitleTRequiredColumn = args.Item.FindControl("aBRelationshipTitleTRequiredColumn") as HtmlTableCell;
                HtmlTableCell aCRelationshipTitleTRequiredColumn = args.Item.FindControl("aCRelationshipTitleTRequiredColumn") as HtmlTableCell;
                HiddenField entryId = args.Item.FindControl("EntryId") as HiddenField;
                aBNm.Text = data.aBNm;
                entryId.Value = data.Id.ToString("N");

                if (data.aBTypeT.EqualsOneOf(E_aTypeT.NonTitleSpouse, E_aTypeT.CurrentTitleOnly))
                {
                    aBTypeT_isTitle.Visible = true;
                    aBTypeT_isTitle.Text = data.aBTypeT == E_aTypeT.NonTitleSpouse ? "Non Title Spouse" : "Current Title Only";
                    aBRelationshipTitleT.Visible = false;
                    aBRelationshipTitleTRequiredColumn.Visible = false;
                    aBRelationshipTitleOtherDesc.Visible = false;
                }
                else
                {
                    aBTypeT_isTitle.Visible = false;
                    aBRelationshipTitleT.Visible = true;
                    aBRelationshipTitleTRequiredColumn.Visible = true;
                    Tools.Bind_aBRelationshipTitleT(aBRelationshipTitleT);
                    aBRelationshipTitleOtherDesc.Visible = true;
                }
                Tools.SetDropDownListValue(aBRelationshipTitleT, data.aBRelationshipTitleT);
                aBRelationshipTitleOtherDesc.Text = data.aBRelationshipTitleOtherDesc;

                if (data.hasCoborrower)
                {
                    aCNm.Text = data.aCNm;
                    if (data.aCTypeT.EqualsOneOf(E_aTypeT.NonTitleSpouse, E_aTypeT.CurrentTitleOnly))
                    {
                        aCTypeT_isTitle.Visible = true;
                        aCTypeT_isTitle.Text = data.aCTypeT == E_aTypeT.NonTitleSpouse ? "Non Title Spouse" : "Current Title Only";
                        aCRelationshipTitleT.Visible = false;
                        aCRelationshipTitleTRequiredColumn.Visible = false;
                        aCRelationshipTitleOtherDesc.Visible = false;
                    }
                    else
                    {
                        aCTypeT_isTitle.Visible = false;
                        aCRelationshipTitleT.Visible = true;
                        aCRelationshipTitleTRequiredColumn.Visible = true;
                        Tools.Bind_aBRelationshipTitleT(aCRelationshipTitleT);
                        aCRelationshipTitleOtherDesc.Visible = true;
                    }
                    Tools.SetDropDownListValue(aCRelationshipTitleT, data.aCRelationshipTitleT);
                    aCRelationshipTitleOtherDesc.Text = data.aCRelationshipTitleOtherDesc;
                }
                else
                {
                    coborrowerRow.Visible = false;
                }
            }
        }

        protected class AppRelationshipData
        {
            public string aBNm { get; set; }
            public string aCNm { get; set; }
            public E_aRelationshipTitleT aBRelationshipTitleT { get; set; }
            public E_aRelationshipTitleT aCRelationshipTitleT { get; set; }
            public string aBRelationshipTitleOtherDesc { get; set; }
            public string aCRelationshipTitleOtherDesc { get; set; }
            public bool hasCoborrower { get; set; }
            public E_aTypeT aBTypeT { get; set; }
            public E_aTypeT aCTypeT { get; set; }
            public bool IsTitleOnly { get; set; }
            public AppRelationshipData() { }
            public Guid Id { get; set; }
        }
    }
}
