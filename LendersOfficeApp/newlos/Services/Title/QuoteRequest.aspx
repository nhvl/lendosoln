﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuoteRequest.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.Title.QuoteRequest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.ObjLib.TitleProvider.Audit" %>
<%@ Import Namespace="DataAccess" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Quote Request</title>
    <style type="text/css">
        body { background-color: gainsboro; }
        h1 { margin: 0 0 5px 0; padding: 0;}
        h2 { font-size: 12px; padding: 0; margin: 0 0 5px 0; }
        .error { color: Red; }
        .wrapper { padding-left: 14px; }
        .loading {  display: none; font-size: 16px; font-weight: bold;  border: solid 3px black; background-color: Yellow;  top: 50px; left: 50px;  width: 150px; padding: 5px; position: absolute; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <h4 id="PageHeader" class="page-header">Online Quote Request</h4>
    <asp:HiddenField runat="server" ID="AuditResult" />
    <asp:HiddenField runat="server" ID="NumApps" />
    <asp:HiddenField runat="server" ID="NumTitleBorrowers" />
    <div>
        <div class="loading" id="WaitDiv">Ordering Quote...</div>
        
        <div class="wrapper">
        
        <asp:PlaceHolder runat="server" ID="CurrentPolicyInfo">
        <h2>Policy Information</h2>
        <table border="0" cellpadding="3">
            <tr class="GridItem">
                <td class="FieldLabel">Policy Requested Date</td>
                <td><ml:EncodedLiteral runat="server" ID="RequestedD"></ml:EncodedLiteral>  </td>
            </tr>
            <tr class="GridAlternatingItem">
                <td class="FieldLabel">Policy Provider</td>
                <td><ml:EncodedLiteral runat="server" ID="PolicyProvider"></ml:EncodedLiteral></td>
            </tr>
            <tr class="GridItem">
                <td class="FieldLabel">Policy Id</td>
                <td><ml:EncodedLiteral runat="server" ID="PolicyId"/></td>
            </tr>
        </table>
        
        <asp:Repeater runat="server" ID="DocsReceived" OnItemDataBound="DocsReceived_OnItemDataBound" >
            <HeaderTemplate>
            <br />
            <h2>Documents Received</h2>
                <ul style="margin:0 0 0 30px; padding:0; ">
            </HeaderTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
            <ItemTemplate>
                <li>
                    <a runat="server" id="DownloadLink"></a>
                </li>
            </ItemTemplate>
        </asp:Repeater>
        
                <br />
        </asp:PlaceHolder>
        
        <h2>Audit</h2>
        
        
        <asp:Repeater runat="server" id="AuditResults" OnItemDataBound="AuditResults_OnItemDataBound">
            <HeaderTemplate>
                <table cellpadding="3" cellspacing="0">
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr runat="server" class='<%# AspxTools.HtmlString( Container.ItemIndex % 2 == 0 ? "GridItem" : "GridAlternatingItem")  %>'>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="Description"></ml:EncodedLiteral>
                    </td>
                    <td>
                        <asp:Repeater runat="server" ID="DataRepeater">
                            <ItemTemplate><%# AspxTools.HtmlString((string)Container.DataItem) %></ItemTemplate>
                            <SeparatorTemplate><br /></SeparatorTemplate>
                        </asp:Repeater>
                    </td>
                    <td class="error" width="250">
                        <ml:EncodedLiteral runat="server" ID="RuleResult"></ml:EncodedLiteral>
                    </td>
                </tr>            
            </ItemTemplate>
            <FooterTemplate>
                <tr class="height">
                    <td colspan="3"></td>
                </tr>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <table cellpadding="3" cellspacing="0">
           <tbody>
           <tr><td></td><td class="FieldLabel">Relationship Title Type</td></tr>
            <asp:Repeater runat="server" ID="Relationships" OnItemDataBound="Relationships_OnItemDataBound">
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:HiddenField runat="server" ID="EntryId" />
                            <ml:EncodedLabel ID="aBNm" runat="server"></ml:EncodedLabel>
                        </td>
                        <td>
                            <asp:DropDownList ID="aBRelationshipTitleT" runat="server" onchange="ToggleRelationshipTitleOther()">
                            </asp:DropDownList>
                            <ml:EncodedLabel ID="aBTypeT_isTitle" runat="server">Non-Title Spouse</ml:EncodedLabel>
                        </td>
                        <td id="aBRelationshipTitleTRequiredColumn" runat="server">
                            <img src="../../../images/require_icon.gif" alt="required" />
                        </td>
                        <td>
                            <asp:TextBox ID="aBRelationshipTitleOtherDesc" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="coborrowerRow" runat="server">
                        <td>
                            <ml:EncodedLabel ID="aCNm" runat="server"></ml:EncodedLabel>
                        </td>
                        <td>
                            <asp:DropDownList ID="aCRelationshipTitleT" runat="server" onchange="ToggleRelationshipTitleOther()">
                            </asp:DropDownList>
                            <ml:EncodedLabel ID="aCTypeT_isTitle" runat="server">Non-Title Spouse</ml:EncodedLabel>
                        </td>
                        <td id="aCRelationshipTitleTRequiredColumn" runat="server">
                            <img src="../../../images/require_icon.gif" alt="required" />
                        </td>
                        <td>
                            <asp:TextBox ID="aCRelationshipTitleOtherDesc" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                </ItemTemplate>
                <SeparatorTemplate>
                    <tr>
                        <td colspan="3">
                            <hr />
                        </td>
                    </tr>
                </SeparatorTemplate>
                <FooterTemplate>
                    <tr class="height">
                    <td colspan="3">
                        <hr />
                    </td>
                    </tr>
                    <tr class="height">
                        <td>Title Vendor</td>
                        <td colspan="2">
                            <asp:DropDownList runat="server" ID="TitleVendor" class="TitleVendor"></asp:DropDownList>
                            <img src="../../../images/require_icon.gif" alt="required" />
                        </td>
                        
                    </tr>
                    <tr class="height">
                        <td>
                          <span id="pt">  Policy Type</span>
                        </td>
                        <td>
                        <asp:DropDownList runat="server" ID="PolicyType" CssClass="PolicyType"></asp:DropDownList>
                        </td> 
                        
                    </tr>
                    <tr class="height" class="ownerpolicyrow">
                        <td>
                            Owner&#39;s Policy Type
                        </td>
                        <td colspan="2">
                            <select id="ownerpolicy" class="val">
                            <option value="-1">None</option>
                            </select>
                            <img src="../../../images/require_icon.gif" alt="required" />
                        </td>
                    </tr>
                    <tr class="height" class="lenderpolicyrow">
                        <td>
                            Lender&#39;s Policy Type
                        </td>
                        <td colspan="2">
                            <select id="lenderpolicy" class="val">
                            <option value="-1">None</option>
                            </select>
                            
                            <img src="../../../images/require_icon.gif" alt="required" />
                        </td>
                    </tr>
                    <tr class="height" class="requestescrow">
                        <td>
                            Request Escrow/Closing? 
                        </td>
                        <td>
                            <input type="radio" name="requestclosing"  value="True" checked="checked" /> Yes 
                            <input type="radio" name="requestclosing" value="False" /> No  <img src="../../../images/require_icon.gif" alt="required" />
                        </td>
                    </tr>
                </FooterTemplate>
            </asp:Repeater>
            </tbody>
        </table>
        <br />
        
        <label><input type="checkbox" id="applycc" />Update closing costs using quote</label>
        <br />
        <br />
        <div class="error"></div>
        <input type="button" NoHighlight="NoHighlight" id="GetQuote" value="Get Quote..." />
        </div>
    </div>
    <script type="text/javascript">
        function ToggleRelationshipTitleOther() {
            for (var i = 0;; i += 2) {
                var sIndex = i > 9 ? i.toString(): "0" + i.toString();
            
                if ( document.getElementById("Relationships_ctl" + sIndex + "_aBRelationshipTitleT") == null )
                    break;
            
                var borrDDL = document.getElementById("Relationships_ctl" + sIndex + "_aBRelationshipTitleT");
                var cobrDDL = document.getElementById("Relationships_ctl" + sIndex + "_aCRelationshipTitleT");
                
                var borrTb = document.getElementById("Relationships_ctl" + sIndex + "_aBRelationshipTitleOtherDesc");
                var cobrTb = document.getElementById("Relationships_ctl" + sIndex + "_aCRelationshipTitleOtherDesc");
                var otrVal = <%= AspxTools.HtmlString(E_aRelationshipTitleT.Other.ToString("d")) %>;
                
                // If the DDL is set to "other", show the textbox
                if (borrDDL.value == otrVal) {
                    borrTb.readOnly = false;
                    borrTb.style.display = "block";
                } else if(borrTb != null) {
                    borrTb.style.display = "none";
                }
                
                if (cobrDDL != null) {
                    if (cobrDDL.value == otrVal) {
                        cobrTb.readOnly = false;
                        cobrTb.style.display = "block";
                    } else {
                        cobrTb.style.display = "none";
                    }
                }
                
                if(borrTb != null) {
                    borrTb.readOnly = borrDDL.value != otrVal;
                }
                    
                if (cobrTb != null) {
                    cobrTb.readOnly = cobrDDL.value != otrVal;
                }
            }
        }
        function GetEntryId(AppID) {
            var sIndex = (AppID * 2) > 9 ? (AppID * 2).toString(): "0" + (AppID * 2).toString();
            return titleTypeDDL = document.getElementById("Relationships_ctl" + sIndex + "_EntryId").value;
        }
        function GetRelationshipTitleType(AppID, isBorrower) {
            var sIndex = (AppID * 2) > 9 ? (AppID * 2).toString(): "0" + (AppID * 2).toString();
            var titleTypeDDL;
            
            if(isBorrower) {
                titleTypeDDL = document.getElementById("Relationships_ctl" + sIndex + "_aBRelationshipTitleT");
            }
            else {
                titleTypeDDL = document.getElementById("Relationships_ctl" + sIndex + "_aCRelationshipTitleT");
            }
            
            if(titleTypeDDL != null) {
                return titleTypeDDL.value;
            }
            else {
                return "";
            }
        }
        
        function GetRelationshipTitleOther(AppID, isBorrower) {
            var sIndex = (AppID * 2) > 9 ? (AppID * 2).toString(): "0" + (AppID * 2).toString();
            var titleOtherTB;
            
            if(isBorrower) {
                titleOtherTB = document.getElementById("Relationships_ctl" + sIndex + "_aBRelationshipTitleOtherDesc");
            }
            else {
                titleOtherTB = document.getElementById("Relationships_ctl" + sIndex + "_aCRelationshipTitleOtherDesc");
            }
            
            if(titleOtherTB != null) {
                return titleOtherTB.value;
            }
            else {
                return "";
            }
        }
    
        $(function() {
            var quoteBtn = $('#GetQuote'),
                passesValidation = $('#AuditResult').val() == 'True',
                titleDdl = $('.TitleVendor'),
                policyDdl = $('.PolicyType'),
                error = $('div.error'),
                requestescrow = $('tr.requestescrow'),
                lenderpolicyrow = $('tr.lenderpolicyrow'),
                ownerpolicyrow = $('tr.ownerpolicyrow'),
                lenderpolicy = $('#lenderpolicy'),
                ownerpolicy = $('#ownerpolicy');



            $('select.TitleVendor,select.val').change(function() {
                var res = !passesValidation || this.value.length == 0;

                if (!res && (lenderpolicy.is(':visible') || ownerpolicy.is(':visible'))) {
                    res = (lenderpolicy.val() == '-1' && ownerpolicy.val() == '-1')
                }

                quoteBtn.prop('disabled', res);
            }).triggerHandler('change');

            titleDdl.change(function() {
                policyDdl.children('option').remove();
                if (this.value == '') {
                    return;
                }

                var passesValidation = $('#AuditResult').val() == 'True';
                if(!passesValidation) {
                    return;
                }

                var args = {
                    ProviderId: this.value,
                    LoanId: ML.sLId
                };

                var results = gService.Quote.call('GetPolicyTypes', args);

                if (results && results.value) {
                    var policyTypes = JSON.parse(results.value.PolicyTypes);
                    var requestPolicy = results.value.RequestPolicy == 'True';
                    var ownerPolicies = JSON.parse(results.value.OwnerPolicy);
                    var lenderPolicies = JSON.parse(results.value.LenderPolicy);
                    var defaultPolicies = JSON.parse(results.value.DefaultPolicies);

                    requestescrow.toggle(requestPolicy);
                    lenderpolicyrow.toggle(requestPolicy && lenderPolicies.length > 0);
                    ownerpolicyrow.toggle(requestPolicy && ownerPolicies.length > 0);

                    var p = lenderpolicy.parent();
                    var y = ownerpolicy.parent();
                    ownerpolicy.detach();
                    lenderpolicy.detach();


                    lenderpolicy.find('option').remove().end().append('<option value="-1">None</option>');
                    ownerpolicy.find('option').remove().end().append('<option value="-1">None</option>');
                    $.each(lenderPolicies, function(i, v) {
                        lenderpolicy.append($('<option/>', { value: this.Id }).text(this.Name));
                    });
                    $.each(ownerPolicies, function(i, v) {
                        ownerpolicy.append($('<option/>', { value: this.Id }).text(this.Name));
                    });
                    
                    p.prepend(lenderpolicy);
                    y.prepend(ownerpolicy);
                    
                    $.each(defaultPolicies, function(i, v) {
                        var defaultID = v;
                        if(0 != $('#lenderpolicy option[value='+defaultID+']').length)
                        {
                            lenderpolicy.val(defaultID);
                            if(passesValidation) {
                                quoteBtn.prop('disabled', false);
                            }
                        }
                        
                        if(0 != $('#ownerpolicy option[value='+defaultID+']').length)
                        {
                            ownerpolicy.val(defaultID);
                            if(passesValidation) {
                                quoteBtn.prop('disabled', false);
                            }
                        }
                    });
                    
                    if (results.value.SupportsQuote == 'False') {
                        $('#PageHeader').html('Online Title Policy Request');
                        $('#WaitDiv').html('Ordering Title Policy...');
                        quoteBtn.val('Get Policy...');
                        policyDdl.hide();
                        $('#pt').hide();
                        $('#applycc').parent().hide();
                    }
                    else {
                        $('#PageHeader').html('Online Quote Request');
                        $('#WaitDiv').html('Ordering Quote...');
                        quoteBtn.val('Get Quote...');
                        policyDdl.show();
                        $('#pt').show();
                        $('#applycc').parent().show();
                    }
                    $.each(policyTypes, function() {

                        var option = $('<option/>', { value: this.Value }).text(this.Desc);
                        policyDdl.append(option);

                    });
                }
            });
            //for some reason we need this in ie9 compat mode with ie9 mode.
            setTimeout(function() {
                titleDdl.triggerHandler('change');
            }, 0);

            if (passesValidation) {
                //               $('td.error').hide();
            }
            function orderQuote() {
                var borrRelationshipTitleType = [];
                var borrRelationshipTitleOther = [];
                var coBorrRelationshipTitleType = [];
                var coBorrRelationshipTitleOther = [];
                var titleOnlyInfo = [];
                
                var iNumApps = parseInt($('#NumApps').val());
                var iNumTitleBorrowers = parseInt($('#NumTitleBorrowers').val());
                var total = iNumApps + iNumTitleBorrowers;
                for(var i=0; i < iNumApps; i++){
                    borrRelationshipTitleType.push(GetRelationshipTitleType(i, true));
                    borrRelationshipTitleOther.push(GetRelationshipTitleOther(i, true));
                    coBorrRelationshipTitleType.push(GetRelationshipTitleType(i, false));
                    coBorrRelationshipTitleOther.push(GetRelationshipTitleOther(i, false));
                }
                
                for(var i = iNumApps; i < total; i++) {
                    titleOnlyInfo.push({
                        RelationshipTitleType : GetRelationshipTitleType(i, true),
                        RelationshipTitleTypeOtherDesc : GetRelationshipTitleOther(i, true),
                        Id : GetEntryId(i)
                    });
                    
                    
                }
                
                
                var args = {
                    ProviderId: titleDdl.val(),
                    LoanId: ML.sLId,
                    PolicyType: policyDdl.val(),
                    ApplyClosingCost: $('#applycc').prop('checked'),
                    OwnerPolicy: ownerpolicy.val(),
                    LenderPolicy: lenderpolicy.val(),
                    RequestEscrow: $("input:radio[name='requestclosing']:checked").val(),
                    BorrowerRelationshipTitleType: JSON.stringify(borrRelationshipTitleType),
                    BorrowerRelationshipTitleOther: JSON.stringify(borrRelationshipTitleOther),
                    CoBorrowerRelationshipTitleType: JSON.stringify(coBorrRelationshipTitleType),
                    CoBorrowerRelationshipTitleOther: JSON.stringify(coBorrRelationshipTitleOther),
                    TitleBorrowerInfo : JSON.stringify(titleOnlyInfo)
                };
                
                var results = gService.Quote.call('OrderQuote', args);

                $('div.loading').hide();

                quoteBtn.prop('disabled', false).val('Get Policy...');
                if (results.error) {
                    error.text(results.UserMessage);
                    return;
                }

                if (results.value.Error) {
                    error.text(results.value.Error);
                    return;
                }
                else if (results.value.Msg) {
                    alert(results.value.Msg);
                    if ($('#l').val() == '1') {
                        window.location.reload(true);
                    }
                }
                else {
                    if ($('#l').val() == '1') {
                        window.location.reload(true);
                    }
                    else {
                        window.location = VRoot + '/newlos/Services/Title/QuoteResult.aspx?loanid=' + ML.sLId;
                    }
                }
            }
            quoteBtn.click(function() {
                if (this.disabled) {
                    return;
                }
                error.empty();
                this.disabled = true;
                this.value = 'Ordering...';
                $('div.loading').show();
                window.setTimeout(orderQuote, 2);
            });
            
            ToggleRelationshipTitleOther();
        });
    </script>
    </form>
</body>
</html>
