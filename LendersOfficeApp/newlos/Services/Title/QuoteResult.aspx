﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuoteResult.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.Title.QuoteResult" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Quote Result</title>
    <style type="text/css">
        body { background-color: gainsboro; }
        h1 { margin: 0 0 5px 0; padding: 0;}
        h2 { font-size: 12px; padding: 0; margin: 0 0 5px 0; }
        ul { list-style: none; margin: 0px; padding: 0;  margin-left: 10px;}
        ul li { margin: 0; padding: 2px; }
        .bod { margin-left: 5px; }
        .fll { float:left;  }
        .na { font-weight: bold; color: Red; }
    </style>
</head>
<body>
    <h4 class="page-header">Online Quote Result</h4>
    <form id="form1" runat="server">
    <div>
        <div class="bod">
        
        <asp:PlaceHolder runat="server" ID="ErrorPanel">
            There is no Quote request on file.
        </asp:PlaceHolder>
        <asp:PlaceHolder  runat ="server" ID="MainPanel">
            <div class="fll" style="width: 300px;">
                <ul>
                    <li>Quote received
                        <ml:EncodedLiteral runat="server" ID="QuoteReceivedD"></ml:EncodedLiteral></li>
                    <li>Quote ID
                        <ml:EncodedLiteral runat="server" ID="QuoteID"></ml:EncodedLiteral></li>
                    <li>Recording Office
                        <ml:EncodedLiteral runat="server" ID="RecordingOffice"></ml:EncodedLiteral>
                    </li>
                    <li>Order Placed
                        <ml:EncodedLiteral runat="server" ID="OrderedPlacedD"></ml:EncodedLiteral></li>
                    <li>Order Id
                        <ml:EncodedLiteral runat="server" ID="OrderId"></ml:EncodedLiteral>
                    </li>
                </ul>
            </div>
            <div class="fll" style="width: 170px; text-align: right">
                <input type="button" value="Apply closing costs to loan file" id="applycost" />
                <br />
                <br />
                <input type="button" value="Place Order ..."  id="orderpolicy"/>
            </div>
            <br style="clear:both;" />
            <h2>HUD-1 Details</h2>
            <asp:Repeater runat="server" ID="Fees" OnItemDataBound="Fees_OnItemDataBound">
                <HeaderTemplate>
                    <table>
                    <tbody>
                </HeaderTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
                <ItemTemplate>
                    <tr class='<%# AspxTools.HtmlString( Container.ItemIndex % 2 == 0 ? "GridItem" : "GridAlternatingItem")  %>'>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="GfeNumber"></ml:EncodedLiteral>
                    </td>
                        <td>
                            <ml:EncodedLiteral runat="server" ID="LineNumber"></ml:EncodedLiteral>
                        </td>
                        <td>
                            <ml:EncodedLiteral runat="server" ID="Description"></ml:EncodedLiteral>
                        </td>
                        <td>
                            <ml:EncodedLabel runat="server" ID="Fee"></ml:EncodedLabel>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <br />
            <a href="#" class="anc" runat="server" id="detailsanchor">order details and warnings</a>
            <asp:Repeater runat="server" ID="Details" OnItemDataBound="Details_OnItemDataBound">
                <HeaderTemplate>
                    <ul style="display: none;" id="detailsul">
                </HeaderTemplate>
                <FooterTemplate>
                </ul>
                </FooterTemplate>
                <ItemTemplate>
                <li>
                    <ml:PassthroughLiteral runat="server" ID="Detail"></ml:PassthroughLiteral>
                </li>
                </ItemTemplate>
            </asp:Repeater>
        </asp:PlaceHolder>
        
    
        </div>
    </div>
    <script type="text/javascript">
        $(function(){
            $('a.anc').click(function(){
                $('#detailsul').toggle();
            });
            $('#applycost').click(function(){
                var args = {
                    LoanId : ML.sLId
                };
                
               var res = gService.Quote.call('ApplyQuote', args);
               if (res.error){
                alert(res.UserMessage);
               }
               else {
                alert('Closing cost applied to loan file.');
               }
            });
            $('#orderpolicy').click(function(){
               alert('to do ');
               
            });
        });
    </script>
    </form>
</body>
</html>
