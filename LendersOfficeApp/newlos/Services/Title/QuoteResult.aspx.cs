﻿// 4/3/2015 tj - This page is out of use with our current (sole) title vendor, First American.
// While refactoring for the CFPB 2015 update, this has been skipped as not necessary.  As such,
// future use of this page should update the closing costs to use the new model, rather than the
// GFE versions of the fees.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.TitleProvider;
using DataAccess;
using CommonProjectLib.Common.Lib;
using LendersOffice.Common;
using System.Xml;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.newlos.Services.Title
{
    public partial class QuoteResult : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterService("Quote", "/newlos/Services/Title/QuoteServices.aspx");

            EnableJqueryMigrate = false;

            QuoteLog log = TitleService.GetQuoteOrderLog(BrokerID, LoanID);

            if (log == null)
            {
                ErrorPanel.Visible = true;
                MainPanel.Visible = false;
                return;
            }

            if (RequestHelper.GetSafeQueryString("XMLDebug") == "1")
            {
                Response.ContentType = "text/xml";
                using (XmlWriter writer = XmlWriter.Create(Response.OutputStream))
                {
                    log.WriteXml(writer);
                }
                Response.End();
                return;
            }

            ErrorPanel.Visible = false;

            QuoteReceivedD.Text = Tools.GetDateTimeDescription(log.QuoteOrderedDate);
            QuoteID.Text = log.QuoteID;
            RecordingOffice.Text = log.RecordingOffice.Attention;

            if (log.PolicyOrderedD.HasValue)
            {
                OrderedPlacedD.Text = Tools.GetDateTimeDescription(log.PolicyOrderedD.Value);
            }

            OrderId.Text = log.PolicyID;

            List<Tuple<string, string, string, decimal>> availablefees = new List<Tuple<string,string, string, decimal>>();

            #region populate fees list

            decimal fee = log.Results.sEscrowF;
            if (!log.Results.IsEscrowFSet || !log.WereClosingCostRequested)
            {
                fee = -1;
            }
            availablefees.Add(Tuple.Create("B4", "1102", "Settlement or closing fee", fee));
            

            if (log.Results.IsNotaryFSet)
            {
                availablefees.Add(Tuple.Create("B4", "1110", "Notary fees", log.Results.sNotaryF));
            }

            if (log.Results.IsOwnerTitleInsFSet)
            {
                availablefees.Add(Tuple.Create("B5", "1103", "Owner's title insurance", log.Results.sOwnerTitleInsF));
            }
            if (log.Results.IsTitleInsFSet)
            {
                availablefees.Add(Tuple.Create("B4", "1104", "Lender's title insurance", log.Results.sTitleInsF));
            }
            if (log.Results.IsRecFSet)
            {
                availablefees.Add(Tuple.Create("B7", "1201", "Government Recording Charges", log.Results.sRecF));
            }
            if (log.Results.IsCountyRtcSet)
            {
                availablefees.Add(Tuple.Create("B8", "1204", "City/County tax/stamps", log.Results.sCountyRtc));
            }
            if (log.Results.IsStateRtcSet)
            {
                availablefees.Add(Tuple.Create("B8", "1205", "State tax/stamps", log.Results.sStateRtc));
            }

            availablefees = availablefees.OrderBy(p => p.Item1).ThenBy(p => p.Item2).ToList();
            #endregion

            Fees.DataSource = availablefees;
            Fees.DataBind();

            Details.DataSource = log.Details;
            Details.DataBind();

        }
        protected void Details_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            QuoteLog.QuoteDetail detail = (QuoteLog.QuoteDetail)args.Item.DataItem;
            PassthroughLiteral detaillable = (PassthroughLiteral)args.Item.FindControl("Detail");
            detaillable.Text = "";
            if (detail.Classification == QuoteLog.E_QuoteDetailType.WARNING)
            {
                detaillable.Text = "WARNING: ";
            }

            detaillable.Text += detail.Detail.Replace("\n", "<br/>").Replace("\t", "&nbsp;&nbsp;&nbsp;");

        }
        protected void Fees_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            var data = (Tuple<string, string, string, decimal>)args.Item.DataItem;

            LosConvert c = new LosConvert();
            EncodedLiteral LineNumber = (EncodedLiteral)args.Item.FindControl("LineNumber");
            EncodedLiteral GfeNumber = (EncodedLiteral)args.Item.FindControl("GfeNumber");
            EncodedLiteral Description = (EncodedLiteral)args.Item.FindControl("Description");
            EncodedLabel Fee = (EncodedLabel)args.Item.FindControl("Fee");
            GfeNumber.Text = data.Item1;
            LineNumber.Text = data.Item2;
            Description.Text = data.Item3;
            if (data.Item4 >= 0)
            {
                Fee.Text = c.ToMoneyString(data.Item4, FormatDirection.ToRep);
            }
            else
            {
                Fee.Text = "Closing cost not available";
                Fee.CssClass = "na";
            }
        }
    }
}
