﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.Constants;
using DataAccess;
using LendersOffice.ObjLib.TitleProvider;
using LendersOffice.ObjLib.TitleProvider.FirstAmerican;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Services.Title
{
    public sealed class TitleBorrowerRelationshipInfo
    {
        public Guid Id { get; set; }
        public E_aRelationshipTitleT RelationshipTitleType { get; set; }
        public string RelationshipTitleTypeOtherDesc { get; set; }
    }
    public partial class QuoteServices : BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "OrderQuote":
                    OrderQuote();
                    break;
                case "GetPolicyTypes":
                    GetPolicyTypes();
                    break;
                case "ApplyQuote":
                    ApplyQuote();
                    break;
                case "OrderPolicy":
                    break;
            }
        }

        private void ApplyQuote()
        {
            Guid loanId = GetGuid("LoanId");
            TitleService.ApplySavedQuote(PrincipalFactory.CurrentPrincipal.BrokerId, loanId);
        }


        private void OrderQuote()
        {
            AbstractUserPrincipal currentUser = PrincipalFactory.CurrentPrincipal;
            if (!currentUser.HasPermission(Permission.AllowOrderingTitleServices))
            {
                SetResult("Error", ErrorMessages.Title.InsufficientTitlePermission);
                return;
            }

            TitleService titleService = GetService();
            Guid loanId = GetGuid("LoanId");
            
            CPageData loanData = CPageData.CreateUsingSmartDependency(loanId, typeof(QuoteServices));
            loanData.InitSave(ConstAppDavid.SkipVersionCheck);

            IEnumerable<PolicyInfo> policies;
            if (titleService is FirstAmericanServiceV3)
            {
                FirstAmericanServiceV3 firstAmService = (FirstAmericanServiceV3)titleService;
                policies = firstAmService.GetPoliciesForTitleOrder(loanData.sSpCity, loanData.sSpCounty, loanData.sSpState, loanData.sLPurposeT, loanData.sLienPosT);
            }
            else
            {
                policies = titleService.GetPolicies(loanData.sLPurposeT, loanData.sSpState);
            }

            int iOwnerPolicyID = GetInt("OwnerPolicy");
            int iLenderPolicyID = GetInt("LenderPolicy");
            bool bEscrowRequested = GetBool("RequestEscrow");

            if (titleService.SupportsQuote)
            {
                bool applyClosingCost = GetBool("ApplyClosingCost");
                E_cTitleInsurancePolicyT policyType = (E_cTitleInsurancePolicyT)GetInt("PolicyType");

                titleService.OrderQuote(loanId, policyType);
                if (applyClosingCost)
                {
                    TitleService.ApplySavedQuote(PrincipalFactory.CurrentPrincipal.BrokerId, loanId);
                }
            }
            else if (!string.IsNullOrEmpty(loanData.sPrelimRprtOd_rep))
            {
                SetResult("Error", ErrorMessages.Title.TitlePolicyAlreadyOrdered);
            }
            else
            {
                List<TitleBorrowerRelationshipInfo> titleBorrower = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<TitleBorrowerRelationshipInfo>>(GetString("TitleBorrowerInfo"));
                var titleBorrowers = loanData.sTitleBorrowers;

                foreach (var entry in titleBorrower)
                {
                    TitleBorrower b = titleBorrowers.FirstOrDefault(p => p.Id == entry.Id);
                    if (b != null)
                    {
                        b.RelationshipTitleT = entry.RelationshipTitleType;
                        b.RelationshipTitleTOtherDesc = entry.RelationshipTitleTypeOtherDesc;
                    }
                }
                loanData.sTitleBorrowers = titleBorrowers;
                
                List<string> borrRelationshipTitleType = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("BorrowerRelationshipTitleType"));
                List<string> borrRelationshipTitleOther = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("BorrowerRelationshipTitleOther"));
                List<string> coBorrRelationshipTitleType = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("CoBorrowerRelationshipTitleType"));
                List<string> coBorrRelationshipTitleOther = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("CoBorrowerRelationshipTitleOther"));

                bool bRelationshipsSaved = false;
                try
                {
                    bRelationshipsSaved = SaveTitleRelationships(loanData, borrRelationshipTitleType, borrRelationshipTitleOther, coBorrRelationshipTitleType, coBorrRelationshipTitleOther);
                }
                catch (CBaseException exc)
                {
                    Tools.LogError(exc);
                    SetResult("Error", exc.UserMessage);
                }

                if (bRelationshipsSaved)
                {
                    titleService.OrderPolicy(loanId, GetPolicyName(policies, iOwnerPolicyID), GetPolicyName(policies, iLenderPolicyID), bEscrowRequested);

                    if (titleService.TitleServiceResult != null)
                    {
                        if (titleService.TitleServiceResult.Success)
                        {
                            SetResult("Msg", titleService.TitleServiceResult.ResultMessage);
                            SavePrelimReportOrderedDate(loanId);
                        }
                        else
                        {
                            SetResult("Error", titleService.TitleServiceResult.ResultMessage);
                        }
                    }
                    else
                    {
                        SetResult("Msg", "Title order sent");
                    }
                }
            }
        }

        private string GetPolicyName(IEnumerable<PolicyInfo> policies, int iPolicyID)
        {
            string sPolicyName = string.Empty;
            if (iPolicyID >= 0)
            {
                foreach (PolicyInfo policy in policies)
                {
                    if (string.Compare(policy.Id, iPolicyID.ToString(), StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        sPolicyName = policy.Name;
                        break;
                    }
                }
            }
            return sPolicyName;
        }

        private void GetPolicyTypes()
        {
            Guid loanId = GetGuid("LoanId");
            TitleService titleService = GetService();
            CPageData loanData = CPageData.CreateUsingSmartDependency(loanId, typeof(QuoteServices));
            loanData.InitLoad();
            var p = from policy in titleService.GetAvailablePolicyTypes(loanData.sLPurposeT, loanData.sSpState)
                    select new
                    {
                        Value = (int)policy,
                        Desc = policy.ToString()
                    };

            IEnumerable<PolicyInfo> policies;
            IEnumerable<int> defaultPolicies;
            if (titleService is FirstAmericanServiceV3)
            {
                FirstAmericanServiceV3 firstAmService = (FirstAmericanServiceV3)titleService;
                policies = firstAmService.GetPoliciesForTitleOrder(loanData.sSpCity, loanData.sSpCounty, loanData.sSpState, loanData.sLPurposeT, loanData.sLienPosT);
                defaultPolicies = firstAmService.GetIDsOfDefaultPolicies(loanData.sSpCity, loanData.sSpCounty, loanData.sSpState, loanData.sLPurposeT, loanData.sLienPosT);
            }
            else
            {
                policies = titleService.GetPolicies(loanData.sLPurposeT, loanData.sSpState);
                defaultPolicies = new List<int>();
            }

            string data = ObsoleteSerializationHelper.JavascriptJsonSerialize(p);
            string ownerPolicyJson = ObsoleteSerializationHelper.JavascriptJsonSerialize(policies.Where(x => x.PolicyT == E_cTitleInsurancePolicyT.Owner));
            string lenderPolicyJson = ObsoleteSerializationHelper.JavascriptJsonSerialize(policies.Where(x => x.PolicyT == E_cTitleInsurancePolicyT.Lender));
            string sDefaultPoliciesJson = ObsoleteSerializationHelper.JavascriptJsonSerialize(defaultPolicies);

            SetResult("PolicyTypes", data);
            SetResult("OwnerPolicy", ownerPolicyJson);
            SetResult("LenderPolicy", lenderPolicyJson);
            SetResult("DefaultPolicies", sDefaultPoliciesJson);
            SetResult("SupportsQuote", titleService.SupportsQuote);
            SetResult("RequestPolicy", titleService.RequestPolicy);
        }

        private TitleService GetService()
        {
            int providerId = GetInt("ProviderId");
            Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            return TitleProvider.GetService(brokerId, providerId);
        }

        private void SavePrelimReportOrderedDate(Guid loanId)
        {
            CPageData loanData = CPageData.CreateUsingSmartDependency(loanId, typeof(QuoteServices));
            loanData.InitSave(ConstAppDavid.SkipVersionCheck);
            loanData.sPrelimRprtOd_rep = DateTime.Now.ToString();

            loanData.Save();
        }

        private bool SaveTitleRelationships(CPageData loanData, List<string> borrRelationshipTitleType, List<string> borrRelationshipTitleOther, List<string> coBorrRelationshipTitleType, List<string> coBorrRelationshipTitleOther)
        {
            int iNumApps = loanData.nApps;
            int iNumRelationships = borrRelationshipTitleType.Count;

            if ((iNumApps != iNumRelationships) || (iNumRelationships != coBorrRelationshipTitleType.Count) || (iNumRelationships != borrRelationshipTitleOther.Count) || (iNumRelationships != coBorrRelationshipTitleOther.Count))
            {
                throw new CBaseException(ErrorMessages.Title.AppRelationshipMismatch, ErrorMessages.Title.AppRelationshipMismatchDevMsg);
            }

            for (int i = 0; i < iNumApps; i++)
            {
                CAppData currentApp = loanData.GetAppData(i);
                if (!currentApp.aBTypeT.EqualsOneOf(E_aTypeT.NonTitleSpouse, E_aTypeT.CurrentTitleOnly))
                {
                    currentApp.aBRelationshipTitleT = ConvertBorrowerRelationshipTitle(borrRelationshipTitleType[i].TrimWhitespaceAndBOM());
                    if (currentApp.aBRelationshipTitleT == E_aRelationshipTitleT.LeaveBlank)
                    {
                        throw new CBaseException(ErrorMessages.Title.RelationshipTitleTypesAreRequired, ErrorMessages.Title.RelationshipTitleTypesAreRequired);
                    }
                    currentApp.aBRelationshipTitleOtherDesc = borrRelationshipTitleOther[i].TrimWhitespaceAndBOM();
                }

                if ((!currentApp.aCTypeT.EqualsOneOf(E_aTypeT.NonTitleSpouse, E_aTypeT.CurrentTitleOnly)) && (currentApp.aCIsValidNameSsn))
                {
                    currentApp.aCRelationshipTitleT = ConvertBorrowerRelationshipTitle(coBorrRelationshipTitleType[i].TrimWhitespaceAndBOM());
                    if (currentApp.aCRelationshipTitleT == E_aRelationshipTitleT.LeaveBlank)
                    {
                        throw new CBaseException(ErrorMessages.Title.RelationshipTitleTypesAreRequired, ErrorMessages.Title.RelationshipTitleTypesAreRequired);
                    }
                    currentApp.aCRelationshipTitleOtherDesc = coBorrRelationshipTitleOther[i].TrimWhitespaceAndBOM();
                }
            }

            loanData.Save();
            return true;
        }

        private E_aRelationshipTitleT ConvertBorrowerRelationshipTitle(string sRelationshipTitleType)
        {
            return (string.IsNullOrEmpty(sRelationshipTitleType)) ? E_aRelationshipTitleT.LeaveBlank : (E_aRelationshipTitleT)Enum.Parse(typeof(E_aRelationshipTitleT), sRelationshipTitleType);
        }
    }
}
