using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.CreditReport;

namespace LendersOfficeApp.newlos.Services
{
	/// <summary>
	/// Summary description for ViewCreditFrame.
	/// </summary>
	public partial class ViewCreditFrame : BasePage
	{
        private CreditReportTypes type;
        protected CreditReportTypes Type { get { return type; } }

		protected void PageLoad(object sender, System.EventArgs e)
		{
            // Set type.
            if (Enum.TryParse(RequestHelper.GetSafeQueryString("type"), true, out type))
            {
                return;
            }

            // If type not specified then use last report type
            Guid applicationID = RequestHelper.GetGuid("applicationid", Guid.Empty);
            Guid loanID = RequestHelper.GetGuid("loanid", Guid.Empty);

            CPageData dataLoan = new CCreditReportViewData(loanID);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(applicationID);

            string fileType = string.Empty;
            SqlParameter[] parameters = { new SqlParameter("@ApplicationID", applicationID) };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(LendersOffice.Security.PrincipalFactory.CurrentPrincipal.BrokerId, "GetLastCreditReportType", parameters))
            {
                if (reader.Read())
                {
                    fileType = (string)reader["FileType"];
                }
            }

            if (string.IsNullOrWhiteSpace(fileType) || fileType == "CreditReport")
            {
                type = CreditReportTypes.Primary;
            }
            else
            {
                type = CreditReportTypes.Lqi;
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
