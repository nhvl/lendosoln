﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransferToPicker.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.TransferToPicker" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Transfer To</title>
    <style type="text/css">
        .row { height: 30px; }
        /* Override padding & indent so width can be 100% without horizontal scrollbar */
        .headerOverride { padding-left: 0px !important; }
        .indent { position: relative; left: 5px; }
        .notification { font-weight: bold; text-align: center; display: block; }
    </style>
    <script type="text/javascript">
        function f_selectTransferToInvestor(desc, code) {
            var arg = {};
            arg.description = desc;
            arg.code = code;
            onClosePopup(arg);
        }

        function _init() {
            resizeForIE6And7(380, 400);
            var val = document.getElementById('ErrorMessage').value;
            if (val !== '') {
                alert(val);
                onClosePopup();
            }
        }
    </script>
</head>
<body bgcolor="gainsboro">
    <h4 class="page-header">Transfer To</h4>
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="m_transferToGrid" runat="server" AutoGenerateColumns="false" EnableViewState="false" Width="100%" ShowHeader="false" OnRowDataBound="TransferTo_RowDataBound">
            <AlternatingRowStyle CssClass="GridAlternatingItem row" />
            <RowStyle CssClass="GridItem row" />
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <a href="#" id="Link" runat="server"></a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <span id="NotificationMessage" class="notification" runat="server" />
        <div style="text-align: center; padding: 10px;">
            <input value="Cancel" type="button" onclick="onClosePopup();" />
        </div>
        <asp:HiddenField ID="ErrorMessage" runat="server"/>
    </div>
    </form>
</body>
</html>
