﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.Services
{
    public partial class ViewEarlyCheckResult : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DisplayCopyRight = false;
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ViewEarlyCheckResult));
            dataLoan.InitLoad();

            string html = dataLoan.sFannieMaeEarlyCheckResultsHtml;

            if (string.IsNullOrEmpty(html) == false)
            {
                Response.Clear();
                byte[] buffer = System.Text.UTF8Encoding.UTF8.GetBytes(html);
                Response.OutputStream.Write(buffer, 0, buffer.Length);
                Response.Flush();
                Response.End();
            }
        }
    }
}
