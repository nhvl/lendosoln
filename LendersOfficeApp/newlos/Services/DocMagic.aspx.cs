namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using global::DocMagic.DsiDocRequest;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Common.SerializationTypes;

    public partial class DocMagic : LendersOfficeApp.newlos.BaseLoanPage
    {
        private CPageData m_dataLoan;

        protected void PageLoad(object sender, System.EventArgs e)
        {
            m_dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(DocMagic));
            m_dataLoan.InitLoad();

            if (m_dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.GFE &&
                m_dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy)
            {
                var mismo33String = "f_export33('{0}', '{1}');";

                Export33Btn.Attributes["onclick"] = string.Format(
                    mismo33String,
                    LoanID,
                    ApplicationID);
            }
            else
            {
                Export33Btn.Visible = false;
            }

            if (m_dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                string errorMessage = null;

                if (m_dataLoan.sHasLoanEstimateArchiveInPendingStatus)
                {
                    this.RegisterJsGlobalVariables("sHasLoanEstimateArchiveInPendingStatus", true);
                    var archive = m_dataLoan.sLoanEstimateArchiveInPendingStatus;
                    errorMessage = ErrorMessages.ArchiveError.UnableToGenerateDocumentsBecauseOfPendingArchive(
                        archive.DateArchived,
                        archive.Status,
                        userCanDetermineStatus: true);
                }
                else if (m_dataLoan.sHasClosingCostArchiveInUnknownStatus)
                {
                    this.RegisterJsGlobalVariables("sHasClosingCostArchiveInUnknownStatus", true);
                    this.RegisterJsGlobalVariables("HasBothCDAndLEArchiveInUnknownStatus", m_dataLoan.sHasBothCDAndLEArchiveInUnknownStatus);
                    var archive = m_dataLoan.sClosingCostArchiveInUnknownStatus;
                    errorMessage = ErrorMessages.ArchiveError.UnableToGenerateDocumentsBecuaseOfUnknownArchive(
                        archive.DateArchived,
                        archive.ClosingCostArchiveType,
                        userCanDetermineStatus: true);
                }

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    this.ArchiveStatusPopupMessage.InnerText = errorMessage;
                }
            }
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            if (E_BrokerBillingVersion.PerTransaction == Broker.BillingVersion && !Broker.IsEnablePTMDocMagicOnlineInterface)
            {
                string userMsg = "You do not have permission to view this page.";
                string devMsg = "user attempted to view DocMagic page but they are on PTM billing and the broker has not allowed it.";
                throw new CBaseException(userMsg, devMsg);
            }

            this.PageID = "DocMagic";
            this.PageTitle = "DocMagic";
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("ArchiveStatusPopup.js");
            this.EnableJqueryMigrate = false;
        }

        protected override void LoadData()
        {
            bool hasWebsheetNumber = m_dataLoan.sDocMagicFileId.TrimWhitespaceAndBOM() != "";
            HasWebsheetNumber.Value = hasWebsheetNumber.ToString();

            CanReadCloser.Value = BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowCloserRead).ToString();
            CanWriteCloser.Value = BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowCloserWrite).ToString();

            bool use2015DataLayer = m_dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy;

            Use2015DataLayer.Value = use2015DataLayer.ToString();

            if (use2015DataLayer)
            {
                SettlementChargesExportSourceDiv.Visible = false;
                CloserWarning.Visible = false;
            }

            Tools.SetDropDownListValue(sSettlementChargesExportSource, m_dataLoan.sSettlementChargesExportSource);
            sIncludeGfeDataForDocMagicComparison.Checked = m_dataLoan.sIncludeGfeDataForDocMagicComparison;

            if (BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowCloserWrite) == false)
            {
                sIncludeGfeDataForDocMagicComparison.Enabled = false;          //they cannot change these fields if they do not have closer write 
                sSettlementChargesExportSource.Enabled = false;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion
    }
}