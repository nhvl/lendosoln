﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="FHATotalDataAudit.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.FHATotalDataAudit" EnableEventValidation="false" EnableViewState="false" EnableViewStateMac="false" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess"%>
<%@ Import Namespace="LendersOfficeApp.newlos.Services" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>FHA Total Data Audit</title>
    <style type="text/css" >
        body { background-color: gainsboro; font-weight: bold; width: 100%;}
        h1, h2, h3 { margin: 0; padding: 0; }
        h2,h3 { font-size: 11px; }
        h2 { padding-left: 10px;}
        h3 { padding-left: 15px; }
        .Fields { border-collapse: collapse; background-color: White; margin-left: 20px; width: 96%;  table-layout: fixed;}
        .Fields tr td { padding-left: 5px; }
        .alt { background-color: #DAEEF3; }
        .nav { width:100%;  text-align: center; margin-top: 10px;}
        .nav input { margin: 0 auto;   }
        .Column1 { width: 200px; }
        .Column2 { width: 180px; }
        .Column3 { color: Red; }
        td { vertical-align: text-top; } 
        .blockAnchor { display: block;  width: 100%;  text-align: center; padding-top: 5px; padding-bottom: 5px; }
        .nav { display: block; width: 100%; margin-top: 10px; }
    </style>
</head>
<body onload="">
    <script type="text/javascript">
        var loanPageUrl = 'LoanInfo.aspx';
        <% if(IsEnableBigLoanPage) { %>
        loanPageUrl = 'BigLoanInfo.aspx';
        <% } %>
        function _init() {
                FHATotalDataAudit.onload();
        }
    </script>
    <form id="form1" runat="server">
    <div id="mainbody">
        <h1 class="MainRightHeader">FHA Total Data Audit</h1>
        <h2 class="LoanFormHeader">Borrower Info</h2>
        <asp:Repeater runat="server" ID="Applications" OnItemDataBound="Applications_OnItemDataBound">
            <ItemTemplate>
                <h3>Application <ml:EncodedLiteral runat="server" ID="AppNumber" ></ml:EncodedLiteral></h3>
                <table  class="Fields">
                    <tr>
                        <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('BorrowerInfo.aspx', <%# AspxTools.JsString( ((CAppData) Container.DataItem ).aAppId ) %>);">Borrower</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerFullName" ></ml:EncodedLiteral></td>
                        <td class="Column3"></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('BorrowerInfo.aspx', <%# AspxTools.JsString( ((CAppData) Container.DataItem ).aAppId ) %>);">Borrower SSN</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerSsn"  ></ml:EncodedLiteral></td>
                        <td class="Column3" ><ml:EncodedLiteral runat="server" ID="aBSsnTotalScorecardWarning" > </ml:EncodedLiteral></td>
                    </tr>
                    <tr>
                        <td class="Column1" ><a href="#" onclick="FHATotalDataAudit.JumpTo('Forms/Loan1003.aspx', <%# AspxTools.JsString( ((CAppData) Container.DataItem ).aAppId ) %>, '&pg=2');">Borrower Citizenship</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerCititzenshipStatus"  ></ml:EncodedLiteral></td>
                        <td  class="Column3"><ml:EncodedLiteral runat="server" ID="aBDecResidencyTotalScorecardWarning"  ></ml:EncodedLiteral> </td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('BorrowerInfo.aspx', <%# AspxTools.JsString( ((CAppData) Container.DataItem ).aAppId ) %>, '&pg=1');" >Borrower Position/Title</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerTitle"  ></ml:EncodedLiteral></td>
                        <td  class="Column3"><ml:EncodedLiteral runat="server" ID="aBEmplrJobTitleTotalScorecardWarning"  ></ml:EncodedLiteral> </td>
                    </tr>
                    
                    <tr>
                        <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('Worksheets/CreditScores.aspx', <%# AspxTools.JsString( ((CAppData) Container.DataItem ).aAppId ) %>);">Borrower Credit Scores</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerCreditScores" ></ml:EncodedLiteral></td>
                        <td class="Column3"></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" onclick="FHATotalDataAudit.LoadDeclarations(<%# AspxTools.JsString( ((CAppData) Container.DataItem ).aAppId ) %>);">Borrower intends to occupy the property as primary residence</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server" ID="BorrowerOccupancy" ></ml:EncodedLiteral></td>
                        <td class="Column3"><ml:EncodedLiteral runat="server" ID="aBDecOccTotalScorecardWarning" ></ml:EncodedLiteral></td>
                    </tr>
                    <asp:PlaceHolder runat="server" id="NonPurchaseCoborrowerFields" Visible="false">
                        <tr>
                            <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('BorrowerInfo.aspx', <%# AspxTools.JsString( ((CAppData) Container.DataItem ).aAppId ) %>);">Co-borrower</a></td>
                            <td class="Column2"><ml:EncodedLiteral runat="server" ID="NonPurchaseCoborrowerName" ></ml:EncodedLiteral> </td>
                            <td>Excluded from TOTAL due to <ml:EncodedLiteral runat="server" ID="NonPurchaseCoborrowerType" ></ml:EncodedLiteral> borrower type.</td>
                        </tr>                    
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" id="CoborrowerFields" Visible="False">
                    <tr>
                        <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('BorrowerInfo.aspx', <%# AspxTools.JsString( ((CAppData) Container.DataItem ).aAppId ) %>);">Co-borrower</a></td>
                        <td class="Column2"><ml:EncodedLiteral runat="server" ID="CoborrowerName" ></ml:EncodedLiteral> </td>
                        <td class="Column3"></td>
                    </tr>
                    <tr>
                        <td class="Column1" onclick="FHATotalDataAudit.JumpTo('BorrowerInfo.aspx', <%# AspxTools.JsString( ((CAppData) Container.DataItem ).aAppId ) %>);"><a href="#">Co-borrower SSN</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server" ID="CoborrowerSsn" ></ml:EncodedLiteral></td>
                        <td class="Column3" ><ml:EncodedLiteral runat="server" ID="aCSsnTotalScorecardWarning" > </ml:EncodedLiteral></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('Forms/Loan1003.aspx', <%# AspxTools.JsString( ((CAppData) Container.DataItem ).aAppId ) %>, '&pg=2');">Co-borrower Citizenship</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server" ID="CoborrowerCitizenshipStatus" ></ml:EncodedLiteral></td>
                        <td class="Column3" ><ml:EncodedLiteral runat="server" ID="aCDecResidencyTotalScorecardWarning" > </ml:EncodedLiteral></td>   
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('BorrowerInfo.aspx', <%# AspxTools.JsString( ((CAppData) Container.DataItem ).aAppId ) %>, '&pg=2');">Co-borrower Position/Title</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server"  ID="CoborrowerTitle"></ml:EncodedLiteral></td>
                        <td  class="Column3"><ml:EncodedLiteral runat="server"  ID="aCEmplrJobTitleTotalScorecardWarning" ></ml:EncodedLiteral> </td>
                    </tr>
                    <tr>
                        <td  class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('Worksheets/CreditScores.aspx', <%# AspxTools.JsString( ((CAppData) Container.DataItem ).aAppId ) %>);">Co-borrower Credit Scores</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server"  ID="CoborrowerScores"></ml:EncodedLiteral></td>
                        <td class="Column3"></td>
                    </tr>
                    <tr>
                        <td  class="Column1"><a href="#" onclick="FHATotalDataAudit.LoadDeclarations(<%# AspxTools.JsString( ((CAppData) Container.DataItem ).aAppId ) %>);">Co-borrower intends to occupy the property as primary residence?</a></td>
                        <td  class="Column2"> <ml:EncodedLiteral runat="server"  ID="CoborrowerOccupancy"></ml:EncodedLiteral></td>
                         <td class="Column3" ><ml:EncodedLiteral runat="server"  ID="aCDecOccTotalScorecardWarning"> </ml:EncodedLiteral></td>   
                    </tr>
    
                    </asp:PlaceHolder>
                    <tr>
                        <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('BorrowerInfo.aspx', <%# AspxTools.JsString( ((CAppData) Container.DataItem ).aAppId ) %>, '&pg=3');">Monthly Income</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server"  ID="MonthlyIncome"></ml:EncodedLiteral> </td>
                        <td class="Column3"></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('BorrowerInfo.aspx', <%# AspxTools.JsString( ((CAppData) Container.DataItem ).aAppId ) %>, '&pg=5');">Gift Funds</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server"  ID="GiftFunds"></ml:EncodedLiteral> </td>
                        <td class="Column3"></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('BorrowerInfo.aspx', <%# AspxTools.JsString( ((CAppData) Container.DataItem ).aAppId ) %>, '&pg=5');">Liquid Assets</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server"  ID="LiquidAssets"></ml:EncodedLiteral> </td>
                        <td class="Column3"></td>
                    </tr>
                    <tr>
                        <td class="Column1"><a href="#" onclick="redirectToUladPage('Liabilities', <%# AspxTools.JsString( ((CAppData) Container.DataItem ).aAppId ) %>);">Non-Mortgage Payment</a></td>
                        <td  class="Column2"><ml:EncodedLiteral runat="server"  ID="NonMortgagePayment"></ml:EncodedLiteral></td>
                        <td class="Column3"></td>
                    </tr>
                </table>
            
                </ItemTemplate>
        </asp:Repeater>
            <br />  
        <h2 class="LoanFormHeader">Loan Summary</h2>
        <table id="LoanSummary" class="Fields">
            <tr>
                <td  class="Column1" onclick="redirectToUladPage('ApplicationManagement', <%= AspxTools.JsString(ApplicationID) %>);"><a href="#">Primary Borrower</a></td>
                <td  class="Column2"> <ml:EncodedLiteral runat="server"  ID="ls_PrimaryBorrower" ></ml:EncodedLiteral></td>
                <td></td>
            </tr>
            <tr >
                <td  class="Column1" onclick="redirectToUladPage('ApplicationManagement', <%= AspxTools.JsString(ApplicationID) %>);"><a href="#">Number of Applicants</a></td>
                <td  class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_NumberOfTotalApps"></ml:EncodedLiteral></td>
                <td  class="Column3"><ml:EncodedLiteral runat="server"  ID="sBorrCountTotalScorecardWarning"></ml:EncodedLiteral></td>
            </tr>
            <tr>
                <td  class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('Worksheets/CreditScores.aspx', <%= AspxTools.JsString(ApplicationID) %>);">Representative Score</a></td>
                <td  class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_RepresentativeScore"></ml:EncodedLiteral></td>
                <td></td>
            </tr>
            <tr>
                    <td class="Column1">Total Monthly Income</td>
                    <td  class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_TotalMonthlyIncome"></ml:EncodedLiteral></td>
                    <td  class="Column3"><ml:EncodedLiteral runat="server"  ID="sLTotITotalScorecardWarning"></ml:EncodedLiteral></td>
                </tr>
            <tr>
                    <td class="Column1">Total Liquid Assets</td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_TotalLiquidAssets"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
            <tr>
                    <td class="Column1">Total Non-Mortgage Payment</td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_TotalNonMortgagePayments"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" onclick="redirectToUladPage('Liabilities', <%= AspxTools.JsString(ApplicationID) %>);">Mortgage Liabilities</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_MortgageLiabilities"></ml:EncodedLiteral></td>
                    <td class="Column3">&nbsp;</td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('BorrowerInfo.aspx', <%= AspxTools.JsString(ApplicationID) %>, '&pg=6');">REO Properties</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_ReoPropertiesCount"></ml:EncodedLiteral></td>
                    <td class="Column3">&nbsp;</td>
                </tr>
                   <tr>
                    <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('Forms/Loan1003.aspx', <%= AspxTools.JsString(ApplicationID) %>,'&pg=2');">Total Cost</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_TotalCosts"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('Forms/Loan1003.aspx', <%= AspxTools.JsString(ApplicationID) %>,'&pg=2');">Borrower's closing costs paid by seller</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_SellerClosingCostPaid"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>   
                <tr>
                    <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('Forms/Loan1003.aspx', <%= AspxTools.JsString(ApplicationID) %>,'&pg=2');">Borrower paid closing costs</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="sTotalScorecardBorrPaidCc"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>   
                               <tr>
                    <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('Forms/Loan1003.aspx', <%= AspxTools.JsString(ApplicationID) %>,'&pg=2');">Total closing costs</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="sTotEstCcNoDiscnt1003"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('Forms/Loan1003.aspx', <%= AspxTools.JsString(ApplicationID) %>, '&pg=2');">Cash from/to Borrower</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_CashftBorrower"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>         
                
            <tr>
                    <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo(loanPageUrl, <%= AspxTools.JsString(ApplicationID) %>, '&pg=0');">Loan Purpose</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_LoanPurpose" /></td>
                    <td class="Column3"><ml:EncodedLiteral runat="server"  ID="sLPurposeTTotalScorecardWarning" /></td>
                </tr>
                    <tr>
                    <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo(loanPageUrl, <%= AspxTools.JsString(ApplicationID) %>, '&pg=0');">Property Type</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="sGseSpTFriendlyDisplay"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>  
            <tr>
                    <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo(loanPageUrl, <%= AspxTools.JsString(ApplicationID) %>, '&pg=0');">Purchase Price</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_PurchasePrice" /></td>
                    <td class="Column3"><ml:EncodedLiteral runat="server"  ID="sPurchPriceTotalScorecardWarning" /></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo(loanPageUrl, <%= AspxTools.JsString(ApplicationID) %>, '&pg=0');">Appraised Value</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_AppraisedValue"></ml:EncodedLiteral></td>
                    <td class="Column3"><ml:EncodedLiteral runat="server"  ID="sApprValTotalScorecardWarning"></ml:EncodedLiteral></td>

                </tr>
            <tr>
                    <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo(loanPageUrl, <%= AspxTools.JsString(ApplicationID) %>, '&pg=0');">Loan Amount</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_LoanAmount"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
            <tr>
                    <td class="Column1" ><a href="#" onclick="FHATotalDataAudit.JumpTo('LoanInfo.aspx', <%= AspxTools.JsString(ApplicationID) %>, '&pg=1');">Upfront MIP</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_UpfrontMip"></ml:EncodedLiteral></td>
                    <td class="Column3"><ml:EncodedLiteral runat="server"  ID="sFfUfmip1003TotalScorecardWarning"></ml:EncodedLiteral></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo('LoanInfo.aspx', <%= AspxTools.JsString(ApplicationID) %>, '&pg=1');">Upfront MIP Financed</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_UpfrontMipFinanced"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
            <tr>
                    <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo(loanPageUrl, <%= AspxTools.JsString(ApplicationID) %>, '&pg=0');">Total Loan Amount</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_TotalLoanAmount"></ml:EncodedLiteral></td>
                    <td class="Column3"> <ml:EncodedLiteral runat="server"  ID="sLAmtCalcTotalScorecardWarning"></ml:EncodedLiteral></td>
                  
                </tr>
            <tr>
                    <td class="Column1"><a href="#" onclick="FHATotalDataAudit.JumpTo(loanPageUrl, <%= AspxTools.JsString(ApplicationID) %>, '&pg=0');">LTV/CLTV</a></td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="ls_ltvcltv"></ml:EncodedLiteral></td>
                    <td></td>
                </tr>
        </table>
        <br />
              <h2 class="LoanFormHeader">Additional Validation</h2>
              <table class="Fields" > 
                <tr>
                    <td class="Column1">Additional Validation Rules</td>
                    <td class="Column2"><ml:EncodedLiteral runat="server"  ID="AdditionalValidationStatus"></ml:EncodedLiteral></td>
                    <td class="Column3"><ml:EncodedLiteral runat="server"  id="AdditioanlValidationError"></ml:EncodedLiteral></td>
                </tr>
              </table>
                <div class="blockAnchor">
              <a href="javascript:FHATotalDataAudit.ToggleTable(this);" runat="server"  id="TableAnchor">Hide validation messages</a>
         </div>
              <table  id="ExtraValidation" class="Fields" >
              <tbody>
              <asp:Repeater runat="server" id="ExtraEmploymentValidation" OnItemDataBound="ExtraValidation_DataBound" >
                
                <ItemTemplate >
                    <tr class="alt">
                        <td class="Column1">Application <ml:EncodedLiteral runat="server"  ID="AppNumber"></ml:EncodedLiteral> 
                        <br />&nbsp&nbsp&nbsp<a id="ValidationFixLink"  runat="server"></a>
                        </td>
                        <td class="Column2"><ml:EncodedLiteral runat="server"  ID="ValidationStatus"></ml:EncodedLiteral></td>
                        <td class="Column3"><ml:EncodedLiteral runat="server"  ID="ValidationError"></ml:EncodedLiteral></td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr  >
                        <td class="Column1">Application <ml:EncodedLiteral runat="server"  ID="AppNumber"></ml:EncodedLiteral> 
                        <br />&nbsp&nbsp&nbsp<a id="ValidationFixLink"  runat="server"></a>
                        </td>
                        <td class="Column2"><ml:EncodedLiteral runat="server"  ID="ValidationStatus"></ml:EncodedLiteral></td>
                        <td class="Column3"><ml:EncodedLiteral runat="server"  ID="ValidationError"></ml:EncodedLiteral></td>
                    </tr>
                </AlternatingItemTemplate>
              </asp:Repeater>
              </tbody>
              </table>
        <div class="nav" >
        <asp:Button runat="server" ID="ContinueButton"  Text="Continue to FHA TOTAL Submission"  OnClick="Continue_OnClick"/>
        </div>
    </div>
    </form>
    <script type="text/javascript">
        var FHATotalDataAudit = {
            onload : function() {
                var tables = document.getElementsByTagName("table");
                var lastAlt; 
                for (var x = 0; x < tables.length; x++ ) {
                    if ( tables[x].className !== 'Fields' || tables[x].id === 'ExtraValidation') {
                        continue;
                    }  
                    var rows = tables[x].getElementsByTagName("tr");
                    var isAlt = false;
                    for(i = 0; i < rows.length; i++){  
                        if ( rows[i].className === "remain" ) { isAlt = lastAlt; }
                        rows[i].className = isAlt ? 'alt' : '';
                        isAlt = !isAlt;
                    }
                    lastAlt = isAlt;
                }
               if ( <%= AspxTools.JsBool(HideTable) %> ) {
                var x = document.getElementById('ExtraValidation');
                x.style.display = 'none';
               }
            },
            
            ToggleTable : function() {
                var a = document.getElementById('TableAnchor');
                var table = document.getElementById('ExtraValidation');
                if ( table.style.display === 'none' ) { 
                    table.style.display = ''; 
                    a.innerHTML  = 'Hide validation messages';
                } 
                else { 
                    table.style.display = 'none';
                    a.innerHTML  = 'Show validation messages';
                }
            },
            
            JumpTo: function(page, appid, params) {
                parent.info.f_setCurrentApplicationID(appid); <%-- Sometimes the active app doesnt match what the app id is for the url so make sure to switch--%>
                if ( !params ) { params = ''; }
                var url =<%= AspxTools.JsString( VirtualRoot ) %> +'/newlos/' + page + '?loanid='+<%= AspxTools.JsString( LoanID ) %>+'&appid=' + appid + params ;
                document.location.href = url;
            },

            LoadDeclarations: function (appid) {
                if (ML.IsTargeting2019Ulad) {
                    this.JumpTo('Ulad/Declarations.aspx', appid);
                }
                else {
                    this.JumpTo('Forms/Loan1003.aspx', appid, '&pg=2');
                }
            }
        };
    </script>
</body>
</html>
