﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos.Services
{
    public partial class DocumentViewer : BaseLoanPage
    {
#if LQB_NET45
        protected global::System.Web.UI.HtmlControls.HtmlIframe ImpoundAnalysisIFrame;
        protected global::System.Web.UI.HtmlControls.HtmlIframe Section32IFrame;
        protected global::System.Web.UI.HtmlControls.HtmlIframe GfeComparisoIFrame;
        protected global::System.Web.UI.HtmlControls.HtmlIframe AprScheduleIFrame;
#else
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl ImpoundAnalysisIFrame;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl Section32IFrame;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl GfeComparisoIFrame;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl AprScheduleIFrame;
#endif

        protected bool RefreshOpenerOnLoad
        {
            get
            {
                return RequestHelper.GetBool("refreshParent");
            }
        }

        protected bool DisplayDisclosureStatusButtons
        {
            get
            {
                return RequestHelper.GetBool("displayDisclosureStatus");
            }
        }

        protected string LoanEstimateArchiveDate
        {
            get
            {
                return RequestHelper.GetSafeQueryString("loanEstimateArchiveDate");
            }
        }

        protected string ClosingDisclosureArchiveDate
        {
            get
            {
                return RequestHelper.GetSafeQueryString("closingDisclosureArchiveDate");
            }
        }

        protected bool DocumentGenerationCreatedLoanEstimateArchive
        {
            get
            {
                return RequestHelper.GetBool("isLoanEstimate");
            }
        }

        protected bool HasBothCDAndLEArchiveInUnknownStatus
        {
            get
            {
                return RequestHelper.GetBool("hasBothCDAndLEArchiveInUnknownStatus");
            }
        }

        protected bool UnknownLoanEstimateArchiveWasSourcedFromPendingCoC
        {
            get
            {
                return RequestHelper.GetBool("wasSourcedFromPendingCoC");
            }
        }

        protected string HtmlKeySection32
        {
            get
            {
                return RequestHelper.GetSafeQueryString("s");
            }
        }
        protected string HtmlKeyImpoundAnalysis
        {
            get
            {
                return RequestHelper.GetSafeQueryString("i");
            }
        }
        protected string HtmlKeyGfeComparison
        {
            get
            {
                return RequestHelper.GetSafeQueryString("g");
            }
        }
        protected string HtmlKeyApr
        {
            get
            {
                return RequestHelper.GetSafeQueryString("a");
            }
        }

        protected string DocumentKey
        {
            get
            {

                return RequestHelper.GetSafeQueryString("d");
            }
        }

        protected string DocUrl
        {
            get
            {
                return "DocumentViewer.aspx?loanid=" + LoanID + "&Doc=" + SelectedDoc;
            }
        }

        protected string SelectedDoc
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.RegisterCSS("jquery-ui-1.11.custom.css");
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterCSS("pdfeditor.css");
            this.RegisterJsScript("PdfEditor.js");
            this.RegisterCSS("Tabs.css");
            this.RegisterJsScript("LQBPrintFix.js");

            this.DisclosureStatusButtons.Visible = this.DisplayDisclosureStatusButtons;
            this.ResubmitDiv.Visible = false;

            if (this.DisplayDisclosureStatusButtons)
            {
                this.RegisterJsGlobalVariables("sHasBothCDAndLEArchiveInUnknownStatus", this.HasBothCDAndLEArchiveInUnknownStatus);

                if (this.DocumentGenerationCreatedLoanEstimateArchive)
                {
                    this.RegisterJsGlobalVariables("sHasLoanEstimateArchiveInUnknownStatus", true);
                    this.InvalidateUnknownArchiveMessage.Text = ErrorMessages.ArchiveError.ConfirmUnknownArchiveInvalidation(
                        this.LoanEstimateArchiveDate);
                }                
                else if (this.HasBothCDAndLEArchiveInUnknownStatus)
                {
                    this.InvalidateUnknownArchiveMessage.Text = ErrorMessages.ArchiveError.ConfirmUnknownArchiveInvalidation(
                    this.ClosingDisclosureArchiveDate);
                }

                if ((this.DocumentGenerationCreatedLoanEstimateArchive && this.UnknownLoanEstimateArchiveWasSourcedFromPendingCoC) || this.HasBothCDAndLEArchiveInUnknownStatus)
                {
                    this.ResubmitDiv.Visible = true;
                    this.NotDisclosed.Value = "Documents and CoC are both invalid and will not be disclosed. Pending CoC will be discarded.";
                }
            }            

            string key = RequestHelper.GetSafeQueryString("key");
            if (false == string.IsNullOrEmpty(key))
            {

                Response.Write(AutoExpiredTextCache.GetFromCache(key));
                Response.End();
                return;
            }

            string doc = RequestHelper.GetSafeQueryString("Doc");
            if (false == string.IsNullOrEmpty(doc))
            {
                string path = FileDBTools.CreateCopy(E_FileDB.Temp, doc);
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.Cache.SetExpires(DateTime.Now.AddDays(1));
                Response.Cache.SetLastModified(DateTime.Now);
                Response.WriteFile(path);
                Response.Flush();
                Response.End();
                return;
            }


            List<string> pdfFileDBKeys = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<String>>(AutoExpiredTextCache.GetFromCache(DocumentKey));
            if (!IsPostBack)
            {
                if (pdfFileDBKeys.Count > 1)
                {
                    m_ddlPDFDocument.Items.Clear();
                    m_ddlPDFDocument.Items.AddRange(pdfFileDBKeys.Select((k, index) => new ListItem((index + 1).ToString(), k)).ToArray());
                }

                if (RefreshOpenerOnLoad)
                {
                    ClientScript.RegisterHiddenField("RefreshOpener", "true");
                }
            }
            if (pdfFileDBKeys.Count == 1)
            {
                m_phPDFChoice.Visible = false;
                SelectedDoc = pdfFileDBKeys[0];
            }
            else 
            {
                SelectedDoc = m_ddlPDFDocument.SelectedValue;                
            }


            AprSchedule.Visible = !string.IsNullOrEmpty(HtmlKeyApr);
            ImpoundAnalysis.Visible = !string.IsNullOrEmpty(HtmlKeyImpoundAnalysis);
            Section32.Visible = !string.IsNullOrEmpty(HtmlKeySection32);
            GfeComparison.Visible = !string.IsNullOrEmpty(HtmlKeyGfeComparison);

            AprScheduleTab.Visible = !string.IsNullOrEmpty(HtmlKeyApr);
            ImpoundAnalysisTab.Visible = !string.IsNullOrEmpty(HtmlKeyImpoundAnalysis);
            Section32Tab.Visible = !string.IsNullOrEmpty(HtmlKeySection32);
            GfeComparisonTab.Visible = !string.IsNullOrEmpty(HtmlKeyGfeComparison);


            string url = Page.ResolveUrl("DocumentViewer.aspx") + "?LoanId=" + LoanID + "&key=";
            AprScheduleIFrame.Attributes.Add("src", url + HtmlKeyApr);
            ImpoundAnalysisIFrame.Attributes.Add("src", url + HtmlKeyImpoundAnalysis);
            Section32IFrame.Attributes.Add("src", url + HtmlKeySection32);
            GfeComparisoIFrame.Attributes.Add("src", url + HtmlKeyGfeComparison);

            string urls = Page.ResolveUrl("DocumentViewer.aspx") + "?LoanId=" + LoanID + "&pdf=" + DocumentKey;
            //DocumentIframe.Attributes.Add("src", Page.ResolveUrl("OnDemandPdfViewer.aspx") + String.Format(
            //"?loanid={0}&key={1}", LoanID, DocumentKey));

            // Old DocMagic (pre-TRID) doesn't have a VendorId.
            Guid VendorId = RequestHelper.GetGuid("VendorId", Guid.Empty);
            if (VendorId != Guid.Empty && LendersOffice.Integration.DocumentVendor.VendorConfig.Retrieve(VendorId).PlatformType == E_DocumentVendor.IDS)
            {
                IDSDocPortalLink.Visible = true;
                this.RegisterJsGlobalVariables("VendorId", VendorId);
            }
        }

        protected void PDFDocumentChanged(object sender, EventArgs e)
        {
            //handled by page load.
        }
    }
}
