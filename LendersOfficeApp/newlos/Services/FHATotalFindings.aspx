﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FHATotalFindings.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.FHATotalFindings" %>
<%@ Register TagPrefix="ML" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>FHA TOTAL Scorecard Findings</title>
    <style type="text/css" >
        .wrap-iframe { width: 100%; overflow: hidden; }
        iframe { width: 99.4%; }
    </style>
</head>
<body bgcolor="gainsboro" onBeforeUnload="return ConditionCheckUnload()">
    <h4 class="page-header">FHA TOTAL Scorecard Findings</h4>
    <form id="form1" runat="server">
    <table border="0" cellspacing="0" cellpadding="5" width="100%">
        <tr>
            <td>
                <input type="button" onclick="ConditionCheck(false);" id="conditionImport" runat="server" value="Import Conditions" />
            </td>
            <td align="right" valign="top">

                <input type="button" onclick="f_print();" value="Print" AlwaysEnable="true"/>
                <input type="button" onclick="onClosePopup();" value="Close" AlwaysEnable="true"/>
            </td>
        </tr>
    </table>
    <div class="wrap-iframe">
        <iframe runat="server" id="Certificate" width="100%"></iframe>
    </div>
    </form>
    <script type="text/javascript">
        resizeForIE6And7( 780, 600 );
        var importConditions = false;
       window.onresize = function() { f_resizeIframe('Certificate',10); };
       function f_print() {
           lqbPrintByFrame(window.frames['Certificate']);
       }

        function a(str) {
            return confirm(str);
        }

        function ConditionCheckUnload()
        {
            if( importConditions || (document.location.search.indexOf('fv=1') === -1) ) { return; }
            var condImport = document.getElementById('conditionImport');
            condImport.disabled = true;

            setTimeout(function(){
                condImport.disabled = false;
            }, 0)

            return UnloadMessage;
        }

        function ConditionCheck(ask) {
            if( importConditions || ( ask && document.location.search.indexOf('fv=1') === -1) ) { return; }
            var condImport = document.getElementById('conditionImport');
            condImport.disabled = true;
            if ( ask ) {
                var answer =   a('Import conditions from FHA TOTAL Scorecard findings?');
                if ( !answer ) { return; }
            }
            var obj = {
                LoanID : <%= AspxTools.JsString(LoanID) %>
            };

            var y = gService.FHATotal.call("ImportConditions", obj );
            if ( y.error)
            {
                alert( y.UserMessage );
                condImport.disabled = false;
            }
            else {
                importConditions = true;
                condImport.disabled = true;
                alert( "Conditions successfully imported from FHA TOTAL findings." );
            }


        }
    </script>
      <ML:CModalDlg id="m_ModalDlg" runat="server"></ML:CModalDlg>
</body>
</html>
