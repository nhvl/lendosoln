<%@ Page Language="c#" CodeBehind="DocMagicApplet.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Services.DocMagicApplet" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en" >
<html>
<head runat="server">
    <title>DocMagic Integration</title>
    <style type="text/css">
        .ErrorMessage
        {
            PADDING-LEFT: 5px;
            FONT-WEIGHT: bold;
            PADDING-BOTTOM: 5px;
            COLOR: red;
            PADDING-TOP: 5px;
            overflow: auto;
            width: 380px;
            height: 201px;
        }

        .ErrorMessage pre
        {
            font-family: Arial, Helvetica, sans-serif;
            word-wrap: break-word;
        }

        .WaitLabel
        {
            FONT-WEIGHT: bold;
            FONT-SIZE: 18pt;
            CURSOR: wait;
        }

        .LeftPadding
        {
            PADDING-LEFT: 5px;
        }
    </style>
</head>

<body class="RightBackground" style="MARGIN-LEFT: 0px">

    <script language="javascript">

        function f_displayWaiting() {
            document.getElementById("WaitPanel").style.display = "";
            document.getElementById("MainPanel").style.display = "none";
            document.getElementById("ErrorPanel").style.display = "none";
        }

        function _init() {
            <% if (m_hasError) { %>
                window.resizeTo(400, 430);
                document.getElementById("ErrorPanel").style.display = "";

            <% }
            else { %>
                window.resizeTo(400, 280);
            <% } %>
            <% if (false == IsPerTransactionBilling) { %>
                <%= AspxTools.JsGetElementById(DocMagicCustomerID) %>.focus();
            <%} %>
        }

        function f_onSubmitClick() {
            var bValid = true;
            if (typeof (Page_ClientValidate) == 'function' || typeof (Page_ClientValidate) == 'object')
                bValid = Page_ClientValidate();

            if (bValid) {
                f_displayWaiting();
                __doPostBack('', '');
            }
            return false;
        }
    </script>

    <form id="DocMagicApplet" method="post" runat="server">
        <table id="MainTable" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tbody>

                <tr class="MainRightHeader">
                    <td nowrap colspan="2">
                        <ml:EncodedLiteral ID="TitleLabel" runat="server"></ml:EncodedLiteral></td>
                </tr>
                <tbody id="WaitPanel" style="DISPLAY: none">
                    <tr>
                        <td colspan="2" height="50" class="WaitLabel" align="middle" valign="center">Please Wait ...</td>
                    </tr>
                </tbody>
            <tbody id="MainPanel">
                <tr>
                <tr id="PackageTypeRow" runat="server">
                    <td nowrap class="FieldLabel LeftPadding">Package Type</td>
                    <td nowrap>
                        <asp:DropDownList ID="DocMagicPackageType" runat="server"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="FieldLabel LeftPadding" nowrap>Websheet Number</td>
                    <td nowrap>
                        <asp:TextBox ID="DocMagicWebsheet" runat="server"></asp:TextBox></td>
                </tr>
                <%if (false == IsPerTransactionBilling)
                  { %>

                <tr>
                    <td nowrap class="FieldLabel LeftPadding">Customer ID</td>
                    <td nowrap>
                        <asp:TextBox ID="DocMagicCustomerID" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="* Required" ControlToValidate="DocMagicCustomerID"></asp:RequiredFieldValidator></td>
                </tr>
                <%} %>
                <% if (CanModifyLoginInfo)
                   { %>
                <tr>
                    <td nowrap class="FieldLabel LeftPadding">User Name</td>
                    <td nowrap>
                        <asp:TextBox ID="DocMagicUserName" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="* Required" ControlToValidate="DocMagicUserName"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td nowrap class="FieldLabel LeftPadding">Password</td>
                    <td nowrap>
                        <asp:TextBox ID="DocMagicPassword" runat="server" TextMode="Password" MaxLength="20">demo</asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="* Required" ControlToValidate="DocMagicPassword"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td nowrap></td>
                    <td nowrap>
                        <asp:CheckBox ID="DocMagicRememberLogon" Text="Remember logon credentials" runat="server" /></td>
                </tr>
                <%} %>
                <tr>
                    <td nowrap></td>
                    <td nowrap>
                        <input type="submit" value="Submit" onclick='return f_onSubmitClick();' id='btnSubmit'>
                        <input type="button" value="Cancel" onclick="onClosePopup();" id="btnCancel">
                    </td>
                </tr>
            </tbody>
            <tbody id="ErrorPanel" style="DISPLAY: none">
                <tr>
                    <td nowrap colspan="2" class="FieldLabel LeftPadding">Error Message</td>
                </tr>
                <tr>
                    <td nowrap colspan="2">
                        <div class="ErrorMessage">
                            <pre><%= AspxTools.HtmlString(m_errorMessage) %></pre>
                        </div>
                    </td>
                </tr>
            </tbody>

        </table>
    </form>
</body>
</html>
