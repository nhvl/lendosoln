<%@ Page Language="c#" CodeBehind="DocMagic.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Services.DocMagic" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head id="Head1" runat="server">
    <title>DocMagic</title>
    <style type="text/css">
        td
        {
            font-weight: bold;
        }

        body
        {
            margin-left: 0px;
        }

        .Hidden
        {
            display: none;
        }
    </style>
</head>
<body class="RightBackground">
    <script type="text/javascript">

        function _init() {
            var hasWebsheetNumber = document.getElementById('<%= AspxTools.ClientId(HasWebsheetNumber)%>').value === 'False';
            if (hasWebsheetNumber) {
                document.getElementById('LaunchBtn').disabled = true;
            }

            var use2015DataLayer = document.getElementById('<%= AspxTools.ClientId(Use2015DataLayer)%>').value === 'True';
            if (!use2015DataLayer) {
                var changeSource = document.getElementById('<%= AspxTools.ClientId(sSettlementChargesExportSource) %>');
                    addEventHandler(changeSource, 'change', sSettlementChargesExportSource_onChange, false);

                sSettlementChargesExportSource_onChange();
                checkCloserPermissions()
            }

            function closeHandler () {
                window.location.href = window.location.href;
            }

            if (ML && ML.sHasLoanEstimateArchiveInPendingStatus) {
                ArchiveStatusPopup.create(
                    'ArchiveStatusPopupMessage',
                    ML.sLId,
                    false,
                    ArchiveStatusPopupMode.PendingNoCancel,
                    ArchiveStatusPopupSource.NonSeamlessDocumentGeneration,
                    closeHandler);
                ArchiveStatusPopup.displayPopup();
            } else if (ML && ML.sHasClosingCostArchiveInUnknownStatus) {
                ArchiveStatusPopup.create(
                    'ArchiveStatusPopupMessage',
                    ML.sLId,
                    false,
                    ML.HasBothCDAndLEArchiveInUnknownStatus ? ArchiveStatusPopupMode.HasBothCDAndLEArchiveInUnknownStatus : ArchiveStatusPopupMode.UnknownNoCancel,
                    ArchiveStatusPopupSource.NonSeamlessDocumentGeneration,
                    closeHandler);
                ArchiveStatusPopup.displayPopup();
            }
        }

        function checkCloserPermissions() {
            var use2015DataLayer = document.getElementById('<%=AspxTools.ClientId(Use2015DataLayer)%>').value === 'True';
            if (use2015DataLayer) {
                return;
            }

            var warning = document.getElementById('CloserWarning');
            var readPermission = document.getElementById('<%= AspxTools.ClientId(CanReadCloser) %>');
            var writePermission = document.getElementById('<%= AspxTools.ClientId(CanWriteCloser) %>');
            var changeSource = document.getElementById('<%= AspxTools.ClientId(sSettlementChargesExportSource) %>');
            var includeGfeLabelCb = document.getElementById('includeGfeLabelCb');

            changeSource.disabled = includeGfeLabelCb.disabled = writePermission.value == 'False';

            if (changeSource.value == '1' && readPermission.value == 'False') {
                document.getElementById('ExportBtn').disabled = true;
                warning.style.visibility = 'visible';
            }
            else {
                warning.style.visibility = 'hidden';
            }
        }

        function displayWindow(cmd) {
            var w = window.open('DocMagicApplet.aspx?loanid=' + <%= AspxTools.JsString(LoanID)%> + '&cmd=' + cmd, 'DocMagic', 'location=no,menubar=no,resizable=yes,status=yes,toolbar=no');
        }

        function sSettlementChargesExportSource_onChange() {
            var changeSource = document.getElementById('<%= AspxTools.ClientId(sSettlementChargesExportSource) %>');
            var includeGfeLabelCb = document.getElementById('includeGfeLabelCb');
            includeGfeLabelCb.style.visibility = changeSource.value == 0 ? 'hidden' : 'visible';
        }

        function f_export() {
            saveMe(true);
            checkCloserPermissions();

            if (document.getElementById('ExportBtn').disabled) {
                alert('You need the closer permission to export.');
                return;
            }

            displayWindow('export');
        }

        function f_export33(LoanID, AppID) {
            saveMe(true);

            var body_url = "DocMagicMISMO33NonSeamless.aspx?loanid=" + LoanID + "&appid=" + AppID;
            var opts = 'location=no,menubar=no,resizable=yes,status=yes,toolbar=no';
            var w = window.open(body_url, 'DocMagic', opts);
        }
        
        function f_launch() {
            saveMe(true);
            checkCloserPermissions();
            displayWindow('launch');
        }
    </script>

    <form id="DocMagic" method="post" runat="server">

        <asp:HiddenField runat="server" ID="HasWebsheetNumber" />
        <asp:HiddenField runat="server" ID="CanReadCloser" />
        <asp:HiddenField runat="server" ID="CanWriteCloser" />
        <asp:HiddenField runat="server" ID="Use2015DataLayer" />

        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tbody>
                <tr>
                    <td class="MainRightHeader">DocMagic Online Integration</td>
                </tr>
                <div id="SettlementChargesExportSourceDiv" runat="server">
                    <tr>
                        <div>
                            <td>&nbsp;&nbsp;<ml:EncodedLabel ID="Label1" runat="server" AssociatedControlID="sSettlementChargesExportSource">Export settlement charges from</ml:EncodedLabel>
                                <asp:DropDownList runat="server" ID="sSettlementChargesExportSource">
                                    <asp:ListItem Text="GFE" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Settlement Charges" Value="1"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </div>
                    </tr>
                    <tr>
                        <td>
                            <div id="includeGfeLabelCb">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:CheckBox runat="server" ID="sIncludeGfeDataForDocMagicComparison" Text="Include GFE data from LendingQB for GFE comparison on HUD-1" />
                            </div>

                        </td>
                    </tr>
                </div>
                <tr>
                    <td height="40" valign="center" style="padding-left: 10px">
                        <input type="button" id="ExportBtn" class="buttonStyle" alwaysenable="True" value="Export to DocMagic ..." onclick="f_export()" />
                        &nbsp;&nbsp; 
                        <input type="button" id="Export33Btn" runat="server" class="buttonStyle" alwaysenable="True" value="Export MISMO 3.3 to DocMagic ..." />
                        &nbsp;&nbsp; 
                        <input type="button" id="LaunchBtn" runat="server" class="buttonStyle" alwaysenable="True" value="Launch DocMagic Applet ..." onclick="f_launch()" />
                    </td>
                </tr>
                <tr id="CloserWarning" runat="server">
                    <td style="color: Red;">"Allow viewing pages in Closer Folder" permission is required to export data from the Settlement Charges page to DocMagic.   
                    </td>
                </tr>
            </tbody>
        </table>
        <div id="ArchiveStatusPopupMessage" runat="server" style="display: none;"></div>
    </form>
</body>
</html>