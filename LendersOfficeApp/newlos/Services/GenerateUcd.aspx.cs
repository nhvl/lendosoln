﻿#region Auto Generated Code
namespace LendersOfficeApp.newlos.Services
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Integration.UcdDelivery;
    using LendersOffice.Security;
    using MeridianLink.CommonControls;

    /// <summary>
    /// Generate UCD popup.
    /// </summary>
    public partial class GenerateUcd : BaseLoanPage
    {
        /// <summary>
        /// The available vendors.
        /// </summary>
        private Lazy<Dictionary<Guid, VendorConfig>> availableVendors = new Lazy<Dictionary<Guid, VendorConfig>>(() => DocumentVendorFactory.AvailableVendors().ToDictionary((config) => config.VendorId));

        /// <summary>
        /// Gets a value indicating the delivery action requested.
        /// </summary>
        /// <value>Which delivery action to present UI elements for. Null if no delivery action parameter was present.</value>
        protected UcdDeliveryAction? DeliveryAction { get; } = RequestHelper.GetNullableEnum<UcdDeliveryAction>("deliveryAction", ignoreCase: true);

        /// <summary>
        /// Gets a value indicating whether the request action goes to DocMagic.
        /// </summary>
        /// <remarks>
        /// The GenerateOnly and GenerateAndDeliver delivery actions are currently only available through DocMagic, 
        /// but we may add features like LQB UCD generation or additional generation vendors in the future. 
        /// In that event, a separate delivery vendor data point may be useful.
        /// </remarks>
        /// <value>Whether the page is for DocMagic.</value>
        protected bool IsDocMagic
        {
            get
            {
                return this.DeliveryAction == UcdDeliveryAction.GenerateOnly || this.DeliveryAction == UcdDeliveryAction.GenerateAndDeliver;
            }
        }

        /// <summary>
        /// Gets the reasons the privilege was not allowed during the page's workflow checks.
        /// </summary>
        /// <returns>An array containing <seealso cref="WorkflowOperations.GenerateUCD"/>.</returns>
        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return new WorkflowOperation[] { WorkflowOperations.GenerateUCD };
        }

        /// <summary>
        /// Gets the extra ops to check during the page's workflow checks.
        /// </summary>
        /// <returns>An array containing <seealso cref="WorkflowOperations.GenerateUCD"/>.</returns>
        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[] { WorkflowOperations.GenerateUCD };
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        protected override void LoadData()
        {
            if (!this.DeliveryAction.HasValue || this.DeliveryAction.Value == UcdDeliveryAction.GetResults)
            {
                throw new CBaseException(ErrorMessages.Generic, "No delivery action passed in request query.");
            }

            bool isDelivery = this.DeliveryAction == UcdDeliveryAction.GenerateAndDeliver || this.DeliveryAction == UcdDeliveryAction.DeliverExisting;

            this.GseDeliveryTargetSection.Visible = isDelivery;
            this.GseCredentialsSection.Visible = isDelivery;

            this.RegisterJsGlobalVariables("CanGenerateUCD", this.UserHasWorkflowPrivilege(WorkflowOperations.GenerateUCD));
            this.RegisterJsGlobalVariables("CanGenerateUCDDenialReason", this.GetReasonUserCannotPerformPrivilege(WorkflowOperations.GenerateUCD));

            switch (this.DeliveryAction.Value)
            {
                case UcdDeliveryAction.DeliverExisting:
                    this.GenUcdHeader.Text = "Deliver UCD (Direct to GSE)";
                    this.GenerateBtn.Value = "Deliver UCD File";
                    this.DocCodeSection.Visible = false;
                    break;
                case UcdDeliveryAction.GenerateAndDeliver:
                    this.GenUcdHeader.Text = "Generate and Deliver UCD (DocMagic)";
                    this.GenerateBtn.Value = "Generate and Deliver with DocMagic";
                    this.DocCodeSection.Visible = true;
                    break;
                case UcdDeliveryAction.GenerateOnly:
                    this.GenUcdHeader.Text = "Generate UCD (DocMagic)";
                    this.GenerateBtn.Value = "Generate with DocMagic";
                    this.DocCodeSection.Visible = true;
                    break;
                default:
                    throw new UnhandledEnumException(this.DeliveryAction.Value);
            }

            var dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(GenerateUcd));
            dataLoan.InitLoad();

            var closingDisclosureDates = dataLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList;
            Dictionary<Guid, bool> invalidArchiveMap = new Dictionary<Guid, bool>();
            Dictionary<Guid, bool> validEdocsMap = new Dictionary<Guid, bool>();
            EDocumentRepository repository = EDocumentRepository.GetUserRepository();

            List<CDDatesHelper> dates = new List<CDDatesHelper>();
            foreach (var date in closingDisclosureDates.Where(dateRecord => IncludeClosingDisclosure(dateRecord)))
            {
                var archiveId = date.ArchiveId;
                bool isInvalid = false;
                if (!invalidArchiveMap.TryGetValue(archiveId, out isInvalid))
                {
                    var archive = dataLoan.sClosingCostArchive.FirstOrDefault(a => archiveId == a.Id);
                    if (archive != null)
                    {
                        isInvalid = !archive.IsDisclosed;
                    }

                    invalidArchiveMap.Add(archiveId, isInvalid);
                }

                if (isInvalid)
                {
                    continue;
                }

                bool canViewEdoc = false;
                EDocument document = null;
                try
                {
                    document = repository.GetDocumentById(date.UcdDocument);
                    canViewEdoc = true;
                }
                catch (CBaseException exc) when (exc is NotFoundException || exc is AccessDenied)
                {
                    // For direct integration, we shouldn't include archives for which there is no UCD to post.
                    canViewEdoc = false;
                    if (this.DeliveryAction == UcdDeliveryAction.DeliverExisting)
                    {
                        continue;
                    }
                }

                CDDatesHelper helper = new CDDatesHelper();
                if (date.VendorId.HasValue)
                {
                    helper.AssociatedVendor = AspxTools.HtmlString(this.availableVendors.Value.GetValueOrNull(date.VendorId.Value)?.VendorName) ?? string.Empty;
                }
                else
                {
                    helper.AssociatedVendor = "<i>Unknown</i>";
                }

                Func<DateTime, string> createDateString = d => d == DateTime.MinValue ? string.Empty : d.ToString("d");
                Func<DateTime, string> createDateTimeString = d => d == DateTime.MinValue ? string.Empty : d.ToString();
                helper.ClosingDisclosureDatesId = date.UniqueId.ToString();
                helper.IssuedDate = createDateString(date.IssuedDate);
                helper.ReceivedDate = createDateString(date.ReceivedDate);
                helper.SignedDate = createDateString(date.SignedDate);
                helper.ArchiveDate = createDateTimeString(date.ArchiveDate);
                helper.IsFinal = date.IsFinal;
                helper.IsPreview = date.IsPreview;
                helper.IsInitial = date.IsInitial;
                helper.IsPostClosing = date.IsPostClosing;
                helper.PostConsummationKnowledgeOfEventDate = createDateString(date.PostConsummationKnowledgeOfEventDate);
                helper.PostConsummationRedisclosureReasonDate = createDateString(date.PostConsummationRedisclosureReasonDate);
                helper.UcdFileId = date.UcdDocument == Guid.Empty ? null : date.UcdDocument.ToString();
                helper.UcdFileDoc = document;
                helper.CanViewUcdDoc = canViewEdoc;
                helper.DocCode = string.IsNullOrEmpty(date.DocCode) ? string.Empty : date.DocCode;

                dates.Add(helper);
            }

            this.CDRepeater.DataSource = dates;
            this.CDRepeater.DataBind();
        }

        /// <summary>
        /// Page init function.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void PageInit(object sender, System.EventArgs e)
        {
            var ucdConfig = PrincipalFactory.CurrentPrincipal.BrokerDB.GetUcdConfig();
            var deliveryOptions = new List<UcdDeliveredToEntity> { UcdDeliveredToEntity.Blank };
            if ((this.DeliveryAction == UcdDeliveryAction.GenerateAndDeliver && ucdConfig.AllowDocMagicDeliveryToFannieMae)
                || (this.DeliveryAction == UcdDeliveryAction.DeliverExisting && ucdConfig.AllowLqbDeliveryToFannieMae))
            {
                deliveryOptions.Add(UcdDeliveredToEntity.FannieMae);
            }

            if ((this.DeliveryAction != UcdDeliveryAction.DeliverExisting && ucdConfig.AllowDocMagicDeliveryToFreddieMac)
                || (this.DeliveryAction == UcdDeliveryAction.DeliverExisting && ucdConfig.AllowLqbDeliveryToFreddieMac && LendersOffice.Constants.ConstStage.EnableLoanClosingAdvisor))
            {
                deliveryOptions.Add(UcdDeliveredToEntity.FreddieMac);
            }

            Tools.BindGenericEnum(this.GseDeliveryTarget, deliveryOptions);

            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("SimplePopups.js");
            this.RegisterJsScript("jquery.tmpl.js");

            this.RegisterCSS("jquery-ui-1.11.custom.css");
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterJsScript("widgets/jquery.docmetadatatooltip.js");

            this.RegisterJsGlobalVariables("DeliveryAction", (int)this.DeliveryAction);
            this.RegisterService("ucd", "/newlos/Services/GenerateUcdService.aspx");
        }

        /// <summary>
        /// Initialize function.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;
            this.InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Binds additional fields in the CD table.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void CDRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.AlternatingItem && e.Item.ItemType != ListItemType.Item)
            {
                return;
            }

            CDDatesHelper helper = (CDDatesHelper)e.Item.DataItem;
            PassthroughLabel associatedVendor = (PassthroughLabel)e.Item.FindControl("AssociatedVendor");
            CheckBox isInitial = (CheckBox)e.Item.FindControl("IsInitial");
            CheckBox isPreview = (CheckBox)e.Item.FindControl("IsPreview");
            CheckBox isFinal = (CheckBox)e.Item.FindControl("IsFinal");

            HtmlAnchor ucdFileLink = (HtmlAnchor)e.Item.FindControl("UcdFileLink");
            HtmlAnchor ucdXmlLink = (HtmlAnchor)e.Item.FindControl("UcdXmlLink");
            HtmlInputHidden ucdFileId = (HtmlInputHidden)e.Item.FindControl("UcdFileId");
            HtmlImage ucdFileInfo = (HtmlImage)e.Item.FindControl("UcdFileInfo");

            CheckBox isPostClosing = (CheckBox)e.Item.FindControl("IsPostClosing");
            HtmlGenericControl postConsummationRedisclosureReasonDate = (HtmlGenericControl)e.Item.FindControl("PostConsummationRedisclosureReasonDate");
            HtmlGenericControl postConsummationKnowledgeOfEventDate = (HtmlGenericControl)e.Item.FindControl("PostConsummationKnowledgeOfEventDate");

            associatedVendor.Text = helper.AssociatedVendor;
            isInitial.Checked = helper.IsInitial;
            isPreview.Checked = helper.IsPreview;
            isFinal.Checked = helper.IsFinal;

            if (!string.IsNullOrEmpty(helper.UcdFileId))
            {
                ucdFileLink.InnerText = "view pdf";
                ucdXmlLink.InnerText = "view xml";
                if (helper.CanViewUcdDoc)
                {
                    ucdFileId.Value = helper.UcdFileId;
                    Tools.ApplyDocMetaDataTooltip(ucdFileInfo, helper.UcdFileDoc);
                }
                else
                {
                    ucdFileLink.Attributes["class"] = "DisabledLink";
                    ucdXmlLink.Attributes["class"] = "DisabledLink";
                }
            }
            else
            {
                ucdXmlLink.Attributes["class"] = "DisabledLink";
                ucdFileLink.Attributes["class"] = "DisabledLink";
            }

            isPostClosing.Checked = helper.IsPostClosing;
            postConsummationKnowledgeOfEventDate.InnerText = helper.PostConsummationKnowledgeOfEventDate;
            postConsummationRedisclosureReasonDate.InnerText = helper.PostConsummationRedisclosureReasonDate;
        }

        /// <summary>
        /// Determines whether to include a closing disclosure date record in the list of available closing disclosures to deliver.
        /// </summary>
        /// <param name="closingDisclosureRecord">The closing disclosure date record to determine whether to include.</param>
        /// <returns>Whether or not to include the date record.</returns>
        private bool IncludeClosingDisclosure(ClosingDisclosureDates closingDisclosureRecord)
        {
            VendorConfig associatedVendor = null;
            if (this.DeliveryAction == UcdDeliveryAction.DeliverExisting)
            {
                return true;
            }
            else
            {
                bool associatedVendorExists = closingDisclosureRecord.VendorId.HasValue && this.availableVendors.Value.TryGetValue(closingDisclosureRecord.VendorId.Value, out associatedVendor);
                return associatedVendorExists
                    && (!this.IsDocMagic || associatedVendor?.PlatformType == E_DocumentVendor.DocMagic);
            }
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
        }

        /// <summary>
        /// Helper class to display closing disclosure dates table.
        /// </summary>
        private class CDDatesHelper
        {
            /// <summary>
            /// Gets or sets the closing disclosure dates id.
            /// </summary>
            /// <value>The closing disclosure dates id.</value>
            public string ClosingDisclosureDatesId
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the doc code associated with the closing disclosure.
            /// </summary>
            /// <value>The doc code associated with the closing disclosure.</value>
            public string DocCode
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the associated vendor.
            /// </summary>
            /// <value>The associated vendor.</value>
            public string AssociatedVendor
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the issued date.
            /// </summary>
            /// <value>The issued date.</value>
            public string IssuedDate
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the received date.
            /// </summary>
            /// <value>The received date.</value>
            public string ReceivedDate
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the signed date.
            /// </summary>
            /// <value>The signed date.</value>
            public string SignedDate { get; set; }

            /// <summary>
            /// Gets or sets the archive date.
            /// </summary>
            /// <value>The archive date.</value>
            public string ArchiveDate { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the CD is the Initial CD.
            /// </summary>
            /// <value>Whether the CD is the initial CD.</value>
            public bool IsInitial
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether the CD is a preview CD.
            /// </summary>
            /// <value>Whether the CD is a preview CD.</value>
            public bool IsPreview
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether the CD is a final CD.
            /// </summary>
            /// <value>Whether the CD is a final CD.</value>
            public bool IsFinal
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the UCD file id.
            /// </summary>
            /// <value>The UCD file id.</value>
            public string UcdFileId
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the EDoc data for the UCD File.
            /// </summary>
            /// <value>The EDoc data for the UCD File.</value>
            public EDocument UcdFileDoc { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the CD is in post closing.
            /// </summary>
            /// <value>Whether the CD is in post closing.</value>
            public bool IsPostClosing
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the event requiring redisclosure date.
            /// </summary>
            /// <value>The event requiring redisclosure date.</value>
            public string PostConsummationRedisclosureReasonDate
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the date creditor received knowledge.
            /// </summary>
            /// <value>The date creditor received knowledge.</value>
            public string PostConsummationKnowledgeOfEventDate
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether the user can view the UCD doc.
            /// </summary>
            /// <value>Whether the user can view the UCD doc.</value>
            public bool CanViewUcdDoc
            {
                get;
                set;
            }
        }
    }
}
