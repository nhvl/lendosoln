﻿namespace LendersOfficeApp.newlos.Services
{
    using System;
    using DataAccess;
    using global::DocuTech;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Conversions.DocuTech;
    using LendersOffice.Security;

    public partial class DocuTechService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName) 
            {
                case "Export":
                    Export();
                    break;
                case "AutoExport":
                    AutoExport();
                    break;
            }
        }

        private void AutoExport()
        {
            Guid sLId = GetGuid("LoanID");
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(DocuTechService));
            dataLoan.InitLoad();

            this.VerifyFileDoesNotHavePendingOrUnknownArchives(dataLoan);

            string url;
            string error;

            bool hasError = DocuTechExporter.Export(sLId, out url, out error, PrincipalFactory.CurrentPrincipal);

            if (!hasError)
            {
                SetResult("URL", url);
            }
            else
            {
                SetResult("Error", error);
            }
        }

        private void Export()
        {
            Guid sLId = GetGuid("LoanID");
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(DocuTechService));
            
            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
            dataLoan.InitSave(sFileVersion);

            this.VerifyFileDoesNotHavePendingOrUnknownArchives(dataLoan);

            if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
            {
                dataLoan.sSettlementChargesExportSource = (E_SettlementChargesExportSource)GetInt("sSettlementChargesExportSource");
                dataLoan.sIncludeGfeDataForDocMagicComparison = GetBool("sIncludeGfeDataForDocMagicComparison");
                dataLoan.Save();

                SetResult("sFileVersion", dataLoan.sFileVersion);
            }

            E_DocumentsPackageType packageType = (E_DocumentsPackageType)GetInt("ddlDocuTechPackageType");
            string userName = GetString("tbUserName");
            string password = GetString("tbPassword");
            bool rememberUserName = GetBool("cbRememberPassword");

            DocuTechSavedAuthentication savedAuthentication = DocuTechSavedAuthentication.Retrieve((AbstractUserPrincipal) this.User);

            if (password == ConstAppDavid.FakePasswordDisplay)
            {
                if (savedAuthentication.Password != "")
                {
                    password = savedAuthentication.Password;
                }
            }

            if (rememberUserName)
            {
                savedAuthentication.UserName = userName;
                savedAuthentication.Password = password;
            }
            else
            {
                savedAuthentication.UserName = "";
                savedAuthentication.Password = "";
            }

            savedAuthentication.Save();

            string url = string.Empty;
            string errorMessage = string.Empty;
            bool hasError = DocuTechExporter.Export(sLId, userName, password, packageType, out url, out errorMessage, PrincipalFactory.CurrentPrincipal);

            SetResult("HasError", hasError);
            SetResult("ErrorMessage", errorMessage);
            SetResult("Url", url);
        }

        private void VerifyFileDoesNotHavePendingOrUnknownArchives(CPageData dataLoan)
        {
            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                string error = null;

                if (dataLoan.sHasLoanEstimateArchiveInPendingStatus)
                {
                    var archive = dataLoan.sLoanEstimateArchiveInPendingStatus;
                    error = ErrorMessages.ArchiveError.UnableToGenerateDocumentsBecauseOfPendingArchive(
                        archive.DateArchived,
                        archive.Status,
                        userCanDetermineStatus: false);
                }
                else if (dataLoan.sHasClosingCostArchiveInUnknownStatus)
                {
                    var archive = dataLoan.sClosingCostArchiveInUnknownStatus;
                    error = ErrorMessages.ArchiveError.UnableToGenerateDocumentsBecuaseOfUnknownArchive(
                        archive.DateArchived,
                        archive.ClosingCostArchiveType,
                        userCanDetermineStatus: false);
                }

                if (!string.IsNullOrEmpty(error))
                {
                    throw new CBaseException(error, "Cannot generate non-seamless docs with Pending/Unknown archive.");
                }
            }
        }
    }
}
