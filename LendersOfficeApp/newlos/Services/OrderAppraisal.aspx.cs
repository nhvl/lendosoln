﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.ConfigSystem.Operations;
using LendersOffice.Constants;
using LendersOffice.Integration.Appraisals;
using LendersOffice.ObjLib.Conversions.GlobalDMS;
using LendersOffice.ObjLib.Conversions.LQBAppraisal;
using LendersOffice.ObjLib.UI.DataContainers;
using LendersOffice.Security;
using LendersOfficeApp.los.admin;
using LQBAppraisal.LQBAppraisalRequest;

namespace LendersOfficeApp.newlos.Services
{
    public partial class OrderAppraisal : BaseLoanPage
    {
        /// <summary>
        /// A list of VendorOptionToJSConverters used for creating the JS script containing the options, which is added to the page. 
        /// If adding another vendor config option to the OrderAppraisal page, put it here.
        /// </summary>
        private List<VendorOptionToJSConverter> VendorConfigOptions = new List<VendorOptionToJSConverter>()
        {
            new VendorOptionToJSConverter("GDMSVendors", (vendor) => { return vendor.UsesGlobalDMS ? "true" : "false"; } ),
            new VendorOptionToJSConverter("HideAppraisalNeededDate", (vendor) => { return vendor.AppraisalNeededDateOption.Equals(E_AppraisalNeededDateOptions.Hidden) ? "true" : "false"; } ),
            new VendorOptionToJSConverter("AppraisalNeededDateRequired", (vendor) => { return vendor.AppraisalNeededDateOption.Equals(E_AppraisalNeededDateOptions.Required) ? "true" : "false"; } ),
            new VendorOptionToJSConverter("HideNotes", (vendor) => {return vendor.HideNotes ? "true" : "false";} ),
            new VendorOptionToJSConverter("AllowOnlyOnePrimaryProduct", (vendor) => { return vendor.AllowOnlyOnePrimaryProduct ? "true" : "false"; } ),
            new VendorOptionToJSConverter("ShowAdditionalEmails", (vendor) => { return vendor.ShowAdditionalEmails ? "true" : "false"; } )
        };

        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[] { WorkflowOperations.OrderAppraisal };
        }
        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return new WorkflowOperation[] { WorkflowOperations.OrderAppraisal };
        }

        public bool IsEdit
        {
            get { return !string.IsNullOrEmpty(OrderNumber); }
        }

        protected string OrderNumber
        {
            get { return RequestHelper.GetSafeQueryString("orderNumber"); }
        }

        protected override void LoadData()
        {
            if (false == UserHasWorkflowPrivilege(WorkflowOperations.OrderAppraisal))
            {
                m_WorkflowAlertMessage.Value = GetReasonUserCannotPerformPrivilege(WorkflowOperations.OrderAppraisal);
                Disable.Value = true.ToString();
            }
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            this.RegisterService("OrderAppraisal", "/newlos/services/OrderAppraisalService.aspx");

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(OrderAppraisal));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(0);

            this.PopulatePropertyInfo(dataLoan);

            sLNm.Text = dataLoan.sLNm;
            sLNm_dup.Text = dataLoan.sLNm;
            GDMSCaseNum.Text = dataLoan.sAgencyCaseNum;
            sAgencyCaseNum.Text = dataLoan.sAgencyCaseNum;
            sLT.Text = dataLoan.sLT_rep;
            sLPurposeT.Text = dataLoan.sLPurposeT_rep;
            sGseSpT.Text = dataLoan.sGseSpT_rep;
            aOccT.Text = dataApp.aOccT_rep;
            LoanOfficerName.Text = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject).AgentName;

            LoadGDMSPropertyType.Value = GetGDMSPropertyType(dataLoan.sGseSpT, dataLoan.sUnitsNum);
            LoadGDMSIntendedUse.Value = GetGDMSIntendedUse(dataLoan.sLPurposeT);
            LoadGDMSLoanType.Value = GetGDMSLoanType(dataLoan.sLT);
            LoadGDMSOccupancyType.Value = GetGDMSOccupancyType(dataApp.aOccT);

            Tools.Bind_CreditCards(CCType);
            
            if (IsEdit)
            {
                LQBAppraisalOrder order = LQBAppraisalOrder.Load(LoanID.ToString(), OrderNumber);

                if (order == null)
                    throw new CBaseException("Order Number not found", "Failed to load order number " + OrderNumber);

                Tools.SetDropDownListValue(this.PropertySelector, order.LinkedReoId ?? Guid.Empty);

                SelectedAMC.Items.Clear();
                var vendor = AppraisalVendorConfig.Retrieve(order.VendorId);
                SelectedAMC.Items.Add(new ListItem(vendor.VendorName, vendor.VendorId.ToString()));
                AddVendorConfigOptions(vendor);
                SelectedAMC.Attributes.Add("disabled", "disabled");

                GDMSContactName.Text = order.AppraisalOrder.ContactInfo.ContactName;
                GDMSContactPhone.Text = order.AppraisalOrder.ContactInfo.ContactPhone;
                GDMSContactWork.Text = order.AppraisalOrder.ContactInfo.ContactWork;
                GDMSContactOther.Text = order.AppraisalOrder.ContactInfo.ContactOther;
                GDMSContactEmail.Text = order.AppraisalOrder.ContactInfo.ContactEmail;
                GDMSAdditionalEmail.Text = order.AppraisalOrder.ContactInfo.AdditionalEmail;
                var products = order.AppraisalOrder.OrderInfo.OrderedProductList;
                if (products.Count > 0)
                    LoadOrderReportType.Value = products[0].ProductName;
                if (products.Count > 1)
                    LoadOrderReportType2.Value = products[1].ProductName;
                if (products.Count > 2)
                    LoadOrderReportType3.Value = products[2].ProductName;
                if (products.Count > 3)
                    LoadOrderReportType4.Value = products[3].ProductName;
                if (products.Count > 4)
                    LoadOrderReportType5.Value = products[4].ProductName;
                DateTime neededD;
                if(DateTime.TryParseExact(
                    order.AppraisalOrder.OrderInfo.AppraisalNeededDate
                    , "dd-MM-yyyy"
                    , null
                    , System.Globalization.DateTimeStyles.None
                    , out neededD))
                {
                    OrderAppraisalNeededD.Text = neededD.ToShortDateString();
                }
                RushOrder.Checked = order.AppraisalOrder.OrderInfo.RushOrderIndicator == LQBAppraisal.E_YNIndicator.Y;
                LoadGDMSBillingMethod.Value = order.AppraisalOrder.Billing.BillingMethodName;
                CCBillName.Text = order.AppraisalOrder.Billing.BillingInfo.BillingName;
                CCAddress1.Text = order.AppraisalOrder.Billing.BillingInfo.BillingStreet;
                CCCity.Text = order.AppraisalOrder.Billing.BillingInfo.BillingCity;
                CCState.Value = order.AppraisalOrder.Billing.BillingInfo.BillingState;
                CCZip.Text = order.AppraisalOrder.Billing.BillingInfo.BillingZIP;
                CCType.SelectedValue = order.AppraisalOrder.Billing.CCInfo.CCType;
                CCNumber.Text = order.AppraisalOrder.Billing.CCInfo.CCNumber;
                CCExpMonth.Text = order.AppraisalOrder.Billing.CCInfo.CCExpMonth;
                CCExpYear.Text = order.AppraisalOrder.Billing.CCInfo.CCExpYear;
                CCCode.Text = order.AppraisalOrder.Billing.CCInfo.CCSecurityCode;
                Notes.Text = order.AppraisalOrder.OrderInfo.OrderNotes;

                OrderAppraisalBtn.Value = "Edit Order";
            }
            else
            {
                var vendors = AppraisalVendorFactory.GetAvailableVendorsForBroker(BrokerID);
                if (vendors.Count() == 1)
                {
                    SelectedAMC.Items.Clear(); //Remove empty option
                }
                foreach (AppraisalVendorConfig vendor in vendors)
                {
                    SelectedAMC.Items.Add(new ListItem(vendor.VendorName, vendor.VendorId.ToString()));
                    AddVendorConfigOptions(vendor);
                }
                OrderAppraisalBtn.Value = "Place Order";

                //OPM 124655 - Populate contact info for these loan types
                if(dataLoan.sLPurposeT == E_sLPurposeT.Refin
                    || dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout
                    || dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance
                    || dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl)
                {
                    GDMSContactName.Text = dataApp.aBNm;
                    GDMSContactPhone.Text = dataApp.aBHPhone;
                    GDMSContactWork.Text = dataApp.aBBusPhone;
                    GDMSContactOther.Text = dataApp.aBCellPhone;
                    GDMSContactEmail.Text = dataApp.aBEmail;
                }
            }

            //Save UI settings for each vendor in javascript objects for determining UI
            string script = "";
            foreach (VendorOptionToJSConverter converter in VendorConfigOptions)
            {
                script += converter.BuildJSArray() + Environment.NewLine;
            }
            
            ClientScript.RegisterStartupScript(typeof(OrderAppraisal), "UISettings", script, true);
        }

        private void PopulatePropertyInfo(CPageData dataLoan)
        {
            var propertyInfoDetails = Tools.PopulatePropertyInfoDetails(dataLoan, this.PropertySelector, includeSubjPropInDDL: true);
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("PropertyInfoDetails", propertyInfoDetails);
        }

        private void AddVendorConfigOptions(AppraisalVendorConfig vendor)
        {
            foreach (VendorOptionToJSConverter converter in VendorConfigOptions)
            {
                converter.AddVendor(vendor);
            }
        }

        /// <summary>
        /// Represents a list of vendor-option pairs for a particular vendor config option, to be converted into javascript.
        /// The JavaScript takes the form:
        /// 
        /// var ListName = {"vendorid1":"option1", "vendorid2":"option2" , ... };
        /// </summary>
        public class VendorOptionToJSConverter
        {
            public string ListName { get; set; }

            /// <summary>
            /// A list of vendorID : value pairs (as strings) to be added to the JS script for the option
            /// </summary>
            private List<string> ListOfOptions { get; set; }

            /// <summary>
            /// A function which converts a vendor option from a given vendor object into a JS-friendly string to be used in the javascript
            /// </summary>
            /// <example>
            /// A boolean value would usually be converted to the string "true" or "false" depending on the vendor option's value:
            ///     (vendor) => {return vendor.BooleanVendorOption ? "true" : "false"; }
            /// </example>
            /// <param name="vendor">The vendor to get the option from</param>
            /// <returns>The value of this object's assigned vendor config option, stringified for adding to a JavaScript array</returns>
            public delegate string OptionStringifier(AppraisalVendorConfig vendor);

            /// <summary>
            /// A function that takes in a vendor and returns the this converter's specific option's stringified JavaScript value for that vendor
            /// </summary>
            private OptionStringifier getOptionValueAsString { get; set; }

            /// <summary>
            /// Creates an empty converter with a name and function to determine option values.
            /// </summary>
            /// <param name="listName">The name of the option list to be used in the JavaScript.</param>
            /// <param name="optionDelegate">A function that takes in a vendor and returns the this converter's specific option's stringified JavaScript value for that vendor.</param>
            public VendorOptionToJSConverter(string listName, OptionStringifier optionDelegate)
            {
                this.ListName = listName;
                this.ListOfOptions = new List<string>();
                this.getOptionValueAsString = optionDelegate;
            }

            /// <summary>
            /// Add a vendor's ID and option value to this converter.
            /// </summary>
            /// <param name="vendor">The vendor to be added.</param>
            public void AddVendor(AppraisalVendorConfig vendor)
            {
                ListOfOptions.Add(string.Format("'{0}':{1}",
                    vendor.VendorId.ToString(),
                    getOptionValueAsString(vendor)));
            }

            /// <summary>
            /// Builds a JavaScript array with this object's name and [ID] : [value] pairs. 
            /// </summary>
            /// <returns>The definition of a Javascript array in string form.</returns>
            public string BuildJSArray()
            {
                string JSArray = "";
                JSArray = "var " + this.ListName + " = {" + String.Join(", ", this.ListOfOptions.ToArray()) + "};";
                return JSArray;
            }
        }

        private string GetGDMSPropertyType(E_sGseSpT t, int sUnitsNum)
        {
            if (sUnitsNum == 2 || sUnitsNum == 3 || sUnitsNum == 4)
                return string.Format("Multi ({0})", sUnitsNum);
            switch(t)
            {
                case E_sGseSpT.Attached:
                    return "Single Family Attached";
                case E_sGseSpT.Condominium:
                case E_sGseSpT.DetachedCondominium:
                case E_sGseSpT.ManufacturedHomeCondominium:
                    return "Condo";
                case E_sGseSpT.Cooperative:
                    return "Coop";
                case E_sGseSpT.Detached:
                    return "Single Family Detached";
                case E_sGseSpT.HighRiseCondominium:
                    return "High Rise";
                case E_sGseSpT.ManufacturedHomeMultiwide:
                case E_sGseSpT.ManufacturedHousing:
                case E_sGseSpT.ManufacturedHousingSingleWide:
                    return "Mobile Home";
                case E_sGseSpT.Modular:
                    return "Prefab";
                case E_sGseSpT.PUD:
                    return "PUD";
                default: return string.Empty;
            }
        }
        private string GetGDMSIntendedUse(E_sLPurposeT t)
        {
            switch (t)
            {
                case E_sLPurposeT.Purchase:
                    return "Purchase";
                case E_sLPurposeT.Refin:
                    return "Refinance";
                case E_sLPurposeT.RefinCashout:
                    return "Cash-out";
                case E_sLPurposeT.Construct:
                    return "Construction";
                case E_sLPurposeT.ConstructPerm:
                    return "Construction to Permanent";
                case E_sLPurposeT.FhaStreamlinedRefinance:
                    return "FHA";
                case E_sLPurposeT.VaIrrrl:
                    return "VA";
                case E_sLPurposeT.Other:
                    return "Other";
                case E_sLPurposeT.HomeEquity:
                    return "Home Equity";
                default: return string.Empty;
            }
        }
        private string GetGDMSLoanType(E_sLT t)
        {
            switch (t)
            {
                case E_sLT.Conventional:
                    return "Conventional";
                case E_sLT.FHA:
                    return "FHA";
                case E_sLT.Other:
                    return "Other";
                case E_sLT.UsdaRural:
                    return "USDA";
                case E_sLT.VA:
                    return "VA";
                default: return string.Empty;
            }
        }
        private string GetGDMSOccupancyType(E_aOccT t)
        {
            switch (t)
            {
                case E_aOccT.Investment:
                    return "Investment";
                case E_aOccT.PrimaryResidence:
                    return "Primary Residence";
                case E_aOccT.SecondaryResidence:
                    return "Second Home";
                default: return string.Empty;
            }
        }

        protected void PageInit(object sender, EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
