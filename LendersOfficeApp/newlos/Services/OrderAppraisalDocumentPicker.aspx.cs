﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDocs;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Services
{
    public partial class OrderAppraisalDocumentPicker : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            IList<EDocument> docs = repo.GetDocumentsByLoanId(LoanID);
            List<DocType> p = EDocumentDocType.GetStackOrderedDocTypesByBroker(Broker).ToList();
            Dictionary<string, int> docTypeIndeces = new Dictionary<string, int>();

            for (int i = 0; i < p.Count; i++)
            {
                docTypeIndeces.Add(p[i].DocTypeId, i);
            }
            DocListing.DataSource = docs.OrderBy(y => GetIndex(y.DocumentTypeId.ToString(), docTypeIndeces));
            DocListing.DataBind();

            var docTypeFolders = new List<string>();
            docTypeFolders.Add("All");
            docTypeFolders.AddRange(p.Select(d => d.Folder.FolderNm).Distinct());
            DocTypeFilter.DataSource = docTypeFolders;
            DocTypeFilter.DataBind();

            EnableJqueryMigrate = false;
            DisplayCopyRight = false;
            RegisterJsScript("jquery.tablesorter.min.js");
        }

        protected override bool EnableJQueryInitCode
        {
            get
            {
                return true;
            }
        }

        public int GetIndex(string id, Dictionary<string, int> indeces)
        {
            int index;
            if (indeces.TryGetValue(id, out index))
            {
                return index;
            }

            return int.MaxValue;
        }
    }
}
