﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Order4506T.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.Order4506T" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Order 4506-T</title>
    <style type="text/css">
        div.BlockMask {
            position: fixed;
        }
    </style>
</head>
<body class="RightBackground">
<script type="text/javascript">

function _init() {
    $('#addOrderBtn').click(f_onAddOrderBtn_Click);
}

var g_eDocumentId = '';
function f_onAddOrderBtn_Click() {
    clearDirty();
    var args = {
        LoanID : ML.sLId,
        AppID : ML.aAppId,
        AppName :<%= AspxTools.JsGetElementById(m_applicationDDL) %>.value,
        VendorId : <%= AspxTools.JsGetElementById(m_vendorDDL) %>.value,
        DocumentID: g_eDocumentId
    };
    
    var result = gService.loanedit.call("Submit", args);
    
    if (!result.error)
    {
        if (result.value.Status === "OK")
        {
            location.href = location.href;
        }
        else
        {
            alert('Error: ' + result.value.ErrorMessage);
        }
    }
    else {
        alert('Error: ' + (result.UserMessage ? result.UserMessage : 'System error.'));
    }
}

function f_checkStatus(button, orderNumber, vendorId, username, password)
{
    $(button).prop('disabled', true);

    var args = {
        LoanId : ML.sLId,
        OrderNumber : orderNumber,
        VendorId : vendorId,
        Username : username,
        Password : password
    };

    var result = gService.loanedit.callAsync(
        /* methodName */ "GetStatus", 
        args, 
        /* keepQuiet */ false, 
        /* bUseParentForError */ false, 
        /* dontHandleError*/ true, 
        /* isAsync */ true,
        /* successCallback */ function(result) { checkStatusSuccess(result); $(button).prop('disabled', false); },
        /* errorCallback */ function(result) { checkStatusFailure(result); $(button).prop('disabled', false); });
}

function checkStatusSuccess(result)
{
    if (result.value.Status === "Unknown" || result.value.Status === "Error")
    {
        if (result.value.ErrorMessage)
        {
            alert('Error: ' + result.value.ErrorMessage);
        }
        else if (result.value.StatusMessage)
        {
            alert(result.value.StatusMessage);
        }
        else
        {
            alert('System error.')
        }
    } 
    else if (result.value.Status === "Login")
    {
        ML.OrderInfo = JSON.parse(result.value.OrderInfo);
        LQBPopup.ShowElement($('#LoginPopup'), { hideCloseButton: true, height: '150' });
    }
    else
    {
        location.href = location.href;
    }
}

function checkStatusFailure(result)
{
    if (result.UserMessage)
    {
        alert('Error: ' + result.UserMessage);
    }
    else {
        alert('Error: System error.');
    }
}

function f_selectEdocs()
{
    var args = {};
    showModal('/newlos/ElectronicDocs/SingleDocumentPicker.aspx?loanid=' + ML.sLId, args, null, null, function(result){ 
        if (result.OK)
        {
            g_eDocumentId = result.Document.DocumentId;
            $('#m_edocFormsTB').val(result.Document.FolderName + ' : ' + result.Document.DocTypeName);
        }
    },{hideCloseButton:true});    
    return false;

}

function toggleStatusDetails(link)
{
    var jqLink = $(link);

    if (jqLink.data('open'))
    {
        jqLink.siblings('#StatusDetailsContainer').empty();
        jqLink.html('details');
        jqLink.data('open', false);
    }
    else
    {
        jqLink.siblings('#StatusDetailsContainer').text(jqLink.prop('title'));
        jqLink.html('hide');
        jqLink.data('open', true);
    }
}

</script>
    <form id="form1" runat="server">
    <div>
    <div class="MainRightHeader">Order 4506-T</div>
    
    <ml:CommonDataGrid ID="m_dg" runat="server" AutoGenerateColumns="false" >
        <Columns>
            <asp:BoundColumn DataField="ApplicationName" HeaderText="Application" />
            <asp:BoundColumn DataField="OrderNumber" HeaderText="Order #" ItemStyle-Width="100px" />
            <asp:BoundColumn DataField="OrderDate" HeaderText="Ordered Date" DataFormatString="{0:g}" ItemStyle-Width="120px"/>
            <asp:TemplateColumn HeaderText="Status" ItemStyle-Width="120px">
                <ItemTemplate>
                    <div><asp:Label ID="StatusDescriptionLabel" runat="server"></asp:Label> <a id="StatusDetailsLink" runat="server" onclick="toggleStatusDetails(this); return false;">details</a><div id="StatusDetailsContainer"></div></div>
                    <asp:Button type="button" runat="server" id="CheckStatusButton" Text="Check Status" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="StatusDate" HeaderText="Status Date" DataFormatString="{0:g}"  ItemStyle-Width="120px"/>
            <asp:TemplateColumn HeaderText="Uploaded Documents">
                <ItemTemplate>
                <ml:PassthroughLiteral ID="UploadDocumentsLinksHtml" runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
        
    </ml:CommonDataGrid>
    <div style="width:100px;margin-left:auto; margin-right:auto;font-weight:bold;" id="m_noRecordLabel" runat="server">No Orders</div>
    <br />
    <table class="InsetBorder" style="width:700px" border="0" cellpadding="2" cellspacing="0">
        <tr><td colspan="2" class="FormTableSubheader">Create Order</td></tr>
        <tr>
            <td class="FieldLabel" style="width:100px">4506-T Vendors:</td>
            <td><asp:DropDownList ID="m_vendorDDL" runat="server" /></td>
        </tr>
        <tr>
            <td class="FieldLabel">Applications:</td>
            <td><asp:DropDownList ID="m_applicationDDL" runat="server" /></td>
        </tr>
        <tr>
            <td class="FieldLabel">Form:</td>
            <td>
                <input type="text" id="m_edocFormsTB" readonly="readonly" style="width:300px" />
                <a href="#" onclick="return f_selectEdocs();">select from eDocs</a>
            </td>
        </tr>
        <tr>
            <td colspan="2"><input type="button" id="addOrderBtn" value="Add Order"/></td>
        </tr>
    </table>
        <div class="Hidden" id="LoginPopup">
            <div class="MainRightHeader">Enter 4506-T Credentials</div>
            <div style="margin-top:30px; margin-bottom: 20px">
                <div class="FieldLabel">
                    Username: <input class="Input loginUsername" type="text" />
                </div>
                <div class="FieldLabel">
                    Password: <input class="Input loginPassword" type="password" />
                </div>
            </div>
            <input type="button" value="Submit" onclick="f_checkStatus(this, ML.OrderInfo.OrderNumber, ML.OrderInfo.VendorId, $(this).parent().find('input.loginUsername').val() || '', $(this).parent().find('input.loginPassword').val() || ''); LQBPopup.Hide();" /> 
            <input type="button" value="Cancel" onclick="LQBPopup.Hide()"/>
        </div>
    </div>
    </form>
</body>
</html>
