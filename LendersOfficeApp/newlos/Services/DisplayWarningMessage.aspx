<%@ Page language="c#" Codebehind="DisplayWarningMessage.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Services.DisplayWarningMessage" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>DisplayWarningMessage</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
		<link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="FlowLayout" bgcolor=gainsboro>
	<script language=javascript>
<!--
function _init() {
  resize(500, 260);
}
function f_choice(choice) {
  var args = window.dialogArguments || {};
  args.choice = choice;
  onClosePopup(args);
  
}
//-->
	</script>
	   <h4 class="page-header">Warning</h4>
    <form id="DisplayWarningMessage" method="post" runat="server">
      <table width="100%">
        <tr><td style="FONT-WEIGHT:bold;COLOR:black">Warning message regarding this credit provider posted on&nbsp;<%=AspxTools.HtmlString(RequestHelper.GetQueryString("WarningStartD"))%>:</td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td style="FONT-WEIGHT:bold;COLOR:black"><%=AspxTools.HtmlString(RequestHelper.GetQueryString("WarningMsg"))%></td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td align=center><input type=button value="Try it anyway" class="buttonstyle" onclick="f_choice(0);">&nbsp;<input type=button value="Cancel" class="buttonstyle" onclick="f_choice(1);"></td></tr>
      </table>
    </form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
	
  </body>
</html>
