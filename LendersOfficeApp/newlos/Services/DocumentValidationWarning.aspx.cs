﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOfficeApp.common.ModalDlg;
using DataAccess;
using LendersOffice.Common;
using System.Xml.Linq;

namespace LendersOfficeApp.newlos.Services
{
    public partial class DocumentValidationWarning : BaseLoanPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            m_cancelBtn.Click += new EventHandler(m_cancelBtn_Click);
            m_continueBtn.Click += new EventHandler(m_continueBtn_Click);
        }

        void m_continueBtn_Click(object sender, EventArgs e)
        {

            cModalDlg.CloseDialog(this, new string[] { 
                "OK=true", 
                "ContinueProcessing=true",
                "ValidationWarningId='dylan'"
            });
        }

        void m_cancelBtn_Click(object sender, EventArgs e)
        {

            // TODO - Discard the DocResponse
            cModalDlg.CloseDialog(this, new string[] { "OK=true" });
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void LoadData()
        {
            string cacheId = RequestHelper.GetSafeQueryString("cacheid");
            string xml = AutoExpiredTextCache.GetFromCache(cacheId);

            string errorMessage = ErrorMessages.Generic;

            if (string.IsNullOrEmpty(xml) == false)
            {
                XElement element = XElement.Parse(xml, LoadOptions.PreserveWhitespace);

                errorMessage = element.Element("Message").Value;

                if (element.Element("Parameter").Attribute("sLNm") != null)
                {
                    sLNm.Text = element.Element("Parameter").Attribute("sLNm").Value;
                }
                //string passthroughParameter = element.Element("Parameter").ToString();

                //ClientScript.RegisterHiddenField("PassthroughParameterXml", passthroughParameter);
            }
            else
            {
                // Hide continue button when we could not retrieve warning msg.
                m_continueBtn.Visible = false; 
            }

            ErrorRepeater.DataSource = errorMessage.Split(new string[] { "\n" }, StringSplitOptions.None);
            ErrorRepeater.DataBind();
        }
    }
}
