using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.OnlineDocuments;

namespace LendersOfficeApp.newlos.Services
{
	public partial class OnlineDocumentsService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "Submit":
                    Submit();
                    break;
            }
        }
        private void Submit() 
        {
            Guid sLId = GetGuid("LoanID");
            string userName = GetString("UserName");
            string password = GetString("Password");
            string customerCode = GetString("CustomerCode");
            int scenarioId = GetInt("ScenarioId", 0);
            string lenderLoanIdentifier = GetString("LenderLoanIdentifier");

            OnlineDocumentsRequest request = new OnlineDocumentsRequest();
            request.CustomerCode = customerCode;
            request.ScenarioID = scenarioId;
            request.LoginName = userName;
            request.Password = password;
            request.sLId = sLId;
            request.LenderLoanIdentifier = lenderLoanIdentifier;

            OnlineDocumentsResponse response = OnlineDocumentsServer.Submit(request);

            if (null != response) 
            {
                SetResult("LoginUrl", response.LoginUrl);
                SetResult("Result", response.Result);
                SetResult("ResultText", response.ResultText);
            } 
            else 
            {
                SetResult("Result", "-1");
                SetResult("ResultText", ErrorMessages.Generic);

            }



        }
	}
}
