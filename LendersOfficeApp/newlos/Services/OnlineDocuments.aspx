<%@ Page language="c#" Codebehind="OnlineDocuments.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Services.OnlineDocuments" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>OnlineDocuments</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </HEAD>
  <body MS_POSITIONING="FlowLayout" class="RightBackground" style="MARGIN-LEFT:0px">
	<script language=javascript>
<!--
function f_onSubmit() {
  var args = new Object();
  args["LoanID"] = ML.sLId;
  args["CustomerCode"] = <%= AspxTools.JsGetElementById(CustomerCode) %>.value;
  args["UserName"] = <%= AspxTools.JsGetElementById(UserName) %>.value;
  args["Password"] = <%= AspxTools.JsGetElementById(Password) %>.value;
  args["ScenarioId"] = <%= AspxTools.JsGetElementById(ScenarioId) %>.value;
  args["LenderLoanIdentifier"] = <%= AspxTools.JsGetElementById(sLNm) %>.value;

  var result = gService.loanedit.call("Submit", args);
  if (!result.error) {
    var ret = result.value["Result"];
    if (ret == "0" || ret == "1") {
      f_clearError();
      window.open(result.value["LoginUrl"]);
    } else {
      f_displayError(result.value["ResultText"]);
    }
      
  } else {
    f_displayError(result.UserMessage);
  }

}
function f_displayError(errMsg) {
  document.getElementById("ErrorMsg").innerHTML = "ERROR:<br>" + errMsg;
}
function f_clearError() {
  document.getElementById("ErrorMsg").innerText = "";
}
//-->
</script>

    <form id="OnlineDocuments" method="post" runat="server">
<table cellspacing=0 cellpadding=0 width="100%" border=0>
<tr><td class="MainRightHeader" nowrap>Online Documents Integration</td></tr>
<tr><td style='PADDING-LEFT:10px'>
      <TABLE id=Table1 cellSpacing=0 cellPadding=0 width=300 border=0>
        <TR>
          <TD noWrap class="FieldLabel">Lender ID</TD>
          <TD noWrap><asp:TextBox id=CustomerCode runat="server" NotForEdit="true" MaxLength="4" Width="48px"></asp:TextBox>&nbsp;(4 
            digits)</TD></TR>
        <TR>
          <TD noWrap class="FieldLabel">User Name</TD>
          <TD noWrap><asp:TextBox id=UserName runat="server" NotForEdit="true" Width="150px"></asp:TextBox></TD></TR>
        <TR>
          <TD noWrap class="FieldLabel">Password</TD>
          <TD noWrap><asp:TextBox id=Password runat="server" NotForEdit="true" TextMode="Password" Width="150px"></asp:TextBox></TD></TR>
        <TR>
          <TD noWrap class="FieldLabel">Scenario ID</TD>
          <TD noWrap><asp:TextBox id=ScenarioId runat="server" NotForEdit="true" Width="52px">0</asp:TextBox> (Leave value at 0 if unknown.)</TD></TR>
        <TR>
          <TD noWrap class="FieldLabel">Loan 
            Identifier&nbsp;&nbsp;</TD>
          <TD noWrap><asp:TextBox id=sLNm runat="server" NotForEdit="true" Width="221px"></asp:TextBox></TD></TR>
        <TR>
          <TD noWrap></TD>
          <TD noWrap><INPUT type=button value=Submit onclick="f_onSubmit();"></TD></TR></TABLE></td></tr>
<tr><td style='PADDING-LEFT:10px;FONT-WEIGHT:bold;COLOR:red'><div id="ErrorMsg"></div></td></tr>
  
</table>
     </form>
	
  </body>
</HTML>
