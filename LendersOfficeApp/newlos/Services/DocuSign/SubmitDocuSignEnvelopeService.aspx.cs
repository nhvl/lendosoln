﻿#region Auto-generated code
namespace LendersOfficeApp.newlos.Services.DocuSign
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Admin.DocuSign;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Conversions.ComplianceEagleIntegration;
    using LendersOffice.Integration.DocumentSigning;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Service page for the DocuSign Envelope page.
    /// </summary>
    public partial class SubmitDocuSignEnvelopeService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Process a web service call.
        /// </summary>
        /// <param name="methodName">The method to call.</param>
        protected override void Process(string methodName)
        {
            switch (methodName) 
            {
                case nameof(GetDocInfo):
                    GetDocInfo();
                    break;
                case nameof(SubmitEnvelope):
                    SubmitEnvelope();
                    break;
                case nameof(GetRecipientView):
                    GetRecipientView();
                    break;
                case nameof(GetBorrowers):
                    GetBorrowers();
                    break;
                default:
                    throw new ArgumentException($"Unknown method: {methodName}.");
            }
        }

        /// <summary>
        /// Gets a list of recipient objects corresponding to the borrowers on the loan.
        /// </summary>
        private void GetBorrowers() => this.SetResult("recipients", DocuSignEnvelopeOrderingData.GetBorrowerRecipients(CPageData.CreateUsingSmartDependencyForLoad(this.GetGuid("loanid"), typeof(DocuSignEnvelopeOrderingData))));

        /// <summary>
        /// Gets a recipient's embedded view URL.
        /// </summary>
        private void GetRecipientView()
        {
            string liveSigningJson = DocuSignRecipientView.GetLiveSigningViewModel(
                    user: PrincipalFactory.CurrentPrincipal,
                    loanId: this.GetGuid("loanid"),
                    envelopeId: this.GetInt("envelopeId"),
                    signingOrder: this.GetInt("signingOrder", 0),
                    eventString: this.GetString("event", null),
                    state: this.GetString("state", null));
            this.SetResult("RecipientViewModel", liveSigningJson);
        }

        /// <summary>
        /// Gets document information for the DocuSign Envelope UI.
        /// </summary>
        private void GetDocInfo()
        {
            List<Guid> parsedDocIds = SerializationHelper.JsonNetDeserialize<List<Guid>>(this.GetString("docIds"));
            List<string> rolesToExclude = SerializationHelper.JsonNetDeserialize<List<string>>(this.GetString("roles"));
            var loan = CPageData.CreateUsingSmartDependencyForLoad(this.GetGuid("loanid"), typeof(DocuSignEnvelopeOrderingData));
            var repository = EDocumentRepository.GetUserRepository();
            var documents = parsedDocIds.Select(docId => repository.GetDocumentById(docId));
            this.SetResult("docsAndRecipients", DocuSignEnvelopeOrderingData.GetDocumentAndRecipientJsonString(documents, loan, rolesToExclude));
        }

        /// <summary>
        /// Submits the envelope to DocuSign.
        /// </summary>
        private void SubmitEnvelope()
        {
            AbstractUserPrincipal user = PrincipalFactory.CurrentPrincipal;
            if (!user.CanCreateDocuSignEnvelopes())
            {
                this.SetResult("ErrorMessage", "You do not have permission to create signing envelopes.");
                return;
            }

            CPageData loanData = CPageData.CreateUsingSmartDependency(GetGuid("loanId"), typeof(SubmitDocuSignEnvelopeService));
            loanData.InitLoad();

            LenderDocuSignSettings docuSignSettings = LenderDocuSignSettings.Retrieve(user.BrokerId);
            DocuSignAuthenticationData docuSignLoginData = docuSignSettings.ToAuthenticationData();
            Result<EnvelopeRequest> requestResult = BuildEnvelopeRequest(user, loanData);
            if (requestResult.HasError)
            {
                this.SetResult("ErrorMessage", requestResult.Error.Message);
                return;
            }

            EnvelopeRequest request = requestResult.Value;

            // Until it's set up with generic locator, instantiate the driver right here.
            var driver = new DocuSignDriver();

            DateTime requested = DateTime.Now;
            Result<EnvelopeCreateSummary> summaryResult = driver.CreateEnvelope(docuSignLoginData, request);
            if (summaryResult.HasError)
            {
                Tools.LogError(summaryResult.Error);
                this.SetResult("ErrorMessage", "An error occurred communicating with DocuSign.");
                return;
            }
            else
            {
                var envelope = DocuSignEnvelope.CreateAndSave(request, summaryResult.Value, requested, user, user.DisplayName, EmployeeDB.RetrieveById(user.BrokerId, user.EmployeeId).Email, loanData.sLId);

                if (envelope.Recipients.Any(recipient => recipient.Type == RecipientType.InPersonSigner))
                {
                    string state = DocuSignRecipientView.GenerateStateToken(user, loanData.sLId);
                    var callbackUri = DocuSignRecipientView.GetRecipientViewCallbackUri(
                        urlBase: HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + Tools.VRoot,
                        loanId: loanData.sLId,
                        envelopeId: envelope.Id,
                        nextSigningOrder: 0,
                        state: state).ToString();
                    this.SetResult("RecipientViewUrl", callbackUri);
                }

                this.SetResult("ResponseMessage", $"Created envelope ID {summaryResult.Value.Identifier} with status \"{summaryResult.Value.Status}\" at {summaryResult.Value.StatusDateTime}.");
            }
        }
        
        /// <summary>
        /// Build a request to create an envelope in DocuSign.
        /// </summary>
        /// <param name="user">The user making the request.</param>
        /// <param name="loanData">The loan file to create the request from.</param>
        /// <returns>A new envelope request with all the necessary data, or a failure result.</returns>
        private Result<EnvelopeRequest> BuildEnvelopeRequest(AbstractUserPrincipal user, CPageData loanData)
        {
            List<Guid> documentIds = SerializationHelper.JsonNetDeserialize<List<Guid>>(this.GetString("documentIds"));
            Dictionary<string, EnvelopeRecipient> recipientDictionary = DocuSignEnvelopeOrderingData.RecipientDictionary(user, this.GetString("recipients"));
            IEnumerable<EnvelopeRecipient> recipients = recipientDictionary.Values.Distinct();
            var docusignInfo = DocuSignEnvelopeOrderingData.DocumentInfoForDocuSign(user, documentIds, recipientDictionary);

            if (docusignInfo.HasError)
            {
                return Result<EnvelopeRequest>.Failure(docusignInfo.Error);
            }

            var docusignDocuments = docusignInfo.Value.Item1;
            var docusignTabs = docusignInfo.Value.Item2;

            var docuSignSettings = LenderDocuSignSettings.Retrieve(user.BrokerId);
            var docuSignBranchSettings = BranchDocuSignSettings.Retrieve(user.BrokerId, loanData.sBranchId);

            EnvelopeRequest request = new EnvelopeRequest(
                emailSubject: $"{BranchDB.RetrieveById(loanData.sBrokerId, loanData.sBranchId).DisplayNm}: Document update for loan {loanData.sLNm} {docusignDocuments.First().Name}",
                emailBody: this.GetString("emailMessage", string.Empty),
                webhookUrl: LqbAbsoluteUri.Create(ConstStage.IntegrationWebhookDefaultBaseUrl + "/DocuSignEnvelopeInformationHandler.aspx").ForceValue(),
                recipients: recipients,
                documents: docusignDocuments,
                tabs: docusignTabs,
                docuSignUserName: docuSignSettings.DocuSignUserName,
                docuSignEmail: docuSignSettings.DocuSignEmail,
                docuSignBrandId: LenderDocuSignSettings.ComputeBrandId(loanData.sBranchChannelT, docuSignSettings, docuSignBranchSettings));
            return Result<EnvelopeRequest>.Success(request);
        }
    }
}
