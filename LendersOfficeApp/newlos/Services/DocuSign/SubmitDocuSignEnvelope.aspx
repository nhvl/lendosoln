﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubmitDocuSignEnvelope.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.DocuSign.SubmitDocuSignEnvelope" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Submit DocuSign Envelope</title>
</head>
<body id="app" ng-controller="DocuSignEnvelopeController as controller" ng-app="DocuSignEnvelope">
    <form id="aspform" runat="server"></form>
    <div class="MainRightHeader float-top faint-shadow z1" class="ng-cloak">
        <span ng-show="!loaded">Loading </span>{{pageTitle}}<span ng-show="!loaded">...<span class="fa fa-spin fa-spinner fa-fw"></span></span>
    </div>
    <div class="ng-cloak" ng-show="loaded && !loadSuccess">
        Something went wrong communicating with the server:
        <br />{{errorMessage}}
        <div class="bottomButtons"><button ng-click="closeWindow()">Close</button></div>
    </div>
    <div ng-view></div>
</body>
</html>
