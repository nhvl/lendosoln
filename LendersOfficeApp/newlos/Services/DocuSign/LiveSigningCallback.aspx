﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LiveSigningCallback.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.DocuSign.LiveSigningCallback" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title>LendingQB - DocuSign In-Person Signing</title>
    <style type="text/css">
        body {
            background-color: gainsboro;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 16px;
            text-align: center;
        }
        .box-container {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
        }
        button.mdc-button {
            font-weight: bold;
            margin: 8px 0;
        }
        button.mdc-button:hover {
            background-color: rgba(128, 0, 0, .12);
        }
        button.mdc-button:focus {
            background-color: rgba(128, 0, 0, .25);
        }
        button.mdc-button:active {
            background-color: rgba(128, 0, 0, .4);
        }
        button:not(:disabled).mdc-button {
            color: maroon;
        }
        button:not(:disabled).mdc-button--stroked {
            border-color: maroon;
        }
        .error-container {
            border: 1px solid black;
            border-radius: 2px;
            text-align:left;
            max-width: 400px;
            padding: 3px;
            margin: 2px;
        }
        .error-message {
            font-weight: bold;
        }
        .error-description {
            font-style: italic;
        }
        .recipient-name {
            color: maroon;
        }
    </style>
</head>
<body ng-app="DocuSignCallback" ng-controller="LiveCallbackController">
    <div class="box-container">
        <div ng-show="status === 'loading'">LendingQB is processing the signing envelope.</div>
        <div ng-show="status === 'success' && lastRecipient && lastRecipient.name">
            Signing for <span class="recipient-name">{{lastRecipient.name}}</span> succeeded.
        </div>
        <div ng-show="status === 'systemError' || status === 'eventError'">
            <div ng-show="status === 'eventError'">
                <div ng-show="lastRecipient.name">
                    Signing for <span class="recipient-name">{{lastRecipient.name}}</span> returned the following error:
                </div>
                <div class="error-container">
                    <div ng-show="signingError.code">
                        <span class="error-message">Code:</span> <span class="error-description">{{signingError.code}}</span>
                    </div> 
                    <div ng-show="signingError.code && signingError.description">
                        <span class="error-message">Description:</span> <span class="error-description">{{signingError.description}}</span>
                    </div>
                </div>
            </div>
            <div ng-show="status === 'systemError' && errorMessage">
                There was a system error: <span class="error-description">{{errorMessage}}</span>
            </div>
            <div ng-show="status === 'systemError' && !errorMessage">
                <span class="error-message">There was an unknown error.</span>
            </div>
            <br /> Would you like to try again?
        </div>
        <div ng-show="status === 'stateError'">
            Please confirm: Would you like to start a DocuSign signing session for <span class="recipient-name">{{nextRecipient.name}}</span>?
        </div>
        <div ng-show="status === 'success'">
            You will be redirected to a DocuSign signing session for <span class="recipient-name">{{nextRecipient.name}}</span> in {{secondsToAutoRedirect}} seconds.
        </div>
        <div ng-show="status === 'complete'">
            There are no more in-person signers. The window will automatically close in {{secondsToAutoRedirect}} seconds.
        </div>
        <div>
            <button class="mdc-button" ng-click="closeWindow()" >{{closeMessage()}}</button>
            <button class="mdc-button" ng-show="status === 'eventError' || status === 'systemError'" ng-click="continueSigning()" >Try Again</button>
            <button class="mdc-button" ng-show="status === 'success' || status === 'stateError'" ng-click="continueSigning()" >{{continueMessage()}}</button>
        </div>
    </div>
</body>
</html>
