﻿#region Auto-generated code
namespace LendersOfficeApp.newlos.Services.DocuSign
#endregion
{
    using System;
    using LendersOffice.Common;

    /// <summary>
    /// Codebehind for the "DocuSign Live Signing Callback" page.
    /// This is intended to be a very simple page that displays immediately and uses 
    /// AJAX calls to <see cref="SubmitDocuSignEnvelopeService"/>  to determine where 
    /// (if anywhere) to send the user to continue signing.
    /// </summary>
    public partial class LiveSigningCallback : BaseServicePage
    {
        /// <summary>
        /// Specifies that Internet Explorer should display in Edge mode. Angular doesn't load properly for older versions.
        /// </summary>
        /// <returns>Edge mode.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode() => E_XUAComaptibleValue.Edge;

        /// <summary>
        /// Page initialization event handler.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("simpleservices.js");
            this.RegisterJsScript("angular-1.5.5.min.js");
            this.RegisterJsScript("angularModules/LqbForms.module.js");
            this.RegisterJsScript("DocuSignLiveCallback.js");

            this.RegisterCSS("material-components-web.min.css");
        }
    }
}
