﻿#region Auto-generated code
namespace LendersOfficeApp.newlos.Services.DocuSign
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using EDocs;
    using LendersOffice.Admin.DocuSign;
    using LendersOffice.Common;
    using LendersOffice.Integration.DocumentSigning;
    using LendersOffice.Security;

    /// <summary>
    /// Codebehind for the "Submit DocuSign Envelope" page.
    /// </summary>
    public partial class SubmitDocuSignEnvelope : BaseServicePage
    {
        /// <summary>
        /// Whether the current user has the needed permissions and settings for this page.
        /// </summary>
        private bool currentUserHasDocuSignEnabled = PrincipalFactory.CurrentPrincipal.CanCreateDocuSignEnvelopes();

        /// <summary>
        /// Gets a JSON string containing the necessary UI information for the given list of documents.
        /// </summary>
        /// <param name="documents">The documents to load data for.</param>
        /// <param name="loanId">The loan ID of the loan the documents are from.</param>
        /// <returns>A JSON string to send to the DocuSign Envelope UI.</returns>
        internal static string GetDocumentObjectString(IEnumerable<EDocument> documents, Guid loanId)
            => SerializationHelper.JsonNetAnonymousSerialize(DocuSignDocumentData.GetDocumentObjects(documents, CPageData.CreateUsingSmartDependencyForLoad(loanId, typeof(SubmitDocuSignEnvelope))));

        /// <summary>
        /// Specifies that Internet Explorer should display in Edge mode. Angular doesn't load properly for older versions.
        /// </summary>
        /// <returns>Edge mode.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode() => E_XUAComaptibleValue.Edge;

        /// <summary>
        /// Page initialization event handler.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            if (!currentUserHasDocuSignEnabled)
            {
                throw new AccessDenied();
            }

            this.EnableJqueryMigrate = false;
            Guid loanId = RequestHelper.GetGuid("loanid");
            CPageData loan = CPageData.CreateUsingSmartDependencyForLoad(loanId, typeof(SubmitDocuSignEnvelope));

            IEnumerable<Guid> parsedDocIds = RequestHelper.GetQueryString("docIds").Split(',').Select(guidString => new Guid(guidString));
            var repository = EDocumentRepository.GetUserRepository();
            List<EDocument> docs = parsedDocIds.Select(docId => repository.GetDocumentById(docId)).ToList();
            HashSet<E_AgentRoleT> roles = new HashSet<E_AgentRoleT>();
            foreach (var doc in docs)
            {
                foreach (var annotation in doc.ESignTags.AnnotationList)
                {
                    if (annotation.AssociatedRole.HasValue)
                    {
                        roles.Add(annotation.AssociatedRole.Value);
                    }
                }
            }

            IEnumerable<Tuple<Guid, E_BorrowerModeT>> borrowerIds = DocuSignEnvelopeOrderingData.GetBorrowerIdsForDocs(docs);

            var edocInfoList = DocuSignDocumentData.GetDocumentObjects(docs, loan);
            var docuSignSettings = LenderDocuSignSettings.Retrieve(PrincipalFactory.CurrentPrincipal.BrokerId);

            var enabledMfaList = Enum.GetValues(typeof(MfaOptions))
                .Cast<MfaOptions>().Except(new MfaOptions[] { MfaOptions.NoSelection })
                .Where(option => docuSignSettings.EnabledMfaOptions.HasFlag(option))
                .ToDictionary(option => option.ToString("G"), option => new { value = (int)option, description = EnumUtilities.GetDescription(option) });

            this.RegisterService("main", "/newlos/services/docusign/SubmitDocuSignEnvelopeService.aspx");

            this.RegisterJsObjectWithJsonNetSerializer("recipients", DocuSignEnvelopeOrderingData.GetRecipientObjects(loan, roles, borrowerIds));
            this.RegisterJsObjectWithJsonNetSerializer("enabledMfaOptions", enabledMfaList);
            this.RegisterJsGlobalVariables("defaultMfaValue", (int)docuSignSettings.DefaultMfaOption);
            this.RegisterJsGlobalVariables("docuSignUserName", docuSignSettings.DocuSignUserName);
            this.RegisterJsObjectWithJsonNetSerializer("edocsInfoList", edocInfoList);
            this.RegisterJsGlobalVariables("sLId", RequestHelper.GetGuid("loanid"));

            this.RegisterJsScript("angular-1.5.5.min.js");
            this.RegisterJsScript("angular-route.1.4.8.min.js");
            this.RegisterJsScript("angularModules/LqbForms.module.js");
            this.RegisterJsScript("DocuSignEnvelope.js");
            this.RegisterJsScript("LQBPopup.js");

            this.RegisterCSS("font-awesome.css");
            this.RegisterCSS("SubmitDocuSignEnvelope.css");
            this.RegisterCSS("material-components-web.min.css");
            base.OnInit(e);
        }
    }
}
