﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Integration.TotalScorecard;
using System.Web.UI.HtmlControls;
using CommonLib;

namespace LendersOfficeApp.newlos.Services
{
    public partial class FHADataConflictAlert : LendersOfficeApp.newlos.BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.DisplayCopyRight = false;
            LoadUIData();
            m_cancelButton.Attributes.Add("onClick", "javascript:onClosePopup();");
        }

        protected DataConflictRow GetItem(RepeaterItem container)
        {
            return (DataConflictRow)container.DataItem;
        }

        protected void OnCancelClick(object sender, EventArgs e)
        {
            LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this.Page, new string[] { "usercommand='cancel'" });
        }

        private void LoadUIData()
        {
            CFHATotalAuditData dataLoan = new CFHATotalAuditData(this.LoanID);
            dataLoan.InitLoad();

            DataConflictRows.DataSource = GenerateConflictData(dataLoan);
            DataConflictRows.DataBind();
        }

        private List<DataConflictRow> GenerateConflictData(CPageData loan)
        {
            // This function is very calculation-intensive and should go into the data layer, but my refactor is focused on trying to sanitize the literals from the UI
            // TODO: Move calculation of FHA data conflicts to data layer
            decimal totalFHATransIncome = 0;
            decimal total1003AssetsAvail = 0;
            foreach (CAppData app in loan.Apps)
            {
                totalFHATransIncome += app.aFHAGrossMonI;
                total1003AssetsAvail += app.aAsstLiqTot;
            }

            char[] parentheses = { '(', ')' };
            var conflicts = new List<DataConflictRow>();
            if (loan.sLTotI != totalFHATransIncome)
            {
                conflicts.Add(new DataConflictRow("Applicant(s) Combined Monthly Income", loan.sLTotI_rep, loan.m_convertLos.ToMoneyString(totalFHATransIncome, FormatDirection.ToRep)));
            }

            try
            {
                if (loan.sProFirstMPmt != loan.sFHAPro1stMPmt)
                {
                    conflicts.Add(new DataConflictRow("Contract P & I", loan.sProFirstMPmt_rep, loan.sFHAPro1stMPmt_rep));
                }
            }
            catch (OverflowException oe)
            {
                Tools.LogError("Unable to calculate sProFirstMPmt for FHA TOTAL data conflict alert: " + oe.ToString());
            }

            if (loan.sTransNetCash != loan.sTransmFntc)
            {
                conflicts.Add(new DataConflictRow("Total Cash Requirements", loan.sTransNetCash_rep.Trim(parentheses), loan.sTransmFntc_rep.Trim(parentheses)));
            }

            if (total1003AssetsAvail != loan.sVerifAssetAmt)
            {
                conflicts.Add(new DataConflictRow("Assets Available", loan.m_convertLos.ToMoneyString(total1003AssetsAvail, FormatDirection.ToRep), loan.sVerifAssetAmt_rep));
            }

            if (loan.sConcurSubFin != loan.sFHASecondaryFinancingAmt)
            {
                conflicts.Add(new DataConflictRow("Secondary Financing Amount", loan.sConcurSubFin_rep, loan.sFHASecondaryFinancingAmt_rep));
            }

            if (loan.sTotCcPbs != loan.sFHASellerContribution)
            {
                conflicts.Add(new DataConflictRow("Seller Concessions", loan.sTotCcPbs_rep, loan.sFHASellerContribution_rep));
            }

            return conflicts;
        }

        protected class DataConflictRow
        {
            public DataConflictRow(string description, string value1003, string valueFhaTransmittalSummary)
            {
                this.Description = description;
                this.Value1003 = value1003;
                this.ValueFhaTransmittalSummary = valueFhaTransmittalSummary;
            }

            public string Description { get; }
            public string Value1003 { get; }
            public string ValueFhaTransmittalSummary { get; }
        }
    }
}