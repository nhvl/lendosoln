﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocuTechDownload.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.DocuTechDownload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <script type="text/javascript">
        window.onload = function() {
            var errorMsg = document.getElementById('ErrorMessage').value;
            if (errorMsg !== '') {
                alert(errorMsg);
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:HiddenField ID="ErrorMessage" runat="server" Value="" />
    </div>
    </form>
</body>
</html>
