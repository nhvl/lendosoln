using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

using LendersOffice.CreditReport;

namespace LendersOfficeApp.newlos.Services
{
    /// <summary>
    /// Summary description for ViewCreditMenu.
    /// </summary>
    public partial class ViewCreditMenu : BasePage
    {
        protected void PageInit(object sender, EventArgs e)
        {
            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);
        }

        protected void PageLoad(object sender, System.EventArgs e)
		{
            Guid dbFileKey = RequestHelper.GetGuid("dbfilekey", Guid.Empty);
            Guid applicationID = RequestHelper.GetGuid("applicationid", Guid.Empty);
            Guid loanID = RequestHelper.GetGuid("loanid", Guid.Empty);

            if (Guid.Empty != loanID)
            {
                CPageData dataLoan = new CCreditReportViewData(loanID);
                dataLoan.InitLoad();

                CAppData dataApp = dataLoan.GetAppData(applicationID);
                ICreditReportView primaryCreditReportView = dataApp.CreditReportView.Value;
                ICreditReportView lqiCreditReportView = dataApp.LqiCreditReportView.Value;

                RegisterJsGlobalVariables("HasPrimaryReportPdf", primaryCreditReportView != null && primaryCreditReportView.HasPdf);
                RegisterJsGlobalVariables("HasLqiReportPdf", lqiCreditReportView != null && lqiCreditReportView.HasPdf);

                // Figure out which report type to show.
                CreditReportTypes type = CreditReportTypes.Primary;
                Enum.TryParse(RequestHelper.GetSafeQueryString("type"), true, out type);

                switch (type)
                {
                    case CreditReportTypes.Primary:
                        primaryCreditReport.Checked = true;
                        break;
                    case CreditReportTypes.Lqi:
                        lqiCreditReport.Checked = true;
                        break;
                    default:
                        throw new UnhandledEnumException(type);
                }
            }
            else if (dbFileKey != Guid.Empty)
            {
                tdReportTypes.Visible = false;
                ICreditReportView creditReportView = CAppBase.GetCreditReportViewByFileDbKey(dbFileKey).Value;
                RegisterJsGlobalVariables("HasDbReportPdf", creditReportView != null && creditReportView.HasPdf);
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
            EnableJqueryMigrate = false;
            base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
		#endregion
	}
}
