﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AutoDisclosureAuditResults.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.AutoDisclosureAuditResults" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Audit Results</title>
    <style type="text/css">
        .EditLink
        {
            cursor: pointer;
        }

        #AuditList
        {
            list-style-type: none;
            list-style-image: none;
            list-style-position: outside;
            margin-left: 0px;
            margin-top: 0px;
            padding-top: 5px;
            padding-left: 5px;
        }

        span.AuditEntry
        {
            font-weight: bold;
        }

        span.AuditEntry.FATAL
        {
            color: Red;
        }
        
        span.AuditEntry.CRITICAL
        {
            color: Red;
        }
        
        span.AuditEntry.WARNING
        {
            color: Orange;
        }
        
        #AuditResults
        {
            height: 400px;
            overflow-y: auto; 
        }
        
        body
        {
            background: gainsboro;
        }
        
        .Header
        {
            padding-top: 2px;
            padding-left: 2px;
            background-color: darkgray;
            color: navy;
            font-weight: bold;
            height: 15px;
        }
        
        .Center
        {
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="Header">
        <ml:EncodedLiteral runat="server" ID="LoanNumber"></ml:EncodedLiteral> -
        Disclosure Audit Results
    </div>
    <div id="AuditResults">
        <ml:PassthroughLiteral runat="server" ID="AuditResultHtml"></ml:PassthroughLiteral>
    </div>
    <div class="Center">
        <input type="button" value="OK" onclick="onClosePopup();" />
    </div>
    </form>
</body>
</html>
