<!DOCTYPE html>
<html lang="en">
    <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <%@ Page language="c#" Codebehind="ViewCreditFrame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Services.ViewCreditFrame" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
    <title>Credit Report</title>
    <script>
        function setBody(type)
        {
            var body = document.getElementById('body');
            body.src = "ViewCredit.aspx?loanid=" + <%=AspxTools.JsString(RequestHelper.GetGuid("loanid"))%> + "&applicationid=" + <%=AspxTools.JsString(RequestHelper.GetGuid("applicationid"))%> + "&type=" + type;
        }
    </script>
  </head>
  <body class="body-iframe">
  <h4 class="page-header">Credit Report</h4>

      <form runat="server">
          <div class="top">
              <iframe class="height-50px" id="menu" name=menu src=<%=AspxTools.SafeUrl("ViewCreditMenu.aspx?loanid=" + RequestHelper.GetGuid("loanid") + "&applicationid=" + RequestHelper.GetGuid("applicationid") + "&type=" + AspxTools.JsNumeric(Type))%> scrolling=no></iframe>
          </div>
          <div class="bottom">
              <iframe id="body" name=body src=<%=AspxTools.SafeUrl("ViewCredit.aspx?loanid=" + RequestHelper.GetGuid("loanid") + "&applicationid=" + RequestHelper.GetGuid("applicationid") + "&type=" + AspxTools.JsNumeric(Type))%>></iframe>
          </div>
      </form>
  </body>
</html>
