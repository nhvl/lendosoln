﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LendersOfficeApp.newlos.Services
{
    public partial class FannieMaeEarlyCheck : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Page_Init(object sender, EventArgs e)
        {
            this.PageID = "FannieMaeEarlyCheck";
            this.PageTitle = "Fannie Mae EarlyCheck";

            this.RegisterJsScript("LQBPrintFix.js");

            if (string.IsNullOrEmpty(Broker.FnmaEarlyCheckUserName) == false &&
                string.IsNullOrEmpty(Broker.FnmaEarlyCheckInstutionId) == false)
            {
                LoginPanel.Visible = false;
            }
        }
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;

            base.OnInit(e);
        }
    }
}
