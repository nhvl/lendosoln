/// Author: David Dao

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos.Services
{
	public partial class ViewDUFindingMenu : BaseLoanPage
	{
        protected bool m_isDisplaySampleTotalScorePreview = false;

		protected void PageLoad(object sender, System.EventArgs e)
		{
            // 2/5/2010 dd - Only broker with this option turn on and user must be in administrator role.

            if (Broker.HasTotalScorePreviewFeature)
            {
                if (User.IsInRole(ConstApp.ROLE_ADMINISTRATOR))
                {
                    CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ViewDUFindingMenu));
                    dataLoan.InitLoad();

                    m_isDisplaySampleTotalScorePreview = dataLoan.sTotalScoreTempCertificateXmlContent.Value != "";
                }
            }
            m_btnViewTotalScore.Visible = m_isDisplaySampleTotalScorePreview;
            this.DisplayCopyRight = false;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
