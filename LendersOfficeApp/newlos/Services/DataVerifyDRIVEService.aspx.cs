﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using LendersOffice.Constants;
using DataAccess;

namespace LendersOfficeApp.newlos.Services
{
    public partial class DataVerifyDRIVEService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "SaveData":
                    SaveData();
                    break;
                case "CalculateData":
                    break;
                default:
                    throw new NotImplementedException(methodName);
            }
        }
        
        private void SaveData()
        {
            var Auth = DriveSavedAuthentication.Retrieve(BrokerUserPrincipal.CurrentPrincipal);

            Auth.UserName = GetString("m_tbUserName");
            if (GetString("m_tbPassword") != ConstAppDavid.FakePasswordDisplay)
            {
                Auth.Password = GetString("m_tbPassword");
            }
            if (GetBool("m_cbRememberLogon"))
            {
                Auth.Save();
            }
        }
    }
}
