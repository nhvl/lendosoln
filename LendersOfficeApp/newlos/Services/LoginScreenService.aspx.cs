﻿#region Auto generated code
namespace LendersOfficeApp.newlos.Services
#endregion
{
    using System;
    using System.Text;
    using CommonLib;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Service page for LoginScreen.aspx.
    /// </summary>
    public partial class LoginScreenService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Runs the service method.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(this.ValidateCreditCardInfo):
                    this.ValidateCreditCardInfo();
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Method not implemented");
            }
        }

        /// <summary>
        /// Validates the credit card info.
        /// </summary>
        private void ValidateCreditCardInfo()
        {
            string firstName = this.GetString("BillingFirstName");
            string middleName = this.GetString("BillingMiddleName");
            string lastName = this.GetString("BillingLastName");
            string number = this.GetString("BillingCardNumber");
            string cvv = this.GetString("BillingCVV");
            string streetAddress = this.GetString("BillingStreetAddress");
            string city = this.GetString("BillingCity");
            string state = this.GetString("BillingState");
            string zip = this.GetString("BillingZipcode");
            int expirationYear;
            int expirationMonth;
            string errorMesseges = string.Empty;

            bool invalid = false;
            StringBuilder errorMessages = new StringBuilder();
            if (int.TryParse(this.GetString("BillingExpirationMonth"), out expirationMonth) && expirationMonth < 1)
            {
                errorMessages.AppendLine("Invalid expiration month.");
                invalid = true;
            }

            if (int.TryParse(this.GetString("BillingExpirationYear"), out expirationYear) && expirationYear < 1)
            {
                errorMessages.AppendLine("Invalid expiration year.");
                invalid = true;
            }

            if (expirationYear == DateTime.Now.Year && expirationMonth < DateTime.Now.Month)
            {
                errorMessages.AppendLine("Credit card is expired.");
                invalid = true;
            }

            SimpleAddress address = null;

            try
            {
                address = new SimpleAddress(streetAddress, city, state, zip);
            }
            catch (ApplicationException exception)
            {
                errorMessages.AppendLine(exception.Message);
                invalid = true;
            }

            if (invalid)
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", errorMessages.ToString());
                return;
            }

            if (number == "1111222233334444")
            {
                // 5/23/2014 dd - This is a test credit card number. Return information as is instead of perform validation.
                this.SetResult("Success", true);
                this.SetResult("BillingFirstName", firstName);
                this.SetResult("BillingMiddleName", middleName);
                this.SetResult("BillingLastName", lastName);
                this.SetResult("BillingStreetAddress", address.StreetAddress);
                this.SetResult("BillingCity", address.City);
                this.SetResult("BillingState", address.State);
                this.SetResult("BillingZipcode", address.Zipcode);
                this.SetResult("BillingCardNumber", number);
                this.SetResult("BillingExpirationMonth", expirationMonth);
                this.SetResult("BillingExpirationYear", expirationYear);
                this.SetResult("BillingCVV", cvv);
                return;
            }

            CreditCard cc = null;
            try
            {
                cc = new CreditCard(firstName, lastName, number, expirationMonth, expirationYear, cvv, address);
            }
            catch (ApplicationException ex)
            {
                errorMessages.AppendLine(ex.Message);
                this.SetResult("Success", false);
                this.SetResult("Errors", errorMessages.ToString());
                return;
            }

            this.SetResult("Success", true);
            this.SetResult("BillingFirstName", cc.FirstName);
            this.SetResult("BillingMiddleName", middleName);
            this.SetResult("BillingLastName", cc.LastName);
            this.SetResult("BillingStreetAddress", cc.Address.StreetAddress);
            this.SetResult("BillingCity", cc.Address.City);
            this.SetResult("BillingState", cc.Address.State);
            this.SetResult("BillingZipcode", cc.Address.Zipcode);
            this.SetResult("BillingCardNumber", cc.Number);
            this.SetResult("BillingExpirationMonth", cc.ExpMonth);
            this.SetResult("BillingExpirationYear", cc.ExpYear);
            this.SetResult("BillingCVV", cc.CVV);
        }
    }
}