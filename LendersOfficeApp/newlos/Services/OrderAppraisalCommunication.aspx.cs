﻿namespace LendersOfficeApp.Newlos.Services
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Web.UI.WebControls;
    using LendersOffice.Common;
    using LendersOffice.Integration.Appraisals;
    using LendersOffice.Integration.VendorCommunication;
    using LendersOffice.Integration.VendorCommunication.Appraisal;
    using LendersOffice.Security;
    using newlos;

    /// <summary>
    /// Bi-directional communication page.
    /// 370296.
    /// </summary>
    public partial class OrderAppraisalCommunication : BaseLoanPage
    {
        /// <summary>
        /// The review message mode.
        /// </summary>
        private const string NoneStatus = "none";

        /// <summary>
        /// The reply mode.
        /// </summary>
        private const string ReplyStatus = "reply";

        /// <summary>
        /// The write new mode.
        /// </summary>
        private const string NewStatus = "new";

        /// <summary>
        /// The appraisal Mesage Handler.
        /// </summary>
        private AppraisalMessageHandler appraisalMessage;

        /// <summary>
        /// Gets the vendor ID.
        /// </summary>
        /// <value>The value of vendor.</value>
        protected string VendorID
        {
            get { return RequestHelper.GetSafeQueryString("vendorID"); }
        }

        /// <summary>
        /// Gets the order number.
        /// </summary>
        /// <value>The value of order number.</value>
        protected string OrderNumber
        {
            get { return RequestHelper.GetSafeQueryString("orderNumber"); }
        }

        /// <summary>
        /// The page Load function.
        /// </summary>
        /// <param name="sender">The sender value.</param>
        /// <param name="e">The e value.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterService("OrderAppraisal", "/newlos/services/OrderAppraisalService.aspx");

            var vendor = AppraisalVendorConfig.Retrieve(new Guid(this.VendorID));
            string script = string.Format("var usingGlobalDMS = '{0}';", vendor.UsesGlobalDMS);
            ClientScript.RegisterStartupScript(typeof(OrderAppraisalCommunication), "UISettings", script, true);

            this.VendorId.Value = this.VendorID;
            this.AppraisalOrderNumber.Value = this.OrderNumber;
            if (!this.IsPostBack)
            {
                this.UpdateUI();
                this.HandleUnreadSelected();
            }
            else
            {
                this.UpdateUI(false);
            }

            this.ReplyBtn.ServerClick += this.ReplyBtn_ServerClick;
            this.SendBtn.ServerClick += this.SendBtn_ServerClick;
            this.CancelBtn.ServerClick += this.CancelBtn_ServerClick;
            this.WriteMsgBtn.ServerClick += this.WriteMsgBtn_ServerClick;
            this.history_grid.SortCommand += this.History_grid_SortCommand;
            this.history_grid.SelectedIndexChanged += this.History_grid_SelectedIndexChanged;
        }

        /// <summary>
        /// On Init Event.
        /// </summary>
        /// <param name="e">The e value.</param>
        protected override void OnInit(EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            base.OnInit(e);
        }

        /// <summary>
        /// Display the received date.
        /// </summary>
        /// <param name="dt">The date value.</param>
        /// <returns>The formatted date string.</returns>
        protected string DisplayReceivedDate(object dt)
        {
            return ((DateTime)dt).ToString("MM-dd-yy hh:mm:ss tt");
        }

        /// <summary>
        /// Display the subject.
        /// </summary>
        /// <param name="subject">The subject value.</param>
        /// <returns>The formatted subject.</returns>
        protected string DisplaySubject(object subject)
        {
            var s = subject.ToString();
            const int MaxLength = 75;
            if (s.Length > MaxLength)
            {
                // Truncate with ellipsis
                s = s.Substring(0, MaxLength) + "...";
            }

            return s;
        }

        /// <summary>
        /// Display the sort direction arrow.
        /// </summary>
        /// <returns>The arrow value.</returns>
        protected string DisplaySortDirection()
        {
            return Server.HtmlDecode(this.SortDirection.Value == "ASC" ? "&#x25bc;" : "&#x25b2;");
        }

        /// <summary>
        /// Bind Data function.
        /// </summary>
        private void BindData()
        {
            if (string.IsNullOrWhiteSpace(this.SortExpression.Value))
            {
                this.SortExpression.Value = "Date";
                this.SortDirection.Value = "DESC";
            }

            if (this.appraisalMessage == null)
            {
                this.appraisalMessage = new AppraisalMessageHandler(this.LoanID);
            }

            var messages = this.appraisalMessage.GetAllMessages().Where(m => m.VendorId == new Guid(this.VendorID) && m.OrderNumber == this.OrderNumber);
            foreach (var m in messages)
            {
                if (string.IsNullOrWhiteSpace(m.SendingParty))
                {
                    m.SendingParty = this.BrokerUser.LoginNm;
                }

                if (string.IsNullOrWhiteSpace(m.ReceivingParty))
                {
                    m.ReceivingParty = this.BrokerUser.LoginNm;
                }
            }

            messages = (this.SortDirection.Value == "ASC") ? messages.OrderBy(x => x.GetType().GetProperty(this.SortExpression.Value).GetValue(x, null)).ToList() : messages.OrderByDescending(x => x.GetType().GetProperty(this.SortExpression.Value).GetValue(x, null)).ToList();
            this.history_grid.DataSource = messages;
            this.history_grid.DataBind();
            this.history_grid.Attributes.Add("tabindex", "0");

            for (var i = 0; i < messages.Count(); i++)
            {
                if (this.IsUnReadMessage(messages.ElementAt(i).MessageId))
                {
                    this.history_grid.Items[i].Attributes["preset"] = "unread";
                }

                if (messages.ElementAt(i).MessageId.ToString() == this.UnreadSelectedID.Value)
                {
                    this.history_grid.Items[i].Attributes["preset"] = "unread";
                }
            }
        }

        /// <summary>
        /// Selected Index Changed Event.
        /// </summary>
        /// <param name="sender">The sender event.</param>
        /// <param name="e">The e event.</param>
        private void History_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.HandleUnreadSelected();

            this.WriteMode.Value = NoneStatus;
            this.UpdateUI();
        }

        /// <summary>
        /// Sort History Event.
        /// </summary>
        /// <param name="source">The source value.</param>
        /// <param name="e">The e value.</param>
        private void History_grid_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            if (this.SortExpression.Value == e.SortExpression)
            {
                this.SortDirection.Value = (this.SortDirection.Value == "ASC") ? "DESC" : "ASC";
            }
            else
            {
                this.SortExpression.Value = e.SortExpression;
                this.SortDirection.Value = "ASC";
            }

            this.WriteMode.Value = NoneStatus;
            this.UpdateUI();
        }

        /// <summary>
        /// Write New Message Event.
        /// </summary>
        /// <param name="sender">The sender value.</param>
        /// <param name="e">The event value.</param>
        private void WriteMsgBtn_ServerClick(object sender, EventArgs e)
        {
            this.MarkAsRead();

            this.WriteMode.Value = NewStatus;
            this.UpdateUI();
        }

        /// <summary>
        /// Cancel Event.
        /// </summary>
        /// <param name="sender">The sender value.</param>
        /// <param name="e">The event value.</param>
        private void CancelBtn_ServerClick(object sender, EventArgs e)
        {
            this.WriteMode.Value = NoneStatus;
            this.UpdateUI();
        }

        /// <summary>
        /// Send Message Event.
        /// </summary>
        /// <param name="sender">The sender value.</param>
        /// <param name="e">The event value.</param>
        private void SendBtn_ServerClick(object sender, EventArgs e)
        {
            var historyItems = (List<VendorMessage>)this.history_grid.DataSource;
            if (this.appraisalMessage == null)
            {
                this.appraisalMessage = new AppraisalMessageHandler(this.LoanID);
            }

            var message = new VendorMessage()
            {
                VendorId = new Guid(this.VendorID),
                BrokerId = this.BrokerID,
                LoanId = this.LoanID,
                OrderNumber = this.OrderNumber,
                SendingParty = this.fromLb.InnerText,
                ReceivingParty = this.appraisalSelector.Text,
                Subject = this.subjectTxt.Text,
                Message = this.PlainTextToHtml(this.contentTxt.Text),
                Date = DateTime.Now
            };
            if (this.history_grid.SelectedIndex != -1)
            {
                message.MessageId = historyItems[this.history_grid.SelectedIndex].MessageId;
            }

            this.appraisalMessage.SendMessage(message, AppraisalVendorConfig.Retrieve(new Guid(this.VendorID)).PrimaryExportPath);
            this.WriteMode.Value = NoneStatus;
            this.UpdateUI();
        }

        /// <summary>
        /// Reply Message Event.
        /// </summary>
        /// <param name="sender">The sender value.</param>
        /// <param name="e">The event value.</param>
        private void ReplyBtn_ServerClick(object sender, EventArgs e)
        {
            this.MarkAsRead();

            this.WriteMode.Value = ReplyStatus;
            this.UpdateUI();
        }

        /// <summary>
        /// Set selected item as read but still keep the red mark.
        /// </summary>
        private void HandleUnreadSelected()
        {
            var isUnreadSelected = false;
            var items = (List<VendorMessage>)this.history_grid.DataSource;
            if (this.history_grid.SelectedIndex >= 0)
            {
                isUnreadSelected = this.IsUnReadMessage(items[this.history_grid.SelectedIndex].MessageId);
            }

            this.MarkAsRead();

            if (this.history_grid.SelectedIndex >= 0)
            {
                this.SelectedID.Value = items[this.history_grid.SelectedIndex].MessageId.ToString();
                if (isUnreadSelected)
                {
                    this.UnreadSelectedID.Value = items[this.history_grid.SelectedIndex].MessageId.ToString();
                }
            }
        }

        /// <summary>
        /// Update UI.
        /// </summary>
        /// <param name="isClearValues">Is Reset Values or not.</param>
        private void UpdateUI(bool isClearValues = true)
        {
            this.BindData();
            string status = string.IsNullOrWhiteSpace(this.WriteMode.Value) ? NoneStatus : this.WriteMode.Value;
            var historyItems = (List<VendorMessage>)this.history_grid.DataSource;
            this.history_grid.Focus();

            switch (status)
            {
                case NoneStatus:
                    this.titleLb.InnerText = "View Message";
                    this.SetState(false);
                    if (this.history_grid.SelectedIndex == -1)
                    {
                        this.panel.Style.Add("visibility", "hidden");
                    }
                    else
                    {
                        this.panel.Style.Add("visibility", "visible");
                        if (isClearValues)
                        {
                            this.dateLb.InnerText = this.DisplayReceivedDate(historyItems[this.history_grid.SelectedIndex].Date);
                            this.fromLb.InnerText = historyItems[this.history_grid.SelectedIndex].SendingParty;
                            this.toLb.InnerText = historyItems[this.history_grid.SelectedIndex].ReceivingParty;
                            this.subjectTxt.Text = historyItems[this.history_grid.SelectedIndex].Subject;
                            this.contentTxt.Text = this.HtmlToPlainText(historyItems[this.history_grid.SelectedIndex].Message);
                        }
                    }

                    break;
                case ReplyStatus:
                    this.titleLb.InnerText = "Reply";
                    this.SetState(true);
                    this.panel.Style.Add("visibility", "visible");
                    this.dateLb.InnerText = string.Empty;
                    if (isClearValues)
                    {
                        if (!this.subjectTxt.Text.StartsWith("RE: "))
                        {
                            this.subjectTxt.Text = "RE: " + this.subjectTxt.Text;
                        }

                        this.contentTxt.Text = Environment.NewLine + Environment.NewLine
                            + "----------------------------------------------------------------------------------------------------------------" + Environment.NewLine + this.contentTxt.Text;
                        this.contentTxt.Focus();
                        if (this.history_grid.SelectedIndex != -1)
                        {
                            this.fromLb.InnerText = this.BrokerUser.LoginNm;
                            if (historyItems[this.history_grid.SelectedIndex].SendingParty == "Appraiser" || historyItems[this.history_grid.SelectedIndex].SendingParty == "AMC")
                            {
                                this.appraisalSelector.Text = historyItems[this.history_grid.SelectedIndex].SendingParty;
                            }
                            else
                            {
                                this.appraisalSelector.Text = historyItems[this.history_grid.SelectedIndex].ReceivingParty;
                            }
                        }
                    }

                    break;
                case NewStatus:
                    this.titleLb.InnerText = "New Message";
                    this.history_grid.SelectedIndex = -1;
                    this.SelectedID.Value = string.Empty;
                    this.SetState(true);
                    this.panel.Style.Add("visibility", "visible");
                    if (isClearValues)
                    {
                        this.fromLb.InnerText = this.BrokerUser.LoginNm;
                        this.appraisalSelector.Text = "Appraiser";
                        this.dateLb.InnerText = string.Empty;
                        this.subjectTxt.Text = string.Empty;
                        this.contentTxt.Text = string.Empty;
                    }

                    break;
                default:
                    throw new Exception("Invalid Status");
            }
        }

        /// <summary>
        /// Set State Editable.
        /// </summary>
        /// <param name="isEditable">Is Editable or not.</param>
        private void SetState(bool isEditable)
        {
            this.toLb.Visible = !isEditable;
            this.appraisalSelector.Visible = isEditable;
            this.ReplyBtn.Visible = this.WriteMsgBtn.Visible = !isEditable;
            this.SendBtn.Visible = this.CancelBtn.Visible = isEditable;
            this.subjectTxt.ReadOnly = this.contentTxt.ReadOnly = !isEditable;
            this.history_grid.Enabled = !isEditable;

            var historyItems = (List<VendorMessage>)this.history_grid.DataSource;

            if (!this.history_grid.Enabled)
            {
                for (var i = 0; i < this.history_grid.Items.Count; i++)
                {
                    this.history_grid.Items[i].Attributes.Add("onclick", string.Empty);
                }
            }
            else
            {
                for (var i = 0; i < this.history_grid.Columns.Count; i++)
                {
                    this.history_grid.Columns[i].HeaderStyle.CssClass = this.history_grid.Columns[i].HeaderStyle.CssClass.Replace(" headerSortDown", string.Empty);
                    this.history_grid.Columns[i].HeaderStyle.CssClass = this.history_grid.Columns[i].HeaderStyle.CssClass.Replace(" headerSortUp", string.Empty);
                    if (this.history_grid.Columns[i].SortExpression == this.SortExpression.Value)
                    {
                        this.history_grid.Columns[i].HeaderStyle.CssClass += (this.SortDirection.Value == "ASC") ? " headerSortDown" : " headerSortUp";
                    }
                }

                if (this.history_grid.Items.Count != 0)
                {
                    if (string.IsNullOrWhiteSpace(this.SelectedID.Value))
                    {
                        if (this.history_grid.Items.Count != 0)
                        {
                            this.SelectedID.Value = historyItems[0].MessageId.ToString();
                            this.history_grid.SelectedIndex = 0;
                        }
                        else
                        {
                            this.history_grid.SelectedIndex = -1;
                        }
                    }
                    else
                    {
                        // Lây selected Index
                        for (var i = 0; i < historyItems.Count; i++)
                        {
                            if (this.SelectedID.Value == historyItems[i].MessageId.ToString())
                            {
                                this.history_grid.SelectedIndex = i;
                            }
                        }
                    }

                    for (var i = 0; i < this.history_grid.Items.Count; i++)
                    {
                        var id = (i + 2).ToString();
                        if (i + 2 < 10)
                        {
                            id = "0" + id;
                        }

                        this.history_grid.Items[i].Attributes.Add("onclick", "javascript:__doPostBack('history_grid$ctl" + id + "$linkButton','')");
                    }
                }
                else
                {
                    this.history_grid.SelectedIndex = -1;
                }
            }
        }

        /// <summary>
        /// Mark message as read.
        /// </summary>
        private void MarkAsRead()
        {
            this.UnreadSelectedID.Value = string.Empty;
            if (this.appraisalMessage == null)
            {
                this.appraisalMessage = new AppraisalMessageHandler(this.LoanID);
            }

            if (this.history_grid.SelectedIndex != -1)
            {
                var historyItems = (List<VendorMessage>)this.history_grid.DataSource;
                this.appraisalMessage.SetMessageRead(historyItems[this.history_grid.SelectedIndex].MessageId);
            }
        }

        /// <summary>
        /// Check if message is read or not.
        /// </summary>
        /// <param name="messageId">The Message Id.</param>
        /// <returns>Message is read or not.</returns>
        private bool IsUnReadMessage(int messageId)
        {
            if (this.appraisalMessage == null)
            {
                this.appraisalMessage = new AppraisalMessageHandler(this.LoanID);
            }

            return this.appraisalMessage.GetUnreadMessages().Contains(messageId);
        }

        /// <summary>
        /// Parse html to plain text.
        /// </summary>
        /// <param name="input">The input value.</param>
        /// <returns>The plain text.</returns>
        private string HtmlToPlainText(string input)
        {
            return input.Replace("<br />", Environment.NewLine);
        }

        /// <summary>
        /// Parse Plain text to html.
        /// </summary>
        /// <param name="plainText">The plain text.</param>
        /// <returns>The html string.</returns>
        private string PlainTextToHtml(string plainText)
        {
            return plainText.Replace(Environment.NewLine, "<br />");
        }
    }
}
