﻿namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;

    public partial class DocMagicDocumentGenerationDBK : BaseLoanPage
    {
        protected void PageLoad(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                var dbkKey = RequestHelper.GetSafeQueryString("dbkKey");
                var anonObj = ObsoleteSerializationHelper.JavascriptJsonDeserializer<Dictionary<string, string>>(AutoExpiredTextCache.GetFromCache(dbkKey));

                var userId = anonObj["userid"];
                var autoExpireKey = anonObj["datapath"];
                if (BrokerUser.UserId != new Guid(userId))
                {
                    throw new CBaseException(ErrorMessages.GenericAccessDenied,
                        "user with id: " + BrokerUser.UserId + " attempted to access dbk file that was created by user with id: " + userId
                        + " and autoExpireKey " + autoExpireKey);
                }

                Response.Clear();
                Response.ContentType = "application/download";//"application/dbk";
                Response.AddHeader("Content-Disposition", "attachment; filename=\"generatedDocMasterDoc.dbk\"");
                var filepath = AutoExpiredTextCache.GetFileFromCache(autoExpireKey);

                Tools.WriteFileToResponseWithBuffer(Response, filepath);
                
                FileOperationHelper.Delete(filepath);
                
                Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }
        protected void DownloadFile(object sender, EventArgs e)
        {
           
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            UseNewFramework = false; // There is no service page, so set this to false so that the smoke test won't choke on this page
            DisplayCopyRight = false;
            Title = "Download DBK Document";
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            //this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion
    }
}
