﻿namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using DataAccess;
    using LendersOffice.CreditReport;
    using LendersOffice.Migration;
    using MeridianLink.CommonControls;
    using LendersOffice.AntiXss;

    public partial class FHATotalSubmit : LendersOfficeApp.newlos.BaseLoanPage
    {
        protected bool hasCreditReport = false;
        private bool useLegacyCounselingDefinition;

        class GiftAssetInfo
        {
            public string BorrowerName { get; set; }
            public string Description { get; set; }
            public string Amount { get; set; }
            public E_GiftFundSourceT Source { get; set; }
            public Guid aAppId { get; set; }
            public Guid AssetId { get; set; }
        }

        private GiftAssetInfo ExtractInfo(CAppData app, IAssetRegular giftAsset)
        {

            GiftAssetInfo asset = new GiftAssetInfo();

            switch (giftAsset.OwnerT)
            {
                case E_AssetOwnerT.Borrower:
                    asset.BorrowerName = app.aBNm;
                    break;
                case E_AssetOwnerT.CoBorrower:
                    asset.BorrowerName = app.aCNm;
                    break;
                case E_AssetOwnerT.Joint:
                    asset.BorrowerName = app.aAppFullNm;
                    break;
                default:
                    throw new UnhandledEnumException(giftAsset.OwnerT);
            }

            asset.Amount = giftAsset.Val_rep;
            asset.AssetId = giftAsset.RecordId;
            asset.Description = giftAsset.Desc;
            asset.Source = giftAsset.GiftSource;
            asset.aAppId = app.aAppId;

            return asset;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterService("FHATotal", "/newlos/services/FHATotalSubmitService.aspx");
            this.RegisterJsScript("LQBPopup.js");	
            this.EnableJqueryMigrate = false;
            
            PageID = "FHATotalAudit";
            SubmissionIdSection.Visible = (false == Broker.IsHideFhaLenderIdInTotalScorecard);
            if (Page.IsPostBack)
            {
                return;
            }
            CPageData data = new CFHATotalAuditData(LoanID);
            data.InitLoad();

            this.RegisterJsGlobalVariables("IsTargeting2019Ulad", data.sIsTargeting2019Ulad);

            // OPM 106244 - Check for credit report
            ICreditReportView creditReportView = null;
            CAppData dataApp = data.GetAppData(ApplicationID);
            creditReportView = dataApp.CreditReportView.Value;
            if (creditReportView != null)
                hasCreditReport = true;

            List<CAppData> apps = new List<CAppData>(data.nApps);
            List<GiftAssetInfo> assets = new List<GiftAssetInfo>();

            for (int i = 0; i < data.nApps; i++)
            {
                CAppData currentApp = data.GetAppData(i);
                apps.Add(currentApp);

                foreach (var item in currentApp.aAssetCollection.GetSubcollection(true, E_AssetGroupT.GiftFunds))
                {
                    var regular = (IAssetRegular)item;
                    assets.Add(ExtractInfo(currentApp, regular));
                }
            }

            if (assets.Count > 0)
            {
                GiftFundPanel.Visible = true;
                GiftFundAssets.DataSource = assets;
                GiftFundAssets.DataBind();
                sPrimaryGiftFundSource.Text = data.sPrimaryGiftFundSource.ToString();
                sTotGiftFundAsset.Text = data.sTotGiftFundAsset_rep;
            }



            switch (data.sFhaLenderIdT)
            {
                case E_sFhaLenderIdT.RegularFhaLender:
                    sFhaLenderIdT_RegularFhaLender.Checked = true;
                    break;
                case E_sFhaLenderIdT.SponsoredOriginatorEIN:
                    sFhaLenderIdT_SponsoredOriginatorEIN.Checked = true;
                    break;
                case E_sFhaLenderIdT.NoOriginatorId:
                    sFhaLenderIdT_NoOriginatorId.Checked = true;
                    break;
                default:
                    throw new UnhandledEnumException(data.sFhaLenderIdT);
            }
            
            Tools.Bind_sTotalScoreRefiT(sTotalScoreRefiT);
            Tools.SetDropDownListValue(sTotalScoreRefiT, data.sTotalScoreRefiT);


            Tools.Bind_sTotalScoreFhaProductT(sTotalScoreFhaProductT);
            Tools.SetDropDownListValue(sTotalScoreFhaProductT, data.sTotalScoreFhaProductT);

            Tools.Bind_sFinMethT(sFinMethT);
            Tools.SetDropDownListValue(sFinMethT, data.sFinMethT); 

            sTotalScoreAppraisedFairMarketRent.Text = data.sTotalScoreAppraisedFairMarketRent_rep;
            sTotalScoreVacancyFactor.Text = data.sTotalScoreVacancyFactor_rep;
            sTotalScoreNetRentalIncome.Text = data.sTotalScoreNetRentalIncome_rep;

            sTotalScoreIsIdentityOfInterest.ClearSelection();
            sTotalScoreIsTransactionException.ClearSelection();

            if (data.sTotalScoreIsIdentityOfInterest)
            {
                sTotalScoreIsIdentityOfInterest.Items.FindByText("Yes").Selected = true;
                if (data.sTotalScoreIsIdentityOfInterestException)
                {
                    sTotalScoreIsTransactionException.Items.FindByText("Yes").Selected = true;
                }
                else
                {
                    sTotalScoreIsTransactionException.Items.FindByText("No").Selected = true;
                }
            }
            else
            {
                sTotalScoreIsIdentityOfInterest.Items.FindByText("No").Selected = true;
            }


            if (data.sConcurSubFin <= 0 && data.sSubFin <= 0)
            {
                Seconfin.Visible = false; 
            }


            E_sLPurposeT[] refinancePurposes = new E_sLPurposeT[] { E_sLPurposeT.Refin, E_sLPurposeT.RefinCashout, E_sLPurposeT.FhaStreamlinedRefinance };
            RefinanceInformation.Visible = refinancePurposes.Contains(data.sLPurposeT);

            if (RefinanceInformation.Visible)
            {
                var list = data.sMortgageLiaList;
         
                MortgageLiabilitiesRepeater.DataSource = list;
                MortgageLiabilitiesRepeater.DataBind();
                ClientScript.RegisterHiddenField("MortgageLiabilitiesId", MortgageLiabilitiesRepeater.ClientID);
                ClientScript.RegisterHiddenField("MortgageLiabilitiesCount", list.Count.ToString());
            }

            Tools.Bind_sTotalScoreSecondaryFinancingSrc(this.sTotalScoreSecondaryFinancingSrc, data.sTotalScoreSecondaryFinancingSrc);
            Tools.SetDropDownListValue(this.sTotalScoreSecondaryFinancingSrc, data.sTotalScoreSecondaryFinancingSrc);
            this.sTotalScoreSecondaryFinancingSrcLckd.Checked = data.sTotalScoreSecondaryFinancingSrcLckd;

            // OPM 72500 (overwrites opm 48103's behavior)
            if (Tools.IsValidAndNonBlankFhaLenderID(data.sFHALenderIdCode, Broker.UseFHATOTALProductionAccount))  
            {
                sFHALenderIdCode.Text = data.sFHALenderIdCode;
            }
            else
            {
                var fhaLenderIDToUse = Tools.GetFhaLenderIDFromLoanBranchOrBroker(data.sBrokerId, data.sLId, data.sBranchId);
                sFHALenderIdCode.Text = fhaLenderIDToUse;
                if (fhaLenderIDToUse != String.Empty)
                {
                    sFHALenderIdCode.ReadOnly = true;
                }
            }
            
            //Also, lender can choose to hide Lender Id and Sponsor Id.
            if (Broker.IsHideFhaLenderIdInTotalScorecard)
            {
                LenderIdDiv.Visible = false;

            }

            SetRequiredValidatorMessage(sFHALenderIdIs10DigitNumber);
            SetRequiredValidatorMessage(sFHALenderIdCodeRequired);
            SetRequiredValidatorMessage(sFHASponsoredOriginatorEINIs9Digits);
            SetRequiredValidatorMessage(sFHASponsorAgentIdCodeIs10Digits);
            SetRequiredValidatorMessage(sFHASponsoredOriginatorEINRequired);

            sFHASponsoredOriginatorEINIs9Digits.ValidationExpression = "[0-9]{9}";
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(data.sLoanVersionT, LoanVersionT.V18_ConsolidateTaxIdFields))
            {
                // This will hook the field into mask.js for migrated files.
                sFHASponsoredOriginatorEIN.Attributes.Add("preset", "employerIdentificationNumber");
                sFHASponsoredOriginatorEINIs9Digits.ValidationExpression = "\\d{2}-?\\d{7}";
            }

            sFHASponsoredOriginatorEIN.Text = data.sFHASponsoredOriginatorEIN;
            sFHASponsorAgentIdCode.Text = data.sFHASponsorAgentIdCode;
            SetRequiredValidatorMessage(sFHASponsorAgentIdCodeIsRequired);
            sAgencyCaseNum.Text = data.sAgencyCaseNum;

            ThreeFourUnitProperties.Visible = data.sUnitsNum >= 3;
            sNoteIR.Text = data.sNoteIR_rep;
            sTerm.Text = data.sTerm_rep;
            sRAdj1stCapMon.Text = data.sRAdj1stCapMon_rep;

            this.useLegacyCounselingDefinition = data.sUseLegacyTotalCounselTypeDefinition;
            this.RegisterJsGlobalVariables("useLegacyCounselingDefinition", this.useLegacyCounselingDefinition);
            Borrowers.DataSource = apps;
            Borrowers.DataBind();

            sTotalScoreUploadDataFHATransmittal.Checked = data.sTotalScoreUploadDataFHATransmittal;
            NumberOfApplications.Value = data.nApps.ToString();
            BorrowersClientId.Value = Borrowers.ClientID;
        }

        private void BindData()
        {
 
        }

        protected void GiftFundAssets_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            GiftAssetInfo asset = args.Item.DataItem as GiftAssetInfo;
            if (asset == null)
            {
                return;
            }

            EncodedLiteral borrowerName = args.Item.FindControl("BorrowerName") as EncodedLiteral;
            EncodedLiteral description = args.Item.FindControl("Description") as EncodedLiteral;
            EncodedLiteral amount = args.Item.FindControl("Amount") as EncodedLiteral;
            DropDownList source = args.Item.FindControl("Source") as DropDownList;
            RegularExpressionValidator validator = args.Item.FindControl("Validator") as RegularExpressionValidator;


            borrowerName.Text = asset.BorrowerName;
            description.Text = asset.Description;
            amount.Text = asset.Amount;
            Tools.Bind_GiftFundSourceT(source);
            Tools.SetDropDownListValue(source, asset.Source);
            source.Attributes.Add("htmldataassetid", asset.AssetId.ToString());
            source.Attributes.Add("htmldataappid", asset.aAppId.ToString());
            source.Attributes.Add("onchange", "FHASubmitForm.UpdateAssetData()");
            SetRequiredValidatorMessage(validator); 
        }

        private void SetRequiredValidatorMessage(System.Web.UI.WebControls.BaseValidator v)
        {
            HtmlImage img = new HtmlImage();
            img.Src = ResolveUrl("~/images/error_icon.gif");
            v.Controls.Add(img);
        }
        protected void MortgageLiabilitiesRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            ListItemType[] skipTypes = new ListItemType[] { ListItemType.Footer, ListItemType.Header, ListItemType.Pager, ListItemType.Separator };
            if (skipTypes.Contains(args.Item.ItemType))
            {
                return;
            }

            ILiabilityRegular liability = args.Item.DataItem as ILiabilityRegular;

            HtmlInputRadioButton subjProp1stMort = args.Item.FindControl("SubjProp1stMort") as HtmlInputRadioButton;
            HiddenField id = args.Item.FindControl("MortLiabIdentifier") as HiddenField;
            Label sCredNm = args.Item.FindControl("sCredNm") as Label;
            Label balance = args.Item.FindControl("balance") as Label;
            RadioButtonList propStatus = args.Item.FindControl("PropStatus") as RadioButtonList;

            subjProp1stMort.Attributes.Add("onclick", "SetUniqueRadioButton('MortgageLiabilitiesRepeater.*SubjProp1stMort', this)");


            subjProp1stMort.Attributes.Add("Name", "SubjProp1stMort");
            sCredNm.Text= liability.ComNm;
            balance.Text = liability.Bal_rep;
            subjProp1stMort.Checked = liability.IsSubjectProperty1stMortgage;
            subjProp1stMort.Attributes.Add("CName", propStatus.ClientID);
            id.Value = liability.RecordId.ToString();

            if (liability.IsSubjectProperty1stMortgage || liability.IsSubjectPropertyMortgage)
            {
                Tools.SetRadioButtonListValue(propStatus, "SubjectProperty");
            }
            else 
            {
               
                Tools.SetRadioButtonListValue(propStatus, "OtherProperty" );
            }

            if (liability.IsSubjectProperty1stMortgage)
            {
                propStatus.Attributes.Add("disabled", "true");
            }
            HtmlTableRow tr = args.Item.FindControl("row") as HtmlTableRow;
            if (args.Item.ItemIndex % 2 != 0)
            {
                tr.Style.Add("background-color", "#FFF");
            }
        }

        protected void Borrowers_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType == ListItemType.Separator)
            {
                return;
            }

            CAppData data = args.Item.DataItem as CAppData;
            HiddenField hf = args.Item.FindControl("BorrowerApplicationId") as HiddenField;
            HiddenField hc = args.Item.FindControl("HasCoborrower") as HiddenField;

            CheckBox aBTotalScoreIsCAIVRSAuthClear = args.Item.FindControl("aBTotalScoreIsCAIVRSAuthClear") as CheckBox;
            CheckBox aCTotalScoreIsCAIVRSAuthClear = args.Item.FindControl("aCTotalScoreIsCAIVRSAuthClear") as CheckBox;
            TextBox aFHABCaivrsNum = args.Item.FindControl("aFHABCaivrsNum") as TextBox;
            TextBox aFHACCaivrsNum = args.Item.FindControl("aFHACCaivrsNum") as TextBox;
            RadioButtonList aFHABLdpGsaTri = args.Item.FindControl("aFHABLdpGsaTri") as RadioButtonList;
            RadioButtonList aFHACLdpGsaTri = args.Item.FindControl("aFHACLdpGsaTri") as RadioButtonList;
            HtmlInputCheckBox aBTotalScoreIsFthb = args.Item.FindControl("aBTotalScoreIsFthb") as HtmlInputCheckBox;
            HtmlInputCheckBox aCTotalScoreIsFthb = args.Item.FindControl("aCTotalScoreIsFthb") as HtmlInputCheckBox;
            PlaceHolder borrFthbExplanation = args.Item.FindControl("BorrFthbExplanation") as PlaceHolder;
            PlaceHolder coborrFthbExplanation = args.Item.FindControl("CoborrFthbExplanation") as PlaceHolder;
            HtmlAnchor borrowerDeclarationsLink = args.Item.FindControl("BorrowerDeclarationsLink") as HtmlAnchor;
            HtmlAnchor coborrowerDeclarationsLink = args.Item.FindControl("CoborrowerDeclarationsLink") as HtmlAnchor;

    

            PlaceHolder cbFields = args.Item.FindControl("CoborrowerFields") as PlaceHolder; 

            Label borrowerCounselingLabel = args.Item.FindControl("aBTotalScoreFhtbCounselingTLabel") as Label;
            DropDownList BFTHBBCounseling = args.Item.FindControl("aBTotalScoreFhtbCounselingT") as DropDownList;

            Label coborrowerCounselingLabel = args.Item.FindControl("aCTotalScoreFhtbCounselingTLabel") as Label;
            DropDownList CFTHBCCounseling = args.Item.FindControl("aCTotalScoreFhtbCounselingT") as DropDownList;

            Image BFTHBR = args.Item.FindControl("BFTHBR") as Image;
            Image CFTHBR = args.Item.FindControl("CFTHBR") as Image;


            if (this.useLegacyCounselingDefinition)
            {
                Tools.Bind_aBTotalScoreFhtbCounselingT_Legacy(BFTHBBCounseling);
                Tools.Bind_aBTotalScoreFhtbCounselingT_Legacy(CFTHBCCounseling);
            }
            else
            {
                borrowerCounselingLabel.Text = "Counseling Completed";
                coborrowerCounselingLabel.Text = "Counseling Completed";
                Tools.Bind_aBTotalScoreFhtbCounselingT(BFTHBBCounseling);
                Tools.Bind_aBTotalScoreFhtbCounselingT(CFTHBCCounseling);
            }

            hf.Value = data.aAppId.ToString();
            
            aFHABCaivrsNum.Text = data.aFHABCaivrsNum;
            aFHACCaivrsNum.Text = data.aFHACCaivrsNum;

            aBTotalScoreIsFthb.Checked = data.aBTotalScoreIsFthb;
            borrowerDeclarationsLink.Attributes.Add("onclick", "goToDeclarations('" + AspxTools.JsStringUnquoted(data.aAppId.ToString()) + "');");
            aBTotalScoreIsFthb.Disabled = data.aBTotalScoreIsFthbReadOnly;
            aCTotalScoreIsFthb.Checked = data.aCTotalScoreIsFthb;
            coborrowerDeclarationsLink.Attributes.Add("onclick", "goToDeclarations('" + AspxTools.JsStringUnquoted(data.aAppId.ToString()) + "');");
            aCTotalScoreIsFthb.Disabled = data.aCTotalScoreIsFthbReadOnly;

            if (data.aFHABLdpGsaTri == E_TriState.Yes)
            {
                aFHABLdpGsaTri.ClearSelection();
                aFHABLdpGsaTri.Items.FindByValue("1").Selected = true;
            }
            else if (data.aFHABLdpGsaTri == E_TriState.No)
            {
                aFHABLdpGsaTri.ClearSelection();
                aFHABLdpGsaTri.Items.FindByValue("2").Selected = true;
            }

            if (data.aFHACLdpGsaTri == E_TriState.Yes)
            {
                aFHACLdpGsaTri.ClearSelection();
                aFHACLdpGsaTri.Items.FindByValue("1").Selected = true;
            }
            else if (data.aFHACLdpGsaTri == E_TriState.No)
            {
                aFHACLdpGsaTri.ClearSelection();
                aFHACLdpGsaTri.Items.FindByValue("2").Selected = true;
            }

            aBTotalScoreIsCAIVRSAuthClear.Checked = data.aBTotalScoreIsCAIVRSAuthClear;
            aCTotalScoreIsCAIVRSAuthClear.Checked = data.aCTotalScoreIsCAIVRSAuthClear;


            if (!aBTotalScoreIsFthb.Checked && this.useLegacyCounselingDefinition)
            {
                BFTHBBCounseling.Enabled = false;          //set to not na because not fthb
                Tools.SetDropDownListValue(BFTHBBCounseling, E_aTotalScoreFhtbCounselingT.NotApplicable);
            }
            else {
                //remove the na option
                BFTHBBCounseling.Items.Remove(BFTHBBCounseling.Items.FindByValue(((int)E_aTotalScoreFhtbCounselingT.NotApplicable).ToString()));
                if (data.aBTotalScoreFhtbCounselingT != E_aTotalScoreFhtbCounselingT.NotApplicable)    //set the ddl to whatever the user picked
                {
                    Tools.SetDropDownListValue(BFTHBBCounseling, data.aBTotalScoreFhtbCounselingT);
                }
            }
            if (!aCTotalScoreIsFthb.Checked && this.useLegacyCounselingDefinition)
            {
                CFTHBCCounseling.Enabled = false;
                Tools.SetDropDownListValue(CFTHBCCounseling, E_aTotalScoreFhtbCounselingT.NotApplicable);
            }
            else
            {
                if (data.aCTotalScoreFhtbCounselingT != E_aTotalScoreFhtbCounselingT.NotApplicable)  //this if needed in case osmehow data went bad.
                {
                    Tools.SetDropDownListValue(CFTHBCCounseling, data.aCTotalScoreFhtbCounselingT);
                }

               CFTHBCCounseling.Items.Remove(CFTHBCCounseling.Items.FindByValue(((int)E_aTotalScoreFhtbCounselingT.NotApplicable).ToString()));
            }

            CFTHBCCounseling.Attributes.Add("RI", CFTHBR.ClientID);
            CFTHBCCounseling.Attributes.Add("CB", aCTotalScoreIsFthb.ClientID);
  

            BFTHBBCounseling.Attributes.Add("RI", BFTHBR.ClientID);
            BFTHBBCounseling.Attributes.Add("CB", aBTotalScoreIsFthb.ClientID);

            hc.Value = bool.FalseString;
            if (data.aCIsValidNameSsn)
            {
                hc.Value = bool.TrueString;
            }
            cbFields.Visible = data.aCIsValidNameSsn; 



        }
    }
}
