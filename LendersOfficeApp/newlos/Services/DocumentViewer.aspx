﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentViewer.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.DocumentViewer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="LendersOffice.AntiXss" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Document Viewer</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <style type="text/css">
        body {  background-color: gainsboro; height: 100%; }
        form { background-color: black; height: 100%} 
        .Tab { display: none; padding-left: 10px;  background-color:gainsboro; }
        iframe { width: 98%; margin: 0 auto; background-color: gainsboro;  }
        .visible { display: block; }
        #DisclosureStatusContainer { background-color : gainsboro; text-align: center; }
        #DocPortalLinkContainer 
        { 
            background-color : gainsboro; 
            text-align: left; 
            padding-left: 10px;
        }
        #Tabs { background-color : gainsboro; margin-top: 0px;  }
        ul.TabNav { background-color: gainsboro; overflow:visible; }
        .centeringDiv { text-align: center; margin-top: 10px;}
       .centeringDiv input { margin: 0 auto; width: 50px;}
        #InvalidateUnknownArchive
        {
            display: none;
        }
        .padTop
        {
            padding-top: 5px;
        }
        .padBottom
        {
            padding-bottom: 5px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="DocPortalLinkContainer">
            <a id="IDSDocPortalLink" runat="server" visible="false" target="_blank">Open IDS Document Portal</a>
            <div id="DocPortalError" class="hidden"></div>
        </div>
        <asp:PlaceHolder ID="DisclosureStatusButtons" runat="server">
            <div id="DisclosureStatusContainer">
                <div class="padBottom padTop">
                    <input type="button" class="DisclosureStatus" id="Disclosed" value="Documents have been or will be provided to the borrower manually." />
                </div>
                <div id="ResubmitDiv" runat="server" class="padBottom">
                    <input type="button" class="DisclosureStatus" id="Resubmit" value="Contained CoC is valid, but generated documents will not be disclosed to the borrower. Pending CoC will be re-submitted later." />
                </div>
                <div>
                    <input type="button" class="DisclosureStatus" id="NotDisclosed" value="Documents are invalid and will not be disclosed." runat="server" />
                </div>
                <div id="InvalidateUnknownArchive" style="text-align: center; margin-top: 5px;">
                    <div style="width: 300px; margin-left: auto; margin-right: auto;">
                        <ml:EncodedLiteral runat="server" ID="InvalidateUnknownArchiveMessage"></ml:EncodedLiteral>
                    </div>
                    <div>
                        <input type="checkbox" id="userAgreesToInvalidate" /><label for="userAgreesToInvalidate">I have read the above.</label>
                    </div>
                    <div>
                        <input type="button" id="confirmInvalidate" value="Confirm"/>
                        <input type="button" id="cancelInvalidate" value="Cancel"/>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <div class="Tabs">
            <ul class="tabnav">
                <li id="Documents" runat="server" class="selected"><a href="#DocumentTab"  >Documents</a></li>
                <li id="AprSchedule" runat="server"><a href="#AprScheduleTab" >Apr/Payment Schedule</a></li>
                <li id="ImpoundAnalysis" runat="server"><a href="#ImpoundAnalysisTab" >Impound Analysis</a></li>
                <li id="Section32" runat="server"><a href="#Section32Tab" >Section 32</a></li>
                <li id="GfeComparison" runat="server"><a href="#GfeComparisonTab" >GFE Comparison</a></li>
            </ul>
        </div>
        <div id="TabHolder">
            <div id="DocumentTab" class="Tab visible">
                    <asp:PlaceHolder runat="server" ID="m_phPDFChoice">
                        Select PDF:
                        <asp:DropDownList ID="m_ddlPDFDocument" runat="server" OnSelectedIndexChanged="PDFDocumentChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </asp:PlaceHolder>
                    <object data=<%= AspxTools.SafeUrl(DocUrl) %>  id="pdf"  width="98%" type="application/pdf" >
                        <param name="src" value=<%= AspxTools.SafeUrl(DocUrl) %>  />
                        <embed src=<%= AspxTools.SafeUrl(DocUrl) %> type="application/pdf" />
                        <a href=<%= AspxTools.SafeUrl(DocUrl) %>  >Download document package</a>
                     </object> 
                     <div class="centeringDiv">
                     <input type="button" value="Close" onclick="onClosePopup()" />
                     </div>
            </div>
            <div id="AprScheduleTab" class="Tab" runat="server"> 
                <iframe id="AprScheduleIFrame" runat="server"  ></iframe>
                     <div class="centeringDiv">
                
                <input type="button" value="Print" onclick="lqbPrintByFrame(frames['AprScheduleIFrame']);"  /> &nbsp;
                <input type="button" value="Close" onclick="onClosePopup()" />
                </div>            
            </div>
            <div id="ImpoundAnalysisTab" class="Tab" runat="server">
                <iframe id="ImpoundAnalysisIFrame" runat="server" ></iframe>
                <div class="centeringDiv">
                    <input type="button" value="Print" onclick="lqbPrintByFrame(frames['ImpoundAnalysisIFrame']);" />&nbsp;
                    <input type="button" value="Close" onclick="onClosePopup()" />
                </div>
            </div>
            <div id="Section32Tab" class="Tab" runat="server">
                <iframe id="Section32IFrame" runat="server"></iframe>
                <div class="centeringDiv">
                    <input type="button" value="Print" onclick="lqbPrintByFrame(frames['Section32IFrame']);" />&nbsp;
                    <input type="button" value="Close" onclick="onClosePopup()" />
                </div>
            </div>
            <div id="GfeComparisonTab" class="Tab" runat="server">
                <iframe id="GfeComparisoIFrame" runat="server"></iframe>
                <div class="centeringDiv">
                    <input type="button" value="Print" onclick="lqbPrintByFrame(frames['GfeComparisoIFrame']);" />&nbsp;
                    <input type="button" value="Close" onclick="onClosePopup()" />
                </div>
            </div>
        </div>
    </form>
    
    <script type="text/javascript">
        jQuery(function($) {
            resize(784, 700);
            var args = getModalArgs();
            
            if($('#RefreshOpener').length){
                if(args && args.body.updateDisclosedAPR) 
                {
                    args.body.updateDisclosedAPR();
                }
                
                if (args && args.treeview)
                {
                    window.onbeforeunload = function() 
                    { 
                        try 
                        {
                            args.treeview.location.reload();
                        }
                        catch (e) {}
                    };
                }    
            }

            function callArchiveStatusMethod(methodName, args) {
                var result = gService.utils.call(methodName, args);

                if (result.error && result.UserMessage) {
                    alert(result.UserMessage);
                } else if (!result.error) {
                    $('#DisclosureStatusContainer').hide();
                }
            }

            function setStatus(status) {
                var args = {
                    sLId: ML.sLId,
                    status: status
                };

                callArchiveStatusMethod('UpdateStatusOfArchiveInUnknownStatus', args);
            }

            function setStatuses(leStatus, cdStatus) {
                var args = {
                    sLId: ML.sLId,
                    LEStatus: leStatus,
                    CDStatus: cdStatus
                };

                gService.utils.callAsyncSimple('UpdateStatusesOfBothLeAndCdArchivesInUnknownStatus', args, function (result) { 
                    if (result.error && result.UserMessage) {
                        alert(result.UserMessage);
                    } else if (!result.error) {
                        $('#DisclosureStatusContainer').hide();
                    }
                });
            }

            $('.DisclosureStatus').click(function () {
                if (ML && ML.sHasBothCDAndLEArchiveInUnknownStatus) {
                    var leStatus;
                    var cdStatus;

                    if (this.id === 'Disclosed') {
                        leStatus = <%= AspxTools.JsString(LendersOffice.Common.SerializationTypes.ClosingCostArchive.E_ClosingCostArchiveStatus.IncludedInClosingDisclosure.ToString("D")) %>;
                        cdStatus = <%= AspxTools.JsString(LendersOffice.Common.SerializationTypes.ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed.ToString("D")) %>;
                        setStatuses(leStatus, cdStatus);
                    } else if (this.id === 'NotDisclosed') {
                        $('#InvalidateUnknownArchive').show();
                    } else if (this.id === 'Resubmit') {
                        leStatus = <%= AspxTools.JsString(LendersOffice.Common.SerializationTypes.ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration.ToString("D")) %>;
                        cdStatus = <%= AspxTools.JsString(LendersOffice.Common.SerializationTypes.ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid.ToString("D")) %>;
                        setStatuses(leStatus, cdStatus);
                    }
                }
                else {
                    var status;

                    if (this.id === 'Disclosed') {
                        status = <%= AspxTools.JsString(LendersOffice.Common.SerializationTypes.ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed.ToString("D")) %>;
                        setStatus(status);
                    } else if (this.id === 'NotDisclosed') {
                        if (ML && ML.sHasLoanEstimateArchiveInUnknownStatus) {
                            $('#InvalidateUnknownArchive').show();
                        } else {
                            status = <%= AspxTools.JsString(LendersOffice.Common.SerializationTypes.ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid.ToString("D")) %>;
                            setStatus(status);
                        }
                    } else if (this.id === 'Resubmit') {
                        var args = {
                            sLId: ML.sLId
                        };
                    
                        callArchiveStatusMethod('PrepareUnknownLoanEstimateArchiveForResubmission', args);
                    }
                }                
            });

            $('#cancelInvalidate').click(function() {
                $('#InvalidateUnknownArchive').hide();
            });

            $('#confirmInvalidate').click(function() {
                if (!$('#userAgreesToInvalidate').prop('checked')) {
                    alert('You must confirm you have read the message.');
                } else {
                    var status = <%= AspxTools.JsString(LendersOffice.Common.SerializationTypes.ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid.ToString("D")) %>;

                    if (ML.sHasBothCDAndLEArchiveInUnknownStatus) {
                        setStatuses(status, status);
                    }
                    else {                        
                        setStatus(status);
                    }                    
                }
            });
            
            $('.Tabs a').click(function(e) {
                $('.tabnav li').removeClass('selected');
                $('div.Tab').hide();
                $(this).parent().addClass('selected');

                var iframe = $($(this).attr('href')).show().find('iframe');
                if (iframe.length) resizeFrame(iframe[0].id);
                return false;
            });

            $('#IDSDocPortalLink').click(function() {
                var result = gService.utils.call(
                    "GetIdsLoanFileLink", 
                    { VendorId : ML.VendorId, LoanId : ML.sLId });

                if (result.error)
                {
                    $('#DocPortalError').text(result.UserMessage).show();
                }
                else if (result.value.success !== "True")
                {
                    $('#DocPortalError').text('There was an error getting the Document Portal URL: "' + response.value.error + '"').show();
                }
                else
                {
                    this.href = result.value.url;
                }
            });

            $('iframe, object').on('load', function() {
                resizeFrame(this.id);
            });


            $(window).resize(function() {
                $('iframe, object').each(function() {
                    resizeFrame(this.id);
                });
            });

            resizeFrame("pdf");
            
            var iframeBuffer = 30;
            function resizeFrame(frame) {
                myResizeIframe(frame, iframeBuffer);
                $('#TabHolder').css({'width': '100%'});
            }

            <%/*This is duplicated from f_resizeIframe in common.js, because half the time it doesn't show up when I load the page */%>
            function myResizeIframe(id, buffer) {
                if (!buffer) { buffer = 10; }
                var iframe = document.getElementById(id);
                var height = self.innerHeight || (document.documentElement.clientHeight || document.body.clientHeight);
                height -= pageY(iframe) + buffer;
                height = (height < 0) ? 0 : height;
                iframe.style.height = height + 'px';
            }
            
          <%//Have to wait until after we're done with document.load to tweak the tab's width, otherwise
            //it won't realize that it needs to resize until the window gets moved. %>
            window.setTimeout(TweakTabHolder, 1);
            function TweakTabHolder(){
                var tabs = $('#TabHolder');
                tabs.width(784);
            }
        });
       
    </script>
</body>
</html>
