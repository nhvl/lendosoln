﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using LendersOffice.GDMS.LookupMethods;
using LendersOffice.ObjLib.Conversions.GlobalDMS;
using LendersOffice.Integration.Appraisals;
using LendersOfficeApp.los.admin;
using System.Text;
using System.Text.RegularExpressions;
using DataAccess;
using E_sendEmailTo = LendersOffice.GDMS.Orders.SendEmailMessageObject.SendEmailTo;

namespace LendersOfficeApp.newlos.Services
{
    public partial class OrderAppraisalEmailMessage : BaseLoanPage
    {
        protected string Mode
        {
            get { return RequestHelper.GetSafeQueryString("mode"); }
        }
        protected string VendorID
        {
            get { return RequestHelper.GetSafeQueryString("vendorID"); }
        }
        protected string OrderNumber
        {
            get { return RequestHelper.GetSafeQueryString("orderNumber"); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterService("OrderAppraisal", "/newlos/services/OrderAppraisalService.aspx");
            Tools.LogInfo(" MODE : " + Mode);
            if (Mode == "send")
            {
                PageName.Text = "Send Email";
                ReceivedEmailFromRow.Visible = false;

                var sendEmailToNames = System.Enum.GetNames(typeof(E_sendEmailTo));
                var sendEmailToValues = System.Enum.GetValues(typeof(E_sendEmailTo));

                foreach (var value in sendEmailToValues)
                {
                    if (((E_sendEmailTo)value).ToString() != "Client")
                        SendEmailTo.Items.Add(new ListItem(((E_sendEmailTo)value).ToString(), value.ToString()));
                }
                SendEmailTo.SelectedValue = RequestHelper.GetSafeQueryString("sendTo");
                VendorId.Value = VendorID;
                AppraisalOrderNumber.Value = OrderNumber;
            }
            else if (Mode == "view")
            {
                PageName.Text = "View Email";
                SendEmailTo.Visible = false;
                SubjectText.Visible = false;
                SendBtn.Visible = false;
                ExitBtn.Value = "Close";

                string passedEmailLogID = RequestHelper.GetSafeQueryString("emailLogID");

                clsEmailLog emailLog = GetEmailLogByEmailLogID(int.Parse(passedEmailLogID));

                if (emailLog == null)
                {
                    return;
                }
                ReceivedFromField.Text = emailLog.FromUserType;
                ReceivedDateField.Text = emailLog.DateTimeStamp_String;
                ReceivedToField.Text = emailLog.ToUserType;
                ReceivedSubjectField.Text = emailLog.Subject;
                MessageText.Text = emailLog.Message;
                MessageText.ReadOnly = true;
            }
            else
                throw new CBaseException("Invalid email mode.", "Query string 'mode' must be set to either 'send' or 'view'.");
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            DisplayCopyRight = false;
            EnableJqueryMigrate = false;
        }

        protected clsEmailLog GetEmailLogByEmailLogID(int emailLogID)
        {
            clsEmailLog[] allEmailLogs = null;
            AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(new Guid(VendorID));

            string errorMessage;
            var credentials = vendor.GetGDMSCredentials(BrokerID, EmployeeID, out errorMessage);
            if (credentials == null)
            {
                ErrorMessage.Value = "Error: " + errorMessage;
                return null;
            }

            int orderNumber;
            if (!int.TryParse(OrderNumber, out orderNumber))
            {
                ErrorMessage.Value = "Error: Order Number must be an integer";
                return null;
            }

            using (var ordersClient = new OrdersClient(credentials))
            {
                try
                {
                    allEmailLogs = ordersClient.GetOrderEmailLog(orderNumber);
                }
                catch (GDMSErrorResponseException exc)
                {
                    StringBuilder errorMsg = new StringBuilder();
                    errorMsg.AppendLine("There was an error processing your request.");
                    errorMsg.AppendFormat(" {0}.", exc.ErrorMessage);
                    foreach (string error in exc.Errors)
                    {
                        errorMsg.AppendFormat(" {0}.", error);
                    }

                    ErrorMessage.Value = errorMsg.ToString();
                    string logMsg = String.Format("GDMS::Error retrieving order info for file #{0}. LoanID: {1}, GDMS Company Id: {2}, GDMS Username: {3}. Error message: {4}",
                                        orderNumber, LoanID, credentials.CompanyId, credentials.UserName, errorMsg.ToString());
                    Tools.LogWarning(logMsg, exc);

                    return null;
                }
            }
            if (allEmailLogs == null)
                return null;
            return allEmailLogs.First(o => o.EmailLogID == emailLogID);
        }
    }
}
