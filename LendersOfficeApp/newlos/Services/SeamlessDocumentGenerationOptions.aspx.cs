﻿namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.ObjLib.DocumentGeneration;
    using LendersOffice.Security;

    public partial class SeamlessDocumentGenerationOptions : BaseLoanPage
    {
        protected void PageLoad(object sender, EventArgs e)
        {
        }

        protected Guid VendorId
        {
            get
            {
                var values = Request.QueryString.GetValues("VendorId");
                if (values.Length > 0)
                    return new Guid(values[0]);

                throw new NotFoundException("Document Vendor not found", "VendorId invalid Guid");
            }
        }

        protected bool VendorIsLegacyDocMagic
        {
            get { return this.VendorId == Guid.Empty; }
        }

        protected bool IsDisplayDocumentValidationWarning { get; set; }
        protected List<E_RoleT> rolesToRetrieve = new List<E_RoleT>
        {
            E_RoleT.LoanOpener,
            E_RoleT.LoanOfficer,
            E_RoleT.Processor,
            E_RoleT.LenderAccountExecutive,
            E_RoleT.Underwriter
        };

        protected void PageInit(object sender, EventArgs e)
        {
            this.RegisterJsScript("utilities.js");

            ClientScript.RegisterHiddenField("ValidationWarningId", "");
            var vendor = DocumentVendorFactory.Create(BrokerUser.BrokerId, VendorId);
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
            AlertMessage.Value = "";
            
            //OPM 115489: Users will need to be able to read these fields even if they don't have access to them normally.
            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(LoanID, typeof(SeamlessDocumentGenerationOptions));
            dataLoan.InitLoad();

            bool allowEPortal = RequestHelper.GetBool("allowEPortalService");
            bool disableESign = RequestHelper.GetBool("disableESign");
            bool enableEClose = RequestHelper.GetBool("allowEClose");
            bool allowManualFulfillment = RequestHelper.GetBool("allowManualFulfillment") && !DocumentVendorBrokerSettings.BlockManualFulfillmentForVendor(VendorId);
            string packageType = RequestHelper.GetSafeQueryString("packageType");

            RegisterJsStruct("Settings", vendor.Skin);

            if (!this.IsPostBack)
            {
                this.PollingId.Value = Guid.NewGuid().ToString();
            }

            var permResponse = vendor.GetPermissions(LoanID, allowEPortal, disableESign, enableEClose, allowManualFulfillment, 
                                                        packageType, PrincipalFactory.CurrentPrincipal);
            if (permResponse.HasError)
            {
                AlertMessage.Value = "There was an error retrieving your permissions." + permResponse.ErrorMessage;
            }

            if (vendor.Config.PreviewExportPath == default(Uri) ||
                string.IsNullOrEmpty(vendor.Config.PreviewExportPath.AbsoluteUri))
            {
                PreviewDocumentsButton.Visible = false;
            }

            var permissions = permResponse.Result;
            RegisterJsStruct("Permissions", permissions);

            CFM.Type = "19";
            CFM.EmailField = "m_tbEmailTo";

            if (false == IsPostBack)
            {
                Package.Value = packageType;
                Program.Value = HttpContext.Current.Request.QueryString["planCode"];

                m_cbMERSRegistration.Visible = permissions.EnableMERSRegistration;
                m_cbEmailDocuments.Enabled = permissions.EnableEmailDocuments;
                m_cbRequirePassword.Enabled = permissions.EnableEmailPasswords;
                m_cbNotifyOnRetrieval.Enabled = permissions.EnableEmailRetrievalNotification;

                AllowEPortalService.Value = BoolToYN(permissions.EnableEDisclose);
                DisableESign.Value = BoolToYN(!permissions.EnableESign);

                this.WillECloseSection.Visible = permissions.EnableEClose;

                SelectedForms.Value = RequestHelper.GetSafeQueryString("selectedForms");

                var appData = dataLoan.GetAppData(0); // use the Primary applicant.

                m_ddlRequirePasswordType.Items.Add(new ListItem("Loan Number", dataLoan.sLNm));
                if (!string.IsNullOrEmpty(appData.aBLastNm))
                {
                    m_ddlRequirePasswordType.Items.Add(new ListItem("Primary Borrower Last Name", appData.aBLastNm));
                }
                if (appData.aSsn.Length == 11) // SK, only include this as an option if the applicant actually has the information filled in.
                {
                    m_ddlRequirePasswordType.Items.Add(new ListItem("Last 4 Digits of Primary SSN", appData.aSsn.Substring(7)));
                }
                m_ddlRequirePasswordType.Items.Add(new ListItem("Other", ""));

                BName.Value = appData.aBNm;
                BStreet.Value = appData.aBAddr;
                BCity.Value = appData.aBCity;
                BState.Value = appData.aBState;
                BZip.Value = appData.aBZip;
                BPhone.Value = appData.aBHPhone; //!? what about work phone / cellphone?

                m_bDeliveryInfo.Visible = !vendor.Config.DisableManualFulfilmentAddr;

                NotifyOtherAdditionalAgentsTextBox.ReadOnly = true;
            }
            else // if IsPostBack
            {
                if (string.IsNullOrEmpty(Request["ValidationWarningId"]) == false)
                {
                    // 3/31/2014 dd - OPM 174204 - Even response has validation warning but user decide to proceed.
                    ContinueWithValidationWarning(Request["ValidationWarningId"]);
                }
            }

            // 3/18/14 gf - opm 174489, Always hide Allow Online Signing for USDA and add ability to hide it for FHA by lender bit.
            m_phAllowOnlineSigning.Visible = Broker.IsDocMagicESignEnabled && (DisableESign.Value != "Y") 
                && (dataLoan.sLT != E_sLT.UsdaRural || Broker.ShowAllowOnlineSigningOptionForUSDA)
                && !(Broker.HideAllowOnlineSigningOptionForFHA && dataLoan.sLT == E_sLT.FHA);

            
            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                dataLoan.sHasClosingCostArchiveInUnknownStatus)
            {
                var archive = dataLoan.sClosingCostArchiveInUnknownStatus;
                var errorMessage = ErrorMessages.ArchiveError.UnableToGenerateDocumentsBecuaseOfUnknownArchive(
                    archive.DateArchived,
                    archive.ClosingCostArchiveType,
                    userCanDetermineStatus: false);

                this.AlertMessage.Value = errorMessage;
                this.FatalErrorRepeeater.DataSource = new string[] { errorMessage };
                this.FatalErrorRepeeater.DataBind();
                this.FatalErrors.Visible = true;
                this.m_bCreateDocuments.Visible = false;
            }

            if (BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
            {
                rolesToRetrieve.Add(E_RoleT.Pml_LoanOfficer);
                rolesToRetrieve.Add(E_RoleT.Pml_BrokerProcessor);
            }

            LoanAssignmentContactTable loanAssignmentContactTable = new LoanAssignmentContactTable(BrokerUser.BrokerId, LoanID);
            AdditionalAgentsRepeater.DataSource = rolesToRetrieve.ToDictionary(role => role, role => loanAssignmentContactTable.FindByRoleT(role));
            AdditionalAgentsRepeater.DataBind();
        }

        private void ContinueWithValidationWarning(string validationWarningId)
        {
            throw CBaseException.GenericException("We no longer handle Validation Errors in the UI and support continuing with a validation warning");
        }

        protected static string BoolToYN(bool input)
        {
            return input ? "Y" : "N";
        }

        protected void bCreateDocumentsClickHandler(object sender, EventArgs e)
        {
            var vendor = DocumentVendorFactory.Create(BrokerUser.BrokerId, VendorId);

            bool allowEPortal = RequestHelper.GetBool("allowEPortalService");
            bool disableESign = RequestHelper.GetBool("disableESign");
            bool allowEClose = RequestHelper.GetBool("allowEClose");
            bool allowManualFulfilment = RequestHelper.GetBool("allowManualFulfillment");

            string packageType = RequestHelper.GetSafeQueryString("packageType");

            var permResponse = vendor.GetPermissions(LoanID, allowEPortal, disableESign, allowEClose, allowManualFulfilment, packageType, PrincipalFactory.CurrentPrincipal);
            if (permResponse.HasError)
            {
                AlertMessage.Value = "There was an error retrieving your permissions." + permResponse.ErrorMessage;
                return;
            }

            DocumentGenerator documentGenerator = new DocumentGenerator(vendor, LoanID, ApplicationID);

            IEnumerable<FormInfo> formsSelected = null;
            if (!string.IsNullOrEmpty(SelectedForms.Value))
            {
                formsSelected = SelectedForms.Value.Split(';').Select(x =>
                    {
                        string[] formData = x.Split(':');
                        return new FormInfo(borrowerNumber: formData[0], id: formData[1]);
                    });
            }

            if (vendor.Config.PlatformType == E_DocumentVendor.IDS)
            {
                this.AddInitialRequestStatusToAutoExpireCache();
            }

            List<string> eDisclosureNotificationEmails = null;

            if (cbNotifyAdditionalAgents.Checked)
            {
                IEnumerable<int> selectedIndices = AdditionalAgentsRepeater.Items.Cast<RepeaterItem>().SelectIndicesWhere(repeaterItem => (repeaterItem.FindControl("NotifyAdditionalAgentsCheckbox") as CheckBox).Checked);

                LoanAssignmentContactTable loanAssignmentContactTable = new LoanAssignmentContactTable(BrokerUser.BrokerId, LoanID);
                eDisclosureNotificationEmails = selectedIndices.Select(index => loanAssignmentContactTable.FindByRoleT(rolesToRetrieve[index]).Email).ToList();
                if (NotifyOtherAdditionalAgentsCheckbox.Checked)
                {
                    eDisclosureNotificationEmails.AddRange(NotifyOtherAdditionalAgentsTextBox.Text.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
                }
            }

            bool isPreviewOrder = (sender as WebControl).ID == "PreviewDocumentsPostbackButton";
            DocumentVendorResult<IDocumentGenerationResult> generationResult = null;

            try
            {
                generationResult = documentGenerator.Generate(
                packageTypeId: packageType,
                loanProgramId: HttpContext.Current.Request.QueryString["planCode"],
                permissions: permResponse.Result,
                isEDisclosureRequested: m_cbSendEDisclosures.Checked,
                isECloseRequested: this.WillEClose.Checked,
                isESignRequested: m_cbAllowOnlineSigning.Checked,
                isMersRegistrationEnabled: m_cbMERSRegistration.Checked,
                principal: BrokerUser,
                emailDocumentInformation: m_cbEmailDocuments.Checked
                    ? new DocumentGenerator.EmailDocumentInformation(
                        emailAddress: m_tbEmailTo.Text,
                        isPasswordRequired: m_cbRequirePassword.Checked,
                        requiredPassword: m_tbPassword.Text,
                        passwordHint: m_ddlRequirePasswordType.SelectedItem.Text,
                        isNotificationOnRetrievalRequested: m_cbNotifyOnRetrieval.Checked)
                    : null,
                manualFulfillmentInformation: m_cbDSIPrintAndDeliver.Checked
                    ? new DocumentGenerator.ManualFulfillmentInformation(
                        name: Name.Value,
                        streetAddress: Street.Value,
                        city: City.Value,
                        state: State.Value,
                        zipcode: Zip.Value,
                        attention: Attention.Value,
                        phone: Phone.Value,
                        notes: DSINotes.Value)
                    : null,
                documentFormat: m_rbDocumentFormatDocMaster.Checked ? "DBK" : "PDF",
                forms: formsSelected,
                isPreviewOrder: isPreviewOrder,
                additionalNotificationEmails: eDisclosureNotificationEmails);
            }       
            catch (CBaseException err)
            {
                AlertMessage.Value = err.UserMessage;
                return;
            }

            var response = generationResult;
            if (response.HasError)
            {
                if (response.IsValidationError)
                {
                    throw CBaseException.GenericException("We no longer handle Validation Errors in the UI");
                }
                else
                {
                    FatalErrorRepeeater.DataSource = response.ErrorMessage.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                    FatalErrorRepeeater.DataBind();
                    FatalErrors.Visible = true;
                    AlertMessage.Value = ErrorMessages.DocMagicGenericError + Environment.NewLine + response.ErrorMessage;
                }

                return;
            }
            else if (!string.IsNullOrEmpty(response.ErrorMessage))
            {
                AlertMessage.Value = response.ErrorMessage;
            }
            else if (!string.IsNullOrWhiteSpace(response.Result.ErrorMessage))
            {
                string msg = "Document request completed with some errors: " + Environment.NewLine + response.Result.ErrorMessage;
                AlertMessage.Value = msg;
            }

            DisplayGenerationResult(response.Result, vendor);
        }

        private void DisplayGenerationResult(IDocumentGenerationResult result, IDocumentVendor vendor)
        {
            if (m_cbAllowOnlineSigning.Checked)
            {
                ESignConfirmation.Visible = true;
                HideButtons.Value = "true";
                if (result.IsDisclosurePackage)
                {
                    ClientScript.RegisterHiddenField("RefreshParent", "true");
                }
            }
            else
            {
                if (result.DocsAreForViewing)
                {
                    //If we're redirecting locally, let the destination page know to refresh the parent.
                    //We'll know we're redirecting locally if it's not a well-formed url (those are always local)
                    //Or if it's not an absolute url (always local)
                    //or if it's a well-formed absolute url which includes our hostname
                    var temp = result.ViewUrl.ToUriOrNull();
                    if (temp == null ||
                        !temp.IsAbsoluteUri ||
                        result.ViewUrl.ToLower().Contains(Request.Url.Host.ToLower()))
                    {
                        var url = result.ViewUrl;
                        //Do we need to tell the destination page to refresh the parent when it shows up?
                        //This only happens for disclosure packages.
                        if (result.IsDisclosurePackage)
                        {
                            url = url + (url.Contains("?") ? "&" : "?") + "refreshParent=t";
                        }

                        if (result.CreatedArchiveInUnknownStatus)
                        {
                            url = url + (url.Contains("?") ? "&" : "?") + "displayDisclosureStatus=t";

                            if (result.UnknownArchiveType.Value == ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate)
                            {
                                url = url + "&isLoanEstimate=t&loanEstimateArchiveDate=" + HttpUtility.UrlEncode(result.UnknownArchiveDate);

                                if (result.UnknownArchiveWasSourcedFromPendingCoC)
                                {
                                    url = url + "&wasSourcedFromPendingCoC=t";
                                }
                            }
                            else
                            {
                                if (result.HasBothCDAndLEArchiveInUnknownStatus)
                                {
                                    url += "&hasBothCDAndLEArchiveInUnknownStatus=t&closingDisclosureArchiveDate=" + HttpUtility.UrlEncode(result.UnknownArchiveDate);
                                }
                            }
                            
                        }

                        Response.Redirect(url, false);
                        return;
                    }

                    //Otherwise, we'll have already opened the window. Tell our audit window to refresh the treeview and close itself.
                    ClientScript.RegisterHiddenField("Autoclose", "true");
                    ClientScript.RegisterHiddenField("RefreshTreeview", "true");

                    if (vendor.Config.PlatformType == E_DocumentVendor.IDS)
                    {
                        this.AddResultDetailsToAutoExpireCache(result);
                    }
                }
                else if (result.DocsAreForDownload)
                {
                    if (result.IsDisclosurePackage)
                    {
                        ClientScript.RegisterHiddenField("RefreshParent", "true");
                    }

                    DownloadDocs.Value = "true";
                    DownloadDBKURL = result.DownloadUrl;
                }
            }
        }

        private string GetAutoExpireCacheKeyForRequestStatus()
        {
            return Tools.ShortenGuid(new Guid(this.PollingId.Value)) 
                + Tools.ShortenGuid(this.BrokerUser.UserId);
        }

        private void AddInitialRequestStatusToAutoExpireCache()
        {
            var status = new DocumentGenerationRequestStatus
            {
                RequestCompleted = false
            };

            var key = this.GetAutoExpireCacheKeyForRequestStatus();

            AutoExpiredTextCache.AddToCache(
                SerializationHelper.JsonNetSerialize(status),
                TimeSpan.FromMinutes(10),
                key);
        }

        private void AddResultDetailsToAutoExpireCache(IDocumentGenerationResult result)
        {
            var status = new DocumentGenerationRequestStatus
            {
                RequestCompleted = true,
                ArchiveDetails = new DocumentGenerationArchiveDetails
                {
                    CreatedArchiveInUnknownStatus = result.CreatedArchiveInUnknownStatus,
                    UnknownArchiveType = result.UnknownArchiveType,
                    UnknownArchiveDate = result.UnknownArchiveDate,
                    UnknownArchiveWasSourcedFromPendingCoC = result.UnknownArchiveWasSourcedFromPendingCoC,
                    HasBothCDAndLEArchiveInUnknownStatus = result.HasBothCDAndLEArchiveInUnknownStatus
                }

            };

            var key = this.GetAutoExpireCacheKeyForRequestStatus();

            AutoExpiredTextCache.UpdateCache(
                SerializationHelper.JsonNetSerialize(status),
                key);
        }

        protected string DownloadDBKURL = "";

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            UseNewFramework = false; // There is no service page, so set this to false so that the smoke test won't choke on this page
            DisplayCopyRight = false;
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion

        protected void AdditionalAgentsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            KeyValuePair<E_RoleT, EmployeeLoanAssignment> employee = (KeyValuePair<E_RoleT, EmployeeLoanAssignment>)(e.Item.DataItem);

            CheckBox additionalAgentCheckbox = e.Item.FindControl("NotifyAdditionalAgentsCheckbox") as CheckBox;

            string roleName = string.Empty;
            if (!specialMappedRoles.TryGetValue(employee.Key, out roleName))
            {
                roleName = Role.Get(employee.Key).ModifiableDesc;
            }

            additionalAgentCheckbox.Text = $"{roleName} - {(string.IsNullOrEmpty(employee.Value?.FullName) ? "unassigned" : employee.Value.FullName)}";
            if (string.IsNullOrEmpty(employee.Value?.FullName))
            {
                additionalAgentCheckbox.Enabled = false;
            }
        }

        private Dictionary<E_RoleT, string> specialMappedRoles = new Dictionary<E_RoleT, string>
        {
            { E_RoleT.Pml_LoanOfficer, "Loan Officer (External)" }
        };
    }
}
