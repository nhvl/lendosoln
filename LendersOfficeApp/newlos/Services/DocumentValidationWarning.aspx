﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentValidationWarning.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.DocumentValidationWarning" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Document Validation Warning</title>
    <style>
        .ErrorMessage { color:Red;}
    </style>
</head>
<body class="RightBackground" style="margin-left:0px">
    <script type="text/javascript">
    function _init() {
        resizeForIE6And7(600, 500);
    }

    </script>
    <form id="form1" runat="server">
    <table id="MainTable" cellspacing="2" cellpadding="0" border="0" width="100%" class="FieldLabel">
        <tr><td nowrap="nowrap" class="MainRightHeader">Document Validation Warning</td></tr>
        <tr><td></td></tr>
        <tr><td>Loan Number: <ml:EncodedLiteral ID="sLNm" runat="server" /></td></tr>
        <tr><td class="ErrorMessage">
                    <asp:Repeater runat="server" ID="ErrorRepeater">
                <HeaderTemplate>
                    <ul>
                </HeaderTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
                <ItemTemplate>
                    <li><%# AspxTools.HtmlString(Container.DataItem.ToString()) %></li>
                </ItemTemplate>    
            </asp:Repeater>
        </td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td align="center">
                <asp:Button ID="m_cancelBtn" runat="server" Text="Cancel Document Generation" />
                &nbsp;&nbsp;
                <asp:Button ID="m_continueBtn" runat="server" Text="Continue" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
