﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="DocMagicDeliveryInformation.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.DocMagicDeliveryInformation" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <STYLE>
      .LeftPadding { PADDING-LEFT: 5px }
    </STYLE>
</head>
<body class="RightBackground" style="MARGIN-LEFT:0px" MS_POSITIONING="FlowLayout" onload="init();" bgcolor="gainsboro">
    <h4 class="page-header">Delivery Information</h4>
    <form id="DocMagicDeliveryInformation" runat="server">
    <div>
        <table id=MainTable cellspacing=0 cellpadding=2 border=0 width="100%">
            <tbody>
                <tr>
                    <td class="FieldLabel LeftPadding" nowrap colspan=1 width="70">Attention </td>
                    <td width="125"><asp:TextBox ID="Attention" runat="server" Width="125"/></td>
                </tr>
                <tr>
                    <td class="FieldLabel LeftPadding" nowrap colspan=1>Name </td>
                    <td><asp:TextBox ID="Name" runat="server" Width="125" /></td>
                    <td><asp:RequiredFieldValidator ID="rfvName" ControlToValidate="Name" runat="server"><IMG runat="server" src="../../images/require_icon.gif" > </asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td class="FieldLabel LeftPadding" nowrap colspan=1>Street </td>
                    <td><asp:TextBox ID="Street" runat="server" Width="125"/></td>
                    <td><asp:RequiredFieldValidator ID="rfvStreet" ControlToValidate="Street" runat="server"><IMG runat="server" src="../../images/require_icon.gif" > </asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td class="FieldLabel LeftPadding" nowrap colspan=1>City </td>
                    <td><asp:TextBox ID="City" runat="server" Width="125"/></td>
                    <td><asp:RequiredFieldValidator ID="rfvCity" ControlToValidate="City" runat="server"><IMG runat="server" src="../../images/require_icon.gif" > </asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td class="FieldLabel LeftPadding" nowrap colspan=1>State </td>
                    <td><ml:StateDropDownList ID="State" runat="server" Width="125"/></td>
                    <td><asp:CustomValidator ID="cvState" runat="server" ClientValidationFunction="validateState"><IMG runat="server" src="../../images/require_icon.gif" ></asp:CustomValidator></td>
                </tr>
                <tr>
                    <td class="FieldLabel LeftPadding" nowrap colspan=1>Zip </td>
                    <td><ml:ZipcodeTextBox ID="Zip" runat="server" preset="zipcode"/></td>
                    <td><asp:RequiredFieldValidator ID="rfvZip" ControlToValidate="Zip" runat="server"><IMG runat="server" src="../../images/require_icon.gif" > </asp:RequiredFieldValidator></td>
                </tr>
               <tr>
                    <td class="FieldLabel LeftPadding" nowrap colspan=1>Phone </td>
                    <td><ml:PhoneTextBox ID="Phone" runat="server" Width="125" preset="phone"/></td>
                    <td><asp:RequiredFieldValidator ID="rfvPhone" ControlToValidate="Phone" runat="server"><IMG runat="server" src="../../images/require_icon.gif" > </asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td class="FieldLabel LeftPadding" nowrap colspan=2>Notes</td>
                </tr>
                <tr>
                    <td colspan=3 align="center"> <asp:TextBox ID="DSINotes" runat="server" TextMode="MultiLine" Rows="3" Width="200"></asp:TextBox></td>
                </tr>
            </tbody>
        </table>
    </div>
    <uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg>
    
       <div align="center">
        <table>
        <tbody>
            <tr>
                <td>
                    <asp:Button ID="OK"  OnClick="OK_OnClick" runat="server" Text="OK" />
                    <input type="button" value="Cancel" onclick="onClosePopup();"> 
                </td>
            </tr>
        </tbody>
        </table>
        </div>
        
    </form>
    <script type="text/javascript">
        function init() {
            resize(200, 325);  

                addEventHandler(name, 'blur', enableDisableOK, false);
                addEventHandler(street, 'blur', enableDisableOK, false);
                addEventHandler(city, 'blur', enableDisableOK, false);
                addEventHandler(state, 'blur', enableDisableOK, false);
                addEventHandler(zip, 'blur', enableDisableOK, false);
                addEventHandler(phone, 'blur', enableDisableOK, false);
            enableDisableOK();
        }
        function enableDisableOK()
        {
            Page_ClientValidate();
            ok.disabled = !Page_IsValid;
        }
        function validateState(source, arguments)
        {
            var stateVal = state.options[state.selectedIndex].value;
            if (stateVal == ''){
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }
        var name = <%= AspxTools.JsGetElementById(Name) %>;
	    var street = <%= AspxTools.JsGetElementById(Street) %>;
	    var city = <%= AspxTools.JsGetElementById(City) %>;
	    var state = <%= AspxTools.JsGetElementById(State) %>;
	    var zip = <%= AspxTools.JsGetElementById(Zip) %>;
	    var phone = <%= AspxTools.JsGetElementById(Phone) %>;
	    var ok = <%= AspxTools.JsGetElementById(OK) %>;
	</script>
</body>
</html>
