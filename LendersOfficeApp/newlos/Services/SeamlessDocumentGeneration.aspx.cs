﻿namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Services;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.ObjLib.DocMagicLib;
    using LendersOffice.ObjLib.DocumentGeneration;
    using LendersOffice.Security;

    public sealed class AuditResult
    {
        public AuditResult()
        {
            Errors = false;
            IntegrationError = false;
            IntegrationErrorMessage = string.Empty;
            SummaryText = string.Empty;
            AuditHtml = string.Empty;
            NextPageUrl = string.Empty;
        }

        public bool Errors { get; set; }
        public bool IntegrationError { get; set; }
        public string IntegrationErrorMessage { get; set; }
        public string SummaryText { get; set; }
        public string AuditHtml { get; set; }
        public string NextPageUrl { get; set; }

        public static AuditResult Error(string errorMessage)
        {
            return new AuditResult
            {
                Errors = true,
                IntegrationError = true,
                IntegrationErrorMessage = errorMessage,
                SummaryText = "An error occurred while connecting to the remote server: " + errorMessage,
            };
        }
    }

    public partial class SeamlessDocumentGeneration : BaseLoanPage
    {
        protected IDocumentVendor Vendor;

        protected Guid VendorId
        {
            get
            {
                var values = Request.QueryString.GetValues("VendorId");
                if (values.Length > 0)
                {
                    return new Guid(values[0]);
                }

                throw new NotFoundException("Document Vendor not found", "VendorId invalid Guid");
            }
        }

        protected override void LoadData()
        {
            // Hide transfer to row **before** the checks that will abort load.
            // Otherwise, the user will be able to click it and receive an error.
            if (!Vendor.Skin.SupportsTransferToInvestors)
            {
                TransferToRow.Attributes.Add("class", "Hidden");
            }

            //OPM 115489: Users will need to be able to read these fields even if they don't have access to them normally.
            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(LoanID, typeof(SeamlessDocumentGeneration));
            dataLoan.InitLoad();

            var packages = Vendor.GetAvailableDocumentPackages(LoanID, PrincipalFactory.CurrentPrincipal);
            if (packages.HasError)
            {
                m_AlertMessage.Value = packages.ErrorMessage;
                Disable.Value = true.ToString();
                return;
            }

            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                if (dataLoan.sIsInitialLoanEstimateDateAssociatedWithInvalidArchive)
                {
                    this.RegisterJsGlobalVariables("sIsInitialLoanEstimateDateAssociatedWithInvalidArchive", true);
                }
                else if (dataLoan.sHasClosingCostArchiveInUnknownStatus)
                {
                    var archive = dataLoan.sClosingCostArchiveInUnknownStatus;

                    this.RegisterJsGlobalVariables("sHasArchiveInUnknownStatus", true);
                    this.RegisterJsGlobalVariables("HasBothCDAndLEArchiveInUnknownStatus", dataLoan.sHasBothCDAndLEArchiveInUnknownStatus);
                    this.ArchiveStatusUnknownMessage.Text = ErrorMessages.ArchiveError.UnableToGenerateDocumentsBecuaseOfUnknownArchive(
                        archive.DateArchived,
                        archive.ClosingCostArchiveType,
                        userCanDetermineStatus: true);

                    bool isLoanEstimate = archive.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate;

                    if (isLoanEstimate)
                    {
                        this.RegisterJsGlobalVariables("sHasLoanEstimateArchiveInUnknownStatus", true);
                        this.RegisterJsGlobalVariables("UnknownLoanEstimateWasSourcedFromCoC", archive.WasSourcedFromPendingCoC);
                        this.InvalidateUnknownArchiveMessage.Text = ErrorMessages.ArchiveError.ConfirmUnknownArchiveInvalidation(
                            archive.DateArchived);
                    }
                }
            }

            // Default to null for legacy DocMagic seamless.
            var vendor = VendorId == Guid.Empty ? null : DocumentVendorFactory.AvailableVendors().First(v => v.VendorId == VendorId);

            BindPackageTypes(packages.Result, vendor);
            BindPlanCodePicker(LoanID);

            if (vendor != null && vendor.IsTest)
            {
                TestEnvironmentDisclaimerVendorName.InnerText = vendor.VendorName;

                string reason = string.Empty;
                if (vendor.VendorIsTest)
                {
                    reason += "due to lender configuration";

                    if (vendor.UserIsTest)
                    {
                        reason += " and user permission setting";
                    }
                }
                else if (vendor.UserIsTest)
                {
                    reason += "due to user permission setting";
                }

                TestEnvironmentDisclaimerReason.InnerText = reason;
            }
            else
            {
                TestEnvironmentDisclaimerSpan.Visible = false;
                TestEnvironmentDisclaimerDivider.Visible = false;
            }

            if (!string.IsNullOrEmpty(dataLoan.sDocMagicPlanCodeNm)) m_ddlPlanCode.SelectedValue = dataLoan.sDocMagicPlanCodeNm;
            else if (Vendor.Skin.AutomaticPlanCodes) m_ddlPlanCode.Attributes["disabled"] = "disabled";

            m_transferToDesc.Value = dataLoan.sDocMagicTransferToInvestorNm;
            m_transferToCode.Value = dataLoan.sDocMagicTransferToInvestorCode;

            m_transferToDescLabel.InnerHtml = string.IsNullOrEmpty(dataLoan.sDocMagicTransferToInvestorNm) 
                ? "&#8211; None &#8211;" 
                : AspxTools.HtmlString(dataLoan.sDocMagicTransferToInvestorNm);

            ClientScript.RegisterHiddenField("PlanCodeLabel", Vendor.Skin.NameForPlanCodes);
        }

        /// <summary>
        /// Binds the received packages to the Package Type dropdownlist. Any packages supported by
        /// the vendor but not configured in LendingQB (not including custom packages) should trigger
        /// a notification email and then record itself to the vendor config so the notification is not
        /// sent more than once.
        /// </summary>
        /// <param name="packages">A package list received from the vendor.</param>
        /// <param name="vendor">The document vendor.</param>
        private void BindPackageTypes(List<DocumentPackage> packages, VendorConfig vendor)
        {
            bool vendorConfigDirty = false;
            m_ddlPackageType.Items.Add(new ListItem(string.Empty, string.Empty));

            foreach (var package in packages)
            {
                if (vendor == null)
                {
                    m_ddlPackageType.Items.Add(new ListItem(package.Description.ToString(), GetPackageString(package)));
                }
                else if (vendor.PackageExists(package.Type))
                {
                    m_ddlPackageType.Items.Add(new ListItem(package.Description.ToString(), GetPackageString(package)));

                    if (vendor.UnconfiguredPackages.Contains(package.Type))
                    {
                        vendor.UnconfiguredPackages.Remove(package.Type);
                        vendorConfigDirty = true;
                    }
                }
                else
                {
                    if (!vendor.UnconfiguredPackages.Contains(package.Type))
                    {
                        vendor.UnconfiguredPackages.Add(package.Type);
                        vendorConfigDirty = true;

                        // Send email to notify IS of unconfigured package.
                        if (ConstAppDavid.CurrentServerLocation == ServerLocation.Production)
                        {
                            string subject = "Unconfigured Package \"" + package.Type + "\" for " + vendor.VendorName;
                            string body = "The document package \"" + package.Type + "\" is not configured in LendingQB "
                                + "on Production. Please contact " + vendor.VendorName + " for configuration information.";
                            EmailUtilities.SendToIntegrationSpecialist(subject, body);
                        }
                    }
                }
            }

            if (vendorConfigDirty)
            {
                VendorConfig.Save(vendor);
            }
        }

        private static string GetPackageString(DocumentPackage p)
        {
            return p.Type + ":" + BoolToYN(p.AllowEPortal) + ":" + BoolToYN(p.DisableESign) + ":" + BoolToYN(p.AllowEClose) + ":" + BoolToYN(p.AllowManualFulfillment);
        }

        private static string BoolToYN(bool input)
        {
            return input ? "Y" : "N";
        }

        private void BindPlanCodePicker(Guid LoanId)
        {
            var planCodes = Vendor.GetAvailablePlanCodes(LoanId, PrincipalFactory.CurrentPrincipal);
            if (planCodes.HasError)
            {
                m_AlertMessage.Value = planCodes.ErrorMessage;
                Disable.Value = true.ToString();
                return;
            }

            BindPlanCodeDictionary(m_ddlPlanCode, planCodes.Result);
        }

        private void BindPlanCodeDictionary(DropDownList planCodes, Dictionary<string, IEnumerable<PlanCode>> codes)
        {
            foreach (var type in codes.Keys)
            {
                planCodes.Items.Add(string.Empty);

                string header = type.ToUpper() + " " + Vendor.Skin.NameForPlanCodes.ToUpper() + "S";
                planCodes.Items.Add(new ListItem("<----- " + header + " ----->", ""));

                if (type.Equals("Investor", StringComparison.OrdinalIgnoreCase))
                {
                    foreach (var group in codes[type].ToLookup(p => p.Investor))
                    {
                        planCodes.Items.Add(new ListItem("<----- " + group.Key + " ----->", ""));
                        foreach (var planCode in group.OrderBy(a => a.Description))
                        {
                            planCodes.Items.Add(new ListItem(planCode.Description, planCode.Code));
                        }
                    }
                }
                else
                {
                    foreach (var planCode in codes[type].OrderBy(a => a.Description))
                    {
                        planCodes.Items.Add(new ListItem(planCode.Description, planCode.Code));
                    }
                }
            }
        }

        [WebMethod] //For Framework Doc vendors, PackageType == PackageID, ReqPlanCode == LoanProgramID
        public static AuditResult PerformDocMagicAudit(Guid sLId, Guid AppId, string PackageType, string ReqPlanCode, string TransferToDescription, string TransferToCode, Guid VendorId, string TemporaryArchiveSource)
        {
            try
            {
                var vendor = DocumentVendorFactory.Create(PrincipalFactory.CurrentPrincipal.BrokerId, VendorId);
                var auditor = new DocumentAuditor(vendor, sLId, AppId);
                var package = vendor.Config.RetrievePackage(PackageType);

                DocumentAuditor.TemporaryArchiveSource? tempArchiveSource = 
                    DetermineTempArchiveSource(auditor, package, TemporaryArchiveSource);

                var auditResult = auditor.Audit(PackageType, ReqPlanCode, TransferToCode, TransferToDescription, PrincipalFactory.CurrentPrincipal, tempArchiveSource);
                if (auditResult.HasError)
                {
                    return AuditResult.Error(auditResult.ErrorMessage);
                }

                return new AuditResult
                {
                    NextPageUrl = "SeamlessDocumentGenerationOptions.aspx?loanid=" + sLId
                        + "&appid=" + AppId
                        + "&packageType=" + HttpUtility.UrlEncode(PackageType)
                        + "&planCode=" + HttpUtility.UrlEncode(ReqPlanCode),
                    AuditHtml = AuditHtmlGenerator.GenerateAuditHtml(auditResult),
                    Errors = auditResult.Result.Results.Any(a => a.Severity == AuditSeverity.Fatal)
                };
            }
            catch (AccessDenied e)
            {
                return new AuditResult
                {
                    Errors = true,
                    IntegrationError = true,
                    IntegrationErrorMessage = e.UserMessage,
                    SummaryText = "User does not have permission to generate this document package: " + e.UserMessage
                };
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return AuditResult.Error(e.UserMessage);
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                return AuditResult.Error(e.Message);
            }
        }

        private static DocumentAuditor.TemporaryArchiveSource? DetermineTempArchiveSource(
            DocumentAuditor auditor,
            VendorConfig.DocumentPackage package,
            string archiveSourceFromClient)
        {
            DocumentAuditor.TemporaryArchiveSource? tempArchiveSource = null;

            if (auditor.MustSpecifyTemporaryArchiveSource(package) &&
                !string.IsNullOrEmpty(archiveSourceFromClient))
            {
                if (archiveSourceFromClient == "live")
                {
                    tempArchiveSource = DocumentAuditor.TemporaryArchiveSource.LiveData;
                }
                else if (archiveSourceFromClient == "pending")
                {
                    tempArchiveSource = DocumentAuditor.TemporaryArchiveSource.PendingArchive;
                }
                else
                {
                    throw new CBaseException(
                        ErrorMessages.Generic,
                        "Invalid temporary archive source.");
                }
            }

            return tempArchiveSource;
        }

        [WebMethod]
        public static bool NeedsPlanCode(Guid sLId, Guid VendorId)
        {
            HttpContext.Current.Response.AddHeader("Connection", "close");
            if (!DocumentVendorFactory.Create(PrincipalFactory.CurrentPrincipal.BrokerId, VendorId).Skin.AutomaticPlanCodes)
            {
                return false;
            }

            var dataLoan = new CPageData(sLId, new[] { "sDocMagicPlanCodeId", "sDocMagicPlanCodeNm" });
            dataLoan.InitLoad();

            //Need to use the plan code name, because it gets cleared out if the plan code doesn't exist (but the id doesn't).
            if (string.IsNullOrEmpty(dataLoan.sDocMagicPlanCodeNm))
            {
                Guid result = DocMagicPlanCodeEvaluator.CalculateDocMagicPlanCode(sLId);
                if (result == DocMagicPlanCode.PendingUserChoice)
                {
                    return true;
                }

                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.sDocMagicPlanCodeId = result;
                dataLoan.Save();
            }

            return false;
        }

        [WebMethod]
        public static object InvestorsExistForPlanCode(string PlanCode, Guid VendorId)
        {
            var vendorResult = DocumentVendorFactory.Create(PrincipalFactory.CurrentPrincipal.BrokerId, VendorId).GetTransferToInvestors(PlanCode);
            return new
            {
                Error = vendorResult.HasError,
                InvestorsExist = vendorResult.Result != null && vendorResult.Result.Any()
            };
        }

        protected bool SupportsTransferToInvestors
        {
            get { return Vendor.Skin.SupportsTransferToInvestors; }
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "DocMagicDocGeneration";
            this.PageID = "DocMagicDocGeneration";
            this.RegisterJsScript("LQBPopup.js");
        }

        private void PageLoad(object sender, System.EventArgs e)
        {
            Vendor = DocumentVendorFactory.Create(PrincipalFactory.CurrentPrincipal.BrokerId, VendorId);
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.IE9;
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = false; // There is no service page, so set this to false so that the smoke test won't choke on this page
            DisplayCopyRight = false;
            RegisterJsScript("ModelessDlg.js");
            EnableJqueryMigrate = false;
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("ArchiveStatusPopup.js");

            this.Init += new System.EventHandler(this.PageInit);
            this.Load += new System.EventHandler(this.PageLoad);
            base.OnInit(e);
        }

        /// <summary>
        /// Sets a message in the Summary element.
        /// </summary>
        /// <param name="message">The message to set.</param>
        protected void SetSummaryMessage(string message)
        {
            Summary.InnerText = message;
            Summary.Visible = true;

            if (TestEnvironmentDisclaimerSpan.Visible)
            {
                TestEnvironmentDisclaimerDivider.Visible = true;
            }
        }

        protected void FallbackPost_OnClick(object sender, EventArgs args)
        {
            string plancodeValue = m_ddlPackageType.SelectedValue;
            string[] parts = plancodeValue.Split(':');

            AuditResult obj = PerformDocMagicAudit(LoanID, ApplicationID, parts[0], m_ddlPlanCode.SelectedValue, m_transferToDesc.Value, m_transferToCode.Value, VendorId, this.TemporaryArchiveSourceForAudit.Value);
            this.SetSummaryMessage(obj.SummaryText);

            string url = string.Format("{0}&allowEPortalService={1}&disableESign={2}&allowEClose={3}&allowManualFulfillment={4}&vendorId={5}",
                obj.NextPageUrl.Replace("=Y", "=1"), YNToBool(parts[1]), YNToBool(parts[2]), YNToBool(parts[3]), YNToBool(parts[4]), VendorId.ToString());

            m_PageUrl.Value = url;
            AuditResults.InnerHtml = obj.AuditHtml;
            if (obj.Errors)
            {
                m_btnNext.Disabled = true;
            }
            else
            {
                ClientScript.RegisterHiddenField("EnableNxt", "true");
                m_btnNext.Disabled = false;
            }
        }

        private static string YNToBool(string item)
        {
            if (item.Equals("Y", StringComparison.OrdinalIgnoreCase))
            {
                return "T";
            }
            else
            {
                return "F";
            }
        }
    }
}
