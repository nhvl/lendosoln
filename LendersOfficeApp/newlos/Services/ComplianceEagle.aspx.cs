﻿// <copyright file="ComplianceEagleRequestProvider.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Brian Beery
//    Date:   7/28/2014 05:00 PM 
// </summary>
namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Web.UI;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Conversions.ComplianceEagleIntegration;

    /// <summary>
    /// The ComplianceEagle order page.
    /// </summary>
    public partial class ComplianceEagle : BaseLoanPage
    {
        /// <summary>
        /// The response from ComplianceEagle in HTML format.
        /// </summary>
        private string responseHTMLContent;

        /// <summary>
        /// Loads data for the page from the user account and loan file.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var savedCredentials = LendersOffice.Security.ComplianceEagleUserCredentials.Retrieve(this.BrokerUser);
            
            if (!string.IsNullOrEmpty(savedCredentials.Password))
            {
                this.UserName.Text = savedCredentials.UserName;
                //// this.Password set to ConstAppDavid.FakePasswordDisplay in the javascript
                this.RememberMe.Checked = true;
            }

            this.ShowPriorResponse();

            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                this.LoanID,
                typeof(ComplianceEagle));
            loan.InitLoad();

            if (loan.sIsInitialLoanEstimateDateAssociatedWithInvalidArchive)
            {
                this.ErrorMessageDiv.InnerText = ErrorMessages.ArchiveError.InitialLoanEstimateAssociatedWithInvalidArchiveCompliance;
                btnSubmit.Visible = false;
            }
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.PageID = "ComplianceEagle";
            this.PageTitle = "ComplianceEagle Integration";
            this.EnableJqueryMigrate = false;
            this.RegisterService("loanedit", "/newlos/Services/ComplianceEagleService.aspx");
        }

        /// <summary>
        /// Render the XML response from ComplianceEagle as HTML.
        /// </summary>
        private void DisplayResult()
        {
            resultsPlaceholder.Visible = true;
            noResponsePlaceholder.Visible = false;

            resultsPlaceholder.SetRenderMethodDelegate(new RenderMethod(delegate(HtmlTextWriter tw, Control container)
            {
                tw.Write(this.responseHTMLContent);
            }));
        }

        /// <summary>
        /// Load the latest prior response on file and render as HTML.
        /// </summary>
        private void ShowPriorResponse()
        {
            if (!Page.IsPostBack)
            {
                ComplianceEagleStatusHelper responseConvertor = new ComplianceEagleStatusHelper(LoanID);
                this.responseHTMLContent = responseConvertor.GetResponseHTMLFromResponseXML();

                if (!string.IsNullOrEmpty(this.responseHTMLContent))
                {
                    this.DisplayResult();
                }
                else
                {
                    noResponsePlaceholder.Visible = true;
                }
            }
        }
    }
}
