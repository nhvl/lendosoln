﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComplianceEagle.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.ComplianceEagle" %>

<%@ Import Namespace="LendersOffice.Constants" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Conversions.ComplianceEagleIntegration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ComplianceEagle</title>
    <style type="text/css">
        .BoldErrorMessage
        {
            color: red;
            font-weight: bold;
        }

        .ErrorMessage
        {
            color: red;
        }

        #LatestResultsHeader
        {
            background-color: #5778ab;
            color: white;
            font-weight: bold;
            font-family: verdana, arial, helvetica, sans-serif;
            font-size: 11px;
        }

        .CredentialTextbox
        {
            width: 200px;
            margin-left: -40px;
        }

        #OrderTable
        {
            display: block;
            border: none;
            border-color: #ffffff;
            border-collapse: collapse;
            padding-left: 5px;
        }

        #LoadingSection {
            display: none;
        }

        .DelimitingRow
        {
            border-bottom: 2px groove #eeeeee;
        }

        .SpacerCell
        {
            height: 10px;
        }
    </style>
</head>
<body bgcolor="gainsboro">
    <script type="text/javascript">
        function _init() {
            $('#btnSubmit').click(btnSubmit_Click);
            $('#btnSubmit').prop('disabled', true);

            $('.ComplianceEagleProduct').click(onProductChecked);
            $('#MaventReview').click(onMaventReviewProductChecked);
            $('#FraudComplete').click(onFraudCompleteProductChecked);
            $('#Flood').click(onFloodProductChecked);
            $('#FloodQuickCheck').click(onFloodQuickCheckProductChecked);
            $('#AllProducts').click(onAllProductsChecked);

            document.getElementById('_ReadOnly').value = "True";

            if ($('#RememberMe').prop('checked')) {
                $('#Password').val(<%= AspxTools.JsString(ConstAppDavid.FakePasswordDisplay) %>);
            }
        }

        function onAllProductsChecked() {
            var allProductsSelected = $('#AllProducts').prop('checked');
            $('.ComplianceEagleProduct input').prop('checked', allProductsSelected);

            onMaventReviewProductChecked();

            // When all products is selected, Flood takes precedence over FloodQuickCheck
            // If the user then unselects Flood, selects FloodQuickCheck, then unselects
            // all products, the if-statement prevents Flood from still being disabled.
            onFloodProductChecked();
            if (!allProductsSelected) {
                onFloodQuickCheckProductChecked();
            }

            onFraudCompleteProductChecked();
            onProductChecked();
        }

        // MaventReview includes HighCost
        function onMaventReviewProductChecked() {
            var maventReviewSelected = $('#MaventReview').prop('checked');

            $('#HighCost')
                .prop('disabled', maventReviewSelected)
                .prop('checked', maventReviewSelected);
        }

        // FraudComplete includes both FraudBorrower and FraudCollateral
        function onFraudCompleteProductChecked() {
            var fraudCompleteSelected = $('#FraudComplete').prop('checked');

            $('#FraudBorrower')
                .prop('disabled', fraudCompleteSelected)
                .prop('checked', fraudCompleteSelected);

            $('#FraudCollateral')
                .prop('disabled', fraudCompleteSelected)
                .prop('checked', fraudCompleteSelected);
        }

        // Flood is mutually exclusive with FloodQuickCheck
        function onFloodProductChecked() {
            var floodSelected = $('#Flood').prop('checked');

            $('#FloodQuickCheck')
                .prop('disabled', floodSelected)
                .prop('checked', false);
        }

        // FloodQuickCheck is mutually exclusive with Flood
        function onFloodQuickCheckProductChecked() {
            var floodQuickCheckSelected = $('#FloodQuickCheck').prop('checked');

            $('#Flood')
                .prop('disabled', floodQuickCheckSelected)
                .prop('checked', false);
        }

        function onProductChecked() {
            var totalProducts = $('.ComplianceEagleProduct').length || 0;
            var selectedProducts = $('.ComplianceEagleProduct input[type=checkbox]:checked').length || 0;
            var disabledProducts = $('.ComplianceEagleProduct input[type=checkbox]:not(:checked):disabled').length || 0;

            var allProductsSelected = totalProducts === (selectedProducts + disabledProducts);
            $('#AllProducts').prop('checked', allProductsSelected);
                
            var noProductsSelected = selectedProducts === 0;
            $('#btnSubmit').prop('disabled', noProductsSelected);
        }

        function toggleLoadingSection(enable) {
            if (enable) {
                $('#LoadingSection').show();
                $('#btnSubmit').hide();
            }
            else {
                $('#LoadingSection').hide();
                $('#btnSubmit').show();
            }
        }

        function btnSubmit_Click() {
            toggleLoadingSection(true);
            $('#ErrorMessageDiv').empty();
            var args = getAllFormValues();

            // Products subsumed within another product are displayed as ticked in the UI,
            // but for accurate billing, we do not want to indicate that to the backend.
            // Ex: If MaventReview is selected, don't indicate that HighCost was also ordered.
            $('.ComplianceEagleProduct input[type=checkbox]:disabled').each(function () {
                var id = this.id;
                args[id] = "False";
            });

            gService.loanedit.callAsyncSimple("Export", args, submitCallback);
            return false;
        }

        function submitCallback(result) {
            if (!result.error) {
                toggleLoadingSection(false);

                if (result.value.Success === 'True') {
                    if (result.value.SuccessSummary !== '') {
                        alert(result.value.SuccessSummary);
                    }

                    window.location.reload(true);
                }
                else {
                    var errorMessage = result.value.TransmissionErrors;
                    $('#ErrorMessageDiv').text(errorMessage);
                }
            }
            else {
                var errMsg = result.UserMessage || 'Unable to export data. Please try again.';
                alert(errMsg);
            }
        }
    </script>
    <form id="form2" runat="server">
        <table cellpadding="0" cellspacing="0" width="100%" border="0">
            <tbody>
                <tr>
                    <td class="MainRightHeader">ComplianceEagle Integration</td>
                </tr>
            </tbody>
        </table>
        <table id="OrderTable">
            <tr>
                <td class="FieldLabel DelimitingRow">Mavent Services</td>
                <td class="DelimitingRow">
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="MaventReview" Text="Mavent Review" />
                </td>
                <td class="DelimitingRow">
                    <asp:CheckBox runat="server" class="ComplianceEagleProduct" ID="HighCost" Text="High Cost Review" />
                </td>
                <td class="DelimitingRow">
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="MaventNmls" Text="NMLS License Review" />
                </td>
                <td class="DelimitingRow"></td>
            </tr>
            <tr>
                <td class="FieldLabel DelimitingRow">Instant HMDA</td>
                <td class="DelimitingRow">
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="HmdaCheck" Text="HMDA Review" />
                </td>
                <td class="DelimitingRow">
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="Geocode" Text="Geocode / Demographics" />
                </td>
                <td class="DelimitingRow">
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="RateSpreadCheck" Text="Rate Spread Check" />
                </td>
                <td class="DelimitingRow">
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="CraReview" Text="CRA Eligibility Review" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel DelimitingRow">Fraud/Risk Services</td>
                <td class="DelimitingRow">
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="FraudComplete" Text="CoreLogic Risk Manager" />
                </td>
                <td class="DelimitingRow">
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="FraudBorrower" Text="CoreLogic Fraud Manager" />
                </td>
                <td class="DelimitingRow">
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="FraudCollateral" Text="CoreLogic Collateral Manager" />
                </td>
                <td class="DelimitingRow"></td>
            </tr>
            <tr>
                <td class="FieldLabel">Additional Services</td>
                <td>
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" id="ExclusionaryList" Text="Exclusionary List" />
                </td>
                <td>
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="ServiceProviderSearch" Text="RESPA Closing Fees" />
                </td>
                <td>
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="Flood" Text="Flood Certification" />
                </td>
                <td>
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="FloodQuickCheck" Text="Flood PreCert"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="RecordingFee" Text="Transfer Tax / Recording Fees" />
                </td>
                <td>
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="LefCheck" Text="LEF Check" />
                </td>
                <td>
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="IncomeReview" Text="Income Review" />
                </td>
                <td>
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="SsnReview" Text="SSN Review" />
                </td>
            </tr>
            <tr>
                <td class="DelimitingRow"></td>
                <td class="DelimitingRow">
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="QmReview" Text="LoanScoreCard QM Findings" />
                </td>
                <td class="DelimitingRow">
                    <asp:Checkbox runat="server" class="ComplianceEagleProduct" ID="AvmReview" Text="AVM Review" />
                </td>
                <td class="DelimitingRow">
                    <asp:CheckBox runat="server" class="ComplianceEagleProduct" ID="AusReview" Text="Portfolio Underwriter™" />
                </td>
                <td class="DelimitingRow">
                    <asp:CheckBox runat="server" class="ComplianceEagleProduct" ID="NmlsMcrReview" Text="NMLS Mortgage Call Report Review" />
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="4">
                    <asp:CheckBox runat="server" ID="AllProducts" Text="Select/Unselect All Products" />
                </td>
            </tr>
            <tr>
                <td class="SpacerCell"></td>
            </tr>
            <tr>
                <td colspan="5">
                    <asp:RadioButton ID="New" class="FieldLabel" runat="server" GroupName="OrderType" Text="New Report or Reorder Existing" Checked="True" />
                    <asp:RadioButton ID="Review" class="FieldLabel" runat="server" GroupName="OrderType" Text="Review Existing Report" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">Username</td>
                <td colspan="4">
                    <asp:TextBox ID="UserName" runat="server" NotForEdit="True" class="CredentialTextbox" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">Password</td>
                <td colspan="4">
                    <asp:TextBox ID="Password" runat="server" TextMode="Password" NotForEdit="True" class="CredentialTextbox" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel" colspan="5">
                    <asp:CheckBox ID="RememberMe" runat="server" Text="Remember logon credentials" NotForEdit="True" />
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <asp:Button runat="server" ID="btnSubmit" Text="Submit Order" CausesValidation="false" />
                    &nbsp;&nbsp;
                    <span id="LoadingSection">
                        <label class="FieldLabel">Uploading, please wait.</label>
                        <br />
                        <img id="LoadingImage" src="../../images/status.gif" alt="Loading" />
                    </span>
                </td>
            </tr>
        </table>
        <br />
        <div id="ErrorMessageDiv" class="BoldErrorMessage" runat="server"></div>
        <br />

        <table width="100%">
            <tr>
                <td colspan="3" id="LatestResultsHeader">Latest Results</td>
            </tr>
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td></td>
                <td id="responseTD">
                    <asp:PlaceHolder ID="noResponsePlaceholder" runat="server" Visible="false">
                        <span class="ErrorMessage">The ComplianceEagle report has not been pulled yet.</span>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="resultsPlaceholder" runat="server" Visible="false"></asp:PlaceHolder>
                </td>
                <td></td>
            </tr>
        </table>
    </form>
</body>
</html>
