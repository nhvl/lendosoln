﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FHAErrorResults.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.FHAErrorResults" %>
<%@ Register TagPrefix="ML" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

    <html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <title>FHA TOTAL Errors</title>
        <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name="vs_defaultClientScript" content="JavaScript">
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
        <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet"/>
    </head>
    <body MS_POSITIONING="FlowLayout" scroll="no" onload="onInit();">
        <script>
            function onInit()
            {
                resizeForIE6And7(600, 390);
            }
        </script>
    <form id="form1" runat="server">
        <table width="100%" border="0" cellspacing="2" cellpadding="3" style="border-color:Maroon">
            <tr>
               <td class="FormTableHeader" style="background-color:maroon">
                    FHA TOTAL Errors
                </td>
            </tr>
        </table>
        <table width="100%" border="0" cellspacing="2" cellpadding="3">
            <tr>
                <td class="FieldLabel" style="padding-top:10px;padding-bottom:10px;padding-left:20px">
                    The following errors were returned by the FHA TOTAL Scorecard system.  Please correct them
                    and try submitting to FHA TOTAL Scorecard again.
                </td>
            </tr>
            <tr>
                <td style="padding-left:20px;padding-right:20px;">
                    <div style="HEIGHT: 200px;overflow:auto">
                        <ml:PassthroughLiteral id="htmlList" runat="server" EnableViewState="False"></ml:PassthroughLiteral>
                    </div>
                </td>
            </tr>
            <tr >
                <td colspan="3" align="center" style="padding-top:50px">
                    <asp:Button ID="m_okButton" Width="70px" Text="OK" runat="server" />
                </td>
            </tr>
        </table>
        <ML:CModalDlg id="m_ModalDlg" runat="server"></ML:CModalDlg>
    </form>
</body>
</html>
