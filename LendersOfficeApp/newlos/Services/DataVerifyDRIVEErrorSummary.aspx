﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataVerifyDRIVEErrorSummary.aspx.cs"
    Inherits="LendersOfficeApp.newlos.Services.DataVerifyDRIVEErrorSummary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="LendersOffice.AntiXss"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript">

        jQuery(function($) {
            $('#Close').click(function() { onClosePopup() });
        });
        $(function() {
            $('#ErrorsDiv').html($('#errorDiv', window.opener.document).html());
        });
        
    
    </script>
    <style type="text/css">
    #Close
    {
        margin-left: 45%;
    }
    body
    {
        background-color: gainsboro;
    }
    #ErrorsDiv
    {
        height:300px;
        width:94%;
        margin-left: 2%;
        margin-top: 5px;
        margin-bottom: 5px;
        overflow-y: scroll;
        border: solid 1px black;
    }
    #ErrorList
    {
        list-style-type: none;
        padding: 2px;
    }
    #ErrorList li
    {
        margin:0px;
        margin-bottom: 5px;
    }
    tr.errorRow
    {
        background-color:#FFFFFF; 
    }
    tr.altErrorRow
    {
        background-color:#E0E0E0;
    }
    td.secondCell
    {
        border-left-style:solid;
        border-left-color:Black; 
        border-left-width:medium;
    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="FormTableHeader" style="background-color:Red">
            DataVerify DRIVE Errors
        </div>
        <div id="ErrorsDiv">
           
        </div>
        <input type="button" id="Close" value="Close" />
    </div>
    </form>
</body>
</html>
