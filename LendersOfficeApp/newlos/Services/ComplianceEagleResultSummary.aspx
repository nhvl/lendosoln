﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComplianceEagleResultSummary.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.ComplianceEagleResultSummary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="LendersOffice.AntiXss"%>
<%@ Import Namespace="LendersOffice.Conversions.ComplianceEagleIntegration" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>ComplianceEagle Audit</title>
    <script type="text/javascript">
        jQuery(function($) {
            $('#Close').click(function() { onClosePopup() });
        });
    </script>
    <style type="text/css">
    .ErrorMessage
    {
        color:red;
    }
    #Controls
    {
        margin-left: 45%;
    }
    body
    {
        background-color: gainsboro;
    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="FormTableHeader">
            ComplianceEagle
        </div>
        <table width="100%">
            <tr><td colspan=3 style="background-color:#5778ab; color:white; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Latest Results</td></tr>
            <tr><td colspan=3></td></tr>
            <tr runat="server" id="warningRow"><td colspan="3" style="color:red;" runat="server" id="warningCell"></td></tr>
            <tr><td></td><td id="responseTD">
            <asp:PlaceHolder ID="noResponsePlaceholder" runat="server" Visible="false">
                <span style="color:Red">The ComplianceEagle report has not been pulled yet.</span>
            </asp:PlaceHolder>
            <asp:Placeholder ID="resultsPlaceholder" runat="server" Visible="false">
            </asp:Placeholder>
            </td><td></td>
            </tr>
        </table>
        <div id="Controls">
            <input type="button" id="Close" value="Close" />
        </div>
    </div>
    </form>
</body>
</html>
