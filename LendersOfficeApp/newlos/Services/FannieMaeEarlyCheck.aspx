﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FannieMaeEarlyCheck.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.FannieMaeEarlyCheck" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>Fannie Mae EarlyCheck</title>
<style>
.blockMask { Z-INDEX: 200; FILTER: alpha(opacity=30); BACKGROUND-COLOR: #333333; opacity: .3 }
.ErrorWindow { BORDER-RIGHT: #335ea8 5px inset; PADDING-RIGHT: 15px; BORDER-TOP: #335ea8 5px inset; PADDING-LEFT: 15px; Z-INDEX: 205; PADDING-BOTTOM: 15px; BORDER-LEFT: #335ea8 5px inset; PADDING-TOP: 15px; BORDER-BOTTOM: #335ea8 5px inset; BACKGROUND-COLOR: white; TEXT-ALIGN: center }
</style>
    
</head>
<body bgcolor="gainsboro">
    <script type="text/javascript">
    var g_globallyUniqueIdentifier = "";
    var g_pollingInterval = 10000; // 10 seconds

    function f_submit() {
        f_displayWait(true);
        g_globallyUniqueIdentifier = "";
        f_submitImpl();
    }

    function f_displayWait(bDisplay) {
        var maskDiv = document.getElementById('MaskDiv');
        maskDiv.style.display = bDisplay ? '' : 'none';
        maskDiv.style.width = '100%';
        maskDiv.style.height = '100%';
        maskDiv.style.position = 'absolute';
        maskDiv.style.left = '0px';
        maskDiv.style.top = '0px';

        var waitMessage = document.getElementById('WaitMessage');
        waitMessage.style.display = bDisplay ? '' : 'none';
        waitMessage.style.width = '100%';
        waitMessage.style.height = '100%';
        waitMessage.style.position = 'absolute';
        waitMessage.style.left = '0px';
        waitMessage.style.top = '0px';
    }
    function f_submitImpl() {
      var args = {};
      args["LoanID"] = ML.sLId;
      if (<%= AspxTools.JsGetElementById(FannieMaeMORNETUserID) %> != null)
      {
          args["FannieMaeMORNETUserID"] = <%= AspxTools.JsGetElementById(FannieMaeMORNETUserID) %>.value;
          args["FannieMaeMORNETPassword"] = <%= AspxTools.JsGetElementById(FannieMaeMORNETPassword) %>.value;
          args["FannieMaeMORNETInstitutionId"] = <%= AspxTools.JsGetElementById(FannieMaeMORNETInstitutionId) %>.value;
      }
      args["GloballyUniqueIdentifier"] = g_globallyUniqueIdentifier;
      
      var result = gService.loanedit.call("SubmitEarlyCheck", args);
      if (!result.error) {
        switch (result.value["Status"]) {
          case "ERROR":
            f_displayErrorMessage(result.value["ErrorMessage"]);
            break;
          case "PROCESSING":
            g_globallyUniqueIdentifier = result.value["GloballyUniqueIdentifier"];
            setTimeout(f_submitImpl, g_pollingInterval);
            break;
          case "DONE":
          
            f_displayWait(false);
            f_refreshEarlyResult();
            break;
        }
      } else {
        var errMsg = 'Error.';
        if (null != result.UserMessage)
          errMsg = result.UserMessage;
          
          f_displayErrorMessage(errMsg);
          return false;
      }
      
    }   
    function f_refreshEarlyResult()
    {
        f_displayEarlyResult(true);
        window.frames["frmView"].location.href = window.frames["frmView"].location.href;
    } 
    function f_displayEarlyResult(bVisible)
    {
        document.getElementById('frmView').style.display = bVisible ? '' : 'none';
        
    }
    function f_closeErrorMsg() 
    {
	    document.getElementById('DUErrorPanel').style.display = 'none';
	    f_displayEarlyResult(true);
    }
    function f_print()
    {
        lqbPrintByFrame(window.frames["frmView"]);
    }
function f_displayErrorMessage(errMsg) {
  f_displayWait(false);
f_displayEarlyResult(false);
		var panel = document.getElementById('DUErrorPanel');
		panel.style.position = "absolute";
		panel.style.left = "100px";
		panel.style.top = "100px";
		panel.style.width = "400px";
		panel.style.height = "150px";
		panel.style.display = "";
	      
		document.getElementById('ErrorMsg').innerHTML = errMsg;


}    
    </script>
    <form id="form1" runat="server">
    <div>
        <div class="MainRightHeader">Fannie Mae EarlyCheck</div>
        <table cellpadding="2" cellspacing="0" border="0" style="margin-left:10px">
            <tbody id="LoginPanel" runat="server">
            <tr><td class="FieldLabel">User Id</td><td><asp:TextBox id=FannieMaeMORNETUserID runat="server" /></td></tr>
            <tr><td class="FieldLabel">Password</td><td><asp:TextBox id=FannieMaeMORNETPassword runat="server" TextMode="Password" /></td></tr>
            <tr><td class="FieldLabel">Institution ID</td><td><asp:TextBox ID="FannieMaeMORNETInstitutionId" runat="server" /></td></tr>
            </tbody>
            <tr><td colspan="2"><input type="button" value="Submit loan to EarlyCheck" onclick="f_submit();" /></td></tr>
        </table>
        <hr />
        <div>
        <table width="100%" border="0">
            <tr><td align=right><input type="button" value="Print" onclick="f_print();" /></td></tr>
        </table>
<iframe id="frmView" name="frmView" style="width:99.5%;height:500px" src=<%=AspxTools.SafeUrl("ViewEarlyCheckResult.aspx?loanid=" + LoanID) %>></iframe>
</div>

    </div>
    <div class="ErrorWindow" id="DUErrorPanel" style="display: none">
        <div id="ErrorMsg" style="font-weight: bold; color: red"></div>
        <br>[<a onclick="f_closeErrorMsg();" href="#">Close</a>] </div>
    <div class="blockMask" id="MaskDiv" style="display: none"></div>
    <div id="WaitMessage" style="display: none; z-index: 205">
        <table height="100%" width="100%">
            <tr>
                <td valign="middle" align="center">
                    <div style="padding-right: 30px; padding-left: 30px; font-weight: bold; z-index: 205; padding-bottom: 30px; width: 250px; color: black; padding-top: 30px; height: 60px; background-color: white">Submit to EarlyCheck. Please wait ... <br><img height="10" src="~/images/status.gif" runat="server"></div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
