<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import namespace="LendersOffice.CreditReport" %>
<%@ Page language="c#" Codebehind="OrderCredit.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Services.OrderCredit" %>
<%@ Register TagPrefix="uc1" TagName="CreditReportAuthorization" Src="~/newlos/Status/CreditReportAuthorization.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
		<title>Order Credit Report</title>
        <link href="../../css/stylesheetnew.css" type="text/css" rel="Stylesheet"/>
        <style type="text/css">
            body { font-weight: bold; }
            .blockMask { Z-INDEX: 200; FILTER: alpha(opacity=30); BACKGROUND-COLOR: #333333; opacity: .3 }
	        .ErrorWindow { BORDER-RIGHT: #335ea8 5px inset; PADDING-RIGHT: 15px; BORDER-TOP:  groove 2px #F7F7F8; PADDING-LEFT: 15px; Z-INDEX: 205; PADDING-BOTTOM: 15px; BORDER-LEFT: #335ea8 5px inset; PADDING-TOP: 15px; BORDER-BOTTOM: #335ea8 5px inset; BACKGROUND-COLOR: white; TEXT-ALIGN: center }
            .ErrorMessage { font-weight: bold; color: red; }
            ul {  list-style: none; margin: 0; padding-top: 5px; }
            .BureauCheckboxList { padding-left: 0px; }
            .OptionsMenu {margin-left: 10px; }
            #CreditRequestOptions, #CreditLoginInformation,#BorrowerInformation, #CreditReportAuthorization { padding:0; width: 375px; margin: 10px 0 0 10px} 
            #BorrowerInformation { width: auto; }
            .col1, .col2 { float: left; }
           #pagefooter { width: 100%; text-align: center; clear:both; padding-top: 10px;}
           .FormTableSubheader { padding: 5px; margin-bottom: 5px; }
           .Hidden { display: none; }
           .MarginLeft { margin-left : 20px; }
           Label { margin-right: 2px; }
           .OptionsMenu li table { margin: 5px 0 0 25px; font-weight: bold; }
           .WarningIcon { height: 25px; vertical-align: bottom; }
           .ReportIdInput { width: 110px; }
           #PageContent { display: inline-block; }
           #CreditReportAuthorization { width: calc(100% - 10px); box-sizing: border-box; }
       </style>
    </head>
    <body  class="RightBackground" style="MARGIN-LEFT: 0px" >
        <h4 class="page-header">Credit Report Request</h4>
        <form id="OrderCredit" method="post" runat="server">
            <div id="Page" >
                <div id="PageContent">
                    <div id="CreditReportAuthorization" class="InsetBorder">
                        <div class="FormTableSubheader">
                            Credit Report Authorization
                        </div>
                        <uc1:CreditReportAuthorization runat="server" ID="CreditReportAuthorizationCtrl"></uc1:CreditReportAuthorization>
                    </div>
                    <div class="col1">
                    <div id="CreditRequestOptions" class="InsetBorder">
                        <div class="FormTableSubheader" >
                            Credit Request Options
                        </div>
                        <div style="margin-left: 5px">
                        <ml:EncodedLabel ID="Label1" runat="server" AssociatedControlID="CreditAgencyList" >Credit Agency </ml:EncodedLabel>
                        <img  alt="Required" id="m_CreditAgencyReq" src="../../images/require_icon.gif" />
                        <asp:customvalidator id="CRADropDownValidator" runat="server" ControlToValidate="CreditAgencyList" ClientValidationFunction="validateCRADropDown" Display="Dynamic">
                            <img runat="server" src="../../images/error_icon.gif" alt="Please select a credit agency.">
                        </asp:customvalidator>

                        <asp:dropdownlist id="CreditAgencyList"  runat="server" enableviewstate="False" onchange="f_onCraChange();" NotForEdit />
                        </div>
                        <ul class="OptionsMenu">
                            <li>
                                <asp:radiobutton id="m_OrderNewReport" onclick="f_refreshUI();" runat="server" NotForEdit GroupName="ReportType" Text="Order new report" />
                                <table id="m_OrderNewReportOptions" cellpadding="0" cellspacing="0">
                                    <tr id="orderNew_generalReportTypeOption">
                                        <td>
                                            <div>
                                                <label for="orderNew_generalReportTypeDropdown">Credit Report Type</label>
                                            </div>
                                            <div>
                                                <select id="orderNew_generalReportTypeDropdown" NotForEdit></select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ul class="BureauCheckboxList">
                                                <li><label><input type="checkbox" id="orderNew_Equifax" NotForEdit />Equifax</label></li>
                                                <li><label><input type="checkbox" id="orderNew_Experian" NotForEdit />Experian</label></li>
                                                <li><label><input type="checkbox" id="orderNew_TransUnion" NotForEdit />TransUnion</label></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                             </li>
                             <li>
                                <asp:radiobutton id="m_ReorderReport" onclick="f_refreshUI();" runat="server" GroupName="ReportType" Text="Re-issue existing report" />

                            <table id="m_ReorderReportOptions" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align:left">
                                        <ml:EncodedLabel  id="ReportIdLabel"  runat="server" AssociatedControlID="m_reportIDTF">Report ID</ml:EncodedLabel>
                                        <img id="m_ReportIdReq" src="../../images/require_icon.gif" alt="Required" />
                                    </td>
                                    <td>
                                        <ml:EncodedLabel ID="InstantViewLabel" CssClass="Hidden MarginLeft" runat="server" AssociatedControlID="m_iviewIDTF"  > Instant View Password </ml:EncodedLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:textbox id="m_reportIDTF"  onblur="f_ToUpper(this); return false;" runat="server" NotForEdit class="ReportIdInput"></asp:textbox>
                                    </td>
                                    <td>
                                        <asp:textbox id="m_iviewIDTF" class="Hidden MarginLeft" onkeyup="f_oniViewIDkeyUp();" onchange="f_oniViewIDkeyUp();" runat="server" NotForEdit Width="130px"></asp:textbox>
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li id="m_upgradeReportPanel">
                            <asp:radiobutton id="m_UpgradeReport" onclick="f_refreshUI();" runat="server" GroupName="ReportType" Text="Upgrade existing report to tri-merge report" />
                            <table id="m_UpgradeReportOptions" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <span id="upgradeReportIdLabel" >Report ID</span>
                                        <img id="m_upgradeReportIdReq" src="../../images/require_icon.gif" alt="Required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:textbox id="m_upgradeReportIDTF" onblur="f_ToUpper(this); return false;" runat="server" NotForEdit class="ReportIdInput"/>
                                    </td>
                                </tr>
                                <tr id="upgradeReportBureaus">
                                  <td>
                                    <ul class="BureauCheckboxList">
                                      <li><label><input type="checkbox" id="upgradeBureau_Equifax" NotForEdit />Equifax</label></li>
                                      <li><label><input type="checkbox" id="upgradeBureau_Experian" NotForEdit />Experian</label></li>
                                      <li><label><input type="checkbox" id="upgradeBureau_TransUnion" NotForEdit />TransUnion</label></li>
                                    </ul>
                                  </td>
                                </tr>
                            </table>
                        </li>
                        <li id="m_retrieveReportPanel" style="display:none">
                          <asp:RadioButton ID="m_RetrieveReport" onclick="f_refreshUI();" runat="server" GroupName="ReportType" Text="Retrieve report" />
                            <table id="m_RetrieveReportOptions" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <span>Report ID</span>
                                        <img id="m_retrieveReportIdReq" src="../../images/require_icon.gif" alt="Required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:textbox id="m_retrieveReportIDTF" onblur="f_ToUpper(this); return false;" runat="server" NotForEdit class="ReportIdInput" />
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li id="orderLqiPanel" style="display:none">
                          <asp:RadioButton ID="orderLqi" onclick="f_refreshUI();" runat="server" GroupName="ReportType" Text="Order new LQI credit report" />
                            <table id="orderLqiOptions" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <span id="orderLqiReportIdLabel">Report ID</span>
                                        <img id="orderLqiReportIdReq" src="../../images/require_icon.gif" alt="Required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:textbox id="orderLqiReportId" onblur="f_ToUpper(this); return false;" runat="server" NotForEdit class="ReportIdInput" />
                                    </td>
                                </tr>
                                <tr id="orderLqi_generalReportTypeOption">
                                    <td>
                                        <div>
                                            <label for="orderLqi_generalReportTypeDropdown">Credit Report Type</label>
                                        </div>
                                        <div>
                                            <select id="orderLqi_generalReportTypeDropdown" NotForEdit></select>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="orderLqiBureaus">
                                    <td>
                                        <ul class="BureauCheckboxList">
                                            <li><label><input type="checkbox" id="orderLqiBureau_Equifax" NotForEdit />Equifax</label></li>
                                            <li><label><input type="checkbox" id="orderLqiBureau_Experian" NotForEdit />Experian</label></li>
                                            <li><label><input type="checkbox" id="orderLqiBureau_TransUnion" NotForEdit />TransUnion</label></li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li id="reissueLqiPanel" style="display:none;">
                          <asp:RadioButton ID="reissueLqi" onclick="f_refreshUI();" runat="server" GroupName="ReportType" Text="Re-issue LQI report" />
                          <table id="reissueLqiOptions" cellpadding="0" cellspacing="0">
                            <tr>
                              <td>
                                <span id="reissueLqiReportIdLabel">LQI Report ID</span>
                                <img id="reissueLqiReportIdReq" src="../../images/require_icon.gif" alt="Required" />
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <asp:TextBox ID="reissueLqiReportId" onblur="f_ToUpper(this); return false;" runat="server" NotForEdit class="ReportIdInput" />
                              </td>
                            </tr>
                          </table>
                        </li>
                        <li id="removeApplicantPanel" style="display:none;">
                          <input type="radio" id="removeApplicant" name="ReportType" onclick="f_refreshUI();" NotForEdit />
                          <label for="removeApplicant">Remove Applicant</label>
                          <table id="removeApplicantOptions">
                            <tr>
                              <td>
                                <input type="radio" id="removeApplicantRemoveBorrower" name="removeApplicantType" NotForEdit/>
                                <label for="removeApplicantRemoveBorrower">Remove Borrower</label>
                                <div id="RemoveBorrowerWarningMessage" class="ErrorMessage"></div>
                                <%-- Borrower/Co should be separate rows, but we want them to look closer than the other rows, and adjusting the padding doesn't seem to cut it.  The div will suffice as a hack, since we sometimes want a new block anyway. --%>
                                <input type="radio" id="removeApplicantRemoveCoBorrower" name="removeApplicantType" NotForEdit/>
                                <label for="removeApplicantRemoveCoBorrower">Remove Co-Borrower</label>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <span id="removeApplicantReportIdLabel">Report ID</span>
                                <img id="removeApplicantReportIdReq" src="../../images/require_icon.gif" alt="Required"/>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <input name="removeApplicantReportId" type="text" id="removeApplicantReportId" onblur="f_ToUpper(this); return false;" NotForEdit class="ReportIdInput"/>
                              </td>
                            </tr>
                          </table>
                        </li>
                        <li id="orderSupplementPanel" style="display:none;">
                          <input type="radio" id="orderSupplement" name="ReportType" onclick="f_refreshUI();" NotForEdit />
                          <label for="orderSupplement">Order Supplement</label>
                          <table id="orderSupplementOptions">
                            <tr>
                              <td>
                                <span id="orderSupplementReportIdLabel">Report ID</span>
                                <img id="orderSupplementReportIdReq" src="../../images/require_icon.gif" alt="Required"/>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <input name="orderSupplementReportId" type="text" id="orderSupplementReportId" onblur="f_ToUpper(this); return false;" NotForEdit class="ReportIdInput"/>
                              </td>
                            </tr>
                          </table>
                        </li>
                        <li id="retrieveSupplementPanel" style="display:none;">
                          <input type="radio" id="retrieveSupplement" name="ReportType" onclick="f_refreshUI();" NotForEdit />
                          <label for="retrieveSupplement">Retrieve Supplement</label>
                          <table id="retrieveSupplementOptions">
                            <tr>
                              <td>
                                <span id="retrieveSupplementReportIdLabel">Report ID</span>
                                <img id="retrieveSupplementReportIdReq" src="../../images/require_icon.gif" alt="Required"/>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <input name="retrieveSupplementReportId" type="text" id="retrieveSupplementReportId" onblur="f_ToUpper(this); return false;" NotForEdit class="ReportIdInput"/>
                              </td>
                            </tr>
                          </table>
                        </li>
                        <li id="retrieveBillingReportPanel" style="display:none;">
                          <input type="radio" id="retrieveBillingReport" name="ReportType" onclick="f_refreshUI();" NotForEdit />
                          <label for="retrieveBillingReport">Retrieve Billing Report</label>
                          <table id="retrieveBillingReportOptions">
                            <tr>
                              <td>
                                <span id="retrieveBillingReportReportIdLabel">Report ID</span>
                                <img id="retrieveBillingReportReportIdReq" src="../../images/require_icon.gif" alt="Required"/>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <input name="retrieveBillingReportReportId" type="text" id="retrieveBillingReportReportId" onblur="f_ToUpper(this); return false;" NotForEdit class="ReportIdInput"/>
                              </td>
                            </tr>
                          </table>
                        </li>
                     </ul>
                     <ul class="OptionsMenu" style="margin: 0; padding: 5px 0 0 0">
                        <li id="mclReportTypeOption" class="MarginLeft">
                          <ml:EncodedLabel runat="server" AssociatedControlID="mclReportTypeDropdown">Credit Report Type</ml:EncodedLabel>
                          <br />
                          <asp:DropDownList id="mclReportTypeDropdown" runat="server" />
                        </li>
                        <li>
                            <span id="m_showPayWithCreditCardOption" >
                                <asp:checkbox id="UseCreditCardPayment" runat="server" Text="Pay with credit card" />
					        </span>
                        </li>
                        <li id="m_mortgageOnlyOption">
                          <asp:CheckBox ID="m_isMortgageOnlyCB" runat="server" Text="Mortgage Only Report" />
                        </li>
                        <li>
                            <asp:checkbox id="m_isImport0BalanceCB" runat="server" Text="Import tradelines with $0 balance" CssClass="FieldLabel" Checked="False"></asp:checkbox>
                        </li>
                     </ul>


                    </div>
                    <div id="CreditLoginInformation" class="InsetBorder">
                        <div class="FormTableSubheader" >
                            Credit Login Information
                        </div>
                        <ul class="OptionsMenu" style="margin-bottom: 10px; margin-left: 5px" >
                            <li id="m_AccountIdentifierPanel" style="margin-top: 0; padding-top:0">
                                <ul style="margin: 0px; padding:0px;">
                                    <li>
                                       <ml:EncodedLabel id="AccountIDLabel" runat="server" AssociatedControlID="m_accountIdentifier">Account ID</ml:EncodedLabel><img alt="Required" style="DISPLAY:none" id="m_AccountIdReq" src="../../images/require_icon.gif" />
                                    </li>
                                    <li>
                                      <asp:textbox id="m_accountIdentifier" runat="server" Width="137px" NotForEdit></asp:textbox>
                                   <asp:requiredfieldvalidator id="AccountIdRequiredValidator" runat="server" ControlToValidate="m_accountIdentifier" Display="Dynamic">
                                       <img runat="server" src="../../images/error_icon.gif">
                                   </asp:requiredfieldvalidator>
                                </li>
                            </ul>
                        </li>
                        <li id="LoginPasswordPanel">
                            <ul style="margin: 0px; padding:0px;" >
                                <li>
                                    <ml:EncodedLabel runat="server" ID="LoginLabel" AssociatedControlID="m_loginName">
                                        Login
                                    </ml:EncodedLabel><img id="m_loginReq" src="../../images/require_icon.gif" alt="Required" />
                                </li>
                                <li>
                                    <asp:textbox id="m_loginName" runat="server" NotForEdit onblur="f_ToUpper(this); return false;" Width="137px"></asp:textbox>
                                    <asp:requiredfieldvalidator id="LoginNameValidator" runat="server" ControlToValidate="m_loginName" Display="Dynamic">
                                        <img runat="server" src="../../images/error_icon.gif">
                                    </asp:requiredfieldvalidator>
                                    <asp:checkbox id="m_rememberLoginCB" runat="server" ></asp:checkbox> <ml:EncodedLabel runat="server" ID="RememberLabel" AssociatedControlID="m_rememberLoginCB">Remember My Login</ml:EncodedLabel>
                                </li>
                                <li>
                                    <ml:EncodedLabel ID="Label4" runat="server" AssociatedControlID="m_password">Password</ml:EncodedLabel>
                                    <img id="m_passwordReq" src="../../images/require_icon.gif"  alt="Required" />
                                </li>
                                <li>
                                    <asp:textbox id="m_password" runat="server" NotForEdit Width="137px" textmode="Password"></asp:textbox>
                                    <asp:requiredfieldvalidator id="PasswordValidator" runat="server" ControlToValidate="m_password" Display="Dynamic">
                                        <img runat="server" src="../../images/error_icon.gif">
                                    </asp:requiredfieldvalidator>
                                </li>
                            </ul>
                        </li>
                        <li id="FannieMaePanel">
                            <ul style="margin: 0px; padding:0px;" >
                                <li>
                                    <ml:EncodedLabel ID="Label2" runat="server" AssociatedControlID="DUUserID">
                                    DU UserID
                                    </ml:EncodedLabel>
                                    <img src="../../images/require_icon.gif"/ alt="Required" />
                                </li>
                                <li>
                                    <asp:textbox id="DUUserID" runat="server" Width="137px"></asp:textbox>
                                    <asp:requiredfieldvalidator id="DUUserIDValidator" runat="server" ControlToValidate="DUUserID" Display="Dynamic">
                                        <img runat="server" src="../../images/error_icon.gif">
                                    </asp:requiredfieldvalidator>
                               </li>
                                <li>
                                    <ml:EncodedLabel runat="server" AssociatedControlID="DUPassword">
                                        DU Password
                                    </ml:EncodedLabel> <img src="../../images/require_icon.gif" alt="Required"/>
                                </li>
                                <li>
                                    <asp:textbox id=DUPassword runat="server" Width="137px" TextMode="Password"></asp:textbox>
                                    <asp:requiredfieldvalidator id="DUPasswordValidator" runat="server" ControlToValidate="DUPassword" Display="Dynamic">
                                        <img runat="server" src="../../images/error_icon.gif">
                                    </asp:requiredfieldvalidator>
                                </li>
                                </ul>
                            </li>

                        </ul>


                    </div>
                    </div>
                    <div class="col2">
                    <div id="BorrowerInformation" class="InsetBorder">
                        <table cellSpacing="0" cellPadding="0" border="0" width="100%" style="border-collapse:collapse">
                        <tr><td class="FormTableSubheader">Borrower Information</td></tr>
                        <tr><td>
                                <table id=Table10 cellSpacing=0 cellPadding=0 border=0 style="padding: 5px;">
                                    <tr>
                                        <td class=FieldLabel> <ml:EncodedLabel runat="server" AssociatedControlID="aBFirstNm" >First Name</ml:EncodedLabel><IMG id="m_FirstNameReqIcon" src="../../images/require_icon.gif" ></td>
                                        <td class=FieldLabel noWrap><ml:EncodedLabel ID="Label3" runat="server" AssociatedControlID="aBMidNm" >Middle</ml:EncodedLabel></td>
                                        <td class=FieldLabel><ml:EncodedLabel ID="Label5" runat="server" AssociatedControlID="aBLastNm" >Last Name</ml:EncodedLabel><IMG id="m_LastNameReqIcon" src="../../images/require_icon.gif" ></td>
                                        <td class=FieldLabel><ml:EncodedLabel ID="Label6" runat="server" AssociatedControlID="aBSuffix" >Suffix</ml:EncodedLabel></td>

                                    <tr>
                                        <td noWrap><asp:textbox id=aBFirstNm runat="server" enableviewstate="False" Width="97px" maxlength="21"></asp:textbox>
                                            <asp:requiredfieldvalidator id=aBFirstNmValidator runat="server" ControlToValidate="aBFirstNm" Display="Dynamic">
                                                <img runat="server" src="../../images/error_icon.gif">
                                            </asp:requiredfieldvalidator>&nbsp; </td>
                                        <td><asp:textbox id=aBMidNm runat="server" enableviewstate="False" Width="81px"></asp:textbox>&nbsp;</td>
                                        <td noWrap><asp:textbox id=aBLastNm runat="server" enableviewstate="False" Width="88px" maxlength="21"></asp:textbox>
                                            <asp:requiredfieldvalidator id=aBLastNmValidator runat="server" ControlToValidate="aBLastNm" Display="Dynamic">
                                                <img runat="server" src="../../images/error_icon.gif">
                                            </asp:requiredfieldvalidator>&nbsp;</td>
                                        <td><ml:combobox id=aBSuffix runat="server" enableviewstate="False" Width="30px"></ml:combobox>&nbsp;</td>
                                    </tr>
                              <tr>
                                    <td class=FieldLabel><ml:EncodedLabel ID="Label7" runat="server" AssociatedControlID="aBSsn" >SSN</ml:EncodedLabel><IMG id="m_SsnReqIcon" src="../../images/require_icon.gif" ></td>
                                    <td class=FieldLabel><ml:EncodedLabel ID="Label8" runat="server" AssociatedControlID="aBDob" >DOB</ml:EncodedLabel></td>
                                    <td class="FieldLabel" colspan="6"><ml:EncodedLabel ID="Label9" runat="server" AssociatedControlID="aBMaritalStatT" > Marital Status</ml:EncodedLabel></td>
                                  </tr>

                                  <tr><td noWrap><ml:ssntextbox id=aBSsn runat="server" enableviewstate="False" CssClass="mask" onblur="ValidateSsnTB();" preset="ssn" width="82px"></ml:ssntextbox>
                                      <asp:requiredfieldvalidator id=aBSsnValidator runat="server" ControlToValidate="aBSsn" Display="Dynamic">
                                          <img runat="server" src="../../images/error_icon.gif">
                                      </asp:requiredfieldvalidator>&nbsp;</td>
                                        <td><asp:textbox id=aBDob runat="server" enableviewstate="False" Width="81px" MaxLength="10"></asp:textbox></td>
                                         <td colspan="6"><asp:dropdownlist id=aBMaritalStatT runat="server" enableviewstate="False"></asp:dropdownlist></td>

                                  </tr>
                                  <tr style="padding-top:10px">
                                    <td class=FieldLabel><ml:EncodedLabel ID="Label10" runat="server" AssociatedControlID="aCFirstNm" >Spouse Name</ml:EncodedLabel></td>
                                    <td class=FieldLabel><ml:EncodedLabel ID="Label11" runat="server" AssociatedControlID="aCMidNm" >Middle</ml:EncodedLabel></td>
                                    <td class=FieldLabel><ml:EncodedLabel ID="Label12" runat="server" AssociatedControlID="aCLastNm" >Last Name</ml:EncodedLabel></td>
                                    <td class=FieldLabel><ml:EncodedLabel ID="Label13" runat="server" AssociatedControlID="aCSuffix" >Suffix</ml:EncodedLabel></td>
                                  </tr>
                                  <tr>
                                    <td><asp:textbox id=aCFirstNm runat="server" enableviewstate="False" Width="97px" maxlength="21"></asp:textbox></td>
                                    <td><asp:textbox id=aCMidNm runat="server" enableviewstate="False" Width="81px"></asp:textbox></td>
                                    <td><asp:textbox id=aCLastNm runat="server" enableviewstate="False" Width="88px" maxlength="21"></asp:textbox></td>
                                    <td><ml:combobox id=aCSuffix runat="server" enableviewstate="False" Width="30px"></ml:combobox></td>
                                  </tr>
                                    <tr>
                                        <td class=FieldLabel><ml:EncodedLabel ID="Label14" runat="server" AssociatedControlID="aCSsn" >SSN</ml:EncodedLabel></td>
                                        <td colspan="2" class=FieldLabel><ml:EncodedLabel ID="Label15" runat="server" AssociatedControlID="aCDob" >DOB</ml:EncodedLabel></td>
                                    </tr>
                                    <tr>
                                        <td><ml:ssntextbox id=aCSsn runat="server" enableviewstate="False" CssClass="mask" preset="ssn" width="82px"></ml:ssntextbox></td>
                                        <td colspan="2" ><asp:textbox id=aCDob runat="server" enableviewstate="False" Width="81px" MaxLength="10"></asp:textbox></td></tr>
                                </table>
                            </td>
                        </tr>

                            <tr>
                              <td class="FormTableSubheader">Present Address</td></tr>
                            <tr>
                              <td>
                                <TABLE id=Table3 cellSpacing=0 cellPadding=0 width=300 border=0 style="padding: 5px;">
                                  <TR>
                                    <TD class=FieldLabel noWrap><ml:EncodedLabel ID="Label16" runat="server" AssociatedControlID="aBAddr" >Street Address</ml:EncodedLabel> <IMG id="m_StreetAddReqIcon" src="../../images/require_icon.gif" ></TD></TR>
                                  <TR>
                                    <TD noWrap><asp:textbox id=aBAddr runat="server" Width="265px"></asp:textbox>
                                        <asp:requiredfieldvalidator id=aBAddrRequiredValidator runat="server" ControlToValidate="aBAddr">
                                            <img runat="server" src="../../images/error_icon.gif">
                                        </asp:requiredfieldvalidator></TD></TR></TABLE>
                                    <table id=Table9 cellSpacing=0 cellPadding=0 border=0 style="padding: 5px;">
                                  <tr>
                                    <td class=FieldLabel><ml:EncodedLabel ID="Label17" runat="server" AssociatedControlID="aBCity" >City </ml:EncodedLabel><IMG id="m_CityReqIcon" src="../../images/require_icon.gif" ></td>
                                    <td class=FieldLabel><ml:EncodedLabel ID="Label18" runat="server" AssociatedControlID="aBState" >State</ml:EncodedLabel> <IMG id="m_StateReqIcon" src="../../images/require_icon.gif" ></td>
                                    <td class=FieldLabel><ml:EncodedLabel ID="Label19" runat="server" AssociatedControlID="aBZip" >Zipcode</ml:EncodedLabel><IMG id="m_ZipcodeReqIcon" src="../../images/require_icon.gif" ></td>
                                    <td class=FieldLabel><ml:EncodedLabel ID="Label20" runat="server" AssociatedControlID="aBAddrYrs" >Length (Yrs)</ml:EncodedLabel></td></tr>
                                  <tr>
                                    <td noWrap><asp:textbox id=aBCity runat="server" enableviewstate="False" Width="180px"></asp:textbox>
                                        <asp:requiredfieldvalidator id=aBCityValidator runat="server" Display="Dynamic" ControlToValidate="aBCity">
                                            <img runat="server" src="../../images/error_icon.gif">
                                        </asp:requiredfieldvalidator>&nbsp;&nbsp;&nbsp;</td>
                                    <td noWrap><ml:statedropdownlist id=aBState runat="server" onblur="ValidateStateDD();" enableviewstate="False"></ml:statedropdownlist><asp:customvalidator id=aBStateValidator runat="server" Display="Dynamic" ClientValidationFunction="validateStateDropDown">
                                        <img runat="server" src="../../images/error_icon.gif"></asp:customvalidator>&nbsp;&nbsp;&nbsp;</td>
                                    <td noWrap><ml:zipcodetextbox id=aBZip runat="server" onchange="ValidateCityState();" CssClass="mask" preset="zipcode" width="50"></ml:zipcodetextbox>
                                        <asp:requiredfieldvalidator id=aBZipValidator runat="server" Display="Dynamic" ControlToValidate="aBZip">
                                            <img runat="server" src="../../images/error_icon.gif">
                                        </asp:requiredfieldvalidator>&nbsp;&nbsp;&nbsp;</td>
                                    <td><asp:textbox id=aBAddrYrs runat="server" Width="70px" MaxLength="3"></asp:textbox></td>
                                 </tr>
                                </table>
                               </td>
                              </tr>
                            <tr >
                              <td class="FormTableSubheader">Previous Address</td></tr>
                            <tr>
                              <td>
                                <TABLE id=Table13 cellSpacing=0 cellPadding=0 width=300 border=0 style="padding: 5px;">
                                  <TR>
                                    <TD class=FieldLabel noWrap><ml:EncodedLabel ID="Label21" runat="server" AssociatedControlID="aBPrev1Addr" >Street Address</ml:EncodedLabel></TD></TR>
                                  <TR>
                                    <TD noWrap><asp:textbox id=aBPrev1Addr runat="server" Width="265px"></asp:textbox></TD></TR></TABLE>
                                <table id=Table12 cellSpacing=0 cellPadding=0 border=0 style="padding: 5px;">
                                  <tr>
                                    <td class=FieldLabel><ml:EncodedLabel ID="Label22" runat="server" AssociatedControlID="aBPrev1City" >City</ml:EncodedLabel></td>
                                    <td class=FieldLabel><ml:EncodedLabel ID="Label23" runat="server" AssociatedControlID="aBPrev1State" >State</ml:EncodedLabel></td>
                                    <td class=FieldLabel><ml:EncodedLabel ID="Label24" runat="server" AssociatedControlID="aBPrev1Zip" >Zipcode</ml:EncodedLabel></td>
                                    <td class=FieldLabel><ml:EncodedLabel ID="Label25" runat="server" AssociatedControlID="aBPrev1AddrYrs" >Length (Yrs)</ml:EncodedLabel></td></tr>
                                  <tr>
                                    <td><asp:textbox id=aBPrev1City runat="server" enableviewstate="False" Width="180px"></asp:textbox></td>
                                    <td><ml:statedropdownlist id=aBPrev1State runat="server" enableviewstate="False"></ml:statedropdownlist></td>
                                    <td><ml:zipcodetextbox id=aBPrev1Zip runat="server" enableviewstate="False" CssClass="mask" preset="zipcode" width="50"></ml:zipcodetextbox></td>
                                    <td><asp:textbox id=aBPrev1AddrYrs runat="server" enableviewstate="False" Width="70" MaxLength="3"></asp:textbox></td></tr>
                                </table>
                                </td>
                            </tr>

                            <TR>
                              <TD></TD></TR></table>

            </div>
            </div>
                <div id="pagefooter">
                    <input class="ButtonStyle" id="btnSubmit" onclick="f_onCreditClick();" type="button" value="Submit Credit Request" />
                    <img src="../../images/warning.svg" id="WarningIcon" class="WarningIcon" />
                </div>
            </div>
            </div>
            <br style="clear:both" /> <%-- This is to stop the added HTML (copyright notice) =( from showing up in the middle of the page. --%>

             <div class="blockMask" id="MaskDiv" style="DISPLAY: none"></div>
             <div id="WaitMessage" style="DISPLAY: none; Z-INDEX: 205">
                <table height="100%" width="100%">
                    <tr>
                        <td vAlign=middle align=center>
                            <div style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; FONT-WEIGHT: bold; Z-INDEX: 205; PADDING-BOTTOM: 30px; WIDTH: 250px; COLOR: black; PADDING-TOP: 30px; HEIGHT: 60px; BACKGROUND-COLOR: white" >
                                <span id="WaitMessageTextSpan">Ordering credit report. Please wait ...</span> <br >
                                <img height=10 id="WaitMessageLoadingImg" src="../../images/status.gif" alt="Status">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

        </form>
<script  type="text/javascript">
var canOrderCredit = false;
function _init()
{
    resize(790, 520); <%-- Only valid if window is ModalDialog --%>

    var protocol = f_getSelectedCraProtocol();
    var lqiReportId = <%= AspxTools.JsString(this.LqiFileId) %>;
    var reportId = <%= AspxTools.JsString(this.FileId) %>;
    if (lqiReportId !== '' && protocol.ReissueLqiReportSettings)
    {
        <%= AspxTools.JsGetElementById(reissueLqi) %>.checked = true;
    }
    else if (lqiReportId !== '' && protocol.OrderLqiReportSettings)
    {
        <%= AspxTools.JsGetElementById(orderLqi) %>.checked = true;
    }
    else if (reportId !== '')
    {
      <%= AspxTools.JsGetElementById(m_ReorderReport) %>.checked = true;
    }
    else
    {
      <%= AspxTools.JsGetElementById(m_OrderNewReport) %>.checked = true;
    }

    f_onCraChange();

    if (typeof Page_ClientValidate === 'function')
    {<%--
       This is defined by the .aspx page, and used to validate prior to ordering credit.  We want Page_ClientValidate
       to run normally as called by the page (during OrderCredit validation), but not when called by the
       "new framework" (loanframework2.js) in the saveMe function.  We should be able to save the page without
       a set of CRA credentials, for example. --%>
        var oldPage_ClientValidate = Page_ClientValidate;
        Page_ClientValidate = function(validationGroup, actuallyValidate) {
            if (actuallyValidate) {
                return oldPage_ClientValidate(validationGroup);
            }

            return true;
        };
    }

    addEventHandler(document.getElementById('orderLqi_generalReportTypeDropdown'), 'change', handleOrderLqiReportTypeDDLChange);
    addEventHandler(document.getElementById('removeApplicantRemoveBorrower'), 'change', handleRemoveApplicantTypeChange);
    addEventHandler(document.getElementById('removeApplicantRemoveCoBorrower'), 'change', handleRemoveApplicantTypeChange);
    var list = [<%= AspxTools.JsGetElementById(m_OrderNewReport) %>,
                <%= AspxTools.JsGetElementById(m_ReorderReport) %>,
                <%= AspxTools.JsGetElementById(m_UpgradeReport) %>,
                <%= AspxTools.JsGetElementById(m_RetrieveReport) %>,
                <%= AspxTools.JsGetElementById(orderLqi) %>,
                <%= AspxTools.JsGetElementById(reissueLqi) %>,
                <%= AspxTools.JsGetElementById(UseCreditCardPayment) %>,
                <%= AspxTools.JsGetElementById(m_rememberLoginCB) %>,
                <%= AspxTools.JsGetElementById(m_isImport0BalanceCB) %>,
                <%= AspxTools.JsGetElementById(m_isMortgageOnlyCB) %>
                ];

    for (var i = 0; i < list.length; i++)
    {
        if (list[i] != null)
        {
                removeEventHandler(list[i], 'click',updateDirtyBit,false);
        }
    }
}

            function f_validateBeforeSubmit()
            {
                var bValid = false;
                var bIssueNewReport = <%= AspxTools.JsGetElementById(m_OrderNewReport) %>.checked;
                var confirmMsg = '';
                var upgradeMsg = '';

		        var oProtocol = f_getSelectedCraProtocol();
		        var missingReportIdMessage = 'Please enter the Report ID.';
		        if (oProtocol.IsLenderMapping)
		        {
			        <%-- 7/12/2006 nw - OPM 3638 - Allow reissue for Lender Mapping CRA --%>
			        if (bIssueNewReport)
				        bValid = true;
			        else
			        {
				        if (document.getElementById("<%= AspxTools.ClientId(m_reportIDTF) %>").value != "")
					        bValid = true;
			        }
		        }
		        else
		        {
			        if (bIssueNewReport)
			        {
			            if (!oProtocol.IsCBC)
			            {
			                f_enableAllValidators(true);
			            }

				        f_enableBorrowerRequiredIcons(true);
				        if (ReportTypeData.orderNew.getAtLeastOneBureauIsSelected())
					        bValid = true;
			        }
			        else
			        {
			            var reportIdtf = document.getElementById('<%= AspxTools.ClientId(m_reportIDTF) %>');
				        if (oProtocol.IsMcl && reportIdtf.value != "" &&
				            document.getElementById('<%= AspxTools.ClientId(m_iviewIDTF) %>').value != "")
				        {
					        <%-- User reissue MCL report using file id and instant viewid. Disable all required validator because those are not needed.--%>
					        f_enableBorrowerValidator(false);
					        f_enableBorrowerRequiredIcons(false);
					        bValid = true;
				        }
				        else if (reportIdtf.value != "")
				        {
					        if (oProtocol.IsMcl)
					        {
						        <%-- MCL Reissue credit report doesn't require borrower information--%>
						        f_enableBorrowerValidator(false);
						        f_enableBorrowerRequiredIcons(false);
					        }
					        bValid = true;
				        }
			        }
		        }

		        if( <%= AspxTools.JsGetElementById(m_UpgradeReport) %>.checked )
		        {
			        if( oProtocol.IsMcl || oProtocol.IsSharperLending || oProtocol.IsKrollFactualData || oProtocol.IsCBC || oProtocol.IsIR || oProtocol.IsCredco || oProtocol.IsEquifax)
			        {
			            if (!oProtocol.IsCBC)
			            {
			                f_enableAllValidators(true);
			            }

			            f_enableBorrowerRequiredIcons(true);

			            bValid = ReportTypeData.upgrade.getReportId() != "";
			            if(!bValid)
			            {
			                upgradeMsg = missingReportIdMessage;
			            }
			        }
			        else
			        {
				        bValid = false;
				        upgradeMsg = "Upgrade feature is only available for MCL, Sharper Lending, Kroll Factual Data, CBC, Informative Research, and Credco credit agencies.";
			        }
		        }
        		if (<%= AspxTools.JsGetElementById(m_RetrieveReport) %>.checked)
        		{
        		  if (oProtocol.IsKrollFactualData || oProtocol.IsIR || oProtocol.IsCSD || oProtocol.IsEquifax)
        		  {
        		        var retrieveReportId = <%= AspxTools.JsGetElementById(m_retrieveReportIDTF) %>;
        		        bValid = retrieveReportId.value != "";
			            if(!bValid)
			            {
			                upgradeMsg = missingReportIdMessage;
			            }
        		  }
        		  else
        		  {
        		    bValid = false;
        		    upgradeMsg = "Retrieve feature is only available for Kroll Factual Data, Informative Research, Credit Systems Design, and Equifax.";
        		  }
        		}

        		if (<%= AspxTools.JsGetElementById(orderLqi) %>.checked)
        		{
        		    if (!oProtocol.OrderLqiReportSettings)
        		    {
        		        bValid = false;
        		    }
        		    else if (ReportTypeData.orderLqi.getReportId() === '')
        		    {
        		        bValid = false;
        		        upgradeMsg = "Please enter the " + oProtocol.ReportIdLabel + " associated with the existing credit report.";
        		    }
        		    else if (oProtocol.OrderLqiReportSettings.EnableBureauSelection && !ReportTypeData.orderLqi.getAtLeastOneBureauIsSelected())
        		    {
        		        bValid = false;
        		        upgradeMsg = "Please select at least one bureau.";
        		    }
        		    else
        		    {
        		        bValid = true;
        		    }
        		}

                if (<%= AspxTools.JsGetElementById(reissueLqi) %>.checked)
                {
                    if (!oProtocol.ReissueLqiReportSettings)
                    {
                        bValid = false;
                    }
                    else if (ReportTypeData.reissueLqi.getReportId() === '')
                    {
                        bValid = false;
                        upgradeMsg = oProtocol.ReissueLqiReportSettings.MissingReportIdMessage;
                    }
                    else
                    {
                        bValid = true;
                    }
                }

                if (document.getElementById('removeApplicant').checked) {
                    if (!oProtocol.RemoveApplicantSettings) {
                        bValid = false;
                    } else if (!ReportTypeData.removeApplicant.getReportId()) {
                        bValid = false;
                        upgradeMsg = missingReportIdMessage;
                    } else if (!document.getElementById('removeApplicantRemoveBorrower').checked && !document.getElementById('removeApplicantRemoveCoBorrower').checked) {
                        bValid = false;
                        upgradeMsg = 'Please select a borrower to remove';
                    } else {
                        bValid = true;
                        confirmMsg = 'Regulatory disclosure may be required prior to removal of an applicant\'s credit data. Do you wish to proceed?';
                    }
                }

                if (document.getElementById('orderSupplement').checked) {
                    if (!oProtocol.OrderSupplementSettings) {
                        bValid = false;
                    } else if (!ReportTypeData.orderSupplement.getReportId()) {
                        bValid = false;
                        upgradeMsg = missingReportIdMessage;
                    } else {
                        bValid = true;
                    }
                }

                if (document.getElementById('retrieveSupplement').checked) {
                    if (!oProtocol.RetrieveSupplementSettings) {
                        bValid = false;
                    } else if (!ReportTypeData.retrieveSupplement.getReportId()) {
                        bValid = false;
                        upgradeMsg = missingReportIdMessage;
                    } else {
                        bValid = true;
                    }
                }

                if (document.getElementById('retrieveBillingReport').checked) {
                    if (!oProtocol.RetrieveBillingReportSettings) {
                        bValid = false;
                    } else if (!ReportTypeData.retrieveBillingReport.getReportId()) {
                        bValid = false;
                        upgradeMsg = missingReportIdMessage;
                    } else {
                        bValid = true;
                    }
                }

                if (bValid && confirmMsg) {
                    bValid = window.confirm(confirmMsg);
                }

                if (!bValid) {
                    if (confirmMsg) {<%--no-op, user already knows via confirmation why they're not continuing--%>
                    } else if (upgradeMsg) {
                        alert(upgradeMsg);
                    } else {
                        alert(<%= AspxTools.JsString(JsMessages.EnterCredit_MissingInformation) %>);
                    }
                } else {
                    <%-- dd 12/06/04 - Submit button is set CauseValidation=false therefore the below line is necessary to fire client side validation.--%>
                    if (typeof(Page_ClientValidate) == 'function') bValid = Page_ClientValidate(undefined, true);
                }
                return bValid;
            }


            function validateStateDropDown(source, args)
            {
		        args.IsValid = document.getElementById('<%= AspxTools.ClientId(aBState) %>').selectedIndex != 0;
            }


            function validateCRADropDown(source, args)
            {
		        args.IsValid = document.getElementById('<%= AspxTools.ClientId(CreditAgencyList) %>').selectedIndex != 0;
            }


function f_enablePayWithCreditCard(bEnabled)
{
    if (!bEnabled)
    {
        if (<%= AspxTools.JsGetElementById(UseCreditCardPayment) %>)
        {
            <%= AspxTools.JsGetElementById(UseCreditCardPayment) %>.checked = false;
        }
        document.getElementById("m_showPayWithCreditCardOption").style.display = "none";
    }
    else
    {
        document.getElementById("m_showPayWithCreditCardOption").style.display = "";
    }
}


	        function ClearAllFieldMissingIconsAndSetAllFieldValidation()
	        {
		        f_enableAllValidators(false);
		        if (typeof(Page_ClientValidate) == 'function') bValid = Page_ClientValidate(undefined, true);
		        f_enableAllValidators(true);
	        }

            function f_enableInstantView(bEnabled)
            {
		        var iViewIDTF = <%= AspxTools.JsGetElementById(m_iviewIDTF) %>;
		        if(bEnabled == false)
		        {
			        iViewIDTF.value = "";
			        iViewIDTF.readOnly = true;
		        }
		        else
			        iViewIDTF.readOnly = false;
            }

    var ReportTypeData = (function(){
        function createCreditReportTypeDDL(dropdown, dropdownContainer) {
            return {
                setup: function(reportTypes) {
                    dropdownContainer.style.display = reportTypes ? '' : 'none';
                    populateDropdown(dropdown, reportTypes);
                },
                getSelectedCreditReportType: function() {
                    return dropdown.value;
                }
            };
        }

        var blankReportTypeDDL = { setup: function(){}, getSelectedCreditReportType: function() {} };
        var orderNewCreditReportTypeDDL = createCreditReportTypeDDL(document.getElementById('orderNew_generalReportTypeDropdown'), document.getElementById('orderNew_generalReportTypeOption'));
        var orderLqiCreditReportTypeDDL = createCreditReportTypeDDL(document.getElementById('orderLqi_generalReportTypeDropdown'), document.getElementById('orderLqi_generalReportTypeOption'));

        var orderNewBureauCheckboxes = [document.getElementById('orderNew_Equifax'), document.getElementById('orderNew_Experian'), document.getElementById('orderNew_TransUnion')];
        var upgradeBureauCheckboxes = [document.getElementById('upgradeBureau_Equifax'), document.getElementById('upgradeBureau_Experian'), document.getElementById('upgradeBureau_TransUnion')];
        var orderLqiBureauCheckboxes = [document.getElementById('orderLqiBureau_Equifax'), document.getElementById('orderLqiBureau_Experian'), document.getElementById('orderLqiBureau_TransUnion')];
        var initializeBureaus = function(o, bureauCheckboxes) {
            o['getIsEquifax'] = function() {return bureauCheckboxes[0].checked;};
            o['getIsExperian'] = function() {return bureauCheckboxes[1].checked;};
            o['getIsTransUnion'] = function() {return bureauCheckboxes[2].checked;};
            o['setIsEquifax'] = function(b) {bureauCheckboxes[0].checked = !!b;};
            o['setIsExperian'] = function(b) {bureauCheckboxes[1].checked = !!b;};
            o['setIsTransUnion'] = function(b) {bureauCheckboxes[2].checked = !!b;};
            o['getAtLeastOneBureauIsSelected'] = function() {
                return o.getIsEquifax() || o.getIsExperian() || o.getIsTransUnion();
            };
            return o;
        }

        function create(reportIdField, reportIdRequiredImage, optionSection, bureauCheckboxes, reportTypeDropdown, toggleEnabledCallback) {
            var additionalCallback = function() {};
            if (typeof toggleEnabledCallback === 'function') {
                additionalCallback = toggleEnabledCallback;
            } else if (bureauCheckboxes) {
                additionalCallback = function(isEnabled){ toggleBureaus(bureauCheckboxes, isEnabled, isEnabled); };
            }

            var obj = {
                getReportId: function() { if (reportIdField) return reportIdField.value;},
                enable: function(reportId) {
                    if (reportIdField) {
                        reportIdField.readOnly = false;
                        reportIdField.value = reportId;
                        reportIdRequiredImage.style.display = "";
                    }
                    optionSection.style.display = '';
                    additionalCallback(true);
                    return this;
                },
                disable: function() {
                    if (reportIdField) {
                        reportIdField.value = "";
                        reportIdField.readOnly = true;
                        reportIdRequiredImage.style.display = "none";
                    }
                    optionSection.style.display = 'none';
                    additionalCallback(false);
                    return this;
                },
                reportTypeDropdown: reportTypeDropdown || blankReportTypeDDL
            };

            if (bureauCheckboxes) {
                initializeBureaus(obj, bureauCheckboxes);
            }

            return obj;
        }

        return {
            orderNew: create(undefined, undefined, document.getElementById('m_OrderNewReportOptions'), orderNewBureauCheckboxes, orderNewCreditReportTypeDDL),
            reissue: create(<%= AspxTools.JsGetElementById(m_reportIDTF) %>, document.getElementById('m_ReportIdReq'), document.getElementById('m_ReorderReportOptions'), undefined, undefined, f_enableInstantView),
            upgrade: create(<%= AspxTools.JsGetElementById(m_upgradeReportIDTF) %>, document.getElementById('m_upgradeReportIdReq'), document.getElementById('m_UpgradeReportOptions'), upgradeBureauCheckboxes),
            retrieve: create(<%= AspxTools.JsGetElementById(m_retrieveReportIDTF) %>, document.getElementById('m_retrieveReportIdReq'), document.getElementById('m_RetrieveReportOptions')),
            orderLqi: create(<%= AspxTools.JsGetElementById(orderLqiReportId) %>, document.getElementById('orderLqiReportIdReq'), document.getElementById('orderLqiOptions'), orderLqiBureauCheckboxes, orderLqiCreditReportTypeDDL),
            reissueLqi: create(<%= AspxTools.JsGetElementById(reissueLqiReportId) %>, document.getElementById('reissueLqiReportIdReq'), document.getElementById('reissueLqiOptions')),
            removeApplicant: create(document.getElementById('removeApplicantReportId'), document.getElementById('removeApplicantReportIdReq'), document.getElementById('removeApplicantOptions')),
            orderSupplement: create(document.getElementById('orderSupplementReportId'), document.getElementById('orderSupplementReportIdReq'), document.getElementById('orderSupplementOptions')),
            retrieveSupplement: create(document.getElementById('retrieveSupplementReportId'), document.getElementById('retrieveSupplementReportIdReq'), document.getElementById('retrieveSupplementOptions')),
            retrieveBillingReport: create(document.getElementById('retrieveBillingReportReportId'), document.getElementById('retrieveBillingReportReportIdReq'), document.getElementById('retrieveBillingReportOptions'))
        };
    })();

	        function onReportTypeClick()
            {
		        ClearAllFieldMissingIconsAndSetAllFieldValidation();
		        var oProtocol = f_getSelectedCraProtocol();
                var enableBureauSelection = false;
                var enableMortgageOnlyReport = true;

                var allReportTypeData = [<%-- ReportTypeData.orderNew is special and is intentionally excluded from this list --%>
                    ReportTypeData.reissue,
                    ReportTypeData.upgrade,
                    ReportTypeData.retrieve,
                    ReportTypeData.orderLqi,
                    ReportTypeData.reissueLqi,
                    ReportTypeData.removeApplicant,
                    ReportTypeData.orderSupplement,
                    ReportTypeData.retrieveSupplement,
                    ReportTypeData.retrieveBillingReport
                ];
                var enabledReportTypeData = undefined;<%-- When this has a value, we'll disable all the others from allReportTypes --%>

		        <%--If issuing new report--%>
		        if (<%= AspxTools.JsGetElementById(m_OrderNewReport) %>.checked)
		        {
			        f_enablePayWithCreditCard(oProtocol.IsMcl || oProtocol.IsCBC || oProtocol.IsKrollFactualData || oProtocol.IsSharperLending || oProtocol.IsIR || oProtocol.IsCreditInterlink || oProtocol.IsCredco);
			        enableBureauSelection = !oProtocol.IsLenderMapping; // OPM 3638
			        f_enableBorrowerRequiredIcons(true);

                    enabledReportTypeData = ReportTypeData.orderNew;
                }
                else if(<%= AspxTools.JsGetElementById(m_ReorderReport) %>.checked)<%--if re-issuing report--%>
                {
                    f_enablePayWithCreditCard(false);
                    enabledReportTypeData = ReportTypeData.reissue;
                    enabledReportTypeData.enable(<%= AspxTools.JsString(FileId) %>);
                    f_enableInstantView(oProtocol.IsMcl);

                    enableBureauSelection = oProtocol.IsLandsafe || oProtocol.IsFiserv;

			        if(oProtocol.IsMcl)
			        {
				        <%--if re-issuing and IsMcl, don't display required field icons for Borrower Information--%>
				        f_enableBorrowerValidator(false);
				        f_enableDULogin(false)
				        f_enableBorrowerRequiredIcons(false);
			        }
			        else
			        {
				        f_enableAllValidators(true);
				        f_enableBorrowerRequiredIcons(true);
			        }
		        }
                else if (<%= AspxTools.JsGetElementById(m_UpgradeReport) %>.checked) {<%--upgrade selected--%>
                    <%--Disable order stuff--%>
                    f_enablePayWithCreditCard(true);
                    enabledReportTypeData = ReportTypeData.upgrade;

			        <%--Enable upgrade stuff--%>
			        if(oProtocol.IsMcl)
			        {
				        <%--if upgrading and IsMcl, don't display required field icons for Borrower Information--%>
				        f_enableBorrowerValidator(false);
				        f_enableDULogin(false)
				        f_enableBorrowerRequiredIcons(true);
                        ReportTypeData.upgrade.enable(<%=AspxTools.JsString(FileId) %>);
			        }
			        else if (oProtocol.IsKrollFactualData || oProtocol.IsEquifax)
			        {
			            f_enablePayWithCreditCard(false);
                        ReportTypeData.upgrade.enable(<%=AspxTools.JsString(FileId) %>);
			        }
			        else if (oProtocol.IsSharperLending || oProtocol.IsCBC || oProtocol.IsIR || oProtocol.IsCredco)
			        {
			            f_enablePayWithCreditCard(true);
                        ReportTypeData.upgrade.enable(<%=AspxTools.JsString(FileId) %>);
			        }
			        else
			        {
			            f_enablePayWithCreditCard(false);
                        ReportTypeData.upgrade.enable("");
                    }
                }
                else if (<%= AspxTools.JsGetElementById(m_RetrieveReport) %>.checked) {<%-- retrieve is selected --%>
                    enabledReportTypeData = ReportTypeData.retrieve;
                    enabledReportTypeData.enable(<%= AspxTools.JsString(FileId) %>);

			        if(!oProtocol.IsEquifax)
			        {
			            f_enablePayWithCreditCard(true);
			        }
                }
                else if (<%= AspxTools.JsGetElementById(orderLqi) %>.checked) <%-- order LQI is selected --%>
                {
                    <%--Disable order stuff--%>
                    if(!oProtocol.IsEquifax)
                    {
                        f_enablePayWithCreditCard(true);
                    }

                    f_enableBorrowerRequiredIcons(true);
                    enableMortgageOnlyReport = false;

                    enabledReportTypeData = ReportTypeData.orderLqi;
                    enabledReportTypeData.enable(<%= AspxTools.JsString(FileId) %>);
                }
                else if (<%= AspxTools.JsGetElementById(reissueLqi) %>.checked) <%-- reissue LQI is selected --%>
                {
                    f_enableBorrowerRequiredIcons(true);
                    enabledReportTypeData = ReportTypeData.reissueLqi;
                    enabledReportTypeData.enable(<%= AspxTools.JsString(LqiFileId) %>);
                } else if (document.getElementById('removeApplicant').checked) {
                    f_enablePayWithCreditCard(false);
                    f_enableBorrowerRequiredIcons(true);
                    enabledReportTypeData = ReportTypeData.removeApplicant;
                    enabledReportTypeData.enable(<%= AspxTools.JsString(FileId) %>);
                } else if (document.getElementById('orderSupplement').checked) {
                    f_enablePayWithCreditCard(false);
                    enabledReportTypeData = ReportTypeData.orderSupplement;
                    enabledReportTypeData.enable(<%= AspxTools.JsString(FileId) %>);
                } else if (document.getElementById('retrieveSupplement').checked) {
                    f_enablePayWithCreditCard(false);
                    enabledReportTypeData = ReportTypeData.retrieveSupplement;
                    enabledReportTypeData.enable(<%= AspxTools.JsString(FileId) %>);
                } else if (document.getElementById('retrieveBillingReport').checked) {
                    f_enablePayWithCreditCard(false);
                    enabledReportTypeData = ReportTypeData.retrieveBillingReport;
                    enabledReportTypeData.enable(<%= AspxTools.JsString(FileId) %>);
                }

                if (enabledReportTypeData) {
                    for (var i = 0; i < allReportTypeData.length; ++i) {
                        if (allReportTypeData[i] !== enabledReportTypeData) {
                            allReportTypeData[i].disable();
                        }
                    }
                }

                toggleEnableCheckbox(<%= AspxTools.JsGetElementById(m_isMortgageOnlyCB) %>, enableMortgageOnlyReport);
                if (enableBureauSelection) {
                    ReportTypeData.orderNew.enable();
                } else {
                    ReportTypeData.orderNew.disable();
                }
            }

    function toggleBureaus(bureauCBs, enableBureauSelection, checkAllBureauCBs) {
        for (var i = 0; i < bureauCBs.length; i++) {
            if (checkAllBureauCBs) bureauCBs[i].checked = true;
            toggleEnableCheckbox(bureauCBs[i], enableBureauSelection);
        }
    }
    function toggleEnableCheckbox(cb, enable) {
        cb.disabled = !enable;
        if (!enable) cb.checked = false;
    }

    function populateDropdown(ddl, optionList) {
        if (!ddl || !optionList || typeof optionList.length !== 'number') {
            return;
        }
        while (ddl.firstChild) {
            ddl.removeChild(ddl.firstChild);
        }
        for (var i = 0; i < optionList.length; ++i) {
            var option = document.createElement('option');
            option.value = optionList[i].Value;
            option.appendChild(document.createTextNode(optionList[i].Text));
            ddl.appendChild(option);
        }
    }

    function handleOrderLqiReportTypeDDLChange(event) {
        var protocol = f_getSelectedCraProtocol();
        if (<%= AspxTools.JsGetElementById(orderLqi) %>.checked && protocol.OrderLqiReportSettings && protocol.OrderLqiReportSettings.ReportTypeChangeHandler) {
            protocol.OrderLqiReportSettings.ReportTypeChangeHandler(retrieveEventTarget(event).value);
        }
    }

    function handleRemoveApplicantTypeChange(event) {
        var warningMessageElement = document.getElementById('RemoveBorrowerWarningMessage');
        while (warningMessageElement.firstChild) {
            warningMessageElement.removeChild(warningMessageElement.firstChild);
        }
        if (document.getElementById('removeApplicantRemoveBorrower').checked) {
            warningMessageElement.appendChild(document.createTextNode('Removing a borrower will swap applicant positions.'));
        }
    }

            function f_enableAccountIdField(bEnabled)
            {
		        if(!bEnabled)
		        {
		            document.getElementById("m_AccountIdentifierPanel").style.display = "none";
			        document.getElementById("m_AccountIdReq").style.display = "none";
		        }
		        else
		        {
		            document.getElementById("m_AccountIdentifierPanel").style.display = "inline";
			        document.getElementById("<%= AspxTools.ClientId(m_accountIdentifier) %>").readOnly = false;
			        document.getElementById("m_AccountIdReq").style.display = "inline";
		        }
            }

            function f_enableLoginPasswordPanel(bEnabled)
            {
		        if(!bEnabled)
		        {
		            document.getElementById("LoginPasswordPanel").style.display = "none";
		        }
		        else
		        {
		            document.getElementById("LoginPasswordPanel").style.display = "inline";
		        }
            }


            function f_setLoginLabel(label)
            {
		        document.getElementById("LoginLabel").innerText = label;
		        <%--also set the Remember Login label to reflect whatever we've now labeled the login field--%>
		        setRememberLoginCBLabel("Remember My " + label);
            }


            function setRememberLoginCBLabel(label)
            {
		        document.getElementById("RememberLabel").innerText = label;
            }

function f_refreshUI()
{
    var value = <%= AspxTools.JsGetElementById(CreditAgencyList) %>.value;
    var oProtocol = f_getSelectedCraProtocol();

    var credentialsStored = f_areCredentialsStored(value, oProtocol);

    if (value == <%=AspxTools.JsString(Guid.Empty)%>)
    {
        ClearAllFieldMissingIconsAndSetAllFieldValidation();
        document.getElementById("FannieMaePanel").style.display = "none";
        f_enableDULogin(false);
        <%= AspxTools.JsGetElementById(m_accountIdentifier) %>.readOnly = true;
        f_enableAccountIdField(false);
        document.getElementById("btnSubmit").disabled = true;
        f_enablePayWithCreditCard(false);
        f_enableLoginPassword(false);
        f_enableLoginPasswordPanel(true);
        f_enableAllValidators(false);
        f_enableInstantView(false);
        f_hideCredentialAreaIfEmpty();
        return;
    }

    document.getElementById("btnSubmit").disabled = !(<%= AspxTools.JsBool(!IsReadOnly) %> && canOrderCredit);

    if (oProtocol.IsCBC)
    {
        <%-- dd 07/26/2013 - OPM 128903 Support Mortgage Only Credit Report --%>
        document.getElementById("m_mortgageOnlyOption").style.display="";
    }
    else
    {
        document.getElementById("m_mortgageOnlyOption").style.display="none";
    }

    if (oProtocol.IsMcl)
    {
        <%-- jl 10/16/2014 - OPM 134848 Support Mortgage-Only Credit Reports from MCL --%>
        document.getElementById("mclReportTypeOption").style.display="";
    }
    else
    {
        document.getElementById("mclReportTypeOption").style.display="none";
    }

    document.getElementById("CreditLoginInformation").style.display = oProtocol.IsLenderMapping  ? "none" : "block";

    document.getElementById('upgradeReportIdLabel').innerText = oProtocol.ReportIdLabel;
    document.getElementById('orderLqiReportIdLabel').innerText = oProtocol.ReportIdLabel;
    document.getElementById('ReportIdLabel').innerText = oProtocol.ReportIdLabel;
    document.getElementById('reissueLqiReportIdLabel').innerText = (oProtocol.ReissueLqiReportSettings && oProtocol.ReissueLqiReportSettings.LqiReportIdLabel) || oProtocol.ReportIdLabel;

    showInstantView(oProtocol.IsMcl, <%= AspxTools.JsGetElementById(m_OrderNewReport) %>.checked);
    onReportTypeClick();

    <%-- This is needed in case they had entered an instant view id and then selected a new CRA--%>
    <%-- We want to make sure the login and password validators get disabled since they're not required with instant view --%>
    f_oniViewIDkeyUp();

    <%-- 7/12/2006 nw - OPM 3638 - Allow reissue for Lender Mapping CRA--%>
    <%-- <%= AspxTools.JsGetElementById(m_OrderNewReport) %>.disabled = oProtocol.IsLenderMapping;--%>
    <%-- <%= AspxTools.JsGetElementById(m_ReorderReport) %>.disabled = oProtocol.IsLenderMapping;   --%>
    f_enableLoginPasswordPanel(!credentialsStored);
    f_enableLoginPassword(!oProtocol.IsLenderMapping && !credentialsStored);
    f_enableLoginPasswordValidators(!oProtocol.IsLenderMapping && !credentialsStored);


    <%-- 7/13/2006 nw - OPM 3638 - For Lender Mapping CRA, display which bureaus will be ordered on a new report  --%>
    if (oProtocol.IsLenderMapping)
    {
        <%--
            /*
             * 11/6/2013 gf - Added an object to keep track of each lender mapped CRA's settings.
             * The lenderMappedCRAs variable is defined/registered in the code behind. It is only
             * included when the broker has at least 1 lender mapped cra.
             */
        --%>
        var bureauSettings = lenderMappedCRAs[value];
        ReportTypeData.orderNew.setIsEquifax(bureauSettings.IsEquifaxPulled);
        ReportTypeData.orderNew.setIsExperian(bureauSettings.IsExperianPulled);
        ReportTypeData.orderNew.setIsTransUnion(bureauSettings.IsTransUnionPulled);
    }

    <%--Protocol for Kroll is Fannie, so we need to use the actual value for Kroll--%>
    <%--OPM 17962 - potentially obsolete message--%>
    <%--document.GetElementById("KrollMsg.style.display = (value == "da66612f-d574-408a-9785-6eafbf1a57dd")? "" : "none";--%>
    <%--OPM 4063 - hide the account id field instead of disabling it to reduce user confusion --%>

    if (credentialsStored)
    {
        f_enableAccountIdField(false);
        f_enableAccountIdValidation(false);
    }
    else if (oProtocol.IsKrollFactualData || oProtocol.IsMismo || oProtocol.IsSharperLending || oProtocol.IsInfoNetwork || oProtocol.IsIR)
    {
        f_enableAccountIdField(true);
        f_enableAccountIdValidation(true);
    }
    else if (oProtocol.IsCBC)
    {
        f_enableAccountIdField(true);
        f_enableAccountIdValidation(false);
    }
    else
    {
        f_enableAccountIdField(false);
        f_enableAccountIdValidation(false);
    }

    if (oProtocol.IsSharperLending || oProtocol.IsIR)
    {
        document.getElementById("AccountIDLabel").innerText = "Client ID";
        document.getElementById("m_AccountIdReq").style.display = "";
    }
    else if (oProtocol.IsCBC)
    {
        document.getElementById("AccountIDLabel").innerText = "Account ID";
        document.getElementById("m_AccountIdReq").style.display = "none";
    }
    else
    {
        document.getElementById("AccountIDLabel").innerText = "Account ID";
        document.getElementById("m_AccountIdReq").style.display = "";
    }

    if (oProtocol.IsCredco || oProtocol.IsUniversalCredit || oProtocol.IsCredco)
    {
        f_setLoginLabel("Account Number");
    }
    else if (oProtocol.IsSharperLending)
    {
        f_setLoginLabel("User Name");
    }
    else if (oProtocol.IsIR)
    {
        f_setLoginLabel("User ID");
    }
    else if (value == "da66612f-d574-408a-9785-6eafbf1a57dd")
    {
        <%-- 5/26/2006 nw - OPM 5099 - If user selects "Kroll Factual Data (052)" or "Info1/LandAmerica (018)", --%>
        <%-- change the "Login" field label to "Account Number" or "Client ID", respectively.  --%>
        <%-- We can't use oProtocol because both of these entries use .FannieMae, which is not a unique protocol type.  --%>

        f_setLoginLabel("Account Number");
    }
    else if (value == "7c6e1bdd-a963-48aa-8eca-2a4c0b355132")
    {
        f_setLoginLabel("Client ID");
    }
    else if (value == "55e6cc10-2ae9-4772-88ec-ad2dfeb730bb")
    {
        <%-- 6/22/2006 nw - OPM 5099 - If user selects "NMR E-MERGE (046)", change "Login" to "Company ID". --%>
        f_setLoginLabel("Company ID");
    }
    else if (value == "44bd6469-4330-4891-87c2-9ab82787d6b6")
    {
        <%-- 7/26/2006 nw - OPM 4455 - If user selects "CREDCO (FANNIE MAE)", change "Login" to "Account Number". --%>
        f_setLoginLabel("Account Number");
    }
    else
    {
        f_setLoginLabel("Login");
    }

    if (oProtocol.IsFannieMae)
    {
        var bHasBrokerDUInfo = <%= AspxTools.JsBool(m_hasBrokerDUInfo) %>;
        document.getElementById("FannieMaePanel").style.display = bHasBrokerDUInfo ? "none" : "";
        f_enableDULogin(!bHasBrokerDUInfo);
    }
    else
    {
        document.getElementById("FannieMaePanel").style.display = "none";
        f_enableDULogin(false);
    }

    if (oProtocol.IsKrollFactualData || oProtocol.IsIR || oProtocol.IsCSD || oProtocol.IsEquifax)
    {
        document.getElementById("m_retrieveReportPanel").style.display = "";
    }
    else
    {
        <%-- // 4/20/2012 dd - Only Kroll Factual Data, Informative Research, and Credit Systems Design support Retrieve --%>
        document.getElementById("m_retrieveReportPanel").style.display = "none";
    }

    if (oProtocol.OrderNewStandardReportSettings) {
        ReportTypeData.orderNew.reportTypeDropdown.setup(oProtocol.OrderNewStandardReportSettings.ReportTypes);
    }

    document.getElementById("orderLqiPanel").style.display = oProtocol.OrderLqiReportSettings ? "" : "none";
    if (oProtocol.OrderLqiReportSettings) {
        <%= AspxTools.JsGetElementById(orderLqi) %>.nextSibling.innerText = oProtocol.OrderLqiReportSettings.NewReportLabelText;

        ReportTypeData.orderLqi.reportTypeDropdown.setup(oProtocol.OrderLqiReportSettings.ReportTypes);
        document.getElementById('orderLqiBureaus').style.display = oProtocol.OrderLqiReportSettings.EnableBureauSelection ? "" : "none";
    }

    document.getElementById("reissueLqiPanel").style.display = oProtocol.ReissueLqiReportSettings ? "" : "none";
    if (oProtocol.ReissueLqiReportSettings) {
        <%= AspxTools.JsGetElementById(reissueLqi) %>.nextSibling.innerText = oProtocol.ReissueLqiReportSettings.ReissueLqiLabelText;
    }

    document.getElementById('removeApplicantPanel').style.display = oProtocol.RemoveApplicantSettings ? '' : 'none';
    document.getElementById('orderSupplementPanel').style.display = oProtocol.OrderSupplementSettings ? '' : 'none';
    if (oProtocol.OrderSupplementSettings && document.getElementById('orderSupplement').checked) {
        f_enableBorrowerValidator(false);
        f_enableBorrowerRequiredIcons(false);
        f_enableAccountIdValidation(false);
        f_enableLoginPassword(false);
        f_enableLoginPasswordValidators(false);
    }
    document.getElementById('retrieveSupplementPanel').style.display = oProtocol.RetrieveSupplementSettings ? '' : 'none';
    if (oProtocol.RetrieveSupplementSettings && document.getElementById('retrieveSupplement').checked) {
        f_enableBorrowerValidator(false);
        f_enableBorrowerRequiredIcons(false);
    }
    document.getElementById('retrieveBillingReportPanel').style.display = oProtocol.RetrieveBillingReportSettings ? '' : 'none';

    document.getElementById("m_upgradeReportPanel").style.display = oProtocol.UpgradeReportSettings ? '' : 'none';
    if (oProtocol.UpgradeReportSettings) {
        document.getElementById('upgradeReportBureaus').style.display = oProtocol.UpgradeReportSettings.EnableBureauSelection ? '' : 'none';
    }

    <%-- // This UI can now get into a state where no login is needed --%>
    f_hideCredentialAreaIfEmpty();
}


    function f_areCredentialsStored(creditId, protocol)
    {
        <%-- // If we already have the credential data, completely hide the UI elements. --%>
        var hasCredentials = false;
        if (savedCredentials != null && (creditId != null || protocol.IsCBC))
        {
            for (var i = 0; i < savedCredentials.length; i++)
            {
                if (creditId == savedCredentials[i])
                {
                    // We have the creds saved, hide what we already know.
                    return true;
                }
            }
        }
        return false;
    }

            function f_getSelectedCraProtocol()
            {
                var value = <%= AspxTools.JsGetElementById(CreditAgencyList) %>.value;
                var protocolType = getProtocolType(value);

                var retValue = {
                    IsMcl              : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.Mcl) %>,
                    IsLandsafe         : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.Landsafe) %>,
                    IsFiserv           : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.Fiserv) %>,
                    IsLenderMapping    : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.LenderMappingCRA) %>,
                    IsKrollFactualData : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.KrollFactualData) %>,
                    IsCredco           : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.Credco) %>,
                    IsUniversalCredit  : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.UniversalCredit) %>,
                    IsMismo21          : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.MISMO_2_1) %>,
                    IsMismo23          : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.MISMO_2_3) %>,
                    IsInfoNetwork      : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.InfoNetwork) %>,
                    IsMismo            : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.MISMO_2_1) %> ||
                                         protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.MISMO_2_3) %>,
                    IsFannieMae        : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.FannieMae) %>,
                    IsSharperLending   : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.SharperLending) %>,
                    IsCSC              : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.CSC) %>,
                    IsCSD              : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.CSD) %>,
                    IsCBC              : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.CBC) %>,
                    IsIR               : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.InformativeResearch) %>,
                    IsCreditInterlink  : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.CreditInterlink) %>,
                    IsEquifax          : protocolType == <%= AspxTools.JsNumeric(CreditReportProtocol.Equifax) %>
                };

                retValue['OrderNewStandardReportSettings'] = getOrderNewStandardReportSettings(retValue);
                retValue['ReportIdLabel'] = retValue.IsMcl ? 'File #' : 'Report ID';
                retValue['UpgradeReportSettings'] = getUpgradeReportSettings(retValue);
                retValue['OrderLqiReportSettings'] = getOrderLqiReportSettings(retValue);
                retValue['ReissueLqiReportSettings'] = getReissueLqiReportSettings(retValue);
                retValue['RemoveApplicantSettings'] = getRemoveApplicantSettings(retValue);
                retValue['OrderSupplementSettings'] = getOrderSupplementSettings(retValue);
                retValue['RetrieveSupplementSettings'] = getRetrieveSupplementSettings(retValue);
                retValue['RetrieveBillingReportSettings'] = getRetrieveBillingReportSettings(retValue);

                return retValue;
            }
    function getOrderNewStandardReportSettings(prot) {
        var reportTypes, reportTypeChangeHandler;
        if (prot.IsIR) {
            reportTypes = [
                { Value: '0', Text: 'Premier' },
                { Value: '1', Text: 'SoftQual' },
                { Value: '2', Text: 'Mortgage Only' },
                { Value: '3', Text: 'Mortgage Only with Scores' }
            ];
        }
        return {
            ReportTypes: reportTypes
        };
    }
    function getUpgradeReportSettings(prot) {
        var enableBureaus;
        if (prot.IsIR) {
            enableBureaus = true;
        } else if (prot.IsMcl || prot.IsKrollFactualData || prot.IsEquifax || prot.IsSharperLending || prot.IsCBC || prot.IsCredco) {
            enableBureaus = false;
        }

        if (enableBureaus !== undefined) {
            return {
                EnableBureauSelection: enableBureaus
            };
        }
    }
    function getOrderLqiReportSettings(prot) {
        var orderNewLqiLabelText, enableBureaus = false, reportTypes, reportTypeChangeHandler;
        if (prot.IsIR) {
            orderNewLqiLabelText = "Order new LQI credit report";
            reportTypes = [
                { Value: '0', Text: 'PreClose' },
                { Value: '1', Text: 'QuickLook' },
                { Value: '2', Text: 'QuickLook with Scores' }
            ];
            reportTypeChangeHandler = function(newValue) {
                ReportTypeData.orderLqi.enable(ReportTypeData.orderLqi.getReportId());<%--Reset the bureau checkboxes--%>
                document.getElementById('orderLqiBureaus').style.display = newValue == '0' ? 'none' : '';
            }
        } else if (prot.IsMcl) {
            orderNewLqiLabelText = "Order new Refresh report";
        } else if (prot.IsEquifax) {
            orderNewLqiLabelText = "Order new Preclose credit report";
        } else if (prot.IsCBC) {
            orderNewLqiLabelText = "Order new Loan Quality Cross Check report";
            enableBureaus = true;
        } else if (prot.IsKrollFactualData) {
            orderNewLqiLabelText = "Order new Loan Review Report";
        }

        if (orderNewLqiLabelText) {
            return {
                NewReportLabelText: orderNewLqiLabelText,
                EnableBureauSelection: enableBureaus,
                ReportTypes: reportTypes,
                ReportTypeChangeHandler: reportTypeChangeHandler
            };
        }

        return undefined;
    }
    function getReissueLqiReportSettings(prot) {
        var reissueLqiLabelText, lqiReportIdLabel, missingReportIdMessage;
        if (prot.IsKrollFactualData) {
            reissueLqiLabelText = "Re-issue Loan Review Report";
            lqiReportIdLabel = "LRR Report ID";
            missingReportIdMessage = "Please enter the Report ID associated with the existing Loan Review Report.";
        }

        if (reissueLqiLabelText) {
            return {
                ReissueLqiLabelText: reissueLqiLabelText,
                LqiReportIdLabel: lqiReportIdLabel,
                MissingReportIdMessage: missingReportIdMessage
            };
        }
    }

    function getRemoveApplicantSettings(prot) {
        if (prot.IsIR) {
            return {};
        }
        return undefined;
    }
    function getOrderSupplementSettings(prot) {
        if (prot.IsIR) {
            return {};
        }
        return undefined;
    }
    function getRetrieveSupplementSettings(prot) {
        if (prot.IsIR) {
            return {};
        }
        return undefined;
    }
    function getRetrieveBillingReportSettings(prot) {
        if (prot.IsIR) {
            return {};
        }
        return undefined;
    }

            function getProtocolType(id)
            {
		          if (id == <%= AspxTools.JsString(Guid.Empty) %>)
		          {
		            return -1; <%-- Default value.--%>
		          }
		          var ret = creditHash[id];
		          if (ret == null)
		          {
		            return <%= AspxTools.JsNumeric(CreditReportProtocol.Mcl) %>;
		          }

		          return ret;
            }


            function f_enableLoginPasswordValidators(bEnabled)
            {
                if(document.getElementById('LoginPasswordPanel').style.display == "none")
                {
                    f_setEnabled(false, '<%= AspxTools.ClientId(LoginNameValidator) %>','<%= AspxTools.ClientId(PasswordValidator) %>');
                }
                else
                {
                    f_setEnabled(bEnabled, '<%= AspxTools.ClientId(LoginNameValidator) %>','<%= AspxTools.ClientId(PasswordValidator) %>');
                }
            }


            function f_enableAllValidators(bEnabled)
            {
		        f_enableBorrowerValidator(bEnabled);
		        f_enableDULogin(bEnabled);
		        f_enableAccountIdValidation(bEnabled);
		        f_enableLoginPasswordValidators(bEnabled);
            }


            function f_enableAccountIdValidation(bEnabled)
            {
		        if(document.getElementById('m_AccountIdentifierPanel').style.display == "none")
			        f_setEnabled(false, '<%= AspxTools.ClientId(AccountIdRequiredValidator) %>');
		        else
			        f_setEnabled(bEnabled, '<%= AspxTools.ClientId(AccountIdRequiredValidator) %>');
            }


            function f_enableDULogin(bEnabled)
            {
		        if(document.getElementById('FannieMaePanel').style.display == "none")
			        f_setEnabled(false, '<%= AspxTools.ClientId(DUUserIDValidator) %>','<%= AspxTools.ClientId(DUPasswordValidator) %>');
		        else
			        f_setEnabled(bEnabled, '<%= AspxTools.ClientId(DUUserIDValidator) %>','<%= AspxTools.ClientId(DUPasswordValidator) %>');
            }

    function f_hideCredentialAreaIfEmpty()
    {
        if(document.getElementById('m_AccountIdentifierPanel').style.display == "none"
            && document.getElementById('FannieMaePanel').style.display == "none"
            && document.getElementById('LoginPasswordPanel').style.display == "none")
        {
            document.getElementById('CreditLoginInformation').style.display = "none";
        }
        else
        {
            document.getElementById('CreditLoginInformation').style.display = "block";
        }
    }


            function f_enableBorrowerValidator(bEnabled)
            {
		        f_setEnabled(bEnabled, '<%= AspxTools.ClientId(aBFirstNmValidator) %>','<%= AspxTools.ClientId(aBLastNmValidator) %>','<%= AspxTools.ClientId(aBSsnValidator) %>','<%= AspxTools.ClientId(aBCityValidator) %>',
                           '<%= AspxTools.ClientId(aBStateValidator) %>','<%= AspxTools.ClientId(aBZipValidator) %>','<%= AspxTools.ClientId(aBAddrRequiredValidator) %>');
            }


            function f_enableBorrowerRequiredIcons(bEnabled)
            {
		        f_setEnabledBorrReqIcons(bEnabled, 'm_FirstNameReqIcon','m_LastNameReqIcon','m_SsnReqIcon','m_StreetAddReqIcon',
                           'm_CityReqIcon','m_StateReqIcon','m_ZipcodeReqIcon');
            }


            function f_enableLoginPassword(bEnabled)
            {
		        if((document.getElementById("<%= AspxTools.ClientId(m_iviewIDTF) %>").value.length != 0))
			        bEnabled = false;

		        var list = document.getElementById('<%= AspxTools.ClientId(m_loginName) %>','<%= AspxTools.ClientId(m_password) %>');

		        for (var i = 0; i < list.length; i++)
		        {
			        list[i].readOnly = !bEnabled;
		        }

		        if(bEnabled == true)
		        {
			        document.getElementById('m_loginReq').style.display = "";
			        document.getElementById('m_passwordReq').style.display = "";
		        }
		        else
		        {
			        document.getElementById('m_loginReq').style.display = "none";
			        document.getElementById('m_passwordReq').style.display = "none";
			        <%= AspxTools.JsGetElementById(m_loginName) %>.value = "";
			        <%= AspxTools.JsGetElementById(m_password) %>.value = "";
			        f_enableLoginPasswordValidators(false);
		        }
            }


            function f_setEnabledBorrReqIcons(bEnabled)
            {
		        if (arguments.length < 2) return;
		        for (var i = 1; i < arguments.length; i++)
		        {
			        if(bEnabled == true)
				        document.getElementById(arguments[i]).style.display = "";
			        else
				        document.getElementById(arguments[i]).style.display = "none";
		        }
            }


            function f_setEnabled(bEnabled)
            {
		        if (arguments.length < 2) return;
		        for (var i = 1; i < arguments.length; i++)
		        {
			        document.getElementById(arguments[i]).enabled = bEnabled;
		        }
            }

            function f_onCraChange()
            {
                var args = new Object();
				args["loanid"] = ML.sLId;
				args["craId"] = <%= AspxTools.JsGetElementById(CreditAgencyList) %>.value;

				var result = gService.loanedit.call("GetOrderCreditPermission", args);

                if (!result.error)
                {
                    canOrderCredit = result.value.CanOrderCredit == '1';
                    var reason = result.value.CanOrderCreditReason;
                    var warningIcon = document.getElementById('WarningIcon');

                    warningIcon.style.display = canOrderCredit ? 'none' : 'inline';
                    warningIcon.title = canOrderCredit ? '' : reason;
                }
                else
                {
                    var errMsg = 'Fail checking order credit.';
                    f_displayErrorMessage(errMsg);
                    return false;
                }

                f_refreshUI();
            }

            function f_oniViewIDkeyUp()
            {
		        var bEnabled = document.getElementById("<%= AspxTools.ClientId(m_iviewIDTF) %>").value.length == 0;
		        f_enableLoginPassword(bEnabled);
		        f_enableLoginPasswordValidators(bEnabled);
		        if ( (bEnabled == false) && typeof(ValidatorValidate) == 'function')
		        {
			        var loginValidator = document.getElementById('<%= AspxTools.ClientId(LoginNameValidator) %>');
			        var pwValidator = document.getElementById('<%= AspxTools.ClientId(PasswordValidator) %>');
			        ValidatorValidate(loginValidator);
			        ValidatorValidate(pwValidator);
		        }
            }

function f_onCSCToUpper()
{
    var oProtocol = f_getSelectedCraProtocol();
    var value = <%= AspxTools.JsGetElementById(CreditAgencyList) %>.value;

    <%-- 11/28/07 db - OPM 19004 For CSC, login and Report ID must be upper case --%>
    <%-- 11/13/09 db - OPM 23392 For Fannie Version of Equifax (004), login and Report ID must be upper case --%>
    if(oProtocol.IsCSC || value == "ae23b898-b527-4ebe-b139-830d11d6b4bd")
    {
      <%= AspxTools.JsGetElementById(m_loginName) %>.value = <%= AspxTools.JsGetElementById(m_loginName) %>.value.toUpperCase();
      <%= AspxTools.JsGetElementById(m_reportIDTF) %>.value = <%= AspxTools.JsGetElementById(m_reportIDTF) %>.value.toUpperCase();
    }
}


var g_reportId = '';

function f_onCreditClick()
{
    f_onCSCToUpper();
    var bValid = f_validateBeforeSubmit();

    if (bValid === false)
    {
        return;
    }

    if (<%= AspxTools.JsGetElementById(UseCreditCardPayment) %> && <%= AspxTools.JsGetElementById(UseCreditCardPayment) %>.checked)
    {
        showModal('/newlos/Services/LoginScreen.aspx', null, null, null, function(ret){
            if (!ret.OK)
            {
                return false;
            }
            else
            {
                document.forms[0]['BillingFirstName'].value = ret.BillingFirstName;
                document.forms[0]['BillingLastName'].value = ret.BillingLastName;
                document.forms[0]['BillingStreetAddress'].value = ret.BillingStreetAddress;
                document.forms[0]['BillingCity'].value = ret.BillingCity;
                document.forms[0]['BillingState'].value = ret.BillingState;
                document.forms[0]['BillingZipcode'].value = ret.BillingZipcode;
                document.forms[0]['BillingCardNumber'].value = ret.BillingCardNumber;
                document.forms[0]['BillingExpirationMonth'].value = ret.BillingExpirationMonth;
                document.forms[0]['BillingExpirationYear'].value = ret.BillingExpirationYear;
                document.forms[0]['BillingCVV'].value = ret.BillingCVV;
            }
         },{hideCloseButton: true});
    }

    g_reportId = <%= AspxTools.JsGetElementById(m_reportIDTF) %>.value;

    <%-- 6/20/2006 - nw - OPM 4455, strip out '-' in Report ID when reissuing Credco credit reports. --%>
    var oProtocol = f_getSelectedCraProtocol();
    var value = <%= AspxTools.JsGetElementById(CreditAgencyList) %>.value;
    if (oProtocol.IsCredco || value == "44bd6469-4330-4891-87c2-9ab82787d6b6")
    {
      g_reportId = g_reportId.replace(/\-/g, "");
    }

    setTimeout(function() { f_orderCredit(false); }, 100);
}


            function f_displayWait(bDisplay)
            {
                if (bDisplay) {
                    var showLoading = true;
                    var waitMessage = 'Ordering credit report. Please wait ...';
                    if (document.getElementById('orderSupplement').checked) {
                        waitMessage = 'Waiting for IR window to be closed to proceed.';
                        showLoading = false;
                    } else if (document.getElementById('retrieveSupplement').checked) {
                        waitMessage = 'Retrieving supplements. Please wait ...';
                    } else if (document.getElementById('retrieveBillingReport').checked) {
                        waitMessage = 'Retrieving the billing report. Please wait ...';
                    }

                    document.getElementById('WaitMessageLoadingImg').style.display = showLoading ? 'inline' : 'none';
                    var el = document.getElementById('WaitMessageTextSpan');
                    if (el.firstChild && el.firstChild.nodeType === 3 && el.firstChild.nodeValue !== waitMessage) {
                        el.firstChild.nodeValue = waitMessage;
                    }
                }

                var maskDiv = document.getElementById('MaskDiv');
                maskDiv.style.display = bDisplay ? '' : 'none';
                maskDiv.style.width = '100%';
                maskDiv.style.height = '100%';
                maskDiv.style.position = 'absolute';
                maskDiv.style.left = '0px';
                maskDiv.style.top = '0px';

                var waitMessage = document.getElementById('WaitMessage');
                waitMessage.style.display = bDisplay ? '' : 'none';
                waitMessage.style.width = '100%';
                waitMessage.style.height = '100%';
                waitMessage.style.position = 'absolute';
                waitMessage.style.left = '0px';
                waitMessage.style.top = '0px';
            }

            function showInstantView(show, isOrderNew)
            {
                //document.getElementById('ReportIdLabel').style.width = isOrderNew ? '114px' : '125px';

                document.getElementById('<%= AspxTools.ClientId(m_iviewIDTF) %>').className = show ? "MarginLeft" : "Hidden";
                document.getElementById('<%= AspxTools.ClientId(InstantViewLabel) %>').className = show ? "MarginLeft" : "Hidden";
            }

    function boolString(b) {return b ? "True" : "False";}
function f_orderCredit(ispoll)
{
    var comId = document.getElementById('CreditAgencyList').value;

    var args = getAllFormValues(null);
    args["loanname"] = <%= AspxTools.JsString(m_LoanName)%>; <%--OPM 15995 --%>

    args["comid"] = comId;
    args["OrderNewReport"] = boolString(<%= AspxTools.JsGetElementById(m_OrderNewReport) %>.checked);
    args["OrderUpgradeReport"] = boolString(<%= AspxTools.JsGetElementById(m_UpgradeReport) %>.checked);<%--OPM 12769 - jM--%>
    args["OrderLqiReport"] = boolString(<%= AspxTools.JsGetElementById(orderLqi) %>.checked);
    args["ReissueLqiReport"] = boolString(<%= AspxTools.JsGetElementById(reissueLqi)%>.checked);
    args["IsEquifax"] = boolString(ReportTypeData.orderNew.getIsEquifax());
    args["IsExperian"] = boolString(ReportTypeData.orderNew.getIsExperian());
    args["IsTransunion"] = boolString(ReportTypeData.orderNew.getIsTransUnion());
    args["ReportId"] = g_reportId;
    args["ActionType"] = "GET";

    var selectedReportType = null;
    if (<%= AspxTools.JsGetElementById(m_OrderNewReport) %>.checked  && !ispoll)
    {
        args["ActionType"] = "NEW";
        selectedReportType = ReportTypeData.orderNew;
    }
    else if (<%= AspxTools.JsGetElementById(m_UpgradeReport) %>.checked  && !ispoll)
    {
        args["ActionType"] = "UPGRADE";
        selectedReportType = ReportTypeData.upgrade;
    }
    else if (<%= AspxTools.JsGetElementById(m_RetrieveReport) %>.checked && !ispoll)
    {
        args["ActionType"] = "RETRIEVE";
        selectedReportType = ReportTypeData.retrieve;
    }
    else if (<%= AspxTools.JsGetElementById(orderLqi) %>.checked && !ispoll)
    {
        args["ActionType"] = "NEW_LQI";
        selectedReportType = ReportTypeData.orderLqi;
    }
    else if (<%= AspxTools.JsGetElementById(reissueLqi) %>.checked && !ispoll)
    {
        args["ActionType"] = "REISSUE_LQI";
        selectedReportType = ReportTypeData.reissueLqi;
    }
    else if (document.getElementById('removeApplicant').checked && !ispoll) {
        args['ActionType'] = document.getElementById('removeApplicantRemoveBorrower').checked ? 'REMOVE_BORROWER' : 'REMOVE_COBORROWER';
        selectedReportType = ReportTypeData.removeApplicant;
    } else if (document.getElementById('orderSupplement').checked && !ispoll) {
        beginOrderSupplement();
        return false;
    } else if (document.getElementById('retrieveSupplement').checked && !ispoll) {
        args['ActionType'] = 'RETRIEVE_SUPPLEMENT';
        selectedReportType = ReportTypeData.retrieveSupplement;
    } else if (document.getElementById('retrieveBillingReport').checked && !ispoll) {
        args['ActionType'] = 'RETRIEVE_BILLING_REPORT';
        selectedReportType = ReportTypeData.retrieveBillingReport;
    }

    if (selectedReportType) {
        args['ReportId'] = selectedReportType.getReportId() || g_reportId;
        args['CreditReportType'] = selectedReportType.reportTypeDropdown.getSelectedCreditReportType();

        if (typeof selectedReportType['getIsEquifax'] === 'function') {
            args["IsEquifax"] = boolString(selectedReportType.getIsEquifax());
            args["IsExperian"] = boolString(selectedReportType.getIsExperian());
            args["IsTransunion"] = boolString(selectedReportType.getIsTransUnion());
        }
    }

    args["InstantViewId"] = <%= AspxTools.JsGetElementById(m_iviewIDTF) %>.value;
    args["DUUserID"] = <%= AspxTools.JsGetElementById(DUUserID) %>.value;
    args["DUPassword"] = <%= AspxTools.JsGetElementById(DUPassword) %>.value;
    args["AccountIdentifier"] = <%= AspxTools.JsGetElementById(m_accountIdentifier) %>.value;
    args["LoginName"] = <%= AspxTools.JsGetElementById(m_loginName) %>.value;
    args["Password"] = <%= AspxTools.JsGetElementById(m_password) %>.value;
    args["RememberLogin"] = <%= AspxTools.JsGetElementById(m_rememberLoginCB) %>.checked ? "True" : "False";;
    args["IsImport0Balance"] = <%= AspxTools.JsGetElementById(m_isImport0BalanceCB) %>.checked ? "True" : "False";

    if (<%= AspxTools.JsGetElementById(UseCreditCardPayment) %>)
    {
        args["UseCreditCardPayment"] = <%= AspxTools.JsGetElementById(UseCreditCardPayment) %>.checked ? 'True' : 'False';
    }
    else
    {
        args["UseCreditCardPayment"] = 'False';
    }

    if (<%= AspxTools.JsGetElementById(m_isMortgageOnlyCB) %>)
    {
        args["IsMortgageOnly"] = <%= AspxTools.JsGetElementById(m_isMortgageOnlyCB) %>.checked ? 'True' : 'False';
    }
    else
    {
        args["IsMortgageOnly"] = 'False';
    }

    var oProtocol = f_getSelectedCraProtocol();
    if (oProtocol.IsMcl)
    {
        var reportType = <%= AspxTools.JsGetElementById(mclReportTypeDropdown) %>.value;
        args["MclReportType"] = reportType;
    }

    <%--3/29/2007 nw - OPM 10105 - let users know the CRA they want to pull or reissue credit is having technical difficulty --%>
    var warningResult = gService.loanedit.call("CheckCRAWarningMessage", args);
    if (!warningResult.error)
    {
        if ((null != warningResult.value["IsWarningOn"]) && warningResult.value["IsWarningOn"]) {
            var sWarningMsg = warningResult.value["WarningMsg"];
            var sWarningStartD = warningResult.value["WarningStartD"];
            var sURL = "/newlos/Services/DisplayWarningMessage.aspx?WarningMsg=" + encodeURIComponent(sWarningMsg) + "&WarningStartD=" + encodeURIComponent(sWarningStartD);
            showModal(sURL, null, null, null, function (ret) {
                if (ret.choice != 0)	<%-- if user clicks "Cancel" or closes the modal dialog window --%> {
                    f_displayWait(false);
                    f_refreshUI();
                    return false;
                }
                else {
                    OrderCredit(args);
                }
            }, { hideCloseButton: true });
        }
        else {
            OrderCredit(args);
        }
    }
    else
    {
        var errMsg = 'Error occurred while checking for CRA warning message.';
        f_displayErrorMessage(errMsg);
        return false;
    }
}

    function OrderCredit(args) {
        var loadingPopup = SimplePopups.CreateLoadingPopup("Processing Credit Report request...", "Credit Report request");
        LQBPopup.ShowElement($('<div>').append(loadingPopup), {
            width: 350,
            height: 200,
            elementClasses: 'FullWidthHeight',
            hideCloseButton: true
        });

        var creditOrderingMethod = ML.DisableCreditOrderingViaBJP ? "OrderCreditSynchronously" : "OrderCredit";
        gService.loanedit.callAsyncSimple(creditOrderingMethod, args, 
            function(result) {
                if(result.error) {
                    LQBPopup.Return(null)
                    var errMsg = <%= AspxTools.JsString(JsMessages.FailedToCompleteOperation) %>;
                    if (null != result.UserMessage)
                    {
                        errMsg = result.UserMessage;
                    }

                    f_displayErrorMessage(errMsg);
                }
                else if (result.value.Status === "PROCESSING" && !ML.DisableCreditOrderingViaBJP) {
                    var pollingIntervalInMilliseconds = parseInt(result.value.PollingIntervalInSeconds) * 1000;
                    var publicJobId = result.value.PublicJobId;
                    var attemptNumber = 1;
                    var chosenAppId = result.value.ChosenAppId;
                    var cacheKey = result.value.CacheKey;
                    window.setTimeout(function () { PollForRequestResults(publicJobId, pollingIntervalInMilliseconds, attemptNumber, chosenAppId, cacheKey, args); }, pollingIntervalInMilliseconds);
                }
                else {
                    LQBPopup.Return(null);
                    ResolveStatusAction(result);
                }
            });
    }

    function PollForRequestResults(publicJobId, pollingIntervalInMilliseconds, attemptCount, chosenAppId, cacheKey, previousArgs) {
        var args = previousArgs;
        args.PublicJobId = publicJobId;
        args.AttemptCount = attemptCount;
        args.ChosenAppId = chosenAppId;
        args.CacheKey = cacheKey;

        gService.loanedit.callAsyncBypassOverlay("PollForCreditReportRequestResults", args,
            function(result)
            {
                if(result.error) {
                    LQBPopup.Return(null);
                    var errMsg = <%= AspxTools.JsString(JsMessages.FailedToCompleteOperation) %>;
                    if (null != result.UserMessage)
                    {
                        errMsg = result.UserMessage;
                    }

                    f_displayErrorMessage(errMsg);
                }
                else if(result.value.Status == "PROCESSING") {
                    window.setTimeout(function () { PollForRequestResults(publicJobId, pollingIntervalInMilliseconds, ++attemptCount, chosenAppId, cacheKey, args) }, pollingIntervalInMilliseconds);
                }
                else {
                    LQBPopup.Return(null);
                    ResolveStatusAction(result);
                }
            });
    }

    function ResolveStatusAction(result) 
    {
        populateForm(result.value, null);
        switch (result.value["Status"])
        {
            case "READY":
                f_displayWait(false);
                f_creditReportReady(result.value);
                break;
            case "LOGIN_FAILED":
                f_displayErrorMessage(result.value["ErrorMessage"]);
                break;
            case "ERROR":
                f_displayErrorMessage(result.value["ErrorMessage"]);
                break;
            case "NOOP":
                f_displayWait(false);
                break;
            default:
                g_reportId = result.value["ReportID"];
                document.forms[0]['StatusQuery'].value = result.value["StatusQuery"] == null ? "" : result.value["StatusQuery"];
                f_refreshUI();
                setTimeout(function() { f_orderCredit( true ) } , 5000);
                break;
        }
    }

    function beginOrderSupplement() {
        var requestData = {
            'CraId': document.getElementById('CreditAgencyList').value,
            'ReportId': ReportTypeData.orderSupplement.getReportId()
        };
        gService.loanedit.callAsyncSimple('GetOrderSupplementUrl', requestData,
            function successCallback(result) {
                if (result.value['Success'] !== 'True') {
                    f_displayErrorMessage(result.value['ErrorMessage']);
                    return;
                }
                var popup = window.open(result.value['url'], 'OrderCreditSupplement');
                var timer = setInterval(function() {
                    if (!popup || popup.closed) {
                        clearInterval(timer);
                        f_displayWait(false);
                    }
                }, 500);
            }, function errorCallback(result) {
                f_displayErrorMessage(result.UserMessage || <%= AspxTools.JsString(JsMessages.FailedToCompleteOperation) %>);
            });
    }

            function f_creditReportReady(args)
            {
                var isNonCreditReport = args['IsNonCreditReport'] === 'True';
                var isLqiReport = <%= AspxTools.JsGetElementById(orderLqi) %>.checked || <%=AspxTools.JsGetElementById(reissueLqi)%>.checked;
                if (!isLqiReport && !isNonCreditReport) {
                    <%= AspxTools.JsGetElementById(m_reportIDTF) %>.value = args["ReportID"];<%-- We need a better solution - this gets overwritten in f_refreshUI -> onReportTypeClick --%>
                    <%= AspxTools.JsGetElementById(m_ReorderReport) %>.value = true;
                }

                f_refreshUI();
                if (isNonCreditReport) {
                    var message = 'Response received from vendor:\n' + args['NonCreditReportStatusDescription'];
                    if (args['NonCreditReportDocumentUploadStatus']) {
                        message += '\n\n' + args['NonCreditReportDocumentUploadStatus'];
                    }
                    alert(message);
                    return;
                }
                
                var reportType = isLqiReport ? "1" : "0";

                if (args["HasExistingLiabilities"] == "True"){
                    showModal("/newlos/Services/ExistingLiabilitiesConfirmation.aspx", null, null, null, function(ret){
                        var shouldImportLiability = ret.choice == 0 || ret.choice == 1;
                        if (shouldImportLiability){
                            var o = new Object();
                            o["loanid"] = ML.sLId;
                            o["applicationid"] = ML.aAppId;
                            o["IsImport0Balance"] = <%= AspxTools.JsGetElementById(m_isImport0BalanceCB) %>.checked ? "True" : "False";
                            o["DeleteExistingLiabilities"] = ret.choice == 0 ? "True" : "False";

                            var result = gService.loanedit.call("ImportLiabilities", o);
                        }
                        
                        f_displayCreditReport(reportType);
                    },{hideCloseButton: true});
                    return;
                }

                f_displayCreditReport(reportType);
            }

	        function ValidateCityState()
	        {
		        ValidateStateDD();
		        var cityValidator = <%= AspxTools.JsGetElementById(aBCityValidator) %>;
		        if (cityValidator.enabled == true)
		        {
			        if (typeof(ValidatorValidate) == 'function')
				        ValidatorValidate(cityValidator);
		        }
	        }


          function ValidateStateDD()
	        {
		        var stateValidator = <%= AspxTools.JsGetElementById(aBStateValidator) %>;
		        if (stateValidator.enabled == true)
		        {
			        if (typeof(ValidatorValidate) == 'function')
				        ValidatorValidate(stateValidator);
		        }
	        }


	        function ValidateSsnTB()
	        {
		        var ssnValidator = <%= AspxTools.JsGetElementById(aBSsnValidator) %>;
		        if (ssnValidator.enabled == true)
		        {
			        if (typeof(ValidatorValidate) == 'function')
				        ValidatorValidate(ssnValidator);
		        }
	        }

            function f_displayErrorMessage(msg)
            {
                f_displayWait(false);

                var errorPopup = SimplePopups.CreateErrorPopup(msg, /* errors */ null, /* header */ '');
                LQBPopup.ShowElement($('<div>').append(errorPopup), {
                    width: 500,
                    height: 300,
                    hideCloseButton: true,
                    elementClasses: 'FullWidthHeight'
                });
            }

            function f_displayCreditReport(type)
            {
                var reportUrl = ML.VirtualRoot + '/newlos/services/ViewCreditFrame.aspx?loanid=' + ML.sLId + '&applicationid=' + ML.aAppId + '&type=' + type;

		        var o = openWindowWithArguments(reportUrl, "creditreport", "scrollbars=yes,toolbar=no,location=no,titlebar=no,menubar=no,resizable=yes");
		        o.focus();
		        window.close();
            }


	        function f_prePostBack()
	        {
		        if ( <%= AspxTools.JsBool(m_hasLiabilities) %>) {
			        showModal("/newlos/Services/ExistingLiabilitiesConfirmation.aspx", null, null, null, function(ret){
                        if (ret.choice == 0 || ret.choice == 1)
                        {
                            if (ret.choice == 0) document.forms[0]["DeleteLiabilities"].value = "1";
                            else document.forms[0]["DeleteLiabilities"].value = "0";
                            return true;
                        }
                        else
                            return false;
                     },{hideCloseButton: true});
		        }
		        return true;
	        }

	        function f_ToUpper(tb)
	        {
		          var oProtocol = f_getSelectedCraProtocol();
		          <%-- 11/28/07 db - OPM 19004 For CSC, login and Report ID must be upper case --%>
		          if(oProtocol.IsCSC)
			          tb.value = tb.value.toUpperCase();
	        }

        </script>
    </body>
</html>
