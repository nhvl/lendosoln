﻿namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.IO;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Security;

    public partial class ComplianceEaseReport : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Guid sLId = RequestHelper.GetGuid("loanid", Guid.Empty);

            if (sLId == Guid.Empty)
            {
                RenderNotFound();
            }
            else
            {
                // Render PDF
                try
                {
                    byte[] buffer;
                    if (RequestHelper.GetBool("useEdocs"))
                    {
                        var edoc = EDocs.EDocument.GetDocsAvailableForDownload(BrokerUserPrincipal.CurrentPrincipal, sLId)
                               .Where((a) => a.DocTypeName.Equals("COMPLIANCEREPORT"))
                               .OrderByDescending((a) => a.LastModifiedDate)  // 2/16/2016 - dd - Just in case there are more than 1 edocs, use the latest one.
                               .FirstOrDefault();
                        if (edoc == null)
                        {
                            RenderNotFound();
                            return;
                        }

                        buffer = BinaryFileHelper.ReadAllBytes(edoc.GetPDFTempFile_Current());
                    }
                    else
                    {
                        buffer = FileDBTools.ReadData(E_FileDB.Normal, sLId.ToString("N") + "_COMPLIANCE_EASE_REPORT_PDF");
                    }

                    Response.Clear();
                    Response.Cookies.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"ComplianceEase.pdf\"");
                    Response.OutputStream.Write(buffer, 0, buffer.Length);
                    Response.Flush();
                    Response.End();
                }
                catch (FileNotFoundException)
                {
                    RenderNotFound();
                }
            }
        }

        private void RenderNotFound()
        {
            m_literal.Text = "ComplianceEase report does not exist.";
        }
    }
}
