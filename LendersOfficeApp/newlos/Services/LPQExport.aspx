﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LPQExport.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.LPQExport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>LoansPQ Export</title>
    <style type="text/css">
        body { background-color: gainsboro; }
        #LoginForm,#DownloadProgress { font-weight: bold; line-height: 2;  }
        label { display: block; width: 70px; float: left; margin-left: 5px; }
        br  { clear: both; }
        .saveUpdateInfo label { display: inline; float: none; }
        #DownloadProgress { padding: 5px; }
        legend { font-weight: bold; }
        fieldset { margin: 5px; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
     <div class="MainRightHeader">LPQ Export</div>
    
    <asp:PlaceHolder runat="server" ID="CannotExportPanel">
        This loan has already been exported to LPQ with loan number <ml:EncodedLiteral runat="server" ID="lpqnum"></ml:EncodedLiteral>. To create a new LPQ loan click <asp:LinkButton runat="server" ID="ClearLPQId2" OnClick="ClearLPQId_OnClick">here</asp:LinkButton>.
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="FormLogin">
       <div id="LoginForm" >
        <fieldset >
            <legend>LPQ Member Numbers</legend>
        <div id="MemberNumberForm" >
            <asp:Repeater runat="server" OnItemDataBound="MemberNumbers_OnItemDataBound" ID="MemberNumbers">
                <HeaderTemplate>
                    <table id="memberNumberTable">
                        <thead>
                            <tr>
                                <th>Application</th>
                                <th>Member Number</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                       <tr>
                            <td>
                                <ml:EncodedLiteral runat="server" ID="BorrowerNm"></ml:EncodedLiteral>
                                <input class="aAppId" type="hidden" runat="server" id="AppId" />
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="MemberNumber" CssClass="memberNumber"></asp:TextBox>
                            </td>
                       </tr>
                </ItemTemplate>
                <FooterTemplate>
                        </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        </fieldset>
        <br />
        <fieldset>
            <legend>LPQ Login Information</legend>
     
            <ml:EncodedLabel runat="server" AssociatedControlID="Username">Username:</ml:EncodedLabel>
            <asp:TextBox runat="server" ID="Username" class="username"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="UsernameIsRequired" ControlToValidate="Username"></asp:RequiredFieldValidator>
            <br />
            <ml:EncodedLabel runat="server" AssociatedControlID="Password">Password:</ml:EncodedLabel>
            <asp:TextBox runat="server" ID="Password" TextMode="Password" class="password"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="PasswordIsRequired" ControlToValidate="Password"></asp:RequiredFieldValidator>
            <br />
            <asp:CheckBox runat="server" ID="SaveUpdateInfo" class="saveUpdateInfo" Text="Remember login information" />
            <br />
          <input type="button" value="Export" id="Submit" />
          
        </fieldset>
        </div>
        
        <script type="text/javascript">
                $(function(){
                    var dp = $('#DownloadProgress').hide();
                    var lf = $('#LoginForm');
                    $(document).ajaxStart(function(){
                        dp.show();
                        lf.hide();
                    });
                    $(document).ajaxStop(function(){
                        dp.hide();
                        lf.show();
                    });
                    
                    Page_ClientValidate("");
                    $('#Submit').click(function(){
                        var isValid = Page_ClientValidate("");
                        if( isValid == false ){
                            alert('Username and password is required.');
                            return;
                        }
                        var appInfo = [];
                        
                        $('#memberNumberTable tbody tr').each(function(i,o){
                            var appId = $("input.aAppId", this).val();
                            var memberNumber = $("input.memberNumber", this).val();
                            
                            appInfo.push({
                                aAppId : appId,
                                appMemberNumber : memberNumber
                            });
                        });
                        
                        var data = {
                            username : $('input.username').val(),
                            password : $('input.password').val(),
                            updatePassword : $('.saveUpdateInfo input').is(':checked'),
                            loanid : ML.sLId, 
                            memberAppInfo : appInfo
                        };
                        
                        var jsonData = JSON.stringify(data);
                        
                        callWebMethodAsync({
                            type: 'POST',
                            url: 'LPQExport.aspx/Export',
                            contentType: "application/json; charset=utf-8",
                            data: jsonData,
                            dataType: 'json' 
                        }).then(
                            function(msg){
                                if (msg.d.Error) {
                                    alert( 'There was a problem with your request. ' + msg.d.Error );
                                }
                                window.location.reload();
                            },
                            function(e){
                                var msg = 'There was an error with your request.';
                                if (e.responseText) {
                                    var data = JSON.parse(e.responseText);
                                    if( data.Message ){
                                        msg += " Message: " + data.Message; 
                                    }
                                }
                                alert(msg);
                                window.location.reload();
                            }
                        );        
                        
                    });
                });
            </script>
        
        <div  id="DownloadProgress">
            Please wait while we export your loan to LoansPQ
            
            <br />
            <img src="../../images/status.gif" alt="Loading" />
        </div>
        
    </asp:PlaceHolder>
    </form>
</body>
</html>
