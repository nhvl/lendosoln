<%@ Page Language="c#" CodeBehind="ViewLPFeedbackMenu.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Services.ViewLPFeedbackMenu" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<html>
<head>
  <title>View LP Feedback Menu</title>
</head>
<body>
  <script type="text/javascript">
function f_print() {
    lqbPrintByFrame(parent.frames['MainFrame']);
}
function f_rbClick(rb) {
  parent.body.location = 'ViewLPFeedback.aspx?loanid=' + <%=AspxTools.JsString(RequestHelper.GetGuid("loanid")) %> + '&type=' + encodeURIComponent(rb.value);
}
function f_viewLpDirectly() {
  parent.body.location = '../LoanProspectorMain.aspx?loanid=' + <%=AspxTools.JsString(RequestHelper.GetGuid("loanid"))%> + '&entrypoint=' + <%=AspxTools.JsString(ConstAppDavid.LoanProspector_EntryPoint_ViewFeedback) %>;
}
  </script>

  <form id="ViewLPFeedbackMenu" method="post" runat="server">
  <table border="0" cellspacing="0" cellpadding="5" width="100%">
    <tr>
        <td valign="top">
          <input type="radio" name="type" value="feedback" id="rbFeedback" checked onclick="f_rbClick(this);" />
          <label for="rbFeedback">Full Feedback</label>
          <input type="radio" name="type" value="checklist" id="rbChecklist" onclick="f_rbClick(this);" />
          <label for="rbChecklist">Checklist</label>
          <input type="radio" name="type" value="merged" id="rbMerged" onclick="f_rbClick(this);" />
          <label for="rbMerged">Merged Credit</label>
          <input type="radio" name="type" value="infile" id="rbInfile" onclick="f_rbClick(this);" />
          <label for="rbInfile">Credit Infile</label>
          <input type="radio" name="type" value="viewlp" id="rbViewLp" onclick="f_viewLpDirectly();" />
          <label for="rbViewLp">View Feedback in Loan Product Advisor</label>
        </td>
        <td align="right" valign="top">
          <input type="button" onclick="f_print();" value="Print" />
          <input type="button" onclick="parent.onClosePopup()" value="Close" />
      </td>
    </tr>
  </table>
  </form>
</body>
</html>
