﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Integration.Appraisals;
//using LendersOffice.ObjLib.Conversions.GlobalDMS;
using LendersOffice.ObjLib.Conversions.LQBAppraisal;
//using LendersOffice.Security;
//using LendersOfficeApp.los.admin;
//using LQBAppraisal.LQBAppraisalRequest;

namespace LendersOfficeApp.newlos.Services
{
    public partial class OrderAppraisalAttachDocs : BaseLoanPage
    {
        protected string VendorID
        {
            get { return RequestHelper.GetSafeQueryString("vendorID"); }
        }
        protected string OrderNumber
        {
            get { return RequestHelper.GetSafeQueryString("orderNumber"); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterService("OrderAppraisal", "/newlos/services/OrderAppraisalService.aspx");

            var vendor = AppraisalVendorConfig.Retrieve(new Guid(VendorID));
            string script = string.Format("var usingGlobalDMS = '{0}';",vendor.UsesGlobalDMS);
            ClientScript.RegisterStartupScript(typeof(OrderAppraisalAttachDocs), "UISettings", script, true);

            VendorId.Value = VendorID;
            AppraisalOrderNumber.Value = OrderNumber;
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }
    }
}
