﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="DataVerifyDRIVE.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.DataVerifyDRIVE" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Conversions.Drive" %>
<%@ Import Namespace="LendersOffice.Conversions.Drive.Response" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html>
<head runat="server">
    <title>DataVerify DRIVE</title>
    <style type="text/css">
        td { font-weight: bold; padding-bottom: 4px; }
        body { margin-left: 0px; }
        .Hidden { display: none; }
        #WaitingPanel div {
          width:225px;
          padding:10px,20px,10px,20px;
          margin-top:20px;
          margin-left:20px;
          margin-right:20px;
          border-color:Black;
          border-width:medium;
          border-style:solid;
        } 
        .results-title {
          background-color:#5778ab; 
          color:white; 
          font-weight:bold; 
          font-family: verdana,arial,helvetica,sans-serif;
          font-size: 11px; 
          padding-left: 5px;
        }       
    </style>
</head>
<body class="RightBackground">
    <script type="text/javascript">
        function addContactsForWatchlist() {
            // just redirect to agents page for now.
            parent.frames['treeview'].load('Status/Agents.aspx');
        }
        function onSubmit() {
            document.getElementById('WaitingPanel').style.display = 'block';
            return true;
        }
    </script>
    <form id="DataVerifyDRIVE" method="post" runat="server">
        <asp:HiddenField ID="EncodedEDoc" runat="server" />
      <table cellspacing="0" cellpadding="0" width="100%" border="0">
  <tbody>
    <tr>
        <td width="66px"></td>
        <td width="250px"></td>
        <td></td>
    </tr>
    <tr>
      <td class="MainRightHeader" colspan=3>DataVerify DRIVE</td>        
    </tr>
    <tr>
       <td colspan=3>
        <a href="#" onclick="addContactsForWatchlist(); return false;">Add Contacts for watchlist comparison</a>
       </td>
    </tr>
    <tr>
        <td>User Name</td>
        <td><asp:TextBox ID="m_tbUserName" runat="server" MaxLength="50" Width="250px"></asp:TextBox></td>
    </tr>
    <tr>
        <td>Password</td>
        <td><asp:TextBox TextMode="Password" ID="m_tbPassword" runat="server" MaxLength="20" Width=250px></asp:TextBox></td>
    </tr>
    <tr>
        <td colspan=2>
            <asp:CheckBox ID="m_cbRememberLogon" runat="server" /> Remember Logon Credentials
        </td>
    </tr>
    <tr>
        <td colspan=3>
            <asp:Button ID="m_bSubmit" runat="server" Text="Submit to DRIVE"  OnClientClick="return onSubmit();" OnClick="OnSubmitClick"/>
        </td>
    </tr>

    
    </tbody>
  </table>
  <div id="WaitingPanel" style="display:none">
    <div>
      <span style="font-weight: bold; font-size: 12px">Please wait ... order may take up to <ml:EncodedLiteral runat="server" ID="timeout"></ml:EncodedLiteral> minute(s) to complete</span>
      <br /><br />
      <img id="Img1" src="~/images/status.gif" runat="server" />
    </div>
  </div>
  <asp:PlaceHolder ID="m_phNoResponse" runat="server" Visible="false">
      <div>
        <span style="font-weight: bold; font-size: 12px; color:Red">No response from server.  Please try again later.</span>
      </div>
  </asp:PlaceHolder>
  <asp:PlaceHolder ID="m_phNoLoanList" runat="server" Visible="false">
      <div>
        <span style="font-weight: bold; font-size: 12px; color:Red">Please try filling in more loan info before proceeding.</span>
      </div>
  </asp:PlaceHolder>
  <asp:PlaceHolder ID="m_phNullResponse" runat="server" Visible="false">
      <div>
        <span style="font-weight: bold; font-size: 12px; color:Red">DRIVE returned an unusual response.  Please try again and contact us if the issue continues.</span>
      </div>
  </asp:PlaceHolder>
  <asp:PlaceHolder ID="m_phErrors" runat="server" Visible="false">
    <div id="errorDiv" style="display:none">
        <table width="100%" cellpadding="10px" cellspacing="0" border=0>
            <tbody style="margin-bottom:10px" >
                <tr>
                    <td></td>
                    <td ></td>
                </tr>
                <asp:Repeater ID="m_rErrors" runat="server">
                    <ItemTemplate>
                        <tr class="errorRow">
                            <td ><%# AspxTools.HtmlString( ((Error)Container.DataItem).ErrorCode)  %></td>
                            <td class="secondCell" ><%# AspxTools.HtmlString( ((Error)Container.DataItem).ErrorDesc)  %></td>
                        </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <tr class="altErrorRow">
                            <td ><%# AspxTools.HtmlString( ((Error)Container.DataItem).ErrorCode)  %></td>
                            <td class="secondCell"><%# AspxTools.HtmlString( ((Error)Container.DataItem).ErrorDesc)  %></td>
                        </tr>
                    </AlternatingItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>        
    </div>
    <script type="text/javascript">
    
        $(function() {
            gVirtualRoot = document.getElementById('VirtualRoot') == null ? '' : document.getElementById('VirtualRoot').value;
            var w = window.open(gVirtualRoot + '/newlos/Services/DataVerifyDRIVEErrorSummary.aspx?loanid='+<%= AspxTools.JsString(LoanID)%>);
        });

    </script>
  </asp:PlaceHolder>
  <table width="100%">
  <tr><td colspan=3 class="results-title">Results</td></tr>
  <tr><td></td><td></td><td></td></tr>
  <tr><td width="20px"></td><td><img src="../../images/DRIVE_logo.png" alt="DRIVE logo" /></td><td></td></tr>
  <tr><td></td><td id="responseTD">
  <asp:PlaceHolder ID="m_phNoResponseOnFile" runat="server" Visible="false">
  <span style="color:Red">The Drive report has not been pulled yet.</span>
  </asp:PlaceHolder>
  <asp:Placeholder ID="m_phResults" runat="server" Visible="false">
  </asp:Placeholder>
  <asp:PlaceHolder ID="m_finishUploadEDoc" runat="server" Visible="false">
    <script type="text/javascript">
      $(function()
        {
            if ($("#EncodedEDoc").val() === "")
            {
                alert("WARNING: 0 documents have been uploaded to eDocs. \nPlease contact LQB Integrations Support at integrations@lendingqb.com if you continue to experience this warning.");
            }
            else 
            {
                alert("1 document has been successfully uploaded to eDocs.");
            }
      });
    </script>
  </asp:PlaceHolder>
  </td><td></td></tr>
  </table>
  
  <asp:PlaceHolder ID="m_phPickDocType" runat="server" Visible="false">
    <asp:HiddenField ID="m_selectedDriveDocType" runat="server" />
    <script type="text/javascript">
        function selectDriveDocType() {
            var docTypePickerLink = "/newlos/ElectronicDocs/DocTypePicker.aspx";
            var queryString = "?showSystemFolders=false&brokerid="+<%= AspxTools.JsString(BrokerID) %>;
            queryString += "&isDrive=true";
            showModal(docTypePickerLink + queryString, null, null, null, function(result){
                if (result && result.docTypeId && result.docTypeName) { // New doc type selected
                    <%= AspxTools.JsGetElementById(m_selectedDriveDocType)%>.value = result.docTypeId;
                    selectDriveDocTypeCallback(result.docTypeId);
                }
                else {
                    selectDriveDocTypeCallback(false);
                }
            },{hideCloseButton: true});
        }

        function selectDriveDocTypeCallback(id) {
            if(id === false)
            {
                return;
            }
                
            var dataObj = {
                EncodedEDoc:<%=AspxTools.JsGetElementById(EncodedEDoc) %>.value,
                docTypeID: id,
                brokerID: <%= AspxTools.JsString(BrokerID) %>,
                    loanID:<%=AspxTools.JsString(LoanID) %>,
                appID:<%=AspxTools.JsString(m_dataLoan.GetAppData(0).aAppId)%>
                };
            var methodName = 'TriggerUpload';
            // don't use $.post() as it leads to extra errors (see FF web developer console, not Firebug)
            $('#WaitingPanel').toggle();
            callWebMethodAsync({
                url: ML.VirtualRoot + '/newlos/Services/DataVerifyDrive.aspx/'+methodName,
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(dataObj)
            }).then(
                function() {
                    $('#WaitingPanel').toggle();
                    //  alert('successfully uploaded to EDocs');
                },
                function(xhr, sets, thrownError) {
                    alert('error occurred during the uploading to EDocs' + xhr.responseText);
                }
            );
        }

        $(function()
        {
            var resp = confirm("Would you like to select a doctype to upload the drive report to?");
            if (resp) {
                selectDriveDocType();              
            }
        });
    </script>
    
  </asp:PlaceHolder>
    </form>
</body>
</html>
