﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderAppraisalEmailMessage.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.OrderAppraisalEmailMessage" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Send Email</title>
    <link href="../../css/stylesheet.css" rel="stylesheet" type="text/css" />
<style type="text/css">
#EmailContent
{
    margin-left: 40px;
}
#SubjectText
{
    width: 500px;
}
#MessageText
{
    overflow-y: scroll;
    width: 600px;
    height: 380px;
}
#BtnContainer
{
    text-align: center;
}
.infoLine
{
    display:block;
    margin-top:10px;
}
td
{
    text-align: left;
}
.messageRow
{
    margin-bottom: 0px;
}
.infoLabel
{
    font-weight: bold;
    width: 75px;
}
.infoValue
{
    width: 225px;
}
</style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField id="VendorId" runat="server" />
    <asp:HiddenField id="AppraisalOrderNumber" runat="server" />
    <asp:HiddenField ID="ErrorMessage" runat="server" />
    <div>
        <h2 class="MainRightHeader"><ml:EncodedLiteral ID="PageName" runat="server"></ml:EncodedLiteral></h2>
        <table id="EmailContent">
            <tbody>
                <tr id="ReceivedEmailFromRow" runat="server">
                    <td class="infoLabel">From</td>
                    <td class="infoValue"><ml:EncodedLiteral ID="ReceivedFromField" runat="server"></ml:EncodedLiteral></td>
                    <td class="infoLabel">Date</td>
                    <td class="infoValue"><ml:EncodedLiteral ID="ReceivedDateField" runat="server"></ml:EncodedLiteral></td>
                </tr>
                <tr>
                    <td class="infoLabel">To</td>
                    <td class="infoValue">
                        <ml:EncodedLiteral ID="ReceivedToField" runat="server"></ml:EncodedLiteral>
                        <asp:DropDownList ID="SendEmailTo" runat="server"></asp:DropDownList>
                    </td>
                    <td class="infoLabel" style="display:none;"></td>
                    <td class="infoValue" style="display:none;"></td>
                </tr>
                <tr>
                    <td class="infoLabel">Subject</td>
                    <td class="infoValue"><ml:EncodedLiteral ID="ReceivedSubjectField" runat="server"></ml:EncodedLiteral><asp:TextBox id="SubjectText" TextMode="SingleLine" runat="server"></asp:TextBox></td>
                    <td class="infoLabel" style="display:none;"></td>
                    <td class="infoValue" style="display:none;"></td>
                </tr>
                <tr class="messageRow">
                    <td class="infoLabel" colspan="4">Message</td>
                </tr>
                <tr class="messageRow">
                    <td colspan="4"><asp:TextBox id="MessageText" TextMode="MultiLine" Wrap="true" runat="server"></asp:TextBox></td>
                </tr>
            </tbody>
        </table>
        <div id="BtnContainer">
                <input type="button" id="SendBtn" value="Send" runat="server" />
                <input type="button" id="ExitBtn" value="Cancel" onclick="onClosePopup()" runat="server" />
        </div>
    </div>
    </form>
</body>
<script type="text/javascript">
    $(function() {
        if ($.trim($('#ErrorMessage').val()) != '') {
            alert($('#ErrorMessage').val());
        }
    });
    $(window).on("load", function() {
        $('#SendBtn').prop('disabled', true);
    });

    $('#SubjectText').keyup(function() {
        $('#SendBtn').prop('disabled', !($('#SubjectText').val() !== '' && $('#MessageText').val() !== ''));
    });
    $('#MessageText').keyup(function() {
        $('#SendBtn').prop('disabled', !($('#SubjectText').val() !== '' && $('#MessageText').val() !== ''));
    });
    $('#SendBtn').click(function() {
        var args = {
            VendorId: $('#VendorId').val(),
            AppraisalOrderNumber: $('#AppraisalOrderNumber').val(),
            ToField: $('#SendEmailTo').val(),
            SubjectField: $('#SubjectText').val(),
            MessageField: $('#MessageText').val()
        }, result;
        result = gService.OrderAppraisal.call("SendEmail", args);
        var showGenericErrorMessage = true;
        if (result && result.value && result.value.ErrorMessage) {
            alert(result.value.ErrorMessage);
            showGenericErrorMessage = false;
        }
        if (result && result.value && result.value.Success && result.value.Success === "True") {
            onClosePopup();
        }
        else if (showGenericErrorMessage) {
            alert("Sending message failed.");
        }
    });
</script>
</html>
