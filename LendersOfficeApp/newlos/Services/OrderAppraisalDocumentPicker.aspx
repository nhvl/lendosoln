﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderAppraisalDocumentPicker.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.OrderAppraisalDocumentPicker" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="EDocs" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>EDocument Selector</title>
    <style type="text/css">
        div.scrollable {
            height: 400px;
            overflow-y: scroll;
            width:100%;
        }
        
        div.buttonPanel  {
            text-align: center;
            margin-top: 10px;
        }
        th { color: Blue; text-decoration: underline; cursor: pointer; }
        th.none { text-decoration: none; cursor: auto; }
    </style>
</head>
<body class="EditBackground">
    <h4 class="page-header">Select documents to add to this order</h4>
    <form id="form1" runat="server">
        <label>Search <input type="text" name="Query" id="Query" /> </label>
        <label>Folder <asp:DropDownList runat="server" ID="DocTypeFilter"></asp:DropDownList></label>
    <div class="scrollable">
        <div style="overflow-x:hidden">
        <asp:Repeater runat="server" ID="DocListing"> 
            
            <HeaderTemplate>
                <table id="DocTable" width="100%">
                    <thead>
                        <tr class="GridHeader"> 
                            <th class="none"><input type="checkbox" name="selectAll" id="selectAll" /></th>
                            <th >Folder</th>
                            <th>DocType</th>
                            <th>Description</th>
                            <th>Comments</th>
                            <th>Last Modified</th>
                            <th class="none">&nbsp;</td>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <input type="checkbox" data-docid='<%# AspxTools.HtmlString( ((EDocument)Container.DataItem).DocumentId.ToString())  %>'
                        data-docname='<%# AspxTools.HtmlString(((EDocument)Container.DataItem).Folder.FolderNm) %> : <%# AspxTools.HtmlString(((EDocument)Container.DataItem).DocTypeName) %>'  class="picked" />
                    </td>
                    <td>
                        <%# AspxTools.HtmlString( ((EDocument)Container.DataItem).Folder.FolderNm ) %>
                    </td>
                    <td>
                        <%# AspxTools.HtmlString( ((EDocument)Container.DataItem).DocTypeName ) %>
                    </td>
                    <td>
                        <%# AspxTools.HtmlString( ((EDocument)Container.DataItem).PublicDescription ) %>
                    </td>
                    <td>
                        <%# AspxTools.HtmlString( ((EDocument)Container.DataItem).InternalDescription ) %>
                    </td>
                    <td>
                        <%# AspxTools.HtmlString( ((EDocument)Container.DataItem).LastModifiedDate.ToString("M/d/yy h:mm tt")) %>
                    </td>
                    <td>
                        <a href="#" data-docid='<%# AspxTools.HtmlString( ((EDocument)Container.DataItem).DocumentId.ToString() )%>' class="view"  >view</a>
                    </td>
                </tr> 
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        </div>
    </div>
        <div class="buttonPanel">
            <input type="button" value=" OK "  id="OkBtn"/> 
            <input type="button" value="Cancel" id="CancelBtn" />
        </div>

        </form>
    <script type="text/javascript">
        $(function($){
            var rows = $('#DocTable tbody tr'), data = [], $query = $('#Query'), timeoutID = null, 
                str = null, $o, $tds, isAlt = false, cbx = $('input.picked'), $filter = $('#DocTypeFilter');
            
            resizeForIE6And7(600,500);
            var selected = [];
            var args = getModalArgs();
            if (args && args.SelectedDocsIds){
                selected = args.SelectedDocsIds;
            }
            rows.each( function(i,o){
                $o = $(o); 
                $tds = $o.children('td'); 
                if ($.inArray($tds.eq(0).find('input.picked').data('docid'), selected)> -1) {
                    $tds.eq(0).find('input.picked').prop('checked', true);
                }
                str = $tds.eq(2).text().toLowerCase() + '~' + $tds.eq(3).text().toLowerCase() + '~' + $tds.eq(4).text().toLowerCase(); 
                data.push(str);
                $o.addClass(isAlt ? 'GridAlternatingItem' : 'GridItem');
                isAlt = !isAlt;
            });
            
            rows = $('#DocTable tbody tr');
            
            $('#DocTable').on('click', 'a.view', function(){
                var docid= $(this).data('docid');
                window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + docid , '_parent');
                return false;
            }).tablesorter({
                headers : { 0: { sorter: false}, 6: {sorter: false} }
            }).on("sortEnd", function(){
                var alt = false;
                $('#DocTable tbody tr').each(function(i,o){
                    if ( alt ) { $(o).removeClass('GridItem').addClass('GridAlternatingItem');}
                    else { $(o).removeClass('GridAlternatingItem').addClass('GridItem'); }
                    alt = !alt;
                });
            });
            
            function searchTable() {
                var value = $query.val(), i = 0, rowCount = data.length, row; 
                if (value.length == 0) {
                    rows.show();
                    return;
                }   
                
                value = value.toLowerCase();
                
                for (i; i < rowCount; i++) {
                    row = data[i];
                    if (row.indexOf(value) == -1) {
                        rows.eq(i).hide();
                    }
                    else {
                        rows.eq(i).show();
                    }
                }
            }
            
            $query.keyup (function(){
                if (timeoutID) {
                    clearTimeout(timeoutID);
                }
                
                timeoutID = setTimeout(searchTable, 500); 
            });
            
            $('#selectAll').change(function(){
                var $o = $(this);
                
                if ($o.is(':checked')) {
                    cbx.filter(':visible').prop('checked', true);
                }
                else {
                    cbx.prop('checked', false);
                }
            });
            
            $('#CancelBtn').click(function(){
                onClosePopup();
            });
            
            $('#OkBtn').click(function(){
                var docIds = [];
                var docNames = [];
                cbx.filter(':checked').each(function(){
                    docIds.push($(this).data('docid'));
                    docNames.push($(this).data('docname'));
                });
                var arg = {};
                arg.DocIds = docIds;
                arg.DocNames = docNames;
                onClosePopup(arg);
            });
            
            function filterStatus() {
                var value = $filter.val(), i = 0, rowCount = data.length, rowType; 
                if (value == 'All') {
                    rows.show();
                    return;
                }
                for (i; i < rowCount; i++) {
                    rowType = rows.eq(i).children('td').eq(1).text().replace(/^\s+|\s+$/g, '');
                    if (rowType.toLowerCase() == value.toLowerCase()) {
                        rows.eq(i).show();
                    }
                    else {
                        rows.eq(i).hide();
                    }
                }
            }
            $filter.change(filterStatus);            
            
            $('form').submit(function(){
                return false;
            });
        });
    </script>
</body>
</html>
