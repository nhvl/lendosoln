﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using EDocs;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Drivers.Gateways;
using LendersOffice.Integration.Irs4506T;
using LendersOffice.Security;
using LQB4506T;
using LQB4506T.LQB4506TRequest;
using LQB4506T.LQB4506TResponse;
using LqbGrammar.Commands;

namespace LendersOfficeApp.newlos.Services
{
    public partial class Order4506TService : BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName) 
            {
                case "Submit":
                    Submit();
                    break;
                case "GetStatus":
                    GetStatus();
                    break;
            }
        }

        private void GetStatus()
        {
            AbstractUserPrincipal user = PrincipalFactory.CurrentPrincipal;

            Guid loanId = GetGuid("LoanId");
            string orderNumber = GetString("OrderNumber");
            Guid vendorId = GetGuid("VendorId");
            string username = this.GetString("Username", null);
            string password = this.GetString("Password", null);

            Irs4506TOrderInfo order = Irs4506TOrderInfo.Retrieve(vendorId, loanId, orderNumber, user.BrokerId);
            if (order == null)
            {
                SetResult("Status", "Error");
                SetResult("ErrorMessage", "The order was not found. Please refresh the page and try again.");
                return;
            }

            Irs4506TVendorConfiguration vendor = Irs4506TVendorConfiguration.RetrieveById(order.VendorId);
            if (vendor.CommunicationModel == Irs4506TCommunicationModel.LendingQB)
            {
                SetResult("Status", "Error");
                SetResult("ErrorMessage", "The vendor for this order does not support checking status. The order will automatically update when it is done.");
                return;
            }

            if (vendor.CommunicationModel == Irs4506TCommunicationModel.EquifaxVerificationServices)
            {
                I4506TServer server = vendor.Server;
                Irs4506TVendorCredential credential;
                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {
                    credential = new Irs4506TVendorCredential(user.BrokerId, username, password, null, vendorId, user.UserId);
                }
                else
                {
                    credential = Irs4506TVendorCredential.Retrieve(user.BrokerId, user.UserId, vendor.VendorId);
                    if ((string.IsNullOrEmpty(credential.DecryptedPassword) || string.IsNullOrEmpty(credential.UserName)) && (username == null || password == null))
                    {
                        SetResult("Status", "Login");
                        SetResult("OrderInfo", SerializationHelper.JsonNetAnonymousSerialize(new { OrderNumber = orderNumber, VendorId = vendorId }));
                        return;
                    }
                }

                IStatusDetails<Irs4506TOrderStatus, string> statusDetails = server.CheckOrderStatus(loanId, order, user, credential);

                SetResult("Status", statusDetails.Status.ToString());
                if (statusDetails.Status == Irs4506TOrderStatus.Error)
                {
                    SetResult("ErrorMessage", statusDetails.Messages.Any() ? statusDetails.Messages[0] : string.Empty);
                }
                else
                {
                    SetResult("StatusMessage", statusDetails.Messages.Any() ? statusDetails.Messages[0] : string.Empty);
                }
                
                return;
            }
        }

        private void Submit()
        {
            Guid sLId = GetGuid("LoanID");

            // AppName has the following format: {aAppId}:{B | C}:{Name}
            string appName = GetString("AppName");

            string[] parts = appName.Split(':');

            Guid aAppId = new Guid(parts[0]);
            string borrowerName = parts[2];
            string borrType = parts[1];
            Guid vendorId = GetGuid("VendorId");
            Guid documentId = GetGuid("DocumentID", Guid.Empty);

            Guid transactionId = Guid.NewGuid();

            Guid userId = BrokerUserPrincipal.CurrentPrincipal.UserId;

            Irs4506TVendorCredential credential = Irs4506TVendorCredential.Retrieve(BrokerUserPrincipal.CurrentPrincipal.BrokerId, userId, vendorId);

            Irs4506TVendorConfiguration vendor = Irs4506TVendorConfiguration.RetrieveById(vendorId);

            EDocument edoc = null;
            if (documentId != Guid.Empty)
            {
                EDocumentRepository repository = EDocumentRepository.GetUserRepository();

                edoc = repository.GetDocumentById(documentId);
            }

            if (string.IsNullOrEmpty(credential.UserName))
            {
                SetResult("Status", "Error");
                SetResult("ErrorMessage", "No saved credential");
                return;
            }

            if (edoc == null)
            {
                SetResult("Status", "Error");
                SetResult("ErrorMessage", "No document was selected.");
                return;
            }

            IStatusDetails<bool, string> result = 
                vendor.Server.SubmitNewOrder(
                    sLId, 
                    aAppId, 
                    borrType == "C" ? E_BorrowerModeT.Coborrower : E_BorrowerModeT.Borrower, 
                    edoc, 
                    user: PrincipalFactory.CurrentPrincipal, 
                    credential: null,
                    applicationName:borrowerName);

            SetResult("Status", result.Status ? "OK" : "Error");
            if (!result.Status)
            {
                SetResult("ErrorMessage", string.Join(Environment.NewLine, result.Messages));
            }
        }
    }
}
