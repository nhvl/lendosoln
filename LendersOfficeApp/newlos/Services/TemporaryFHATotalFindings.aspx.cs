﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Xsl;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Services
{
    public partial class TemporaryFHATotalFindings : LendersOffice.Common.BaseXsltPage
    {
        protected override string XsltFileLocation
        {
            get { return "LendersOffice.Integration.TotalScorecard.Findings.xslt.config"; }
        }

        protected override bool IsXsltFromEmbeddedResource
        {
            get { return true; }
        }
        private Guid LoanID
        {
            get { return RequestHelper.LoanID; }
        }

        protected override System.Xml.Xsl.XsltArgumentList XsltParams
        {
            get
            {
                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("currentYear", "", DateTime.Now.Year);
                args.AddParam("AlternateTitle", "", "Sample FHA TOTAL Scorecard Findings Preview");
                return args;
            }
        }
        protected override string NoContentMessage
        {
            get
            {
                return "There are no FHA TOTAL Scorecard Findings on this loan file.";
            }
        }
        protected override void GenerateXmlData(System.Xml.XmlWriter writer)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(TemporaryFHATotalFindings));
            dataLoan.InitLoad();
            if (dataLoan.sTotalScoreTempCertificateXmlContent.Value != "")
            {
                writer.WriteRaw(dataLoan.sTotalScoreTempCertificateXmlContent.Value);
            }
            else
            {
                HasContent = false;
            }
        }
    }
}
