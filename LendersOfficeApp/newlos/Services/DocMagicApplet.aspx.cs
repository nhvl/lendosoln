namespace LendersOfficeApp.newlos.Services
{
    using System;
    using System.Net;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using global::DocMagic.DsiDocRequest;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Conversions;
    using LendersOffice.Security;
    using DsiRequest = global::DocMagic.DsiDocRequest;
    using DsiResponse = global::DocMagic.DsiDocResponse;

    public partial class DocMagicApplet : LendersOffice.Common.BasePage
    {
        private bool DoWriteAppletHTML = false;

        private string AppletHTML = null;

        private bool LoginInfoChanged = false;

        private LendersOffice.Security.DocMagicAuthentication x_auth;

        private LendersOffice.Security.DocMagicAuthentication Auth
        {
            get
            {
                if (null == x_auth)
                {
                    if (IsPerTransactionBilling)
                    {
                        var bp = BrokerUserPrincipal.CurrentPrincipal;
                        x_auth = (DocMagicAuthentication)new DocMagicSavedAuthenticationForPTM(bp.UserId, bp.BranchId, bp.BrokerId);
                    }
                    else
                    {
                        x_auth = (DocMagicAuthentication)DocMagicSavedAuthentication.Current;
                    }
                }
                return x_auth;
            }
            
        }

        protected bool IsPerTransactionBilling
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BillingVersion == E_BrokerBillingVersion.PerTransaction; }
        }

        protected bool CanModifyLoginInfo
        {
            get
            {
                return (false == IsPerTransactionBilling) || (IsPerTransactionBilling && Broker.DocMagicIsAllowuserCustomLogin);
            }
        }

        private BrokerDB x_broker;
        protected BrokerDB Broker
        {
            get
            {
                if (x_broker == null)
                {
                    x_broker = BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                }
                return x_broker;
            }
        }

        protected bool m_hasError
        {
            get { return m_errorMessage != null && m_errorMessage.Length > 0; }
        }

        protected string m_errorMessage = "";

        protected void PageInit(object sender, System.EventArgs e)
        {
            if (E_BrokerBillingVersion.PerTransaction == Broker.BillingVersion && !Broker.IsEnablePTMDocMagicOnlineInterface)
            {
                string userMsg = "You do not have permission to view this page.";
                string devMsg = "user attempted to view DocMagicApplet page but they are on PTM billing and the broker has not allowed it.";
                throw new CBaseException(userMsg, devMsg);
            }

            ClientScript.GetPostBackEventReference(this, "");
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            string cmd = RequestHelper.GetSafeQueryString("cmd");

            switch (cmd)
            {
                case "export":
                    TitleLabel.Text = "Export to DocMagic";
                    break;
                case "launch":
                default:
                    TitleLabel.Text = "Launch DocMagic Applet";
                    break;
            }
            if (Page.IsPostBack)
            {
                GetTemporaryAuthentication();
                try
                {
                    switch (cmd)
                    {
                        case "export":
                            ExportToDocMagic2();
                            break;
                        case "launch":
                        default:
                            LaunchApplet2();
                            break;
                    }
                }
                finally
                {
                    SaveAuthentication();
                }
            }
            else
            {
                LoadData();
            }
        }

        private void GetTemporaryAuthentication()
        {
            if (false == CanModifyLoginInfo)
            {
                return;
            }
            if (false == IsPerTransactionBilling)
            {
                if (Auth.CustomerId != DocMagicCustomerID.Text)
                {
                    Auth.CustomerId = DocMagicCustomerID.Text;
                    LoginInfoChanged = true;
                }
            }
            if (Auth.UserName != DocMagicUserName.Text)
            {
                Auth.UserName = DocMagicUserName.Text;
                LoginInfoChanged = true;
            }

            if (DocMagicPassword.Text != ConstAppDavid.FakePasswordDisplay)
            {
                Auth.Password = DocMagicPassword.Text;
                LoginInfoChanged = true;
            }
            DocMagicPassword.Attributes.Add("value", ConstAppDavid.FakePasswordDisplay);
        }

        private void SaveAuthentication()
        {
            if (false == CanModifyLoginInfo)
            {
                return;
            }

            if (DocMagicRememberLogon.Visible && false == DocMagicRememberLogon.Checked)
            {
                Auth.CustomerId = "";
                Auth.UserName = "";
                Auth.Password = "";
                if (false == IsPerTransactionBilling)
                {
                    DocMagicCustomerID.Text = "";
                }
                DocMagicUserName.Text = "";
                DocMagicPassword.Text = "";
                DocMagicPassword.Attributes.Add("value", "");

                LoginInfoChanged = true;
            }

            if (LoginInfoChanged)
            {
                Auth.Save();
            }
        }

        private void LoadData()
        {
            CPageData dataLoan = new CDocMagicFileIdData(RequestHelper.LoanID);
            dataLoan.InitLoad();

            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                string errorMessage = null;

                if (dataLoan.sHasLoanEstimateArchiveInPendingStatus)
                {
                    var archive = dataLoan.sLoanEstimateArchiveInPendingStatus;
                    errorMessage = ErrorMessages.ArchiveError.UnableToGenerateDocumentsBecauseOfPendingArchive(
                        archive.DateArchived,
                        archive.Status,
                        userCanDetermineStatus: false);
                }
                else if (dataLoan.sHasClosingCostArchiveInUnknownStatus)
                {
                    var archive = dataLoan.sClosingCostArchiveInUnknownStatus;
                    errorMessage = ErrorMessages.ArchiveError.UnableToGenerateDocumentsBecuaseOfUnknownArchive(
                        archive.DateArchived,
                        archive.ClosingCostArchiveType,
                        userCanDetermineStatus: false);
                }

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    throw new CBaseException(errorMessage, "Cannot generate non-seamless docs with Pending/Unknown archive.");
                }
            }

            string sDocMagicFileId = dataLoan.sDocMagicFileId;
            DocMagicWebsheet.Text = string.IsNullOrEmpty(sDocMagicFileId.TrimWhitespaceAndBOM()) ? "NEW" : sDocMagicFileId;

            this.Bind_DocMagicPackageType(DocMagicPackageType, dataLoan.sIsInTRID2015Mode);

            if (dataLoan.sIsLegacyClosingCostVersion)
            {
                PackageTypeRow.Visible = false;
            }

            if (false == CanModifyLoginInfo)
            {
                return;
            }

            if (Auth.CustomerId != "")
            {
                if (false == IsPerTransactionBilling)
                {
                    DocMagicCustomerID.Text = Auth.CustomerId;
                }
                DocMagicUserName.Text = Auth.UserName;
                DocMagicPassword.Attributes.Add("value", ConstAppDavid.FakePasswordDisplay);
                DocMagicRememberLogon.Checked = true;
            }
        }

        private void LaunchApplet2()
        {
            if ( string.IsNullOrEmpty(DocMagicWebsheet.Text) )
            {
                ShowError("Please import loan file into DocMagic First.");
                return;
            }
            DsiRequest.DsiDocumentServerRequest request = DocMagicMismoRequest.CreateLaunchAppletRequest(Auth.CustomerId, Auth.UserName, Auth.Password, RequestHelper.LoanID, DocMagicWebsheet.Text);
            DsiResponse.DsiDocumentServerResponse response = DocMagicServer2.Submit(request);

            if (null != response)
            {
                switch (response.Status)
                {
                    case DsiResponse.E_DsiDocumentServerResponseStatus.Success:
                        WriteAppletHtml(response.AppletResponse.AppletHtml);
                        break;
                    case DsiResponse.E_DsiDocumentServerResponseStatus.Failure:
                        BuildErrorMessage(response);
                        break;
                }
            }
        }

        private void ExportToDocMagic2()
        {
            // First, export to DocMagic
            E_DocumentsPackageType packageType = (E_DocumentsPackageType)Enum.Parse(typeof(E_DocumentsPackageType), DocMagicPackageType.SelectedValue);
            DsiDocumentServerRequest request;
            try
            {
                request = DocMagicMismoRequest.CreateSaveRequest(Auth.CustomerId, Auth.UserName, Auth.Password, RequestHelper.LoanID, DocMagicWebsheet.Text, packageType);
            }
            catch (PermissionException)
            {
                ShowError("Closer permission is required to export the settlement charges to DocMagic.");
                return;
            }
            catch (FeeDiscrepancyException e)
            {
                ShowError(e.UserMessage);
                return;
            }

            DsiResponse.DsiDocumentServerResponse response;
            try
            {
                response = DocMagicServer2.Submit(request);
            }
            catch (WebException e)
            {
                ShowError("An unexpected communication issue has occurred with DocMagic, please try again.");
                Tools.LogError("DocMagic might not be responding", e);
                return;
            }

            if (response == null)
            {
                return;
            }
            if (response.Status == DsiResponse.E_DsiDocumentServerResponseStatus.Failure)
            {
                BuildErrorMessage(response);
                return;
            }

            DsiResponse.WebsheetResponse a = response.WebsheetResponseList[0];
            SaveWebsheetNumber(a.WebsheetInfoList[0].WebsheetNumber);

            // Then, launch the DM applet
            request = DocMagicMismoRequest.CreateLaunchAppletRequest(Auth.CustomerId, Auth.UserName, Auth.Password, RequestHelper.LoanID, DocMagicWebsheet.Text);
            response = DocMagicServer2.Submit(request);

            if (response == null)
            {
                return;
            }

            switch (response.Status)
            {
                case DsiResponse.E_DsiDocumentServerResponseStatus.Failure:
                    BuildErrorMessage(response);
                    break;
                case DsiResponse.E_DsiDocumentServerResponseStatus.Success:
                    if (response.AppletResponse == null)
                    {
                        ShowError("Unable to launch DocMagicApplet");
                    }
                    AppletHTML = response.AppletResponse.AppletHtml;
                    DoWriteAppletHTML = true;
                    break;
                case DsiResponse.E_DsiDocumentServerResponseStatus.Undefined:
                    ShowError("Unforeseen error occurred. Please try again");
                    Tools.LogError("Got back undefined response type from DocMagic");
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Binds package options to the DocMagic Package Type dropdown list for new datalayer loans.
        /// </summary>
        /// <param name="ddl">The dropdown list to bind to.</param>
        private void Bind_DocMagicPackageType(DropDownList ddl, bool isTRID)
        {
            if (isTRID)
            {
                ddl.Items.Add(new ListItem("Loan Estimate", E_DocumentsPackageType.Initial.ToString("D")));
                ddl.Items.Add(new ListItem("Closing Disclosure", E_DocumentsPackageType.Closing.ToString("D")));
                ddl.Items.Add(new ListItem("Other", E_DocumentsPackageType.Other.ToString("D")));
            }
            else
            {
                ddl.Items.Add(new ListItem("Initial", E_DocumentsPackageType.Initial.ToString("D")));
                ddl.Items.Add(new ListItem("Closing", E_DocumentsPackageType.Closing.ToString("D")));
            }
        }

        private void ShowError(string msg)
        {
            m_errorMessage = msg;
        }

        private void BuildErrorMessage(DsiResponse.DsiDocumentServerResponse dsiServerResponse)
        {
            StringBuilder sb = new StringBuilder();
            foreach (DsiResponse.Message msg in dsiServerResponse.MessageList)
            {
                if (msg.Type == global::DocMagic.DsiDocResponse.E_MessageType.Fatal )
                    sb.Append(msg.InnerText).Append(Environment.NewLine);
            }
            ShowError(sb.ToString());

        }

        private void WriteAppletHtml(string appletHtml)
        {
            Response.ClearContent();
            Response.Output.Write(appletHtml);
            Response.Flush();
            Response.End();
        }

        private void SaveWebsheetNumber(string websheetNumber)
        {
            CPageData dataLoan = new CDocMagicFileIdData(RequestHelper.LoanID);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sDocMagicFileId = websheetNumber;
            dataLoan.Save();
            if (null != websheetNumber && "" != websheetNumber)
            {
                DocMagicWebsheet.Text = websheetNumber;
            }
        }

        private void PageLoadComplete(object sender, EventArgs e)
        {
            if (DoWriteAppletHTML)
            {
                WriteAppletHtml(AppletHTML);
            }
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
            this.LoadComplete += new System.EventHandler(this.PageLoadComplete);
        }
		#endregion
    }
}
