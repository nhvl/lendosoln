<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderAppraisal.aspx.cs" Inherits="LendersOfficeApp.newlos.Services.OrderAppraisal" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title>Order GDMS Appraisal</title>
<link href="../../css/stylesheet.css" rel="stylesheet" type="text/css" />
<style type="text/css">
input
{
    border: default;
}
h2
{
    margin: 0 0 2px 0;
    padding: 2px;
}
input
{
    padding: 0;
    height: 1.5em;
}
img
{
    vertical-align: top;
}
.row
{
    width: 100%;
    margin: 0 auto;
    overflow: hidden;
    font-size: 11px;
}

.onecol,.twocol,.threecol,.fourcol
{
    float: left;
    margin-left: 0px;
    margin-right: 2px;
    min-height: 1px;
}

.onecol
{
    width: 50px;
}

.twocol
{
    width: 106px;
}

.threecol
{
    width: 162px;
}

.fourcol
{
    width: 218px;
}

.fivecol
{
    width: 274px;
}

.fillRow
{
    width: 307px;
}

.FieldLabel
{
    vertical-align: middle;
    height: 1.5em;
    display: block;
    padding: 2px;
}

.FormTable
{
    width: 45em;
    border: 0;
}

.ShortTextBox
{
    width:96px;
    display:inline;
}

#OrderAppraisalBtnContainer
{
    text-align: center;
}
#OrderAppraisalBtn
{
    height: 2em;
    width: 10em;
}
#ProcessingOrderWithAttachment
{
    display: none;
}

</style>
</head>
<script type="text/javascript">
var lastPrimaryProduct= null;

    $(window).on("load", function() {
        CheckForWorkflowError(true);
    
        $('#_ReadOnly').val("True"); // Don't allow saving on this page.
        $('#GDMSBillingMethod').change(function(e) {
            var selectedMethod = $(this).children('option').filter(':selected');
            $('#BillingInfoCreditCardPanel').toggle(IsCreditCard(selectedMethod));
        });

        $OrderAppraisalBtn = $('#OrderAppraisalBtn');
        $OrderAppraisalBtn.prop('disabled', !validatePage());
        $('#GDMSProcessor2').prop('disabled', $('#GDMSProcessor').val() == -1);

        $('input,select,textarea').change(function() {
            determineAppraisalBtnStatus();

            $('#GDMSProcessor2').prop('disabled', ($('#GDMSProcessor').val() == -1 || $('#GDMSProcessor').prop('disabled')));
        });
        $('#SelectedAMC').change(function() {
            determineAppraisalUIStatus();
        });
        $('.ProductList').change(function() {
        
            determineAppraisalBtnStatus();

            $('#GDMSProcessor2').prop('disabled', $('#GDMSProcessor').val() == -1);
            updateProductLists();
        });
        $('input[preset="date"]').blur(determineAppraisalBtnStatus);
        determineAppraisalUIStatus();
        
        if(<%=AspxTools.JsBool(IsEdit) %>)
        {
            $("#OrderReportType").val($("#LoadOrderReportType").val());
            $("#OrderReportType2").val($("#LoadOrderReportType2").val());
            $("#OrderReportType3").val($("#LoadOrderReportType3").val());
            $("#OrderReportType4").val($("#LoadOrderReportType4").val());
            $("#OrderReportType5").val($("#LoadOrderReportType5").val());
            $("#GDMSBillingMethod").val($("#LoadGDMSBillingMethod").val());
            $("#CCType").val($("#LoadCCType").val());
            
            $('#GDMSBillingMethod').change(); //Update "credit card" panel
            
        }
        $('#AttachmentType').change(function() {
            setDisabledAttr($('#SelectDocumentsLink'), ($(this).val() === '-1'));
        });
        $('.contactphone').change(function() {
            var bAtLeastOneEntered = $('.contactphone[value!=""]').length > 0;
            $('.contactphonevalidator').toggle(!bAtLeastOneEntered);
        });
        $('.additionalEmail').change(function() {
            var bLessThan8 = $('.additionalEmail').val().split(';').length <= 8;
            $('.additionalEmailValidator').toggle(!bLessThan8);
        });

        $('.AgentPicker').mouseover(function() {
            $(this).attr("src", '../../images/contacts_clicked.png');
        });

        $('.AgentPicker').mouseout(function() {
            $(this).attr("src", '../../images/contacts.png');
        });

        $('.AgentPicker').click(function() {
            var rolodex = new cRolodex();
            rolodex.chooseFromRolodex(-1, ML.sLId, false, true, function(args){

                if (args.OK == true) {
                    $('#GDMSContactName').val(args.AgentName);
                    $('#GDMSContactPhone').val(args.AgentPhone);
                    $('#GDMSContactWork').val(args.PhoneOfCompany);
                    $('#GDMSContactOther').val(args.AgentAltPhone);
                    $('#GDMSContactEmail').val(args.AgentEmail);

                    $('.contactphone').change();   // Toggles phone warning;
                }
            });
        });

        $('.contactphone').change();

        $('.additionalEmail').change();

        $('#PropertySelector').change(function() {
            var selectedGuid = $(this).val();
            var propertyDetails = PropertyInfoDetails[selectedGuid];
            if(propertyDetails) {
                $('#BorrowerName').val(propertyDetails.BorrowerName);
                $('#PropertyAddress').val(propertyDetails.Address);
                $('#PropertyCity').val(propertyDetails.City);
                $('#PropertyState').val(propertyDetails.State);
                $('#PropertyZip').val(propertyDetails.Zip);
                $('#BorrowerEmail').val(propertyDetails.BorrowerEmail);
            }
            else {
                $('.PropertyDetail').val('');
            }
        }).change();
    });

function CheckForWorkflowError(shouldAlert) {
    if (document.getElementById('m_WorkflowAlertMessage').value != '') {
        jQuery('#ErrorSummary').text(document.getElementById('m_WorkflowAlertMessage').value);
        
        if(shouldAlert) {
            alert(document.getElementById('m_WorkflowAlertMessage').value);
        }
    }
}
function loadBillingMethods(billingMethods) {
    for (var i = 0; i < billingMethods.length; i++) {
        var name = billingMethods[i].Name;
        var identifier = billingMethods[i].Identifier;
        var ccIndicator = billingMethods[i].CreditCardIndicator;
        $('#GDMSBillingMethod').append( $('<option/>', { text: name, value: identifier,}).data('cc-indicator', ccIndicator) );
    }
}
function loadOptions() {
    var result;
    var args = {
        SelectedAMC : $('#SelectedAMC').val(),
        LoanID : <%= AspxTools.JsString(LoanID.ToString()) %>
    };

    if (args.SelectedAMC != -1)
    {
        result = gService.OrderAppraisal.call("LoadOptions", args);
    }
    if(result && result.value && result.value.IsValid == "True")
    {
        if (result.value.IsGdms == "True") {
            var propT = JSON.parse(result.value.GDMSPropertyType);
            var reportT = JSON.parse(result.value.GDMSReportType);
            var useT = JSON.parse(result.value.GDMSIntendedUse);
            var loanT = JSON.parse(result.value.GDMSLoanType);
            var occT = JSON.parse(result.value.GDMSOccupancyType);
            var billT = JSON.parse(result.value.GDMSBillingMethod);
            var client2Opt = JSON.parse(result.value.GDMSClient2);
            var processorOpt = JSON.parse(result.value.GDMSProcessor);
            var processor2Opt = JSON.parse(result.value.GDMSProcessor2);
            var orderFileUploadT = JSON.parse(result.value.GDMSOrderFileUploadFileType);

            var LoadGDMSPropertyType = $('#LoadGDMSPropertyType').val();
            var LoadGDMSIntendedUse = $('#LoadGDMSIntendedUse').val();
            var LoadGDMSLoanType = $('#LoadGDMSLoanType').val();
            var LoadGDMSOccupancyType = $('#LoadGDMSOccupancyType').val();

            $('#GDMSPropertyType').empty().append('<option value="-1"></option>');
            for (var i = 0; i < propT.length; i++) {
                $('#GDMSPropertyType').append('<option value="' + propT[i][0] + (propT[i][1] == LoadGDMSPropertyType?'" selected="true':'') +'">' + propT[i][1] + '</option>');
            }
            $('.GDMSReportType').empty().append('<option value="-1"></option>');
            for (var i = 0; i < reportT.length; i++) {
                $('.GDMSReportType').append('<option value="' + reportT[i][0] + '">' + reportT[i][1] + '</option>');
            }
            $('#GDMSIntendedUse').empty().append('<option value="-1"></option>');
            for (var i = 0; i < useT.length; i++) {
                $('#GDMSIntendedUse').append('<option value="' + useT[i][0] + (useT[i][1] == LoadGDMSIntendedUse?'" selected="true':'') + '">' + useT[i][1] + '</option>');
            }
            $('#GDMSLoanType').empty().append('<option value="-1"></option>');
            for (var i = 0; i < loanT.length; i++) {
                $('#GDMSLoanType').append('<option value="' + loanT[i][0] + (loanT[i][1] == LoadGDMSLoanType?'" selected="true':'') + '">' + loanT[i][1] + '</option>');
            }
            $('#GDMSOccupancyType').empty().append('<option value="-1"></option>');
            for (var i = 0; i < occT.length; i++) {
                $('#GDMSOccupancyType').append('<option value="' + occT[i][0] + (occT[i][1] == LoadGDMSOccupancyType?'" selected="true':'') + '">' + occT[i][1] + '</option>');
            }
            $('#GDMSBillingMethod').empty().append('<option value="-1"></option>');
            loadBillingMethods(billT);
            $('#GDMSClient2').empty().append('<option value="-1"></option>');
            for (var i = 0; i < client2Opt.length; i++) {
                $('#GDMSClient2').append('<option value="' + client2Opt[i][0] + '">' + client2Opt[i][1] + '</option>');
            }
            $('#GDMSProcessor').empty().append('<option value="-1"></option>');
            for (var i = 0; i < processorOpt.length; i++) {
                $('#GDMSProcessor').append('<option value="' + processorOpt[i][0] + '">' + processorOpt[i][1] + '</option>');
            }
            $('#GDMSProcessor2').empty().append('<option value="-1"></option>');
            for (var i = 0; i < processor2Opt.length; i++) {
                $('#GDMSProcessor2').append('<option value="' + processor2Opt[i][0] + '">' + processor2Opt[i][1] + '</option>');
            }
            $('#AttachmentType').empty().append('<option value="-1"></option>');
            for (var i = 0; i < orderFileUploadT.length; i++) {
                $('#AttachmentType').append('<option value="' + orderFileUploadT[i][0] + '">' + orderFileUploadT[i][1] + '</option>');
            }
        }
        else {
            LQBAppraisalProductList = JSON.parse(result.value.ProductsList);
            var billingMethods = JSON.parse(result.value.BillingList);
            
            updateProductLists();

            $('#GDMSBillingMethod').empty();
            loadBillingMethods(billingMethods);
            
            if(result.value.ErrorMessage)
                alert(result.value.ErrorMessage);
        }
        $('img:visible').css('display', 'inline'); //Fix positioning of Required img tags
        $('#AttachmentTypeReq').hide();
        
        return true;
    }
    if(result)
    {
        var errorMsg = 'Error loading Account Info';
        if(result.value && result.value.ErrorMessage)
            errorMsg = result.value.ErrorMessage;
        alert(errorMsg);
    }
    $('#GDMSPropertyType').empty();
    $('.GDMSReportType').empty();
    $('#GDMSIntendedUse').empty();
    $('#GDMSLoanType').empty();
    $('#GDMSOccupancyType').empty();
    $('#GDMSBillingMethod').empty();
    $('#GDMSClient2').empty();
    $('#GDMSProcessor').empty();
    $('#GDMSProcessor2').empty();
    $('.ProductList').empty();
    $('#AttachmentType').empty();
    return false;
}
function updateProductLists()
{
    var selectedValues = [];
    selectedValues.push($('#OrderReportType').val());
    selectedValues.push($('#OrderReportType2').val());
    selectedValues.push($('#OrderReportType3').val());
    selectedValues.push($('#OrderReportType4').val());
    selectedValues.push($('#OrderReportType5').val());
    $('.ProductList').empty().append('<option value=""></option>');
    //for some reason, a DLL is being called 2-3 times, so its unbound just to stop the uneeded iterations
    $('.ProductList').off();
    var $SelectedAMC = $('#SelectedAMC');
    var AMCId = $SelectedAMC.val();
    var onlyOnePrimaryAllowed = AllowOnlyOnePrimaryProduct[AMCId];
    
    var primaryProduct;
    var hasPrimaryProduct;
    var primaryIndex;
    
    var deselectedPrimary = true;
    for(var i = 0; i < selectedValues.length; i++)
    {
        deselectedPrimary &= !(lastPrimaryProduct == null || lastPrimaryProduct == selectedValues[i])
    }
    
    if(deselectedPrimary)
    {
        selectedValues = [];
        selectedValues.push($(""));
        selectedValues.push($(""));
        selectedValues.push($(""));
        selectedValues.push($(""));
        selectedValues.push($(""));
    }
    
    if(onlyOnePrimaryAllowed)
    {

    
        //iterate through all the products, and check if you have a primary selected
        for(var i = 0; i < LQBAppraisalProductList.length; i++)
        {
            var product = LQBAppraisalProductList[i];
            if(!product.requires)
            {
                switch (product.name)
                {
                    case selectedValues[0]:
                    {
                        primaryProduct = selectedValues[0];
                        hasPrimaryProduct = true;
                        primaryIndex = 0;
                        break;
                    }
                    case selectedValues[1]:
                    {
                        primaryProduct = selectedValues[1];
                        hasPrimaryProduct = true;
                        primaryIndex = 1;
                        break;
                    }
                    case selectedValues[2]:
                    {
                        primaryProduct = selectedValues[2];
                        hasPrimaryProduct = true;
                        primaryIndex = 2;
                        break;
                    }
                    case selectedValues[3]:
                    {
                        primaryProduct = selectedValues[3];
                        hasPrimaryProduct = true;
                        primaryIndex = 3;
                        break;
                    }
                    case selectedValues[4]:
                    {
                        primaryProduct = selectedValues[4];
                        hasPrimaryProduct = true;
                        primaryIndex = 4;
                        break;
                    }
                }
                if(hasPrimaryProduct)
                break;
            }
        }
    }
    
    
    for (var i = 0; i < LQBAppraisalProductList.length; i++) {
        if(LQBAppraisalProductList[i].requires)
        {
            //If product[i] is secondary (requires another product), make sure the required product is one of the previously selected values before adding product[i]
            var required = LQBAppraisalProductList[i].requires;
            if(selectedValues[0] != required && selectedValues[1] != required && selectedValues[2] != required && selectedValues[3] != required && selectedValues[4] != required)
                continue;            
        }
        else
        {
            if(onlyOnePrimaryAllowed && hasPrimaryProduct && primaryProduct != LQBAppraisalProductList[i].name)
                continue;
        }
        $('.ProductList').each(function(index) {
            if(primaryProduct == LQBAppraisalProductList[i].name)
            {
                if(primaryIndex == index)
                this.add(new Option(LQBAppraisalProductList[i].name, LQBAppraisalProductList[i].name, false, selectedValues[index] == LQBAppraisalProductList[i].name));
            }
            else
            //add the option to every product list; if it was the already selected one, mark it as selected
            this.add(new Option(LQBAppraisalProductList[i].name, LQBAppraisalProductList[i].name, false, selectedValues[index] == LQBAppraisalProductList[i].name));
        });
    }
    //disable all the empty lists
    $('.ProductList').each(function() {
        var size = this.options.length;
        this.disabled = size <= 1 && onlyOnePrimaryAllowed && primaryProduct != null;
        $(this).blur();
    });
    
    
    // 8/22/2013 EM - rebind the change method
    $('.ProductList').change(function() {
            determineAppraisalBtnStatus();

            $('#GDMSProcessor2').prop('disabled', $('#GDMSProcessor').val() == -1);
            updateProductLists();
        });
    
    
    lastPrimaryProduct = primaryProduct;
}

function validatePage() {
    if ($('#Disable').val() == 'True') {
        CheckForWorkflowError(false);
        return false;
    }

    if ($('#SelectedAMC').val() == -1)
        return false;

    var valid = true;
    var isGdms = GDMSVendors[$('#SelectedAMC').val()];
    if (isGdms) {
        valid &= $('#GDMSReportType').val() != -1;
        valid &= $('#GDMSIntendedUse').val() != -1;
        valid &= $('#GDMSBillingMethod').val() != -1;
    }
    else {    
        valid &= $('#OrderReportType').val() != "";
    }

    if (AppraisalNeededDateRequired[$('#SelectedAMC').val()]) {
        if (isGdms) {
            valid &= $('#GDMSAppraisalNeeded').val() != '';
        }
        else {
            valid &= $('#OrderAppraisalNeededD').val() != '';
        }
    }
    
    var selectedBillingMethod = $('#GDMSBillingMethod').children('option').filter(':selected');
    if (IsCreditCard(selectedBillingMethod)) {
        valid &= $('#CCBillName').val() != '';
        valid &= $('#CCAddress1').val() != '';
        valid &= $('#CCCity').val() != '';
        valid &= $('#CCState').val() != -1;
        valid &= $('#CCZip').val() != '';
        valid &= $('#CCType').val() != '';
        valid &= $('#CCNumber').val() != '';
        valid &= $('#CCExpMonth').val() != '';
        valid &= $('#CCExpYear').val() != '';
        valid &= $('#CCCode').val() != '';
    }
    
    valid &= $('#PropertyAddress').val() != '';
    valid &= $('#PropertyCity').val() != '';
    valid &= $('#PropertyState').val() != '';
    valid &= $('#PropertyZip').val() != '';
    
    valid &= $('#GDMSContactName').val() != '';
    valid &= $('.contactphone[value!=""]').length > 0;

    // Validate additional emails
    valid &= $('.additionalEmail').val().split(';').length <= 8;
    
    return valid;
}
function IsCreditCard(selectedMethod) {
    var regex = /credit.*card/i; // check if the selection option contains the words "credit" and "card"
    var selectedText = selectedMethod.text();
    var creditCardIndicator = selectedMethod.data('cc-indicator');
    var isCreditCard = creditCardIndicator == 1/*E_TriState.Yes*/ || (creditCardIndicator == 0/*E_TriState.Blank*/ && regex.test(selectedText));

    return isCreditCard;
}
function determineAppraisalBtnStatus(forceInvalid) {
    var $OrderAppraisalBtn = $('#OrderAppraisalBtn');
    $OrderAppraisalBtn.prop('disabled', !((validatePage() && !forceInvalid)));
}
function determineAppraisalUIStatus() {
    var $SelectedAMC = $('#SelectedAMC');
    var AMCId = $SelectedAMC.val();
    $('#LoanInfoSection').toggle(!!!GDMSVendors[AMCId]);
    $('#OrderInfoSection').toggle(!!!GDMSVendors[AMCId]);
    $('#AttachmentTypeSection').toggle(!!GDMSVendors[AMCId]);
    $('#ReferenceInfoSection').toggle(!!GDMSVendors[AMCId]);
    $('#AssignmentInfoSection').toggle(!!GDMSVendors[AMCId]);
    $('#ClientInfoSection').toggle(!!GDMSVendors[AMCId]);

    $('#AdditionalEmails').toggle(!!ShowAdditionalEmails[AMCId]);
    $('.AppraisalNeededDSection').toggle(!HideAppraisalNeededDate[AMCId]);
    $('.AppraisalNeededDIndicator').toggle(!!AppraisalNeededDateRequired[AMCId]);
    $('#NotesSection').toggle(!HideNotes[AMCId]);
    
    $('#ExtraInfoSection').toggle(!(HideNotes[AMCId] && GDMSVendors[AMCId]));
    
    var success = loadOptions();
    if (!success) {
        $('input,select,textarea,a').not('.AlwaysEditable').prop('disabled', true);
    }
    else {
        $('input,select,textarea,a').not('.AlwaysEditable').prop('disabled', false);
        setDisabledAttr($('#SelectDocumentsLink'), (!!GDMSVendors[AMCId] || AMCId === '-1'));
    }
    $('#GDMSBillingMethod').change(); //Update "credit card" panel
    determineAppraisalBtnStatus(!success);
}
function orderAppraisalClick() {
    if ($('#AttachedDocuments').val() != ''){
        $('#ProcessingOrderWithAttachment').show();
    }
    else {
        $('#ProcessingOrder').show();
    }

    $('#OrderAppraisalBtn').hide();
    window.setTimeout(function() {
        args = {
            SelectedAMC : $('#SelectedAMC').val(),
            LoanID : <%= AspxTools.JsString(LoanID.ToString()) %>,
            sFileVersion : <%= AspxTools.JsString(sFileVersion.ToString()) %>,
            sLNm : $('#sLNm').val(),
            GDMSCaseNum : $('#GDMSCaseNum').val(),
            sAgencyCaseNum : $('#sAgencyCaseNum').val(),
            sLPurposeT : $('#sLPurposeT').val(),
            sLT : $('#sLT').val(),
            LoanOfficerName: $('#LoanOfficerName').val(),
            BorrowerName : $('#BorrowerName').val(),
            PropertyAddress : $('#PropertyAddress').val(),
            PropertyCity : $('#PropertyCity').val(),
            PropertyState : $('#PropertyState').val(),
            PropertyZip : $('#PropertyZip').val(),
            BorrowerEmail : $('#BorrowerEmail').val(),
            ReoId: $('#PropertySelector').val(),
            GDMSContactName : $('#GDMSContactName').val(),
            GDMSContactPhone : $('#GDMSContactPhone').val(),
            GDMSContactWork : $('#GDMSContactWork').val(),
            GDMSContactOther : $('#GDMSContactOther').val(),
            GDMSContactEmail : $('#GDMSContactEmail').val(),
            GDMSPropertyTypeId : $('#GDMSPropertyType').val(),
            GDMSPropertyTypeName : $('#GDMSPropertyType > option:selected').text(),
            GDMSReportTypeName : $('#GDMSReportType > option:selected').text(),
            GDMSReportTypeId : $('#GDMSReportType').val(),
            GDMSReportType2Id : $('#GDMSReportType2').val(),
            GDMSReportType3Id : $('#GDMSReportType3').val(),
            GDMSReportType4Id : $('#GDMSReportType4').val(),
            GDMSReportType5Id : $('#GDMSReportType5').val(),
            GDMSIntendedUseName : $('#GDMSIntendedUse > option:selected').text(),
            GDMSIntendedUseId : $('#GDMSIntendedUse').val(),
            GDMSAppraisalNeeded : $('#GDMSAppraisalNeeded').val(),
            GDMSClient2Id : $('#GDMSClient2').val(),
            GDMSProcessorId : $('#GDMSProcessor').val(),
            GDMSProcessor2Id : $('#GDMSProcessor2').val(),
            GDMSLenderName : $('#GDMSLenderName').val(),
            GDMSBillingMethodId : $('#GDMSBillingMethod').val(),
            GDMSBillingMethodName : $('#GDMSBillingMethod > option:selected').text(),
            CreditCardIndicator: $('#GDMSBillingMethod > option:selected').data('cc-indicator'),
            CCBillName : $('#CCBillName').val(),
            CCAddress1 : $('#CCAddress1').val(),
            CCCity : $('#CCCity').val(),
            CCState : $('#CCState').val(),
            CCZip : $('#CCZip').val(),
            CCType : $('#CCType').val(),
            CCNumber : $('#CCNumber').val(),
            CCExpMonth : $('#CCExpMonth').val(),
            CCExpYear : $('#CCExpYear').val(),
            CCCode : $('#CCCode').val(),
            Notes : $('#Notes').val(),
            GDMSLoanTypeName : $('#GDMSLoanType > option:selected').text(),
            GDMSLoanTypeId : $('#GDMSLoanType').val(),
            GDMSOccupancyTypeName : $('#GDMSOccupancyType > option:selected').text(),
            GDMSOccupancyTypeId : $('#GDMSOccupancyType').val(),
            LQBAppraisalNeeded : $('#OrderAppraisalNeededD').val(),
            OrderReportType1 : $('#OrderReportType').val(),
            OrderReportType2 : $('#OrderReportType2').val(),
            OrderReportType3 : $('#OrderReportType3').val(),
            OrderReportType4 : $('#OrderReportType4').val(),
            OrderReportType5 : $('#OrderReportType5').val(),
            OrderNumber : <%= AspxTools.JsString(OrderNumber) %>,
            RushOrder : $('#RushOrder').prop('checked'),
            AttachedDocumentsIds : $('#AttachedDocumentsIds').val(),
            AttachmentType : $('#AttachmentType').val(),
            GDMSAdditionalEmail : $('#GDMSAdditionalEmail').val(),
        };

        var result = gService.OrderAppraisal.call("SubmitOrder", args);
        var noMessage = true;
        if(result && result.value && result.value.ErrorMessage)
        {
            alert(result.value.ErrorMessage);
            noMessage = false;
        }
        if(result && result.value && result.value.Success == "True")
        {
            if (result.value.FileErrorMessage)
            {
                alert(result.value.FileErrorMessage);
            }
            window.location = "GlobalDMSOrderAppraisalList.aspx?loanid=" + <%=AspxTools.JsString(LoanID.ToString())%>;
        }
        else if(noMessage)
        {
            alert("Error ordering appraisal.");
            $('#ProcessingOrder').hide();
            $('#ProcessingOrderWithAttachment').hide();
            $('#OrderAppraisalBtn').show();
        }
        else
        {
            $('#ProcessingOrder').hide();
            $('#ProcessingOrderWithAttachment').hide();
            $('#OrderAppraisalBtn').show();
        }
    }, 100);
}
function selectDocumentsClick()
{
    var args = {};
    try
    {
        var ids = JSON.parse(document.getElementById('AttachedDocumentsIds').value);
        args['SelectedDocsIds'] = ids;
    }
    catch(e) { }
    
    showModal('/newlos/Services/OrderAppraisalDocumentPicker.aspx?loanid=<%= AspxTools.HtmlString(LoanID.ToString()) %>', args, null, null, function(result){ 
        if(result.DocNames && result.DocIds)
        {
            var text = '';
            for(var i = 0; i<result.DocNames.length; i++)
            {
                text += result.DocNames[i] + '\n';
            }
            document.getElementById('AttachedDocuments').value = text;
            document.getElementById('AttachedDocumentsIds').value = JSON.stringify(result.DocIds);
        }
        $('#AttachmentTypeReq').toggle($('#AttachedDocuments').val() != '');
    },{hideCloseButton:true});
}
</script>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
    <asp:HiddenField ID="m_WorkflowAlertMessage" runat="server" />
    <fieldset class="FormTable">
        <asp:HiddenField id="ErrorMessage" runat="server" />
        <asp:HiddenField id="FileErrorMessage" runat="server" />
        <asp:HiddenField ID="ProductsList" runat="server" />
        <asp:HiddenField ID="BillingList" runat="server" />
        <asp:HiddenField ID="LoadOrderReportType" runat="server" />
        <asp:HiddenField ID="LoadOrderReportType2" runat="server" />
        <asp:HiddenField ID="LoadOrderReportType3" runat="server" />
        <asp:HiddenField ID="LoadOrderReportType4" runat="server" />
        <asp:HiddenField ID="LoadOrderReportType5" runat="server" />
        <asp:HiddenField ID="LoadGDMSBillingMethod" runat="server" />
        <asp:HiddenField ID="LoadCCType" runat="server" />
        <div>
            <h2 class="FormTableHeader">Order Appraisal</h2>
        </div>

        <label class="FieldLabel twocol">
            Select AMC:
        </label>
        <asp:DropDownList runat="server" ID="SelectedAMC" CssClass="AlwaysEditable">
            <asp:ListItem Value="-1" Text=""></asp:ListItem>
        </asp:DropDownList>
        
        <div class="InsetBorder" id="ReferenceInfoSection">
            <h2 class="FormTableSubheader">
                Reference Info
            </h2>
            <div class="row">
                <label class="FieldLabel onecol">
                    Loan #
                </label>
                <asp:TextBox id="sLNm" class="threecol" runat="server" readonly />

                <label class="FieldLabel onecol">
                    Case #
                </label>
                <asp:TextBox ID="GDMSCaseNum" class="twocol" runat="server"/>
            </div>
        </div>
        
        <div class="InsetBorder" id="LoanInfoSection">
			<h2 class="FormTableSubheader">
				Loan Info
			</h2>
			<div class="row">
			    <label class="FieldLabel" style="display:inline">
				    Loan #
			    </label>
			    <asp:TextBox ID="sLNm_dup" runat="server" class="ShortTextBox" style="margin-left: 85px" readonly/>
			    &nbsp;
			    <label class="FieldLabel" style="display:inline">
				    Case #
			    </label>
			    <asp:TextBox ID="sAgencyCaseNum" runat="server" class="ShortTextBox" style="margin-left: 53px" readonly/>
			</div>
			<div class="row">
			    <label class="FieldLabel" style="display:inline">
				    Loan Type
			    </label>
			    <asp:TextBox ID="sLT" runat="server" class="ShortTextBox" style="margin-left: 66px" readonly/>
			    &nbsp;
			    <label class="FieldLabel" style="display:inline">
				    Intended Use
			    </label>
			    <asp:TextBox ID="sLPurposeT" runat="server" class="ShortTextBox" style="margin-left: 19px" readonly/>
			</div>
			<div class="row">
			    <label class="FieldLabel" style="display:inline">
				    Property Type
			    </label>
			    <asp:TextBox ID="sGseSpT" runat="server" class="ShortTextBox" style="margin-left: 47px" readonly/>
			    &nbsp;
			    <label class="FieldLabel" style="display:inline">
				    Occupancy Type
			    </label>
			    <asp:TextBox ID="aOccT" runat="server" class="ShortTextBox" readonly/>
			</div>
            <div class="row">
                <label class="FieldLabel" style="display:inline">
				    Loan Officer Full Name
			    </label>
			    <asp:TextBox ID="LoanOfficerName" runat="server" class="ShortTextBox" readonly/>
            </div>
		</div>
		
        <div class="InsetBorder">
            <h2 class="FormTableSubheader">
                Borrower / Property Info
            </h2>
            <div class="row">
                <label class="FieldLabel twocol">
                    Select Property
                </label>
                <asp:DropDownList ID="PropertySelector" runat="server" CssClass="AlwaysEditable"></asp:DropDownList>
            </div>
            <div class="row">
                <label class="FieldLabel twocol">
                    Borrower Name 
                </label>
                <asp:TextBox ID="BorrowerName" class="fivecol PropertyDetail" runat="server" readonly/>
            </div>

            <div class="row">
                <label class="FieldLabel twocol">
                    Property Address
                </label>
                <asp:TextBox ID="PropertyAddress" class="fivecol PropertyDetail" runat="server"/>
                <img id="Img6" src="../../images/require_icon.gif" alt="Required" />
                
            </div>

            <div class="row">
                <label class="FieldLabel twocol">
                    &nbsp;
                </label>
                <%-- All scrunched together because of IE<=7 --%>
                <asp:TextBox runat="server" class="threecol PropertyDetail" ID="PropertyCity" ></asp:TextBox><ml:StateDropDownList runat="server" class="onecol PropertyDetail" ID="PropertyState" style="width: 54px" Enabled="true" />
                <ml:ZipcodeTextBox runat="server" class="onecol PropertyDetail" ID="PropertyZip" ></ml:ZipcodeTextBox>
                <img id="Img9" src="../../images/require_icon.gif" alt="Required" />
                
            </div>

            <div class="row">
                <label class="FieldLabel twocol">
                    Borrower Email
                </label>
                <asp:TextBox ID="BorrowerEmail" class="fivecol PropertyDetail" runat="server" readonly />
            </div>
            
        </div>

        <div class="InsetBorder">
            <h2 class="FormTableSubheader">
                Property Access / Contact Info
            </h2>

            <div class="row">
                <label class="FieldLabel twocol">
                    Contact Name
                </label>
                <asp:TextBox ID="GDMSContactName" class="fivecol" runat="server" />
                <img id="AgentPicker" class="AgentPicker" alt="pick from agents" src="../../images/contacts.png" />
                <img id="Img10" src="../../images/require_icon.gif" alt="Required" />
            </div>

            <div class="row">
                <label class="FieldLabel twocol">
                    Phone Number
                </label>
                <ml:PhoneTextBox preset="phone" ID="GDMSContactPhone" class="twocol contactphone" runat="server" />
            </div>

            <div class="row">
                <label class="FieldLabel twocol">
                    Work Number
                </label>
                <ml:PhoneTextBox preset="phone" ID="GDMSContactWork" class="twocol contactphone" runat="server" />
                <span class="contactphonevalidator" style="color:red;">Please provide at least one phone number.</span>
            </div>

            <div class="row">
                <label class="FieldLabel twocol">
                    Other Number
                </label>
                <ml:PhoneTextBox preset="phone" ID="GDMSContactOther" class="twocol contactphone" runat="server" />
            </div>

            <div class="row">
                <label class="FieldLabel twocol">
                    Email
                </label>
                <asp:TextBox ID="GDMSContactEmail" class="twocol" runat="server" />
            </div>

            <div class="row" style="height:4em;" id="AdditionalEmails" runat="server">
                <label class="FieldLabel twocol" style="float:left;">
                    Additional Emails<br />
                    (up to 8 and separated by ;)
                </label>
                <div style="float:left">
                    <asp:TextBox ID="GDMSAdditionalEmail" class="fivecol additionalEmail" runat="server" /><br />
                    <span class="additionalEmailValidator" style="color:red;">Please limit the number of additional emails to 8.</span>
                </div>
            </div>
        </div>

        <div class="InsetBorder" id="AssignmentInfoSection">
            <h2 class="FormTableSubheader">
                Assignment Info
            </h2>
            <div class="row">
                <label class="FieldLabel twocol">
                    Property Type
                </label>
                <asp:HiddenField runat="server" ID="LoadGDMSPropertyType" />
                <asp:DropDownList runat="server" ID="GDMSPropertyType" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="row">
                <label class="FieldLabel twocol">
                    Report Type
                </label>
                <asp:DropDownList runat="server" ID="GDMSReportType" class="GDMSReportType" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                </asp:DropDownList>
                <img id="GDMSReportTypeReq" src="../../images/require_icon.gif" alt="Required" />
            </div>

            <div class="row">
                <label class="FieldLabel twocol">
                    Report Type 2
                </label>
                <asp:DropDownList runat="server" ID="GDMSReportType2" class="GDMSReportType" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="row">
                <label class="FieldLabel twocol">
                    Report Type 3
                </label>
                <asp:DropDownList runat="server" ID="GDMSReportType3" class="GDMSReportType" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="row">
                <label class="FieldLabel twocol">
                    Report Type 4
                </label>
                <asp:DropDownList runat="server" ID="GDMSReportType4" class="GDMSReportType" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="row">
                <label class="FieldLabel twocol">
                    Report Type 5
                </label>
                <asp:DropDownList runat="server" ID="GDMSReportType5" class="GDMSReportType" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="row">
                <label class="FieldLabel twocol">
                    Intended Use
                </label>
                <asp:HiddenField runat="server" ID="LoadGDMSIntendedUse" />
                <asp:DropDownList runat="server" ID="GDMSIntendedUse" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                </asp:DropDownList>
                <img id="GDMSIntendedUseReq" src="../../images/require_icon.gif" alt="Required" />
            </div>
            
            <div class="row">
                <label class="FieldLabel twocol">
                    Loan Type
                </label>
                <asp:HiddenField runat="server" ID="LoadGDMSLoanType" />
                <asp:DropDownList runat="server" ID="GDMSLoanType" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                </asp:DropDownList>
            </div>
            
            <div class="row">
                <label class="FieldLabel twocol">
                    Occupancy Type
                </label>
                <asp:HiddenField runat="server" ID="LoadGDMSOccupancyType" />
                <asp:DropDownList runat="server" ID="GDMSOccupancyType" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                </asp:DropDownList>
            </div>

        </div>
        <div class="InsetBorder" id="ClientInfoSection">
            <h2 class="FormTableSubheader">
                Client Info
            </h2>
            <div class="row AppraisalNeededDSection">
                <label class="FieldLabel twocol">
                    Appraisal Needed
                </label>
                <ml:DateTextBox runat="server" ID="GDMSAppraisalNeeded" onchange="determineAppraisalBtnStatus()"></ml:DateTextBox>
                <img id="GDMSAppraisalNeededReq" class="AppraisalNeededDIndicator" src="../../images/require_icon.gif" alt="Required" />
            </div>

            <div class="row">
                <label class="FieldLabel twocol">
                    Client 2
                </label>
                <asp:DropDownList runat="server" ID="GDMSClient2" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="row">
                <label class="FieldLabel twocol">
                    Processor
                </label>
                <asp:DropDownList runat="server" ID="GDMSProcessor" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="row">
                <label class="FieldLabel twocol">
                    Processor 2
                </label>
                <asp:DropDownList runat="server" ID="GDMSProcessor2" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="row">
                <label class="FieldLabel twocol">
                    Lender
                </label>
                <asp:TextBox ID="GDMSLenderName" runat="server" />
            </div>
        </div>
        
        <div class="InsetBorder" id="OrderInfoSection">
			<h2 class="FormTableSubheader">
				Order Info
			</h2>
			<div class="row">
				<label class="FieldLabel twocol">
					Report Type
				</label>
				<asp:DropDownList runat="server" ID="OrderReportType" class="ProductList">
					<asp:ListItem Value="-1" Text=""></asp:ListItem>
				</asp:DropDownList>
                <img id="Img8" src="../../images/require_icon.gif" alt="Required" />
			</div>
			<div class="row">
				<label class="FieldLabel twocol">
					Report Type 2
				</label>
				<asp:DropDownList runat="server" ID="OrderReportType2" class="ProductList">
					<asp:ListItem Value="-1" Text=""></asp:ListItem>
				</asp:DropDownList>
			</div>
			<div class="row">
				<label class="FieldLabel twocol">
					Report Type 3
				</label>
				<asp:DropDownList runat="server" ID="OrderReportType3" class="ProductList">
					<asp:ListItem Value="-1" Text=""></asp:ListItem>
				</asp:DropDownList>
			</div>
			<div class="row">
				<label class="FieldLabel twocol">
					Report Type 4
				</label>
				<asp:DropDownList runat="server" ID="OrderReportType4" class="ProductList">
					<asp:ListItem Value="-1" Text=""></asp:ListItem>
				</asp:DropDownList>
			</div>
			<div class="row">
				<label class="FieldLabel twocol">
					Report Type 5
				</label>
				<asp:DropDownList runat="server" ID="OrderReportType5" class="ProductList">
					<asp:ListItem Value="-1" Text=""></asp:ListItem>
				</asp:DropDownList>
			</div>
			<div class="row AppraisalNeededDSection">
				<label class="FieldLabel twocol">
					Appraisal Needed
				</label>
				<ml:DateTextBox runat="server" ID="OrderAppraisalNeededD" onchange="determineAppraisalBtnStatus()"></ml:DateTextBox>
                <img id="OrderAppraisalNeededDRequired" class="AppraisalNeededDIndicator" src="../../images/require_icon.gif" alt="Required" />
			</div>
			<div class="row">
			    <asp:CheckBox runat="server" ID="RushOrder" Text="&nbsp;Rush Order" />
			</div>
		</div>
        
        <div class="InsetBorder">
            <h2 class="FormTableSubheader">
                Billing Info
            </h2>
            <div class="row">
                <label class="FieldLabel twocol">
                    Billing Method
                </label>
                <asp:DropDownList runat="server" ID="GDMSBillingMethod" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1" Text=""></asp:ListItem>
                </asp:DropDownList>
                <img id="GDMSBillingMethodReq" src="../../images/require_icon.gif" alt="Required" />
            </div>
            <div id="BillingInfoCreditCardPanel">
                <div class="row">
                    <label class="FieldLabel twocol">
                        Billing Name
                    </label>
                    <asp:TextBox ID="CCBillName" class="fivecol" runat="server" />
                    <img id="Img1" src="../../images/require_icon.gif" alt="Required" />
                </div>
                
                <div class="row">
                    <label class="FieldLabel twocol">
                        Billing Address
                    </label>
                    <asp:TextBox ID="CCAddress1" class="fivecol" runat="server" />
                    <img id="Img2" src="../../images/require_icon.gif" alt="Required" />
                </div>
                
                <div class="row">
                    <label class="FieldLabel twocol">
                        &nbsp;
                    </label>
                    <asp:TextBox runat="server" class="threecol" ID="CCCity"></asp:TextBox><ml:StateDropDownList runat="server" class="onecol" ID="CCState" style="width: 54px"
                    /><ml:ZipcodeTextBox runat="server" class="onecol" ID="CCZip"></ml:ZipcodeTextBox>
                    <img id="Img3" src="../../images/require_icon.gif" alt="Required" />
                </div>
                
                
                <div class="row">
                    <label class="FieldLabel twocol">
                        Card Type
                    </label>
                    <asp:DropDownList runat="server" ID="CCType"></asp:DropDownList>
                    <img id="Img4" src="../../images/require_icon.gif" alt="Required" />
                </div>
                
                <div class="row">
                    <label class="FieldLabel twocol">
                        Card Number
                    </label>
                    <asp:TextBox ID="CCNumber" class="fivecol" runat="server" />
                    <img id="Img5" src="../../images/require_icon.gif" alt="Required" />
                </div>
                
                <div class="row">
                    <label class="FieldLabel twocol">
                        Expiration
                    </label>
                    
                    <div class="twocol">
                        <asp:TextBox ID="CCExpMonth" width="3.5em" runat="server" />
                        /
                        <asp:TextBox ID="CCExpYear" width="3.5em" runat="server" />
                    </div>

                    <label class="FieldLabel onecol" style="width:54px">
                        CVV/CCID
                    </label>
                    <asp:TextBox ID="CCCode" class="onecol" runat="server" />
                    <img id="Img7" src="../../images/require_icon.gif" alt="Required" />
                </div>
                
            </div>
        </div>
        <div class="InsetBorder" id="ExtraInfoSection">
            <h2 class="FormTableSubheader">
                Extra Info
            </h2>
            <div id="DocumentsSection">
                <div class="row" id="AttachmentTypeSection">
                    <label class="FieldLabel twocol">
                        Attachment Type
                    </label>
                    <asp:DropDownList runat="server" ID="AttachmentType">
                        <asp:ListItem Value="-1" Text=""></asp:ListItem>
                    </asp:DropDownList>
                    <img id="AttachmentTypeReq" src="../../images/require_icon.gif" alt="Required" style="display: none;" />
                </div>
                <div class="row">
                    <label class="FieldLabel twocol">
                        Attach Documents
                    </label>
                    <asp:HiddenField runat="server" ID="AttachedDocumentsIds" />
                    <asp:TextBox runat="server" ID="AttachedDocuments" cssclass="fillRow" TextMode="MultiLine" Height="50px" ReadOnly="true"></asp:TextBox><br />
                    <span style="float:right;">
                        <a href="javascript:void(0);" onclick="if (!(hasDisabledAttr(this))) selectDocumentsClick();" id="SelectDocumentsLink">select documents</a>
                    </span>
                    <br />&nbsp;
                </div>
            </div>
            <div class="row"  id="NotesSection">
                <label class="FieldLabel twocol">
                    Notes
                </label>
                <asp:TextBox runat="server" ID="Notes" cssclass="fillRow" TextMode="MultiLine" Rows="5"></asp:TextBox>
            </div>
        </div>
        <div id="ErrorMessageDiv" runat="server">
            <span class="FieldLabel" id="ErrorSummary" style="color:Red;" runat="server"></span>
        </div>
        <div id="OrderAppraisalBtnContainer">
            <input id="OrderAppraisalBtn" runat="server" value="Order Appraisal" type="button" onclick="orderAppraisalClick();" />
            <span id="ProcessingOrder" style="display:none;">Processing order...</span>
            <span id="ProcessingOrderWithAttachment">
                <p>Processing...</p>
                <p>It may take up to 5 minutes for larger attachments. Please do not leave the window.</p>
            </span>
        </div>
    </fieldset>
    <asp:HiddenField ID="Disable" runat="server" />
    </form>
</body>
</html>
