<%@ Page language="c#" Codebehind="ViewLPFeedbackFrame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Services.ViewLPFeedbackFrame" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML> 

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  </head>
  <h4 class="page-header">Loan Product Advisor Feedback</h4>
  <body class="body-iframe">
    <form runat="server">
      <div class="top">
        <iframe name=menu src='ViewLPFeedbackMenu.aspx?loanid=<%=AspxTools.HtmlStringFromQueryString("loanid")%>'></iframe>
      </div>
      <div class="bottom">
        <iframe id="MainFrame" name=body src='ViewLPFeedback.aspx?loanid=<%=AspxTools.HtmlStringFromQueryString("loanid")%>'></iframe>
      </div>
    </form>
  </body>
</html>
