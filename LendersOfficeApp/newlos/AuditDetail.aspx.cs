using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using LendersOffice.Audit;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendersOffice.HttpModule;
using LqbGrammar.DataTypes;

namespace LendersOfficeApp.newlos
{
	public partial class AuditDetail : BasePage
	{
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.IE10;
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
            try 
            {
                // 3/24/2008 dd - Disable the render of timing output code.
                PerformanceMonitorItem item = PerformanceMonitorItem.Current;
                if (null != item)
                    item.IsEnabledDebugOutput = false;
            } 
            catch {}

            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
            if (null == principal)
            {
                RequestHelper.Signout();
            }

            string key = RequestHelper.GetSafeQueryString("key");
            if (string.IsNullOrEmpty(key))
            {
                Guid sLId = RequestHelper.LoanID;
                Guid auditId = RequestHelper.GetGuid("auditid");
                if (!Tools.GetAuditHistoryMigrated(sLId, principal.BrokerId)) // old audit format
                {
                    if (RequestHelper.GetBool("prepForNewAudit"))
                    {
                        // We're attempting to view the details of an audit
                        // inbetween when the audit was added and when the loan's
                        // audit XML was moved to permanent storage. In this case,
                        // prep for the new audit by moving the non-permanent audits
                        // into permanent storage.
                        AuditManager.MoveToPermanentStorage(sLId);
                    }

                    AuditManager.RenderAuditDetail(sLId, auditId, Response.OutputStream);
                }
                else
                {
                    AuditManager.RenderAuditDetailVer2(principal.BrokerId, sLId, auditId, Response.OutputStream);
                }
            }
            else
            {
                string data = LendersOffice.Drivers.Encryption.EncryptionHelper.DecodeAndDecrypt(EncryptionKeyIdentifier.AuditKeyPassword, key);
                var tokens = data.Split('/');
                Guid sLId = new Guid(tokens[0]);
                long auditSeq = long.Parse(tokens[1]);
                AuditManager.RenderAuditDetailVer2(principal.BrokerId, sLId, auditSeq, Response.OutputStream);
            }

            Response.Flush();
            Response.End();

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
