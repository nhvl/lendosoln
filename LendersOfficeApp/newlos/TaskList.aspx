<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="TaskList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.TaskList" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>TaskList</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
<body class=RightBackground MS_POSITIONING="FlowLayout" scroll=yes>
<script language=javascript>

	<!--

    function _init()
    {
		resize( 750 , 550 );
    }

    function editReminder(event)
    {
        var notifId = retrieveEventTarget(event).getAttribute("data-value");
		    showModal( "/los/reminders/TaskEditor.aspx?notifId=" + notifId + "&hideViewEditLoan=t" , null, null, null, function(args){ 
          if( args != null && args.OK != null && args.OK == true )
          {		
            self.location = self.location;
          }
        },{hideCloseButton:true});
  	}
    function createNewTask()
    {
		showModal( "/los/reminders/TaskEditor.aspx?loanId=<%=AspxTools.JsStringUnquoted(RequestHelper.LoanID.ToString())%>&hideViewEditLoan=t" , null, null, null, function(args){ 
      if( args != null && args.OK != null && args.OK == true )
      {		
        self.location = self.location;
      }
    },{hideCloseButton:true});
  }		
    function deleteReminder(notifId) {
      if (confirm('Are you sure you want to remove yourself from the task?')) 
        __doPostBack('<%= AspxTools.ClientId(m_yourTasksDG) %>', notifId);
    }
    function joinDiscussion(event)
    {
        var discLogId = retrieveEventTarget(event).getAttribute("data-value");
		showModal( "/los/reminders/TaskEditor.aspx?discLogId=" + discLogId + "&hideViewEditLoan=t" , null, null, null, function(args){ 
        if( args != null && args.OK != null && args.OK == true )
        {		
          self.location = self.location;
        }
      },{hideCloseButton:true});
    }  
    function activateNotif(event)
    {
        var notifId = retrieveEventTarget(event).getAttribute("data-value");
		showModal( "/los/reminders/TaskEditor.aspx?notifId=" + notifId + "&hideViewEditLoan=t" , null, null, null, function(args){
        if( args != null && args.OK != null && args.OK == true )
        {		
          self.location = self.location;
        }
      },{hideCloseButton:true});
    }  

    function printTasksList()
    {
        var url = 'pdf/LoanTask.aspx?loanid=<%=AspxTools.JsStringUnquoted(LoanID.ToString())%>&crack=' + new Date();
        LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
    }      

	//-->

</script>

<form id=TaskList method=post runat="server">
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  <tr>
    <td class=TabRow>
<div class="Tabs">
    <ul class="tabnav">
        <li id="tab0" runat="server"><a href="#" onclick="__doPostBack('changeTab', '0');">Your Tasks</a></li>
        <li id="tab1" runat="server"><a href="#" onclick="__doPostBack('changeTab', '1');">Your Tracked Tasks</a></li>
        <li id="tab2" runat="server"><a href="#" onclick="__doPostBack('changeTab', '2');">Other Tasks</a></li>
    </ul>
</div>
</td>
    <td class=TabRow align=right>
		<input onclick=createNewTask(); type=button value="Add new task" AlwaysEnable="true">&nbsp;&nbsp;<input onclick="printTasksList();" type="button" value="Print task list" AlwaysEnable="true">
		<asp:Button id="m_refreshAll" runat="server" Text="Refresh">
		</asp:Button>
	</td>
    </tr>
  <tr>
    <td colSpan=2><ml:commondatagrid id=m_yourTasksDG runat="server" visible="False">
<alternatingitemstyle cssclass="GridAlternatingItem">
</AlternatingItemStyle>

<itemstyle cssclass="GridItem">
</ItemStyle>

<headerstyle cssclass="GridHeader">
</HeaderStyle>

<columns>
<asp:TemplateColumn>
<itemtemplate>
              <a href="#" onclick="editReminder(event);" data-value="<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "NotifId").ToString())%>">edit</a>
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="DiscStatus" HeaderText="Status">
<itemtemplate>
<%#AspxTools.HtmlControl(DisplayDiscStatus((int) DataBinder.Eval(Container.DataItem, "DiscStatus")))%>
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="DiscPriority" HeaderText="Priority">
<itemtemplate>
<%#AspxTools.HtmlControl(DisplayDiscPriority((int)DataBinder.Eval(Container.DataItem, "DiscPriority")))%>
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="DiscSubject" HeaderText="Subject">
<itemtemplate>
				      <%#AspxTools.HtmlControl(DisplayDiscSubject((string) DataBinder.Eval(Container.DataItem, "DiscSubject"), (bool) DataBinder.Eval(Container.DataItem, "NotifIsRead")))%>
			      
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="DiscDueDate" HeaderText="Due Date">
<itemtemplate>
				      <%#AspxTools.HtmlControl(DisplayDiscDueDate(DataBinder.Eval( Container.DataItem, "DiscDueDate" )))%>
			      
</ItemTemplate>
</asp:TemplateColumn>
<asp:BoundColumn DataField="DiscCreatorLoginNm" HeaderText="Creator"></asp:BoundColumn>
<asp:boundcolumn datafield="DiscCreatedDate" headertext="Created Date"></asp:boundcolumn>
					<asp:templatecolumn>
						<itemtemplate>
							<a href="#" onclick='deleteReminder("<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "NotifId").ToString())%>");'>
								quit</a>
						</itemtemplate>
					</asp:templatecolumn>
</Columns>        
      </ml:commondatagrid></td></tr>
  <tr>
    <td colSpan=2><ml:commondatagrid id=m_yourTaskTracksDG runat="server" visible="False">
<alternatingitemstyle cssclass="GridAlternatingItem">
</AlternatingItemStyle>

<itemstyle cssclass="GridItem">
</ItemStyle>

<headerstyle cssclass="GridHeader">
</HeaderStyle>

<columns>
<asp:templatecolumn sortexpression="TrackNotifIsRead" headertext="Read?">
  <itemtemplate>				
  <%#AspxTools.HtmlControl(DisplayTrackNotifIsRead((bool)DataBinder.Eval(Container.DataItem,"TrackNotifIsRead"), (bool)DataBinder.Eval(Container.DataItem,"NotifIsValid")))%>
</itemtemplate>
</asp:templatecolumn>
<asp:TemplateColumn SortExpression="DiscStatus" HeaderText="Status">
<itemtemplate>
<%#AspxTools.HtmlControl(DisplayDiscStatus((int) DataBinder.Eval(Container.DataItem, "DiscStatus")))%>
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="DiscPriority" HeaderText="Priority">
<itemtemplate>
<%#AspxTools.HtmlControl(DisplayDiscPriority((int)DataBinder.Eval(Container.DataItem, "DiscPriority")))%>
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="DiscSubject" HeaderText="Subject">
<itemtemplate>
				      <%#AspxTools.HtmlControl(DisplayDiscSubject((string) DataBinder.Eval(Container.DataItem, "DiscSubject"), true))%>
			      
</ItemTemplate>
</asp:TemplateColumn>
<asp:boundcolumn datafield="NotifReceiverLoginNm" HeaderText="Tracked Person" sortexpression="NotifReceiverLoginNm"/>
<asp:TemplateColumn SortExpression="DiscDueDate" HeaderText="Due Date">
<itemtemplate>
				      <%#AspxTools.HtmlControl(DisplayDiscDueDate(DataBinder.Eval( Container.DataItem, "DiscDueDate" )))%>
			      
</ItemTemplate>
</asp:TemplateColumn>
<asp:BoundColumn DataField="DiscCreatorLoginNm" HeaderText="Creator"></asp:BoundColumn>
</Columns>        
      </ml:commondatagrid></td></tr>
  <tr>
    <td colSpan=2><ml:commondatagrid id=m_otherTasksDG runat="server" visible="False">
<alternatingitemstyle cssclass="GridAlternatingItem">
</AlternatingItemStyle>

<itemstyle cssclass="GridItem">
</ItemStyle>

<headerstyle cssclass="GridHeader">
</HeaderStyle>

<columns>
<asp:TemplateColumn>
<itemtemplate>
							<%#AspxTools.HtmlControl(GenerateDiscJoinLink( DataBinder.Eval(Container.DataItem, "DiscLogId"), DataBinder.Eval(Container.DataItem, "NotifId"), DataBinder.Eval(Container.DataItem, "NotifIsValid")))%>

            
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="DiscStatus" HeaderText="Status">
<itemtemplate>
<%#AspxTools.HtmlControl(DisplayDiscStatus((int) DataBinder.Eval(Container.DataItem, "DiscStatus")))%>
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="DiscPriority" HeaderText="Priority">
<itemtemplate>
<%#AspxTools.HtmlControl(DisplayDiscPriority((int)DataBinder.Eval(Container.DataItem, "DiscPriority")))%>
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="DiscSubject" HeaderText="Subject">
<itemtemplate>
				      <%#AspxTools.HtmlControl(DisplayDiscSubject((string) DataBinder.Eval(Container.DataItem, "DiscSubject"), (bool) true ))%>
			      
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="DiscDueDate" HeaderText="Due Date">
<itemtemplate>
				      <%#AspxTools.HtmlControl(DisplayDiscDueDate(DataBinder.Eval( Container.DataItem, "DiscDueDate" )))%>
    
			      
</ItemTemplate>
</asp:TemplateColumn>
<asp:boundcolumn datafield="DiscCreatorLoginNm" headertext="Creator"></asp:boundcolumn>
</Columns>        
      </ml:commondatagrid></td></tr>
  <tr>
    <td colSpan=2></td></tr></table></form><uc1:cmodaldlg id=CModalDlg1 runat="server"></uc1:cmodaldlg>
	
  </body>
</html>
