namespace LendersOfficeApp.newlos
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Conversions.LoanProspector;
    using LendersOffice.ObjLib.Conversions.Aus;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;
    using static LendersOffice.ObjLib.Conversions.Aus.AusCreditOrderingInfo;

    public partial class FreddieExport : BaseLoanPage
    {
        protected bool m_sHasFreddieInfileCredit = false;
        protected IEnumerable<string> m_mcrnList = System.Linq.Enumerable.Empty<string>();
        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterJsScript("LQBPopup.js");
            this.PageTitle = "Export to Freddie Mac";
            this.PageID = "ExportFreddie";
            this.EnableJqueryMigrate = false;

            Tools.Bind_sBuildingStatusT(sBuildingStatusT); // Should Have IPair map.
            Tools.Bind_sBuydownContributorT(sBuydownContributorT); // Should Have IPair map.
            Tools.Bind_sFreddieArmIndexT(sFreddieArmIndexT); // Should Have IPair map.
            Tools.Bind_sNegAmortT(sNegAmortT); // Should Have IPair map.
            Tools.Bind_sFredAffordProgId(sFredAffordProgId);
            Tools.Bind_sFreddieConstructionT(sFreddieConstructionT);

            bool isSeamlessLpaEnabled = this.Broker.IsSeamlessLpaEnabled;
            bool isNonSeamlessLpaEnabled = this.Broker.IsNonSeamlessLpaEnabled(PrincipalFactory.CurrentPrincipal);
            NonSeamlessButtons.Visible = isNonSeamlessLpaEnabled;
            SeamlessButtons.Visible = isSeamlessLpaEnabled;

            Guid branchID = Tools.GetLoanBranchIdByLoanId(BrokerUser.BrokerId, LoanID);

            IEnumerable<ServiceCredential> credentials = ServiceCredential.ListAvailableServiceCredentials(BrokerUser, branchID, ServiceCredentialService.AusSubmission);
            var seamlessUserCredential = credentials.FirstOrDefault(credential => credential.ChosenAusOption == AusOption.LpaUserLogin);
            var seamlessSellerCredential = credentials.FirstOrDefault(credential => credential.ChosenAusOption == AusOption.LpaSellerCredential);

            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("widgets/jquery.downpayment.js");

            this.RegisterJsObjectWithJsonNetAnonymousSerializer("downpaymentSources", Tools.Options_sDwnPmtSrc);
        }

        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return new WorkflowOperation[] { WorkflowOperations.RunLp };
        }

        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[] { WorkflowOperations.RunLp };
        }

        protected bool CanRunLp
        {
            get
            {
                return this.UserHasWorkflowPrivilege(WorkflowOperations.RunLp);
            }
        }

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FreddieExport));
            dataLoan.InitLoad();

            m_sHasFreddieInfileCredit = dataLoan.sHasFreddieInfileCredit;
            int nApps = dataLoan.nApps;

            CAppData dataApp = dataLoan.GetAppData(0);
            Tools.SetDropDownListValue(sBuildingStatusT, dataLoan.sBuildingStatusT);
            Tools.SetDropDownListValue(sFreddieConstructionT, dataLoan.sFreddieConstructionT);
            sBuydown.Checked = dataLoan.sBuydown;
            Tools.SetDropDownListValue(sBuydownContributorT, dataLoan.sBuydownContributorT);
            sFHASalesConcessions.Text = dataLoan.sFHASalesConcessions_rep;
            sFredAffordProgId.Text = dataLoan.sFredAffordProgId;
            Tools.SetDropDownListValue(sFreddieArmIndexT, dataLoan.sFreddieArmIndexT);
            sFreddieArmIndexTLckd.Checked = dataLoan.sFreddieArmIndexTLckd;
            sFreddieFHABorrPdCc.Text = dataLoan.sFreddieFHABorrPdCc_rep;
            sFredieReservesAmt.Text = dataLoan.sFredieReservesAmt_rep;
            sHelocBal.Text = dataLoan.sHelocBal_rep;
            sHelocCreditLimit.Text = dataLoan.sHelocCreditLimit_rep;
            Tools.SetDropDownListValue(sNegAmortT, dataLoan.sNegAmortT);
            sPayingOffSubordinate.Checked = dataLoan.sPayingOffSubordinate;
            sProdCashoutAmt.Text = dataLoan.sProdCashoutAmt_rep;
            sSpIsInPud.Checked = dataLoan.sSpIsInPud;
            sSpMarketVal.Text = dataLoan.sSpMarketVal_rep;
            sFHAFinancedDiscPtAmt.Text = dataLoan.sFHAFinancedDiscPtAmt_rep;
            sLpAusKey.Text = dataLoan.sLpAusKey;
            sFreddieLoanId.Text = dataLoan.sFreddieLoanId;
            sFreddieTransactionId.Text = dataLoan.sFreddieTransactionId;
            sFredieReservesAmtLckd.Checked = dataLoan.sFredieReservesAmtLckd;

            var canExportLoan = BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanExportLoan);
            this.exportButtonsPanel.Visible = canExportLoan;
            this.errMsg.Visible = !canExportLoan;

            this.DUDownPaymentSource1003.Checked = !dataLoan.sIsUseDUDwnPmtSrc;
            this.DUDownPaymentSourceJSON.Checked = dataLoan.sIsUseDUDwnPmtSrc;

            Page.ClientScript.RegisterClientScriptBlock(typeof(FreddieExport), "DownPaymentData", string.Format("var downPaymentData = {0};", dataLoan.sDUDwnPmtSrc.Serialize()), true);
        }

        protected override void SaveData() 
        {
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
            // OPM 245347 - Workflow restrictions for running LP.
            RegisterJsGlobalVariables("CanRunLp", this.CanRunLp);
            RegisterJsGlobalVariables("RunLpDenialReason", this.GetReasonUserCannotPerformPrivilege(WorkflowOperations.RunLp));
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion

    }
}
