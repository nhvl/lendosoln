using System;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Migration;

namespace LendersOfficeApp.newlos
{
    public class LoanInfoServiceItem : AbstractBackgroundServiceItem
    {
        protected override void Process(string methodName)
        {
            switch (methodName) 
            {
                case "GenerateNewLoanNumber":
                    GenerateNewLoanNumber();
                    break;
            }
        }

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(LoanInfoServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            dataLoan.sLpTemplateNm = GetString("sLpTemplateNm");
            if (dataLoan.BrokerDB.ShowLoanProductIdentifier)
            {
                dataLoan.sLoanProductIdentifier = GetString("sLoanProductIdentifier");
            }
            dataLoan.sCcTemplateNm = GetString("sCcTemplateNm");
            dataLoan.sLT = (E_sLT)GetInt("sLT");
            dataLoan.sLienPosT = (E_sLienPosT)GetInt("sLienPosT");
            dataLoan.sLPurposeT = (E_sLPurposeT)GetInt("sLPurposeT");
            dataLoan.sIsStudentLoanCashoutRefi = GetBool("sIsStudentLoanCashoutRefi");
            dataLoan.sFinMethT = (E_sFinMethT)GetInt("sFinMethT");
            dataLoan.sFinMethDesc = GetString("sFinMethDesc");
            dataLoan.sPurchPrice_rep = GetString("sPurchPrice");
            dataLoan.sApprVal_rep = GetString("sApprVal");
            dataLoan.sEquityCalc_rep = GetString("sEquityCalc");
            dataLoan.sLAmtCalc_rep = GetString("sLAmtCalc");
            dataLoan.sNoteIR_rep = GetString("sNoteIR");
            dataLoan.sQualIR_rep = GetString("sQualIR");
            dataLoan.sQualIRLckd = this.GetBool(nameof(dataLoan.sQualIRLckd), dataLoan.sQualIRLckd);
            dataLoan.sTerm_rep = GetString("sTerm");
            dataLoan.sDue_rep = GetString("sDue");
            dataLoan.sIsOptionArm = GetBool("sIsOptionArm");
            dataLoan.sOptionArmTeaserR_rep = GetString("sOptionArmTeaserR");

            if (!dataLoan.sRealEstateTaxExpenseInDisbMode)
            {
                dataLoan.sProRealETxMb_rep = GetString("sProRealETxMb");
                dataLoan.sProRealETxT = (E_PercentBaseT)GetInt("sProRealETxT");
                dataLoan.sProRealETxR_rep = GetString("sProRealETxR");
            }
            
            dataLoan.sProHoAssocDues_rep = GetString("sProHoAssocDues");
            dataLoan.sProOHExp_rep = GetString("sProOHExp");
            dataLoan.sProOHExpLckd = GetBool("sProOHExpLckd");
            //// 2/11/2005 dd - Relate to OPM case 1064. If user just calculate data then ignore sLNm, even if it blank.
            if (dataLoan.DataState == E_DataState.InitSave)
            {
                dataLoan.sLNm = GetString("sLNm");
            }

            dataLoan.sCrmNowLeadId = GetString("sCrmNowLeadId");
            if (!dataLoan.sHazardExpenseInDisbursementMode)
            {
                dataLoan.sProHazInsT = (E_PercentBaseT)GetInt("sProHazInsT");
                dataLoan.sProHazInsR_rep = GetString("sProHazInsR");
                dataLoan.sProHazInsMb_rep = GetString("sProHazInsMb");
            }
            
            dataLoan.sProMIns_rep = GetString("sProMIns");
            dataLoan.sProMInsLckd = GetBool("sProMInsLckd");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sLAmtLckd = GetBool("sLAmtLckd");
            dataApp.aOccT = (E_aOccT)GetInt("aOccT");
            dataLoan.sGseSpT = (E_sGseSpT)GetInt("sGseSpT");
            dataLoan.sHomeIsMhAdvantageTri = this.GetEnum<E_TriState>("sHomeIsMhAdvantageTri");
            dataLoan.sIsQRateIOnly = GetBool("sIsQRateIOnly");
            dataLoan.sOriginalAppraisedValue_rep = GetString("sOriginalAppraisedValue");
            dataLoan.sHighPricedMortgageTLckd = GetBool("sHighPricedMortgageTLckd");
            dataLoan.sHighPricedMortgageT = GetEnum<E_HighPricedMortgageT>("sHighPricedMortgageT");
            dataLoan.sIsEmployeeLoan = GetBool("sIsEmployeeLoan");
            dataLoan.sIsNewConstruction = GetBool("sIsNewConstruction");
            dataLoan.sIsLineOfCredit = GetBool("sIsLineOfCredit");
            dataLoan.sCreditLineAmt_rep = GetString("sCreditLineAmt");
            dataLoan.sLeadSrcDesc = GetString("sLeadSrcDesc");

            dataLoan.sIsRenovationLoan = GetBool("sIsRenovationLoan", dataLoan.sIsRenovationLoan);
            dataLoan.sFHASpAsIsVal_rep = GetString("sFHASpAsIsVal");
            dataLoan.sInducementPurchPrice_rep = GetString("sInducementPurchPrice");

            dataLoan.sTotalRenovationCostsLckd = GetBool("sTotalRenovationCostsLckd");
            dataLoan.sTotalRenovationCosts_rep = GetString("sTotalRenovationCosts");

            dataLoan.sQualTermCalculationType = (QualTermCalculationType)GetInt("sQualTermCalculationType");
            dataLoan.sQualTerm_rep = GetString("sQualTerm");

            if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges))
            {
                dataLoan.sConstructionPeriodMon_rep = GetString("sConstructionPeriodMon");
                dataLoan.sConstructionPeriodIR_rep = GetString("sConstructionPeriodIR");
                dataLoan.sConstructionImprovementAmt_rep = GetString("sConstructionImprovementAmt");
                dataLoan.sLandCost_rep = GetString("sLandCost");
            }

            LoanInfo.BindDataFromControls(dataLoan, dataApp, this);

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("aOccT", dataApp.aOccT);
            SetResult("sApprVal", dataLoan.sApprVal_rep);
            SetResult("sCcTemplateNm", dataLoan.sCcTemplateNm);
            SetResult("sLPurposeT", dataLoan.sLPurposeT);
            SetResult("sIsStudentLoanCashoutRefi", dataLoan.sIsStudentLoanCashoutRefi);
            SetResult("sCltvR", dataLoan.sCltvR_rep);
            SetResult("sDownPmtPc", dataLoan.sDownPmtPc_rep);
            SetResult("sDue", dataLoan.sDue_rep);
            SetResult("sEquityCalc", dataLoan.sEquityCalc_rep);
            SetResult("sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep);
            SetResult("sFinMethDesc", dataLoan.sFinMethDesc);
            SetResult("sFinMethT", dataLoan.sFinMethT);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            SetResult("sLAmtLckd", dataLoan.sLAmtLckd);
            SetResult("sHighPricedMortgageT", dataLoan.sHighPricedMortgageT);
            SetResult("sHighPricedMortgageTLckd", dataLoan.sHighPricedMortgageTLckd);
            SetResult("sIsEmployeeLoan", dataLoan.sIsEmployeeLoan);
            SetResult("sIsNewConstruction", dataLoan.sIsNewConstruction);

            if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges))
            {
                SetResult("sConstructionPeriodMon", dataLoan.sConstructionPeriodMon_rep);
                SetResult("sConstructionPeriodIR", dataLoan.sConstructionPeriodIR_rep);
                SetResult("sConstructionImprovementAmt", dataLoan.sConstructionImprovementAmt_rep);
                SetResult("sLandCost", dataLoan.sLandCost_rep);
            }

            // 7/25/2005 dd - OPM 1064. Since we don't set sLNm to dataobject therefore don't replace the current value in UI.
            if (dataLoan.DataState == E_DataState.InitSave)
            {
                SetResult("sLNm", dataLoan.sLNm);
            }

            SetResult("sCrmNowLeadId",dataLoan.sCrmNowLeadId);
            SetResult("sLT", dataLoan.sLT);
            SetResult("sLTotI", dataLoan.sLTotI_rep);
            SetResult("sLiaMonLTot", dataLoan.sLiaMonLTot_rep);
            SetResult("sLienPosT", dataLoan.sLienPosT);
            SetResult("sLpTemplateNm", dataLoan.sLpTemplateNm);
            SetResult("sLoanProductIdentifier", dataLoan.sLoanProductIdentifier);
            SetResult("sLtvR", dataLoan.sLtvR_rep);
            SetResult("sMonthlyPmt", dataLoan.sMonthlyPmt_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sPresLTotPersistentHExp", dataLoan.sPresLTotPersistentHExp_rep);
            SetResult("sProHazIns", dataLoan.sProHazIns_rep);
            SetResult("sProHazInsBaseAmt", dataLoan.sProHazInsBaseAmt_rep);
            SetResult("sProHazInsMb", dataLoan.sProHazInsMb_rep);
            SetResult("sProHazInsR", dataLoan.sProHazInsR_rep);
            SetResult("sProHazInsT", dataLoan.sProHazInsT);
            SetResult("sProHoAssocDues", dataLoan.sProHoAssocDues_rep);
            SetResult("sProMIns", dataLoan.sProMIns_rep);
            SetResult("sProMInsLckd", dataLoan.sProMInsLckd);
            SetResult("sProOFinPmt", dataLoan.sProOFinPmt_rep);
            SetResult("sProOHExp", dataLoan.sProOHExp_rep);
            SetResult("sProOHExpLckd", dataLoan.sProOHExpLckd);
            SetResult("sProRealETx", dataLoan.sProRealETx_rep);
            SetResult("sProRealETxBaseAmt", dataLoan.sProRealETxBaseAmt_rep);
            SetResult("sProRealETxR", dataLoan.sProRealETxR_rep);
            SetResult("sProRealETxT", dataLoan.sProRealETxT);
            SetResult("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            SetResult("sPurchPrice", dataLoan.sPurchPrice_rep);
            SetResult("sQualBottomR", dataLoan.sQualBottomR_rep);
            SetResult("sQualIR", dataLoan.sQualIR_rep);
            this.SetResult(nameof(dataLoan.sQualIRLckd), dataLoan.sQualIRLckd);
            SetResult("sQualTopR", dataLoan.sQualTopR_rep);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sTerm", dataLoan.sTerm_rep);
            SetResult("sOptionArmTeaserR", dataLoan.sOptionArmTeaserR_rep);
            SetResult("sIsOptionArm", dataLoan.sIsOptionArm);
            SetResult("sIsQRateIOnly", dataLoan.sIsQRateIOnly);
            SetResult("sOriginalAppraisedValue", dataLoan.sOriginalAppraisedValue_rep);
            SetResult("sFHAPurposeIsStreamlineRefiWithoutAppr", dataLoan.sFHAPurposeIsStreamlineRefiWithoutAppr ? "1" : "0");
            SetResult("sIsLineOfCredit", dataLoan.sIsLineOfCredit);
            SetResult("sCreditLineAmt", dataLoan.sCreditLineAmt_rep);

            SetResult("sIsRenovationLoan", dataLoan.sIsRenovationLoan);
            SetResult("sFHASpAsIsVal", dataLoan.sFHASpAsIsVal_rep);
            SetResult("sInducementPurchPrice", dataLoan.sInducementPurchPrice_rep);

            SetResult("sTotalRenovationCostsLckd", dataLoan.sTotalRenovationCostsLckd);
            SetResult("sTotalRenovationCosts", dataLoan.sTotalRenovationCosts_rep);

            SetResult("sQualTermCalculationType", dataLoan.sQualTermCalculationType);
            SetResult("sQualTerm", dataLoan.sQualTerm_rep);

            this.SetResult("sHomeIsMhAdvantageTri", dataLoan.sHomeIsMhAdvantageTri, forDropdown: true);

            LoanInfo.LoadDataForControls(dataLoan, dataApp, this);
        }

        private void GenerateNewLoanNumber()
        {
            try
            {
                SetResult("NewLoanName", LendersOffice.Services.LoanInfoService.GenerateNewLoanNumber(sLId));
            }
            catch (CBaseException e)
            {
                SetResult("ErrMsg", "Failed to generate a new loan number.");

                Tools.LogError("Failed to generate a new loan number.", e);
            }
        }
    }

    public class UpfrontMIPServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(UpfrontMIPServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sProMInsT = (E_PercentBaseT)GetInt("sProMInsT"); // opm 146309
            dataLoan.sLT = (E_sLT)GetInt("sLT");
            dataLoan.sFfUfmipR_rep = GetString("sFfUfmipR");
            dataLoan.sFfUfmip1003_rep = GetString("sFfUfmip1003");
            dataLoan.sFfUfmip1003Lckd = GetBool("sFfUfmip1003Lckd");
            dataLoan.sMipFrequency = (E_MipFrequency)GetInt("sMipFrequency");

            if (GetBool("m_sFfUfMipIsBeingFinancedChanged"))
            {
                dataLoan.OnChangesFfUfMipIsBeingFinanced(GetBool("sFfUfMipIsBeingFinanced"));
            }
            else
            {
                dataLoan.sFfUfMipIsBeingFinanced = GetBool("sFfUfMipIsBeingFinanced");
            }

            dataLoan.sMipPiaMon_rep = GetString("sMipPiaMon");
            dataLoan.sUfCashPdLckd = GetBool("sUfCashPdLckd");
            dataLoan.sUfCashPd_rep = GetString("sUfCashPd");

            dataLoan.sProMInsR_rep = GetString("sProMInsR");
            dataLoan.sProMInsMb_rep = GetString("sProMInsMb");
            dataLoan.sProMIns_rep = GetString("sProMIns");
            dataLoan.sProMInsLckd = GetBool("sProMInsLckd");
            ////dataLoan.sProMInsT = (E_PercentBaseT)GetInt("sProMInsT"); // opm 146309, set this before setting sLT.
            dataLoan.sIncludeUfmipInLtvCalc = GetBool("sIncludeUfmipInLtvCalc", false);

            dataLoan.sMiLenderPaidCoverage_rep = GetString("sMiLenderPaidCoverage");
            dataLoan.sMiCommitmentRequestedD_rep = GetString("sMiCommitmentRequestedD");
            dataLoan.sMiCommitmentReceivedD_rep = GetString("sMiCommitmentReceivedD");
            dataLoan.sMiCommitmentExpirationD_rep = GetString("sMiCommitmentExpirationD");
            dataLoan.sMiCertId = GetString("sMiCertId");
            dataLoan.sProMInsMon_rep = GetString("sProMInsMon");
            dataLoan.sProMIns2Mon_rep = GetString("sProMIns2Mon");
            dataLoan.sProMInsR2_rep = GetString("sProMInsR2");
            dataLoan.sProMInsCancelLtv_rep = GetString("sProMInsCancelLtv");
            dataLoan.sProMInsCancelAppraisalLtv_rep = GetString("sProMInsCancelAppraisalLtv");
            dataLoan.sProMInsCancelMinPmts_rep = GetString("sProMInsCancelMinPmts");
            dataLoan.sProMInsMidptCancel = GetBool("sProMInsMidptCancel");
            dataLoan.sMiInsuranceT = (E_sMiInsuranceT)GetInt("sMiInsuranceT");
            dataLoan.sMiCompanyNmT = (E_sMiCompanyNmT)GetInt("sMiCompanyNmT");
            dataLoan.sLenderUfmipR_rep = GetString("sLenderUfmipR");
            dataLoan.sLenderUfmip_rep = GetString("sLenderUfmip");
            dataLoan.sLenderUfmipLckd = GetBool("sLenderUfmipLckd");
            dataLoan.sUfmipIsRefundableOnProRataBasis = GetBool("sUfmipIsRefundableOnProRataBasis");
            dataLoan.sLAmtCalc_rep = GetString("sLAmtCalc");

            if (dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && dataLoan.sIsHousingExpenseMigrated)
            {
                dataLoan.sMInsRsrvEscrowedTri = GetBool("sMInsRsrvEscrowedTri") ? E_TriState.Yes : E_TriState.No;
                dataLoan.sMInsRsrvMonLckd = GetBool("sMInsRsrvMonLckd");
                dataLoan.sMInsRsrvMon_rep = GetString("sMInsRsrvMon");
                dataLoan.sMIPaymentRepeat = (E_DisbursementRepIntervalT)GetInt("sMIPaymentRepeat");

                if (dataLoan.sMIPaymentRepeat == E_DisbursementRepIntervalT.Annual)
                {
                    int[,] initEscAcc = new int[13, 9];
                    Array.Copy(dataLoan.sInitialEscrowAcc, initEscAcc, 117);
                    initEscAcc[0, 2] = GetInt("MICush", 0);
                    initEscAcc[1, 2] = GetInt("MIJan", 0);
                    initEscAcc[2, 2] = GetInt("MIFeb", 0);
                    initEscAcc[3, 2] = GetInt("MIMar", 0);
                    initEscAcc[4, 2] = GetInt("MIApr", 0);
                    initEscAcc[5, 2] = GetInt("MIMay", 0);
                    initEscAcc[6, 2] = GetInt("MIJun", 0);
                    initEscAcc[7, 2] = GetInt("MIJul", 0);
                    initEscAcc[8, 2] = GetInt("MIAug", 0);
                    initEscAcc[9, 2] = GetInt("MISep", 0);
                    initEscAcc[10, 2] = GetInt("MIOct", 0);
                    initEscAcc[11, 2] = GetInt("MINov", 0);
                    initEscAcc[12, 2] = GetInt("MIDec", 0);
                    dataLoan.sInitialEscrowAcc = initEscAcc;
                }
            }

        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sLT", dataLoan.sLT);
            SetResult("sFfUfmipR", dataLoan.sFfUfmipR_rep);
            SetResult("sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
            SetResult("sFfUfmip1003Lckd", dataLoan.sFfUfmip1003Lckd);
            SetResult("sMipFrequency", dataLoan.sMipFrequency);
            SetResult("sFfUfMipIsBeingFinanced", dataLoan.sFfUfMipIsBeingFinanced);
            SetResult("sMipPiaMon", dataLoan.sMipPiaMon_rep);
            SetResult("sUfCashPd", dataLoan.sUfCashPd_rep);
            SetResult("sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep);
            SetResult("sUfCashPdLckd", dataLoan.sUfCashPdLckd);

            SetResult("sProMInsT", dataLoan.sProMInsT);
            SetResult("sProMInsR", dataLoan.sProMInsR_rep);
            SetResult("sProMInsBaseAmt", dataLoan.sProMInsBaseAmt_rep);
            SetResult("sProMInsBaseMonthlyPremium", dataLoan.sProMInsBaseMonthlyPremium_rep);
            SetResult("sProMInsMb", dataLoan.sProMInsMb_rep);
            SetResult("sProMIns", dataLoan.sProMIns_rep);
            SetResult("sProMInsLckd", dataLoan.sProMInsLckd);
            SetResult("sIncludeUfmipInLtvCalc", dataLoan.sIncludeUfmipInLtvCalc);

            SetResult("sMiCompanyNmT", dataLoan.sMiCompanyNmT);
            SetResult("sMiInsuranceT", dataLoan.sMiInsuranceT);
            SetResult("sProMInsMidptCancel", dataLoan.sProMInsMidptCancel);
            SetResult("sProMInsCancelMinPmts", dataLoan.sProMInsCancelMinPmts_rep);
            SetResult("sProMInsCancelLtv", dataLoan.sProMInsCancelLtv_rep);
            SetResult("sProMInsCancelAppraisalLtv", dataLoan.sProMInsCancelAppraisalLtv_rep);
            SetResult("sProMIns2", dataLoan.sProMIns2_rep);
            SetResult("sProMInsR2", dataLoan.sProMInsR2_rep);
            SetResult("sProMIns2Mon", dataLoan.sProMIns2Mon_rep);
            SetResult("sProMInsMon", dataLoan.sProMInsMon_rep);
            SetResult("sMiCertId", dataLoan.sMiCertId);
            SetResult("sMiCommitmentExpirationD", dataLoan.sMiCommitmentExpirationD_rep);
            SetResult("sMiCommitmentReceivedD", dataLoan.sMiCommitmentReceivedD_rep);
            SetResult("sMiCommitmentRequestedD", dataLoan.sMiCommitmentRequestedD_rep);
            SetResult("sMiLenderPaidCoverage", dataLoan.sMiLenderPaidCoverage_rep);
            SetResult("sLenderUfmipR", dataLoan.sLenderUfmipR_rep);
            SetResult("sLenderUfmip", dataLoan.sLenderUfmip_rep);
            SetResult("sLenderUfmipLckd", dataLoan.sLenderUfmipLckd);
            SetResult("sUfmipIsRefundableOnProRataBasis", dataLoan.sUfmipIsRefundableOnProRataBasis);
            SetResult("sForceSinglePaymentMipFrequency", dataLoan.sForceSinglePaymentMipFrequency.ToString());
            SetResult("IsVersionAt_V4_ForceSinglePmtUfmipTypeForFhaVaUsda", LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V4_ForceSinglePmtUfmipTypeForFhaVaUsda_Opm237060).ToString());
            SetResult("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);

            if (dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && dataLoan.sIsHousingExpenseMigrated)
            {
                SetResult("sMInsRsrvEscrowedTri", dataLoan.sMInsRsrvEscrowedTri == E_TriState.Yes);
                SetResult("sIssMInsRsrvEscrowedTriReadOnly", dataLoan.sIssMInsRsrvEscrowedTriReadOnly);
                SetResult("sMInsRsrvMonLckd", dataLoan.sMInsRsrvMonLckd);
                SetResult("sMInsRsrvMon", dataLoan.sMInsRsrvMon_rep);
                SetResult("sMInsRsrv", dataLoan.sMInsRsrv_rep);
                SetResult("sMIPaymentRepeat", dataLoan.sMIPaymentRepeat);

                // MI column is 2.
                int[,] initEscAcc = dataLoan.sInitialEscrowAcc;
                SetResult("MICush", initEscAcc[0, 2].ToString());
                SetResult("MIJan", initEscAcc[1, 2].ToString());
                SetResult("MIFeb", initEscAcc[2, 2].ToString());
                SetResult("MIMar", initEscAcc[3, 2].ToString());
                SetResult("MIApr", initEscAcc[4, 2].ToString());
                SetResult("MIMay", initEscAcc[5, 2].ToString());
                SetResult("MIJun", initEscAcc[6, 2].ToString());
                SetResult("MIJul", initEscAcc[7, 2].ToString());
                SetResult("MIAug", initEscAcc[8, 2].ToString());
                SetResult("MISep", initEscAcc[9, 2].ToString());
                SetResult("MIOct", initEscAcc[10, 2].ToString());
                SetResult("MINov", initEscAcc[11, 2].ToString());
                SetResult("MIDec", initEscAcc[12, 2].ToString());
            }
        }
    }

    public class OtherFinancingServiceItem : AbstractBackgroundServiceItem
    {
        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected override void Process(string methodName)
        {
            switch (methodName) {
                case "UnlinkLoan":
                    UnlinkLoan();
                    break;
                case "UpdateLinkLoan":
                    UpdateLinkLoan();
                    break;
            }
        }

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(OtherFinancingServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            if (dataLoan.sLienPosT == E_sLienPosT.First)
            {
                dataLoan.sSubFin_rep = GetString("sSubFin");
                dataLoan.sConcurSubFin_rep = GetString("sConcurSubFin");
                dataLoan.sSubFinIR_rep = GetString("sSubFinIR");
                dataLoan.sSubFinTerm_rep = GetString("sSubFinTerm");
                dataLoan.sSubFinMb_rep = GetString("sSubFinMb");
                dataLoan.sSubFinPmt_rep = GetString("sSubFinPmt");
                dataLoan.sSubFinPmtLckd = GetBool("sSubFinPmtLckd");

                dataLoan.sIsIOnlyForSubFin = GetBool("sIsIOnlyForSubFin");
                dataLoan.sOtherLFinMethT = (E_sFinMethT)GetInt("sOtherLFinMethT");
                dataLoan.sIsOFinNew = GetBool("sIsOFinNew");
                dataLoan.sIsOFinCreditLineInDrawPeriod = GetBool("sIsOFinCreditLineInDrawPeriod");
                dataLoan.sLayeredTotalForgivableBalance_rep = GetString("sLayeredTotalForgivableBalance");
            }
            else
            {
                dataLoan.s1stMtgOrigLAmt_rep = GetString("s1stMtgOrigLAmt");
                dataLoan.sRemain1stMBal_rep = GetString("sRemain1stMBal");
                dataLoan.sRemain1stMPmt_rep = GetString("sRemain1stMPmt");
                dataLoan.sIsIOnlyForSubFin = GetBool("sIsIOnlyForSubFin_2");
                dataLoan.sOtherLFinMethT = (E_sFinMethT)GetInt("sOtherLFinMethT_2");
                dataLoan.sIsOFinNew = GetBool("sIsOFinNew_2");
                dataLoan.sLpIsNegAmortOtherLien = GetBool("sLpIsNegAmortOtherLien");
            }
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            if (dataLoan.sLienPosT == E_sLienPosT.First)
            {
                SetResult("sSubFin", dataLoan.sSubFin_rep);
                SetResult("sConcurSubFin", dataLoan.sConcurSubFin_rep);
                SetResult("sSubFinPmt", dataLoan.sSubFinPmt_rep);
                SetResult("sSubFinPmtLckd", dataLoan.sSubFinPmtLckd);
                SetResult("sSubFinIR", dataLoan.sSubFinIR_rep);
                SetResult("sSubFinTerm", dataLoan.sSubFinTerm_rep);
                SetResult("sSubFinMb", dataLoan.sSubFinMb_rep);
                SetResult("sIsIOnlyForSubFin", dataLoan.sIsIOnlyForSubFin);
                SetResult("sIsOFinNew", dataLoan.sIsOFinNew);
                SetResult("sIsOFinCreditLineInDrawPeriod", dataLoan.sIsOFinCreditLineInDrawPeriod);
                SetResult("sOtherLFinMethT", dataLoan.sOtherLFinMethT);
                SetResult("sLayeredTotalForgivableBalance", dataLoan.sLayeredTotalForgivableBalance_rep);
            }
            else
            {
                SetResult("s1stMtgOrigLAmt", dataLoan.s1stMtgOrigLAmt_rep);
                SetResult("sRemain1stMBal", dataLoan.sRemain1stMBal_rep);
                SetResult("sRemain1stMPmt", dataLoan.sRemain1stMPmt_rep);
                SetResult("sIsIOnlyForSubFin_2", dataLoan.sIsIOnlyForSubFin);
                SetResult("sIsOFinNew_2", dataLoan.sIsOFinNew);
                SetResult("sLpIsNegAmortOtherLien", dataLoan.sLpIsNegAmortOtherLien);
                SetResult("sOtherLFinMethT_2", dataLoan.sOtherLFinMethT);
            }
        }

        private void UnlinkLoan()
        {
            // 6/29/2005 kb - We now (case 2218) secure delete requests, so
            // get the calling user's credentials and check.

            Tools.UnlinkLoans(BrokerUser, sLId);
        }

        private void UpdateLinkLoan()
        {
            var brokerDB = this.BrokerUser.BrokerDB;
            var linkedLoanUpdater = new CPopulate80To20(brokerDB, this.sLId, Tools.GetLinkedLoanId(brokerDB.BrokerID, this.sLId));
            try
            {
                var updatedFirstLienData = linkedLoanUpdater.UpdateLinkedLoans();
                linkedLoanUpdater.AddMsgToUser("The linked loan has been updated. Please verify its data", false);

                SetResult("sIsOFinCreditLineInDrawPeriod", updatedFirstLienData.sIsOFinCreditLineInDrawPeriod);
                SetResult("sIsIOnlyForSubFin", updatedFirstLienData.sIsIOnlyForSubFin);
                SetResult("sSubFin", updatedFirstLienData.sSubFin);
                SetResult("sConcurSubFin", updatedFirstLienData.sConcurSubFin);
                SetResult("sSubFinIR", updatedFirstLienData.sSubFinIR);
                SetResult("sSubFinTerm", updatedFirstLienData.sSubFinTerm);
                SetResult("sSubFinMb", updatedFirstLienData.sSubFinMb);
                SetResult("sSubFinPmt", updatedFirstLienData.sSubFinPmt);
                SetResult("sSubFinPmtLckd", updatedFirstLienData.sSubFinPmtLckd);
                SetResult("sOtherLFinMethT", updatedFirstLienData.sOtherLFinMethT);
            }
            catch (CBaseException ex)
            {
                linkedLoanUpdater.AddMsgToUser("Update Error.", false);
                Tools.LogError("UpdateLinkLoan Error: " + ex.Message, ex);
            }
            finally
            {
                SetResult("MsgToUser", linkedLoanUpdater.MsgToUser);
            }
        }
    }

    public partial class LoanInfoService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("LoanInfoUC", new LoanInfoServiceItem());
            AddBackgroundItem("UpfrontMIP", new UpfrontMIPServiceItem());
            AddBackgroundItem("OtherFinancing", new OtherFinancingServiceItem());
        }
    }
}
