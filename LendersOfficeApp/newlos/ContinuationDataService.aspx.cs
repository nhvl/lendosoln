﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using System.Collections.Generic;

namespace LendersOfficeApp.newlos
{
    public class ContinuationDataServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(ContinuationDataServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.a1003ContEditSheet = GetString("a1003ContEditSheet");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public partial class ContinuationDataService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new ContinuationDataServiceItem());
        }
    }
}
