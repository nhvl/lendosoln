﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using System.Collections.Generic;
using LendersOffice.Migration;

namespace LendersOfficeApp.newlos
{
    public class LoanOfficerLicensingInfoServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        private E_sDisclosureRegulationT disclosureRegulationTBeforeBind;
        private E_sDisclosureRegulationT disclosureRegulationTOnLoad;

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(LoanOfficerLicensingInfoServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            this.disclosureRegulationTBeforeBind = dataLoan.sDisclosureRegulationT;

            dataApp.aIntrvwrMethodT = (E_aIntrvwrMethodT)GetInt("aIntrvwrMethodT");
            dataApp.aIntrvwrMethodTLckd = GetBool("aIntrvwrMethodTLckd");
            dataApp.a1003InterviewD_rep = GetString("a1003InterviewD");
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V17_UpdateApplicationSubmittedCalculation))
            {
                dataApp.a1003InterviewDLckd = GetBool("a1003InterviewDLckd");
            }

            IPreparerFields interviewer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
            interviewer.PreparerName = GetString("LoanOfficerName");
            interviewer.LicenseNumOfAgent = GetString("LoanOfficerLicenseNumber");
            interviewer.LicenseNumOfCompany = GetString("BrokerLicenseNumber");
            interviewer.Phone = GetString("LoanOfficerPhone");
            interviewer.EmailAddr = GetString("LoanOfficerEmail");
            interviewer.CompanyName = GetString("BrokerName");
            interviewer.StreetAddr = GetString("BrokerStreetAddr");
            interviewer.City = GetString("BrokerCity");
            interviewer.State = GetString("BrokerState");
            interviewer.Zip = GetString("BrokerZip");
            interviewer.PhoneOfCompany = GetString("BrokerPhone");
            interviewer.FaxOfCompany = GetString("BrokerFax");
            interviewer.AgentRoleT = (E_AgentRoleT)GetInt("CFM_AgentRoleT");
            interviewer.LoanOriginatorIdentifier = GetString("LoanOfficerLoanOriginatorIdentifier");
            interviewer.CompanyLoanOriginatorIdentifier = GetString("LoanOfficerCompanyLoanOriginatorIdentifier");
            interviewer.IsLocked = GetBool("CFM_IsLocked");
            interviewer.Update();
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            this.disclosureRegulationTOnLoad = dataLoan.sDisclosureRegulationT;

            SetResult("aIntrvwrMethodT", dataApp.aIntrvwrMethodT);
            SetResult("aIntrvwrMethodTLckd", dataApp.aIntrvwrMethodTLckd);
            SetResult("a1003InterviewD", dataApp.a1003InterviewD_rep);
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V17_UpdateApplicationSubmittedCalculation))
            {
                SetResult("a1003InterviewDLckd", dataApp.a1003InterviewDLckd);
            }

            IPreparerFields interviewer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("LoanOfficerName", interviewer.PreparerName);
            SetResult("LoanOfficerLicenseNumber", interviewer.LicenseNumOfAgent);
            SetResult("BrokerLicenseNumber", interviewer.LicenseNumOfCompany);
            SetResult("LoanOfficerPhone", interviewer.Phone);
            SetResult("LoanOfficerEmail", interviewer.EmailAddr);
            SetResult("BrokerName", interviewer.CompanyName);
            SetResult("BrokerStreetAddr", interviewer.StreetAddr);
            SetResult("BrokerCity", interviewer.City);
            SetResult("BrokerState", interviewer.State);
            SetResult("BrokerZip", interviewer.Zip);
            SetResult("BrokerPhone", interviewer.PhoneOfCompany);
            SetResult("BrokerFax", interviewer.FaxOfCompany);
            SetResult("LoanOfficerLoanOriginatorIdentifier", interviewer.LoanOriginatorIdentifier);
            SetResult("LoanOfficerCompanyLoanOriginatorIdentifier", interviewer.CompanyLoanOriginatorIdentifier);

            bool refreshNavigation = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                this.disclosureRegulationTBeforeBind != this.disclosureRegulationTOnLoad;
            if (refreshNavigation)
            {
                SetResult("RefreshNavigation", refreshNavigation);
            }
        }
    }

    public partial class LoanOfficerLicensingInfoService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new LoanOfficerLicensingInfoServiceItem());
        }
    }
}
