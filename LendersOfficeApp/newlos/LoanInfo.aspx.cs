namespace LendersOfficeApp.newlos
{
    using System;
    using DataAccess;
    using LendersOffice.Common;

    public partial class LoanInfo : BaseLoanPage
    {
        protected LoanInfoUserControl LoanInfoUC;
        protected UpfrontMIP UpfrontMIP;
        protected OtherFinancing OtherFinancing;
        protected String HeaderDisplay;

        internal static void BindDataFromControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem item)
        {
            LoanInfoUserControl.BindDataFromControls(loan, application, item);
        }

        internal static void LoadDataForControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem item)
        {
            LoanInfoUserControl.LoadDataForControls(loan, application, item);
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            RegisterJsObject("MipCalcTypeNonFha_map", DataAccess.CPageBase.MipCalcTypeNonFha_map);
            RegisterJsObject("MipCalcTypeFha_map", DataAccess.CPageBase.MipCalcTypeFha_map);
            Tabs.AddToQueryString("loanid", LoanID.ToString());
            var infoTabName = "This Loan Information";
            if (IsLeadPage)
            {
                Tabs.AddToQueryString("isLead", "t");
                infoTabName = "This Lead Information";
            }
            Tabs.RegisterControl(infoTabName, LoanInfoUC);
            Tabs.RegisterControl("Mortgage Insurance / Funding Fee", UpfrontMIP);
            Tabs.RegisterControl("Other Financing", OtherFinancing);

            Tabs.Visible = false;

            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterCSS("jquery-ui-1.11.custom.css");
            this.RegisterJsScript("LQBPopup.js");
        }

        override protected void OnPreRender(EventArgs e)
        {

            switch (Tabs.TabIndex)
            {
                case 0:
                    this.PageTitle = "Loan Information";
                    this.PageID = "LoanInformation";
                    HeaderDisplay = "This Loan Info";
                    break;
                case 1:
                    this.PageTitle = "Upfront MIP/FF";
                    this.PageID = "UpfrontMIP";
                    HeaderDisplay = "Upfront MIP/FF";
                    break;

                case 2:
                    this.PageTitle = "Other Financing";
                    this.PageID = "Subfinancing";
                    HeaderDisplay = "Other Financing";
                    break;

            }
            base.OnPreRender(e);
        }
		public bool CanEditLoanProgramName
		{
			// 5/29/07 mf. The LoanInfo page is very commonly viewed, so we expose this
			// setting for the child loaninfo control. This way we will not load the broker
			// twice every time. This property must be accessed after the Init when we retrieve.
			get { return BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowEditingLoanProgramName); }
		}
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

    }
}
