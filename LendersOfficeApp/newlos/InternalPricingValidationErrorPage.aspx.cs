﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Web.UI.HtmlControls;
using DataAccess.LoanValidation;

namespace LendersOfficeApp.newlos
{
    public partial class InternalPricingValidationErrorPage : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(InternalPricingValidationErrorPage));
            dataLoan.InitLoad();

            HtmlGenericControl ul = new HtmlGenericControl("ul");

            var resultList = Tools.RunInternalPricingValidation(dataLoan, this.BrokerUser);
            if (resultList.ErrorCount > 0)
            {
                foreach (var result in resultList.ErrorList)
                {
                    HtmlGenericControl li = new HtmlGenericControl("li");
                    li.InnerText = result.Message;
                    ul.Controls.Add(li);
                }
            }
            ErrorMessagePlaceHolder.Controls.Add(ul);
        }
    }
}
