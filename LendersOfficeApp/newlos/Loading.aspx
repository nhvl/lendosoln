<%@ Page language="c#" Codebehind="Loading.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Loading" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>Loading ...</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </HEAD>
<body class=RightBackground MS_POSITIONING="FlowLayout">
<form id=Loading method=post runat="server">
<table height="100%" width="100%">
  <% if (!m_hasError) { %>
  <TR><TD class=FieldLabel vAlign=center align=middle>Loading<IMG src="../images/status.gif" ></TD></TR>
  <% } else { %>
  <tr>
    <td vAlign=center align=middle class="FieldLabel">
      <ml:EncodedLabel id=m_errorLabel runat="server" ForeColor="Red" Font-Bold="True"></ml:EncodedLabel>
    </TD>
  </TR>
  <% } %>
</TABLE>
</FORM>
  </body>
</HTML>
