﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VersionMismatchDialog.aspx.cs" Inherits="LendersOfficeApp.newlos.VersionMismatchDialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Error</title>
    <style>
      .ErrorMessage { padding-left:10px; color:red; font-weight:bold;}
      .ButtonPanel { width:200px; margin-left:auto;margin-right:auto}
    </style>
</head>
<body bgcolor="gainsboro">
  <script type="text/javascript">
    function _init()
    {
      resize(300, 150);
    }
    function btnDiscardChanges_OnClick()
    {
      var arg = window.dialogArguments || {};
      arg.Action = 'Reload';
      onClosePopup(arg);
    }
    function btnPrintPage_OnClick()
    {
      var arg = window.dialogArguments || {};
      arg.Action = 'Print';
      onClosePopup(arg);
    }
  </script>
    <h4 class="page-header">ERROR</h4>
    <form id="form1" runat="server">
    <div class="ErrorMessage">
    Another user has updated this loan file. Your changes have not been saved.
    <br />
    <br />
    </div>
    <div class="ButtonPanel">
    <input id="btnDiscardChanges" type="button" value="Discard Changes" onclick="btnDiscardChanges_OnClick();"/>
    <input id="btnPrintPage" type="button" value="Print Page" onclick="btnPrintPage_OnClick();"/>
    </div>
    </form>
</body>
</html>
