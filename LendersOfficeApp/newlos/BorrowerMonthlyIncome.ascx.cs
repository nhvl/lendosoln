using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOfficeApp.common;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos
{

	public partial  class BorrowerMonthlyIncome : BaseLoanUserControl, IAutoLoadUserControl
	{
        protected string m_sBName = string.Empty;
        protected string m_sCName = string.Empty;

        protected string brokerID = "";

        private void BindOtherIncomeDescription(MeridianLink.CommonControls.ComboBox cb) 
        {
            LendersOffice.Admin.BrokerDB brokerDB = this.BrokerDB;
            brokerID = BrokerUser.BrokerId.ToString();

            Tools.Bind_aOIDescT(cb);
        }

        private void PageInit(object sender, System.EventArgs e) 
        {
            BindOtherIncomeDescription(OtherIncomeCombo);
        }
        public void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(BorrowerMonthlyIncome));
            dataLoan.InitLoad();
            sFileVersion = dataLoan.sFileVersion;
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            m_sBName = dataApp.aBNm;
            m_sCName = dataApp.aCNm;
      

            aBBaseI.Text = dataApp.aBBaseI_rep;
            aBOvertimeI.Text = dataApp.aBOvertimeI_rep;
            aBBonusesI.Text = dataApp.aBBonusesI_rep;
            aBCommisionI.Text = dataApp.aBCommisionI_rep;
            aBDividendI.Text = dataApp.aBDividendI_rep;
            aBNetRentI1003.Text = dataApp.aBNetRentI1003_rep;
            aCBaseI.Text = dataApp.aCBaseI_rep;
            aCOvertimeI.Text = dataApp.aCOvertimeI_rep;
            aCBonusesI.Text = dataApp.aCBonusesI_rep;
            aCCommisionI.Text = dataApp.aCCommisionI_rep;
            aCDividendI.Text = dataApp.aCDividendI_rep;
			aCNetRentI1003.Text = dataApp.aCNetRentI1003_rep;
			aNetRentI1003Lckd.Checked = dataApp.aNetRentI1003Lckd;

            aBSpPosCf.Text = dataApp.aBSpPosCf_rep;
            aCSpPosCf.Text = dataApp.aCSpPosCf_rep;
            aTotSpPosCf.Text = dataApp.aTotSpPosCf_rep;

            aBTotI.Text = dataApp.aBTotI_rep;
            aCTotI.Text = dataApp.aCTotI_rep;

            OtherIncomeJson.Value = ObsoleteSerializationHelper.JsonSerialize(dataApp.aOtherIncomeList);

            aTotI.Text = dataApp.aTotI_rep;
            aBTotOI.Text = dataApp.aBTotOI_rep;
            aCTotOI.Text = dataApp.aCTotOI_rep;
            aTotOI.Text = dataApp.aTotOI_rep;

            aTotNetRentI1003.Text = dataApp.aTotNetRentI1003_rep;
            aTotDividendI.Text = dataApp.aTotDividendI_rep;
            aTotCommisionI.Text = dataApp.aTotCommisionI_rep;
            aTotBonusesI.Text = dataApp.aTotBonusesI_rep;
            aTotBaseI.Text = dataApp.aTotBaseI_rep;
            aTotOvertimeI.Text = dataApp.aTotOvertimeI_rep;

            sLTotI.Text = dataLoan.sLTotI_rep;
        }

        public void SaveData() 
        {
            // Save is handled via BorrowerInfoService.aspx.cs
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();

            this.Init += new System.EventHandler(this.PageInit);

			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

        }
		#endregion
    }
}
