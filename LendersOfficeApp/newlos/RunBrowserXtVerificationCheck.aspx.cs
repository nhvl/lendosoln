﻿#region Generated Code
namespace LendersOfficeApp.newlos
#endregion
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a post-log in page to verify that BrowserXT
    /// is not installed on a client's machine.
    /// </summary>
    [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Log-in issues are SLA 1 violations. At support's request, redirect user to next step on error.")]
    public partial class RunBrowserXtVerificationCheck : BasePage
    {
        /// <summary>
        /// Adds a log for when a user has BrowserXT installed
        /// and is intercepted during the log in process to 
        /// display the warning or hard-stop message.
        /// </summary>
        [System.Web.Services.WebMethod]
        public static void AddInterceptLog()
        {
            try
            {
                var currentUser = PrincipalFactory.CurrentPrincipal;
                var log = $"User {currentUser.LoginNm} ({currentUser.UserId}, {currentUser.BrokerDB.CustomerCode}) has BrowserXT installed and their LQB log-in has been intercepted.";
                DataAccess.Tools.LogInfo("[BrowserXT Login Intercept]", log);
            }
            catch
            {
                // No-op
            }
        }        

        /// <summary>
        /// Gets the forced compatibility mode for the page.
        /// </summary>
        /// <returns>
        /// The forced compatibility mode.
        /// </returns>
        /// <remarks>
        /// Require IE 8 so we have access to the object
        /// literal notation for AddBypassLog without 
        /// losing the ability to generate ActiveX objects
        /// in higher modes like Edge.
        /// </remarks>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode() => E_XUAComaptibleValue.IE8;

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                this.EnableJqueryMigrate = false;
                base.OnInit(e);
            }
            catch (Exception exc)
            {
                DataAccess.Tools.LogError("Exception encountered during BrowserXT verification, redirecting to next login step.", exc);
                RequestHelper.DoNextPostLoginTask(PostLoginTask.RunBrowserXtVerificationCheck);
            }
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var currentUser = PrincipalFactory.CurrentPrincipal;

                this.HardStopDate.InnerText = ConstApp.BrowserXtVerificationHardStopDate.ToString("MMMM d, yyyy");

                this.RegisterJsGlobalVariables("HardStopIfDetected", ShouldHardStopIfBrowserXtDetected());
                this.RegisterJsGlobalVariables("BrowserXtInfoPdfDownloadKey", ConstAppDavid.DownloadFile_BrowserXtInfo);
                this.RegisterJsGlobalVariables("BrowserXtUninstallDirectionsDownloadKey", ConstAppDavid.DownloadFile_BrowserXtUninstallDirections);
                this.RegisterJsGlobalVariables("BrowserXtUninstallerDownloadKey", ConstAppDavid.DownloadFile_BrowserXtUninstaller);
            }
            catch (Exception exc)
            {
                DataAccess.Tools.LogError("Exception encountered during BrowserXT verification, redirecting to next login step.", exc);
                RequestHelper.DoNextPostLoginTask(PostLoginTask.RunBrowserXtVerificationCheck);
            }
        }

        /// <summary>
        /// Handles the "Continue" button click.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void ContinueButton_Click(object sender, EventArgs e)
        {
            RequestHelper.DoNextPostLoginTask(PostLoginTask.RunBrowserXtVerificationCheck);
        }

        /// <summary>
        /// Determines whether the page should provide a hard stop
        /// if BrowserXT is detected.
        /// </summary>
        /// <returns>
        /// True if the page should provide a hard stop, false otherwise.
        /// </returns>
        private static bool ShouldHardStopIfBrowserXtDetected() => DateTime.Now.Date >= ConstApp.BrowserXtVerificationHardStopDate;
    }
}