<%@ Page language="c#" Codebehind="FreddieExport.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FreddieExport" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import namespace="LendersOffice.Security" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import namespace="LendersOffice.Constants" %>
<%@ Import namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<head runat="server">
  <title>Loan Product Advisor</title>
  <style type="text/css">
      .WarningIcon { height: 25px; vertical-align: bottom; }
      #btnGoToMain { width: 265px; margin-right: 30px; }
      #btnSendToLp { width: 200px; }
      #btnViewLp { width: 255px; }
  </style>
</head>
<body bgColor=gainsboro scroll=yes>
<script type="text/javascript">
  var g_lpWindow = null;
  var g_pollingInterval = 1000;
  var g_asyncId = '';
    
  function f_displayMask(bDisplay) {

    f_displayBlockMask(bDisplay);
    parent.treeview.f_displayBlockMask(bDisplay);
    parent.info.f_displayBlockMask(bDisplay);

    parent.header.f_disableCloseLink(bDisplay);
  }

  function _postGetAllFormValues(args)
  {
      args.sDUDwnPmtSrc = JSON.stringify($('#sDUDownPmtSrcs').downpayments('serialize'));
      args.sIsUseDUDwnPmtSrc = $('#DUDownPaymentSourceJSON').is(':checked');
  }
  
  function _postSaveCallback(values) {
      $('#DUDownPaymentSource1003').prop('checked', values.sIsUseDUDwnPmtSrc === 'False');
      $('#DUDownPaymentSourceJSON').prop('checked', values.sIsUseDUDwnPmtSrc === 'True');
      $('#sDUDownPmtSrcs').downpayments('initialize', JSON.parse(values.sDUDwnPmtSrc) );
  }
  
  function _init() {
    registerPostSaveMeCallback(_postSaveCallback);
           
    $('#sDUDownPmtSrcs').downpayments({
        'downpaymentSources': downpaymentSources, 
        'data' : downPaymentData,
        readonly : $('#_ReadOnly').val() == 'True' || $('#DUDownPaymentSource1003').is(':checked')
    });
    
    $('#DUDownPaymentSource1003').change(function(){
        $('#sDUDownPmtSrcs').downpayments('setReadOnly', true);
    });
    $('#DUDownPaymentSourceJSON').change(function(){
        $('#sDUDownPmtSrcs').downpayments('setReadOnly', false);
    });
    onclick_sBuydown();
    
    lockField(<%=AspxTools.JsGetElementById(sFredieReservesAmtLckd)%>, 'sFredieReservesAmt');
     <%= AspxTools.JsGetElementById(sFreddieArmIndexT) %>.disabled = !<%= AspxTools.JsGetElementById(sFreddieArmIndexTLckd) %>.checked;

    if (ML.CanRunLp) {
        $(".WarningIcon").css("visibility", "hidden");
    } else {
        $(".WarningIcon").attr('title', ML.RunLpDenialReason);
        $(".btnSendToLp").prop('disabled', 'true');
    }
}

function onclick_sBuydown() {
    var b = <%= AspxTools.JsGetElementById(sBuydown) %>.checked;
    
    <%=AspxTools.JsGetElementById(sBuydownContributorT) %>.disabled = !b;
    <%= AspxTools.JsGetElementById(sBuydownContributorT)%>.style.backgroundColor = b ? "" : gReadonlyBackgroundColor;
    
  }
    function openNonSeamlessLogin() {
        if (isDirty() && ConfirmSaveNoCancel())
        {
            f_saveMe();
        }
        var popupSettings = { 
            height: 540, 
            width: 540, 
            hideCloseButton: true 
        };
        LQBPopup.Show(
            ML.VirtualRoot + '/newlos/LPANonSeamlessLogin.aspx?loanid=' + ML.sLId,
            popupSettings);
    }

    function f_submitSys2Sys()
    {
        if (isDirty() && ConfirmSaveNoCancel())
        {
            f_saveMe();
        }
        var popupSettings = { 
            height: 800, 
            width: '80%', 
            hideCloseButton: true 
        };
        LQBPopup.Show(
            ML.VirtualRoot + '/newlos/SeamlessLPA/SeamlessLpa.aspx#/audit?loanid=' + ML.sLId, 
            popupSettings);
    }

  function f_displayErrorMessage(msg) {
    document.getElementById('errorMessagePanel').style.display = '';
    document.getElementById('errorMessagePanel').innerHTML = '<pre>ERROR:\n\n' + msg + '</pre>';
  }
  function f_hideErrorMessage() {
    document.getElementById('errorMessagePanel').style.display = 'none';
  }
  function f_launch(entryPoint,bIsBlockEditor) {
    
      var b = saveMe();
      if (b == false) {
        return;
      }
    
    var bMaximize = true;
    var bCenter = false;
  
    var w = 0;
    var h = 0;
    var left = 0;
    var top = 0;
  
    if (!bMaximize) {
      var preferedWidth = 1024;
      var preferedHeight = 768;
      w = preferedWidth < screen.width ? preferedWidth : screen.width - 10;
      h = preferedHeight < screen.height ? preferedHeight : screen.height - 60;
    } else {
      w = screen.availWidth - 10;
      h = screen.availHeight - 50;
    }
    
    // opm 228663 ejm - LP will not allow us to open their site within a frame on a page with a different domain. So instead we'll open the page that loads up the LP page. No frame. 
    g_lpWindow = window.open('LoanProspectorMain.aspx?loanid=' + ML.sLId + '&entrypoint=' + entryPoint, 'ExportLP' + <%= AspxTools.JsString(LoanID.ToString("N"))%>, 'width=' + w + ',height=' + h + ',left=' + left + ',top=' + top + ',menu=no,status=yes,location=no,resizable=yes,scrollbars=yes');
    
    //g_lpWindow = window.open('LoanProspectorMainFrame.aspx?loanid=' + ML.sLId + '&entrypoint=' + entryPoint, 'ExportLP' + <%= AspxTools.JsString(LoanID.ToString("N"))%>, 'width=' + w + ',height=' + h + ',left=' + left + ',top=' + top + ',menu=no,status=yes,location=no,resizable=yes,scrollbars=yes');
  
    if (null == g_lpWindow) {
      alert('Unable to open new window.  This may be caused by a pop-up blocker program on your computer.  Please contact Technical Support for assistance.');
    } else {
      if (bIsBlockEditor) {
        f_displayMask(true);
        f_detectLpWindowStatus();
      }

    }
    return false;
    

  }
  function f_viewFeedback() {
    var w = openWindowWithArguments('services/ViewLPFeedbackFrame.aspx?loanid=' + ML.sLId, 'ExportLP' + <%= AspxTools.JsString(LoanID.ToString("N"))%>, 'toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=780,height=600');  
    if (null == w) {
      alert('Unable to open new window.  This may be caused by a pop-up blocker program on your computer.  Please contact Technical Support for assistance.');
    } else {
      w.focus();
    }
    return false;
  }
  function f_detectLpWindowStatus() {
    if (null != g_lpWindow && g_lpWindow.closed) {
        f_displayMask(false);
        f_load('FreddieExport.aspx?loanid=' + ML.sLId);
        g_lpWindow = null;
        return;
    }
    window.setTimeout(f_detectLpWindowStatus, g_pollingInterval);
  }
  var g_mcrnList = <%= AspxTools.JsArray(m_mcrnList) %>;
</script>

<form id=FreddieExport method=post runat="server">
<table cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td class="MainRightHeader" nowrap><ml:EncodedLiteral id="headerMsg" runat="server">Submit to Freddie Mac's Loan Product Advisor</ml:EncodedLiteral></td>
  </tr>
  <tr>
    <td style="padding-left: 5px">
      <table class="InsetBorder" cellspacing="0" cellpadding="0" width="500">
        <tr>
          <td>
            <table cellspacing="0" cellpadding="0">
              <tr>
                <td class="FieldLabel" style="padding-left: 5px" >LPA Loan Id</td>
                <td><asp:TextBox ID="sFreddieLoanId" runat="server" /></td>
              </tr>
              <tr>
                <td class="FieldLabel" style="padding-left: 5px" >LPA Transaction ID</td>
                <td><asp:TextBox ID="sFreddieTransactionId" runat="server" /></td>
              </tr>
              <tr>
                <td class="FieldLabel" style="padding-left: 5px" nowrap>LPA AUS Key Number</td>
                <td><asp:TextBox ID="sLpAusKey" runat="server" /></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td style="padding-left: 5px">
      <table class="InsetBorder" cellspacing="0" cellpadding="0" width="500">
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">Planned Unit Development Indicator</td>
          <td><asp:CheckBox ID="sSpIsInPud" runat="server" /></td>
        </tr>
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">Building Status Type</td>
          <td><asp:DropDownList ID="sBuildingStatusT" runat="server" Width="250px" /></td>
        </tr>
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">New Construction</td>
          <td><asp:DropDownList ID="sFreddieConstructionT" runat="server" /></td>
        </tr>
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">Property Estimated Value Amount</td>
          <td><ml:MoneyTextBox ID="sSpMarketVal" runat="server" Width="90" preset="money" /></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td style="padding-left: 5px">
      <table class="InsetBorder" cellspacing="0" cellpadding="0" width="500">
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">Secondary Financing Refinance Indicator</td>
          <td><asp:CheckBox ID="sPayingOffSubordinate" runat="server" /></td>
        </tr>
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">Cash Out Amount</td>
          <td><ml:MoneyTextBox ID="sProdCashoutAmt" runat="server" Width="90" preset="money" /></td>
        </tr>
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">HELOC Maximum Balance Amount</td>
          <td><ml:MoneyTextBox ID="sHelocCreditLimit" runat="server" Width="90" preset="money" /></td>
        </tr>
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">Subordinate Lien - HELOC Amount</td>
          <td><ml:MoneyTextBox ID="sHelocBal" runat="server" Width="90" preset="money" /></td>
        </tr>
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">Sales Concession Amount</td>
          <td><ml:MoneyTextBox ID="sFHASalesConcessions" runat="server" Width="90" preset="money" /></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td style="padding-left: 5px">
      <table class="InsetBorder" cellspacing="0" cellpadding="0" width="500">
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">Is Buydown Rate?</td>
          <td><asp:CheckBox ID="sBuydown" onclick="onclick_sBuydown();" runat="server" /></td>
        </tr>
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">Buydown Contributor Type</td>
          <td>
            <asp:DropDownList ID="sBuydownContributorT" runat="server" />
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td style="padding-left: 5px">
      <table class="InsetBorder" id="Table1" cellspacing="0" cellpadding="0" width="500">
        <tr>
          <td class="FieldLabel" nowrap>Offering Identifier</td>
          <td nowrap>
            <ml:ComboBox ID="sFredAffordProgId" runat="server" Width="200px" /></td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap>Reserves</td>
          <td nowrap><ml:MoneyTextBox ID="sFredieReservesAmt" runat="server" Width="90" preset="money" /> <asp:CheckBox ID="sFredieReservesAmtLckd" runat="server" Text="Lock" onclick="refreshCalculation();" /> </td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap>Negative Amortization Type</td>
          <td nowrap><asp:DropDownList ID="sNegAmortT" runat="server" /></td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap>ARM Index Type</td>
          <td nowrap><asp:DropDownList ID="sFreddieArmIndexT" runat="server" />&nbsp;<asp:CheckBox ID="sFreddieArmIndexTLckd" runat="server" onclick="refreshCalculation();" /></td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap>Borrower Paid FHA/VA Closing Cost Amount</td>
          <td nowrap><ml:MoneyTextBox ID="sFreddieFHABorrPdCc" runat="server" Width="90" preset="money" ReadOnly="True" /></td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap>Borrower Financed FHA Discount Points Amount</td>
          <td nowrap><ml:MoneyTextBox ID="sFHAFinancedDiscPtAmt" runat="server" Width="90" preset="money" /></td>
        </tr>
      </table>
    </td>
  </tr>
    <tr>
        <td style="PADDING-LEFT:5px">
            <table id="sDUDownPmtSrcs" class="InsetBorder" cellpadding="0" cellspacing="0" >
            <colgroup>
                <col width="130" />
                <col width="330" />
                <col width="200" />
                <col width="50" />
            </colgroup>
                <thead>
                    <tr>
                        <td class="LoanFormHeader" colspan="4">Down Payment</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:RadioButton runat="server" ID="DUDownPaymentSource1003"  CssClass="FieldLabel"  Text="Use 1003 downpayment source" GroupName="DwnSrc" />
                            <asp:RadioButton runat="server" ID="DUDownPaymentSourceJSON"  CssClass="FieldLabel" Text="Enter new downpayment sources" GroupName="DwnSrc" />
                        </td>
                    </tr>
                </thead>
            </table>
        </td>
    </tr>
  <tr>
    <td><hr></td>
  </tr>
  <tr>
    <td style="padding-left: 5px">
      <ml:EncodedLabel runat="server" ID="errMsg" CssClass="Error" Visible="false">You do not have permission to export.</ml:EncodedLabel>
      <div id="exportButtonsPanel" runat="server">
        <asp:PlaceHolder ID="NonSeamlessButtons" runat="server" >
            <input type="button" id="btnGoToMain" value="Go to Loan Product Advisor Main Menu" onclick="return f_launch('h', true);" class="buttonStyle" nohighlight runat="server"> 
            <input type="button" id="btnSendToLp" value="Send to Loan Product Advisor" onclick="return openNonSeamlessLogin();" class="buttonStyle btnSendToLp" nohighlight runat="server" /> 
            <img src="../images/warning25x25.png" class="WarningIcon" />
        </asp:PlaceHolder>
        
        <asp:PlaceHolder ID="SeamlessButtons" runat="server">
            <input type="button" id="btnBetaSys2Sys" value="Send to Loan Product Advisor (Seamless)" onclick="return f_submitSys2Sys();" class="buttonStyle btnSendToLp" nohighlight runat="server" />
            <img src="../images/warning25x25.png" class="WarningIcon" />
        </asp:PlaceHolder>

        <input type="button" id="btnViewLp" value="View Loan Product Advisor Feedback" onclick="return f_viewFeedback();" class="buttonStyle" nohighlight runat="server" /> 
      </div>
    </td>
  </tr>
  <tr>
    <td>
      <div id="errorMessagePanel" style="display:none; color:Red;background-color:White; padding:10px;margin:5px;"></div>
    </td>
  </tr>
</table>
</form>
    </body>
</HTML>
