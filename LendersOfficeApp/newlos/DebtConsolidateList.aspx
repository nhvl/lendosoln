<%@ Page language="c#" Codebehind="DebtConsolidateList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.DebtConsolidateList" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>DebtConsolidateList</title>
  </HEAD>
  <body MS_POSITIONING="FlowLayout" scroll="yes" leftmargin="0" bgcolor="gainsboro" style="CURSOR:default">
	<script type="text/javascript">

    function f_getSpecialValues(args) {
      args["Alimony_Pmt"] = parent.getAlimony_Pmt();
      args["Alimony_ComNm"] = parent.getAlimony_ComNm();
      args["Alimony_RemainMons"] = parent.getAlimony_RemainMons();
      args["Alimony_NotUsedInRatio"] = parent.getAlimony_NotUsedInRatio();
      args["ChildSupport_Pmt"] = parent.getChildSupport_Pmt();
      args["ChildSupport_ComNm"] = parent.getChildSupport_ComNm();
      args["ChildSupport_RemainMons"] = parent.getChildSupport_RemainMons();
      args["ChildSupport_NotUsedInRatio"] = parent.getChildSupport_NotUsedInRatio();
      args["JobRelated1_ComNm"] = parent.getJobRelated1_ComNm();
      args["JobRelated1_Pmt"] = parent.getJobRelated1_Pmt();
      args["JobRelated1_NotUsedInRatio"] = parent.getJobRelated1_NotUsedInRatio();
      args["JobRelated2_ComNm"] = parent.getJobRelated2_ComNm();
      args["JobRelated2_Pmt"] = parent.getJobRelated2_Pmt();    
      args["JobRelated2_NotUsedInRatio"] = parent.getJobRelated2_NotUsedInRatio();
    }
    function f_setSpecialValues(args) {
      parent.updateTotal(args["aLiaBalTot"], args["aLiaMonTot"], args["aLiaPdOffTot"], args["sLiaBalLTot"], args["sLiaMonLTot"], args["sRefPdOffAmt"]);
      parent.setAlimony_ComNm(args["Alimony_ComNm"]);
      parent.setAlimony_Pmt(args["Alimony_Pmt"]);
      parent.setAlimony_RemainMons(args["Alimony_RemainMons"]);
      parent.setAlimony_NotUsedInRatio(args["Alimony_NotUsedInRatio"] == "True");
      parent.setChildSupport_ComNm(args["ChildSupport_ComNm"]);
      parent.setChildSupport_Pmt(args["ChildSupport_Pmt"]);
      parent.setChildSupport_RemainMons(args["ChildSupport_RemainMons"]);
      parent.setChildSupport_NotUsedInRatio(args["ChildSupport_NotUsedInRatio"] == "True");
      parent.setJobRelated1_ComNm(args["JobRelated1_ComNm"]);
      parent.setJobRelated1_Pmt(args["JobRelated1_Pmt"]);
      parent.setJobRelated1_NotUsedInRatio(args["JobRelated1_NotUsedInRatio"] == "True");
      parent.setJobRelated2_ComNm(args["JobRelated2_ComNm"]);
      parent.setJobRelated2_Pmt(args["JobRelated2_Pmt"]);    
      parent.setJobRelated2_NotUsedInRatio(args["JobRelated2_NotUsedInRatio"] == "True");
      
    }
    function f_onPaidOffCheck(index)
    {
  var cbWillBePdOff = document.getElementById("willBePdOff_" + index);
  var cbUsedInRatio = document.getElementById("usedInRatio_" + index);
  
  if (null != cbUsedInRatio) {
    cbUsedInRatio.checked = false;
  }
  f_refreshCalculation();
  
}
function f_backgroundCalculation() {
  var args = f_gatherData();

  var result = gService.loanedit.call("CalculateData", args);
  
  if (!result.error) {
      f_setSpecialValues(result.value);
      callFrameMethod(parent, "SingleIFrame", "f_update", [result]);
  } else {
    alert(result.UserMessage);
    parent.f_reloadPage();
  }

}
function f_refreshCalculation() {
    var calcMode = callFrameMethod(parent, "SingleIFrame", "f_getCalcMode");
    if (calcMode == 0) {
        f_backgroundCalculation();
    }
}

function f_getRadioValue(id) {
  var o = document.getElementById(id);
  if (null != o) {
    return o.checked ? "True" : "False";
  }
  return "";
}

function saveMe() {
  var args = f_gatherData();
  args.sFileVersion = document.getElementById("sFileVersion").value;
  var result = gService.loanedit.call("SaveData", args);
  if (!result.error) {
    parent.clearDirty();
    if (result.value["sFileVersion"] != null)
    {
      var o = document.getElementById("sFileVersion");
      if (null != o) o.value = result.value["sFileVersion"];
    }
  } 
  else
  {
    if (result.ErrorType === 'VersionMismatchException')
    {
      f_displayVersionMismatchSingleEdit();
    }
    else if (result.ErrorType === 'LoanFieldWritePermissionDenied') {
        f_displayFieldWriteDeniedSingleEdit(result.UserMessage);
    }
    else
    {
      alert(result.UserMessage);
      parent.f_reloadPage();
      
    }
  }
}
function f_displayVersionMismatchSingleEdit()
{
  showModal('/newlos/VersionMismatchDialog.aspx', null, null, null, function(args){ 
    if (args.Action === 'Print')
    {
        lqbPrintByFrame(parent);
    }
    parent.location.href = parent.location.href;
  },{ hideCloseButton: true });
  
}
function f_displayFieldWriteDeniedSingleEdit(msg) {
    showModal('/newlos/LoanSaveDenial.aspx?msg=' + msg, null, null, null, function(args){
      if (args.Action === 'Print') {
          lqbPrintByFrame(parent);
      }
      location.href = location.href;
    },{ hideCloseButton: true });
}
function f_gatherData() {
  var args = callFrameMethod(parent, "SingleIFrame", "f_getFormValues");

  args["loanid"] = ML.sLId;
  args["applicationid"] = ML.aAppId;
  args["count"] = g_aIds.length;
  for (var i = 0; i < g_aIds.length; i++) {
    args["RecordId_" + i] = g_aIds[i];
    args["WillBePdOff_" + i] = f_getRadioValue("willBePdOff_" + i);
    args["UsedInRatio_" + i] = f_getRadioValue("usedInRatio_" + i);
  }
  f_getSpecialValues(args);
  
  return args;
}
function updateDirtyBit_callback() { parent.updateDirtyBit(); }

    </script>

    <form id="DebtConsolidateList" method="post" runat="server">
      <asp:DataGrid id=m_dg runat="server" DataKeyField="RecordId" BorderColor="Gainsboro" AutoGenerateColumns="False" Width="100%" enableviewstate="False">
        <alternatingitemstyle cssclass="GridItem" />
        <itemstyle cssclass="GridItem" />
        <headerstyle wrap="False" cssclass="GridHeader" />
        <footerstyle cssclass="GridFooter" />

        <columns>
          <asp:TemplateColumn SortExpression="OwnerT" HeaderText="Owner">
            <headerstyle width="40px" />
            <itemstyle width="40px" />
            <itemtemplate>
              <%# AspxTools.HtmlString(DisplayOwnerType(DataBinder.Eval(Container.DataItem, "OwnerT").ToString())) %>
            </ItemTemplate>
          </asp:TemplateColumn>
          <asp:TemplateColumn SortExpression="DebtT" HeaderText="Debt Type">
            <headerstyle width="70px" />
            <itemstyle width="70px" />
            <itemtemplate>
<%# AspxTools.HtmlString(DisplayLiabilityType(DataBinder.Eval(Container.DataItem, "DebtT").ToString())) %>
</ItemTemplate>
</asp:TemplateColumn>
<asp:templatecolumn headertext="Company" sortexpression="ComNm">
  <itemtemplate>
    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ComNm").ToString()) %>
  </itemtemplate>
</asp:templatecolumn>
<asp:TemplateColumn SortExpression="Bal" HeaderText="Balance">
<headerstyle width="80px">
</HeaderStyle>

<itemstyle horizontalalign="Right" width="80px">
</ItemStyle>

<itemtemplate>
<%# AspxTools.HtmlString(DisplayMoneyString(DataBinder.Eval(Container.DataItem, "Bal").ToString())) %>
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="Pmt" HeaderText="Payment">
<headerstyle wrap="False" width="80px">
</HeaderStyle>

<itemstyle horizontalalign="Right" width="80px">
</ItemStyle>

<itemtemplate>
<%# AspxTools.HtmlString(DisplayMoneyString(DataBinder.Eval(Container.DataItem, "Pmt").ToString())) %>
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="WillBePdOff" HeaderText="Pd Off">
  <headerstyle width="40px" />
  <itemstyle width="40px" HorizontalAlign="Center" />
  <itemtemplate>
    <%# AspxTools.HtmlControl(DisplayPayoffCheckbox(Container.DataItem)) %>
  </ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="NotUsedInRatio" HeaderText="Used in Ratio">
  <headerstyle width="80px" />
  <itemstyle width="80px" />
  <itemtemplate>
    <%#AspxTools.HtmlControl(DisplayUsedInRatioCheckbox(Container.DataItem)) %>
  </ItemTemplate>
</asp:TemplateColumn>


</Columns>
      </asp:DataGrid>

     </form>
	
  </body>
</HTML>
