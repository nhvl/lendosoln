<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="LiabilityRecord.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.LiabilityRecord" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../newlos/Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>Borrower Liabilities</title>
		<STYLE type="text/css">
        .PaddedSection { display: inline-block; padding-right: 20px; }
        label.padRight { display:inline-block; padding-right: 5px; }
		</STYLE>
</HEAD>
<body bgColor=gainsboro leftMargin=0 scroll=yes 
MS_POSITIONING="FlowLayout">
<script type="text/javascript">
    var oRolodex;
        
    function _init() {
      oRolodex = new cRolodex();
      parent.parent_initScreen(1);
      var DebtT = document.getElementById('DebtT');
      parent.updateVOButtons(DebtT.value);
      lockField(document.getElementById('PayoffAmtLckd'), 'PayoffAmt');
      lockField(document.getElementById('PayoffTimingLckd'), 'PayoffTiming');
      validateWillBePdOff(true);
    }
    
    function getSpecialValues(args) {
      args["Alimony_Pmt"] = parent.getAlimony_Pmt();
      args["Alimony_ComNm"] = parent.getAlimony_ComNm();
      args["Alimony_RemainMons"] = parent.getAlimony_RemainMons();
      args["Alimony_NotUsedInRatio"] = parent.getAlimony_NotUsedInRatio();
      args["ChildSupport_Pmt"] = parent.getChildSupport_Pmt();
      args["ChildSupport_ComNm"] = parent.getChildSupport_ComNm();
      args["ChildSupport_RemainMons"] = parent.getChildSupport_RemainMons();
      args["ChildSupport_NotUsedInRatio"] = parent.getChildSupport_NotUsedInRatio();
      args["JobRelated1_ComNm"] = parent.getJobRelated1_ComNm();
      args["JobRelated1_Pmt"] = parent.getJobRelated1_Pmt();
      args["JobRelated1_NotUsedInRatio"] = parent.getJobRelated1_NotUsedInRatio();
      args["JobRelated2_ComNm"] = parent.getJobRelated2_ComNm();
      args["JobRelated2_Pmt"] = parent.getJobRelated2_Pmt();    
      args["JobRelated2_NotUsedInRatio"] = parent.getJobRelated2_NotUsedInRatio();
      args["aAsstLiaCompletedNotJointly"] = parent.getAsstLiaCompletedNotJointly();
      args["sLienToPayoffTotDebt"] = parent.getLienToPayoffTotDebt();
    }
    function setSpecialValues(args) {
      parent.updateTotal(args["aLiaBalTot"], args["aLiaMonTot"], args["aLiaPdOffTot"], args["sLiaBalLTot"], args["sLiaMonLTot"], args["sRefPdOffAmt"]);
      parent.setAlimony_ComNm(args["Alimony_ComNm"]);
      parent.setAlimony_Pmt(args["Alimony_Pmt"]);
      parent.setAlimony_RemainMons(args["Alimony_RemainMons"]);
      parent.setAlimony_NotUsedInRatio(args["Alimony_NotUsedInRatio"] == "True");
      parent.setChildSupport_ComNm(args["ChildSupport_ComNm"]);
      parent.setChildSupport_Pmt(args["ChildSupport_Pmt"]);
      parent.setChildSupport_RemainMons(args["ChildSupport_RemainMons"]);
      parent.setChildSupport_NotUsedInRatio(args["ChildSupport_NotUsedInRatio"] == "True");
      parent.setJobRelated1_ComNm(args["JobRelated1_ComNm"]);
      parent.setJobRelated1_Pmt(args["JobRelated1_Pmt"]);
      parent.setJobRelated1_NotUsedInRatio(args["JobRelated1_NotUsedInRatio"] == "True");
      parent.setJobRelated2_ComNm(args["JobRelated2_ComNm"]);
      parent.setJobRelated2_Pmt(args["JobRelated2_Pmt"]);    
      parent.setJobRelated2_NotUsedInRatio(args["JobRelated2_NotUsedInRatio"] == "True");
      parent.setLienToPayoffTotDebt(args["sLienToPayoffTotDebt"]);
        var DebtT = document.getElementById('DebtT');
       parent.updateVOButtons(DebtT.value);      
    }
    
    function refreshCalculation() {
      invokeMethod("CalculateData", document.getElementById("RecordID").value);
    }
    
    function updateDirtyBit_callback() {
      parent.parent_disableSaveBtn(false);
    }
    function refreshUI(isFocus) {
      displayPropertyPanel();
      if (document.getElementById("DebtT").selectedIndex == 1) {
        document.getElementById("UsedInRatio").disabled = true;
        document.getElementById("UsedInRatio").checked = false;  
      }
      else{
        document.getElementById("UsedInRatio").disabled = false;
      }
      if (document.getElementById("MainEdit").style.display != "none" && isFocus) {
          <%= AspxTools.JsGetElementById(ComNm) %>.focus();
      }

      lockField(document.getElementById('PayoffAmtLckd'), 'PayoffAmt');
      lockField(document.getElementById('PayoffTimingLckd'), 'PayoffTiming');
      document.getElementById('PayoffTimingLckd').disabled = !document.getElementById('WillBePdOff').checked;
        
      <% if (IsReadOnly) { %>
        document.getElementById('VerifSentD').title = document.getElementById('VerifReorderedD').title = document.getElementById('VerifRecvD').title = document.getElementById('VerifExpD').title = "";
      <% } %>
    }    
    
    function onDebtTypeChange() {
      displayPropertyPanel();
      
      try{
          var debtType = document.getElementById("DebtT");
          var usedInRatio = document.getElementById("UsedInRatio");
          var isChecked =usedInRatio.checked;
          
          if (debtType.selectedIndex == 3) {
            document.getElementById('RemainMons').value = "(R)";
            usedInRatio.disabled = false;    
          } else if (debtType.selectedIndex == 1) {
            usedInRatio.disabled = true;
            usedInRatio.checked = false;  
          } else {
            usedInRatio.disabled = false;
          }
          
          if (isChecked != usedInRatio.checked)
            refreshCalculation();
        
          parent.updateVOButtons(debtType.value);
      }catch(e){}
      
      
    }

    function populateAccountName() {
      var value = document.getElementById("BorrowerName").value;
      if (document.getElementById("OwnerT").selectedIndex == 1)
        value = document.getElementById("CoborrowerName").value;
      else if (document.getElementById("OwnerT").selectedIndex == 2)
        value = document.getElementById("BorrowerName").value + " & " + document.getElementById("CoborrowerName").value;
      document.getElementById("AccNm").value = value;
    }      

    function validateUsedInRatio() { 
      refreshCalculation();
    }
    
    function validateWillBePdOff(suppressBackgroundCalculation){
        var payoffTimingLockCB = document.getElementById('PayoffTimingLckd');

        if (document.getElementById("WillBePdOff").checked) {
            document.getElementById("UsedInRatio").checked = false;

            payoffTimingLockCB.disabled = false;
        }
        else {
            payoffTimingLockCB.checked = false;
            payoffTimingLockCB.disabled = true;
        }

        lockField(payoffTimingLockCB, 'PayoffTiming');
        enableDisableBlankOption(payoffTimingLockCB.checked);

        if (!suppressBackgroundCalculation) {
            refreshCalculation();
        }
    }

    function displayPropertyPanel() {
      var oMatchedReRecordId = <%= AspxTools.JsGetElementById(MatchedReRecordId) %>;
      
      oMatchedReRecordId.disabled = document.getElementById("DebtT").selectedIndex != 1;
      oMatchedReRecordId.style.backgroundColor = document.getElementById("DebtT").selectedIndex != 1 ? "gainsboro" : "";
      
      document.getElementById("btnAddToReo").disabled = oMatchedReRecordId.disabled || oMatchedReRecordId.selectedIndex == 0;
      
    }
    
    function MatchedReRecordId_onchange() {
      var index = <%= AspxTools.JsGetElementById(MatchedReRecordId) %>.selectedIndex;
      if (index != 0) {
        <%= AspxTools.JsGetElementById(Desc) %>.value = <%= AspxTools.JsGetElementById(MatchedReRecordId) %>.options[index].text;
      }
      document.getElementById("btnAddToReo").disabled = index == 0;
    }


    function updateListIFrame(iIndex) {
      var list_updateRow = retrieveFrameProperty(parent, "ListIFrame", "list_updateRow");
      if (list_updateRow) {
        var OwnerT = getDropDownValue(document.getElementById("<%= AspxTools.ClientId(OwnerT) %>")).charAt(0);
        var DebtT = getDropDownValue(document.getElementById("<%= AspxTools.ClientId(DebtT) %>"));
        var ComNm = <%= AspxTools.JsGetElementById(ComNm) %>.value;
        var Bal = <%= AspxTools.JsGetElementById(Bal) %>.value;
        var Pmt = <%= AspxTools.JsGetElementById(Pmt) %>.value;
        var WillBePdOff = <%= AspxTools.JsGetElementById(WillBePdOff) %>.checked ? "Yes" : "No";
        var UsedInRatio = <%= AspxTools.JsGetElementById(DebtT) %>.value == <%=AspxTools.JsString(DataAccess.E_DebtT.Mortgage)%> ? "See REO" : (<%= AspxTools.JsGetElementById(UsedInRatio) %>.checked ? "Yes" : "No");
      <% if (HasPmlEnabled) { %>
        var PmlAltered = "No";
        list_updateRow(iIndex, document.getElementById("RecordID").value, PmlAltered, OwnerT, DebtT, ComNm, Bal, Pmt, WillBePdOff, UsedInRatio);
        
      <% } else { %>          
        list_updateRow(iIndex, document.getElementById("RecordID").value, OwnerT, DebtT, ComNm, Bal, Pmt, WillBePdOff, UsedInRatio);
        <% } %>
      }
    }

    function getDropDownValue(ddl) {
      return ddl.options[ddl.selectedIndex].innerText;
    }  


    function f_addToReo() {
      var bal = <%= AspxTools.JsGetElementById(Bal) %>.value;
      var pmt = <%= AspxTools.JsGetElementById(Pmt) %>.value;
      var reoId = <%= AspxTools.JsGetElementById(MatchedReRecordId) %>.value;
      
      var args = new Object();
      args["LoanID"] = ML.sLId;
      args["ApplicationID"] = ML.aAppId;
      args["ReoId"] = reoId;
      args["Bal"] = bal;
      args["Payment"] = pmt;
      
      var result = gService.singleedit.call("AddToReo", args);
      
      if (!result.error) {
        document.getElementById("sFileVersion").value = result.value["sFileVersion"];
        alert('The balance and payment information has been added to the REO record.');
      }
    }
    
    <% if (HasPmlEnabled) { %>
    function postPopulateForm(value) {
        document.getElementById("PmlAuditTrailXmlContent").innerHTML = value["PmlAuditTrailXmlContent"];
    }
    <% } %>

    function lockField(cb, tbID) {
        var clientID = "";
        if (document.getElementById("_ClientID") != null)
            clientID = document.getElementById("_ClientID").value + "_";

        var item = document.getElementById(clientID + tbID);

        if (item.tagName.toLowerCase() === 'select') {
            item.disabled = !cb.checked;
        } else {
            item.readOnly = !cb.checked;

            if (!cb.checked) {
                item.style.backgroundColor = "lightgrey";
            }
            else {
                item.style.backgroundColor = "";
            }
        }
    }
    function enableDisableBlankOption(payoffTimingLocked) {
        var payoffTiming = document.getElementById('PayoffTiming');
        var options = payoffTiming.getElementsByTagName('option');
        options[0].disabled = payoffTimingLocked;
    }

    function onExcFromUnderwritingChecked()
    {
        if (document.getElementById('ExcFromUnderwriting').checked)
        {
            document.getElementById('UsedInRatio').checked = false;
            validateUsedInRatio();
        }
    }
</script>

<form id=LiabilityRecord method=post runat="server">
<div id=MainEdit>
<table class=FormTable id=Table2 cellSpacing=0 cellPadding=0 border=0>
  <tr>
    <td>
      <TABLE class=InsetBorder id=Table10 cellSpacing=0 cellPadding=0 
      width="99%" border=0>
        <TR>
          <TD noWrap>
            <TABLE id=Table3 cellSpacing=0 cellPadding=0 border=0 
            >
              <TR>
                <TD class=FieldLabel>Owner</TD>
                <TD class=FieldLabel colSpan=3><asp:dropdownlist id=OwnerT tabIndex=10 runat="server" onchange="populateAccountName();" Width="106px">
<asp:ListItem Value="0" Selected="True">Borrower</asp:ListItem>
<asp:ListItem Value="1">Coborrower</asp:ListItem>
<asp:ListItem Value="2">Joint</asp:ListItem>
                                                </asp:dropdownlist>&nbsp;&nbsp;Debt 
                  Type&nbsp;&nbsp;<asp:dropdownlist id=DebtT tabIndex=10 runat="server" onchange="onDebtTypeChange();"></asp:dropdownlist></TD></TR>
              <tr height=5><td class=FieldLabel nowrap></td></tr>
              <tr>
				<td class=FieldLabel nowrap></td>
				<td nowrap>
					<UC:CFM runat=server CompanyNameField="ComNm" DepartmentNameField="DepartmentName" StreetAddressField="ComAddr" PhoneField="ComPhone" FaxField="ComFax" CityField="ComCity" StateField="ComState" ZipField="ComZip" ID="m_CFM"></UC:CFM>
				</td>
              </tr>
              <TR>
                <TD class=FieldLabel>Company Name</TD>
                <TD colSpan=3><asp:textbox id=ComNm tabIndex=10 runat="server" Width="243px"></asp:textbox></TD></TR>
              <TR>
                <TD class=FieldLabel>Company Address</TD>
                <TD><asp:textbox id=ComAddr tabIndex=10 runat="server" Width="243px"></asp:textbox></TD>
                <td class=FieldLabel>Phone</td>
                <td><ml:phonetextbox id=ComPhone tabIndex=15 runat="server" width="120" preset="phone" CssClass="mask"></ml:phonetextbox></td></TR>
              <TR>
                <TD class=FieldLabel>Company City</TD>
                <TD><asp:textbox id=ComCity tabIndex=10 runat="server" Width="144px"></asp:textbox><ml:statedropdownlist id=ComState tabIndex=10 runat="server"></ml:statedropdownlist><ml:zipcodetextbox id=ComZip tabIndex=10 runat="server" width="50" preset="zipcode" CssClass="mask"></ml:zipcodetextbox></TD>
                <td class=FieldLabel>Fax</td>
                <td><ml:phonetextbox id=ComFax tabIndex=15 runat="server" width="120" preset="phone" CssClass="mask"></ml:phonetextbox></td></TR>
              <TR>
                <TD class=FieldLabel>Description</TD>
                <TD colSpan=3><ml:combobox id=Desc tabIndex=20 runat="server" Width="299px"></ml:combobox></TD></TR>
              <TR>
                <TD class=FieldLabel>Property Address</TD>
                <TD noWrap colSpan=3><asp:dropdownlist id=MatchedReRecordId tabIndex=20 runat="server" onchange="MatchedReRecordId_onchange();" Width="294px"></asp:dropdownlist>&nbsp;&nbsp;<INPUT id=btnAddToReo type=button value="Add Bal / Pmt Info to REO" onclick="f_addToReo();" tabIndex=20 NoHighlight></TD></TR></TABLE></TD></TR></TABLE></td></tr>
  <TR>
    <TD></TD></TR>
  <TR>
    <TD>
      <TABLE id=Table12 cellSpacing=0 cellPadding=0 border=0 
      >
        <TR>
          <TD vAlign=top noWrap>
            <TABLE class=InsetBorder id=Table9 cellSpacing=0 cellPadding=0 
            width="99%" border=0>
              <TR>
                <TD>
                  <TABLE id=Table4 cellSpacing=0 cellPadding=0 width="100%" 
                  border=0>
                    <TR>
                      <TD class=FieldLabel noWrap colSpan=6 
                        >Account Holder Name <asp:textbox id=AccNm tabIndex=30 runat="server" Width="157px"></asp:textbox>&nbsp;Acc. 
                        Number <asp:textbox id=AccNum tabIndex=30 runat="server" Width="146px"></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel>Bal.&nbsp;</TD>
                      <TD><ml:moneytextbox id=Bal tabIndex=30 runat="server" onchange="refreshCalculation();" width="70px" preset="money" CssClass="mask"></ml:moneytextbox></TD>
                      <TD class=FieldLabel noWrap>&nbsp;Pmt.&nbsp;</TD>
                      <TD><ml:moneytextbox id=Pmt tabIndex=30 runat="server" onchange="refreshCalculation();" width="70px" preset="money" CssClass="mask"></ml:moneytextbox></TD>
                      <TD class=FieldLabel noWrap>&nbsp;Mos. 
                        Left&nbsp;<asp:textbox id=RemainMons tabIndex=30 runat="server" Width="30px"></asp:textbox>&nbsp;Rate&nbsp;<ml:percenttextbox id=Rate tabIndex=30 runat="server" width="50px" preset="percent" CssClass="mask"></ml:percenttextbox>&nbsp;Term&nbsp;<asp:textbox id=OrigTerm tabIndex=30 runat="server" Width="30px"></asp:textbox>&nbsp; 
                        Due In <asp:textbox id=Due tabIndex=30 runat="server" Width="30px"></asp:textbox></TD>
                    </TR>
                    <tr>
                      <td colspan="6" class=FieldLabel noWrap>
                          <span class="PaddedSection">
                              <asp:checkbox id=WillBePdOff onclick=validateWillBePdOff(); tabIndex=50 runat="server"></asp:checkbox>
                              <label for="WillBePdOff">Will be paid off</label>
                          </span>

                          <span class="PaddedSection">
                              <label for="PayoffAmt" class="padRight">Payoff Amt.</label>
                              <ml:MoneyTextBox runat="server" ID="PayoffAmt" width="70px" preset="money" CssClass="mask"></ml:MoneyTextBox>
                              <asp:CheckBox runat="server" ID="PayoffAmtLckd" onclick="lockField(this, 'PayoffAmt'); refreshCalculation();" />
                              <label for="PayoffAmtLckd">Lock</label>
                          </span>

                          <span>
                              <label for="PayoffTiming" class="padRight">Payoff</label>
                              <asp:DropDownList runat="server" ID="PayoffTiming"></asp:DropDownList>
                              <asp:CheckBox runat="server" ID="PayoffTimingLckd" onclick="lockField(this, 'PayoffTiming'); enableDisableBlankOption(this.checked); refreshCalculation();" />
                              <label for="PayoffTimingLckd">Lock</label>
                          </span>
                      </td>
                    </tr>
                  </TABLE></TD></TR></TABLE></TD>
          <TD vAlign=top noWrap>
            <TABLE class=InsetBorder id=Table13 cellSpacing=0 cellPadding=0 
            border=0>
              <TR>
                <TD noWrap>
                  <TABLE id=Table14 cellSpacing=0 cellPadding=0 border=0 
                  >
                    <TR>
                      <TD class=FieldLabel noWrap>Late 30 
                        &nbsp;<asp:textbox id=Late30 tabIndex=40 runat="server" Width="36px"></asp:textbox> 
                      </TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Late 60 
                        &nbsp;<asp:textbox id=Late60 tabIndex=40 runat="server" Width="36px"></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Late 
90+<asp:textbox id=Late90Plus tabIndex=40 runat="server" Width="35px"></asp:textbox></TD></TR></TABLE></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
  <TR>
    <TD>
      <TABLE class=InsetBorder id=Table7 cellSpacing=0 cellPadding=0 width="99%" 
      border=0>
        <TR>
          <TD class=FieldLabel noWrap>
              <span class="PaddedSection">
                  <asp:CheckBox ID="UsedInRatio" onclick="validateUsedInRatio();" TabIndex="50" runat="server" Text=" Debt should be included in ratios" Checked="True"></asp:CheckBox>
              </span>
              <span class="PaddedSection">
                  <asp:CheckBox ID="IsPiggyBack" TabIndex="50" runat="server" Text="Debt will be resubordinated"></asp:CheckBox>
              </span>
              <span class="PaddedSection">
                  <asp:CheckBox ID="ExcFromUnderwriting" onclick="onExcFromUnderwritingChecked()" TabIndex="50" runat="server" Text="Excl. from underwriting"></asp:CheckBox>
              </span>
          </TD>
        </TR></TABLE></TD></TR>
  <TR>
    <TD>
      <TABLE class=InsetBorder id=Table5 cellSpacing=0 cellPadding=0 width="99%" 
      border=0>
        <TR>
          <TD class=FieldLabel>
              <span class="PaddedSection">
                  <asp:CheckBox ID="IncInReposession" TabIndex="50" runat="server" Text="Incl. in repossession"></asp:CheckBox>
              </span>
              <span class="PaddedSection">
                  <asp:CheckBox ID="IncInBankruptcy" TabIndex="50" runat="server" Text="Incl. in bankruptcy"></asp:CheckBox>
              </span>
              <span class="PaddedSection">
                  <asp:CheckBox ID="IncInForeclosure" TabIndex="50" runat="server" Text="Incl. in foreclosure"></asp:CheckBox>
              </span>
          </TD>
        </TR></TABLE></TD></TR>
  <TR>
    <TD>
      <TABLE class=InsetBorder id=Table6 cellSpacing=0 cellPadding=0 width="99%" 
      border=0>
        <TR>
          <TD class=FieldLabel>Verif. Sent</TD>
          <TD><ml:datetextbox id=VerifSentD tabIndex=50 runat="server" width="65px" preset="date" CssClass="mask"></ml:datetextbox></TD>
          <TD class=FieldLabel>Re-order</TD>
          <TD class=FieldLabel><ml:datetextbox id=VerifReorderedD tabIndex=50 runat="server" width="65px" preset="date" CssClass="mask"></ml:datetextbox></TD>
          <TD class=FieldLabel>Received</TD>
          <TD><ml:datetextbox id=VerifRecvD tabIndex=50 runat="server" width="65px" preset="date" CssClass="mask"></ml:datetextbox></TD>
          <TD class=FieldLabel>Expected</TD>
          <TD><ml:datetextbox id=VerifExpD tabIndex=50 runat="server" width="65px" preset="date" CssClass="mask"></ml:datetextbox></TD></TR></TABLE></TD></TR>
                <% if (m_isFHA) { %>
                <tr>
    <td>
      <table class=InsetBorder cellSpacing=0 cellPadding=0 width="98%" 
      >
        <tr>
          <td class=LoanFormHeader>Additional Information 
            required for HUD-56001 (FHA Only)</td></tr>
        <tr>
          <td class=FieldLabel>Original Debt Amount <ml:moneytextbox id=OrigDebtAmt tabIndex=60 runat="server" onchange="refreshCalculation();" width="70px" preset="money" CssClass="mask"></ml:moneytextbox>&nbsp; 
<asp:checkbox id=IsMortFHAInsured tabIndex=60 runat="server" Text="Mortgage is FHA insured"></asp:checkbox></td></tr>
        <tr>
          <td class=FieldLabel><asp:checkbox id=IsForAuto tabIndex=60 runat="server" Text="Is Debt for Auto?"></asp:checkbox>Auto 
            Year Make <asp:textbox id=AutoYearMake tabIndex=60 runat="server" Width="50px"></asp:textbox></td></tr></table></td></tr>  
                <% } %>            
                <% if (HasPmlEnabled) { %>
                <tr>
                <td>
                <table class=InsetBorder cellspacing=0 cellpadding=0 width="99%">
                <tr><td class=FieldLabel>PML Audit History</td></tr>
                <tr><td><div id=PmlAuditTrailXmlContent></div></td></tr>
                </table>
                </td>
                </tr>
      <% } %>
      </table></div></form><uc1:cmodaldlg id=CModalDlg1 runat="server"></uc1:cmodaldlg>
    </body>
</HTML>
