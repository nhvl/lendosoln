﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreviousAddrList1003.aspx.cs" Inherits="LendersOfficeApp.newlos.Forms.PreviousAddrList1003" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:DataGrid ID="grdAddresses" runat="server" OnItemDataBound="grdAddresses_OnItemDataBound" AutoGenerateColumns="false">
        <Columns>
            <asp:TemplateColumn></asp:TemplateColumn>
            <asp:BoundColumn DataField="Street" HeaderText="Street"></asp:BoundColumn>
            <asp:BoundColumn DataField="City" HeaderText="City"></asp:BoundColumn>
            <asp:BoundColumn DataField="State" HeaderText="State"></asp:BoundColumn>
            <asp:BoundColumn DataField="Zip" HeaderText="Zip"></asp:BoundColumn>
        </Columns>
    </asp:DataGrid>
    </div>
    </form>
</body>
</html>
