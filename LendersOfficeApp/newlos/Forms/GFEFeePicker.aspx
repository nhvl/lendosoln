﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GFEFeePicker.aspx.cs" Inherits="LendersOfficeApp.newlos.Forms.GFEFeePicker" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<script type="text/javascript">
<!--
function _init() {
    resizeForIE6And7(400, 600);

    //var args = window.dialogArguments || {};
    //var data = JSON.parse(args.data);
    // populate data
}

function f_cancel() {
    var results = window.dialogArguments || {};
    results.OK = false;
    onClosePopup(results);
}

function f_selectFee(name, fieldId) {

    var results = window.dialogArguments || {};
    results.OK = true;

    var valid = true;
    if (typeof (Page_ClientValidate) == 'function')
        valid = Page_ClientValidate();
    if (!valid) {
        results.OK = false;
        onClosePopup(results);
    }

    // JSON it
    var data = {
        Name : name,
        FieldID : fieldId
    };
    results.data = JSON.stringify(data);

    onClosePopup(results);
}

function f_openSection(sectionNumber) {
    window.location = 'GFEFeePicker.aspx' + '?loanid=' + ML.sLId + '&section=' + encodeURIComponent(sectionNumber);
}

function f_toSections() {
    window.location = 'GFEFeePicker.aspx' + '?loanid=' + ML.sLId;
}
//-->
</script>

<body bgcolor="gainsboro">
<form id="EditAliasesPOA" method="post" runat="server">

<div class="InsetBorder">
    <div class="FormTableHeader">
            Select a GFE fee
        </div>

        <div id="ExplorerDiv">
        
            <%-- Selecting an item here will open up the GFE fees list --%>
            <div id="GFESectionDiv" runat="server">
                <ml:EncodedLabel ID="m_SectionMessage" runat="server" Text="Choose a GFE section:"></ml:EncodedLabel>
                <asp:GridView ID="m_GFESectionExplorer" runat="server" AutoGenerateColumns="false" EnableViewState="false" Width="380px">
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAlternatingItem" />
                    <RowStyle CssClass="GridItem"/>
                    <Columns>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="AlignLeft" />
                            <HeaderTemplate>GFE Section</HeaderTemplate>
                            <ItemTemplate>
                                <a href='#' onclick="f_openSection(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "SectionNumber").ToString()) %>)">
                                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "SectionName").ToString()) %>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            
            <%-- Selecting an item here will return the fee and fee name through window arguments --%>
            <div id="GFEFeeDiv" runat="server">
                <ml:EncodedLabel ID="m_FeeMessage" runat="server" Text="Choose a GFE fee:"></ml:EncodedLabel>
                <asp:GridView ID="m_GFEFeeExplorer" runat="server" AutoGenerateColumns="false" EnableViewState="false" Width="380px">
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAlternatingItem" />
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="" />
                            <HeaderTemplate>Section Fees</HeaderTemplate>
                            <ItemTemplate>
                                <a href='#' onclick="f_selectFee(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Name").ToString()) %>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "FieldId").ToString())%>)">
                                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Name").ToString())%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            
        </div>
        <div id="Div1">
            <input type="button" value="Cancel" onclick="f_cancel();" NoHighlight />
        </div>
</div>

</form>
</body>

</html>

