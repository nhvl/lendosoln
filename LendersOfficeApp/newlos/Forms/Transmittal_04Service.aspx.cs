namespace LendersOfficeApp.newlos.Forms
{
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using System;

    public class Transmittal_04ServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(Transmittal_04ServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            CAgentFields appraiser = dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser,E_ReturnOptionIfNotExist.CreateNew);
            appraiser.CompanyName = GetString("AppraiserCompanyName");
            appraiser.LicenseNumOfAgent = GetString("AppraiserLicense") ;
            appraiser.AgentName = GetString("AppraiserName") ;
            appraiser.Update();

            CAgentFields underwritter = dataLoan.GetAgentOfRole( E_AgentRoleT.Underwriter, E_ReturnOptionIfNotExist.CreateNew );
            underwritter.AgentName = GetString("UnderwriterName");
            underwritter.Update();

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.TransmMOriginator, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("TransmMOriginatorCompanyName");
            preparer.Update();

            dataLoan.sLenContractD_rep        = GetString("sLenContractD");
            dataLoan.sLenContactPhone         = GetString("sLenContactPhone");
            dataLoan.sLenContactTitle         = GetString("sLenContactTitle");
            dataLoan.sLenContactNm            = GetString("sLenContactNm");
            dataLoan.sTransmUwerComments      = GetString("sTransmUwerComments");

            dataLoan.sContractNum             = GetString("sContractNum");
            dataLoan.sCommitNum               = GetString("sCommitNum");
            dataLoan.sLenLNumLckd             = GetBool("sLenLNumLckd");
            dataLoan.sLenLNum                 = GetString("sLenLNum");
            dataLoan.sInvestLNumLckd          = GetBool("sInvestLNumLckd");
            dataLoan.sInvestLNum              = GetString("sInvestLNum");
            dataLoan.sLenNum                  = GetString("sLenNum");
            dataLoan.sLenZip                  = GetString("sLenZip");
            dataLoan.sLenState                = GetString("sLenState");
            dataLoan.sLenCity                 = GetString("sLenCity");
            dataLoan.sLenAddr                 = GetString("sLenAddr");
            dataLoan.sLenNm                   = GetString("sLenNm");
            dataLoan.sQualIRDeriveT           = (E_sQualIRDeriveT) GetInt("sQualIRDeriveT");
            dataLoan.sNoteIR_rep              = GetString("sNoteIR");
            dataLoan.sProjNm                  = GetString("sProjNm");
            dataLoan.s1stMtgOrigLAmt_rep      = GetString("s1stMtgOrigLAmt");
            dataLoan.s1stMOwnerT              = (E_s1stMOwnerT) GetInt("s1stMOwnerT");
            dataLoan.sBuydown                 = GetString("sBuydown") == "1";
            dataLoan.sMOrigT                  = (E_sMOrigT) GetInt("sMOrigT");
            dataLoan.sTerm_rep                = GetString("sTerm");
            dataLoan.sNoteIR_rep              = GetString("sNoteIR");
            dataLoan.sSubFin_rep              = GetString("sSubFin");
            dataLoan.sLienPosT                = (E_sLienPosT) GetInt("sLienPosT");
            
            dataLoan.sLPurposeT               = (E_sLPurposeT) GetInt("sLPurposeT");
            dataLoan.sFinMethDesc             = GetString("sFinMethDesc");
            dataLoan.sFinMethT                = (E_sFinMethT) GetInt("sFinMethT");
            dataLoan.sLT                      = (E_sLT) GetInt("sLT");
            dataLoan.sApprVal_rep             = GetString("sApprVal");
            dataLoan.sPurchPrice_rep          = GetString("sPurchPrice");
            dataLoan.sUnitsNum_rep            = GetString("sUnitsNum");
            
            dataLoan.sSpAddr                  = GetString("sSpAddr") ;
            dataLoan.sSpCity                  = GetString("sSpCity") ;
            dataLoan.sSpState                 = GetString("sSpState") ;
            dataLoan.sSpZip                   = GetString("sSpZip") ;
            dataApp.aBLastNm                  = GetString("aBLastNm") ;
            dataApp.aBMidNm                   = GetString("aBMidNm") ;
            dataApp.aBFirstNm                 = GetString("aBFirstNm") ;
            dataApp.aBSuffix                  = GetString("aBSuffix") ;
            dataApp.aBSsn                     = GetString("aBSsn") ;
            dataApp.aCLastNm                  = GetString("aCLastNm") ;
            dataApp.aCMidNm                   = GetString("aCMidNm") ;
            dataApp.aCFirstNm                 = GetString("aCFirstNm") ;
            dataApp.aCSuffix                  = GetString("aCSuffix") ;
            dataApp.aCSsn                     = GetString("aCSsn") ;

            dataApp.aOccT                     = (E_aOccT) GetInt("aOccT");

            if (!dataLoan.sIsIncomeCollectionEnabled)
            {
                dataApp.aCBaseI_rep = GetString("aCBaseI");
                dataApp.aBBaseI_rep = GetString("aBBaseI");
            }

            dataApp.aOIFrom1008Desc			= GetString("aOIFrom1008Desc");
            dataApp.aBOIFrom1008_rep		= GetString("aBOIFrom1008");
            dataApp.aCOIFrom1008_rep		= GetString("aCOIFrom1008");
            dataApp.aOpNegCf_rep			= GetString("aOpNegCf");
            dataApp.aSpNegCf_rep			= GetString("aSpNegCf");
            dataApp.aSpNegCfLckd            = GetBool("aSpNegCfLckd");
            dataApp.aOpNegCfLckd            = GetBool("aOpNegCfLckd");
            dataApp.aCSpPosCf_rep			= GetString("aCSpPosCf");
            dataApp.aBSpPosCf_rep			= GetString("aBSpPosCf");


            dataLoan.sBiweeklyPmt = GetBool("sBiweeklyPmt");
            dataLoan.sBalloonPmt = GetBool("sBalloonPmt");
            dataLoan.sEstateHeldT = (E_sEstateHeldT) GetInt("sEstateHeldT");
            dataLoan.sGseRefPurposeT = (E_sGseRefPurposeT) GetInt("sGseRefPurposeT");
            dataLoan.sGseSpT = (E_sGseSpT) GetInt("sGseSpT");
            dataLoan.sQualIR_rep = GetString("sQualIR");

            dataLoan.sApprFull = GetBool("sApprFull");
            dataLoan.sApprDriveBy = GetBool("sApprDriveBy");

            dataLoan.sHcltvR_rep = GetString("sHcltvR");
            dataLoan.sHcltvRLckd = GetBool("sHcltvRLckd");
            dataLoan.sDebtToHousingGapR_rep = GetString("sDebtToHousingGapR");
            dataLoan.sDebtToHousingGapRLckd = GetBool("sDebtToHousingGapRLckd");
            dataLoan.sIsSpReviewNoAppr = GetBool("sIsSpReviewNoAppr");
            dataLoan.sSpReviewFormNum = GetString("sSpReviewFormNum");
            dataLoan.sTransmFntc_rep = GetString("sTransmFntc");
            dataLoan.sTransmFntcLckd = GetBool("sTransmFntcLckd");
            dataLoan.sVerifAssetAmt_rep = GetString("sVerifAssetAmt");
            dataLoan.sFntcSrc = GetString("sFntcSrc");
            dataLoan.sRsrvMonNumDesc = GetString("sRsrvMonNumDesc");
            dataLoan.sInterestedPartyContribR_rep = GetString("sInterestedPartyContribR");
            dataLoan.sIsManualUw = GetBool("sIsManualUw");
            dataLoan.sIsDuUw = GetBool("sIsDuUw");
            dataLoan.sIsLpUw = GetBool("sIsLpUw");
            dataLoan.sIsOtherUw = GetBool("sIsOtherUw");
            dataLoan.sOtherUwDesc = GetString("sOtherUwDesc");
            dataLoan.sAusRecommendation = GetString("sAusRecommendation");
            dataLoan.sLpAusKey = GetString("sLpAusKey");
            dataLoan.sDuCaseId = GetString("sDuCaseId");
            dataLoan.sLpDocClass = GetString("sLpDocClass");
            dataLoan.sRepCrScore = GetString("sRepCrScore");
            dataLoan.sIsCommLen = GetString("sIsCommLen") == "1";
            dataLoan.sIsHOwnershipEdCertInFile = GetString("sIsHOwnershipEdCertInFile") == "1";
            dataLoan.sIsMOrigBroker = GetBool("sIsMOrigBroker");
            dataLoan.sIsMOrigCorrespondent = GetBool("sIsMOrigCorrespondent");
            dataLoan.sWillEscrowBeWaived = GetString("sWillEscrowBeWaived") == "0";            
            dataLoan.sTransmBuydwnTermDesc = GetString("sTransmBuydwnTermDesc");
            dataLoan.sFinMethPrintAsOtherDesc = GetString("sFinMethPrintAsOtherDesc");
            dataLoan.sFinMethodPrintAsOther = GetBool("sFinMethodPrintAsOther");
            dataLoan.sSpProjectClassFannieT = (E_sSpProjectClassFannieT) GetInt("sSpProjectClassFannieT");
            dataLoan.sApprovD_rep = GetString("sApprovD");
            dataLoan.sUnderwritingD_rep = GetString("sUnderwritingD");
            dataLoan.sSpProjectClassFreddieT = (E_sSpProjectClassFreddieT)GetInt("sSpProjectClassFreddieT");
            dataLoan.sCpmProjectId = GetString("sCpmProjectId");


            dataLoan.sQualTermCalculationType = (QualTermCalculationType)GetInt("sQualTermCalculationType");
            dataLoan.sQualTerm_rep = GetString("sQualTerm");

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V12_CalculateLevelOfPropertyReviewSection))
            {
                dataLoan.sSpValuationMethodT = (E_sSpValuationMethodT)GetInt("sSpValuationMethodT");
                dataLoan.sSpAppraisalFormT = (E_sSpAppraisalFormT)GetInt("sSpAppraisalFormT");
            }

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {

            SetResult("sLenContractD", dataLoan.sLenContractD_rep);
            SetResult("sLenContactPhone", dataLoan.sLenContactPhone);
            SetResult("sLenContactTitle", dataLoan.sLenContactTitle);
            SetResult("sLenContactNm", dataLoan.sLenContactNm);
            SetResult("sTransmUwerComments", dataLoan.sTransmUwerComments);
            SetResult("sContractNum", dataLoan.sContractNum);
            SetResult("sCommitNum", dataLoan.sCommitNum);
            SetResult("sLenLNumLckd", dataLoan.sLenLNumLckd);
            SetResult("sLenLNum", dataLoan.sLenLNum);
            SetResult("sInvestLNumLckd", dataLoan.sInvestLNumLckd);
            SetResult("sInvestLNum", dataLoan.sInvestLNum);
            SetResult("sLenNum", dataLoan.sLenNum);
            SetResult("sLenZip", dataLoan.sLenZip);
            SetResult("sLenState", dataLoan.sLenState);
            SetResult("sLenCity", dataLoan.sLenCity);
            SetResult("sLenAddr", dataLoan.sLenAddr);
            SetResult("sLenNm", dataLoan.sLenNm);
            SetResult("aTransmTotMonPmt", dataApp.aTransmTotMonPmt_rep);
            SetResult("aTransmOMonPmt", dataApp.aTransmOMonPmt_rep);
            SetResult("aSpNegCf", dataApp.aSpNegCf_rep);
            SetResult("aTransmProTotHExp", dataApp.aTransmProTotHExp_rep);
            SetResult("aTransmOtherProHExp", dataApp.aTransmOtherProHExp_rep);
            SetResult("aTransmProHoAssocDues", dataApp.aTransmProHoAssocDues_rep);
            SetResult("aTransmProMIns", dataApp.aTransmProMIns_rep);
            SetResult("aTransmProRealETx", dataApp.aTransmProRealETx_rep);
            SetResult("aTransmProHazIns", dataApp.aTransmProHazIns_rep);
            SetResult("aTransmProOFin", dataApp.aTransmPro2ndMPmt_rep);
            SetResult("aTransmPro1stM", dataApp.aTransmPro1stMPmt_rep);
            SetResult("sQualIRDeriveT", dataLoan.sQualIRDeriveT);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sQualIR", dataLoan.sQualIR_rep);
            SetResult("sCltvR", dataLoan.sCltvR_rep);
            SetResult("aQualBottomR", dataApp.aQualBottomR_rep);
            SetResult("sLtvR", dataLoan.sLtvR_rep);
            SetResult("aQualTopR", dataApp.aQualTopR_rep);
            SetResult("aTransmTotI", dataApp.aTransmTotI_rep);
            SetResult("aTotSpPosCf", dataApp.aTotSpPosCf_rep);
            SetResult("aCSpPosCf", dataApp.aCSpPosCf_rep);
            SetResult("aBSpPosCf", dataApp.aBSpPosCf_rep);
            SetResult("aSpNegCfLckd", dataApp.aSpNegCfLckd);
            SetResult("aOpNegCfLckd", dataApp.aOpNegCfLckd);
                
            SetResult("aTotNonbaseI", dataApp.aTotNonbaseI_rep);
            SetResult("aCNonbaseI", dataApp.aCNonbaseI_rep		);
            SetResult("aBNonbaseI", dataApp.aBNonbaseI_rep);
            SetResult("aTotBaseI", dataApp.aTotBaseI_rep);
            SetResult("aCBaseI", dataApp.aCBaseI_rep);
            SetResult("aBBaseI", dataApp.aBBaseI_rep);

            SetResult("aOIFrom1008Desc", dataApp.aOIFrom1008Desc);
            SetResult("aBOIFrom1008", dataApp.aBOIFrom1008_rep);
            SetResult("aCOIFrom1008", dataApp.aCOIFrom1008_rep);
            SetResult("aTotOIFrom1008", dataApp.aTotOIFrom1008_rep);

            SetResult("aTransmBTotI", dataApp.aTransmBTotI_rep);
            SetResult("aTransmCTotI", dataApp.aTransmCTotI_rep);

            SetResult("sSpAddr", dataLoan.sSpAddr);
            SetResult("sSpCity", dataLoan.sSpCity);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sSpZip", dataLoan.sSpZip);

            SetResult("aBLastNm", dataApp.aBLastNm);
            SetResult("aBMidNm", dataApp.aBMidNm);
            SetResult("aBFirstNm", dataApp.aBFirstNm);
            SetResult("aBSuffix", dataApp.aBSuffix);
            SetResult("aBSsn", dataApp.aBSsn);

            SetResult("aCLastNm", dataApp.aCLastNm);
            SetResult("aCMidNm", dataApp.aCMidNm);
            SetResult("aCFirstNm", dataApp.aCFirstNm);
            SetResult("aCSuffix", dataApp.aCSuffix);
            SetResult("aCSsn", dataApp.aCSsn);
            SetResult("sApprFull", dataLoan.sApprFull);
            SetResult("sApprDriveBy", dataLoan.sApprDriveBy);

            CAgentFields appraiser = dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser,E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("AppraiserCompanyName", appraiser.CompanyName);
            SetResult("AppraiserLicense", appraiser.LicenseNumOfAgent);
            SetResult("AppraiserName", appraiser.AgentName);

            CAgentFields underwritter = dataLoan.GetAgentOfRole( E_AgentRoleT.Underwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject );
            SetResult("UnderwriterName", underwritter.AgentName);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.TransmMOriginator, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("TransmMOriginatorCompanyName", preparer.CompanyName);

            SetResult("sProjNm", dataLoan.sProjNm);
            SetResult("s1stMtgOrigLAmt", dataLoan.s1stMtgOrigLAmt_rep);
            SetResult("s1stMOwnerT", dataLoan. s1stMOwnerT);
            SetResult("sBuydown", dataLoan.sBuydown ? "1" : "0");
            SetResult("sMOrigT", dataLoan.sMOrigT);
            SetResult("sTerm", dataLoan.sTerm_rep);
            SetResult("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sSubFin", dataLoan.sSubFin_rep);
            SetResult("sLienPosT", dataLoan.sLienPosT);
            SetResult("sLPurposeT", dataLoan.sLPurposeT);
            SetResult("sFinMethDesc", dataLoan.sFinMethDesc);
            SetResult("sFinMethT", dataLoan.sFinMethT);
            SetResult("sLT", dataLoan.sLT);
            SetResult("sApprVal", dataLoan.sApprVal_rep);
            SetResult("sPurchPrice", dataLoan.sPurchPrice_rep);
            SetResult("sUnitsNum", dataLoan.sUnitsNum_rep);
            
            SetResult("aOccT", dataApp.aOccT);
            SetResult("aOpNegCf", dataApp.aOpNegCf_rep);

            SetResult("sBalloonPmt", dataLoan.sBalloonPmt);
            SetResult("sBiweeklyPmt", dataLoan.sBiweeklyPmt);
            SetResult("sEstateHeldT", dataLoan.sEstateHeldT);
            SetResult("sGseRefPurposeT", dataLoan.sGseRefPurposeT);
            SetResult("sGseSpT", dataLoan.sGseSpT);

            SetResult("sHcltvR", dataLoan.sHcltvR_rep);
            SetResult("sHcltvRLckd", dataLoan.sHcltvRLckd);
            SetResult("sDebtToHousingGapR", dataLoan.sDebtToHousingGapR_rep);
            SetResult("sDebtToHousingGapRLckd", dataLoan.sDebtToHousingGapRLckd);
            SetResult("sIsSpReviewNoAppr", dataLoan.sIsSpReviewNoAppr);
            SetResult("sSpReviewFormNum", dataLoan.sSpReviewFormNum);
            SetResult("sTransmFntc", dataLoan.sTransmFntc_rep);
            SetResult("sTransmFntcLckd", dataLoan.sTransmFntcLckd);
            SetResult("sVerifAssetAmt", dataLoan.sVerifAssetAmt_rep);
            SetResult("sFntcSrc", dataLoan.sFntcSrc);
            SetResult("sRsrvMonNumDesc", dataLoan.sRsrvMonNumDesc);
            SetResult("sInterestedPartyContribR", dataLoan.sInterestedPartyContribR_rep);
            SetResult("sIsManualUw", dataLoan.sIsManualUw);
            SetResult("sIsDuUw", dataLoan.sIsDuUw);
            SetResult("sIsLpUw", dataLoan.sIsLpUw);
            SetResult("sIsOtherUw", dataLoan.sIsOtherUw);
            SetResult("sOtherUwDesc", dataLoan.sOtherUwDesc);
            SetResult("sAusRecommendation", dataLoan.sAusRecommendation);
            SetResult("sLpAusKey", dataLoan.sLpAusKey);
            SetResult("sDuCaseId", dataLoan.sDuCaseId);
            SetResult("sLpDocClass", dataLoan.sLpDocClass);
            SetResult("sRepCrScore", dataLoan.sRepCrScore);
            SetResult("sIsCommLen", dataLoan.sIsCommLen ? "1" : "0");
            SetResult("sIsHOwnershipEdCertInFile", dataLoan.sIsHOwnershipEdCertInFile ? "1" : "0");
            SetResult("sIsMOrigBroker", dataLoan.sIsMOrigBroker);
            SetResult("sIsMOrigCorrespondent", dataLoan.sIsMOrigCorrespondent);
            SetResult("sWillEscrowBeWaived", dataLoan.sWillEscrowBeWaived ? "0" : "1");			
            SetResult("sTransmBuydwnTermDesc", dataLoan.sTransmBuydwnTermDesc);
            SetResult("sFinMethodPrintAsOther", dataLoan.sFinMethodPrintAsOther);
            SetResult("sFinMethPrintAsOtherDesc", dataLoan.sFinMethPrintAsOtherDesc);
            SetResult("sSpProjectClassFannieT", dataLoan.sSpProjectClassFannieT);
            SetResult("sApprovD", dataLoan.sApprovD_rep);
            SetResult("sUnderwritingD", dataLoan.sUnderwritingD_rep);
            SetResult("sSpProjectClassFreddieT", dataLoan.sSpProjectClassFreddieT);
            SetResult("sCpmProjectId", dataLoan.sCpmProjectId);

            SetResult("sQualTermCalculationType", dataLoan.sQualTermCalculationType);
            SetResult("sQualTerm", dataLoan.sQualTerm_rep);

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V12_CalculateLevelOfPropertyReviewSection))
            {
                SetResult("sSpValuationMethodT", dataLoan.sSpValuationMethodT);
                SetResult("sSpAppraisalFormT", dataLoan.sSpAppraisalFormT);
            }
        }


        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "CalculateAssetTotal":
                    CalculateAssetTotal();
                    break;
            }
        }

        private void CalculateAssetTotal() 
        {
            Guid sLId = GetGuid("LoanId");

            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitLoad();

            // 5/10/2012 vm - OPM 25561 - Copy the value from aAsstLiqTot
            int nApps = dataLoan.nApps;

            decimal total = 0.0M;
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = dataLoan.GetAppData(i);
                total += dataApp.aAsstLiqTot;
            }

            SetResult("VerifiedAssetTotal", dataLoan.m_convertLos.ToMoneyString(total, FormatDirection.ToRep));

        }
    }

    public partial class Transmittal_04Service :LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize() 
        {
            AddBackgroundItem("", new Transmittal_04ServiceItem());
        }

    }
}
