﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using CommonProjectLib.Common.Lib;

namespace LendersOfficeApp.newlos.Forms
{

    public partial class GFEFeePicker : BaseLoanPage
    {
        /// <summary>
        /// All the sections of the GFE, the fees, and their respective field names.
        /// We use the field names instead of the actual values here so that it's easier to update the change of circumstances page
        ///   when the GFE changes.
        /// </summary>
        public Dictionary<string, List<Tuple<string, E_GfeSectionT, string>>> GFE
        {
            get
            {
                if (m_gfe == null)
                {
                    m_gfe = new Dictionary<string, List<Tuple<string, E_GfeSectionT, string>>>()
                    {
                        {       
                        "800", new List<Tuple<string,E_GfeSectionT, string>>()
                        {
                        Tuple.Create("801 Loan origination fee", E_GfeSectionT.B1, "sLOrigF"),
                        Tuple.Create("801 Originator compensation", E_GfeSectionT.B1, "sGfeOriginatorCompF"),
                        Tuple.Create("802 Credit or charge", E_GfeSectionT.B2, "sLDiscnt"),
                        Tuple.Create("804 Appraisal fee", E_GfeSectionT.B3, "sApprF"),
                        Tuple.Create("805 Credit report fee", E_GfeSectionT.B3, "sCrF"),
                        Tuple.Create("806 Tax service fee", E_GfeSectionT.B3, "sTxServF"),
                        Tuple.Create("807 Flood certification", E_GfeSectionT.B3, "sFloodCertificationF"),
                        Tuple.Create("808 Mortgage broker fee", E_GfeSectionT.B1, "sMBrokF"),
                        Tuple.Create("809 Lender's inspection fee", E_GfeSectionT.B1, "sInspectF"),
                        Tuple.Create("810 Processing fee", E_GfeSectionT.B1, "sProcF"),
                        Tuple.Create("811 Underwriting fee", E_GfeSectionT.B1, "sUwF"),
                        Tuple.Create("812 Wire transfer", E_GfeSectionT.B1, "sWireF"),
                        }
                    },
                {
                    "900", new List<Tuple<string, E_GfeSectionT, string>>()
                    {
                        Tuple.Create("901 Interim Interest", E_GfeSectionT.B10, "sIPia"),
                        Tuple.Create("902 Mortgage Insurance Premium", E_GfeSectionT.B3, "sMipPia"),
                        Tuple.Create("903 Hazard Insurance", E_GfeSectionT.B11, "sHazInsPia"),
                    }
                },
                {
                    "1100", new List<Tuple<string, E_GfeSectionT, string>>()
                    {
                        Tuple.Create("1102 Closing/Escrow Fee", m_dataLoan.sEscrowFGfeSection, "sEscrowF"),
                        Tuple.Create("1103 Owner's title Insurance", E_GfeSectionT.B5, "sOwnerTitleInsF"),
                        Tuple.Create("1104 Lender's title Insurance", E_GfeSectionT.B4, "sTitleInsF"),
                        Tuple.Create("1109 Doc preparation fee", m_dataLoan.sDocPrepFGfeSection, "sDocPrepF"),
                        Tuple.Create("1110 Notary fees", m_dataLoan.sNotaryFGfeSection ,"sNotaryF"),
                        Tuple.Create("1111 Attorney fees", m_dataLoan.sAttorneyFGfeSection,"sAttorneyF"),
                    }
                },
                {
                    "1200", new List<Tuple<string, E_GfeSectionT, string>>()
                    {
                        Tuple.Create("1201 Recording fees", E_GfeSectionT.B7 ,"sRecF"),
                        Tuple.Create("1204 City tax/stamps", E_GfeSectionT.B8, "sCountyRtc"),
                        Tuple.Create("1205 State tax/stamps", E_GfeSectionT.B8, "sStateRtc"),
                    }
                },
                {
                    "1300", new List<Tuple<string, E_GfeSectionT, string>>()
                    {
                        Tuple.Create("1302 Pest Inspection", E_GfeSectionT.B6, "sPestInspectF"),
                    }
                },
                    }; // end dictionary


                }// end if
                return m_gfe;
            }// end get
        }
        
        
        private Dictionary<string, List<Tuple<string, E_GfeSectionT, string>>> m_gfe;
        
        

        public string SelectedSection
        {
            get { return RequestHelper.GetSafeQueryString("section"); }
        }

        protected void PageInit(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
        }
        private CPageData m_dataLoan;

        protected void PageLoad(object sender, EventArgs e)
        {
            m_dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(GFEFeePicker));
            m_dataLoan.InitLoad();

            if (string.IsNullOrEmpty(SelectedSection))
            {
                GFESectionDiv.Visible = true;
                GFEFeeDiv.Visible = false;

                m_GFESectionExplorer.DataSource = from sectionNumber in GFE.Keys
                                                  select new { SectionNumber = sectionNumber, SectionName = sectionNumber + " Section" };
                m_GFESectionExplorer.DataBind();
            }
            else
            {
                string section = SelectedSection;

                GFESectionDiv.Visible = false;
                GFEFeeDiv.Visible = true;

                if (section == "800")
                {
                    GFE[section].Add(Tuple.Create("813 " + m_dataLoan.s800U1FDesc, m_dataLoan.s800U1FGfeSection, "s800U1F"));
                    GFE[section].Add(Tuple.Create("814 " + m_dataLoan.s800U2FDesc, m_dataLoan.s800U2FGfeSection, "s800U2F"));
                    GFE[section].Add(Tuple.Create("815 " + m_dataLoan.s800U3FDesc, m_dataLoan.s800U3FGfeSection, "s800U3F"));
                    GFE[section].Add(Tuple.Create("816 " + m_dataLoan.s800U4FDesc, m_dataLoan.s800U4FGfeSection, "s800U4F"));
                    GFE[section].Add(Tuple.Create("817 " + m_dataLoan.s800U5FDesc, m_dataLoan.s800U5FGfeSection, "s800U5F"));
                }
                if (section == "900")
                {
                    GFE[section].Add(Tuple.Create("904 " + m_dataLoan.s904PiaDesc, m_dataLoan.s904PiaGfeSection, "s904Pia"));
                    GFE[section].Add(Tuple.Create("905 VA Funding Fee", E_GfeSectionT.B3, "sVaFf"));
                    GFE[section].Add(Tuple.Create("906 " + m_dataLoan.s900U1PiaDesc, m_dataLoan.s900U1PiaGfeSection, "s900U1Pia"));
                }
                else if (section == "1100")
                {
                    GFE[section].Add(Tuple.Create("1112 " + m_dataLoan.sU1TcDesc, m_dataLoan.sU1TcGfeSection, "sU1Tc"));
                    GFE[section].Add(Tuple.Create("1113 " + m_dataLoan.sU2TcDesc, m_dataLoan.sU2TcGfeSection, "sU2Tc"));
                    GFE[section].Add(Tuple.Create("1114 " + m_dataLoan.sU3TcDesc, m_dataLoan.sU3TcGfeSection, "sU3Tc"));
                    GFE[section].Add(Tuple.Create("1115 " + m_dataLoan.sU4TcDesc, m_dataLoan.sU4TcGfeSection, "sU4Tc"));
                }
                else if (section == "1200")
                {
                    GFE[section].Add(Tuple.Create("1206 " + m_dataLoan.sU1GovRtcDesc, m_dataLoan.sU1GovRtcGfeSection, "sU1GovRtc"));
                    GFE[section].Add(Tuple.Create("1207 " + m_dataLoan.sU2GovRtcDesc, m_dataLoan.sU2GovRtcGfeSection, "sU2GovRtc"));
                    GFE[section].Add(Tuple.Create("1208 " + m_dataLoan.sU3GovRtcDesc, m_dataLoan.sU3GovRtcGfeSection, "sU3GovRtc"));
                }
                else if (section == "1300")
                {
                    GFE[section].Add(Tuple.Create("1303 " + m_dataLoan.sU1ScDesc, m_dataLoan.sU1ScGfeSection, "sU1Sc"));
                    GFE[section].Add(Tuple.Create("1304 " + m_dataLoan.sU2ScDesc, m_dataLoan.sU2ScGfeSection, "sU2Sc"));
                    GFE[section].Add(Tuple.Create("1305 " + m_dataLoan.sU3ScDesc, m_dataLoan.sU3ScGfeSection, "sU3Sc"));
                    GFE[section].Add(Tuple.Create("1306 " + m_dataLoan.sU4ScDesc, m_dataLoan.sU4ScGfeSection, "sU4Sc"));
                    GFE[section].Add(Tuple.Create("1307 " + m_dataLoan.sU5ScDesc, m_dataLoan.sU5ScGfeSection, "sU5Sc"));
                }
                
                // ideally instead of Tuples we should use the respafeeitems so we don't have to hardcode alot of it, 
                // but unfortunately that list doesn't have the field id.
                m_GFEFeeExplorer.DataSource = from fee in GFE[SelectedSection]
                                              select new { Name = ConvertToName(fee.Item1, fee.Item2), FieldId = fee.Item3 };
                
                m_GFEFeeExplorer.DataBind();

            }
        }

        private string ConvertToName(string desc, E_GfeSectionT gfeSection)
        {
            return desc + " (" + GfeSectionAsString(gfeSection) + ")";   
        }

        private static string GfeSectionAsString(E_GfeSectionT gfe)
        {
            switch (gfe)
            {
                case E_GfeSectionT.B1:
                    return "A1";
                case E_GfeSectionT.B2:
                    return "A2";
                case E_GfeSectionT.B3:
                case E_GfeSectionT.B4:
                case E_GfeSectionT.B5:
                case E_GfeSectionT.B6:
                case E_GfeSectionT.B7:
                case E_GfeSectionT.B8:
                case E_GfeSectionT.B9:
                case E_GfeSectionT.B10:
                case E_GfeSectionT.B11:
                    return gfe.ToString();
                case E_GfeSectionT.NotApplicable:
                    return "N/A";
                default:
                    Tools.LogError(new UnhandledEnumException(gfe));
                    return "";
            }
        }
            

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion
    }
}
