using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
	public partial class AmortizationTable : LendersOfficeApp.newlos.BaseLoanPage
	{
        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.DisplayCopyRight = false;
            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterCSS("jquery-ui-1.11.custom.css");

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
        }

        protected override void LoadData() 
        {
            try
            {
                CPageData dataLoan = new CTILData(LoanID);
                dataLoan.InitLoad();

                AmortTable table = dataLoan.GetAmortTable(E_AmortizationScheduleT.Standard, false);
                this.StandardAmortTable.DataSource = table.Items;
                this.StandardAmortTable.DataBind();

                // 4/17/2015 gf - For old files with sRAdjWorstIndex set to true,
                // we're going to maintain the existing behavior of only showing
                // the worst case.
                if (dataLoan.sFinMethT != E_sFinMethT.ARM || dataLoan.sRAdjWorstIndex)
                {
                    this.TabsPlaceHolder.Visible = false;
                    this.BestWorstCasePlaceHolder.Visible = false;
                }
                else
                {
                    table = dataLoan.GetAmortTable(E_AmortizationScheduleT.BestCase, false);
                    this.BestCaseAmortTable.DataSource = table.Items;
                    this.BestCaseAmortTable.DataBind();

                    table = dataLoan.GetAmortTable(E_AmortizationScheduleT.WorstCase, false);
                    this.WorstCaseAmortTable.DataSource = table.Items;
                    this.WorstCaseAmortTable.DataBind();
                }
            }
            catch (InvalidCalculationException)
            {
                // Invalid calculations don't need to be logged.
            }
            catch (Exception exc)
            {
                // Ignore. Reason for exception because of missing interest rate or loan amount.
                Tools.LogError(exc);
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
