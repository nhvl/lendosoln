﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.AntiXss;
using System.Web.UI.HtmlControls;
using LendersOffice.Common.SerializationTypes;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class ChangeOfCircumstances : BaseLoanPage
    {
        public int selectedTabIndex;
        protected class CCReason
        {
            public string Amount { get; private set; }
            public string Reason { get; private set; }
            public string FeeID { get; private set; }
            

            public CCReason(string amt, string rsn, string id)
            {
                // The rsn should never be empty unless it was never filled out or the user cleared the fees
                Amount = string.IsNullOrEmpty(rsn) ? "" : amt;
                Reason = rsn;
                FeeID = id;
            }

            public CCReason()
            {
                Amount = null;
                Reason = "";
                FeeID = "";
            }
        }
        private BrokerDB x_Broker = null;
        protected new BrokerDB Broker //! really...
        {
            get
            {
                if (x_Broker == null)
                {
                    x_Broker = BrokerDB.RetrieveById(BrokerUser.BrokerId);
                }
                return x_Broker;
            }
        }
        private bool? x_hasCoCArchives = null;
        protected bool HasCoCArchives
        {
            get
            {
                if(false == Broker.IsGFEandCoCVersioningEnabled)
                {
                    return false;
                }

                if (x_hasCoCArchives == null)
                {
                    var dataLoan = new CPageData(LoanID, new string[]{});
                    dataLoan.InitLoad();
                    x_hasCoCArchives = (dataLoan.CoCArchives.Count() > 0);
                }
                return x_hasCoCArchives.Value;
            }
        }

        /// <summary>
        /// If you change the order of the tabs, or add a new one, update this.
        /// </summary>
        protected enum E_Tab
        {
            CreateNew,
            ViewPast
        }
        protected bool IsTabViewPast
        {
            get { return IsArchivePage && selectedTabIndex == (int)E_Tab.ViewPast; }
        }
        protected bool IsTabCreateNew
        {
            get { return IsArchivePage && !IsTabViewPast; }
        }

        protected bool IsArchivePage
        {
            get { return Broker.IsGFEandCoCVersioningEnabled && RequestHelper.GetSafeQueryString("IsArchivePage") == "true" && !IsLeadPage; }
        }
        public override bool IsReadOnly
        {
            get
            {
                return base.IsReadOnly || IsTabViewPast;
            }
        }
        protected bool IsProtectDisclosureDates
        {
            get
            {
                return Broker.IsProtectDisclosureDates;
            }
        }

        private CPageData _pageData = null;
        private CPageData pageData
        {
            get
            {
                if (_pageData == null)
                {
                    _pageData = CPageData.CreateUsingSmartDependency(LoanID, this.GetType().BaseType);
                    pageData.InitLoad();
                }

                return _pageData;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            EnableJqueryMigrate = false;
            this.PageTitle = "Change of Circumstances";
            this.PageID = "ChangeOfCircumstances";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CChangeOfCircumstancesPDF);

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);

            InitializeComponent();
            base.OnInit(e);
        }
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        protected void PageLoad(object sender, EventArgs e)
        {
            IncludeStyleSheet("~/css/Tabs.css");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterJsScript("LQBPopup.js");
            RegisterJsGlobalVariables("sFileVersion", pageData.sFileVersion_rep);

            if (Request.Params["__EVENTTARGET"] == "changeTab")
                selectedTabIndex = Int32.Parse(Request.Params["__EVENTARGUMENT"]);

            // OPM 223480 - Clear CoC data.
            if (!IsPostBack || Request.Params["__EVENTTARGET"] == "clearData")
            {
                if (Broker.IsGFEandCoCVersioningEnabled)
                {
                    pageData.ClearCoCData();
                }
            }

            if (IsTabViewPast)
            {
                tab1.Attributes.Add("class", "selected");
                tab0.Attributes.Remove("class");
                var dataLoan = new CPageData(LoanID, new string[] { "sLastDisclosedGFEArchiveD" });
                dataLoan.InitLoad();

                // if there aren't any archives, then viewing past changes shouldn't be allowed, so i'm switching the tab.  (can't do it in aspx page)
                if (dataLoan.CoCArchives.Count() == 0)
                {
                    selectedTabIndex = (int)E_Tab.CreateNew;
                    if (IsPostBack)
                    {
                        LoadData();
                    }
                    return;
                }

                var selectedValue = CoCArchives.SelectedValue;
                Tools.Bind_CoCArchives(CoCArchives, dataLoan.CoCArchives);
                if (string.IsNullOrEmpty(selectedValue) || hfSavedCoC.Value == "true")
                {
                    Tools.SetDropDownListValue(CoCArchives, dataLoan.sLastDisclosedGFEArchiveD_rep);
                    hfSavedCoC.Value = "false";
                }
                else
                {
                    Tools.SetDropDownListValue(CoCArchives, selectedValue);
                }
            }
            else
            {
                tab0.Attributes.Add("class", "selected");
                tab1.Attributes.Remove("class");
            }
            if (IsArchivePage && IsPostBack)
            {
                LoadData();
            }
        }
        

        protected override void LoadData()
        {
            if (IsTabViewPast)
            {
                
                var cocas = (from c in pageData.CoCArchives
                            where c.DateArchived == CoCArchives.SelectedValue
                            select c);
                if (cocas.Count() > 0)
                {
                    var coca = cocas.First();
                    pageData.ApplyCoCArchive(coca);
                }
                
            }

            LoadData(pageData);
            
        }
        private void LoadData(CPageData initializedDataLoan)
        {
            if (IsTabCreateNew && IsArchivePage)
            {
                changeList.Controls.Add(BuildChangesTable(pageData.sCoCFeeChanges));
            }

            // must keep in sync with LoadData(GFEArchiveData)
            FeeChangeReasons.DataSource = GetChangeReasons();
            FeeChangeReasons.DataBind();

            sCircumstanceChangeExplanation.Value = pageData.sCircumstanceChangeExplanation;

            
            sGfeRedisclosureD.Text = pageData.sGfeRedisclosureD_rep;
            if (IsProtectDisclosureDates)
            {
                sGfeRedisclosureD.ReadOnly = true;
            }
            sCircumstanceChangeD.Text = pageData.sCircumstanceChangeD_rep;

            IPreparerFields gfe = pageData.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            GfePrepareDate.Text = gfe.PrepareDate_rep;
        }

        private HtmlTable BuildChangesTable( List<FeeChange> feeChanges)
        {
            var reasons = GetChangeReasons();

            HtmlTable changeTable = new HtmlTable();
            changeTable.Width = "100%";
            HtmlTableRow headerRow = new HtmlTableRow();
            headerRow.Attributes.Add("class", "GridHeader");
            headerRow.Cells.Add(new HtmlTableCell() { InnerText = "" });
            headerRow.Cells.Add(new HtmlTableCell() { InnerText = "Fee Name" });
            headerRow.Cells.Add(new HtmlTableCell() { InnerText = "GFE Archive Value" });
            headerRow.Cells.Add(new HtmlTableCell() { InnerText = "Current GFE Value" });
            changeTable.Rows.Add(headerRow);

            int rowCount = 0;
            
            
            var loanAmtFees = feeChanges.Where(p => p.IsPercentFee && p.PercentBase == E_PercentBaseT.LoanAmount);
            if (loanAmtFees.Count() != 0)
            {
                
                HtmlTableRow groupRow = new HtmlTableRow();
                groupRow.Attributes.Add("class", "GridItem");

                bool isChecked = false;
                foreach (var change in loanAmtFees)
                {
                    if ( reasons.Exists( p => p.FeeID == change.FeeId && p.Reason != ""))
                    {
                        isChecked = true;
                        break;
                    }
                }


                string checkboxStr = string.Format(
                    "<input type='checkbox' onClick='onLoanAmtClick()' id='allLAmtFeeChk' {0} />",
                    isChecked ? "checked='checked'" : ""
                    );
                
                groupRow.Cells.Add(new HtmlTableCell() { InnerHtml = checkboxStr});
                groupRow.Cells.Add(new HtmlTableCell() { InnerText = "Loan Amount Change" });
                groupRow.Cells.Add(new HtmlTableCell());
                groupRow.Cells.Add(new HtmlTableCell());
                changeTable.Rows.Add(groupRow);
                rowCount++;

                foreach (var change in loanAmtFees)
                {
                    HtmlTableRow changeRow = new HtmlTableRow();
                    changeRow.Attributes.Add("class", "GridItem");

                    checkboxStr = string.Format(
                        "<input type='checkbox' data1='{0}' data2='{1}' data3='{2}' class='lAmtFeeChk' onClick='onFeeClick();' style='display:none' {3} />",
                        AspxTools.HtmlString(change.CurrentValue),
                        AspxTools.HtmlString(change.Description + " (" + change.GfeSection + ")"),
                        AspxTools.HtmlString(change.FeeId),
                        isChecked ? "checked='checked'" : ""
                        );
                    changeRow.Cells.Add(new HtmlTableCell() { InnerHtml = checkboxStr });

                    var descCell = new HtmlTableCell() { InnerText = change.Description };
                    descCell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "23px");
                    changeRow.Cells.Add(descCell);
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = "Fee Amount: " + change.ArchiveValue });
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = "Fee Amount: " + change.CurrentValue });
                    changeTable.Rows.Add(changeRow);

                }
            }


            var totalLoanAmtFees = feeChanges.Where(p => p.IsPercentFee && p.PercentBase == E_PercentBaseT.TotalLoanAmount);
            if (totalLoanAmtFees.Count() != 0)
            {

                HtmlTableRow groupRow = new HtmlTableRow();
                groupRow.Attributes.Add("class", "GridItem");

                bool isChecked = false;
                foreach (var change in totalLoanAmtFees)
                {
                    if (reasons.Exists(p => p.FeeID == change.FeeId && p.Reason != ""))
                    {
                        isChecked = true;
                        break;
                    }
                }


                string checkboxStr = string.Format(
                    "<input type='checkbox' onClick='onTotLoanAmtClick()' id='allTotLAmtFeeChk' {0} />",
                    isChecked ? "checked='checked'" : ""
                    );

                groupRow.Cells.Add(new HtmlTableCell() { InnerHtml = checkboxStr });
                groupRow.Cells.Add(new HtmlTableCell() { InnerText = "Total Loan Amount Change" });
                groupRow.Cells.Add(new HtmlTableCell());
                groupRow.Cells.Add(new HtmlTableCell());
                changeTable.Rows.Add(groupRow);
                rowCount++;

                foreach (var change in totalLoanAmtFees)
                {
                    HtmlTableRow changeRow = new HtmlTableRow();
                    changeRow.Attributes.Add("class", "GridItem");

                    checkboxStr = string.Format(
                        "<input type='checkbox' data1='{0}' data2='{1}' data3='{2}' class='totLAmtFeeChk' onClick='onFeeClick();' style='display:none' {3} />",
                        AspxTools.HtmlString(change.CurrentValue),
                        AspxTools.HtmlString(change.Description + " (" + change.GfeSection + ")"),
                        AspxTools.HtmlString(change.FeeId),
                        isChecked ? "checked='checked'" : ""
                        );
                    changeRow.Cells.Add(new HtmlTableCell() { InnerHtml = checkboxStr });

                    var descCell = new HtmlTableCell() { InnerText = change.Description };
                    descCell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "23px");
                    changeRow.Cells.Add(descCell);
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = "Fee Amount: " + change.ArchiveValue });
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = "Fee Amount: " + change.CurrentValue });
                    changeTable.Rows.Add(changeRow);

                }
            }


            var purchasePriceFees = feeChanges.Where(p => p.IsPercentFee && p.PercentBase == E_PercentBaseT.SalesPrice);
            if (purchasePriceFees.Count() != 0)
            {
                HtmlTableRow groupRow = new HtmlTableRow();
                groupRow.Attributes.Add("class", "GridItem");

                bool isChecked = false;
                foreach (var change in purchasePriceFees)
                {
                    if (reasons.Exists(p => p.FeeID == change.FeeId && p.Reason != ""))
                    {
                        isChecked = true;
                        break;
                    }
                }


                string checkboxStr = string.Format(
                    "<input type='checkbox' onClick='onPurchPriceClick()' id='allPurchPriceFeeChk' {0} />",
                    isChecked ? "checked='checked'" : ""
                    );

                groupRow.Cells.Add(new HtmlTableCell() { InnerHtml = checkboxStr });
                groupRow.Cells.Add(new HtmlTableCell() { InnerText = "Purchase Price Change" });
                groupRow.Cells.Add(new HtmlTableCell());
                groupRow.Cells.Add(new HtmlTableCell());
                changeTable.Rows.Add(groupRow);
                rowCount++;

                foreach (var change in purchasePriceFees)
                {
                    HtmlTableRow changeRow = new HtmlTableRow();
                    changeRow.Attributes.Add("class", "GridItem");

                    checkboxStr = string.Format(
                        "<input type='checkbox' data1='{0}' data2='{1}' data3='{2}' class='purchPriceFeeChk' onClick='onFeeClick();' style='display:none' {3} />",
                        AspxTools.HtmlString(change.CurrentValue),
                        AspxTools.HtmlString(change.Description + " (" + change.GfeSection + ")"),
                        AspxTools.HtmlString(change.FeeId),
                        isChecked ? "checked='checked'" : ""
                        );
                    changeRow.Cells.Add(new HtmlTableCell() { InnerHtml = checkboxStr });

                    var descCell = new HtmlTableCell() { InnerText = change.Description };
                    descCell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "23px");
                    changeRow.Cells.Add(descCell);
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = "Fee Amount: " + change.ArchiveValue });
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = "Fee Amount: " + change.CurrentValue });
                    changeTable.Rows.Add(changeRow);

                }
            }



            var appraisalFees = feeChanges.Where(p => p.IsPercentFee && p.PercentBase == E_PercentBaseT.AppraisalValue);
            if (appraisalFees.Count() != 0)
            {
                HtmlTableRow groupRow = new HtmlTableRow();
                groupRow.Attributes.Add("class", "GridItem");

                bool isChecked = false;
                foreach (var change in appraisalFees)
                {
                    if (reasons.Exists(p => p.FeeID == change.FeeId && p.Reason != ""))
                    {
                        isChecked = true;
                        break;
                    }
                }


                string checkboxStr = string.Format(
                    "<input type='checkbox' onClick='onAppraisalClick()' id='allAppraisalFeeChk' {0} />",
                    isChecked ? "checked='checked'" : ""
                    );

                groupRow.Cells.Add(new HtmlTableCell() { InnerHtml = checkboxStr });
                groupRow.Cells.Add(new HtmlTableCell() { InnerText = "Appraisal Value Change" });
                groupRow.Cells.Add(new HtmlTableCell());
                groupRow.Cells.Add(new HtmlTableCell());
                changeTable.Rows.Add(groupRow);
                rowCount++;

                foreach (var change in appraisalFees)
                {
                    HtmlTableRow changeRow = new HtmlTableRow();
                    changeRow.Attributes.Add("class", "GridItem");

                    checkboxStr = string.Format(
                        "<input type='checkbox' data1='{0}' data2='{1}' data3='{2}' class='appraisalFeeChk' onClick='onFeeClick();' style='display:none' {3} />",
                        AspxTools.HtmlString(change.CurrentValue),
                        AspxTools.HtmlString(change.Description + " (" + change.GfeSection + ")"),
                        AspxTools.HtmlString(change.FeeId),
                        isChecked ? "checked='checked'" : ""
                        );
                    changeRow.Cells.Add(new HtmlTableCell() { InnerHtml = checkboxStr });

                    var descCell = new HtmlTableCell() { InnerText = change.Description };
                    descCell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "23px");
                    changeRow.Cells.Add(descCell);
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = "Fee Amount: " + change.ArchiveValue });
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = "Fee Amount: " + change.CurrentValue });
                    changeTable.Rows.Add(changeRow);

                }
            }



            foreach (var change in feeChanges.Where(p => !p.IsPercentFee))
            {

                string checkboxStr = string.Format(
                    "<input type='checkbox' data1='{0}' data2='{1}' data3='{2}' class='feechk' onClick='onFeeClick();' {3} />",
                    AspxTools.HtmlString(change.CurrentValue),
                    AspxTools.HtmlString(change.Description + " (" + change.GfeSection + ")"),
                    AspxTools.HtmlString(change.FeeId),
                    reasons.Exists(p => p.FeeID == change.FeeId && p.Reason != "") ? "checked='checked'" : ""
                    );

                string feeNameStr = change.Description;


                if (change.ArchiveValue != change.CurrentValue)
                {
                    HtmlTableRow changeRow = new HtmlTableRow();
                    changeRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                    changeRow.Cells.Add(new HtmlTableCell() { InnerHtml = checkboxStr });
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = feeNameStr });
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = "Fee Amount: " + change.ArchiveValue });
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = "Fee Amount: " + change.CurrentValue });

                    checkboxStr = "";
                    feeNameStr = "";
                    changeTable.Rows.Add(changeRow);
                }

                if (change.ArchivePercent != change.Percent)
                {
                    var percentRow = new HtmlTableRow();
                    percentRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");

                    percentRow.Cells.Add(new HtmlTableCell() { InnerHtml = checkboxStr });
                    percentRow.Cells.Add(new HtmlTableCell() { InnerText = feeNameStr });
                    percentRow.Cells.Add(new HtmlTableCell() { InnerText = "Fee Percent: " + change.ArchivePercent });
                    percentRow.Cells.Add(new HtmlTableCell() { InnerText = "Fee Percent: " + change.Percent });

                    checkboxStr = "";
                    feeNameStr = "";
                    changeTable.Rows.Add(percentRow);
                }

                if (change.FixedFee != change.ArchiveFixedFee)
                {
                    var fixedRow = new HtmlTableRow();
                    fixedRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");

                    fixedRow.Cells.Add(new HtmlTableCell() { InnerHtml = checkboxStr });
                    fixedRow.Cells.Add(new HtmlTableCell() { InnerText = feeNameStr });
                    fixedRow.Cells.Add(new HtmlTableCell() { InnerText = "Fixed Amount: " + change.ArchiveFixedFee });
                    fixedRow.Cells.Add(new HtmlTableCell() { InnerText = "Fixed Amount: " + change.FixedFee });

                    checkboxStr = "";
                    feeNameStr = "";
                    changeTable.Rows.Add(fixedRow);
                }

                if (change.RawDescription != change.ArchiveDescription)
                {
                    var nameRow = new HtmlTableRow();
                    nameRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");

                    nameRow.Cells.Add(new HtmlTableCell() { InnerHtml = checkboxStr });
                    nameRow.Cells.Add(new HtmlTableCell() { InnerText = feeNameStr });
                    nameRow.Cells.Add(new HtmlTableCell() { InnerText = "Fee Name: " + change.ArchiveDescription });
                    nameRow.Cells.Add(new HtmlTableCell() { InnerText = "Fee Name: " + change.RawDescription });

                    checkboxStr = "";
                    feeNameStr = "";
                    changeTable.Rows.Add(nameRow);
                }

                if (change.ArchiveGfeSection != change.GfeSection)
                {
                    var gfeSectionRow = new HtmlTableRow();
                    gfeSectionRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");

                    gfeSectionRow.Cells.Add(new HtmlTableCell() { InnerHtml = checkboxStr });
                    gfeSectionRow.Cells.Add(new HtmlTableCell() { InnerText = feeNameStr });
                    gfeSectionRow.Cells.Add(new HtmlTableCell() { InnerText = "GFE Box: " + change.ArchiveGfeSection });
                    gfeSectionRow.Cells.Add(new HtmlTableCell() { InnerText = "GFE Box: " + change.GfeSection });

                    checkboxStr = "";
                    feeNameStr = "";
                    changeTable.Rows.Add(gfeSectionRow);
                }

                rowCount++;
            }

            return changeTable;
        }

        private List<CCReason> GetChangeReasons()
        {
            return new List<CCReason>()
            {
                new CCReason(pageData.sCircumstanceChange1Amount_rep, pageData.sCircumstanceChange1Reason, pageData.sCircumstanceChange1FeeId),
                new CCReason(pageData.sCircumstanceChange2Amount_rep, pageData.sCircumstanceChange2Reason, pageData.sCircumstanceChange2FeeId),
                new CCReason(pageData.sCircumstanceChange3Amount_rep, pageData.sCircumstanceChange3Reason, pageData.sCircumstanceChange3FeeId),
                new CCReason(pageData.sCircumstanceChange4Amount_rep, pageData.sCircumstanceChange4Reason, pageData.sCircumstanceChange4FeeId),
                new CCReason(pageData.sCircumstanceChange5Amount_rep, pageData.sCircumstanceChange5Reason, pageData.sCircumstanceChange5FeeId),
                new CCReason(pageData.sCircumstanceChange6Amount_rep, pageData.sCircumstanceChange6Reason, pageData.sCircumstanceChange6FeeId),
                new CCReason(pageData.sCircumstanceChange7Amount_rep, pageData.sCircumstanceChange7Reason, pageData.sCircumstanceChange7FeeId),
                new CCReason(pageData.sCircumstanceChange8Amount_rep, pageData.sCircumstanceChange8Reason, pageData.sCircumstanceChange8FeeId),
            };
        }
    }
}
