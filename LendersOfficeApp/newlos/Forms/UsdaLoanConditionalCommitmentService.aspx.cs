﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class UsdaLoanConditionalCommitmentServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(UsdaLoanConditionalCommitmentServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var lender = dataLoan.GetPreparerOfForm(E_PreparerFormT.UsdaConditionalCommitment, E_ReturnOptionIfNotExist.CreateNew);
            lender.CompanyName = GetString("LenderNm");
            lender.StreetAddr = GetString("LenderAddr");
            lender.City = GetString("LenderCity");
            lender.State = GetString("LenderState");
            lender.Zip = GetString("LenderZip");
            lender.Update();

            dataLoan.sUsdaBorrowerId = GetString("sUsdaBorrowerId");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sSpCounty = GetString("sSpCounty");
            dataLoan.sUsdaStateCode = GetString("sUsdaStateCode");
            dataLoan.sUsdaCountyCode = GetString("sUsdaCountyCode");

            dataLoan.sMiCommitmentReceivedD_rep = GetString("sMiCommitmentReceivedD");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            var lender = dataLoan.GetPreparerOfForm(E_PreparerFormT.UsdaConditionalCommitment, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("LenderNm", lender.CompanyName);
            SetResult("LenderAddr", lender.StreetAddr);
            SetResult("LenderCity", lender.City);
            SetResult("LenderState", lender.State);
            SetResult("LenderZip", lender.Zip);

            SetResult("sUsdaBorrowerId", dataLoan.sUsdaBorrowerId);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sSpCounty", dataLoan.sSpCounty);
            SetResult("sUsdaStateCode", dataLoan.sUsdaStateCode);
            SetResult("sUsdaCountyCode", dataLoan.sUsdaCountyCode);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);

            SetResult("sCombinedBorFirstNm", dataLoan.sCombinedBorFirstNm);
            SetResult("sCombinedBorMidNm", dataLoan.sCombinedBorMidNm);
            SetResult("sCombinedBorLastNm", dataLoan.sCombinedBorLastNm);
            SetResult("sCombinedBorSuffix", dataLoan.sCombinedBorSuffix);
            SetResult("sCombinedBorSsn", dataLoan.sCombinedBorSsn);

            SetResult("sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sUsdaAnnualFee", dataLoan.sUsdaAnnualFee_rep);
            SetResult("sMiCommitmentReceivedD", dataLoan.sMiCommitmentReceivedD_rep);
        }
    }

    public partial class UsdaLoanConditionalCommitmentService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new UsdaLoanConditionalCommitmentServiceItem());
        }
    }
}
