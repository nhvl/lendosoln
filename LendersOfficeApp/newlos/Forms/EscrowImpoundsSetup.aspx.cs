﻿namespace LendersOfficeApp.newlos.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOfficeApp.los.LegalForm;

    public partial class EscrowImpoundsSetup : BaseLoanPage
    {
        private void PageInit(object sender, EventArgs e)
        {
            this.PageTitle = "Escrow / Impounds Setup";
            this.PageID = "EscrowImpoundsSetup";
            //this.PDFPrintClass = null; // no pdf version yet.
            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("FeeTypePropertiesSetter.js");

            Tools.Bind_PercentBaseT(sProHazInsT);
            Tools.Bind_PercentBaseT(sProRealETxT);

            Tools.Bind_CustomFeeDesc(s1006ProHExpDesc, BrokerID, "1000");
            Tools.Bind_CustomFeeDesc(s1007ProHExpDesc, BrokerID, "1000");
            Tools.Bind_CustomFeeDesc(sU3RsrvDesc, BrokerID, "1000");
            Tools.Bind_CustomFeeDesc(sU4RsrvDesc, BrokerID, "1000");

            Tools.Bind_CustomaryCalcMinT(sCustomaryEscrowImpoundsCalcMinT);
            Tools.Bind_AggregateEscrowCalcModeT(sAggEscrowCalcModeT, Broker.EnableCustomaryEscrowImpoundsCalculation);
            
            phAdditionalSection1000CustomFees.Visible = Broker.EnableAdditionalSection1000CustomFees;
        }
        protected override void LoadData()
        {
            var dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(EscrowImpoundsSetup));
            dataLoan.InitLoad();
            
            bool heMigrated = dataLoan.sIsHousingExpenseMigrated;

            sProHazInsR.Text = dataLoan.sProHazInsR_rep;
            Tools.SetDropDownListValue(sProHazInsT, dataLoan.sProHazInsT);
            sProHazInsMb.Text = dataLoan.sProHazInsMb_rep;
            sProHazInsHolder.Visible = !dataLoan.sHazardExpenseInDisbursementMode;
            sProHazIns.Text = dataLoan.sHazardExpenseInDisbursementMode ? dataLoan.sHazardExpense.MonthlyAmtServicing_rep : dataLoan.sProHazIns_rep;
            sHazInsRsrvEscrowedTri.Checked = (E_TriState.Yes == dataLoan.sHazInsRsrvEscrowedTri);
            sHazInsRsrvMonLckd.Checked = dataLoan.sHazInsRsrvMonLckd;
            sHazInsRsrvMon.Text = dataLoan.sHazInsRsrvMon_rep;
            sHazInsRsrv.Text = dataLoan.sHazInsRsrv_rep;

            sProMIns.Text = dataLoan.sProMIns_rep;
            sMInsRsrvEscrowedTri.Checked = (E_TriState.Yes == dataLoan.sMInsRsrvEscrowedTri);
            if (dataLoan.sIssMInsRsrvEscrowedTriReadOnly)
            {
                sMInsRsrvEscrowedTri.Enabled = false;
            }
            sMInsRsrvMonLckd.Checked = dataLoan.sMInsRsrvMonLckd;
            sMInsRsrvMon.Text = dataLoan.sMInsRsrvMon_rep;
            sMInsRsrv.Text = dataLoan.sMInsRsrv_rep;
            
            sProRealETxR.Text = dataLoan.sProRealETxR_rep;
            Tools.SetDropDownListValue(sProRealETxT, dataLoan.sProRealETxT);
            sProRealETxMb.Text = dataLoan.sProRealETxMb_rep;
            sProRealETxHolder.Visible = !dataLoan.sRealEstateTaxExpenseInDisbMode;
            sProRealETx.Text = dataLoan.sRealEstateTaxExpenseInDisbMode ? dataLoan.sRealEstateTaxExpense.MonthlyAmtServicing_rep : dataLoan.sProRealETx_rep;
            sRealETxRsrvEscrowedTri.Checked = (E_TriState.Yes == dataLoan.sRealETxRsrvEscrowedTri);
            sRealETxRsrvMonLckd.Checked = dataLoan.sRealETxRsrvMonLckd;
            sRealETxRsrvMon.Text = dataLoan.sRealETxRsrvMon_rep;
            sRealETxRsrv.Text = dataLoan.sRealETxRsrv_rep;
            if (heMigrated && dataLoan.sSchoolTaxExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                sProSchoolTx.Text = dataLoan.sSchoolTaxExpense.MonthlyAmtServicing_rep;
                sProSchoolTx.ReadOnly = true;
            }
            else
            {
                sProSchoolTx.Text = dataLoan.sProSchoolTx_rep;
                sProSchoolTx.ReadOnly = false;
            }
            sSchoolTxRsrvEscrowedTri.Checked = (E_TriState.Yes == dataLoan.sSchoolTxRsrvEscrowedTri);
            sSchoolTxRsrvMonLckd.Checked = dataLoan.sSchoolTxRsrvMonLckd;
            sSchoolTxRsrvMon.Text = dataLoan.sSchoolTxRsrvMon_rep;
            sSchoolTxRsrv.Text = dataLoan.sSchoolTxRsrv_rep;
            if (heMigrated && dataLoan.sFloodExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                sProFloodIns.Text = dataLoan.sFloodExpense.MonthlyAmtServicing_rep;
                sProFloodIns.ReadOnly = true;
            }
            else
            {
                sProFloodIns.Text = dataLoan.sProFloodIns_rep;
                sProFloodIns.ReadOnly = false;
            }
            sFloodInsRsrvEscrowedTri.Checked = (E_TriState.Yes == dataLoan.sFloodInsRsrvEscrowedTri);
            sFloodInsRsrvMonLckd.Checked = dataLoan.sFloodInsRsrvMonLckd;
            sFloodInsRsrvMon.Text = dataLoan.sFloodInsRsrvMon_rep;
            sFloodInsRsrv.Text = dataLoan.sFloodInsRsrv_rep;

            s1006ProHExpDesc.Text = dataLoan.s1006ProHExpDesc;
            if (heMigrated && dataLoan.sLine1008Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                s1006ProHExp.Text = dataLoan.sLine1008Expense.MonthlyAmtServicing_rep;
                s1006ProHExp.ReadOnly = true;
            }
            else
            {
                s1006ProHExp.Text = dataLoan.s1006ProHExp_rep;
                s1006ProHExp.ReadOnly = false;
            }
            s1006RsrvEscrowedTri.Checked = (E_TriState.Yes == dataLoan.s1006RsrvEscrowedTri);
            s1006RsrvMonLckd.Checked = dataLoan.s1006RsrvMonLckd;
            s1006RsrvMon.Text = dataLoan.s1006RsrvMon_rep;
            s1006Rsrv.Text = dataLoan.s1006Rsrv_rep;

            s1007ProHExpDesc.Text = dataLoan.s1007ProHExpDesc;
            if (heMigrated && dataLoan.sLine1009Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                s1007ProHExp.Text = dataLoan.sLine1009Expense.MonthlyAmtServicing_rep;
                s1007ProHExp.ReadOnly = true;
            }
            else
            {
                s1007ProHExp.Text = dataLoan.s1007ProHExp_rep;
                s1007ProHExp.ReadOnly = false;
            }
            s1007RsrvEscrowedTri.Checked = (E_TriState.Yes == dataLoan.s1007RsrvEscrowedTri);
            s1007RsrvMonLckd.Checked = dataLoan.s1007RsrvMonLckd;
            s1007RsrvMon.Text = dataLoan.s1007RsrvMon_rep;
            s1007Rsrv.Text = dataLoan.s1007Rsrv_rep;

            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                sU3RsrvDesc.Text = dataLoan.sU3RsrvDesc;
                if (heMigrated && dataLoan.sLine1010Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                {
                    sProU3Rsrv.Text = dataLoan.sLine1010Expense.MonthlyAmtServicing_rep;
                    sProU3Rsrv.ReadOnly = true;
                }
                else
                {
                    sProU3Rsrv.Text = dataLoan.sProU3Rsrv_rep;
                    sProU3Rsrv.ReadOnly = false;
                }
                sU3RsrvEscrowedTri.Checked = (E_TriState.Yes == dataLoan.sU3RsrvEscrowedTri);
                sU3RsrvMonLckd.Checked = dataLoan.sU3RsrvMonLckd;
                sU3RsrvMon.Text = dataLoan.sU3RsrvMon_rep;
                sU3Rsrv.Text = dataLoan.sU3Rsrv_rep;

                sU4RsrvDesc.Text = dataLoan.sU4RsrvDesc;
                if (heMigrated && dataLoan.sLine1011Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                {
                    sProU4Rsrv.Text = dataLoan.sLine1011Expense.MonthlyAmtServicing_rep;
                    sProU4Rsrv.ReadOnly = true;
                }
                else
                {
                    sProU4Rsrv.Text = dataLoan.sProU4Rsrv_rep;
                    sProU4Rsrv.ReadOnly = false;
                }
                sU4RsrvEscrowedTri.Checked = (E_TriState.Yes == dataLoan.sU4RsrvEscrowedTri);
                sU4RsrvMonLckd.Checked = dataLoan.sU4RsrvMonLckd;
                sU4RsrvMon.Text = dataLoan.sU4RsrvMon_rep;
                sU4Rsrv.Text = dataLoan.sU4Rsrv_rep;
            }

            sSchedDueD1Lckd.Checked = dataLoan.sSchedDueD1Lckd;
            sSchedDueD1.Text = dataLoan.sSchedDueD1_rep;

            sAggregateAdjRsrvLckd.Checked = dataLoan.sAggregateAdjRsrvLckd;
            sAggregateAdjRsrv.Text = dataLoan.sAggregateAdjRsrv_rep;

            sGfeInitialImpoundDeposit.Text = dataLoan.sGfeInitialImpoundDeposit_rep;

            // initial locked fields readonlification:
            sHazInsRsrvMon.ReadOnly = (false == dataLoan.sHazInsRsrvMonLckd) || IsReadOnly;
            sMInsRsrvMon.ReadOnly = (false == dataLoan.sMInsRsrvMonLckd) || IsReadOnly;
            sRealETxRsrvMon.ReadOnly = (false == dataLoan.sRealETxRsrvMonLckd) || IsReadOnly;
            sSchoolTxRsrvMon.ReadOnly = (false == dataLoan.sSchoolTxRsrvMonLckd) || IsReadOnly;
            sFloodInsRsrvMon.ReadOnly = (false == dataLoan.sFloodInsRsrvMonLckd) || IsReadOnly;
            s1006RsrvMon.ReadOnly = (false == dataLoan.s1006RsrvMonLckd) || IsReadOnly;
            s1007RsrvMon.ReadOnly = (false == dataLoan.s1007RsrvMonLckd) || IsReadOnly;
            sU3RsrvMon.ReadOnly = (false == dataLoan.sU3RsrvMonLckd) || IsReadOnly;
            sU4RsrvMon.ReadOnly = (false == dataLoan.sU4RsrvMonLckd) || IsReadOnly;
            sSchedDueD1.ReadOnly = (false == dataLoan.sSchedDueD1Lckd) || IsReadOnly;
            sAggregateAdjRsrv.ReadOnly = (false == dataLoan.sAggregateAdjRsrvLckd) || IsReadOnly;

            // hidden props for feeType updates.
            s1006RsrvProps_ctrl.InitItemProps(dataLoan.s1006RsrvProps);
            s1007RsrvProps_ctrl.InitItemProps(dataLoan.s1007RsrvProps);
            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                sU3RsrvProps_ctrl.InitItemProps(dataLoan.sU3RsrvProps);
                sU4RsrvProps_ctrl.InitItemProps(dataLoan.sU4RsrvProps);
            }

            if (dataLoan.sIsHousingExpenseMigrated)
            {
                Tools.SetDropDownListValue(sAggEscrowCalcModeT, dataLoan.sAggEscrowCalcModeT);
            }
            else
            {
                aggCalcMode.Visible = false;
            }

            if (dataLoan.sIsHousingExpenseMigrated && Broker.EnableCustomaryEscrowImpoundsCalculation)
            {
                Tools.SetDropDownListValue(sCustomaryEscrowImpoundsCalcMinT, dataLoan.sCustomaryEscrowImpoundsCalcMinT);
            }
            else
            {
                customaryMin.Visible = false;
            }
        }
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            IsAppSpecific = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        private void InitializeComponent()
        {
            //this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
    }
}
