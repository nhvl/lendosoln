﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QualRateCalculationPopup.ascx.cs" Inherits="LendersOfficeApp.newlos.Forms.QualRateCalculationPopup" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<div id="QualRateCalcPopup" class="qual-rate-popup" runat="server">
    <table class="qual-rate-table">
        <tr>
            <td colspan="3">
                Use Qual Rate? &nbsp;
                <input type="checkbox" runat="server" id="sUseQualRate" NoHighlight="true" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Calculation Type &nbsp;
                <input type="radio" name="sQualRateCalculationT" class="sQualRateCalculationT" id="QualRateCalculationFlatValue" runat="server" data-field-id="sQualRateCalculationT" /> Flat Value
                <input type="radio" name="sQualRateCalculationT" class="sQualRateCalculationT" id="QualRateCalculationMaxOf" runat="server" data-field-id="sQualRateCalculationT" /> Max of
            </td>
        </tr>
        <tr class="inset">
            <td>
                <asp:DropDownList runat="server" ID="sQualRateCalculationFieldT1" CssClass="QualRateCalculationField"></asp:DropDownList> +
            </td>
            <td>
                <ml:percenttextbox id="sQualRateCalculationAdjustment1" runat="server" onchange="refreshCalculation();" preset="percent"></ml:percenttextbox> =
            </td>
            <td>
                <ml:PercentTextBox ID="QualRateCalculationResult1" runat="server" preset="percent" ReadOnly="true"></ml:PercentTextBox>
            </td>
        </tr>
        <tr id="SecondCalculationRow" class="inset">
            <td>
                <asp:DropDownList runat="server" ID="sQualRateCalculationFieldT2" CssClass="QualRateCalculationField"></asp:DropDownList> +
            </td>
            <td>
                <ml:percenttextbox id="sQualRateCalculationAdjustment2" runat="server" onchange="refreshCalculation();" preset="percent"></ml:percenttextbox> =
            </td>
            <td>
                <ml:PercentTextBox ID="QualRateCalculationResult2" runat="server" preset="percent" ReadOnly="true"></ml:PercentTextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Qual Rate = <ml:PercentTextBox ID="sQualIR" runat="server" preset="percent" ReadOnly="true"></ml:PercentTextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="center">
                <input type="button" id="QualRateCalculationPopup_Ok" value="OK" runat="server" />
                <input type="button" id="QualRateCalculationPopup_Close" value="Close" runat="server" />
            </td>
        </tr>
    </table>
</div>