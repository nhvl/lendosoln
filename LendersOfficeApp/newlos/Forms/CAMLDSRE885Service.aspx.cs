using System;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.Admin;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class CAMLDSRE885Service : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected BrokerDB Broker
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BrokerDB; }
        }

        private CPageData ConstructPageData(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(CAMLDSRE885Service));
        }
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "CAMLDSRE885pg1_CalculateData":
                    CAMLDSRE885pg1_CalculateData();
                    break;
                case "CAMLDSRE885pg1_SaveData":
                    CAMLDSRE885pg1_SaveData();
                    break;
                case "CAMLDSRE885pg2_CalculateData":
                    CAMLDSRE885pg2_CalculateData();
                    break;
                case "CAMLDSRE885pg2_SaveData":
                    CAMLDSRE885pg2_SaveData();
                    break;
                case "CAMLDSRE885pg3_CalculateData":
                    CAMLDSRE885pg3_CalculateData();
                    break;
                case "CAMLDSRE885pg3_SaveData":
                    CAMLDSRE885pg3_SaveData();
                    break;
                case "CAMLDSRE885pg4_CalculateData":
                    CAMLDSRE885pg4_CalculateData();
                    break;
                case "CAMLDSRE885pg4_SaveData":
                    CAMLDSRE885pg4_SaveData();
                    break;

            }
        }

        private int sFileVersion
        {
            get { return GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck); }
        }

        #region MLDS Page 4
        private void CAMLDSRE885pg4_CalculateData()
        {
            Guid loanID = GetGuid("loanid");
            CPageData dataLoan = ConstructPageData(loanID);
            dataLoan.InitLoad();
            CAMLDSRE885pg4_BindData(dataLoan);
            CAMLDSRE885pg4_LoadData(dataLoan);
        }
        private void CAMLDSRE885pg4_SaveData()
        {
            Guid loanID = GetGuid("loanid");
            CPageData dataLoan = ConstructPageData(loanID);
            dataLoan.InitSave(sFileVersion);

            CAMLDSRE885pg4_BindData(dataLoan);

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            CAMLDSRE885pg4_LoadData(dataLoan);
        }
        private void CAMLDSRE885pg4_BindData(CPageData dataLoan)
        {
            //dataLoan.sFinalLAmt_rep = GetString("sFinalLAmt");
            //dataLoan.sTermInYr_rep = GetString("sTermInYr");

            // ** PrincipalInterest
            dataLoan.sRe885ComparisonPrincipalInterestIsNotOffered = GetBool("sRe885ComparisonPrincipalInterestIsNotOffered");

            dataLoan.sRe885ComparisonPrincipalInterestNoteIR_rep = GetString("sRe885ComparisonPrincipalInterestNoteIR");

            dataLoan.sRe885ComparisonPrincipalInterestMinPmtFirst5Yrs_rep = GetString("sRe885ComparisonPrincipalInterestMinPmtFirst5Yrs");
            dataLoan.sRe885ComparisonPrincipalInterestMPmt6YrsNoRateChange_rep = GetString("sRe885ComparisonPrincipalInterestMPmt6YrsNoRateChange");
            dataLoan.sRe885ComparisonPrincipalInterestMPmt6Yrs2PercentRaise_rep = GetString("sRe885ComparisonPrincipalInterestMPmt6Yrs2PercentRaise");
            dataLoan.sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaise_rep = GetString("sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaise");

            //dataLoan.sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaiseDiff_rep = GetString("sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaiseDiff");
            dataLoan.sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaise_rep = GetString("sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaise");

            //dataLoan.sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaiseDiff_rep = GetString("sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaiseDiff");

            dataLoan.sRe885ComparisonPrincipalInterestBalanceAfter5Yrs_rep = GetString("sRe885ComparisonPrincipalInterestBalanceAfter5Yrs");
            //dataLoan.sRe885ComparisonPrincipalInterestBalanceAfter5YrsReduceBy_rep = GetString("sRe885ComparisonPrincipalInterestBalanceAfter5YrsReduceBy");

            // ** InterestOnly
            dataLoan.sRe885ComparisonInterestOnlyIsNotOffered = GetBool("sRe885ComparisonInterestOnlyIsNotOffered");

            dataLoan.sRe885ComparisonInterestOnlyNoteIR_rep = GetString("sRe885ComparisonInterestOnlyNoteIR");

            dataLoan.sRe885ComparisonInterestOnlyMinPmtFirst5Yrs_rep = GetString("sRe885ComparisonInterestOnlyMinPmtFirst5Yrs");
            dataLoan.sRe885ComparisonInterestOnlyMPmt6YrsNoRateChange_rep = GetString("sRe885ComparisonInterestOnlyMPmt6YrsNoRateChange");
            dataLoan.sRe885ComparisonInterestOnlyMPmt6Yrs2PercentRaise_rep = GetString("sRe885ComparisonInterestOnlyMPmt6Yrs2PercentRaise");
            dataLoan.sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaise_rep = GetString("sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaise");

            //dataLoan.sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaiseDiff_rep = GetString("sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaiseDiff");
            dataLoan.sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaise_rep = GetString("sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaise");

            //dataLoan.sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaiseDiff_rep = GetString("sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaiseDiff");

            dataLoan.sRe885ComparisonInterestOnlyBalanceAfter5Yrs_rep = GetString("sRe885ComparisonInterestOnlyBalanceAfter5Yrs");

            // ** 5YrsArm
            dataLoan.sRe885Comparison5YrsArmIsNotOffered = GetBool("sRe885Comparison5YrsArmIsNotOffered");

            dataLoan.sRe885Comparison5YrsArmInitialNoteIR_rep = GetString("sRe885Comparison5YrsArmInitialNoteIR");
            dataLoan.sRe885Comparison5YrsArmMaximumNoteIR_rep = GetString("sRe885Comparison5YrsArmMaximumNoteIR");

            dataLoan.sRe885Comparison5YrsArmMinPmtFirst5Yrs_rep = GetString("sRe885Comparison5YrsArmMinPmtFirst5Yrs");
            dataLoan.sRe885Comparison5YrsArmMPmt6YrsNoRateChange_rep = GetString("sRe885Comparison5YrsArmMPmt6YrsNoRateChange");
            dataLoan.sRe885Comparison5YrsArmMPmt6Yrs2PercentRaise_rep = GetString("sRe885Comparison5YrsArmMPmt6Yrs2PercentRaise");
            dataLoan.sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaise_rep = GetString("sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaise");

            //dataLoan.sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaiseDiff_rep = GetString("sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaiseDiff");
            dataLoan.sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaise_rep = GetString("sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaise");

            //dataLoan.sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaiseDiff_rep = GetString("sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaiseDiff");

            dataLoan.sRe885Comparison5YrsArmBalanceAfter5Yrs_rep = GetString("sRe885Comparison5YrsArmBalanceAfter5Yrs");
            //dataLoan.sRe885Comparison5YrsArmBalanceAfter5YrsReduceBy_rep = GetString("sRe885Comparison5YrsArmBalanceAfter5YrsReduceBy");

            // ** 5YrsArmInterestOnly
            dataLoan.sRe885Comparison5YrsArmInterestOnlyIsNotOffered = GetBool("sRe885Comparison5YrsArmInterestOnlyIsNotOffered");

            dataLoan.sRe885Comparison5YrsArmInterestOnlyInitialNoteIR_rep = GetString("sRe885Comparison5YrsArmInterestOnlyInitialNoteIR");
            dataLoan.sRe885Comparison5YrsArmInterestOnlyMaximumNoteIR_rep = GetString("sRe885Comparison5YrsArmInterestOnlyMaximumNoteIR");

            dataLoan.sRe885Comparison5YrsArmInterestOnlyMinPmtFirst5Yrs_rep = GetString("sRe885Comparison5YrsArmInterestOnlyMinPmtFirst5Yrs");
            dataLoan.sRe885Comparison5YrsArmInterestOnlyMPmt6YrsNoRateChange_rep = GetString("sRe885Comparison5YrsArmInterestOnlyMPmt6YrsNoRateChange");
            dataLoan.sRe885Comparison5YrsArmInterestOnlyMPmt6Yrs2PercentRaise_rep = GetString("sRe885Comparison5YrsArmInterestOnlyMPmt6Yrs2PercentRaise");
            dataLoan.sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaise_rep = GetString("sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaise");

            //dataLoan.sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaiseDiff_rep = GetString("sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaiseDiff");
            dataLoan.sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaise_rep = GetString("sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaise");

            //dataLoan.sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaiseDiff_rep = GetString("sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaiseDiff");

            dataLoan.sRe885Comparison5YrsArmInterestOnlyBalanceAfter5Yrs_rep = GetString("sRe885Comparison5YrsArmInterestOnlyBalanceAfter5Yrs");

            // ** Option Payment
            dataLoan.sRe885ComparisonOptionPaymentIsNotOffered = GetBool("sRe885ComparisonOptionPaymentIsNotOffered");

            dataLoan.sRe885ComparisonOptionPaymentFirstMonthNoteIR_rep = GetString("sRe885ComparisonOptionPaymentFirstMonthNoteIR");
            dataLoan.sRe885ComparisonOptionPaymentInitialNoteIR_rep = GetString("sRe885ComparisonOptionPaymentInitialNoteIR");
            dataLoan.sRe885ComparisonOptionPaymentMaximumNoteIR_rep = GetString("sRe885ComparisonOptionPaymentMaximumNoteIR");

            dataLoan.sRe885ComparisonOptionPaymentMinPmtFirst5Yrs_rep = GetString("sRe885ComparisonOptionPaymentMinPmtFirst5Yrs");
            dataLoan.sRe885ComparisonOptionPaymentMPmt6YrsNoRateChange_rep = GetString("sRe885ComparisonOptionPaymentMPmt6YrsNoRateChange");
            dataLoan.sRe885ComparisonOptionPaymentMPmt6Yrs2PercentRaise_rep = GetString("sRe885ComparisonOptionPaymentMPmt6Yrs2PercentRaise");
            dataLoan.sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaise_rep = GetString("sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaise");

            //dataLoan.sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaiseDiff_rep = GetString("sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaiseDiff");
            dataLoan.sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaise_rep = GetString("sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaise");

            //dataLoan.sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaiseDiff_rep = GetString("sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaiseDiff");

            dataLoan.sRe885ComparisonOptionPaymentBalanceAfter5Yrs_rep = GetString("sRe885ComparisonOptionPaymentBalanceAfter5Yrs");
            //dataLoan.sRe885ComparisonOptionPaymentBalanceAfter5YrsReduceBy_rep = GetString("sRe885ComparisonOptionPaymentBalanceAfter5YrsReduceBy");

            // ** Proposed Loan
            dataLoan.sRe885ComparisonProposedLoanTypeOfLoan = GetString("sRe885ComparisonProposedLoanTypeOfLoan");
            dataLoan.sRe885ComparisonProposedLoanAmortType = GetString("sRe885ComparisonProposedLoanAmortType");

            dataLoan.sRe885ComparisonProposedLoanProductDesc = GetString("sRe885ComparisonProposedLoanProductDesc");

            dataLoan.sRe885ComparisonProposedLoanMinPmtFirst5Yrs_rep = GetString("sRe885ComparisonProposedLoanMinPmtFirst5Yrs");
            dataLoan.sRe885ComparisonProposedLoanMPmt6YrsNoRateChange_rep = GetString("sRe885ComparisonProposedLoanMPmt6YrsNoRateChange");
            dataLoan.sRe885ComparisonProposedLoanMPmt6Yrs2PercentRaise_rep = GetString("sRe885ComparisonProposedLoanMPmt6Yrs2PercentRaise");
            dataLoan.sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaise_rep = GetString("sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaise");

            //dataLoan.sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaiseDiff_rep = GetString("sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaiseDiff");
            dataLoan.sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaise_rep = GetString("sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaise");

            //dataLoan.sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaiseDiff_rep = GetString("sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaiseDiff");

            dataLoan.sRe885ComparisonProposedLoanBalanceAfter5Yrs_rep = GetString("sRe885ComparisonProposedLoanBalanceAfter5Yrs");
            //dataLoan.sRe885ComparisonProposedLoanBalanceAfter5YrsReduceBy_rep = GetString("sRe885ComparisonProposedLoanBalanceAfter5YrsReduceBy");

            //dataLoan.sRe885ComparisionProposedLoanBalanceReduceYesNoDesc = GetString("sRe885ComparisionProposedLoanBalanceReduceYesNoDesc");
            //dataLoan.sRe885ComparisionProposedLoanBalanceReduceDesc = GetString("sRe885ComparisionProposedLoanBalanceReduceDesc");

            dataLoan.sRe885ComparisonProductsNotOfferedDeemReliableTri = GetTriState("sRe885ComparisonProductsNotOfferedDeemReliableTri");

            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew);
            gfeTil.PreparerName = GetString("GfeTilPreparerName");
            gfeTil.LicenseNumOfAgent = GetString("GfeTilLicenseNumOfAgent");
            gfeTil.CompanyName = GetString("GfeTilCompanyName");
            gfeTil.StreetAddr = GetString("GfeTilStreetAddr");
            gfeTil.City = GetString("GfeTilCity");
            gfeTil.State = GetString("GfeTilState");
            gfeTil.Zip = GetString("GfeTilZip");
            gfeTil.LicenseNumOfCompany = GetString("GfeTilLicenseNumOfCompany");
            gfeTil.AgentRoleT = (E_AgentRoleT)GetInt("CFM_AgentRoleT");
            gfeTil.IsLocked = GetBool("CFM_IsLocked");
            gfeTil.Update();
        }
        private void CAMLDSRE885pg4_LoadData(CPageData dataLoan)
        {
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sTermInYr", dataLoan.sTermInYr_rep);

            // ** PrincipalInterest
            SetResult("sRe885ComparisonPrincipalInterestIsNotOffered", dataLoan.sRe885ComparisonPrincipalInterestIsNotOffered);

            SetResult("sRe885ComparisonPrincipalInterestNoteIR", dataLoan.sRe885ComparisonPrincipalInterestNoteIR_rep);

            SetResult("sRe885ComparisonPrincipalInterestMinPmtFirst5Yrs", dataLoan.sRe885ComparisonPrincipalInterestMinPmtFirst5Yrs_rep);
            SetResult("sRe885ComparisonPrincipalInterestMPmt6YrsNoRateChange", dataLoan.sRe885ComparisonPrincipalInterestMPmt6YrsNoRateChange_rep);
            SetResult("sRe885ComparisonPrincipalInterestMPmt6Yrs2PercentRaise", dataLoan.sRe885ComparisonPrincipalInterestMPmt6Yrs2PercentRaise_rep);
            SetResult("sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaise", dataLoan.sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaise_rep);

            SetResult("sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaiseDiff", dataLoan.sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaiseDiff_rep);
            SetResult("sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaise", dataLoan.sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaise_rep);

            SetResult("sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaiseDiff", dataLoan.sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaiseDiff_rep);

            SetResult("sRe885ComparisonPrincipalInterestBalanceAfter5Yrs", dataLoan.sRe885ComparisonPrincipalInterestBalanceAfter5Yrs_rep);
            SetResult("sRe885ComparisonPrincipalInterestBalanceAfter5YrsReduceBy", dataLoan.sRe885ComparisonPrincipalInterestBalanceAfter5YrsReduceBy_rep);

            // ** InterestOnly
            SetResult("sRe885ComparisonInterestOnlyIsNotOffered", dataLoan.sRe885ComparisonInterestOnlyIsNotOffered);

            SetResult("sRe885ComparisonInterestOnlyNoteIR", dataLoan.sRe885ComparisonInterestOnlyNoteIR_rep);

            SetResult("sRe885ComparisonInterestOnlyMinPmtFirst5Yrs", dataLoan.sRe885ComparisonInterestOnlyMinPmtFirst5Yrs_rep);
            SetResult("sRe885ComparisonInterestOnlyMPmt6YrsNoRateChange", dataLoan.sRe885ComparisonInterestOnlyMPmt6YrsNoRateChange_rep);
            SetResult("sRe885ComparisonInterestOnlyMPmt6Yrs2PercentRaise", dataLoan.sRe885ComparisonInterestOnlyMPmt6Yrs2PercentRaise_rep);
            SetResult("sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaise", dataLoan.sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaise_rep);

            SetResult("sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaiseDiff", dataLoan.sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaiseDiff_rep);
            SetResult("sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaise", dataLoan.sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaise_rep);

            SetResult("sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaiseDiff", dataLoan.sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaiseDiff_rep);

            SetResult("sRe885ComparisonInterestOnlyBalanceAfter5Yrs", dataLoan.sRe885ComparisonInterestOnlyBalanceAfter5Yrs_rep);

            // ** 5YrsArm
            SetResult("sRe885Comparison5YrsArmIsNotOffered", dataLoan.sRe885Comparison5YrsArmIsNotOffered);

            SetResult("sRe885Comparison5YrsArmInitialNoteIR", dataLoan.sRe885Comparison5YrsArmInitialNoteIR_rep);
            SetResult("sRe885Comparison5YrsArmMaximumNoteIR", dataLoan.sRe885Comparison5YrsArmMaximumNoteIR_rep);

            SetResult("sRe885Comparison5YrsArmMinPmtFirst5Yrs", dataLoan.sRe885Comparison5YrsArmMinPmtFirst5Yrs_rep);
            SetResult("sRe885Comparison5YrsArmMPmt6YrsNoRateChange", dataLoan.sRe885Comparison5YrsArmMPmt6YrsNoRateChange_rep);
            SetResult("sRe885Comparison5YrsArmMPmt6Yrs2PercentRaise", dataLoan.sRe885Comparison5YrsArmMPmt6Yrs2PercentRaise_rep);
            SetResult("sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaise", dataLoan.sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaise_rep);

            SetResult("sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaiseDiff", dataLoan.sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaiseDiff_rep);
            SetResult("sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaise", dataLoan.sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaise_rep);

            SetResult("sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaiseDiff", dataLoan.sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaiseDiff_rep);

            SetResult("sRe885Comparison5YrsArmBalanceAfter5Yrs", dataLoan.sRe885Comparison5YrsArmBalanceAfter5Yrs_rep);
            SetResult("sRe885Comparison5YrsArmBalanceAfter5YrsReduceBy", dataLoan.sRe885Comparison5YrsArmBalanceAfter5YrsReduceBy_rep);

            // ** 5YrsArmInterestOnly
            SetResult("sRe885Comparison5YrsArmInterestOnlyIsNotOffered", dataLoan.sRe885Comparison5YrsArmInterestOnlyIsNotOffered);

            SetResult("sRe885Comparison5YrsArmInterestOnlyInitialNoteIR", dataLoan.sRe885Comparison5YrsArmInterestOnlyInitialNoteIR_rep);
            SetResult("sRe885Comparison5YrsArmInterestOnlyMaximumNoteIR", dataLoan.sRe885Comparison5YrsArmInterestOnlyMaximumNoteIR_rep);

            SetResult("sRe885Comparison5YrsArmInterestOnlyMinPmtFirst5Yrs", dataLoan.sRe885Comparison5YrsArmInterestOnlyMinPmtFirst5Yrs_rep);
            SetResult("sRe885Comparison5YrsArmInterestOnlyMPmt6YrsNoRateChange", dataLoan.sRe885Comparison5YrsArmInterestOnlyMPmt6YrsNoRateChange_rep);
            SetResult("sRe885Comparison5YrsArmInterestOnlyMPmt6Yrs2PercentRaise", dataLoan.sRe885Comparison5YrsArmInterestOnlyMPmt6Yrs2PercentRaise_rep);
            SetResult("sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaise", dataLoan.sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaise_rep);

            SetResult("sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaiseDiff", dataLoan.sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaiseDiff_rep);
            SetResult("sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaise", dataLoan.sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaise_rep);

            SetResult("sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaiseDiff", dataLoan.sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaiseDiff_rep);

            SetResult("sRe885Comparison5YrsArmInterestOnlyBalanceAfter5Yrs", dataLoan.sRe885Comparison5YrsArmInterestOnlyBalanceAfter5Yrs_rep);

            // ** Option Payment
            SetResult("sRe885ComparisonOptionPaymentIsNotOffered", dataLoan.sRe885ComparisonOptionPaymentIsNotOffered);

            SetResult("sRe885ComparisonOptionPaymentFirstMonthNoteIR", dataLoan.sRe885ComparisonOptionPaymentFirstMonthNoteIR_rep);
            SetResult("sRe885ComparisonOptionPaymentInitialNoteIR", dataLoan.sRe885ComparisonOptionPaymentInitialNoteIR_rep);
            SetResult("sRe885ComparisonOptionPaymentMaximumNoteIR", dataLoan.sRe885ComparisonOptionPaymentMaximumNoteIR_rep);

            SetResult("sRe885ComparisonOptionPaymentMinPmtFirst5Yrs", dataLoan.sRe885ComparisonOptionPaymentMinPmtFirst5Yrs_rep);
            SetResult("sRe885ComparisonOptionPaymentMPmt6YrsNoRateChange", dataLoan.sRe885ComparisonOptionPaymentMPmt6YrsNoRateChange_rep);
            SetResult("sRe885ComparisonOptionPaymentMPmt6Yrs2PercentRaise", dataLoan.sRe885ComparisonOptionPaymentMPmt6Yrs2PercentRaise_rep);
            SetResult("sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaise", dataLoan.sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaise_rep);

            SetResult("sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaiseDiff", dataLoan.sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaiseDiff_rep);
            SetResult("sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaise", dataLoan.sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaise_rep);

            SetResult("sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaiseDiff", dataLoan.sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaiseDiff_rep);

            SetResult("sRe885ComparisonOptionPaymentBalanceAfter5Yrs", dataLoan.sRe885ComparisonOptionPaymentBalanceAfter5Yrs_rep);
            SetResult("sRe885ComparisonOptionPaymentBalanceAfter5YrsReduceBy", dataLoan.sRe885ComparisonOptionPaymentBalanceAfter5YrsReduceBy_rep);

            // ** Proposed Loan
            SetResult("sRe885ComparisonProposedLoanTypeOfLoan", dataLoan.sRe885ComparisonProposedLoanTypeOfLoan);
            SetResult("sRe885ComparisonProposedLoanAmortType", dataLoan.sRe885ComparisonProposedLoanAmortType);

            SetResult("sRe885ComparisonProposedLoanProductDesc", dataLoan.sRe885ComparisonProposedLoanProductDesc);

            SetResult("sRe885ComparisonProposedLoanMinPmtFirst5Yrs", dataLoan.sRe885ComparisonProposedLoanMinPmtFirst5Yrs_rep);
            SetResult("sRe885ComparisonProposedLoanMPmt6YrsNoRateChange", dataLoan.sRe885ComparisonProposedLoanMPmt6YrsNoRateChange_rep);
            SetResult("sRe885ComparisonProposedLoanMPmt6Yrs2PercentRaise", dataLoan.sRe885ComparisonProposedLoanMPmt6Yrs2PercentRaise_rep);
            SetResult("sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaise", dataLoan.sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaise_rep);

            SetResult("sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaiseDiff", dataLoan.sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaiseDiff_rep);
            SetResult("sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaise", dataLoan.sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaise_rep);

            SetResult("sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaiseDiff", dataLoan.sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaiseDiff_rep);

            SetResult("sRe885ComparisonProposedLoanBalanceAfter5Yrs", dataLoan.sRe885ComparisonProposedLoanBalanceAfter5Yrs_rep);
            SetResult("sRe885ComparisonProposedLoanBalanceAfter5YrsReduceBy", dataLoan.sRe885ComparisonProposedLoanBalanceAfter5YrsReduceBy_rep);

            SetResult("sRe885ComparisionProposedLoanBalanceReduceYesNoDesc", dataLoan.sRe885ComparisionProposedLoanBalanceReduceYesNoDesc);
            SetResult("sRe885ComparisionProposedLoanBalanceReduceDesc", dataLoan.sRe885ComparisionProposedLoanBalanceReduceDesc);

            SetResult("sRe885ComparisonProductsNotOfferedDeemReliableTri", dataLoan.sRe885ComparisonProductsNotOfferedDeemReliableTri);

            // -- Your Gross Income
            SetResult("sRe885ComparisonPrincipalInterestMinPmt_sLTotI",  dataLoan.sLTotI_rep);
            SetResult("sRe885ComparisonInterestOnlyMinPmt_sLTotI",  dataLoan.sLTotI_rep);
            SetResult("sRe885Comparison5YrsArmMinPmt_sLTotI",  dataLoan.sLTotI_rep);
            SetResult("sRe885Comparison5YrsArmInterestMinPmt_sLTotI",  dataLoan.sLTotI_rep);
            SetResult("sRe885ComparisonOptionPaymentMinPmt_sLTotI",  dataLoan.sLTotI_rep);
            SetResult("sRe885ComparisonProposedLoanMinPmt_sLTotI",  dataLoan.sLTotI_rep);

            SetResult("sRe885ComparisonPrincipalInterestMaxPmt_sLTotI",  dataLoan.sLTotI_rep);
            SetResult("sRe885ComparisonInterestOnlyMaxPmt_sLTotI",  dataLoan.sLTotI_rep);
            SetResult("sRe885Comparison5YrsArmMaxPmt_sLTotI",  dataLoan.sLTotI_rep);
            SetResult("sRe885Comparison5YrsArMaxterestMaxPmt_sLTotI",  dataLoan.sLTotI_rep);
            SetResult("sRe885ComparisonOptionPaymentMaxPmt_sLTotI",  dataLoan.sLTotI_rep);
            SetResult("sRe885ComparisonProposedLoanMaxPmt_sLTotI", dataLoan.sLTotI_rep);
        }
        #endregion

        #region MLDS Page 3
        private void CAMLDSRE885pg3_CalculateData()
        {
        }
        private void CAMLDSRE885pg3_SaveData()
        {
            Guid loanID = GetGuid("loanid");
            CPageData dataLoan = this.ConstructPageData(loanID);
            dataLoan.InitSave(sFileVersion);

            CAMLDSRE885pg3_BindData(dataLoan);

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            CAMLDSRE885pg3_LoadData(dataLoan);
        }
        private void CAMLDSRE885pg3_BindData(CPageData dataLoan)
        {
            dataLoan.sLienholder1NmBefore = GetString("sLienholder1NmBefore");
            dataLoan.sLien1AmtBefore_rep = GetString("sLien1AmtBefore");
            dataLoan.sLien1PriorityBefore = GetString("sLien1PriorityBefore");

            dataLoan.sLienholder2NmBefore = GetString("sLienholder2NmBefore");
            dataLoan.sLien2AmtBefore_rep = GetString("sLien2AmtBefore");
            dataLoan.sLien2PriorityBefore = GetString("sLien2PriorityBefore");

            dataLoan.sLienholder3NmBefore = GetString("sLienholder3NmBefore");
            dataLoan.sLien3AmtBefore_rep = GetString("sLien3AmtBefore");
            dataLoan.sLien3PriorityBefore = GetString("sLien3PriorityBefore");

            dataLoan.sLienholder1NmAfter = GetString("sLienholder1NmAfter");
            dataLoan.sLien1AmtAfter_rep = GetString("sLien1AmtAfter");
            dataLoan.sLien1PriorityAfter = GetString("sLien1PriorityAfter");

            dataLoan.sLienholder2NmAfter = GetString("sLienholder2NmAfter");
            dataLoan.sLien2AmtAfter_rep = GetString("sLien2AmtAfter");
            dataLoan.sLien2PriorityAfter = GetString("sLien2PriorityAfter");

            dataLoan.sLienholder3NmAfter = GetString("sLienholder3NmAfter");
            dataLoan.sLien3AmtAfter_rep = GetString("sLien3AmtAfter");
            dataLoan.sLien3PriorityAfter = GetString("sLien3PriorityAfter");

            dataLoan.sBrokControlledFundT = (E_sBrokControlledFundT)GetInt("sBrokControlledFundT");
            dataLoan.sMldsIsNoDocTri = GetTriState("sMldsIsNoDocTri");
        }
        private void CAMLDSRE885pg3_LoadData(CPageData dataLoan)
        {
        }
        #endregion


        #region MLDS Page 2
        private void CAMLDSRE885pg2_CalculateData() 
        {
            Guid loanID = GetGuid("loanid");
            CPageData dataLoan = ConstructPageData(loanID);
            dataLoan.InitLoad();

            CAMLDSRE885pg2_BindData(dataLoan);

            CAMLDSRE885pg2_LoadData(dataLoan);


        }
        private void CAMLDSRE885pg2_SaveData() 
        {
            Guid loanID = GetGuid("loanid");
            CPageData dataLoan = ConstructPageData(loanID);
            dataLoan.InitSave(sFileVersion);

            CAMLDSRE885pg2_BindData(dataLoan);

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            CAMLDSRE885pg2_LoadData(dataLoan);
        }
        private void CAMLDSRE885pg2_BindData(CPageData dataLoan) 
        {

            dataLoan.sDisabilityIns_rep = GetString("sDisabilityIns");
            dataLoan.sFinMethT = (E_sFinMethT) GetInt("sFinMethT");
            dataLoan.sMldsPpmtBaseT = (E_sMldsPpmtBaseT) GetInt("sMldsPpmtBaseT");
            dataLoan.sMldsPpmtMonMax_rep = GetString("sMldsPpmtMonMax");
            dataLoan.sMldsPpmtT = (E_sMldsPpmtT) GetInt("sMldsPpmtT");
            dataLoan.sNoteIR_rep = GetString("sNoteIR");
            dataLoan.sTerm_rep = GetString("sTerm");
            dataLoan.sMldsImpoundOtherDesc = GetString("sMldsImpoundOtherDesc");
            dataLoan.sMldsImpoundIncludeOther = GetBool("sMldsImpoundIncludeOther");
            dataLoan.sMldsImpoundIncludeFloodIns = GetBool("sMldsImpoundIncludeFloodIns");
            dataLoan.sMldsImpoundIncludeMIns = GetBool("sMldsImpoundIncludeMIns");
            dataLoan.sMldsImpoundIncludeRealETx = GetBool("sMldsImpoundIncludeRealETx");
            dataLoan.sMldsImpoundIncludeHazIns = GetBool("sMldsImpoundIncludeHazIns");
            dataLoan.sMldsHasImpound = GetString("sMldsHasImpound") ==  "1";

            dataLoan.sMldsPpmtPeriod_rep = GetString("sMldsPpmtPeriod");
            dataLoan.sMldsPpmtOtherDetail = GetString("sMldsPpmtOtherDetail");
            dataLoan.sMldsPpmtMaxAmt_rep = GetString("sMldsPpmtMaxAmt");


            dataLoan.sMldsRe885PmtOptionEndMons_rep = GetString("sMldsRe885PmtOptionEndMons");
            dataLoan.sMldsRe885PmtOptionEndOrigBalPc_rep = GetString("sMldsRe885PmtOptionEndOrigBalPc");
            dataLoan.sMldsRe885OptionArmMinPayPeriod_rep = GetString("sMldsRe885OptionArmMinPayPeriod");
            dataLoan.sMldsRe885MaxMonthlyPmt_rep = GetString("sMldsRe885MaxMonthlyPmt");
            dataLoan.sMldsRe885MaxMonthlyPmtMons_rep = GetString("sMldsRe885MaxMonthlyPmtMons");
            dataLoan.sMldsRe885NegAmortLoanBalance_rep = GetString("sMldsRe885NegAmortLoanBalance");
            dataLoan.sMldsRe885InitialAdjRPeriod_rep = GetString("sMldsRe885InitialAdjRPeriod");
            dataLoan.sMldsRe885FullyIndexR_rep = GetString("sMldsRe885FullyIndexR");
            dataLoan.sMldsRe885MaxLifeCapRAdjustable_rep = GetString("sMldsRe885MaxLifeCapRAdjustable");
            dataLoan.sBalloonPmt = GetBool("sBalloonPmt");
            dataLoan.sRAdjCapRAdjustableMlds_rep = GetString("sRAdjCapRAdjustableMlds");
            dataLoan.sRAdjCapMonAdjustableMlds_rep = GetString("sRAdjCapMonAdjustableMlds");
        }
        private void CAMLDSRE885pg2_LoadData(CPageData dataLoan) 
        {
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sTotEstScMlds", dataLoan.sTotEstScMlds_rep);
            SetResult("sDisabilityIns", dataLoan.sDisabilityIns_rep);
            SetResult("sTotDeductFromFinalLAmt", dataLoan.sTotDeductFromFinalLAmt_rep);
            SetResult("sTerm", dataLoan.sTerm_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sFinMethT", dataLoan.sFinMethT);
            SetResult("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            SetResult("sFinalBalloonPmt", dataLoan.sFinalBalloonPmt_rep);
            SetResult("sFinalBalloonPmtDueD", dataLoan.sFinalBalloonPmtDueD_rep);
            SetResult("sMldsPpmtT", dataLoan.sMldsPpmtT);
            SetResult("sMldsPpmtPeriod", dataLoan.sMldsPpmtPeriod_rep);
            SetResult("sMldsPpmtMaxAmt", dataLoan.sMldsPpmtMaxAmt_rep);
            SetResult("sMldsPpmtBaseT", dataLoan.sMldsPpmtBaseT);
            SetResult("sMldsPpmtMonMax", dataLoan.sMldsPpmtMonMax_rep);
            SetResult("sMldsPpmtOtherDetail", dataLoan.sMldsPpmtOtherDetail);
            SetResult("sMldsHasImpound", dataLoan.sMldsHasImpound ? "1" : "0");
            SetResult("sMldsImpoundIncludeOther", dataLoan.sMldsImpoundIncludeOther);
            SetResult("sMldsImpoundIncludeFloodIns", dataLoan.sMldsImpoundIncludeFloodIns);
            SetResult("sMldsImpoundIncludeMIns", dataLoan.sMldsImpoundIncludeMIns);
            SetResult("sMldsImpoundIncludeRealETx", dataLoan.sMldsImpoundIncludeRealETx);
            SetResult("sMldsImpoundIncludeHazIns", dataLoan.sMldsImpoundIncludeHazIns);
            SetResult("sMldsImpoundOtherDesc", dataLoan.sMldsImpoundOtherDesc);
            SetResult("sTotEstFntcCamlds", dataLoan.sTotEstFntcCamlds_rep);
            SetResult("sPurchPrice_sRefPdOffAmtCamlds", dataLoan.sPurchPrice_sRefPdOffAmtCamlds_rep);

            SetResult("sOtherPmtObligationDescMlds", dataLoan.sOtherPmtObligationDescMlds);
            SetResult("sOtherPmtObligationMlds", dataLoan.sOtherPmtObligationMlds_rep);
            SetResult("sMldsMonthlyImpoundPmtPerYear_NoImpound", dataLoan.sMldsMonthlyImpoundPmtPerYear_NoImpound);
            SetResult("sMldsMonthlyImpoundPmt_Impound", dataLoan.sMldsMonthlyImpoundPmt_Impound);
            /* OPM 24617 - In addition to the new TextBox1, 2, etc.  These are in Vinh's aspx:
            sPurchPrice_sRefPdOffAmtCamlds -- DONE dd 7/31/09  (Make it a readonly)
            sOtherPmtObligationDescMlds -- DONE dd 7/31/09 (Make It a readonly)
            sOtherPmtObligationMlds -- DONE dd 7/31/09 (Make it a readonly)
            sIsCashbackToUserMlds -- dd I don't think we need this in UI.
            sTotEstFntcCamldsAbsVal -- DONE By dd. 7/31/09 
             sMldsMonthlyImpoundPmtPerYear_NoImpound -- DONE dd 7/31/09 (make it a readonly)
             */
            SetResult("sMldsRe885PmtOptionEndMons", dataLoan.sMldsRe885PmtOptionEndMons_rep);
            SetResult("sMldsRe885PmtOptionEndOrigBalPc", dataLoan.sMldsRe885PmtOptionEndOrigBalPc_rep);
            SetResult("sMldsRe885OptionArmMinPayPeriod", dataLoan.sMldsRe885OptionArmMinPayPeriod_rep);
            SetResult("sMldsRe885MaxMonthlyPmt", dataLoan.sMldsRe885MaxMonthlyPmt_rep);
            SetResult("sMldsRe885MaxMonthlyPmtMons", dataLoan.sMldsRe885MaxMonthlyPmtMons_rep);
            SetResult("sMldsRe885NegAmortLoanBalance", dataLoan.sMldsRe885NegAmortLoanBalance_rep);
            SetResult("sMldsRe885InitialAdjRPeriod", dataLoan.sMldsRe885InitialAdjRPeriod_rep);
            SetResult("sMldsRe885FullyIndexR", dataLoan.sMldsRe885FullyIndexR_rep);
            SetResult("sMldsRe885MaxLifeCapRAdjustable", dataLoan.sMldsRe885MaxLifeCapRAdjustable_rep);
            SetResult("sBalloonPmt", dataLoan.sBalloonPmt);
            SetResult("sRAdjCapRAdjustableMlds", dataLoan.sRAdjCapRAdjustableMlds_rep);
            SetResult("sRAdjCapMonAdjustableMlds", dataLoan.sRAdjCapMonAdjustableMlds_rep);
        }
        #endregion

        #region MLDS Page 1
        private void CAMLDSRE885pg1_CalculateData() 
        {
            Guid loanID = GetGuid("loanid");
            CPageData dataLoan = ConstructPageData(loanID);
            dataLoan.InitLoad();

            CAMLDSRE885pg1_BindData(dataLoan, false);

            CAMLDSRE885pg1_LoadData(dataLoan);
        }
        private void CAMLDSRE885pg1_SaveData() 
        {
            Guid loanID = GetGuid("loanid");
            bool byPassBgCalcForGfeAsDefault = GetString("ByPassBgCalcForGfeAsDefault", "0") == "1";

            CPageData dataLoan = ConstructPageData(loanID);
            dataLoan.InitSave(sFileVersion);
            CAMLDSRE885pg1_BindData(dataLoan, true);
            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            CAMLDSRE885pg1_LoadData(dataLoan);

            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            SqlParameter[] parameters = {
                                            new SqlParameter("@ByPassBgCalcForGfeAsDefault", byPassBgCalcForGfeAsDefault)
                                            , new SqlParameter("@UserId", principal.UserId)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "SetPassBgCalcForGfeAsDefaultByUserId", 3, parameters);

        }
        private void CAMLDSRE885pg1_LoadData(CPageData dataLoan) 
        {
            SetResult("s1006ProHExp",       dataLoan.s1006ProHExp_rep);
            SetResult("s1006Rsrv",          dataLoan.s1006Rsrv_rep);
            SetResult("s1006RsrvMon",       dataLoan.s1006RsrvMon_rep);
            SetResult("s1007ProHExp",       dataLoan.s1007ProHExp_rep);
            SetResult("s1007Rsrv",          dataLoan.s1007Rsrv_rep);
            SetResult("s1007RsrvMon",       dataLoan.s1007RsrvMon_rep);
            SetResult("sProU3Rsrv",         dataLoan.sProU3Rsrv_rep);
            SetResult("sU3Rsrv",            dataLoan.sU3Rsrv_rep);
            SetResult("sU3RsrvMon",         dataLoan.sU3RsrvMon_rep);
            SetResult("sProU4Rsrv",         dataLoan.sProU4Rsrv_rep);
            SetResult("sU4Rsrv",            dataLoan.sU4Rsrv_rep);
            SetResult("sU4RsrvMon",         dataLoan.sU4RsrvMon_rep);
            SetResult("s900U1Pia",          dataLoan.s900U1Pia_rep);
            SetResult("s904Pia",            dataLoan.s904Pia_rep);
            SetResult("sAggregateAdjRsrv",  dataLoan.sAggregateAdjRsrv_rep);
            SetResult("sAttorneyF",         dataLoan.sAttorneyF_rep);
            SetResult("sBrokComp1Mlds",     dataLoan.sBrokComp1Mlds_rep);
            SetResult("sBrokComp1MldsLckd", dataLoan.sBrokComp1MldsLckd);
            SetResult("sBrokComp2Mlds",     dataLoan.sBrokComp2Mlds_rep);
            SetResult("sBrokComp2MldsLckd", dataLoan.sBrokComp2MldsLckd);
            SetResult("sCountyRtc",         dataLoan.sCountyRtc_rep);
            SetResult("sDaysInYr",          dataLoan.sDaysInYr_rep);
            SetResult("sDocPrepF",          dataLoan.sDocPrepF_rep);
            SetResult("sEscrowF",           dataLoan.sEscrowF_rep);
            SetResult("sFinalLAmt",         dataLoan.sFinalLAmt_rep);
            SetResult("sFloodInsRsrv",      dataLoan.sFloodInsRsrv_rep);
            SetResult("sFloodInsRsrvMon",   dataLoan.sFloodInsRsrvMon_rep);
            SetResult("sHazInsPia",         dataLoan.sHazInsPia_rep);
            SetResult("sHazInsPiaMon",      dataLoan.sHazInsPiaMon_rep);
            SetResult("sHazInsRsrv",        dataLoan.sHazInsRsrv_rep);
            SetResult("sHazInsRsrvMon",     dataLoan.sHazInsRsrvMon_rep);
            SetResult("sIPerDay",           dataLoan.sIPerDay_rep);
            SetResult("sIPia",              dataLoan.sIPia_rep);
            SetResult("sIPiaDy",            dataLoan.sIPiaDy_rep);
            SetResult("sInspectF",          dataLoan.sInspectF_rep);
            SetResult("sLDiscnt",           dataLoan.sLDiscnt_rep);
            SetResult("sLOrigF",            dataLoan.sLOrigF_rep);
            SetResult("sMBrokF",            dataLoan.sMBrokF_rep);
            SetResult("sMInsRsrv",          dataLoan.sMInsRsrv_rep);
            SetResult("sMInsRsrvMon",       dataLoan.sMInsRsrvMon_rep);
            SetResult("sNotaryF",           dataLoan.sNotaryF_rep);
            SetResult("sPestInspectF",      dataLoan.sPestInspectF_rep);
            SetResult("sProFloodIns",       dataLoan.sProFloodIns_rep);
            SetResult("sProHazIns",         dataLoan.sProHazIns_rep);
            SetResult("sProHazInsMb",       dataLoan.sProHazInsMb_rep);
            SetResult("sProHazInsR",        dataLoan.sProHazInsR_rep);
            SetResult("sProHazInsT",        dataLoan.sProHazInsT.ToString("D"));
            SetResult("sProMIns",           dataLoan.sProMIns_rep);
            SetResult("sProMInsMb",         dataLoan.sProMInsMb_rep);
            SetResult("sProMInsR",          dataLoan.sProMInsR_rep);
            SetResult("sProMInsT",          dataLoan.sProMInsT.ToString("D"));
            SetResult("sProRealETx",        dataLoan.sProRealETx_rep);
            SetResult("sProRealETxBaseAmt", dataLoan.sProRealETxBaseAmt_rep);
            SetResult("sProSchoolTx",       dataLoan.sProSchoolTx_rep);
            SetResult("sRealETxRsrv",       dataLoan.sRealETxRsrv_rep);
            SetResult("sRealETxRsrvMon",    dataLoan.sRealETxRsrvMon_rep);
            SetResult("sRecF",              dataLoan.sRecF_rep);
            SetResult("sSchoolTxRsrv",      dataLoan.sSchoolTxRsrv_rep);
            SetResult("sSchoolTxRsrvMon",   dataLoan.sSchoolTxRsrvMon_rep);
            SetResult("sStateRtc",          dataLoan.sStateRtc_rep);
            SetResult("sTitleInsF",         dataLoan.sTitleInsF_rep);
            SetResult("sTotEstScMlds",          dataLoan.sTotEstScMlds_rep);
            SetResult("sU1GovRtc",          dataLoan.sU1GovRtc_rep);
            SetResult("sU2GovRtc",          dataLoan.sU2GovRtc_rep);
            SetResult("sU3GovRtc",          dataLoan.sU3GovRtc_rep);
            SetResult("sVaFf",              dataLoan.sVaFf_rep);
            SetResult("sBrokComp1Pc",       dataLoan.sBrokComp1Pc_rep);

            
            SetResult("sTotCcPto",          dataLoan.sTotCcPto_rep);
            SetResult("sTotCcPtb",          dataLoan.sTotCcPtb_rep);
            SetResult("sIPiaDyLckd", dataLoan.sIPiaDyLckd);
            SetResult("sConsummationD", dataLoan.sConsummationD_rep);
            SetResult("sConsummationDLckd", dataLoan.sConsummationDLckd);

            SetResult("sGfeOriginatorCompF", dataLoan.sGfeOriginatorCompF_rep);
            SetResult("sOriginatorCompensationPaymentSourceT", dataLoan.sOriginatorCompensationPaymentSourceT);

            SetResult("sGfeOriginatorCompFPc", dataLoan.sGfeOriginatorCompFPc_rep);
            SetResult("sGfeOriginatorCompFMb", dataLoan.sGfeOriginatorCompFMb_rep);
            SetResult("sGfeOriginatorCompFBaseT", dataLoan.sGfeOriginatorCompFBaseT);

            SetResult("sSchedDueD1Lckd", dataLoan.sSchedDueD1Lckd);
            SetResult("sSchedDueD1", dataLoan.sSchedDueD1_rep);

            SetResult("sEstCloseDLckd", dataLoan.sEstCloseDLckd);
            SetResult("sEstCloseD", dataLoan.sEstCloseD_rep);
        }
        private void CAMLDSRE885pg1_BindData(CPageData dataLoan, bool isBindAll) 
        {
            if (isBindAll) 
            {
                CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.CreateNew);
                agent.CompanyName = GetString("sAgentLenderCompanyName");
                agent.Update();

                IPreparerFields gfeTil = dataLoan.GetPreparerOfForm( E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew );
                gfeTil.PrepareDate_rep = GetString("GfeTilPrepareDate");
                gfeTil.Update();

                dataLoan.s1006ProHExpDesc   = GetString("s1006ProHExpDesc");
                dataLoan.s1007ProHExpDesc   = GetString("s1007ProHExpDesc");
                if (Broker.EnableAdditionalSection1000CustomFees)
                {
                    dataLoan.sU3RsrvDesc    = GetString("sU3RsrvDesc");
                    dataLoan.sU4RsrvDesc    = GetString("sU4RsrvDesc");
                }
                dataLoan.s800U1FCode        = GetString("s800U1FCode");
                dataLoan.s800U1FDesc        = GetString("s800U1FDesc");
                dataLoan.s800U2FCode        = GetString("s800U2FCode");
                dataLoan.s800U2FDesc        = GetString("s800U2FDesc");
                dataLoan.s800U3FCode        = GetString("s800U3FCode");
                dataLoan.s800U3FDesc        = GetString("s800U3FDesc");
                dataLoan.s800U4FCode        = GetString("s800U4FCode");
                dataLoan.s800U4FDesc        = GetString("s800U4FDesc");
                dataLoan.s800U5FCode        = GetString("s800U5FCode");
                dataLoan.s800U5FDesc        = GetString("s800U5FDesc");
                dataLoan.s900U1PiaCode      = GetString("s900U1PiaCode");
                dataLoan.s900U1PiaDesc      = GetString("s900U1PiaDesc");
                dataLoan.s904PiaDesc        = GetString("s904PiaDesc");
                dataLoan.sCcTemplateNm      = GetString("sCcTemplateNm");
                dataLoan.sCountyRtcDesc     = GetString("sCountyRtcDesc");
                dataLoan.sLpTemplateNm      = GetString("sLpTemplateNm");
                dataLoan.sStateRtcDesc      = GetString("sStateRtcDesc");
                dataLoan.sU1GovRtcCode      = GetString("sU1GovRtcCode");
                dataLoan.sU1GovRtcDesc      = GetString("sU1GovRtcDesc");
                dataLoan.sU1ScCode          = GetString("sU1ScCode");
                dataLoan.sU1ScDesc          = GetString("sU1ScDesc");
                dataLoan.sU1TcCode          = GetString("sU1TcCode");
                dataLoan.sU1TcDesc          = GetString("sU1TcDesc");
                dataLoan.sU2GovRtcCode      = GetString("sU2GovRtcCode");
                dataLoan.sU2GovRtcDesc      = GetString("sU2GovRtcDesc");
                dataLoan.sU2ScCode          = GetString("sU2ScCode");
                dataLoan.sU2ScDesc          = GetString("sU2ScDesc");
                dataLoan.sU2TcCode          = GetString("sU2TcCode");
                dataLoan.sU2TcDesc          = GetString("sU2TcDesc");
                dataLoan.sU3GovRtcCode      = GetString("sU3GovRtcCode");
                dataLoan.sU3GovRtcDesc      = GetString("sU3GovRtcDesc");
                dataLoan.sU3ScCode          = GetString("sU3ScCode");
                dataLoan.sU3ScDesc          = GetString("sU3ScDesc");
                dataLoan.sU3TcCode          = GetString("sU3TcCode");
                dataLoan.sU3TcDesc          = GetString("sU3TcDesc");
                dataLoan.sU4ScCode          = GetString("sU4ScCode");
                dataLoan.sU4ScDesc          = GetString("sU4ScDesc");
                dataLoan.sU4TcCode          = GetString("sU4TcCode");
                dataLoan.sU4TcDesc          = GetString("sU4TcDesc");
                dataLoan.sU5ScCode          = GetString("sU5ScCode");
                dataLoan.sU5ScDesc          = GetString("sU5ScDesc");

            }

            dataLoan.s1006ProHExp_rep      = GetString("s1006ProHExp");
            dataLoan.s1006RsrvMon_rep      = GetString("s1006RsrvMon");
            dataLoan.s1007ProHExp_rep      = GetString("s1007ProHExp");
            dataLoan.s1007RsrvMon_rep      = GetString("s1007RsrvMon");
            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                dataLoan.sProU3Rsrv_rep    = GetString("sProU3Rsrv");
                dataLoan.sU3RsrvMon_rep    = GetString("sU3RsrvMon");
                dataLoan.sProU4Rsrv_rep    = GetString("sProU4Rsrv");
                dataLoan.sU4RsrvMon_rep    = GetString("sU4RsrvMon");
            }
            dataLoan.s800U1F_rep           = GetString("s800U1F");
            dataLoan.s800U2F_rep           = GetString("s800U2F");
            dataLoan.s800U3F_rep           = GetString("s800U3F");
            dataLoan.s800U4F_rep           = GetString("s800U4F");
            dataLoan.s800U5F_rep           = GetString("s800U5F");
            dataLoan.s900U1Pia_rep         = GetString("s900U1Pia");
            dataLoan.s904Pia_rep           = GetString("s904Pia");
            dataLoan.sAggregateAdjRsrv_rep = GetString("sAggregateAdjRsrv");
            dataLoan.sApprFPaid            = GetBool("sApprFPaid");
            dataLoan.sApprF_rep            = GetString("sApprF");
            dataLoan.sAttorneyF_rep        = GetString("sAttorneyF");
            dataLoan.sCountyRtcBaseT = (E_PercentBaseT)GetInt("sCountyRtcBaseT");
            dataLoan.sCountyRtcMb_rep      = GetString("sCountyRtcMb");
            dataLoan.sCountyRtcPc_rep      = GetString("sCountyRtcPc");
            dataLoan.sCrFPaid              = GetBool("sCrFPaid");
            dataLoan.sCrF_rep              = GetString("sCrF");
            dataLoan.sDaysInYr_rep         = GetString("sDaysInYr");
            dataLoan.sDocPrepF_rep         = GetString("sDocPrepF");
            dataLoan.sDue_rep              = GetString("sDue");
            dataLoan.sEscrowFTable         = GetString("sEscrowFTable");
            dataLoan.sEscrowF_rep          = GetString("sEscrowF");
            dataLoan.sEstCloseDLckd       = GetBool("sEstCloseDLckd");
            dataLoan.sEstCloseD_rep        = GetString("sEstCloseD");
            dataLoan.sFloodInsRsrvMon_rep  = GetString("sFloodInsRsrvMon");
            dataLoan.sHazInsPiaMon_rep     = GetString("sHazInsPiaMon");
            dataLoan.sHazInsRsrvMon_rep    = GetString("sHazInsRsrvMon");
            dataLoan.sIPiaDy_rep           = GetString("sIPiaDy");
            dataLoan.sConsummationD_rep    = GetString("sConsummationD");
            dataLoan.sInspectF_rep         = GetString("sInspectF");
            dataLoan.sLDiscntFMb_rep       = GetString("sLDiscntFMb");
            dataLoan.sLDiscntPc_rep        = GetString("sLDiscntPc");
            dataLoan.sLOrigFMb_rep         = GetString("sLOrigFMb");
            dataLoan.sLOrigFPc_rep         = GetString("sLOrigFPc");
            dataLoan.sMBrokFMb_rep         = GetString("sMBrokFMb");
            dataLoan.sMBrokFPc_rep         = GetString("sMBrokFPc");
            dataLoan.sMInsRsrvMon_rep      = GetString("sMInsRsrvMon");
            dataLoan.sNotaryF_rep          = GetString("sNotaryF");
            dataLoan.sNoteIR_rep           = GetString("sNoteIR");
            dataLoan.sPestInspectF_rep     = GetString("sPestInspectF");
            dataLoan.sProFloodIns_rep      = GetString("sProFloodIns");
            if (!dataLoan.sHazardExpenseInDisbursementMode)
            {
                dataLoan.sProHazInsMb_rep = GetString("sProHazInsMb");
                dataLoan.sProHazInsR_rep = GetString("sProHazInsR");
                dataLoan.sProHazInsT = (E_PercentBaseT)GetInt("sProHazInsT");
            }
            if (!dataLoan.sRealEstateTaxExpenseInDisbMode)
            {
                dataLoan.sProRealETxMb_rep = GetString("sProRealETxMb");
                dataLoan.sProRealETxR_rep = GetString("sProRealETxR");
                dataLoan.sProRealETxT = (E_PercentBaseT)GetInt("sProRealETxT");
            }
            dataLoan.sProSchoolTx_rep      = GetString("sProSchoolTx");
            dataLoan.sProcFPaid            = GetBool("sProcFPaid");
            dataLoan.sProcF_rep            = GetString("sProcF");
            dataLoan.sRealETxRsrvMon_rep   = GetString("sRealETxRsrvMon");
            dataLoan.sRecBaseT = (E_PercentBaseT)GetInt("sRecBaseT");
            dataLoan.sRecFDesc             = GetString("sRecFDesc");
            dataLoan.sRecFMb_rep           = GetString("sRecFMb");
            dataLoan.sRecFPc_rep           = GetString("sRecFPc");
            dataLoan.sSchedDueD1Lckd       = GetBool("sSchedDueD1Lckd");
            dataLoan.sSchedDueD1_rep       = GetString("sSchedDueD1");
            dataLoan.sSchoolTxRsrvMon_rep  = GetString("sSchoolTxRsrvMon");
            dataLoan.sStateRtcBaseT = (E_PercentBaseT)GetInt("sStateRtcBaseT");
            dataLoan.sStateRtcMb_rep       = GetString("sStateRtcMb");
            dataLoan.sStateRtcPc_rep       = GetString("sStateRtcPc");
            dataLoan.sTerm_rep             = GetString("sTerm");
            dataLoan.sTitleInsFTable       = GetString("sTitleInsFTable");
            dataLoan.sTitleInsF_rep        = GetString("sTitleInsF");
            dataLoan.sTxServF_rep          = GetString("sTxServF");
            dataLoan.sU1GovRtcBaseT = (E_PercentBaseT)GetInt("sU1GovRtcBaseT");
            dataLoan.sU1GovRtcMb_rep       = GetString("sU1GovRtcMb");
            dataLoan.sU1GovRtcPc_rep       = GetString("sU1GovRtcPc");
            dataLoan.sU1Sc_rep             = GetString("sU1Sc");
            dataLoan.sU1Tc_rep             = GetString("sU1Tc");
            dataLoan.sU2GovRtcBaseT = (E_PercentBaseT)GetInt("sU2GovRtcBaseT");
            dataLoan.sU2GovRtcMb_rep       = GetString("sU2GovRtcMb");
            dataLoan.sU2GovRtcPc_rep       = GetString("sU2GovRtcPc");
            dataLoan.sU2Sc_rep             = GetString("sU2Sc");
            dataLoan.sU2Tc_rep             = GetString("sU2Tc");
            dataLoan.sU3GovRtcBaseT = (E_PercentBaseT)GetInt("sU3GovRtcBaseT");
            dataLoan.sU3GovRtcMb_rep       = GetString("sU3GovRtcMb");
            dataLoan.sU3GovRtcPc_rep       = GetString("sU3GovRtcPc");
            dataLoan.sU3Sc_rep             = GetString("sU3Sc");
            dataLoan.sU3Tc_rep             = GetString("sU3Tc");
            dataLoan.sU4Sc_rep             = GetString("sU4Sc");
            dataLoan.sU4Tc_rep             = GetString("sU4Tc");
            dataLoan.sU5Sc_rep             = GetString("sU5Sc");
            dataLoan.sUwF_rep              = GetString("sUwF");
            dataLoan.sWireF_rep            = GetString("sWireF");
            dataLoan.sBrokComp1Pc_rep      = GetString("sBrokComp1Pc");
            dataLoan.sBrokComp1MldsLckd    = GetBool("sBrokComp1MldsLckd");
            dataLoan.sBrokComp1Mlds_rep    = GetString("sBrokComp1Mlds");
            dataLoan.sBrokComp2Mlds_rep = GetString("sBrokComp2Mlds");
            dataLoan.sBrokComp2MldsLckd = GetBool("sBrokComp2MldsLckd");

            dataLoan.s1006RsrvProps         = RetrieveItemProps( dataLoan.m_convertLos,  "s1006RsrvProps" );
            dataLoan.s1007RsrvProps         = RetrieveItemProps( dataLoan.m_convertLos,  "s1007RsrvProps" );
            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                dataLoan.sU3RsrvProps       = RetrieveItemProps(dataLoan.m_convertLos, "sU3RsrvProps");
                dataLoan.sU4RsrvProps       = RetrieveItemProps(dataLoan.m_convertLos, "sU4RsrvProps");
            }
            dataLoan.s800U1FProps           = RetrieveItemProps( dataLoan.m_convertLos,  "s800U1FProps" );
            dataLoan.s800U2FProps           = RetrieveItemProps( dataLoan.m_convertLos,  "s800U2FProps" );
            dataLoan.s800U3FProps           = RetrieveItemProps( dataLoan.m_convertLos,  "s800U3FProps" );	
            dataLoan.s800U4FProps           = RetrieveItemProps( dataLoan.m_convertLos,  "s800U4FProps" );
            dataLoan.s800U5FProps           = RetrieveItemProps( dataLoan.m_convertLos,  "s800U5FProps" );
            dataLoan.s900U1PiaProps         = RetrieveItemProps( dataLoan.m_convertLos,  "s900U1PiaProps" );
            dataLoan.s904PiaProps           = RetrieveItemProps( dataLoan.m_convertLos,  "s904PiaProps" );
            dataLoan.sAggregateAdjRsrvProps = RetrieveItemProps( dataLoan.m_convertLos,  "sAggregateAdjRsrvProps" );
            dataLoan.sApprFProps            = RetrieveItemProps( dataLoan.m_convertLos,  "sApprFProps" );
            dataLoan.sAttorneyFProps        = RetrieveItemProps( dataLoan.m_convertLos,  "sAttorneyFProps" );
            dataLoan.sCountyRtcProps        = RetrieveItemProps( dataLoan.m_convertLos,  "sCountyRtcProps" );
            dataLoan.sCrFProps              = RetrieveItemProps( dataLoan.m_convertLos,  "sCrFProps" );
            dataLoan.sDocPrepFProps         = RetrieveItemProps( dataLoan.m_convertLos,  "sDocPrepFProps" );
            dataLoan.sEscrowFProps          = RetrieveItemProps( dataLoan.m_convertLos,  "sEscrowFProps" );
            dataLoan.sFloodInsRsrvProps     = RetrieveItemProps( dataLoan.m_convertLos,  "sFloodInsRsrvProps" );
            dataLoan.sHazInsPiaProps        = RetrieveItemProps( dataLoan.m_convertLos,  "sHazInsPiaProps" );
            dataLoan.sHazInsRsrvProps       = RetrieveItemProps( dataLoan.m_convertLos,  "sHazInsRsrvProps" );
            dataLoan.sIPiaProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sIPiaProps" );
            dataLoan.sInspectFProps         = RetrieveItemProps( dataLoan.m_convertLos,  "sInspectFProps" );
            dataLoan.sLDiscntProps          = RetrieveItemProps( dataLoan.m_convertLos,  "sLDiscntProps" );
            dataLoan.sLOrigFProps           = RetrieveItemProps( dataLoan.m_convertLos,  "sLOrigFProps" );
            dataLoan.sMBrokFProps           = RetrieveItemProps( dataLoan.m_convertLos,  "sMBrokFProps" );
            dataLoan.sMInsRsrvProps         = RetrieveItemProps( dataLoan.m_convertLos,  "sMInsRsrvProps" );
            dataLoan.sMipPiaProps           = RetrieveItemProps( dataLoan.m_convertLos,  "sMipPiaProps" );
            dataLoan.sNotaryFProps          = RetrieveItemProps( dataLoan.m_convertLos,  "sNotaryFProps" );
            dataLoan.sPestInspectFProps     = RetrieveItemProps( dataLoan.m_convertLos,  "sPestInspectFProps" );
            dataLoan.sProcFProps            = RetrieveItemProps( dataLoan.m_convertLos,  "sProcFProps" );
            dataLoan.sRealETxRsrvProps      = RetrieveItemProps( dataLoan.m_convertLos,  "sRealETxRsrvProps" );
            dataLoan.sRecFProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sRecFProps" );
            dataLoan.sSchoolTxRsrvProps     = RetrieveItemProps( dataLoan.m_convertLos,  "sSchoolTxRsrvProps" );
            dataLoan.sStateRtcProps         = RetrieveItemProps( dataLoan.m_convertLos,  "sStateRtcProps" );
            dataLoan.sTitleInsFProps        = RetrieveItemProps( dataLoan.m_convertLos,  "sTitleInsFProps" );
            dataLoan.sTxServFProps          = RetrieveItemProps( dataLoan.m_convertLos,  "sTxServFProps" );
            dataLoan.sU1GovRtcProps         = RetrieveItemProps( dataLoan.m_convertLos,  "sU1GovRtcProps" );
            dataLoan.sU1ScProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU1ScProps" );
            dataLoan.sU1TcProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU1TcProps" );
            dataLoan.sU2GovRtcProps         = RetrieveItemProps( dataLoan.m_convertLos,  "sU2GovRtcProps" );
            dataLoan.sU2ScProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU2ScProps" );
            dataLoan.sU2TcProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU2TcProps" );
            dataLoan.sU3GovRtcProps         = RetrieveItemProps( dataLoan.m_convertLos,  "sU3GovRtcProps" );
            dataLoan.sU3ScProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU3ScProps" );
            dataLoan.sU3TcProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU3TcProps" );
            dataLoan.sU4ScProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU4ScProps" );
            dataLoan.sU4TcProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU4TcProps" );
            dataLoan.sU5ScProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU5ScProps" );
            dataLoan.sUwFProps              = RetrieveItemProps( dataLoan.m_convertLos,  "sUwFProps" );
            dataLoan.sVaFfProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sVaFfProps" );
            dataLoan.sWireFProps            = RetrieveItemProps( dataLoan.m_convertLos,  "sWireFProps" );

            dataLoan.sIPiaDyLckd = GetBool("sIPiaDyLckd");
            dataLoan.sConsummationDLckd = GetBool("sConsummationDLckd");

            E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT = (E_sOriginatorCompensationPaymentSourceT)GetInt("sOriginatorCompensationPaymentSourceT");

            string sGfeOriginatorCompFPc_rep = GetString("sGfeOriginatorCompFPc");
            string sGfeOriginatorCompFMb_rep = GetString("sGfeOriginatorCompFMb");
            E_PercentBaseT sGfeOriginatorCompFBaseT = (E_PercentBaseT)GetInt("sGfeOriginatorCompFBaseT");

            dataLoan.SetOriginatorCompensation(sOriginatorCompensationPaymentSourceT, sGfeOriginatorCompFPc_rep, sGfeOriginatorCompFBaseT, sGfeOriginatorCompFMb_rep);


        }
        #endregion

        private int RetrieveItemProps( LosConvert convert, string name)
        {
            bool apr      = GetBool(name + "_ctrl_Apr_chk");
            bool toBr     = GetBool(name + "_ctrl_ToBrok_chk");
            int payer     = GetInt(name + "_ctrl_PdByT_dd");
			bool fhaAllow = GetBool(name + "_ctrl_Fha_chk");
            bool poc      = GetBool(name + "_ctrl_Poc_chk");
            return LosConvert.GfeItemProps_Pack(apr, toBr, payer, fhaAllow, poc );
        }

        private void InitItemProps( LosConvert convert, string name, int props )
        {
            SetResult(name + "_ctrl_Apr_chk", LosConvert.GfeItemProps_Apr(props));
            SetResult(name + "_ctrl_ToBrok_chk", LosConvert.GfeItemProps_ToBr(props));
            SetResult(name + "_ctrl_PdByT_dd", LosConvert.GfeItemProps_Payer(props));
			SetResult(name + "_ctrl_Fha_chk", LosConvert.GfeItemProps_FhaAllow(props));
            SetResult(name + "_ctrl_Poc_chk", LosConvert.GfeItemProps_Poc(props));

        }
	}
}
