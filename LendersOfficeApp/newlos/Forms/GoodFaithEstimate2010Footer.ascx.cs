﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class GoodFaithEstimate2010Footer : BaseLoanUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(GoodFaithEstimate2010Footer));
            dataLoan.InitLoad();

            sGfeTotalFundByLender.Text = dataLoan.sGfeTotalFundByLender_rep;
            sGfeTotalFundBySeller.Text = dataLoan.sGfeTotalFundBySeller_rep;
        }
    }
}