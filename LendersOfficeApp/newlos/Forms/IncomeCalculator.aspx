<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="IncomeCalculator.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.IncomeCalculator" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Base Income Calculator</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
		
		<style type="text/css">
		
		table td
		{
		    border:1;
		}
		</style>
	</HEAD>
	<body bgColor="gainsboro" scroll="yes" MS_POSITIONING="FlowLayout">
		<script language="javascript">
<!--
		    var typeBorr = 0;
		    var typeCoBorr = 0;
		    
function _init() {
  resizeForIE6And7(620, 340);
  var args = getModalArgs() || {}
  document.getElementById('aBBaseI').value = args.aBBaseI;
  document.getElementById('aCBaseI').value = args.aCBaseI;
  document.getElementById('lblBName').innerText = args.aBName;
  document.getElementById('lblCName').innerText = args.aCName;
  document.getElementById('lblPageHeader').innerText = args.aDescription;  
  
  document.getElementById('bIncome').focus();
  document.getElementById('aBBaseI').readOnly = true;
  document.getElementById('aCBaseI').readOnly = true;
  document.getElementById('bHoursPerWeek').readOnly = true;
  document.getElementById('cbHoursPerWeek').readOnly = true;
  document.getElementById('txtBPeriodPYear').readOnly = true;
  document.getElementById('txtCPeriodPYear').readOnly = true;
  bIsDirty = false;
  cbIsDirty = false;  
}

function f_enRadioGroup(borrower,type) {    
    if (borrower == 'b')
    {
      if (type == 0)
        document.getElementById('bHoursPerWeek').readOnly = false;
      else if(type > 0)
        document.getElementById('bHoursPerWeek').readOnly = true;
    }
    else if (borrower == 'cb')
    {
      if (type == 0)
          document.getElementById('cbHoursPerWeek').readOnly = false;
      else if (type > 0)
          document.getElementById('cbHoursPerWeek').readOnly = true;
    }
}

function f_calcIncome(borrower, type)
{
var bor = document.getElementById('bIncome');
var cbor = document.getElementById('cbIncome');

f_enRadioGroup(borrower,type);

var result = 0;
var income = 0;
var hoursweek = 0;
var periodsinyr = 0;

if (borrower == 'b') {
    type = (type == -1) ? typeBorr : type;
    income = bor.value;
    if(document.getElementById("chkBPeriodPYear").checked)
    {
        periodsinyr = parseInt(document.getElementById('txtBPeriodPYear').value);
    }
    typeBorr = type;
}
else {
    type = (type == -1) ? typeCoBorr : type;
    income = cbor.value;
    if (document.getElementById("chkCPeriodPYear").checked)
    {
        periodsinyr = parseInt(document.getElementById('txtCPeriodPYear').value);
    }
    typeCoBorr = type;
}

income = income.replace(/[\$()-,]/g,'');

income = parseFloat(income);
if (isNaN(income))
    income = 0;
if (isNaN(periodsinyr))
    periodsinyr = 0;

switch(type)
{
    case 0: //hourly
        hoursweek = (borrower == 'b') ? document.getElementById('bHoursPerWeek').value : document.getElementById('cbHoursPerWeek').value;
        hoursweek = parseInt(hoursweek);
        if (isNaN(hoursweek)) {
            hoursweek = 0;
        }
        if (periodsinyr <= 0)periodsinyr = 52;
       
        break;
    case 1: //weekly (52 payments/year)
        if (periodsinyr <= 0) periodsinyr = 52;        
        break;
    case 2: //bi-weekly (26 payments/year)
        if (periodsinyr <= 0) periodsinyr = 26;        
        break;
    case 3: //semi-monthly (2 payments/month)
        if (periodsinyr <= 0) periodsinyr = 24;        
        break;
    case 4: //monthly
        if (periodsinyr <= 0) periodsinyr = 12;        
        break;
    case 5: //yearly
        if (periodsinyr <= 0) periodsinyr = 1;
        break;
}

result = income * periodsinyr / 12;

if (type == 0)//hourly
    result = result * hoursweek;

if (borrower == 'b') {
    bIsDirty = true;
}
else {
    cbIsDirty = true;
}

if (borrower == 'b') {
    document.getElementById("txtBPeriodPYear").value = (periodsinyr == 0) ? '' : periodsinyr;
}
else {
    document.getElementById("txtCPeriodPYear").value = (periodsinyr == 0) ? '' : periodsinyr;
}

var ref = (borrower == 'b') ? document.getElementById('aBBaseI') : document.getElementById('aCBaseI');
ref.value = result;
format_money(ref);
}

function enablePayPeriod(borrower, enable) {
    if (borrower == 'b') {
        document.getElementById("txtBPeriodPYear").readOnly = !enable;
    }
    if (borrower == 'cb') {
        document.getElementById("txtCPeriodPYear").readOnly = !enable;
    }
}

function refreshCalculation(type)
{
var radioGroup = null;

if (type == 'b')
  radioGroup = document.IncomeCalculator.bRadioGroup;
else
  radioGroup = document.IncomeCalculator.cbRadioGroup;

for (i = 0; i < radioGroup.length; i++)
{
  if (radioGroup[i].checked)
  {
   f_calcIncome(type,i);
   break;
  }
}

}

function f_close(bOk)
{
  var arg = window.dialogArguments || {};
  arg.OK = bOk;
  if (bOk)
  {
  if (bIsDirty)
    arg.aBBaseI = document.getElementById('aBBaseI').value;
  if (cbIsDirty)  
    arg.aCBaseI = document.getElementById('aCBaseI').value;
  }
  onClosePopup(arg);
}
//-->
		</script>
        <h4 class="page-header" id="lblPageHeader"></h4>
		<form id="IncomeCalculator" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" border="0">
				<tr>
			        <td>
			            <table cellSpacing="5" cellPadding="0" style="margin:0.5em;padding-right:1em;border-right:3px thin groove">
						    <tr>
					            <td class="FormTableSubheader" colspan="2">Borrower:&nbsp;&nbsp;&nbsp;&nbsp;<label style="text-align:right" id="lblBName"></label></td>
					        </tr>
							<tr>
								<td class="FieldLabel">Amount:
								</td>
								<td class="FieldLabel"><ml:moneytextbox id="bIncome" tabIndex="0" preset="money" width="90" runat="server" onchange="refreshCalculation('b');" ></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel">Pay Period:
								</td>
								<td class="FieldLabel"><input type="radio" name="bRadioGroup" onclick="f_calcIncome('b',0);" tabindex=1>Hourly
									<input id="bHoursPerWeek" type="text" size="3" onchange="refreshCalculation('b');" tabindex=2>hrs/week</td>
							</tr>
							<tr>
								<td>
								<td class="FieldLabel"><input type="radio" name="bRadioGroup" onclick="f_calcIncome('b',1);" tabindex=3>Weekly</td>
							</tr>
							<tr>
								<td>
								<td class="FieldLabel"><input type="radio" name="bRadioGroup" onclick="f_calcIncome('b',2);" tabindex=4>Bi-Weekly</td>
							</tr>
							<tr>
								<td>
								<td class="FieldLabel"><input type="radio" name="bRadioGroup" onclick="f_calcIncome('b',3);" tabindex=5>Semi-Monthly</td>
							</tr>
							<tr>
								<td>
								<td class="FieldLabel"><input type="radio" name="bRadioGroup" onclick="f_calcIncome('b',4);" tabindex=6>Monthly</td>
							</tr>
							<tr>
								<td>
								<td class="FieldLabel"><input type="radio" name="bRadioGroup" onclick="f_calcIncome('b',5);" tabindex=7>Yearly</td>
							</tr>
							<tr>
							    <td class="FieldLabel">Periods/Year:</td>
							    <td>
							        <input type="checkbox" id="chkBPeriodPYear" onclick="enablePayPeriod('b',this.checked);f_calcIncome('b', -1);" tabindex="8"/>
							        <input type="text" id="txtBPeriodPYear" onblur="f_calcIncome('b', -1);" size="3" tabindex="9"/>
							    </td>
							</tr>
							<tr>
								<td class="FieldLabel">
								Monthly Income:
								<td><ml:moneytextbox id="aBBaseI" tabIndex="1" preset="money" width="90" runat="server"></ml:moneytextbox></td>
							</tr>
						</table>
			        </td>
			        <td>
			            <table id="Table1" cellSpacing="5" cellPadding="0" style="margin:0.5em">
			                <tr>	
					            <td class="FormTableSubheader" colspan="2">Co-borrower:&nbsp;&nbsp;&nbsp;&nbsp;<label style="text-align:right" id="lblCName"></label></td>
				            </tr>
							<tr>
								<td class="FieldLabel">Amount:
								</td>
								<td class="FieldLabel"><ml:moneytextbox id="cbIncome" tabIndex="8" preset="money" width="90" runat="server" onchange="refreshCalculation('cb');"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel">Pay Period:
								</td>
								<td class="FieldLabel"><input type="radio" name="cbRadioGroup" onclick="f_calcIncome('cb',0);" tabindex=9>Hourly
									<input id="cbHoursPerWeek" type="text" size="3" onchange="refreshCalculation('cb');" tabindex=10>hrs/week</td>
							</tr>
							<tr>
								<td>
								<td class="FieldLabel"><input type="radio" name="cbRadioGroup" onclick="f_calcIncome('cb',1);" tabindex=11>Weekly</td>
							</tr>
							<tr>
								<td>
								<td class="FieldLabel"><input type="radio" name="cbRadioGroup" onclick="f_calcIncome('cb',2);" tabindex=12>Bi-Weekly</td>
							</tr>
							<tr>
								<td>
								<td class="FieldLabel"><input type="radio" name="cbRadioGroup" onclick="f_calcIncome('cb',3);" tabindex=13>Semi-Monthly</td>
							</tr>
							<tr>
								<td>
								<td class="FieldLabel"><input type="radio" name="cbRadioGroup" onclick="f_calcIncome('cb',4);" tabindex=14>Monthly</td>
							</tr>
							<tr>
								<td>
								<td class="FieldLabel"><input type="radio" name="cbRadioGroup" onclick="f_calcIncome('cb',5);" tabindex=15>Yearly</td>
							</tr>
							<tr>
							    <td class="FieldLabel">Periods/Year:</td>
							    <td>
							        <input type="checkbox" id="chkCPeriodPYear" onclick="enablePayPeriod('cb',this.checked);f_calcIncome('cb', -1);" tabindex="16"/>
							        <input type="text" id="txtCPeriodPYear" onblur="f_calcIncome('cb', -1);" size="3" tabindex="17"/>
							    </td>
							</tr>
							<tr>
								<td class="FieldLabel">
								Monthly Income:
								<td><ml:moneytextbox id="aCBaseI" tabIndex="1" preset="money" width="90" runat="server"></ml:moneytextbox></td>
							</tr>
						</table>
			        </td>
				</tr>
				<tr>
					<td align="center" colspan="2"><input id="bOk" style="WIDTH: 75px" type="button" value="OK" name="Button1" NoHighlight onclick="f_close(true)" tabindex=16>
					<input id="bCancel" style="WIDTH: 75px" type="button" value="Cancel" name="Button2" NoHighlight onclick="f_close(false)" tabindex=17></td>
				</tr>
			</table>
		</form>
		<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
	</body>
</HTML>
