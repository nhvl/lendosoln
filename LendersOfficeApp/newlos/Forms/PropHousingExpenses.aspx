﻿<%@ Page MaintainScrollPositionOnPostback="true" Language="C#" AutoEventWireup="false" CodeBehind="PropHousingExpenses.aspx.cs" Inherits="LendersOfficeApp.newlos.Forms.PropHousingExpenses" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Migration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Housing Expenses</title>
    <style type="text/css">
        body 
        { 
            background-color: gainsboro; 
        }
               
        .TotalMonthAmt
        {
            color: black;
        } 

        .FormTableHeader td
        {
            padding-left: 2px;
        }        
        
        #DetailsTabContent .FormTableHeader 
        {
            cursor: pointer;
        }
        
        .FormTableSubheader td
        {
            padding-left: 2px;
        }
        
        .FormTableSubSubHeader
        {
            color: white;
            font-weight: bold;
            font-family: Arial, Helvetica, Sans-Serif;
            font-size: 11px;
            background-color: rgb(8, 83, 147);    
        }
        
        .AddDispBtn
        {
            padding: 0px 1px;
            color: Black;
            font-family: verdana, Arial, Sans-Serif;
            font-size: 13px;
            text-decoration: none;
            border: solid 1px black;
        }
        
        .AddDispBtn:link
        {
            color: Black;
        }
        
        .AddDispBtn:hover
        {
            text-decoration: none;
            color: Black;
            cursor: pointer;
        }
        
        .enabledRem
        {
            cursor: pointer;
        }
        
        .ExpenseTable
        {
            margin: 1px 0px;
        }
        
        .warning
        {
            margin: 4px;
            width: 90%;
            background-color: Yellow;
            border: solid 1px black;
            color: Black;
            font-family: Arial, Helvetica, Sans-Serif;
            padding: 4px;
            font-size: 12px;
            font-weight: bold;
        }
        
        input.month
        {
            width: 30px;
        }
        input.money[preset='money']
        {
            width: 90px;
            text-align: right;
        }
        input.percent[preset='percent']
        {
            width: 70px;
        }
        input.date[preset='date']
        {
            width: 75px;
        }

        .EscrowDataDiv
        {
            border:solid 1px black;
            display:inline-block;
        }
            
        .EscrowDataDiv span
        {
            padding:5px;
            vertical-align:top;
            display:inline-block;
            min-width:80px;
        }

        .EscrowRadioDiv
        {
            display:inline-block; 
            vertical-align:top;
        }
            
        .EscrowDataDiv span.EscrowDescriptionSpan
        {
            min-width:250px;
        }

        .EscrowDataDiv div.EscrowDescriptionDiv
        {
            min-width: 250px;
            margin-top: 5px;
            margin-bottom: 5px;
        }

        #EscrowDescriptionContainer, #NonEscrowDescriptionContainer
        {
            font-style:italic;
        }
        .display-table {
            display: table;
        }
    </style>
    <script type="ractive" id="DatePicker">
        <input readonly="{{readonly}}" type="text" class="mask date" preset="date" id="{{id}}" intro="initMask"
            on-blur="updatePicker" value="{{value}}" />
        <a href="#" on-click="showCalendar:{{id}},{{doRecalc}}">
            <img title="Open calendar" border="0" src="[[VRoot]]/images/pdate.gif" />
        </a>
    </script>
</head>
<body>
    <script type="text/javascript">
        var globalRactive;
        var calendarRecalc = false;

        jQuery(function($)
        {
            var initMask = function(o)
            {
                _initDynamicInput(o.node);
                o.complete();
            };

            Ractive.transitions.initMask = initMask;
            var DatePicker = Ractive.extend({
                template: '#DatePicker',
                data: {
                    value: '',
                    id: '',
                    readonly: false,
                    doRecalc: false
                },
                init: function(){
                    var self = this;
            
                    this.on('showCalendar', function(e,id, doRecalc){
                        if (doRecalc)
                        {
                            if (doRecalc === "true")
                            {
                                calendarRecalc = true;
                            }
                            else
                            {
                                calendarRecalc = false;
                            }
                        }

                        return displayCalendar(id, function(cal) { 
                            if (cal && cal.sel) {
                                queueRactiveUpdate(globalRactive, self, cal.sel.value);
                                cal.hide();
                            }
                        });
                    });
                    this.on('domblur', function(event){
                        var kp = event.keypath;
                        //sadly this blur event fires before the mask.js blur event 
                        //by using setTimeout we queue up the function until after all the other
                        //js code.
                        window.setTimeout(function(){
                            self.updateModel("value", true);
                        });
                    });
                 
                    this.on('updatePicker', function(event)
                    {
                        window.setTimeout(function(){
                            queueRactiveUpdate(globalRactive, self, event.node.value);
                        }, 0);
                    });
                }   
            });

            globalRactive = new Ractive({
                append: true,
                template: '#FullPageTemplate',
                el: '#container',
                data: PageViewModel,
                partials: '#DetailsRowTemplate,#DisbursementsTemplate,#AmtsRowTemplate,#PrepRowTemplate,#EscImpRowTemplate',
                components: {datePicker: DatePicker},
                init: function()
                {
                    var self = this;

                    this.on('domblur', function(event)
                    {
                        var kp = event.keypath;
                        window.setTimeout(function()
                        {
                            self.updateModel();
                        });
                    });

                    this.on('recalc', function(event)
                    {
                        var data =
                        {
                            loanid: document.getElementById("loanid").value,
                            applicationid: document.getElementById("applicationid").value,
                            PageViewModel: JSON.stringify(PageViewModel)
                        }

                        var results = gService.loanedit.call("Calculate", data);
                        if (!results.error)
                        {
                            var newModel = results.value["PageViewModel"];
                            self.set(JSON.parse(newModel));
                        }
                        else
                        {
                            alert(results.UserMessage);
                        }
                    });

                    this.on('removeDisb', function(event, category, expenseIndex, disbIndex)
                    {
                        var model = self.get(category);
                        var expense = model[expenseIndex];
                        var disb = expense.Disbursements[disbIndex];

                        expense.removedDisbs += (disb.DisbId + ",");
                        expense.Disbursements.splice(disbIndex, 1);

                        self.fire('recalc');

                        updateDirtyBit();
                    });

                    this.on('addDisb', function(event, category, expenseIndex)
                    {
                        var model = self.get(category);

                        var newdisb = new Object();
                        newdisb.DisbT = 1;
                        newdisb.PaidDT = 0;
                        newdisb.DueD = "";
                        newdisb.DueAmt = "";
                        newdisb.DisbMon = "";
                        newdisb.DisbPaidD = "";
                        newdisb.PaySource = 0;
                        newdisb.PaidFromD = "";
                        newdisb.PaidToD = "";
                        newdisb.DisbId = "00000000-0000-0000-0000-000000000000";
                        newdisb.IsPaidFromEscrow = false;

                        model[expenseIndex].Disbursements.push(newdisb);
                        self.fire('recalc');

                        updateDirtyBit();
                    });

                    this.on('toggleEscrowByCategory', function(event, category)
                    {
                        var checked = $(event.node).is(':checked');
                        var section = self.get(category);
                        $(section).each(function()
                        {
                            if (checked)
                            {
                                $(this)[0].isEscrowed = true;
                            }
                            else
                            {
                                $(this)[0].isEscrowed = false;
                            }
                        });

                        self.fire('recalc');
                    });

                    this.on('escrowClick', function(event, category)
                    {
                        checkSectionEscrowCB(self, category);
                        self.fire('recalc');
                    });

                    this.on('checkCustomSettings', function(event)
                    {
                        var currExp = self.get(event.keypath);

                        // Don't bother checking if setting to Any or None
                        if (currExp.customLineNum == 0 || currExp.customLineNum == 5)
                        {
                            self.fire('recalc');
                            return;
                        }

                        var propExp = self.get('PropInsExps');
                        var taxExp = self.get('TaxesExps');
                        var otherExp = self.get('OtherExps');

                        $(propExp).each(function(index, val)
                        {
                            if (this !== currExp && this.IsCustom && this.customLineNum == currExp.customLineNum)
                            {
                                this.customLineNum = 5;
                            }
                        });
                        $(taxExp).each(function(index, val)
                        {
                            if (this !== currExp && this.IsCustom && this.customLineNum == currExp.customLineNum)
                            {
                                this.customLineNum = 5;
                            }
                        });
                        $(otherExp).each(function(index, val)
                        {
                            if (this !== currExp && this.IsCustom && this.customLineNum == currExp.customLineNum)
                            {
                                this.customLineNum = 5;
                            }
                        });
                        
                        self.fire('recalc');
                    });
                }
            });

            // Event Handlers
            $(document).on('change', 'input,select', updateDirtyBit);

            // Setup
            $('#DetailsTabContent .FormTableHeader').each(function()
            {
                ToggleHousingContent(this);
            });

            switchTab('AmountsTab', 'AmountsTabContent');

            InitializeSectionEscrowCB();
        });

        var oldSaveMe = window.saveMe;
        window.saveMe = function(bRefreshScreen)
        {
            var shouldEnforceDisbSched = ML.LoanVersionTCurrent >= <%= AspxTools.JsNumeric(LoanVersionT.V16_EnforceDisbursementMonthTotalExpenses) %>;
            if (shouldEnforceDisbSched)
            {
                var areDisbSchedsValidMessage = ValidateExpenseDisbursementSched();
                if (areDisbSchedsValidMessage != null)
                {
                    alert(areDisbSchedsValidMessage);
                    return;
                }
            }

            var data =
            {
                loanid: document.getElementById("loanid").value,
                applicationid: document.getElementById("applicationid").value,
                PageViewModel: JSON.stringify(PageViewModel)
            }

            var results = gService.loanedit.call("Save", data);
            if (!results.error)
            {
                var newModel = results.value["PageViewModel"];
                globalRactive.set(JSON.parse(newModel));
                clearDirty();
                return true;
            }
            else
            {
                if (results.ErrorType == 'LoanFieldWritePermissionDenied')
                {
                    f_displayFieldWriteDenied(results.UserMessage);
                    return;
                }
                else
                {
                    alert(results.UserMessage);
                }
            }
        }

        function ValidateExpenseDisbursementSched()
        {
            var expenses = PageViewModel.PropInsExps;
            expenses = expenses.concat(PageViewModel.TaxesExps);
            expenses = expenses.concat(PageViewModel.OtherExps);
            
            var errorMessage = "Please update the disbursement months for the following non P&I housing expense(s) to add up to 12:";
            var isValid = true;
            $(expenses).each(function(index, val) {
                var sum = val.DisbSched.reduce(function(a, b) {
                    var parsedA = parseInt(a);
                    if(isNaN(parsedA))
                    {
                        parsedA = 0;
                    }

                    var parsedB = parseInt(b);
                    if(isNaN(parsedB))
                    {
                        parsedB = 0;
                    }

                    return parsedA + parsedB;
                });

                if(sum !== 12)
                {
                    isValid = false;
                    errorMessage += ("\n - " + val.expDesc);
                }
            });

            if(isValid)
            {
                return null;
            }
            else
            {
                return errorMessage;
            }
        }
        
        function selectedDate(cal, date)
        {
            cal.sel.value = date;
            cal.callCloseHandler();
            if (typeof (updateDirtyBit) == 'function') updateDirtyBit();
            
            cal.sel.blur();
            if (calendarRecalc && calendarRecalc == true)
            {
                globalRactive.fire('recalc');
                calendarRecalc = false; 
            }
        }

        function HousingHeaderClick(row)
        {
            $('#DetailsTabContent .FormTableHeader').not(row).each(function()
            {
                // Check if the content is visible is displayed. If so, toggle it so it hides.
                if ($(this).siblings('tr').first().css('display') != 'none')
                {
                    ToggleHousingContent(this)         
                }
            });
            
            ToggleHousingContent(row);
        }

        function ToggleHousingContent(row)
        {
            if ($(row).siblings('tr').first().css('display') != 'none')
            {
                $(row).siblings('tr').hide();
                $(row).children('.Arrow').html("&#x25ba;");
            }
            else
            {
                $(row).siblings('tr').show();
                $(row).children('.Arrow').html("&#x25bc;");
            }
        }

        function switchTab(tab, contentId)
        {
            $('#ContentTabs').children('div').each(function()
            {
                if ($(this).attr('id') != contentId)
                {
                    $(this).hide();
                }
            });

            $("#" + contentId).show();

            $('#Tabs').children('li').removeClass('selected');
            $('#' + tab).addClass('selected');
        }

        function ToDetailsTab(headerId)
        {
            switchTab('DetailsTab', 'DetailsTabContent');
            if ($('#' + headerId).siblings('tr').first().css('display') == 'none')
            {
                document.getElementById(headerId).onclick();
            }
        }

        function checkSectionEscrowCB(ractive, category)
        {
            var CB = $('#' + category + 'EscrowCB');
            CB.prop('checked', true);
            var section = ractive.get(category);
            $(section).each(function()
            {
                if ($(this)[0].isEscrowed == false)
                {
                    CB.prop('checked', false);
                    return false;
                }
            });
        }
        
        function InitializeSectionEscrowCB()
        {
            checkSectionEscrowCB(globalRactive, 'PropInsExps');
            checkSectionEscrowCB(globalRactive, 'TaxesExps');
            checkSectionEscrowCB(globalRactive, 'OtherExps');
        }

        function checkCustomSettings(el)
        {
            $("[id$='customLineNum_esc']").each(function()
            {
                var value = $(this).val();
                if ($(this).attr('id') != el.id && value == el.valueOf && el.valueOf != 0)
                {
                    $(this).val(5);
                }
            });
        }
    </script>
    
    <script id="DetailsRowTemplate" type="text/ractive">
        <tr>
            <td width="1%">&nbsp;</td>
            <td>
                <div class="ExpenseTable" id="{{expPrefix}}_Expenses">
                    <table width="98%" border="0" cellspacing="0" cellpadding="0">
                        <tr onclick="HousingHeaderClick(this);" class="FormTableHeader" id={{expPrefix}}_Header>
                            <td width="2%" class="Arrow">
                                &#x25bc;
                            </td>
                            <td width="49%" align="left">
                                {{#if expDesc === "" || expName === expDesc}}
                                    {{expName}}
                                {{else}}
                                    {{expDesc}} - {{expName}}
                                {{/if}}
                            </td>
                            <td width="49%" align="left">
                                {{monTotPITI}}/month
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td id="{{expPrefix}}_Summary">
                                <table width="100%">
                                    <tr>
                                        <td class="FormTableSubheader" colspan="3">
                                            Summary
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Description
                                        </td>
                                        <td colspan="2">
                                            <input on-blur="domblur" style="width: 180px;" type="text" id="{{expPrefix}}Desc" value="{{expDesc}}" />
                                        </td>
                                    </tr>
                                    {{#if IsTax}}
                                    <tr>
                                        <td>
                                            Tax Type
                                        </td>
                                        <td colspan="2">
                                            <asp:Repeater runat="server" ID="taxTypeRepeater">
                                                <HeaderTemplate>
                                                    <select on-blur="domblur" id="{{expPrefix}}taxT" value="{{taxT}}">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                        <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </select>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                    {{/if}}
                                    <tr>
                                        <td>
                                            Calculation Source
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Repeater runat="server" ID="annAmtCalcTRepeater">
                                                <HeaderTemplate>
                                                    <select on-change="recalc" id="{{expPrefix}}annAmtCalcT" value="{{annAmtCalcT}}">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                        <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </select>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Annual Amount
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <input readonly="true" value="{{annAmtTot}}" type="text" class="mask money" preset="money" id="{{expPrefix}}annAmtTot"
                                                intro="initMask"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Monthly amount (PITI)
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <input readonly="true" value="{{monTotPITI}}" type="text" class="mask money" preset="money" id="{{expPrefix}}monTotPITI"
                                                intro="initMask"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Prepaid
                                            <input on-change="recalc" checked="{{isPrepaid}}" type="checkbox" id="{{expPrefix}}isPrepaid" intro="initMask" on-blur="domblur" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Prepaid Amount
                                        </td>
                                        <td>
                                            for
                                            <input readonly="true" type="text" value="{{prepMnth}}" class="mask month" id="{{expPrefix}}prepMnth" intro="initMask" />
                                            mths
                                        </td>
                                        <td>
                                            <input readonly="true" value="{{prepAmt}}" type="text" class="mask money" preset="money" id="{{expPrefix}}prepAmt" intro="initMask" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Escrowed
                                            <input on-change="escrowClick:{{Category}}" checked="{{isEscrowed}}" type="checkbox" id="{{expPrefix}}isEscrowed" intro="initMask" on-blur="domblur" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Monthly Amount (Servicing)
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <input readonly="true" value="{{monTotServ}}" type="text" class="mask money" preset="money" id="{{expPrefix}}monTotServ"
                                                intro="initMask"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Reserves amount
                                        </td>
                                        <td>
                                            for
                                            <input readonly="true" type="text" value="{{rsrvMon}}" class="mask month" id="{{expPrefix}}rsrvMon" intro="initMask"/>
                                            mnths
                                        </td>
                                        <td>
                                            <input readonly="true" value="{{rsrvAmt}}" type="text" class="mask money" preset="money" id="{{expPrefix}}rsrvAmt" intro="initMask"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" id="{{ExpenseName}}_Calculator">
                                <table width="100%">
                                    <tr>
                                        <td colspan="5" class="FormTableSubheader">
                                            Calculator
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Monthly Amount (PITI)
                                        </td>
                                        <td colspan="3">
                                            <input on-change="recalc" readonly="{{annAmtCalcT == 1}}" value="{{annAmtPerc}}" type="text" class="mask percent" preset="percent"
                                                id="{{expPrefix}}annAmtPerc" intro="initMask" on-blur="domblur" />
                                            of
                                            <asp:Repeater runat="server" ID="annAmtBaseRepeater">
                                                <HeaderTemplate>
                                                    <select on-change="recalc" id="{{expPrefix}}annAmtCalcT" disabled="{{annAmtCalcT == 1}}" value="{{annAmtBase}}" on-blur="domblur">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                        <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </select>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            +
                                            <input on-change="recalc" readonly="{{annAmtCalcT == 1}}" value="{{monAmtFixed}}" type="text" class="mask money" preset="money"
                                                id="{{expPrefix}}monAmtFixed" intro="initMask" on-blur="domblur" {{expenseType == 0 ? "decimalDigits=4" : ""}}/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Prepaid amount
                                        </td>
                                        <td>
                                            <input readonly="true" value="{{prepAmt}}" type="text" class="mask money" preset="money" id="{{expPrefix}}prepAmtCalc" intro="initMask" />
                                            for
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <input on-change="recalc" readonly="{{!isPrepaid || annAmtCalcT == 1}}" value="{{prepMnth}}" type="text" class="mask month" id="{{expPrefix}}prepMnthCalc"
                                                intro="initMask" on-blur="domblur" />
                                            months
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Reserves Cushion
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <input on-change="recalc" value="{{Cush}}" type="text" class="mask month" id="{{expPrefix}}Cush"
                                                intro="initMask" on-blur="domblur" />
                                            months
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Reserves Amount
                                        </td>
                                        <td>
                                            <input readonly="true" value="{{rsrvAmt}}" type="text" class="mask money" preset="money" id="{{expPrefix}}rsrvAmtCalc"
                                                intro="initMask" on-blur="domblur" />
                                            for
                                        </td>
                                        <td>
                                            <input on-change="recalc" checked="{{rsrvMonLckd}}" type="checkbox" id="{{expPrefix}}rsrvMonLckd" on-blur="domblur"/>
                                        </td>
                                        <td>
                                            <input on-change="recalc" readonly="{{!rsrvMonLckd}}" value="{{rsrvMon}}" type="text" class="mask month" id="{{expPrefix}}rsrvMonCalc"
                                                intro="initMask" on-blur="domblur" />
                                            months
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td colspan="2">
                                <table width="100%">
                                    <tr class="FormTableSubheader">
                                        <td>
                                            Disbursement schedule
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Payment repeat
                                            <asp:Repeater runat="server" ID="RepIntervalRepeater">
                                                <HeaderTemplate>
                                                    <select on-change="recalc" id="{{expPrefix}}RepInterval" value="{{RepInterval}}" on-blur="domblur">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                        <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </select>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        Jan
                                                    </td>
                                                    <td>
                                                        Feb
                                                    </td>
                                                    <td>
                                                        Mar
                                                    </td>
                                                    <td>
                                                        Apr
                                                    </td>
                                                    <td>
                                                        May
                                                    </td>
                                                    <td>
                                                        Jun
                                                    </td>
                                                    <td>
                                                        Jul
                                                    </td>
                                                    <td>
                                                        Aug
                                                    </td>
                                                    <td>
                                                        Sep
                                                    </td>
                                                    <td>
                                                        Oct
                                                    </td>
                                                    <td>
                                                        Nov
                                                    </td>
                                                    <td>
                                                        Dec
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Months
                                                    </td>
                                                    {{#DisbSched:i}}
                                                    <td>
                                                        <input on-change="recalc" readonly="{{annAmtCalcT == 1 || RepInterval == 1 || RepInterval == 2}}" value="{{this}}" type="text" class="mask month SchedMonth" id="{{expPrefix}}SchedMon_{{i+1}}" intro="initMask"
                                                            on-blur="domblur" />
                                                    </td>
                                                    {{/#DisbSched}}
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td colspan="2">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td>
                                            Disbursements
                                        </td>
                                    </tr>
                                    <tr class="FormTableSubSubHeader">
                                        <td>
                                        </td>
                                        <td>
                                            Due Date
                                        </td>
                                        <td>
                                            Due Amount
                                        </td>
                                        <td>
                                            Months
                                        </td>
                                        <td>
                                            Paid Date
                                        </td>
                                        <td>
                                            Payment Source
                                        </td>
                                        <td>
                                            Paid From Date
                                        </td>
                                        <td>
                                            Paid To Date
                                        </td>
                                        {{#if annAmtCalcT == 1}}
                                        <td>
                                        </td>
                                        {{/if}}
                                    </tr>
                                    {{#Disbursements:num}}
                                        {{>DisbursementsTemplate}}
                                    {{/#Disbursements}}
                                    {{#if annAmtCalcT == 1}} 
                                    <tr>
                                        <td>
                                            <a class="AddDispBtn" on-click="addDisb:{{Category}},{{i}}" id="{{expPrefix}}addDisbBtn">+</a>
                                        </td>
                                    </tr>
                                    {{/if}}
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>        
            </td>
        </tr>
    </script>
    
    <script id="DisbursementsTemplate" type="text/ractive">
        <tr>
            <td>
                <asp:Repeater runat="server" ID="PaidDTRepeater">
                    <HeaderTemplate>
                        <select noteditable="true" id="{{expPrefix}}_{{num}}PaidDT" value="{{PaidDT}}" disabled="{{annAmtCalcT == 0}}" on-blur="domblur">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                            <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                    </ItemTemplate>
                    <FooterTemplate>
                        </select>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
            <td>
            
              <datePicker readonly="{{annAmtCalcT == 0}}" value="{{DueD}}" id="{{expPrefix}}_{{num}}DueD" />
            </td>
            <td>
                <input on-change="recalc" readonly="{{annAmtCalcT == 0}}" type="text" value="{{DueAmt}}" class="mask money" preset="money" id="{{expPrefix}}_{{num}}DueAmt"
                    intro="initMask" on-blur="domblur" />
            </td>
            <td>
                <input style="width: 30px;" readonly="{{annAmtCalcT == 0}}" type="text" value="{{DisbMon}}" class="mask" id="{{expPrefix}}_{{num}}DisbMon"
                    intro="initMask" on-blur="domblur" on-change="recalc" />
            </td>
            <td>
              <datePicker readonly="{{annAmtCalcT == 0}}" value="{{DisbPaidD}}" id="{{expPrefix}}_{{num}}DisbPaidD" />
            </td>
            <td>
                <asp:Repeater runat="server" ID="PaySrcRepeater">
                    <HeaderTemplate>
                        <select on-change="recalc" id="{{expPrefix}}_{{num}}PaySource" noteditable="true" value="{{PaySource}}"
                            disabled="{{annAmtCalcT == 0}}" on-blur="domblur">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                            <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                    </ItemTemplate>
                    <FooterTemplate>
                        </select>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
            <td>
              <datePicker readonly="{{annAmtCalcT == 0}}" value="{{PaidFromD}}" id="{{expPrefix}}_{{num}}PaidFromD" />
            </td>
            <td>
              <datePicker readonly="{{annAmtCalcT == 0}}" value="{{PaidToD}}" id="{{expPrefix}}_{{num}}PaidToD" />
            </td>
            {{#if annAmtCalcT != 0}}
            <td>
                <a class="enabledRem" on-click="removeDisb:{{Category}},{{i}},{{num}}" id="{{expPrefix}}_{{num}}remBtn">Remove </a>
            </td>
            {{/if}}
        </tr>
    </script>
    
    <script id="AmtsRowTemplate" type="text/ractive">
        <tr class="AmtsRow">
            <td>&nbsp;</td>
            <td>
                <table cellspacing="0" cellpadding="0" border="0">
                    {{#if expDesc === "" || expDesc === expName}}
                        <tr>
                            <td class="FieldLabel">{{expName}}</td>
                        </tr>
                    {{else}}
                        <tr>
                            <td class="FieldLabel">{{expDesc}}</td>
                        </tr>
                        <tr>
                            <td style="padding-left: 5px;">{{expName}}</td>
                        </tr>
                    {{/if}}
                </table>
            </td>
            <td>
                {{#if IsTax}}
                <asp:Repeater runat="server" ID="taxTypeRepeater_amts">
                    <HeaderTemplate>
                        <select on-blur="domblur" id="{{expPrefix}}taxT_amts" value="{{taxT}}">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                            <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                    </ItemTemplate>
                    <FooterTemplate>
                        </select>
                    </FooterTemplate>
                </asp:Repeater>
                {{/if}}
            </td>
            <td>
                {{#if annAmtCalcT == 0}}
                    Calculator
                {{else}}
                    <a class="link" href="javascript:void(0);" onclick="ToDetailsTab('{{expPrefix}}_Header');">Disbursements</a>
                {{/if}}
            </td>
            <td>
                {{#if annAmtCalcT == 0}}
                    <input on-change="recalc" value="{{annAmtPerc}}" type="text" class="mask percent" preset="percent"
                        id="{{expPrefix}}annAmtPerc_amts" intro="initMask" on-blur="domblur" />
                    of
                    <asp:Repeater runat="server" ID="annAmtBaseRepeater_amts">
                        <HeaderTemplate>
                            <select on-change="recalc" id="{{expPrefix}}annAmtCalcT_amts" value="{{annAmtBase}}" on-blur="domblur">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                        </ItemTemplate>
                        <FooterTemplate>
                            </select>
                        </FooterTemplate>
                    </asp:Repeater>
                    /12 +
                    <input on-change="recalc" value="{{monAmtFixed}}" type="text" class="mask money" preset="money"
                        id="{{expPrefix}}monAmtFixed_amts" intro="initMask" on-blur="domblur" {{expenseType == 0 ? "decimalDigits=4" : ""}} />
                {{/if}}
            </td>
            <td>
                <input readonly="true" value="{{monTotPITI}}" type="text" class="mask money" preset="money" id="{{expPrefix}}monTotPITI_amts"
                    intro="initMask" />
            </td>
            <td>
                <input on-change="escrowClick:{{Category}}" checked="{{isEscrowed}}" type="checkbox" id="{{expPrefix}}isEscrowed_amts" intro="initMask" on-blur="domblur" />
            </td>
            <td>
                <input readonly="true" value="{{monTotServ}}" type="text" class="mask money" preset="money" id="{{expPrefix}}monTotServ_amts"
                    intro="initMask" />
            </td>
        </tr>
    </script>
    
    <script id="PrepRowTemplate" type="text/ractive">
        <tr class="PrepaidsRow">
            <td>
                &nbsp;
            </td>
            <td>
                <table cellspacing="0" cellpadding="0" border="0">
                    {{#if expDesc === "" || expDesc === expName}}
                        <tr>
                            <td class="FieldLabel">{{expName}}</td>
                        </tr>
                    {{else}}
                        <tr>
                            <td class="FieldLabel">{{expDesc}}</td>
                        </tr>
                        <tr>
                            <td style="padding-left: 5px;">{{expName}}</td>
                        </tr>
                    {{/if}}
                </table>
            </td>
            <td>
                {{#if IsTax}}
                <asp:Repeater runat="server" ID="taxTypeRepeater_prep">
                    <HeaderTemplate>
                        <select on-blur="domblur" id="{{expPrefix}}taxT_prep" value="{{taxT}}">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                            <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                    </ItemTemplate>
                    <FooterTemplate>
                        </select>
                    </FooterTemplate>
                </asp:Repeater>
                {{/if}}
            </td>
            <td>
                {{#if annAmtCalcT == 0}} 
                    Calculator 
                {{else}} 
                    <a class="link" href="javascript:void(0);" onclick="ToDetailsTab('{{expPrefix}}_Header');">
                        Disbursements
                    </a> 
                {{/if}}
            </td>
            <td>
                <input on-change="recalc" checked="{{isPrepaid}}" type="checkbox" id="{{expPrefix}}isPrepaid_prep" intro="initMask" on-blur="domblur" />
            </td>
            <td>
                <input readonly="true" value="{{monTotPITI}}" type="text" class="mask money" preset="money" id="{{expPrefix}}monTotPITI_prep"
                    intro="initMask" />
            </td>
            <td>
                <input on-change="recalc" readonly="{{!isPrepaid || annAmtCalcT == 1}}" value="{{prepMnth}}" type="text" class="mask month"
                    id="{{expPrefix}}prepMnth_prep" intro="initMask" on-blur="domblur" />
            </td>
            <td>
                <input readonly="true" value="{{prepAmt}}" type="text" class="mask money" preset="money" id="{{expPrefix}}prepAmt_prep"
                    intro="initMask" />
            </td>
        </tr>
    </script>
    
    <script id="EscImpRowTemplate" type="text/ractive">
        <tr>
            <td>&nbsp;</td>
            <td>
                <table cellspacing="0" cellpadding="0" border="0">
                    {{#if expDesc === "" || expDesc === expName}}
                        <tr>
                            <td class="FieldLabel">{{expName}}</td>
                        </tr>
                    {{else}}
                        <tr>
                            <td class="FieldLabel">{{expDesc}}</td>
                        </tr>
                        <tr>
                            <td style="padding-left: 5px;">{{expName}}</td>
                        </tr>
                    {{/if}}
                </table>
            </td>
            <td>
                {{#if IsTax}}
                <asp:Repeater runat="server" ID="taxTypeRepeater_esc">
                    <HeaderTemplate>
                        <select on-blur="domblur" id="{{expPrefix}}taxT_esc" value="{{taxT}}">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                            <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                    </ItemTemplate>
                    <FooterTemplate>
                        </select>
                    </FooterTemplate>
                </asp:Repeater>
                {{/if}}
            </td>
            <td>
                <input on-change="escrowClick:{{Category}}" checked="{{isEscrowed}}" type="checkbox" id="{{expPrefix}}isEscrowed_esc" intro="initMask" on-blur="domblur" />
            </td>
            <td>
                {{#if IsCustom}}
                <select on-change="checkCustomSettings" on-blur="domblur" id="{{expPrefix}}customLineNum_esc" value="{{customLineNum}}">
                    <option value=0 {{isEscrowed? "disabled='disabled'" : ""}}>None</option>
                    <option value=5 {{sClosingCostFeeVersionT !== 2? "disabled='disabled'" : ""}}>Any</option>
                    <option value=1>1008</option>
                    <option value=2>1009</option>
                    <option value=3>1010</option>
                    <option value=4>1011</option> 
                </select>
                {{else}}
                    {{customLineNumAsString}}
                {{/if}}
            </td>
            <td>
                <input on-change="recalc" value="{{Cush}}" type="text" class="mask month" id="{{expPrefix}}Cush_esc"
                    intro="initMask" on-blur="domblur" />
            </td>
            <td>
                {{#if annAmtCalcT == 0}} 
                    Calculator 
                {{else}} 
                    <a class="link" href="javascript:void(0);" onclick="ToDetailsTab('{{expPrefix}}_Header');">
                        Disbursements
                    </a> 
                {{/if}}
            </td>
            <td>{{firstDisbD}}</td>
            <td>
                <input readonly="true" value="{{monTotServ}}" type="text" class="mask money" preset="money" id="{{expPrefix}}monTotServ_esc"
                    intro="initMask" />
            </td>
            <td>
                <input on-change="recalc" checked="{{rsrvMonLckd}}" type="checkbox" id="{{expPrefix}}rsrvMonLckd_esc" on-blur="domblur"/>
                <input on-change="recalc" readonly="{{!rsrvMonLckd}}" value="{{rsrvMon}}" type="text" class="mask month" id="{{expPrefix}}rsrvMon_esc"
                        intro="initMask" on-blur="domblur" />
            </td>
            <td>
                <input readonly="true" value="{{rsrvAmt}}" type="text" class="mask money" preset="money" id="{{expPrefix}}rsrvAmt_esc"
                        intro="initMask" on-blur="domblur" />
            </td>
        </tr>
    </script>
    
    <script id="FullPageTemplate" type="text/ractive">
        <div class="warning" id="AggregateSection" {{sAggEscrowCalcModeT != 0 || DisbModeExpenses.length == 0? "style='display: none;'" : ""}}>
            The calculation source for housing expense(s)
            <ul>
                {{#DisbModeExpenses}}
                    <li>{{this}}</li>
                {{/DisbModeExpenses}}
            </ul>
            is set to Disbursements, but the Aggregate Escrow/Impound calculation mode is set to Legacy. The Legacy calculation
            mode may not properly reflect updates to the disbursements. Please switch to the new calculation mode to reflect updated
            disbursement data.
        </div>
        <div class="warning" id="HudlineWarning" {{AnySetToAny? "" : "style='display: none;'"}}>
            ESCROW WARNING: There are housing expenses marked as escrowed that are not assigned to specific HUD-1 lines and the file
            is in Legacy but Migrated closing cost mode. Escrowed housing expenses are required to be assigned to HUD-1 lines to
            correctly calculate in Legacy but Migrated mode.
        </div>
        <div id="DatesSection">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="FieldLabel">
                        Estimated Closing Date
                        <input on-change="recalc" checked="{{sEstCloseDLckd}}" type="checkbox" id="sEstCloseDLckd" intro="initMask" on-blur="domblur" />
                        Lock

        <datePicker value="{{sEstCloseD}}" readonly="{{!sEstCloseDLckd}}" id="sEstCloseD" doRecalc="{{'true'}}" />
                    </td>
                    <td style="width: 60px;">&nbsp;</td>
                    <td class="FieldLabel">
                        Closing Date
                        &nbsp;
                        <input type="text" readonly="true" class="mask date" preset="date" id="sDocMagicClosingD" intro="initMask" on-blur="domblur" value="{{sDocMagicClosingD}}" />
                    </td>
                    <td style="width: 60px;">&nbsp;</td>
                    <td class="FieldLabel">
                        1st Payment Date
                        <input on-change="recalc" checked="{{sSchedDueD1Lckd}}" type="checkbox" id="sSchedDueD1Lckd" intro="initMask" on-blur="domblur" />
                        Lock
                        
                          <datePicker readonly="{{!sSchedDueD1Lckd}}" value="{{sSchedDueD1}}" id="sSchedDueD1" doRecalc="{{'true'}}" />
                    </td>
                </tr>
            </table>
        </div>
        
        <div class="TabSection">
            <ul id="Tabs" class="tabnav">
                <li id="AmountsTab">
                    <a href="javascript:void(0);" onclick="switchTab('AmountsTab', 'AmountsTabContent');">Amounts</a>
                </li>
                <li id="PrepaidsTab">
                    <a href="javascript:void(0);" onclick="switchTab('PrepaidsTab', 'PrepaidsTabContent');">Prepaids</a> 
                </li>
                <li id="EscImpTab">
                    <a href="javascript:void(0);" onclick="switchTab('EscImpTab', 'EscImpTabContent');">Escrow/Impounds</a> 
                </li>
                <li id="DetailsTab">
                    <a href="javascript:void(0);" onclick="switchTab('DetailsTab', 'DetailsTabContent');">Details</a>
                </li>
            </ul>
        </div>
        
        <br />
        
        <div id="ContentTabs">
            <div id="AmountsTabContent">
                <table border="0" cellspacing="0" cellpadding="0" width="99%">
                    <tr class="FormTableHeader">
                        <td colspan="2">Description</td>
                        <td></td>
                        <td>Calculation<br />Source</td>
                        <td>Calculator</td>
                        <td>Monthly Amount<br />(PITI)</td>
                        <td>Escrowed</td>
                        <td>Monthly Amt<br />(Servicing)</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="FieldLabel">Mortgage Insurance</td>
                        <td></td>
                        <td>
                            <a href="javascript:linkMe(gVirtualRoot + '/newlos/LoanInfo.aspx?pg=1');">Upfront MIP/FF</a>
                        </td>
                        <td colspan="1"></td>
                        <td>
                            <input readonly="true" value="{{sProMIns}}" type="text" class="mask money" preset="money" id="sProMIns" intro="initMask"
                                on-blur="domblur" />
                        </td>
                        <td>
                            <input on-change="recalc" checked="{{sMInsRsrvEscrowedTri}}" disabled="{{sIssMInsRsrvEscrowedTriReadOnly}}" type="checkbox" id="sMInsRsrvEscrowedTri" on-blur="domblur" />
                        </td>
                        <td>
                            <input readonly="true" value="{{sProMIns}}" type="text" class="mask money" preset="money" id="sProMInsServicing" intro="initMask"
                                on-blur="domblur" />
                        </td>
                    </tr>
                    <tr class="FormTableSubheader">
                        <td colspan="5">Property Insurance</td>
                        <td class="FieldLabel TotalMonthAmt">
                            Total Monthly Amount (PITI) 
                            <br />
                            <span id="sTotalPropertyInsurancePITI">
                                {{sTotalPropertyInsurancePITI}}
                            </span>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    {{#PropInsExps:i}}
                        {{>AmtsRowTemplate}}
                    {{/#PropInsExps}}
                    <tr class="FormTableSubheader">
                        <td colspan="2">Taxes</td>
                        <td colspan="3">Tax Types</td>
                        <td class="FieldLabel TotalMonthAmt">
                            Total Monthly Amount (PITI) 
                            <br />
                            <span id="sTotalPropertyTaxesPITI">
                                {{sTotalRealtyTaxesPITI}}
                            </span>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    {{#TaxesExps:i}}
                        {{>AmtsRowTemplate}}
                    {{/#TaxesExps}}
                    <tr class="FormTableSubheader">
                        <td colspan="5">Other Housing Expenses</td>
                        <td class="FieldLabel TotalMonthAmt">
                            Total Monthly Amount (PITI) 
                            <br />
                            <span id="sTotalOtherHousingExpensesPITI">
                                {{sTotalOtherHousingExpensesPITI}}
                            </span>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    {{#OtherExps:i}}
                        {{>AmtsRowTemplate}}
                    {{/#OtherExps}}
                    <tr><td colspan="7">&nbsp;</td></tr>
                    <tr>
                        <td colspan="5" class="FieldLabel" style="text-align: right; padding-right: 15px;">Monthly Non-P&amp;I Housing Expense Total (PITI)</td>
                        <td>
                            <input readonly="true" value="{{sMonthlyNonPIExpenseTotalPITI}}" type="text" class="mask money" preset="money" id="sMonthlyNonPIExpenseTotalPITI" intro="initMask"
                                on-blur="domblur" />
                        </td>
                    </tr>    
                    <tr>
                        <td colspan="5" class="FieldLabel" style="text-align: right; padding-right: 15px;">Monthly Escrow Payment (Excluding MI)</td>
                        <td colspan="2"></td>
                        <td>
                            <input readonly="true" value="{{sServicingEscrowPmtNonMI}}" type="text" class="mask money" preset="money" id="sServicingEscrowPmtNonMI" intro="initMask"
                                on-blur="domblur" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="FieldLabel" style="text-align: right; padding-right: 15px;">Monthly Escrow Payment (Including MI)</td>
                        <td colspan="2"></td>
                        <td>
                            <input readonly="true" value="{{sServicingEscrowPmt}}" type="text" class="mask money" preset="money" id="sServicingEscrowPmt" intro="initMask"
                                on-blur="domblur" />
                        </td>
                    </tr>
                </table>
            </div>
            
            <div id="PrepaidsTabContent">
                <table border="0" cellspacing="0" cellpadding="0" width="99%">
                    <tr class="FormTableHeader">
                        <td colspan="2">Description</td>
                        <td></td>
                        <td>Calculation<br />Source</td>
                        <td>Prepaid</td>
                        <td>Monthly Amount<br />(PITI)</td>
                        <td>Prepaid Months</td>
                        <td>Prepaid Amt</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="FieldLabel">Mortgage Insurance</td>
                        <td></td>
                        <td>
                            <a href="javascript:linkMe(gVirtualRoot + '/newlos/LoanInfo.aspx?pg=1');">Upfront MIP/FF</a>
                        </td>
                        <td>
                            <input checked="{{sIsMipPrepaid}}" type="checkbox" id="sIsMipPrepaid" disabled="true" readonly="true" intro="initMask" on-blur="domblur" />
                        </td>
                        <td></td>
                        <td>
                            <input readonly="true" value="{{sMipPiaMon}}" type="text" class="mask month" id="sMipPiaMon" intro="initMask"
                                    on-blur="domblur" />
                        </td>
                        <td>
                            <input readonly="true" value="{{sRecurringMipPia}}" type="text" class="mask money" preset="money" id="sRecurringMipPia" intro="initMask"
                                on-blur="domblur" />
                        </td>
                    </tr>
                    <tr class="FormTableSubheader">
                        <td colspan="8">Property Insurance</td>
                    </tr>
                    {{#PropInsExps:i}} 
                        {{>PrepRowTemplate}} 
                    {{/#PropInsExps}}
                    <tr class="FormTableSubheader">
                        <td colspan="2">Taxes</td>
                        <td colspan="6">Tax Types</td>
                    </tr>
                    {{#TaxesExps:i}} 
                        {{>PrepRowTemplate}} 
                    {{/#TaxesExps}}
                    <tr class="FormTableSubheader">
                        <td colspan="8">Other Housing Expenses</td>
                    </tr>
                    {{#OtherExps:i}} 
                        {{>PrepRowTemplate}} 
                    {{/#OtherExps}}
                </table>
            </div>
        
            <div id="EscImpTabContent">
                <table border="0" cellspacing="0" cellpadding="0" width="99%">
                    <tr class="FormTableHeader">
                        <td colspan="2">Description</td>
                        <td></td>
                        <td>Escrowed</td>
                        <td>HUD-1</td>
                        <td>Reserves<br />Cushion</td>
                        <td>Calculation Source</td>
                        <td>1st Disbursement<br />Date</td>
                        <td>Monthly Amt<br />(Servicing)</td>
                        <td>Mths Rsv</td>
                        <td>Reserves<br />Amt</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="FieldLabel">Mortgage Insurance</td>
                        <td></td>
                        <td>
                            <input on-change="recalc" checked="{{sMInsRsrvEscrowedTri}}" disabled="{{sIssMInsRsrvEscrowedTriReadOnly}}" type="checkbox" id="sMInsRsrvEscrowedTri_esc" intro="initMask" on-blur="domblur" />
                        </td>
                        <td>1003</td>
                        <td>
                            <input on-change="recalc" value="{{MICush}}" type="text" class="mask month" id="MICush" intro="initMask" on-blur="domblur" />
                        </td>
                        <td>
                            <a href="javascript:linkMe(gVirtualRoot + '/newlos/LoanInfo.aspx?pg=1');">Upfront MIP/FF</a>
                        </td>
                        <td></td>
                        <td>
                            <input readonly="true" value="{{sProMIns}}" type="text" class="mask money" preset="money" id="sProMInsServicing_esc" intro="initMask"
                                on-blur="domblur" />
                        </td>
                        <td>
                            <input on-change="recalc" checked="{{sMInsRsrvMonLckd}}" type="checkbox" id="sMInsRsrvMonLckd" on-blur="domblur" />
                            <input on-change="recalc" readonly="{{!sMInsRsrvMonLckd}}" value="{{sMInsRsrvMon}}" type="text" class="mask month" id="sMInsRsrvMon"
                                intro="initMask" on-blur="domblur" />
                        </td>
                        <td>
                            <input readonly="true" value="{{sMInsRsrv}}" type="text" class="mask money" preset="money" id="sMInsRsrv" intro="initMask"
                                on-blur="domblur" />
                        </td>
                    </tr>
                    <tr class="FormTableSubheader">
                        <td colspan="3">Property Insurance</td>
                        <td>
                            <input id="PropInsExpsEscrowCB" type="checkbox" on-click="toggleEscrowByCategory:'PropInsExps'"/>
                        </td>
                        <td colspan="7"></td>
                    </tr>
                    {{#PropInsExps:i}} 
                        {{>EscImpRowTemplate}} 
                    {{/#PropInsExps}}
                    <tr class="FormTableSubheader">
                        <td colspan="2">Taxes</td>
                        <td>Tax Type</td>
                        <td>
                            <input id="TaxesExpsEscrowCB" type="checkbox" on-click="toggleEscrowByCategory:'TaxesExps'"/>
                        </td>
                        <td colspan="7"></td>
                    </tr>
                    {{#TaxesExps:i}} 
                        {{>EscImpRowTemplate}} 
                    {{/#TaxesExps}}
                    <tr class="FormTableSubheader">
                        <td colspan="3">Other Housing Expenses</td>
                        <td>
                            <input id="OtherExpsEscrowCB" type="checkbox" on-click="toggleEscrowByCategory:'OtherExps'"/>
                        </td>
                        <td colspan="7"></td>
                    </tr>
                    {{#OtherExps:i}} 
                        {{>EscImpRowTemplate}} 
                    {{/#OtherExps}}
                </table>
                <br />
                <table border="0" cellspacing="0" cellpadding="0" width="90%">
                    <tr>
                        <td>
                            <table width="80%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="FieldLabel">Aggregate Escrow Impounds Calculation Mode</td>
                                    <td>
                                        <asp:Repeater runat="server" ID="sAggEscrowCalcModeTRep">
                                            <HeaderTemplate>
                                                <select on-change="recalc" on-blur="domblur" id="sAggEscrowCalcModeT" value="{{sAggEscrowCalcModeT}}">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                    <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </select>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                                {{#if sAggEscrowCalcModeT == 2 || sAggEscrowCalcModeT == 3}}
                                <tr>
                                    <td class="FieldLabel">Customary Escrow Impounds Calculation Minimum</td>
                                    <td>
                                        <asp:Repeater runat="server" ID="sCustomaryEscrowImpoundsCalcMinTRep">
                                            <HeaderTemplate>
                                                <select on-change="recalc" on-blur="domblur" id="sCustomaryEscrowImpoundsCalcMinT" value="{{sCustomaryEscrowImpoundsCalcMinT}}">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                    <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </select>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                                {{/if}}
                            </table>
                        </td>
                        <td align="right">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="FieldLabel">Aggregate Escrow Adjustment</td>
                                    <td>
                                        <input on-change="recalc" checked="{{sAggregateAdjRsrvLckd}}" type="checkbox" id="sAggregateAdjRsrvLckd" on-blur="domblur" />
                                        <input on-change="recalc" readonly="{{!sAggregateAdjRsrvLckd}}" value="{{sAggregateAdjRsrv}}" type="text" preset="money" 
                                            class="mask money" id="sAggregateAdjRsrv" intro="initMask" on-blur="domblur" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel">Total Escrow Collected at Closing</td>
                                    <td>
                                        <input readonly="true" value="{{sGfeInitialImpoundDeposit}}" type="text" class="mask money" preset="money" id="sGfeInitialImpoundDeposit" 
                                            intro="initMask" on-blur="domblur" />
                                    </td>
                                </tr>
                            </table> 
                        </td>
                    </tr>
                </table>
                <hr />
                <div>
                    For now, your loan
                    <br />
                    <div class="EscrowRadioDiv">
                        <input type="radio" id="sTridEscrowAccountExists_True" disabled="true" name="sTridEscrowAccountExists" checked="{{sTridEscrowAccountExists_True}}" /> will have an escrow account to pay the property costs listed below because
                    </div>
                    <div class="EscrowRadioDiv">
                        <div class="EscrowRadioDiv">
                            <input type="radio" id="sNonMIHousingExpensesEscrowedReasonT_LenderRequired" disabled="{{!sTridEscrowAccountExists}}" name="sNonMIHousingExpensesEscrowedReasonT" checked="{{sNonMIHousingExpensesEscrowedReasonT_LenderRequired}}" /> your lender requires an escrow account.
                        </div>
                        <br />
                        <div class="EscrowRadioDiv">
                            <input type="radio" id="sNonMIHousingExpensesEscrowedReasonT_BorrowerRequested" disabled="{{!sTridEscrowAccountExists}}" name="sNonMIHousingExpensesEscrowedReasonT" checked="{{sNonMIHousingExpensesEscrowedReasonT_BorrowerRequested}}" /> you requested an escrow account (lender does not require).
                        </div>
                    </div>
                    <br />
                    {{#if sTridEscrowAccountExists}}
                        <div class="EscrowDataDiv" id="HasEscrowDiv" runat="server">
                            <div class="GridItem display-table">
                                <span class="EscrowDescriptionSpan">Escrowed Property Costs over Year 1</span>
                                <span id="sClosingDisclosureEscrowedPropertyCostsFirstYear">
                                    {{sClosingDisclosureEscrowedPropertyCostsFirstYear}}
                                </span>
                                <div class="InlineBlock" id="EscrowDescriptionContainer">
                                    {{#EscrowedDescriptions: i}}
                                        <div class="EscrowDescriptionDiv">{{EscrowedDescriptions[i]}}</div>
                                    {{/EscrowedDescriptions}}
                                </div>
                            </div>
                            <div class="GridAlternatingItem display-table">
                                <span class="EscrowDescriptionSpan">Non-Escrowed Property Costs over Year 1</span>
                                <span id="sClosingDisclosureNonEscrowedPropertyCostsFirstYear">
                                    {{sClosingDisclosureNonEscrowedPropertyCostsFirstYear}}
                                </span>
                                <div class="InlineBlock" id="NonEscrowDescriptionContainer">
                                    {{#NonEscrowedDescriptions: i}}
                                        <div class="EscrowDescriptionDiv">{{NonEscrowedDescriptions[i]}}</div>
                                    {{/NonEscrowedDescriptions}}
                                </div>
                            </div>
                            <div class="GridItem display-table">
                                <span class="EscrowDescriptionSpan">Initial Escrow Payment</span>
                                <span id="sGfeInitialImpoundDeposit">{{sGfeInitialImpoundDeposit}}</span>
                                <span class="EscrowDescriptionSpan"></span>
                            </div>
                            <div class="GridAlternatingItem display-table">
                                <span class="EscrowDescriptionSpan">Monthly Escrow Payment</span>
                                <span id="sClosingDisclosureMonthlyEscrowPayment">{{sClosingDisclosureMonthlyEscrowPayment}}</span>
                                <span class="EscrowDescriptionSpan"></span>
                            </div>
                        </div>
                    {{/if}}
                    <br />
                    <div class="EscrowRadioDiv">
                        <input type="radio" id="sTridEscrowAccountExists_False" disabled="true" name="sTridEscrowAccountExists" checked="{{sTridEscrowAccountExists_False}}" /> will not have an escrow account because 
                    </div>
                    <div class="EscrowRadioDiv">
                        <div class="EscrowRadioDiv">
                            <input type="radio" id="sNonMIHousingExpensesNotEscrowedReasonT_Declined" disabled="{{sTridEscrowAccountExists}}" name="sNonMIHousingExpensesNotEscrowedReasonT" checked="{{sNonMIHousingExpensesNotEscrowedReasonT_Declined}}" /> you declined it.
                        </div>
                        <br />
                        <div class="EscrowRadioDiv">
                            <input type="radio" id="sNonMIHousingExpensesNotEscrowedReasonT_NotOffered" disabled="{{sTridEscrowAccountExists}}" name="sNonMIHousingExpensesNotEscrowedReasonT" checked="{{sNonMIHousingExpensesNotEscrowedReasonT_NotOffered}}" /> your lender does not offer one.
                        </div>
                    </div>
                    <br />
                    {{#if !sTridEscrowAccountExists}}
                        <div class="EscrowDataDiv" id="NonEscrowDiv" runat="server">
                            <div class="GridItem">
                                <span class="EscrowDescriptionSpan">Estimated Property Costs over Year 1</span>
                                <span id="sClosingDisclosurePropertyCostsFirstYear">{{sClosingDisclosurePropertyCostsFirstYear}}</span>
                            </div>
                            <div class="GridAlternatingItem">
                                <span class="EscrowDescriptionSpan">Escrowed Waiver Fee</span>
                                <span class="EscrowDescriptionSpan" id="sTridEscrowWaiverFee">{{sTRIDEscrowWaiverFee}}</span>
                            </div>
                        </div>
                    {{/if}}
                </div>
            </div>
        
            <div id="DetailsTabContent"  style="display: none;">
                <table border="0" cellspacing="0" cellpadding="0" width="99%">
                    <tr class="GridHeader">
                        <td colspan="2">Property Insurance</td>
                    </tr>
                    {{#PropInsExps:i}}
                        {{>DetailsRowTemplate}}
                    {{/#PropInsExps}}
                    <tr class="GridHeader">
                        <td colspan="2">Taxes</td>
                    </tr>
                    {{#TaxesExps:i}}
                        {{>DetailsRowTemplate}}
                    {{/#TaxesExps}}
                    <tr class="GridHeader">
                        <td colspan="2">Other Housing Expense</td>
                    </tr>
                    {{#OtherExps:i}}
                        {{>DetailsRowTemplate}}
                    {{/#OtherExps}}
                </table>
            </div>
        </div>
    </script>
    
    <form id="PropHousingExpenses" runat="server">
    <div class="MainRightHeader" nowrap>Housing Expenses</div>
    
    <div id="container"></div>
    
    </form>
</body>
</html>
