<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Loan1003pg3.ascx.cs" Inherits="LendersOfficeApp.newlos.Forms.Loan1003pg3" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import namespace="DataAccess"%>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.Admin" %>
<%@ Register TagPrefix="HRED" TagName="HmdaRaceEthnicityData" Src="~/newlos/HmdaraceEthnicitydata.ascx" %>
<style type="text/css">
    .tdHeight
    {
        height: 40px;
    }

    .construction {
        display: none;
    }
</style>
<script type="text/javascript">
  var oRolodex = null;  
  function _init() {
    if (null == oRolodex)
      oRolodex = new cRolodex();
        
    lockField(<%= AspxTools.JsGetElementById(sTotCcPbsLocked) %>, "sTotCcPbs");
    lockField(<%= AspxTools.JsGetElementById(sTotEstPp1003Lckd) %>, "sTotEstPp1003");
    lockField(<%= AspxTools.JsGetElementById(sTotEstCc1003Lckd) %>, "sTotEstCcNoDiscnt1003");
    lockField(<%= AspxTools.JsGetElementById(sFfUfmip1003Lckd) %>, "sFfUfmip1003");
    lockField(<%= AspxTools.JsGetElementById(sLDiscnt1003Lckd) %>, "sLDiscnt1003");
    lockField(<%= AspxTools.JsGetElementById(sTransNetCashLckd) %>, "sTransNetCash");
    lockField(<%= AspxTools.JsGetElementById(sOCredit1Lckd) %>, "sOCredit1Amt");
    lockField(<%= AspxTools.JsGetElementById(sOCredit1Lckd) %>, "sOCredit1Desc");    
    lockField(<%= AspxTools.JsGetElementById(sLAmtLckd) %>, "sLAmtCalc");

    if (!ML.AreConstructionLoanDataPointsMigrated || ML.isConstruction) {
        lockField(<%= AspxTools.JsGetElementById(sAltCostLckd) %>, "sAltCost");
    }

    if (ML.AreConstructionLoanDataPointsMigrated && ML.isConstruction) {
        lockField(<%= AspxTools.JsGetElementById(sRefPdOffAmt1003Lckd) %>, "sRefPdOffAmt1003");
    }

    <%= AspxTools.JsGetElementById(aIntrvwrMethodT) %>.disabled = !<%= AspxTools.JsGetElementById(aIntrvwrMethodTLckd) %>.checked;
    var a1003InterviewDLckdCb = <%= AspxTools.JsGetElementById(a1003InterviewDLckd) %>;
    if (a1003InterviewDLckdCb) {
        lockField(a1003InterviewDLckdCb, "a1003InterviewD");
    }

    var bIsPurchase = <%= AspxTools.JsBool(m_isPurchase) %>;
    <%= AspxTools.JsGetElementById(sLAmtLckd) %>.disabled = !bIsPurchase || ML.IsRenovationLoan;

    if (ML.AreConstructionLoanDataPointsMigrated && ML.IsConstruction) {
        $(".construction").show();
        $(".non-construction").hide();
    }
     	
    setForeignNational();
  }

  function validateYesNo(control, event) {
    updateDirtyBit(event);

    if (event != null && !(31 < event.keyCode && event.keyCode < 127)) { // if it's not a letter, ignore it
        return;
    }
    
    var value = '';
    if (control.value.length > 0)
        value = control.value.charAt(0);
    if (value == 'y' || value == 'Y') {
        control.value = 'Y';
    } else if (value == 'n' || value == 'N') {
        control.value = 'N';
    } else {
        control.value = '';
    }
    
    if (event != null && ( retrieveEventTarget(event).id == <%= AspxTools.JsGetClientIdString(aBDecCitizen) %> || retrieveEventTarget(event).id == <%= AspxTools.JsGetClientIdString(aBDecResidency)%> ))
		setForeignNational();
  }

  function setForeignNational()
  {
	<% if ( BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) && ! IsReadOnly ) { %>
    
    var aBDecForeignNational = <%= AspxTools.JsGetElementById(aBDecForeignNational) %>;
	  var saBDecCitizen = <%= AspxTools.JsGetElementById(aBDecCitizen) %>.value;
	  var saBDecResidency = <%= AspxTools.JsGetElementById(aBDecResidency) %>.value;
	
//	if (saBDecCitizen == 'N' && saBDecResidency == 'N')
//		aBDecForeignNational.readOnly = false;
//	else

    if (saBDecCitizen != 'N' || saBDecResidency != 'N')
	{
		//aBDecForeignNational.readOnly = true;
		aBDecForeignNational.value = 'N';
	}
	
	<% } %>
  }
</script>


<table id="Table2" cellspacing="0" cellpadding="0" border="0" width="750">
  <tr>
    <td class="LoanFormHeader">VI. ASSETS AND LIABILITIES (cont.)</td>
  </tr>
  <tr>
    <td>
      <table class="InsetBorder" id="Table8" cellspacing="0" cellpadding="0" width="99%" border="0">
        <tr>
          <td colspan="7"><a tabindex="1" onclick="redirectToUladPage('REO');">Schedule of Real Estate Owned</a> </td>
        </tr>
        <tr>
          <td></td>
          <td class="FieldLabel" title="Present Market Value">Market Value</td>
          <td class="FieldLabel" title="Amount of Mortgages & Liens">Amt of Mort.</td>
          <td class="FieldLabel">Gross Rental Income</td>
          <td class="FieldLabel">Mort. Payments</td>
          <td class="FieldLabel" title="Insurance, Maintenance, Taxes & Misc.">Ins/Tax/Misc</td>
          <td class="FieldLabel">Net Rental Income</td>
        </tr>
        <tr>
          <td class="FieldLabel">Totals</td>
          <td><ml:MoneyTextBox ID="aReTotVal" TabIndex="1" ReadOnly="True" Width="86" preset="money" runat="server" /></td>
          <td><ml:MoneyTextBox ID="aReTotMAmt" TabIndex="1" ReadOnly="True" Width="86px" preset="money" runat="server" /></td>
          <td><ml:MoneyTextBox ID="aReTotGrossRentI" TabIndex="1" ReadOnly="True" Width="86px" preset="money" runat="server" /></td>
          <td><ml:MoneyTextBox ID="aReTotMPmt" TabIndex="1" ReadOnly="True" Width="86px" preset="money" runat="server"/></td>
          <td><ml:MoneyTextBox ID="aReTotHExp" TabIndex="1" ReadOnly="True" Width="86px" preset="money" runat="server" /></td>
          <td><ml:MoneyTextBox ID="aReTotNetRentI" TabIndex="1" ReadOnly="True" Width="86px" preset="money" runat="server" /></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td class=FieldLabel></td></tr>
  <tr>
    <td class="LoanForm">
      <table class="InsetBorder" id="Table3" cellspacing="0" cellpadding="0" width="99%" border="0">
        <tr>
          <td class="FormTableSubheader" colspan="3">List any additional names under which credit has previously been received and indicate appropriate creditor name(s) and account number(s):</td>
        </tr>
        <tr>
          <td class="FieldLabel">Alternate name</td>
          <td class="FieldLabel">Creditor name</td>
          <td class="FieldLabel">Account number</td>
        </tr>
        <tr>
          <td>
            <asp:TextBox ID="aAltNm1" TabIndex="2" runat="server" Width="173px"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aAltNm1CreditorNm" TabIndex="2" runat="server" Width="188px"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aAltNm1AccNum" TabIndex="2" runat="server" Width="164px"></asp:TextBox></td>
        </tr>
        <tr>
          <td>
            <asp:TextBox ID="aAltNm2" TabIndex="2" runat="server" Width="174px"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aAltNm2CreditorNm" TabIndex="2" runat="server" Width="186px"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aAltNm2AccNum" TabIndex="2" runat="server" Width="164px"></asp:TextBox></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td class=LoanFormHeader>VII. DETAILS OF TRANSACTION</td></tr>
  <tr>
    <td>
      <table class="InsetBorder" id="Table1" cellspacing="0" cellpadding="0" width="99%" border="0">
        <tr>
          <td nowrap>
            <table id="Table4" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td></td>
                <td class="align-center" colspan="2">Locked</td>
                <td width="5"></td>
                <td></td>
                <td colspan="2">Locked</td>
              </tr>
              <tr>
                <td>a. Purchase price</td>
                <td style="width: 11px"></td>
                <td style="width: 79px">
                    <ml:MoneyTextBox CssClass="non-construction" ID="sPurchPrice" TabIndex="3" Width="86" preset="money" runat="server" onchange="refreshCalculation();" />
                    <ml:MoneyTextBox CssClass="construction" Readonly="true" ID="sPurchasePrice1003" TabIndex="3" Width="86" preset="money" runat="server" onchange="refreshCalculation();" />
                </td>
                <td></td>
                <td>j. <a tabindex="-1" href="javascript:linkMe('../LoanInfo.aspx?pg=2');" >Subordinate financing</a></td>
                <td style="width: 6px"></td>
                <td><ml:MoneyTextBox ID="sONewFinBal" TabIndex="4" Width="86px" preset="money" runat="server" onchange="refreshCalculation();" ReadOnly="True" /></td>
              </tr>
              <tr>
                <td class="no-wrap">b. Alterations, improvements, repairs</td>
                <td style="width: 11px">
                    <input type="checkbox" id="sAltCostLckd" tabindex="3" runat="server" onclick="refreshCalculation();" />
                </td>
                <td style="width: 79px"><ml:MoneyTextBox ID="sAltCost" TabIndex="3" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                <td></td>
                <td>k. Borrower's closing costs paid by Seller</td>
                <td style="width: 6px">
                  <asp:CheckBox ID="sTotCcPbsLocked" TabIndex="4" runat="server" onclick="refreshCalculation();"></asp:CheckBox></td>
                <td><ml:MoneyTextBox ID="sTotCcPbs" TabIndex="4" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="no-wrap">c. Land (if acquired separately)</td>
                <td style="width: 11px"></td>
                <td style="width: 79px">
                    <ml:MoneyTextBox CssClass="non-construction" ID="sLandCost" TabIndex="3" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox>
                    <ml:MoneyTextBox CssClass="construction" Readonly="true" ID="sLandIfAcquiredSeparately1003" TabIndex="3" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox>
                </td>
                <td style="height: 23px"></td>
                <td>l.
                  <ml:ComboBox ID="sOCredit1Desc" TabIndex="4" runat="server" Width="168px"></ml:ComboBox></td>
                <td>
                  <asp:CheckBox ID="sOCredit1Lckd" runat="server" onclick="refreshCalculation();" TabIndex="4"></asp:CheckBox></td>
                <td style="height: 23px"><ml:MoneyTextBox ID="sOCredit1Amt" TabIndex="4" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="no-wrap">d. Refi (incl. debts to be paid off)&nbsp;</td>
                <td class="align-right">
                  <asp:CheckBox ID="sRefPdOffAmt1003Lckd" TabIndex="3" runat="server" onclick="refreshCalculation();"></asp:CheckBox></td>
                <td style="width: 79px"><ml:MoneyTextBox ID="sRefPdOffAmt1003" TabIndex="3" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                <td></td>
                <td>&nbsp;&nbsp;&nbsp;<ml:ComboBox ID="sOCredit2Desc" TabIndex="4" runat="server" Width="168px"></ml:ComboBox>
                </td>
                <td>
                  </td>
                <td><ml:MoneyTextBox ID="sOCredit2Amt" TabIndex="4" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="no-wrap">e. Estimated prepaid items&nbsp;</td>
                <td class="align-right">
                  <asp:CheckBox ID="sTotEstPp1003Lckd" TabIndex="3" runat="server" onclick="refreshCalculation();"></asp:CheckBox></td>
                <td style="width: 79px"><ml:MoneyTextBox ID="sTotEstPp1003" TabIndex="3" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                <td></td>
                <td colspan="2">&nbsp;&nbsp;&nbsp;<ml:ComboBox ID="sOCredit3Desc" TabIndex="4" runat="server" Width="168px"></ml:ComboBox>
                </td>
                <td><ml:MoneyTextBox ID="sOCredit3Amt" TabIndex="4" Width="86px" preset="money" runat="server" onchange="refreshCalculation();" /></td>
              </tr>
              <tr>
                <td class="little-indent"><ml:EncodedLiteral runat="server" ID="PrepaidsAndReservesLabel">Prepaids and reserves</ml:EncodedLiteral></td>
                <td><ml:MoneyTextBox runat="server" ID="sTotEstPp" ReadOnly="true"></ml:MoneyTextBox></td>
                <td style="width: 79px"></td>
                <td></td>
                <td colspan="2">&nbsp;&nbsp;&nbsp;<ml:ComboBox ID="sOCredit4Desc" TabIndex="4" runat="server" Width="168px"></ml:ComboBox>
                </td>
                <td><ml:MoneyTextBox ID="sOCredit4Amt" TabIndex="4" Width="86px" preset="money" runat="server" onchange="refreshCalculation();" /></td>
              </tr>
              <tr>
                <td class="little-indent"><ml:EncodedLiteral runat="server" ID="ProrationsPaidByBorrowerLabel">Prorations paid by borrower</ml:EncodedLiteral></td>
                <td style="width: 11px"><ml:MoneyTextBox runat="server" ID="sTotalBorrowerPaidProrations" ReadOnly="true"></ml:MoneyTextBox></td>
                <td style="width: 79px"></td>
                <td></td>
                <td colspan="2">&nbsp;&nbsp;&nbsp;Lender credit
                </td>
                <td><ml:MoneyTextBox ID="sOCredit5Amt" TabIndex="4" Width="86px" preset="money" runat="server" readonly="true" /></td>
              </tr>              
              <asp:PlaceHolder runat="server" ID="HideIfOtherFinancingCcIncludedInCc">
              <tr>
                <td></td>
                <td style="width: 11px"></td>
                <td style="width: 79px"></td>
                <td></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;Other financing closing costs</td>
                <td align="right">-&nbsp;</td>
                <td><ml:MoneyTextBox ID="sONewFinCc" TabIndex="4" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
              </tr>
              </asp:PlaceHolder>
              <tr>
                <td>f. Estimated closing costs</td>
                <td class="align-right">
                  <asp:CheckBox ID="sTotEstCc1003Lckd" TabIndex="3" runat="server" onclick="refreshCalculation();"></asp:CheckBox></td>
                <td style="width: 79px"><ml:MoneyTextBox ID="sTotEstCcNoDiscnt1003" TabIndex="3" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                <td colspan="2" runat="server" id="LabelCellM">m. Loan amount (exclude PMI, MIP, FF financed)</td>
                <td style="width: 6px" runat="server" id="LockedCellM">
                  <asp:CheckBox ID="sLAmtLckd" onclick="refreshCalculation();" runat="server" TabIndex="202"></asp:CheckBox></td>
                <td runat="server" id="AmountCellM"><ml:MoneyTextBox ID="sLAmtCalc" TabIndex="4" ReadOnly="True" Width="86px" preset="money" runat="server"></ml:MoneyTextBox></td>
              </tr>
              <asp:PlaceHolder runat="server" ID="DisplayIfOtherFinancingCcIncludedInCc">
              <tr>
                <td class="little-indent">This loan closing costs</td>
                <td>
                    <ml:MoneyTextBox runat="server" ID="sTotEstCcNoDiscnt" ReadOnly="true"></ml:MoneyTextBox>
                </td>
                <td style="width: 79px"></td>
                <td colspan="2"></td>
                <td style="width: 6px"></td>
                <td></td>
              </tr>
              <tr>
                <td class="little-indent">Other financing costs</td>
                <td style="width: 11px">
                    <ml:MoneyTextBox runat="server" ID="sONewFinCc2" data-field-id="sONewFinCc" onchange="refreshCalculation();"></ml:MoneyTextBox>
                </td>
                <td style="width: 79px"></td>
                <td colspan="2"></td>
                <td style="width: 6px"></td>
                <td></td>
              </tr>
              </asp:PlaceHolder>
              <tr>
                <td>g. PMI,&nbsp;MIP, Funding Fee</td>
                <td class="align-right">
                  <asp:CheckBox ID="sFfUfmip1003Lckd" TabIndex="3" runat="server" onclick="refreshCalculation();"></asp:CheckBox></td>
                <td style="width: 79px"><ml:MoneyTextBox ID="sFfUfmip1003" TabIndex="3" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                <td></td>
                <td>n. PMI, MIP, Funding Fee financed</td>
                <td style="width: 6px"></td>
                <td><ml:MoneyTextBox ID="sFfUfmipFinanced" TabIndex="4" ReadOnly="True" Width="86px" preset="money" runat="server"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td>h. Discount (if Borrower will pay)</td>
                <td class="align-right">
                  <asp:CheckBox ID="sLDiscnt1003Lckd" TabIndex="3" runat="server" onclick="refreshCalculation();"></asp:CheckBox></td>
                <td style="width: 79px"><ml:MoneyTextBox ID="sLDiscnt1003" TabIndex="3" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                <td></td>
                <td>o. Loan amount (add m &amp; n)</td>
                <td style="width: 6px"></td>
                <td><ml:MoneyTextBox ID="sFinalLAmt" TabIndex="4" ReadOnly="True" Width="86px" preset="money" runat="server"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel">i. Total costs (add items a to h)</td>
                <td style="width: 11px"></td>
                <td style="width: 79px"><ml:MoneyTextBox ID="sTotTransC" TabIndex="3" ReadOnly="True" Width="86px" preset="money" runat="server"></ml:MoneyTextBox></td>
                <td></td>
                <td>p. Cash from / to Borr (subtract j, k, l &amp; o from i)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td style="width: 6px">
                  <asp:CheckBox ID="sTransNetCashLckd" TabIndex="4" runat="server" onclick="refreshCalculation();"></asp:CheckBox></td>
                <td><ml:MoneyTextBox ID="sTransNetCash" TabIndex="4" Width="86px" preset="money" runat="server"></ml:MoneyTextBox></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td class="LoanFormHeader">VIII. DECLARATIONS</td>
  </tr>
  <tr>
    <td>
      <table id="Table5" cellspacing="0" cellpadding="0" width="600" border="0">
        <tr>
          <td class="FieldLabel" colspan="2">If you answer "Yes" to any questions a through i, please provide the explanation below</td>
          <td class="FieldLabel">Borr&nbsp;&nbsp;</td>
          <td class="FieldLabel" nowrap>Co-borr</td>
        </tr>
        <tr>
          <td class="FieldLabel" colspan="2"></td>
          <td class="FieldLabel" align="left">Y/N</td>
          <td class="FieldLabel" nowrap align="left">Y/N</td>
        </tr>
        <tr valign="top">
          <td>a.</td>
          <td>Are there any outstanding judgments against you?</td>
          <td>
            <asp:TextBox ID="aBDecJudgment" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aCDecJudgment" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>b.</td>
          <td>Have you been declared bankrupt within the past 7 years?</td>
          <td>
            <asp:TextBox ID="aBDecBankrupt" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aCDecBankrupt" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>c.</td>
          <td>Have you had property foreclosed upon or given title or deed in lieu thereof in the last 7 years?</td>
          <td valign="top">
            <asp:TextBox ID="aBDecForeclosure" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="3"></asp:TextBox></td>
          <td valign="top">
            <asp:TextBox ID="aCDecForeclosure" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="3"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>d.</td>
          <td>Are you a party to a lawsuit?</td>
          <td>
            <asp:TextBox ID="aBDecLawsuit" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aCDecLawsuit" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>e.</td>
          <td>Have you directly or indirectly been obligated on any loan which resulted in foreclosure, transfer of title in lieu of foreclosure, or judgment? (This would include such loans as home mortgage loans, SBA loans, home improvement loans, educational loans, manufactured (mobile) home loans, any mortgage, financial obligation, bond, or loan guarantee.&nbsp; If "Yes", provide details, including if any, and reasons for the action.)</td>
          <td valign="top">
            <asp:TextBox ID="aBDecObligated" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td valign="top">
            <asp:TextBox ID="aCDecObligated" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>f.</td>
          <td>Are you presently delinquent or in default on any Federal debt or any other loan, mortgage, financial obligation bond, or loan guarantee? If "Yes", give details as described in the preceding question.</td>
          <td valign="top">
            <asp:TextBox ID="aBDecDelinquent" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td valign="top">
            <asp:TextBox ID="aCDecDelinquent" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>g.</td>
          <td>Are you obligated to pay alimony, child support, or separate maintenance?</td>
          <td>
            <asp:TextBox ID="aBDecAlimony" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aCDecAlimony" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>h.</td>
          <td>Is any part of the down payment borrowed?</td>
          <td>
            <asp:TextBox ID="aBDecBorrowing" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aCDecBorrowing" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>i.</td>
          <td>Are you a co-maker or endorser on a note?</td>
          <td>
            <asp:TextBox ID="aBDecEndorser" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aCDecEndorser" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>j.</td>
          <td>Are you a U.S. citizen?</td>
          <td>
            <asp:TextBox ID="aBDecCitizen" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aCDecCitizen" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>k.</td>
          <td>Are you a permanent resident alien?</td>
          <td>
            <asp:TextBox ID="aBDecResidency" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aCDecResidency" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>

        <tr valign="top" style='display:none;'>
          <td>-</td>
          <td>Are you a non-resident alien (foreign national)? (For PriceMyLoan)</td>
          <td>
            <asp:TextBox ID="aBDecForeignNational" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>&nbsp;</td>
        </tr>
        <tr valign="top">
          <td>l.</td>
          <td>Do you intend to occupy the property as your primary residence?&nbsp; If "Yes," complete question m below</td>
          <td valign="top">
            <asp:TextBox ID="aBDecOcc" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td valign="top">
            <asp:TextBox ID="aCDecOcc" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td style="height: 14px; padding-right: 2px">m.</td>
          <td>Have you had an ownership interest in a property in the last three years?</td>
          <td style="height: 14px">
            <asp:TextBox ID="aBDecPastOwnership" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td style="height: 14px">
            <asp:TextBox ID="aCDecPastOwnership" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td style="height: 15px" colspan="2">
            <p>
              &nbsp;&nbsp; (1)&nbsp;Type of property: (PR - Principal Residence, SH - Second Home, IP - Investment Property)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
          </td>
          <td style="height: 15px">
            <asp:DropDownList ID="aBDecPastOwnedPropT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" TabIndex="5" runat="server">
            </asp:DropDownList>
          </td>
          <td style="height: 15px">
            <asp:DropDownList ID="aCDecPastOwnedPropT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" TabIndex="6" runat="server">
            </asp:DropDownList>
          </td>
        </tr>
        <tr valign="top">
          <td colspan="2">&nbsp;&nbsp; (2) Title held: (S - Solely by Yourself, SP - Jointly w/ Spouse, O - Jointly w/ Another Person)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td>
            <asp:DropDownList ID="aBDecPastOwnedPropTitleT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" TabIndex="5" runat="server">
            </asp:DropDownList>
          </td>
          <td>
            <asp:DropDownList ID="aCDecPastOwnedPropTitleT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" TabIndex="6" runat="server">
            </asp:DropDownList>
          </td>
        </tr>    
        <tr>
            <td colspan="2"></td>
            <td colspan="2" class="tdHeight">
                <input type="button" value="Explanations" onclick="addExplanations();" />
            </td>
        </tr>    
      </table>   
    </td>
  </tr>
  <tr>
    <td class="LoanFormHeader">X. INFORMATION FOR GOVERNMENT MONITORING PURPOSES</td>
  </tr>
  <tr>
    <td>
      <table id="Table6" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
          <td style="padding-left: 5px" colspan="2">
            <HRED:HmdaRaceEthnicityData runat="server" ID="HREData" />
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table id="Table7" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
          <td class="FormTableSubheader" colspan="2" id="ToBeCompletedByLoanOriginator">To be Completed by Loan Originator</td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap colspan="2"><UC:CFM ID="CFM" runat="server" /></td>
        </tr>
        <tr>
          <td class="FieldLabel">This application was taken by</td>
          <td>
            <asp:DropDownList ID="aIntrvwrMethodT" TabIndex="7" runat="server" Width="158px" Height="24px" />
            <label><input type="checkbox" runat="server" id="aIntrvwrMethodTLckd" onchange="refreshCalculation();"/>Lock</label>
          </td>
        </tr>
        <tr>
          <td class="FieldLabel">Interview Date</td>
          <td>
              <ml:DateTextBox id="a1003InterviewD" runat="server" width="75" preset="date"></ml:DateTextBox>
              <asp:CheckBox runat="server" ID="a1003InterviewDLckd" onclick="refreshCalculation();" Text="Lckd"/>
          </td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Originator's Name</td>
          <td>
            <asp:TextBox ID="LoanOfficerName" TabIndex="7" runat="server" Width="267px" /></td>
        </tr>
        <tr>
          <%--OPM 55321: changed the label to its current value, added the mask.js settings to the text field to will make it harder to submit non-numbers. --%>
          <td class="FieldLabel">Loan Originator NMLS ID</td>
          <td><asp:TextBox ID="LoanOfficerLoanOriginatorIdentifier" TabIndex="7" runat="server" CssClass="mask" preset="nmlsID"/></td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Originator's License Number</td>
          <td>
            <asp:TextBox ID="LoanOfficerLicenseNumber" TabIndex="7" runat="server" Width="120px" /></td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Originator's Phone</td>
          <td><ml:PhoneTextBox ID="LoanOfficerPhone" TabIndex="7" runat="server" preset="phone" Width="120" /></td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Originator's Email</td>
          <td><ml:PhoneTextBox ID="LoanOfficerEmail" TabIndex="7" runat="server" Width="120" /></td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Origination Company's Name</td>
          <td><asp:TextBox ID="BrokerName" TabIndex="7" runat="server" Width="357px"/></td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Origination Company NMLS ID</td>
          <td><asp:TextBox ID="LoanOfficerCompanyLoanOriginatorIdentifier" runat="server" TabIndex="7" CssClass="mask" preset="nmlsID"/></td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Origination Company's License Number</td>
          <td><asp:TextBox ID="BrokerLicenseNumber" TabIndex="7" runat="server" Width="120px" /></td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Origination Company's Address</td>
          <td><asp:TextBox ID="BrokerStreetAddr" TabIndex="7" runat="server" Width="359px" /></td>
        </tr>
        <tr>
          <td></td>
          <td>
            <asp:TextBox ID="BrokerCity" TabIndex="7" runat="server" Width="265px"></asp:TextBox><ml:StateDropDownList ID="BrokerState" TabIndex="7" runat="server"></ml:StateDropDownList>
            <ml:ZipcodeTextBox ID="BrokerZip" TabIndex="7" runat="server" preset="zipcode" Width="50" CssClass="mask" /></td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Origination Company's Phone</td>
          <td class="FieldLabel"><ml:PhoneTextBox ID="BrokerPhone" TabIndex="7" runat="server" preset="phone" Width="120" CssClass="mask" /> 
          Fax <ml:PhoneTextBox ID="BrokerFax" TabIndex="7" runat="server" preset="phone" Width="120" CssClass="mask" /></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

