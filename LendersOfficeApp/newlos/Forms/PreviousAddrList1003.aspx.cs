﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
    public class Address
    {
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public Address(string sStreet, string sCity, string sState, string sZip)
        {
            Street = sStreet;
            City = sCity;
            State = sState;
            Zip = sZip;
        }
    }

    public partial class PreviousAddrList1003 : LendersOffice.Common.BasePage
    {
        Guid m_guidLoanId;
        Guid m_guidAppId;
        string m_sCallbackFunction;

        List<Address> m_Addresses = new List<Address>();

        protected void Page_Load(object sender, EventArgs e)
        {
            //get loan id
            //Get function name to call back

            m_guidLoanId = RequestHelper.GetGuid("loanid");
            m_guidAppId = RequestHelper.GetGuid("appid");

            m_sCallbackFunction = RequestHelper.GetSafeQueryString("callback");

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(m_guidLoanId, typeof(PreviousAddrList1003));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(m_guidAppId);

            Address add1 = new Address(dataApp.aBPrev1Addr, dataApp.aBPrev1City, dataApp.aBPrev1State, dataApp.aBPrev1Zip);
            InsertAddressWithDeDup(add1);
            Address add2 = new Address(dataApp.aBPrev2Addr, dataApp.aBPrev2City, dataApp.aBPrev2State, dataApp.aBPrev2Zip);
            InsertAddressWithDeDup(add2);
            Address add3 = new Address(dataApp.aCPrev1Addr, dataApp.aCPrev1City, dataApp.aCPrev1State, dataApp.aCPrev1Zip);
            InsertAddressWithDeDup(add3);
            Address add4 = new Address(dataApp.aCPrev2Addr, dataApp.aCPrev2City, dataApp.aCPrev2State, dataApp.aCPrev2Zip);
            InsertAddressWithDeDup(add4);

            if (m_Addresses.Count == 0)
            {

            }
            else
            {
                grdAddresses.DataSource = m_Addresses;
                grdAddresses.DataBind();
            }
        }

        private void InsertAddressWithDeDup(Address addr)
        {
            var result = from a in m_Addresses
                         where (a.Street == addr.Street) && (a.City == addr.City) && (a.State == addr.State) && (a.Zip == addr.Zip)
                         select a;

            if (result.Count() == 0)
                m_Addresses.Add(addr);
        }

        protected void grdAddresses_OnItemDataBound(object send, EventArgs e)
        { 
        
        }
    }
}
