<%@ Import Namespace="DataAccess" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>

<%@ Page Language="c#" CodeBehind="Transmittal_04.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.Transmittal_04" %>

<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>Single Transmittal Summary</title>
    <link type="text/css" href="Transmittal_04.css" />
</head>
<body bgcolor="gainsboro" ms_positioning="FlowLayout">
    <script language="javascript" type="text/javascript">
<!--
        var oRolodex = null;

        function _init() {
            if (null == oRolodex)
            {
                oRolodex = new cRolodex();
            }

            <%= AspxTools.JsGetElementById(sPurchPrice) %>.readOnly = <%= AspxTools.JsGetElementById(sLPurposeT) %>.value != <%=AspxTools.JsString(E_sLPurposeT.Purchase)%>;
  
            lockField(<%= AspxTools.JsGetElementById(sHcltvRLckd) %>, 'sHcltvR');
            lockField(<%= AspxTools.JsGetElementById(sDebtToHousingGapRLckd) %>, 'sDebtToHousingGapR');
            lockField(<%= AspxTools.JsGetElementById(sTransmFntcLckd) %>, 'sTransmFntc');
            lockField(<%= AspxTools.JsGetElementById(sLenLNumLckd) %>, 'sLenLNum');
            lockField(<%= AspxTools.JsGetElementById(sInvestLNumLckd) %>, 'sInvestLNum');

            var bSpNegCfLckd = <%= AspxTools.JsGetElementById(aSpNegCfLckd) %>.checked;
            var bOpNegCfLckd = <%= AspxTools.JsGetElementById(aOpNegCfLckd) %>.checked;
      
            <%= AspxTools.JsGetElementById(aOpNegCf) %>.readOnly = !bOpNegCfLckd;
            <%= AspxTools.JsGetElementById(aSpNegCf) %>.readOnly = !bSpNegCfLckd;
            <%= AspxTools.JsGetElementById(aBSpPosCf) %>.readOnly = !bSpNegCfLckd;
            <%= AspxTools.JsGetElementById(aCSpPosCf) %>.readOnly = !bSpNegCfLckd;  
      
            var inputs = <%= AspxTools.JsGetElementById(sBuydown) %>.getElementsByTagName("INPUT");
            var bBuyDown = false;
            for (var i = 0; i < inputs.length; i++)
            {
                if (inputs[i].value == "1" && inputs[i].checked == true)
                {
                    bBuyDown = true;
                    break;
                }
            }
      
            <%= AspxTools.JsGetElementById(sTransmBuydwnTermDesc) %>.readOnly = !bBuyDown;

            var isFirstLien = <%= AspxTools.JsGetElementById(sLienPosT) %>.value == <%=AspxTools.JsString(E_sLienPosT.First)%>;
  
            <%= AspxTools.JsGetElementById(sSubFin) %>.readOnly = !isFirstLien ;  
            disableDDL(<%= AspxTools.JsGetElementById(s1stMOwnerT) %>, isFirstLien);
            <%= AspxTools.JsGetElementById(s1stMtgOrigLAmt) %>.readOnly = isFirstLien;
            on_sFinMethodPrintAsOtherClick();
            resetAppraisalTypeCheckboxes();

            SetQualTermReadonly();
        }

        function SetQualTermReadonly() {
            var isManual = $(".sQualTermCalculationType").val() == '<%=AspxTools.JsStringUnquoted(QualTermCalculationType.Manual)%>';
            $(".sQualTerm").prop("readonly", !isManual);
        }

          function on_sFinMethodPrintAsOtherClick() {
            if (<%= AspxTools.JsGetElementById(sFinMethodPrintAsOther) %>.checked) {
              <%= AspxTools.JsGetElementById(sFinMethPrintAsOtherDesc) %>.readOnly = false;
            } else {
              <%= AspxTools.JsGetElementById(sFinMethPrintAsOtherDesc) %>.readOnly = true;
              <%= AspxTools.JsGetElementById(sFinMethPrintAsOtherDesc) %>.value = "";
    
            }
          }
  
          function _postSaveMe() 
          {
            updateOccupancyStatusSavedValue();
            onChangeOccupancyStatus();
          }
  
          //updates the hidden field with the saved value of the occupancy status. 
          function updateOccupancyStatusSavedValue() 
          {
            var dropdown      = <%= AspxTools.JsGetElementById(aOccT) %>;
            var selectedText  = dropdown.options[dropdown.selectedIndex].text.toLowerCase();
            if ( !parent.body.isDirty() ) 
            {
                document.getElementById('savedOccupency').value = selectedText;
            }
          }
  
          // 07/18/07 av disables/enables the anchor around "Borrower's Primary Residence" 
          // when the user changes the occupancy status. 
          function onChangeOccupancyStatus() {

            var initialText	  = document.getElementById('savedOccupency').value;
            var dropdown      = <%= AspxTools.JsGetElementById(aOccT) %>;
            var selectedText  = dropdown.options[dropdown.selectedIndex].text.toLowerCase(); 

            if (initialText == selectedText ||  (
                initialText  == "secondary residence" && 
                selectedText == "investment") || (
                initialText	 == "investment" && 
                selectedText == "secondary residence") )
            {
                document.getElementById("BPRAnchor").setAttribute('href', 'javascript:hrefBorrowPrimaryResidence()');		 
                document.getElementById("BPRAnchor").style.color  = 'blue'; 
            } 
            else 
            {
                document.getElementById("BPRAnchor").removeAttribute('href'); 
                document.getElementById("BPRAnchor").style.color  = 'black'; 
            }	
          }
  
  
          //called when the user clicks on the Borrower's Primary Residence Anchor tag
          function hrefBorrowPrimaryResidence() 
          {
            var dropdown = <%= AspxTools.JsGetElementById(aOccT) %>;

            if ( dropdown.options[dropdown.selectedIndex].text.toLowerCase() == "primary residence" ) 
            {
                linkMe('../LoanInfo.aspx');
            } 
            else 
            {
                linkMe('../BorrowerInfo.aspx?pg=7');
            }
        }

        function f_populateFromAsset() {
          var args = new Object();
          args["LoanID"] = <%= AspxTools.JsString(LoanID) %>;
 
          var result = gService.loanedit.call("CalculateAssetTotal", args);
          if (!result.error) {
            <%= AspxTools.JsGetElementById(sVerifAssetAmt) %>.value = result.value["VerifiedAssetTotal"];
            updateDirtyBit();
          }
        }
//-->
    </script>
    <form id="TransmittalSingle" method="post" runat="server">
        <table class="FormTable" id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="MainRightHeader no-wrap">Uniform Underwriting and Transmittal Summary</td>
            </tr>
            <tr>
                <td style="padding-left: 10px" class="no-wrap">
                    <table id="Table5" cellspacing="0" cellpadding="0" width="770" border="0">
                        <tr>
                            <td class="LoanFormHeader" style="padding-left: 5px">I. Borrower and Property Information</td>
                        </tr>
                        <tr>
                            <td>
                                <table class="InsetBorder" id="Table2" cellspacing="0" cellpadding="0" width="98%" border="0">
                                    <tr>
                                        <td class="no-wrap">
                                            <table id="Table20" cellspacing="0" cellpadding="0" border="0">
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Borrower</td>
                                                    <td class="FieldLabel no-wrap"></td>
                                                    <td class="no-wrap" width="20">&nbsp;
                                                    </td>
                                                    <td class="FieldLabel no-wrap">Co-borrower</td>
                                                    <td class="no-wrap"></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">First Name</td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="aBFirstNm" runat="server"></asp:TextBox></td>
                                                    <td class="no-wrap"></td>
                                                    <td class="FieldLabel no-wrap">First Name</td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="aCFirstNm" TabIndex="5" runat="server"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Middle Name</td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="aBMidNm" runat="server"></asp:TextBox></td>
                                                    <td class="no-wrap"></td>
                                                    <td class="FieldLabel no-wrap">Middle Name</td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="aCMidNm" TabIndex="5" runat="server"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Last Name</td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="aBLastNm" runat="server"></asp:TextBox></td>
                                                    <td class="no-wrap"></td>
                                                    <td class="FieldLabel no-wrap">Last Name</td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="aCLastNm" TabIndex="5" runat="server"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Suffix</td>
                                                    <td class="no-wrap">
                                                        <ml:ComboBox ID="aBSuffix" runat="server"></ml:ComboBox></td>
                                                    <td class="no-wrap"></td>
                                                    <td class="FieldLabel no-wrap">Suffix
                                                    </td>
                                                    <td class="no-wrap">
                                                        <ml:ComboBox ID="aCSuffix" TabIndex="5" runat="server"></ml:ComboBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">SSN</td>
                                                    <td class="no-wrap">
                                                        <ml:SSNTextBox ID="aBSsn" runat="server" Width="75px" preset="ssn"></ml:SSNTextBox></td>
                                                    <td class="no-wrap"></td>
                                                    <td class="FieldLabel no-wrap">SSN</td>
                                                    <td class="no-wrap">
                                                        <ml:SSNTextBox ID="aCSsn" TabIndex="5" runat="server" Width="75px" preset="ssn"></ml:SSNTextBox></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="InsetBorder" id="Table21" cellspacing="0" cellpadding="0" width="98%" border="0">
                                    <tr>
                                        <td class="FieldLabel no-wrap">Property Address
                                        </td>
                                        <td class="no-wrap" width="100%">
                                            <asp:TextBox ID="sSpAddr" TabIndex="10" runat="server" Width="265px"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="no-wrap"></td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sSpCity" TabIndex="10" runat="server" Width="166px"></asp:TextBox><ml:StateDropDownList ID="sSpState" TabIndex="10" runat="server" IncludeTerritories="false"></ml:StateDropDownList>
                                            <ml:ZipcodeTextBox ID="sSpZip" TabIndex="10" runat="server" Width="50" preset="zipcode"></ml:ZipcodeTextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="InsetBorder" id="Table16" cellspacing="0" cellpadding="0" width="98%" border="0">
                                    <tr>
                                        <td class="no-wrap">
                                            <table id="Table10" cellspacing="0" cellpadding="0" border="0">
                                                <tr>
                                                    <td class="FieldLabel" style="height: 16px">Property Type</td>
                                                    <td style="height: 16px">
                                                        <asp:DropDownList ID="sGseSpT" TabIndex="10" runat="server"></asp:DropDownList></td>
                                                    <td class="FieldLabel" style="height: 16px" width="20"></td>
                                                    <td class="FieldLabel" style="height: 16px">Number of Units</td>
                                                    <td style="height: 16px">
                                                        <asp:TextBox ID="sUnitsNum" TabIndex="10" runat="server" Width="82px" MaxLength="4" BackColor="White"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Occupancy Status</td>
                                                    <td class="FieldLabel" valign="top">
                                                        <asp:DropDownList ID="aOccT" TabIndex="10" runat="server" onchange="refreshCalculation();"></asp:DropDownList></td>
                                                    <td class="FieldLabel"></td>
                                                    <td class="FieldLabel">Sales Price</td>
                                                    <td>
                                                        <ml:MoneyTextBox ID="sPurchPrice" TabIndex="10" runat="server" Width="90" preset="money" onchange="refreshCalculation();" ReadOnly="True"></ml:MoneyTextBox></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td class="FieldLabel no-wrap"></td>
                                                    <td class="FieldLabel no-wrap">Appraised Value</td>
                                                    <td>
                                                        <ml:MoneyTextBox ID="sApprVal" TabIndex="10" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel">Freddie Mac Project Classification</td>
                                                    <td class="FieldLabel">
                                                        <asp:DropDownList ID="sSpProjectClassFreddieT" TabIndex="10" runat="server"></asp:DropDownList></td>
                                                    <td class="FieldLabel no-wrap"></td>
                                                    <td class="FieldLabel no-wrap"></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel">Fannie Mae Project Classification</td>
                                                    <td class="FieldLabel">
                                                        <asp:DropDownList ID="sSpProjectClassFannieT" TabIndex="10" runat="server"></asp:DropDownList></td>
                                                    <td class="FieldLabel no-wrap"></td>
                                                    <td class="FieldLabel no-wrap"></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel">Project Name</td>
                                                    <td class="FieldLabel">
                                                        <asp:TextBox ID="sProjNm" TabIndex="10" runat="server" Width="181px" MaxLength="72"></asp:TextBox></td>
                                                    <td class="FieldLabel"></td>
                                                    <td class="FieldLabel">Property Rights</td>
                                                    <td>
                                                        <asp:DropDownList ID="sEstateHeldT" TabIndex="10" runat="server"></asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel">CPM Project ID# (if any)</td>
                                                    <td>
                                                        <asp:TextBox ID="sCpmProjectId" runat="server" TabIndex="10" Width="181px"></asp:TextBox></td>
                                                    <td class="FieldLabel no-wrap"></td>
                                                    <td class="FieldLabel no-wrap"></td>
                                                    <td></td>

                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="LoanFormHeader" style="padding-left: 5px">II. Mortgage Information</td>
                        </tr>
                        <tr>
                            <td>
                                <table id="Table7" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td class="FieldLabel">Loan Type</td>
                                        <td>
                                            <asp:DropDownList ID="sLT" TabIndex="10" runat="server" onchange="refreshCalculation();" EnableViewState="False"></asp:DropDownList>
                                        </td>
                                        <td class="FieldLabel">Amortization Type</td>
                                        <td>
                                            <asp:DropDownList ID="sFinMethT" TabIndex="10" runat="server" onchange="refreshCalculation();" EnableViewState="False"></asp:DropDownList>
                                        </td>
                                        <td class="FieldLabel no-wrap">Amort. Desc.</td>
                                        <td>
                                            <asp:TextBox ID="sFinMethDesc" TabIndex="10" runat="server" Width="152px" MaxLength="50"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel"></td>
                                        <td></td>
                                        <td class="FieldLabel" colspan="3">
                                            <asp:CheckBox ID="sFinMethodPrintAsOther" onclick="on_sFinMethodPrintAsOtherClick();" TabIndex="10" runat="server" Text="Amortization Type Other"></asp:CheckBox>
                                            &nbsp;-
                                            <asp:TextBox ID="sFinMethPrintAsOtherDesc" runat="server"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel"></td>
                                        <td></td>
                                        <td class="FieldLabel">
                                            <asp:CheckBox ID="sBiweeklyPmt" TabIndex="10" runat="server" Text="Biweekly Payment"></asp:CheckBox>
                                        </td>
                                        <td class="FieldLabel"><asp:CheckBox ID="sBalloonPmt" TabIndex="10" runat="server"></asp:CheckBox>Balloon</td>
                                        <td class="FieldLabel"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Loan Purpose</td>
                                        <td>
                                            <asp:DropDownList ID="sLPurposeT" TabIndex="10" runat="server" onchange="refreshCalculation();" EnableViewState="False"></asp:DropDownList>
                                        </td>
                                        <td class="FieldLabel no-wrap"></td>
                                        <td></td>
                                        <td class="FieldLabel">Lien Position</td>
                                        <td>
                                            <asp:DropDownList ID="sLienPosT" TabIndex="10" runat="server" onchange="refreshCalculation();" EnableViewState="False"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Purpose of Refi</td>
                                        <td colspan="2">
                                            <asp:DropDownList ID="sGseRefPurposeT" TabIndex="10" runat="server"></asp:DropDownList>
                                        </td>
                                        <td></td>
                                        <td class="FieldLabel">Sub. Fin.</td>
                                        <td>
                                            <ml:MoneyTextBox ID="sSubFin" TabIndex="10" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="Table11" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td class="FieldLabel" colspan="2">&nbsp;</td>
                                        <td></td>
                                        <td></td>
                                        <td class="FieldLabel" colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel" colspan="2">Note Information</td>
                                        <td></td>
                                        <td></td>
                                        <td class="FieldLabel" colspan="2">If Second Mortgage</td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">Original Loan Amount</td>
                                        <td>
                                            <ml:MoneyTextBox ID="sFinalLAmt" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                        </td>
                                        <td class="FieldLabel">Mortgage Originator</td>
                                        <td>
                                            <asp:DropDownList ID="sMOrigT" TabIndex="10" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                                        </td>
                                        <td class="FieldLabel">First Mortgage Owner</td>
                                        <td>
                                            <asp:DropDownList ID="s1stMOwnerT" TabIndex="10" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">Initial P&amp;I Payment</td>
                                        <td>
                                            <ml:MoneyTextBox ID="sProThisMPmt" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                        </td>
                                        <td class="FieldLabel" colspan="2">
                                            <asp:CheckBox ID="sIsMOrigBroker" TabIndex="10" runat="server" Text="Broker"></asp:CheckBox>
                                            <asp:CheckBox ID="sIsMOrigCorrespondent" TabIndex="10" runat="server" Text="Correspondent"></asp:CheckBox>
                                        </td>
                                        <td class="FieldLabel">Loan Amount</td>
                                        <td>
                                            <ml:MoneyTextBox ID="s1stMtgOrigLAmt" TabIndex="10" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">Initial Note Rate</td>
                                        <td>
                                            <ml:PercentTextBox ID="sNoteIR" TabIndex="10" runat="server" Width="70" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox>
                                        </td>
                                        <td class="FieldLabel" colspan="3">&nbsp;Broker/Correspondent Name and Company Name</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">Term</td>
                                        <td>
                                            <asp:TextBox ID="sTerm" TabIndex="10" runat="server" Width="60px" MaxLength="4" onchange="refreshCalculation();"></asp:TextBox>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TransmMOriginatorCompanyName" TabIndex="10" runat="server" Width="293px"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel"></td>
                                        <td></td>
                                        <td class="FieldLabel">Buydown:
                                            <asp:RadioButtonList ID="sBuydown" onclick="refreshCalculation();" TabIndex="10" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td class="FieldLabel">Buydown Terms</td>
                                        <td>
                                            <asp:TextBox ID="sTransmBuydwnTermDesc" TabIndex="10" runat="server" Width="100px"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="LoanFormHeader" style="padding-left: 5px">III. Underwriting Information</td>
                        </tr>
                        <tr>
                            <td>
                                <table id="Table17" cellspacing="0" cellpadding="0" width="98%" border="0">
                                    <tr>
                                        <td valign="top" class="no-wrap">
                                            <table class="InsetBorder" id="Table15" cellspacing="0" cellpadding="0" width="98%" border="0">
                                                <tr>
                                                    <td class="no-wrap">
                                                        <table id="Table3" cellspacing="0" cellpadding="0" border="0">
                                                            <tr>
                                                                <td class="FieldLabel">Underwriter's Name</td>
                                                                <td>
                                                                    <asp:TextBox ID="UnderwriterName" TabIndex="10" runat="server" Width="220px" MaxLength="56"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">Appraiser's Name</td>
                                                                <td>
                                                                    <asp:TextBox ID="AppraiserName" TabIndex="10" runat="server" Width="220px" MaxLength="56"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel no-wrap">Appraiser's License Number</td>
                                                                <td>
                                                                    <asp:TextBox ID="AppraiserLicense" TabIndex="10" runat="server" Width="220px" MaxLength="21"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">Appraisal Company Name</td>
                                                                <td>
                                                                    <asp:TextBox ID="AppraiserCompanyName" TabIndex="10" runat="server" Width="220px" MaxLength="21"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table class="InsetBorder" id="Table19" cellspacing="0" cellpadding="0" width="98%" border="0">
                                                <tr>
                                                    <td>
                                                        <table id="Table13" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td style="width: 91px"></td>
                                                                <td class="FieldLabel">Borrower</td>
                                                                <td class="FieldLabel">Co-borrower</td>
                                                                <td class="FieldLabel">Total</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel" style="width: 91px">Base Income</td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="aBBaseI" TabIndex="10" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                                </td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="aCBaseI" TabIndex="10" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                                </td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="aTotBaseI" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel" style="width: 91px">Other Income</td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="aBNonbaseI" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                                                </td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="aCNonbaseI" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                                                </td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="aTotNonbaseI" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel" style="width: 91px">
                                                                    <asp:TextBox ID="aOIFrom1008Desc" TabIndex="10" runat="server" ReadOnly="False"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="aBOIFrom1008" TabIndex="10" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                                </td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="aCOIFrom1008" TabIndex="10" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                                </td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="aTotOIFrom1008" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel" style="width: 91px"><a tabindex="-1" href="javascript:linkMe('../PropertyInfo.aspx?pg=2');" title="Go to Rental Income">Positive 
                                                                            Cash Flow&nbsp;(Subject Property)</a></td>
                                                                <td valign="top">
                                                                    <ml:MoneyTextBox ID="aBSpPosCf" TabIndex="10" runat="server" Width="90" preset="money"></ml:MoneyTextBox>
                                                                </td>
                                                                <td valign="top">
                                                                    <ml:MoneyTextBox ID="aCSpPosCf" TabIndex="10" runat="server" Width="90" preset="money"></ml:MoneyTextBox>
                                                                </td>
                                                                <td valign="top">
                                                                    <ml:MoneyTextBox ID="aTotSpPosCf" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel" style="width: 91px">Total Income</td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="aTransmBTotI" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                                                </td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="aTransmCTotI" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                                                </td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="aTransmTotI" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table class="InsetBorder" id="Table14" cellspacing="0" cellpadding="0" width="98%" border="0">
                                                <tr>
                                                    <td class="FieldLabel" colspan="2">Qualifying Ratios</td>
                                                </tr>
                                                <tr title="Primary Housing Expense/Income">
                                                    <td class="FieldLabel">Housing Ratio</td>
                                                    <td>
                                                        <ml:PercentTextBox ID="aQualTopR" runat="server" Width="70" preset="percent" ReadOnly="True"></ml:PercentTextBox>
                                                    </td>
                                                </tr>
                                                <tr title="Total Obligations/Income">
                                                    <td class="FieldLabel">Total Debt Ratio</td>
                                                    <td>
                                                        <ml:PercentTextBox ID="aQualBottomR" runat="server" Width="70" preset="percent" ReadOnly="True"></ml:PercentTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Debt-to-Housing Gap Ratio (Freddie)<asp:CheckBox ID="sDebtToHousingGapRLckd" onclick="refreshCalculation();" TabIndex="10" runat="server" Text="Lock"></asp:CheckBox></td>
                                                    <td>
                                                        <ml:PercentTextBox ID="sDebtToHousingGapR" TabIndex="10" runat="server" Width="70" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table cellspacing="0" cellpadding="0" width="98%" border="0">
                                                <tr>
                                                    <td>
                                                        <table class="InsetBorder" id="QualifyingRateTable" cellspacing="5px" cellpadding="" width="98%" border="0">
                                                            <tr>
                                                                <td class="FieldLabel no-wrap">Qualifying Rate</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 94px">
                                                                    <ml:PercentTextBox ID="sQualIR" TabIndex="10" runat="server" Width="70" preset="percent" ReadOnly="True"></ml:PercentTextBox></td>
                                                                <td style="width: 94px">
                                                                    <asp:DropDownList ID="sQualIRDeriveT" TabIndex="10" runat="server"></asp:DropDownList></td>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                        <table class="InsetBorder" id="EscrowTITable" cellspacing="5px" cellpadding="5" width="98%" border="0">
                                                            <tr>
                                                                <td class="FieldLabel no-wrap">Escrow (T&amp;I):&nbsp;
                                                                    <asp:RadioButtonList ID="sWillEscrowBeWaived" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" TabIndex="10">
                                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <table id="Table25" cellspacing="0" cellpadding="0" width="98%" border="0">
                                                            <tr>
                                                                <td valign="top" class="no-wrap">
                                                                    <table class="InsetBorder" id="Table23" cellspacing="0" cellpadding="0" border="0" width="98%">
                                                                        <tr>
                                                                            <td>
                                                                                <table id="Table24" cellspacing="0" cellpadding="0" border="0" width="98%">
                                                                                    <tr>
                                                                                        <td class="FieldLabel no-wrap">Loan-to-Value Ratios</td>
                                                                                        <td class="no-wrap"></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="FieldLabel no-wrap">LTV</td>
                                                                                        <td class="no-wrap">
                                                                                            <ml:PercentTextBox ID="sLtvR" runat="server" Width="70" preset="percent" ReadOnly="True"></ml:PercentTextBox></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="FieldLabel no-wrap">CLTV/TLTV</td>
                                                                                        <td class="no-wrap">
                                                                                            <ml:PercentTextBox ID="sCltvR" runat="server" Width="70" preset="percent" ReadOnly="True"></ml:PercentTextBox></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="FieldLabel no-wrap">HCLTV/HTLTV<asp:CheckBox ID="sHcltvRLckd" onclick="refreshCalculation();" TabIndex="10" runat="server" Text="Lock"></asp:CheckBox></td>
                                                                                        <td class="no-wrap">
                                                                                            <ml:PercentTextBox ID="sHcltvR" TabIndex="10" runat="server" Width="70" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>

                                            <table class="InsetBorder" id="QualTermTable" cellspacing="0" cellpadding="0" width="98%" border="0">
                                                <tr>
                                                    <td class="FieldLabel">
                                                        Qualifying Term
                                                        <asp:DropDownList id="sQualTermCalculationType" runat="server" onchange="refreshCalculation();" CssClass="sQualTermCalculationType" />
                                                        &nbsp
                                                        <input type="text" id="sQualTerm" runat="server" preset="numeric" onchange="refreshCalculation();" class="sQualTerm" />
                                                    </td>
                                                </tr>
                                            </table>

                                            <table class="InsetBorder" id="EscrowDetailsTable" cellspacing="0" cellpadding="0" width="98%" border="0">
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Community Lending/Affordable Housing Initiative:
                                                        <asp:RadioButtonList ID="sIsCommLen" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" TabIndex="10">
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Home Buyers/Homeownership Education Certificate in file:
                                                        <asp:RadioButtonList ID="sIsHOwnershipEdCertInFile" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" TabIndex="10">
                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="FormDatesTable" cellpadding="0" cellspacing="5" border="0" class="InsetBorder" width="98%">
                                                <tr>
                                                    <td class="no-wrap FieldLabel">Approved Date
                                                            <ml:DateTextBox ID="sApprovD" TabIndex="10" runat="server" Width="75" preset="date"></ml:DateTextBox>
                                                    </td>
                                                    <td class="FieldLabel">Underwriting Date
                                                            <ml:DateTextBox ID="sUnderwritingD" TabIndex="10" runat="server" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top" class="no-wrap">
                                            <table class="InsetBorder" id="Table18" cellspacing="0" cellpadding="0" border="0">
                                                <tr>
                                                    <td>
                                                        <table id="Table4" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="FieldLabel"><a href="javascript:linkMe('../BorrowerInfo.aspx?pg=7')">Present Housing Payment</a></td>
                                                                    <td class="FieldLabel">
                                                                        <ml:MoneyTextBox ID="aPresTotHExp" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel" colspan="2">Proposed Monthly Payments</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel Underline" colspan="2">
                                                                        <a id="BPRAnchor" href="javascript:hrefBorrowPrimaryResidence()">Borrower's Primary Residence</a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel">1st&nbsp;Mortgage P&amp;I</td>
                                                                    <td>
                                                                        <ml:MoneyTextBox ID="aTransmPro1stM" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel">2nd Mortgage P&amp;I</td>
                                                                    <td>
                                                                        <ml:MoneyTextBox ID="aTransmProOFin" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel">Hazard Insurance</td>
                                                                    <td>
                                                                        <ml:MoneyTextBox ID="aTransmProHazIns" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel">Taxes</td>
                                                                    <td>
                                                                        <ml:MoneyTextBox ID="aTransmProRealETx" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel">Mortgage Insurance</td>
                                                                    <td>
                                                                        <ml:MoneyTextBox ID="aTransmProMIns" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel">Homeowners Assoc. Fees</td>
                                                                    <td>
                                                                        <ml:MoneyTextBox ID="aTransmProHoAssocDues" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel">Other</td>
                                                                    <td>
                                                                        <ml:MoneyTextBox ID="aTransmOtherProHExp" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel">Total Primary Housing Expense</td>
                                                                    <td>
                                                                        <ml:MoneyTextBox ID="aTransmProTotHExp" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel Underline">Other Obligations</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel">Negative Cashflow</td>
                                                                    <td class="FieldLabel"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel" align="right">
                                                                        <a tabindex="-1" href="javascript:linkMe('../PropertyInfo.aspx?pg=2');" title="Go to Rental Income">Subject Property</a>&nbsp;<asp:CheckBox ID="aSpNegCfLckd" TabIndex="10" runat="server" onclick="refreshCalculation();"></asp:CheckBox>
                                                                    </td>
                                                                    <td>
                                                                        <ml:MoneyTextBox ID="aSpNegCf" TabIndex="10" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel" align="right">
                                                                        <a title="Go to Real Estate Schedule" tabindex="-1" onclick="redirectToUladPage('REO');">Other Properties</a>
                                                                        &nbsp;
                                                                        <asp:CheckBox ID="aOpNegCfLckd" TabIndex="10" runat="server" onclick="refreshCalculation();"></asp:CheckBox>
                                                                    </td>
                                                                    <td>
                                                                        <ml:MoneyTextBox ID="aOpNegCf" TabIndex="10" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel" align="left"><a tabindex="-1" onclick="redirectToUladPage('Liabilities');">Non-mortgage Liabilities</a>
                                                                    </td>
                                                                    <td>
                                                                        <ml:MoneyTextBox ID="aTransmOMonPmt" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel">Total All Monthly Payments
                                                                    </td>
                                                                    <td>
                                                                        <ml:MoneyTextBox ID="aTransmTotMonPmt" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel">Borrower Funds to Close
                                                                    </td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel">Required<asp:CheckBox ID="sTransmFntcLckd" onclick="refreshCalculation();" TabIndex="10" runat="server" Text="Lock"></asp:CheckBox>
                                                                    </td>
                                                                    <td>
                                                                        <ml:MoneyTextBox ID="sTransmFntc" TabIndex="10" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel no-wrap">Verified Assets&nbsp;&nbsp;
                                                                        <input style="width: 80px" onclick="f_populateFromAsset();" type="button" value="From Assets" nohighlight>&nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <ml:MoneyTextBox ID="sVerifAssetAmt" TabIndex="10" runat="server" Width="90" preset="money"></ml:MoneyTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel">Source of Funds
                                                                    </td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel" colspan="2">
                                                                        <asp:TextBox ID="sFntcSrc" TabIndex="10" runat="server" Width="244px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel">No. of Months Reserves</td>
                                                                    <td>
                                                                        <asp:TextBox ID="sRsrvMonNumDesc" TabIndex="10" runat="server" Width="59px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabel">Interested Party Contributions</td>
                                                                    <td>
                                                                        <ml:PercentTextBox ID="sInterestedPartyContribR" TabIndex="10" runat="server" Width="70" preset="percent">
                                                                        </ml:PercentTextBox>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table class="InsetBorder" id="AppraisalTable" cellspacing="0" cellpadding="0" border="0" width="99%">
                                                <tbody runat="server" id="LoanVersion12Section">
                                                    <tr>
                                                        <td class="FieldLabel no-wrap">Property valuation method</td>
                                                        <td colspan="3">
                                                            <asp:DropDownList ID="sSpValuationMethodT" runat="server" onchange="onValuationMethodChanged();"></asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel no-wrap">Appraisal Form Type</td>
                                                        <td colspan="3">
                                                            <asp:DropDownList ID="sSpAppraisalFormT" runat="server" onchange="onAppraisalFormChanged();"></asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Level of Property Review</td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">
                                                        <asp:CheckBox ID="sApprFull" TabIndex="10" runat="server" Text="Interior/Exterior (Full)" onclick="onAppraisalTypeChanged(this)"></asp:CheckBox>
                                                    </td>
                                                    <td class="FieldLabel no-wrap">
                                                        <asp:CheckBox ID="sApprDriveBy" TabIndex="10" runat="server" Text="Exterior Only" onclick="onAppraisalTypeChanged(this)"></asp:CheckBox>
                                                    </td>
                                                    <td class="FieldLabel no-wrap">
                                                        <asp:CheckBox ID="sIsSpReviewNoAppr" TabIndex="10" runat="server" Text="No Appraisal" onclick="onAppraisalTypeChanged(this)"></asp:CheckBox>
                                                    </td>
                                                    <td class="FieldLabel no-wrap">Form Number:
                                                <asp:TextBox ID="sSpReviewFormNum" TabIndex="10" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="width: 465px" valign="top" colspan="2">
                                            <table class="InsetBorder" id="RiskAssessmentTable" cellspacing="0" cellpadding="0" border="0">
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Risk Assessment</td>
                                                    <td class="no-wrap"></td>
                                                    <td class="no-wrap" width="10"></td>
                                                    <td class="FieldLabel no-wrap">DU Case ID</td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="sDuCaseId" TabIndex="10" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap" colspan="2">
                                                        <asp:CheckBox ID="sIsManualUw" TabIndex="10" runat="server" Text="Manual Underwriting"></asp:CheckBox></td>
                                                    <td class="no-wrap"></td>
                                                    <td class="FieldLabel no-wrap"><ml:EncodedLiteral id="sLpAusKeyLabel" runat="server">AUS Key #</ml:EncodedLiteral></td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="sLpAusKey" TabIndex="10" runat="server"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap" colspan="2">
                                                        <asp:CheckBox ID="sIsDuUw" TabIndex="10" runat="server" Text="DU"></asp:CheckBox><asp:CheckBox ID="sIsLpUw" TabIndex="10" runat="server" Text="LP"></asp:CheckBox><asp:CheckBox ID="sIsOtherUw" TabIndex="10" runat="server" Text="Other"></asp:CheckBox><asp:TextBox ID="sOtherUwDesc" TabIndex="10" runat="server"></asp:TextBox></td>
                                                    <td class="no-wrap"></td>
                                                    <td class="FieldLabel no-wrap"><ml:EncodedLiteral id="sLpDocClassLabel" runat="server">LP Doc Class (Freddie)</ml:EncodedLiteral></td>
                                                    <td class="no-wrap">
                                                        <ml:ComboBox ID="sLpDocClass" TabIndex="10" runat="server"></ml:ComboBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">AUS Recommendation</td>
                                                    <td class="no-wrap" colspan="1">
                                                        <asp:TextBox ID="sAusRecommendation" TabIndex="10" runat="server"></asp:TextBox></td>
                                                    <td class="no-wrap"></td>
                                                    <td class="FieldLabel no-wrap" colspan="2">Representative Credit/Indicator Score
                                                <asp:TextBox ID="sRepCrScore" TabIndex="10" runat="server" Width="84px"></asp:TextBox>
                                                    </td>
                                            </table>
                                        </td>
                                        <td valign="top" width="100%"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Underwriter Comments</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="sTransmUwerComments" TabIndex="10" runat="server" Width="100%" MaxLength="1000" TextMode="MultiLine" Height="136px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="LoanFormHeader" style="padding-left: 5px">IV. Seller, Contract, and Contact Information</td>
                        </tr>
                        <tr>
                            <td>
                                <table id="Table9" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td class="FieldLabel no-wrap">Seller</td>
                                        <td class="FieldLabel no-wrap">
                                            <UC:CFM ID="CFM" runat="server" CompanyNameField="sLenNm" AgentNameField="sLenContactNm" StreetAddressField="sLenAddr" AgentTitleField="sLenContactTitle" CityField="sLenCity" StateField="sLenState" ZipField="sLenZip" PhoneField="sLenContactPhone"></UC:CFM>
                                        </td>
                                        <td class="no-wrap"></td>
                                        <td class="no-wrap"></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Seller Name</td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sLenNm" TabIndex="10" runat="server" Width="135px" MaxLength="56"></asp:TextBox></td>
                                        <td class="FieldLabel no-wrap">Contact Name</td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sLenContactNm" TabIndex="10" runat="server" Width="164px" MaxLength="21"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Seller Address</td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sLenAddr" TabIndex="10" runat="server" Width="241px" MaxLength="36"></asp:TextBox></td>
                                        <td class="FieldLabel no-wrap">Contact Title</td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sLenContactTitle" TabIndex="10" runat="server" Width="135px" MaxLength="21"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="no-wrap"></td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sLenCity" TabIndex="10" runat="server" Width="150px" MaxLength="36"></asp:TextBox><ml:StateDropDownList ID="sLenState" TabIndex="10" runat="server"></ml:StateDropDownList>
                                            <ml:ZipcodeTextBox ID="sLenZip" TabIndex="10" runat="server" Width="50" preset="zipcode" CssClass="mask"></ml:ZipcodeTextBox></td>
                                        <td class="FieldLabel no-wrap">Contact Phone</td>
                                        <td class="no-wrap">
                                            <ml:PhoneTextBox ID="sLenContactPhone" TabIndex="10" runat="server" preset="phone" Width="88px" MaxLength="21"></ml:PhoneTextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Seller No.</td>
                                        <td class="FieldLabel no-wrap">
                                            <asp:TextBox ID="sLenNum" TabIndex="10" runat="server" Width="108px" MaxLength="21"></asp:TextBox></td>
                                        <td class="FieldLabel no-wrap">Date</td>
                                        <td class="no-wrap">
                                            <ml:DateTextBox ID="sLenContractD" TabIndex="10" runat="server" Width="75" preset="date"></ml:DateTextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Seller Loan No.&nbsp<asp:CheckBox ID="sLenLNumLckd" Text="Lock" runat="server" onclick="refreshCalculation();" /></td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sLenLNum" TabIndex="10" runat="server" Width="212px" MaxLength="21"></asp:TextBox></td>
                                        <td class="no-wrap"></td>
                                        <td class="no-wrap"></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Investor Loan No.&nbsp;
                                    <asp:CheckBox ID="sInvestLNumLckd" Text="Lock" runat="server" onclick="refreshCalculation();" />&nbsp;
                                        </td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sInvestLNum" TabIndex="10" runat="server" Width="141px" MaxLength="21"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Master Commitment No.</td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sCommitNum" TabIndex="10" runat="server" Width="149px" MaxLength="21"></asp:TextBox></td>
                                        <td class="no-wrap"></td>
                                        <td class="no-wrap"></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Contract No.</td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sContractNum" TabIndex="10" runat="server" Width="152px" MaxLength="21"></asp:TextBox></td>
                                        <td class="no-wrap"></td>
                                        <td class="no-wrap"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!-- Stores the saved occupancy status value -->
                    <input type="hidden" id="savedOccupency" value=<%=AspxTools.HtmlAttribute(aOccT.SelectedItem.Text.ToLower() + '/')%>>
                </td>
            </tr>
        </table>
        <uc1:cModalDlg ID="CModalDlg1" runat="server"></uc1:cModalDlg>
    </form>
    <script language="javascript" type="text/javascript" src="Transmittal_04.js"></script>
</body>
</html>
