using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
	public partial class FloridaDisclosureEdit : BaseLoanPage
	{


        protected void PageInit(object sender, System.EventArgs e) 
        {
            RepZipcode.SmartZipcode(RepCity, RepState);
            this.PageTitle = "Florida Lender Disclosure";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CFloridaLenderDisclosurePDF);
        }
        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FloridaDisclosureEdit));
            dataLoan.InitLoad();
            sFileVersion = dataLoan.sFileVersion;

            sIsCorrespondentLender.Checked = dataLoan.sIsCorrespondentLender;
            sIsLicensedLender.Checked = dataLoan.sIsLicensedLender;
            sIsOtherTypeLender.Checked = dataLoan.sIsOtherTypeLender;
            sOtherTypeLenderDesc.Text = dataLoan.sOtherTypeLenderDesc;
            sCommitmentEstimateDays.Text = dataLoan.sCommitmentEstimateDays;
            string value = dataLoan.sRefundabilityOfFeeToLenderT.ToString("D");

            foreach (ListItem item in sRefundabilityOfFeeToLenderT.Items) 
            {
                if (item.Value == value) item.Selected = true;
            }

			IPreparerFields lenderRep = dataLoan.GetPreparerOfForm(E_PreparerFormT.FloridaLenderDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (lenderRep.IsValid) 
            {
				LenderName.Text = lenderRep.CompanyName;
                RepName.Text = lenderRep.PreparerName;
                RepAddr.Text = lenderRep.StreetAddr;
                RepCity.Text = lenderRep.City;
                RepState.Value = lenderRep.State;
                RepZipcode.Text = lenderRep.Zip;
            }

        }

        protected override void SaveData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FloridaDisclosureEdit));
            dataLoan.InitSave(sFileVersion);



            dataLoan.sIsCorrespondentLender = sIsCorrespondentLender.Checked ;
            dataLoan.sIsLicensedLender = sIsLicensedLender.Checked ;
            dataLoan.sIsOtherTypeLender = sIsOtherTypeLender.Checked ;
            dataLoan.sOtherTypeLenderDesc = sOtherTypeLenderDesc.Text ;
            dataLoan.sCommitmentEstimateDays = sCommitmentEstimateDays.Text ;
            if (null != sRefundabilityOfFeeToLenderT.SelectedItem)
                dataLoan.sRefundabilityOfFeeToLenderT = (E_sRefundabilityOfFeeToLenderT) int.Parse(sRefundabilityOfFeeToLenderT.SelectedItem.Value) ;

			IPreparerFields lender = dataLoan.GetPreparerOfForm( E_PreparerFormT.FloridaLenderDisclosure, E_ReturnOptionIfNotExist.CreateNew);
			lender.CompanyName = LenderName.Text ;
			lender.PreparerName = RepName.Text ;
            lender.StreetAddr = RepAddr.Text ;
            lender.City = RepCity.Text ;
            lender.State = RepState.Value ;
            lender.Zip = RepZipcode.Text ;
            lender.Update();

            dataLoan.Save();
        }


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
