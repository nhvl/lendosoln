﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.ObjLib.Rolodex;
using System.Web.Services;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class GoodbyeLetter : BaseLoanPage
    {
        protected override void LoadData()
        {
            BindInvestorNames();

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(GoodbyeLetter));
            dataLoan.InitLoad();

            // General
            sGLServTransEffD.Text = dataLoan.sGLServTransEffD_rep;
            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.GoodbyeLetter, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            sGLCompletedBy.Text = String.IsNullOrEmpty(f.PreparerName) ? String.Format("{0} {1}", PrincipalFactory.CurrentPrincipal.FirstName,
                PrincipalFactory.CurrentPrincipal.LastName) : f.PreparerName;
            sGLCompletedByBrokerName.Value = Broker.Name;

            // Previous Servicer
            sGLPrevServNm.Text = dataLoan.sGLPrevServNm;
            sGLPrevServAddr.Text = dataLoan.sGLPrevServAddr;
            sGLPrevServCity.Text = dataLoan.sGLPrevServCity;
            sGLPrevServState.Value = dataLoan.sGLPrevServState;
            sGLPrevServZip.Text = dataLoan.sGLPrevServZip;
            sGLPrevServTFPhone.Text = dataLoan.sGLPrevServTFPhone;
            sGLPrevServContactNm.Text = dataLoan.sGLPrevServContactNm;
            sGLPrevServContactNum.Text = dataLoan.sGLPrevServContactNum;
            sGLPrevServBusHrAM.Text = dataLoan.sGLPrevServBusHrAM;
            sGLPrevServBusHrPM.Text = dataLoan.sGLPrevServBusHrPM;

            // New Servicer
            sGLNewServNm.Text = dataLoan.sGLNewServNm;
            sGLNewServAddr.Text = dataLoan.sGLNewServAddr;
            sGLNewServCity.Text = dataLoan.sGLNewServCity;
            sGLNewServState.Value = dataLoan.sGLNewServState;
            sGLNewServZip.Text = dataLoan.sGLNewServZip;
            sGLNewServTFPhone.Text = dataLoan.sGLNewServTFPhone;
            sGLNewServContactNm.Text = dataLoan.sGLNewServContactNm;
            sGLNewServContactNum.Text = dataLoan.sGLNewServContactNum;
            sGLNewServBusHrAM.Text = dataLoan.sGLNewServBusHrAM;
            sGLNewServBusHrPM.Text = dataLoan.sGLNewServBusHrPM;
            sGLNewServAccPmtD.Text = dataLoan.sGLNewServAccPmtD_rep;
            sGLNewServPmtAddr.Text = dataLoan.sGLNewServPmtAddr;
            sGLNewServPmtCity.Text = dataLoan.sGLNewServPmtCity;
            sGLNewServPmtState.Value = dataLoan.sGLNewServPmtState;
            sGLNewServPmtZip.Text = dataLoan.sGLNewServPmtZip;
            sGLNewServFirstPmtSchedD.Text = dataLoan.sGLNewServFirstPmtSchedD_rep;
        }

        // Initialize the combobox for investor names. In order to populate the fields based on name
        // we need a map. Names in the investor rolodex are supposed to be unique, so supply a name
        // -> id map that will be used to retrieve the investor information.
        private void BindInvestorNames()
        {
            List<InvestorRolodexEntry> entries = InvestorRolodexEntry.GetAll(PrincipalFactory.CurrentPrincipal.BrokerId, null).ToList();

            var nameToIdMap = new Dictionary<string, string>();
            foreach (var entry in entries.Where(p => p.Status == E_InvestorStatus.Active))
            {
                sGLNewServNm.Items.Add(entry.InvestorName);
                if (!nameToIdMap.ContainsKey(entry.InvestorName))
                {
                    nameToIdMap.Add(entry.InvestorName, entry.Id.ToString());
                }
            }

            var serializedMap = ObsoleteSerializationHelper.JavascriptJsonSerialize(nameToIdMap);
            ClientScript.RegisterStartupScript(typeof(GoodbyeLetter), "investorNameMap", "var investorNameMap = " + serializedMap, true);
        }

        [WebMethod]
        public static object GetInvestorInfo(int id)
        {
            return InvestorRolodexEntry.Get(PrincipalFactory.CurrentPrincipal.BrokerId, id);
        }

        // Get the next payment date after the acceptingD parameter. Use logic similar to 
        // what is used on the servicing page.
        [WebMethod]
        public static string GetSchedDateOfFirstPmt(string loanId, string acceptingD)
        {
            Guid sLId = new Guid(loanId);
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(GoodbyeLetter));
            dataLoan.InitLoad();

            var pmts = dataLoan.sServicingPayments;
            CDateTime accCD = CDateTime.Create(acceptingD, null);
            if (!accCD.IsValid)
            {
                return "";
            }

            DateTime accD = accCD.DateTimeForComputation;
            DateTime lastDueD = DateTime.MinValue;
            CDateTime nextDueD = CDateTime.InvalidWrapValue;

            // Find the first payment with a date >= the accepting date
            foreach (var pmt in pmts)
            {
                if (pmt.ServicingTransactionT.ToLower() == CPageBase.sServicingTransT_map[1].ToLower())
                {
                    lastDueD = pmt.DueD;

                    if (accD <= pmt.DueD)
                    {
                        nextDueD = CDateTime.Create(pmt.DueD);
                        break;
                    }
                }
            }

            // If no valid date was found, increment the last due date
            if (!nextDueD.IsValid)
            {
                // If there were no payments, set the last due date to the first
                // scheduled due date.
                if (pmts.Count == 0 && dataLoan.sSchedDueD1.IsValid)
                {
                    lastDueD = dataLoan.sSchedDueD1.DateTimeForComputation;
                }

                if (lastDueD != DateTime.MinValue)
                {
                    while (accD > lastDueD)
                    {
                        lastDueD = lastDueD.AddMonths(1);
                    }
                    nextDueD = CDateTime.Create(lastDueD);
                }
            }

            return nextDueD.ToString(dataLoan.m_convertLos);
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Goodbye Letter";
            this.PageID = "GoodbyeLetter";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CGoodbyeLetterPDF); // Only if this page has a pdf printing class.
            this.RegisterJsScript("LQBPopup.js");
            
            sGLPrevServZip.SmartZipcode(sGLPrevServCity, sGLPrevServState);
            sGLNewServZip.SmartZipcode(sGLNewServCity, sGLNewServState);
            sGLNewServPmtZip.SmartZipcode(sGLNewServPmtCity, sGLNewServPmtState);
        }

        protected override void OnInit(EventArgs e)
        {
            EnableJqueryMigrate = false;
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
