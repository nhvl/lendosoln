namespace LendersOfficeApp.newlos.Forms
{
    using System;
    using DataAccess;
    using LendersOffice.Migration;

    public partial class Transmittal_04_Combined : BaseLoanPage
    {
        #region Protected member variables

        protected System.Web.UI.WebControls.DropDownList sSpT;
        protected System.Web.UI.WebControls.TextBox sOLPurposeDesc;
        protected MeridianLink.CommonControls.DateTextBox sNoteD;
        protected System.Web.UI.WebControls.DropDownList sSpecCode2_dd;
        #endregion


        private void Bind_sLpDocClass(MeridianLink.CommonControls.ComboBox cb) 
        {
            cb.Items.Add("Accept Plus");
            cb.Items.Add("Streamlined Accept");
            cb.Items.Add("Standard");
            cb.Items.Add("Accept");
            cb.Items.Add("Refer");
        }
        protected void PageInit(object sender, System.EventArgs e)
        {
            sLenZip.SmartZipcode(sLenCity, sLenState);
            sSpZip.SmartZipcode(sSpCity, sSpState);
            Tools.Bind_sLPurposeT(sLPurposeT);
            Tools.Bind_sLT(sLT);
            Tools.Bind_sLienPosT(sLienPosT);
            Tools.Bind_sFinMethT(sFinMethT);
            Tools.Bind_aOccT(aOccT);
            Tools.Bind_sGseSpT(sGseSpT);
            Tools.Bind_sEstateHeldT(sEstateHeldT);
            Tools.Bind_sGseRefPurposeT(sGseRefPurposeT);
            Tools.Bind_sMOrigT(sMOrigT);
            Tools.Bind_s1stMOwnerT(s1stMOwnerT);
            Tools.Bind_sQualIRDeriveT(sQualIRDeriveT);
            Tools.Bind_sSpProjectClassFannieT(sSpProjectClassFannieT);
            Tools.Bind_sSpProjectClassFreddieT(sSpProjectClassFreddieT);
            Tools.Bind_sSpValuationMethodT(sSpValuationMethodT);
            Tools.Bind_sSpAppraisalFormT(sSpAppraisalFormT);
            Tools.Bind_QualTermCalculationType(sQualTermCalculationType);
            Bind_sLpDocClass(sLpDocClass);

            this.PageTitle = "1008 Combined";
            this.PageID = "1008_04Combined";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CTransmittal_09CombinedPDF);
            RegisterJsScript("LQBPopup.js");
            EnableJqueryMigrate = false;
        }

        private void BindDataObject(CPageData dataLoan) 
        {
            CAppData dataApp = dataLoan.GetAppData( 0 );

            Tools.SetDropDownListValue(s1stMOwnerT, dataLoan.s1stMOwnerT );
            Tools.SetDropDownListValue(sMOrigT, dataLoan.sMOrigT );
            Tools.SetDropDownListValue(sQualIRDeriveT, dataLoan.sQualIRDeriveT );
            Tools.SetDropDownListValue(sGseSpT, dataLoan.sGseSpT);
            Tools.SetDropDownListValue(sEstateHeldT, dataLoan.sEstateHeldT);
            Tools.SetDropDownListValue(sGseRefPurposeT, dataLoan.sGseRefPurposeT);
            Tools.SetDropDownListValue(aOccT, dataApp.aOccT);
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            Tools.SetDropDownListValue(sLT, dataLoan.sLT);
            Tools.SetDropDownListValue(sLienPosT, dataLoan.sLienPosT);
            Tools.SetDropDownListValue(sSpProjectClassFannieT, dataLoan.sSpProjectClassFannieT);
            Tools.SetDropDownListValue(sSpProjectClassFreddieT, dataLoan.sSpProjectClassFreddieT);
            sCpmProjectId.Text = dataLoan.sCpmProjectId;
            sFinMethT.Enabled = ! ( dataLoan.sIsRateLocked || IsReadOnly );

            sBiweeklyPmt.Checked = dataLoan.sBiweeklyPmt;
            sBalloonPmt.Checked = dataLoan.sBalloonPmt;
            sBalloonPmt.Enabled = dataLoan.sGfeIsBalloonLckd;
            sPresLTotHExp.Text = dataLoan.sPresLTotHExp_rep;
            s1stMtgOrigLAmt.Text                  = dataLoan.s1stMtgOrigLAmt_rep;
            sApprVal.Text                         = dataLoan.sApprVal_rep;
            sBuydown.SelectedIndex                = dataLoan.sBuydown ? 0 : 1;
            sCltvR.Text                           = dataLoan.sCltvR_rep;
            sCombinedBorFirstNm.Text              = dataLoan.sCombinedBorFirstNm;
            sCombinedBorInfoLckd.Checked          = dataLoan.sCombinedBorInfoLckd;
            sCombinedBorLastNm.Text               = dataLoan.sCombinedBorLastNm;
            sCombinedBorMidNm.Text                = dataLoan.sCombinedBorMidNm;
            sCombinedBorSsn.Text                  = dataLoan.sCombinedBorSsn;
            sCombinedBorSuffix.Text               = dataLoan.sCombinedBorSuffix;
            sCombinedCoborFirstNm.Text            = dataLoan.sCombinedCoborFirstNm;
            sCombinedCoborLastNm.Text             = dataLoan.sCombinedCoborLastNm;
            sCombinedCoborMidNm.Text              = dataLoan.sCombinedCoborMidNm;
            sCombinedCoborSsn.Text                = dataLoan.sCombinedCoborSsn;
            sCombinedCoborSuffix.Text             = dataLoan.sCombinedCoborSuffix;
            sCombinedCoborUsing2ndAppBorr.Checked = dataLoan.sCombinedCoborUsing2ndAppBorr;
            sCommitNum.Text                       = dataLoan.sCommitNum;
            sContractNum.Text                     = dataLoan.sContractNum;
            sFinMethDesc.Text                     = dataLoan.sFinMethDesc;
            sFinalLAmt.Text                       = dataLoan.sFinalLAmt_rep;
            sInvestLNumLckd.Checked               = dataLoan.sInvestLNumLckd;
            sInvestLNum.Text                      = dataLoan.sInvestLNum;
            sLTotBaseI.Text                       = dataLoan.sLTotBaseI_rep;
            sLTotI.Text                           = dataLoan.sLTotI_rep;
            sLTotNonbaseI.Text                    = dataLoan.sLTotNonbaseI_rep;
            sLTotOIFrom1008.Text                  = dataLoan.sLTotOIFrom1008_rep;
            sLTotSpPosCf.Text                     = dataLoan.sLTotSpPosCf_rep;
            sLenAddr.Text                         = dataLoan.sLenAddr;
            sLenCity.Text                         = dataLoan.sLenCity;
            sLenContactNm.Text                    = dataLoan.sLenContactNm;
            sLenContactPhone.Text                 = dataLoan.sLenContactPhone;
            sLenContactTitle.Text                 = dataLoan.sLenContactTitle;
            sLenContractD.Text                    = dataLoan.sLenContractD_rep;
            sLenLNumLckd.Checked                      = dataLoan.sLenLNumLckd;
            sLenLNum.Text                         = dataLoan.sLenLNum;
            sLenNm.Text                           = dataLoan.sLenNm;
            sLenNum.Text                          = dataLoan.sLenNum;
            sLenState.Value                       = dataLoan.sLenState;
            sLenZip.Text                          = dataLoan.sLenZip;
            sLtvR.Text                            = dataLoan.sLtvR_rep;
            sNegCfLckd.Checked                    = dataLoan.sNegCfLckd;
            sNoteIR.Text                          = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sNprimAppsTotI.Text                   = dataLoan.sNprimAppsTotI_rep;
            sNprimAppsTotSpPosCf.Text             = dataLoan.sNprimAppsTotSpPosCf_rep;
            sNprimAppsTotBaseI.Text               = dataLoan.sNprimAppsTotBaseI_rep;
            sNprimAppsTotNonbaseI.Text            = dataLoan.sNprimAppsTotNonbaseI_rep;
            sNprimAppsTotOIFrom1008.Text          = dataLoan.sNprimAppsTotOIFrom1008_rep;
            sOIFrom1008Desc.Text                  = dataLoan.sOIFrom1008Desc;
            sOpNegCf.Text                         = dataLoan.sOpNegCf_rep;
            sPrimAppOIFrom1008.Text               = dataLoan.sPrimAppOIFrom1008_rep;
            sPrimAppTotBaseI.Text                 = dataLoan.sPrimAppTotBaseI_rep;
            sPrimAppTotI.Text                     = dataLoan.sPrimAppTotI_rep;
            sPrimAppTotNonbaseI.Text              = dataLoan.sPrimAppTotNonbaseI_rep;
            sPrimAppTotSpPosCf.Text               = dataLoan.sPrimAppTotSpPosCf_rep;
            sProThisMPmt.Text                     = dataLoan.sProThisMPmt_rep;
            sProjNm.Text                          = dataLoan.sProjNm;
            sPurchPrice.Text                      = dataLoan.sPurchPrice_rep;
            sQualBottomR.Text                     = dataLoan.sQualBottomR_rep;
            sQualIR.Text                          = dataLoan.sQualIR_rep;
            sQualTopR.Text                        = dataLoan.sQualTopR_rep;
            sSpAddr.Text                          = dataLoan.sSpAddr;
            sSpCity.Text                          = dataLoan.sSpCity;
            sSpNegCf.Text                         = dataLoan.sSpNegCf_rep;
            sSpState.Value                        = dataLoan.sSpState;
            sSpZip.Text                           = dataLoan.sSpZip;
            sSubFin.Text                          = dataLoan.sSubFin_rep;
            sTerm.Text                            = dataLoan.sTerm_rep;
            sTerm.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sTransmOMonPmt.Text                   = dataLoan.sTransmOMonPmt_rep;
            sTransmOtherProHExp.Text              = dataLoan.sTransmOtherProHExp_rep;
            sTransmPro1stMPmt.Text                = dataLoan.sTransmPro1stMPmt_rep;
            sTransmPro2ndMPmt.Text                = dataLoan.sTransmPro2ndMPmt_rep;
            sTransmProHazIns.Text                 = dataLoan.sTransmProHazIns_rep;
            sTransmProHoAssocDues.Text            = dataLoan.sTransmProHoAssocDues_rep;
            sTransmProMIns.Text                   = dataLoan.sTransmProMIns_rep;
            sTransmProRealETx.Text                = dataLoan.sTransmProRealETx_rep;
            sTransmProTotHExp.Text                = dataLoan.sTransmProTotHExp_rep;
            sTransmTotMonPmt.Text                 = dataLoan.sTransmTotMonPmt_rep;
            sTransmUwerComments.Text              = dataLoan.sTransmUwerComments;
            sUnitsNum.Text                        = dataLoan.sUnitsNum_rep;

            CAgentFields appraiser = dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser,E_ReturnOptionIfNotExist.ReturnEmptyObject);
            sApprComNm.Text    = appraiser.CompanyName;
            sApprerLicNum.Text = appraiser.LicenseNumOfAgent;
            sApprerNm.Text     = appraiser.AgentName;

            CAgentFields underwritter = dataLoan.GetAgentOfRole( E_AgentRoleT.Underwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject );
            sUwerNm.Text = underwritter.AgentName;

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.TransmMOriginator, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            TransmMOriginatorCompanyName.Text = preparer.CompanyName;


            // TODO: Validate These new fields.
            sHcltvR.Text = dataLoan.sHcltvR_rep;  
            sHcltvRLckd.Checked = dataLoan.sHcltvRLckd; 
            sDebtToHousingGapR.Text = dataLoan.sDebtToHousingGapR_rep;
            sDebtToHousingGapRLckd.Checked = dataLoan.sDebtToHousingGapRLckd; 
            sTransmFntc.Text = dataLoan.sTransmFntc_rep;
            sTransmFntcLckd.Checked = dataLoan.sTransmFntcLckd;
            sVerifAssetAmt.Text = dataLoan.sVerifAssetAmt_rep; 
            sFntcSrc.Text = dataLoan.sFntcSrc; 
            sRsrvMonNumDesc.Text = dataLoan.sRsrvMonNumDesc; 
            sInterestedPartyContribR.Text = dataLoan.sInterestedPartyContribR_rep; 
            sIsManualUw.Checked = dataLoan.sIsManualUw; 
            sIsDuUw.Checked = dataLoan.sIsDuUw; 
            sIsLpUw.Checked = dataLoan.sIsLpUw; 
            sIsOtherUw.Checked = dataLoan.sIsOtherUw; 
            sOtherUwDesc.Text = dataLoan.sOtherUwDesc; 
            sAusRecommendation.Text = dataLoan.sAusRecommendation; 
            sLpAusKey.Text = dataLoan.sLpAusKey; 
            sDuCaseId.Text = dataLoan.sDuCaseId; 
            sLpDocClass.Text = dataLoan.sLpDocClass;
            sRepCrScore.Text = dataLoan.sRepCrScore; 
            sIsMOrigBroker.Checked = dataLoan.sIsMOrigBroker; 
            sIsMOrigCorrespondent.Checked = dataLoan.sIsMOrigCorrespondent; 
            sTransmBuydwnTermDesc.Text = dataLoan.sTransmBuydwnTermDesc;
            sFinMethPrintAsOtherDesc.Text = dataLoan.sFinMethPrintAsOtherDesc;
            sFinMethodPrintAsOther.Checked = dataLoan.sFinMethodPrintAsOther;

            sIsCommLen.SelectedIndex = dataLoan.sIsCommLen ? 0 : 1; 
            sIsHOwnershipEdCertInFile.SelectedIndex = dataLoan.sIsHOwnershipEdCertInFile ? 0 : 1;
            sWillEscrowBeWaived.SelectedIndex = dataLoan.sWillEscrowBeWaived ? 1 : 0;

            sApprovD.Text = dataLoan.sApprovD_rep;
            sUnderwritingD.Text = dataLoan.sUnderwritingD_rep;

            Tools.SetDropDownListValue(sSpValuationMethodT, dataLoan.sSpValuationMethodT);
            Tools.SetDropDownListValue(sSpAppraisalFormT, dataLoan.sSpAppraisalFormT);
            sApprFull.Checked = dataLoan.sApprFull;
            sApprDriveBy.Checked = dataLoan.sApprDriveBy;
            sIsSpReviewNoAppr.Checked = dataLoan.sIsSpReviewNoAppr;
            sSpReviewFormNum.Text = dataLoan.sSpReviewFormNum;

            Tools.SetDropDownListValue(this.sQualTermCalculationType, dataLoan.sQualTermCalculationType);
            this.sQualTerm.Value = dataLoan.sQualTerm_rep;

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V12_CalculateLevelOfPropertyReviewSection))
            {
                if (dataLoan.sSpAppraisalFormT != E_sSpAppraisalFormT.Other)
                {
                    sSpReviewFormNum.ReadOnly = true;
                }
            }
            else
            {
                LoanVersion12Section.Visible = false;
            }
        }

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Transmittal_04_Combined));
            dataLoan.InitLoad();

            BindDataObject(dataLoan);

            sLpAusKeyLabel.Text = "LPA AUS Key #";
            sIsLpUw.Text = "LPA";
            sLpDocClassLabel.Text = "LPA Doc Class (Freddie)";
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            IsAppSpecific = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
            aOccT.Attributes.Add("onchange","javascript:onChangeOccupancyStatus()");
            aOccT.Attributes.Add("onfocus","javascript:updateOccupancyStatusSavedValue()");
            
        }
    }
}
