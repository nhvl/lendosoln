﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SellerClosingCosts.aspx.cs" Inherits="LendersOfficeApp.newlos.Forms.SellerClosingCosts" EnableViewState="false" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>

<!doctype html>
<html lang='en-GB'>
<head runat="server">
    <meta charset='utf-8'>
    <title>Non Borrower-responsible Closing Costs</title>
</head>
<body>
    <form runat="server">
    
    <div style="display: none;" id="dialog-confirm" title="Delete fee?">
        <p style="width: 100%">
        </p>
    </div>
    
    <div id='container'></div>
    
    <div id="ConfirmAgent" style="display: none;">Selecting this contact will add it to the official contact list.</div>
    <div id="ClearAgent" style="display:none;">This action will remove the contact from the official contact list.</div>
    
    <script id="ClosingCostTemplate" type="text/ractive">
        <div class='MainRightHeader'>Non Borrower-responsible Closing Costs</div>
        {{#SectionList:i}}  
         <table border="0" style="width: 1080px; white-space: nowrap; padding-left: 5px;" cellpadding="2" cellspacing="0" class="GfeTable" >
            <thead>
                <tr class="GridHeader">
                    <td colspan="17">
                      {{SectionName}}
                    </td>
                </tr>
                <tr class="FormTableHeader " >
                    <td>
                        HUD<br/>Line
                    </td>
                    <td>
                        Description/Memo 
                    </td>
                    <td>
                        GFE<br/>Box
                    </td>
                    <td>
                        APR
                    </td>
                    <td>
                        FHA
                    </td>
                    <td>
                        Paid to
                    </td>
                    {{#if CanReadDFLP}}
                        <th>DFLP</th>
                    {{/if}}
                    <td>
                        TP
                    </td>
                    <td>
                        AFF
                    </td>
                    <td>
                        Can<br/>Shop
                    </td>
                    <td>
                        Did<br/>Shop
                    </td>
                    <td></td>
                    <td>
                        Amount
                    </td>
                    <td>
                        Paid by
                    </td>
                    <td>
                        Payable
                    </td>
                    <td>
                        Date paid
                    </td>
                    <td>
                        Responsible Party
                    </td>
                    <td>
                    
                    </td>
                </tr>
            </thead>
            <tbody>
                {{#ClosingCostFeeList:j}}
                    {{>ClosingCostFeeTemplate}}
                {{/#ClosingCostFeeList}}
            </tbody>
            {{#if SectionName.indexOf("1000") == -1}}
            <tfooter>
                <tr class="GridFooter">
                    <td colspan="17">
                        <input type="button" value="Add" on-click="addFee:{{i}}"/>
                    </td>
                </tr>
            </tfooter>
            {{/if}}
               </table>
        {{/SectionList}}
        
        <div ID="OptionsContainer" >
            <asp:Repeater runat="server" ID="BeneficiaryRepeater">
                <HeaderTemplate>
                    <ul class="BeneficiaryOptions OptionsList" tabindex="1" on-focus="reselectInput" on-blur="hideDesc" on-mouseenter="onHoverDescriptions" on-mouseleave="onMouseOutDescriptions">
                </HeaderTemplate>
                <ItemTemplate>
                    <li on-mouseenter="highlightLI" on-mouseleave="unhighlightLI" on-click="selectDDL" val="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"> <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></li>
                </ItemTemplate>
                <FooterTemplate>
                </ul>
                </FooterTemplate>
              </asp:Repeater>
              
              <asp:Repeater runat="server" ID="CreditDescRepeater">
                <HeaderTemplate>
                    <ul class="CreditDescOptions OptionsList" tabindex="1" on-focus="reselectInput" on-blur="hideDesc" on-mouseenter="onHoverDescriptions" on-mouseleave="onMouseOutDescriptions">
                </HeaderTemplate>
                <ItemTemplate>
                    <li on-mouseenter="highlightLI" on-mouseleave="unhighlightLI" on-click="selectCombobox" val="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"> <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></li>
                </ItemTemplate>
                <FooterTemplate>
                </ul>
                </FooterTemplate>
              </asp:Repeater>
        </div>
    </script>

    <script id="TimeControl" type='text/ractive'>
      Time: <input class="TimeInput" type="text" id="{{id + ''}}" value="{{value_hour}}" readonly="{{readonly}}" on-blur="hourBlur" on-keyup="hourKeyUp" maxlength="2"/>
      : <input class="TimeInput" type="text" id="{{id + '_minute'}}" value="{{value_min}}" readonly="{{readonly}}" on-blur="minuteBlur" maxlength="2"/>
      <select NotEditable="true" id="{{id + '_am'}}"  value="{{value_am}}" disabled="{{readonly}}">
        <option value="AM">AM</option>
        <option value="PM">PM</option>
      </select>
    </script>

    <script id="DatePicker" type='text/ractive'>
        <input type="text" class="mask date" readonly="{{readonly}}"  preset="date" id="{{id}}" intro="initMask" on-blur="updatePicker"   value="{{value}}" />
        <a on-click="displayCalendar" href="#" on-click="showCalendar:{{id}}" >
            <img title="Open calendar" src="[[VRoot]]/images/pdate.gif" border="0">
        </a>
    </script>

    <script id='RactiveCombobox' type='text/ractive'>
        <div class="RactiveDDLContainer">
            <input on-keydown='keyScroll' field="{{Field}}" value="{{value}}" disabled='{{disabled}}' {{IsReadonly?"": "NotEditable"}} class="RactiveCombobox {{'RactiveCombobox'+i+j}} {{Prefix}}Combobox" type="text" on-blur="hideDesc: '{{Prefix}}Options'"  /><img class="DDLImg" src= "../../images/IEArrow.png" on-click="showOptions: {{i}},{{j}},{{k}}, '{{Prefix}}Options'" >
        </div>
    </script>

    <script id='RactiveDDL' type='text/ractive'>
        <div class="RactiveDDLContainer">
            <input on-keydown='keyScroll' disabled='{{disabled}}' {{IsReadonly?"": "NotEditable"}} class="{{disabled? "Disabled": ""}} RactiveDDL {{'RactiveDDL'+i+j}} {{Prefix}}DDL" readonly type="text" on-click="showOptions: {{i}},{{j}},{{k}},'{{Prefix}}Options'"  on-blur="hideDesc: '{{Prefix}}Options'"  /><img class="DDLImg" src= "../../images/IEArrow.png" on-click="showOptions: {{i}},{{j}},{{k}}, '{{Prefix}}Options'" >
            <input type="hidden" value="{{value}}" />
        </div>
    </script>

    <script id='ClosingCostFeeTemplate' type='text/ractive'>
        <tr class="{{ j % 2 == 0 ? "GridItem" : "GridAlternatingItem"}}">
            <td class="FieldLabel"> 
                <input type="text" value="{{hudline}}"  data-hud-start="{{HudLineStart}}" data-hud-end="{{HudLineEnd}}"  class="hudline" maxlength="4" style="width:25px;" readonly="{{is_system || sIsRequireFeesFromDropDown}}" />
            </td>
            <td>
                <input  type="text" value="{{desc}}" readonly="{{sIsRequireFeesFromDropDown}}" style="width: 160px;" />
                {{#if desc != org_desc}}
                    <div>Type: {{org_desc}} </div>
                {{/if}}
            </td>
            <td>
                <asp:Repeater runat="server" ID="SectionRepeater">
                    <HeaderTemplate>
                        <select NotEditable="true" disabled="{{sIsRequireFeesFromDropDown}}" intro="sectionIntro:{hudline: {{hudline}}, isSystem: {{is_system}}, gfeGroup: {{gfeGrps}} }" value="{{section}}">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                    </ItemTemplate>
                    <FooterTemplate>
                    </select>
                    </FooterTemplate>
                  </asp:Repeater>
            </td>
            <td>
                <input type="checkbox" checked="{{apr}}" disabled="{{sIsRequireFeesFromDropDown}}"/> 
            </td>
            <td>
               <input type="checkbox" checked="{{fha}}" disabled="{{sIsRequireFeesFromDropDown}}"/>
            </td>
            <td>
			    <ractiveDDL Prefix='Beneficiary' value = '{{bene}}' /><img class="ContactImg" on-mouseover="onContactHover" on-mouseout="onContactMouseOut" on-click="showAgentPicker : {{i}}, {{j}}" src="../../images/contacts.png">
			    {{#if bene_desc || bene_id != <%=AspxTools.JsString(Guid.Empty) %>}}
			        <div> Company: {{bene_desc}} </div>
			    {{/if}}
            </td>
            {{#if CanReadDFLP}}
                <td>
                    <input type="checkbox" disabled="{{!CanSetDFLP}}" checked="{{dflp}}" />
                </td>
            {{/if}}
            <td>
                <input type="checkbox" checked="{{tp}}" disabled = "{{!sIsManuallySetThirdPartyAffiliateProps && (bene_id != '<%=AspxTools.HtmlString(Guid.Empty) %>')}}" on-click="tpClick:{{i}},{{j}}" /> 
            </tD>
            <td>
               <input type="checkbox" checked="{{aff}}" disabled="{{!tp || (!sIsManuallySetThirdPartyAffiliateProps && (bene_id != '<%=AspxTools.HtmlString(Guid.Empty) %>'))}}" /> 
            </td>
            <td>
                <input type="checkbox" checked="{{can_shop}}" on-change="CanShopChange: {{i}}, {{j}}"  disabled="{{sIsRequireFeesFromDropDown || disc_sect == 2 || disc_sect == 3}}" /> 
            </td>
            <td>
                <input type="checkbox" checked="{{did_shop}}" disabled="{{!can_shop}}" />
            </td>
            <td>
                {{#if SectionName.indexOf("1000") == -1 && f.t !== 0 && f.t !== 4 && f.t !== 5 && f.t !== 14 && f.t !== 17 && f.t !== 18 && f.t !== 21}}
                    <input type="button" value="C" on-click="updateCalc:{{i}},{{j}}"/>
                {{/if}}
            </td>
            <td>
                {{#if f.t === 3}} 
                    <input type="text" value="{{f.base}}" class="mask money" preset="money" intro="initMask" on-blur="domblur" on-change="recalc"  />
                {{else}}
                    <input type="text" value="{{total}}" class="mask money"  readonly="{{f.t != 3}}" preset="money" intro="initMask" on-blur="domblur" />
                {{/if}}
            </td>
            <td>
                  {{#if (pmts && pmts.length == 1)}} 
                    <select value="{{pmts[0].paid_by}}" on-change="updatePaidBy:{{i}},{{j}},{{total}}">
                        <option value="2">seller</option>
                        {{#if typeid != <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId) %> }}
                            <option value="4">lender</option>
                        {{/if }}
                        <option value="5">broker</option>
                    </select>
                {{else}}
                &nbsp;
                {{/if}}
            </td>
            <td>
                {{#if (pmts && pmts.length == 1)}} 
                    <select disabled="{{pmts[0].paid_by == 3}}" NotEditable="true" value="{{pmts[0].pmt_at}}" on-change="recalc">
                        <option value="1">at closing</option>
                        <option value="2">outside of closing</option>                
                    </selecT>
                {{else}}
                &nbsp;
                {{/if}}
                
            </td>
            <td>
                {{#if pmts && pmts.length == 1}} 
                    <datePicker readonly="{{pmts[0].pmt_at == 1}}" value="{{pmts[0].pmt_dt}}" id="d{{i}}{{j}}" />
                {{else}}
                    &nbsp;
                {{/if}}
            </td>
            <td>
                  {{#if (pmts && pmts.length == 1)}}  
                     <select disabled="true" value="{{pmts[0].responsible}}" >
                        <option value="2">seller</option>
                        <option value="3">lender</option>
                        <option value="4">broker</option>
                     </select>
                  {{else}}
                  &nbsp;
                  {{/if}}
            </td>
            <td> 
                {{#if f.t !== 4 && f.t !== 5 && f.t !== 21 && SectionName.indexOf("1000") == -1  && (typeid !== <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId) %> || total == '$0.00')}}
                    {{#if f.t === 3}} 
                        <input type="button" value=" - " on-click="delete:{{i}},{{j}},{{desc}},{{f.base}},{{org_desc}}"/>
                    {{else}}
                        <input type="button" value=" - " on-click="delete:{{i}},{{j}},{{desc}},{{total}},{{org_desc}}"/>
                    {{/if}}
                {{/if}}
            </td>
        </tr>
        {{#if pmts.length > 1}} 
        {{#pmts:k}}
            {{>ClosingCostFeePaymentTemplate}}
        {{/pmts}}
        {{/if}}
    </script>

    <script id='ClosingCostFeePaymentTemplate' type='text/ractive'>
        <tr>
            <td colspan="10">
                &nbsp;
            </td>
            <td>
                {{#if (is_system)}}
                    <input type="button" value="+" on-click="AddSplitPayment:{{i}},{{j}}" />
                {{/if}}
            </td>
            <td>
                <input on-change="recalc" readonly="{{is_system}}" style="text-align:right" type="text" value="{{amt}}" class="mask money" preset="money"  on-blur="domblur" intro="initMask"/>
            </td>
            <td>
                <select value="{{paid_by}}" on-change="updatePaidBySplit:{{i}},{{j}},{{k}}" >
                        <option value="2">seller</option>
                        {{#if typeid != <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId) %> }}
                            <option value="4">lender</option>
                        {{/if }}
                        <option value="5">broker</option>
               </select>
            </td>
            <td>
                <select NotEditable="true" value="{{pmt_at}}" disabled="{{is_system || paid_by == 3}}" on-change="recalc">
                        <option value="1">at closing</option>
                        <option value="2">outside of closing</option>     
                </select>
            </td>
            <td>   
                <input type="text" class="mask date" preset="date" id="d{{i}}{{j}}{{k}}" intro="initMask" value="{{pmt_dt}}" on-blur="domblur" readonly="{{is_system || pmt_at == 1}}" />
                <a on-click="displayCalendar" href="#" on-click="showCalendar:d{{i}}{{j}}{{k}}" >
                <img title="Open calendar" src="[[VRoot]]/images/pdate.gif" border="0" />
                </a>
            </td>
            <td>
			    {{#if !is_system}}
                    <input type="button" value=" - " on-click="removePayment:{{i}},{{j}},{{k}},{{amt}},{{paid_by}},{{desc}},{{org_desc}}"/>            
			    {{/if}}
            </td>
        </tr>
    </script>

    <script id="CalculationModalContent" type="text/ractive">
      <p>
        {{#if fee.t == 0 || fee.t == 4 || fee.t == 5 || fee.t == 17 || fee.t == 18}} 
            Not Supported
        {{/if}}
           {{#if fee.t == 1 || fee.t == 2 || fee.t == 3}}
                <p>
                    <select value="{{fee.t}}">
                        <option value="3">Flat Amount</option>
                        <option value="1">Full</option>
                        <option value="2">Percent Only</option>
                    </select>
                </p>
            {{/if}}  
        {{#if fee.t == 1}}
        <label>Percent <input type="text" value="{{fee.p}}" preset="percent" intro="initMask" class="money mask"/> </label>  of  
        <asp:Repeater runat="server" ID="PercentRepeater">
                    <HeaderTemplate>
                        <select NotEditable="true" value="{{fee.pt}}" >
                    </HeaderTemplate>
                    <ItemTemplate>
                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                    </ItemTemplate>
                    <FooterTemplate>
                    </select>
                    </FooterTemplate>
                  </asp:Repeater>
                    + <input type="text" value="{{fee.base}}" preset="money" intro="initMask" class="money mask" /> 
        {{/if}}
        {{#if fee.t == 2}} 
            <label>Percent <input type="text" class="mask"  value="{{fee.p}}" preset="percent" intro="initMask" /> </label>  of  
            <asp:Repeater runat="server" ID="PercentRepeater2">
                    <HeaderTemplate>
                        <select NotEditable="true" value="{{fee.pt}}" >
                    </HeaderTemplate>
                    <ItemTemplate>
                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                    </ItemTemplate>
                    <FooterTemplate>
                    </select>
                    </FooterTemplate>
                  </asp:Repeater> 
        {{/if}}
        {{#if fee.t == 3}} 
            <input type="text" value="{{fee.base}}" preset="money" class="mask money"  intro="initMask" /> 
        {{/if}}
        {{#if fee.t == 6}}
            <input type="text" value="{{fee.period}}" intro="initMask" preset="numeric" style="width: 40px;" /> period
        {{/if}}
        {{#if fee.t == 7}}
            <input on-change="recalc" type="checkbox" checked="{{loan.sIPiaDyLckd}}"> Lock
            <input type="text" value="{{loan.sIPiaDy}}" readonly="{{!loan.sIPiaDyLckd}}" class="mask DateUnitInput" preset="numeric" intro="initMask" /> days @
            <input on-change="recalc" type="checkbox" checked="{{loan.sIPerDayLckd}}" /> Lock
            <input type="text" value="{{loan.sIPerDay}}" readonly="{{!loan.sIPerDayLckd}}" decimalDigits="6" class="mask money" preset="money" intro="initMask" /> per day
        {{/if}}
        {{#if fee.t == 8}}
            Not Supported
        {{/if}}
        {{#if fee.t == 9 || fee.t == 10}}
            <input type="text" value="{{fee.period}}" intro="initMask" preset="numeric" /> months @ <input type="text" value="{{fee.base}}" class="mask money" preset="money" intro="initMask" readonly="true" /> / month
        {{/if}}
        {{#if fee.t == 11}}
            <input type="text" value="{{fee.period}}" intro="initMask" preset="numeric" readonly="true" /> periods
        {{/if}}
        {{#if fee.t == 12 || fee.t == 13 || fee.t == 15 || fee.t == 16}}
            <input type="text" preset="numeric" value="{{fee.period}}" intro="initMask" /> months @ <input type="text" readonly="true" value="{{fee.base}}" class="mask money" preset="money" intro="initMask" /> / month
        {{/if}}
      </p>
      
      <p>
        <input type="button" value=" OK " on-click="close: true"/> <input type="button" value=" Cancel " on-click="close: false"/> 
      </p>
        </script>

    <script id="FeePickerModalContent" type="text/ractive">
        <h3>Available Fees for {{SectionName}}</h3>
        <p>
            <ul>
            {{#FeeListAsSellerFees:i}} 
                <li><a href="#" on-click="selectFee:{{i}}">{{desc}}</a></li>
            {{/FeeListAsSellerFees}}
            {{^FeeListAsSellerFees}}
                <li>No Available Fees</li>
            {{/FeeListAsSellerFees}}
            </ul>
            <input type="button" value=" Cancel " on-click="close"/> 
        </p>
    </script>

    <script id="modal" type="text/ractive">
        <div class='modal-background'><div class='modal-outer'><div class='modal'>{{>modalContent}}</div></div></div>
    </script>
    

    <script>
    
    var globalRActive;
    
     function checkHudLines(tbody) 
     {
        var h = {};
        
        tbody.find('input.hudline[readonly]').each(function(i,o) {
         var prop = 'n' + o.value;
            h[prop] = true;
        });
        
        tbody.find('input.hudline:not([readonly])').each(function(i,o) {
            var $o = $(this),
                i = parseInt(o.value),
                r = i >= parseInt($o.attr('data-hud-start')) && i <= parseInt($o.attr('data-hud-end')),
                td = $o.closest('td'),
                ti =$();
            
            td.find('img').remove();
            $o.toggleClass('invalid', !r);     
            
            if (!r){
                if (ti.length == 0) {
                    ti = $('<img/>', {
                        src : VRoot + '/images/fail.png',
                        align : 'bottom',
                        style : 'margin-left: 3px;'
                    }).appendTo(td);
                }
                
                ti.attr('title', 'You can only enter a HUD line number between ' + $o.attr('data-hud-start') + ' and ' + $o.attr('data-hud-end') +'.');
            }
               
            if (o.value == $(o).attr('data-hud-start')){
                return;
            }
            
            if (!r){
                return; 
            }
                
            
            var prop = 'n' + o.value;
            if (h.hasOwnProperty(prop)){
                $o.addClass('dupe');
                if (ti.length == 0) {
                    ti = $('<img/>', {
                        src : VRoot + '/images/fail.png',
                        align : 'bottom',
                        style : 'margin-left: 3px;'
                    }).appendTo(td);
                }
                var t = ti.attr('title');
                if (!t){
                    t = '';
                }
                ti.attr('title', t + ' You cannot enter duplicate HUD line numbers.');
                
            }
            else{ 
                $o.removeClass('dupe');
                h[prop] = true;
            }
        });
    }
        
    $(function(){
        
        $(document).on('change', 'input.hudline:not([readonly])', function(){
            checkHudLines($(this).closest('tbody'));
        });
        
        
        function lqblog(msg){
            if (window.console && window.console.log){
                window.console.log(msg);
            }
        }
        
        var initMask = function(o) {
            _initMask(o.node, true);
            o.complete();
        };
        
        var sectionTransition = function(t) {
            if (t.isIntro) {
                var node = $(t.node);
                var section = node.text();

                var selector = determineShownSections(t.params.hudline, t.params.isSystem, t.params.gfeGroup);
                node.find("option").not(selector).remove();

                var numVisible = node.find("option").length;
                if (numVisible < 2) {
                    //Pseudo disable it so it looks better
                    node.mousedown(function(event) { event.preventDefault(); });
                    node.css("background-color", "white");
                    node.css("color", "grey");
                }
                if (numVisible < 1) {
                    node.css('display', 'none');
                }
            }
        }

        function determineShownSections(hudline, isSystem, gfeGroup) {
            var selector = "";

            // Create the selectors for the custom fees first.
            if (!isSystem) {
                if (hudline >= 800 && hudline <= 899) {
                    selector = addContainsSelector(selector, "A1");
                    selector = addContainsSelector(selector, "B3");
                    selector = addContainsSelector(selector, "N/A");
                } else if (hudline >= 900 && hudline <= 999) {
                    selector = addContainsSelector(selector, "B3");
                    selector = addContainsSelector(selector, "B11");
                    selector = addContainsSelector(selector, "N/A");
                } else if (hudline >= 1000 && hudline <= 1099) {
                    selector = addContainsSelector(selector, "B9");
                } else if (hudline >= 1100 && hudline <= 1199) {
                    selector = addContainsSelector(selector, "B4");
                    selector = addContainsSelector(selector, "B6");
                    selector = addContainsSelector(selector, "N/A");
                } else if (hudline >= 1200 && hudline <= 1299) {
                    selector = addContainsSelector(selector, "B7");
                    selector = addContainsSelector(selector, "B8");
                } else if (hudline >= 1300 && hudline <= 1399) {
                    selector = addContainsSelector(selector, "B4");
                    selector = addContainsSelector(selector, "B6");
                    selector = addContainsSelector(selector, "N/A");
                }

                return selector;
            }

            if (gfeGroup === 0)
            {
                selector = addContainsSelector(selector, "N/A");
            }
            else if (gfeGroup === 1) {
                selector = addContainsSelector(selector, "A1");
            }
            else if (gfeGroup === 2) {
                selector = addContainsSelector(selector, "A2");
            }
            else if (gfeGroup === 3) {
                selector = addContainsSelector(selector, "B3");
            }
            else if (gfeGroup === 4) {
                selector = addContainsSelector(selector, "B4");
            }
            else if (gfeGroup === 5) {
                selector = addContainsSelector(selector, "B5");
            }
            else if (gfeGroup === 6) {
                selector = addContainsSelector(selector, "B6");
            }
            else if (gfeGroup === 7) {
                selector = addContainsSelector(selector, "B7");
            }
            else if (gfeGroup === 8) {
                selector = addContainsSelector(selector, "B8");
            }
            else if (gfeGroup === 9) {
                selector = addContainsSelector(selector, "B9");
            }
            else if (gfeGroup === 10) {
                selector = addContainsSelector(selector, "B10");
            }
            else if (gfeGroup === 11) {
                selector = addContainsSelector(selector, "B11");
            }
            else if (gfeGroup === 12) {
                selector = addContainsSelector(selector, "B4");
                selector = addContainsSelector(selector, "B6");
            }
            else {
                selector = addContainsSelector(selector, "N/A");
            }

            return selector;
        }

        function addContainsSelector(selector, data) {
            if (selector != "") {
                selector += ",";
            }

            selector += ":contains('" + data + "')";
            return selector;
        }
        
        Ractive.transitions.initMask = initMask;
        Ractive.transitions.sectionIntro = sectionTransition;

        $(document).on('change', 'input,select', updateDirtyBit);
        
        function highlightLI(el)
        {
            el.css("background-color", "yellow");
            el.attr("class", "selected");
        }
        
        function unhighlightLI(el)
        {
            el.css("background-color", "");
            el.removeAttr("class");
        }
        
        function callWebMethod(webMethodName, data, error, success, pageName) {
            if (!pageName){
                pageName = 'SellerClosingCosts.aspx/';
            }
            
            var settings = {
                async: false, 
                type: 'POST',
                url : pageName + webMethodName,
                data : JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                error : error,
                success: function (d){ checkWebError(d, success); }
            };

            callWebMethodAsync(settings);
        }
        
        var calculationModalContent = Ractive.parse(document.getElementById('CalculationModalContent').innerHTML),
            feePickerModalContent = Ractive.parse(document.getElementById('FeePickerModalContent').innerHTML);
    
        Modal = Ractive.extend({
            el: document.body,
            append: true,
            template:  '#modal',

            init: function () {
                var self = this, resizeHandler;
                this.outer = this.find( '.modal-outer' );
                this.modal = this.find( '.modal' );

                this.on( 'close', function ( event ) {
                    this.teardown();
                });

                $('window').resize(self.center); 
                
                this.on( 'teardown', function () {
                    $('window').off('resize', self.center);
                }, false );

                this.center();
        },
        
        center: function () {
            var outerHeight, modalHeight, verticalSpace;
            outerHeight = this.outer.clientHeight;
            modalHeight = this.modal.clientHeight;
            verticalSpace = ( outerHeight - modalHeight ) / 2;
            this.modal.style.top = verticalSpace + 'px';
        }
    });
    
    var FeePickerModal = Modal.extend({
        partials : {
            modalContent : feePickerModalContent
        }
    });
    
    var CalculationModal = Modal.extend({
      partials : { 
        modalContent : calculationModalContent
      }, 
      
      init: function ( options ) {
        // wherever we overwrite methods, such as `init`, we can call the
        // overwritten method as `this._super`
        var that = this;
        
        var originalData = $.extend(true, {}, that.data);
        
        this.on('close', function(e, update){
            //this is needed for mask.js
            
            if (!update) {
               $.extend(that.data, originalData);
            }
            else
            {
               that.updateModel();
            }
                 
        });
        
        this.on('recalc', function(){
            ractive.fire('recalc');
            window.setTimeout(function(){
                ractive.update();
                that.data.loan = ractive.get();
                that.update();
            }, 0);
        })
        
        this._super( options );
    
      },
    });
    var startTime = Date.now();
    
          var TimeControl = Ractive.extend({
            template: '#TimeControl',
            data:{value: '',
                id: '',
                readonly: false
                },
            init: function(){
                this.on('minuteBlur', function(e)
                {
                    time_onminuteblur(e.node);
                });
                
                this.on('hourKeyUp', function(e)
                {
                    time_onhourkeyup(e.node, e.node.id + '_minute', e.original );
                });
                
                this.on('hourBlur', function(e)
                {
                    time_onhourblur(e.node);
                });
            }
          
          });

          var DatePicker = Ractive.extend({
            template: '#DatePicker',
            data: {
                value: '',
                id: '',
                readonly: false
                
            },
            init: function(){
                var self = this;
            
                this.on('showCalendar', function(e,id){
                    return displayCalendar(id);
                });
                this.on('domblur', function(event){
                    var kp = event.keypath;
                    //sadly this blur event fires before the mask.js blur event 
                    //by using setTimeout we queue up the function until after all the other
                    //js code.
                    window.setTimeout(function(){
                        self.updateModel("value", true);
                    });
                 });
                 
                this.on('updatePicker', function(event)
                {
                        window.setTimeout(function(){
                            self.set("value", event.node.value);
                            ractive.fire('recalc');
                            }, 0);
                 });
              }   
          });
          
          var RactiveCombobox = Ractive.extend({
              template: '#RactiveCombobox',
              data: {
                  value: '',
                  Prefix: '',
                  disabled: '',
                  Field: ''
              },
              init: function() {

                  
                if(<%=AspxTools.JsBool(!IsReadOnly) %>)
                {
                      this.on('showOptions', function(event, i, j, k, OptionsId) {
                          if(!this.data.disabled)
                          {
                              var options = $("." + OptionsId);
                              options.toggle();

                              var node = $(event.node);
                              if (node.prop('tagName').toLowerCase() == 'input') {
                              
                                  //called from the input
                                  var img = node.parent().find('img');
                                  img.after(options);
                              }
                              else {
                              
                                  //called from the img
                                  node.after(options);
                                  node = node.parent().find("input");
                                  node.focus();
                             }
                         }
                      });

                      this.on('hideDesc', function(event, OptionsId) {
                          var options = $("." + OptionsId);

                          if (canHideDescriptions) {
                              options.hide();
                              $("#OptionsContainer").append(options);
                          }

                      });

                      this.on('keyScroll', function(event){
                      
                        var code = event.original.which; //get the keycode
                        var ul = $(event.node).parent().find('ul'); //get the ul
                        
                        var selectedLI = ul.find(".selected");
                        
                        var hasSelectedLI = selectedLI.length > 0;

                        switch(code)
                        {
                            case 13:
                                if(hasSelectedLI)
                                {
                                    selectedLI.click();
                                }
                            break;
                            case 38: //up
                                if(hasSelectedLI){
                                   var nextLI = selectedLI.prev();
                                   
                                   if(nextLI.length > 0){
                                        unhighlightLI(selectedLI);
                                        highlightLI(nextLI);
                                        ul.scrollTop(ul.scrollTop() + nextLI.position().top - ul.height()/2);
                                   }
                                }
                                else{
                                        //select the first LI if there is none
                                        highlightLI(ul.find('li').first());
                                }
                            break;
                            case 40: //down
                            
                                if(!ul.is(":visible")){
                                   $(event.node).click();
                                }
                                
                                if(hasSelectedLI){
                                
                                   var nextLI = selectedLI.next();
                                   if(nextLI.length > 0){
                                        unhighlightLI(selectedLI);
                                        highlightLI(nextLI);
                                        ul.scrollTop(ul.scrollTop() + nextLI.position().top - ul.height()/2);
                                   }
                                }
                                else{
                                        //select the first LI if there is none
                                        highlightLI(ul.find('li').first());
                                }
                            break;
                        }
                      });

                      this.on('recalc', function(event, success) {

                          ractive.fire('recalc');

                      });
                  }
              }
          });
          
          var RactiveDDL = Ractive.extend({
              template: '#RactiveDDL',
              data: {
                  value: '',
                  Prefix: '',
                  disabled: ''
              },
              init: function() {

                  var input = $(this.el).find(".RactiveDDL");
                  var selectedListItem = $("."+ this.data.Prefix + "Options li[val='" + this.data.value + "']");
                  input.val(selectedListItem.text());
                  
                if(<%=AspxTools.JsBool(!IsReadOnly) %>)
                {
                      this.on('showOptions', function(event, i, j, k, OptionsId) {
                          if(!this.data.disabled)
                          {
                              var options = $("." + OptionsId);
                              options.toggle();

                              currentFeeRow = j;
                              currentSection = i;
                              if(OptionsId == 'PaidBySingleOptions' || (OptionsId == 'PayableOptions' && k == undefined))
                              {
                                k = 0;
                              }
                              currentPayment = k;

                              var node = $(event.node);
                              if (node.prop('tagName').toLowerCase() == 'input') {
                              
                                  //called from the input
                                  var img = node.parent().find('img');
                                  img.after(options);
                              }
                              else {
                              
                                  //called from the img
                                  node.after(options);
                                  node = node.parent().find("input");
                                  node.focus();
                             }
                         }
                      });

                      this.on('hideDesc', function(event, OptionsId) {
                          var options = $("." + OptionsId);

                          if (canHideDescriptions) {
                              options.hide();
                              $("#OptionsContainer").append(options);
                          }

                      });

                      this.on('keyScroll', function(event){
                      
                        var code = event.original.which; //get the keycode
                        var ul = $(event.node).parent().find('ul'); //get the ul
                        
                        var selectedLI = ul.find(".selected");
                        
                        var hasSelectedLI = selectedLI.length > 0;

                        switch(code)
                        {
                            case 13:
                                if(hasSelectedLI)
                                {
                                    selectedLI.click();
                                }
                            break;
                            case 38: //up
                                if(hasSelectedLI){
                                   var nextLI = selectedLI.prev();
                                   
                                   if(nextLI.length > 0){
                                        unhighlightLI(selectedLI);
                                        highlightLI(nextLI);
                                        ul.scrollTop(ul.scrollTop() + nextLI.position().top - ul.height()/2);
                                   }
                                }
                                else{
                                        //select the first LI if there is none
                                        highlightLI(ul.find('li').first());
                                }
                            break;
                            case 40: //down
                            
                                if(!ul.is(":visible")){
                                   $(event.node).click();
                                }
                                
                                if(hasSelectedLI){
                                
                                   var nextLI = selectedLI.next();
                                   if(nextLI.length > 0){
                                        unhighlightLI(selectedLI);
                                        highlightLI(nextLI);
                                        ul.scrollTop(ul.scrollTop() + nextLI.position().top - ul.height()/2);
                                   }
                                }
                                else{
                                        //select the first LI if there is none
                                        highlightLI(ul.find('li').first());
                                }
                            break;
                        }
                      });

                      this.on('recalc', function(event, success) {

                          ractive.fire('recalc');

                      });
                  }
              }
          });

    var ractive = new Ractive({
      // The `el` option can be a node, an ID, or a CSS selector.
      el: 'container',
      append: false,

      // We could pass in a string, but for the sake of convenience
      // we're passing the ID of the <script> tag above.
      template: '#ClosingCostTemplate',
      partials : '#ClosingCostFeeTemplate,#ClosingCostFeePaymentTemplate,#Footer',

              components: { ractiveDDL: RactiveDDL, ractiveCombobox: RactiveCombobox, datePicker: DatePicker, timeControl: TimeControl },
      // Here, we're passing in some initial data
      data: ClosingCostData,
      
      init : function() {
        
      }
    });
    
    var observer = ractive.observe('SectionList.*.ClosingCostFeeList.*.bene', function(newValue, oldValue, keypath, i, j, k){
        if(oldValue != null && newValue != null && oldValue != newValue)
        {
            var splitArray = keypath.split(".")
            var field = splitArray[splitArray.length - 1];
            
            var prefix = "Beneficiary";
            
            var DDLClass = prefix + "DDL.RactiveDDL" + i + j;
            
            var OptionsClass = prefix + "Options";
            
            $("." + DDLClass).val( $("." + OptionsClass).find("li[val='"+newValue+"']").text());
        }
    });
    
    $(".RactiveDDL").css("width", $(".BeneficiaryOptions").width());
	$(".OptionsList").css("width", $(".OptionsList").width() + $(".DDLImg").width() + 2);
	
	$(".RactiveCombobox").css("width", $(".CreditDescOptions").width());
	$(".CreditDescOptions").css("width", $(".CreditDescOptions").width() + $(".DDLImg").width() + 2);
	
	ractive.on('onContactHover', function(event){
	    var el = $(event.node);
	    el.attr("src", '../../images/contacts_clicked.png');
	});
	
	ractive.on('onContactMouseOut', function(event){
	    var el = $(event.node);
	    el.attr("src", '../../images/contacts.png');
	});
	
	ractive.on('showAgentPicker', function(event, i, j){
        var model = ractive.get();
        
        if (model.IsArchive){
            return; 
        }
       
        var el = $(event.node);
        var rolodex = new cRolodex();

        var type = el.parent().find("input[type='hidden']").val();

        rolodex.chooseFromRolodex(type, ML.sLId, true, false, function(args){

            if (args.OK == true) {
                    if(args.Clear == true)
                    {
                        $("#ClearAgent").dialog({
                            modal: true,
                            height: 150,
                            width: 350,
                            title: "",
                            dialogClass: "LQBDialogBox",
                            resizable: false,
                            buttons: [
                                {text: "OK",
                                    click: function(){
                                        // Remove Agent from official contacts
                                        var model = ractive.get();
                                        var data = {
                                            loanId: ML.sLId,
                                            viewModelJson: JSON.stringify(model),
                                            recordId: ractive.get("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id"),
                                        }
                                        
                                        callWebMethod("ClearAgent", 
                                            data,
                                            function(){
                                                alert("Error:  Could not remove agent.");
                                            }, 
                                            //On Success
                                            function(m){
                                                model = JSON.parse(m.d);
                                                ractive.set(model);
                                            });
                                    
                                        $(this).dialog("close");
                                    }
                                },
                                {text: "Cancel",
                                    click: function(){
                                        $(this).dialog("close");
                                    }
                                }
                            ]
                        })
                    }
                    else
                    {
                    var id = "";
                    var populateFromRolodex = false;
                    var agentType = args.AgentType;
                    
                    if(args.RecordId != "" && args.RecordId != <%=AspxTools.JsString(Guid.Empty) %> )
                    {
                        //If they're choosing from an agent record, simply set it's record id as the fee's bene_id.
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id", args.RecordId);
                        //Update paidTo DDL
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene", agentType);
                        //Re-enable beneficiary automation.
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".disable_bene_auto", false);
                        
                        updateDirtyBit();
                        ractive.fire('recalc');
                    }
                    else
                    {
                        if(args.BrokerLevelAgentID != <%=AspxTools.JsString(Guid.Empty) %>)
                        {
                            //If the selcted contact has no record id, then add it to the agents list.
                            //If there is a BrokerLevelAgentID, populate using the rolodex and that id
                            
                            id=args.BrokerLevelAgentID;
                            populateFromRolodex = true;  
                        }
                        else
                        {
                            //If the selcted contact has no record id, then add it to the agents list.
                            //If there isn't a BrokerLevelAgentID, populate using the employee info
                            
                            id=args.EmployeeId;
                            populateFromRolodex = false;  
                        }
                        
                        
                        $("#ConfirmAgent").dialog({
                            modal: true,
                            height: 150,
                            width: 350,
                            title: "",
                            dialogClass: "LQBDialogBox",
                            resizable: false,
                            buttons: [
                                {text: "OK",
                                click: function(){
                                    callWebMethod("CreateAgent", 
                                        {loanId: ML.sLId, id: id, populateFromRolodex: populateFromRolodex, agentType: agentType},
                                        function(){alert("Error:  Could not create a new agent.");}, 
                                        //On Success
                                        function(d){
                                            var m = JSON.parse(d.d);
                                            // Get the recordID and set it for the bene_id
                                            recordId = m.RecordId;
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id", recordId);
                                            //Update the fee's paidTo DDL
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene", agentType);
                                            //Re-enable beneficiary automation.
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".disable_bene_auto", false);
                                            
                                            updateDirtyBit();
                                            ractive.fire('recalc');
                                            
                                        });
                                    $(this).dialog("close");
                                    }
                                },
                                {text: "Cancel",
                                click: function(){
                                    $(this).dialog("close");
                                }}
                            ]
                        });
                    }

                }
            }
        });

	});
    
    ractive.on('domblur', function(event){
        var kp = event.keypath;
        //sadly this blur event fires before the mask.js blur event 
        //by using setTimeout we queue up the function until after all the other
        //js code.
        window.setTimeout(function(){
            ractive.updateModel(kp, true);
        });
    });
    
    ractive.on('delete', function(event, i, j, desc, amt, orgDesc){
        var tbody = $(event.node).closest('tbody');
        var d = desc;
        if (!d)
        {
            d = orgDesc;
        }
        $('#dialog-confirm').find('p').html('Are you sure you would like to remove the following fee? <br />').append(document.createTextNode(d + ' : ' + amt));
        $('#dialog-confirm').dialog({
            modal: true,
            buttons: {
                "Yes": function() {
                    $(this).dialog("close");
                    ClosingCostData.SectionList[i].ClosingCostFeeList.splice(j,1);
                    updateDirtyBit();
                    checkHudLines(tbody);
                },
                "No": function()
                {
                    $(this).dialog("close");
                }
            },
            closeOnEscape: false,
            width: "400",
            draggable: false,
            dialogClass: "LQBDialogBox",
            resizable: false
        });
    });

    ractive.on('updatePaidBy', function(e, i, j, total){
        var obj = e.context, 
            node = e.node,
            value = node.value ;
            
        if (value == -1){
               var model = ractive.get(), 
                   fee = model.SectionList[i].ClosingCostFeeList[j];
               fee.pmts[0].paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Seller.ToString("D")) %>;
               
               // Add new system payment
               var newPayment = new Object();
               newPayment.amt = "";
               newPayment.ent = 0;
               newPayment.is_fin = false;
               newPayment.is_system = true;
               newPayment.made = false;
               newPayment.paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Seller.ToString("D")) %>;
               newPayment.pmt_at = <%= AspxTools.HtmlString(E_GfeClosingCostFeePaymentTimingT.AtClosing.ToString("D")) %>;
               newPayment.pmt_dt = "";
               fee.pmts.push(newPayment);
               
               updateDirtyBit();
        }
        else if(value == 3)
        {
            obj.pmts[0].paid_by = value; 
            obj.pmts[0].pmt_at = 1; // Set to 'at closing'
            ractive.update(e.keypath);
        }
        else {
            obj.pmts[0].paid_by = value; 
            ractive.update(e.keypath);
        }
        
        ractive.fire('recalc');
    });
    
    ractive.on('updatePaidBySplit', function(e){
        var obj = e.context, 
            node = e.node,
            value = node.value ;
            
        if(value == 3)
        {
            obj.paid_by = value; 
            obj.pmt_at = 1; // Set to 'at closing'
            ractive.update(e.keypath);
        }
        else {
            obj.paid_by = value; 
            ractive.update(e.keypath);
        }
        
        ractive.fire('recalc');
    });
    
    ractive.on('AddSplitPayment', function(event, i, j){
        updateDirtyBit();

        var model = ractive.get(), 
        fee = model.SectionList[i].ClosingCostFeeList[j];

        var newPayment = new Object();
        newPayment.amt = "";
        newPayment.ent = 0;
        newPayment.is_fin = false;
        newPayment.is_system = false;
        newPayment.made = false;
        newPayment.paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Seller.ToString("D")) %>;
        newPayment.pmt_at = <%= AspxTools.HtmlString(E_GfeClosingCostFeePaymentTimingT.AtClosing.ToString("D")) %>;
        newPayment.pmt_dt = "";
        fee.pmts.push(newPayment);
        
        ractive.fire('recalc');
    });
    
    ractive.on('updateCalc', function(event, i, j){
       
        var o = this.get('SectionList');
        var m = new CalculationModal({ data : {fee: o[i].ClosingCostFeeList[j].f, loan: this.get()}});
        m.on('teardown', function(){
            ractive.fire('recalc');
        });
    });
    
    ractive.on('recalc', function(event, success){
        window.setTimeout(function(){
            var startRecalc = Date.now(),
                model = ractive.get(), 
                data = { loanId: ML.sLId, viewModelJson: JSON.stringify(model)},  
                fin = function(m) {
                     var load = Date.now();
                     lqblog('Time To Load Data ' + (load - startRecalc) + 'ms.');
                     model = JSON.parse(m.d);
                     var d = Date.now();
                     
                     ractive.set(model);
                  
                     var c = Date.now();
                     lqblog('Time to set data ' + ( c-d) + 'ms.');
                     if (success){
                        success();
                     }
                },
                error = function(e){
                    alert('Error');
                };
                
            callWebMethod('CalculateData', data, error, fin);
            });
    });
    
    function GetPaidByDesc(paidby)
    {
        var paidByDesc = "";
        if(paidby == 1)
        {
            paidByDesc = "borr pd";
        }
        else if(paidby == 2)
        {
            paidByDesc = "seller";
        }
        else if(paidby == 3)
        {
            paidByDesc = "borr fin";
        }
        else if(paidby == 4)
        {
            paidByDesc = "lender";
        }
        else if(paidby == 5)
        {
            paidByDesc = "broker";
        }
        else if(paidby == 6)
        {
            paidByDesc = "other";
        }
        
        return paidByDesc;
    }
           
    ractive.on('removePayment', function(e,i, j, k, amt, paidby, desc, orgDesc){
        var d = desc;
        if (!d)
        {
            d = orgDesc;
        }
        var paidByDesc = GetPaidByDesc(paidby);
        
        $('#dialog-confirm').find('p').text('Are you sure you would like to remove the ' + amt + ' ' + paidByDesc + ' split payment for the ' + d + '?');
        
        $('#dialog-confirm').dialog({
            modal: true,
            buttons: {
                "Yes": function()
                {
                    $(this).dialog("close");
                    updateDirtyBit();
                    var model = ractive.get(); 
                    model.SectionList[i].ClosingCostFeeList[j].pmts.splice(k,1);
                    ractive.fire('recalc');
                },
                "No": function()
                {
                    $(this).dialog("close");
                }
            },
            closeOnEscape: false,
            width: "400",
            draggable: false,
            dialogClass: "LQBDialogBox",
            resizable: false
        });
    });
    
    ractive.on('addFee', function(event,i){
         var model = ractive.get(),
             section =  model.SectionList[i],
             sectionName = section.SectionName,
             data = { loanId: ML.sLId, viewModelJson: JSON.stringify(model),  sectionName : sectionName  }; 
         
         callWebMethod('GetAvailableClosingCostFeeTypes', data, 
         
         function(){
            alert('Error'); 
         },
         
         function(m) {
            var result = JSON.parse(m.d);
            var y = new FeePickerModal({data : result });
            y.on('selectFee', function(e, si){
                // Set as seller paid by.
                result.FeeListAsSellerFees[si].pmts[0].paid_by = 2;
                section.ClosingCostFeeList.push(result.FeeListAsSellerFees[si]);
                y.teardown();
                updateDirtyBit();
                
                $(".RactiveDDL" + i + (section.ClosingCostFeeList.length - 1)).css("width", $(".OptionsList").width() - $(".DDLImg").width() - 2);
                
                checkHudLines($(event.node).closest('table').find('tbody'));
                ractive.fire('recalc');
                return false;
            });
         });
    });

    ractive.on('showCalendar', function(e,id){
        return displayCalendar(id);
    });
    
    ractive.on('CanShopChange', function(e, i, j){
        var keypath = 'SectionList.' + i + '.ClosingCostFeeList.' + j + '.did_shop';
        var can_shop = $(e.node).prop('checked');
        if(!can_shop)
        {
            ractive.set(keypath, false);
        }
    });
    
    ractive.on('tpClick', function(e, i, j){
        var model = ractive.get(); 
        if ( !model.SectionList[i].ClosingCostFeeList[j].tp )
        {
            model.SectionList[i].ClosingCostFeeList[j].aff = false;
            ractive.set(model);
        }
    });
    
    var canHideDescriptions = true;
    var currentFeeRow = 0;
    var currentSection = 0;
    var currentPayment = 0;

        ractive.on('onHoverDescriptions', function(event) {
            canHideDescriptions = false;
        });

        ractive.on('onMouseOutDescriptions', function(event) {
            canHideDescriptions = true;
        });

        ractive.on('highlightLI', function(event) {
            var node = $(event.node);
            var allLI = node.parent().find('li');
            unhighlightLI(allLI);
            highlightLI(node);
            
        });

        ractive.on('unhighlightLI', function(event) {
            var node = $(event.node);
            unhighlightLI(node);
        });
        
        ractive.on('selectCombobox', function(event){
            var node = $(event.node);
            var text = node.text();
            
            //get the keypath to be updated
            var keypath = node.parent().parent().find("input").attr("field");
            
            ractive.set(keypath, text);
            updateDirtyBit(event.original);
            canHideDescriptions = true;
            
            node.parent().hide();
             $("#OptionsContainer").append(node.parent());
        });


        ractive.on('selectDDL', function(event, partialKeypath) {
            var node = $(event.node);
            var text = node.text();
              
            var options = node.parent();

            var td = node.parent().parent();
            
            td.find(".RactiveDDL").val(node.text());
            td.find("input[type='hidden']").val(node.attr('val'));
          
            var value = node.attr('val');

            node.parent().hide();
            $("#OptionsContainer").append(node.parent());
            canHideDescriptions = true;
            
            // Get Agent Id
            var args = new Object();
            args["loanid"] = ML.sLId;
            args["IsArchivePage"] = false;
            args["AgentType"] = value;
              
            var result = gService.cfpb_utils.call("GetAgentIdByAgentType", args);
            
            if (!result.error) {
                //Update the fee's Beneficiary ID
                ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene_id", result.value.RecordId);
                // Update the fee's Beneficiary Description
                ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene_desc", result.value.CompanyName);
                //Update the fee's paidTo DDL
                ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene", value);
                //Re-enable beneficiary automation.
                ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".disable_bene_auto", false);
            }

            updateDirtyBit(event.original);
            ractive.fire('recalc');
         });
         
         ractive.on('reselectInput', function(event) {
             //this is called by the UL itself
             $(event.node).parent().find('input').focus();    
         });
         
         
    var c = 0;
    for (var i = 0; i < ClosingCostData.SectionList.length; i++) {
        c += ClosingCostData.SectionList[i].ClosingCostFeeList.length; 
    }
    lqblog('Number of fees : '+  c);
    lqblog('Initial Render : ' + (Date.now() - startTime) + 'ms');
    
  if(<%=AspxTools.JsBool(IsReadOnly) %>)
  {
    $("*").attr("readonly", "");
    $("input[type!='text'], select").not("#ddlGFEArchives").attr("disabled", "true");
  }

    window.f_saveMe = function() {
    
    if ($('input.invalid,input.dupe').length > 0){
        alert('Cannot save until the HUD line issues are fixed.');
        return false;
    }
            
    var model = ractive.get();
        var data = { loanId: ML.sLId, viewModelJson: JSON.stringify(model)},
            success = false;
        
        callWebMethod('Save', data, function(){ 
            alert('Failed');
        }, 
        function(d) {
            success = true;
            clearDirty();
        });
        return success;
    } 
    

    
    globalRActive = ractive;
    
    window.saveMe = window.f_saveMe;
    
    $wrapper = $("#wrapper");
    $closeIcon = $("#closeIcon");
    $tabLinks = $("#Tabs li");
    $contentDivs = $(".tabContent");
});

/////////////////////////////////////////////////////////////////////

  function openPopup(linkFirstPart, uniqueName)
  {
    var virtualRoot = <%=AspxTools.SafeUrl(VirtualRoot)%>;
    var popup = window.open(virtualRoot+'/'+linkFirstPart+'?loanid='+ML.sLId+
        "&appid="+ML.aAppId+
        "&ispopup=1", uniqueName, "toolbar=no,menubar=no,location=no,status=no,scrollbars=yes,resizable=yes");
    popup.focus();
  }
  
  function selectedDate(cal, date)
    {
        cal.sel.value = date;
        cal.callCloseHandler();
        if (typeof (updateDirtyBit) == 'function') updateDirtyBit();
        cal.sel.blur();
    } 
    
    </script>
    
    </form>
</body>
</html>
