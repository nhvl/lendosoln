﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class GoodbyeLetterServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(GoodbyeLetterServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            // General
            dataLoan.sGLServTransEffD_rep = GetString("sGLServTransEffD");

            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.GoodbyeLetter, E_ReturnOptionIfNotExist.CreateNew);
            f.PreparerName = GetString("sGLCompletedBy");
            f.CompanyName = GetString("sGLCompletedByBrokerName");
            f.Update();

            // Previous Servicer
            dataLoan.sGLPrevServNm = GetString("sGLPrevServNm");
            dataLoan.sGLPrevServAddr = GetString("sGLPrevServAddr");
            dataLoan.sGLPrevServCity = GetString("sGLPrevServCity");
            dataLoan.sGLPrevServState = GetString("sGLPrevServState");
            dataLoan.sGLPrevServZip = GetString("sGLPrevServZip");
            dataLoan.sGLPrevServTFPhone = GetString("sGLPrevServTFPhone");
            dataLoan.sGLPrevServContactNm = GetString("sGLPrevServContactNm");
            dataLoan.sGLPrevServContactNum = GetString("sGLPrevServContactNum");
            dataLoan.sGLPrevServBusHrAM = GetString("sGLPrevServBusHrAM");
            dataLoan.sGLPrevServBusHrPM = GetString("sGLPrevServBusHrPM");

            // New Servicer
            dataLoan.sGLNewServNm = GetString("sGLNewServNm");
            dataLoan.sGLNewServAddr = GetString("sGLNewServAddr");
            dataLoan.sGLNewServCity = GetString("sGLNewServCity");
            dataLoan.sGLNewServState = GetString("sGLNewServState");
            dataLoan.sGLNewServZip = GetString("sGLNewServZip");
            dataLoan.sGLNewServTFPhone = GetString("sGLNewServTFPhone");
            dataLoan.sGLNewServContactNm = GetString("sGLNewServContactNm");
            dataLoan.sGLNewServContactNum = GetString("sGLNewServContactNum");
            dataLoan.sGLNewServBusHrAM = GetString("sGLNewServBusHrAM");
            dataLoan.sGLNewServBusHrPM = GetString("sGLNewServBusHrPM");
            dataLoan.sGLNewServAccPmtD_rep = GetString("sGLNewServAccPmtD");
            dataLoan.sGLNewServPmtAddr = GetString("sGLNewServPmtAddr");
            dataLoan.sGLNewServPmtCity = GetString("sGLNewServPmtCity");
            dataLoan.sGLNewServPmtState = GetString("sGLNewServPmtState");
            dataLoan.sGLNewServPmtZip = GetString("sGLNewServPmtZip");
            dataLoan.sGLNewServFirstPmtSchedD_rep = GetString("sGLNewServFirstPmtSchedD");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            // General
            SetResult("sGLServTransEffD", dataLoan.sGLServTransEffD_rep);
            
            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.GoodbyeLetter, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("sGLCompletedBy", f.PreparerName);
            SetResult("sGLCompletedByBrokerName", f.CompanyName);

            // Previous Servicer
            SetResult("sGLPrevServNm", dataLoan.sGLPrevServNm);
            SetResult("sGLPrevServAddr", dataLoan.sGLPrevServAddr);
            SetResult("sGLPrevServCity", dataLoan.sGLPrevServCity);
            SetResult("sGLPrevServState", dataLoan.sGLPrevServState);
            SetResult("sGLPrevServZip", dataLoan.sGLPrevServZip);
            SetResult("sGLPrevServTFPhone", dataLoan.sGLPrevServTFPhone);
            SetResult("sGLPrevServContactNm", dataLoan.sGLPrevServContactNm);
            SetResult("sGLPrevServContactNum", dataLoan.sGLPrevServContactNum);
            SetResult("sGLPrevServBusHrAM", dataLoan.sGLPrevServBusHrAM);
            SetResult("sGLPrevServBusHrPM", dataLoan.sGLPrevServBusHrPM);

            // New Servicer
            SetResult("sGLNewServNm", dataLoan.sGLNewServNm);
            SetResult("sGLNewServAddr", dataLoan.sGLNewServAddr);
            SetResult("sGLNewServCity", dataLoan.sGLNewServCity);
            SetResult("sGLNewServState", dataLoan.sGLNewServState);
            SetResult("sGLNewServZip", dataLoan.sGLNewServZip);
            SetResult("sGLNewServTFPhone", dataLoan.sGLNewServTFPhone);
            SetResult("sGLNewServContactNm", dataLoan.sGLNewServContactNm);
            SetResult("sGLNewServContactNum", dataLoan.sGLNewServContactNum);
            SetResult("sGLNewServBusHrAM", dataLoan.sGLNewServBusHrAM);
            SetResult("sGLNewServBusHrPM", dataLoan.sGLNewServBusHrPM);
            SetResult("sGLNewServAccPmtD", dataLoan.sGLNewServAccPmtD_rep);
            SetResult("sGLNewServPmtAddr", dataLoan.sGLNewServPmtAddr);
            SetResult("sGLNewServPmtCity", dataLoan.sGLNewServPmtCity);
            SetResult("sGLNewServPmtState", dataLoan.sGLNewServPmtState);
            SetResult("sGLNewServPmtZip", dataLoan.sGLNewServPmtZip);
            SetResult("sGLNewServFirstPmtSchedD", dataLoan.sGLNewServFirstPmtSchedD_rep);
        }
    }

    public partial class GoodbyeLetterService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new GoodbyeLetterServiceItem());
        }
    }
}
