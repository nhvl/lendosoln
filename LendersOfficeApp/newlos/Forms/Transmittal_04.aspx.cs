namespace LendersOfficeApp.newlos.Forms
{
    using System;
    using DataAccess;
    using LendersOffice.Migration;

    public partial class Transmittal_04 : BaseLoanPage
    {

        #region Protected Member Variables
        #endregion

        private void Bind_sLpDocClass(MeridianLink.CommonControls.ComboBox cb) 
        {
            cb.Items.Add("Accept Plus");
            cb.Items.Add("Streamlined Accept");
            cb.Items.Add("Standard");
            cb.Items.Add("Accept");
            cb.Items.Add("Refer");
        }
        protected void PageInit(object sender, System.EventArgs e)
        {
    
            sLenZip.SmartZipcode(sLenCity, sLenState);
            sSpZip.SmartZipcode(sSpCity, sSpState);
            
            Tools.Bind_sLPurposeT(sLPurposeT);
            Tools.Bind_sLT(sLT);
            Tools.Bind_sLienPosT(sLienPosT);
            Tools.Bind_sFinMethT(sFinMethT);
            Tools.Bind_sGseSpT(sGseSpT);
            Tools.Bind_aOccT(aOccT);
            Tools.Bind_sEstateHeldT(sEstateHeldT);
            Tools.Bind_sGseRefPurposeT(sGseRefPurposeT);
            Tools.Bind_sMOrigT(sMOrigT);
            Tools.Bind_s1stMOwnerT(s1stMOwnerT);
            Tools.Bind_sQualIRDeriveT(sQualIRDeriveT);
            Tools.Bind_sSpProjectClassFannieT(sSpProjectClassFannieT);
            Tools.Bind_sSpProjectClassFreddieT(sSpProjectClassFreddieT);
            Tools.Bind_sSpValuationMethodT(sSpValuationMethodT);
            Tools.Bind_sSpAppraisalFormT(sSpAppraisalFormT);
            Bind_sLpDocClass(sLpDocClass);
            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);
            Tools.Bind_QualTermCalculationType(sQualTermCalculationType);

            this.PageTitle = "1008 Single";
            this.PageID = "1008_04";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CTransmittal_09PDF);
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }
    
        private void BindDataObject(CPageData dataLoan) 
        {
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            Tools.SetDropDownListValue(aOccT, dataApp.aOccT);
            Tools.SetDropDownListValue(s1stMOwnerT, dataLoan.s1stMOwnerT );
            Tools.SetDropDownListValue(sEstateHeldT, dataLoan.sEstateHeldT);
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
            Tools.SetDropDownListValue(sGseRefPurposeT, dataLoan.sGseRefPurposeT);
            Tools.SetDropDownListValue(sGseSpT, dataLoan.sGseSpT);
            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            Tools.SetDropDownListValue(sLT, dataLoan.sLT);
            Tools.SetDropDownListValue(sLienPosT, dataLoan.sLienPosT);
            Tools.SetDropDownListValue(sMOrigT, dataLoan.sMOrigT );
            Tools.SetDropDownListValue(sQualIRDeriveT, dataLoan.sQualIRDeriveT );
            Tools.SetDropDownListValue(sSpProjectClassFannieT, dataLoan.sSpProjectClassFannieT);
            Tools.SetDropDownListValue(sSpProjectClassFreddieT, dataLoan.sSpProjectClassFreddieT);
            sFinMethT.Enabled = ! ( dataLoan.sIsRateLocked || IsReadOnly );

            aBBaseI.Text               = dataApp.aBBaseI_rep;
            aBBaseI.ReadOnly = dataLoan.sIsIncomeCollectionEnabled;
            aBFirstNm.Text             = dataApp.aBFirstNm;
            aBLastNm.Text              = dataApp.aBLastNm;
            aBMidNm.Text               = dataApp.aBMidNm;
            aBNonbaseI.Text            = dataApp.aBNonbaseI_rep;
            aBOIFrom1008.Text          = dataApp.aBOIFrom1008_rep;
            aBSpPosCf.Text             = dataApp.aBSpPosCf_rep;
            aBSsn.Text                 = dataApp.aBSsn;
            aBSuffix.Text              = dataApp.aBSuffix;
            aCBaseI.Text               = dataApp.aCBaseI_rep;
            aCBaseI.ReadOnly = dataLoan.sIsIncomeCollectionEnabled;
            aCFirstNm.Text             = dataApp.aCFirstNm;
            aCLastNm.Text              = dataApp.aCLastNm;
            aCMidNm.Text               = dataApp.aCMidNm;
            aCNonbaseI.Text            = dataApp.aCNonbaseI_rep;
            aCOIFrom1008.Text          = dataApp.aCOIFrom1008_rep;
            aCSpPosCf.Text             = dataApp.aCSpPosCf_rep;
            aCSsn.Text                 = dataApp.aCSsn;
            aCSuffix.Text              = dataApp.aCSuffix;
            aOpNegCfLckd.Checked       = dataApp.aOpNegCfLckd;
            aSpNegCfLckd.Checked       = dataApp.aSpNegCfLckd;
            aOIFrom1008Desc.Text       = dataApp.aOIFrom1008Desc;
            aOpNegCf.Text              = dataApp.aOpNegCf_rep;
            aPresTotHExp.Text          = dataApp.aPresTotHExp_rep;
            aQualBottomR.Text          = dataApp.aQualBottomR_rep;
            aQualTopR.Text             = dataApp.aQualTopR_rep;
            aSpNegCf.Text              = dataApp.aSpNegCf_rep;
            aTotBaseI.Text             = dataApp.aTotBaseI_rep;
            aTotNonbaseI.Text          = dataApp.aTotNonbaseI_rep;
            aTotOIFrom1008.Text        = dataApp.aTotOIFrom1008_rep;
            aTotSpPosCf.Text           = dataApp.aTotSpPosCf_rep;
            aTransmBTotI.Text          = dataApp.aTransmBTotI_rep;
            aTransmCTotI.Text          = dataApp.aTransmCTotI_rep;
            aTransmOMonPmt.Text        = dataApp.aTransmOMonPmt_rep;
            aTransmOtherProHExp.Text   = dataApp.aTransmOtherProHExp_rep;
            aTransmPro1stM.Text        = dataApp.aTransmPro1stMPmt_rep;
            aTransmProHazIns.Text      = dataApp.aTransmProHazIns_rep;
            aTransmProHoAssocDues.Text = dataApp.aTransmProHoAssocDues_rep;
            aTransmProMIns.Text        = dataApp.aTransmProMIns_rep;
            aTransmProOFin.Text        = dataApp.aTransmPro2ndMPmt_rep;
            aTransmProRealETx.Text     = dataApp.aTransmProRealETx_rep;
            aTransmProTotHExp.Text     = dataApp.aTransmProTotHExp_rep;
            aTransmTotI.Text           = dataApp.aTransmTotI_rep;
            aTransmTotMonPmt.Text      = dataApp.aTransmTotMonPmt_rep;
            s1stMtgOrigLAmt.Text       = dataLoan.s1stMtgOrigLAmt_rep;
            sApprVal.Text              = dataLoan.sApprVal_rep;
            sBalloonPmt.Checked        = dataLoan.sBalloonPmt;
            sBalloonPmt.Enabled        = dataLoan.sGfeIsBalloonLckd;
            sBiweeklyPmt.Checked       = dataLoan.sBiweeklyPmt;
            sBuydown.SelectedIndex     = dataLoan.sBuydown ? 0 : 1;
            sCltvR.Text                = dataLoan.sCltvR_rep;
            sCommitNum.Text            = dataLoan.sCommitNum;
            sContractNum.Text          = dataLoan.sContractNum;
            sFinMethDesc.Text          = dataLoan.sFinMethDesc;
            sFinalLAmt.Text            = dataLoan.sFinalLAmt_rep;
            sInvestLNumLckd.Checked    = dataLoan.sInvestLNumLckd;
            sInvestLNum.Text           = dataLoan.sInvestLNum;
            sLenAddr.Text              = dataLoan.sLenAddr;
            sLenCity.Text              = dataLoan.sLenCity;
            sLenContactNm.Text         = dataLoan.sLenContactNm;
            sLenContactPhone.Text      = dataLoan.sLenContactPhone;
            sLenContactTitle.Text      = dataLoan.sLenContactTitle;
            sLenContractD.Text         = dataLoan.sLenContractD_rep;
            sLenLNumLckd.Checked       = dataLoan.sLenLNumLckd;
            sLenLNum.Text              = dataLoan.sLenLNum;
            sLenNm.Text                = dataLoan.sLenNm;
            sLenNum.Text               = dataLoan.sLenNum;
            sLenState.Value            = dataLoan.sLenState;
            sLenZip.Text               = dataLoan.sLenZip;
            sLtvR.Text                 = dataLoan.sLtvR_rep;
            sNoteIR.Text               = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sProThisMPmt.Text          = dataLoan.sProThisMPmt_rep;
            sProjNm.Text               = dataLoan.sProjNm;
            sPurchPrice.Text           = dataLoan.sPurchPrice_rep;
            sQualIR.Text               = dataLoan.sQualIR_rep;
            sSpAddr.Text               = dataLoan.sSpAddr;
            sSpCity.Text               = dataLoan.sSpCity;
            sSpState.Value             = dataLoan.sSpState;
            sSpZip.Text                = dataLoan.sSpZip;
            sSubFin.Text               = dataLoan.sSubFin_rep;
            sTerm.Text                 = dataLoan.sTerm_rep;
            sTerm.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sTransmUwerComments.Text   = dataLoan.sTransmUwerComments;
            sUnitsNum.Text             = dataLoan.sUnitsNum_rep;
            sCpmProjectId.Text = dataLoan.sCpmProjectId;
            CAgentFields appraiser    = dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser,E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AppraiserCompanyName.Text = appraiser.CompanyName;
            AppraiserLicense.Text     = appraiser.LicenseNumOfAgent;
            AppraiserName.Text        = appraiser.AgentName;

            CAgentFields underwritter = dataLoan.GetAgentOfRole( E_AgentRoleT.Underwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject );
            UnderwriterName.Text = underwritter.AgentName;

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.TransmMOriginator, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            TransmMOriginatorCompanyName.Text = preparer.CompanyName;

            sHcltvR.Text = dataLoan.sHcltvR_rep;  
            sHcltvRLckd.Checked = dataLoan.sHcltvRLckd; 
            sDebtToHousingGapR.Text = dataLoan.sDebtToHousingGapR_rep;
            sDebtToHousingGapRLckd.Checked = dataLoan.sDebtToHousingGapRLckd; 
            sTransmFntc.Text = dataLoan.sTransmFntc_rep;
            sTransmFntcLckd.Checked = dataLoan.sTransmFntcLckd;
            sVerifAssetAmt.Text = dataLoan.sVerifAssetAmt_rep; 
            sFntcSrc.Text = dataLoan.sFntcSrc; 
            sRsrvMonNumDesc.Text = dataLoan.sRsrvMonNumDesc; 
            sInterestedPartyContribR.Text = dataLoan.sInterestedPartyContribR_rep; 
            sIsManualUw.Checked = dataLoan.sIsManualUw; 
            sIsDuUw.Checked = dataLoan.sIsDuUw; 
            sIsLpUw.Checked = dataLoan.sIsLpUw; 
            sIsOtherUw.Checked = dataLoan.sIsOtherUw; 
            sOtherUwDesc.Text = dataLoan.sOtherUwDesc; 
            sAusRecommendation.Text = dataLoan.sAusRecommendation; 
            sLpAusKey.Text = dataLoan.sLpAusKey; 
            sDuCaseId.Text = dataLoan.sDuCaseId; 
            sLpDocClass.Text = dataLoan.sLpDocClass;
            sRepCrScore.Text = dataLoan.sRepCrScore; 
            
            // 06/16/06 mf - OPM 5636.  Converted these from checkbox to
            // RadioButtonList.
            sIsCommLen.SelectedIndex = dataLoan.sIsCommLen ? 0 : 1;
            sIsHOwnershipEdCertInFile.SelectedIndex = dataLoan.sIsHOwnershipEdCertInFile ? 0 : 1;
            sWillEscrowBeWaived.SelectedIndex = dataLoan.sWillEscrowBeWaived ? 1 : 0;

            sIsMOrigBroker.Checked = dataLoan.sIsMOrigBroker; 
            sIsMOrigCorrespondent.Checked = dataLoan.sIsMOrigCorrespondent; 
            sTransmBuydwnTermDesc.Text = dataLoan.sTransmBuydwnTermDesc;
            sFinMethPrintAsOtherDesc.Text = dataLoan.sFinMethPrintAsOtherDesc;
            sFinMethodPrintAsOther.Checked = dataLoan.sFinMethodPrintAsOther;
            
            // 06/16/06 mf - OPM 4953. Added these fields to the form.
            sApprovD.Text = dataLoan.sApprovD_rep;
            sUnderwritingD.Text = dataLoan.sUnderwritingD_rep;

            Tools.SetDropDownListValue(sSpValuationMethodT, dataLoan.sSpValuationMethodT);
            Tools.SetDropDownListValue(sSpAppraisalFormT, dataLoan.sSpAppraisalFormT);
            sApprFull.Checked = dataLoan.sApprFull;
            sApprDriveBy.Checked = dataLoan.sApprDriveBy;
            sIsSpReviewNoAppr.Checked = dataLoan.sIsSpReviewNoAppr;
            sSpReviewFormNum.Text = dataLoan.sSpReviewFormNum;

            Tools.SetDropDownListValue(this.sQualTermCalculationType, dataLoan.sQualTermCalculationType);
            this.sQualTerm.Value = dataLoan.sQualTerm_rep;

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V12_CalculateLevelOfPropertyReviewSection))
            {
                if (dataLoan.sSpAppraisalFormT != E_sSpAppraisalFormT.Other)
                {
                    sSpReviewFormNum.ReadOnly = true;
                }
            }
            else
            {
                LoanVersion12Section.Visible = false;
            }
        }

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Transmittal_04));
            dataLoan.InitLoad();
            BindDataObject(dataLoan);
            sLpAusKeyLabel.Text = "LPA AUS Key #";
            sIsLpUw.Text = "LPA";
            sLpDocClassLabel.Text = "LPA Doc Class (Freddie)";
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
            aOccT.Attributes.Add("onchange","javascript:onChangeOccupancyStatus()");
            aOccT.Attributes.Add("onfocus","javascript:updateOccupancyStatusSavedValue()");
        }
    }
}
