﻿namespace LendersOfficeApp.newlos.Forms
{
    using System;
    using DataAccess;

    public class LenderCreditsServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(LenderCreditsServiceItem));
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            // Need this bypass for save too.
            dataLoan.ByPassFieldsBrokerLockAdjustXmlContent = true;

            string sLenderPaidBrokerCompF_rep = dataLoan.sLenderPaidBrokerCompF_rep;
            string sLenderPaidBrokerCompF_Neg_rep = dataLoan.sLenderPaidBrokerCompF_Neg_rep;
            string sLenderPaidBrokerCompCreditF_rep = dataLoan.sLenderPaidBrokerCompCreditF_rep;
            string sLenderPaidBrokerCompCreditF_Neg_rep = dataLoan.sLenderPaidBrokerCompCreditF_Neg_rep;
            SetResult("sLenderCreditCalculationMethodT", dataLoan.sLenderCreditCalculationMethodT);
            SetResult("sBrokerLockFinalBrokComp1PcPrice", dataLoan.sBrokerLockFinalBrokComp1PcPrice_rep);
            SetResult("sBrokerLockFinalBrokComp1PcAmt", dataLoan.sBrokerLockFinalBrokComp1PcAmt_rep);
            SetResult("sLenderPaidBrokerCompPc", dataLoan.sLenderPaidBrokerCompPc_rep);
            SetResult("sLenderPaidBrokerCompF", sLenderPaidBrokerCompF_rep);
            SetResult("sLenderPaidBrokerCompCreditF_Neg", sLenderPaidBrokerCompCreditF_Neg_rep);
            SetResult("sLDiscntBaseAmt", dataLoan.sLDiscntBaseAmt_rep);
            SetResult("sLDiscntBaseT2", dataLoan.sLDiscntBaseT);
            SetResult("sBrokerLockOriginatorPriceBrokComp1PcPrice", dataLoan.sBrokerLockOriginatorPriceBrokComp1PcPrice);
            SetResult("sBrokerLockOriginatorPriceBrokComp1PcAmt", dataLoan.sBrokerLockOriginatorPriceBrokComp1PcAmt_rep);

            SetResult("sLenderCreditMaxT", dataLoan.sLenderCreditMaxT);
            if (dataLoan.sLenderCreditMaxT == E_sLenderCreditMaxT.NoLimit)
            {
                SetResult("sLenderCreditMaxAmt_Neg", "");
            }
            else
            {
                SetResult("sLenderCreditMaxAmt_Neg", dataLoan.sLenderCreditMaxAmt_Neg_rep);
            }
            SetResult("sLenderCreditAvailableAmt_Neg", dataLoan.sLenderCreditAvailableAmt_Neg_rep);
            SetResult("sLenderPaidFeeDiscloseLocationT", dataLoan.sLenderPaidFeeDiscloseLocationT);
            SetResult("sLenderPaidFeesAmt_Neg", dataLoan.sLenderPaidFeesAmt_Neg_rep);
            SetResult("sLenderGeneralCreditDiscloseLocationT", dataLoan.sLenderGeneralCreditDiscloseLocationT);
            SetResult("sLenderGeneralCreditAmt_Neg", dataLoan.sLenderGeneralCreditAmt_Neg_rep);
            SetResult("sLenderCustomCredit1DiscloseLocationT", dataLoan.sLenderCustomCredit1DiscloseLocationT);
            SetResult("sLenderCustomCredit1Description", dataLoan.sLenderCustomCredit1Description);
            SetResult("sLenderCustomCredit1Amount", dataLoan.sLenderCustomCredit1Amount_rep);
            SetResult("sLenderCustomCredit2DiscloseLocationT", dataLoan.sLenderCustomCredit2DiscloseLocationT);
            SetResult("sLenderCustomCredit2Description", dataLoan.sLenderCustomCredit2Description);
            SetResult("sLenderCustomCredit2Amount", dataLoan.sLenderCustomCredit2Amount_rep );

            SetResult("sLenderCustomCredit1AmountAsCharge", dataLoan.sLenderCustomCredit1AmountAsCharge_rep);
            SetResult("sLenderCustomCredit2AmountAsCharge", dataLoan.sLenderCustomCredit2AmountAsCharge_rep);

            SetResult("sLenderTargetDiscountPointAmtAsCreditAmt_Neg", dataLoan.sLenderTargetDiscountPointAmtAsCreditAmt_Neg_rep);
            SetResult("sLenderInitialCreditAmt_Neg", dataLoan.sLenderInitialCreditAmt_Neg_rep);
            SetResult("sLenderTargetInitialCreditAmt_Neg", dataLoan.sLenderTargetInitialCreditAmt_Neg_rep);

            SetResult("sLenderLastDisclosedInitialCredit_Neg", dataLoan.sLenderLastDisclosedInitialCredit_Neg_rep);
            SetResult("sLenderCreditToleranceCureLckd", dataLoan.sLenderCreditToleranceCureLckd);
            SetResult("sLenderCreditToleranceCure_Neg", dataLoan.sLenderCreditToleranceCure_Neg_rep);
            SetResult("sLenderActualInitialCreditAmt_Neg", dataLoan.sLenderActualInitialCreditAmt_Neg_rep);
            SetResult("sLenderActualInitialCreditAmt_Neg2", dataLoan.sLenderActualInitialCreditAmt_Neg_rep);
            SetResult("sLenderTargetClosingCreditAmt_Neg", dataLoan.sLenderTargetClosingCreditAmt_Neg_rep);
            SetResult("sLenderTargetTotalCreditAmt_Neg", dataLoan.sLenderTargetTotalCreditAmt_Neg_rep);
            SetResult("sLenderAdditionalCreditAtClosingAmt_Neg", dataLoan.sLenderAdditionalCreditAtClosingAmt_Neg_rep);
            SetResult("sToleranceZeroPercentCure", dataLoan.sToleranceZeroPercentCure_rep);
            SetResult("sToleranceZeroPercentCure_Neg", dataLoan.sToleranceZeroPercentCure_Neg_rep);
            SetResult("sToleranceZeroPercentCureLckd", dataLoan.sToleranceZeroPercentCureLckd);
            SetResult("sToleranceCure_Neg", dataLoan.sToleranceCure_Neg_rep);
            SetResult("sToleranceTenPercentCure", dataLoan.sToleranceTenPercentCure_rep);
            SetResult("sToleranceTenPercentCure_Neg", dataLoan.sToleranceTenPercentCure_Neg_rep);
            SetResult("sToleranceTenPercentCureLckd", dataLoan.sToleranceTenPercentCureLckd);
            SetResult("sSettlementTotalPaidByLender_Neg", dataLoan.sSettlementTotalPaidByLender_Neg_rep);
            SetResult("sLenderPaidBrokerCompF_Neg", sLenderPaidBrokerCompF_Neg_rep);
            SetResult("sLenderActualTotalCreditAmt_Neg", dataLoan.sLenderActualTotalCreditAmt_Neg_rep);
            SetResult("sLenderTargetInitialCreditAmt_Neg2", dataLoan.sLenderTargetInitialCreditAmt_Neg_rep);
            SetResult("sLenderCreditDifferentFromTargetAmt_Neg", dataLoan.sLenderCreditDifferentFromTargetAmt_Neg_rep);
            SetResult("sLenderCreditOverageAmt", dataLoan.sLenderCreditOverageAmt_rep);
            
            // Manual Calculation mode.
            SetResult("sBrokerLockFinalBrokComp1PcPrice2", dataLoan.sBrokerLockFinalBrokComp1PcPrice_rep);
            SetResult("sBrokerLockFinalBrokComp1PcAmt2", dataLoan.sBrokerLockFinalBrokComp1PcAmt_rep);
            SetResult("sLenderPaidBrokerCompPc2", dataLoan.sLenderPaidBrokerCompPc_rep);
            SetResult("sLenderPaidBrokerCompF2", sLenderPaidBrokerCompF_rep);
            SetResult("sLenderPaidBrokerCompCreditF_Neg2", sLenderPaidBrokerCompCreditF_Neg_rep);
            SetResult("sBrokerLockOriginatorPriceBrokComp1PcPrice2", dataLoan.sBrokerLockOriginatorPriceBrokComp1PcPrice);
            SetResult("sBrokerLockOriginatorPriceBrokComp1PcAmt2", dataLoan.sBrokerLockOriginatorPriceBrokComp1PcAmt_rep);

            SetResult("sLDiscntFMb", dataLoan.sLDiscntFMb_rep);
            SetResult("sLDiscntPc", dataLoan.sLDiscntPc_rep);
            SetResult("sLDiscntBaseT", dataLoan.sLDiscntBaseT);
            SetResult("sLDiscnt", dataLoan.sLDiscnt_rep);

            SetResult("sGfeCreditLenderPaidItemT", dataLoan.sGfeCreditLenderPaidItemT);
            SetResult("sGfeCreditLenderPaidItemF", dataLoan.sGfeCreditLenderPaidItemF_rep);

            SetResult("sGfeLenderCreditF", dataLoan.sGfeLenderCreditF_rep);

            SetResult("sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt", dataLoan.sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt_rep);
            SetResult("sLenderCustomCredit1Description2", dataLoan.sLenderCustomCredit1Description);
            SetResult("sLenderCustomCredit1Amount2", dataLoan.sLenderCustomCredit1Amount_rep);
            SetResult("sLenderCustomCredit1AmountAsCharge2", dataLoan.sLenderCustomCredit1AmountAsCharge_rep);

            SetResult("sLenderCustomCredit2Description2", dataLoan.sLenderCustomCredit2Description);
            SetResult("sLenderCustomCredit2Amount2", dataLoan.sLenderCustomCredit2Amount_rep);
            SetResult("sLenderCustomCredit2AmountAsCharge2", dataLoan.sLenderCustomCredit2AmountAsCharge_rep);

            SetResult("sToleranceCureCalculationT", dataLoan.sToleranceCureCalculationT);
            SetResult("sToleranceZeroPercentCure2", dataLoan.sToleranceZeroPercentCure_rep);
            SetResult("sToleranceZeroPercentCure_Neg2", dataLoan.sToleranceZeroPercentCure_Neg_rep);
            SetResult("sToleranceZeroPercentCureLckd2", dataLoan.sToleranceZeroPercentCureLckd);
            SetResult("sToleranceTenPercentCure2", dataLoan.sToleranceTenPercentCure_rep);
            SetResult("sToleranceTenPercentCure_Neg2", dataLoan.sToleranceTenPercentCure_Neg_rep);
            SetResult("sToleranceTenPercentCureLckd2", dataLoan.sToleranceTenPercentCureLckd);

            SetResult("sLenderInitialCreditToBorrowerAmt_Neg", dataLoan.sLenderInitialCreditToBorrowerAmt_Neg_rep);
            SetResult("sGfeDiscountPointF", dataLoan.sGfeDiscountPointF_rep);
            SetResult("sGfeDiscountPointFPc", dataLoan.sGfeDiscountPointFPc_rep);
            SetResult("sToleranceCure_Neg2", dataLoan.sToleranceCure_Neg_rep);
            SetResult("sSettlementTotalPaidByLender_Neg2", dataLoan.sSettlementTotalPaidByLender_Neg_rep);
            SetResult("sLenderActualTotalCreditAmt_Neg2", dataLoan.sLenderActualTotalCreditAmt_Neg_rep);
            SetResult("sLenderPaidBrokerCompF_Neg2", sLenderPaidBrokerCompF_Neg_rep);
            SetResult("sLenderAdditionalCreditAtClosingManualAmt_Neg", dataLoan.sLenderAdditionalCreditAtClosingManualAmt_Neg_rep);
            SetResult("sLenderAdditionalCreditAtClosingAmt_Neg2", dataLoan.sLenderAdditionalCreditAtClosingAmt_Neg_rep);
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.ByPassFieldsBrokerLockAdjustXmlContent = true;

            dataLoan.sLenderCreditCalculationMethodT = GetEnum<E_sLenderCreditCalculationMethodT>("sLenderCreditCalculationMethodT");

            dataLoan.sLenderCreditMaxT = GetEnum<E_sLenderCreditMaxT>("sLenderCreditMaxT");
            dataLoan.sLenderCreditMaxAmt_Neg_rep = GetString("sLenderCreditMaxAmt_Neg");

            dataLoan.sLenderPaidFeeDiscloseLocationT = GetEnum<E_LenderCreditDiscloseLocationT>("sLenderPaidFeeDiscloseLocationT");
            dataLoan.sLenderGeneralCreditDiscloseLocationT = GetEnum<E_LenderCreditDiscloseLocationT>("sLenderGeneralCreditDiscloseLocationT");

            if (dataLoan.sLenderCreditCalculationMethodT == E_sLenderCreditCalculationMethodT.CalculateFromFrontEndRateLock)
            {
                dataLoan.sLDiscntBaseT = GetEnum<E_PercentBaseT>("sLDiscntBaseT2");

                dataLoan.sLenderCustomCredit1DiscloseLocationT = GetEnum<E_LenderCreditDiscloseLocationT>("sLenderCustomCredit1DiscloseLocationT");
                dataLoan.sLenderCustomCredit1Description = GetString("sLenderCustomCredit1Description");
                dataLoan.sLenderCustomCredit1Amount_rep = GetString("sLenderCustomCredit1Amount");

                dataLoan.sLenderCustomCredit2DiscloseLocationT = GetEnum<E_LenderCreditDiscloseLocationT>("sLenderCustomCredit2DiscloseLocationT");
                dataLoan.sLenderCustomCredit2Description = GetString("sLenderCustomCredit2Description");
                dataLoan.sLenderCustomCredit2Amount_rep = GetString("sLenderCustomCredit2Amount");

                dataLoan.sToleranceZeroPercentCure_rep = GetString("sToleranceZeroPercentCure");
                dataLoan.sToleranceZeroPercentCureLckd = GetBool("sToleranceZeroPercentCureLckd");

                dataLoan.sToleranceTenPercentCure_rep = GetString("sToleranceTenPercentCure");
                dataLoan.sToleranceTenPercentCureLckd = GetBool("sToleranceTenPercentCureLckd");

                dataLoan.sLenderCreditToleranceCureLckd = GetBool("sLenderCreditToleranceCureLckd");
            }
            else
            {
                dataLoan.sLDiscntPc_rep = GetString("sLDiscntPc");
                dataLoan.sLDiscntBaseT = GetEnum<E_PercentBaseT>("sLDiscntBaseT");
                dataLoan.sLDiscntFMb_rep = GetString("sLDiscntFMb");
                dataLoan.sGfeCreditLenderPaidItemT = GetEnum<E_CreditLenderPaidItemT>("sGfeCreditLenderPaidItemT");
                dataLoan.sLenderCustomCredit1Description = GetString("sLenderCustomCredit1Description2");
                dataLoan.sLenderCustomCredit1Amount_rep = GetString("sLenderCustomCredit1Amount2");

                dataLoan.sLenderCustomCredit2Description = GetString("sLenderCustomCredit2Description2");
                dataLoan.sLenderCustomCredit2Amount_rep = GetString("sLenderCustomCredit2Amount2");
                dataLoan.sToleranceCureCalculationT = GetEnum<E_sToleranceCureCalculationT>("sToleranceCureCalculationT");

                dataLoan.sToleranceZeroPercentCure_rep = GetString("sToleranceZeroPercentCure2");
                dataLoan.sToleranceZeroPercentCureLckd = GetBool("sToleranceZeroPercentCureLckd2");

                dataLoan.sToleranceTenPercentCure_rep = GetString("sToleranceTenPercentCure2");
                dataLoan.sToleranceTenPercentCureLckd = GetBool("sToleranceTenPercentCureLckd2");
            }

        }
    }

    public partial class LenderCreditsService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new LenderCreditsServiceItem());
        }
    }
}
