<%@ Page language="c#" Codebehind="AmortizationTable.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.AmortizationTable" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AmortTable" Src="~/newlos/Forms/AmortizationTableView.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
    <title>AmortizationTable</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <style type="text/css">
        #tab-list
        {
            background-color: transparent;
            border: none;
        }
        .ui-tabs-active
        {
            border: solid 1px black !important;
            border-bottom: none !important;
        }
        .tab, .ui-tabs, .ui-tabs-panel
        {
            padding-top: .25em !important;
        }
    </style>
  </head>
<body ms_positioning="FlowLayout" margin="0" bgcolor="gainsboro" scroll="no">

    <script type="text/javascript">
    function _init() {
        resize(630, 500);
        $('#tabs').tabs();
    }
    function f_print(file) {
        var url = 'pdf/' + file + '.aspx?loanid=' + ML.sLId;
        LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
    }
    </script>

    <h4 class="page-header">Amortization Table</h4>
    <form id="AmortizationTable" method="post" runat="server">
    <div id="tabs">
        <asp:PlaceHolder runat="server" ID="TabsPlaceHolder">
        <ul id="tab-list">
            <li class="tab"><a href="#standard-tab">Standard</a></li>
            <li class="tab"><a href="#best-case-tab">Best Case</a></li>
            <li class="tab"><a href="#worst-case-tab">Worst Case</a></li>
        </ul>
        </asp:PlaceHolder>
        
        <div id="standard-tab" class="tab">
            <uc1:AmortTable runat="server" ID="StandardAmortTable" ScheduleType="Standard"></uc1:AmortTable>
        </div>
        <asp:PlaceHolder runat="server" ID="BestWorstCasePlaceHolder">
        <div id="best-case-tab" class="tab">
            <uc1:AmortTable runat="server" ID="BestCaseAmortTable" ScheduleType="BestCase"></uc1:AmortTable>
        </div>
        <div id="worst-case-tab" class="tab">
            <uc1:AmortTable runat="server" ID="WorstCaseAmortTable" ScheduleType="WorstCase"></uc1:AmortTable>
        </div>
        </asp:PlaceHolder>
    </div>

    <uc1:cModalDlg ID="CModalDlg1" runat="server"></uc1:cModalDlg>
    </form>
</body>
</html>
