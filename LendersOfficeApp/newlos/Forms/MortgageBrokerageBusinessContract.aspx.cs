using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
	public partial class MortgageBrokerageBusinessContract : BaseLoanPage
	{
        protected System.Web.UI.WebControls.DropDownList DropDownList1;

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CMortgageBrokerageBusinessContractPDF);
        }
        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(MortgageBrokerageBusinessContract));
            dataLoan.InitLoad();

            sFloridaContractDays.Text = dataLoan.sFloridaContractDays_rep;
            sFloridaBorrEstimateSpVal.Text = dataLoan.sFloridaBorrEstimateSpVal_rep;
            sFloridaBorrEstimateExistingMBal.Text = dataLoan.sFloridaBorrEstimateExistingMBal_rep;
            sFloridaBorrDeposit.Text = dataLoan.sFloridaBorrDeposit_rep;
            sFloridaAdditionalCompMinR.Text = dataLoan.sFloridaAdditionalCompMinR_rep;
            sFloridaAdditionalCompMinMb.Text = dataLoan.sFloridaAdditionalCompMinMb_rep;
            sFloridaAdditionalCompMin.Text = dataLoan.sFloridaAdditionalCompMin_rep;
            sFloridaAdditionalCompMaxR.Text = dataLoan.sFloridaAdditionalCompMaxR_rep;
            sFloridaAdditionalCompMaxMb.Text = dataLoan.sFloridaAdditionalCompMaxMb_rep;
            sFloridaAdditionalCompMax.Text = dataLoan.sFloridaAdditionalCompMax_rep;
            sFloridaBrokerFeeR.Text = dataLoan.sFloridaBrokerFeeR_rep;
            sFloridaBrokerFeeMb.Text = dataLoan.sFloridaBrokerFeeMb_rep;
            sFloridaBrokerFee.Text = dataLoan.sFloridaBrokerFee_rep;

            sFloridaAppFee.Text = dataLoan.sFloridaAppFee_rep;

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.FloridaMortBrokerBusinessContract, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (preparer.IsValid) 
            {
                PreparerName.Text = preparer.PreparerName;
                CompanyName.Text = preparer.CompanyName;
                LicenseNumOfCompany.Text = preparer.LicenseNumOfCompany;
            }
            sFloridaIsAppFeeApplicableToCc.Checked = dataLoan.sFloridaIsAppFeeApplicableToCc;
            sFloridaIsAppFeeRefundable.SelectedIndex = dataLoan.sFloridaIsAppFeeRefundable ? 0 : 1;

            // I need this variable to calculate sFloridaBrokerFee, sFloridaAdditionalCompMin, sFloridaAdditionalCompMax.
            // WHy not use server service? Because it is not worth it to create server calculation for these values.
            ClientScript.RegisterHiddenField("sLAmt", dataLoan.sLAmtCalc_rep);

            
        }


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
            IsAppSpecific = false;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

	}
}
