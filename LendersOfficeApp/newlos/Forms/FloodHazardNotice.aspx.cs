using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
	public partial class FloodHazardNotice : BaseLoanPage
	{

        protected void PageInit(object sender, System.EventArgs e)
        {
            FloodHazardNoticeZip.SmartZipcode(FloodHazardNoticeCity, FloodHazardNoticeState);
            this.PageTitle = "Flood Hazard Notice";
            this.PageID = "FloodHazardNotice";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CFloodHazardNoticePDF);
            RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }
        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FloodHazardNotice));
            dataLoan.InitLoad();

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.FloodHazardNotice, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            FloodHazardNoticeCompanyName.Text = preparer.CompanyName;
            FloodHazardNoticeStreetAddr.Text = preparer.StreetAddr;
            FloodHazardNoticeCity.Text = preparer.City;
            FloodHazardNoticeState.Value = preparer.State;
            FloodHazardNoticeZip.Text = preparer.Zip;

            sFloodHazardBuilding.Checked = dataLoan.sFloodHazardBuilding;
            sFloodHazardMobileHome.Checked = dataLoan.sFloodHazardMobileHome;
            sFloodHazardCommunityDesc.Text = dataLoan.sFloodHazardCommunityDesc;

            sFloodCertId.Text = dataLoan.sFloodCertId;

            sFloodHazardFedInsAvail.Checked = dataLoan.sFloodHazardFedInsAvail;
            sFloodHazardFedInsNotAvail.Checked = dataLoan.sFloodHazardFedInsNotAvail;

        }

        protected override void SaveData() 
        {

        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
