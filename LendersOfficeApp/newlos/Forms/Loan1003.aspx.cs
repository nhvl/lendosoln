namespace LendersOfficeApp.newlos.Forms
{
    using System;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Pdf;

    public partial class Loan1003 : BaseLoanPage
	{
        protected Loan1003pg1 Loan1003pg1;
        protected Loan1003pg2 Loan1003pg2;
        protected Loan1003pg3 Loan1003pg3;
        protected Loan1003pg4 Loan1003pg4;

        internal static void BindDataFromQualRatePopup(CPageData loan, CAppData application, AbstractBackgroundServiceItem item)
        {
            QualRateCalculationPopup.BindData(loan, item);
        }

        internal static void LoadDataForQualRatePopup(CPageData loan, CAppData application, AbstractBackgroundServiceItem item)
        {
            QualRateCalculationPopup.LoadData(loan, item);
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            IsAppSpecific = true;
            Tabs.AddToQueryString("loanid", LoanID.ToString());
            Tabs.RegisterControl("Page 1", Loan1003pg1);
            Tabs.RegisterControl("Page 2", Loan1003pg2);
            Tabs.RegisterControl("Page 3", Loan1003pg3);
            Tabs.RegisterControl("Page 4", Loan1003pg4);

            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterCSS("jquery-ui-1.11.custom.css");

            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e) 
        {
            this.RegisterJsScript("DeclarationExplanation.js");
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("jquery.tmpl.js");

            int tabIndex = Tabs.TabIndex + 1;
            this.PageTitle = "1003 application page " +tabIndex ;
            this.PageID = "1003_" + tabIndex;
            switch (tabIndex) 
            {
                case 1:
                    this.PDFPrintClass = typeof(C1003_10_1PDF);
                    break;
                case 2:
                    this.PDFPrintClass = typeof(C1003_10_2PDF);
                    break;
                case 3:
                    this.PDFPrintClass = typeof(C1003_17_3PDF);
                    break;
                case 4:
                    this.PDFPrintClass = typeof(C1003_10_4PDF);
                    break;
            }

            base.OnPreRender(e);
        }

        protected override void LoadData()
        {
            var popupDependencies = CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(QualRateCalculationPopup));

            var loan = new CPageData(this.LoanID, nameof(Loan1003), popupDependencies.Union(new[] { nameof(CPageData.sIsRenovationLoan) }));
            loan.InitLoad();

            this.QualRatePopup.LoadData(loan);
            this.RegisterJsGlobalVariables("IsRenovationLoan", loan.sIsRenovationLoan);
            this.RegisterJsGlobalVariables("sIsIncomeCollectionEnabled", loan.sIsIncomeCollectionEnabled);
        }
    }
}
