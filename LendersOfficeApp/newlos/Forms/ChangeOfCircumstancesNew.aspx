﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangeOfCircumstancesNew.aspx.cs"
    Inherits="LendersOfficeApp.newlos.Forms.ChangeOfCircumstancesNew" %>
    
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>Change Of Circumstances</title>
</head>
<body bgcolor="gainsboro">
<style type="text/css">

/*<%//Get rid of all other padding and margins%>*/
*
{
    margin: 0;
    padding: 0;
}

/*<%//And then add our own%>*/
#MainContent
{
    position: relative;
    left: 10px;
}
.HideBullets
{
    list-style: none;
}
#DateEntry
{
    padding-top: 10px;
}
#DateEntry li
{
    width: 500px;
    padding-bottom: 10px;
}

#DateEntry li label
{
    float: left;
    font-weight: bold;
    width: 150px;
    margin-right: 5px;
}

.AlignedDate
{
    height: 20px;
}

/*<%//The calendar control creates an img that's not well-aligned %>*/
#DateEntry img
{
    vertical-align: bottom;
}

#sCircumstanceChangeExplanation
{
    width: 99.5%;
    height: 260px;
}

#Explanation
{
    width: 550px;
    height: 300px;
}
#Reasons
{
    width: 550px;
}
#ArchiveStatusUnknown, #ArchiveStatusPendingFromCoC, #ArchiveStatusPendingFromManual, #InvalidateUnknownArchive
{
    display:none;
}

#NoLastDisclosedArchiveOnFile
{
    display:none;
}
.no-close .ui-dialog-titlebar-close
{
    display: none;
}
.ui-dialog-buttonset .ui-button .ui-button-text
{
    display: inline-block;
    max-width: 340px;
}
#dialog-confirm img { float: left; padding-right: 5px; padding-bottom: 5px; }
</style>

<script type="text/javascript">
var selectedTabIndex = <%= AspxTools.JsNumeric(selectedTabIndex) %>;
jQuery(document).ready(function($)
{
    function displayNoLastDisclosedLoanEstimateArchive() {
        $('#NoLastDisclosedArchiveOnFile').dialog({
            closeOnEscape: false,
            dialogClass: 'LQBDialogBox no-close',
            modal: true,
            title: 'No Last Disclosed Loan Estimate',
            resizable: false,
            draggable: false,
            buttons: [
                {
                    text: 'OK',
                    click: function () {
                        onClosePopup();
                    }
                },
            ]
        });
    }

    function onPopupClose() {
        if (ArchiveStatusPopup.getMode() === ArchiveStatusPopupMode.Unknown &&
            ML && ML.BlockCoCForNoLastDisclosedLoanEstimate) {
            displayNoLastDisclosedLoanEstimateArchive();
        }
        else if (!ArchiveStatusPopup.getUserCancelled()) {
            window.location.href = window.location.href;
        }
    }

    if (ML && ML.BlockCoCForPendingArchiveFromCoC) {
        ArchiveStatusPopup.create(
            'ArchiveStatusPendingFromCoC',
            ML.sLId,
            null,
            ArchiveStatusPopupMode.PendingFromCoC,
            ArchiveStatusPopupSource.ChangeOfCircumstance,
            onPopupClose);
        ArchiveStatusPopup.displayPopup();
    } else if (ML && ML.BlockCoCForPendingArchiveFromManualArchive) {
        ArchiveStatusPopup.create(
            'ArchiveStatusPendingFromManual',
            ML.sLId,
            ML.PreventCoCIfNotProvidedToBorrower,
            ArchiveStatusPopupMode.PendingFromManual,
            ArchiveStatusPopupSource.ChangeOfCircumstance,
            onPopupClose);
        ArchiveStatusPopup.displayPopup();
    } else if (ML && ML.BlockCoCForArchiveWithUnknownStatus) {
        var confirmInvalidationDialog =  ML.sHasLoanEstimateArchiveInUnknownStatus ? 'InvalidateUnknownArchive' : null;
        var confirmInvalidationCheckboxId = ML.sHasLoanEstimateArchiveInUnknownStatus ? 'userAgreesToInvalidate' : null;
        var mode;

        if (ML.HasBothCDAndLEArchiveInUnknownStatus) {
            mode = ArchiveStatusPopupMode.HasBothCDAndLEArchiveInUnknownStatus;
        }
        else if (ML.sHasLoanEstimateArchiveInUnknownStatus && ML.UnknownLoanEstimateWasSourcedFromCoC) {
            mode = ArchiveStatusPopupMode.UnknownFromCoC;
        }
        else {
            mode = ArchiveStatusPopupMode.Unknown;
        }

        ArchiveStatusPopup.create(
            'ArchiveStatusUnknown',
            ML.sLId,
            ML.PreventCoCIfNotProvidedToBorrower,
            mode,
            ArchiveStatusPopupSource.ChangeOfCircumstance,
            onPopupClose,
            false,
            confirmInvalidationDialog,
            confirmInvalidationCheckboxId);
        ArchiveStatusPopup.displayPopup();
    } else if (ML && ML.BlockCoCForNoLastDisclosedLoanEstimate) {
        displayNoLastDisclosedLoanEstimateArchive();
    }



    $('#dialog-confirm').dialog({
        modal: true,
        buttons: {
            "Continue": function() {
                $(this).dialog("close");
                saveAndApplyChangeOfCircumstancesNewForm();
            },
            "Cancel": function() {
                $(this).dialog("close");
            }
        },
        open: function() {
            $(this).parents('.ui-dialog-buttonpane button:eq(1)').focus();
        },
        autoOpen: false,
        closeOnEscape: false,
        width: "400",
        draggable: false,
        dialogClass: "LQBDialogBox"
    });
    
    var ExplanationCharacters = 600;
    $("#ExplanationOfChange").keyup(MakeTextLimiter(ExplanationCharacters));

    //for some reason, print button is always disabled, enabled here
    $j("input[type='button'], button").each(function(){
        if(this.value == "Print"){
            this.disabled = false;
            return false;
            }
    });

    function MakeTextLimiter(maxLength)
    {
        return (function()
        {
            var text = $(this).val();
            if (text.length > maxLength)
            {
                $(this).val(text.substr(0, maxLength));
            }
        });
    }
    
    <%if(IsTabCreateNew){ %>
    EnableDisableSave();

        <%if (ComplianceEagleEnabled && m_isLoanEstimateMode) { %>
            $j('#ComplianceCOCCategory').prop("disabled", false);
        <%} %>
    <%} %>
    
    <%if(IsTabViewPast){ %>
        window.onunload = function(){};
    <%} %>

    CheckIfApplyIsEnabled();    
});


function onFeeClick() {
    var checked = "";
    
    $j("input[type='checkbox']:checked.lAmtFeeChk").each (function () {
        checked += ( checked == "" ? "": ",") + $j(this).attr("data1");
    });

    $j("input[type='checkbox']:checked.totLAmtFeeChk").each (function () {
        checked += ( checked == "" ? "": ",") + $j(this).attr("data1");
    });
    
    $j("input[type='checkbox']:checked.purchPriceFeeChk").each (function () {
        checked += ( checked == "" ? "": ",") + $j(this).attr("data1");
    });

    $j("input[type='checkbox']:checked.appraisalFeeChk").each (function () {
        checked += ( checked == "" ? "": ",") + $j(this).attr("data1");
    });

    $j("input[type='checkbox']:checked.reserveFeeChk").each (function () {
        checked += ( checked == "" ? "" : ",") + $j(this).attr("data1");
    });

    $j("input[type='checkbox']:checked.feechk").each (function () {
        checked += ( checked == "" ? "": ",") + $j(this).attr("data1");
    });
    
    document.getElementById('selectedFees').value = checked;
}

function onLoanAmtClick()
{
    var setVal = $j('#allLAmtFeeChk').prop('checked');
    $j("input[type='checkbox'].lAmtFeeChk").prop('checked', setVal);
    
    onFeeClick();
}

function onTotLoanAmtClick()
{
    var setVal = $j('#allTotLAmtFeeChk').prop('checked');
    $j("input[type='checkbox'].totLAmtFeeChk").prop('checked', setVal);
    
    onFeeClick();
}

function onPurchPriceClick()
{
    var setVal = $j('#allPurchPriceFeeChk').prop('checked');
    $j("input[type='checkbox'].purchPriceFeeChk").prop('checked', setVal);
    
    onFeeClick();
}

function onAppraisalClick()
{
    var setVal = $j('#allAppraisalFeeChk').prop('checked');
    $j("input[type='checkbox'].appraisalFeeChk").prop('checked', setVal);
    
    onFeeClick();
}

function onReserveClick()
{
    var setVal = $j('#allReserveFeeChk').prop('checked');
    $j("input[type='checkbox'].reserveFeeChk").prop('checked', setVal);

    onFeeClick();
}


<%if(IsTabCreateNew){ %>

function DisplayConfirmationDialog()
{
    if(AnyFeeCheckChecked())
    {
        saveAndApplyChangeOfCircumstancesNewForm();
    }
    else
    {
        $j('#dialog-confirm').dialog('open');
    }
}

function saveAndApplyChangeOfCircumstancesNewForm()
{
    var args = getAllFormValues();
    args.updateAllTotalLoanAmount = $j('#allTotLAmtFeeChk').prop('checked') || false;
    args.updateAllLoanAmount = $j('#allLAmtFeeChk').prop('checked') || false;
    args.updateAllPurchPrice = $j('#allPurchPriceFeeChk').prop('checked') || false;
    args.updateAllAppraisalFeeChk = $j('#allAppraisalFeeChk').prop('checked') || false;
    args.updateAllReserveFeeChk = $j('#allReserveFeeChk').prop('checked') || false;
    args.updateOnlyTotalLoanAmount = $j('#onlyTotLAmtFeeChk').prop('checked') || false;
    args.updateOnlyLoanAmount = $j('#onlyLAmtFeeChk').prop('checked') || false;
    args.updateOnlyPurchPrice = $j('#onlyPurchPriceFeeChk').prop('checked') || false;
    args.updateOnlyAppraisalFeeChk = $j('#onlyAppraisalFeeChk').prop('checked') || false;
    args.sLId = ML.sLId;
    args.selectedFees = document.getElementById('selectedFees').value;
    
    var result = gService.loanedit.call("SaveAndApplyCoC", args); 
    if (!result.error) {
        // switch to the view past tab if the save succeeded.
        if (window.opener) {
            var gfew = window.opener.top.frames["body"];
            if(gfew && gfew.insertAndSelectGFEArchiveDropDown)
            {
                gfew.insertAndSelectGFEArchiveDropDown(result.value.sLastDisclosedGFEArchiveD, gfew);

                // Want to refresh the tolerance indicator in the loan editor when the
                // user creates a CoC.
                if (window.opener.parent && window.opener.parent.info && window.opener.parent.info.f_refreshGfeToleranceInfo) {
                    window.opener.parent.info.f_refreshGfeToleranceInfo();
                }
            }
        }
        
        <%=AspxTools.JsGetElementById(hfSavedCoC) %>.value = "true";        

        //set the index to the past changes tab here maintab ref for vision
        __doPostBack('changeTab', '1');
    } else {
        alert(result.UserMessage);
    }  
}

function EnableDisableSave()
{
    var enableApply = true;
    var bApply = <%=AspxTools.JsGetElementById(bApply) %>;
    if (bApply) {
        bApply.disabled = enableApply ? false : 'disabled';
    }

}
<%} %>

function CheckIfApplyIsEnabled()
{
    var applyBtn = document.getElementById("bApply");
    if(applyBtn)
    {
        applyBtn.disabled = true;
    }
    else
    {
        return;
    }
        
    if(document.getElementById("GfePrepareDate").value &&
        document.getElementById("sCircumstanceChangeD").value &&
        <% if(!IsProtectDisclosureDates) { %>
        document.getElementById("sGfeRedisclosureD").value &&
        <% } %>
        document.getElementById("sCircumstanceChangeExplanation").value)
    {
        applyBtn.disabled = false;
    }

    <%if (ComplianceEagleEnabled && m_isLoanEstimateMode) { %>
    var complianceCategory = $j('#ComplianceCOCCategory').val();
    if (complianceCategory === '0') {
        applyBtn.disabled = true;
    }
    <% } %>
}

function AnyFeeCheckChecked()
{
    var numChecked = $j("input[type='checkbox']:checked.feechk").length
    + $j("input[type='checkbox']:checked.lAmtFeeChk").length
    + $j("input[type='checkbox']:checked.totLAmtFeeChk").length
    + $j("input[type='checkbox']:checked.purchPriceFeeChk").length
    + $j("input[type='checkbox']:checked.appraisalFeeChk").length
    + $j("input[type='checkbox']:checked.reserveFeeChk").length;
    
    return numChecked !== 0;
}


</script>

<form id="ChangeOfCircumstancesNew" runat="server">
<div id="ArchiveStatusPendingFromCoC">
    <ml:EncodedLiteral runat="server" ID="ArchiveStatusPendingFromCoCErrorMessage"></ml:EncodedLiteral>
</div>
<div id="ArchiveStatusPendingFromManual">
    <ml:EncodedLiteral runat="server" ID="ArchiveStatusPendingFromManualArchiveErrorMessage"></ml:EncodedLiteral>
</div>
<div id="ArchiveStatusUnknown">
    <ml:EncodedLiteral runat="server" ID="ArchiveStatusUnknownMessage"></ml:EncodedLiteral>
</div>
<div id="InvalidateUnknownArchive">
    <div>
        <ml:EncodedLiteral runat="server" ID="InvalidateUnknownArchiveMessage"></ml:EncodedLiteral>
    </div>
    <div>
        <input type="checkbox" id="userAgreesToInvalidate" /><label for="userAgreesToInvalidate">I have read the above.</label>
    </div>
</div>
<div id="NoLastDisclosedArchiveOnFile">
    <ml:EncodedLiteral runat="server" ID="NoLastDisclosedArchiveOnFileMessage"></ml:EncodedLiteral>
</div>
<div id="MainContent">
    <div class="MainRightHeader">
        <span>Change of Circumstances</span>
    </div>
    <asp:HiddenField runat="server" ID="hfSavedCoC" Value="false" />
    
    <div id="dialog-confirm" title="Continue with no selected fees?">
        <img src="../../images/warn.png">
        <p>
            No fees were selected for this Change of Circumstance. New disclosures will reflect the fees from the previous disclosure.
        </p>
    </div>
        
    <div class="Tabs">
        <ul class="tabnav">
            <li runat="server" id="tab0"><a href="#" onclick="__doPostBack('changeTab', '0');">Create New Change</a></li>
             <%if (m_hasChange ) { %>
                <li runat="server" id="tab1"><a href="#" onclick="__doPostBack('changeTab', '1');">View Past Changes</a></li>
            <% } else { %>
                <li runat="server" id="tabDummy"><a style="text-decoration:none;color: #ccc;" href="#">View Past Changes</a></li>
            <% } %>
        </ul>
    </div>
    
    <ul id="DateEntry" class="HideBullets">
        <%if (IsTabViewPast)
      { %>
        <li>
        <label class="DateEntryText">Select archived change:</label>
        <asp:DropDownList runat="server" ID="CoCArchives" AlwaysEnable onChange="__doPostBack('changeTab', '1');"></asp:DropDownList>
            <script>
                function openCoCPDF() {
                    <% if ( m_isLoanEstimateMode ) { %>
                    var page = '/pdf/ChangeOfCircumstancesNew.aspx';
                    <% } else { %>
                    var page = '/pdf/Change Of Circumstances.aspx';
                    <% } %>
                    var args = encodeURI('ArchiveDate=' +<%=AspxTools.JsGetElementById(CoCArchives) %>.value);
                    var url = gVirtualRoot + page + '?loanid=' + ML.sLId + "&" + args;
                    LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
                    return false;

                }
		    </script>
        <input type="button" style="padding: 2px;" onclick="openCoCPDF();" value="Print" AlwaysEnable="true" />
        </li>
    <%} %>
        <li id="GFE"><label class="DateEntryText" for="GfePrepareDate">Date of <% if ( m_isLoanEstimateMode ) { %> Loan Estimate <% } else { %> GFE Archive <%} %></label>
            <%//Have to wrap these DateTextBoxes in a div, otherwise the text field 
              //gets separated from the date button %>
            <div class="AlignedDate">
                <ml:DateTextBox ID="GfePrepareDate" runat="server" Width="75" preset="date" CssClass="mask"
                    onblur="CheckIfApplyIsEnabled();" onchange="CheckIfApplyIsEnabled();">
                </ml:DateTextBox>
                <%if (IsTabCreateNew){ %><img src="../../images/require_icon.gif"><%} %>
            </div> 
        </li>
        <li id="Change"><label class="DateEntryText" for="sCircumstanceChangeD">Date of Change</label>
            <div class="AlignedDate">
                <ml:DateTextBox ID="sCircumstanceChangeD" runat="server" Width="75" preset="date"
                    CssClass="mask" onblur="CheckIfApplyIsEnabled();" onchange="CheckIfApplyIsEnabled();">
                </ml:DateTextBox>
                <%if (IsTabCreateNew){ %><img src="../../images/require_icon.gif"><%} %>
            </div>
        </li>
        <li id="GFE_Redisc"><label class="DateEntryText" for="sGfeRedisclosureD">Date of Re-Disclosure </label>
            <div class="AlignedDate">
                <ml:DateTextBox ID="sGfeRedisclosureD" runat="server" Width="75" preset="date" CssClass="mask"
                    onblur="CheckIfApplyIsEnabled();" onchange="CheckIfApplyIsEnabled();">
                </ml:DateTextBox>
                <%if (IsTabCreateNew && !IsProtectDisclosureDates){ %><img src="../../images/require_icon.gif"><%} %>
            </div>
        </li>
        <%if ( ComplianceEagleEnabled && m_isLoanEstimateMode )
      { %>
        <li id="Category"><label class="DateEntryText" for="ComplianceCOCCategory">COC Category</label>
            <div class="AlignedDate">
                <asp:DropDownList runat="server" ID="ComplianceCOCCategory" onchange="CheckIfApplyIsEnabled();" ></asp:DropDownList>
                <%if (IsTabCreateNew){ %><img src="../../images/require_icon.gif"><%} %>
            </div>
        </li>
    <%} %>
    </ul>
    
    <div id="Explanation">
        <div class="FormTableSubheader">
            <span>Explanation of Change</span>
        </div>
        <textarea id="sCircumstanceChangeExplanation" style="float: left" runat="server"
            onblur="CheckIfApplyIsEnabled();"></textarea>
        <%if (IsTabCreateNew){ %><img style="position: absolute" src="../../images/require_icon.gif"><%} %>
    </div>
    <div id="Reasons">
        <div class="FormTableSubheader">
            <span>Fee changes associated with Changed Circumstances</span>
        </div>
        
        <div id="CalulatedReasons">
            <asp:PlaceHolder id="changeList" runat="server" ></asp:PlaceHolder>
        </div>
        

        <%if(IsTabCreateNew){ %>
          <div align="center">
            <asp:Button ID="bApply" Style="padding: 2px;" disabled="disabled" runat="server"
                Text="Apply" OnClientClick="DisplayConfirmationDialog(); return false;" />
            <input type="button" style="padding: 2px;" value="Cancel" onclick="onClosePopup();" />
          </div>
        <%} %>
        
    </div>
    <br />
    <br />
</div>
<input id="selectedFees" type="hidden" value="" />
</form>
    
</body>
</html>
