﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class AmortizationTableView : System.Web.UI.UserControl
    {
        private const string StandardPdf = "Amortization";
        private const string BestCasePdf = "BestCaseAmortization";
        private const string WorstCasePdf = "WorstCaseAmortization";

        [System.ComponentModel.Bindable(true)]
        public E_AmortizationScheduleT ScheduleType { get; set; }

        protected string PdfFileName
        {
            get
            {
                switch (this.ScheduleType)
                {
                    case E_AmortizationScheduleT.Standard:
                        return StandardPdf;
                    case E_AmortizationScheduleT.BestCase:
                        return BestCasePdf;
                    case E_AmortizationScheduleT.WorstCase:
                        return WorstCasePdf;
                    default:
                        throw new UnhandledEnumException(this.ScheduleType);
                }
            }
        }

        public IEnumerable<AmortItem> DataSource { get; set; }

        public override void DataBind()
        {
            this.m_amortizationDG.DataSource = this.DataSource;
            this.m_amortizationDG.DataBind();

            base.DataBind();
        }
    }
}