<%@ Import namespace="DataAccess"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Loan1003pg1.ascx.cs" Inherits="LendersOfficeApp.newlos.Forms.Loan1003pg1" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<script type="text/javascript">
  function displayConstruction() {
    var v = <%= AspxTools.JsGetElementById(sLPurposeT) %>.value;
    var bConstruction = v == <%= AspxTools.JsString(E_sLPurposeT.Construct) %> || v == <%= AspxTools.JsString(E_sLPurposeT.ConstructPerm) %>;
    var bRefinance = v == <%= AspxTools.JsString(E_sLPurposeT.Refin) %> || v == <%= AspxTools.JsString(E_sLPurposeT.RefinCashout)%> || v == <%= AspxTools.JsString(E_sLPurposeT.FhaStreamlinedRefinance)%> || v == <%= AspxTools.JsString(E_sLPurposeT.VaIrrrl) %> || v == <%= AspxTools.JsString(E_sLPurposeT.HomeEquity) %>;
    <%= AspxTools.JsGetElementById(sLotAcqYr) %>.readOnly = !bConstruction;
    <%= AspxTools.JsGetElementById(sLotAcquiredD) %>.readOnly = !bConstruction;
    <%= AspxTools.JsGetElementById(sLotOrigC) %>.readOnly = !bConstruction;
    <%= AspxTools.JsGetElementById(sLotLien) %>.readOnly = ML.AreConstructionLoanCalcsMigrated || !bConstruction;
    <%= AspxTools.JsGetElementById(sLotVal) %>.readOnly = !bConstruction;
    <%= AspxTools.JsGetElementById(sLotImprovC) %>.readOnly = ML.AreConstructionLoanCalcsMigrated || !bConstruction;
    <%= AspxTools.JsGetElementById(sSpAcqYr) %>.readOnly = !bRefinance;
    <%= AspxTools.JsGetElementById(sSpOrigC) %>.readOnly = !bRefinance;
    <%= AspxTools.JsGetElementById(sSpLien) %>.readOnly = !bRefinance;
    <%= AspxTools.JsGetElementById(sRefPurpose) %>.readOnly = !bRefinance;
    <%= AspxTools.JsGetElementById(sSpImprovDesc) %>.readOnly = !bRefinance;
    disableDDL(<%= AspxTools.JsGetElementById(sSpImprovTimeFrameT) %>, !bRefinance)
  
    <%= AspxTools.JsGetElementById(sSpImprovC) %>.readOnly = !bRefinance;

    $("#LotDateSpan").text(ML.AreConstructionLoanCalcsMigrated ? "Date Acquired" : "Yr Acquired");
    $("#sLotAcquiredDTd").toggle(ML.AreConstructionLoanCalcsMigrated);
    $("#sLotAcqYrTd").toggle(!ML.AreConstructionLoanCalcsMigrated);

    $(".sLotLien-span").toggle(!ML.AreConstructionLoanCalcsMigrated);
    $(".liabilities-link").toggle(ML.AreConstructionLoanCalcsMigrated);

    $(".sLotValTd").toggle(!ML.AreConstructionLoanCalcsMigrated);
    $(".sPresentValOfLotTd").toggle(ML.AreConstructionLoanCalcsMigrated);
  }

  function onCopyBorrowerClick() {
  
    if('' == <%= AspxTools.JsGetElementById(aCLastNm) %>.value)
        <%= AspxTools.JsGetElementById(aCLastNm) %>.value = <%= AspxTools.JsGetElementById(aBLastNm) %>.value;
    
    <%= AspxTools.JsGetElementById(aCHPhone) %>.value = <%= AspxTools.JsGetElementById(aBHPhone) %>.value;
    <%= AspxTools.JsGetElementById(aCMaritalStatT) %>.value = <%= AspxTools.JsGetElementById(aBMaritalStatT) %>.value;
    <%= AspxTools.JsGetElementById(aCAddr) %>.value = <%= AspxTools.JsGetElementById(aBAddr) %>.value;
    <%= AspxTools.JsGetElementById(aCCity) %>.value = <%= AspxTools.JsGetElementById(aBCity) %>.value;
    <%= AspxTools.JsGetElementById(aCState) %>.value = <%= AspxTools.JsGetElementById(aBState) %>.value;
    <%= AspxTools.JsGetElementById(aCZip) %>.value = <%= AspxTools.JsGetElementById(aBZip) %>.value;
    <%= AspxTools.JsGetElementById(aCAddrT) %>.value = <%= AspxTools.JsGetElementById(aBAddrT) %>.value;
    <%= AspxTools.JsGetElementById(aCAddrMailSourceT) %>.value = <%= AspxTools.JsGetElementById(aBAddrMailSourceT) %>.value;
    <%= AspxTools.JsGetElementById(aCAddrMail) %>.value = <%= AspxTools.JsGetElementById(aBAddrMail) %>.value;
    <%= AspxTools.JsGetElementById(aCCityMail) %>.value = <%= AspxTools.JsGetElementById(aBCityMail) %>.value;
    <%= AspxTools.JsGetElementById(aCStateMail) %>.value = <%= AspxTools.JsGetElementById(aBStateMail) %>.value;
    <%= AspxTools.JsGetElementById(aCZipMail) %>.value = <%= AspxTools.JsGetElementById(aBZipMail) %>.value;  
    <%= AspxTools.JsGetElementById(aCAddrYrs) %>.value = <%= AspxTools.JsGetElementById(aBAddrYrs) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev1Addr) %>.value = <%= AspxTools.JsGetElementById(aBPrev1Addr) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev1City) %>.value = <%= AspxTools.JsGetElementById(aBPrev1City) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev1State) %>.value = <%= AspxTools.JsGetElementById(aBPrev1State) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev1Zip) %>.value = <%= AspxTools.JsGetElementById(aBPrev1Zip) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev1AddrT) %>.value = <%= AspxTools.JsGetElementById(aBPrev1AddrT) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev1AddrYrs) %>.value = <%= AspxTools.JsGetElementById(aBPrev1AddrYrs) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev2Addr) %>.value = <%= AspxTools.JsGetElementById(aBPrev2Addr) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev2City) %>.value = <%= AspxTools.JsGetElementById(aBPrev2City) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev2State) %>.value = <%= AspxTools.JsGetElementById(aBPrev2State) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev2Zip) %>.value = <%= AspxTools.JsGetElementById(aBPrev2Zip) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev2AddrT) %>.value = <%= AspxTools.JsGetElementById(aBPrev2AddrT) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev2AddrYrs) %>.value = <%= AspxTools.JsGetElementById(aBPrev2AddrYrs) %>.value;
    updateMailingAddressFields();
    updateDirtyBit();
  }	
  function fillPresentAddress() {
    <%= AspxTools.JsGetElementById(aBAddr) %>.value = <%= AspxTools.JsGetElementById(sSpAddr) %>.value;
    <%= AspxTools.JsGetElementById(aBCity) %>.value = <%= AspxTools.JsGetElementById(sSpCity) %>.value;
    <%= AspxTools.JsGetElementById(aBZip) %>.value = <%= AspxTools.JsGetElementById(sSpZip) %>.value;
    <%= AspxTools.JsGetElementById(aBState) %>.value = <%= AspxTools.JsGetElementById(sSpState) %>.value;
    updateDirtyBit();
  }
  function syncMailingAddress() { 
    var source = $(<%= AspxTools.JsGetElementById(aBAddrMailSourceT) %>).val();
    switch(source)
    {
        case "0":   // PresentAddress
            <%= AspxTools.JsGetElementById(aBAddrMail) %>.value = <%= AspxTools.JsGetElementById(aBAddr) %>.value;
            <%= AspxTools.JsGetElementById(aBCityMail) %>.value = <%= AspxTools.JsGetElementById(aBCity) %>.value;
            <%= AspxTools.JsGetElementById(aBStateMail) %>.value = <%= AspxTools.JsGetElementById(aBState) %>.value;
            <%= AspxTools.JsGetElementById(aBZipMail) %>.value = <%= AspxTools.JsGetElementById(aBZip) %>.value;
            break;
        case "1":   // SubjectPropertyAddress
            <%= AspxTools.JsGetElementById(aBAddrMail) %>.value = <%= AspxTools.JsGetElementById(sSpAddr) %>.value;
            <%= AspxTools.JsGetElementById(aBCityMail) %>.value = <%= AspxTools.JsGetElementById(sSpCity) %>.value;
            <%= AspxTools.JsGetElementById(aBStateMail) %>.value = <%= AspxTools.JsGetElementById(sSpState) %>.value;
            <%= AspxTools.JsGetElementById(aBZipMail) %>.value = <%= AspxTools.JsGetElementById(sSpZip) %>.value;
            break;
    }

    source = $(<%= AspxTools.JsGetElementById(aCAddrMailSourceT) %>).val();
    switch(source)
    {
        case "0":   // PresentAddress
            <%= AspxTools.JsGetElementById(aCAddrMail) %>.value = <%= AspxTools.JsGetElementById(aCAddr) %>.value;
            <%= AspxTools.JsGetElementById(aCCityMail) %>.value = <%= AspxTools.JsGetElementById(aCCity) %>.value;
            <%= AspxTools.JsGetElementById(aCStateMail) %>.value = <%= AspxTools.JsGetElementById(aCState) %>.value;
            <%= AspxTools.JsGetElementById(aCZipMail) %>.value = <%= AspxTools.JsGetElementById(aCZip) %>.value;
            break;
        case "1":   // SubjectPropertyAddress
            <%= AspxTools.JsGetElementById(aCAddrMail) %>.value = <%= AspxTools.JsGetElementById(sSpAddr) %>.value;
            <%= AspxTools.JsGetElementById(aCCityMail) %>.value = <%= AspxTools.JsGetElementById(sSpCity) %>.value;
            <%= AspxTools.JsGetElementById(aCStateMail) %>.value = <%= AspxTools.JsGetElementById(sSpState) %>.value;
            <%= AspxTools.JsGetElementById(aCZipMail) %>.value = <%= AspxTools.JsGetElementById(sSpZip) %>.value;
            break;
    }

    updateMailingAddressFields();
  }
  function updateMailingAddressFields() {
    var b = $(<%= AspxTools.JsGetElementById(aBAddrMailSourceT) %>).val() != "2";
    <%= AspxTools.JsGetElementById(aBAddrMail) %>.readOnly = b;
    <%= AspxTools.JsGetElementById(aBCityMail) %>.readOnly = b;
    disableDDL(<%= AspxTools.JsGetElementById(aBStateMail) %>, b);
    <%= AspxTools.JsGetElementById(aBZipMail) %>.readOnly = b;

    b = $(<%= AspxTools.JsGetElementById(aCAddrMailSourceT) %>).val() != "2";
    <%= AspxTools.JsGetElementById(aCAddrMail) %>.readOnly = b;
    <%= AspxTools.JsGetElementById(aCCityMail) %>.readOnly = b;
    disableDDL(<%= AspxTools.JsGetElementById(aCStateMail) %>, b);
    <%= AspxTools.JsGetElementById(aCZipMail) %>.readOnly = b;
  }

  function _init() {
    var purpose = <%= AspxTools.JsGetElementById(sLPurposeT) %>.value;
    var bPurchase = ( purpose == <%= AspxTools.JsString(E_sLPurposeT.Purchase.ToString("D")) %>
    || purpose == <%= AspxTools.JsString(E_sLPurposeT.Construct.ToString("D")) %>
    || purpose == <%= AspxTools.JsString(E_sLPurposeT.ConstructPerm.ToString("D")) %>
    );
    <%= AspxTools.JsGetElementById(sPurchPrice) %>.readOnly = (ML.AreConstructionLoanCalcsMigrated && ML.IsConstructionLoan) || !bPurchase;
      <%= AspxTools.JsGetElementById(sLAmtLckd) %>.disabled = !bPurchase || ML.IsRenovationLoan;  

  	if(<%= AspxTools.JsGetElementById(sLAmtLckd) %>.checked) {
		  <%= AspxTools.JsGetElementById(sDownPmtPc) %>.readOnly = true;
		  <%= AspxTools.JsGetElementById(sEquity) %>.readOnly = true;
		  <%= AspxTools.JsGetElementById(sLAmt) %>.readOnly = false;
	  } else {
  		<%= AspxTools.JsGetElementById(sDownPmtPc) %>.readOnly = false;
	  	<%= AspxTools.JsGetElementById(sEquity) %>.readOnly = false;
		  <%= AspxTools.JsGetElementById(sLAmt) %>.readOnly = true;
	  }
    updateMailingAddressFields();
  
    displayOther(<%= AspxTools.JsGetElementById(sLT) %>, <%= AspxTools.JsString(E_sLT.Other.ToString("D")) %>, <%= AspxTools.JsGetElementById(sLTODesc) %>);
    displayOther(<%= AspxTools.JsGetElementById(sLPurposeT) %>, <%= AspxTools.JsString(E_sLPurposeT.Other.ToString("D")) %>, <%= AspxTools.JsGetElementById(sOLPurposeDesc) %>);
    
    displayConstruction();
    
    <%= AspxTools.JsGetElementById(sLeaseHoldExpireD) %>.readOnly = <%= AspxTools.JsGetElementById(sEstateHeldT) %>.selectedIndex == 0;  
    on_sFinMethodPrintAsOtherClick();
    <%= AspxTools.JsGetElementById(sOccR) %>.readOnly = <%= AspxTools.JsGetElementById(sOccRLckd) %>.checked == false;
    lockField(<%= AspxTools.JsGetElementById(sLenderCaseNumLckd) %>, 'sLenderCaseNum');
    lockField(<%= AspxTools.JsGetElementById(aTitleNm1Lckd) %>, 'aTitleNm1');
    lockField(<%= AspxTools.JsGetElementById(aTitleNm2Lckd) %>, 'aTitleNm2');
    lockField(<%=AspxTools.JsGetElementById(this.sQualIRLckd)%>, 'sQualIR');
    
    showHideCashOut();

    SetQualTermReadonly();
  }

    function SetQualTermReadonly() {
        var isManual = $(".sQualTermCalculationType").val() == '<%=AspxTools.JsStringUnquoted(QualTermCalculationType.Manual)%>';
        $(".sQualTerm").prop("readonly", !isManual);
    }
  
  function updateBorrowerName() {
    var name = <%= AspxTools.JsGetElementById(aBLastNm) %>.value + ", " + <%= AspxTools.JsGetElementById(aBFirstNm) %>.value;
    parent.info.f_updateApplicantDDL(name, <%= AspxTools.JsString(ApplicationID) %>);
  }
  
  function doAfterDateFormat(object)
  {
	if(object.id == (ML._ClientID + "_aBDob"))
	{
	calculateAge(object, "aBAge");
	}
	else if (object.id == (ML._ClientID + "_aCDob"))
	{
		calculateAge(object, "aCAge");
	}
		
  }

  function calculateAge(dob, age) {
    var args = new Object();
    args["dob"] = dob.value;
    args["age"] = document.getElementById(ML._ClientID + "_" + age).value;
    args["_ClientID"] = ML._ClientID
    var result = gService.loanedit.call(ML._ClientID + "_CalculateAge", args);
    if (!result.error) {
      if (result.value["age"] != null) {
        document.getElementById(ML._ClientID + "_" + age).value = result.value["age"];
      }
    }
  }
  function on_sFinMethodPrintAsOtherClick() {
    if (<%= AspxTools.JsGetElementById(sFinMethodPrintAsOther) %>.checked) {
      <%= AspxTools.JsGetElementById(sFinMethPrintAsOtherDesc) %>.readOnly = false;
    } else {
      <%= AspxTools.JsGetElementById(sFinMethPrintAsOtherDesc) %>.readOnly = true;
      <%= AspxTools.JsGetElementById(sFinMethPrintAsOtherDesc) %>.value = "";
    
    }
  }
function displayOther(control, value, otherField) {
    if (control.value == value) {
        otherField.readOnly=false;
    } else {
        otherField.value = "";
        otherField.readOnly=true;
    }
}
function combineName(first, last, middle, suffix) {
  var name = first;
  if (middle.replace(/^[\s]+/g,"").length > 0) name += " " + middle;
  if (last.replace(/^[\s]+/g,"").length > 0) name += " " + last;
  if (suffix.replace(/^[\s]+/g,"").length > 0) name += ", " + suffix;
  
  return name;
  
}
function on_sDownPmtPc_change(event) {
    var a = parseMoneyFloat(<%= AspxTools.JsString(sApprVal) %>);
    var p = parseMoneyFloat(<%= AspxTools.JsGetElementById(sPurchPrice) %>.value);
    var sLPurposeTValue = <%= AspxTools.JsGetElementById(sLPurposeT) %>.value;
    if ( !ML.AreConstructionLoanCalcsMigrated
        && (sLPurposeTValue == <%= AspxTools.JsString(E_sLPurposeT.Construct) %> 
      || sLPurposeTValue == <%= AspxTools.JsString(E_sLPurposeT.ConstructPerm) %> ))
    {
        p = p + parseMoneyFloat(<%= AspxTools.JsGetElementById(sLandCost) %>.value);    
    }
    
    var d = parseFloat(<%= AspxTools.JsGetElementById(sDownPmtPc) %>.value);
    
    var minPurchaseOrAppraisal = p;
    if (a > 0 && a < p)
    {
      minPurchaseOrAppraisal = a;
    }
    
    var e = ((d / 100.0) * minPurchaseOrAppraisal) + 0.005;
    var ctl = <%= AspxTools.JsGetElementById(sEquity) %>;
    ctl.value = e;
    format_money(ctl);
		updateDirtyBit(event);
		refreshCalculation();
}

function f_sendMail(objId){
    var obj = document.getElementById(objId);
    try{
        var to =  obj ? obj.value : "";
        window.open('mailto:' + to);
    }catch(e){}
    return false;
}

function showHideCashOut()
{
    var sRefPurposeClientID = '<%= AspxTools.HtmlString(sRefPurpose.ClientID) %>';
    var sLT = document.getElementById('<%= AspxTools.HtmlString(sLT.ClientID) %>');
    var sRefPurpose = document.getElementById(sRefPurposeClientID);
   
    // Get No Cash-Out Item
    var item = null;
    var tbl = document.getElementById(sRefPurposeClientID + '_cbl');
    for (var i=0; i < tbl.rows.length; i++)
    {
        if (tbl.rows[i].cells[0].innerText.toUpperCase() === 'NO CASH-OUT RATE/TERM')
        {
            item = tbl.rows[i];
            break;
        }
    }
    
    // Show/Hide Item
    if (item && sLT.options[sLT.selectedIndex].text.toUpperCase() === 'CONVENTIONAL'
        && sRefPurpose.value.toUpperCase() !== 'NO CASH-OUT RATE/TERM' )
    {
        item.style.visibility = 'hidden';
    }
    else
    {
        item.style.visibility = 'visible';
    }
    
    return false;
}
</script>


<table id="Table8" cellSpacing="0" cellPadding="0" border="0">
	<tr>
		<td><asp:checkbox id="sMultiApps" tabIndex="2" ToolTip="The income or assets of a person other than the &quot;Borrower&quot; (including the Borrower's spouse) will be used as a basis for loan qualification." Height="18px" runat="server" Text='The income or assets of a person other than the "Borrower"  will be used...' EnableViewState="False"></asp:checkbox></td>
	</tr>
	<tr>
		<td><asp:checkbox id="aSpouseIExcl" tabIndex="4" ToolTip="The income or assets of the Borrower's spouse will not be used as a basis for loan qualification, but his or her liabilities must be considered because the Borrower resides in a community property state, the security property is located in a community property state, or the Borrower is relying on other property located in a community property state as a basis for repayment of the loan." runat="server" Text="The income or assets of the Borrower's spouse will not be used ..." EnableViewState="False"></asp:checkbox></td>
	</tr>
	<tr>
		<td class="LoanFormHeader">I. TYPE OF MORTGAGE AND TERMS OF LOAN</td>
	</tr>
	<tr>
		<td>
			<table class="LoanForm" id="Table1" cellSpacing="0" cellPadding="0" border="0">
				<tr>
					<td class="FieldLabel">Mortgage Applied for:</td>
					<td class="FieldLabel" vAlign="top">Agency Case Number</td>
					<td class="FieldLabel" vAlign="top">Lender Case Number</td>
				</tr>
				<tr>
					<td><asp:dropdownlist id="sLT" tabIndex="6" runat="server" EnableViewState="False" onchange="showHideCashOut();refreshCalculation();"></asp:dropdownlist><asp:textbox id="sLTODesc" tabIndex="8" runat="server" MaxLength="21"></asp:textbox></td>
					<td vAlign="top"><asp:textbox id="sAgencyCaseNum" tabIndex="10" runat="server" MaxLength="21"></asp:textbox></td>
					<td vAlign="top" class="FieldLabel"><asp:textbox id="sLenderCaseNum" tabIndex="12" runat="server" MaxLength="21"></asp:textbox><asp:CheckBox ID="sLenderCaseNumLckd" TabIndex="12" runat="server" onclick="refreshCalculation();"/>Lock</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table class="InsetBorder" id="Table2" cellSpacing="0" cellPadding="0" width="99%" border="0">
				<tr>
					<td class="FieldLabel">Purchase Price</td>
					<td><ml:moneytextbox id="sPurchPrice" tabIndex="14" runat="server" onchange="refreshCalculation();" preset="money" width="90"></ml:moneytextbox></td>
					<td class="FieldLabel"><A title="Go to Upfront MIP/FF screen" tabIndex="25" href="javascript:linkMe('../LoanInfo.aspx?pg=1');" >Upfront MIP / FF</A></td>
					<td><ml:moneytextbox id="sFfUfmipFinanced" tabIndex="26" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></td>
					<td class="FieldLabel">Term</td>
					<td><asp:textbox id="sTerm" tabIndex="34" runat="server" onchange="refreshCalculation();" MaxLength="3" Width="42"></asp:textbox>&nbsp;months</td>
				</tr>
				<tr>
					<td class="FieldLabel">Down Payment %</td>
					<td><ml:percenttextbox id="sDownPmtPc" tabIndex="16" runat="server" onchange="on_sDownPmtPc_change(event);" preset="percent" width="70"></ml:percenttextbox></td>
					<td class="FieldLabel">Total Loan Amt</td>
					<td><ml:moneytextbox id="sFinalLAmt" tabIndex="28" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></td>
					<td class="FieldLabel">Due</td>
					<td><asp:textbox id="sDue" tabIndex="36" runat="server" onchange="refreshCalculation();" MaxLength="3" Width="42px"></asp:textbox>&nbsp;months</td>
				</tr>
				<tr>
					<td class="FieldLabel">Equity / Down Pmt</td>
					<td><ml:moneytextbox id="sEquity" tabIndex="18" runat="server" onchange="refreshCalculation();" preset="money" width="90"></ml:moneytextbox></td>
					<td class="FieldLabel">Note&nbsp;Rate</td>
					<td><ml:percenttextbox id="sNoteIR" tabIndex="30" runat="server" onchange="refreshCalculation();" preset="percent" width="70"></ml:percenttextbox></td>
					<td class="FieldLabel"><A title="Go to Truth-In-Lending" tabIndex="38" href="javascript:linkMe('TruthInLending.aspx');" disableforlead>Monthly 
							Pmt</A></td>
					<td><ml:moneytextbox id="sProThisMPmt" tabIndex="40" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></td>
				</tr>
				<tr>
					<TD class="FieldLabel" noWrap>Loan Amt
						<asp:CheckBox id="sLAmtLckd" Text="lock" runat="server" onclick="refreshCalculation();" tabindex="24"></asp:CheckBox></TD>
					<td><ml:moneytextbox id="sLAmt" tabIndex="24" runat="server" onchange="refreshCalculation();" preset="money" width="90"></ml:moneytextbox></td>
					<td class="FieldLabel"><a id="QualRateCalcLink">Qualifying Rate</a></td>
					<td>
                        <ml:percenttextbox id="sQualIR" tabIndex="32" runat="server" onchange="refreshCalculation();" preset="percent" width="70"></ml:percenttextbox>
                        <asp:CheckBox ID="sQualIRLckd" runat="server" Text="Lock" onclick="refreshCalculation();" />
					</td>
				</tr>
                <tr>
                    <td class="FieldLabel">
                        Qualifying Term
					</td>
					<td colspan="5">
                        <asp:DropDownList id="sQualTermCalculationType" runat="server" onchange="refreshCalculation();" CssClass="sQualTermCalculationType" />
                        &nbsp
                        <input type="text" id="sQualTerm" runat="server" preset="numeric" onchange="refreshCalculation();" class="sQualTerm" />
					</td>
                </tr>
				<TR>
					<TD class="FieldLabel">Amort. Type</TD>
					<TD class="FieldLabel" colspan="5"><asp:dropdownlist id="sFinMethT" tabIndex="42" runat="server" EnableViewState="False" onchange="refreshCalculation();"></asp:dropdownlist><asp:textbox id="sFinMethDesc" tabIndex="44" runat="server" MaxLength="50"></asp:textbox></TD>
				</TR>
				<tr>
					<td class="FieldLabel"></td>
					<td class="FieldLabel" colspan="5"><asp:CheckBox id="sFinMethodPrintAsOther" tabIndex="44" Text="Other" runat="server" onclick="on_sFinMethodPrintAsOtherClick();"></asp:CheckBox>&nbsp;-
						<asp:textbox id="sFinMethPrintAsOtherDesc" tabIndex="44" runat="server" MaxLength="50"></asp:textbox></td>
				</tr>
			</table>
		</td>
	</tr>
	<TR>
		<TD class="LoanFormHeader">II. PROPERTY INFORMATION AND PURPOSE OF LOAN</TD>
	</TR>
	<tr>
		<td vAlign="top">
			<table class="InsetBorder" id="Table13" cellSpacing="0" cellPadding="0" width="99%" border="0">
				<tr>
					<td>
						<table id="Table3" style="HEIGHT: 43px" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="FieldLabel">Street</td>
								<td class="FieldLabel"><asp:textbox id="sSpAddr" tabIndex="46" runat="server" MaxLength="60" Width="271px"></asp:textbox></td>
							</tr>
							<tr>
								<td class="FieldLabel">City</td>
								<td class="FieldLabel"><asp:textbox id="sSpCity" tabIndex="48" runat="server" MaxLength="72"></asp:textbox><ml:statedropdownlist id="sSpState" tabIndex="50" runat="server"></ml:statedropdownlist><ml:zipcodetextbox id="sSpZip" tabIndex="52" runat="server" preset="zipcode" width="50" CssClass="mask"></ml:zipcodetextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel">County</td>
								<td class="FieldLabel"><asp:DropDownList id="sSpCounty" tabIndex="54" runat="server" MaxLength="36"></asp:DropDownList>&nbsp;&nbsp;No. of 
									Units
									<asp:textbox id="sUnitsNum" tabIndex="56" runat="server" MaxLength="4" Width="33px"></asp:textbox>&nbsp;&nbsp;Year 
									Built
									<asp:textbox id="sYrBuilt" tabIndex="58" Height="20px" runat="server" MaxLength="4" Width="50px"></asp:textbox></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="FieldLabel">Legal Description</td>
				</tr>
				<tr>
					<td class="FieldLabel"><asp:textbox id="sSpLegalDesc" tabIndex="60" Height="65px" runat="server" Width="481px" TextMode="MultiLine"></asp:textbox></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table class="InsetBorder" id="Table4" cellSpacing="0" cellPadding="0" width="99%" border="0">
				<tr>
					<td class="FieldLabel" noWrap>Purpose of Loan</td>
					<td colSpan="2"><asp:dropdownlist id="sLPurposeT" tabIndex="62" runat="server" EnableViewState="False" onchange="refreshCalculation();"></asp:dropdownlist><asp:textbox id="sOLPurposeDesc" tabIndex="64" Height="20px" runat="server" MaxLength="21" Width="139px"></asp:textbox></td>
					<TD></TD>
				</tr>
				<tr>
					<td class="FieldLabel" noWrap>Property will be</td>
					<td><asp:dropdownlist id="aOccT" tabIndex="66" Height="18px" runat="server" EnableViewState="False" Width="155px"></asp:dropdownlist></td>
					<td class="FieldLabel" noWrap>Rental occupancy rate
					  <asp:CheckBox id="sOccRLckd" tabIndex="68" runat="server" Text="Lock" onclick="refreshCalculation();"/>
						<ml:percenttextbox id="sOccR" tabIndex="68" runat="server" preset="percent" width="70" /></td>
					<td class="FieldLabel" noWrap>Gross rent</td>
					<td class="FieldLabel" nowrap>
					<ml:moneytextbox id="sSpGrossRent" tabIndex="80" runat="server" preset="money" width="90" /></td>
					<td></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table class="InsetBorder" id="Table12" cellSpacing="0" cellPadding="0" width="99%" border="0">
				<tr>
					<td class="FormTableSubHeader" colSpan="6">Complete this line if construction or 
						construction-permanent loan</td>
				</tr>
				<tr>
					<td class="FieldLabel" title="Year Lot Acquired"><span id="LotDateSpan" /></td>
					<td class="FieldLabel">Original Cost</td>
                    <td class="FieldLabel">
                        <span class="sLotLien-span">Existing Liens</span>
                        <a class="liabilities-link">Existing Liens</a>
					</td>
					<td class="FieldLabel">(a) Lot Value</td>
					<td class="FieldLabel" title="Cost of Improvements">(b) Improvements</td>
					<td class="FieldLabel">Total (a + b)</td>
				</tr>
				<tr>
					<td vAlign="top" id="sLotAcqYrTd" title="Year Lot Acquired">
                        <asp:textbox id="sLotAcqYr" tabIndex="82" Height="20px" runat="server" MaxLength="4" Width="63px"></asp:textbox>
					</td>
                    <td vAlign="top" id="sLotAcquiredDTd" class="date-td" title="Date Lot Acquired">
                        <ml:DateTextBox TabIndex="83" ID="sLotAcquiredD" runat="server"></ml:DateTextBox>
					</td>
					<td vAlign="top"><ml:moneytextbox id="sLotOrigC" tabIndex="84" runat="server" preset="money" width="90"></ml:moneytextbox></td>
					<td vAlign="top"><ml:moneytextbox id="sLotLien" tabIndex="86" runat="server" preset="money" width="90"></ml:moneytextbox></td>
					<td vAlign="top" class="sLotValTd"><ml:moneytextbox id="sLotVal" tabIndex="88" runat="server" onchange="refreshCalculation();" preset="money" width="90"></ml:moneytextbox></td>
                    <td vAlign="top" class="sPresentValOfLotTd"> <ml:MoneyTextBox ID="sPresentValOfLot" Readonly="true" TabIndex="88" runat="server" onchange="refreshCalculation();" preset="money" Width="90"></ml:MoneyTextBox></td>
					<td vAlign="top" title="Cost of Improvements"><ml:moneytextbox id="sLotImprovC" tabIndex="90" runat="server" onchange="refreshCalculation();" preset="money" width="90"></ml:moneytextbox></td>
					<td vAlign="top"><ml:moneytextbox id="sLotWImprovTot" tabIndex="92" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox>
					<asp:HiddenField id="sLandCost" runat="server" Value="0" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table class="InsetBorder" id="Table7" cellSpacing="0" cellPadding="0" width="99%" border="0">
				<tr>
					<td class="FormTableSubHeader" colSpan="4">Complete this line if this is a 
						refinance loan</td>
				</tr>
				<tr>
					<td class="FieldLabel">Yr Acquired</td>
					<td class="FieldLabel">Original Cost</td>
					<td class="FieldLabel">Existing Liens</td>
					<td class="FieldLabel">Purpose of Refinance</td>
				</tr>
				<tr>
					<td vAlign="top"><asp:textbox id="sSpAcqYr" tabIndex="94" runat="server" MaxLength="4" Width="63px"></asp:textbox></td>
					<td vAlign="top"><ml:moneytextbox id="sSpOrigC" tabIndex="96" runat="server" preset="money" width="90"></ml:moneytextbox></td>
					<td vAlign="top"><ml:moneytextbox id="sSpLien" tabIndex="98" runat="server" preset="money" width="90"></ml:moneytextbox></td>
					<td class="FieldLabel" vAlign="top" noWrap><ml:combobox id="sRefPurpose" tabIndex="100" runat="server" Width="200px"></ml:combobox></td>
				</tr>
				<tr>
					<td class="FieldLabel" vAlign="top" colSpan="4">Describe Improvements&nbsp;<asp:textbox id="sSpImprovDesc" tabIndex="102" runat="server" MaxLength="60" Width="226px"></asp:textbox><asp:dropdownlist id="sSpImprovTimeFrameT" tabIndex="104" runat="server"></asp:dropdownlist>&nbsp;&nbsp;Cost&nbsp;<ml:moneytextbox id="sSpImprovC" tabIndex="106" runat="server" preset="money" width="90"></ml:moneytextbox></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table class="InsetBorder" id="Table9" cellSpacing="0" cellPadding="0" width="99%" border="0">
				<tr>
					<td noWrap>
						<table id="Table16" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="FieldLabel">Title will be held in what Name(s)</td>
								<td class="FieldLabel">
								    <asp:textbox id="aTitleNm1" tabIndex="108" runat="server" MaxLength="56" Width="211px"></asp:textbox>
								    <asp:CheckBox id="aTitleNm1Lckd" Text="Lock" runat="server" onclick="refreshCalculation();" tabindex="109"></asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td></td>
								<td class="FieldLabel">
								    <asp:textbox id="aTitleNm2" tabIndex="112" runat="server" MaxLength="56" Width="211px"></asp:textbox>
								    <asp:CheckBox id="aTitleNm2Lckd" Text="Lock" runat="server" onclick="refreshCalculation();" tabindex="113"></asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td class="FieldLabel">Manner in which Title will be held</td>
								<td class="FieldLabel" noWrap><ml:combobox id="aManner" tabIndex="116" runat="server" Width="210px"></ml:combobox></td>
							</tr>
							<tr>
								<td class="FieldLabel">Estate will be held in
								</td>
								<td class="FieldLabel"><asp:dropdownlist id="sEstateHeldT" tabIndex="118" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>&nbsp;&nbsp;Expiration 
									date
									<ml:datetextbox id="sLeaseHoldExpireD" tabIndex="120" disableYearLimit='true' runat="server" preset="date" width="75" dttype="DateTime"></ml:datetextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" title="Source of Down Payment, Settlement Charges and/or Subordinate Financing">Source of Down Payment</td>
								<td class="FieldLabel"><ml:combobox id="sDwnPmtSrc" tabIndex="122" runat="server" Width="350px"></ml:combobox></td>
							</tr>
							<tr>
								<td align=center class="FieldLabel">(Explain on this line)</td>
								<td class="FieldLabel"><asp:textbox id="sDwnPmtSrcExplain" tabIndex="124" runat="server" MaxLength="36" Width="243px"></asp:textbox></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="LoanFormHeader">III. BORROWER INFORMATION</td>
	</tr>
	<tr>
		<td>
			<table class="LoanForm" id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td>
						<table class="InsetBorder" id="Table15" cellSpacing="0" cellPadding="0" width="99%" border="0">
							<tr>
								<td class="FormTableSubHeader" noWrap>Borrower</td>
							</tr>
							<tr>
								<td noWrap>
									<table id="Table6" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<tr>
											<td class="FieldLabel" style="WIDTH: 110px">First Name</td>
											<td class="FieldLabel"><asp:textbox id="aBFirstNm" tabIndex="124" runat="server" EnableViewState="False" onchange="updateBorrowerName();refreshCalculation();" Width="156" maxlength="21"></asp:textbox>&nbsp;&nbsp;Middle 
												Name
												<asp:textbox id="aBMidNm" tabIndex="124" runat="server" EnableViewState="False" Width="156" maxlength="21" onchange="refreshCalculation();"></asp:textbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" style="WIDTH: 110px">Last Name</td>
											<td class="FieldLabel"><asp:textbox id="aBLastNm" tabIndex="126" runat="server" EnableViewState="False" onchange="updateBorrowerName();refreshCalculation();" Width="156px" maxlength="21"></asp:textbox>&nbsp;&nbsp;Suffix
												<ml:ComboBox id="aBSuffix" tabIndex="126" runat="server" EnableViewState="False" Width="46px" maxlength="21" onchange="refreshCalculation();"></ml:ComboBox>&nbsp;&nbsp;SSN
												<ml:ssntextbox id="aBSsn" tabIndex="128" runat="server" EnableViewState="False" preset="ssn" width="90"></ml:ssntextbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" style="WIDTH: 110px">Home Phone</td>
											<td class="FieldLabel"><ml:phonetextbox id="aBHPhone" tabIndex="130" runat="server" preset="phone" width="120"></ml:phonetextbox>&nbsp;&nbsp;Work
												<ml:phonetextbox id="aBBusPhone" tabIndex="132" runat="server" preset="phone" width="120"></ml:phonetextbox>&nbsp;&nbsp;Cell
												<ml:phonetextbox id="aBCellphone" tabIndex="134" runat="server" preset="phone" width="120"></ml:phonetextbox>
											</td>
										</tr>
										<tr>
											<td class="FieldLabel" style="WIDTH: 110px" title="(MM/DD/YYYY)">DOB</td>
											<td class="FieldLabel"><ml:datetextbox id="aBDob" tabIndex="136" runat="server" preset="date" width="75"></ml:datetextbox>&nbsp;&nbsp;Age
												<asp:textbox id="aBAge" tabIndex="138" runat="server" MaxLength="3" Width="34"></asp:textbox>&nbsp;&nbsp;Yrs. 
												School
												<asp:textbox id="aBSchoolYrs" tabIndex="140" runat="server" Width="34px"></asp:textbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" style="WIDTH: 110px">Marital Status</td>
											<td class="FieldLabel" noWrap><asp:dropdownlist id="aBMaritalStatT" tabIndex="142" runat="server"></asp:dropdownlist>&nbsp;&nbsp;No. 
												of Deps
												<asp:textbox id="aBDependNum" tabIndex="142" runat="server" MaxLength="4" Width="32px"></asp:textbox>&nbsp;&nbsp;Dependents' 
												Ages
												<asp:textbox id="aBDependAges" tabIndex="142" runat="server" Width="117px"></asp:textbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" style="WIDTH: 110px">Email</td>
											<td class="FieldLabel">
											    <asp:textbox id="aBEmail" tabIndex="144" runat="server" Width="189px"></asp:textbox>&nbsp;
											    <A onclick="f_sendMail('<%= AspxTools.ClientId(aBEmail) %>'); return false;" tabIndex=146 href="#" >send email</A>
											</td>
										</tr>
                                        <tr id="BorrowerMembershipId" runat="server">
                                            <td class="FieldLabel">
                                                Member ID
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aBCoreSystemId" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
										<tr>
											<td class="FormTableSubHeader" noWrap>Present Address</td>
											<td class="FormTableSubHeader" align="right"><input onclick="fillPresentAddress();" tabIndex="148" type="button" value="Copy from property address">
											</td>
										</tr>
										<tr>
											<td class="FieldLabel" colSpan="2">
												<table id="Table10" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<tr>
														<td class="FieldLabel">Street</td>
														<td class="FieldLabel">City</td>
														<td class="FieldLabel">ST</td>
														<td class="FieldLabel">Zip</td>
														<td class="FieldLabel">Own/Rent</td>
														<td class="FieldLabel">No. Yrs</td>
													</tr>
													<tr>
														<td><asp:textbox id="aBAddr" tabIndex="150" runat="server" onchange="syncMailingAddress();" Width="234px" maxlength="36"></asp:textbox></td>
														<td><asp:textbox id="aBCity" tabIndex="154" runat="server" onchange="syncMailingAddress();" Width="134px"></asp:textbox></td>
														<td><ml:statedropdownlist id="aBState" tabIndex="156" runat="server" onchange="syncMailingAddress();"></ml:statedropdownlist></td>
														<td><ml:zipcodetextbox id="aBZip" tabIndex="158" runat="server" preset="zipcode" width="50" onchange="syncMailingAddress();"></ml:zipcodetextbox></td>
														<td><asp:dropdownlist id="aBAddrT" tabIndex="160" runat="server"></asp:dropdownlist></td>
														<td><asp:textbox id="aBAddrYrs" tabIndex="162" runat="server" MaxLength="4" Width="30"></asp:textbox></td>
													</tr>
													<tr>
														<td>Mailing Address</td>
														<td colSpan="5"><asp:DropDownList ID="aBAddrMailSourceT" onchange="syncMailingAddress();" tabIndex="162" runat="server"/></td>
													</tr>
													<tr>
														<td><asp:textbox id="aBAddrMail" tabIndex="162" runat="server" Width="234px" maxlength="36"></asp:textbox></td>
														<td><asp:textbox id="aBCityMail" tabIndex="162" runat="server" Width="134px"></asp:textbox></td>
														<td><ml:statedropdownlist id="aBStateMail" tabIndex="162" runat="server"></ml:statedropdownlist></td>
														<td><ml:zipcodetextbox id="aBZipMail" tabIndex="162" runat="server" preset="zipcode" width="50"></ml:zipcodetextbox></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td class="FormTableSubHeader" colSpan="6">Former&nbsp;Addresses</td>
													</tr>
													<tr>
														<td><asp:textbox id="aBPrev1Addr" tabIndex="164" runat="server" Width="232px" maxlength="36"></asp:textbox></td>
														<td><asp:textbox id="aBPrev1City" tabIndex="166" runat="server" Width="135px"></asp:textbox></td>
														<td><ml:statedropdownlist id="aBPrev1State" tabIndex="168" runat="server"></ml:statedropdownlist></td>
														<td><ml:zipcodetextbox id="aBPrev1Zip" tabIndex="170" runat="server" preset="zipcode" width="50"></ml:zipcodetextbox></td>
														<td><asp:dropdownlist id="aBPrev1AddrT" tabIndex="172" runat="server"></asp:dropdownlist></td>
														<td><asp:textbox id="aBPrev1AddrYrs" tabIndex="174" runat="server" Width="30px"></asp:textbox></td>
													</tr>
													<tr>
														<td><asp:textbox id="aBPrev2Addr" tabIndex="176" runat="server" Width="232px" maxlength="36"></asp:textbox></td>
														<td><asp:textbox id="aBPrev2City" tabIndex="178" runat="server" Width="137px"></asp:textbox></td>
														<td><ml:statedropdownlist id="aBPrev2State" tabIndex="180" runat="server"></ml:statedropdownlist></td>
														<td><ml:zipcodetextbox id="aBPrev2Zip" tabIndex="182" runat="server" preset="zipcode" width="50"></ml:zipcodetextbox></td>
														<td><asp:dropdownlist id="aBPrev2AddrT" tabIndex="184" runat="server"></asp:dropdownlist></td>
														<td><asp:textbox id="aBPrev2AddrYrs" tabIndex="186" runat="server" Width="30px"></asp:textbox></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table class="InsetBorder" id="Table17" cellSpacing="0" cellPadding="0" width="99%" border="0">
							<tr>
								<td class="FormTableSubheader" noWrap>Co-borrower</td>
								<td class="FormTableSubheader" noWrap align="right"><input onclick="onCopyBorrowerClick();" tabIndex="188" type="button" value="Copy from borrower"></td>
							</tr>
							<tr>
								<td noWrap colSpan="2">
									<table id="Table11" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<tr>
											<td class="FieldLabel" style="WIDTH: 110px">First Name</td>
											<td class="FieldLabel"><asp:textbox id="aCFirstNm" tabIndex="190" runat="server" EnableViewState="False" Width="156" maxlength="21" onchange="refreshCalculation();"></asp:textbox>&nbsp;&nbsp;Middle 
												Name
												<asp:textbox id="aCMidNm" tabIndex="190" runat="server" EnableViewState="False" Width="156" maxlength="21" onchange="refreshCalculation();"></asp:textbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" style="WIDTH: 110px">Last Name</td>
											<td class="FieldLabel"><asp:textbox id="aCLastNm" tabIndex="192" runat="server" EnableViewState="False" Width="156px" maxlength="21" onchange="refreshCalculation();"></asp:textbox>&nbsp;&nbsp;Suffix
												<ml:ComboBox id="aCSuffix" tabIndex="192" runat="server" EnableViewState="False" Width="46px" maxlength="21" onchange="refreshCalculation();"></ml:ComboBox>&nbsp;&nbsp;SSN
												<ml:ssntextbox id="aCSsn" tabIndex="194" runat="server" EnableViewState="False" preset="ssn" width="90"></ml:ssntextbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" style="WIDTH: 110px">Home Phone</td>
											<td class="FieldLabel"><ml:phonetextbox id="aCHPhone" tabIndex="196" runat="server" preset="phone" width="120"></ml:phonetextbox>&nbsp;&nbsp;Work
												<ml:phonetextbox id="aCBusPhone" tabIndex="196" runat="server" preset="phone" width="120"></ml:phonetextbox>&nbsp;&nbsp;Cell
												<ml:phonetextbox id="aCCellphone" tabIndex="196" runat="server" preset="phone" width="120"></ml:phonetextbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" style="WIDTH: 110px" title="(MM/DD/YYYY)">DOB</td>
											<td class="FieldLabel"><ml:datetextbox id="aCDob" tabIndex="198" runat="server" preset="date" width="75"></ml:datetextbox>&nbsp;&nbsp;Age
												<asp:textbox id="aCAge" tabIndex="198" runat="server" EnableViewState="False" MaxLength="3" Width="34"></asp:textbox>&nbsp;&nbsp;Yrs. 
												School
												<asp:textbox id="aCSchoolYrs" tabIndex="198" runat="server" Width="34px"></asp:textbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" style="WIDTH: 110px">Marital Status</td>
											<td class="FieldLabel" noWrap><asp:dropdownlist id="aCMaritalStatT" tabIndex="200" runat="server"></asp:dropdownlist>&nbsp;&nbsp;No. 
												of Deps
												<asp:textbox id="aCDependNum" tabIndex="202" runat="server" MaxLength="4" Width="32px"></asp:textbox>&nbsp;&nbsp;Dependents' 
												Ages
												<asp:textbox id="aCDependAges" tabIndex="204" runat="server" Width="117px"></asp:textbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" style="WIDTH: 110px">Email</td>
											<td class="FieldLabel">
											    <asp:textbox id="aCEmail" tabIndex="210" runat="server" Width="189px"></asp:textbox>&nbsp;
											    <A onclick="f_sendMail('<%= AspxTools.ClientId(aCEmail) %>'); return false;" tabIndex=212 href="#" >send email</A>
											</td>
										</tr>
                                        <tr id="CoborrowerMembershipId" runat="server">
                                            <td class="FieldLabel">
                                                Member ID
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aCCoreSystemId" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
										<tr style="HEIGHT:21px">
											<td class="FormTableSubHeader" noWrap colSpan="2">Present Address</td>
										</tr>
										<tr>
											<td class="FieldLabel" colSpan="2">
												<table id="Table14" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<tr>
														<td class="FieldLabel">Street</td>
														<td class="FieldLabel">City</td>
														<td class="FieldLabel">ST</td>
														<td class="FieldLabel">Zip</td>
														<td class="FieldLabel">Own/Rent</td>
														<td class="FieldLabel">No. Yrs</td>
													</tr>
													<tr>
														<td><asp:textbox id="aCAddr" tabIndex="214" runat="server" onchange="syncMailingAddress();" Width="234px" maxlength="36"></asp:textbox></td>
														<td><asp:textbox id="aCCity" tabIndex="216" runat="server" onchange="syncMailingAddress();" Width="134px"></asp:textbox></td>
														<td><ml:statedropdownlist id="aCState" tabIndex="218" runat="server" onchange="syncMailingAddress();"></ml:statedropdownlist></td>
														<td><ml:zipcodetextbox id="aCZip" tabIndex="220" runat="server" preset="zipcode" width="50" onchange="syncMailingAddress();"></ml:zipcodetextbox></td>
														<td><asp:dropdownlist id="aCAddrT" tabIndex="224" runat="server"></asp:dropdownlist></td>
														<td><asp:textbox id="aCAddrYrs" tabIndex="226" runat="server" MaxLength="4" Width="30"></asp:textbox></td>
													</tr>
													<tr>
														<td>Mailing Address</td>
														<td colSpan="5"><asp:DropDownList ID="aCAddrMailSourceT" onchange="syncMailingAddress();" tabIndex="226" runat="server"/></td>
													</tr>
													<tr>
														<td><asp:textbox id="aCAddrMail" tabIndex="226" runat="server" Width="234px" maxlength="36"></asp:textbox></td>
														<td><asp:textbox id="aCCityMail" tabIndex="226" runat="server" Width="134px"></asp:textbox></td>
														<td><ml:statedropdownlist id="aCStateMail" tabIndex="226" runat="server"></ml:statedropdownlist></td>
														<td><ml:zipcodetextbox id="aCZipMail" tabIndex="226" runat="server" preset="zipcode" width="50"></ml:zipcodetextbox></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td class="FormTableSubHeader" colSpan="6">Former&nbsp;Addresses</td>
													</tr>
													<tr>
														<td><asp:textbox id="aCPrev1Addr" tabIndex="228" runat="server" Width="234px" maxlength="36"></asp:textbox></td>
														<td><asp:textbox id="aCPrev1City" tabIndex="230" runat="server" Width="134px"></asp:textbox></td>
														<td><ml:statedropdownlist id="aCPrev1State" tabIndex="232" runat="server"></ml:statedropdownlist></td>
														<td><ml:zipcodetextbox id="aCPrev1Zip" tabIndex="234" runat="server" preset="zipcode" width="50"></ml:zipcodetextbox></td>
														<td><asp:dropdownlist id="aCPrev1AddrT" tabIndex="236" runat="server"></asp:dropdownlist></td>
														<td><asp:textbox id="aCPrev1AddrYrs" tabIndex="238" runat="server" Width="30px"></asp:textbox></td>
													</tr>
													<tr>
														<td><asp:textbox id="aCPrev2Addr" tabIndex="240" runat="server" Width="234px" rows="36"></asp:textbox></td>
														<td><asp:textbox id="aCPrev2City" tabIndex="242" runat="server" Width="134px"></asp:textbox></td>
														<td><ml:statedropdownlist id="aCPrev2State" tabIndex="244" runat="server"></ml:statedropdownlist></td>
														<td><ml:zipcodetextbox id="aCPrev2Zip" tabIndex="246" runat="server" preset="zipcode" width="50"></ml:zipcodetextbox></td>
														<td><asp:dropdownlist id="aCPrev2AddrT" tabIndex="248" runat="server"></asp:dropdownlist></td>
														<td><asp:textbox id="aCPrev2AddrYrs" tabIndex="250" runat="server" Width="30px"></asp:textbox></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="LoanFormHeader">IV. EMPLOYMENT INFORMATION</td>
	</tr>
	<tr>
		<td class="FieldLabel" align="center"><A tabIndex="252" href="javascript:linkMe('../BorrowerInfo.aspx?pg=1');">Go 
				to Employment</A></td>
	</tr>
</table>