using System;

using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{

	public partial class CreditDenial : BaseLoanPage
	{
        #region Protected member variables
        #endregion

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(CreditDenial));
            dataLoan.InitLoad();

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditDenialStatement, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            prepareBy.Text = preparer.PreparerName;
            BrokerName.Text = preparer.CompanyName;
            BrokerAddress.Text = preparer.StreetAddr;
            BrokerCity.Text = preparer.City;
            BrokerState.Value = preparer.State;
            BrokerZip.Text = preparer.Zip;
            BrokerPhone.Text = preparer.PhoneOfCompany;

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.CreditReport, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            CRAName.Text = agent.CompanyName;
            CRAAddress.Text = agent.StreetAddr;
            CRACity.Text = agent.City;
            CRAState.Value = agent.State;
            CRAZipcode.Text = agent.Zip;
            CRAPhone.Text = agent.Phone;

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.CreditReportAgency2, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            CRA2Name.Text = agent.CompanyName;
            CRA2Address.Text = agent.StreetAddr;
            CRA2City.Text = agent.City;
            CRA2State.Value = agent.State;
            CRA2Zipcode.Text = agent.Zip;
            CRA2Phone.Text = agent.Phone;

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.CreditReportAgency3, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            CRA3Name.Text = agent.CompanyName;
            CRA3Address.Text = agent.StreetAddr;
            CRA3City.Text = agent.City;
            CRA3State.Value = agent.State;
            CRA3Zipcode.Text = agent.Zip;
            CRA3Phone.Text = agent.Phone;

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.ECOA, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (agent.IsValid) 
            {
                ECOA_CompanyName.Text = agent.CompanyName;
                ECOA_StreetAddr.Text = agent.StreetAddr;
                ECOA_City.Text = agent.City;
                ECOA_State.Value = agent.State;
                ECOA_Zip.Text = agent.Zip;
            }

            sRejectD.Text = dataLoan.sRejectD_rep;
            sHmdaDeniedFormDoneD.Text = dataLoan.sHmdaDeniedFormDoneD_rep;
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            CRAZipcode.SmartZipcode(CRACity, CRAState);
            CRA2Zipcode.SmartZipcode(CRA2City, CRA2State);
            CRA3Zipcode.SmartZipcode(CRA3City, CRA3State);
            BrokerZip.SmartZipcode(BrokerCity, BrokerState);
            ScoreContactZip.SmartZipcode(ScoreContactCity, ScoreContactState);
            ECOA_Zip.SmartZipcode(ECOA_City, ECOA_State);
            this.PageTitle = "Credit Denial";
            this.PageID = "CreditDenial";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CCreditDenialPDF);
            this.RegisterJsScript("LQBPopup.js");
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
