﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace LendersOfficeApp.newlos.Forms {
    
    
    public partial class CAMLDSRE885pg4 {
        
        /// <summary>
        /// sFinalLAmt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sFinalLAmt;
        
        /// <summary>
        /// sTermInYr control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sTermInYr;
        
        /// <summary>
        /// sRe885ComparisonPrincipalInterestIsNotOffered control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sRe885ComparisonPrincipalInterestIsNotOffered;
        
        /// <summary>
        /// sRe885ComparisonInterestOnlyIsNotOffered control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sRe885ComparisonInterestOnlyIsNotOffered;
        
        /// <summary>
        /// sRe885Comparison5YrsArmIsNotOffered control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sRe885Comparison5YrsArmIsNotOffered;
        
        /// <summary>
        /// sRe885Comparison5YrsArmInterestOnlyIsNotOffered control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sRe885Comparison5YrsArmInterestOnlyIsNotOffered;
        
        /// <summary>
        /// sRe885ComparisonOptionPaymentIsNotOffered control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sRe885ComparisonOptionPaymentIsNotOffered;
        
        /// <summary>
        /// sRe885ComparisonProposedLoanTypeOfLoan control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sRe885ComparisonProposedLoanTypeOfLoan;
        
        /// <summary>
        /// sRe885ComparisonProposedLoanAmortType control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sRe885ComparisonProposedLoanAmortType;
        
        /// <summary>
        /// sRe885ComparisonPrincipalInterestNoteIR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sRe885ComparisonPrincipalInterestNoteIR;
        
        /// <summary>
        /// sRe885ComparisonInterestOnlyNoteIR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sRe885ComparisonInterestOnlyNoteIR;
        
        /// <summary>
        /// sRe885Comparison5YrsArmInitialNoteIR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sRe885Comparison5YrsArmInitialNoteIR;
        
        /// <summary>
        /// sRe885Comparison5YrsArmMaximumNoteIR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sRe885Comparison5YrsArmMaximumNoteIR;
        
        /// <summary>
        /// sRe885Comparison5YrsArmInterestOnlyInitialNoteIR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sRe885Comparison5YrsArmInterestOnlyInitialNoteIR;
        
        /// <summary>
        /// sRe885Comparison5YrsArmInterestOnlyMaximumNoteIR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sRe885Comparison5YrsArmInterestOnlyMaximumNoteIR;
        
        /// <summary>
        /// sRe885ComparisonOptionPaymentFirstMonthNoteIR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sRe885ComparisonOptionPaymentFirstMonthNoteIR;
        
        /// <summary>
        /// sRe885ComparisonOptionPaymentInitialNoteIR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sRe885ComparisonOptionPaymentInitialNoteIR;
        
        /// <summary>
        /// sRe885ComparisonOptionPaymentMaximumNoteIR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sRe885ComparisonOptionPaymentMaximumNoteIR;
        
        /// <summary>
        /// sRe885ComparisonProposedLoanProductDesc control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sRe885ComparisonProposedLoanProductDesc;
        
        /// <summary>
        /// sRe885ComparisonPrincipalInterestMinPmtFirst5Yrs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonPrincipalInterestMinPmtFirst5Yrs;
        
        /// <summary>
        /// sRe885ComparisonInterestOnlyMinPmtFirst5Yrs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonInterestOnlyMinPmtFirst5Yrs;
        
        /// <summary>
        /// sRe885Comparison5YrsArmMinPmtFirst5Yrs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmMinPmtFirst5Yrs;
        
        /// <summary>
        /// sRe885Comparison5YrsArmInterestOnlyMinPmtFirst5Yrs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmInterestOnlyMinPmtFirst5Yrs;
        
        /// <summary>
        /// sRe885ComparisonOptionPaymentMinPmtFirst5Yrs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonOptionPaymentMinPmtFirst5Yrs;
        
        /// <summary>
        /// sRe885ComparisonProposedLoanMinPmtFirst5Yrs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonProposedLoanMinPmtFirst5Yrs;
        
        /// <summary>
        /// sRe885ComparisonPrincipalInterestMPmt6YrsNoRateChange control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonPrincipalInterestMPmt6YrsNoRateChange;
        
        /// <summary>
        /// sRe885ComparisonInterestOnlyMPmt6YrsNoRateChange control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonInterestOnlyMPmt6YrsNoRateChange;
        
        /// <summary>
        /// sRe885Comparison5YrsArmMPmt6YrsNoRateChange control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmMPmt6YrsNoRateChange;
        
        /// <summary>
        /// sRe885Comparison5YrsArmInterestOnlyMPmt6YrsNoRateChange control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmInterestOnlyMPmt6YrsNoRateChange;
        
        /// <summary>
        /// sRe885ComparisonOptionPaymentMPmt6YrsNoRateChange control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonOptionPaymentMPmt6YrsNoRateChange;
        
        /// <summary>
        /// sRe885ComparisonProposedLoanMPmt6YrsNoRateChange control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonProposedLoanMPmt6YrsNoRateChange;
        
        /// <summary>
        /// sRe885ComparisonPrincipalInterestMPmt6Yrs2PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonPrincipalInterestMPmt6Yrs2PercentRaise;
        
        /// <summary>
        /// sRe885ComparisonInterestOnlyMPmt6Yrs2PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonInterestOnlyMPmt6Yrs2PercentRaise;
        
        /// <summary>
        /// sRe885Comparison5YrsArmMPmt6Yrs2PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmMPmt6Yrs2PercentRaise;
        
        /// <summary>
        /// sRe885Comparison5YrsArmInterestOnlyMPmt6Yrs2PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmInterestOnlyMPmt6Yrs2PercentRaise;
        
        /// <summary>
        /// sRe885ComparisonOptionPaymentMPmt6Yrs2PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonOptionPaymentMPmt6Yrs2PercentRaise;
        
        /// <summary>
        /// sRe885ComparisonProposedLoanMPmt6Yrs2PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonProposedLoanMPmt6Yrs2PercentRaise;
        
        /// <summary>
        /// sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaise;
        
        /// <summary>
        /// sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaise;
        
        /// <summary>
        /// sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaise;
        
        /// <summary>
        /// sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaise;
        
        /// <summary>
        /// sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaise;
        
        /// <summary>
        /// sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaise;
        
        /// <summary>
        /// sRe885ComparisonPrincipalInterestMinPmt_sLTotI control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonPrincipalInterestMinPmt_sLTotI;
        
        /// <summary>
        /// sRe885ComparisonInterestOnlyMinPmt_sLTotI control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonInterestOnlyMinPmt_sLTotI;
        
        /// <summary>
        /// sRe885Comparison5YrsArmMinPmt_sLTotI control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmMinPmt_sLTotI;
        
        /// <summary>
        /// sRe885Comparison5YrsArmInterestMinPmt_sLTotI control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmInterestMinPmt_sLTotI;
        
        /// <summary>
        /// sRe885ComparisonOptionPaymentMinPmt_sLTotI control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonOptionPaymentMinPmt_sLTotI;
        
        /// <summary>
        /// sRe885ComparisonProposedLoanMinPmt_sLTotI control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonProposedLoanMinPmt_sLTotI;
        
        /// <summary>
        /// sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaiseDiff control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaiseDiff;
        
        /// <summary>
        /// sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaiseDiff control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaiseDiff;
        
        /// <summary>
        /// sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaiseDiff control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaiseDiff;
        
        /// <summary>
        /// sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaiseDiff control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaiseDiff;
        
        /// <summary>
        /// sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaiseDiff control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaiseDiff;
        
        /// <summary>
        /// sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaiseDiff control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaiseDiff;
        
        /// <summary>
        /// sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaise;
        
        /// <summary>
        /// sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaise;
        
        /// <summary>
        /// sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaise;
        
        /// <summary>
        /// sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaise;
        
        /// <summary>
        /// sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaise;
        
        /// <summary>
        /// sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaise control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaise;
        
        /// <summary>
        /// sRe885ComparisonPrincipalInterestMaxPmt_sLTotI control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonPrincipalInterestMaxPmt_sLTotI;
        
        /// <summary>
        /// sRe885ComparisonInterestOnlyMaxPmt_sLTotI control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonInterestOnlyMaxPmt_sLTotI;
        
        /// <summary>
        /// sRe885Comparison5YrsArmMaxPmt_sLTotI control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmMaxPmt_sLTotI;
        
        /// <summary>
        /// sRe885Comparison5YrsArMaxterestMaxPmt_sLTotI control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArMaxterestMaxPmt_sLTotI;
        
        /// <summary>
        /// sRe885ComparisonOptionPaymentMaxPmt_sLTotI control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonOptionPaymentMaxPmt_sLTotI;
        
        /// <summary>
        /// sRe885ComparisonProposedLoanMaxPmt_sLTotI control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonProposedLoanMaxPmt_sLTotI;
        
        /// <summary>
        /// sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaiseDiff control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaiseDiff;
        
        /// <summary>
        /// sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaiseDiff control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaiseDiff;
        
        /// <summary>
        /// sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaiseDiff control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaiseDiff;
        
        /// <summary>
        /// sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaiseDiff control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaiseDiff;
        
        /// <summary>
        /// sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaiseDiff control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaiseDiff;
        
        /// <summary>
        /// sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaiseDiff control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaiseDiff;
        
        /// <summary>
        /// sRe885ComparisonPrincipalInterestBalanceAfter5Yrs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonPrincipalInterestBalanceAfter5Yrs;
        
        /// <summary>
        /// sRe885ComparisonInterestOnlyBalanceAfter5Yrs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonInterestOnlyBalanceAfter5Yrs;
        
        /// <summary>
        /// sRe885Comparison5YrsArmBalanceAfter5Yrs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmBalanceAfter5Yrs;
        
        /// <summary>
        /// sRe885Comparison5YrsArmInterestOnlyBalanceAfter5Yrs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmInterestOnlyBalanceAfter5Yrs;
        
        /// <summary>
        /// sRe885ComparisonOptionPaymentBalanceAfter5Yrs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonOptionPaymentBalanceAfter5Yrs;
        
        /// <summary>
        /// sRe885ComparisonProposedLoanBalanceAfter5Yrs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonProposedLoanBalanceAfter5Yrs;
        
        /// <summary>
        /// sRe885ComparisonPrincipalInterestBalanceAfter5YrsReduceBy control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonPrincipalInterestBalanceAfter5YrsReduceBy;
        
        /// <summary>
        /// sRe885Comparison5YrsArmBalanceAfter5YrsReduceBy control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885Comparison5YrsArmBalanceAfter5YrsReduceBy;
        
        /// <summary>
        /// sRe885ComparisonOptionPaymentBalanceAfter5YrsReduceBy control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonOptionPaymentBalanceAfter5YrsReduceBy;
        
        /// <summary>
        /// sRe885ComparisionProposedLoanBalanceReduceYesNoDesc control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sRe885ComparisionProposedLoanBalanceReduceYesNoDesc;
        
        /// <summary>
        /// sRe885ComparisionProposedLoanBalanceReduceDesc control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sRe885ComparisionProposedLoanBalanceReduceDesc;
        
        /// <summary>
        /// sRe885ComparisonProposedLoanBalanceAfter5YrsReduceBy control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sRe885ComparisonProposedLoanBalanceAfter5YrsReduceBy;
        
        /// <summary>
        /// sRe885ComparisonProductsNotOfferedDeemReliableTri control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBoxList sRe885ComparisonProductsNotOfferedDeemReliableTri;
        
        /// <summary>
        /// CFM control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LendersOfficeApp.newlos.Status.ContactFieldMapper CFM;
        
        /// <summary>
        /// GfeTilCompanyName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox GfeTilCompanyName;
        
        /// <summary>
        /// GfeTilLicenseNumOfCompany control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox GfeTilLicenseNumOfCompany;
        
        /// <summary>
        /// GfeTilStreetAddr control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox GfeTilStreetAddr;
        
        /// <summary>
        /// GfeTilCity control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox GfeTilCity;
        
        /// <summary>
        /// GfeTilState control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.StateDropDownList GfeTilState;
        
        /// <summary>
        /// GfeTilZip control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.ZipcodeTextBox GfeTilZip;
        
        /// <summary>
        /// GfeTilPreparerName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox GfeTilPreparerName;
        
        /// <summary>
        /// GfeTilLicenseNumOfAgent control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox GfeTilLicenseNumOfAgent;
    }
}
