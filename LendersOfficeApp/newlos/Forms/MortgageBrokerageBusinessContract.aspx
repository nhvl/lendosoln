<%@ Page language="c#" Codebehind="MortgageBrokerageBusinessContract.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.MortgageBrokerageBusinessContract" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>MortgageBrokerageBusinessContract</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="FlowLayout" class="RightBackground">
	<script language=javascript>
  <!--
  function calculateTotal(id) {
    var sLAmt = parseMoneyFloat(document.getElementById("sLAmt").value);
    var r = parseMoneyFloat(document.getElementById(id + "R").value);
    var mb = parseMoneyFloat(document.getElementById(id + "Mb").value);
    document.getElementById(id).value = (sLAmt * r) / 100 + mb;
    format_money(document.getElementById(id));
  }
  //-->
</script>

    <form id="MortgageBrokerageBusinessContract" method="post" runat="server">
<table id=Table1 cellspacing=0 cellpadding=0 border=0>
  <tr>
    <td nowrap colspan=2 class=MainRightHeader>Mortgage Brokerage Business Contract (FL Only)</td></tr>
  <tr>
    <td class=FieldLabel nowrap>Contract Period</td>
    <td nowrap><asp:TextBox id=sFloridaContractDays runat="server" Width="88px"></asp:TextBox></td></tr>
  <tr>
    <td class=FieldLabel nowrap>Estimate Market Value</td>
    <td nowrap><ml:MoneyTextBox id=sFloridaBorrEstimateSpVal runat="server" width="90" preset="money"></ml:MoneyTextBox></td></tr>
  <tr>
    <td class=FieldLabel nowrap>Estimate Balance</td>
    <td nowrap><ml:MoneyTextBox id=sFloridaBorrEstimateExistingMBal runat="server" width="90" preset="money"></ml:MoneyTextBox></td></tr>
  <tr>
    <td class=FieldLabel nowrap>Deposit</td>
    <td nowrap><ml:MoneyTextBox id=sFloridaBorrDeposit runat="server" width="90" preset="money"></ml:MoneyTextBox></td></tr>
  <tr>
    <td class=FieldLabel nowrap>Mortgage Broker Fee</td>
    <td nowrap><ml:PercentTextBox id=sFloridaBrokerFeeR runat="server" width="70" preset="percent" onchange="calculateTotal('sFloridaBrokerFee');"></ml:PercentTextBox> 
      + <ml:MoneyTextBox id=sFloridaBrokerFeeMb runat="server" width="90" preset="money" onchange="calculateTotal('sFloridaBrokerFee');"></ml:MoneyTextBox>=<ml:MoneyTextBox id=sFloridaBrokerFee runat="server" width="90" preset="money" readonly="True"></ml:MoneyTextBox> 
    </td></tr>
  <tr>
    <td class=FieldLabel nowrap>Additional Compensation</td>
    <td nowrap><ml:PercentTextBox id=sFloridaAdditionalCompMinR runat="server" width="70" preset="percent" onchange="calculateTotal('sFloridaAdditionalCompMin');"></ml:PercentTextBox> 
      + <ml:MoneyTextBox id=sFloridaAdditionalCompMinMb runat="server" width="90" preset="money" onchange="calculateTotal('sFloridaAdditionalCompMin');"></ml:MoneyTextBox>=<ml:MoneyTextBox id=sFloridaAdditionalCompMin runat="server" width="90" preset="money" readonly="True"></ml:MoneyTextBox></td></tr>
  <tr>
    <td class=FieldLabel 
      nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      To</td>
    <td nowrap><ml:PercentTextBox id=sFloridaAdditionalCompMaxR runat="server" width="70" preset="percent" onchange="calculateTotal('sFloridaAdditionalCompMax');"></ml:PercentTextBox> 
      + <ml:MoneyTextBox id=sFloridaAdditionalCompMaxMb runat="server" width="90" preset="money" onchange="calculateTotal('sFloridaAdditionalCompMax');"></ml:MoneyTextBox>=<ml:MoneyTextBox id=sFloridaAdditionalCompMax runat="server" width="90" preset="money" readonly="True"></ml:MoneyTextBox></td></tr>
  <tr>
    <td class=FieldLabel nowrap>Application Fee</td>
    <td nowrap><ml:MoneyTextBox id=sFloridaAppFee runat="server" width="90" preset="money"></ml:MoneyTextBox></td></tr>
  <tr>
    <td class=FieldLabel nowrap></td>
    <td nowrap><asp:RadioButtonList id=sFloridaIsAppFeeRefundable runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
<asp:ListItem Value="0">Refundable</asp:ListItem>
<asp:ListItem Value="1">Non refundable</asp:ListItem>
</asp:RadioButtonList><asp:CheckBox id=sFloridaIsAppFeeApplicableToCc runat="server" Text="Applicable"></asp:CheckBox></td></tr>
  <tr>
    <td class=FieldLabel nowrap>Prepare By</td>
    <td 
nowrap><asp:TextBox id=PreparerName runat="server"></asp:TextBox></td></tr>
  <tr>
    <td class=FieldLabel nowrap>Company Name</td>
    <td nowrap><asp:TextBox id=CompanyName runat="server"></asp:TextBox></td></tr>
  <tr>
    <td class=FieldLabel nowrap>License Number</td>
    <td 
  nowrap><asp:TextBox id=LicenseNumOfCompany runat="server"></asp:TextBox></td></tr></table>

     </form>
	
  </body>
</html>
