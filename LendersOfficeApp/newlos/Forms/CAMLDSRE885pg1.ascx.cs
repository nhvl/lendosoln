﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOfficeApp.los.LegalForm;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class CAMLDSRE885pg1 : BaseLoanUserControl, IAutoLoadUserControl
    {

        protected string ClosingCostPageUrl
        {
            get
            {
                BrokerDB db = this.BrokerDB;
                if (db.AreAutomatedClosingCostPagesVisible)
                {
                    return string.Format("/los/Template/ClosingCost/ClosingCostTemplatePicker.aspx?sLId={0}", LoanID.ToString("N"));
                }
                else
                {
                    return string.Format("/los/view/closingcostlist.aspx?loanid={0}", LoanID.ToString("N"));
                }
            }
        }
        private LosConvert m_convertLos;

        private bool IsByPassBgCalc()
        {
            bool ret = false;

            SqlParameter[] parameters = { new SqlParameter("@UserId", BrokerUser.UserId) };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerUser.ConnectionInfo, "RetrieveByPassBgCalcForGfeAsDefaultByUserId", parameters))
            {
                if (reader.Read())
                {
                    ret = (bool)reader["ByPassBgCalcForGfeAsDefault"];

                }

            }

            return ret;
        }
        protected string m_openedDate;
        protected void PageInit(object sender, System.EventArgs e)
        {
            Tools.Bind_sProRealETxT(sProRealETxT);
            Tools.Bind_PercentBaseT(sProHazInsT);
            Tools.Bind_sOriginatorCompensationPaymentSourceT(sOriginatorCompensationPaymentSourceT);
            Tools.Bind_sGfeOriginatorCompFBaseT(sGfeOriginatorCompFBaseT);

            bool showAdditionalSection1000CustomFees = this.BrokerDB.EnableAdditionalSection1000CustomFees;
            phAdditionalSection1000CustomFees.Visible = showAdditionalSection1000CustomFees;
            AggregateAdjustmentLineNum.Text = showAdditionalSection1000CustomFees ? "1010" : "1008";
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here


            // OPM 17652 - Ethan
            CFM.Type = "21";
            CFM.CompanyNameField = sAgentLenderCompanyName.ClientID;
        }
        private void InitItemProps(GoodFaithEstimateRightColumn ctrl, int props)
        {
            ctrl.Apr = LosConvert.GfeItemProps_Apr(props);
            ctrl.ToBrok = LosConvert.GfeItemProps_ToBr(props);
            ctrl.PdByT = LosConvert.GfeItemProps_Payer(props);
            ctrl.Fha = LosConvert.GfeItemProps_FhaAllow(props);
            ctrl.Poc = LosConvert.GfeItemProps_Poc(props);
        }

        private void BindDataObject(CPageData dataLoan)
        {
            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (agent.IsValid)
            {
                sAgentLenderCompanyName.Text = agent.CompanyName;
            }
            bool heMigrated = dataLoan.sIsHousingExpenseMigrated;
            sEstCloseDLckd.Checked = dataLoan.sEstCloseDLckd;
            sEstCloseD.Text = dataLoan.sEstCloseD_rep;
            sEstCloseD.ToolTip = "Hint:  Enter 't' for today's date.";
            m_openedDate = dataLoan.sOpenedD_rep;
            sDue.Text = dataLoan.sDue_rep;
            sDue.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            LeaveEmptyField(sDue);

            sTerm.Text = dataLoan.sTerm_rep;
            sTerm.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            LeaveEmptyField(sTerm);

            sSchedDueD1Lckd.Checked = dataLoan.sSchedDueD1Lckd;
            sSchedDueD1.Text = dataLoan.sSchedDueD1_rep;
            sSchedDueD1.ToolTip = "Hint:  Enter 't' for today's date.";
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            LeaveEmptyField(sNoteIR);

            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sLpTemplateNm.Text = dataLoan.sLpTemplateNm;
            // 5/29/07 mf. OPM 12061. There is now a corporate setting for editing
            // the loan program name.
            BrokerDB broker = this.BrokerDB;

            if(! BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowEditingLoanProgramName))
                sLpTemplateNm.ReadOnly = true;

            sCcTemplateNm.Text = dataLoan.sCcTemplateNm;
            sDaysInYr.Text = dataLoan.sDaysInYr_rep;
            LeaveEmptyField(sDaysInYr);

            GfeTilPrepareDate.Text = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject).PrepareDate_rep;
            GfeTilPrepareDate.ToolTip = "Hint:  Enter 't' for today's date.  Enter 'o' for opened date.";

            s800U5F.Text = dataLoan.s800U5F_rep;
            LeaveEmptyField(s800U5F);

            s800U5FDesc.Text = dataLoan.s800U5FDesc;
            s800U5FCode.Text = dataLoan.s800U5FCode;
            s800U4F.Text = dataLoan.s800U4F_rep;
            LeaveEmptyField(s800U4F);

            s800U4FDesc.Text = dataLoan.s800U4FDesc;
            s800U4FCode.Text = dataLoan.s800U4FCode;
            s800U3F.Text = dataLoan.s800U3F_rep;
            LeaveEmptyField(s800U3F);

            s800U3FDesc.Text = dataLoan.s800U3FDesc;
            s800U3FCode.Text = dataLoan.s800U3FCode;
            s800U2F.Text = dataLoan.s800U2F_rep;
            LeaveEmptyField(s800U2F);

            s800U2FDesc.Text = dataLoan.s800U2FDesc;
            s800U2FCode.Text = dataLoan.s800U2FCode;
            s800U1F.Text = dataLoan.s800U1F_rep;
            LeaveEmptyField(s800U1F);

            s800U1FDesc.Text = dataLoan.s800U1FDesc;
            s800U1FCode.Text = dataLoan.s800U1FCode;
            sWireF.Text = dataLoan.sWireF_rep;
            LeaveEmptyField(sWireF);

            sUwF.Text = dataLoan.sUwF_rep;
            LeaveEmptyField(sUwF);

            sProcF.Text = dataLoan.sProcF_rep;
            LeaveEmptyField(sProcF);

            sProcFPaid.Checked = dataLoan.sProcFPaid;
            sTxServF.Text = dataLoan.sTxServF_rep;
            LeaveEmptyField(sTxServF);

            sMBrokF.Text = dataLoan.sMBrokF_rep;
            LeaveEmptyField(sMBrokF);

            sMBrokFMb.Text = dataLoan.sMBrokFMb_rep;
            LeaveEmptyField(sMBrokFMb);

            sMBrokFPc.Text = dataLoan.sMBrokFPc_rep;
            LeaveEmptyField(sMBrokFPc);

            sInspectF.Text = dataLoan.sInspectF_rep;
            LeaveEmptyField(sInspectF);

            sCrF.Text = dataLoan.sCrF_rep;
            LeaveEmptyField(sCrF);

            sCrFPaid.Checked = dataLoan.sCrFPaid;
            sApprF.Text = dataLoan.sApprF_rep;
            LeaveEmptyField(sApprF);

            sApprFPaid.Checked = dataLoan.sApprFPaid;
            sLDiscnt.Text = dataLoan.sLDiscnt_rep;
            LeaveEmptyField(sLDiscnt);

            sLDiscntFMb.Text = dataLoan.sLDiscntFMb_rep;
            LeaveEmptyField(sLDiscntFMb);

            sLDiscntPc.Text = dataLoan.sLDiscntPc_rep;
            LeaveEmptyField(sLDiscntPc);

            sLOrigF.Text = dataLoan.sLOrigF_rep;
            LeaveEmptyField(sLOrigF);

            sLOrigFMb.Text = dataLoan.sLOrigFMb_rep;
            LeaveEmptyField(sLOrigFMb);

            sLOrigFPc.Text = dataLoan.sLOrigFPc_rep;
            LeaveEmptyField(sLOrigFPc);

            s900U1Pia.Text = dataLoan.s900U1Pia_rep;
            LeaveEmptyField(s900U1Pia);

            s900U1PiaDesc.Text = dataLoan.s900U1PiaDesc;
            s900U1PiaCode.Text = dataLoan.s900U1PiaCode;
            sVaFf.Text = dataLoan.sVaFf_rep;
            LeaveEmptyField(sVaFf);

            s904Pia.Text = dataLoan.s904Pia_rep;
            s904PiaDesc.Text = dataLoan.s904PiaDesc;
            sHazInsPia.Text = dataLoan.sHazInsPia_rep;
            LeaveEmptyField(sHazInsPia);

            sHazInsRsrvMon.ReadOnly = (false == dataLoan.sHazInsRsrvMonLckd) || IsReadOnly;
            sMInsRsrvMon.ReadOnly = (false == dataLoan.sMInsRsrvMonLckd) || IsReadOnly;
            sRealETxRsrvMon.ReadOnly = (false == dataLoan.sRealETxRsrvMonLckd) || IsReadOnly;
            sSchoolTxRsrvMon.ReadOnly = (false == dataLoan.sSchoolTxRsrvMonLckd) || IsReadOnly;
            sFloodInsRsrvMon.ReadOnly = (false == dataLoan.sFloodInsRsrvMonLckd) || IsReadOnly;
            s1006RsrvMon.ReadOnly = (false == dataLoan.s1006RsrvMonLckd) || IsReadOnly;
            s1007RsrvMon.ReadOnly = (false == dataLoan.s1007RsrvMonLckd) || IsReadOnly;
            sU3RsrvMon.ReadOnly = (false == dataLoan.sU3RsrvMonLckd) || IsReadOnly;
            sU4RsrvMon.ReadOnly = (false == dataLoan.sU4RsrvMonLckd) || IsReadOnly;
            sSchedDueD1.ReadOnly = (false == dataLoan.sSchedDueD1Lckd) || IsReadOnly;
            sEstCloseD.ReadOnly = (false == dataLoan.sEstCloseDLckd) || IsReadOnly;
            sAggregateAdjRsrv.ReadOnly = (false == dataLoan.sAggregateAdjRsrvLckd) || IsReadOnly;


            sHazInsPiaMon.Text = dataLoan.sHazInsPiaMon_rep;
            LeaveEmptyField(sHazInsPiaMon);

            sProHazInsT.SelectedIndex = (int)dataLoan.sProHazInsT;
            sProHazInsR.Text = dataLoan.sProHazInsR_rep;
            LeaveEmptyField(sProHazInsR);
            sProHazInsMb.Text = dataLoan.sProHazInsMb_rep;
            LeaveEmptyField(sProHazInsMb);
            sProHazInsHolder.Visible = !dataLoan.sHazardExpenseInDisbursementMode;

            sMipPia.Text = dataLoan.sMipPia_rep;
            LeaveEmptyField(sMipPia);

            sIPia.Text = dataLoan.sIPia_rep;
            LeaveEmptyField(sIPia);

            sIPiaDyLckd.Checked = dataLoan.sIPiaDyLckd;
            sIPiaDy.Text = dataLoan.sIPiaDy_rep;
            LeaveEmptyField(sIPiaDy);

            sIPerDay.Text = dataLoan.sIPerDay_rep;
            LeaveEmptyField(sIPerDay);

            sAggregateAdjRsrv.Text = dataLoan.sAggregateAdjRsrv_rep;
            LeaveEmptyField(sAggregateAdjRsrv);

            s1007Rsrv.Text = dataLoan.s1007Rsrv_rep;
            LeaveEmptyField(s1007Rsrv);

            s1007ProHExp.Text = dataLoan.s1007ProHExp_rep;
            s1007ProHExp.ReadOnly = (heMigrated && dataLoan.sLine1009Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements);
            LeaveEmptyField(s1007ProHExp);

            s1007RsrvMon.Text = dataLoan.s1007RsrvMon_rep;
            LeaveEmptyField(s1007RsrvMon);

            s1007ProHExpDesc.Text = dataLoan.s1007ProHExpDesc;
            s1006Rsrv.Text = dataLoan.s1006Rsrv_rep;
            LeaveEmptyField(s1006Rsrv);

            s1006ProHExp.Text = dataLoan.s1006ProHExp_rep;
            s1006ProHExp.ReadOnly = (heMigrated && dataLoan.sLine1008Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements);
            LeaveEmptyField(s1006ProHExp);

            s1006RsrvMon.Text = dataLoan.s1006RsrvMon_rep;
            LeaveEmptyField(s1006RsrvMon);

            s1006ProHExpDesc.Text = dataLoan.s1006ProHExpDesc;

            #region opm 126772
            sU3Rsrv.Text = dataLoan.sU3Rsrv_rep;
            LeaveEmptyField(sU3Rsrv);

            sProU3Rsrv.Text = dataLoan.sProU3Rsrv_rep;
            sProU3Rsrv.ReadOnly = (heMigrated && dataLoan.sLine1010Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements);
            LeaveEmptyField(sProU3Rsrv);

            sU3RsrvMon.Text = dataLoan.sU3RsrvMon_rep;
            LeaveEmptyField(sU3RsrvMon);

            sU3RsrvDesc.Text = dataLoan.sU3RsrvDesc;

            sU4Rsrv.Text = dataLoan.sU4Rsrv_rep;
            LeaveEmptyField(sU4Rsrv);

            sProU4Rsrv.Text = dataLoan.sProU4Rsrv_rep;
            sProU4Rsrv.ReadOnly = (heMigrated && dataLoan.sLine1011Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements);
            LeaveEmptyField(sProU4Rsrv);

            sU4RsrvMon.Text = dataLoan.sU4RsrvMon_rep;
            LeaveEmptyField(sU4RsrvMon);

            sU4RsrvDesc.Text = dataLoan.sU4RsrvDesc;
            #endregion

            sFloodInsRsrv.Text = dataLoan.sFloodInsRsrv_rep;
            LeaveEmptyField(sFloodInsRsrv);

            sProFloodIns.Text = dataLoan.sProFloodIns_rep;
            sProFloodIns.ReadOnly = (heMigrated && dataLoan.sFloodExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements);
            LeaveEmptyField(sProFloodIns);

            sFloodInsRsrvMon.Text = dataLoan.sFloodInsRsrvMon_rep;
            LeaveEmptyField(sFloodInsRsrvMon);

            sRealETxRsrv.Text = dataLoan.sRealETxRsrv_rep;
            LeaveEmptyField(sRealETxRsrv);
            sProRealETxR.Text = dataLoan.sProRealETxR_rep;
            LeaveEmptyField(sProRealETxR);
            sProRealETxMb.Text = dataLoan.sProRealETxMb_rep;
            LeaveEmptyField(sProRealETxMb);
            sProRealETxT.SelectedIndex = (int)dataLoan.sProRealETxT;
            sProRealETxHolder.Visible = !dataLoan.sRealEstateTaxExpenseInDisbMode;

            sRealETxRsrvMon.Text = dataLoan.sRealETxRsrvMon_rep;
            LeaveEmptyField(sRealETxRsrvMon);

            sSchoolTxRsrv.Text = dataLoan.sSchoolTxRsrv_rep;
            LeaveEmptyField(sSchoolTxRsrv);

            sProSchoolTx.Text = dataLoan.sProSchoolTx_rep;
            sProSchoolTx.ReadOnly = (heMigrated && dataLoan.sSchoolTaxExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements);
            LeaveEmptyField(sProSchoolTx);

            sSchoolTxRsrvMon.Text = dataLoan.sSchoolTxRsrvMon_rep;
            LeaveEmptyField(sSchoolTxRsrvMon);

            sMInsRsrv.Text = dataLoan.sMInsRsrv_rep;
            LeaveEmptyField(sMInsRsrv);

            sProMIns.Text = dataLoan.sProMIns_rep;
            LeaveEmptyField(sProMIns);



            sMInsRsrvMon.Text = dataLoan.sMInsRsrvMon_rep;
            LeaveEmptyField(sMInsRsrvMon);

            sHazInsRsrv.Text = dataLoan.sHazInsRsrv_rep;
            LeaveEmptyField(sHazInsRsrv);

            sProHazIns.Text = dataLoan.sProHazIns_rep;
            LeaveEmptyField(sProHazIns);

            sHazInsRsrvMon.Text = dataLoan.sHazInsRsrvMon_rep;
            LeaveEmptyField(sHazInsRsrvMon);

            sU4Tc.Text = dataLoan.sU4Tc_rep;
            LeaveEmptyField(sU4Tc);

            sU4TcDesc.Text = dataLoan.sU4TcDesc;
            sU4TcCode.Text = dataLoan.sU4TcCode;
            sU3Tc.Text = dataLoan.sU3Tc_rep;
            LeaveEmptyField(sU3Tc);

            sU3TcDesc.Text = dataLoan.sU3TcDesc;
            sU3TcCode.Text = dataLoan.sU3TcCode;
            sU2Tc.Text = dataLoan.sU2Tc_rep;
            LeaveEmptyField(sU2Tc);

            sU2TcDesc.Text = dataLoan.sU2TcDesc;
            sU2TcCode.Text = dataLoan.sU2TcCode;
            sU1Tc.Text = dataLoan.sU1Tc_rep;
            LeaveEmptyField(sU1Tc);

            sU1TcDesc.Text = dataLoan.sU1TcDesc;
            sU1TcCode.Text = dataLoan.sU1TcCode;
            sTitleInsF.Text = dataLoan.sTitleInsF_rep;
            LeaveEmptyField(sTitleInsF);

            sTitleInsFTable.Text = dataLoan.sTitleInsFTable;
            sAttorneyF.Text = dataLoan.sAttorneyF_rep;
            LeaveEmptyField(sAttorneyF);

            sNotaryF.Text = dataLoan.sNotaryF_rep;
            LeaveEmptyField(sNotaryF);

            sDocPrepF.Text = dataLoan.sDocPrepF_rep;
            LeaveEmptyField(sDocPrepF);

            sEscrowF.Text = dataLoan.sEscrowF_rep;
            LeaveEmptyField(sEscrowF);

            sEscrowFTable.Text = dataLoan.sEscrowFTable;
            sU3GovRtc.Text = dataLoan.sU3GovRtc_rep;
            LeaveEmptyField(sU3GovRtc);

            sU3GovRtcMb.Text = dataLoan.sU3GovRtcMb_rep;
            LeaveEmptyField(sU3GovRtcMb);

            sU3GovRtcBaseT.SelectedIndex = (int)dataLoan.sU3GovRtcBaseT;
            sU3GovRtcPc.Text = dataLoan.sU3GovRtcPc_rep;
            LeaveEmptyField(sU3GovRtcPc);

            sU3GovRtcDesc.Text = dataLoan.sU3GovRtcDesc;
            sU3GovRtcCode.Text = dataLoan.sU3GovRtcCode;
            sU2GovRtc.Text = dataLoan.sU2GovRtc_rep;
            LeaveEmptyField(sU2GovRtc);

            sU2GovRtcMb.Text = dataLoan.sU2GovRtcMb_rep;
            LeaveEmptyField(sU2GovRtcMb);

            sU2GovRtcBaseT.SelectedIndex = (int)dataLoan.sU2GovRtcBaseT;
            sU2GovRtcPc.Text = dataLoan.sU2GovRtcPc_rep;
            LeaveEmptyField(sU2GovRtcPc);

            sU2GovRtcDesc.Text = dataLoan.sU2GovRtcDesc;
            sU2GovRtcCode.Text = dataLoan.sU2GovRtcCode;
            sU1GovRtc.Text = dataLoan.sU1GovRtc_rep;
            LeaveEmptyField(sU1GovRtc);

            sU1GovRtcMb.Text = dataLoan.sU1GovRtcMb_rep;
            LeaveEmptyField(sU1GovRtcMb);

            sU1GovRtcBaseT.SelectedIndex = (int)dataLoan.sU1GovRtcBaseT;
            sU1GovRtcPc.Text = dataLoan.sU1GovRtcPc_rep;
            LeaveEmptyField(sU1GovRtcPc);

            sU1GovRtcDesc.Text = dataLoan.sU1GovRtcDesc;
            sU1GovRtcCode.Text = dataLoan.sU1GovRtcCode;
            sStateRtc.Text = dataLoan.sStateRtc_rep;
            LeaveEmptyField(sStateRtc);

            sStateRtcMb.Text = dataLoan.sStateRtcMb_rep;
            LeaveEmptyField(sStateRtcMb);

            sStateRtcBaseT.SelectedIndex = (int)dataLoan.sStateRtcBaseT;
            sStateRtcPc.Text = dataLoan.sStateRtcPc_rep;
            LeaveEmptyField(sStateRtcPc);

            sCountyRtc.Text = dataLoan.sCountyRtc_rep;
            LeaveEmptyField(sCountyRtc);

            sCountyRtcMb.Text = dataLoan.sCountyRtcMb_rep;
            LeaveEmptyField(sCountyRtcMb);

            sCountyRtcBaseT.SelectedIndex = (int)dataLoan.sCountyRtcBaseT;
            sCountyRtcPc.Text = dataLoan.sCountyRtcPc_rep;
            LeaveEmptyField(sCountyRtcPc);

            sRecF.Text = dataLoan.sRecF_rep;
            LeaveEmptyField(sRecF);

            sRecFMb.Text = dataLoan.sRecFMb_rep;
            LeaveEmptyField(sRecFMb);

            sRecBaseT.SelectedIndex = (int)dataLoan.sRecBaseT;
            sRecFPc.Text = dataLoan.sRecFPc_rep;
            LeaveEmptyField(sRecFPc);

            sU5Sc.Text = dataLoan.sU5Sc_rep;
            LeaveEmptyField(sU5Sc);

            sU5ScDesc.Text = dataLoan.sU5ScDesc;
            sU5ScCode.Text = dataLoan.sU5ScCode;
            sU4Sc.Text = dataLoan.sU4Sc_rep;
            LeaveEmptyField(sU4Sc);

            sU4ScDesc.Text = dataLoan.sU4ScDesc;
            sU4ScCode.Text = dataLoan.sU4ScCode;
            sU3Sc.Text = dataLoan.sU3Sc_rep;
            LeaveEmptyField(sU3Sc);

            sU3ScDesc.Text = dataLoan.sU3ScDesc;
            sU3ScCode.Text = dataLoan.sU3ScCode;
            sU2Sc.Text = dataLoan.sU2Sc_rep;
            LeaveEmptyField(sU2Sc);

            sU2ScDesc.Text = dataLoan.sU2ScDesc;
            sU2ScCode.Text = dataLoan.sU2ScCode;
            sU1Sc.Text = dataLoan.sU1Sc_rep;
            LeaveEmptyField(sU1Sc);

            sU1ScDesc.Text = dataLoan.sU1ScDesc;
            sU1ScCode.Text = dataLoan.sU1ScCode;
            sPestInspectF.Text = dataLoan.sPestInspectF_rep;
            LeaveEmptyField(sPestInspectF);

            sTotEstScMlds.Text = dataLoan.sTotEstScMlds_rep;
            sBrokComp1Mlds.Text = dataLoan.sBrokComp1Mlds_rep;
            LeaveEmptyField(sBrokComp1Mlds);
            sBrokComp1Pc.Text = dataLoan.sBrokComp1Pc_rep;
            sBrokComp1Pc.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sBrokComp1MldsLckd.Checked = dataLoan.sBrokComp1MldsLckd;


            sBrokComp2Mlds.Text = dataLoan.sBrokComp2Mlds_rep;
            sBrokComp2MldsLckd.Checked = dataLoan.sBrokComp2MldsLckd;
            LeaveEmptyField(sBrokComp2Mlds);

            InitItemProps(sLOrigFProps_ctrl, dataLoan.sLOrigFProps);
            InitItemProps(sLDiscntProps_ctrl, dataLoan.sLDiscntProps);
            InitItemProps(sApprFProps_ctrl, dataLoan.sApprFProps);
            InitItemProps(sCrFProps_ctrl, dataLoan.sCrFProps);
            InitItemProps(sInspectFProps_ctrl, dataLoan.sInspectFProps);
            InitItemProps(sMBrokFProps_ctrl, dataLoan.sMBrokFProps);
            InitItemProps(sTxServFProps_ctrl, dataLoan.sTxServFProps);
            InitItemProps(sProcFProps_ctrl, dataLoan.sProcFProps);
            InitItemProps(sUwFProps_ctrl, dataLoan.sUwFProps);
            InitItemProps(sWireFProps_ctrl, dataLoan.sWireFProps);
            InitItemProps(s800U1FProps_ctrl, dataLoan.s800U1FProps);
            InitItemProps(s800U2FProps_ctrl, dataLoan.s800U2FProps);
            InitItemProps(s800U3FProps_ctrl, dataLoan.s800U3FProps);
            InitItemProps(s800U4FProps_ctrl, dataLoan.s800U4FProps);
            InitItemProps(s800U5FProps_ctrl, dataLoan.s800U5FProps);
            InitItemProps(sIPiaProps_ctrl, dataLoan.sIPiaProps);
            InitItemProps(sMipPiaProps_ctrl, dataLoan.sMipPiaProps);
            InitItemProps(sHazInsPiaProps_ctrl, dataLoan.sHazInsPiaProps);
            InitItemProps(s904PiaProps_ctrl, dataLoan.s904PiaProps);
            InitItemProps(sVaFfProps_ctrl, dataLoan.sVaFfProps);
            InitItemProps(s900U1PiaProps_ctrl, dataLoan.s900U1PiaProps);
            InitItemProps(sHazInsRsrvProps_ctrl, dataLoan.sHazInsRsrvProps);
            InitItemProps(sMInsRsrvProps_ctrl, dataLoan.sMInsRsrvProps);
            InitItemProps(sSchoolTxRsrvProps_ctrl, dataLoan.sSchoolTxRsrvProps);
            InitItemProps(sRealETxRsrvProps_ctrl, dataLoan.sRealETxRsrvProps);
            InitItemProps(sFloodInsRsrvProps_ctrl, dataLoan.sFloodInsRsrvProps);
            InitItemProps(s1006RsrvProps_ctrl, dataLoan.s1006RsrvProps);
            InitItemProps(s1007RsrvProps_ctrl, dataLoan.s1007RsrvProps);
            InitItemProps(sU3RsrvProps_ctrl, dataLoan.sU3RsrvProps);
            InitItemProps(sU4RsrvProps_ctrl, dataLoan.sU4RsrvProps);
            InitItemProps(sAggregateAdjRsrvProps_ctrl, dataLoan.sAggregateAdjRsrvProps);
            InitItemProps(sEscrowFProps_ctrl, dataLoan.sEscrowFProps);
            InitItemProps(sDocPrepFProps_ctrl, dataLoan.sDocPrepFProps);
            InitItemProps(sNotaryFProps_ctrl, dataLoan.sNotaryFProps);
            InitItemProps(sAttorneyFProps_ctrl, dataLoan.sAttorneyFProps);
            InitItemProps(sTitleInsFProps_ctrl, dataLoan.sTitleInsFProps);
            InitItemProps(sU1TcProps_ctrl, dataLoan.sU1TcProps);
            InitItemProps(sU2TcProps_ctrl, dataLoan.sU2TcProps);
            InitItemProps(sU3TcProps_ctrl, dataLoan.sU3TcProps);
            InitItemProps(sU4TcProps_ctrl, dataLoan.sU4TcProps);
            InitItemProps(sRecFProps_ctrl, dataLoan.sRecFProps);
            InitItemProps(sCountyRtcProps_ctrl, dataLoan.sCountyRtcProps);
            InitItemProps(sStateRtcProps_ctrl, dataLoan.sStateRtcProps);
            InitItemProps(sU1GovRtcProps_ctrl, dataLoan.sU1GovRtcProps);
            InitItemProps(sU2GovRtcProps_ctrl, dataLoan.sU2GovRtcProps);
            InitItemProps(sU3GovRtcProps_ctrl, dataLoan.sU3GovRtcProps);
            InitItemProps(sPestInspectFProps_ctrl, dataLoan.sPestInspectFProps);
            InitItemProps(sU1ScProps_ctrl, dataLoan.sU1ScProps);
            InitItemProps(sU2ScProps_ctrl, dataLoan.sU2ScProps);
            InitItemProps(sU3ScProps_ctrl, dataLoan.sU3ScProps);
            InitItemProps(sU4ScProps_ctrl, dataLoan.sU4ScProps);
            InitItemProps(sU5ScProps_ctrl, dataLoan.sU5ScProps);

            sTotCcPto.Text = dataLoan.sTotCcPto_rep;
            sTotCcPtb.Text = dataLoan.sTotCcPtb_rep;
            sStateRtcDesc.Text = dataLoan.sStateRtcDesc;
            sRecFDesc.Text = dataLoan.sRecFDesc;
            sCountyRtcDesc.Text = dataLoan.sCountyRtcDesc;
            sRecFDesc.ToolTip = sCountyRtcDesc.ToolTip = sStateRtcDesc.ToolTip = "Enter description";
            sConsummationDLckd.Checked = dataLoan.sConsummationDLckd;
            sConsummationD.Text = dataLoan.sConsummationD_rep;
            Tools.SetRadioButtonListValue(sOriginatorCompensationPaymentSourceT, dataLoan.sOriginatorCompensationPaymentSourceT);

            sGfeOriginatorCompF.Text = dataLoan.sGfeOriginatorCompF_rep;
            sGfeOriginatorCompFPc.Text = dataLoan.sGfeOriginatorCompFPc_rep;
            sGfeOriginatorCompFMb.Text = dataLoan.sGfeOriginatorCompFMb_rep;
            Tools.SetDropDownListValue(sGfeOriginatorCompFBaseT, dataLoan.sGfeOriginatorCompFBaseT);
            if (dataLoan.sIsOriginationCompensationSourceRequired)
            {
                // 2/22/2011 dd - Need to remove Not specified option.
                foreach (ListItem item in sOriginatorCompensationPaymentSourceT.Items)
                {
                    if (item.Value == E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified.ToString("D"))
                    {
                        sOriginatorCompensationPaymentSourceT.Items.Remove(item);
                        break;
                    }
                }
            }
        }
        private int RetrieveItemProps(GoodFaithEstimateRightColumn ctrl)
        {
            return LosConvert.GfeItemProps_Pack(ctrl.Apr, ctrl.ToBrok, ctrl.PdByT, ctrl.Fha, ctrl.Poc);
        }
        public void LoadData()
        {
            m_convertLos = new LosConvert();

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(CAMLDSRE885pg1));
            dataLoan.InitLoad();
            BindDataObject(dataLoan);
            ByPassBgCalcForGfeAsDefault.SelectedIndex = IsByPassBgCalc() ? 1 : 0;

        }
        public void SaveData()
        {
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion

        /// <summary>
        /// Request by AFG users, if GFE in manual calculation mode then they want
        /// $0.00, or 0.000% to be blank. 9/8/03
        /// </summary>
        /// <param name="tb"></param>
        private void LeaveEmptyField(TextBox tb)
        {

            if (!tb.ReadOnly)
            {
                if (tb.Text == "$0.00" || tb.Text == "0.000%" || tb.Text == "0")
                {
                    tb.Text = "";
                }
            }
        }
        private void BindOtherCreditDescription(MeridianLink.CommonControls.ComboBox cb)
        {
            cb.Items.Add("Cash Deposit on sales contract");
            cb.Items.Add("Seller Credit");
            cb.Items.Add("Lender Credit");
            cb.Items.Add("Relocation Funds");
            cb.Items.Add("Employer Assisted Housing");
            cb.Items.Add("Lease Purchase Fund");
            cb.Items.Add("Borrower Paid Fees");
            cb.Items.Add("Paid Outside of Closing");
            cb.Items.Add("Broker Credit");


        }
    }

}