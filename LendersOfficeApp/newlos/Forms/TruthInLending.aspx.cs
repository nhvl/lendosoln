using System;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.UI.DataContainers;

namespace LendersOfficeApp.newlos.Forms
{
    /// <summary>
    /// If you add new fields, you'll also want to change TILData.cs.
    /// </summary>
    public partial class TruthInLending : BaseLoanPage
    {
        protected bool m_IsRateLocked = false;
        // OPM 12773

        protected string m_openedDate
        {
            get { return (string)ViewState["OpenedDate"]; }
            set { ViewState["OpenedDate"] = value; }
        }

        private void BindDataObject(CPageData dataLoan)
        {
            Tools.Bind_MipCalcType_rep(sProMInsT, dataLoan.sLT);
            Tools.SetDropDownListValue(sProMInsT, dataLoan.sProMInsT);
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
            Tools.SetDropDownListValue(sPrepmtRefundT, dataLoan.sPrepmtRefundT);
            Tools.SetDropDownListValue(sPrepmtPenaltyT, dataLoan.sPrepmtPenaltyT);
            Tools.SetDropDownListValue(sRAdjRoundT, dataLoan.sRAdjRoundT);
            Tools.SetDropDownListValue(sAssumeLT, dataLoan.sAssumeLT);
            Tools.SetDropDownListValue(sRedisclosureMethodT, dataLoan.sRedisclosureMethodT);
            string hintForTodaysDate = "Hint:  Enter 't' for today's date.";

            // 4/17/2015 gf - We do not want to display all 3 tables for old files
            // that would display the worst case as the standard.
            if (dataLoan.sFinMethT == E_sFinMethT.ARM && !dataLoan.sRAdjWorstIndex)
            {
                this.AmortizationScheduleBtn.Value = "View Amortization Schedules";
            }

            sFinMethT.Enabled = !(dataLoan.sIsRateLocked || IsReadOnly);

            sRAdj1stCapR.Text = dataLoan.sRAdj1stCapR_rep;
            sRAdjCapR.Text = dataLoan.sRAdjCapR_rep;
            sRAdjLifeCapR.Text = dataLoan.sRAdjLifeCapR_rep;
            sRAdjMarginR.Text = dataLoan.sRAdjMarginR_rep;
            sRAdjMarginR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sRAdjIndexR.Text = dataLoan.sRAdjIndexR_rep;
            sRAdjFloorR_readonly.Text = dataLoan.sRAdjFloorR_rep;
            sRAdjRoundToR.Text = dataLoan.sRAdjRoundToR_rep;
            sIsConvertibleMortgage.Checked = dataLoan.sIsConvertibleMortgage;
            sPmtAdjCapR.Text = dataLoan.sPmtAdjCapR_rep;
            sProMInsBaseAmt.Text = dataLoan.sProMInsBaseAmt_rep;
            sPmtAdjMaxBalPc.Text = dataLoan.sPmtAdjMaxBalPc_rep;
            sLateChargePc.Text = dataLoan.sLateChargePc.TrimWhitespaceAndBOM();
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sFinancedAmt.Text = dataLoan.sFinancedAmt_rep;
            sTerm.Text = dataLoan.sTerm_rep;
            sTerm.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sDue.Text = dataLoan.sDue_rep;
            sDue.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            m_openedDate = dataLoan.sOpenedD_rep;
            sFinMethDesc.Text = dataLoan.sFinMethDesc;
            sFinCharge.Text = dataLoan.sFinCharge_rep;
            sLateChargeBaseDesc.Text = dataLoan.sLateChargeBaseDesc;
            sVarRNotes.Text = dataLoan.sVarRNotes;
            sSecurityCurrentOwn.Checked = dataLoan.sSecurityCurrentOwn;
            sSecurityPurch.Checked = dataLoan.sSecurityPurch;
            sSchedPmtNumTot.Text = dataLoan.sSchedPmtNumTot_rep;
            sIOnlyMon.Text = dataLoan.sIOnlyMon_rep;
            sIOnlyMon.ToolTip = "Enter the # of months (if any) that the loan will be interest-only";
            sProMIns2Mon.Text = dataLoan.sProMIns2Mon_rep;
            sProMInsMon.Text = dataLoan.sProMInsMon_rep;
            sGradPmtR.Text = dataLoan.sGradPmtR_rep;
            sGradPmtYrs.Text = dataLoan.sGradPmtYrs_rep;
            sRAdjCapMon.Text = dataLoan.sRAdjCapMon_rep;
            sRAdj1stCapMon.Text = dataLoan.sRAdj1stCapMon_rep;
            sAprIncludesReqDeposit.Checked = dataLoan.sAprIncludesReqDeposit;
            sOnlyLatePmtEstimate.Checked = dataLoan.sOnlyLatePmtEstimate;
            sAsteriskEstimate.Checked = dataLoan.sAsteriskEstimate;

            IPreparerFields til = dataLoan.GetPreparerOfForm(E_PreparerFormT.Til, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (false == Broker.IsRemovePreparedDates)
            {
                TilPrepareDate.Text = til.PrepareDate_rep;
                TilPrepareDate.ToolTip = "Hint:  Enter 't' for today's date.  Enter 'o' for opened date.";
            }
            else
            {
                trPreparedDate.Visible = false;
            }
            TilCompanyName.Text = til.CompanyName;
            TilStreetAddr.Text = til.StreetAddr;
            TilCity.Text = til.City;
            TilState.Value = til.State;
            TilZip.Text = til.Zip;
            TilPhoneOfCompany.Text = til.PhoneOfCompany;
            TilFaxOfCompany.Text = til.FaxOfCompany;
            CFM1.IsLocked = til.IsLocked;
            CFM1.AgentRoleT = til.AgentRoleT;

            sHasDemandFeature.Checked = dataLoan.sHasDemandFeature;
            sHasVarRFeature.Checked = dataLoan.sHasVarRFeature;
            sIsPropertyBeingSoldByCreditor.Checked = dataLoan.sIsPropertyBeingSoldByCreditor;
            sPmtAdjRecastStop.Text = dataLoan.sPmtAdjRecastStop_rep;
            sPmtAdjRecastPeriodMon.Text = dataLoan.sPmtAdjRecastPeriodMon_rep;
            sPmtAdjCapMon.Text = dataLoan.sPmtAdjCapMon_rep;
            sBuydwnMon5.Text = dataLoan.sBuydwnMon5_rep;
            sBuydwnMon4.Text = dataLoan.sBuydwnMon4_rep;
            sBuydwnMon3.Text = dataLoan.sBuydwnMon3_rep;
            sBuydwnMon2.Text = dataLoan.sBuydwnMon2_rep;
            sBuydwnMon1.Text = dataLoan.sBuydwnMon1_rep;
            sSpFullAddr.Text = dataLoan.sSpFullAddr;
            sLateDays.Text = dataLoan.sLateDays.TrimWhitespaceAndBOM();
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sSchedDueD1Lckd.Checked = dataLoan.sSchedDueD1Lckd;
            sSchedDueD1.Text = dataLoan.sSchedDueD1_rep;
            sSchedDueD1.ToolTip = hintForTodaysDate;

            sBuydwnR1.Text = dataLoan.sBuydwnR1_rep;
            sBuydwnR2.Text = dataLoan.sBuydwnR2_rep;
            sBuydwnR3.Text = dataLoan.sBuydwnR3_rep;
            sBuydwnR4.Text = dataLoan.sBuydwnR4_rep;
            sBuydwnR5.Text = dataLoan.sBuydwnR5_rep;
            sProMInsCancelLtv.Text = dataLoan.sProMInsCancelLtv_rep;
            sFilingF.Text = dataLoan.sFilingF.TrimWhitespaceAndBOM();

            sProMInsR.Text = dataLoan.sProMInsR_rep;
            sProMIns.Text = dataLoan.sProMIns_rep;
            sProMInsR2.Text = dataLoan.sProMInsR2_rep;
            sProMIns2.Text = dataLoan.sProMIns2_rep;

            sSchedPmtTot.Text = dataLoan.sSchedPmtTot_rep;
            sApr.Text = dataLoan.sApr_rep;
            sApr.ReadOnly = !dataLoan.sIsUseManualApr || IsReadOnly;
            sAprIncludedCc.Text = dataLoan.sAprIncludedCc_rep;
            sAprIncludedCc.ToolTip = "This amount includes any costs that have the 'A' (APR) box checked on the GFE";
            sRAdjWorstIndex.Checked = dataLoan.sRAdjWorstIndex;
            sRAdjWorstIndex.Visible = dataLoan.sRAdjWorstIndex;

            sPpmtAmt.Text = dataLoan.sPpmtAmt_rep;
            sPpmtMon.Text = dataLoan.sPpmtMon_rep;
            sPpmtStartMon.Text = dataLoan.sPpmtStartMon_rep;
            sPpmtOneAmt.Text = dataLoan.sPpmtOneAmt_rep;
            sPpmtOneMon.Text = dataLoan.sPpmtOneMon_rep;

            sIsOptionArm.Checked = dataLoan.sIsOptionArm;
            sIsOptionArm.Enabled = !(dataLoan.sIsRateLocked || IsReadOnly);



            sSchedIR1.Text = dataLoan.sSchedIR_Rep(0, PmtInfoType.InterestRate);
            sSchedDueD12.Text = dataLoan.sSchedIR_Rep(0, PmtInfoType.DueDate);
            sSchedPmtNum1.Text = dataLoan.sSchedIR_Rep(0, PmtInfoType.PmtNumber);
            sSchedPmt1.Text = dataLoan.sSchedIR_Rep(0, PmtInfoType.PmtAmount);
            sSchedBal1.Text = dataLoan.sSchedIR_Rep(0, PmtInfoType.Bal);


            sSchedIR2.Text = dataLoan.sSchedIR2_rep;
            sSchedDueD2.Text = dataLoan.sSchedDueD2_rep;
            sSchedPmtNum2.Text = dataLoan.sSchedPmtNum2_rep;
            sSchedPmt2.Text = dataLoan.sSchedPmt2_rep;
            sSchedBal2.Text = dataLoan.sSchedBal2_rep;

            sSchedIR3.Text = dataLoan.sSchedIR3_rep;
            sSchedDueD3.Text = dataLoan.sSchedDueD3_rep;
            sSchedPmtNum3.Text = dataLoan.sSchedPmtNum3_rep;
            sSchedPmt3.Text = dataLoan.sSchedPmt3_rep;
            sSchedBal3.Text = dataLoan.sSchedBal3_rep;

            sSchedIR4.Text = dataLoan.sSchedIR4_rep;
            sSchedDueD4.Text = dataLoan.sSchedDueD4_rep;
            sSchedPmtNum4.Text = dataLoan.sSchedPmtNum4_rep;
            sSchedPmt4.Text = dataLoan.sSchedPmt4_rep;
            sSchedBal4.Text = dataLoan.sSchedBal4_rep;

            sSchedIR5.Text = dataLoan.sSchedIR5_rep;
            sSchedDueD5.Text = dataLoan.sSchedDueD5_rep;
            sSchedPmtNum5.Text = dataLoan.sSchedPmtNum5_rep;
            sSchedPmt5.Text = dataLoan.sSchedPmt5_rep;
            sSchedBal5.Text = dataLoan.sSchedBal5_rep;

            sSchedIR6.Text = dataLoan.sSchedIR6_rep;
            sSchedDueD6.Text = dataLoan.sSchedDueD6_rep;
            sSchedPmtNum6.Text = dataLoan.sSchedPmtNum6_rep;
            sSchedPmt6.Text = dataLoan.sSchedPmt6_rep;
            sSchedBal6.Text = dataLoan.sSchedBal6_rep;

            sSchedIR7.Text = dataLoan.sSchedIR7_rep;
            sSchedDueD7.Text = dataLoan.sSchedDueD7_rep;
            sSchedPmtNum7.Text = dataLoan.sSchedPmtNum7_rep;
            sSchedPmt7.Text = dataLoan.sSchedPmt7_rep;
            sSchedBal7.Text = dataLoan.sSchedBal7_rep;

            sSchedIR8.Text = dataLoan.sSchedIR8_rep;
            sSchedDueD8.Text = dataLoan.sSchedDueD8_rep;
            sSchedPmtNum8.Text = dataLoan.sSchedPmtNum8_rep;
            sSchedPmt8.Text = dataLoan.sSchedPmt8_rep;
            sSchedBal8.Text = dataLoan.sSchedBal8_rep;

            sSchedIR9.Text = dataLoan.sSchedIR9_rep;
            sSchedDueD9.Text = dataLoan.sSchedDueD9_rep;
            sSchedPmtNum9.Text = dataLoan.sSchedPmtNum9_rep;
            sSchedPmt9.Text = dataLoan.sSchedPmt9_rep;
            sSchedBal9.Text = dataLoan.sSchedBal9_rep;

            sSchedIR10.Text = dataLoan.sSchedIR10_rep;
            sSchedDueD10.Text = dataLoan.sSchedDueD10_rep;
            sSchedPmtNum10.Text = dataLoan.sSchedPmtNum10_rep;
            sSchedPmt10.Text = dataLoan.sSchedPmt10_rep;
            sSchedBal10.Text = dataLoan.sSchedBal10_rep;

            sInsReqDesc.Text = dataLoan.sInsReqDesc;
            sReqCreditLifeIns.Checked = dataLoan.sReqCreditLifeIns;
            sReqCreditDisabilityIns.Checked = dataLoan.sReqCreditDisabilityIns;
            sReqPropIns.Checked = dataLoan.sReqPropIns;
            sReqFloodIns.Checked = dataLoan.sReqFloodIns;
            sIfPurchInsFrCreditor.Checked = dataLoan.sIfPurchInsFrCreditor;
            sIfPurchPropInsFrCreditor.Checked = dataLoan.sIfPurchPropInsFrCreditor;
            sIfPurchFloodInsFrCreditor.Checked = dataLoan.sIfPurchFloodInsFrCreditor;
            sInsFrCreditorAmt.Text = dataLoan.sInsFrCreditorAmt_rep;
            sProMInsMidptCancel.Checked = dataLoan.sProMInsMidptCancel;
            sProMInsCancelMinPmts.Text = dataLoan.sProMInsCancelMinPmts_rep;

            sOptionArmMinPayPeriod.Text = dataLoan.sOptionArmMinPayPeriod_rep;
            sOptionArmMinPayIsIOOnly.Checked = dataLoan.sOptionArmMinPayIsIOOnly;
            sOptionArmIntroductoryPeriod.Text = dataLoan.sOptionArmIntroductoryPeriod_rep;
            sOptionArmInitialFixMinPmtPeriod.Text = dataLoan.sOptionArmInitialFixMinPmtPeriod_rep;
            Tools.SetDropDownListValue(sOptionArmMinPayOptionT, dataLoan.sOptionArmMinPayOptionT);
            sOptionArmPmtDiscount.Text = dataLoan.sOptionArmPmtDiscount_rep;
            sOptionArmNoteIRDiscount.Text = dataLoan.sOptionArmNoteIRDiscount_rep;
            sIsFullAmortAfterRecast.Checked = dataLoan.sIsFullAmortAfterRecast;
            sOptionArmTeaserR.Text = dataLoan.sOptionArmTeaserR_rep;
            sOptionArmTeaserR.ReadOnly = (dataLoan.sIsRateLocked || IsReadOnly);
            m_IsRateLocked = dataLoan.sIsRateLocked;
            sDtiUsingMaxBalPc.Checked = dataLoan.sDtiUsingMaxBalPc;

            CFM1.Type = "21";
            CFM1.RestrictOfficialDropDownTypes = false;
            CFM1.CompanyNameField = TilCompanyName.ClientID;
            CFM1.StreetAddressField = TilStreetAddr.ClientID;
            CFM1.CityField = TilCity.ClientID;
            CFM1.StateField = TilState.ClientID;
            CFM1.ZipField = TilZip.ClientID;
            CFM1.CompanyPhoneField = TilPhoneOfCompany.ClientID;
            CFM1.CompanyFaxField = TilFaxOfCompany.ClientID;
            CFM1.IsAllowLockableFeature = true;

            if (Broker.IsProtectDisclosureDates)
            {
                sTilInitialDisclosureD.ReadOnly = true;
                sTilRedisclosureD.ReadOnly = true;
                sTilRedisclosuresReceivedD.ReadOnly = true;
                sRedisclosureMethodT.Enabled = false;
            }
            sTilInitialDisclosureD.Text = dataLoan.sTilInitialDisclosureD_rep;
            sTilInitialDisclosureD.ToolTip = hintForTodaysDate;
            sTilRedisclosureD.Text = dataLoan.sTilRedisclosureD_rep;
            sTilRedisclosureD.ToolTip = hintForTodaysDate;
            sTilRedisclosuresReceivedD.Text = dataLoan.sTilRedisclosureD_rep;
            sTilRedisclosuresReceivedD.ToolTip = hintForTodaysDate;
            sTilRedisclosuresReceivedD.Text = dataLoan.sTilRedisclosuresReceivedD_rep;
            sTilRedisclosuresReceivedD.ToolTip = hintForTodaysDate;

            // These fields will be disabled unless sPrepmtPenaltyT is set to "may"
            // Does this affect DM export? Let's say yes. OPM 61315
            if (E_sPrepmtPenaltyT.May == dataLoan.sPrepmtPenaltyT)
            {
                sPrepmtPeriodMonths.Text = dataLoan.sPrepmtPeriodMonths_rep;
                sSoftPrepmtPeriodMonths.Text = dataLoan.sSoftPrepmtPeriodMonths_rep;
            }

            sInitAPR.Text = dataLoan.sInitAPR_rep;
            sLastDiscAPR.Text = dataLoan.sLastDiscAPR_rep;

            this.sInitAPR.ReadOnly = dataLoan.sCalculateInitAprAndLastDiscApr;
            this.sLastDiscAPR.ReadOnly = dataLoan.sCalculateInitAprAndLastDiscApr;
            
            sArmIndexNameVstr.Text = dataLoan.sArmIndexNameVstr;

            RateFloorPopupFields popupFields = new RateFloorPopupFields();
            popupFields.sIsRAdjFloorRReadOnly = dataLoan.sIsRAdjFloorRReadOnly.ToString();
            popupFields.sNoteIR = dataLoan.sNoteIR_rep;
            popupFields.sRAdjFloorAddR = dataLoan.sRAdjFloorAddR_rep;
            popupFields.sRAdjFloorBaseR = dataLoan.sRAdjFloorBaseR_rep;
            popupFields.sRAdjFloorCalcT = dataLoan.sRAdjFloorCalcT;
            popupFields.sRAdjFloorCalcTLabel = Tools.GetsRAdjFloorCalcTLabel(dataLoan.sRAdjFloorCalcT);
            popupFields.sRAdjFloorLifeCapR = dataLoan.sRAdjFloorLifeCapR_rep;
            popupFields.sRAdjFloorR = dataLoan.sRAdjFloorR_rep;
            popupFields.sRAdjFloorTotalR = dataLoan.sRAdjFloorTotalR_rep;
            popupFields.sRAdjLifeCapR = dataLoan.sRAdjLifeCapR_rep;

            RateFloorPopup.SetRateLockPopupFieldValues(popupFields);
        }



        protected override void LoadData()
        {
            DataAccess.CTILData dataLoan = new CTILData(LoanID);
            dataLoan.InitLoad();

            BindDataObject(dataLoan);
        }


        protected void PageInit(object sender, System.EventArgs e)
        {
            Tools.Bind_sFinMethT(sFinMethT);
            Tools.Bind_sRAdjRoundT(sRAdjRoundT);
            Tools.Bind_sOptionArmMinPayOptionT(sOptionArmMinPayOptionT);
            Tools.Bind_sRedisclosureMethodT(sRedisclosureMethodT);
            RegisterService("truthinlendinginfo", "/newlos/forms/TruthInLendingService.aspx");
            this.PageTitle = "Truth In Lending";
            this.PageID = "TIL";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CTruthInLendingPDF);
            TilZip.SmartZipcode(TilCity, TilState);
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterCSS("jquery-ui-1.11.custom.css");
            this.EnableJqueryMigrate = false;        
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            IsAppSpecific = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
