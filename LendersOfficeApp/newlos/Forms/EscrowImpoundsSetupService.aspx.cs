﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Forms
{
    public class EscrowImpoundsSetupServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected BrokerDB Broker
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BrokerDB; }
        }
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(EscrowImpoundsSetupServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            bool heMigrated = dataLoan.sIsHousingExpenseMigrated;

            if (!dataLoan.sHazardExpenseInDisbursementMode)
            {
                dataLoan.sProHazInsR_rep = GetString("sProHazInsR");
                dataLoan.sProHazInsT = (E_PercentBaseT)GetInt("sProHazInsT");
                dataLoan.sProHazInsMb_rep = GetString("sProHazInsMb");
            }
            dataLoan.sHazInsRsrvEscrowedTri = GetBool("sHazInsRsrvEscrowedTri") ? E_TriState.Yes : E_TriState.No;
            dataLoan.sHazInsRsrvMonLckd = GetBool("sHazInsRsrvMonLckd");
            if (dataLoan.sHazInsRsrvMonLckd) { dataLoan.sHazInsRsrvMon_rep = GetString("sHazInsRsrvMon"); }

            dataLoan.sMInsRsrvEscrowedTri = GetBool("sMInsRsrvEscrowedTri") ? E_TriState.Yes : E_TriState.No;
            dataLoan.sMInsRsrvMonLckd = GetBool("sMInsRsrvMonLckd");
            if (dataLoan.sMInsRsrvMonLckd) { dataLoan.sMInsRsrvMon_rep = GetString("sMInsRsrvMon"); }

            if (!dataLoan.sRealEstateTaxExpenseInDisbMode)
            {
                dataLoan.sProRealETxR_rep = GetString("sProRealETxR");
                dataLoan.sProRealETxT = (E_PercentBaseT)GetInt("sProRealETxT");
                dataLoan.sProRealETxMb_rep = GetString("sProRealETxMb");
            }
            dataLoan.sRealETxRsrvEscrowedTri = GetBool("sRealETxRsrvEscrowedTri") ? E_TriState.Yes : E_TriState.No;
            dataLoan.sRealETxRsrvMonLckd = GetBool("sRealETxRsrvMonLckd");
            if (dataLoan.sRealETxRsrvMonLckd) { dataLoan.sRealETxRsrvMon_rep = GetString("sRealETxRsrvMon"); }

            if (!heMigrated || dataLoan.sSchoolTaxExpense.AnnualAmtCalcType != E_AnnualAmtCalcTypeT.Disbursements)
            {
                dataLoan.sProSchoolTx_rep = GetString("sProSchoolTx");
            }
            dataLoan.sSchoolTxRsrvEscrowedTri = GetBool("sSchoolTxRsrvEscrowedTri") ? E_TriState.Yes : E_TriState.No;
            dataLoan.sSchoolTxRsrvMonLckd = GetBool("sSchoolTxRsrvMonLckd");
            if (dataLoan.sSchoolTxRsrvMonLckd) { dataLoan.sSchoolTxRsrvMon_rep = GetString("sSchoolTxRsrvMon"); }

            if (!heMigrated || dataLoan.sFloodExpense.AnnualAmtCalcType != E_AnnualAmtCalcTypeT.Disbursements)
            {
                dataLoan.sProFloodIns_rep = GetString("sProFloodIns");
            }
            dataLoan.sFloodInsRsrvEscrowedTri = GetBool("sFloodInsRsrvEscrowedTri") ? E_TriState.Yes : E_TriState.No;
            dataLoan.sFloodInsRsrvMonLckd = GetBool("sFloodInsRsrvMonLckd");
            if (dataLoan.sFloodInsRsrvMonLckd) { dataLoan.sFloodInsRsrvMon_rep = GetString("sFloodInsRsrvMon"); }

            dataLoan.s1006ProHExpDesc = GetString("s1006ProHExpDesc");
            if (!heMigrated || dataLoan.sLine1008Expense.AnnualAmtCalcType != E_AnnualAmtCalcTypeT.Disbursements)
            {
                dataLoan.s1006ProHExp_rep = GetString("s1006ProHExp");
            }
            dataLoan.s1006RsrvEscrowedTri = GetBool("s1006RsrvEscrowedTri") ? E_TriState.Yes : E_TriState.No;
            dataLoan.s1006RsrvMonLckd = GetBool("s1006RsrvMonLckd");
            if (dataLoan.s1006RsrvMonLckd) { dataLoan.s1006RsrvMon_rep = GetString("s1006RsrvMon"); }

            dataLoan.s1007ProHExpDesc = GetString("s1007ProHExpDesc");
            if (!heMigrated || dataLoan.sLine1009Expense.AnnualAmtCalcType != E_AnnualAmtCalcTypeT.Disbursements)
            {
                dataLoan.s1007ProHExp_rep = GetString("s1007ProHExp");
            }
            dataLoan.s1007RsrvEscrowedTri = GetBool("s1007RsrvEscrowedTri") ? E_TriState.Yes : E_TriState.No;
            dataLoan.s1007RsrvMonLckd = GetBool("s1007RsrvMonLckd");
            if (dataLoan.s1007RsrvMonLckd) { dataLoan.s1007RsrvMon_rep = GetString("s1007RsrvMon"); }

            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                dataLoan.sU3RsrvDesc = GetString("sU3RsrvDesc");
                if (!heMigrated || dataLoan.sLine1010Expense.AnnualAmtCalcType != E_AnnualAmtCalcTypeT.Disbursements)
                {
                    dataLoan.sProU3Rsrv_rep = GetString("sProU3Rsrv");
                }
                dataLoan.sU3RsrvEscrowedTri = GetBool("sU3RsrvEscrowedTri") ? E_TriState.Yes : E_TriState.No;
                dataLoan.sU3RsrvMonLckd = GetBool("sU3RsrvMonLckd");
                if (dataLoan.sU3RsrvMonLckd) { dataLoan.sU3RsrvMon_rep = GetString("sU3RsrvMon"); }
                //dataLoan.sU3Rsrv_rep = GetString("sU3Rsrv"); // don't set since it's calculated.

                dataLoan.sU4RsrvDesc = GetString("sU4RsrvDesc");
                if (!heMigrated || dataLoan.sLine1011Expense.AnnualAmtCalcType != E_AnnualAmtCalcTypeT.Disbursements)
                {
                    dataLoan.sProU4Rsrv_rep = GetString("sProU4Rsrv");
                }
                dataLoan.sU4RsrvEscrowedTri = GetBool("sU4RsrvEscrowedTri") ? E_TriState.Yes : E_TriState.No;
                dataLoan.sU4RsrvMonLckd = GetBool("sU4RsrvMonLckd");
                if (dataLoan.sU4RsrvMonLckd) { dataLoan.sU4RsrvMon_rep = GetString("sU4RsrvMon"); }
            }

            dataLoan.sSchedDueD1Lckd = GetBool("sSchedDueD1Lckd");
            if(dataLoan.sSchedDueD1Lckd) { dataLoan.sSchedDueD1_rep = GetString("sSchedDueD1");}
            dataLoan.sAggregateAdjRsrvLckd = GetBool("sAggregateAdjRsrvLckd");
            if(dataLoan.sAggregateAdjRsrvLckd){ dataLoan.sAggregateAdjRsrv_rep = GetString("sAggregateAdjRsrv");}

            dataLoan.s1006RsrvProps = RetrieveItemProps("s1006RsrvProps", true, false, false, false, dataLoan.s1006RsrvProps);
            dataLoan.s1007RsrvProps = RetrieveItemProps("s1007RsrvProps", true, false, false, false, dataLoan.s1007RsrvProps);
            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                dataLoan.sU3RsrvProps = RetrieveItemProps("sU3RsrvProps", true, false, false, false, dataLoan.sU3RsrvProps);
                dataLoan.sU4RsrvProps = RetrieveItemProps("sU4RsrvProps", true, false, false, false, dataLoan.sU4RsrvProps);
            }

            if (dataLoan.sIsHousingExpenseMigrated)
            {
                dataLoan.sAggEscrowCalcModeT = (E_AggregateEscrowCalculationModeT)GetInt("sAggEscrowCalcModeT");
            }
            if (dataLoan.sIsHousingExpenseMigrated && Broker.EnableCustomaryEscrowImpoundsCalculation)
            {
                dataLoan.sCustomaryEscrowImpoundsCalcMinT = (E_CustomaryEscrowImpoundsCalcMinT)GetInt("sCustomaryEscrowImpoundsCalcMinT");
            }
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            bool heMigrated = dataLoan.sIsHousingExpenseMigrated;

            SetResult("sProHazInsR", dataLoan.sProHazInsR_rep);
            SetResult("sProHazInsT", dataLoan.sProHazInsT);
            SetResult("sProHazInsMb", dataLoan.sProHazInsMb_rep);
            if (heMigrated && dataLoan.sHazardExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                SetResult("sProHazIns", dataLoan.sHazardExpense.MonthlyAmtServicing_rep);
            }
            else
            {
                SetResult("sProHazIns", dataLoan.sProHazIns_rep);
            }
            SetResult("sHazInsRsrvEscrowedTri", (E_TriState.Yes == dataLoan.sHazInsRsrvEscrowedTri));
            SetResult("sHazInsRsrvMonLckd", dataLoan.sHazInsRsrvMonLckd);
            SetResult("sHazInsRsrvMon", dataLoan.sHazInsRsrvMon_rep);
            SetResult("sHazInsRsrv", dataLoan.sHazInsRsrv_rep);

            SetResult("sProMIns", dataLoan.sProMIns_rep);
            SetResult("sMInsRsrvEscrowedTri", E_TriState.Yes == dataLoan.sMInsRsrvEscrowedTri);
            SetResult("sMInsRsrvMonLckd", dataLoan.sMInsRsrvMonLckd);
            SetResult("sMInsRsrvMon", dataLoan.sMInsRsrvMon_rep); 
            SetResult("sMInsRsrv", dataLoan.sMInsRsrv_rep);

            SetResult("sProRealETxR", dataLoan.sProRealETxR_rep);
            SetResult("sProRealETxT", dataLoan.sProRealETxT);
            SetResult("sProRealETxMb", dataLoan.sProRealETxMb_rep);
            if (heMigrated && dataLoan.sRealEstateTaxExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                SetResult("sProRealETx", dataLoan.sRealEstateTaxExpense.MonthlyAmtServicing_rep);
            }
            else
            {
                SetResult("sProRealETx", dataLoan.sProRealETx_rep);
            }
            SetResult("sRealETxRsrvEscrowedTri", (E_TriState.Yes == dataLoan.sRealETxRsrvEscrowedTri));
            SetResult("sRealETxRsrvMonLckd", dataLoan.sRealETxRsrvMonLckd);
            SetResult("sRealETxRsrvMon", dataLoan.sRealETxRsrvMon_rep);
            SetResult("sRealETxRsrv", dataLoan.sRealETxRsrv_rep);

            if (heMigrated && dataLoan.sSchoolTaxExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                SetResult("sProSchoolTx", dataLoan.sSchoolTaxExpense.MonthlyAmtServicing_rep);
            }
            else
            {
                SetResult("sProSchoolTx", dataLoan.sProSchoolTx_rep);
            }
            SetResult("sSchoolTxRsrvEscrowedTri", (E_TriState.Yes == dataLoan.sSchoolTxRsrvEscrowedTri));
            SetResult("sSchoolTxRsrvMonLckd", dataLoan.sSchoolTxRsrvMonLckd);
            SetResult("sSchoolTxRsrvMon", dataLoan.sSchoolTxRsrvMon_rep);
            SetResult("sSchoolTxRsrv", dataLoan.sSchoolTxRsrv_rep);

            if (heMigrated && dataLoan.sFloodExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                SetResult("sProFloodIns", dataLoan.sFloodExpense.MonthlyAmtServicing_rep);
            }
            else
            {
                SetResult("sProFloodIns", dataLoan.sProFloodIns_rep);
            }
            SetResult("sFloodInsRsrvEscrowedTri", (E_TriState.Yes == dataLoan.sFloodInsRsrvEscrowedTri));
            SetResult("sFloodInsRsrvMonLckd", dataLoan.sFloodInsRsrvMonLckd);
            SetResult("sFloodInsRsrvMon", dataLoan.sFloodInsRsrvMon_rep);
            SetResult("sFloodInsRsrv", dataLoan.sFloodInsRsrv_rep); 

            SetResult("s1006ProHExpDesc", dataLoan.s1006ProHExpDesc);
            if (heMigrated && dataLoan.sLine1008Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                SetResult("s1006ProHExp", dataLoan.sLine1008Expense.MonthlyAmtServicing_rep);
            }
            else
            {
                SetResult("s1006ProHExp", dataLoan.s1006ProHExp_rep);
            }
            SetResult("s1006RsrvEscrowedTri", (E_TriState.Yes == dataLoan.s1006RsrvEscrowedTri));
            SetResult("s1006RsrvMonLckd", dataLoan.s1006RsrvMonLckd);
            SetResult("s1006RsrvMon", dataLoan.s1006RsrvMon_rep);
            SetResult("s1006Rsrv", dataLoan.s1006Rsrv_rep); 

            SetResult("s1007ProHExpDesc", dataLoan.s1007ProHExpDesc);
            if (heMigrated && dataLoan.sLine1009Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                SetResult("s1007ProHExp", dataLoan.sLine1009Expense.MonthlyAmtServicing_rep);
            }
            else
            {
                SetResult("s1007ProHExp", dataLoan.s1007ProHExp_rep);
            }
            SetResult("s1007RsrvEscrowedTri", (E_TriState.Yes == dataLoan.s1007RsrvEscrowedTri));
            SetResult("s1007RsrvMonLckd", dataLoan.s1007RsrvMonLckd);
            SetResult("s1007RsrvMon", dataLoan.s1007RsrvMon_rep);
            SetResult("s1007Rsrv", dataLoan.s1007Rsrv_rep); 

            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                SetResult("sU3RsrvDesc", dataLoan.sU3RsrvDesc);
                if (heMigrated && dataLoan.sLine1010Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                {
                    SetResult("sProU3Rsrv", dataLoan.sLine1010Expense.MonthlyAmtServicing_rep);
                }
                else
                {
                    SetResult("sProU3Rsrv", dataLoan.sProU3Rsrv_rep);
                }
                SetResult("sU3RsrvEscrowedTri", (E_TriState.Yes == dataLoan.sU3RsrvEscrowedTri));
                SetResult("sU3RsrvMonLckd", dataLoan.sU3RsrvMonLckd);
                SetResult("sU3RsrvMon", dataLoan.sU3RsrvMon_rep);
                SetResult("sU3Rsrv", dataLoan.sU3Rsrv_rep); 

                SetResult("sU4RsrvDesc", dataLoan.sU4RsrvDesc);
                if (heMigrated && dataLoan.sLine1011Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                {
                    SetResult("sProU4Rsrv", dataLoan.sLine1011Expense.MonthlyAmtServicing_rep);
                }
                else
                {
                    SetResult("sProU4Rsrv", dataLoan.sProU4Rsrv_rep);
                }
                SetResult("sU4RsrvEscrowedTri", (E_TriState.Yes == dataLoan.sU4RsrvEscrowedTri));
                SetResult("sU4RsrvMonLckd", dataLoan.sU4RsrvMonLckd);
                SetResult("sU4RsrvMon", dataLoan.sU4RsrvMon_rep);
                SetResult("sU4Rsrv", dataLoan.sU4Rsrv_rep); 
            }

            SetResult("sSchedDueD1Lckd",dataLoan.sSchedDueD1Lckd);
            SetResult("sSchedDueD1", dataLoan.sSchedDueD1_rep);
            SetResult("sAggregateAdjRsrvLckd", dataLoan.sAggregateAdjRsrvLckd);
            SetResult("sAggregateAdjRsrv", dataLoan.sAggregateAdjRsrv_rep);
            SetResult("sGfeInitialImpoundDeposit",dataLoan.sGfeInitialImpoundDeposit_rep);

            InitItemProps("s1006RsrvProps", dataLoan.s1006RsrvProps);
            InitItemProps("s1007RsrvProps", dataLoan.s1007RsrvProps);
            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                InitItemProps("sU3RsrvProps", dataLoan.sU3RsrvProps);
                InitItemProps("sU4RsrvProps", dataLoan.sU4RsrvProps);
            }

            if (dataLoan.sIsHousingExpenseMigrated)
            {
                SetResult("sAggEscrowCalcModeT", dataLoan.sAggEscrowCalcModeT);
            }
            if (dataLoan.sIsHousingExpenseMigrated && Broker.EnableCustomaryEscrowImpoundsCalculation)
            {
                SetResult("sCustomaryEscrowImpoundsCalcMinT", dataLoan.sCustomaryEscrowImpoundsCalcMinT);
            }
        }
        private void InitItemProps(string name, int props) // Todo: make this and RetrieveItemProps common with gfe2010service. 
        {
            SetResult(name + "_ctrl_Apr_chk", LosConvert.GfeItemProps_Apr(props));
            SetResult(name + "_ctrl_Fha_chk", LosConvert.GfeItemProps_FhaAllow(props));
            SetResult(name + "_ctrl_ToBrok_chk", LosConvert.GfeItemProps_ToBr(props));
            SetResult(name + "_ctrl_PdByT_dd", LosConvert.GfeItemProps_Payer(props));
            SetResult(name + "_ctrl_Borr_chk", LosConvert.GfeItemProps_Borr(props));
            SetResult(name + "_ctrl_Poc_chk", LosConvert.GfeItemProps_Poc(props));
            SetResult(name + "_ctrl_BF_chk", LosConvert.GfeItemProps_BF(props));
            // SetResult(name + "_ctrl_GBF_chk", LosConvert.GfeItemProps_GBF(props));
            SetResult(name + "_ctrl_TrdPty_hdn", LosConvert.GfeItemProps_PaidToThirdParty(props) ? "Y" : "N");
            SetResult(name + "_ctrl_Aff_hdn", LosConvert.GfeItemProps_ThisPartyIsAffiliate(props) ? "Y" : "N");
            SetResult(name + "_ctrl_QmWarn_hdn", LosConvert.GfeItemProps_ShowQmWarning(props) ? "Y" : "N");
        }
        private int RetrieveItemProps(string name, bool hasPOC, bool hasB, bool hasGBF, bool hasBF, int originalProps)
        {
            return RetrieveItemProps(name, hasPOC, hasB, hasGBF, hasBF, false /*hasBorr*/, originalProps);
        }
        // 2/26/14 gf - opm 150695, Since it is now possible to combine the GFE/SC pages, we
        // maintain some of the settings where they may possibly be set on the SC page.
        // POC only needs to be preserved when it isn't shown. DFLP will need to be preserved as
        // long as POC is false.
        private int RetrieveItemProps(string name, bool hasPOC, bool hasB, bool hasGBF, bool hasBF, bool hasBorr, int originalProps)
        {
            bool apr = GetBool(name + "_ctrl_Apr_chk", false);
            bool fhaAllow = GetBool(name + "_ctrl_Fha_chk", false);
            bool toBr = hasB ? GetBool(name + "_ctrl_ToBrok_chk") : false;
            int payer = GetInt(name + "_ctrl_PdByT_dd", 0);
            bool poc = hasPOC ? GetBool(name + "_ctrl_Poc_chk") : LosConvert.GfeItemProps_Poc(originalProps);

            bool gbf = false;
            // bool gbf = hasGBF ? GetBool(name + "_ctrl_GBF_chk") : false;

            bool bf = hasBF ? GetBool(name + "_ctrl_BF_chk") : false;

            bool borr = hasBorr ? GetBool(name + "_ctrl_Borr_chk") : false;

            bool dflp = false;
            if (!poc)
            {
                dflp = LosConvert.GfeItemProps_Dflp(originalProps);
            }
            bool trdPty = GetString(name + "_ctrl_TrdPty_hdn", "N") == "Y";
            bool aff = GetString(name + "_ctrl_Aff_hdn", "N") == "Y";
            bool qmWarn = GetString(name + "_ctrl_QmWarn_hdn", "N") == "Y";

            return LosConvert.GfeItemProps_Pack(apr, toBr, payer, fhaAllow, poc, dflp, gbf, bf, borr, trdPty, aff, qmWarn);
        }
    }
    public partial class EscrowImpoundsSetupService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new EscrowImpoundsSetupServiceItem());
        }
    }
}
