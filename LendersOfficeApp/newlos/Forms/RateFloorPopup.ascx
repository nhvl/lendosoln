﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RateFloorPopup.ascx.cs" Inherits="LendersOfficeApp.newlos.Forms.RateFloorPopup" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<style type="text/css">
    #RateFloorCalcPopup
    {
        display:none;
    }
    .RateFloorPopup .ui-dialog-titlebar-close
    {
        display: none;
    }
    .sRAdjFloorR
    {
        margin-left: 28px;
    }
    .RateFloorCalcPopup_Btns
    {
        text-align: center;
    }
    .RateFloorText
    {
        padding-top: 4px;
        vertical-align: text-top;
        text-align: right;
    }
</style>

<script type="text/javascript">
     
    var oldsRAdjFloorCalcT;
    var oldsRAdjFloorAddR;
    var oldsRAdjFloorR;

    var RateFloorCalcPopup_CloseBtn;
    var RateFloorCalcPopup_OkBtn;
    var RateFloorCalcPopup;
        
    var sRAdjFloorCalcT;
    var sRAdjFloorCalcT_hidden;
    var sRAdjFloorCalcT;

    var sRAdjFloorAddR;
    var sRAdjFloorR;
    var sRAdjFloorBaseR;
    var sRAdjFloorTotalR;
    var sRAdjFloorLifeCapR;
    var sRAdjFloorTotalR;
    var sIsRAdjFloorRReadOnly;

    var sNoteIR_popup;
    var sRAdjLifeCapR_popup;
    var sRAdjFloorCalcT_label;
    
    function RateFloorPopupInit()
    {
        $j(sRAdjFloorCalcT_label).text($j(sRAdjFloorCalcT_hidden).val());
        if ($j(sIsRAdjFloorRReadOnly).val() === "True") {
            $j(sRAdjFloorR).prop("readonly", true);
            $j(".HideOnSetManually").show();
        }
        else {
            $j(sRAdjFloorR).prop("readonly", false);
            $j(".HideOnSetManually").hide();
        }
    }

    jQuery(function ($) {
        RateFloorCalcPopup_CloseBtn = "#" + <%=AspxTools.JsGetClientIdString(RateFloorCalcPopup_CloseBtn)%>;
        RateFloorCalcPopup_OkBtn = "#" + <%=AspxTools.JsGetClientIdString(RateFloorCalcPopup_OkBtn)%>;
        RateFloorCalcPopup = "#" + <%=AspxTools.JsGetClientIdString(RateFloorCalcPopup)%>;
        
        sRAdjFloorCalcT = "#" + <%=AspxTools.JsGetClientIdString(sRAdjFloorCalcT)%>;
        sRAdjFloorCalcT_hidden = "#" + <%=AspxTools.JsGetClientIdString(sRAdjFloorCalcT_hidden)%>;
        sRAdjFloorCalcT = "#" + <%=AspxTools.JsGetClientIdString(sRAdjFloorCalcT)%>;

        sRAdjFloorAddR = "#" + <%=AspxTools.JsGetClientIdString(sRAdjFloorAddR)%>;
        sRAdjFloorR = "#" + <%=AspxTools.JsGetClientIdString(sRAdjFloorR)%>;
        sRAdjFloorBaseR = "#" + <%=AspxTools.JsGetClientIdString(sRAdjFloorBaseR)%>;
        sRAdjFloorTotalR = "#" + <%=AspxTools.JsGetClientIdString(sRAdjFloorTotalR)%>;
        sRAdjFloorLifeCapR = "#" + <%=AspxTools.JsGetClientIdString(sRAdjFloorLifeCapR)%>;
        sRAdjFloorTotalR = "#" + <%=AspxTools.JsGetClientIdString(sRAdjFloorTotalR)%>;
        sIsRAdjFloorRReadOnly = "#" + <%=AspxTools.JsGetClientIdString(sIsRAdjFloorRReadOnly)%>;

        sNoteIR_popup = "#" + <%=AspxTools.JsGetClientIdString(sNoteIR_popup)%>;
        sRAdjLifeCapR_popup = "#" + <%=AspxTools.JsGetClientIdString(sRAdjLifeCapR_popup)%>;
        sRAdjFloorCalcT_label = "#" + <%=AspxTools.JsGetClientIdString(sRAdjFloorCalcT_label)%>;

        RateFloorCalcPopup = "#" + <%=AspxTools.JsGetClientIdString(RateFloorCalcPopup)%>;

        $("#RateFloorCalcLink").on("click", function () {
            showRateFloorPopup();
        });

        $(RateFloorCalcPopup_CloseBtn).on("click", function () {
            $(sRAdjFloorCalcT).val(oldsRAdjFloorCalcT);
            $(sRAdjFloorAddR).val(oldsRAdjFloorAddR);
            $(sRAdjFloorR).val(oldsRAdjFloorR);
            $(RateFloorCalcPopup).dialog("close");
            refreshCalculation();
        });

        $(RateFloorCalcPopup_OkBtn).on("click", function () {
            $(RateFloorCalcPopup).dialog("close");
        });

        $(".RefreshCalculation").on("change", function () {
            refreshCalculation();
        });
    });

    function _postGetAllFormValues(obj) {
        obj["sRAdjFloorCalcT"] = $j(sRAdjFloorCalcT).val();
        obj["sRAdjFloorAddR"] = $j(sRAdjFloorAddR).val();
        obj["sRAdjFloorR"] = $j(sRAdjFloorR).val();
    }

    function _postRefreshCalculation(resultValue, clientId) {
        $j(sRAdjFloorCalcT).val(resultValue["sRAdjFloorCalcT"]);
        $j(sRAdjFloorCalcT_hidden).val(resultValue["sRAdjFloorCalcT_hidden"]);
        $j(sRAdjFloorBaseR).val(resultValue["sRAdjFloorBaseR"]);
        $j(sRAdjFloorAddR).val(resultValue["sRAdjFloorAddR"]);
        $j(sNoteIR_popup).val(resultValue["sNoteIR_popup"]);
        $j(sRAdjLifeCapR_popup).val(resultValue["sRAdjLifeCapR_popup"]);
        $j(sRAdjFloorTotalR).val(resultValue["sRAdjFloorTotalR"]);
        $j(sRAdjFloorLifeCapR).val(resultValue["sRAdjFloorLifeCapR"]);
        $j(sRAdjFloorR).val(resultValue["sRAdjFloorR"]);
        $j(sIsRAdjFloorRReadOnly).val(resultValue["sIsRAdjFloorRReadOnly"]);
    }

    function showRateFloorPopup() {
        oldsRAdjFloorCalcT = $j(sRAdjFloorCalcT).val();
        oldsRAdjFloorAddR = $j(sRAdjFloorAddR).val();
        oldsRAdjFloorR = $j(sRAdjFloorR).val();

        $j(RateFloorCalcPopup).dialog({
            dialogClass: "RateFloorPopup",
            width: 630,
            height: 200,
            modal: true,
            title: "Rate Floor Calculation",
            resizable: false,
            draggable: false,
        });
    }
</script>

<div id="RateFloorCalcPopup" style="display: none;" runat="server">
    <table>
        <tr>
            <td>
                Loan Program Rate Floor Calculation
            </td>
            <td>
                <asp:DropDownList ID="sRAdjFloorCalcT" runat="server" class="RefreshCalculation"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="RateFloorText">
                Rate Floor 
            </td>
            <td>
                <table>
                    <tr class="HideOnSetManually">
                        <td valign="top">
                            = Max( <span id="sRAdjFloorCalcT_label" runat="server"></span> + Fixed Percent, Note Rate - Life Adj Cap )
                            <asp:HiddenField ID="sRAdjFloorCalcT_hidden" runat="server" />
                        </td>
                    </tr>
                    <tr class="HideOnSetManually">
                        <td>
                            = Max(
                            <ml:PercentTextBox ID="sRAdjFloorBaseR" runat="server" ReadOnly="true"></ml:PercentTextBox> + 
                            <ml:PercentTextBox ID="sRAdjFloorAddR" runat="server" class="RefreshCalculation"></ml:PercentTextBox>,
                            <ml:PercentTextBox ID="sNoteIR_popup" runat="server" ReadOnly="true" data-field-id="sNoteIR"></ml:PercentTextBox> - 
                            <ml:PercentTextBox ID="sRAdjLifeCapR_popup" runat="server" ReadOnly="true" data-field-id="sRAdjLifeCapR"></ml:PercentTextBox> )
                        </td>
                    </tr>
                    <tr class="HideOnSetManually">
                        <td>
                            = Max(
                            <ml:PercentTextBox ID="sRAdjFloorTotalR" runat="server" ReadOnly="true"></ml:PercentTextBox>,
                            <ml:PercentTextBox ID="sRAdjFloorLifeCapR" runat="server" ReadOnly="true"></ml:PercentTextBox> )
                        </td>
                    </tr>
                    <tr>
                        <td>
                            = <ml:PercentTextBox ID="sRAdjFloorR" runat="server" class="sRAdjFloorR RefreshCalculation"></ml:PercentTextBox>
                            <asp:HiddenField ID="sIsRAdjFloorRReadOnly" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div class="RateFloorCalcPopup_Btns">
        <input type="button" value="OK" id="RateFloorCalcPopup_OkBtn" runat="server"/>
        <input type="button" value="Close" id="RateFloorCalcPopup_CloseBtn" runat="server"/>
    </div>
</div>
