<%@ Register TagPrefix="uc1" TagName="MLDSpg2" Src="MLDSpg2.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MLDSpg1" Src="MLDSpg1.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="CAMLDS.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.CAMLDS" %>
<%@ Register TagPrefix="uc1" TagName="Tabs" Src="~/common/BaseTabPage.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
    <title>CAMLDS</title>
    <style type="text/css">
      .w-400 {
        width: 400px;
      }
    </style>
  </head>
  <body MS_POSITIONING="FlowLayout" class="RightBackground" scroll="yes">
	<script language=javascript>
<!--
  var oRolodex;
  function _init() {
    oRolodex = new cRolodex();

    if (typeof(_initControl) == 'function') _initControl();
  }

//-->
</script>

    <form id="CAMLDS" method="post" runat="server">
<TABLE id=Table1 cellSpacing=0 cellPadding=0 width="100%" border=0>
  <TR>
        <TD noWrap class="Tabs">
            <uc1:Tabs runat="server" ID="Tabs" />
        </TD>
    </TR>
  <TR>
    <TD noWrap><uc1:MLDSpg1 id=MLDSpg1 runat="server" enableviewstate="False"></uc1:MLDSpg1><uc1:MLDSpg2 id=MLDSpg2 runat="server"></uc1:MLDSpg2></TD></TR></TABLE>

     </form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
	
  </body>
</html>
