﻿function uncheckAppraisalTypeCheckboxes() {
    document.getElementById("sApprFull").checked = false;
    document.getElementById("sApprDriveBy").checked = false;
    document.getElementById("sIsSpReviewNoAppr").checked = false;
}

function resetAppraisalTypeCheckboxes()
{
    var valuationMethod = document.getElementById("sSpValuationMethodT");
    if (valuationMethod == null) { return; }
    var selectedMethod = valuationMethod.options[valuationMethod.selectedIndex].value;

    // Previous Appraisal Used, Other
    var valuationMethodAllowsAppraisalTypeSelection = selectedMethod === "5" || selectedMethod === "8";

    if (ML.LoanVersionTCurrent < 12 || valuationMethodAllowsAppraisalTypeSelection) {
        document.getElementById("sApprFull").removeAttribute("disabled");
        document.getElementById("sApprDriveBy").removeAttribute("disabled");
        document.getElementById("sIsSpReviewNoAppr").removeAttribute("disabled");

        if (valuationMethodAllowsAppraisalTypeSelection) {
            uncheckAppraisalTypeCheckboxes();
        }
    }
    else {
        document.getElementById("sApprFull").disabled = true;
        document.getElementById("sApprDriveBy").disabled = true;
        document.getElementById("sIsSpReviewNoAppr").disabled = true;
    }
}

function onValuationMethodChanged() {
    if (ML.LoanVersionTCurrent < 12) {
        return;
    }

    resetAppraisalTypeCheckboxes();
    refreshCalculation();
}

function onAppraisalTypeChanged(checkbox) {
    if (ML.LoanVersionTCurrent < 12) {
        return;
    }

    var value = checkbox.checked;
    uncheckAppraisalTypeCheckboxes();
    checkbox.checked = value;
}

function onAppraisalFormChanged() {
    if (ML.LoanVersionTCurrent < 12) {
        return;
    }

    var appraisalForm = document.getElementById("sSpAppraisalFormT");
    var selectedForm = appraisalForm.options[appraisalForm.selectedIndex].text;

    var formNumber = document.getElementById("sSpReviewFormNum");
    refreshCalculation();

    if (selectedForm == "Other")
    {
        formNumber.value = "";
        formNumber.readOnly = false;
    }
    else
    {
        formNumber.readOnly = true;
    }
}
