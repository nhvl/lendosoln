﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="TaxForm_1098.aspx.cs" Inherits="LendersOfficeApp.newlos.Forms.TaxForm_1098" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="LendersOffice.Common" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
        .field-container .section{
            display: inline;
        }

        .section > div{
            display: table-row;
        }

        .section > div > div{
            display: table-cell;
        }

        .number-container, #FormDetailsTable label
        {
            font-weight: bold;
            text-align: left;
        }

        #sSpAddr{
            margin-bottom: 5px;
        }

        #sSpAddr, #sAssessorsParcelId
        {
            width: 275px;
        }

        #FormDetailsTable td
        {
            line-height:21px;
        }
        .lock-label{
            margin-left: 5px;
            display: inline-block;
        }
        #FormDetailsTable input[type="checkbox"]{
            vertical-align: sub;
        }
        .width180 {
            width: 180px;
        }

    </style>
    <script type="text/javascript">
        var fieldCounter = 1;

        $(document).ready(function () {
            _init();
            reportYearDdlChange();
        });

        function _init() {
            fieldCounter = 1;

            var year = $("#s1098ReportingYear").val();            
            var oldYears = year != "" && year !== null && year < 2016;
            var yearsBefore2017 = year != "" && year !== null && year < 2017;

            // remove this line of code when implementing form 2019
            $("#s1098ReportingYear option[value='2019']").prop("disabled", true);

            $("#sTax1098OriginationDate").toggle(!oldYears);
            $("#AddressIsSameContainer").toggle(!oldYears);
            $("#PropertyAddressContainer").toggle(!oldYears);
            $("#PropertyDescContainer").toggle(!oldYears);
            $("#NumPropertiesContainer").toggle(!oldYears);
            $("#OriginationDateContainer").toggle(!oldYears);
            $("#OutstandingPrincipalContainer").toggle(!oldYears);
            $("#OtherInfo2Container").toggle(year == 2014);
            $("#MipContainer").toggle(!(year == 2014 || year == 2012));
            $("#NumPropertiesContainer").toggle(!yearsBefore2017);

            if (!oldYears) {
                var labelStr = "Outstanding principal as of 1/1/";
                $("#OutstandingPrincipalLabel").text((year == "" || year === null) ? labelStr + new Date().getFullYear() : labelStr + year);
                $("#MipLabel").text("MI Premiums");
            }
            else {
                $("#MipLabel").text("MIP");
            }

            var isSameAddr = $("#sTax1098IsPropertyAddrBorrowerAddr").is(":checked");
            $(".borr-same-show").toggle(isSameAddr);
            $(".borr-same-hide").toggle(!isSameAddr);

            var is2018OrBeyond = year != '' && year !== null && year >= 2018;
            $('.hide-for-2018').toggle(!is2018OrBeyond);
            $('.show-for-2018').toggle(is2018OrBeyond);

            if (is2018OrBeyond) {
                $('#PropertyDescContainer').toggle(!isSameAddr);
            }

            $('#PropertyDescContainerNotApplicableLabel').toggle(isSameAddr && !is2018OrBeyond);
            
            // handle lock/disable logic
            $("#sTax1098IsPropertyAddrBorrowerAddr").prop('disabled', !$("#sTax1098IsPropertyAddrBorrowerAddrLckd").is(":checked"));
            var addressSelector = "#sSpAddr, #sSpCity, #sSpState, #sSpZip, #sAssessorsParcelId";
            $(addressSelector).prop("disabled", isSameAddr);

            lockField(document.getElementById("sTax1098OriginationDateLckd"), "sTax1098OriginationDate");

            lockField(document.getElementById("sTax1098PayerNameLckd"), "sTax1098PayerName");
            lockField(document.getElementById("sTax1098PayerAddrLckd"), "sTax1098PayerAddr");
            lockField(document.getElementById("sTax1098PayerAddrLckd"), "sTax1098PayerCity");

            disableDDL(document.getElementById("sTax1098PayerState"), !$("#sTax1098PayerAddrLckd").is(":checked"));

            lockField(document.getElementById("sTax1098PayerAddrLckd"), "sTax1098PayerZip");

            lockField(document.getElementById("sTax1098PayerSsnLckd"), "sTax1098PayerSsn");
            lockField(document.getElementById("sTax1098InterestLckd"), "sTax1098Interest");
            lockField(document.getElementById("sTax1098AccountNumLckd"), "sTax1098AccountNum");

            lockField(document.getElementById("sTax1098OutstandingPrincipalLckd"), "sTax1098OutstandingPrincipal");

            $("#sTax1098PointPaid").prop('readonly', !($("#s1098PointsCalcT").val() === "0"));
        }

        function reportYearDdlChange() {
            var year = $("#s1098ReportingYear").val();
            var oldYears = year != "" && year !== null && year < 2016;

            var $formDetailsTable = $("#FormDetailsTable");
            var rows = $formDetailsTable.find('tr').get();
            var sortYearAttr = 'sortnorm';
            if (year == 2013) {
                sortYearAttr = 'sort2013';
            }
            else if (!oldYears) {
                sortYearAttr = 'sort2016';
            }

            rows.sort(function (item1, item2) {
                var item1SortBy = parseInt($(item1).attr(sortYearAttr));
                var item2SortBy = parseInt($(item2).attr(sortYearAttr));

                if (item1SortBy === undefined || item2SortBy === undefined) {
                    return 0;
                }

                if (item1SortBy > item2SortBy) {
                    return 1;
                }
                if (item1SortBy < item2SortBy) {
                    return -1;
                }
                return 0;
            });

            $.each(rows, function (index, row) {
                $formDetailsTable.append(row);
            })

            // number columns to match according to PDF
            numberColumn(1);
        }

        function numberColumn(columnIndex) {
            $("td.number-container:nth-child(" + columnIndex + "):visible").each(function () {
                $(this).text(fieldCounter + ".");
                fieldCounter += 1;
            });
        }

        function downloadPdf() {
            var fileName;
            var year = $("#s1098ReportingYear").val();

            if (year === "") {
                alert("Please select a valid year.")
                return;
            }

            if (2009 < year && year != 2015) {
                fileName = "TaxForm_1098_" + year + ".aspx";
            }
            else {
                fileName = "TaxForm_1098.aspx";
            }

            var args = "applicationid=" + ML.appid;
            var url = gVirtualRoot + "/pdf/" + fileName + "?loanid=" + ML.sLId + "&printoptions=0&crack=" + new Date() + "&" + args;
            LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
        }
    </script>
</head>
<body bgcolor="gainsboro">
    <form id="TaxForm_1098" runat="server">
    <table class="FormTable" width="100%" cellSpacing="0" cellPadding="0" border="0">
        <tr>
            <td class="MainRightHeader" noWrap>1098 Mortgage Interest Statement</td>
        </tr>
        <tr><td style="padding-left: 10px" noWrap><table cellSpacing="0" cellPadding="0" width="770" border="0">
            <tr>
                <td class="LoanFormHeader" style="padding-left: 5px">I. Recipient/Lender</td>
            </tr>
            <tr><td><table class="InsetBorder" cellSpacing="0" cellPadding="0" width="98%" border="0">
                <tr><td noWrap><table cellSpacing="0" cellPadding="0" border="0">
					<tr>
					    <td colspan="3"><asp:CheckBox ID="sTax1098IsCorrected" Text="Is corrected" Font-Bold="true" runat="server"/></td>
					</tr>
					<tr>
						<td class=FieldLabel nowrap colspan="3"><UC:CFM ID="CFM" runat="server" /></td>
					</tr>
					<tr>
						<td class="FieldLabel" colspan="2" noWrap>Prepared By</td>
						<td noWrap><asp:textbox id="Tax1098CompanyName" runat="server" Width="258px" /></td>	
					</tr>
					<tr>
						<td class="FieldLabel" colspan="2" noWrap>Address</td>
						<td noWrap><asp:textbox id="Tax1098StreetAddr" runat="server" Width="254px" /></td>	
					</tr>
					<tr>
						<td class="FieldLabel" colspan="2" noWrap></td>
						<td class="FieldLabel" nowrap><asp:textbox id="Tax1098City" runat="server" />
						<ml:statedropdownlist id="Tax1098State" runat="server" />
						<ml:zipcodetextbox id="Tax1098Zip" runat="server" width="50" preset="zipcode" /></td>
					</tr>
					<tr>
						<td class="FieldLabel" colspan="2" noWrap>Phone</td>
						<td class="FieldLabel" nowrap colspan="5">
						<ml:phonetextbox id="Tax1098Phone" runat="server" width="110" preset="phone" /></td>
					</tr>
					<tr>
						<td class="FieldLabel" noWrap>Federal Tax ID</td>
						<td noWrap width="10">&nbsp;</td>
						<td noWrap><asp:textbox id="Tax1098TaxID" runat="server" width="110" /></td>	
					</tr>
				</table></td></tr>
            </table></td></tr>
            <tr>
                <td class="LoanFormHeader" style="padding-left: 5px">II. Payer/Borrower</td>
            </tr>
            <tr><td><table class="InsetBorder" cellSpacing="0" cellPadding="0" width="98%" border="0">
                <tr><td noWrap><table cellSpacing="0" cellPadding="0" border="0">
					<tr>
						<td class="FieldLabel" colspan="2" noWrap>Name</td>
						<td noWrap><asp:textbox id="sTax1098PayerName" runat="server" /></td>	
                        <td><asp:CheckBox runat="server" ID="sTax1098PayerNameLckd" onclick="refreshCalculation();"/></td>
                        <td class="FieldLabel">Lock</td>
					</tr>
					<tr>
						<td class="FieldLabel" noWrap>Address</td>
						<td noWrap width="10">&nbsp;</td>
						<td noWrap><asp:textbox id="sTax1098PayerAddr" runat="server" Width="254px" /></td>	
                        <td><asp:CheckBox runat="server" ID="sTax1098PayerAddrLckd" onclick="refreshCalculation();"/></td>
                        <td class="FieldLabel">Lock</td>
					</tr>
					<tr>
						<td class="FieldLabel" colspan="2" noWrap></td>
						<td class="FieldLabel" nowrap colspan="5"><asp:textbox id="sTax1098PayerCity" runat="server" />
						<ml:statedropdownlist id="sTax1098PayerState" runat="server" />
						<ml:zipcodetextbox id="sTax1098PayerZip" runat="server" width="50" preset="zipcode" /></td>
					</tr>
					<tr>
						<td class="FieldLabel" colspan="2" noWrap>SSN</td>
						<td noWrap><ml:ssntextbox id="sTax1098PayerSsn" runat="server" width="75px" preset="ssn" /></td>
                        <td><asp:CheckBox runat="server" ID="sTax1098PayerSsnLckd" onclick="refreshCalculation();"/></td>
                        <td class="FieldLabel">Lock</td>
					</tr>
				</table></td></tr>
            </table></td></tr>
            <tr>
                <td class="LoanFormHeader" style="padding-left: 5px">III. Form Details</td>
            </tr>
            <tr>
                <td>
                    <table runat="server" id="FormDetailsTable">
                        <tr sort2013="0" sort2016="0" sortnorm="0">
                            <td></td>
                            <td class="FieldLabel" width="">
                                Reporting Year
                            </td>
                            <td class="field-container">
                                <asp:DropDownList runat="server" id="s1098ReportingYear" onchange="refreshCalculation(); reportYearDdlChange(); "></asp:DropDownList>
                            </td>
                            <td>
                                <a onclick="downloadPdf();">Download PDF</a>
                            </td>
                        </tr>     
                        <tr runat="server" id="AccountNumContainer" sort2013="1" sort2016="1" sortnorm="1">
                            <td class=""></td>
                            <td class="label-container">
                                <label>Account Number</label>
                            </td>
                            <td class="field-container">
                                <asp:textbox runat="server" ID="sTax1098AccountNum" class="width180"></asp:textbox>
                                &nbsp;                                
                                <asp:CheckBox runat="server" ID="sTax1098AccountNumLckd" onclick="refreshCalculation();"/>
                                <label class="lock-label">Lock</label>
                            </td>
                        </tr>
                        <tr runat="server" id="InterestContainer" sort2013="2" sort2016="2" sortnorm="2">
                            <td class="number-container"></td>
                            <td class="label-container">
                                <label>Interest</label>
                            </td>
                            <td class="field-container">
                                <ml:MoneyTextBox runat="server" ID="sTax1098Interest"></ml:MoneyTextBox>
                                &nbsp;                                
                                <asp:CheckBox runat="server" ID="sTax1098InterestLckd" onclick="refreshCalculation();"/>
                                <label class="lock-label">Lock</label>
                            </td>
                        </tr>
                        <tr runat="server" id="OutstandingPrincipalContainer" sort2013="3" sort2016="3" sortnorm="3">
                            <td class="number-container"></td>
                            <td class="label-container">
                                <label runat="server" id="OutstandingPrincipalLabel">Outstanding principal as of</label>
                            </td>
                            <td class="field-container">
                                <ml:MoneyTextBox runat="server" ID="sTax1098OutstandingPrincipal"></ml:MoneyTextBox>
                                &nbsp;                                
                                <asp:CheckBox runat="server" ID="sTax1098OutstandingPrincipalLckd" onclick="refreshCalculation();"/>
                                <label class="lock-label">Lock</label>
                            </td>
                        </tr>
                        <tr runat="server" id="OriginationDateContainer" sort2013="4" sort2016="4" sortnorm="4">
                            <td class="number-container"></td>
                            <td class="label-container">
                                <label>Origination Date</label>
                            </td>
                            <td class="field-container">
                                <ml:DateTextBox runat="server" ID="sTax1098OriginationDate"></ml:DateTextBox>
                                <asp:CheckBox runat="server" ID="sTax1098OriginationDateLckd" onclick="refreshCalculation();"/>
                                <label class="lock-label">Lock</label>
                            </td>
                        </tr>
                        <tr runat="server" id="InterestRefundContainer" sort2013="7" sort2016="5" sortnorm="7">
                            <td class="number-container"></td>
                            <td class="label-container">
                                <label>Interest Refund</label>
                            </td>
                            <td class="field-container">
                                <ml:MoneyTextBox runat="server" ID="sTax1098InterestRefund"></ml:MoneyTextBox>
                            </td>
                        </tr>    
                        <tr runat="server" id="MipContainer" sort2013="8" sort2016="6" sortnorm="8">
                            <td class="number-container"></td>
                            <td class="label-container">
                                <label runat="server" id="MipLabel"></label>
                            </td>
                            <td class="field-container">
                                <ml:MoneyTextBox id="sTax1098MIP" runat="server" preset="money" />
                            </td>
                        </tr>    
                        <tr runat="server" id="PointsContainer" sort2013="5" sort2016="7" sortnorm="5">
                            <td class="number-container"></td>
                            <td class="label-container">
                                <label>Points Paid</label>
                            </td>
                            <td class="field-container">
                                <ml:MoneyTextBox runat="server" ID="sTax1098PointPaid"></ml:MoneyTextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="PointsCalculationContainer" sort2013="6" sort2016="8" sortnorm="6">
                            <td></td>
                            <td class="label-container">
                                <label>Points Calculation</label>
                            </td>
                            <td class="field-container">
                                <asp:DropDownList runat="server" ID="s1098PointsCalcT" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                        </tr>                                                                                                                                                                                                                
                        <tr runat="server" id="AddressIsSameContainer" sort2013="9" sort2016="9" sortnorm="9">
                            <td class="number-container"></td>
                            <td colspan="2" class="property-address-same-label label-container">
                                <label>Address of property same as Payer's?</label>
                                <asp:Checkbox runat="server" ID="sTax1098IsPropertyAddrBorrowerAddr" onclick="refreshCalculation();"></asp:Checkbox>                                
                                <asp:CheckBox runat="server" ID="sTax1098IsPropertyAddrBorrowerAddrLckd" onclick="refreshCalculation();"/>
                                <label class="lock-label">Lock</label>
                            </td>
                        </tr>
                        <tr runat="server" id="PropertyAddressContainer" sort2013="10" sort2016="10" sortnorm="10">
                            <td class="number-container"></td>
                            <td class="label-container">
                                <label class="hide-for-2018">Property Address</label>
                                <label class="show-for-2018">Address or Description of Property</label>
                            </td>
                            <td colspan="4" class="field-container borr-same-hide">
                                <asp:textbox runat="server" ID="sSpAddr"></asp:textbox>
                                <br />
                                <asp:textbox runat="server" ID="sSpCity"></asp:textbox>
                                <ml:StateDropDownList runat="server" ID="sSpState" />
                                <ml:ZipcodeTextBox runat="server" ID="sSpZip"></ml:ZipcodeTextBox>
                            </td>
                            <td class="borr-same-show">
                                Not applicable.
                            </td>
                        </tr>
                        <tr runat="server" id="PropertyDescContainer" sort2013="11" sort2016="11" sortnorm="11">
                            <td class="number-container hide-for-2018"></td>
                            <td class="label-container hide-for-2018">
                                <label>Property Description</label>
                            </td>
                            <td colspan="2" class="show-for-2018">&nbsp;</td>
                            <td colspan="4" class="field-container borr-same-hide" >
                                <asp:textbox TextMode="MultiLine" Rows="4" runat="server" ID="sAssessorsParcelId"></asp:textbox>
                            </td>
                            <td id="PropertyDescContainerNotApplicableLabel">
                                Not applicable.
                            </td>
                        </tr>
                        <tr runat="server" id="NumPropertiesContainer" sort2013="12" sort2016="12" sortnorm="12">
                            <td class="number-container"></td>
                            <td class="label-container">
                                <label>Num of Properties</label>
                            </td>
                            <td class="field-container">
                                <asp:textbox runat="server" ID="sTax1098NumberOfPropertiesSecuringThisMortgage" preset="numeric"></asp:textbox>
                            </td>
                        </tr>                        
                        <tr runat="server" id="OtherInfo2Container" sort2013="13" sort2016="13" sortnorm="13">
                            <td class="number-container"></td>
                            <td class="label-container">
                                <label>Other Info</label>
                            </td>
                            <td class="field-container">
                                <asp:textbox runat="server" ID="sTax1098OtherInfo2"></asp:textbox>
                            </td>
                        </tr>
                        <tr runat="server" id="OtherInfo1Container" sort2013="14" sort2016="14" sortnorm="14">
                            <td class="number-container"></td>
                            <td class="label-container">
                                <label>Other Info</label>
                            </td>
                            <td class="field-container">
                                <asp:textbox runat="server" ID="sTax1098OtherInfo"></asp:textbox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table></td></tr>
    </table>
    <uc1:cmodaldlg id="CModalDlg1" runat="server"/>
    </form>
</body>
</html>
