using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Forms
{
	public partial  class Loan1003pg2 : BaseLoanUserControl, IAutoLoadUserControl
	{
        #region Protected member variables
        #endregion
        protected string brokerID = "";
        private void BindOtherIncomeDescription(MeridianLink.CommonControls.ComboBox cb) 
        {
            LendersOffice.Admin.BrokerDB brokerDB = this.BrokerDB;
            brokerID = BrokerUser.BrokerId.ToString();
            Tools.Bind_aOIDescT(cb);
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
		}

        protected void PageInit(object sender, System.EventArgs e) 
        {
            BindOtherIncomeDescription(OtherIncomeCombo);
            Tools.Bind_sProRealETxT(sProRealETxT);
            Tools.Bind_PercentBaseT(sProHazInsT);

        }
        public void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Loan1003pg2));
            dataLoan.InitLoad();

            bool heMigrated = dataLoan.sIsHousingExpenseMigrated;

            var incomeFields = new[]
            {
                aBBaseI,
                aBOvertimeI,
                aBBonusesI,
                aBCommisionI,
                aBDividendI,
                aCBaseI,
                aCOvertimeI,
                aCBonusesI,
                aCCommisionI,
                aCDividendI,
            };

            foreach (var incomeField in incomeFields)
            {
                incomeField.ReadOnly = dataLoan.sIsIncomeCollectionEnabled;
            }

            if(dataLoan.sIsIncomeCollectionEnabled)
            {
                (this.Page as BasePage).Form.Attributes.Add("class", "is-income-collection-enabled");
            }

            AddMoreOtherInc.Visible = !dataLoan.sIsIncomeCollectionEnabled;

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            aBNm.Value = dataApp.aBNm;
            aCNm.Value = dataApp.aCNm;
            aBBaseI.Text = dataApp.aBBaseI_rep;
            aCBaseI.Text = dataApp.aCBaseI_rep;
            aTotBaseI.Text = dataApp.aTotBaseI_rep;
            aBOvertimeI.Text = dataApp.aBOvertimeI_rep;
            aCOvertimeI.Text = dataApp.aCOvertimeI_rep;
            aTotOvertimeI.Text = dataApp.aTotOvertimeI_rep;
            aBBonusesI.Text = dataApp.aBBonusesI_rep;
            aCBonusesI.Text = dataApp.aCBonusesI_rep;
            aTotBonusesI.Text = dataApp.aTotBonusesI_rep;
            aBCommisionI.Text = dataApp.aBCommisionI_rep;
            aCCommisionI.Text = dataApp.aCCommisionI_rep;
            aTotCommisionI.Text = dataApp.aTotCommisionI_rep;
            aBDividendI.Text = dataApp.aBDividendI_rep;
            aCDividendI.Text = dataApp.aCDividendI_rep;
            aTotDividendI.Text = dataApp.aTotDividendI_rep;
            aBNetRentI1003.Text = dataApp.aBNetRentI1003_rep;
            aCNetRentI1003.Text = dataApp.aCNetRentI1003_rep;
			aNetRentI1003Lckd.Checked = dataApp.aNetRentI1003Lckd;
            aTotNetRentI1003.Text = dataApp.aTotNetRentI1003_rep;

            // Set from the loan file XML.
            OtherIncomeJson.Value = ObsoleteSerializationHelper.JsonSerialize( dataApp.aOtherIncomeList);

            aBSpPosCf.Text = dataApp.aBSpPosCf_rep;
            aCSpPosCf.Text = dataApp.aCSpPosCf_rep;
            aTotSpPosCf.Text = dataApp.aTotSpPosCf_rep;

            aBTotOI.Text = dataApp.aBTotOI_rep;
            aCTotOI.Text = dataApp.aCTotOI_rep;
            aTotOI.Text = dataApp.aTotOI_rep;

            aBTotI.Text = dataApp.aBTotI_rep;
            aCTotI.Text = dataApp.aCTotI_rep;
            aTotI.Text = dataApp.aTotI_rep;
            aPresRent.Text = dataApp.aPresRent_rep;
            aPres1stM.Text = dataApp.aPres1stM_rep;
            aPresOFin.Text = dataApp.aPresOFin_rep;
            aPresHazIns.Text = dataApp.aPresHazIns_rep;
            aPresRealETx.Text = dataApp.aPresRealETx_rep;
            aPresMIns.Text = dataApp.aPresMIns_rep;
            aPresHoAssocDues.Text = dataApp.aPresHoAssocDues_rep;
            aPresOHExp.Text = dataApp.aPresOHExp_rep;
            aPresTotHExp.Text = dataApp.aPresTotHExp_rep;

            aLiaBalTot.Text = dataApp.aLiaBalTot_rep;
            aLiaMonTot.Text = dataApp.aLiaMonTot_rep;
            aAsstLiaCompletedNotJointly.SelectedIndex = dataApp.aAsstLiaCompletedNotJointly ? 1 : 0;
            aAsstLiqTot.Text		= dataApp.aAsstLiqTot_rep;
            aReTotVal.Text			= dataApp.aReTotVal_rep;
            aAsstNonReSolidTot.Text	= dataApp.aAsstNonReSolidTot_rep;
            aAsstValTot.Text		= dataApp.aAsstValTot_rep;
            aNetWorth.Text			= dataApp.aNetWorth_rep;

            bool isHazardCalcVisible = !dataLoan.sHazardExpenseInDisbursementMode;
            sProFirstMPmt.Text = dataLoan.sProFirstMPmt_rep;
            sProHazIns.Text = dataLoan.sProHazIns_rep;
            sProHazInsMb.Text = dataLoan.sProHazInsMb_rep;
            sProHazInsMbHolder.Visible = isHazardCalcVisible;
            sProHazInsR.Text = dataLoan.sProHazInsR_rep;
            sProHazInsRHolder.Visible = isHazardCalcVisible;
            Tools.SetDropDownListValue(sProHazInsT, dataLoan.sProHazInsT);
            sProHazInsTHolder.Visible = isHazardCalcVisible;
            sProHazInsBaseAmt.Text = dataLoan.sProHazInsBaseAmt_rep;
            sProHazInsBaseAmtHolder.Visible = isHazardCalcVisible;

            bool isRealEstateCalcVisible = !dataLoan.sRealEstateTaxExpenseInDisbMode;
            sProRealETx.Text = dataLoan.sProRealETx_rep;
			sProRealETxMb.Text = dataLoan.sProRealETxMb_rep;
            sProRealETxMbHolder.Visible = isRealEstateCalcVisible;
			sProRealETxR.Text = dataLoan.sProRealETxR_rep;
            sProRealETxRHolder.Visible = isRealEstateCalcVisible;
			Tools.SetDropDownListValue(sProRealETxT, dataLoan.sProRealETxT);
            sProRealETxTHolder.Visible = isRealEstateCalcVisible;
			sProRealETxBaseAmt.Text = dataLoan.sProRealETxBaseAmt_rep;
            sProRealETxBaseAmtPlaceHolder.Visible = isRealEstateCalcVisible;
            
            sProMIns.Text = dataLoan.sProMIns_rep;
            
            sProHoAssocDues.Text = dataLoan.sProHoAssocDues_rep;
            sProHoAssocDues.ReadOnly = (heMigrated && dataLoan.sHOADuesExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements);
            sProOHExp.Text = dataLoan.sProOHExp_rep;
            sProOHExpLckd.Checked = dataLoan.sProOHExpLckd;
            sProSecondMPmt.Text = dataLoan.sProSecondMPmt_rep;
            sMonthlyPmt.Text = dataLoan.sMonthlyPmt_rep;
            sProOHExpDesc.Text = dataLoan.sProOHExpDesc;

            if (dataLoan.BrokerDB.IsEnableBigLoanPage)
            {
                subfinancingLink.NavigateUrl = dataLoan.sLienPosT == E_sLienPosT.First ? "javascript:linkMe('../BigLoanInfo.aspx');" : "javascript:linkMe('../LoanInfo.aspx?pg=2');";
            }
            else
            {
                // 07/24/07 av OPM 7583
                subfinancingLink.NavigateUrl = string.Format("javascript:linkMe('../LoanInfo.aspx?pg={0}');", dataLoan.sLienPosT == E_sLienPosT.First ? "2" : "0");
            }
			

        }

        public void SaveData() 
        {
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion


	}
}
