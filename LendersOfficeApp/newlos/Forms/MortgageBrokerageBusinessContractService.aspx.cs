using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
    public class MortgageBrokerageBusinessContractServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(MortgageBrokerageBusinessContractServiceItem));
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataLoan.sFloridaContractDays_rep = GetString("sFloridaContractDays");
            dataLoan.sFloridaBorrEstimateSpVal_rep = GetString("sFloridaBorrEstimateSpVal");
            dataLoan.sFloridaBorrEstimateExistingMBal_rep = GetString("sFloridaBorrEstimateExistingMBal");
            dataLoan.sFloridaBorrDeposit_rep = GetString("sFloridaBorrDeposit");
            dataLoan.sFloridaAdditionalCompMinR_rep = GetString("sFloridaAdditionalCompMinR");
            dataLoan.sFloridaAdditionalCompMinMb_rep = GetString("sFloridaAdditionalCompMinMb");
            dataLoan.sFloridaAdditionalCompMaxR_rep = GetString("sFloridaAdditionalCompMaxR");
            dataLoan.sFloridaAdditionalCompMaxMb_rep = GetString("sFloridaAdditionalCompMaxMb");
            dataLoan.sFloridaBrokerFeeR_rep = GetString("sFloridaBrokerFeeR");
            dataLoan.sFloridaBrokerFeeMb_rep = GetString("sFloridaBrokerFeeMb");
            dataLoan.sFloridaAppFee_rep = GetString("sFloridaAppFee");

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.FloridaMortBrokerBusinessContract, E_ReturnOptionIfNotExist.CreateNew);

            preparer.PreparerName = GetString("PreparerName");
            preparer.CompanyName = GetString("CompanyName");
            preparer.LicenseNumOfCompany = GetString("LicenseNumOfCompany") ;

            preparer.Update();

            dataLoan.sFloridaIsAppFeeApplicableToCc = GetBool("sFloridaIsAppFeeApplicableToCc");
            dataLoan.sFloridaIsAppFeeRefundable = GetString("sFloridaIsAppFeeRefundable") == "0";

        }
    }
	public partial class MortgageBrokerageBusinessContractService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new MortgageBrokerageBusinessContractServiceItem());
        }

	}
}
