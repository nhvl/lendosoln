﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using System.Collections;
using LendersOfficeApp.newlos.Status;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class CAMLDSRE885pg2 : BaseLoanUserControl, IAutoLoadUserControl
    {

        #region member variables
        protected bool sUseObsoleteGfeForm;
        #endregion

        protected void PageInit(object sender, System.EventArgs e)
        {
            Tools.Bind_sFinMethT(sFinMethT);

        }
        private void BindDataObject(CPageData dataLoan)
        {

            sTerm.Text = dataLoan.sTerm_rep;
            sTerm.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
            sFinMethT.Enabled = !(dataLoan.sIsRateLocked || IsReadOnly);
            sMldsPpmtMonMax.Text = dataLoan.sMldsPpmtMonMax_rep;
            Tools.SetDropDownListValue(sMldsPpmtT, dataLoan.sMldsPpmtT);
            Tools.SetDropDownListValue(sMldsPpmtBaseT, dataLoan.sMldsPpmtBaseT);

            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sDisabilityIns.Text = dataLoan.sDisabilityIns_rep;
            sTotDeductFromFinalLAmt.Text = dataLoan.sTotDeductFromFinalLAmt_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sFinalBalloonPmt.Text = dataLoan.sFinalBalloonPmt_rep;
            sProThisMPmt.Text = dataLoan.sProThisMPmt_rep;
            sFinalBalloonPmtDueD.Text = dataLoan.sFinalBalloonPmtDueD_rep;

            sUseObsoleteGfeForm = dataLoan.sUseObsoleteGfeForm;

            sMldsImpoundIncludeOther.Checked = dataLoan.sMldsImpoundIncludeOther;
            sMldsImpoundIncludeFloodIns.Checked = dataLoan.sMldsImpoundIncludeFloodIns;
            sMldsImpoundIncludeMIns.Checked = dataLoan.sMldsImpoundIncludeMIns;
            sMldsImpoundIncludeRealETx.Checked = dataLoan.sMldsImpoundIncludeRealETx;
            sMldsImpoundIncludeHazIns.Checked = dataLoan.sMldsImpoundIncludeHazIns;

            Tools.SetDropDownListValue(sMldsHasImpound, dataLoan.sMldsHasImpound ? "1" : "0");

            sMldsPpmtPeriod.Text = dataLoan.sMldsPpmtPeriod_rep;
            sMldsPpmtOtherDetail.Text = dataLoan.sMldsPpmtOtherDetail;
            sMldsPpmtMaxAmt.Text = dataLoan.sMldsPpmtMaxAmt_rep;

            sTotEstScMlds.Text = dataLoan.sTotEstScMlds_rep;
            sMldsImpoundOtherDesc.Text = dataLoan.sMldsImpoundOtherDesc;

            sMldsRe885PmtOptionEndMons.Text = dataLoan.sMldsRe885PmtOptionEndMons_rep;
            sMldsRe885PmtOptionEndOrigBalPc.Text = dataLoan.sMldsRe885PmtOptionEndOrigBalPc_rep;
            sMldsRe885OptionArmMinPayPeriod.Text = dataLoan.sMldsRe885OptionArmMinPayPeriod_rep;
            sMldsRe885MaxMonthlyPmt.Text = dataLoan.sMldsRe885MaxMonthlyPmt_rep;
            sMldsRe885MaxMonthlyPmtMons.Text = dataLoan.sMldsRe885MaxMonthlyPmtMons_rep;
            sMldsRe885NegAmortLoanBalance.Text = dataLoan.sMldsRe885NegAmortLoanBalance_rep;
            sMldsRe885InitialAdjRPeriod.Text = dataLoan.sMldsRe885InitialAdjRPeriod_rep;
            sMldsRe885FullyIndexR.Text = dataLoan.sMldsRe885FullyIndexR_rep;
            sMldsRe885MaxLifeCapRAdjustable.Text = dataLoan.sMldsRe885MaxLifeCapRAdjustable_rep;
            sTotEstFntcCamlds.Text = dataLoan.sTotEstFntcCamlds_rep;
            sPurchPrice_sRefPdOffAmtCamlds.Text = dataLoan.sPurchPrice_sRefPdOffAmtCamlds_rep;
            sBalloonPmt.Checked = dataLoan.sBalloonPmt;
            sBalloonPmt.Enabled = dataLoan.sGfeIsBalloonLckd;
            sRAdjCapRAdjustableMlds.Text = dataLoan.sRAdjCapRAdjustableMlds_rep;
            sRAdjCapMonAdjustableMlds.Text = dataLoan.sRAdjCapMonAdjustableMlds_rep;

            sOtherPmtObligationDescMlds.Text = dataLoan.sOtherPmtObligationDescMlds;
            sOtherPmtObligationMlds.Text = dataLoan.sOtherPmtObligationMlds_rep;
            sMldsMonthlyImpoundPmtPerYear_NoImpound.Text = dataLoan.sMldsMonthlyImpoundPmtPerYear_NoImpound;
            sMldsMonthlyImpoundPmt_Impound.Text = dataLoan.sMldsMonthlyImpoundPmt_Impound;


        }
        public void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(CAMLDSRE885pg2));
            dataLoan.InitLoad();

            BindDataObject(dataLoan);

        }
        public void SaveData()
        {
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {

            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
        }

    }

}