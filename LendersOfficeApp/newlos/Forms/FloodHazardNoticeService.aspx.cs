using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{

	public class FloodHazardNoticeServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
	{
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FloodHazardNoticeServiceItem));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.FloodHazardNotice, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("FloodHazardNoticeCompanyName");
            preparer.StreetAddr = GetString("FloodHazardNoticeStreetAddr");
            preparer.City = GetString("FloodHazardNoticeCity");
            preparer.State = GetString("FloodHazardNoticeState");
            preparer.Zip = GetString("FloodHazardNoticeZip");
            preparer.Update();

            dataLoan.sFloodHazardBuilding = GetBool("sFloodHazardBuilding");
            dataLoan.sFloodHazardMobileHome = GetBool("sFloodHazardMobileHome");
            dataLoan.sFloodHazardCommunityDesc = GetString("sFloodHazardCommunityDesc");

            dataLoan.sFloodCertId = GetString("sFloodCertId");

            dataLoan.sFloodHazardFedInsAvail = GetBool("sFloodHazardFedInsAvail");
            dataLoan.sFloodHazardFedInsNotAvail = GetBool("sFloodHazardFedInsNotAvail");
            
        }
	}
    public partial class FloodHazardNoticeService : LendersOffice.Common.BaseSimpleServiceXmlPage 
    {
        protected override void Initialize() 
        {
            AddBackgroundItem("", new FloodHazardNoticeServiceItem());
        }
    }
}
