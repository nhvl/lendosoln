<%@ Page language="c#" Codebehind="CreditDenial.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.CreditDenial" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="DataAccess" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>CreditDenial</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    
	 <style type="text/css">    
       
        ul.tab        
        {
           margin-left: 0;
           padding-left: 0;
           white-space: nowrap;
           margin:5px 0 5px 0;
           font-family:verdana; 
           font-size:8pt;
        }
        
        ul.tab li
        {
            display : inline;
           list-style-type: none;
        }
        
        
        li.tab 
        {           
          background-color:gray;                   
          color:#ffffff;
          height:21px;
          text-align:center;
          padding-left:5px;
          border-top:outset 1px;
          border-left:outset 2px;
          border-right:outset 2px;
          border-bottom:outset 2px;
          padding-right:5px;
          padding-top:2px;
          font-weight:bold;
          cursor:default;         
        }
        
        li.tabseparator
        {  
            border-bottom:outset 2px;            
            width:10px;
            height:21px;            
        }
        li.tab_hoover
        {
          background-color:gainsboro;
          color:#003366;
        }
        li.tab_selected
        {
          background-color:gainsboro;
          color:#003366;
          border-bottom:outset 0px gainsboro;        
        }
        
        .WarningLabelStyle { BORDER-RIGHT: black 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: black 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: bold; PADDING-BOTTOM: 5px; MARGIN: 5px; BORDER-LEFT: black 1px solid; COLOR: red; PADDING-TOP: 5px; BORDER-BOTTOM: black 1px solid; BACKGROUND-COLOR: white }
    </style>    
</HEAD>
  <body MS_POSITIONING="FlowLayout" class="RightBackground">
	<script type="text/javascript">
    function _init() {
      f_displayWarning('RejectWarningLabel', false);
    }

    function f_displayWarning(id, bVisible) {
      document.getElementById(id).style.display = bVisible ? "" : "none";
    }

    // Used to parse URL parameters
    // http://james.padolsey.com/javascript/bujs-1-getparameterbyname/
    function f_getParameterByName(name) {
        var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
        return match ? decodeURIComponent(match[1].replace(/\+/g, ' ')) : null;
    }
    
    function f_shouldDisplayBorrower() {
        // Get the borrtype parameter from URL
      var borrtype = f_getParameterByName("borrtype");
      if ( borrtype == null || borrtype.toUpperCase() == "B")
        return true;
      else
        return false;
    }
    
    var oPreviousSelectedTab = null;
    function _init() {
      $("#tabBorrower,#tabCoborrower").mouseover(f_onTabFocusIn).mouseout(f_onTabFocusOut).click(f_onTabClick);
      // Look in the url to see whether supposed to display info for borrower or coborrower        
      if (f_shouldDisplayBorrower()) {
        oPreviousSelectedTab = $("#tabBorrower");
        $("#tabBorrower").click();          
      }
      else {
        oPreviousSelectedTab = $("#tabCoborrower");
        $("#tabCoborrower").click();
      }

      oPreviousSelectedTab.addClass('tab_selected');
      toggleDenialCreditFields(!($(<%= AspxTools.JsGetElementById(aDenialCreditScoreLckd)%>).prop('checked')));
    }
    
    function f_onTabFocusIn(evt)
    {
      var target = $(evt.target);
      if (target.hasClass('tab_selected') == false)
      {
        target.addClass('tab_hoover');
      }
    }
    function f_onTabFocusOut(evt)
    {
      var target = $(evt.target);
      if (target.hasClass('tab_selected') == false)
      {
        target.removeClass('tab_hoover');
      }    
    }
    function f_onTabClick(evt) {
        var target = $(evt.target);      
      
      
      if (target.hasClass('tab_selected') == false) {
          
        var bIsDirty = isDirty();
        var method = 'LoadTab';
        PolyShouldShowConfirmSave(isDirty(), function(){
          if (null != oPreviousSelectedTab)
          {
            //alert(oPreviousSelectedTab.attr('id') + '-' + oPreviousSelectedTab.hasClass('tab_selected'));
            oPreviousSelectedTab.removeClass('tab_selected').removeClass('tab_hoover');
          }
          target.addClass('tab_selected');
          oPreviousSelectedTab = target;
          
          var selectedId = target.attr('id');

          document.getElementById('CopyFromBorrBtn').style.display = selectedId === 'tabBorrower' ? 'none' : 'inline';
        
          var args = getAllFormValues();
          args.IsBorrower = selectedId === 'tabBorrower' ? 'True' : 'False';

          var result = gService.loanedit.call(method, args);
          
          if (!result.error) {
            clearDirty();
            populateForm(result.value, null);

            toggleDenialCreditFields(!($(<%= AspxTools.JsGetElementById(aDenialCreditScoreLckd)%>).prop('checked')));

            return true;
          } else {
            updateDirtyBit();
            if (result.ErrorType === 'VersionMismatchException')
            {
              f_displayVersionMismatch();
            }
            else if (result.ErrorType === 'LoanFieldWritePermissionDenied') 
            {
                f_displayFieldWriteDenied(result.UserMessage);
            }
            else
            {
              var errMsg = result.UserMessage || 'Unable to save data. Please try again.';
              alert(errMsg);
            }
            return false;
          }
        }, function(){ method =  'SaveAndLoadTab';});
      }    
    }

    function toggleDenialCreditFields(setReadonly)
    {
        $(<%= AspxTools.JsGetElementById(aDenialCreditScore)%>).prop('readonly', setReadonly);
        $(<%= AspxTools.JsGetElementById(aDenialCreditScoreD)%>).prop('readonly', setReadonly);
        $(<%= AspxTools.JsGetElementById(aDenialCreditScoreRangeFrom)%>).prop('readonly', setReadonly);
        $(<%= AspxTools.JsGetElementById(aDenialCreditScoreRangeTo)%>).prop('readonly', setReadonly);
        $(<%= AspxTools.JsGetElementById(aDenialCreditScoreFactor1)%>).prop('readonly', setReadonly);
        $(<%= AspxTools.JsGetElementById(aDenialCreditScoreFactor2)%>).prop('readonly', setReadonly);
        $(<%= AspxTools.JsGetElementById(aDenialCreditScoreFactor3)%>).prop('readonly', setReadonly);
        $(<%= AspxTools.JsGetElementById(aDenialCreditScoreFactor4)%>).prop('readonly', setReadonly);

        $(<%= AspxTools.JsGetElementById(ScoreContactName)%>).prop('readonly', setReadonly);
        $(<%= AspxTools.JsGetElementById(ScoreContactAddress)%>).prop('readonly', setReadonly);
        $(<%= AspxTools.JsGetElementById(ScoreContactCity)%>).prop('readonly', setReadonly);
        $(<%= AspxTools.JsGetElementById(ScoreContactState)%>).prop('disabled', setReadonly);
        $(<%= AspxTools.JsGetElementById(ScoreContactZip)%>).prop('readonly', setReadonly);
        $(<%= AspxTools.JsGetElementById(ScoreContactPhone)%>).prop('readonly', setReadonly);

        setDisabledAttr($('#ScoreContact_m_linkPickFromContact'), setReadonly);
        setDisabledAttr($('#ScoreContact_m_linkAddToContact'), setReadonly);
    }

    function f_onSetManuallyClick() {
        var selectedId = oPreviousSelectedTab.attr('id');
        var args = getAllFormValues();
        args.IsBorrower = selectedId === 'tabBorrower' ? 'True' : 'False';

        var result = gService.loanedit.call('BindAndLoadTab', args);

        if (!result.error) {
            clearDirty();
            populateForm(result.value, null);

            toggleDenialCreditFields(!($(<%= AspxTools.JsGetElementById(aDenialCreditScoreLckd)%>).prop('checked')));

            return true;
        } else {
            updateDirtyBit();
            if (result.ErrorType === 'VersionMismatchException') {
                f_displayVersionMismatch();
            }
            else if (result.ErrorType === 'LoanFieldWritePermissionDenied') {
                f_displayFieldWriteDenied(result.UserMessage);
            }
            else {
                var errMsg = result.UserMessage || 'Unable to save data. Please try again.';
                alert(errMsg);
            }
            return false;
        }
    }
    
    function saveMe() {
        var selectedId = oPreviousSelectedTab.attr('id');
        var args = getAllFormValues();
        args.IsBorrower = selectedId === 'tabBorrower' ? 'True' : 'False';
        var result = gService.loanedit.call('SaveTab', args);
        if (!result.error) {
          clearDirty();
          populateForm(result.value, null);
          return true;
        } else {
          updateDirtyBit();
          if (result.ErrorType === 'VersionMismatchException')
          {
            f_displayVersionMismatch();
          }
          if (result.ErrorType === 'LoanFieldWritePermissionDenied') {
              f_displayFieldWriteDenied(result.UserMessage);
          }
          else
          {
            var errMsg = result.UserMessage || 'Unable to save data. Please try again.';
            alert(errMsg);
          }
          return false;
        }
      
    }

    function copyFromBorrower() {
        var args = getAllFormValues();
        var result = gService.loanedit.call('CopyBorrowerDenialReasons', args);

        if (!result.error) {
            updateDirtyBit();
            populateForm(result.value, null);
            return true;
        }
        else {
            alert('Failed to copy denial reasons.');
        }
    }
</script>

<form id="CreditDenial" method="post" runat="server">
<TABLE class=FormTable id=Table1 cellSpacing=0 cellPadding=0 width="100%" border=0>
  <TR>
    <TD noWrap class=MainRightHeader>Statement of Credit Denial, Termination, or Change</TD></TR>
  <TR>
    <TD class=WarningLabelStyle id=RejectWarningLabel>WARNING: &nbsp;Modifying the denied date 
                  could remove your permission to edit this loan. &nbsp;Please make sure you complete the HMDA form before you enter a denied date here.</TD></TR>
  
   <tr>
      <td nowrap="nowrap">
      <ul class="tab">
      <li class="tabseparator" id="Span1"></li>
      <li class="tab" id="tabBorrower">Borrower</li>
      <li class="tabseparator" id="Span2"></li>
      <li class="tab" id="tabCoborrower">Co-Borrower</li>
      <li class="tabseparator" id="Span3" style="width:100%"></li>
      </ul>
      </td>
    </tr>    
  <TR>
    <TD noWrap>
      Credit Report Date <ml:DateTextBox id=aCrRd runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      Mailed On <ml:DateTextBox id=sHmdaDeniedFormDoneD runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      Date Denied <ml:DateTextBox id=sRejectD runat="server" CssClass="mask" preset="date" width="75" onfocus="f_displayWarning('RejectWarningLabel', true);" onblur="f_displayWarning('RejectWarningLabel', false);"></ml:DateTextBox></TD></TR>
  <TR>
    <TD noWrap>
      <TABLE id=Table2 cellSpacing=0 cellPadding=0 width="100%" border=0 class=GroupTable>
        <TR>
          <TD colSpan=2 class=FormTableSubHeader>1. Your application denial was based&nbsp;on the 
            following reason(s)</TD></TR>
        <tr>
            <td>
                <input type="button" id="CopyFromBorrBtn" onclick="copyFromBorrower();" value="Copy denial reasons from borrower" />
            </td>
        </tr>
        <TR>
          <TD class=FieldLabel>A - CREDIT</TD>
          <TD class=FieldLabel>D - RESIDENCY</TD></TR>
        <TR>
          <TD><asp:CheckBox id=aDenialNoCreditFile runat="server" Text="No Credit File"></asp:CheckBox></TD>
          <TD><asp:CheckBox id=aDenialTempResidence runat="server" Text="Temporary Residence"></asp:CheckBox></TD></TR>
        <TR>
          <TD><asp:CheckBox id=aDenialInsufficientCreditRef runat="server" Text="Insufficient Credit Reference"></asp:CheckBox></TD>
          <TD><asp:CheckBox id=aDenialShortResidencePeriod runat="server" Text="Too Short a Period of Residence"></asp:CheckBox></TD></TR>
        <TR>
          <TD><asp:CheckBox id=aDenialInsufficientCreditFile runat="server" Text="Insufficient Credit File"></asp:CheckBox></TD>
          <TD><asp:CheckBox id=aDenialUnableVerifyResidence runat="server" Text="Unable to Verify Residence"></asp:CheckBox></TD></TR>
        <TR>
          <TD><asp:CheckBox id=aDenialUnableVerifyCreditRef runat="server" Text="Unable to Verify Credit References"></asp:CheckBox></TD>
          <TD class=FieldLabel>E - INS, GUARANTY OR PURCH DENIED BY:</TD></TR>
        <TR>
          <TD><asp:CheckBox id=aDenialGarnishment runat="server" Text="Garnishment, Attachment, Foreclosure, Repossession or Suit"></asp:CheckBox></TD>
          <TD><asp:CheckBox id=aDenialByHUD runat="server" Text="Department of Housing and Urban Development"></asp:CheckBox></TD></TR>
        <TR>
          <TD><asp:CheckBox id=aDenialExcessiveObligations runat="server" Text="Excessive Obligations"></asp:CheckBox></TD>
          <TD><asp:CheckBox id=aDenialByVA runat="server" Text="Department of Veterans Affairs"></asp:CheckBox></TD></TR>
        <TR>
          <TD>&nbsp;&nbsp;&nbsp;&nbsp; <asp:CheckBox id=aDenialInsufficientIncome runat="server" Text="Insufficient Income for Total Obligations"></asp:CheckBox></TD>
          <TD><asp:CheckBox id=aDenialByFedNationalMortAssoc runat="server" Text="Federal National Mortgage Association"></asp:CheckBox></TD></TR>
        <TR>
          <TD>&nbsp;&nbsp;&nbsp;&nbsp; <asp:CheckBox id=aDenialUnacceptablePmtRecord runat="server" Text="Unacceptable Payment Record on Previous Mortgage"></asp:CheckBox></TD>
          <TD><asp:CheckBox id=aDenialByFedHomeLoanMortCorp runat="server" Text="Federal Home Loan Mortgage Corporation"></asp:CheckBox></TD></TR>
        <TR>
          <TD>&nbsp;&nbsp;&nbsp;&nbsp; <asp:CheckBox id=aDenialLackOfCashReserves runat="server" Text="Lack of Cash Reserves"></asp:CheckBox></TD>
          <TD><asp:CheckBox id=aDenialByOther runat="server" Text="Other - "></asp:CheckBox><asp:TextBox id=aDenialByOtherDesc style="width:325px" runat="server"></asp:TextBox></TD></TR>
        <TR>
          <TD><asp:CheckBox id=aDenialDeliquentCreditObligations runat="server" Text="Delinquent Credit Obligations"></asp:CheckBox></TD>
          <TD class=FieldLabel>F - OTHER</TD></TR>
        <TR>
          <TD><asp:CheckBox id=aDenialBankruptcy runat="server" Text="Bankruptcy"></asp:CheckBox></TD>
          <TD><asp:CheckBox id=aDenialInsufficientFundsToClose runat="server" Text="Insufficient Funds to Close the Loan"></asp:CheckBox></TD></TR>
        <TR>
          <TD><asp:CheckBox id=aDenialInfoFromConsumerReportAgency runat="server" Text="Information From a Consumer Reporting Agency"></asp:CheckBox></TD>
          <TD><asp:CheckBox id=aDenialCreditAppIncomplete runat="server" Text="Credit Application Incomplete"></asp:CheckBox></TD></TR>
        <TR>
          <TD class=FieldLabel>B - EMPLOYMENT STATUS</TD>
          <TD><asp:CheckBox id=aDenialInadequateCollateral runat="server" Text="Inadequate Collateral"></asp:CheckBox></TD></TR>
        <TR>
          <TD><asp:CheckBox id=aDenialUnableVerifyEmployment runat="server" Text="Unable to Verify Employment"></asp:CheckBox></TD>
          <TD>&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox id=aDenialUnacceptableProp runat="server" Text="Unacceptable Property"></asp:CheckBox></TD></TR>
        <TR>
          <TD><asp:CheckBox id=aDenialLenOfEmployment runat="server" Text="Length of Employment"></asp:CheckBox></TD>
          <TD>&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox id=aDenialInsufficientPropData runat="server" Text="Insufficient Data - Property"></asp:CheckBox></TD></TR>
        <TR>
          <TD><asp:CheckBox id=aDenialTemporaryEmployment runat="server" Text="Temporary or Irregular Employment, Insufficient Stability of Income"></asp:CheckBox></TD>
          <TD vAlign=top>&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox id=aDenialUnacceptableAppraisal runat="server" Text="Unacceptable Appraisal"></asp:CheckBox></TD></TR>
        <TR>
          <TD class=FieldLabel>C - INCOME</TD>
          <TD>&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox id=aDenialUnacceptableLeasehold runat="server" Text="Unacceptable Leasehold Estate"></asp:CheckBox></TD></TR>
        <TR>
          <TD vAlign=top><asp:CheckBox id=aDenialInsufficientIncomeForMortgagePmt runat="server" Text="Insufficient Income for Mortgage Payments"></asp:CheckBox></TD>
          <TD><asp:CheckBox id=aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds runat="server" Text="We do not grant credit to any applicant on the terms and conditions you have requested"></asp:CheckBox>&nbsp;</TD></TR>
        <TR>
          <TD><asp:CheckBox id=aDenialUnableVerifyIncome runat="server" Text="Unable to Verify Income"></asp:CheckBox></TD>
          <TD><asp:CheckBox id=aDenialWithdrawnByApp runat="server" Text="Withdrawn by Applicant"></asp:CheckBox></TD></TR>
        <TR>
          <TD></TD>
          <TD><asp:CheckBox id=aDenialOtherReason1 runat="server" Text="Other - "></asp:CheckBox><asp:TextBox id=aDenialOtherReason1Desc style="width:325px" runat="server"></asp:TextBox></TD></TR>
        <TR>
          <TD></TD>
          <TD><asp:CheckBox id=aDenialOtherReason2 runat="server" Text="Other - "></asp:CheckBox><asp:TextBox id=aDenialOtherReason2Desc style="width:325px" runat="server"></asp:TextBox></TD></TR></TABLE></TD></TR>
  <TR>
    <TD noWrap>
      <TABLE id=Table3 cellSpacing=0 cellPadding=0 width="100%" border=0 class=GroupTable>
        <TR>
          <TD class=FormTableSubHeader>2. Disclosure of use of information obtained from an outside 
          source.</TD></TR>
        <TR>
          <TD class=FieldLabel><asp:CheckBox id=aDenialDecisionBasedOnReportAgency runat="server" Text="Information obtained in a report from consumer reporting agency:"></asp:CheckBox></TD></TR>
        <TR>
          <TD>
            <TABLE id=Table4 cellSpacing=0 cellPadding=0 border=0>
			  <tr>
				<td nowrap class=FieldLabel></td>
				<td nowrap class=FieldLabel>
					<uc:CFM name=CFM1 runat="server" Type="3" CompanyNameField="CRAName" StreetAddressField="CRAAddress" CityField="CRACity" StateField="CRAState" ZipField="CRAZipcode" PhoneField="CRAPhone"></uc:CFM>
				</td>
			  </tr>
              <TR>
                <TD noWrap class=FieldLabel>Name</TD>
                <TD noWrap><asp:TextBox id=CRAName runat="server" Width="239px"></asp:TextBox></TD></TR>
              <TR>
                <TD noWrap class=FieldLabel>Address</TD>
                <TD noWrap><asp:TextBox id=CRAAddress runat="server" Width="226px"></asp:TextBox></TD></TR>
              <TR>
                <TD noWrap></TD>
                <TD noWrap><asp:TextBox id=CRACity runat="server"></asp:TextBox><ml:StateDropDownList id=CRAState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=CRAZipcode runat="server" CssClass="mask" preset="zipcode" width="50"></ml:ZipcodeTextBox></TD></TR>
              <TR>
                <TD noWrap class=FieldLabel>Phone</TD>
                <TD noWrap><ml:PhoneTextBox id=CRAPhone runat="server" CssClass="mask" preset="phone" width="120"></ml:PhoneTextBox></TD></TR></TABLE>
            <TABLE id=Table7 cellSpacing=0 cellPadding=0 border=0>
			  <tr>
				<TD nowrap class=FieldLabel></TD>
				<TD nowrap class=FieldLabel>
					<uc:CFM name=CFM2 runat="server" Type="3" CompanyNameField="CRA2Name" StreetAddressField="CRA2Address" CityField="CRA2City" StateField="CRA2State" ZipField="CRA2Zipcode" PhoneField="CRA2Phone"></uc:CFM>
				</TD>
			  </tr>
              <TR>
                <TD noWrap class=FieldLabel>Name</TD>
                <TD noWrap><asp:TextBox id=CRA2Name runat="server" Width="239px"></asp:TextBox></TD></TR>
              <TR>
                <TD noWrap class=FieldLabel>Address</TD>
                <TD noWrap><asp:TextBox id=CRA2Address runat="server" Width="226px"></asp:TextBox></TD></TR>
              <TR>
                <TD noWrap></TD>
                <TD noWrap><asp:TextBox id=CRA2City runat="server"></asp:TextBox><ml:StateDropDownList id=CRA2State runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=CRA2Zipcode runat="server" CssClass="mask" preset="zipcode" width="50"></ml:ZipcodeTextBox></TD></TR>
              <TR>
                <TD noWrap class=FieldLabel>Phone</TD>
                <TD noWrap><ml:PhoneTextBox id=CRA2Phone runat="server" CssClass="mask" preset="phone" width="120"></ml:PhoneTextBox></TD></TR></TABLE></TD></TR>
        <TR>
          <TD>
            <TABLE id=Table8 cellSpacing=0 cellPadding=0 border=0>
			  <tr>
				<td nowrap class=FieldLabel></td>
				<td nowrap class=FieldLabel>
					<uc:CFM name=CFM3 runat="server" Type="3" CompanyNameField="CRA3Name" StreetAddressField="CRA3Address" CityField="CRA3City" StateField="CRA3State" ZipField="CRA3Zipcode" PhoneField="CRA3Phone"></uc:CFM>
				</td>
			  </tr>
              <TR>
                <TD noWrap class=FieldLabel>Name</TD>
                <TD noWrap><asp:TextBox id=CRA3Name runat="server" Width="239px"></asp:TextBox></TD></TR>
              <TR>
                <TD noWrap class=FieldLabel>Address</TD>
                <TD noWrap><asp:TextBox id=CRA3Address runat="server" Width="226px"></asp:TextBox></TD></TR>
              <TR>
                <TD noWrap></TD>
                <TD noWrap><asp:TextBox id=CRA3City runat="server"></asp:TextBox><ml:StateDropDownList id=CRA3State runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=CRA3Zipcode runat="server" CssClass="mask" preset="zipcode" width="50"></ml:ZipcodeTextBox></TD></TR>
              <TR>
                <TD noWrap class=FieldLabel>Phone</TD>
                <TD noWrap><ml:PhoneTextBox id=CRA3Phone runat="server" CssClass="mask" preset="phone" width="120"></ml:PhoneTextBox></TD></TR></TABLE>
            </TD></TR>
        <tr>
            <td class="FieldLabel" style="padding-top: 25px">
                <asp:CheckBox id="aDenialWeObtainedCreditScoreFromCRA" runat="server" Text="We also obtained your credit score from this consumer reporting agency and used it in making our credit decision."></asp:CheckBox>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="padding-left: 20px">
                <asp:CheckBox id="aDenialCreditScoreLckd" runat="server" Text="Set manually" onclick="f_onSetManuallyClick();"></asp:CheckBox>
            </td>
        </tr>
          <tr><td>&nbsp;</td></tr>
        <tr>
            <td style="padding-left: 20px">
                <table cellSpacing="0" cellPadding="0" border="0">
                    <tr>
                        <td class="FieldLabel">
                            Credit Score
                        </td>
                        <td>
                            <asp:TextBox ID="aDenialCreditScore" runat="server" Width="50px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Date
                        </td>
                        <td>
                            <ml:datetextbox id="aDenialCreditScoreD" runat="server" width="75" preset="date" CssClass="mask"></ml:datetextbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="FieldLabel">
                            Scores range from
                            <asp:TextBox ID="aDenialCreditScoreRangeFrom" runat="server" Width="50px"></asp:TextBox>
                            to
                            <asp:TextBox ID="aDenialCreditScoreRangeTo" runat="server" Width="50px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="FieldLabel">
                            Key factors that adversely affect credit score
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left:20px">
                            <asp:TextBox ID="aDenialCreditScoreFactor1" runat="server" Width="500px" MaxLength="70"></asp:TextBox>                        
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left:20px">
                            <asp:TextBox ID="aDenialCreditScoreFactor2" runat="server" Width="500px" MaxLength="70"></asp:TextBox>                        
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left:20px">
                            <asp:TextBox ID="aDenialCreditScoreFactor3" runat="server" Width="500px" MaxLength="70"></asp:TextBox>                        
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left:20px">
                            <asp:TextBox ID="aDenialCreditScoreFactor4" runat="server" Width="500px" MaxLength="70"></asp:TextBox>                        
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left:20px">                       
                            <asp:CheckBox id="aDenialIsFactorNumberOfRecentInquiries" runat="server" Text="Number of recent inquiries on consumer report."></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="FieldLabel">
                            Contact information for questions regarding credit score:
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
					        <uc:CFM ID="ScoreContact" runat="server" Type="3" CompanyNameField="ScoreContactName" StreetAddressField="ScoreContactAddress" CityField="ScoreContactCity" StateField="ScoreContactState" ZipField="ScoreContactZip" PhoneField="ScoreContactPhone"></uc:CFM>                        
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Name
                        </td>
                        <td>                            
                            <asp:TextBox id="ScoreContactName" runat="server" Width="239px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Address
                        </td>
                        <td>                            
                            <asp:TextBox id="ScoreContactAddress" runat="server" Width="226px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:TextBox id="ScoreContactCity" runat="server"></asp:TextBox><ml:StateDropDownList id="ScoreContactState" runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id="ScoreContactZip" runat="server" CssClass="mask" preset="zipcode" width="50"></ml:ZipcodeTextBox>                            
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Phone
                        </td>
                        <td>
                            <ml:PhoneTextBox id="ScoreContactPhone" runat="server" CssClass="mask" preset="phone" width="120"></ml:PhoneTextBox>                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <TR>
          <TD class="FieldLabel" style="padding-top: 25px"><asp:CheckBox id=aDenialDecisionBasedOnCRA runat="server" Text="Information obtained from an outside source other than a consumer reporting agency. You have the right to make a written request of us for disclosure of the nature of this information."></asp:CheckBox></TD></TR>
        <TR>
          <TD class="FieldLabel">Additional Statement</TD></TR>
        <TR>
          <TD><asp:TextBox id=aDenialAdditionalStatement runat="server" TextMode="MultiLine" Width="540px" Height="116px"></asp:TextBox></TD></TR></TABLE></TD></TR>
  <TR>
    <TD class=FormTableSubHeader noWrap>3. The Federal Agency that administers 
      compliance with this law is:</TD></TR>
  <TR>
    <TD noWrap>
      <TABLE id=Table6 cellSpacing=0 cellPadding=0 border=0>
		<tr>
			<td class=FieldLabel nowrap></td>
			<td nowrap>
				<uc:CFM name=CFM2 runat="server" Type="28" CompanyNameField="ECOA_CompanyName" StreetAddressField="ECOA_StreetAddr" CityField="ECOA_City" StateField="ECOA_State" ZipField="ECOA_Zip"></uc:CFM>
			</td>
		</tr>
        <TR>
          <TD class=FieldLabel noWrap>Name</TD>
          <TD noWrap><asp:TextBox id=ECOA_CompanyName runat="server" Width="239px"></asp:TextBox></TD></TR>
        <TR>
          <TD class=FieldLabel noWrap>Address</TD>
          <TD noWrap><asp:TextBox id=ECOA_StreetAddr runat="server" Width="226px"></asp:TextBox></TD></TR>
        <TR>
          <TD noWrap></TD>
          <TD noWrap><asp:TextBox id=ECOA_City runat="server"></asp:TextBox><ml:StateDropDownList id=ECOA_State runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=ECOA_Zip runat="server" CssClass="mask" preset="zipcode" width="50"></ml:ZipcodeTextBox></TD></TR></TABLE></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap>&nbsp;</TD></TR>
  <TR>
    <TD noWrap class=FieldLabel>Completion of letter by <asp:TextBox id=prepareBy runat="server"></asp:TextBox></TD></TR>
  <tr>
    <td class=FieldLabel nowrap>
      <table id=Table5 cellspacing=0 cellpadding=0 border=0>
		<tr height=10><td class=FieldLabel nowrap></td></tr>
		<tr>
		  <td class=FieldLabel nowrap></td>
		  <td nowrap>
			<uc:CFM name=CFM3 runat="server" Type="26" CompanyNameField="BrokerName" StreetAddressField="BrokerAddress" CityField="BrokerCity" StateField="BrokerState" ZipField="BrokerZip" CompanyPhoneField="BrokerPhone"></uc:CFM>
		  </td>
		</tr>
        <tr>
          <td class=FieldLabel nowrap>Broker Name</td>
          <td nowrap><asp:TextBox id=BrokerName runat="server" Width="237px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Broker Address</td>
          <td nowrap><asp:TextBox id=BrokerAddress runat="server" Width="237px"></asp:TextBox></td></tr>
        <tr>
          <td nowrap></td>
          <td nowrap><asp:TextBox id=BrokerCity runat="server" Width="137px"></asp:TextBox><ml:StateDropDownList id=BrokerState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=BrokerZip runat="server" preset="zipcode" width="50"></ml:ZipcodeTextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Broker Phone</td>
          <td nowrap><ml:PhoneTextBox id=BrokerPhone runat="server" preset="phone" width="120"></ml:PhoneTextBox></td></tr></table></td></tr></TABLE>


<input type="hidden" id="IsBorrower" />

     </form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
	
  </body>
</HTML>
