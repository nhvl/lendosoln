<%@ Register TagPrefix="uc1" TagName="Loan1003pg4" Src="Loan1003pg4.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Loan1003pg3" Src="Loan1003pg3.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Loan1003pg2" Src="Loan1003pg2.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Loan1003pg1" Src="Loan1003pg1.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Tabs" Src="~/common/BaseTabPage.ascx" %>
<%@ Register TagPrefix="QRP" TagName="QualRatePopup" Src="~/newlos/Forms/QualRateCalculationPopup.ascx" %>
<%@ Page language="c#" Codebehind="Loan1003.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.Loan1003" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<html>
  <head runat="server">
    <title>Loan1003</title>
      <style type="text/css">
        .InsetBorder tr td.little-indent { padding-left: 10px; }

        .income-collection-dependent label {
            display: none;
        }

        .is-income-collection-enabled .income-collection-dependent label {
            display: initial;
        }

        .is-income-collection-enabled .income-collection-dependent a {
            display: none;
        }
      </style>
  </head>
<body ms_positioning="FlowLayout" bgcolor="gainsboro" scroll="yes">
  <form id="Loan1003" method="post" runat="server">
  <QRP:QualRatePopup ID="QualRatePopup" runat="server" />
  <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
    <tr>
      <td class="Tabs">
            <uc1:Tabs runat="server" ID="Tabs" />
      </td>
    </tr>
    <tr>
      <td style="padding-left: 5px">
        <uc1:Loan1003pg1 ID="Loan1003pg1" runat="server" />
        <uc1:Loan1003pg2 ID="Loan1003pg2" runat="server" />
        <uc1:Loan1003pg3 ID="Loan1003pg3" runat="server" />
        <uc1:Loan1003pg4 ID="Loan1003pg4" runat="server" />
      </td>
    </tr>
  </table>
  </form>
  <script type="text/javascript">
    function _postSaveMe(result) {
        if (result.RefreshNavigation && parent && parent.treeview) {
            parent.treeview.location.reload();
            window.setTimeout(function() {
                if (parent.treeview && typeof parent.treeview.selectPageID === 'function') {
                    parent.treeview.selectPageID(<%= AspxTools.JsString(this.PageID) %>);
                }
            }, 500);
        }
    }
  </script>
</body>
</html>
