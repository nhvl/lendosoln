<%@ Page language="c#" Codebehind="LoanTerms.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.LoanTerms" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="Rfp" TagName="RateFloorPopup" Src="~/newlos/Forms/RateFloorPopup.ascx" %>
<%@ Import namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/tr/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head id="Head1" runat="server">
    <title>Loan Terms</title>
    <style type="text/css">
        #InitialApr
        {
            display: inline-block;
            margin-right: 11px;
        }
        #AmortizationTableDiv
        {
            width:630px;
            height:500px;
            display:none;
            overflow: inherit;
        }
        #AmortizationTableFrame
        {
            width:100%;
            height:98%;
        }
        .ui-dialog
        {
            border: solid 1px black !important;
        }
        .ui-dialog-titlebar
        {
            background-color:Maroon !important;
            border: 1px solid Maroon !important;
        }
        .width-550 {
            width: 550px;
        }
        .additional-terms-data {
            border: 0;
            width: 181px;
            border-spacing: 0;
            border-collapse: collapse;
        }
        .additional-terms-data td {
            padding: 0;
        }
    </style>
  </head>
<body class=RightBackground MS_POSITIONING="FlowLayout">
<script type="text/javascript">
<!--
    var oRolodex = null; 

    $(function () {
        // Attempting to apply this attribute to the ASP radio button
        // directly will result in the container span receiving the
        // attribute, causing Ctrl+Alt+Q to not pick up the ID. Apply 
        // the attribute here instead.
        $('input[name="sGfeIsBalloon"]').attr('data-field-id', 'sGfeIsBalloon');
    })

function _init() {
    if (null == oRolodex) {
        oRolodex = new cRolodex();
    }

    var sRAdjWorstIndexCheckbox = <%=AspxTools.JsGetElementById(sRAdjWorstIndex)%>;
    if (sRAdjWorstIndexCheckbox) {
        <%=AspxTools.JsGetElementById(sRAdjIndexR)%>.readOnly = sRAdjWorstIndexCheckbox.checked;
    }
    lockField(<%=AspxTools.JsGetElementById(sSchedDueD1Lckd)%>, 'sSchedDueD1');
    lockField(document.getElementById('sIPerDayLckd'), 'sIPerDay');

    f_setAmortizationStatus( false );
    f_setPrepmtPeriodMonthsStatus();
    
    $("#AmortizationTableFrame").attr("src", "../Forms/AmortizationTable.aspx?loanid=" + ML.sLId);

    $("#AmortizationTableFrame").on("load", function() {
        var contents = $("#AmortizationTableFrame").contents();
        var closeBtn = contents.find("input[value='Close']");
        closeBtn.off("click");

        var header = contents.find("h4.page-header");
        header.hide();
        closeBtn.click(closeDialog);
    });

    if (typeof (RateFloorPopupInit) == "function")
    {
        RateFloorPopupInit();
    }
}

function closeDialog() {
    $("#AmortizationTableDiv").dialog("close");
}

function showAmortTable() {
    PolyShouldShowConfirmSave(isDirty(), function(){
      $("#AmortizationTableDiv").dialog({
          width: 630,
          height: 540,
          modal: true,
          title: "Amortization Table",
          resizable: false,
          draggable: false,
      });
    }, saveMe);
}

function updateDisclosedAPR()
{
    location.reload();
}
function f_setPrepmtPeriodMonthsStatus() {
    var ddl = <%=AspxTools.JsGetElementById(sPrepmtPenaltyT)%>;
    var sPrepmtPeriodMonths = <%=AspxTools.JsGetElementById(sPrepmtPeriodMonths)%>;
    var sSoftPrepmtPeriodMonths = <%=AspxTools.JsGetElementById(sSoftPrepmtPeriodMonths)%>;
    var sGfeMaxPpmtPenaltyAmt = <%=AspxTools.JsGetElementById(sGfeMaxPpmtPenaltyAmt)%>;
    if (ddl.options[ddl.selectedIndex].text === "may") {
        sPrepmtPeriodMonths.readOnly = false;
        sSoftPrepmtPeriodMonths.readOnly = false;
        sGfeMaxPpmtPenaltyAmt.readOnly = false;
    } else {
        sPrepmtPeriodMonths.readOnly = true;
        sPrepmtPeriodMonths.value = "";
        sSoftPrepmtPeriodMonths.readOnly = true;
        sSoftPrepmtPeriodMonths.value = "";
        
        if (ML && !ML.leaveMaxPrepaymentEnabled) {
            sGfeMaxPpmtPenaltyAmt.readOnly = true;
        }
    }
}

var openedDate = <%=AspxTools.JsString(m_openedDate)%>;
function onDateKeyUp(o, event) {
    if (event.keyCode == 79) {
        o.value = openedDate;
        updateDirtyBit(event);
    }
}
function f_setAmortizationStatus()
{
    <% if ( ! IsReadOnly ) { %>
    var scheduleBtn = document.getElementById('AmortizationScheduleBtn');
    var scheduleTbl = document.getElementById('PmtScheduleTable');
    var sIsOptionArm = <%=AspxTools.JsGetElementById(sIsOptionArm)%>;
    var sOptionArmMinPayOptionT = <%=AspxTools.JsGetElementById(sOptionArmMinPayOptionT)%>;
    var sOptionArmNoteIRDiscount = <%=AspxTools.JsGetElementById(sOptionArmNoteIRDiscount)%>;
    var ssOptionArmMinPayOptionT = sOptionArmMinPayOptionT[sOptionArmMinPayOptionT.selectedIndex].value
    var sOptionArmPmtDiscount = <%=AspxTools.JsGetElementById(sOptionArmPmtDiscount)%>;

    sOptionArmMinPayOptionT.disabled = ! sIsOptionArm.checked ;
    sOptionArmPmtDiscount.readOnly = (! sIsOptionArm.checked )  || (sIsOptionArm.checked && ssOptionArmMinPayOptionT != <%=AspxTools.JsString(E_sOptionArmMinPayOptionT.ByDiscountPmt.ToString("D"))%>);
    sOptionArmNoteIRDiscount.readOnly = (! sIsOptionArm.checked ) || (sIsOptionArm.checked && ssOptionArmMinPayOptionT != <%=AspxTools.JsString(E_sOptionArmMinPayOptionT.ByDiscountNoteIR.ToString("D"))%>);

    <%=AspxTools.JsGetElementById(sOptionArmMinPayPeriod)%>.readOnly = ! sIsOptionArm.checked;
    <%=AspxTools.JsGetElementById(sOptionArmMinPayIsIOOnly)%>.disabled = ! sIsOptionArm.checked;
    <%=AspxTools.JsGetElementById(sOptionArmIntroductoryPeriod)%>.readOnly = ! sIsOptionArm.checked;  
    <%=AspxTools.JsGetElementById(sOptionArmInitialFixMinPmtPeriod)%>.readOnly = ! sIsOptionArm.checked;
    <%=AspxTools.JsGetElementById(sIsFullAmortAfterRecast)%>.disabled = ! sIsOptionArm.checked;
    <%=AspxTools.JsGetElementById(sOptionArmTeaserR)%>.readOnly = (! sIsOptionArm.checked) || <%=AspxTools.JsBool(m_IsRateLocked) %>;
    <% } %>
  
}
function f_chooseFromTemplate() {
    showModal('/newlos/Forms/TruthInLendingTemplateList.aspx', null, null, null, function(arg){
      if (arg.OK) {
          var args = getAllFormValues(null);
          if(typeof(_postGetAllFormValues) == 'function')
          {
              _postGetAllFormValues(args);
          }
          args["loanid"] = document.getElementById("loanid").value;
          args["applicationid"] = document.getElementById("applicationid").value;    
          args["lLpTemplateId"] = arg.id;
          var result = gService.loanedit.call("PopulateFromLoanProgramTemplate", args);
  
          if (!result.error) {
              populateForm(result.value, null);    
              if(typeof(_postRefreshCalculation) == "function")
              {
                  _postRefreshCalculation(result.value, null);
              }
              updateDirtyBit();
              _init();
          }
      } 
    },{ hideCloseButton: true });
}

function f_update_sTilRedisclosuresReceivedD() {
    var args = {
        sRedisclosureMethodT : document.getElementById("sRedisclosureMethodT").value,
        sTilRedisclosureD : document.getElementById("sTilRedisclosureD").value,
        loanid : ML.sLId
    }
    
    var result = gService.loanedit.call("CalculateRedisclosuresReceivedD", args);
    
    if (!result.error) {
        if ("True" == result.value.OK) {
            document.getElementById("sTilRedisclosuresReceivedD").value = result.value.sRedisclosuresReceivedD;
            updateDirtyBit();
        }
    } else {
        alert(result.UserMessage);
    }
}
//-->
</script>

<form id="LoanTerms" method="post" runat="server">
<div id="AmortizationTableDiv">
    <iframe id="AmortizationTableFrame" src=""></iframe>
</div>
<Rfp:RateFloorPopup runat="server" ID="RateFloorPopup" />
<div class="MainRightHeader">Loan Terms</div>
<table class="FormTable" id="Table21" cellSpacing="0" cellPadding="0" border="0">
  
  <tr>
        <td noWrap>
            <table class="InsetBorder" style="width:99%">
                <tr>
                    <td class="FieldLabel">Loan Program</td>
                    <td><input type="text" style="width:250px" runat="server" id="sLpTemplateNm" /></td>
                </tr>
                <tr>
                    <td class="FieldLabel">Registered Loan Program</td>
                    <td><input id="sLpTemplateNmSubmitted" readonly style="width:250px" type="text" runat="server" /></td>
                </tr>
            </table>   
        </td>
      </tr>
  <tr>
    <td noWrap>
      <table class=InsetBorder id=Table13 cellSpacing=0 cellPadding=0 width="99%" border=0>
        <tr>
          <td>
            <table id=Table15 cellSpacing=0 cellPadding=0 border=0 style="width:100%">
              <tr runat="server" id="trPreparedDate" visible="true">
                <td class=FieldLabel>Total Loan Amt</td>
                <td><ml:moneytextbox id=sFinalLAmt runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                <td class=FieldLabel style="PADDING-LEFT:10px">Interest Rate</td>
                <td><ml:percenttextbox id=sNoteIR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
              <tr>
                <td class=FieldLabel>Amort Type</td>
                <td><asp:dropdownlist id=sFinMethT runat="server" onchange="refreshCalculation();">
						</asp:dropdownlist></td>
                <td class=FieldLabel style="PADDING-LEFT:10px">Term/Due</td>
                <td><asp:textbox id=sTerm runat="server" Width="38px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox> / <asp:textbox id=sDue runat="server" Width="38px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel>Amort Desc</td>
                <td><asp:textbox id=sFinMethDesc runat="server" MaxLength="50"></asp:textbox></td>
                <td class=FieldLabel style="PADDING-LEFT:10px; padding-right: 5px;">
                    <label>1st Payment Date</label>
                    <asp:CheckBox runat="server" ID="sSchedDueD1Lckd" onclick="lockField(this, 'sSchedDueD1'); refreshCalculation();"/>
                </td>
                <td><ml:datetextbox id=sSchedDueD1 runat="server" width="75" preset="date" CssClass="mask" onchange="refreshCalculation();"></ml:datetextbox></td></tr>
              <tr>
                  <td class="FieldLabel">Days in Year</td>
                  <td>
                      <asp:TextBox ID="sDaysInYr" runat="server" Width="38px" onchange="refreshCalculation();"></asp:TextBox>
                  </td>
                  <td class="FieldLabel" style="PADDING-LEFT:10px; padding-right: 5px;">Per-diem Interest</td>
                  <td class="FieldLabel">
                      <ml:MoneyTextBox ID="sIPerDay" runat="server" onchange="refreshCalculation();" decimalDigits="6"></ml:MoneyTextBox> per day
                      <asp:CheckBox ID="sIPerDayLckd" runat="server" onclick="lockField(this, 'sIPerDay'); refreshCalculation();"/>Lock
                  </td>
              </tr>
              </table></td></tr></table></td></tr>
  <tr>
    <td noWrap><INPUT type=button value="Populate from loan program template ..." onclick="f_chooseFromTemplate();" style="WIDTH: 229px"></td></tr>
  <tr>
    <td noWrap>
      <table id=Table14 cellSpacing=0 cellPadding=0 width="100%" border=0>
        <tr>
          <td>
            <table id=Table1 width="100%" border=0>
              <tr>
                <td vAlign=top>
                  <table id=Table4 cellSpacing=0 cellPadding=0 border=0>
                    <tr>
                      <td>
                        <table class=InsetBorder id=Table3 cellSpacing=0 cellPadding=0 width=190 border=0>
                          <tr>
                            <td>
                              <table cellSpacing=0 cellPadding=0>
                                <tr>
                                    <td class="FormTableSubheader FieldLabel" colSpan=2 >Adjustable Rate Mortgage</td>
                                </tr>
                                <tr>
                                <td class=FieldLabel colSpan=2 
                                >Rate Adjustment</td></tr>
                                <tr>
                                <td style="WIDTH: 113px" 
                                >1st Adj Cap</td>
                                <td><ml:percenttextbox id=sRAdj1stCapR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
                                <tr>
                                <td style="WIDTH: 113px" 
                                >1st Change</td>
                                <td><asp:textbox id=sRAdj1stCapMon runat="server" Width="42px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox>&nbsp;mths</td></tr>
                                <tr>
                                <td style="WIDTH: 113px" 
                                >Adj Cap</td>
                                <td><ml:percenttextbox id=sRAdjCapR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
                                <tr>
                                <td style="WIDTH: 113px" 
                                >Adj Period</td>
                                <td><asp:textbox id=sRAdjCapMon runat="server" Width="42px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox> mths</td></tr>
                                <tr>
                                <td style="WIDTH: 113px" 
                                >Life Adj Cap</td>
                                <td><ml:percenttextbox id=sRAdjLifeCapR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
                                <tr>
                                <td style="WIDTH: 113px" >Margin</td>
                                <td><ml:percenttextbox id=sRAdjMarginR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
                                <tr>
                                <td style="WIDTH: 113px">Index</td>
                                <td noWrap><asp:CheckBox id=sRAdjWorstIndex runat="server" Text="Worst Case" onclick="refreshCalculation();"></asp:CheckBox></td></tr>
                                <tr>
                                <td><asp:Textbox ToolTip="ARM Index Name" runat="server" ID="sArmIndexNameVstr" onchange="refreshCalculation();"></asp:Textbox></td>
                                <td><ml:percenttextbox id=sRAdjIndexR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
                                <tr>
                                    <td style="WIDTH: 140px" nowrap>
                                        Rate Floor
                                        <a href="javascript:void(0);" id="RateFloorCalcLink">calculate</a>
                                    </td>
                                    <td>
                                        <ml:percenttextbox data-field-id="sRAdjFloorR" ReadOnly="true" id="sRAdjFloorR_readonly" runat="server" width="70" preset="percent"></ml:percenttextbox>
                                    </td>
                                </tr>
                                <tr>
                                <td style="WIDTH: 113px" 
                                >Round&nbsp; <asp:dropdownlist id=sRAdjRoundT runat="server" onchange="refreshCalculation();"></asp:dropdownlist></td>
                                <td><ml:percenttextbox id=sRAdjRoundToR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
                                <tr>
                                <td>Convertible Mortgage</td>
                                <td><asp:CheckBox runat="server" id="sIsConvertibleMortgage" onclick="refreshCalculation();" /></td>
                                </tr>
                                
                                </table>
                                </td>
                                </tr>
                                </table>
                                </td>
                                </tr>
                    <tr>
                      <td>
                        <table class=InsetBorder id=Table11 cellSpacing=0 
                        cellPadding=0 width=190 border=0>
                          <tr>
                            <td class="FormTableSubheader" colSpan=2 
                            >ARM Potential Negative 
                            Amort.</td></tr>
                          <tr>
                            <td class=FieldLabel colSpan=2 
                              >Payment Adjustment</td></tr>
                          <tr>
                            <td>Adj Cap</td>
                            <td><ml:percenttextbox id=sPmtAdjCapR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
                          <tr>
                            <td>Adj Period</td>
                            <td><asp:textbox id=sPmtAdjCapMon runat="server" Width="42px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox>&nbsp;mths</td></tr>
                          <tr>
                            <td>Recast Pd</td>
                            <td><asp:textbox id=sPmtAdjRecastPeriodMon runat="server" Width="42px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox>&nbsp;mths</td></tr>
                          <tr>
                            <td>Recast Stop</td>
                            <td><asp:textbox id=sPmtAdjRecastStop runat="server" Width="42px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox>&nbsp;mths</td></tr>
                          <tr>
                            <td>Max Bal</td>
                            <td><ml:percenttextbox id=sPmtAdjMaxBalPc runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
                          <tr>
                            <td>Use Max Bal for DTI</td>
                            <td><asp:CheckBox runat="server" id="sDtiUsingMaxBalPc" onclick="refreshCalculation();" /></td></tr>
                      </table></td></tr></table></td>
                <td vAlign=top align=left width="100%">
                    <table class="InsetBorder" cellSpacing="0" width="99%" border=0>
                        <tr>
                            <td noWrap>
                                <label>Initial APR</label>&nbsp;
                                <ml:percenttextbox id="sInitAPR" runat="server" width="60px" preset="percent"></ml:percenttextbox>

                                <label style="margin-left: 10px;">Last Disclosed APR</label>&nbsp;
                                <ml:percenttextbox id="sLastDiscAPR" runat="server" width="60px" preset="percent"></ml:percenttextbox>
                            </td>
                        </tr>
                    </table>
                  <table class=InsetBorder id=Table10 cellSpacing=0 
                  cellPadding=0 width="99%" border=0>
                    <tr>
                      <td noWrap>
                        <table id="PmtScheduleTable" cellSpacing=0 cellPadding=0 border=0>
                          <tr>
                            <td class=FormTableSubheader noWrap colSpan=3>Pmt Schedule</td>
                            <td class=FormTableSubheader noWrap align=right colSpan=3>
                                <span title="APR-related closing costs">APR-related CC</span>
                                :
                                <ml:moneytextbox id=sAprIncludedCc runat="server" width="78px" preset="money" ReadOnly="True"></ml:moneytextbox>
                                &nbsp;
                                APR:
                                <ml:percenttextbox id=sApr runat="server" width="60px" preset="percent" ReadOnly="True"></ml:percenttextbox>
                            </td></tr>
                          <tr>
                            <td noWrap align=right colSpan=2>
                                # of Pmts
                            </td>
                            <td align=center>Pmt Date</td>
                            <td nowrap>Interest Rate</td>
                            <td noWrap>Monthly Payment</td>
                            <td>Balance</td></tr>
                          <tr>
                            <td></td>
                            <td><asp:textbox id=sSchedPmtNum1 runat="server" Width="35" ReadOnly="True"></asp:textbox></td>
                            <td><asp:textbox id=sSchedDueD12 runat="server" Width="62" ReadOnly="True"></asp:textbox></td>
                            <td><ml:percenttextbox id=sSchedIR1 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></td>
                            <td><ml:moneytextbox id=sSchedPmt1 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id=sSchedBal1 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
                          <tr>
                            <td></td>
                            <td><asp:textbox id=sSchedPmtNum2 runat="server" Width="35px" ReadOnly="True"></asp:textbox></td>
                            <td><asp:textbox id=sSchedDueD2 runat="server" Width="62px" ReadOnly="True"></asp:textbox></td>
                            <td><ml:percenttextbox id=sSchedIR2 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></td>
                            <td><ml:moneytextbox id=sSchedPmt2 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id=sSchedBal2 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
                          <tr>
                            <td></td>
                            <td><asp:textbox id=sSchedPmtNum3 runat="server" Width="35px" ReadOnly="True"></asp:textbox></td>
                            <td><asp:textbox id=sSchedDueD3 runat="server" Width="62px" ReadOnly="True"></asp:textbox></td>
                            <td><ml:percenttextbox id=sSchedIR3 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></td>
                            <td><ml:moneytextbox id=sSchedPmt3 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id=sSchedBal3 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
                          <tr>
                            <td></td>
                            <td><asp:textbox id=sSchedPmtNum4 runat="server" Width="35px" ReadOnly="True"></asp:textbox></td>
                            <td><asp:textbox id=sSchedDueD4 runat="server" Width="62px" ReadOnly="True"></asp:textbox></td>
                            <td><ml:percenttextbox id=sSchedIR4 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></td>
                            <td><ml:moneytextbox id=sSchedPmt4 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id=sSchedBal4 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
                          <tr>
                            <td></td>
                            <td><asp:textbox id=sSchedPmtNum5 runat="server" Width="35px" ReadOnly="True"></asp:textbox></td>
                            <td><asp:textbox id=sSchedDueD5 runat="server" Width="62px" ReadOnly="True"></asp:textbox></td>
                            <td><ml:percenttextbox id=sSchedIR5 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></td>
                            <td><ml:moneytextbox id=sSchedPmt5 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id=sSchedBal5 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
                          <tr>
                            <td></td>
                            <td><asp:textbox id=sSchedPmtNum6 runat="server" Width="35px" ReadOnly="True"></asp:textbox></td>
                            <td><asp:textbox id=sSchedDueD6 runat="server" Width="62px" ReadOnly="True"></asp:textbox></td>
                            <td><ml:percenttextbox id=sSchedIR6 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></td>
                            <td><ml:moneytextbox id=sSchedPmt6 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id=sSchedBal6 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
                          <tr>
                            <td></td>
                            <td><asp:textbox id=sSchedPmtNum7 runat="server" Width="35px" ReadOnly="True"></asp:textbox></td>
                            <td><asp:textbox id=sSchedDueD7 runat="server" Width="62px" ReadOnly="True"></asp:textbox></td>
                            <td><ml:percenttextbox id=sSchedIR7 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></td>
                            <td><ml:moneytextbox id=sSchedPmt7 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id=sSchedBal7 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
                          <tr>
                            <td></td>
                            <td><asp:textbox id=sSchedPmtNum8 runat="server" Width="35px" ReadOnly="True"></asp:textbox></td>
                            <td><asp:textbox id=sSchedDueD8 runat="server" Width="62px" ReadOnly="True"></asp:textbox></td>
                            <td><ml:percenttextbox id=sSchedIR8 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></td>
                            <td><ml:moneytextbox id=sSchedPmt8 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id=sSchedBal8 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
                          <tr>
                            <td></td>
                            <td><asp:textbox id=sSchedPmtNum9 runat="server" Width="35px" ReadOnly="True"></asp:textbox></td>
                            <td><asp:textbox id=sSchedDueD9 runat="server" Width="62px" ReadOnly="True"></asp:textbox></td>
                            <td><ml:percenttextbox id=sSchedIR9 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></td>
                            <td><ml:moneytextbox id=sSchedPmt9 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id=sSchedBal9 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
                          <tr>
                            <td></td>
                            <td><asp:textbox id=sSchedPmtNum10 runat="server" Width="35px" ReadOnly="True"></asp:textbox></td>
                            <td><asp:textbox id=sSchedDueD10 runat="server" Width="62px" ReadOnly="True"></asp:textbox></td>
                            <td><ml:percenttextbox id=sSchedIR10 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></td>
                            <td><ml:moneytextbox id=sSchedPmt10 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id=sSchedBal10 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
                          <tr>
                            <td>Total&nbsp;</td>
                            <td><asp:textbox id=sSchedPmtNumTot runat="server" Width="35px" ReadOnly="True" Height="20px"></asp:textbox></td>
                            <td align=right colSpan=2>Sum of Monthly Payments&nbsp;</td>
                            <td><ml:moneytextbox id=sSchedPmtTot runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                            <td></td></tr>
                          <tr>
                            <td colSpan=4>Amount Financed <ml:moneytextbox id=sFinancedAmt runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                            <td align=right>Finance Charge&nbsp;</td>
                            <td><ml:moneytextbox id=sFinCharge runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
                            
                          <tr>
                            <td colSpan=4><INPUT id="AmortizationScheduleBtn" type=button value="View Amortization Schedule" NoHighlight onclick="showAmortTable();" runat="server"></td>
                            <td></td>
                            <td></td></tr></table></td></tr></table>
                  <table>
                    <tr>
                      <td>
                                <table class="InsetBorder additional-terms-data" id="Table7">
                          <tr>
                                        <td class="FormTableSubheader" colspan="2">Graduated Payment Mortgage</td>
                                    </tr>
                          <tr>
                                        <td style="width: 16px">Years</td>
                                        <td style="width: 45px">
                                            <asp:TextBox ID="sGradPmtYrs" runat="server" Width="81px" onchange="refreshCalculation();" MaxLength="4" Height="22px"></asp:TextBox></td>
                                    </tr>
                          <tr>
                                        <td style="width: 16px">Rate</td>
                                        <td style="width: 45px">
                                            <ml:PercentTextBox ID="sGradPmtR" runat="server" Width="70" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                                <table class="InsetBorder additional-terms-data" id="Table9">
                          <tr>
                                        <td class="FormTableSubheader" colspan="2">Interest Only</td>
                                    </tr>
                          <tr>
                            <td>Months</td>
                                        <td nowrap>
                                            <asp:TextBox ID="sIOnlyMon" runat="server" Width="36" onchange="refreshCalculation();" MaxLength="3"></asp:TextBox>&nbsp;mths</td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                                <table class="InsetBorder additional-terms-data">
                                    <tr>
                                        <td class="FormTableSubheader" colspan="2">Balloon Payment</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RadioButton ID="sGfeIsBalloon_No" GroupName="sGfeIsBalloon" runat="server" onclick="refreshCalculation();" value="false" />
                                            No
                                        </td>
                                        <td>
                                            <asp:RadioButton ID="sGfeIsBalloon_Yes" GroupName="sGfeIsBalloon" runat="server" onclick="refreshCalculation();" value="true" />
                                            Yes
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td></tr></table>
            <table id=Table5 cellSpacing=0 cellPadding=0 width="100%" border=0 
            >
            <tr><td colspan="2">
              <table class=InsetBorder id=Table2 cellSpacing=0 cellPadding=0 border=0>
                <tr><td class="FormTableSubheader" colspan="4">Option ARM</td></tr>
                <tr>
                  <td>Option ARM</td>
                  <td><asp:CheckBox id="sIsOptionArm" Runat="server" onclick="refreshCalculation();" /></td>
                  <td></td>
                  <td></td>
                <tr>
                  <td>Minimum Payment Period</td>
                  <td><asp:TextBox runat="server" id="sOptionArmMinPayPeriod" onchange="refreshCalculation();" Width="35px" />&nbsp;mths</td>
                  <td style="PADDING-LEFT: 12px; WHITE-SPACE: nowrap">Minimum Payment Calculation Method</td>
                  <td><asp:DropDownList runat="server" id="sOptionArmMinPayOptionT" onchange="refreshCalculation();"></asp:DropDownList></td>
                </tr>
                <tr>
                  <td>Initial Fixed Rate Period</td>
                  <td><asp:TextBox runat="server" id="sOptionArmIntroductoryPeriod" onchange="refreshCalculation();" Width="35px" />&nbsp;mths</td>
                  <td style="PADDING-LEFT: 12px">Teaser Rate</td>
                  <td><ml:percenttextbox runat="server" id="sOptionArmTeaserR" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td>
                </tr>
                <tr>
                  <td style="WHITE-SPACE: nowrap">Initial Fixed Minimum Payment</td>
                  <td><asp:TextBox runat="server" id="sOptionArmInitialFixMinPmtPeriod" onchange="refreshCalculation();" Width="35px" />&nbsp;mths</td>
                  <td style="PADDING-LEFT: 12px">Discount from Note Rate</td>
                  <td><ml:percenttextbox runat="server" id="sOptionArmNoteIRDiscount" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td>
                </tr>
                 <tr>
                  <td style="WHITE-SPACE: nowrap">Is Minimum Payment I/O Only?</td>
                  <td><asp:CheckBox runat="server" id="sOptionArmMinPayIsIOOnly" onclick="refreshCalculation();" /></td>
                  <td style="PADDING-LEFT: 12px; WHITE-SPACE: nowrap">Discount from Fully Amortizing Payment</td>
                  <td><ml:percenttextbox runat="server" id="sOptionArmPmtDiscount" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td>
                </tr>
                <tr>
                  <td>Fully Amortizing at Recast</td>
                  <td><asp:CheckBox runat="server" id="sIsFullAmortAfterRecast" onclick="refreshCalculation();" /></td>
                  <td></td>
                  <td></td>
                </tr>
              </table>
            </td></tr>
              <tr>
                <td vAlign=top>
                  <table class=InsetBorder id=Table22 cellSpacing=0 cellPadding=0 
                  border=0>
                    <tr>
                      <td class="FormTableSubheader" noWrap colSpan=3 
                      >Buydown Mortgage</td></tr>
                    <tr>
                      <td noWrap></td>
                      <td noWrap>Rate Reduction</td>
                      <td noWrap>Term (mths)</td></tr>
                    <tr>
                      <td noWrap>1</td>
                      <td noWrap><ml:percenttextbox id=sBuydwnR1 runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td>
                      <td noWrap><asp:textbox id=sBuydwnMon1 runat="server" Width="36px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox></td></tr>
                    <tr>
                      <td noWrap>2</td>
                      <td noWrap><ml:percenttextbox id=sBuydwnR2 runat="server" width="70" preset="percent" onchange="refreshCalculation();" DESIGNTIMEDRAGDROP="3491"></ml:percenttextbox></td>
                      <td noWrap><asp:textbox id=sBuydwnMon2 runat="server" Width="36px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox></td></tr>
                    <tr>
                      <td noWrap>3</td>
                      <td noWrap><ml:percenttextbox id=sBuydwnR3 runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td>
                      <td noWrap><asp:textbox id=sBuydwnMon3 runat="server" Width="36px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox></td></tr>
                    <tr>
                      <td noWrap>4</td>
                      <td noWrap><ml:percenttextbox id=sBuydwnR4 runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td>
                      <td noWrap><asp:textbox id=sBuydwnMon4 runat="server" Width="36px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox></td></tr>
                    <tr>
                      <td noWrap>5</td>
                      <td noWrap><ml:percenttextbox id=sBuydwnR5 runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td>
                      <td noWrap><asp:textbox id=sBuydwnMon5 runat="server" Width="36px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox></td></tr>
                    <tr>
                      <td noWrap></td>
                      <td noWrap></td>
                      <td noWrap></td></tr></table></td>
                <td vAlign=top width="100%">
                  <table class=InsetBorder id=Table8 cellSpacing=0 cellPadding=0 
                  border=0>
                    <tr>
                      <td class="FormTableSubheader" colSpan=6 
                      >Mortgage Insurance</td></tr>
                    <tr>
                      <td style="WIDTH: 106px">Base 
                      type:</td>
                      <td>Base amount:</td>
                      <td style="WIDTH: 19px"></td>
                      <td>Rate %</td>
                      <td>Amount</td>
                      <td>Term</td></tr>
                    <tr>
                      <td style="WIDTH: 106px"><asp:dropdownlist id=sProMInsT runat="server" onchange="refreshCalculation();" Height="25px">
									</asp:dropdownlist></td>
                      <td><ml:moneytextbox id=sProMInsBaseAmt runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                      <td style="WIDTH: 19px">1</td>
                      <td><ml:percenttextbox id=sProMInsR runat="server" width="61px" preset="percent" onchange="refreshCalculation();" decimalDigits="6"></ml:percenttextbox></td>
                      <td><ml:moneytextbox id=sProMIns runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                      <td><asp:textbox id=sProMInsMon runat="server" Width="36px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox></td></tr>
                    <tr>
                      <td style="WIDTH: 106px"></td>
                      <td></td>
                      <td style="WIDTH: 19px">2</td>
                      <td><ml:percenttextbox id=sProMInsR2 runat="server" width="61px" preset="percent" onchange="refreshCalculation();"  decimalDigits="6" ></ml:percenttextbox></td>
                      <td><ml:moneytextbox id=sProMIns2 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                      <td><asp:textbox id=sProMIns2Mon runat="server" Width="36px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox></td></tr>
                    <tr>
                      <td style="WIDTH: 237px" colSpan=3 
                        >Cancel at LTV&nbsp;<ml:percenttextbox id=sProMInsCancelLtv runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox>&nbsp;</td>
                      <td colSpan=3><INPUT 
                        id=sProMInsMidptCancel onclick=refreshCalculation(); 
                        type=checkbox runat="server">Cancel 
                        at midpoint</td>
                    </tr>
                      <tr>
                          <td>Cancel at Appraisal LTV
                                <ml:PercentTextBox runat="server" ID="sProMInsCancelAppraisalLtv" onchange="refreshCalculation();"></ml:PercentTextBox>
                          </td>
                      </tr>
                        <tr><td colspan="5">Minimum number of payments before cancellation&nbsp;&nbsp;<asp:textbox id="sProMInsCancelMinPmts" runat="server" Width="34px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox>
                        
                        </td>
                        
                        </tr></table>
                  <table class=InsetBorder id=Table6 cellSpacing=0 cellPadding=0 border=0>
                    <tr>
                      <td class=FormTableSubheader colSpan=2>Prepayment</td>
                      <td class=FormTableSubheader></td>
                      <td class=FormTableSubheader></td>
                      <td class=FormTableSubheader></td>
                      <td class=FormTableSubheader></td></tr>
                    <tr>
                      <td noWrap>Extra Amt</td>
                      <td style="WIDTH: 45px"><ml:moneytextbox id=sPpmtAmt runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                      <td>every</td>
                      <td noWrap><asp:textbox id=sPpmtMon runat="server" Width="35px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox> mths</td>
                      <td noWrap>start after</td>
                      <td noWrap><asp:textbox id=sPpmtStartMon runat="server" Width="34px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox> mths</td></tr>
                    <tr>
                      <td noWrap colSpan=3>One Time 
                        Prepayment</td>
                      <td style="WIDTH: 45px"><ml:moneytextbox id=sPpmtOneAmt runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                      <td style="WIDTH: 45px">after</td>
                      <td><asp:textbox id=sPpmtOneMon runat="server" Width="33px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox> mths</td></tr></table></td></tr></table></td></tr>
        <tr>
          <td>
            <table id=Table16 cellSpacing=0 cellPadding=0 width="100%" border=0 
            >
              <tr>
                <td><asp:checkbox id=sAprIncludesReqDeposit runat="server" Text="REQUIRED DEPOSIT:&nbsp; The annual percentage rate does not take into account your required deposit."></asp:checkbox></td></tr>
              <tr>
                <td><asp:checkbox id=sHasDemandFeature runat="server" DESIGNTIMEDRAGDROP="88" Text="DEMAND FEATURE:&nbsp; This obligation has a demand feature."></asp:checkbox></td></tr>
              <tr>
                <td><asp:CheckBox ID="sIsPropertyBeingSoldByCreditor" runat="server" Text="CREDIT SALE: The property for this loan is being sold by the creditor." /></td></tr>
              <tr>
                <td><asp:checkbox id=sHasVarRFeature runat="server" DESIGNTIMEDRAGDROP="87" Text="VARIABLE RATE FEATURE:&nbsp; This loan contains a variable rate feature.&nbsp; A variable rate disclosure has been provided earlier."></asp:checkbox></td></tr>
              <tr>
                <td><asp:textbox id=sVarRNotes runat="server" Width="531px" MaxLength="150" DESIGNTIMEDRAGDROP="398" Rows="3" TextMode="MultiLine"></asp:textbox></td></tr></table></td></tr>
        <tr>
          <td>
            <table class=InsetBorder id=Table18 cellSpacing=0 cellPadding=0 
            width="99%" border=0>
              <tr>
                <td noWrap>
                  <table id=Table17 cellSpacing=0 cellPadding=0 width="100%" 
                  border=0>
                    <tr>
                      <td class=FieldLabel vAlign=top noWrap >Insurance</td>
                      <td>The following insurance is 
                        required to obtain credit:&nbsp; <asp:textbox id=sInsReqDesc runat="server" Width="340px" MaxLength="21"></asp:textbox></td></tr>
                    <tr>
                      <td class=FieldLabel></td>
                      <td><asp:checkbox id=sReqCreditLifeIns runat="server" Text="Credit life insurance"></asp:checkbox><asp:checkbox id=sReqCreditDisabilityIns runat="server" Text="Credit disability"></asp:checkbox><asp:checkbox id=sReqPropIns runat="server" Text="Property insurance"></asp:checkbox><asp:checkbox id=sReqFloodIns runat="server" Text="Flood insurance"></asp:checkbox></td></tr>
                    <tr>
                      <td class=FieldLabel></td>
                      <td>
                        You may obtain the insurance 
                        from anyone you want that is acceptable to creditor</td></tr>
                    <tr>
                      <td class=FieldLabel></td>
                      <td><asp:checkbox id=sIfPurchInsFrCreditor runat="server" Text="If you purchase"></asp:checkbox><asp:checkbox id=sIfPurchPropInsFrCreditor runat="server" Text="property"></asp:checkbox><asp:checkbox id=sIfPurchFloodInsFrCreditor runat="server" Text="flood insurance from creditor you will pay"></asp:checkbox>&nbsp;<ml:moneytextbox id=sInsFrCreditorAmt runat="server" width="90" preset="money"></ml:moneytextbox>&nbsp;for 
                        a one year term.</td></tr>
                    <tr>
                      <td class=FieldLabel vAlign=top noWrap >Security</td>
                      <td>You are giving a security 
                        interest in:&nbsp; <asp:textbox id=sSpFullAddr runat="server" Width="491px" ReadOnly="True"></asp:textbox></td></tr>
                    <tr>
                      <td></td>
                      <td><asp:checkbox id=sSecurityPurch runat="server" Text="The goods or property being purchased"></asp:checkbox>&nbsp;&nbsp; 
<asp:checkbox id=sSecurityCurrentOwn runat="server" DESIGNTIMEDRAGDROP="387" Text="Real property you already own."></asp:checkbox></td></tr>
                    <tr>
                      <td class=FieldLabel vAlign=top noWrap >Filing Fees</td>
                      <td><ml:moneytextbox id=sFilingF runat="server" width="90" preset="money"></ml:moneytextbox></td></tr>
                    <tr>
                      <td class=FieldLabel vAlign=top noWrap >Late Charges</td>
                      <td>If your payment is more than <asp:textbox id=sLateDays runat="server" Width="43px" MaxLength="4" DESIGNTIMEDRAGDROP="17"></asp:textbox>&nbsp;days 
                        late, you will be charged&nbsp;<ml:percenttextbox id=sLateChargePc runat="server" width="70" preset="percent"></ml:percenttextbox>
                        of <asp:textbox id=sLateChargeBaseDesc runat="server" Width="128px" MaxLength="36"></asp:textbox></td></tr>
                    <tr>
                      <td class=FieldLabel vAlign=top noWrap >Prepayment</td>
                      <td>If you pay off early, you&nbsp;
                        <asp:dropdownlist id=sPrepmtPenaltyT onclick="f_setPrepmtPeriodMonthsStatus()" onchange="refreshCalculation();" runat="server">
                            <asp:ListItem Value="0" Text=""></asp:ListItem>
                            <asp:ListItem Value="1">will not</asp:ListItem>
                            <asp:ListItem Value="2">may</asp:ListItem>
                        </asp:dropdownlist>
                        &nbsp;be required to pay a penalty and
                        <asp:dropdownlist id=sPrepmtRefundT runat="server">
                            <asp:ListItem Value="0" Text=""></asp:ListItem>
                            <asp:ListItem Value="1">will not</asp:ListItem>
                            <asp:ListItem Value="2">may</asp:ListItem>
						</asp:dropdownlist>&nbsp;be entitled to a refund 
                        of part of the finance charge.
                        <br />
                        Prepayment Period <asp:textbox id="sPrepmtPeriodMonths" runat="server" Width="43px"></asp:textbox> months
                        &nbsp;&nbsp;&nbsp;
                        Soft Prepayment Period <asp:textbox id="sSoftPrepmtPeriodMonths" runat="server" Width="43px"></asp:textbox> months
                        &nbsp;&nbsp;&nbsp;
                        Maximum amount <ml:MoneyTextBox runat="server" ID="sGfeMaxPpmtPenaltyAmt"></ml:MoneyTextBox>
                      </td></tr>
                    <tr>
                      <td class=FieldLabel vAlign=top noWrap >Assumption</td>
                      <td>Someone buying your property
                        <asp:dropdownlist id=sAssumeLT runat="server">
                            <asp:ListItem Value="0" Text=""></asp:ListItem>
                            <asp:ListItem Value="1">may not</asp:ListItem>
                            <asp:ListItem Value="2">may</asp:ListItem>
                            <asp:ListItem Value="3">may, subject to conditions</asp:ListItem>
						</asp:dropdownlist>&nbsp;assume the remainder of 
                        your loan on the original 
              terms.</td></tr></table></td></tr></table></td></tr>
        <tr>
          <td>See your contract documents for any 
            additional information about nonpayment, default, any required repayment 
            in full before the scheduled date and prepayment refunds and 
            penalties.</td></tr>
        <tr>
          <td><asp:checkbox id=sAsteriskEstimate runat="server" Text="* means an estimate"></asp:checkbox></td></tr>
        <tr>
          <td><asp:checkbox id=sOnlyLatePmtEstimate runat="server" Text="all dates and numerical disclosures except the late payment disclosures are estimates."></asp:checkbox>
          </td>
        </tr>
        <tr>
          <td class="FieldLabel">
              Other Loan Features (Explain)
          </td>
        </tr>
        <tr>
          <td>
              <asp:textbox class="width-550" id="sOtherLoanFeaturesDescription" runat="server" MaxLength="100"></asp:textbox> 
          </td>
        </tr>
      </table></td></tr></table></form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
	
  </body>
</HTML>
