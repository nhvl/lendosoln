﻿using System;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class UsdaLoanGuaranteeServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                default:
                    break;
            }
        }

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(UsdaLoanGuaranteeServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            // ** Approved Lender Table
            IPreparerFields approvedLender = dataLoan.GetPreparerOfForm(E_PreparerFormT.UsdaGuaranteeApprovedLender, E_ReturnOptionIfNotExist.CreateNew);
            approvedLender.CompanyName = GetString("UsdaGuaranteeApprovedLenderCompanyName");
            approvedLender.TaxId = GetString("UsdaGuaranteeApprovedLenderTaxId");

            approvedLender.PreparerName = GetString("UsdaGuaranteeApprovedLenderPreparerName");
            approvedLender.EmailAddr = GetString("UsdaGuaranteeApprovedLenderEmailAddr");
            approvedLender.PhoneOfCompany = GetString("UsdaGuaranteeApprovedLenderPhoneOfCompany");
            approvedLender.FaxOfCompany = GetString("UsdaGuaranteeApprovedLenderFaxOfCompany");
            approvedLender.Title = GetString("UsdaGuaranteeApprovedLenderTitle");
            approvedLender.Update();

            IPreparerFields thirdPartyTPO = dataLoan.GetPreparerOfForm(E_PreparerFormT.UsdaGuaranteeThirdPartyOriginator, E_ReturnOptionIfNotExist.CreateNew);
            thirdPartyTPO.CompanyName = GetString("UsdaGuaranteeThirdPartyOriginatorCompanyName");
            thirdPartyTPO.TaxId = GetString("UsdaGuaranteeThirdPartyOriginatorTaxId");
            thirdPartyTPO.Update();

            dataLoan.sUsdaGuaranteeNum = GetString("sUsdaGuaranteeNum"); // this should probably be readonly
            dataLoan.sLNm = GetString("sLNm");

            // ** Applicant/Co-Applicant Info Table
            dataApp.aBFirstNm = GetString("aBFirstNm");
            dataApp.aBMidNm = GetString("aBMidNm");
            dataApp.aBLastNm = GetString("aBLastNm");
            dataApp.aBSuffix = GetString("aBSuffix");
            dataApp.aBSsn = GetString("aBSsn");
            dataApp.aBDob_rep = GetString("aBDob");
            dataApp.aBDecCitizen = GetString("aBDecCitizen");
            dataApp.aBDecResidency = GetString("aBDecResidency");

            dataApp.aBUsdaGuaranteeVeteranTri = GetTriState("aBUsdaGuaranteeVeteranTri");
            dataApp.aBUsdaGuaranteeDisabledTri = GetTriState("aBUsdaGuaranteeDisabledTri");
            dataApp.aBGender = (E_GenderT)GetInt("aBGenderT");
            dataApp.aBUsdaGuaranteeFirstTimeHomeBuyerTri = GetTriState("aBUsdaGuaranteeFirstTimeHomeBuyerTri");

            dataApp.aBHispanicT = (E_aHispanicT)GetInt("aBHispanicT");

            dataApp.aBIsAmericanIndian = GetBool("aBIsAmericanIndian");
            dataApp.aBIsPacificIslander = GetBool("aBIsPacificIslander");
            dataApp.aBIsAsian = GetBool("aBIsAsian");
            dataApp.aBIsWhite = GetBool("aBIsWhite");
            dataApp.aBIsBlack = GetBool("aBIsBlack");

            dataApp.aBMaritalStatT = (E_aBMaritalStatT) GetInt("aBMaritalStatT");

            dataApp.aBUsdaGuaranteeEmployeeRelationshipT = (E_aUsdaGuaranteeEmployeeRelationshipT)GetTriState("aBUsdaGuaranteeEmployeeRelationshipT"); // It's not really a tristate
            dataApp.aBUsdaGuaranteeEmployeeRelationshipDesc = GetString("aBUsdaGuaranteeEmployeeRelationshipDesc");

            //dataApp.aBHighestScore_rep = GetString("aBHighestScore");
            //dataApp.aBHasHighestScore = GetBool("aBHasHighestScore");

            dataApp.aCFirstNm = GetString("aCFirstNm");
            dataApp.aCMidNm = GetString("aCMidNm");
            dataApp.aCLastNm = GetString("aCLastNm");
            dataApp.aCSuffix = GetString("aCSuffix");
            dataApp.aCSsn = GetString("aCSsn");
            dataApp.aCDob_rep = GetString("aCDob");
            dataApp.aCDecCitizen = GetString("aCDecCitizen");
            dataApp.aCDecResidency = GetString("aCDecResidency");

            dataApp.aCUsdaGuaranteeVeteranTri = GetTriState("aCUsdaGuaranteeVeteranTri");
            dataApp.aCUsdaGuaranteeDisabledTri = GetTriState("aCUsdaGuaranteeDisabledTri");
            dataApp.aCGender = (E_GenderT) GetInt("aCGenderT");
            dataApp.aCUsdaGuaranteeFirstTimeHomeBuyerTri = GetTriState("aCUsdaGuaranteeFirstTimeHomeBuyerTri");

            dataApp.aCHispanicT = (E_aHispanicT) GetInt("aCHispanicT");

            dataApp.aCIsAmericanIndian = GetBool("aCIsAmericanIndian");
            dataApp.aCIsPacificIslander = GetBool("aCIsPacificIslander");
            dataApp.aCIsAsian = GetBool("aCIsAsian");
            dataApp.aCIsWhite = GetBool("aCIsWhite");
            dataApp.aCIsBlack = GetBool("aCIsBlack");

            dataApp.aCMaritalStatT = (E_aCMaritalStatT) GetInt("aCMaritalStatT");

            dataApp.aCUsdaGuaranteeEmployeeRelationshipT = (E_aUsdaGuaranteeEmployeeRelationshipT)GetTriState("aCUsdaGuaranteeEmployeeRelationshipT"); // It's not really a tristate
            dataApp.aCUsdaGuaranteeEmployeeRelationshipDesc = GetString("aCUsdaGuaranteeEmployeeRelationshipDesc");

            //dataApp.aCHighestScore_rep = GetString("aCHighestScore");
            //dataApp.aCHasHighestScore = GetBool("aCHasHighestScore");

            // ** Extra information
            dataLoan.sSpAddr = GetString("sSpAddr");
            dataLoan.sSpCity = GetString("sSpCity");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sSpZip = GetString("sSpZip");
            dataLoan.sSpCounty = GetString("sSpCounty");

            //dataLoan.sUsdaGuaranteeIsRefinanceLoan = GetBool("sUsdaGuaranteeIsRefinanceLoan");

            dataLoan.sUsdaGuaranteeRefinancedLoanT = (E_sUsdaGuaranteeRefinancedLoanT)GetTriState("sUsdaGuaranteeRefinancedLoanT"); // It's not really a tristate

            dataLoan.sUsdaGuaranteeNumOfPersonInHousehold_rep = GetString("sUsdaGuaranteeNumOfPersonInHousehold");
            dataLoan.sUsdaGuaranteeNumOfDependents_rep = GetString("sUsdaGuaranteeNumOfDependents");

            //dataLoan.sLTotI_rep = GetString("sLTotI");
            dataLoan.sUsdaGuaranteeAdjustedAnnualIncome_rep = GetString("sUsdaGuaranteeAdjustedAnnualIncome");

            //dataLoan.sQualTopR_rep = GetString("sQualTopR");
            //dataLoan.sQualBottomR_rep = GetString("sQualBottomR");

            //dataLoan.sLAmtCalc_rep = GetString("sLAmtCalc");
           // dataLoan.sNoteIR_rep = GetString("sNoteIR");
            //dataLoan.sMonthlyPmt_rep = GetString("sMonthlyPmt");

            dataLoan.sUsdaGuaranteeIsNoteRateBasedOnFnma = GetBool("sUsdaGuaranteeIsNoteRateBasedOnFnma");
            dataLoan.sUsdaGuaranteeNoteRateBasedOnFnmaD_rep = GetString("sUsdaGuaranteeNoteRateBasedOnFnmaD");

            dataLoan.sUsdaGuaranteeIsNoteRateLocked = GetBool("sUsdaGuaranteeIsNoteRateLocked");
            dataLoan.sUsdaGuaranteeNoteRateLockedD_rep = GetString("sUsdaGuaranteeNoteRateLockedD");

            dataLoan.sUsdaGuaranteeIsNoteRateFloat = GetBool("sUsdaGuaranteeIsNoteRateFloat");

            // ** Loan funds purpose
            dataLoan.sUsdaGuaranteePurchaseAmtDesc = GetString("sUsdaGuaranteePurchaseAmtDesc");
            dataLoan.sUsdaGuaranteePurchaseAmt_rep = GetString("sUsdaGuaranteePurchaseAmt");

            dataLoan.sUsdaGuaranteeClosingCostsDesc = GetString("sUsdaGuaranteeClosingCostsDesc");
            dataLoan.sUsdaGuaranteeClosingCosts_rep = GetString("sUsdaGuaranteeClosingCosts");

            dataLoan.sUsdaGuaranteeRepairFeeDesc = GetString("sUsdaGuaranteeRepairFeeDesc");
            dataLoan.sUsdaGuaranteeRepairFee_rep = GetString("sUsdaGuaranteeRepairFee");

            dataLoan.sUsdaGuaranteeGuaranteeFeeDesc = GetString("sUsdaGuaranteeGuaranteeFeeDesc");
            dataLoan.sUsdaGuaranteeGuaranteeFee_rep = GetString("sUsdaGuaranteeGuaranteeFee");

            //dataLoan.sUsdaGuaranteeTotalRequestFee_rep = GetString("sUsdaGuaranteeTotalRequestFee");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            // ** Approved Lender Table
            IPreparerFields approvedLender = dataLoan.GetPreparerOfForm(E_PreparerFormT.UsdaGuaranteeApprovedLender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("UsdaGuaranteeApprovedLenderCompanyName", approvedLender.CompanyName);
            SetResult("UsdaGuaranteeApprovedLenderTaxId", approvedLender.TaxId);

            SetResult("UsdaGuaranteeApprovedLenderPreparerName", approvedLender.PreparerName);
            SetResult("UsdaGuaranteeApprovedLenderEmailAddr", approvedLender.EmailAddr);
            SetResult("UsdaGuaranteeApprovedLenderPhoneOfCompany", approvedLender.PhoneOfCompany);
            SetResult("UsdaGuaranteeApprovedLenderFaxOfCompany", approvedLender.FaxOfCompany);
            SetResult("UsdaGuaranteeApprovedLenderTitle", approvedLender.Title);

            IPreparerFields thirdPartyTPO = dataLoan.GetPreparerOfForm(E_PreparerFormT.UsdaGuaranteeThirdPartyOriginator, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("UsdaGuaranteeThirdPartyOriginatorCompanyName", thirdPartyTPO.CompanyName);
            SetResult("UsdaGuaranteeThirdPartyOriginatorTaxId", thirdPartyTPO.TaxId);

            // ** Applicant/Co-Applicant Info Table
            // Applicant
            SetResult("aBFirstNm", dataApp.aBFirstNm);
            SetResult("aBMidNm", dataApp.aBMidNm);
            SetResult("aBLastNm", dataApp.aBLastNm);
            SetResult("aBSuffix", dataApp.aBSuffix);
            SetResult("aBSsn", dataApp.aBSsn);
            SetResult("aBDob", dataApp.aBDob_rep);
            SetResult("aBDecCitizen", dataApp.aBDecCitizen);
            SetResult("aBDecResidency", dataApp.aBDecResidency);

            SetResult("aBUsdaGuaranteeVeteranTri", dataApp.aBUsdaGuaranteeVeteranTri);
            SetResult("aBUsdaGuaranteeDisabledTri", dataApp.aBUsdaGuaranteeDisabledTri);
            SetResult("aBGenderT", dataApp.aBGender);
            SetResult("aBUsdaGuaranteeFirstTimeHomeBuyerTri", dataApp.aBUsdaGuaranteeFirstTimeHomeBuyerTri);

            SetResult("aBHispanicT", dataApp.aBHispanicT);

            SetResult("aBIsAmericanIndian", dataApp.aBIsAmericanIndian);
            SetResult("aBIsPacificIslander", dataApp.aBIsPacificIslander);
            SetResult("aBIsAsian", dataApp.aBIsAsian);
            SetResult("aBIsWhite", dataApp.aBIsWhite);
            SetResult("aBIsBlack", dataApp.aBIsBlack);

            SetResult("aBMaritalStatT", dataApp.aBMaritalStatT);

            SetResult("aBUsdaGuaranteeEmployeeRelationshipT", dataApp.aBUsdaGuaranteeEmployeeRelationshipT);
            SetResult("aBUsdaGuaranteeEmployeeRelationshipDesc", dataApp.aBUsdaGuaranteeEmployeeRelationshipDesc);

            SetResult("aBDecisionCreditScore", dataApp.aBDecisionCreditScore_rep);
            SetResult("aBHasHighestScore", !dataApp.aBHasHighestScore);

            // Co-applicant
            SetResult("aCFirstNm", dataApp.aCFirstNm);
            SetResult("aCMidNm", dataApp.aCMidNm);
            SetResult("aCLastNm", dataApp.aCLastNm);
            SetResult("aCSuffix", dataApp.aCSuffix);
            SetResult("aCSsn", dataApp.aCSsn);
            SetResult("aCDob", dataApp.aCDob_rep);
            SetResult("aCDecCitizen", dataApp.aCDecCitizen);
            SetResult("aCDecResidency", dataApp.aCDecResidency);

            SetResult("aCUsdaGuaranteeVeteranTri", dataApp.aCUsdaGuaranteeVeteranTri);
            SetResult("aCUsdaGuaranteeDisabledTri", dataApp.aCUsdaGuaranteeDisabledTri);
            SetResult("aCGenderT", dataApp.aCGender);
            SetResult("aCUsdaGuaranteeFirstTimeHomeBuyerTri", dataApp.aCUsdaGuaranteeFirstTimeHomeBuyerTri);

            SetResult("aCHispanicT", dataApp.aCHispanicT);

            SetResult("aCIsAmericanIndian", dataApp.aCIsAmericanIndian);
            SetResult("aCIsPacificIslander", dataApp.aCIsPacificIslander);
            SetResult("aCIsAsian", dataApp.aCIsAsian);
            SetResult("aCIsWhite", dataApp.aCIsWhite);
            SetResult("aCIsBlack", dataApp.aCIsBlack);

            SetResult("aCMaritalStatT", dataApp.aCMaritalStatT);

            SetResult("aCUsdaGuaranteeEmployeeRelationshipT", dataApp.aCUsdaGuaranteeEmployeeRelationshipT);
            SetResult("aCUsdaGuaranteeEmployeeRelationshipDesc", dataApp.aCUsdaGuaranteeEmployeeRelationshipDesc);

            SetResult("aCDecisionCreditScore", dataApp.aCDecisionCreditScore_rep);
            SetResult("aCHasHighestScore", !dataApp.aCHasHighestScore);

            // ** Extra information
            SetResult("sSpAddr", dataLoan.sSpAddr);
            SetResult("sSpCity", dataLoan.sSpCity);

            SetResult("sSpState", dataLoan.sSpState);

            SetResult("sSpZip", dataLoan.sSpZip);

            SetResult("sSpCounty", dataLoan.sSpCounty);

            SetResult("sUsdaGuaranteeIsRefinanceLoan", dataLoan.sUsdaGuaranteeIsRefinanceLoan);

            SetResult("sUsdaGuaranteeRefinancedLoanT", dataLoan.sUsdaGuaranteeRefinancedLoanT);

            SetResult("sUsdaGuaranteeNumOfPersonInHousehold", dataLoan.sUsdaGuaranteeNumOfPersonInHousehold_rep);
            SetResult("sUsdaGuaranteeNumOfDependents", dataLoan.sUsdaGuaranteeNumOfDependents_rep);

            SetResult("sLTotI", dataLoan.sLTotI_rep);
            SetResult("sUsdaGuaranteeAdjustedAnnualIncome", dataLoan.sUsdaGuaranteeAdjustedAnnualIncome_rep);

            SetResult("sQualTopR", dataLoan.sQualTopR_rep);
            SetResult("sQualBottomR", dataLoan.sQualBottomR_rep);

            SetResult("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sMonthlyPmt", dataLoan.sMonthlyPmt_rep);

            SetResult("sUsdaGuaranteeIsNoteRateBasedOnFnma", dataLoan.sUsdaGuaranteeIsNoteRateBasedOnFnma);
            SetResult("sUsdaGuaranteeNoteRateBasedOnFnmaD", dataLoan.sUsdaGuaranteeNoteRateBasedOnFnmaD_rep);

            SetResult("sUsdaGuaranteeIsNoteRateLocked", dataLoan.sUsdaGuaranteeIsNoteRateLocked);
            SetResult("sUsdaGuaranteeNoteRateLockedD", dataLoan.sUsdaGuaranteeNoteRateLockedD_rep);

            SetResult("sUsdaGuaranteeIsNoteRateFloat", dataLoan.sUsdaGuaranteeIsNoteRateFloat);

            // ** Loan funds purpose
            SetResult("sUsdaGuaranteePurchaseAmtDesc", dataLoan.sUsdaGuaranteePurchaseAmtDesc);
            SetResult("sUsdaGuaranteePurchaseAmt", dataLoan.sUsdaGuaranteePurchaseAmt_rep);

            SetResult("sUsdaGuaranteeClosingCostsDesc", dataLoan.sUsdaGuaranteeClosingCostsDesc);
            SetResult("sUsdaGuaranteeClosingCosts", dataLoan.sUsdaGuaranteeClosingCosts_rep);

            SetResult("sUsdaGuaranteeRepairFeeDesc", dataLoan.sUsdaGuaranteeRepairFeeDesc);
            SetResult("sUsdaGuaranteeRepairFee", dataLoan.sUsdaGuaranteeRepairFee_rep);

            SetResult("sUsdaGuaranteeGuaranteeFeeDesc", dataLoan.sUsdaGuaranteeGuaranteeFeeDesc);
            SetResult("sUsdaGuaranteeGuaranteeFee", dataLoan.sUsdaGuaranteeGuaranteeFee_rep);

            SetResult("sUsdaGuaranteeTotalRequestFee", dataLoan.sUsdaGuaranteeTotalRequestFee_rep);
        }
    }

    public partial class UsdaLoanGuaranteeService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new UsdaLoanGuaranteeServiceItem());
        }
    }
}
