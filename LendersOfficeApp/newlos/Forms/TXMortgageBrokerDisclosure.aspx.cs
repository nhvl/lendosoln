using System;

using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
	public partial class TXMortgageBrokerDisclosure : BaseLoanPage
	{
        #region Protected member variables
        #endregion
    
		protected void PageInit(object sender, System.EventArgs e)
		{
            this.PageID = "TXMortgageBrokerDisclosure";
            this.PageTitle = "Texas Mortgage Broker Disclosure";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CTXBrokerDisclosurePDF);
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(TXMortgageBrokerDisclosure));
            dataLoan.InitLoad();

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.TXMortgageBrokerDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            TXMortgageBrokerDisclosureLicenseNumOfAgent.Text = preparer.LicenseNumOfAgent;
            TXMortgageBrokerDisclosurePreparerName.Text = preparer.PreparerName;

            sTexasDiscWillSubmitToLender.Checked = dataLoan.sTexasDiscWillSubmitToLender;
            sTexasDiscAsIndependentContractor.Checked = dataLoan.sTexasDiscAsIndependentContractor;
            sTexasDiscWillActAsFollows.Checked = dataLoan.sTexasDiscWillActAsFollows;
            sTexasDiscWillActAsFollowsDesc.Text = dataLoan.sTexasDiscWillActAsFollowsDesc;
            sTexasDiscCompensationIncluded.Checked = dataLoan.sTexasDiscCompensationIncluded;
            sTexasDiscChargeVaried.Checked = dataLoan.sTexasDiscChargeVaried;
            sTexasDiscChargeVariedDesc.Text = dataLoan.sTexasDiscChargeVariedDesc;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
            IsAppSpecific = false;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

	}
}
