using System;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.Admin;

namespace LendersOfficeApp.newlos.Forms
{
	public partial class CAMLDSService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected BrokerDB Broker
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BrokerDB; }
        }

        private int sFileVersion 
        { 
            get { return GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck); } 
        }
        private CPageData CreatePageData(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(CAMLDSService));
        }
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "MLDSpg1_CalculateData":
                    Mldspg1_CalculateData();
                    break;
                case "MLDSpg1_SaveData":
                    Mldspg1_SaveData();
                    break;
                case "MLDSpg2_CalculateData":
                    Mldspg2_CalculateData();
                    break;
                case "MLDSpg2_SaveData":
                    Mldspg2_SaveData();
                    break;

            }
        }

        #region MLDS Page 2
        private void Mldspg2_CalculateData() 
        {
            Guid loanID = GetGuid("loanid");
            CPageData dataLoan = CreatePageData(loanID);
            dataLoan.InitLoad();

            Mldspg2_BindData(dataLoan, false);

            Mldspg2_LoadData(dataLoan);


        }
        private void Mldspg2_SaveData() 
        {
            Guid loanID = GetGuid("loanid");
            
            CPageData dataLoan = CreatePageData(loanID);
            dataLoan.InitSave(sFileVersion);

            Mldspg2_BindData(dataLoan, true);

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            Mldspg2_LoadData(dataLoan);
        }
        private void Mldspg2_BindData(CPageData dataLoan, bool isBindAll) 
        {
            if (isBindAll) 
            {
                
                IPreparerFields gfeTil = dataLoan.GetPreparerOfForm( E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew );
                gfeTil.PreparerName = GetString("GfeTilPreparerName");
                gfeTil.LicenseNumOfAgent = GetString("GfeTilLicenseNumOfAgent");
                gfeTil.CompanyName = GetString("GfeTilCompanyName");
                gfeTil.StreetAddr = GetString("GfeTilStreetAddr");
                gfeTil.City = GetString("GfeTilCity");
                gfeTil.State = GetString("GfeTilState");
                gfeTil.Zip = GetString("GfeTilZip");
                gfeTil.LicenseNumOfCompany = GetString("GfeTilLicenseNumOfCompany");
                gfeTil.AgentRoleT = (E_AgentRoleT)GetInt("CFM_AgentRoleT");
                gfeTil.IsLocked = GetBool("CFM_IsLocked");
                gfeTil.Update();
            }

            dataLoan.sBalloonPmt = GetBool("sBalloonPmt");
            dataLoan.sBrokControlledFundT = (E_sBrokControlledFundT) GetInt("sBrokControlledFundT");
            dataLoan.sDisabilityIns_rep = GetString("sDisabilityIns");
            dataLoan.sDue_rep = GetString("sDue");
            dataLoan.sFinMethT = (E_sFinMethT) GetInt("sFinMethT");
            dataLoan.sLien1AmtAfter_rep = GetString("sLien1AmtAfter");
            dataLoan.sLien1AmtBefore_rep = GetString("sLien1AmtBefore");
            dataLoan.sLien1PriorityAfter = GetString("sLien1PriorityAfter");
            dataLoan.sLien1PriorityBefore = GetString("sLien1PriorityBefore");
            dataLoan.sLien2AmtAfter_rep = GetString("sLien2AmtAfter");
            dataLoan.sLien2AmtBefore_rep = GetString("sLien2AmtBefore");
            dataLoan.sLien2PriorityAfter = GetString("sLien2PriorityAfter");
            dataLoan.sLien2PriorityBefore = GetString("sLien2PriorityBefore");
            dataLoan.sLien3AmtAfter_rep = GetString("sLien3AmtAfter");
            dataLoan.sLien3AmtBefore_rep = GetString("sLien3AmtBefore");
            dataLoan.sLien3PriorityAfter = GetString("sLien3PriorityAfter");
            dataLoan.sLien3PriorityBefore = GetString("sLien3PriorityBefore");
            dataLoan.sLienholder1NmAfter = GetString("sLienholder1NmAfter");
            dataLoan.sLienholder1NmBefore = GetString("sLienholder1NmBefore");
            dataLoan.sLienholder2NmAfter = GetString("sLienholder2NmAfter");
            dataLoan.sLienholder2NmBefore = GetString("sLienholder2NmBefore");
            dataLoan.sLienholder3NmAfter = GetString("sLienholder3NmAfter");
            dataLoan.sLienholder3NmBefore = GetString("sLienholder3NmBefore");
            dataLoan.sMldsPpmtBaseT = (E_sMldsPpmtBaseT) GetInt("sMldsPpmtBaseT");
            dataLoan.sMldsPpmtMonMax_rep = GetString("sMldsPpmtMonMax");
            dataLoan.sMldsPpmtT = (E_sMldsPpmtT) GetInt("sMldsPpmtT");
            dataLoan.sNoteIR_rep = GetString("sNoteIR");
            dataLoan.sPurchPrice_rep = GetString("sPurchPrice");
            dataLoan.sTerm_rep = GetString("sTerm");
            dataLoan.sU1FntcDesc = GetString("sU1FntcDesc");
            dataLoan.sU1Fntc_rep = GetString("sU1Fntc");
            dataLoan.sMldsIsNoDocTri = GetTriState("sMldsIsNoDocTri");
            dataLoan.sMldsLateChargeTri = GetTriState("sMldsLateChargeTri");

            dataLoan.sMldsImpoundOtherDesc = GetString("sMldsImpoundOtherDesc");
            dataLoan.sMldsImpoundIncludeOther = GetBool("sMldsImpoundIncludeOther");
            dataLoan.sMldsImpoundIncludeFloodIns = GetBool("sMldsImpoundIncludeFloodIns");
            dataLoan.sMldsImpoundIncludeMIns = GetBool("sMldsImpoundIncludeMIns");
            dataLoan.sMldsImpoundIncludeRealETx = GetBool("sMldsImpoundIncludeRealETx");
            dataLoan.sMldsImpoundIncludeHazIns = GetBool("sMldsImpoundIncludeHazIns");
            dataLoan.sMldsMonthlyImpoundPmt_rep = GetString("sMldsMonthlyImpoundPmt");
            dataLoan.sMldsHasImpound = GetString("sMldsHasImpound") ==  "1";

            dataLoan.sMldsPpmtPeriod_rep = GetString("sMldsPpmtPeriod");
            dataLoan.sMldsPpmtOtherDetail = GetString("sMldsPpmtOtherDetail");
            dataLoan.sMldsPpmtMaxAmt_rep = GetString("sMldsPpmtMaxAmt");
        }
        private void Mldspg2_LoadData(CPageData dataLoan) 
        {
            SetResult("sBalloonPmt",             dataLoan.sBalloonPmt);
            SetResult("sBrokControlledFundT",    dataLoan.sBrokControlledFundT);
            SetResult("sDisabilityIns",          dataLoan.sDisabilityIns_rep);
            SetResult("sDue",                    dataLoan.sDue_rep);
            SetResult("sFinMethT",               dataLoan.sFinMethT);
            SetResult("sFinalBalloonPmt",        dataLoan.sFinalBalloonPmt_rep);
            SetResult("sFinalBalloonPmtDueD",    dataLoan.sFinalBalloonPmtDueD_rep);
            SetResult("sFinalLAmt",              dataLoan.sFinalLAmt_rep);
            SetResult("sLien1AmtAfter",          dataLoan.sLien1AmtAfter_rep);
            SetResult("sLien1AmtBefore",         dataLoan.sLien1AmtBefore_rep);
            SetResult("sLien1PriorityAfter",     dataLoan.sLien1PriorityAfter);
            SetResult("sLien1PriorityBefore",    dataLoan.sLien1PriorityBefore);
            SetResult("sLien2AmtAfter",          dataLoan.sLien2AmtAfter_rep);
            SetResult("sLien2AmtBefore",         dataLoan.sLien2AmtBefore_rep);
            SetResult("sLien2PriorityAfter",     dataLoan.sLien2PriorityAfter);
            SetResult("sLien2PriorityBefore",    dataLoan.sLien2PriorityBefore);
            SetResult("sLien3AmtAfter",          dataLoan.sLien3AmtAfter_rep);
            SetResult("sLien3AmtBefore",         dataLoan.sLien3AmtBefore_rep);
            SetResult("sLien3PriorityAfter",     dataLoan.sLien3PriorityAfter);
            SetResult("sLien3PriorityBefore",    dataLoan.sLien3PriorityBefore);
            SetResult("sLienholder1NmAfter",     dataLoan.sLienholder1NmAfter);
            SetResult("sLienholder1NmBefore",    dataLoan.sLienholder1NmBefore);
            SetResult("sLienholder2NmAfter",     dataLoan.sLienholder2NmAfter);
            SetResult("sLienholder2NmBefore",    dataLoan.sLienholder2NmBefore);
            SetResult("sLienholder3NmAfter",     dataLoan.sLienholder3NmAfter);
            SetResult("sLienholder3NmBefore",    dataLoan.sLienholder3NmBefore);
            SetResult("sMldsPpmtBaseT",          dataLoan.sMldsPpmtBaseT);
            SetResult("sMldsPpmtMonMax",         dataLoan.sMldsPpmtMonMax_rep);
            SetResult("sMldsPpmtT",              dataLoan.sMldsPpmtT);
            SetResult("sNoteIR",                 dataLoan.sNoteIR_rep);
            SetResult("sProThisMPmt",            dataLoan.sProThisMPmt_rep);
            SetResult("sPurchPrice",             dataLoan.sPurchPrice_rep);
            SetResult("sRefPdOffAmtCamlds",      dataLoan.sRefPdOffAmtCamlds_rep);
            SetResult("sTerm",                   dataLoan.sTerm_rep);
            SetResult("sTotCcPboPbs",            "(" + dataLoan.sTotCcPboPbs_rep + ")");
            SetResult("sTotDeductFromFinalLAmt", dataLoan.sTotDeductFromFinalLAmt_rep);
            SetResult("sTotEstFntcCamlds",             dataLoan.sTotEstFntcCamlds_rep);
            SetResult("sTotEstScMlds",               dataLoan.sTotEstScMlds_rep);
            SetResult("sU1Fntc",                 dataLoan.sU1Fntc_rep);
            SetResult("sU1FntcDesc",             dataLoan.sU1FntcDesc);
            SetResult("sMldsIsNoDocTri", dataLoan.sMldsIsNoDocTri);
            SetResult("sMldsLateChargeTri", dataLoan.sMldsLateChargeTri);

            SetResult("sMldsImpoundOtherDesc", dataLoan.sMldsImpoundOtherDesc);
            SetResult("sMldsImpoundIncludeOther", dataLoan.sMldsImpoundIncludeOther);
            SetResult("sMldsImpoundIncludeFloodIns", dataLoan.sMldsImpoundIncludeFloodIns);
            SetResult("sMldsImpoundIncludeMIns", dataLoan.sMldsImpoundIncludeMIns);
            SetResult("sMldsImpoundIncludeRealETx", dataLoan.sMldsImpoundIncludeRealETx);
            SetResult("sMldsImpoundIncludeHazIns", dataLoan.sMldsImpoundIncludeHazIns);
            SetResult("sMldsMonthlyImpoundPmt", dataLoan.sMldsMonthlyImpoundPmt_rep);

            SetResult("sMldsHasImpound", dataLoan.sMldsHasImpound ? "1" : "0");
            SetResult("sMldsPpmtPeriod", dataLoan.sMldsPpmtPeriod_rep);
            SetResult("sMldsPpmtOtherDetail", dataLoan.sMldsPpmtOtherDetail);
            SetResult("sMldsPpmtMaxAmt", dataLoan.sMldsPpmtMaxAmt_rep);
        }
        #endregion

        #region MLDS Page 1
        private void Mldspg1_CalculateData() 
        {
            Guid loanID = GetGuid("loanid");
            CPageData dataLoan = CreatePageData(loanID);
            dataLoan.InitLoad();

            Mldspg1_BindData(dataLoan, false);

            Mldspg1_LoadData(dataLoan);
        }
        private void Mldspg1_SaveData() 
        {
            Guid loanID = GetGuid("loanid");
            bool byPassBgCalcForGfeAsDefault = GetString("ByPassBgCalcForGfeAsDefault", "0") == "1";

            CPageData dataLoan = CreatePageData(loanID);
            dataLoan.InitSave(sFileVersion);
            Mldspg1_BindData(dataLoan, true);
            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            Mldspg1_LoadData(dataLoan);

            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            SqlParameter[] parameters = {
                                            new SqlParameter("@ByPassBgCalcForGfeAsDefault", byPassBgCalcForGfeAsDefault)
                                            , new SqlParameter("@UserId", principal.UserId)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "SetPassBgCalcForGfeAsDefaultByUserId", 3, parameters);

        }
        private void Mldspg1_LoadData(CPageData dataLoan) 
        {
            SetResult("s1006ProHExp",       dataLoan.s1006ProHExp_rep);
            SetResult("s1006Rsrv",          dataLoan.s1006Rsrv_rep);
            SetResult("s1006RsrvMon",       dataLoan.s1006RsrvMon_rep);
            SetResult("s1007ProHExp",       dataLoan.s1007ProHExp_rep);
            SetResult("s1007Rsrv",          dataLoan.s1007Rsrv_rep);
            SetResult("s1007RsrvMon",       dataLoan.s1007RsrvMon_rep);
            SetResult("sProU3Rsrv",         dataLoan.sProU3Rsrv_rep);
            SetResult("sU3Rsrv",            dataLoan.sU3Rsrv_rep);
            SetResult("sU3RsrvMon",         dataLoan.sU3RsrvMon_rep);
            SetResult("sProU4Rsrv",         dataLoan.sProU4Rsrv_rep);
            SetResult("sU4Rsrv",            dataLoan.sU4Rsrv_rep);
            SetResult("sU4RsrvMon",         dataLoan.sU4RsrvMon_rep);
            SetResult("s900U1Pia",          dataLoan.s900U1Pia_rep);
            SetResult("s904Pia",            dataLoan.s904Pia_rep);
            SetResult("sAggregateAdjRsrv",  dataLoan.sAggregateAdjRsrv_rep);
            SetResult("sAttorneyF",         dataLoan.sAttorneyF_rep);
            SetResult("sBrokComp1Mlds",     dataLoan.sBrokComp1Mlds_rep);
            SetResult("sBrokComp1MldsLckd", dataLoan.sBrokComp1MldsLckd);
            SetResult("sBrokComp2Mlds",     dataLoan.sBrokComp2Mlds_rep);
            SetResult("sBrokComp2MldsLckd", dataLoan.sBrokComp2MldsLckd);
            SetResult("sCountyRtc",         dataLoan.sCountyRtc_rep);
            SetResult("sDaysInYr",          dataLoan.sDaysInYr_rep);
            SetResult("sDocPrepF",          dataLoan.sDocPrepF_rep);
            SetResult("sEscrowF",           dataLoan.sEscrowF_rep);
            SetResult("sFinalLAmt",         dataLoan.sFinalLAmt_rep);
            SetResult("sFloodInsRsrv",      dataLoan.sFloodInsRsrv_rep);
            SetResult("sFloodInsRsrvMon",   dataLoan.sFloodInsRsrvMon_rep);
            SetResult("sHazInsPia",         dataLoan.sHazInsPia_rep);
            SetResult("sHazInsPiaMon",      dataLoan.sHazInsPiaMon_rep);
            SetResult("sHazInsRsrv",        dataLoan.sHazInsRsrv_rep);
            SetResult("sHazInsRsrvMon",     dataLoan.sHazInsRsrvMon_rep);
            SetResult("sIPerDay",           dataLoan.sIPerDay_rep);
            SetResult("sIPia",              dataLoan.sIPia_rep);
            SetResult("sIPiaDy",            dataLoan.sIPiaDy_rep);
            SetResult("sInspectF",          dataLoan.sInspectF_rep);
            SetResult("sLDiscnt",           dataLoan.sLDiscnt_rep);
            SetResult("sLOrigF",            dataLoan.sLOrigF_rep);
            SetResult("sMBrokF",            dataLoan.sMBrokF_rep);
            SetResult("sMInsRsrv",          dataLoan.sMInsRsrv_rep);
            SetResult("sMInsRsrvMon",       dataLoan.sMInsRsrvMon_rep);
            SetResult("sNotaryF",           dataLoan.sNotaryF_rep);
            SetResult("sPestInspectF",      dataLoan.sPestInspectF_rep);
            SetResult("sProFloodIns",       dataLoan.sProFloodIns_rep);
            SetResult("sProHazIns",         dataLoan.sProHazIns_rep);
            SetResult("sProHazInsMb",       dataLoan.sProHazInsMb_rep);
            SetResult("sProHazInsR",        dataLoan.sProHazInsR_rep);
            SetResult("sProHazInsT",        dataLoan.sProHazInsT.ToString("D"));
            SetResult("sProMIns",           dataLoan.sProMIns_rep);
            SetResult("sProMInsMb",         dataLoan.sProMInsMb_rep);
            SetResult("sProMInsR",          dataLoan.sProMInsR_rep);
            SetResult("sProMInsT",          dataLoan.sProMInsT.ToString("D"));
            SetResult("sProRealETx",        dataLoan.sProRealETx_rep);
            SetResult("sProRealETxBaseAmt", dataLoan.sProRealETxBaseAmt_rep);
            SetResult("sProSchoolTx",       dataLoan.sProSchoolTx_rep);
            SetResult("sRealETxRsrv",       dataLoan.sRealETxRsrv_rep);
            SetResult("sRealETxRsrvMon",    dataLoan.sRealETxRsrvMon_rep);
            SetResult("sRecF",              dataLoan.sRecF_rep);
            SetResult("sSchoolTxRsrv",      dataLoan.sSchoolTxRsrv_rep);
            SetResult("sSchoolTxRsrvMon",   dataLoan.sSchoolTxRsrvMon_rep);
            SetResult("sStateRtc",          dataLoan.sStateRtc_rep);
            SetResult("sTitleInsF",         dataLoan.sTitleInsF_rep);
            SetResult("sTotEstScMlds",          dataLoan.sTotEstScMlds_rep);
            SetResult("sU1GovRtc",          dataLoan.sU1GovRtc_rep);
            SetResult("sU2GovRtc",          dataLoan.sU2GovRtc_rep);
            SetResult("sU3GovRtc",          dataLoan.sU3GovRtc_rep);
            SetResult("sVaFf",              dataLoan.sVaFf_rep);
            SetResult("sBrokComp1Pc",       dataLoan.sBrokComp1Pc_rep);
            SetResult("sTotCcPto",          dataLoan.sTotCcPto_rep);
            SetResult("sTotCcPtb",          dataLoan.sTotCcPtb_rep);
            SetResult("sSchedDueD1Lckd", dataLoan.sSchedDueD1Lckd);
            SetResult("sSchedDueD1", dataLoan.sSchedDueD1_rep);
            SetResult("sEstCloseDLckd", dataLoan.sEstCloseDLckd);
            SetResult("sEstCloseD", dataLoan.sEstCloseD_rep);
            SetResult("sConsummationD", dataLoan.sConsummationD_rep);
            SetResult("sConsummationDLckd", dataLoan.sConsummationDLckd);
            SetResult("sIPiaDyLckd", dataLoan.sIPiaDyLckd);
            SetResult("sMBrokFPc", dataLoan.sMBrokFPc_rep);
            SetResult("sMBrokFBaseT", dataLoan.sMBrokFBaseT);
            SetResult("sLDiscntPc", dataLoan.sLDiscntPc_rep);
            SetResult("sLDiscntBaseT", dataLoan.sLDiscntBaseT);
            SetResult("sGfeOriginatorCompF", dataLoan.sGfeOriginatorCompF_rep);
            SetResult("sOriginatorCompensationPaymentSourceT", dataLoan.sOriginatorCompensationPaymentSourceT);

            SetResult("sGfeOriginatorCompFPc", dataLoan.sGfeOriginatorCompFPc_rep);
            SetResult("sGfeOriginatorCompFMb", dataLoan.sGfeOriginatorCompFMb_rep);
            SetResult("sGfeOriginatorCompFBaseT", dataLoan.sGfeOriginatorCompFBaseT);
        }
        private void Mldspg1_BindData(CPageData dataLoan, bool isBindAll) 
        {
            if (isBindAll) 
            {
                CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.CreateNew);
                agent.CompanyName = GetString("sAgentLenderCompanyName");
                agent.Update();

                IPreparerFields gfeTil = dataLoan.GetPreparerOfForm( E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew );
                gfeTil.PrepareDate_rep = GetString("GfeTilPrepareDate");
                gfeTil.Update();

                dataLoan.s1006ProHExpDesc   = GetString("s1006ProHExpDesc");
                dataLoan.s1007ProHExpDesc   = GetString("s1007ProHExpDesc");
                if (Broker.EnableAdditionalSection1000CustomFees)
                {
                    dataLoan.sU3RsrvDesc    = GetString("sU3RsrvDesc");
                    dataLoan.sU4RsrvDesc    = GetString("sU4RsrvDesc");
                }
                dataLoan.s800U1FCode        = GetString("s800U1FCode");
                dataLoan.s800U1FDesc        = GetString("s800U1FDesc");
                dataLoan.s800U2FCode        = GetString("s800U2FCode");
                dataLoan.s800U2FDesc        = GetString("s800U2FDesc");
                dataLoan.s800U3FCode        = GetString("s800U3FCode");
                dataLoan.s800U3FDesc        = GetString("s800U3FDesc");
                dataLoan.s800U4FCode        = GetString("s800U4FCode");
                dataLoan.s800U4FDesc        = GetString("s800U4FDesc");
                dataLoan.s800U5FCode        = GetString("s800U5FCode");
                dataLoan.s800U5FDesc        = GetString("s800U5FDesc");
                dataLoan.s900U1PiaCode      = GetString("s900U1PiaCode");
                dataLoan.s900U1PiaDesc      = GetString("s900U1PiaDesc");
                dataLoan.s904PiaDesc        = GetString("s904PiaDesc");
                dataLoan.sCcTemplateNm      = GetString("sCcTemplateNm");
                dataLoan.sCountyRtcDesc     = GetString("sCountyRtcDesc");
                dataLoan.sLpTemplateNm      = GetString("sLpTemplateNm");
                dataLoan.sStateRtcDesc      = GetString("sStateRtcDesc");
                dataLoan.sU1GovRtcCode      = GetString("sU1GovRtcCode");
                dataLoan.sU1GovRtcDesc      = GetString("sU1GovRtcDesc");
                dataLoan.sU1ScCode          = GetString("sU1ScCode");
                dataLoan.sU1ScDesc          = GetString("sU1ScDesc");
                dataLoan.sU1TcCode          = GetString("sU1TcCode");
                dataLoan.sU1TcDesc          = GetString("sU1TcDesc");
                dataLoan.sU2GovRtcCode      = GetString("sU2GovRtcCode");
                dataLoan.sU2GovRtcDesc      = GetString("sU2GovRtcDesc");
                dataLoan.sU2ScCode          = GetString("sU2ScCode");
                dataLoan.sU2ScDesc          = GetString("sU2ScDesc");
                dataLoan.sU2TcCode          = GetString("sU2TcCode");
                dataLoan.sU2TcDesc          = GetString("sU2TcDesc");
                dataLoan.sU3GovRtcCode      = GetString("sU3GovRtcCode");
                dataLoan.sU3GovRtcDesc      = GetString("sU3GovRtcDesc");
                dataLoan.sU3ScCode          = GetString("sU3ScCode");
                dataLoan.sU3ScDesc          = GetString("sU3ScDesc");
                dataLoan.sU3TcCode          = GetString("sU3TcCode");
                dataLoan.sU3TcDesc          = GetString("sU3TcDesc");
                dataLoan.sU4ScCode          = GetString("sU4ScCode");
                dataLoan.sU4ScDesc          = GetString("sU4ScDesc");
                dataLoan.sU4TcCode          = GetString("sU4TcCode");
                dataLoan.sU4TcDesc          = GetString("sU4TcDesc");
                dataLoan.sU5ScCode          = GetString("sU5ScCode");
                dataLoan.sU5ScDesc          = GetString("sU5ScDesc");

            }

            dataLoan.s1006ProHExp_rep      = GetString("s1006ProHExp");
            dataLoan.s1006RsrvMon_rep      = GetString("s1006RsrvMon");
            dataLoan.s1007ProHExp_rep      = GetString("s1007ProHExp");
            dataLoan.s1007RsrvMon_rep      = GetString("s1007RsrvMon");
            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                dataLoan.sProU3Rsrv_rep    = GetString("sProU3Rsrv");
                dataLoan.sU3RsrvMon_rep    = GetString("sU3RsrvMon");
                dataLoan.sProU4Rsrv_rep    = GetString("sProU4Rsrv");
                dataLoan.sU4RsrvMon_rep    = GetString("sU4RsrvMon");
            }
            dataLoan.s800U1F_rep           = GetString("s800U1F");
            dataLoan.s800U2F_rep           = GetString("s800U2F");
            dataLoan.s800U3F_rep           = GetString("s800U3F");
            dataLoan.s800U4F_rep           = GetString("s800U4F");
            dataLoan.s800U5F_rep           = GetString("s800U5F");
            dataLoan.s900U1Pia_rep         = GetString("s900U1Pia");
            dataLoan.s904Pia_rep           = GetString("s904Pia");
            dataLoan.sAggregateAdjRsrv_rep = GetString("sAggregateAdjRsrv");
            dataLoan.sApprFPaid            = GetBool("sApprFPaid");
            dataLoan.sApprF_rep            = GetString("sApprF");
            dataLoan.sAttorneyF_rep        = GetString("sAttorneyF");
            dataLoan.sCountyRtcBaseT = (E_PercentBaseT)GetInt("sCountyRtcBaseT");
            dataLoan.sCountyRtcMb_rep      = GetString("sCountyRtcMb");
            dataLoan.sCountyRtcPc_rep      = GetString("sCountyRtcPc");
            dataLoan.sCrFPaid              = GetBool("sCrFPaid");
            dataLoan.sCrF_rep              = GetString("sCrF");
            dataLoan.sDaysInYr_rep         = GetString("sDaysInYr");
            dataLoan.sDocPrepF_rep         = GetString("sDocPrepF");
            dataLoan.sDue_rep              = GetString("sDue");
            dataLoan.sEscrowFTable         = GetString("sEscrowFTable");
            dataLoan.sEscrowF_rep          = GetString("sEscrowF");
            dataLoan.sEstCloseD_rep        = GetString("sEstCloseD");
            dataLoan.sFloodInsRsrvMon_rep  = GetString("sFloodInsRsrvMon");
            dataLoan.sHazInsPiaMon_rep     = GetString("sHazInsPiaMon");
            dataLoan.sHazInsRsrvMon_rep    = GetString("sHazInsRsrvMon");
            dataLoan.sIPiaDy_rep           = GetString("sIPiaDy");
            dataLoan.sConsummationD_rep    = GetString("sConsummationD");
            dataLoan.sInspectF_rep         = GetString("sInspectF");
            dataLoan.sLDiscntFMb_rep       = GetString("sLDiscntFMb");
            dataLoan.sLDiscntPc_rep        = GetString("sLDiscntPc");
            dataLoan.sLDiscntBaseT         = (E_PercentBaseT)GetInt("sLDiscntBaseT");
            dataLoan.sLOrigFMb_rep         = GetString("sLOrigFMb");
            dataLoan.sLOrigFPc_rep         = GetString("sLOrigFPc");
            dataLoan.sMBrokFMb_rep         = GetString("sMBrokFMb");
            dataLoan.sMBrokFPc_rep         = GetString("sMBrokFPc");
            dataLoan.sMBrokFBaseT          = (E_PercentBaseT)GetInt("sMBrokFBaseT");
            dataLoan.sMInsRsrvMon_rep      = GetString("sMInsRsrvMon");
            dataLoan.sNotaryF_rep          = GetString("sNotaryF");
            dataLoan.sNoteIR_rep           = GetString("sNoteIR");
            dataLoan.sPestInspectF_rep     = GetString("sPestInspectF");
            dataLoan.sProFloodIns_rep      = GetString("sProFloodIns");
            if (!dataLoan.sHazardExpenseInDisbursementMode)
            {
                dataLoan.sProHazInsMb_rep = GetString("sProHazInsMb");
                dataLoan.sProHazInsR_rep = GetString("sProHazInsR");
                dataLoan.sProHazInsT = (E_PercentBaseT)GetInt("sProHazInsT");
            }
            if (!dataLoan.sRealEstateTaxExpenseInDisbMode)
            {
                dataLoan.sProRealETxMb_rep = GetString("sProRealETxMb");
                dataLoan.sProRealETxR_rep = GetString("sProRealETxR");
                dataLoan.sProRealETxT = (E_PercentBaseT)GetInt("sProRealETxT");
            }
            dataLoan.sProSchoolTx_rep      = GetString("sProSchoolTx");
            dataLoan.sProcFPaid            = GetBool("sProcFPaid");
            dataLoan.sProcF_rep            = GetString("sProcF");
            dataLoan.sRealETxRsrvMon_rep   = GetString("sRealETxRsrvMon");
            dataLoan.sRecBaseT = (E_PercentBaseT)GetInt("sRecBaseT");
            dataLoan.sRecFDesc             = GetString("sRecFDesc");
            dataLoan.sRecFMb_rep           = GetString("sRecFMb");
            dataLoan.sRecFPc_rep           = GetString("sRecFPc");
            dataLoan.sSchedDueD1_rep       = GetString("sSchedDueD1");
            dataLoan.sSchoolTxRsrvMon_rep  = GetString("sSchoolTxRsrvMon");
            dataLoan.sStateRtcBaseT = (E_PercentBaseT)GetInt("sStateRtcBaseT");
            dataLoan.sStateRtcMb_rep       = GetString("sStateRtcMb");
            dataLoan.sStateRtcPc_rep       = GetString("sStateRtcPc");
            dataLoan.sTerm_rep             = GetString("sTerm");
            dataLoan.sTitleInsFTable       = GetString("sTitleInsFTable");
            dataLoan.sTitleInsF_rep        = GetString("sTitleInsF");
            dataLoan.sTxServF_rep          = GetString("sTxServF");
            dataLoan.sU1GovRtcBaseT = (E_PercentBaseT)GetInt("sU1GovRtcBaseT");
            dataLoan.sU1GovRtcMb_rep       = GetString("sU1GovRtcMb");
            dataLoan.sU1GovRtcPc_rep       = GetString("sU1GovRtcPc");
            dataLoan.sU1Sc_rep             = GetString("sU1Sc");
            dataLoan.sU1Tc_rep             = GetString("sU1Tc");
            dataLoan.sU2GovRtcBaseT = (E_PercentBaseT)GetInt("sU2GovRtcBaseT");
            dataLoan.sU2GovRtcMb_rep       = GetString("sU2GovRtcMb");
            dataLoan.sU2GovRtcPc_rep       = GetString("sU2GovRtcPc");
            dataLoan.sU2Sc_rep             = GetString("sU2Sc");
            dataLoan.sU2Tc_rep             = GetString("sU2Tc");
            dataLoan.sU3GovRtcBaseT = (E_PercentBaseT)GetInt("sU3GovRtcBaseT");
            dataLoan.sU3GovRtcMb_rep       = GetString("sU3GovRtcMb");
            dataLoan.sU3GovRtcPc_rep       = GetString("sU3GovRtcPc");
            dataLoan.sU3Sc_rep             = GetString("sU3Sc");
            dataLoan.sU3Tc_rep             = GetString("sU3Tc");
            dataLoan.sU4Sc_rep             = GetString("sU4Sc");
            dataLoan.sU4Tc_rep             = GetString("sU4Tc");
            dataLoan.sU5Sc_rep             = GetString("sU5Sc");
            dataLoan.sUwF_rep              = GetString("sUwF");
            dataLoan.sWireF_rep            = GetString("sWireF");
            dataLoan.sBrokComp1Pc_rep      = GetString("sBrokComp1Pc");
            dataLoan.sBrokComp1MldsLckd    = GetBool("sBrokComp1MldsLckd");
            dataLoan.sBrokComp1Mlds_rep    = GetString("sBrokComp1Mlds");

            dataLoan.sBrokComp2Mlds_rep = GetString("sBrokComp2Mlds");
            dataLoan.sBrokComp2MldsLckd = GetBool("sBrokComp2MldsLckd");


            dataLoan.s1006RsrvProps         = RetrieveItemProps( dataLoan.m_convertLos,  "s1006RsrvProps" );
            dataLoan.s1007RsrvProps         = RetrieveItemProps( dataLoan.m_convertLos,  "s1007RsrvProps" );
            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                dataLoan.sU3RsrvProps       = RetrieveItemProps( dataLoan.m_convertLos,  "sU3RsrvProps" );
                dataLoan.sU4RsrvProps       = RetrieveItemProps( dataLoan.m_convertLos,  "sU4RsrvProps" );
            }
            dataLoan.s800U1FProps           = RetrieveItemProps( dataLoan.m_convertLos,  "s800U1FProps" );
            dataLoan.s800U2FProps           = RetrieveItemProps( dataLoan.m_convertLos,  "s800U2FProps" );
            dataLoan.s800U3FProps           = RetrieveItemProps( dataLoan.m_convertLos,  "s800U3FProps" );	
            dataLoan.s800U4FProps           = RetrieveItemProps( dataLoan.m_convertLos,  "s800U4FProps" );
            dataLoan.s800U5FProps           = RetrieveItemProps( dataLoan.m_convertLos,  "s800U5FProps" );
            dataLoan.s900U1PiaProps         = RetrieveItemProps( dataLoan.m_convertLos,  "s900U1PiaProps" );
            dataLoan.s904PiaProps           = RetrieveItemProps( dataLoan.m_convertLos,  "s904PiaProps" );
            dataLoan.sAggregateAdjRsrvProps = RetrieveItemProps( dataLoan.m_convertLos,  "sAggregateAdjRsrvProps" );
            dataLoan.sApprFProps            = RetrieveItemProps( dataLoan.m_convertLos,  "sApprFProps" );
            dataLoan.sAttorneyFProps        = RetrieveItemProps( dataLoan.m_convertLos,  "sAttorneyFProps" );
            dataLoan.sCountyRtcProps        = RetrieveItemProps( dataLoan.m_convertLos,  "sCountyRtcProps" );
            dataLoan.sCrFProps              = RetrieveItemProps( dataLoan.m_convertLos,  "sCrFProps" );
            dataLoan.sDocPrepFProps         = RetrieveItemProps( dataLoan.m_convertLos,  "sDocPrepFProps" );
            dataLoan.sEscrowFProps          = RetrieveItemProps( dataLoan.m_convertLos,  "sEscrowFProps" );
            dataLoan.sFloodInsRsrvProps     = RetrieveItemProps( dataLoan.m_convertLos,  "sFloodInsRsrvProps" );
            dataLoan.sHazInsPiaProps        = RetrieveItemProps( dataLoan.m_convertLos,  "sHazInsPiaProps" );
            dataLoan.sHazInsRsrvProps       = RetrieveItemProps( dataLoan.m_convertLos,  "sHazInsRsrvProps" );
            dataLoan.sIPiaProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sIPiaProps" );
            dataLoan.sInspectFProps         = RetrieveItemProps( dataLoan.m_convertLos,  "sInspectFProps" );
            dataLoan.sLDiscntProps          = RetrieveItemProps( dataLoan.m_convertLos,  "sLDiscntProps" );
            dataLoan.sLOrigFProps           = RetrieveItemProps( dataLoan.m_convertLos,  "sLOrigFProps" );
            dataLoan.sMBrokFProps           = RetrieveItemProps( dataLoan.m_convertLos,  "sMBrokFProps" );
            dataLoan.sMInsRsrvProps         = RetrieveItemProps( dataLoan.m_convertLos,  "sMInsRsrvProps" );
            dataLoan.sMipPiaProps           = RetrieveItemProps( dataLoan.m_convertLos,  "sMipPiaProps" );
            dataLoan.sNotaryFProps          = RetrieveItemProps( dataLoan.m_convertLos,  "sNotaryFProps" );
            dataLoan.sPestInspectFProps     = RetrieveItemProps( dataLoan.m_convertLos,  "sPestInspectFProps" );
            dataLoan.sProcFProps            = RetrieveItemProps( dataLoan.m_convertLos,  "sProcFProps" );
            dataLoan.sRealETxRsrvProps      = RetrieveItemProps( dataLoan.m_convertLos,  "sRealETxRsrvProps" );
            dataLoan.sRecFProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sRecFProps" );
            dataLoan.sSchoolTxRsrvProps     = RetrieveItemProps( dataLoan.m_convertLos,  "sSchoolTxRsrvProps" );
            dataLoan.sStateRtcProps         = RetrieveItemProps( dataLoan.m_convertLos,  "sStateRtcProps" );
            dataLoan.sTitleInsFProps        = RetrieveItemProps( dataLoan.m_convertLos,  "sTitleInsFProps" );
            dataLoan.sTxServFProps          = RetrieveItemProps( dataLoan.m_convertLos,  "sTxServFProps" );
            dataLoan.sU1GovRtcProps         = RetrieveItemProps( dataLoan.m_convertLos,  "sU1GovRtcProps" );
            dataLoan.sU1ScProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU1ScProps" );
            dataLoan.sU1TcProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU1TcProps" );
            dataLoan.sU2GovRtcProps         = RetrieveItemProps( dataLoan.m_convertLos,  "sU2GovRtcProps" );
            dataLoan.sU2ScProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU2ScProps" );
            dataLoan.sU2TcProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU2TcProps" );
            dataLoan.sU3GovRtcProps         = RetrieveItemProps( dataLoan.m_convertLos,  "sU3GovRtcProps" );
            dataLoan.sU3ScProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU3ScProps" );
            dataLoan.sU3TcProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU3TcProps" );
            dataLoan.sU4ScProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU4ScProps" );
            dataLoan.sU4TcProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU4TcProps" );
            dataLoan.sU5ScProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sU5ScProps" );
            dataLoan.sUwFProps              = RetrieveItemProps( dataLoan.m_convertLos,  "sUwFProps" );
            dataLoan.sVaFfProps             = RetrieveItemProps( dataLoan.m_convertLos,  "sVaFfProps" );
            dataLoan.sWireFProps            = RetrieveItemProps( dataLoan.m_convertLos,  "sWireFProps" );

            dataLoan.sIPiaDyLckd = GetBool("sIPiaDyLckd");
            dataLoan.sConsummationDLckd = GetBool("sConsummationDLckd");
            dataLoan.sSchedDueD1Lckd = GetBool("sSchedDueD1Lckd");
            dataLoan.sEstCloseDLckd = GetBool("sEstCloseDLckd");

            E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT = (E_sOriginatorCompensationPaymentSourceT)GetInt("sOriginatorCompensationPaymentSourceT");

            string sGfeOriginatorCompFPc_rep = GetString("sGfeOriginatorCompFPc");
            string sGfeOriginatorCompFMb_rep = GetString("sGfeOriginatorCompFMb");
            E_PercentBaseT sGfeOriginatorCompFBaseT = (E_PercentBaseT)GetInt("sGfeOriginatorCompFBaseT");

            dataLoan.SetOriginatorCompensation(sOriginatorCompensationPaymentSourceT, sGfeOriginatorCompFPc_rep, sGfeOriginatorCompFBaseT, sGfeOriginatorCompFMb_rep);

        }
        #endregion

        private int RetrieveItemProps( LosConvert convert, string name)
        {
            bool apr      = GetBool(name + "_ctrl_Apr_chk");
            bool toBr     = GetBool(name + "_ctrl_ToBrok_chk");
            int payer     = GetInt(name + "_ctrl_PdByT_dd");
			bool fhaAllow = GetBool(name + "_ctrl_Fha_chk");
            bool poc      = GetBool(name + "_ctrl_Poc_chk");
            return LosConvert.GfeItemProps_Pack(apr, toBr, payer, fhaAllow, poc );
        }

        private void InitItemProps( LosConvert convert, string name, int props )
        {
            SetResult(name + "_ctrl_Apr_chk", LosConvert.GfeItemProps_Apr(props));
            SetResult(name + "_ctrl_ToBrok_chk", LosConvert.GfeItemProps_ToBr(props));
            SetResult(name + "_ctrl_PdByT_dd", LosConvert.GfeItemProps_Payer(props));
			SetResult(name + "_ctrl_Fha_chk", LosConvert.GfeItemProps_FhaAllow(props));
            SetResult(name + "_ctrl_Poc_chk", LosConvert.GfeItemProps_Poc(props));

        }
	}
}
