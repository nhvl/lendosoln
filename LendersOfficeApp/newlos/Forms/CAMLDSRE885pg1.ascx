﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="CAMLDSRE885pg1.ascx.cs" Inherits="LendersOfficeApp.newlos.Forms.CAMLDSRE885pg1" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>
<%@ Register TagPrefix="uc1" TagName="GoodFaithEstimateRightColumn" Src="../../los/LegalForm/GoodFaithEstimateRightColumn.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<script type="text/javascript">
var bIsFirst = true;
var openedDate = <%= AspxTools.JsString(m_openedDate) %>;
function onDateKeyUp(o, event) {
  if (event.keyCode == 79) {
    o.value = openedDate;
    updateDirtyBit(event);
  }
}


function _initControl() {
  bIsAutoCalculate = document.forms[0][<%=AspxTools.JsString(ByPassBgCalcForGfeAsDefault.UniqueID)%>][0].checked;
  document.getElementById("btnCalculate").disabled = bIsAutoCalculate;

  if (bIsFirst) {
    // Only init one time.
    bIsFirst = false;
    var length = document.forms[0][<%=AspxTools.JsString(ByPassBgCalcForGfeAsDefault.UniqueID)%>].length;
    

        for (var i = 0; i < length; i++) {
            addEventHandler(document.forms[0][<%=AspxTools.JsString(ByPassBgCalcForGfeAsDefault.UniqueID)%>][i], "click", backgroundCalculation, false);
            }
      
    <%= AspxTools.JsGetElementById(GfeTilPrepareDate) %>.focus();    
  } 
  lockField(<%= AspxTools.JsGetElementById(sBrokComp1MldsLckd) %>, 'sBrokComp1Mlds');  
  lockField(<%= AspxTools.JsGetElementById(sBrokComp2MldsLckd) %>, 'sBrokComp2Mlds');
  lockField(<%= AspxTools.JsGetElementById(sIPiaDyLckd) %>, 'sIPiaDy');
  lockField(<%=AspxTools.JsGetElementById(sSchedDueD1Lckd)%>, 'sSchedDueD1');
  lockField(<%=AspxTools.JsGetElementById(sEstCloseDLckd)%>, 'sEstCloseD');
  lockField(<%= AspxTools.JsGetElementById(sConsummationDLckd) %>, 'sConsummationD');
  updateOriginatorCompensationUI();
}
  function findCCTemplate() {
    // Before choosing a closing cost template, you always need to save the loan.
    PolyShouldShowConfirmSave(isDirty(), function(){
		showModal(<%= AspxTools.JsString(ClosingCostPageUrl) %>, null, null, null, function(args){
			if (args.OK) {
			self.location = self.location; // Refresh;
			}
		},{ hideCloseButton: true });
	}, saveMe);
  }
  function getPaymentSource()
  {
    // THere are 3 options.
    for (var i = 0; i < 3; i++)
    {
      var o = document.getElementById(<%= AspxTools.JsString(ClientID) %> + '_sOriginatorCompensationPaymentSourceT_' + i);
      if (null != o && o.checked)
      {
        return o.value;
      }
    }
    return "";
  }
  function updateOriginatorCompensationUI()
  {
    <% if (IsReadOnly == false) { %>
    
    var paidBy = getPaymentSource(); // 0 - not specified, 1 - borrower, 2 - lender
    
    var sGfeOriginatorCompFPcDisabled = paidBy == 2 || (paidBy == 0 && <%= AspxTools.JsGetElementById(sGfeOriginatorCompFBaseT) %>.value == '6');
    
    <%= AspxTools.JsGetElementById(sGfeOriginatorCompFPc) %>.readOnly =  sGfeOriginatorCompFPcDisabled;
    disableDDL(<%= AspxTools.JsGetElementById(sGfeOriginatorCompFBaseT) %>, paidBy == 2);
    <%= AspxTools.JsGetElementById(sGfeOriginatorCompFMb) %>.readOnly = paidBy == 2;
    
    document.getElementById("of_label").style.visibility = <%= AspxTools.JsGetElementById(sGfeOriginatorCompFBaseT) %>.value == '6' ? 'hidden' : 'visible';
    if (paidBy === "0" || paidBy === "1")
    {
      var sGfeOriginatorCompFBaseT = <%= AspxTools.JsGetElementById(sGfeOriginatorCompFBaseT) %>;
      if (paidBy === "0")
      {
        var bHasAllYsp = false;
        for (var i = 0; i < sGfeOriginatorCompFBaseT.options.length; i++)
        {
          if (sGfeOriginatorCompFBaseT.options[i].value === '6')
          {
            bHasAllYsp = true;
            break;
          }
        }
        
        if (bHasAllYsp === false)
        {
          var opt = document.createElement("option");
          opt.text = "All YSP";
          opt.value = "6";
          sGfeOriginatorCompFBaseT.options.add(opt);
        }
      }
      else if (paidBy === "1")
      {
        for (var i = 0; i < sGfeOriginatorCompFBaseT.options.length; i++)
        {
          var opt = sGfeOriginatorCompFBaseT.options[i]
          if (opt.value === '6')
          {
            sGfeOriginatorCompFBaseT.options.remove(i);
            break;
          }
        }
      }
    }
    <% } %>
    }

    function doAfterDateFormat(e)
    {
        if(e.id.indexOf("sEstCloseD") !== -1)
        {
            refreshCalculation();
        }
    }
</script>
<table id="Table3" cellSpacing="0" cellPadding="0" border="0">
	<tr>
		<td>
			<table id="Table12" cellSpacing="0" cellPadding="0" border="0" width="100%">
        <TR>
          <TD class=FieldLabel noWrap colSpan=6>Perform calculations:&nbsp;<asp:RadioButtonList id=ByPassBgCalcForGfeAsDefault runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem Value="0">automatically</asp:ListItem>
<asp:ListItem Value="1">manually</asp:ListItem>
</asp:RadioButtonList>   
            <INPUT accessKey=c type=button value="Recalculate  (Alt + C)" name="btnCalculate" id="btnCalculate" tabindex=-1 onclick="backgroundCalculation();" disabled></TD></TR>			
				<tr style="PADDING-TOP:10px">
					<td class="FieldLabel">Prepared Date</td>
					<td class="FieldLabel" colSpan="5"><ml:DateTextBox id="GfeTilPrepareDate" runat="server" onkeyup="onDateKeyUp(this, event);" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
				</tr>
				<tr>
					<td class="FieldLabel">Total Loan Amt</td>
					<td><ml:MoneyTextBox id="sFinalLAmt" runat="server" preset="money" width="90" ReadOnly="True"></ml:MoneyTextBox></td>
					<td class="FieldLabel">Interest Rate</td>
					<td><ml:PercentTextBox id="sNoteIR" runat="server" preset="percent" width="70" onchange="refreshCalculation();"></ml:PercentTextBox></td>
					<td class="FieldLabel">
					    <label style="width: 115px;">1st Payment Date</label>
                        <asp:CheckBox runat="server" ID="sSchedDueD1Lckd" Text="Lock" onclick="lockField(this, 'sSchedDueD1'); refreshCalculation();"/>
					</td>
					<td><ml:DateTextBox id="sSchedDueD1" runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
				</tr>
				<tr>
					<td class="FieldLabel">Days in Year</td>
					<td><asp:textbox id="sDaysInYr" runat="server" Width="54px" MaxLength="4" onchange="refreshCalculation();"></asp:textbox></td>
					<td class="FieldLabel">Term / Due In
					</td>
					<td class="FieldLabel"><asp:textbox id="sTerm" runat="server" Width="35px" onchange="refreshCalculation();"></asp:textbox>&nbsp;/
						<asp:textbox id="sDue" runat="server" Width="38px" onchange="refreshCalculation();"></asp:textbox>&nbsp;mths</td>
					<td class="FieldLabel">
                        <label style="width: 115px;">Est. Close Date</label>
                        <asp:CheckBox runat="server" ID="sEstCloseDLckd" Text="Lock" onclick="lockField(this, 'sEstCloseD'); refreshCalculation();"/>
					</td>
					<td><ml:DateTextBox id="sEstCloseD" runat="server" width="75" preset="date" CssClass="mask" onchange="date_onblur(null, this);"></ml:DateTextBox></td>
				</tr>
				<TR>
					<TD class="FieldLabel" valign="top">Lender Name</TD>
					<TD colSpan="3"><asp:TextBox id="sAgentLenderCompanyName" runat="server" Width="287px"></asp:TextBox>&nbsp;
						<uc:CFM id="CFM" runat="server" />
					</TD>
					<td class="FieldLabel" valign="top">
					    <label style="width: 115px;">Consummation Date</label>
    					<asp:CheckBox runat="server" ID="sConsummationDLckd" Text="Lock" onclick="refreshCalculation();" />
					</td>
					<td valign="top"><ml:DateTextBox ID="sConsummationD" runat="server" ReadOnly="true" /></td>
				</TR>
				<TR>
					<TD class="FieldLabel">Loan Program</TD>
					<TD colSpan="5">
						<asp:TextBox id="sLpTemplateNm" Width="369px" runat="server"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD class="FieldLabel">CC Template</TD>
					<TD colSpan="5">
						<asp:TextBox id="sCcTemplateNm" Width="367px" runat="server"></asp:TextBox><input type=button value="Find Closing Cost Template ..." onclick="findCCTemplate();"></TD>
				</TR>
			</table>
		</td>
	</tr>
	<tr>
		<td></td>
	</tr>


	<tr>
		<td>
			<table id="Table1" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px" cellSpacing="0" cellPadding="0" width="100%" border="0">
										    <tr class="FormTableHeader">
									      <td></td>
									      <td colspan="11">Loan originator compensation source</td>
									    </tr>
									    <tr>
									      <td></td>
									      <td colspan="11" class="FieldLabel" style="padding-top:5px;padding-bottom:5px">Loan originator is paid by
									      <asp:RadioButtonList ID="sOriginatorCompensationPaymentSourceT" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" onclick="refreshCalculation();"/>
									      </td>
									    </tr>			
				<tr>
					<td class="FormTableHeader"></td>
					<td class="FormTableHeader"></td>
					<td class="FormTableHeader" colspan=5>B&nbsp;= Paid to broker&nbsp;&nbsp;&nbsp;A 
						= APR&nbsp;&nbsp;&nbsp;F = FHA Allowable&nbsp;&nbsp;&nbsp;POC = Paid Outside of Closing</td>
				</tr>
				<tr>
					<td class="FormTableHeader"></td>
					<td class="FormTableHeader">Description of Charge</td>
					<td class="FormTableHeader"></td>
					<td class="FormTableHeader">Amount</td>
					<td class="FormTableHeader">Paid By</td>
					<td class="FormTableHeader" style="WIDTH: 44px"></td>
					<td class="FormTableHeader"></td>
				</tr>
				<tr>
					<td class="FormTableSubheader">800</td>
					<td class="FormTableSubheader" colSpan="7">ITEMS PAYABLE IN CONNECTION WITH LOAN</td>
				</tr>
				<tr>
					<td class="FieldLabel">801</td>
					<td class="FieldLabel">Loan origination fee</td>
					<td><ml:PercentTextBox id="sLOrigFPc" runat="server" preset="percent" width="70" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:PercentTextBox> +
						<ml:MoneyTextBox id="sLOrigFMb" runat="server" preset="money" width="90" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td><ml:MoneyTextBox id="sLOrigF" runat="server" preset="money" width="82" ReadOnly="True"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sLOrigFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
										<tr>
										  <td></td>
										  <td class="FieldLabel">Originator compensation</td>
										  <td class="FieldLabel"><ml:PercentTextBox ID="sGfeOriginatorCompFPc" runat="server" preset="percent" Width="70" onchange="refreshCalculation();" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" />
										  <span id="of_label">of</span>
										  <asp:DropDownList ID="sGfeOriginatorCompFBaseT" runat="server" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" onchange="refreshCalculation();" />
										  +
										  <ml:MoneyTextBox ID="sGfeOriginatorCompFMb" runat="server" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" preset="money" Width="59" onchange="refreshCalculation();" />
										  </td>
										  <td><ml:MoneyTextBox ID="sGfeOriginatorCompF" runat="server" ReadOnly="true" Width="82"/></td>

										  
										</tr>				

				<tr>
					<td class="FieldLabel">802</td>
					<td class="FieldLabel">Loan discount</td>
					<td noWrap><ml:PercentTextBox id="sLDiscntPc" runat="server" preset="percent" width="70" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:PercentTextBox> +
						<ml:MoneyTextBox id="sLDiscntFMb" runat="server" preset="money" width="90" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td><ml:MoneyTextBox id="sLDiscnt" runat="server" preset="money" width="82px" ReadOnly="True"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sLDiscntProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">803</td>
					<td class="FieldLabel">Appraisal fee</td>
					<td align="right"><asp:checkbox id="sApprFPaid" runat="server" Width="49px" Text="Paid"></asp:checkbox>&nbsp;</td>
					<td><ml:MoneyTextBox id="sApprF" runat="server" preset="money" width="82px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sApprFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">804</td>
					<td class="FieldLabel">Credit report</td>
					<td align="right"><asp:checkbox id="sCrFPaid" runat="server" Width="49px" Text="Paid"></asp:checkbox>&nbsp;</td>
					<td><ml:MoneyTextBox id="sCrF" runat="server" preset="money" width="82px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sCrFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">805</td>
					<td class="FieldLabel">Lender's inspection fee</td>
					<td></td>
					<td><ml:MoneyTextBox id="sInspectF" runat="server" preset="money" width="82px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sInspectFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">808</td>
					<td class="FieldLabel">Mortgage broker fee</td>
					<td><ml:PercentTextBox id="sMBrokFPc" runat="server" preset="percent" width="70" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:PercentTextBox>&nbsp;+
						<ml:MoneyTextBox id="sMBrokFMb" runat="server" preset="money" width="90" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td><ml:MoneyTextBox id="sMBrokF" runat="server" preset="money" width="82px" ReadOnly="True"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sMBrokFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">809</td>
					<td class="FieldLabel">Tax service fee</td>
					<td></td>
					<td><ml:MoneyTextBox id="sTxServF" runat="server" preset="money" width="82px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sTxServFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">810</td>
					<td class="FieldLabel">Processing fee</td>
					<td align="right"><asp:checkbox id="sProcFPaid" runat="server" Width="49px" Text="Paid"></asp:checkbox>&nbsp;</td>
					<td><ml:MoneyTextBox id="sProcF" runat="server" preset="money" width="82px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sProcFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">811</td>
					<td class="FieldLabel">Underwriting fee</td>
					<td></td>
					<td><ml:MoneyTextBox id="sUwF" runat="server" preset="money" width="82px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sUwFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">812</td>
					<td class="FieldLabel">Wire transfer</td>
					<td></td>
					<td><ml:MoneyTextBox id="sWireF" runat="server" preset="money" width="82px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sWireFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td><asp:textbox id="s800U1FCode" runat="server" Width="34" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td colSpan="2"><asp:textbox id="s800U1FDesc" runat="server" Width="95%" MaxLength="100" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td><ml:MoneyTextBox id="s800U1F" runat="server" preset="money" width="82px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s800U1FProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td><asp:textbox id="s800U2FCode" runat="server" Width="34px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td colSpan="2"><asp:textbox id="s800U2FDesc" runat="server" Width="95%" MaxLength="100" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td><ml:MoneyTextBox id="s800U2F" runat="server" preset="money" width="82px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s800U2FProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td><asp:textbox id="s800U3FCode" runat="server" Width="34px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td colSpan="2"><asp:textbox id="s800U3FDesc" runat="server" Width="95%" MaxLength="100" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td><ml:MoneyTextBox id="s800U3F" runat="server" preset="money" width="82px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s800U3FProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td><asp:textbox id="s800U4FCode" runat="server" Width="34px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td colSpan="2"><asp:textbox id="s800U4FDesc" runat="server" Width="95%" MaxLength="100" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td><ml:MoneyTextBox id="s800U4F" runat="server" preset="money" width="82px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s800U4FProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td><asp:textbox id="s800U5FCode" runat="server" Width="34px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td colSpan="2"><asp:textbox id="s800U5FDesc" runat="server" Width="95%" MaxLength="100" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td><ml:MoneyTextBox id="s800U5F" runat="server" preset="money" width="82px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s800U5FProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FormTableSubheader"><a name="900"></a>900</td>
					<td class="FormTableSubheader" colSpan="7">ITEMS REQUIRED BY LENDER TO BE PAID IN ADVANCE</td></tr>
				<tr>
					<td class="FieldLabel">901</td>
					<td class="FieldLabel">Interest for&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="sIPiaDyLckd" runat="server" Text="Locked" onclick="refreshCalculation();"/></td>
					<td noWrap class=FieldLabel><asp:textbox id="sIPiaDy" runat="server" Width="40px" MaxLength="4" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></asp:textbox>&nbsp;days 
						@
						<ml:MoneyTextBox id="sIPerDay" runat="server" preset="money" ReadOnly="True"  decimalDigits="6"></ml:MoneyTextBox> per 
						day</td>
					<td><ml:MoneyTextBox id="sIPia" runat="server" preset="money" width="82px" ReadOnly="True"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sIPiaProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">902</td>
					<td class="FieldLabel" colSpan="2"><a href="javascript:linkMe('../LoanInfo.aspx?pg=1');">Mortgage Insurance Premium</a></td>
					<td><ml:MoneyTextBox id="sMipPia" runat="server" preset="money" width="82px" ReadOnly="True"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sMipPiaProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">903</td>
					<td noWrap class="FieldLabel" colSpan="2">
                        Haz Ins.&nbsp;
                        <asp:PlaceHolder runat="server" ID="sProHazInsHolder">
                            @&nbsp;<ml:PercentTextBox id="sProHazInsR" runat="server" preset="percent" width="70" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:PercentTextBox> 
                            of <asp:dropdownlist id="sProHazInsT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist> 
                            + <ml:MoneyTextBox id="sProHazInsMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" preset="money" width="60px" runat="server" onchange="refreshCalculation();" decimalDigits="4"></ml:MoneyTextBox>
                            for 
                        </asp:PlaceHolder>
						<asp:textbox id="sHazInsPiaMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" MaxLength="4" Width="32px" onchange="refreshCalculation();"></asp:textbox>&nbsp; mths
					</td>
					<TD><ml:MoneyTextBox id="sHazInsPia" preset="money" width="83" runat="server" ReadOnly="True"></ml:MoneyTextBox></TD>
					<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sHazInsPiaProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
				</tr>
				<TR>
					<TD class="FieldLabel">904</TD>
					<td colSpan="2"><asp:textbox id="s904PiaDesc" runat="server" Width="95%" MaxLength="100" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></asp:textbox></td>
					<td><ml:MoneyTextBox id="s904Pia" runat="server" preset="money" width="83px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s904PiaProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</TR>
				<tr>
					<td class="FieldLabel">905</td>
					<td class="FieldLabel"><a href="javascript:linkMe('../LoanInfo.aspx?pg=1');">VA Funding Fee</a></td>
					<td></td>
					<td><ml:MoneyTextBox id="sVaFf" runat="server" preset="money" width="83px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" ReadOnly="True"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sVaFfProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td><asp:textbox id="s900U1PiaCode" runat="server" Width="35" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></asp:textbox></td>
					<td colSpan="2"><asp:textbox id="s900U1PiaDesc" runat="server" Width="95%" MaxLength="100" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></asp:textbox></td>
					<td><ml:MoneyTextBox id="s900U1Pia" runat="server" preset="money" width="83px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s900U1PiaProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FormTableSubheader"><a name="1000"></a>1000</td>
					<td class="FormTableSubheader" colSpan="7">RESERVES DEPOSITED WITH LENDER</td></tr>
				<tr>
					<td class="FieldLabel">1001</td>
					<td class="FieldLabel">Haz ins. reserve</td>
					<td class=FieldLabel><asp:textbox id="sHazInsRsrvMon" runat="server" Width="41" MaxLength="4" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></asp:textbox>&nbsp;mths 
						@
						<ml:MoneyTextBox id="sProHazIns" runat="server" preset="money" width="90" ReadOnly="True"></ml:MoneyTextBox> / 
						month</td>
					<td><ml:MoneyTextBox id="sHazInsRsrv" runat="server" preset="money" width="83px" ReadOnly="True"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sHazInsRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">1002</td>
					<td class="FieldLabel">Mtg ins. reserve</td>
					<td class=FieldLabel><asp:textbox id="sMInsRsrvMon" runat="server" Width="41px" MaxLength="4" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></asp:textbox>&nbsp;mths 
						@
						<ml:MoneyTextBox id="sProMIns" runat="server" preset="money" width="90" ReadOnly="True"></ml:MoneyTextBox> / 
						month</td>
					<td><ml:MoneyTextBox id="sMInsRsrv" runat="server" preset="money" width="83px" ReadOnly="True"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sMInsRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">1003</td>
					<td class="FieldLabel">School taxes</td>
					<td class=FieldLabel><asp:textbox id="sSchoolTxRsrvMon" runat="server" Width="41px" MaxLength="4" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></asp:textbox>&nbsp;mths 
						@
						<ml:MoneyTextBox id="sProSchoolTx" runat="server" preset="money" width="90" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox> / 						month</td>
					<td><ml:MoneyTextBox id="sSchoolTxRsrv" runat="server" preset="money" width="83px" ReadOnly="True"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sSchoolTxRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">1004</td>
					<td colSpan="2" class=FieldLabel>
                        Tax resrv&nbsp; 
                        <asp:PlaceHolder runat="server" ID="sProRealETxHolder">
                            @&nbsp;<ml:PercentTextBox id="sProRealETxR" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" preset="percent" width="67px" runat="server" onchange="refreshCalculation();"></ml:PercentTextBox> 
                            of <asp:dropdownlist id="sProRealETxT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist> 
                            + <ml:MoneyTextBox id="sProRealETxMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" preset="money" width="56px" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox>&nbsp; for 
                        </asp:PlaceHolder>
                        <asp:textbox id="sRealETxRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();" MaxLength="4" Width="41px"></asp:textbox> mths
					</td>
					<td><ml:MoneyTextBox id="sRealETxRsrv" runat="server" preset="money" width="83px" ReadOnly="True"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3">
						<uc1:goodfaithestimaterightcolumn id="sRealETxRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">1005</td>
					<td class="FieldLabel">Flood ins. reserve</td>
					<td class=FieldLabel><asp:textbox id="sFloodInsRsrvMon" runat="server" Width="41px" MaxLength="4" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></asp:textbox>&nbsp;mths 
						@
						<ml:MoneyTextBox id="sProFloodIns" runat="server" preset="money" width="90" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox> / 
						month</td>
					<td><ml:MoneyTextBox id="sFloodInsRsrv" runat="server" preset="money" width="83px" ReadOnly="True"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sFloodInsRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">1006</td>
					<td><asp:textbox id="s1006ProHExpDesc" runat="server" Width="135" Height="20px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td class=FieldLabel><asp:textbox id="s1006RsrvMon" runat="server" Width="41px" MaxLength="4" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></asp:textbox>&nbsp;mths 
						@
						<ml:MoneyTextBox id="s1006ProHExp" runat="server" preset="money" width="90" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox> / 
						month</td>
					<td><ml:MoneyTextBox id="s1006Rsrv" runat="server" preset="money" width="83px" ReadOnly="True"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s1006RsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">1007</td>
					<td><asp:textbox id="s1007ProHExpDesc" runat="server" Width="136" Height="20px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td class=FieldLabel><asp:textbox id="s1007RsrvMon" runat="server" Width="41px" MaxLength="4" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></asp:textbox>&nbsp;mths 
						@
						<ml:MoneyTextBox id="s1007ProHExp" runat="server" preset="money" width="90" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox> / 
						month</td>
					<td><ml:MoneyTextBox id="s1007Rsrv" runat="server" preset="money" width="83px" ReadOnly="True"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s1007RsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<asp:PlaceHolder runat="server" ID="phAdditionalSection1000CustomFees">
    				<tr>
    					<td class="FieldLabel">1008</td>
    					<td><asp:textbox id="sU3RsrvDesc" runat="server" Width="136" Height="20px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
    					<td class=FieldLabel><asp:textbox id="sU3RsrvMon" runat="server" Width="41px" MaxLength="4" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></asp:textbox>&nbsp;mths 
    						@
    						<ml:MoneyTextBox id="sProU3Rsrv" runat="server" preset="money" width="90" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox> / 
    						month</td>
    					<td><ml:MoneyTextBox id="sU3Rsrv" runat="server" preset="money" width="83px" ReadOnly="True"></ml:MoneyTextBox></td>
    					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU3RsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
    				</tr>
    				<tr>
    					<td class="FieldLabel">1009</td>
    					<td><asp:textbox id="sU4RsrvDesc" runat="server" Width="136" Height="20px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
    					<td class=FieldLabel><asp:textbox id="sU4RsrvMon" runat="server" Width="41px" MaxLength="4" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></asp:textbox>&nbsp;mths 
    						@
    						<ml:MoneyTextBox id="sProU4Rsrv" runat="server" preset="money" width="90" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox> / 
    						month</td>
    					<td><ml:MoneyTextBox id="sU4Rsrv" runat="server" preset="money" width="83px" ReadOnly="True"></ml:MoneyTextBox></td>
    					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU4RsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
    				</tr>
				</asp:PlaceHolder>
				<tr>
					<td class="FieldLabel"><ml:EncodedLiteral runat="server" ID="AggregateAdjustmentLineNum" Text="1008"></ml:EncodedLiteral></td>
					<td class="FieldLabel">Aggregate adjustment</td>
					<td></td>
					<td><ml:MoneyTextBox id="sAggregateAdjRsrv" runat="server" preset="money" width="83px" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sAggregateAdjRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FormTableSubheader">1100</td>
					<td class="FormTableSubheader" colSpan="7">TITLE CHARGES</td></tr>
				<tr>
					<td class="FieldLabel">1101</td>
					<td class="FieldLabel">Closing/Escrow Fee</td>
					<td><asp:textbox id="sEscrowFTable" runat="server" Width="212px" MaxLength="36" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sEscrowF" runat="server" preset="money" width="83px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sEscrowFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">1105</td>
					<td class="FieldLabel">Doc preparation fee</td>
					<td></td>
					<td><ml:MoneyTextBox id="sDocPrepF" runat="server" preset="money" width="83px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sDocPrepFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">1106</td>
					<td class="FieldLabel">Notary fees</td>
					<td></td>
					<td><ml:MoneyTextBox id="sNotaryF" runat="server" preset="money" width="83px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sNotaryFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">1107</td>
					<td class="FieldLabel">Attorney fees</td>
					<td></td>
					<td><ml:MoneyTextBox id="sAttorneyF" runat="server" preset="money" width="83px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sAttorneyFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FieldLabel">1108</td>
					<td class="FieldLabel">Title Insurance</td>
					<td><asp:textbox id="sTitleInsFTable" runat="server" Width="212px" MaxLength="36" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTitleInsF" runat="server" preset="money" width="84" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sTitleInsFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td><asp:textbox id="sU1TcCode" runat="server" Width="33px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td colSpan="2"><asp:textbox id="sU1TcDesc" runat="server" Width="95%" MaxLength="100" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sU1Tc" runat="server" preset="money" width="84px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU1TcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td><asp:textbox id="sU2TcCode" runat="server" Width="33px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td colSpan="2"><asp:textbox id="sU2TcDesc" runat="server" Width="95%" MaxLength="100" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sU2Tc" runat="server" preset="money" width="84px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU2TcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td><asp:textbox id="sU3TcCode" runat="server" Width="33px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td colSpan="2"><asp:textbox id="sU3TcDesc" runat="server" Width="95%" MaxLength="100" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sU3Tc" runat="server" preset="money" width="84px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU3TcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td><asp:textbox id="sU4TcCode" runat="server" Width="34" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td colSpan="2"><asp:textbox id="sU4TcDesc" runat="server" Width="95%" MaxLength="100" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sU4Tc" runat="server" preset="money" width="84px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU4TcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td class="FormTableSubheader">1200</td>
					<td class="FormTableSubheader" colSpan="7" nowrap>GOVERNMENT RECORDING &amp; TRANSFER CHARGES</td></tr>
				<tr>
					<td class="FieldLabel">1201</td>
					<td noWrap class="FieldLabel">Recording fees <asp:TextBox id="sRecFDesc" Width="89px" runat="server" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></asp:TextBox></td>
					<td noWrap><ml:PercentTextBox id="sRecFPc" runat="server" preset="percent" width="52" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:PercentTextBox> of
						<asp:dropdownlist id="sRecBaseT" runat="server" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);">
							<asp:ListItem Value="0">Loan Amount</asp:ListItem>
							<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
							<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
						</asp:dropdownlist> + <ml:MoneyTextBox id="sRecFMb" runat="server" preset="money" width="60px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<TD><ml:MoneyTextBox id="sRecF" runat="server" preset="money" width="84px" ReadOnly="True"></ml:MoneyTextBox></TD>
					<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sRecFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
				</tr>
				<TR>
					<td class="FieldLabel">1202</td>
					<td noWrap class="FieldLabel">City tax/stamps <asp:TextBox id="sCountyRtcDesc" Width="86px" runat="server" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></asp:TextBox></td>
					<td noWrap><ml:PercentTextBox id="sCountyRtcPc" runat="server" preset="percent" width="52px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:PercentTextBox> of
						<asp:dropdownlist id="sCountyRtcBaseT" runat="server" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);">
							<asp:ListItem Value="0">Loan Amount</asp:ListItem>
							<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
							<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
						</asp:dropdownlist> + <ml:MoneyTextBox id="sCountyRtcMb" runat="server" preset="money" width="60px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<TD><ml:MoneyTextBox id="sCountyRtc" ReadOnly="True" runat="server" preset="money" width="84px"></ml:MoneyTextBox></TD>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sCountyRtcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</TR>
				<tr>
					<td class="FieldLabel">1203</td>
					<td noWrap class="FieldLabel">State tax/stamps <asp:TextBox id="sStateRtcDesc" Width="82px" runat="server" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></asp:TextBox></td>
					<td noWrap><ml:PercentTextBox id="sStateRtcPc" runat="server" preset="percent" width="52px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:PercentTextBox> of
						<asp:dropdownlist id="sStateRtcBaseT" runat="server" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);">
							<asp:ListItem Value="0">Loan Amount</asp:ListItem>
							<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
							<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
						</asp:dropdownlist> + <ml:MoneyTextBox id="sStateRtcMb" runat="server" preset="money" width="60px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td><ml:MoneyTextBox id="sStateRtc" runat="server" preset="money" width="84px" ReadOnly="True"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sStateRtcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td><asp:textbox id="sU1GovRtcCode" runat="server" Width="34px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td><asp:textbox id="sU1GovRtcDesc" runat="server" Width="104" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td noWrap><ml:PercentTextBox id="sU1GovRtcPc" runat="server" preset="percent" width="52px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:PercentTextBox> of
						<asp:dropdownlist id="sU1GovRtcBaseT" runat="server" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);">
							<asp:ListItem Value="0">Loan Amount</asp:ListItem>
							<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
							<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
						</asp:dropdownlist> + <ml:MoneyTextBox id="sU1GovRtcMb" runat="server" preset="money" width="60px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<TD><ml:MoneyTextBox id="sU1GovRtc" runat="server" preset="money" width="84px" ReadOnly="True"></ml:MoneyTextBox></TD>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU1GovRtcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td><asp:textbox id="sU2GovRtcCode" runat="server" Width="34px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td><asp:textbox id="sU2GovRtcDesc" runat="server" Width="104px" Height="20px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td noWrap><ml:PercentTextBox id="sU2GovRtcPc" runat="server" preset="percent" width="52px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:PercentTextBox> of
						<asp:dropdownlist id="sU2GovRtcBaseT" runat="server" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);">
							<asp:ListItem Value="0">Loan Amount</asp:ListItem>
							<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
							<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
						</asp:dropdownlist> + <ml:MoneyTextBox id="sU2GovRtcMb" runat="server" preset="money" width="60px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<TD><ml:MoneyTextBox id="sU2GovRtc" runat="server" preset="money" width="84px" ReadOnly="True"></ml:MoneyTextBox></TD>
					<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU2GovRtcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
				</tr>
				<TR>
					<TD><asp:textbox id="sU3GovRtcCode" runat="server" MaxLength="21" Width="34px" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></TD>
					<TD><asp:textbox id="sU3GovRtcDesc" runat="server" MaxLength="21" Width="104px" Height="20px" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></TD>
					<TD noWrap><ml:PercentTextBox id="sU3GovRtcPc" runat="server" preset="percent" width="52px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:PercentTextBox> of
						<asp:dropdownlist id="sU3GovRtcBaseT" runat="server" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);">
							<asp:ListItem Value="0">Loan Amount</asp:ListItem>
							<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
							<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
						</asp:dropdownlist> + <ml:MoneyTextBox id="sU3GovRtcMb" runat="server" preset="money" width="60px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></TD>
					<TD><ml:MoneyTextBox id="sU3GovRtc" runat="server" preset="money" width="84px" ReadOnly="True"></ml:MoneyTextBox></TD>
					<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU3GovRtcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
				</TR>
				<TR>
					<TD class="FormTableSubheader">1300</TD>
					<TD class="FormTableSubheader" colSpan="7">ADDITIONAL SETTLEMENT CHARGES</TD></TR>
				<TR>
					<TD class="FieldLabel">1302</TD>
					<td class="FieldLabel">Pest Inspection</td>
					<td></td>
					<td><ml:MoneyTextBox id="sPestInspectF" runat="server" preset="money" width="84px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sPestInspectFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</TR>
				<tr>
					<td><asp:textbox id="sU1ScCode" runat="server" Width="34px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td colSpan="2"><asp:textbox id="sU1ScDesc" runat="server" Width="95%" MaxLength="100" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sU1Sc" runat="server" preset="money" width="84px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU1ScProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td><asp:textbox id="sU2ScCode" runat="server" Width="34px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td colSpan="2"><asp:textbox id="sU2ScDesc" runat="server" Width="95%" MaxLength="100" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sU2Sc" runat="server" preset="money" width="84px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU2ScProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td><asp:textbox id="sU3ScCode" runat="server" Width="34px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td colSpan="2"><asp:textbox id="sU3ScDesc" runat="server" Width="95%" MaxLength="100" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sU3Sc" runat="server" preset="money" width="84px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU3ScProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td><asp:textbox id="sU4ScCode" runat="server" Width="34px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td colSpan="2"><asp:textbox id="sU4ScDesc" runat="server" Width="95%" MaxLength="100" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sU4Sc" runat="server" preset="money" width="84px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU4ScProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
				<tr>
					<td><asp:textbox id="sU5ScCode" runat="server" Width="34px" MaxLength="21" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td colSpan="2"><asp:textbox id="sU5ScDesc" runat="server" Width="95%" MaxLength="100" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" SkipMe="true"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sU5Sc" runat="server" preset="money" width="84px" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:MoneyTextBox></td>
					<td noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU5ScProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table id="Table3" cellSpacing="0" cellPadding="0" border="0" class="InsetBorder">
				<tr>
					<td class="FieldLabel">Initial Fees, Commissions, Costs and Expenses Paid to Other</td>
					<td><ml:MoneyTextBox id="sTotCcPto" preset="money" width="90" runat="server" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" tabIndex=201 readonly="True"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td class="FieldLabel">Initial Fees, Commissions, Costs and Expenses Paid to Broker</td>
					<td><ml:MoneyTextBox id="sTotCcPtb" preset="money" width="90" runat="server" onchange="refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" tabIndex=201 readonly="True"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td class="FieldLabel">Totals of Initial Fees, Commissions, Costs and Expenses</td>
					<td><ml:MoneyTextBox id="sTotEstScMlds" preset="money" width="90" runat="server" ReadOnly="True" tabIndex=201></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td class="FormTableSubheader" colSpan="2">Compensation to Broker (Not Paid Out of 
						Loan Proceeds)</td>
				</tr>
				<tr>
					<td class="FieldLabel">Mortgage Broker Commission 
            / 
            Fee&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
<ml:PercentTextBox id=sBrokComp1Pc tabIndex=201 runat="server" preset="percent" width="70" onchange="refreshCalculation();"  onfocus="highlightRow(this);" onblur="unhighlightRow(this);"></ml:PercentTextBox></td>
					<td><ml:MoneyTextBox id="sBrokComp1Mlds" preset="money" width="90" runat="server" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" tabIndex=201></ml:MoneyTextBox><asp:CheckBox id=sBrokComp1MldsLckd tabIndex=201 runat="server" Text="Locked" onclick="refreshCalculation();" /></td>
				</tr>
				<tr>
					<td class="FieldLabel">Any Additional Compensation from Lender</td>
					<td><ml:MoneyTextBox id="sBrokComp2Mlds" preset="money" width="90" runat="server" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" tabIndex=201 /><asp:CheckBox ID="sBrokComp2MldsLckd" TabIndex="201" runat="server" Text="Locked"  onclick="refreshCalculation();"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
