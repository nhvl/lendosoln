﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GoodFaithEstimate2010Footer.ascx.cs" Inherits="LendersOfficeApp.newlos.Forms.GoodFaithEstimate2010Footer" %>
<%@ Register TagPrefix="uc1" TagName="DetailsofTransaction" Src="~/newlos/DetailsofTransaction.ascx" %>

<style type="text/css">
    .tabContent {
        background-color: gainsboro;
        padding: 10px;
    }
    
    #Tabs {
        background-color: Transparent;
    }
    
    #closeIcon{
        padding: 10px;
        float:right;
    }
    
    #closeIcon a,
    #closeIcon a:hover
    {
        font-size:15px;
        color: Gray;
        text-decoration: none;
    }
    
</style>

<ul id="Tabs" class="tabnav">
  <li id="PaidTotalsLnk"><a href="#PaidTotalsLnk" onclick="switchTab('PaidTotals');">Lender and Seller paid totals</a></li>
  <li id="DotLnk"><a href="#DotLnk" onclick="switchTab('Dot');">Details of Transaction</a></li>
  <li id="MinLnk"><a href="#MinLnk" onclick="switchTab('Min');">Minimize</a></li>
</ul>

<div id="closeIcon">
    <a href="#MinLnk" onclick="switchTab('Min');">&times;</a>
</div>

<div id="PaidTotals" class="tabContent">
    <table>
        <tr>
            <td>
                Total fees paid by lender: 
            </td>
            <td>
                <ml:moneytextbox id="sGfeTotalFundByLender" runat="server" preset="money" width="77" ReadOnly="True" />
            </td>
            <td>
                Total fees paid by seller: 
            </td>
            <td>
                <ml:moneytextbox id="sGfeTotalFundBySeller" runat="server" preset="money" width="77" ReadOnly="True" />
            </td>
        </tr>
    </table>
</div>

<div id="Dot" class="tabContent">
    <uc1:DetailsofTransaction ID="dot" runat="server" hideHeaderAndBoarder="true" />
</div>

<script type="text/javascript">
    var $wrapper = $j("#wrapper");
    var $closeIcon = $j("#closeIcon");
    var $tabLinks = $j("#Tabs li");
    var $contentDivs = $j(".tabContent");

    function switchTab(tabId) {
        $closeIcon.hide();
        $contentDivs.hide();
        $tabLinks.removeClass();

        $tabLinks.filter("#" + tabId + "Lnk").addClass("selected");

        $div = $contentDivs.filter("#" + tabId);
        if ($div.length != 0) {   // need check because minimize has no content
            $div.show();
            $closeIcon.show();
        }

        $j(window).trigger('footer/switchTab')
    }

    switchTab("Min");
</script>