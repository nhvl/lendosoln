﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using LendersOffice.Pdf;
using LendersOffice.Common;
using DataAccess;
using System.Web.UI.HtmlControls;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class TaxReturn4506T : BaseLoanPage
    {

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.PageID = "TaxReturn4506T";
            this.PageTitle = "Request for Transcript of Tax Return";

            this.PDFPrintClass = typeof(CIRS_4506_TPDF);

            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("LQBPopup.js");

            // Init your user controls here, i.e: bind drop down list.
            CFM.IsAllowLockableFeature = false;
            CFM.PhoneField = a4506TThirdPartyPhone.ClientID;
            CFM.AgentNameField = a4506TThirdPartyName.ClientID;
            CFM.StreetAddressField = a4506TThirdPartyStreetAddr.ClientID;
            CFM.CityField = a4506TThirdPartyCity.ClientID;
            CFM.StateField = a4506TThirdPartyState.ClientID;
            CFM.ZipField = a4506TThirdPartyZip.ClientID;

            a4506TPrevZip.SmartZipcode(a4506TPrevCity, a4506TPrevState);
            a4506TThirdPartyZip.SmartZipcode(a4506TThirdPartyCity, a4506TThirdPartyState);
            RegisterService("loanedit", "/newlos/forms/TaxReturn4506TService.aspx");

        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
        }
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(TaxReturn4506T));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            aB4506TSsnTinEin.Text = dataApp.aB4506TSsnTinEin;
            aC4506TSsnTinEin.Text = dataApp.aC4506TSsnTinEin;
            a4506TPrevStreetAddr.Text = dataApp.aB4506TPrevStreetAddr;
            a4506TPrevCity.Text = dataApp.aB4506TPrevCity;
            a4506TPrevState.Value = dataApp.aB4506TPrevState;
            a4506TPrevZip.Text = dataApp.aB4506TPrevZip;
            a4506TThirdPartyName.Text = dataApp.aB4506TThirdPartyName;
            a4506TThirdPartyStreetAddr.Text = dataApp.aB4506TThirdPartyStreetAddr;
            a4506TThirdPartyCity.Text = dataApp.aB4506TThirdPartyCity;
            a4506TThirdPartyState.Value = dataApp.aB4506TThirdPartyState;
            a4506TThirdPartyZip.Text = dataApp.aB4506TThirdPartyZip;
            a4506TThirdPartyPhone.Text = dataApp.aB4506TThirdPartyPhone;
            a4506TTranscript.Text = dataApp.aB4506TTranscript;
            a4056TIsReturnTranscript.Checked = dataApp.aB4056TIsReturnTranscript;
            a4506TIsAccountTranscript.Checked = dataApp.aB4506TIsAccountTranscript;
            a4056TIsRecordAccountTranscript.Checked = dataApp.aB4056TIsRecordAccountTranscript;
            a4506TVerificationNonfiling.Checked = dataApp.aB4506TVerificationNonfiling;
            a4506TSeriesTranscript.Checked = dataApp.aB4506TSeriesTranscript;
            a4506TYear1.Text = dataApp.aB4506TYear1_rep;
            a4506TYear2.Text = dataApp.aB4506TYear2_rep;
            a4506TYear3.Text = dataApp.aB4506TYear3_rep;
            a4506TYear4.Text = dataApp.aB4506TYear4_rep;
            a4506TRequestYrHadIdentityTheft.Checked = dataApp.aB4506TRequestYrHadIdentityTheft;
            a4506TSignatoryAttested.Checked = dataApp.aB4506TSignatoryAttested;

            aB4506TNm.Text = dataApp.aB4506TNm;
            aB4506TNmLckd.Checked = dataApp.aB4506TNmLckd;

            aC4506TNm.Text = dataApp.aC4506TNm;
            aC4506TNmLckd.Checked = dataApp.aC4506TNmLckd;

            if (dataApp.aIs4506TFiledTaxesSeparately)
            {
                radioSeparate.Checked = true;
            }
            else
            {
                radioJointly.Checked = true;
            }
        }

        protected override void SaveData()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
    }
}
