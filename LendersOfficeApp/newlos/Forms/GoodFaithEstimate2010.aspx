<%@ Register TagPrefix="uc1" TagName="GoodFaithEstimate2010RightColumn" Src="../../los/LegalForm/GoodFaithEstimate2010RightColumn.ascx" %>
<%@ Register TagPrefix="uc1" TagName="GoodFaithEstimate2010Footer" Src="~/newlos/Forms/GoodFaithEstimate2010Footer.ascx" %>
<%@ Page language="c#" Codebehind="GoodFaithEstimate2010.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.GoodFaithEstimate2010" enableViewState="False"%>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import namespace="DataAccess"%>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="LendersOffice.Security" %>
<%@ Import Namespace="LendersOffice.Constants" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
		<title>GoodFaithEstimate</title>
		
		<style type="text/css">
		    /* Adding "_" before attribute name makes those attributes valid only in IE 6 or quirks (IE 5.5) mode */
		    html, body{
	            _height: 100%;
	            _overflow: hidden;
            }

            #wrapper {
	            _width: 100%;
	            _height: 100%;
	            _overflow: auto;
	            _position: relative;
            }
            
		    #footerWrapper 
		    {
		        position: fixed;
		        left: 0px;
		        bottom: 0px;
		        width: 100%;
		        
	            _position: absolute; /* Overrides position attribute in quirks mode */
            }
            
            #footer {
	            _margin-right: 17px; /* for right scroll bar in quirks mode */
	            
	            /**
	            /* Following properties needed to keep mouse clicks from falling through to underlying divs.
	            /* Placed at #footer level instead of #footerWrapper so that user can still click on #wrapper's scrollbar
	            **/
	            background: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7); /* background image set to 1x1 transparent GIF. */
	            width: 100%; /* needs to be set to 100%, or #footer width will match width of active tab */
            }
        </style>
  </HEAD>
	<body bgcolor="gainsboro" MS_POSITIONING="FlowLayout">
		<style>
		    .RightPadding {padding-right:12px;}
		    .AlignLabelForLockCB { width: 125px; }
		</style>
		<script type="text/javascript">
  <!--
  var bIsFirst = true;
  var oRolodex = null;  
  // If it is a purchase loan, sOwnerTitleInsFProps_ctrl_PaidTo_tb will be added to linkedPaidTo
  var linkedPaidTo = new Array("sApprFProps_ctrl_PaidTo_tb"
                              ,"sCrFProps_ctrl_PaidTo_tb"
                              ,"sFloodCertificationFProps_ctrl_PaidTo_tb"
                              ,"sEscrowFProps_ctrl_PaidTo_tb"
                              ,"sTitleInsFProps_ctrl_PaidTo_tb"
                              ,"sAttorneyFProps_ctrl_PaidTo_tb"
                              );

  //Returns false if the loan needs saving and the user cancels out of the save dialog
  function saveIfModified(handler){
	// Before popping up a modal dialog, you always need to save the loan.
	PolyShouldShowConfirmSave(isDirty(), handler, saveMe)
    return true;
  }
  
  function findCCTemplate() {
	saveIfModified(function(){
		showModal(<%= AspxTools.JsString(ClosingCostPageUrl) %>, null, null, null, function(args){ 
			if (args.OK) {
				self.location = self.location; // Refresh;
			}
		},{ hideCloseButton: true });
	});
  }
  function quoteRequest(){
    saveIfModified(function(){
		var windowOpts = 'dialogWidth:670px;dialogHeight:700px;center:yes;resizable:no;scroll:yes;status=no;help=no;'
		showModal('/newlos/Services/Title/QuoteRequest.aspx?loanid=' + ML.sLId + '&appid=' + ML.aAppId, null, windowOpts, null, function(args){ 
			window.location = window.location;
		});
	});
  }
  
  function viewStoredQuote(){
    var args = {"isModal": true};
    var windowOpts = 'dialogWidth:630px;dialogHeight:440px;center:yes;resizable:no;scroll:yes;status=no;help=no;';
    showModal('/newlos/Services/Title/QuoteResult.aspx?loanid=' + ML.sLId + '&appid=' + ML.aAppId, args, windowOpts, null, function(result){ 
		if(result.newLocation)
		{
			window.location = result.newLocation;
		}
		else
		{
			window.location = window.location;
		}
	});
  }
  function openPopup(linkFirstPart, uniqueName)
  {
    var virtualRoot = <%=AspxTools.SafeUrl(VirtualRoot)%>;
    var popup = window.open(virtualRoot+'/'+linkFirstPart+'?loanid='+ML.sLId+
        "&appid="+ML.aAppId+
        "&ispopup=1", uniqueName, "toolbar=no,menubar=no,location=no,status=no,scrollbars=yes,resizable=yes");
    popup.focus();
  }
  function _init() {
    $(function(){
        $('form').on("keyup keypress", function(e) {
          var code = e.keyCode || e.which; 
          if (code  == 13) {               
            e.preventDefault();
            return false;
          }
        });
    });

    // Because these fields are calculated, we cannot set their value to blank
    // if an invalid date is entered. As such, we'll set the old value to the
    // original value when the page loads, and if an invalid value is entered,
    // the field will revert to the original value. This matches the behavior 
    // on the new TRID GFE page.
    var sSchedDueInput = document.getElementById('sSchedDueD1');
    var sEstCloseInput = document.getElementById('sEstCloseD');

    if(!sSchedDueInput.oldValueSet)
    {
        sSchedDueInput.oldValue = sSchedDueInput.value;
        sSchedDueInput.oldValueSet = true;
    }

    if(!sEstCloseInput.oldValueSet)
    {
        sEstCloseInput.oldValue = sEstCloseInput.value;
        sEstCloseInput.oldValueSet = true;
    }

    <%if(ShowGFEArchiveRecorder){ %>
        if(<%= AspxTools.JsGetElementById(hfDoWindowRefresh) %>.value == 'true')
        {
           var body_url = <%=AspxTools.JsNumeric(E_UrlOption.Page_2010GFE) %>;
           window.top.location = ML.VirtualRoot + '/newlos/LoanApp.aspx' + '?loanid='+ML.sLId+
            "&appid="+ML.aAppId+'&body_url='+ body_url;
        }
    <%} %>
    if (null == oRolodex)
      oRolodex = new cRolodex();
    
    lockField(<%= AspxTools.JsGetElementById(sGfeRateLockPeriodLckd) %>, 'sGfeRateLockPeriod');    
    lockField(<%=AspxTools.JsGetElementById(sAggregateAdjRsrvLckd)%>, 'sAggregateAdjRsrv');
    lockField(<%=AspxTools.JsGetElementById(sIPerDayLckd)%>, 'sIPerDay');
    lockField(<%=AspxTools.JsGetElementById(sIPiaDyLckd) %>, 'sIPiaDy');
    lockField(<%= AspxTools.JsGetElementById(sSchedDueD1Lckd) %>, 'sSchedDueD1');
    lockField(<%= AspxTools.JsGetElementById(sEstCloseDLckd) %>, 'sEstCloseD');
    lockField(<%= AspxTools.JsGetElementById(sConsummationDLckd) %>, 'sConsummationD');
    lockField(document.getElementById("sGfeHavePpmtPenalty_1"), 'sGfeMaxPpmtPenaltyAmt');
    lockField(<%=AspxTools.JsGetElementById(sGfeEstScAvailTillDLckd) %>, 'sGfeEstScAvailTillD');
    lockField(<%=AspxTools.JsGetElementById(sGfeNoteIRAvailTillDLckd) %>, 'sGfeNoteIRAvailTillD');
    
    // OPM 122786 - If it is a purchase loan then we want to have line
    // 1103 mimic line 1104
    if (<%= AspxTools.JsGetElementById(hfsLPurposeT) %>.value === 
            <%= AspxTools.JsString(E_sLPurposeT.Purchase) %>) {
        linkedPaidTo.push('sOwnerTitleInsFProps_ctrl_PaidTo_tb');
    }
    
    setPaidToReadOnly();
    
    bIsAutoCalculate = document.getElementById("ByPassBgCalcForGfeAsDefault_0").checked;
    document.getElementById("btnCalculate").disabled = bIsAutoCalculate;
    toggleTimeInput(); 
    updateOriginatorCompensationUI(); 

    <%if(!IsArchivePage){ %>
	displayCCOptions();
    <%} %>
    
    f_setFinancedCredits();
    
    if (bIsFirst) {
      // Only init one time.
      bIsFirst = false;
      var length = document.forms[0].ByPassBgCalcForGfeAsDefault.length;
      
            for (var i = 0; i < length; i++)
                addEventHandler(document.forms[0].ByPassBgCalcForGfeAsDefault[i], "click", backgroundCalculation, false);
            length = document.forms[0].sGfeHavePpmtPenalty.length;
            for (var i = 0; i < length; i++)
                addEventHandler(document.forms[0].sGfeHavePpmtPenalty[i], "click", backgroundCalculation, false);
          
            length = document.forms[0].sMldsHasImpound.length;
            for (var i = 0; i < length; i++)
                addEventHandler(document.forms[0].sMldsHasImpound[i], "click", backgroundCalculation, false);
        
      document.onkeydown = document_keyDown;
      document.onkeyup = document_keyUp;
      bAltDown = false;
      <%= AspxTools.JsGetElementById(GfeTilCompanyName) %>.focus();
      
      <% if(IsClosingCostMigrationArchivePage) { %>
            $('#Table2>tbody>tr').not('.ShowForClosingCostMigrationArchive').hide();
            $('#Table5 tr.HideForClosingCostMigrationArchive').hide();
      <% } %>
      
      toggleRecFFields()
      
      initComboboxes();
    }  
    
    if (typeof _initControl === 'function') {
        _initControl();
    }
    
    var ShowRequireFeesFromDropDownCheckbox = <%= AspxTools.JsBool(ShowRequireFeesFromDropDownCheckbox) %>;
    if(!ShowRequireFeesFromDropDownCheckbox)
    {
        $(".sIsRequireFeesFromDropDown").hide();
    }
    
    if(!ML.ShowRequirePaidToFromOfficialContactListCheckbox)
    {
        $(".sGfeRequirePaidToFromContacts").hide();
    }
  }
  
  function displayCalendar(field) {
        var el = document.getElementById(field);
        if (el.readOnly || el.disabled)
            return false;
            
        if (gCalendar != null) {
            gCalendar.hide();
        } else {
            var cal = new Calendar(false, null, selectedDate, closeCalHandler);
            gCalendar = cal;
            cal.setRange(1900, 2070);
            cal.setDateFormat('mm/dd/y');
            cal.create();
            
            // For IE6 and Quirks mode, the parent of the calendar needs to be wrapper
            // to scroll with the page to accomodate for the position:fixed workarounds.
            if(document.compatMode !== 'CSS1Compat' || navigator.appVersion.indexOf("MSIE 6.") != -1)
            {
                document.getElementById("wrapper").appendChild(cal.element);
            }
        }
        gCalendar.sel = el;
        var dateVal = gCalendar.sel.value;
        if (dateVal != '') {
            gCalendar.sel.blur();
            gCalendar._init(false, new Date(dateVal));
        }
        gCalendar.showAtElement(el);
        return false;
    }
  
  function toggleRecFFields()
  {
      var sRecFLckd = <%= AspxTools.JsGetElementById(sRecFLckd)%>;
      var sp1201 = $("#sp1201");
      var sRecDeed = <%= AspxTools.JsGetElementById(sRecDeed)%>;
      var sRecMortgage = <%= AspxTools.JsGetElementById(sRecMortgage)%>;
      var sRecRelease = <%= AspxTools.JsGetElementById(sRecRelease)%>;
      
      if( sRecFLckd.checked )
      {
        sp1201.show();
        sRecDeed.readOnly = true;
        sRecMortgage.readOnly = true;
        sRecRelease.readOnly = true;
      }
      else
      {
        sp1201.hide();
        sRecDeed.readOnly = false;
        sRecMortgage.readOnly = false;
        sRecRelease.readOnly = false;
      }
  }
  
  // init comboboxes correctly
  function initComboboxes() {
    var $wrapper = $("#wrapper");
    var $footerCBs = $(".combobox").filter("[id^=Gfe2010Footer_dot_]");
    var $nonFooterCBs = $(".combobox").filter(":not([id^=Gfe2010Footer_dot_])");
       
    // Set combobox item lists in footer to fixed position when running in IE 10 or
    // rendering in standards mode (not Quirks mode) on IE9 or below
    if (document.documentMode >= 8) {
        $footerCBs.css("position", "fixed");
    }
    else {
        $nonFooterCBs.appendTo($wrapper);
    }
    
    // Add blank element to top of drop down
    $nonFooterCBs.each( function() {
        var	oRow = this.insertRow(0);
		oRow.onmouseover = function()	{	selectItem(this.rowIndex); };
		oRow.onclick = function()	{	onItemClick(this, event); };

		var	oCell	=	oRow.insertCell();
		oCell.style.width	=	"100%";
		
		oCell.innerHTML = "&zwnj;"; // zero-width non-joiner. Invisible to browser. Unlikely input.
    });
    
    // Hide drop down if FeeTypes are disabled, or in archive mode
    var HideComboboxArrows = <%= AspxTools.JsBool(HideComboboxArrows) %>;
    if(HideComboboxArrows)
    {
        $wrapper.find(".FeeTypeComboBox").next(".combobox_img").hide();
    }
    
    // Hide agent picker if in archive mode.
    var HidePaidToAgentPicker = <%= AspxTools.JsBool(HidePaidToAgentPicker) %>;
    if(HidePaidToAgentPicker)
    {
        $wrapper.find(".PaidToTextBox").next(".AgentPicker").hide();
    }
    
    // Set combobox text boxes to readonly if require fees from drop down box is checked.
    setBoxesReadOnly();
  }
  
  function setBoxesReadOnly()
  {
    var sIsRequireFeesFromDropDown = <%= AspxTools.JsGetElementById(sIsRequireFeesFromDropDown) %>; 
    var $feeTypeComboBoxes = $(".FeeTypeComboBox");
    
    if(sIsRequireFeesFromDropDown.checked){
        // text box readonly
        $feeTypeComboBoxes.prop("readOnly", true); // set text portion of combobox read only
        $feeTypeComboBoxes.each(function() { this.NotEditable = true; }); // Needed so drop down will still open
        $feeTypeComboBoxes.css("background-color", "white"); // removes grey (disabled) background
    } else {
        // text box not readonly
        $feeTypeComboBoxes.prop("readOnly", false);
        $feeTypeComboBoxes.each(function() { this.NotEditable = false; });
    }
  }
  
  function setPaidToReadOnly()
  {
    var sGfeRequirePaidToFromContacts = <%= AspxTools.JsGetElementById(sGfeRequirePaidToFromContacts) %>; 
    var $PaidToTextBox = $(".PaidToTextBox");
    
    var setReadOnly = ML.isLegacy ? sGfeRequirePaidToFromContacts.checked : !ML.sIsManuallySetThirdPartyAffiliateProps;
    $PaidToTextBox.prop("readOnly", setReadOnly);
    $PaidToTextBox.each(function() { EnableAgentPicker(this.id.substring(0, this.id.indexOf('_PaidTo_tb'))) }); // Needed so picker will still open
        
    // Set readonly based on other criteria
    verifyPaidToTBStatus();
    verifyLinkedPaidToTBStatus(<%=AspxTools.JsGetElementById(sGfeUsePaidToFromOfficialContact)%>);
  }
  
  function _agentPicked(id, recordId, legacyType)
  {   
    var args = new Object();
    args["loanid"] = ML.sLId;
    args["hfIsArchivePage"] = $("#hfIsArchivePage").val();
    args["RecordID"] = recordId;
    args["LegacyType"] = legacyType;
      
    var result = gService.loanedit.call("AgentPicked", args);
    
    if (!result.error) {
        // Skip sMipPiaProps since it doesn't make use of TP & AFF bits
        // NOTE: We still want to associate beneficiary, hence why we still run service call (OPM 209868)
        if( id == 'sMipPiaProps_ctrl' ) {
            return;
        }
    
        var tp = document.getElementById(id + '_TrdPty_hdn');
	    var aff = document.getElementById(id + '_Aff_hdn');
	    var QmWarn = document.getElementById(id + '_QmWarn_hdn');
        
        tp.value = result.value.TP;
        aff.value = result.value.AFF;
        QmWarn.value = result.value.QmWarn;
    }
  }
  
  function _clearAgent(id, legacyType)
  {
    var args = new Object();
    args["loanid"] = ML.sLId;
    args["hfIsArchivePage"] = $("#hfIsArchivePage").val();
    args["LegacyType"] = legacyType;
      
    var result = gService.loanedit.call("ClearAgent", args);
    
    // Note: this function does nothing outside of service call.
  }
  
    var gfepObj = {}; // page object.
    gfepObj.feeTypePropertySetter = new FeeTypePropertiesSetter($("#hfIsArchivePage").val());
    function setFeeTypeProperties(combobox, lineNumber) {
        if (lineNumber == "1200")
        {
            gfepObj.feeTypePropertySetter.setFeeTypeProperties(combobox, lineNumber, <%= AspxTools.JsString(E_GfeSectionT.B8.ToString("D")) %>);
        }
        else
        {
            gfepObj.feeTypePropertySetter.setFeeTypeProperties(combobox, lineNumber);
        }
    }
    
  var openedDate = <%=AspxTools.JsString(m_openedDate)%>;
  function onDateKeyUp(o, event) {
    if (event.keyCode == 79) {
      o.value = openedDate;
      updateDirtyBit(event);
    }
  }
  function toggleTimeInput() {
<%if(!IsReadOnly){ %>
    var bPrintTime = document.getElementById("sIsPrintTimeForGfeNoteIRAvailTillD").checked;
    
    document.getElementById("sGfeNoteIRAvailTillD_Time").readOnly = bPrintTime == false;
    document.getElementById("sGfeNoteIRAvailTillD_Time_minute").readOnly = bPrintTime == false;
    document.getElementById("sGfeNoteIRAvailTillD_Time_am").disabled = bPrintTime == false;
    document.getElementById("sGfeNoteIRAvailTillDTimeZoneT").disabled = bPrintTime == false;
    if (bPrintTime == false) 
    {
        document.getElementById("sGfeNoteIRAvailTillD_Time").value = "12";
        document.getElementById("sGfeNoteIRAvailTillD_Time_minute").value = "00";
        document.getElementById("sGfeNoteIRAvailTillD_Time_am").value = "AM";
        document.getElementById("sGfeNoteIRAvailTillDTimeZoneT").value = "0";

    }
    
    bPrintTime = document.getElementById("sIsPrintTimeForsGfeEstScAvailTillD").checked;
    
    document.getElementById("sGfeEstScAvailTillD_Time").readOnly = bPrintTime == false;
    document.getElementById("sGfeEstScAvailTillD_Time_minute").readOnly = bPrintTime == false;
    document.getElementById("sGfeEstScAvailTillD_Time_am").disabled = bPrintTime == false;
    document.getElementById("sGfeEstScAvailTillDTimeZoneT").disabled = bPrintTime == false;
    if (bPrintTime == false) 
    {
        document.getElementById("sGfeEstScAvailTillD_Time").value = "12";
        document.getElementById("sGfeEstScAvailTillD_Time_minute").value = "00";
        document.getElementById("sGfeEstScAvailTillD_Time_am").value = "AM";
        document.getElementById("sGfeEstScAvailTillDTimeZoneT").value = "0";
    
    }
<%} %>
<%else if(IsReadOnly){ %>
    document.getElementById("sGfeNoteIRAvailTillD_Time").readOnly = true;
    document.getElementById("sGfeNoteIRAvailTillD_Time_minute").readOnly = true;
    document.getElementById("sGfeNoteIRAvailTillD_Time_am").disabled = true;
    document.getElementById("sGfeNoteIRAvailTillDTimeZoneT").disabled = true;
    document.getElementById("sGfeEstScAvailTillD_Time").readOnly = true;
    document.getElementById("sGfeEstScAvailTillD_Time_minute").readOnly = true;
    document.getElementById("sGfeEstScAvailTillD_Time_am").disabled = true;
    document.getElementById("sGfeEstScAvailTillDTimeZoneT").disabled = true;
<%} %>

  }
  function verifyPaidToTBStatus()
  {
    unlockPaidTo('sTxServFProps_ctrl_ToBrok_chk');
    unlockPaidTo('sInspectFProps_ctrl_ToBrok_chk');
    unlockPaidTo('sProcFProps_ctrl_ToBrok_chk');
    unlockPaidTo('sUwFProps_ctrl_ToBrok_chk');
    unlockPaidTo('sWireFProps_ctrl_ToBrok_chk');
    unlockPaidTo('s800U1FProps_ctrl_ToBrok_chk');
    unlockPaidTo('s800U2FProps_ctrl_ToBrok_chk');
    unlockPaidTo('s800U3FProps_ctrl_ToBrok_chk');
    unlockPaidTo('s800U4FProps_ctrl_ToBrok_chk');
    unlockPaidTo('s800U5FProps_ctrl_ToBrok_chk');
    unlockPaidTo('sOwnerTitleInsFProps_ctrl_ToBrok_chk');
    unlockPaidTo('sDocPrepFProps_ctrl_ToBrok_chk');
    unlockPaidTo('sNotaryFProps_ctrl_ToBrok_chk');
    unlockPaidTo('sU1TcProps_ctrl_ToBrok_chk');
    unlockPaidTo('sU2TcProps_ctrl_ToBrok_chk');
    unlockPaidTo('sU3TcProps_ctrl_ToBrok_chk');
    unlockPaidTo('sU4TcProps_ctrl_ToBrok_chk');
    unlockPaidTo('sPestInspectFProps_ctrl_ToBrok_chk');
    unlockPaidTo('sU1ScProps_ctrl_ToBrok_chk');
    unlockPaidTo('sU2ScProps_ctrl_ToBrok_chk');
    unlockPaidTo('sU3ScProps_ctrl_ToBrok_chk');
    unlockPaidTo('sU4ScProps_ctrl_ToBrok_chk');
    unlockPaidTo('sU5ScProps_ctrl_ToBrok_chk');
  }
  
  function verifyLinkedPaidToTBStatus(cb) {
    var i, id, cb, tb, tbID;
    var sGfeRequirePaidToFromContacts = <%= AspxTools.JsGetElementById(sGfeRequirePaidToFromContacts) %>;
    var setReadOnly = ML.isLegacy ? sGfeRequirePaidToFromContacts.checked : !ML.sIsManuallySetThirdPartyAffiliateProps;
        
    for (i in linkedPaidTo) {
      tbID = linkedPaidTo[i];
      tb = document.getElementById(tbID);
      id = tbID.substring(0, tbID.indexOf('_PaidTo_tb'));
      Brok_cb = document.getElementById(id + '_ToBrok_chk');
      
      tb.readOnly = setReadOnly || Brok_cb.checked || cb.checked;
      
      if (Brok_cb.checked || cb.checked)
      {
        DisableAgentPicker(id);
      } 
    }
  }
  
  function unlockPaidTo(id) {
    tbID = id.substring(0, id.indexOf('_ToBrok_chk')) + '_PaidTo_tb';
    var LinkCbChecked = <%= AspxTools.JsGetElementById(sGfeUsePaidToFromOfficialContact) %>.checked;
    var sGfeRequirePaidToFromContacts = <%= AspxTools.JsGetElementById(sGfeRequirePaidToFromContacts) %>;
    var setReadOnly = ML.isLegacy ? sGfeRequirePaidToFromContacts.checked : !ML.sIsManuallySetThirdPartyAffiliateProps;
    var tb = document.getElementById(tbID);
    var cb = document.getElementById(id);
    
    if(!LinkCbChecked || !checkLinkedPaidTo(tbID))
    {
      tb.readOnly = setReadOnly || cb.checked;
      
      if(cb.checked)
      {
          DisableAgentPicker(id.substring(0, id.indexOf('_ToBrok_chk')));
      }
    }
  }
  
  function checkLinkedPaidTo(tbID) {
    var i = "";
    for (i in linkedPaidTo) {
      if (tbID == linkedPaidTo[i])
        return true;
    }
    return false;
  }
  
  function f_setFinancedCredits()
  {
    
    // <%--// OPM 180309.  PDE wants UI to reflect financing of the MI. Rely on asp.net name not change. --%>
    
    if ( <%= AspxTools.JsBool(!m_isVaLoan && m_isFfUfMipIsBeingFinanced) %> ) // nonVaFinanced
    {
        var ddl = document.getElementById( 'sMipPiaProps_ctrl_PdByT_dd');
        ddl.selectedIndex = 1;
        disableDDL( ddl, true );
    }
    
    if ( <%= AspxTools.JsBool(m_isVaLoan && m_isFfUfMipIsBeingFinanced) %> ) // vaFinanaced
    {
        var ddl = document.getElementById( 'sVaFfProps_ctrl_PdByT_dd');
        ddl.selectedIndex = 1;
        disableDDL( ddl, true ); 
    }
  }
  
  function doAfterDateFormat(e) 
  {
      if(e.id === "sGfeInitialDisclosureD" ||
          e.id === "sGfeRedisclosureD" ||
          e.id === "sEstCloseD" || 
          e.id === "sSchedDueD1" ||
          e.id === "sGfeNoteIRAvailTillD")
      {
          refreshCalculation();
      }
  }
  function getPaymentSource()
  {
    // THere are 3 options.
    for (var i = 0; i < 3; i++)
    {
      var o = document.getElementById("sOriginatorCompensationPaymentSourceT_" + i);
      if (null != o && o.checked)
      {
        return o.value;
      }
    }
    return "";
  }
  function updateOriginatorCompensationUI()
  {
    <% if (IsReadOnly == false) { %>
    
    var paidBy = getPaymentSource(); // 0 - not specified, 1 - borrower, 2 - lender
    var isChannelBrokered = <%= AspxTools.JsBool(m_isChannelBrokered) %>;
    var isChannelCorrespondent = <%= AspxTools.JsBool(m_isChannelCorrespondent) %>;
    var isTpo = <%= AspxTools.JsGetElementById(sGfeIsTPOTransaction) %>.checked;
    
    var disableOrigComp = paidBy === '2' && !(isTpo && (isChannelBrokered || isChannelCorrespondent));
    
    <%= AspxTools.JsGetElementById(sGfeOriginatorCompFPc) %>.readOnly =  disableOrigComp;
    disableDDL(<%= AspxTools.JsGetElementById(sGfeOriginatorCompFBaseT) %>, disableOrigComp);
    <%= AspxTools.JsGetElementById(sGfeOriginatorCompFMb) %>.readOnly = disableOrigComp;
    
    document.getElementById("of_label").style.visibility = <%= AspxTools.JsGetElementById(sGfeOriginatorCompFBaseT) %>.value == '6' ? 'hidden' : 'visible';
    var sGfeOriginatorCompFBaseT = <%= AspxTools.JsGetElementById(sGfeOriginatorCompFBaseT) %>;
    if (paidBy === "0" || paidBy === "1")
    {

      if (paidBy === "0")
      {
        var bHasAllYsp = false;
        for (var i = 0; i < sGfeOriginatorCompFBaseT.options.length; i++)
        {
          if (sGfeOriginatorCompFBaseT.options[i].value === '6')
          {
            bHasAllYsp = true;
            break;
          }
        }
        
        if (bHasAllYsp === false)
        {
          var opt = document.createElement("option");
          opt.text = "All YSP";
          opt.value = "6";
          sGfeOriginatorCompFBaseT.options.add(opt);
        }
      }
      else if (paidBy === "1")
      {
        for (var i = 0; i < sGfeOriginatorCompFBaseT.options.length; i++)
        {
          var opt = sGfeOriginatorCompFBaseT.options[i]
          if (opt.value === '6')
          {
            sGfeOriginatorCompFBaseT.options.remove(i);
            break;
          }
        }
      }
    } else if (paidBy === "2")
    {
        for (var i = 0; i < sGfeOriginatorCompFBaseT.options.length; i++)
        {
          var opt = sGfeOriginatorCompFBaseT.options[i]
          if (opt.value === '6')
          {
            sGfeOriginatorCompFBaseT.options.remove(i);
            break;
          }
        }
    
    }
    <% } %>
  }
  function allowGFEArchive()
    {
        if(isDirty())
        {
            alert('Please save the page before attempting to archive');
            return false;
        }
        else
        {
            return true;
        }
    }
    <%if(Broker.IsGFEandCoCVersioningEnabled && !IsLeadPage) {%>
    
        <%if(!IsArchivePage){ %>
        function insertAndSelectGFEArchiveDropDown(newestDate)
        {
          
          var ddl = <%=AspxTools.JsGetElementById(sLastDisclosedGFEArchiveD)%>;
          var option=document.createElement("option");
          option.text = newestDate;
          option.value = newestDate;
          
          ddl.add(option, 0);  //prepend it.
          
          ddl.selectedIndex = 0;
        }
        <%} %>
        <%if(IsArchivePage){ %>
         function insertAndSelectGFEArchiveDropDown(newestDate, framef)
         {
            framef.location.reload();
         }
        <%} %>
        
    <%} %>
    function document_keyDown(event)
    {
       if(event.keyCode == 18) //alt key
       {
            bAltDown = true;
       }
       else if(!event.altKey) //don't get stuck with bAltDown indefinitely
       {
            bAltDown = false;
       }
       else if(event.keyCode == 67 && bAltDown) //"c" key
       {
            if(document.getElementById("ByPassBgCalcForGfeAsDefault_1").checked)
            {
                backgroundCalculation();
            }
       }

    }
    function document_keyUp(event)
    {
        if(event.keyCode == 18) //alt key
        {
            bAltDown = false;
        }
    }
	
    <%if(!IsArchivePage){ %>
	function displayCCOptions()
	{
	    document.getElementById('ccTable').style.display = <%=AspxTools.JsGetElementById(sIsShowsIsPreserveGFEFees)%>.value == "1" ? 'block' : 'none';
	}
    <%} %>
	
    <%if(IsArchivePage){ %>
    refreshCalculation = function(){};
    saveMe = function(){};
    saveIfModified = function(){};
    backgroundCalculation = function(){};
    <%} %>
//-->
		</script>
	<form id="GoodFaithEstimate" method="post" runat="server">
	    <div id="wrapper">
		<asp:HiddenField runat="server" ID="hfIsArchivePage" />
		<asp:HiddenField runat="server" ID="hfDoWindowRefresh" />
		<asp:HiddenField runat="server" ID="hfsLPurposeT" />
		<asp:HiddenField runat="server" ID="sIsShowsIsPreserveGFEFees" Value="0" />
			<TABLE id="Table2" cellSpacing="0" cellPadding="0" border="0">
				<TR class="ShowForClosingCostMigrationArchive">
		    <% if (IsClosingCostMigrationArchivePage)
           { %>
                    <td class="MainRightHeader" noWrap><%= AspxTools.HtmlString(ClosingCostMigrationArchiveTitle) %></td>
		    <% }
           else if (IsArchivePage)
           { %>
                    <TD class="MainRightHeader" noWrap>GFE Archive</TD>
            <%}
           else if (false == IsLeadPage)
           { %>
					<TD class="MainRightHeader" noWrap>2010 Good Faith Estimate</TD>
					<%}
           else
           { %>
					<TD class="MainRightHeader" noWrap>Initial Fees Worksheet</TD>
					<%} %>
				</TR>
				<%if (!IsArchivePage)
                  { %>
				<TR>
				    <TD>
				    <table class="InsetBorder" cellspacing="0" cellpadding="0" border="0">
				        <tr>
				            <td noWrap>
				                <input type="button" onclick="openPopup('newlos/Disclosure/SafeHarborDisclosureAdjustable.aspx','SHDA' );" value="Edit adjustable rate anti-steering disclosure form"/>
		                        <input type="button" onclick="openPopup('newlos/Disclosure/SafeHarborDisclosureFixed.aspx','SHDF');" value="Edit fixed rate anti-steering disclosure form" id="bEditFixed" />
		                    </td>
		                </tr>
		            </table>
		            </TD>
		        </TR>
		        <%} %>
		        <asp:PlaceHolder runat="server" ID="phGFEArchiveRow">
		        <tr>
		            <td>
		                <table class="InsetBorder" cellspacing="0" cellpadding="0" border="0">
				            <tr>
				                <td class="FieldLabel" noWrap>
				                    <asp:Button runat="server" ID="bRecordGFEToArchive" Text="Record GFE data to archive" OnClick="RecordGFEToArchive" OnClientClick="return allowGFEArchive();"/>
				                    Last Disclosed GFE archive: <asp:DropDownList runat="server" ID="sLastDisclosedGFEArchiveD"></asp:DropDownList>
		                            
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		        </asp:PlaceHolder>
		        <asp:PlaceHolder runat="server" ID="phSelectGFERow">
		        <script type="text/javascript">
		            function openGFEPDF() {
                        var args = encodeURI('GFEArchiveDate=' + <%=AspxTools.JsGetElementById(ddlGFEArchives) %>.value);
                        var url = gVirtualRoot + '/pdf/GoodFaithEstimate2010Archive.aspx?loanid=' + ML.sLId + "&" + args;
                        LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
		                return false;

		            }
		        </script>
		        <tr><td><table><tr>
		            <td class="FieldLabel">
		               Select Archived GFE: 
		               <asp:DropDownList runat="server" ID="ddlGFEArchives" AutoPostBack="true" AlwaysEnable></asp:DropDownList> 
		               <input type="button" id="bPrintGFE" value="Print Archived GFE" onclick="openGFEPDF()" AlwaysEnable/>
		            </td>
		        </tr></table></td></tr>
		        </asp:PlaceHolder>
		        <tr>
		        <td>
		             <TABLE class=InsetBorder id=Table12 cellSpacing=0 cellPadding=0 
      border=0>
        <TR>
        <td>
        <table id=DisclosureAndRedisclosureDateTable cellSpacing=0 cellPadding=0 border=0 >
        <tr>
         <td class=FieldLabel nowrap>
          Initial GFE Disclosure Date
         </td>
         <td>
          <ml:datetextbox id=sGfeInitialDisclosureD runat="server" width="75" preset="date" CssClass="mask" onchange="date_onblur(null, this);" ></ml:datetextbox>
         </td>
         <td>&nbsp;</td>
         <td class=FieldLabel nowrap>
          GFE Redisclosure Date
         </td>
         <td>
          <ml:datetextbox id=sGfeRedisclosureD runat="server" width="75" preset="date" CssClass="mask" onchange="date_onblur(null, this);" ></ml:datetextbox>
         </td>
          <%--<TD>
            
            <TABLE id=Table19 cellSpacing=0 cellPadding=0 border=0 >
            
            </TABLE>
          </TD>--%>
          </tr>
          </table>
          </td>
        </TR>
      </TABLE>
    </TD>
  </TR>
				<TR>
					<TD noWrap>
						<table class="InsetBorder" id="Table6" cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td nowrap>
									<table id="Table3" cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td class="FieldLabel" nowrap colspan="6">Perform calculations:<asp:RadioButtonList id="ByPassBgCalcForGfeAsDefault" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
													<asp:ListItem Value="0">automatically</asp:ListItem>
													<asp:ListItem Value="1">manually</asp:ListItem>
												</asp:RadioButtonList>
												<input disabled onclick="backgroundCalculation();" tabindex="-1" type="button" value="Recalculate  (Alt + C)" name="btnCalculate" id="btnCalculate"/></td>
										</tr>
										<tr style="PADDING-TOP:10px">
											<td class=FieldLabel nowrap colspan="7">
												<uc:CFM ID="CFM" runat="server" />
											</td>
										</tr>
										<tr>
											<td class="FieldLabel" nowrap>Prepared By</td>
											<td class="FieldLabel" nowrap colspan="5"><asp:textbox id="GfeTilCompanyName" runat="server" Width="258px" /></td>
										</tr>
										<tr>
											<td class="FieldLabel" nowrap>Address</td>
											<td class="FieldLabel" nowrap colspan="5"><asp:textbox id="GfeTilStreetAddr" runat="server" Width="254px" /></td>
										</tr>
										<tr>
											<td class="FieldLabel" nowrap></td>
											<td class="FieldLabel" nowrap colspan="5"><asp:textbox id="GfeTilCity" runat="server" /><ml:statedropdownlist id="GfeTilState" runat="server" EnableViewState="False"></ml:statedropdownlist><ml:zipcodetextbox id="GfeTilZip" runat="server" width="50" preset="zipcode" EnableViewState="False" /></td>
										</tr>
										<tr>
											<td class="FieldLabel" nowrap>Phone</td>
											<td class="FieldLabel" nowrap colspan="5"><ml:phonetextbox id="GfeTilPhoneOfCompany" runat="server" width="120" preset="phone" />&nbsp;
											Email <asp:textBox ID="GfeTilEmailAddr" runat="server" Width="200" EnableViewState="false"></asp:textBox></td>
										</tr>
										<tr runat="server" id="trPreparedDate" visible="true">
											<td class="FieldLabel" nowrap>Prepared Date</td>
											<td class="FieldLabel" nowrap colspan="5"><ml:datetextbox id="GfeTilPrepareDate" onkeyup="onDateKeyUp(this, event);" runat="server" width="75" preset="date" CssClass="mask" EnableViewState="False"></ml:datetextbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" style="HEIGHT: 18px" nowrap>Loan Program</td>
											<td nowrap colspan="5"><asp:textbox id="sLpTemplateNm" runat="server" Width="432px" /></td>
										</tr>
										<tr>
											<td class="FieldLabel" nowrap>CC Template</td>
											<td nowrap colspan="5"><asp:textbox id="sCcTemplateNm" runat="server" Width="432px" />
												<input onclick="findCCTemplate();" type="button" value="Find Closing Cost Template ..."></td>
										</tr>
										<asp:PlaceHolder runat="server" ID="TitlePlaceHolder">
                                        <tr>
                                            <td>
                                            </td>
                                            <td colspan="5" id="GetRecordingChargesTransferTaxesAndTitleQuoteContainer" runat="server" visible = "false">
                                                <input onclick="quoteRequest();" type="button" value="Get Recording Charges, Transfer Taxes, and Title Quote...">
                                                <ml:PassthroughLiteral runat="server" ID="QuoteText"></ml:PassthroughLiteral>
                                            </td>
                                        </tr>
                                        </asp:PlaceHolder>
									</table>
								</td>
							</tr>
						</table>
					</TD>
				</TR>
				<TR>
					<TD noWrap></TD>
				</TR>
				<TR>
					<TD>
					    <TABLE class="InsetBorder">
					        <TR>
					            <TD>
                        <table>
                          <tr>
                            <td class="FieldLabel">Total Loan Amt</td>
                            <td><ml:MoneyTextBox ID="sFinalLAmt" runat="server" Width="90" preset="money" ReadOnly="True" /></td>
                          </tr>
                          <tr>
                            <td class="FieldLabel">Interest Rate</td>
                            <td>
                              <ml:PercentTextBox ID="sNoteIR" runat="server" Width="70" preset="percent" onchange="refreshCalculation();" /></td>
                          </tr>
                          <tr>
                            <td class="FieldLabel">Term/Due</td>
                            <td>
                              <asp:TextBox ID="sTerm" runat="server" Width="38px" onchange="refreshCalculation();" MaxLength="3" />
                              /
                              <asp:TextBox ID="sDue" runat="server" Width="38px" onchange="refreshCalculation();" MaxLength="3" /></td>
                          </tr>
                          <tr>
                            <td class="FieldLabel">Amort Type</td>
                            <td>
                              <asp:DropDownList ID="sFinMethT" runat="server" onchange="refreshCalculation();" />
                            </td>
                          </tr>
                        </table>
					            </TD>
					            <TD style="vertical-align:top">
					                <TABLE>
    					                <TR>
    					                    <TD class=FieldLabel>
    					                        <label class="AlignLabelForLockCB">1st Payment Date</label>
        					                    <asp:CheckBox runat="server" ID="sSchedDueD1Lckd" Text="Lock" onclick="lockField(this,'sSchedDueD1'); refreshCalculation();"/>
					                        </TD>
                                            <TD><ml:datetextbox id=sSchedDueD1 runat="server" width="75" preset="date" CssClass="mask"></ml:datetextbox></TD>
    					                </TR>
    					                <TR>
    					                    <TD class=FieldLabel>
                                                <label class="AlignLabelForLockCB">Estimated Closing Date</label>
        					                    <asp:CheckBox runat="server" ID="sEstCloseDLckd" Text="Lock" onclick="lockField(this,'sEstCloseD'); refreshCalculation();"/>
    					                    </TD>
                                            <TD><ml:datetextbox id=sEstCloseD runat="server" width="75" preset="date" CssClass="mask"></ml:datetextbox></TD>
    					                </TR>
    					                <tr>
    					                  <td class="FieldLabel">
                                            <label class="AlignLabelForLockCB">Consummation Date</label>
					                        <asp:CheckBox runat="server" ID="sConsummationDLckd" Text="Lock" onclick="lockField(this,'sConsummationD'); refreshCalculation();" /></td>
    					                  <td><ml:DateTextBox ID="sConsummationD" runat="server" width="75"  ReadOnly="true" /></td>
    					                </tr>
    					                <tr>
    					                    <td class="FieldLabel">Days in Year</td>
    					                    <td><asp:textbox id="sDaysInYr" runat="server" Width="50px" onchange="refreshCalculation();" MaxLength="3" /></td>
    					                </tr>
					                </TABLE>
					            </TD>
                            </TR>
                            <TR>
                                <TD>
                                    <table class="InsetBorder">
                                        <tr>
                                            <td class=FormTableSubHeader colSpan="4">Adjustable Rate Mortgage - Rate Adjustment</td>
                                        </tr>
                                        <tr>
                                            <td class=FieldLabel>1st Adj Cap</td>
                                            <td style="width:90px"><ml:percenttextbox id=sRAdj1stCapR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td>
                                            <td class=FieldLabel>Adj Cap</td>
                                            <td><ml:percenttextbox id=sRAdjCapR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td>
                                        </tr>
                                        <tr>
                                            <td class=FieldLabel>1st Change</td>
                                            <td><asp:textbox id=sRAdj1stCapMon runat="server" Width="42px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox>&nbsp;mths</td>
                                            <td class=FieldLabel>Adj Period</td>
                                            <td><asp:textbox id=sRAdjCapMon runat="server" Width="42px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox> mths</td>
                                        </tr>
                                        <tr>
                                            <td class=FieldLabel>Life Adj Cap</td>
                                            <td><ml:percenttextbox id=sRAdjLifeCapR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table class=InsetBorder>
                                        <tr>
                                            <td class=FormTablesubheader colSpan="4">ARM Potential Negative Amort. - Payment Adjustment</td>
                                        </tr>
                                        <tr>
                                            <td class=FieldLabel>Adj Cap</td>
                                            <td style="width:90px"><ml:percenttextbox id=sPmtAdjCapR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td>
                                            <td class=FieldLabel>Recast Pd</td>
                                            <td><asp:textbox id=sPmtAdjRecastPeriodMon runat="server" Width="42px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox>&nbsp;mths</td>
                                        </tr>
                                        <tr>
                                            <td class=FieldLabel>Adj Period</td>
                                            <td><asp:textbox id=sPmtAdjCapMon runat="server" Width="42px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox>&nbsp;mths</td>
                                            <td class=FieldLabel>Recast Stop</td>
                                            <td><asp:textbox id=sPmtAdjRecastStop runat="server" Width="42px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox>&nbsp;mths</td>
                                        </tr>
                                        <tr>
                                            <td class=FieldLabel>Max Balance Pc</td>
                                            <td><ml:percenttextbox id=sPmtAdjMaxBalPc runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td>
                                        </tr>
                                    </table>
                                </TD>
                            </TR>
                            <TR>
                                <TD><strong>Interest Only Months</strong> <asp:textbox id=sIOnlyMon runat="server" Width="36" onchange="refreshCalculation();" MaxLength="3"></asp:textbox> mths</TD>
                            </TR>
					    </TABLE>
					</TD>
				</TR>
				<TR>
				    <TD noWrap></TD>
				</TR>
				<TR>
					<TD>
					    <TABLE class="InsetBorder">
					        <TR class=FieldLabel>
					            <TD colspan="3">1. The interest rate for this GFE is available through <asp:CheckBox ID="sGfeNoteIRAvailTillDLckd" runat="server" Text="Lock" onclick="lockField(this,'sGfeNoteIRAvailTillD'); refreshCalculation();"></asp:CheckBox><ml:datetextbox id="sGfeNoteIRAvailTillD" runat="server" width="75" preset="date" CssClass="mask" onchange="date_onblur(null, this);" /> <asp:CheckBox ID="sIsPrintTimeForGfeNoteIRAvailTillD" runat="server" Text="Time" onclick="toggleTimeInput();"/><ml:TimeTextBox ID="sGfeNoteIRAvailTillD_Time" runat="server" />
					            <asp:DropDownList ID="sGfeNoteIRAvailTillDTimeZoneT" runat="server" /></TD>
					        </TR>
					        <TR class=FieldLabel>
					            <TD colspan="3">2. This estimate for all other settlement charges is available <asp:CheckBox ID="sGfeEstScAvailTillDLckd" runat="server" Text="Lock" onclick="lockField(this,'sGfeEstScAvailTillD');refreshCalculation();"></asp:CheckBox> <ml:datetextbox id="sGfeEstScAvailTillD" runat="server" width="75" preset="date" CssClass="mask" /> <asp:CheckBox ID="sIsPrintTimeForsGfeEstScAvailTillD" runat="server" Text="Time"  onclick="toggleTimeInput();"/><ml:TimeTextBox ID="sGfeEstScAvailTillD_Time" runat="server" />
					            <asp:DropDownList ID="sGfeEstScAvailTillDTimeZoneT" runat="server" />
					            </TD>
					        </TR>
					        <TR class=FieldLabel>
					            <TD colspan="3">3. The rate lock period is <asp:CheckBox runat="server" ID="sGfeRateLockPeriodLckd" Text="Lock" onclick="lockField(this, 'sGfeRateLockPeriod'); refreshCalculation();"/><asp:textbox id="sGfeRateLockPeriod" runat="server" Width="36" onchange="refreshCalculation();" MaxLength="3"></asp:textbox> days.</TD>
					        </TR>
					        <TR class=FieldLabel>
					            <TD colspan="3">4. The rate must be locked <asp:textbox id="sGfeLockPeriodBeforeSettlement" runat="server" Width="36" onchange="refreshCalculation();" MaxLength="3"></asp:textbox> days before settlement.</TD>
					        </TR>
					        <TR>
					            <TD class=FieldLabel>Can the interest rate rise?</TD>
					            <TD><asp:RadioButtonList id="sGfeCanRateIncrease" runat="server" Enabled="false" RepeatDirection="Horizontal">
										<asp:ListItem Value="0">No</asp:ListItem>
										<asp:ListItem Value="1">Yes</asp:ListItem>
									</asp:RadioButtonList></TD>
					            <TD>it can rise to a maximum of <ml:percenttextbox id="sRLifeCapR" ReadOnly="true" runat="server" preset="percent" width="70"></ml:percenttextbox>. 
					                The first change will be in <asp:textbox id="sGfeFirstInterestChangeIn" ReadOnly="true" runat="server" Width="60" MaxLength="3"></asp:textbox>.</TD>
					        </TR>
					        <TR>
					            <TD class=FieldLabel>Can the loan balance rise?</TD>
					            <TD><asp:RadioButtonList id="sGfeCanLoanBalanceIncrease" Enabled="false" runat="server" RepeatDirection="Horizontal">
										<asp:ListItem Value="0">No</asp:ListItem>
										<asp:ListItem Value="1">Yes</asp:ListItem>
									</asp:RadioButtonList></TD>
					            <TD>it can rise to a maximum of <ml:moneytextbox id="sGfeMaxLoanBalance" ReadOnly="true" runat="server" preset="money" width="76px"></ml:moneytextbox>.</TD>
					        </TR>
					        <TR>
					            <TD class=FieldLabel style="vertical-align:top">Can the monthly amount rise?</TD>
					            <TD style="vertical-align:top"><asp:RadioButtonList id="sGfeCanPaymentIncrease" Enabled="false" runat="server" RepeatDirection="Horizontal">
										<asp:ListItem Value="0">No</asp:ListItem>
										<asp:ListItem Value="1">Yes</asp:ListItem>
									</asp:RadioButtonList></TD>
					            <TD>the first increase can be in <asp:textbox id="sGfeFirstPaymentChangeIn" runat="server" ReadOnly="true" Width="60" MaxLength="3"></asp:textbox> 
					                and the monthly amount owed can rise to <ml:moneytextbox ReadOnly="true" id="sGfeFirstAdjProThisMPmtAndMIns" runat="server" preset="money" width="76px"></ml:moneytextbox>.<br />
					                The maximum it can ever rise <ml:moneytextbox id="sGfeMaxProThisMPmtAndMIns" ReadOnly="true" runat="server" preset="money" width="76px"></ml:moneytextbox>.</TD>
					        </TR>
					        <TR>
					            <TD class=FieldLabel>Is there a prepay penalty?</TD>
					            <TD><asp:RadioButtonList id="sGfeHavePpmtPenalty" onclick="lockField(document.getElementById('sGfeHavePpmtPenalty_1'), 'sGfeMaxPpmtPenaltyAmt')" runat="server" RepeatDirection="Horizontal">
										<asp:ListItem Value="0">No</asp:ListItem>
										<asp:ListItem Value="1">Yes</asp:ListItem>
									</asp:RadioButtonList></TD>
					            <TD>the maximum prepayment <ml:moneytextbox id="sGfeMaxPpmtPenaltyAmt" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox>.</TD>
					        </TR>
					        <TR>
					            <TD class=FieldLabel>Is there a balloon?</TD>
					            <TD><asp:RadioButtonList id="sGfeIsBalloon" runat="server" RepeatDirection="Horizontal">
										<asp:ListItem Value="0">No</asp:ListItem>
										<asp:ListItem Value="1">Yes</asp:ListItem>
									</asp:RadioButtonList></TD>
					            <TD>the balloon payment of <ml:moneytextbox id="sGfeBalloonPmt" ReadOnly="true" runat="server" preset="money" width="76px"></ml:moneytextbox> 
					                due in <asp:textbox id="sGfeBalloonDueInYrs" ReadOnly="true" runat="server" Width="36" MaxLength="3"></asp:textbox> yrs.</TD>
					        </TR>
					        <TR>
					            <TD class=FieldLabel>Is an escrow account required?</TD>
					            <TD><asp:RadioButtonList id="sMldsHasImpound" runat="server" RepeatDirection="Horizontal">
										<asp:ListItem Value="0">No</asp:ListItem>
										<asp:ListItem Value="1">Yes</asp:ListItem>
									</asp:RadioButtonList></TD>
					        </TR>
					    </TABLE>
					</TD>
				</TR>


				<tr>
				    <td >
				        <table class="InsetBorder" id="ccTable" style="display: none">
				            <tr class="FormTableHeader">
				                <td>
				                    Closing Cost Automation Options
				                </td>
				            </tr>
				            <tr>
				                <td>
				                    <span style="color: Red">Conditions used to previously set the closing costs are no longer true.  How should the pricing engine handle fees based on these conditions?</span>
				                    <asp:RadioButtonList id="sIsPreserveGFEFees" RepeatDirection="Vertical" RepeatLayout="Table" runat="server">
				                        <asp:ListItem Value="0">Use automation to update the fees whose conditions no longer apply.</asp:ListItem>
				                        <asp:ListItem Value="1">Use closing costs currently on the loan file for similar scenarios.</asp:ListItem>
				                    </asp:RadioButtonList>
				                </td>
				            </tr>
				        </table>
				    </td>
				</tr>
				
				
				<TR>
				    <TD noWrap></TD>
				</TR>
				<TR class="ShowForClosingCostMigrationArchive">
					<TD noWrap>
						<table id="Table5" cellspacing="0" cellpadding="0" border="0" class="InsetBorder">
							<tr>
								<td>
									<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0">
									    <TR class="FieldLabel HideForClosingCostMigrationArchive">
									        <TD colspan="4" >
									        </TD>
									        <TD colspan="8" align="right">
									            <asp:CheckBox ID="sGfeRequirePaidToFromContacts" CssClass="sGfeRequirePaidToFromContacts" Text="Require �Paid To� to be selected from the contact list" onclick="setPaidToReadOnly();refreshCalculation();" runat="server"/>
									        </TD>
									        <td></td>
									    </TR>
									    
									    <TR class="FieldLabel HideForClosingCostMigrationArchive">
									        <TD colspan="4" >
									            <asp:CheckBox ID="sIsRequireFeesFromDropDown" CssClass="sIsRequireFeesFromDropDown" Text="Require custom fees to be selected from the drop-down list" onclick="setBoxesReadOnly();refreshCalculation();" runat="server"/>
									        </TD>
									        <TD colspan="8" align="right"><asp:CheckBox ID="sGfeUsePaidToFromOfficialContact" Text="Link the &quot;Paid To&quot; to the Offical Contact List" onclick="verifyLinkedPaidToTBStatus(this); refreshCalculation();" runat="server"/></TD>
									        <td></td>
									    </TR>
									    <tr class="FormTableHeader HideForClosingCostMigrationArchive">
									      <td></td>
									      <td colspan="12">Loan originator compensation source</td>
									    </tr>
									    <tr class="HideForClosingCostMigrationArchive">
									      <td></td>
									      <td colspan="11" class="FieldLabel" style="padding-top:5px;padding-bottom:5px">Loan originator is paid by
									      <asp:RadioButtonList ID="sOriginatorCompensationPaymentSourceT" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" onclick="updateOriginatorCompensationUI(); refreshCalculation();"/>
									      </td>
									    </tr>
										<TR>
											<TD class="FormTableHeader" colSpan="13" align="center">
											    BF = Bona Fide&nbsp;&nbsp;&nbsp;
											    A = APR&nbsp;&nbsp;&nbsp;
											    F = FHA Allowable&nbsp;&nbsp;&nbsp;
											    POC = Paid Outside of Closing&nbsp;&nbsp;&nbsp;
											    PD = Paid&nbsp;&nbsp;&nbsp;
											    B = Paid to broker</TD>
										</TR>
										<TR class="FormTableHeader">
											<TD ></TD>
											<TD >Description of Charge</TD>
											<TD ></TD>
											<TD >Amount</TD>
											<TD >Page 2</TD>
											<TD >Paid By</TD>
											<TD  colspan="5"></TD>
											<TD >Paid To</TD>
											<td></td>
										</TR>
										<TR>
											<TD class="FormTableSubheader">800</TD>
											<TD class="FormTableSubheader" colSpan="12">
												ITEMS PAYABLE IN CONNECTION WITH LOAN</TD>
										</TR>
										<TR>
											<TD class="FieldLabel">801</TD>
											<TD class="FieldLabel">Loan origination fee</TD>
											<TD class="FieldLabel"><ml:percenttextbox autofocus="focus" id="sLOrigFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();" EnableViewState="False"></ml:percenttextbox>
												+
												<ml:moneytextbox id="sLOrigFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="59" onchange="refreshCalculation();" EnableViewState="False"></ml:moneytextbox></TD>
											<TD><ml:moneytextbox id="sLOrigF" runat="server" preset="money" width="77" ReadOnly="True" /></TD>
											<uc1:goodfaithestimate2010rightcolumn id="sLOrigFProps_ctrl" LegacyGfeFieldT="sLOrigF" Page2Value="A1" PocCBVisible="false" PaidToTBVisible="false" PaidToBCBVisible="true" BFVisible="true" GBFVisible="false" runat="server" />
										</TR>
										<tr>
										  <td></td>
										  <td class="FieldLabel">Originator compensation</td>
										  <td class="FieldLabel"><ml:PercentTextBox ID="sGfeOriginatorCompFPc" runat="server" preset="percent" Width="70" onchange="refreshCalculation();" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" />
										  <span id="of_label">of</span>
										  <asp:DropDownList ID="sGfeOriginatorCompFBaseT" runat="server" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" onchange="refreshCalculation();" />
										  +
										  <ml:MoneyTextBox ID="sGfeOriginatorCompFMb" runat="server" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" preset="money" Width="59" onchange="refreshCalculation();" />
										  </td>
										  <td><ml:MoneyTextBox ID="sGfeOriginatorCompF" runat="server" ReadOnly="true" Width="77"/></td>
										  <uc1:GoodFaithEstimate2010RightColumn ID="sGfeOriginatorCompFProps_ctrl" LegacyGfeFieldT="sGfeOriginatorCompF" Page2Value="A1" PocCBVisible="false" PaidToTBVisible="false" PaidToBCBVisible="true" BFVisible="true" GBFVisible="false" runat="server" />
										</tr>
										<TR>
											<TD class="FieldLabel">802</TD>
											<TD class="FieldLabel">Credit (-) or Charge (+)</TD>
											<TD class="FieldLabel" noWrap>
											    <div id="creditChargeBreakdown" runat="server" >
										            <ml:percenttextbox id="sLDiscntPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();" />
										            &nbsp;of
											        <asp:dropdownlist id="sLDiscntBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
											        +
											        <ml:moneytextbox id="sLDiscntFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="59" onchange="refreshCalculation();" /></ml:moneytextbox>
                                                </div>
											</TD>
											<TD><ml:moneytextbox id="sLDiscnt" runat="server" preset="money" width="76px" ReadOnly="True" /></TD>
											<td>
											    <input type="radio" value="A2" checked="checked" disabled="disabled" />
											    <label>A2</label>
											</td>
										</TR>
										<tr>
										  <td>&nbsp;</td>
										  <td class="FieldLabel">Credit for lender paid fees</td>
										  <td align="right" style="padding-right:7px">
										    <asp:DropDownList ID="sGfeCreditLenderPaidItemT" runat="server" onchange="refreshCalculation();"/>
										    <ml:MoneyTextBox ID="sGfeCreditLenderPaidItemF" width="76px" runat="server" ReadOnly="true" />
										  </td>
										</tr>
										
										<tr>
										  <td>&nbsp;</td>
										  <td class="FieldLabel">General Lender credit</td>
										  <td align="right" style="padding-right:7px">
										    <ml:PercentTextBox ID="sGfeLenderCreditFPc" runat="server" preset="percent" Width="76" ReadOnly="true" />
										    <ml:MoneyTextBox ID="sGfeLenderCreditF" runat="server" preset="money" Width="76px" ReadOnly="true" />
										  </td>
										  <td>&nbsp;</td>
										  <uc1:GoodFaithEstimate2010RightColumn ID="sGfeLenderCreditFProps_ctrl" Page2A_rbVisible="false" runat="server" PaidToTBVisible="false" PocCBVisible="false" PdByTDDVisible="false"/>
										</tr>
										<tr>
										  <td>&nbsp;</td>
										  <td class="FieldLabel">Discount points</td>
										  <td align="right" style="padding-right:7px">
										    <ml:PercentTextBox ID="sGfeDiscountPointFPc" runat="server" preset="percent" Width="76" ReadOnly="true" />
										    <ml:MoneyTextBox ID="sGfeDiscountPointF" runat="server" preset="money" Width="76px" ReadOnly="true" />
										  </td>
										  <td>&nbsp;</td>
										  <uc1:GoodFaithEstimate2010RightColumn ID="sGfeDiscountPointFProps_ctrl" LegacyGfeFieldT="sLDiscnt" Page2A_rbVisible="false" runat="server" PaidToTBVisible="false" PocCBVisible="false" Page2PlaceholderVisible="true" BFVisible="true" GBFVisible="false"/>
										</tr>
										<tr>
										  <td></td>
										  <td colspan="2" class="FieldLabel"><asp:CheckBox ID="sGfeIsTPOTransaction" runat="server" Text="This transaction involves a TPO" onclick="refreshCalculation();"/></td>
										</tr>
										<tr>
										  <td></td>
										  <td colspan="2" class="FieldLabel"><asp:CheckBox ID="sIsItemizeBrokerCommissionOnIFW" runat="server" Text="Itemize originator compensation on Initial Fees Worksheet" /></td>
										</tr>
										<TR>
											<TD class="FieldLabel">804</TD>
											<TD class="FieldLabel">Appraisal fee</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sApprF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();" EnableViewState="False"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sApprFProps_ctrl" LegacyGfeFieldT="sApprF" Page2Value="B3" runat="server" PaidCBVisible="true" ShowAgentPicker="true"></uc1:GoodFaithEstimate2010RightColumn>
										</TR>
										<TR>
											<TD class="FieldLabel">805</TD>
											<TD class="FieldLabel">Credit report</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sCrF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();" EnableViewState="False"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sCrFProps_ctrl" LegacyGfeFieldT="sCrF" Page2Value="B3" runat="server" PaidCBVisible="true" ShowAgentPicker="true"></uc1:GoodFaithEstimate2010RightColumn>
										</TR>
										<TR>
											<TD class="FieldLabel">806</TD>
											<TD class="FieldLabel">Tax service fee</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sTxServF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sTxServFProps_ctrl" LegacyGfeFieldT="sTxServF" Page2Value="B3" runat="server" ShowAgentPicker="true"></uc1:GoodFaithEstimate2010RightColumn>
										</TR>
										<TR>
										    <TD class="FieldLabel">807</TD>
											<TD class="FieldLabel">Flood Certification</TD>
											<TD><asp:dropdownlist id="sFloodCertificationDeterminationT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist></TD>
											<TD><ml:moneytextbox id="sFloodCertificationF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sFloodCertificationFProps_ctrl" LegacyGfeFieldT="sFloodCertificationF" Page2Value="B3" runat="server" ShowAgentPicker="true"></uc1:GoodFaithEstimate2010RightColumn>
										</TR>
										<TR>
											<TD class="FieldLabel">808</TD>
											<TD class="FieldLabel">Mortgage broker fee</TD>
											<TD class="FieldLabel"><ml:percenttextbox id="sMBrokFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();" EnableViewState="False"></ml:percenttextbox>&nbsp;of
												<asp:dropdownlist id="sMBrokFBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
												+
												<ml:moneytextbox id="sMBrokFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="59" onchange="refreshCalculation();" EnableViewState="False"></ml:moneytextbox></TD>
											<TD><ml:moneytextbox id="sMBrokF" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sMBrokFProps_ctrl" LegacyGfeFieldT="sMBrokF" Page2Value="A1" PaidToTBVisible="false" PaidToBCBVisible="true" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
										</TR>
										<TR>
											<TD class="FieldLabel">809</TD>
											<TD class="FieldLabel">Lender's inspection fee</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sInspectF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();" EnableViewState="False"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sInspectFProps_ctrl" LegacyGfeFieldT="sInspectF" Page2Value="A1" runat="server" ShowAgentPicker="true"></uc1:GoodFaithEstimate2010RightColumn>
										</TR>
										<TR>
											<TD class="FieldLabel">810</TD>
											<TD class="FieldLabel">Processing fee</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sProcF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sProcFProps_ctrl" LegacyGfeFieldT="sProcF" Page2Value="A1" PaidCBVisible="true" runat="server" ShowAgentPicker="true"></uc1:GoodFaithEstimate2010RightColumn>
										</TR>
										<TR>
											<TD class="FieldLabel">811</TD>
											<TD class="FieldLabel">Underwriting fee</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sUwF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sUwFProps_ctrl" LegacyGfeFieldT="sUwF" Page2Value="A1" runat="server" ShowAgentPicker="true"></uc1:GoodFaithEstimate2010RightColumn>
										</TR>
										<TR>
											<TD class="FieldLabel">812</TD>
											<TD class="FieldLabel">Wire transfer</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sWireF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sWireFProps_ctrl" LegacyGfeFieldT="sWireF" Page2Value="A1" runat="server" ShowAgentPicker="true"></uc1:GoodFaithEstimate2010RightColumn>
										</TR>
										<TR>
											<TD class="FieldLabel">813</TD>
											<TD colSpan="2">
											    <ml:ComboBox id="s800U1FDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '800')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="95%" MaxLength="100"></ml:ComboBox>
											</TD>
											<TD><ml:moneytextbox id="s800U1F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="s800U1FProps_ctrl" LegacyGfeFieldT="s800U1F" Page2Option1Text="A1" Page2Option1Value="B1" Page2Option2Text="B3" Page2Option2Value="B3" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server" ShowAgentPicker="true"></uc1:GoodFaithEstimate2010RightColumn>
										</TR>
										<TR>
											<TD class="FieldLabel">814</TD>
											<TD colSpan="2">
											    <ml:ComboBox id="s800U2FDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '800')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="95%" MaxLength="100"></ml:ComboBox>
											</TD>
											<TD><ml:moneytextbox id="s800U2F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="s800U2FProps_ctrl" LegacyGfeFieldT="s800U2F" Page2Option1Text="A1" Page2Option1Value="B1" Page2Option2Text="B3" Page2Option2Value="B3" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server" ShowAgentPicker="true"></uc1:GoodFaithEstimate2010RightColumn>
										</TR>
										<TR>
											<TD class="FieldLabel">815</TD>
											<TD colSpan="2">
											    <ml:ComboBox id="s800U3FDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '800')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="95%" MaxLength="100"></ml:ComboBox>
											</TD>
											<TD><ml:moneytextbox id="s800U3F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="s800U3FProps_ctrl" LegacyGfeFieldT="s800U3F" Page2Option1Text="A1" Page2Option1Value="B1" Page2Option2Text="B3" Page2Option2Value="B3" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server" ShowAgentPicker="true"></uc1:GoodFaithEstimate2010RightColumn>
										</TR>
										<TR>
											<TD class="FieldLabel">816</TD>
											<TD colSpan="2">
											    <ml:ComboBox id="s800U4FDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '800')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="95%" MaxLength="100"></ml:ComboBox>
											</TD>
											<TD><ml:moneytextbox id="s800U4F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="s800U4FProps_ctrl" LegacyGfeFieldT="s800U4F" Page2Option1Text="A1" Page2Option1Value="B1" Page2Option2Text="B3" Page2Option2Value="B3" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server" ShowAgentPicker="true"></uc1:GoodFaithEstimate2010RightColumn>
										</TR>
										<TR>
											<TD class="FieldLabel">817</TD>
											<TD colSpan="2">
											    <ml:ComboBox id="s800U5FDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '800')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="95%" MaxLength="100"></ml:ComboBox>
											</TD>
											<TD><ml:moneytextbox id="s800U5F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="s800U5FProps_ctrl" LegacyGfeFieldT="s800U5F" Page2Option1Text="A1" Page2Option1Value="B1" Page2Option2Text="B3" Page2Option2Value="B3" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server" ShowAgentPicker="true"></uc1:GoodFaithEstimate2010RightColumn>
										</TR>
										<TR>
											<TD class="FormTableSubheader"><A name="900"></A>900</TD>
											<TD class="FormTableSubheader" colSpan="12">ITEMS REQUIRED BY LENDER TO BE PAID IN 
												ADVANCE</TD>
										</TR>
										<TR>
											<TD class="FieldLabel">901</TD>
											<TD class="FieldLabel">Interest for&nbsp;&nbsp;<asp:CheckBox ID="sIPiaDyLckd" onclick="lockField(this, 'sIPiaDy'); refreshCalculation();" Text="Lock" runat="server"/></TD>
											<TD class="FieldLabel" noWrap><asp:textbox id="sIPiaDy" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="40px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;days 
												@
												<asp:CheckBox id="sIPerDayLckd" runat="server" onclick="lockField(this, 'sIPerDay'); refreshCalculation();" Text="Lock"></asp:CheckBox>
												<ml:moneytextbox id="sIPerDay" runat="server" preset="money" onchange="refreshCalculation();" decimalDigits="6"></ml:moneytextbox>
												per day</TD>
											<TD><ml:moneytextbox id="sIPia" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sIPiaProps_ctrl" LegacyGfeFieldT="sIPia" Page2Value="B10" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">902</TD>
											<TD class="FieldLabel" noWrap colSpan="2" rowSpan="1"><A href="javascript:linkMe('../LoanInfo.aspx?pg=1');">Mortgage 
													Insurance Premium</A></TD>
											<TD><ml:moneytextbox id="sMipPia" runat="server" preset="money" width="76px" readonly="True"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn ID="sMipPiaProps_ctrl" Page2Value="B3" PaidToBCBVisible="false"
                                                runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">903</TD>
											<TD class="FieldLabel" noWrap colSpan="2">Haz Ins. @
												<ml:percenttextbox id="sProHazInsR" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();" DESIGNTIMEDRAGDROP="236"></ml:percenttextbox>
												of
												<asp:dropdownlist id="sProHazInsT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
												+
												<ml:moneytextbox id="sProHazInsMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="60px" onchange="refreshCalculation();" decimalDigits="4"></ml:moneytextbox>
												for
												<asp:textbox id="sHazInsPiaMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="30px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>
												mths</TD>
											<TD><ml:moneytextbox id="sHazInsPia" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sHazInsPiaProps_ctrl" LegacyGfeFieldT="sHazInsPia" Page2Value="B11" PaidToBCBVisible="false" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">904</TD>
											<TD colSpan="2">
											    <ml:ComboBox id="s904PiaDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '900')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="95%" MaxLength="100" Height="20px"></ml:ComboBox>
											</TD>
											<TD><ml:moneytextbox id="s904Pia" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="s904PiaProps_ctrl" LegacyGfeFieldT="s904Pia" Page2Option1Text="B3" Page2Option1Value="B3" Page2Option2Text="B11" Page2Option2Value="B11" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">905</TD>
											<TD class="FieldLabel"><A href="javascript:linkMe('../LoanInfo.aspx?pg=1');">VA Funding 
													Fee</A></TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sVaFf" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" ReadOnly="True" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sVaFfProps_ctrl" LegacyGfeFieldT="sVaFf" Page2Value="B3" PaidToBCBVisible="false" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">906</TD>
											<TD colSpan="2">
											    <ml:ComboBox id="s900U1PiaDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '900')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="95%" MaxLength="100" ></ml:ComboBox>
											</TD>
											<TD><ml:moneytextbox id="s900U1Pia" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="s900U1PiaProps_ctrl" LegacyGfeFieldT="s900U1Pia" Page2Option1Text="B3" Page2Option1Value="B3" Page2Option2Text="B11" Page2Option2Value="B11" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FormTableSubheader"><A name="1000"></A>1000</TD>
											<TD class="FormTableSubheader" colSpan="12">RESERVES DEPOSITED WITH LENDER</TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1002</TD>
											<TD class="FieldLabel">Haz ins. reserve</TD>
											<TD class="FieldLabel"><asp:textbox id="sHazInsRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
												@
												<ml:moneytextbox id="sProHazIns" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox>
												/ month</TD>
											<TD><ml:moneytextbox id="sHazInsRsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sHazInsRsrvProps_ctrl" LegacyGfeFieldT="sHazInsRsrv" Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1003</TD>
											<TD class="FieldLabel" colSpan="2"><A href="javascript:linkMe('../LoanInfo.aspx?pg=1');">Mtg ins.</A> 
												reserve&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = 
												<ml:moneytextbox id="sProMIns" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox>&nbsp;&nbsp;for
												<asp:textbox id="sMInsRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>
												mths</TD>
											<TD colSpan="1"><ml:moneytextbox id="sMInsRsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sMInsRsrvProps_ctrl" LegacyGfeFieldT="sMInsRsrv" Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1004</TD>
											<TD class="FieldLabel" colSpan="2" noWrap>Tax resrv @
												<ml:percenttextbox id="sProRealETxR" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();"></ml:percenttextbox>&nbsp;of
												<asp:dropdownlist id="sProRealETxT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
												+
												<ml:moneytextbox id="sProRealETxMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="59px" onchange="refreshCalculation();"></ml:moneytextbox>
												for
												<asp:textbox id="sRealETxRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="31px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>
												mths</TD>
											<TD><ml:moneytextbox id="sRealETxRsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sRealETxRsrvProps_ctrl" LegacyGfeFieldT="sRealETxRsrv" Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1005</TD>
											<TD class="FieldLabel">School taxes</TD>
											<TD class="FieldLabel"><asp:textbox id="sSchoolTxRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
												@
												<ml:moneytextbox id="sProSchoolTx" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>
												/ month</TD>
											<TD><ml:moneytextbox id="sSchoolTxRsrv" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sSchoolTxRsrvProps_ctrl" LegacyGfeFieldT="sSchoolTxRsrv" Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1006</TD>
											<TD class="FieldLabel">Flood ins. reserve</TD>
											<TD class="FieldLabel"><asp:textbox id="sFloodInsRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
												@
												<ml:moneytextbox id="sProFloodIns" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>
												/ month</TD>
											<TD><ml:moneytextbox id="sFloodInsRsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sFloodInsRsrvProps_ctrl" LegacyGfeFieldT="sFloodInsRsrv" Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1007</TD>
											<TD class="FieldLabel">Aggregate adjustment</TD>
											<TD align="right"><INPUT type="button" value="Aggregate Escrow" onclick="linkMe('AggregateEscrowDisclosure.aspx');" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<asp:CheckBox id="sAggregateAdjRsrvLckd" runat="server" onclick="lockField(this, 'sAggregateAdjRsrv'); refreshCalculation();" Text="Locked" CssClass="FieldLabel"></asp:CheckBox></TD>
											<TD><ml:moneytextbox id="sAggregateAdjRsrv" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sAggregateAdjRsrvProps_ctrl" LegacyGfeFieldT="sAggregateAdjRsrv" Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1008</TD>
											<TD>
											    <ml:ComboBox id="s1006ProHExpDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1000')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="135" MaxLength="21" Height="20px" ></ml:ComboBox>
											</TD>
											<TD class="FieldLabel"><asp:textbox id="s1006RsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
												@
												<ml:moneytextbox id="s1006ProHExp" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>
												/ month</TD>
											<TD><ml:moneytextbox id="s1006Rsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="s1006RsrvProps_ctrl" LegacyGfeFieldT="s1006Rsrv" Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1009</TD>
											<TD>
											    <ml:ComboBox id="s1007ProHExpDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1000')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="136" MaxLength="21" Height="20px" ></ml:ComboBox>
											</TD>
											<TD class="FieldLabel"><asp:textbox id="s1007RsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
												@
												<ml:moneytextbox id="s1007ProHExp" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>
												/ month</TD>
											<TD><ml:moneytextbox id="s1007Rsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="s1007RsrvProps_ctrl" LegacyGfeFieldT="s1007Rsrv" Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<asp:PlaceHolder runat="server" ID="phAdditionalSection1000CustomFees">
    										<tr>
    											<td class="FieldLabel">1010</td>
    											<td>
    											    <ml:ComboBox id="sU3RsrvDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1000')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="136" MaxLength="21" Height="20px" ></ml:ComboBox>
    											</td>
    											<td class="FieldLabel"><asp:textbox id="sU3RsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
    												@
    												<ml:moneytextbox id="sProU3Rsrv" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>
    												/ month</td>
    											<td><ml:moneytextbox id="sU3Rsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></td>
    											<uc1:GoodFaithEstimate2010RightColumn id="sU3RsrvProps_ctrl" LegacyGfeFieldT="sU3Rsrv" Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
    										</tr>
    										<tr>
    											<td class="FieldLabel">1011</td>
    											<td>
    											    <ml:ComboBox id="sU4RsrvDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1000')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="136" MaxLength="21" Height="20px" ></ml:ComboBox>
    											</td>
    											<td class="FieldLabel"><asp:textbox id="sU4RsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
    												@
    												<ml:moneytextbox id="sProU4Rsrv" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>
    												/ month</td>
    											<td><ml:moneytextbox id="sU4Rsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></td>
    											<uc1:GoodFaithEstimate2010RightColumn id="sU4RsrvProps_ctrl" LegacyGfeFieldT="sU4Rsrv" Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
    										</tr>
										</asp:PlaceHolder>
										<TR>
											<TD class="FormTableSubheader">1100</TD>
											<TD class="FormTableSubheader" colSpan="12">TITLE CHARGES</TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1102</TD>
											<TD class="FieldLabel">Closing/Escrow Fee</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sEscrowF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sEscrowFProps_ctrl" LegacyGfeFieldT="sEscrowF" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" BorrVisible="true" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1103</TD>
											<TD class="FieldLabel">Owner's title Insurance</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sOwnerTitleInsF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sOwnerTitleInsFProps_ctrl" LegacyGfeFieldT="sOwnerTitleInsF" Page2Value="B5" BorrVisible="true" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1104</TD>
											<TD class="FieldLabel">Lender's title Insurance</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sTitleInsF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sTitleInsFProps_ctrl" LegacyGfeFieldT="sTitleInsF" Page2Value="B4" BorrVisible="true" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1109</TD>
											<TD class="FieldLabel">Doc preparation fee</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sDocPrepF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sDocPrepFProps_ctrl" LegacyGfeFieldT="sDocPrepF" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1110</TD>
											<TD class="FieldLabel">Notary fees</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sNotaryF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sNotaryFProps_ctrl" LegacyGfeFieldT="sNotaryF" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1111</TD>
											<TD class="FieldLabel">Attorney fees</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sAttorneyF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sAttorneyFProps_ctrl" LegacyGfeFieldT="sAttorneyF" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1112</TD>
											<TD colSpan="2">
											    <ml:ComboBox id="sU1TcDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1100')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="95%" MaxLength="100" ></ml:ComboBox>
											</TD>
											<TD><ml:moneytextbox id="sU1Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sU1TcProps_ctrl" LegacyGfeFieldT="sU1Tc" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1113</TD>
											<TD colSpan="2">
											    <ml:ComboBox id="sU2TcDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1100')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="95%" MaxLength="100" ></ml:ComboBox>
											</TD>
											<TD><ml:moneytextbox id="sU2Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sU2TcProps_ctrl" LegacyGfeFieldT="sU2Tc" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1114</TD>
											<TD colSpan="2">
											    <ml:ComboBox id="sU3TcDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1100')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="95%" MaxLength="100" ></ml:ComboBox>
											</TD>
											<TD><ml:moneytextbox id="sU3Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sU3TcProps_ctrl" LegacyGfeFieldT="sU3Tc" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1115</TD>
											<TD colSpan="2">
											    <ml:ComboBox id="sU4TcDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1100')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="95%" MaxLength="100" ></ml:ComboBox>
											</TD>
											<TD><ml:moneytextbox id="sU4Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sU4TcProps_ctrl" LegacyGfeFieldT="sU4Tc" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FormTableSubheader">1200</TD>
											<TD class="FormTableSubheader" noWrap colSpan="12">GOVERNMENT RECORDING &amp; 
												TRANSFER CHARGES</TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1201&nbsp;&nbsp;</TD>
											<TD class="FieldLabel" noWrap>Recording fees</TD>
											<TD class="FieldLabel" noWrap>
											    <span id="sp1201" style="float:left">
											        <ml:percenttextbox id="sRecFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51" onchange="refreshCalculation();"></ml:percenttextbox>
												    of
												    <asp:dropdownlist id="sRecBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
												    +
												    <ml:moneytextbox id="sRecFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56" onchange="refreshCalculation();"></ml:moneytextbox>
												</span>
												<asp:CheckBox ID="sRecFLckd" runat="server" style="float:right" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" onclick="toggleRecFFields(); refreshCalculation();"/>
											</TD>
											<TD><ml:moneytextbox id="sRecF" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sRecFProps_ctrl" LegacyGfeFieldT="sRecF" Page2Value="B7" PaidToBCBVisible="false" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1202&nbsp;&nbsp;</TD>
											<TD class="FieldLabel" noWrap>Deed <ml:moneytextbox id="sRecDeed" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD class="FieldLabel" noWrap>
											    <span style="float:left">Mortgage <ml:moneytextbox id="sRecMortgage" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56" onchange="refreshCalculation();"></ml:moneytextbox></span>
											    <span style="float:right; margin-right:25px">Release <ml:moneytextbox id="sRecRelease" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56" onchange="refreshCalculation();"></ml:moneytextbox></span>
											</TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1204</TD>
											<TD class="FieldLabel" noWrap>City/County tax stamps</TD>
											<TD class="FieldLabel" noWrap><ml:percenttextbox id="sCountyRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>
												of
												<asp:dropdownlist id="sCountyRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
												+
												<ml:moneytextbox id="sCountyRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD><ml:moneytextbox id="sCountyRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sCountyRtcProps_ctrl" LegacyGfeFieldT="sCountyRtc" Page2Value="B8" PaidToBCBVisible="false" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1205</TD>
											<TD class="FieldLabel" noWrap>State tax/stamps</TD>
											<TD class="FieldLabel" noWrap><ml:percenttextbox id="sStateRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>
												of
												<asp:dropdownlist id="sStateRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
												+
												<ml:moneytextbox id="sStateRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD><ml:moneytextbox id="sStateRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sStateRtcProps_ctrl" LegacyGfeFieldT="sStateRtc" Page2Value="B8" PaidToBCBVisible="false" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1206</TD>
											<TD>
											    <ml:ComboBox id="sU1GovRtcDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1200')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="104" MaxLength="21" ></ml:ComboBox>
											</TD>
											<TD class="FieldLabel" noWrap><ml:percenttextbox id="sU1GovRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>
												of
												<asp:dropdownlist id="sU1GovRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
												+
												<ml:moneytextbox id="sU1GovRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD><ml:moneytextbox id="sU1GovRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sU1GovRtcProps_ctrl" LegacyGfeFieldT="sU1GovRtc" Page2Option1Text="B7" Page2Option1Value="B7" Page2Option2Text="B8" Page2Option2Value="B8" PaidToBCBVisible="false" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1207</TD>
											<TD>
											    <ml:ComboBox id="sU2GovRtcDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1200')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="104px" MaxLength="21" Height="20px" ></ml:ComboBox>
											</TD>
											<TD class="FieldLabel" noWrap><ml:percenttextbox id="sU2GovRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>
												of
												<asp:dropdownlist id="sU2GovRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
												+
												<ml:moneytextbox id="sU2GovRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD><ml:moneytextbox id="sU2GovRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sU2GovRtcProps_ctrl" LegacyGfeFieldT="sU2GovRtc" Page2Option1Text="B7" Page2Option1Value="B7" Page2Option2Text="B8" Page2Option2Value="B8" PaidToBCBVisible="false" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1208</TD>
											<TD>
											    <ml:ComboBox id="sU3GovRtcDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1200')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="104px" MaxLength="21" Height="20px" ></ml:ComboBox>
											</TD>
											<TD class="FieldLabel" noWrap><ml:percenttextbox id="sU3GovRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>
												of
												<asp:dropdownlist id="sU3GovRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
												+
												<ml:moneytextbox id="sU3GovRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD><ml:moneytextbox id="sU3GovRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sU3GovRtcProps_ctrl" LegacyGfeFieldT="sU3GovRtc" Page2Option1Text="B7" Page2Option1Value="B7" Page2Option2Text="B8" Page2Option2Value="B8" PaidToBCBVisible="false" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FormTableSubheader">1300</TD>
											<TD class="FormTableSubheader" colSpan="12">ADDITIONAL SETTLEMENT CHARGES</TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1302</TD>
											<TD class="FieldLabel">Pest Inspection</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sPestInspectF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sPestInspectFProps_ctrl" LegacyGfeFieldT="sPestInspectF" BorrVisible="true" Page2Value="B6" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1303</TD>
											<TD colSpan="2">
											    <ml:ComboBox id="sU1ScDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1300')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="95%" MaxLength="100" ></ml:ComboBox>
											</TD>
											<TD><ml:moneytextbox id="sU1Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sU1ScProps_ctrl" LegacyGfeFieldT="sU1Sc" Page2Option1Text="B4" BorrVisible="true" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1304</TD>
											<TD colSpan="2">
											    <ml:ComboBox id="sU2ScDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1300')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="95%" MaxLength="100" Height="20px" ></ml:ComboBox>
											</TD>
											<TD><ml:moneytextbox id="sU2Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sU2ScProps_ctrl" LegacyGfeFieldT="sU2Sc" Page2Option1Text="B4" BorrVisible="true" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1305</TD>
											<TD colSpan="2">
											    <ml:ComboBox id="sU3ScDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1300')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="95%" MaxLength="100" ></ml:ComboBox>
											</TD>
											<TD><ml:moneytextbox id="sU3Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sU3ScProps_ctrl" LegacyGfeFieldT="sU3Sc" Page2Option1Text="B4" BorrVisible="true" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1306</TD>
											<TD colSpan="2">
											    <ml:ComboBox id="sU4ScDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1300')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="95%" MaxLength="100" ></ml:ComboBox>
											</TD>
											<TD><ml:moneytextbox id="sU4Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sU4ScProps_ctrl" LegacyGfeFieldT="sU4Sc" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel">1307</TD>
											<TD colSpan="2">
											    <ml:ComboBox id="sU5ScDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1300')" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" Width="95%" MaxLength="100" ></ml:ComboBox>
											</TD>
											<TD><ml:moneytextbox id="sU5Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<uc1:GoodFaithEstimate2010RightColumn id="sU5ScProps_ctrl" LegacyGfeFieldT="sU5Sc" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server" ShowAgentPicker="true"></uc1:goodfaithestimate2010rightcolumn>
										</TR>
										<TR>
											<TD class="FieldLabel" colSpan="3">TOTAL ESTIMATED SETTLEMENT CHARGES</TD>
											<TD><ml:moneytextbox id="sGfeTotalEstimateSettlementCharge3" tabIndex="201" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
				    <td>
				        <table class="InsetBorder FieldLabel">
				            <tr>
						        <td class="FormTableSubHeader" colspan="10">
						            GFE Summary
						        </td>
						    </tr>
						    <tr>
						        <td>
						            A1
						        </td>
						        <td class="RightPadding">
									<ml:moneytextbox id="sGfeOriginationF" runat="server" ReadOnly="true" preset="money"></ml:moneytextbox>
						        </td>
						        <td>
						            B3
						        </td>
						        <td class="RightPadding">
									<ml:moneytextbox id="sGfeRequiredServicesTotalFee" runat="server" ReadOnly="true" preset="money"></ml:moneytextbox>
						        </td>
						        <td>
						            B6
						        </td>
						        <td class="RightPadding">
									<ml:moneytextbox id="sGfeServicesYouShopTotalFee" runat="server" ReadOnly="true" preset="money"></ml:moneytextbox>
						        </td>
						        <td>
						            B9
						        </td>
						        <td class="RightPadding">
									<ml:moneytextbox id="sGfeInitialImpoundDeposit" runat="server" ReadOnly="true" preset="money"></ml:moneytextbox>
						        </td>
						        <td>
						            B
						        </td>
						        <td>
									<ml:moneytextbox id="sGfeTotalOtherSettlementServiceFee" runat="server" ReadOnly="true" preset="money"></ml:moneytextbox>
						        </td>
						    </tr>
						    <tr>
						        <td>
						            A2
						        </td>
						        <td>
									<ml:moneytextbox id="sLDiscnt2" runat="server" ReadOnly="true" preset="money"></ml:moneytextbox>
						        </td>
						        <td>
						            B4
						        </td>
						        <td>
									<ml:moneytextbox id="sGfeLenderTitleTotalFee" runat="server" ReadOnly="true" preset="money"></ml:moneytextbox>
						        </td>
						        <td>
						            B7
						        </td>
						        <td>
									<ml:moneytextbox id="sGfeGovtRecTotalFee" runat="server" ReadOnly="true" preset="money"></ml:moneytextbox>
						        </td>
						        <td>
						            B10
						        </td>
						        <td>
									<ml:moneytextbox id="sGfeDailyInterestTotalFee" runat="server" ReadOnly="true" preset="money"></ml:moneytextbox>
						        </td>
						        <td>
						            A+B
						        </td>
						        <td>
									<ml:moneytextbox id="sGfeTotalEstimateSettlementCharge4" runat="server" ReadOnly="true" preset="money"></ml:moneytextbox>
						        </td>
						    </tr>
						    <tr>
						        <td>
						            A
						        </td>
						        <td>
									<ml:moneytextbox id="sGfeAdjOriginationCharge" runat="server" ReadOnly="true" preset="money"></ml:moneytextbox>
						        </td>
						        <td>
						            B5
						        </td>
						        <td>
									<ml:moneytextbox id="sGfeOwnerTitleTotalFee" runat="server" ReadOnly="true" preset="money"></ml:moneytextbox>
						        </td>
						        <td>
						            B8
						        </td>
						        <td>
									<ml:moneytextbox id="sGfeTransferTaxTotalFee" runat="server" ReadOnly="true" preset="money"></ml:moneytextbox>
						        </td>
						        <td>
						            B11
						        </td>
						        <td>
									<ml:moneytextbox id="sGfeHomeOwnerInsuranceTotalFee" runat="server" ReadOnly="true" preset="money"></ml:moneytextbox>
						        </td>
						        <td>
						            N/A
						        </td>
						        <td>
									<ml:moneytextbox id="sGfeNotApplicableF" runat="server" ReadOnly="true" preset="money"></ml:moneytextbox>
						        </td>
						    </tr>
				        </table>
				    </td>
				</tr>
				<tr>
				    <td>
						<TABLE ID="table4" class="InsetBorder">
						    <tr>
						        <td class="FormTableSubHeader" colspan="4">Tradeoffs Table</td>
						    </tr>
						    <tr>
						        <td></td>
						        <td class="FieldLabel" style="width:120px">This GFE</td>
						        <td class="FieldLabel" style="width:150px">Lower closing costs</td>
						        <td class="FieldLabel">Lower rate</td>
						    </tr>
						    <tr>
						        <td class="FieldLabel">Initial Loan Amt</td>
						        <td><asp:TextBox ID="sFinalLAmt2" preset="money" ReadOnly="true" runat="server"></asp:TextBox></td>
						        <td><asp:TextBox ID="sGfeTradeOffLowerCCLoanAmt" tabIndex="202" preset="money" onchange="refreshCalculation();" runat="server"></asp:TextBox></td>
						        <td><asp:TextBox ID="sGfeTradeOffLowerRateLoanAmt" tabIndex="205" preset="money" onchange="refreshCalculation();" runat="server"></asp:TextBox></td>
						    </tr>
						    <tr>
						        <td class="FieldLabel">Initial Rate</td>
						        <td><asp:TextBox ID="sNoteIR2" preset="percent" ReadOnly="true" runat="server"></asp:TextBox></td>
						        <td><asp:TextBox ID="sGfeTradeOffLowerCCNoteIR" tabIndex="203" preset="percent" onchange="refreshCalculation();" runat="server"></asp:TextBox></td>
						        <td><asp:TextBox ID="sGfeTradeOffLowerRateNoteIR" tabIndex="206" preset="percent" onchange="refreshCalculation();" runat="server"></asp:TextBox></td>
						    </tr>
						    <tr>
						        <td class="FieldLabel">Monthly Payment</td>
						        <td><asp:TextBox ID="sGfeProThisMPmtAndMIns2" preset="money" ReadOnly="true" runat="server"></asp:TextBox></td>
						        <td><asp:TextBox ID="sGfeTradeOffLowerCCMPmtAndMIns" preset="money" ReadOnly="true" runat="server"></asp:TextBox></td>
						        <td><asp:TextBox ID="sGfeTradeOffLowerRateMPmtAndMIns" preset="money" ReadOnly="true" runat="server"></asp:TextBox></td>
						    </tr>
						    <tr>
						        <td class="FieldLabel">Monthly Payment Change</td>
						        <td style="font-style:italic">No Change</td>
						        <td><asp:TextBox ID="sGfeTradeOffLowerCCMPmtAndMInsDiff" preset="money" ReadOnly="true" runat="server"></asp:TextBox> more</td>
						        <td><asp:TextBox ID="sGfeTradeOffLowerRateMPmtAndMInsDiff" preset="money" ReadOnly="true" runat="server"></asp:TextBox> less</td>
						    </tr>
						    <tr>
						        <td class="FieldLabel">Closing Cost Change</td>
						        <td style="font-style:italic">No Change</td>
						        <td><asp:TextBox ID="sGfeTradeOffLowerCCClosingCostDiff" preset="money" ReadOnly="true" runat="server"></asp:TextBox> less</td>
						        <td><asp:TextBox ID="sGfeTradeOffLowerRateClosingCostDiff" preset="money" ReadOnly="true" runat="server"></asp:TextBox> more</td>
						    </tr>
						    <tr>
						        <td class="FieldLabel">Total Closing Costs</td>
						        <td><asp:TextBox ID="sGfeTotalEstimateSettlementCharge" preset="money" ReadOnly="true" runat="server"></asp:TextBox></td>
						        <td><asp:TextBox ID="sGfeTradeOffLowerCCClosingCost" tabIndex="204" preset="money" onchange="refreshCalculation();" runat="server"></asp:TextBox></td>
						        <td><asp:TextBox ID="sGfeTradeOffLowerRateClosingCost" tabIndex="207" preset="money" onchange="refreshCalculation();" runat="server"></asp:TextBox></td>
						    </tr>
			            </TABLE>
					</TD>
				</TR>
				<TR>
				    <TD>
				        <TABLE ID="table7" class="InsetBorder">
				            <TR>
				                <TD class="FormTableSubHeader" colspan="5">Shopping Cart</TD>
				            </TR>
				            <TR>
				                <TD></TD>
				                <TD class="FieldLabel">This loan</TD>
				                <TD class="FieldLabel">Loan 2</TD>
				                <TD class="FieldLabel">Loan 3</TD>
				                <TD class="FieldLabel">Loan 4</TD>
				            </TR>
				            <TR>
				                <TD class="FieldLabel">Loan originator name</TD>
				                <TD><asp:TextBox ID="GfeTilCompanyName2" ReadOnly="true" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan1OriginatorName" tabIndex="208" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan2OriginatorName" tabIndex="220" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan3OriginatorName" tabIndex="232" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				            </TR>
				            <TR>
				                <TD class="FieldLabel">Initial loan amount</TD>
				                <TD><asp:TextBox ID="sFinalLAmt3" ReadOnly="true" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan1LoanAmt" tabIndex="209" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan2LoanAmt" tabIndex="221" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan3LoanAmt" tabIndex="233" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				            </TR>
				            <TR>
				                <TD class="FieldLabel">Loan term</TD>
				                <TD><asp:TextBox ID="sTerm2" ReadOnly="true" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan1LoanTerm" tabIndex="210" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan2LoanTerm" tabIndex="222" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan3LoanTerm" tabIndex="234" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				            </TR>
				            <TR>
				                <TD class="FieldLabel">Initial interest rate</TD>
				                <TD><asp:TextBox ID="sNoteIR3" ReadOnly="true" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan1NoteIR" tabIndex="211" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan2NoteIR" tabIndex="223" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan3NoteIR" tabIndex="235" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				            </TR>
				            <TR>
				                <TD class="FieldLabel">Initial monthly payment</TD>
				                <TD><asp:TextBox ID="sGfeProThisMPmtAndMIns" ReadOnly="true" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan1InitialPmt" tabIndex="212" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan2InitialPmt" tabIndex="224" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan3InitialPmt" tabIndex="236" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				            </TR>
				            <TR>
				                <TD class="FieldLabel">Rate lock period</TD>
				                <TD><asp:TextBox ID="sGfeRateLockPeriod2" ReadOnly="true" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan1RateLockPeriod" tabIndex="213" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan2RateLockPeriod" tabIndex="225" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan3RateLockPeriod" tabIndex="237" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				            </TR>
				            <TR>
				                <TD class="FieldLabel">Can interest rate rise?</TD>
				                <TD><asp:CheckBox ID="sGfeCanRateIncrease3" Enabled="false" runat="server" /></TD>
				                <TD><asp:CheckBox ID="sGfeShoppingCartLoan1CanRateIncreaseTri" tabIndex="214" onclick="refreshCalculation();" runat="server" /></TD>
				                <TD><asp:CheckBox ID="sGfeShoppingCartLoan2CanRateIncreaseTri" tabIndex="226" onclick="refreshCalculation();" runat="server" /></TD>
				                <TD><asp:CheckBox ID="sGfeShoppingCartLoan3CanRateIncreaseTri" tabIndex="238" onclick="refreshCalculation();" runat="server" /></TD>
				            </TR>
				            <TR>
				                <TD class="FieldLabel">Can loan balance rise?</TD>
				                <TD><asp:CheckBox ID="sGfeCanLoanBalanceIncrease2" Enabled="false" runat="server" /></TD>
				                <TD><asp:CheckBox ID="sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri" tabIndex="215" onclick="refreshCalculation();" runat="server" /></TD>
				                <TD><asp:CheckBox ID="sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri" tabIndex="227" onclick="refreshCalculation();" runat="server" /></TD>
				                <TD><asp:CheckBox ID="sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri" tabIndex="239" onclick="refreshCalculation();" runat="server" /></TD>
				            </TR>
				            <TR>
				                <TD class="FieldLabel">Can monthly payment rise?</TD>
				                <TD><asp:CheckBox ID="sGfeCanRateIncrease4" Enabled="false" runat="server" /></TD>
				                <TD><asp:CheckBox ID="sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri" tabIndex="216" onclick="refreshCalculation();" runat="server" /></TD>
				                <TD><asp:CheckBox ID="sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri" tabIndex="228" onclick="refreshCalculation();" runat="server" /></TD>
				                <TD><asp:CheckBox ID="sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri" tabIndex="240" onclick="refreshCalculation();" runat="server" /></TD>
				            </TR>
				            <TR>
				                <TD class="FieldLabel">Prepayment penalty?</TD>
				                <TD><asp:CheckBox ID="sGfeHavePpmtPenalty2" Enabled="false" runat="server" /></TD>
				                <TD><asp:CheckBox ID="sGfeShoppingCartLoan1HavePpmtPenaltyTri" tabIndex="217" onclick="refreshCalculation();" runat="server" /></TD>
				                <TD><asp:CheckBox ID="sGfeShoppingCartLoan2HavePpmtPenaltyTri" tabIndex="229" onclick="refreshCalculation();" runat="server" /></TD>
				                <TD><asp:CheckBox ID="sGfeShoppingCartLoan3HavePpmtPenaltyTri" tabIndex="241" onclick="refreshCalculation();" runat="server" /></TD>
				            </TR>
				            <TR>
				                <TD class="FieldLabel">Balloon payment?</TD>
				                <TD><asp:CheckBox ID="sGfeIsBalloon2" Enabled="false" runat="server" /></TD>
				                <TD><asp:CheckBox ID="sGfeShoppingCartLoan1IsBalloonTri" tabIndex="218" onclick="refreshCalculation();" runat="server" /></TD>
				                <TD><asp:CheckBox ID="sGfeShoppingCartLoan2IsBalloonTri" tabIndex="230" onclick="refreshCalculation();" runat="server" /></TD>
				                <TD><asp:CheckBox ID="sGfeShoppingCartLoan3IsBalloonTri" tabIndex="242" onclick="refreshCalculation();" runat="server" /></TD>
				            </TR>
				            <TR>
				                <TD class="FieldLabel">Total Settlement Charge?</TD>
				                <TD><asp:TextBox ID="sGfeTotalEstimateSettlementCharge2" ReadOnly="true" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan1TotalClosingCost" tabIndex="219" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan2TotalClosingCost" tabIndex="231" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				                <TD><asp:TextBox ID="sGfeShoppingCartLoan3TotalClosingCost" tabIndex="243" onchange="refreshCalculation();" runat="server"></asp:TextBox></TD>
				            </TR>
				        </TABLE>
				    </TD>
				</TR>
			</TABLE>

			<%-- OPM 170146 - Copywright notice must be placed here or it will be hidden by footer.--%>
			<br />
			<hr>
			<table>
			    <tr>
			        <td nowrap style='font-size:11px'><%=AspxTools.HtmlString(ConstAppDavid.CopyrightMessage)%></td>
			    </tr>
			</table>
		</div>

<%if (!IsArchivePage) { %>
        <div id="footerWrapper">
            <div id="footer">
                <uc1:GoodFaithEstimate2010Footer ID="Gfe2010Footer" runat="server" />
            </div>
        </div>
		        
        <script type="text/javascript">
            var $wrapper = $("#wrapper");
            var $footerWrapper = $("#footerWrapper");
            var lastWidth = 0;

            // Polling function to catch zoom/resize event
            function pollZoomFireEvent() {
                // jQuery does not support quirks mode. As such, we have
                // to grab the client width to determine the width of
                // the window.
                var widthNow = document.body.clientWidth;
                if (lastWidth == widthNow) return;
                lastWidth = widthNow;
                // Length changed, user must have zoomed or resized. Reset foorter bottom
                setFooterBottom();
            }

            // Moves footer out of the way of horizontal scrollbar, if it's showing
            // Only needed for rendering in quirks mode
            function setFooterBottom() {
                // OPM 220126, ML, 10/19/2015, 
                // Checking the scroll width against the inner width is a more reliable
                // way of determining if there is a scrollbar on the page, as it includes
                // instances where (like in case 220126) a page is opened outside of the
                // loan editor.
                if ($wrapper.get(0).scrollWidth > $wrapper.innerWidth())
                    $footerWrapper.css("bottom", "17px");
                else
                    $footerWrapper.css("bottom", "0px");
            }

            function setWrapperPadding() {
                $wrapper.css("padding-bottom", $footerWrapper.height() + "px");
            }

            // This should only run in Quirks mode on IE9 or below
            // NOTE: IE10 quirks mode is not the same as in previous versions of IE.
            if (document.documentMode < 8) {
                setFooterBottom();
                setInterval(pollZoomFireEvent, 100);
            }

            setWrapperPadding();
            $.event.add(this, "footer/switchTab", setWrapperPadding);
        </script>
<% } %>
</form>
	</body>
</HTML>
