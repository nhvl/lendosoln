namespace LendersOfficeApp.newlos.Forms
{
    using System;

    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;

    /// <summary>
    ///		Summary description for Loan1003pg1.
    /// </summary>
    public partial  class Loan1003pg1 : LendersOfficeApp.newlos.BaseLoanUserControl, IAutoLoadUserControl
	{
        #region Protected member variables
		protected System.Web.UI.WebControls.TextBox aPrev1AddrYrs;

        #endregion

        protected string sApprVal { get; set; }
		protected void PageInit(object sender, System.EventArgs e) 
		{
            BorrowerMembershipId.Visible = false;
            CoborrowerMembershipId.Visible = false;
			Tools.Bind_sLT(sLT);
			Tools.Bind_sLPurposeT(sLPurposeT);
			Tools.Bind_sEstateHeldT(sEstateHeldT);
			Tools.Bind_sFinMethT(sFinMethT);
			Tools.Bind_sSpImprovTimeFrameT(sSpImprovTimeFrameT);
			Tools.Bind_aOccT(aOccT);
			sSpZip.SmartZipcode(sSpCity, sSpState, sSpCounty);
			Tools.Bind_aMaritalStatT(aBMaritalStatT);
			Tools.Bind_aMaritalStatT(aCMaritalStatT);
			Tools.Bind_aAddrT(aBAddrT);
			Tools.Bind_aAddrT(aBPrev1AddrT);
			Tools.Bind_aAddrT(aBPrev2AddrT);
			Tools.Bind_aAddrT(aCAddrT);
			Tools.Bind_aAddrT(aCPrev1AddrT);
			Tools.Bind_aAddrT(aCPrev2AddrT);

            Tools.Bind_aAddrMailSourceT(aBAddrMailSourceT);
            Tools.Bind_aAddrMailSourceT(aCAddrMailSourceT);

			aBZip.SmartZipcode(aBCity, aBState);
            aBZipMail.SmartZipcode(aBCityMail, aBStateMail);
			aCZip.SmartZipcode(aCCity, aCState);
            aCZipMail.SmartZipcode(aCCityMail, aCStateMail);
			aBPrev1Zip.SmartZipcode(aBPrev1City, aBPrev1State);
			aBPrev2Zip.SmartZipcode(aBPrev2City, aBPrev2State);
			aCPrev1Zip.SmartZipcode(aCPrev1City, aCPrev1State);
			aCPrev2Zip.SmartZipcode(aCPrev2City, aCPrev2State);

            Tools.BindComboBox_DownPaymentSource(sDwnPmtSrc);
            Tools.BindComboBox_aManner(aManner);

			sRefPurpose.Items.Add("No Cash-Out Rate/Term");
			sRefPurpose.Items.Add("Limited Cash-Out Rate/Term");
			sRefPurpose.Items.Add("Cash-Out/Home Improvement");
			sRefPurpose.Items.Add("Cash-Out/Debt Consolidation");
			sRefPurpose.Items.Add("Cash-Out/Other");
            if (this.BrokerDB.IsCreditUnion)
            {
                BorrowerMembershipId.Visible = true;
                CoborrowerMembershipId.Visible = true;
            }

			// 01-14-08 av 18913
			sSpState.Attributes.Add( "onchange", "javascript:UpdateCounties(this,document.getElementById('" + sSpCounty.ClientID + "'), event);" );

            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);

            Tools.Bind_QualTermCalculationType(sQualTermCalculationType);
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
		}

        public void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Loan1003pg1));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Text = dataLoan.sSpZip.TrimWhitespaceAndBOM();
			
			//01-10-08 av opm 18913 Populate the new dropdownlist with counties in the state and select the one that matches it. 
			Tools.Bind_sSpCounty( dataLoan.sSpState, sSpCounty, true );
			Tools.SetDropDownListCaseInsensitive( sSpCounty, dataLoan.sSpCounty );

            sUnitsNum.Text = dataLoan.sUnitsNum_rep;
            sYrBuilt.Text = dataLoan.sYrBuilt.TrimWhitespaceAndBOM();
            sSpLegalDesc.Text = dataLoan.sSpLegalDesc;
            sPurchPrice.Text = dataLoan.sPurchPrice_rep;
            sTerm.Text = dataLoan.sTerm_rep;
            sTerm.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sEquity.Text = dataLoan.sEquityCalc_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sDue.Text = dataLoan.sDue_rep;
            sDue.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sQualIR.Text = dataLoan.sQualIR_rep;
            this.sQualIRLckd.Checked = dataLoan.sQualIRLckd;
            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            sLTODesc.Text = dataLoan.sLTODesc;
            Tools.SetDropDownListValue(sLT, dataLoan.sLT);
            sSpGrossRent.Text = dataLoan.sSpGrossRent_rep;
            
            sMultiApps.Checked = dataLoan.sMultiApps;
            sLTODesc.Text = dataLoan.sLTODesc;
            sAgencyCaseNum.Text = dataLoan.sAgencyCaseNum;
            sLenderCaseNum.Text = dataLoan.sLenderCaseNum;
            sLenderCaseNumLckd.Checked = dataLoan.sLenderCaseNumLckd;
            sLAmt.Text = dataLoan.sLAmtCalc_rep;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sEquity.Text = dataLoan.sEquityCalc_rep;
            sFfUfmipFinanced.Text = dataLoan.sFfUfmipFinanced_rep;
            sProThisMPmt.Text = dataLoan.sProThisMPmt_rep;
            sDownPmtPc.Text = dataLoan.sDownPmtPc_rep;
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
			sFinMethT.Enabled = ! ( dataLoan.sIsRateLocked || IsReadOnly );
            sFinMethDesc.Text = dataLoan.sFinMethDesc;
            sOLPurposeDesc.Text = dataLoan.sOLPurposeDesc;
            sOccRLckd.Checked = dataLoan.sOccRLckd;
            sOccR.Text = dataLoan.sOccR_rep;
            sLotAcqYr.Text = dataLoan.sLotAcqYr;
            sLotAcquiredD.Text = dataLoan.sLotAcquiredD_rep;
            sLotOrigC.Text = dataLoan.sLotOrigC_rep;
            sLotLien.Text = dataLoan.sLotLien_rep;
            sLotVal.Text = dataLoan.sLotVal_rep;
            sPresentValOfLot.Text = dataLoan.sPresentValOfLot_rep;
            sLotImprovC.Text = dataLoan.sLotImprovC_rep;
            sLotWImprovTot.Text = dataLoan.sLotWImprovTot_rep;
            sSpAcqYr.Text = dataLoan.sSpAcqYr;
            sSpOrigC.Text = dataLoan.sSpOrigC_rep;
            sSpLien.Text = dataLoan.sSpLien_rep;
            sRefPurpose.Text = dataLoan.sRefPurpose;
            sSpImprovDesc.Text = dataLoan.sSpImprovDesc;
            aTitleNm1.Text = dataApp.aTitleNm1;
            aTitleNm1Lckd.Checked = dataApp.aTitleNm1Lckd;
            aTitleNm2.Text = dataApp.aTitleNm2;
            aTitleNm2Lckd.Checked = dataApp.aTitleNm2Lckd;
            Tools.SetDropDownListValue(sEstateHeldT, dataLoan.sEstateHeldT);
            sLeaseHoldExpireD.Text = dataLoan.sLeaseHoldExpireD_rep;

            sDwnPmtSrc.Text = dataLoan.sDwnPmtSrc;
            sDwnPmtSrcExplain.Text = dataLoan.sDwnPmtSrcExplain;

            aManner.Text = dataApp.aManner;
            Tools.SetDropDownListValue(sSpImprovTimeFrameT, dataLoan.sSpImprovTimeFrameT);
            sSpImprovC.Text = dataLoan.sSpImprovC_rep;
			sLAmtLckd.Checked  = dataLoan.sLAmtLckd;

            aSpouseIExcl.Checked = dataApp.aSpouseIExcl;

            Tools.SetDropDownListValue(aOccT, dataApp.aOccT);


            aBFirstNm.Text = dataApp.aBFirstNm;
            aBMidNm.Text = dataApp.aBMidNm;
            aBLastNm.Text = dataApp.aBLastNm;
            aBSuffix.Text = dataApp.aBSuffix;
            aBSsn.Text = dataApp.aBSsn;
            aBDob.Text = dataApp.aBDob_rep;
            aBAge.Text = dataApp.aBAge_rep;
            aBSchoolYrs.Text = dataApp.aBSchoolYrs_rep;
            aBHPhone.Text = dataApp.aBHPhone;
            aBBusPhone.Text = dataApp.aBBusPhone;
            aBCellphone.Text = dataApp.aBCellPhone;
            aBEmail.Text = dataApp.aBEmail;
            aBCoreSystemId.Text = dataApp.aBCoreSystemId;

            aBDependNum.Text = dataApp.aBDependNum_rep;
            aBDependAges.Text = dataApp.aBDependAges;
            Tools.SetDropDownListValue(aBMaritalStatT, dataApp.aBMaritalStatT);
            Tools.SetDropDownListValue(aBAddrT, dataApp.aBAddrT);
            aBAddr.Text = dataApp.aBAddr;
            aBCity.Text = dataApp.aBCity;
            aBState.Value = dataApp.aBState;
            aBAddrYrs.Text = dataApp.aBAddrYrs;
            aBZip.Text = dataApp.aBZip;
            Tools.SetDropDownListValue(aBPrev1AddrT, dataApp.aBPrev1AddrT);

            aBAddrMail.Text = dataApp.aBAddrMail;
            aBCityMail.Text = dataApp.aBCityMail;
            aBZipMail.Text = dataApp.aBZipMail;
            aBStateMail.Value = dataApp.aBStateMail;

            aBPrev1Addr.Text = dataApp.aBPrev1Addr;
            aBPrev1City.Text = dataApp.aBPrev1City;
            aBPrev1State.Value = dataApp.aBPrev1State;
            aBPrev1AddrYrs.Text = dataApp.aBPrev1AddrYrs;
            aBPrev1Zip.Text = dataApp.aBPrev1Zip;
            Tools.SetDropDownListValue(aBPrev2AddrT, dataApp.aBPrev2AddrT);
            aBPrev2Addr.Text = dataApp.aBPrev2Addr;
            aBPrev2City.Text = dataApp.aBPrev2City;
            aBPrev2State.Value = dataApp.aBPrev2State;
            aBPrev2AddrYrs.Text = dataApp.aBPrev2AddrYrs;
            aBPrev2Zip.Text = dataApp.aBPrev2Zip;

            aCFirstNm.Text = dataApp.aCFirstNm;
            aCMidNm.Text = dataApp.aCMidNm;
            aCLastNm.Text = dataApp.aCLastNm;
            aCSuffix.Text = dataApp.aCSuffix;
            aCDob.Text = dataApp.aCDob_rep;
            aCSsn.Text = dataApp.aCSsn;
            aCAge.Text = dataApp.aCAge_rep;
            aCSchoolYrs.Text = dataApp.aCSchoolYrs_rep;
            aCHPhone.Text = dataApp.aCHPhone;
            aCBusPhone.Text = dataApp.aCBusPhone;
            aCCellphone.Text = dataApp.aCCellPhone;
            aCEmail.Text = dataApp.aCEmail;
            aCCoreSystemId.Text = dataApp.aCCoreSystemId;

            aCDependNum.Text = dataApp.aCDependNum_rep;
            aCDependAges.Text = dataApp.aCDependAges;
            Tools.SetDropDownListValue(aCMaritalStatT, dataApp.aCMaritalStatT);
            Tools.SetDropDownListValue(aCAddrT, dataApp.aCAddrT);
            aCAddr.Text = dataApp.aCAddr;
            aCCity.Text = dataApp.aCCity;
            aCState.Value = dataApp.aCState;
            aCAddrYrs.Text = dataApp.aCAddrYrs;
            aCZip.Text = dataApp.aCZip;
            aCAddrMail.Text = dataApp.aCAddrMail;
            aCCityMail.Text = dataApp.aCCityMail;
            aCStateMail.Value = dataApp.aCStateMail;
            aCZipMail.Text = dataApp.aCZipMail;
            Tools.SetDropDownListValue(aCPrev1AddrT, dataApp.aCPrev1AddrT);
            aCPrev1Addr.Text = dataApp.aCPrev1Addr;
            aCPrev1City.Text = dataApp.aCPrev1City;
            aCPrev1State.Value = dataApp.aCPrev1State;
            aCPrev1AddrYrs.Text = dataApp.aCPrev1AddrYrs;
            aCPrev1Zip.Text = dataApp.aCPrev1Zip;
            Tools.SetDropDownListValue(aCPrev2AddrT, dataApp.aCPrev2AddrT);
            aCPrev2Addr.Text = dataApp.aCPrev2Addr;
            aCPrev2City.Text = dataApp.aCPrev2City;
            aCPrev2State.Value = dataApp.aCPrev2State;
            aCPrev2AddrYrs.Text = dataApp.aCPrev2AddrYrs;
            aCPrev2Zip.Text = dataApp.aCPrev2Zip;
            sLandCost.Value = dataLoan.sLandCost_rep;

            sFinMethPrintAsOtherDesc.Text = dataLoan.sFinMethPrintAsOtherDesc;
            sFinMethodPrintAsOther.Checked = dataLoan.sFinMethodPrintAsOther;
            sApprVal = dataLoan.sApprVal_rep;

            Tools.SetDropDownListValue(aBAddrMailSourceT, dataApp.aBAddrMailSourceT);
            Tools.SetDropDownListValue(aCAddrMailSourceT, dataApp.aCAddrMailSourceT);

            Tools.SetDropDownListValue(this.sQualTermCalculationType, dataLoan.sQualTermCalculationType);
            this.sQualTerm.Value = dataLoan.sQualTerm_rep;

            ((BasePage)Page).RegisterJsGlobalVariables("IsConstructionLoan", dataLoan.sIsConstructionLoan);

            ((BasePage)Page).RegisterJsGlobalVariables("AreConstructionLoanCalcsMigrated",
                LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges));
        }

        public void SaveData() 
        {
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion
	}
}
