﻿namespace LendersOfficeApp.newlos.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;

    public partial class ChangeOfCircumstancesNew : BaseLoanPage
    {
        public int selectedTabIndex;

        /// <summary>
        /// If you change the order of the tabs, or add a new one, update this.
        /// </summary>
        protected enum E_Tab
        {
            CreateNew,
            ViewPast
        }
        protected bool IsTabViewPast
        {
            get { return selectedTabIndex == (int)E_Tab.ViewPast; }
        }
        protected bool IsTabCreateNew
        {
            get { return !IsTabViewPast; }
        }
        protected bool IsProtectDisclosureDates
        {
            get
            {
                return Broker.IsProtectDisclosureDates;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the ComplianceEagle integration is enabled.
        /// </summary>
        protected bool ComplianceEagleEnabled
        {
            get
            {
                return Broker.IsEnableComplianceEagleIntegration;
            }
        }

        public override bool IsReadOnly
        {
            get
            {
                return base.IsReadOnly || IsTabViewPast;
            }
        }
        

        private CPageData pageData = null;
        protected bool m_hasChange = true; // need to know to render UI.
        protected bool m_isLoanEstimateMode = false;

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.PageTitle = "Change of Circumstances";
            this.PageID = "ChangeOfCircumstancesNew";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CChangeOfCircumstancesPDF);

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);

            InitializeComponent();
            base.OnInit(e);
        }
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        protected void PageLoad(object sender, EventArgs e)
        {
            if (Broker.IsGFEandCoCVersioningEnabled == false
                || Broker.IsEnableGfe2015 == false)
            {
                throw CBaseException.GenericException("Lender not enabled for new closing costs.");
            }
            this.EnableJqueryMigrate = false;

            IncludeStyleSheet("~/css/Tabs.css");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("ArchiveStatusPopup.js");
            if (Request.Params["__EVENTTARGET"] == "changeTab")
                selectedTabIndex = Int32.Parse(Request.Params["__EVENTARGUMENT"]);

            if (IsTabViewPast)
            {
                tab1.Attributes.Add("class", "selected");
                tab0.Attributes.Remove("class");
                var dataLoan = new CPageData(LoanID, new string[] { "sLastDisclosedGFEArchiveD", "sClosingCostCoCArchive", "sDisclosureRegulationT" });
                dataLoan.InitLoad();
                m_isLoanEstimateMode = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;

                // if there aren't any archives, then viewing past changes shouldn't be allowed, so i'm switching the tab.  (can't do it in aspx page)
                if (dataLoan.sClosingCostCoCArchive.Count() == 0)
                {
                    selectedTabIndex = (int)E_Tab.CreateNew;
                    if (IsPostBack)
                    {
                        LoadData();
                    }
                    return;
                }

                var selectedValue = CoCArchives.SelectedValue;
                CoCArchives.Items.Clear();
                foreach (var cocArchive in dataLoan.sClosingCostCoCArchive)
                {
                    CoCArchives.Items.Add(new ListItem(cocArchive.DateArchived, cocArchive.DateArchived));
                }

                if (hfSavedCoC.Value == "true")
                {
                    CoCArchives.SelectedIndex = 0;
                    hfSavedCoC.Value = "false";
                }
                else if (string.IsNullOrEmpty(selectedValue))
                {
                    Tools.SetDropDownListValue(CoCArchives, dataLoan.sLastDisclosedGFEArchiveD_rep);
                    if (this.Broker.IsRemovePreparedDates)
                    {
                    	GfePrepareDate.Text = dataLoan.sLastDisclosedGFEArchiveD.Value.ToString("MM/dd/yyyy");
                	}
                }
                else
                {
                    Tools.SetDropDownListValue(CoCArchives, selectedValue);
                    var cocArchive = dataLoan.sClosingCostArchive.FirstOrDefault(archive => archive.DateArchived == selectedValue);

                    if (this.Broker.IsRemovePreparedDates)
                    {
                    	GfePrepareDate.Text = DateTime.Parse(selectedValue).ToString("MM/dd/yyyy");
                	}
                }

                this.BindComplianceCOCCategory(dataLoan.sDisclosureRegulationT);
            }
            else
            {
                tab0.Attributes.Add("class", "selected");
                tab1.Attributes.Remove("class");

                var dataLoan = new CPageData(LoanID, new string[] { "sClosingCostCoCArchive", "sDisclosureRegulationT" });
                dataLoan.InitLoad();

                m_hasChange = (dataLoan.sClosingCostCoCArchive.Count() != 0);
                m_isLoanEstimateMode = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;

                this.BindComplianceCOCCategory(dataLoan.sDisclosureRegulationT);
            }

            if (IsPostBack)
            {
                LoadData();
            }
        }
        

        protected override void LoadData()
        {
            pageData = CPageData.CreateUsingSmartDependency(LoanID,typeof(ChangeOfCircumstancesNew));
            pageData.InitLoad();

            if (pageData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                bool preventCoC = false;
                // Creating the CoC will result in an archive in the Pending Document Generation
                // status. We do not want to display this popup after the user performs a CoC, so
                // we verify the tab setting too.
                // gf opm 228619 - Check the status of the pending archive. Superseded archives
                // are meant to be treated as pending, but in this case we want to make sure
                // the status is only PendingDocumentGeneration.
                if (this.IsTabCreateNew && this.pageData.sHasLoanEstimateArchiveInPendingStatus && 
                    this.pageData.sLoanEstimateArchiveInPendingStatus.Status == ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration)
                {
                    var pendingArchive = pageData.sLoanEstimateArchiveInPendingStatus;

                    if (pendingArchive.Source == ClosingCostArchive.E_ClosingCostArchiveSource.ChangeOfCircumstance 
                        || pendingArchive.WasSourcedFromPendingCoC)
                    {
                        this.RegisterJsGlobalVariables("BlockCoCForPendingArchiveFromCoC", true);
                        this.ArchiveStatusPendingFromCoCErrorMessage.Text = ErrorMessages.ArchiveError.UnableToCoCBecauseOfPendingArchive(
                            ClosingCostArchive.E_ClosingCostArchiveSource.ChangeOfCircumstance,
                            pendingArchive.DateArchived);
                    }
                    else if (pendingArchive.Source == ClosingCostArchive.E_ClosingCostArchiveSource.Manual)
                    {
                        this.RegisterJsGlobalVariables("BlockCoCForPendingArchiveFromManualArchive", true);
                        this.RegisterJsGlobalVariables("PreventCoCIfNotProvidedToBorrower", !pageData.sHasLastDisclosedLoanEstimateArchive);
                        this.ArchiveStatusPendingFromManualArchiveErrorMessage.Text = ErrorMessages.ArchiveError.UnableToCoCBecauseOfPendingArchive(
                            pendingArchive.Source,
                            pendingArchive.DateArchived);
                    }
                    else
                    {
                        throw new GenericUserErrorMessageException(
                            "We have a loan estimate archive in the pending status from an unexpected source.");
                    }

                    preventCoC = true;
                }
                else if (pageData.sHasClosingCostArchiveInUnknownStatus)
                {
                    RegisterJsGlobalVariables("BlockCoCForArchiveWithUnknownStatus", true);
                    RegisterJsGlobalVariables("PreventCoCIfNotProvidedToBorrower", !pageData.sHasLastDisclosedLoanEstimateArchive);                    
                    RegisterJsGlobalVariables("HasBothCDAndLEArchiveInUnknownStatus", pageData.sHasBothCDAndLEArchiveInUnknownStatus);
                    
                    var archive = pageData.sClosingCostArchiveInUnknownStatus;

                    this.ArchiveStatusUnknownMessage.Text = ErrorMessages.ArchiveError.UnableToCoCBecauseOfUnknownArchive(
                        archive.DateArchived,
                        archive.ClosingCostArchiveType,
                        userCanDetermineStatus: true);

                    bool isLoanEstimate = archive.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate;

                    if (isLoanEstimate)
                    {
                        this.RegisterJsGlobalVariables("sHasLoanEstimateArchiveInUnknownStatus", true);
                        this.RegisterJsGlobalVariables("UnknownLoanEstimateWasSourcedFromCoC", archive.WasSourcedFromPendingCoC);
                        this.InvalidateUnknownArchiveMessage.Text = ErrorMessages.ArchiveError.ConfirmUnknownArchiveInvalidation(
                            archive.DateArchived);
                    }

                    preventCoC = true;
                }

                if (pageData.sLastDisclosedGFEArchiveId == Guid.Empty)
                {
                    RegisterJsGlobalVariables("BlockCoCForNoLastDisclosedLoanEstimate", true);
                    this.NoLastDisclosedArchiveOnFileMessage.Text = ErrorMessages.ArchiveError.UnableToCoCBecauseNoLastDisclosedArchive;
                    preventCoC = true;
                }

                if (preventCoC)
                {
                    this.bApply.Visible = false;

                    // OPM 219294. Do not want previous CoCs value for these.
                    sCircumstanceChangeD.Text = "";
                    sCircumstanceChangeExplanation.Value = "";

                    return;
                }
            }
                
            if (IsTabViewPast)
            {

                var cocas = (from c in pageData.sClosingCostCoCArchive
                            where c.DateArchived == CoCArchives.SelectedValue
                            select c);
                if (cocas.Count() > 0)
                {
                    var coca = cocas.First();

                    // Load UI from this Archive
                    BindArchiveTable(coca);
                    pageData.sGfeRedisclosureD_rep = coca.RedisclosureD;
                    pageData.sCircumstanceChangeExplanation = coca.CircumstanceChangeExplanation;
                    pageData.sCircumstanceChangeD_rep = coca.CircumstanceChangeD;

                    if (this.ComplianceEagleEnabled && pageData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                    {
                        this.ComplianceCOCCategory.SelectedIndex = (int)coca.ComplianceCOCCategory;
                    }
                }
            }

            // gf opm 228619 -- If the file is in a state where we cannot determine the
            // last disclosed archive and the user is trying to create a new CoC, the 
            // code will exit before this point.
            LoadData(pageData);
        }

        private void LoadData(CPageData initializedDataLoan)
        {
            if (IsTabCreateNew)
            {
                BindChangeTables(pageData.sClosingCostCoCFeeChanges);

                // OPM 219294. Do not want previous CoCs value for these.
                sCircumstanceChangeD.Text = "";
                sCircumstanceChangeExplanation.Value = "";

                if (this.Broker.IsRemovePreparedDates)
                {
	                IPreparerFields gfe = pageData.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
	                GfePrepareDate.Text = gfe.PrepareDate_rep;
	            }
            }
            else
            {
                sCircumstanceChangeExplanation.Value = pageData.sCircumstanceChangeExplanation;
                sCircumstanceChangeD.Text = pageData.sCircumstanceChangeD_rep;
            }

            sGfeRedisclosureD.Text = pageData.sGfeRedisclosureD_rep;
            if (IsProtectDisclosureDates)
            {
                sGfeRedisclosureD.ReadOnly = true;
            }

            if(!this.Broker.IsRemovePreparedDates)
            {
                IPreparerFields gfe = pageData.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                GfePrepareDate.Text = gfe.PrepareDate_rep;
            }
        }

        private void BindChangeTables(List<FeeChange> feeChanges)
        {
            HtmlTable changeTable = new HtmlTable();

            changeTable.Width = "100%";
            HtmlTableRow headerRow = new HtmlTableRow();
            headerRow.Attributes.Add("class", "GridHeader");
            headerRow.Cells.Add(new HtmlTableCell() { InnerText = "" });
            headerRow.Cells.Add(new HtmlTableCell() { InnerText = "Fee" });
            headerRow.Cells.Add(new HtmlTableCell() { InnerText = "Archive Value" });
            headerRow.Cells.Add(new HtmlTableCell() { InnerText = "Current Value" });
            changeTable.Rows.Add(headerRow);

            int rowCount = 0;
            var loanAmtFees = feeChanges.Where(p => p.IsPercentFee && p.PercentBase == E_PercentBaseT.LoanAmount && !p.IsLoanField && !p.IsReserveOrPrepaid());
            if (loanAmtFees.Count() != 0)
            {
                
                HtmlTableRow groupRow = new HtmlTableRow();
                groupRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                groupRow.Cells.Add(new HtmlTableCell() { InnerHtml = "<input type='checkbox' onClick='onLoanAmtClick()' id='allLAmtFeeChk' />" });
                groupRow.Cells.Add(new HtmlTableCell() { InnerText = "Loan Amount Change" });
                groupRow.Cells.Add(new HtmlTableCell());
                groupRow.Cells.Add(new HtmlTableCell());
                changeTable.Rows.Add(groupRow);

                foreach (var change in loanAmtFees)
                {
                    HtmlTableRow changeRow = new HtmlTableRow();
                    changeRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                    if (change.DoNotShow)
                    {
                        changeRow.Style["display"] = "none";
                    }
                    changeRow.Cells.Add(new HtmlTableCell() { InnerHtml = string.Format("<input type='checkbox' data1='{0}' class='lAmtFeeChk' onClick='onFeeClick();' style='display:none' />", AspxTools.HtmlString(change.FeeId)) });

                    var descCell = new HtmlTableCell() { InnerText = change.Description };
                    descCell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "23px");
                    changeRow.Cells.Add(descCell);
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = change.ArchiveValue });
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = change.CurrentValue });
                    changeTable.Rows.Add(changeRow);

                }
                rowCount++;
            }

            Func<E_PercentBaseT, bool> percentBaseGroupedWithTotalLoanAmount = percentBase =>
                percentBase == E_PercentBaseT.TotalLoanAmount
                || percentBase == E_PercentBaseT.AverageOutstandingBalance
                || percentBase == E_PercentBaseT.DecliningRenewalsMonthly
                || percentBase == E_PercentBaseT.DecliningRenewalsAnnually;
            var totalLoanAmtFees = feeChanges.Where(p => p.IsPercentFee && percentBaseGroupedWithTotalLoanAmount(p.PercentBase) && !p.IsLoanField && !p.IsReserveOrPrepaid());
            if (totalLoanAmtFees.Count() != 0)
            {

                HtmlTableRow groupRow = new HtmlTableRow();
                groupRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                groupRow.Cells.Add(new HtmlTableCell() { InnerHtml = "<input type='checkbox' onClick='onTotLoanAmtClick()' id='allTotLAmtFeeChk' />" });
                groupRow.Cells.Add(new HtmlTableCell() { InnerText = "Total Loan Amount Change" });
                groupRow.Cells.Add(new HtmlTableCell());
                groupRow.Cells.Add(new HtmlTableCell());
                changeTable.Rows.Add(groupRow);

                foreach (var change in totalLoanAmtFees)
                {
                    HtmlTableRow changeRow = new HtmlTableRow();
                    changeRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                    if (change.DoNotShow)
                    {
                        changeRow.Style["display"] = "none";
                    }
                    changeRow.Cells.Add(new HtmlTableCell() { InnerHtml = string.Format("<input type='checkbox' data1='{0}' class='totLAmtFeeChk' onClick='onFeeClick();' style='display:none' />", AspxTools.HtmlString(change.FeeId))});

                    var descCell = new HtmlTableCell() { InnerText = change.Description };
                    descCell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "23px");
                    changeRow.Cells.Add(descCell);
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = change.ArchiveValue });
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = change.CurrentValue });
                    changeTable.Rows.Add(changeRow);

                }
                rowCount++;
            }

            var purchasePriceFees = feeChanges.Where(p => p.IsPercentFee && p.PercentBase == E_PercentBaseT.SalesPrice && !p.IsLoanField && !p.IsReserveOrPrepaid());
            if (purchasePriceFees.Count() != 0)
            {
                HtmlTableRow groupRow = new HtmlTableRow();
                groupRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                groupRow.Cells.Add(new HtmlTableCell() { InnerHtml = "<input type='checkbox' onClick='onPurchPriceClick()' id='allPurchPriceFeeChk' />" });
                groupRow.Cells.Add(new HtmlTableCell() { InnerText = "Purchase Price Change" });
                groupRow.Cells.Add(new HtmlTableCell());
                groupRow.Cells.Add(new HtmlTableCell());
                changeTable.Rows.Add(groupRow);
                
                foreach (var change in purchasePriceFees)
                {
                    HtmlTableRow changeRow = new HtmlTableRow();
                    changeRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                    if (change.DoNotShow)
                    {
                        changeRow.Style["display"] = "none";
                    }
                    changeRow.Cells.Add(new HtmlTableCell() { InnerHtml = string.Format( "<input type='checkbox' data1='{0}' class='purchPriceFeeChk' onClick='onFeeClick();' style='display:none' />", AspxTools.HtmlString(change.FeeId)) });

                    var descCell = new HtmlTableCell() { InnerText = change.Description };
                    descCell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "23px");
                    changeRow.Cells.Add(descCell);
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = change.ArchiveValue });
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = change.CurrentValue });
                    changeTable.Rows.Add(changeRow);
                }
                rowCount++;
            }

            var appraisalFees = feeChanges.Where(p => p.IsPercentFee && p.PercentBase == E_PercentBaseT.AppraisalValue && !p.IsLoanField && !p.IsReserveOrPrepaid());
            if (appraisalFees.Count() != 0)
            {
                HtmlTableRow groupRow = new HtmlTableRow();
                groupRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                groupRow.Cells.Add(new HtmlTableCell() { InnerHtml = "<input type='checkbox' onClick='onAppraisalClick()' id='allAppraisalFeeChk' />" });
                groupRow.Cells.Add(new HtmlTableCell() { InnerText = "Appraisal Value Change" });
                groupRow.Cells.Add(new HtmlTableCell());
                groupRow.Cells.Add(new HtmlTableCell());
                changeTable.Rows.Add(groupRow);

                foreach (var change in appraisalFees)
                {
                    HtmlTableRow changeRow = new HtmlTableRow();
                    changeRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                    if (change.DoNotShow)
                    {
                        changeRow.Style["display"] = "none";
                    }
                    changeRow.Cells.Add(new HtmlTableCell() { InnerHtml = string.Format( "<input type='checkbox' data1='{0}' class='appraisalFeeChk' onClick='onFeeClick();' style='display:none' />", AspxTools.HtmlString(change.FeeId)) });

                    var descCell = new HtmlTableCell() { InnerText = change.Description };
                    descCell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "23px");
                    changeRow.Cells.Add(descCell);
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = change.ArchiveValue });
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = change.CurrentValue });
                    changeTable.Rows.Add(changeRow);
                }
                rowCount++;
            }

            var reserveFees = feeChanges.Where(p => !p.IsLoanField && p.IsReserveOrPrepaid());
            if (reserveFees.Any())
            {
                HtmlTableRow groupRow = new HtmlTableRow();
                groupRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                groupRow.Cells.Add(new HtmlTableCell() { InnerHtml = "<input type='checkbox' onClick='onReserveClick()' id='allReserveFeeChk' />" });
                groupRow.Cells.Add(new HtmlTableCell() { InnerText = "Housing Expenses/Prepaids and Reserves" });
                groupRow.Cells.Add(new HtmlTableCell());
                groupRow.Cells.Add(new HtmlTableCell());
                changeTable.Rows.Add(groupRow);

                foreach (var change in reserveFees)
                {
                    HtmlTableRow changeRow = new HtmlTableRow();
                    changeRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                    if (change.DoNotShow)
                    {
                        changeRow.Style["display"] = "none";
                    }
                    changeRow.Cells.Add(new HtmlTableCell() { InnerHtml = string.Format("<input type='checkbox' data1='{0}' class='reserveFeeChk' onClick='onFeeClick();' style='display:none' />", AspxTools.HtmlString(change.FeeId)) });

                    var cellDescription = change.Description;
                    if (change.IsPrepaid())
                    {
                        cellDescription = "Prepaid - " + cellDescription;
                    }

                    var descCell = new HtmlTableCell() { InnerText = cellDescription };
                    descCell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "23px");
                    changeRow.Cells.Add(descCell);
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = change.ArchiveValue });
                    changeRow.Cells.Add(new HtmlTableCell() { InnerText = change.CurrentValue });
                    changeTable.Rows.Add(changeRow);
                }
                rowCount++;
            }

            HashSet<string> seenIds = new HashSet<string>();
            foreach (var change in feeChanges.Where(p => !p.IsPercentFee && !p.IsLoanField && !p.IsReserveOrPrepaid()))
            {
                if (seenIds.Contains(change.FeeId)) continue;

                // Grouping same fees with different name.
                string className = rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem";
                bool firstItem = true;
                foreach (var changeItem in feeChanges.Where(p => !p.IsPercentFee && p.FeeId == change.FeeId && !p.IsLoanField))
                {
                    if (changeItem.ArchiveValue != changeItem.CurrentValue)
                    {
                        HtmlTableRow changeRow = new HtmlTableRow();
                        changeRow.Attributes.Add("class", className);
                        if (changeItem.DoNotShow)
                        {
                            changeRow.Style["display"] = "none";
                        }
                        changeRow.Cells.Add(new HtmlTableCell() { InnerHtml = firstItem ? string.Format("<input type='checkbox' data1='{0}' class='feechk' onClick='onFeeClick();' />", AspxTools.HtmlString(changeItem.FeeId)) : "" });
                        changeRow.Cells.Add(new HtmlTableCell() { InnerText = changeItem.Description });
                        changeRow.Cells.Add(new HtmlTableCell() { InnerText = changeItem.ArchiveValue });
                        changeRow.Cells.Add(new HtmlTableCell() { InnerText = changeItem.CurrentValue });
                        changeTable.Rows.Add(changeRow);

                        firstItem = false;
                    }
                }

                if (!firstItem) rowCount++;
                seenIds.Add(change.FeeId);
                
            }

            var loanValueChanges = feeChanges.Where(p => p.IsLoanField);
            if (loanValueChanges.Count() != 0)
            {
                // Add spacer row.
                HtmlTableRow spacerRow = new HtmlTableRow();
                //spacerRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                spacerRow.Cells.Add(new HtmlTableCell() { Height = "20px" });
                spacerRow.Cells.Add(new HtmlTableCell());
                spacerRow.Cells.Add(new HtmlTableCell());
                changeTable.Rows.Add(spacerRow);
            }

            foreach (var change in loanValueChanges)
            {
                string checkboxId = "";
                switch (change.PercentBase)
                {
                    case E_PercentBaseT.SalesPrice: checkboxId = "onlyPurchPriceFeeChk"; break;
                    case E_PercentBaseT.LoanAmount: checkboxId = "onlyLAmtFeeChk"; break;
                    case E_PercentBaseT.TotalLoanAmount: checkboxId = "onlyTotLAmtFeeChk"; break;
                    case E_PercentBaseT.AppraisalValue: checkboxId = "onlyAppraisalFeeChk"; break;
                    default:
                        throw new UnhandledEnumException(change.PercentBase);
                }

                HtmlTableRow changeRow = new HtmlTableRow();
                changeRow.Attributes.Add("class", rowCount++ % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                changeRow.Cells.Add(new HtmlTableCell() { InnerHtml = "<input type='checkbox' onClick='onAppraisalClick()' id='" + checkboxId + "' />" });
                changeRow.Cells.Add(new HtmlTableCell() { InnerText = change.Description });
                changeRow.Cells.Add(new HtmlTableCell() { InnerText = change.ArchiveValue });
                changeRow.Cells.Add(new HtmlTableCell() { InnerText = change.CurrentValue });
                changeTable.Rows.Add(changeRow);
            }

            changeList.Controls.Add(changeTable);
            
        }

        private void BindArchiveTable(ClosingCostCoCArchive archive)
        {
            HtmlTable changeTable = new HtmlTable();
            changeTable.Width = "100%";
            HtmlTableRow headerRow = new HtmlTableRow();
            headerRow.Attributes.Add("class", "GridHeader");
            headerRow.Cells.Add(new HtmlTableCell() { InnerText = "Fee" });
            headerRow.Cells.Add(new HtmlTableCell() { InnerText = "Value" });
            changeTable.Rows.Add(headerRow);

            int rowCount = 0;
            foreach (var fee in archive.Fees)
            {
                HtmlTableRow feeRow = new HtmlTableRow();
                feeRow.Attributes.Add("class", rowCount++ % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                feeRow.Cells.Add(new HtmlTableCell() { InnerText = fee.Description });
                feeRow.Cells.Add(new HtmlTableCell() { InnerText = fee.Value.ToString("C") });
                changeTable.Rows.Add(feeRow);
            }

            if (archive.LoanValues != null && archive.LoanValues.Count() != 0)
            {
                // Add spacer row.
                HtmlTableRow spacerRow = new HtmlTableRow();
                //spacerRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                spacerRow.Cells.Add(new HtmlTableCell() { Height = "20px" });
                spacerRow.Cells.Add(new HtmlTableCell());
                spacerRow.Cells.Add(new HtmlTableCell());
                changeTable.Rows.Add(spacerRow);

                foreach (var loanValue in archive.LoanValues)
                {
                    HtmlTableRow feeRow = new HtmlTableRow();
                    feeRow.Attributes.Add("class", rowCount++ % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                    feeRow.Cells.Add(new HtmlTableCell() { InnerText = loanValue.Description });
                    feeRow.Cells.Add(new HtmlTableCell() { InnerText = loanValue.Value.ToString("C") });
                    changeTable.Rows.Add(feeRow);
                }

            }

            changeList.Controls.Add(changeTable);
        }

        /// <summary>
        /// Binds the changed circumstance category dropdown from the associated enumeration.
        /// </summary>
        /// <param name="regulationType">The regulation type of the loan file.</param>
        private void BindComplianceCOCCategory(E_sDisclosureRegulationT regulationType)
        {
            if (this.ComplianceEagleEnabled && regulationType == E_sDisclosureRegulationT.TRID && this.ComplianceCOCCategory.Items.Count == 0)
            {
                LendersOffice.Conversions.ComplianceEagleIntegration.ComplianceEagleUtil.Bind_CEagleChangedCircumstanceCategory(this.ComplianceCOCCategory);
            }
        }
    }
}
