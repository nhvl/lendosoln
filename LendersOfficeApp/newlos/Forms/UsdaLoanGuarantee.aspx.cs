﻿using System;
using System.Collections;
using System.Linq;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Migration;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class UsdaLoanGuarantee : BaseLoanPage
    {        
        protected void PageInit(object sender, System.EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            Tools.Bind_aHispanicT(aBHispanicT, shouldIncludeBothOption: true);
            
            Tools.Bind_TriState(aBUsdaGuaranteeVeteranTri);
            aBUsdaGuaranteeVeteranTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);

            Tools.Bind_TriState(aBUsdaGuaranteeDisabledTri);
            aBUsdaGuaranteeDisabledTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
            Tools.Bind_GenderT(aBGenderT, shouldIncludeBothOption: true);
            Tools.Bind_TriState(aBUsdaGuaranteeFirstTimeHomeBuyerTri);
            aBUsdaGuaranteeFirstTimeHomeBuyerTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
            Tools.Bind_aMaritalStatT(aBMaritalStatT);
            Tools.Bind_aUsdaGuaranteeEmployeeRelationshipT(aBUsdaGuaranteeEmployeeRelationshipT);
            aBUsdaGuaranteeEmployeeRelationshipT.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);

            Tools.Bind_aHispanicT(aCHispanicT, shouldIncludeBothOption: true);
            Tools.Bind_TriState(aCUsdaGuaranteeVeteranTri);
            aCUsdaGuaranteeVeteranTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
            Tools.Bind_TriState(aCUsdaGuaranteeDisabledTri);
            aCUsdaGuaranteeDisabledTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
            Tools.Bind_GenderT(aCGenderT, shouldIncludeBothOption: true);
            Tools.Bind_TriState(aCUsdaGuaranteeFirstTimeHomeBuyerTri);
            aCUsdaGuaranteeFirstTimeHomeBuyerTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
            Tools.Bind_aMaritalStatT(aCMaritalStatT);
            Tools.Bind_aUsdaGuaranteeEmployeeRelationshipT(aCUsdaGuaranteeEmployeeRelationshipT);
            aCUsdaGuaranteeEmployeeRelationshipT.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);

            Tools.Bind_sUsdaGuaranteeRefinancedLoanT(sUsdaGuaranteeRefinancedLoanT);
            sUsdaGuaranteeRefinancedLoanT.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);

            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);
        }

        // Populate the controls
        private void BindDataObject(CPageData dataLoan, CAppData dataApp)
        {
            // ** Approved Lender Table
            IPreparerFields approvedLender = dataLoan.GetPreparerOfForm(E_PreparerFormT.UsdaGuaranteeApprovedLender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            UsdaGuaranteeApprovedLenderCompanyName.Text = approvedLender.CompanyName;
            UsdaGuaranteeApprovedLenderTaxId.Text = approvedLender.TaxId;
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V18_ConsolidateTaxIdFields))
            {
                UsdaGuaranteeApprovedLenderTaxId.Attributes.Add("preset", "employerIdentificationNumber");
            }

            UsdaGuaranteeApprovedLenderPreparerName.Text = approvedLender.PreparerName;
            UsdaGuaranteeApprovedLenderEmailAddr.Text = approvedLender.EmailAddr;
            UsdaGuaranteeApprovedLenderPhoneOfCompany.Text = approvedLender.PhoneOfCompany;
            UsdaGuaranteeApprovedLenderFaxOfCompany.Text = approvedLender.FaxOfCompany;
            UsdaGuaranteeApprovedLenderTitle.Text = approvedLender.Title;

            IPreparerFields thirdPartyTPO = dataLoan.GetPreparerOfForm(E_PreparerFormT.UsdaGuaranteeThirdPartyOriginator, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            UsdaGuaranteeThirdPartyOriginatorCompanyName.Text = thirdPartyTPO.CompanyName;
            UsdaGuaranteeThirdPartyOriginatorTaxId.Text = thirdPartyTPO.TaxId;
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V18_ConsolidateTaxIdFields))
            {
                UsdaGuaranteeThirdPartyOriginatorTaxId.Attributes.Add("preset", "employerIdentificationNumber");
            }

            sUsdaGuaranteeNum.Text = dataLoan.sUsdaGuaranteeNum;
            sLNm.Text = dataLoan.sLNm;
            sLNm.ReadOnly = !PrincipalFactory.CurrentPrincipal.HasPermission(Permission.CanModifyLoanName);

            // ** Applicant/Co-Applicant Info Table
            aBFirstNm.Text = dataApp.aBFirstNm;
            aBMidNm.Text = dataApp.aBMidNm;
            aBLastNm.Text = dataApp.aBLastNm;
            aBSuffix.Text = dataApp.aBSuffix;
            aBSsn.Text = dataApp.aBSsn;
            aBDob.Text = dataApp.aBDob_rep;
            aBDecCitizen.Text = dataApp.aBDecCitizen;
            aBDecResidency.Text = dataApp.aBDecResidency;

            Tools.Set_TriState(aBUsdaGuaranteeVeteranTri, dataApp.aBUsdaGuaranteeVeteranTri);
            Tools.Set_TriState(aBUsdaGuaranteeDisabledTri, dataApp.aBUsdaGuaranteeDisabledTri);
            Tools.SetDropDownListValue(aBGenderT, dataApp.aBGender);
            Tools.Set_TriState(aBUsdaGuaranteeFirstTimeHomeBuyerTri, dataApp.aBUsdaGuaranteeFirstTimeHomeBuyerTri);

            Tools.SetDropDownListValue(aBHispanicT, dataApp.aBHispanicT);
            
            aBIsAmericanIndian.Checked = dataApp.aBIsAmericanIndian;
            aBIsPacificIslander.Checked = dataApp.aBIsPacificIslander;
            aBIsAsian.Checked = dataApp.aBIsAsian;
            aBIsWhite.Checked = dataApp.aBIsWhite;
            aBIsBlack.Checked = dataApp.aBIsBlack;

            Tools.SetDropDownListValue(aBMaritalStatT, dataApp.aBMaritalStatT);
            
            Tools.Set_aUsdaGuaranteeEmployeeRelationshipT(aBUsdaGuaranteeEmployeeRelationshipT, dataApp.aBUsdaGuaranteeEmployeeRelationshipT);
            aBUsdaGuaranteeEmployeeRelationshipDesc.Text = dataApp.aBUsdaGuaranteeEmployeeRelationshipDesc;

            aBDecisionCreditScore.Text = dataApp.aBDecisionCreditScore_rep;
            aBHasHighestScore.Checked = !dataApp.aBHasHighestScore;

            aCFirstNm.Text = dataApp.aCFirstNm;
            aCMidNm.Text = dataApp.aCMidNm;
            aCLastNm.Text = dataApp.aCLastNm;
            aCSuffix.Text = dataApp.aCSuffix;
            aCSsn.Text = dataApp.aCSsn;
            aCDob.Text = dataApp.aCDob_rep;
            aCDecCitizen.Text = dataApp.aCDecCitizen;
            aCDecResidency.Text = dataApp.aCDecResidency;

            Tools.Set_TriState(aCUsdaGuaranteeVeteranTri, dataApp.aCUsdaGuaranteeVeteranTri);
            Tools.Set_TriState(aCUsdaGuaranteeDisabledTri, dataApp.aCUsdaGuaranteeDisabledTri);
            Tools.SetDropDownListValue(aCGenderT, dataApp.aCGender);
            Tools.Set_TriState(aCUsdaGuaranteeFirstTimeHomeBuyerTri, dataApp.aCUsdaGuaranteeFirstTimeHomeBuyerTri);

            Tools.SetDropDownListValue(aCHispanicT, dataApp.aCHispanicT);

            aCIsAmericanIndian.Checked = dataApp.aCIsAmericanIndian;
            aCIsPacificIslander.Checked = dataApp.aCIsPacificIslander;
            aCIsAsian.Checked = dataApp.aCIsAsian;
            aCIsWhite.Checked = dataApp.aCIsWhite;
            aCIsBlack.Checked = dataApp.aCIsBlack;

            Tools.SetDropDownListValue(aCMaritalStatT, dataApp.aCMaritalStatT);

            Tools.Set_aUsdaGuaranteeEmployeeRelationshipT(aCUsdaGuaranteeEmployeeRelationshipT, dataApp.aCUsdaGuaranteeEmployeeRelationshipT);
            aCUsdaGuaranteeEmployeeRelationshipDesc.Text = dataApp.aCUsdaGuaranteeEmployeeRelationshipDesc;

            aCDecisionCreditScore.Text = dataApp.aCDecisionCreditScore_rep;
            aCHasHighestScore.Checked = !dataApp.aCHasHighestScore;

            // ** Extra information
            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;

            sSpZip.Text = dataLoan.sSpZip;
            sSpZip.SmartZipcode(sSpCity, sSpState);

            if (dataLoan.sSpState.Length != 0)
            {
                ArrayList list = StateInfo.Instance.GetCountiesFor(dataLoan.sSpState);
                list.Insert(0, String.Empty);
                sSpCounty.DataSource = list;
                sSpCounty.DataBind();
            }
            Tools.Bind_sSpCounty(dataLoan.sSpState, sSpCounty, true);
            Tools.SetDropDownListCaseInsensitive(sSpCounty, dataLoan.sSpCounty); 

            sUsdaGuaranteeIsRefinanceLoan.Checked = dataLoan.sUsdaGuaranteeIsRefinanceLoan;
            Tools.Set_sUsdaGuaranteeRefinancedLoanT(sUsdaGuaranteeRefinancedLoanT, dataLoan.sUsdaGuaranteeRefinancedLoanT);

            sUsdaGuaranteeNumOfPersonInHousehold.Text = dataLoan.sUsdaGuaranteeNumOfPersonInHousehold_rep;
            sUsdaGuaranteeNumOfDependents.Text = dataLoan.sUsdaGuaranteeNumOfDependents_rep;

            sLTotAnnualI.Text = dataLoan.sLTotAnnualI_rep;
            sUsdaGuaranteeAdjustedAnnualIncome.Text = dataLoan.sUsdaGuaranteeAdjustedAnnualIncome_rep;

            sQualTopR.Text = dataLoan.sQualTopR_rep;
            sQualBottomR.Text = dataLoan.sQualBottomR_rep;

            sLAmtCalc.Text = dataLoan.sLAmtCalc_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sMonthlyPmt.Text = dataLoan.sMonthlyPmt_rep;

            sUsdaGuaranteeIsNoteRateBasedOnFnma.Checked = dataLoan.sUsdaGuaranteeIsNoteRateBasedOnFnma;
            sUsdaGuaranteeNoteRateBasedOnFnmaD.Text = dataLoan.sUsdaGuaranteeNoteRateBasedOnFnmaD_rep;

            sUsdaGuaranteeIsNoteRateLocked.Checked = dataLoan.sUsdaGuaranteeIsNoteRateLocked;
            sUsdaGuaranteeNoteRateLockedD.Text = dataLoan.sUsdaGuaranteeNoteRateLockedD_rep;

            sUsdaGuaranteeIsNoteRateFloat.Checked = dataLoan.sUsdaGuaranteeIsNoteRateFloat;

            // ** Loan funds purpose
            sUsdaGuaranteePurchaseAmtDesc.Text = dataLoan.sUsdaGuaranteePurchaseAmtDesc;
            sUsdaGuaranteePurchaseAmt.Text = dataLoan.sUsdaGuaranteePurchaseAmt_rep;

            sUsdaGuaranteeClosingCostsDesc.Text = dataLoan.sUsdaGuaranteeClosingCostsDesc;
            sUsdaGuaranteeClosingCosts.Text = dataLoan.sUsdaGuaranteeClosingCosts_rep;

            sUsdaGuaranteeRepairFeeDesc.Text = dataLoan.sUsdaGuaranteeRepairFeeDesc;
            sUsdaGuaranteeRepairFee.Text = dataLoan.sUsdaGuaranteeRepairFee_rep;

            sUsdaGuaranteeGuaranteeFeeDesc.Text = dataLoan.sUsdaGuaranteeGuaranteeFeeDesc;
            sUsdaGuaranteeGuaranteeFee.Text = dataLoan.sUsdaGuaranteeGuaranteeFee_rep;

            sUsdaGuaranteeTotalRequestFee.Text = dataLoan.sUsdaGuaranteeTotalRequestFee_rep;
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(UsdaLoanGuarantee));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            BindDataObject(dataLoan, dataApp);
        }

        protected override void SaveData()
        {

        }

        private void ZeroOneCheckBoxList_Load(object sender, System.EventArgs e)
        {
            CheckBoxList cbl = (CheckBoxList)sender;
            ArrayList list = new ArrayList();

            foreach (ListItem o in cbl.Items)
            {
                list.Add("'" + o.Value + "'");
            }

            Page.ClientScript.RegisterArrayDeclaration(cbl.ClientID + "_items", string.Join(",", (string[])list.ToArray(typeof(string))));
            Page.ClientScript.RegisterHiddenField("cbl_" + cbl.ClientID, "");
            ((BasePage)Page).AddInitScriptFunctionWithArgs("_registerCBL", string.Format("'{0}', {1}", cbl.ClientID, cbl.Items.Count));
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            IsAppSpecific = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        /// 
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion

        protected override void OnPreRender(EventArgs e)
        {
            this.PageTitle = "USDA RD3555-21";
            this.PageID = "UsdaLoanGuarantee";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CUsdaLoanGuaranteePDF);
            base.OnPreRender(e);
        }
    }
}

