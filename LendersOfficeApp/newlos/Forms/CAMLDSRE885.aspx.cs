﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class CAMLDSRE885 : BaseLoanPage
    {
        protected CAMLDSRE885pg1 CAMLDSRE885pg1;
        protected CAMLDSRE885pg2 CAMLDSRE885pg2;
        protected CAMLDSRE885pg3 CAMLDSRE885pg3;
        protected CAMLDSRE885pg4 CAMLDSRE885pg4;

        protected override void OnInit(EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");

            UseNewFramework = true;
            this.EnableJqueryMigrate = false;
            IsAppSpecific = false;
            base.OnInit(e);

            Tabs.AddToQueryString("loanid", LoanID.ToString());
            Tabs.RegisterControl("Page 1", CAMLDSRE885pg1);
            Tabs.RegisterControl("Page 2", CAMLDSRE885pg2);
            Tabs.RegisterControl("Page 3", CAMLDSRE885pg3);
            Tabs.RegisterControl("Page 4", CAMLDSRE885pg4);
        }



        private void InitializeComponent()
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            int index = Tabs.TabIndex + 1;
            this.PageTitle = "CA RE 885 page " + index;
            this.PageID = "CARE885_" + index;
            switch (index)
            {
                case 1:
                    this.PDFPrintClass = typeof(LendersOffice.Pdf.CRe885_1PDF);
                    break;
                case 2:
                    this.PDFPrintClass = typeof(LendersOffice.Pdf.CRe885_2PDF);
                    break;
                case 3:
                    this.PDFPrintClass = typeof(LendersOffice.Pdf.CRe885_3PDF);
                    break;
                case 4:
                    this.PDFPrintClass = typeof(LendersOffice.Pdf.CRe885_4PDF);
                    break;
            }
            base.OnPreRender(e);
        }
    }
}
