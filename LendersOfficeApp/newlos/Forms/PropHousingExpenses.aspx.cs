﻿namespace LendersOfficeApp.newlos.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.TRID2;

    [DataContract]
    public class PageViewModel
    {
        public PageViewModel()
        {
            this.PropInsExps = new List<HousingExpenseViewModel>();
            this.TaxesExps = new List<HousingExpenseViewModel>();
            this.OtherExps = new List<HousingExpenseViewModel>();
            this.DisbModeExpenses = new List<string>();
            this.EscrowedDescriptions = new List<string>();
            this.NonEscrowedDescriptions = new List<string>();

            VRoot = Tools.VRoot;
        }

        [DataMember]
        public List<HousingExpenseViewModel> PropInsExps { get; set; }
        [DataMember]
        public List<HousingExpenseViewModel> TaxesExps { get; set; }
        [DataMember]
        public List<HousingExpenseViewModel> OtherExps { get; set; }
        [DataMember]
        public bool AnySetToAny { get; set; }
        [DataMember]
        public string VRoot { get; set; }
        [DataMember]
        public string sEstCloseD { get; set; }
        [DataMember]
        public bool sEstCloseDLckd { get; set; }
        [DataMember]
        public string sDocMagicClosingD { get; set; }
        [DataMember]
        public string sSchedDueD1 { get; set; }
        [DataMember]
        public bool sSchedDueD1Lckd { get; set; }
        [DataMember]
        public string sProMIns { get; set; }
        [DataMember]
        public bool sMInsRsrvEscrowedTri { get; set; }
        [DataMember]
        public bool sIssMInsRsrvEscrowedTriReadOnly { get; set; }
        [DataMember]
        public bool sIsMipPrepaid { get; set; }
        [DataMember]
        public string sMipPiaMon { get; set; }
        [DataMember]
        public string sMipPia { get; set; }
        [DataMember]
        public string sRecurringMipPia { get; set; }
        [DataMember]
        public bool sMInsRsrvMonLckd { get; set; }
        [DataMember]
        public string sMInsRsrvMon { get; set; }
        [DataMember]
        public string sMInsRsrv { get; set; }
        [DataMember]
        public string MICush { get; set; }
        [DataMember]
        public E_AggregateEscrowCalculationModeT sAggEscrowCalcModeT { get; set; }
        [DataMember]
        public E_CustomaryEscrowImpoundsCalcMinT sCustomaryEscrowImpoundsCalcMinT { get; set; }
        [DataMember]
        public bool sAggregateAdjRsrvLckd { get; set; }
        [DataMember]
        public string sAggregateAdjRsrv { get; set; }
        [DataMember]
        public string sGfeInitialImpoundDeposit { get; set; }
        [DataMember]
        public string sMonthlyNonPIExpenseTotalPITI { get; set; }
        [DataMember]
        public string sServicingEscrowPmtNonMI { get; set; }
        [DataMember]
        public string sServicingEscrowPmt { get; set; }
        [DataMember]
        public E_sClosingCostFeeVersionT sClosingCostFeeVersionT { get; set; }
        [DataMember]
        public string sTotalPropertyInsurancePITI { get; set; }
        [DataMember]
        public string sTotalRealtyTaxesPITI { get; set; }
        [DataMember]
        public string sTotalOtherHousingExpensesPITI { get; set; }
        [DataMember]
        public List<string> DisbModeExpenses { get; set; }
        [DataMember]
        public List<string> EscrowedDescriptions { get; set; }
        [DataMember]
        public List<string> NonEscrowedDescriptions { get; set; }
        [DataMember]
        public bool sTridEscrowAccountExists { get; set; }
        [DataMember]
        public bool sTridEscrowAccountExists_True { get; set; }
        [DataMember]
        public bool sTridEscrowAccountExists_False { get; set; }
        [DataMember]
        public bool sNonMIHousingExpensesEscrowedReasonT_LenderRequired { get; set; }
        [DataMember]
        public bool sNonMIHousingExpensesEscrowedReasonT_BorrowerRequested { get; set; }
        [DataMember]
        public string sClosingDisclosureEscrowedPropertyCostsFirstYear { get; set; }
        [DataMember]
        public string sClosingDisclosureNonEscrowedPropertyCostsFirstYear { get; set; }
        [DataMember]
        public string sClosingDisclosureMonthlyEscrowPayment { get; set; }
        [DataMember]
        public bool sNonMIHousingExpensesNotEscrowedReasonT_Declined { get; set; }
        [DataMember]
        public bool sNonMIHousingExpensesNotEscrowedReasonT_NotOffered { get; set; }
        [DataMember]
        public string sClosingDisclosurePropertyCostsFirstYear { get; set; }
        [DataMember]
        public string sTRIDEscrowWaiverFee { get; set; }

    }

    [DataContract]
    public class HousingExpenseViewModel
    {
        public HousingExpenseViewModel()
        {
            this.Disbursements = new List<DisbursementViewModel>();
            this.isVisible = true;
            this.DisbSched = new string[12];
        }

        [DataMember]
        public string Category { get; set; }
        [DataMember]
        public bool isVisible { get; set; }
        [DataMember]
        public E_HousingExpenseTypeT expenseType { get; set; }
        [DataMember]
        public E_TaxTableTaxT taxT { get; set; }
        [DataMember]
        public bool isPrepaid { get; set; }
        [DataMember]
        public bool IsCustom { get; set; }
        [DataMember]
        public bool IsInsurance { get; set; }
        [DataMember]
        public bool IsTax { get; set; }
        [DataMember]
        public bool IsOther { get; set; }
        [DataMember]
        public E_CustomExpenseLineNumberT customLineNum { get; set; }
        [DataMember]
        public string customLineNumAsString { get; set; }
        [DataMember]
        public string removedDisbs { get; set; }
        [DataMember]
        public E_AnnualAmtCalcTypeT calcMode { get; set; }
        [DataMember]
        public bool flippedOnce { get; set; }
        [DataMember]
        public string expName { get; set; }
        [DataMember]
        public string expDesc { get; set; }
        [DataMember]
        public string expPrefix { get; set; }
        [DataMember]
        public bool isEscrowed { get; set; }
        [DataMember]
        public E_AnnualAmtCalcTypeT annAmtCalcT { get; set; }
        [DataMember]
        public string annAmtTot { get; set; }
        [DataMember]
        public string monTotPITI { get; set; }
        [DataMember]
        public string prepMnth { get; set; }
        [DataMember]
        public string prepAmt { get; set; }
        [DataMember]
        public string monTotServ { get; set; }
        [DataMember]
        public string rsrvMon { get; set; }
        [DataMember]
        public bool rsrvMonLckd { get; set; }
        [DataMember]
        public string rsrvAmt { get; set; }
        [DataMember]
        public string annAmtPerc { get; set; }
        [DataMember]
        public E_PercentBaseT annAmtBase { get; set; }
        [DataMember]
        public string monAmtFixed { get; set; }
        [DataMember]
        public string firstDisbD { get; set; }
        [DataMember]
        public string[] DisbSched { get; set; }
        [DataMember]
        public string Cush { get; set; }
        [DataMember]
        public E_DisbursementRepIntervalT RepInterval { get; set; }
        [DataMember]
        public List<DisbursementViewModel> Disbursements { get; set; }
    }

    [DataContract]
    public class DisbursementViewModel
    {
        [DataMember]
        public E_DisbursementTypeT DisbT { get; set; }
        [DataMember]
        public E_DisbPaidDateType PaidDT { get; set; }
        [DataMember]
        public string DueD { get; set; }
        [DataMember]
        public string DueAmt { get; set; }
        [DataMember]
        public string DisbMon { get; set; }
        [DataMember]
        public string DisbPaidD { get; set; }
        [DataMember]
        public E_DisbursementPaidByT PaySource { get; set; }
        [DataMember]
        public string PaidFromD { get; set; }
        [DataMember]
        public string PaidToD { get; set; }
        [DataMember]
        public string DisbId { get; set; }
        [DataMember]
        public bool IsPaidFromEscrow { get; set; }
    }

    public partial class PropHousingExpenses : BaseLoanPage
    {
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            IsAppSpecific = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
        }

        #endregion

        private CPageData dataLoan;

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(PropHousingExpenses));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            RegisterJsScript("ractive-0.7.1.min.js");
            RegisterJsScript("Ractive-Common.js");
            RegisterJsScript("mask.js");
            PageID = "PropHousingExpenses";
            UseNewFramework = true;

            DropDownList ddl = new DropDownList();
            Tools.Bind_PaidDateType(ddl);
            PaidDTRepeater.DataSource = ddl.Items;
            PaidDTRepeater.DataBind();

            ddl = new DropDownList();
            Tools.Bind_DisbPaidByType(ddl);
            PaySrcRepeater.DataSource = ddl.Items;
            PaySrcRepeater.DataBind();

            ddl = new DropDownList();
            Tools.Bind_sTaxTableT(ddl);
            taxTypeRepeater.DataSource = ddl.Items;
            taxTypeRepeater.DataBind();
            taxTypeRepeater_amts.DataSource = ddl.Items;
            taxTypeRepeater_amts.DataBind();
            taxTypeRepeater_prep.DataSource = ddl.Items;
            taxTypeRepeater_prep.DataBind();
            taxTypeRepeater_esc.DataSource = ddl.Items;
            taxTypeRepeater_esc.DataBind(); 

            ddl = new DropDownList();
            Tools.Bind_AnnAmtCalcType(ddl);
            annAmtCalcTRepeater.DataSource = ddl.Items;
            annAmtCalcTRepeater.DataBind();

            ddl = new DropDownList();
            Tools.Bind_PercentBaseT(ddl);
            annAmtBaseRepeater.DataSource = ddl.Items;
            annAmtBaseRepeater.DataBind();
            annAmtBaseRepeater_amts.DataSource = ddl.Items;
            annAmtBaseRepeater_amts.DataBind();

            ddl = new DropDownList();
            Tools.Bind_DisbursementReptIntervalT(ddl);
            RepIntervalRepeater.DataSource = ddl.Items;
            RepIntervalRepeater.DataBind();

            /* Custom expense DDL set manually in RActive. 
            ddl = new DropDownList();
            Tools.Bind_CustomExpenseLineNumberT(ddl);
            customLineNumRep.DataSource = ddl.Items;
            customLineNumRep.DataBind();
            */

            ddl = new DropDownList();
            Tools.Bind_AggregateEscrowCalcModeT(ddl, Broker.EnableCustomaryEscrowImpoundsCalculation);
            sAggEscrowCalcModeTRep.DataSource = ddl.Items;
            sAggEscrowCalcModeTRep.DataBind();

            ddl = new DropDownList();
            Tools.Bind_CustomaryCalcMinT(ddl);
            sCustomaryEscrowImpoundsCalcMinTRep.DataSource = ddl.Items;
            sCustomaryEscrowImpoundsCalcMinTRep.DataBind();
        }

        private HousingExpenseViewModel CreateHousingExpenseModel(CPageData dataLoan, BaseHousingExpense exp, string remDisbs, bool flippedOnce)
        {
            HousingExpenseViewModel model = new HousingExpenseViewModel();

            model.IsInsurance = exp.IsInsurance;
            model.IsTax = exp.IsTax;
            model.IsOther = !exp.IsTax && !exp.IsInsurance;
            model.expenseType = exp.HousingExpenseType;
            model.taxT = exp.TaxType;
            model.isPrepaid = exp.IsPrepaid;
            model.IsCustom = exp.CanHaveCustomLineNum;
            model.removedDisbs = remDisbs;
            model.customLineNum = exp.CustomExpenseLineNum;
            model.customLineNumAsString = exp.LineNumAsString;
            model.calcMode = exp.AnnualAmtCalcType;
            model.flippedOnce = flippedOnce;
            model.expName = exp.DefaultExpenseDescription;
            model.expDesc = exp.ExpenseDescription;
            model.expPrefix = exp.ExpensePrefix;
            model.isEscrowed = exp.IsEscrowedAtClosing == E_TriState.Yes ? true : false;
            model.annAmtCalcT = exp.AnnualAmtCalcType;
            model.annAmtTot = exp.AnnualAmt_rep;
            model.monTotPITI = exp.MonthlyAmtTotal_rep;
            model.prepMnth = exp.PrepaidMonths_rep;
            model.prepAmt = exp.PrepaidAmt_rep;
            model.monTotServ = exp.MonthlyAmtServicing_rep;
            model.rsrvMon = exp.ReserveMonths_rep;
            model.rsrvMonLckd = exp.ReserveMonthsLckd;
            model.rsrvAmt = exp.ReserveAmt_rep;
            model.annAmtPerc = exp.AnnualAmtCalcBasePerc_rep;
            model.annAmtBase = exp.AnnualAmtCalcBaseType;
            model.monAmtFixed = exp.MonthlyAmtFixedAmt_rep;
            model.RepInterval = exp.DisbursementRepInterval;
            model.firstDisbD = exp.FirstDisbDate_rep;

            model.DisbSched = exp.DisbursementScheduleMonths.Skip(1).Select((value) => value.ToString()).ToArray();
            model.Cush = exp.DisbursementScheduleMonths[0].ToString();

            IEnumerable<SingleDisbursement> disbList;
            if (exp.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.LoanValues)
            {
                disbList = exp.ProjectedDisbursements;
            }
            else
            {
                disbList = exp.ActualDisbursementsList;
            }

            model.Disbursements = new List<DisbursementViewModel>();
            foreach (SingleDisbursement disb in disbList.OrderBy(d => d.DueDate))
            {
                model.Disbursements.Add(CreateDisbursementModel(disb));
            }
            return model;
        }

        private DisbursementViewModel CreateDisbursementModel(SingleDisbursement disb)
        {
            DisbursementViewModel model = new DisbursementViewModel();

            model.DisbT = disb.DisbursementType;
            model.PaidDT = disb.DisbPaidDateType;
            model.DueD = disb.DueDate_rep;
            model.DueAmt = disb.DisbursementAmt_rep;
            model.DisbMon = disb.CoveredMonths_rep;
            model.DisbPaidD = disb.PaidDate_rep;
            model.PaySource = disb.PaidBy;
            model.PaidFromD = disb.BillingPeriodStartD_rep;
            model.PaidToD = disb.BillingPeriodEndD_rep;
            model.DisbId = disb.DisbId.ToString();
            model.IsPaidFromEscrow = disb.IsPaidFromEscrowAcc;

            return model;
        }

        protected override void LoadData()
        {
            PageViewModel viewmodel = new PageViewModel();

            if (!this.dataLoan.sIsHousingExpenseMigrated)
            {
                RegisterJsObject("PageViewModel", viewmodel);
                return;
            }

            foreach (BaseHousingExpense exp in dataLoan.sHousingExpenses.ExpensesToUse)
            {
                HousingExpenseViewModel model = this.CreateHousingExpenseModel(this.dataLoan, exp, "", false);
                if (model.IsInsurance)
                {
                    viewmodel.PropInsExps.Add(model);
                    model.Category = "PropInsExps";
                }
                else if (model.IsTax)
                {
                    viewmodel.TaxesExps.Add(model);
                    model.Category = "TaxesExps";
                }
                else if (model.IsOther)
                {
                    viewmodel.OtherExps.Add(model);
                    model.Category = "OtherExps";
                }

                if (exp.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                {
                    viewmodel.DisbModeExpenses.Add(exp.DescriptionOrDefault);
                }

                if (exp.MonthlyAmtTotal != 0)
                {
                    if (exp.IsEscrowedAtClosing == E_TriState.Yes)
                    {
                        viewmodel.EscrowedDescriptions.Add(exp.DescriptionOrDefault);
                    }
                    else
                    {
                        viewmodel.NonEscrowedDescriptions.Add(exp.DescriptionOrDefault);
                    }
                }
            }

            if (dataLoan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017 && dataLoan.sProMIns > 0)
            {
                viewmodel.EscrowedDescriptions.Add("Mortgage Insurance");
            }

            // The rest of the fields.
            viewmodel.sEstCloseD = dataLoan.sEstCloseD_rep;
            viewmodel.sEstCloseDLckd = dataLoan.sEstCloseDLckd;
            viewmodel.sDocMagicClosingD = dataLoan.sDocMagicClosingD_rep;
            viewmodel.sSchedDueD1 = dataLoan.sSchedDueD1_rep;
            viewmodel.sSchedDueD1Lckd = dataLoan.sSchedDueD1Lckd;
            viewmodel.sProMIns = dataLoan.sProMIns_rep;
            viewmodel.sMInsRsrvEscrowedTri = dataLoan.sMInsRsrvEscrowedTri == E_TriState.Yes;
            viewmodel.sIssMInsRsrvEscrowedTriReadOnly = dataLoan.sIssMInsRsrvEscrowedTriReadOnly;
            viewmodel.sMInsRsrv = dataLoan.sMInsRsrv_rep;
            viewmodel.sMInsRsrvMon = dataLoan.sMInsRsrvMon_rep;
            viewmodel.sMInsRsrvMonLckd = dataLoan.sMInsRsrvMonLckd;
            viewmodel.MICush = dataLoan.sHousingExpenses.MIEscrowSchedule[0].ToString();
            viewmodel.sIsMipPrepaid = dataLoan.sIsMipPrepaid;
            viewmodel.sMipPiaMon = dataLoan.sMipPiaMon_rep;
            viewmodel.sMipPia = dataLoan.sMipPia_rep;
            viewmodel.sRecurringMipPia = dataLoan.sRecurringMipPia_rep;
            viewmodel.sAggEscrowCalcModeT = dataLoan.sAggEscrowCalcModeT;
            viewmodel.sCustomaryEscrowImpoundsCalcMinT = dataLoan.sCustomaryEscrowImpoundsCalcMinT;
            viewmodel.sAggregateAdjRsrv = dataLoan.sAggregateAdjRsrv_rep;
            viewmodel.sAggregateAdjRsrvLckd = dataLoan.sAggregateAdjRsrvLckd;
            viewmodel.sGfeInitialImpoundDeposit = dataLoan.sGfeInitialImpoundDeposit_rep;
            viewmodel.sClosingCostFeeVersionT = dataLoan.sClosingCostFeeVersionT;
            viewmodel.AnySetToAny = dataLoan.sHousingExpenses.IsAnyExpenseSetToAny() && dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated;
            viewmodel.sMonthlyNonPIExpenseTotalPITI = dataLoan.sMonthlyNonPIExpenseTotalPITI_rep;
            viewmodel.sServicingEscrowPmtNonMI = dataLoan.sServicingEscrowPmtNonMI_rep;
            viewmodel.sServicingEscrowPmt = dataLoan.sServicingEscrowPmt_rep;
            viewmodel.sTotalPropertyInsurancePITI = dataLoan.sTotalPropertyInsurancePITI_rep;
            viewmodel.sTotalRealtyTaxesPITI = dataLoan.sTotalRealtyTaxesPITI_rep;
            viewmodel.sTotalOtherHousingExpensesPITI = dataLoan.sTotalOtherHousingExpensesPITI_rep;
            viewmodel.sTridEscrowAccountExists = dataLoan.sTridEscrowAccountExists;
            viewmodel.sTridEscrowAccountExists_True = dataLoan.sTridEscrowAccountExists;
            viewmodel.sTridEscrowAccountExists_False = !dataLoan.sTridEscrowAccountExists;
            viewmodel.sNonMIHousingExpensesEscrowedReasonT_LenderRequired = dataLoan.sNonMIHousingExpensesEscrowedReasonT == E_sNonMIHousingExpensesEscrowedReasonT.LenderRequired;
            viewmodel.sNonMIHousingExpensesEscrowedReasonT_BorrowerRequested = dataLoan.sNonMIHousingExpensesEscrowedReasonT == E_sNonMIHousingExpensesEscrowedReasonT.BorrowerRequested;
            viewmodel.sClosingDisclosureEscrowedPropertyCostsFirstYear = dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep;
            viewmodel.sClosingDisclosureNonEscrowedPropertyCostsFirstYear = dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep;
            viewmodel.sClosingDisclosureMonthlyEscrowPayment = dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep;
            viewmodel.sNonMIHousingExpensesNotEscrowedReasonT_Declined = dataLoan.sNonMIHousingExpensesNotEscrowedReasonT == E_sNonMIHousingExpensesNotEscrowedReasonT.BorrowerDeclined;
            viewmodel.sNonMIHousingExpensesNotEscrowedReasonT_NotOffered = dataLoan.sNonMIHousingExpensesNotEscrowedReasonT == E_sNonMIHousingExpensesNotEscrowedReasonT.LenderDidNotOffer;
            viewmodel.sClosingDisclosurePropertyCostsFirstYear = dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep;
            viewmodel.sTRIDEscrowWaiverFee = dataLoan.sTRIDEscrowWaiverFee_rep;

            RegisterJsObject("PageViewModel", viewmodel);
        }
    }
}
