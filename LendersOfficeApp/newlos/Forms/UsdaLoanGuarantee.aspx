﻿<%@
    Language="C#"
    AutoEventWireup="false"
    CodeBehind="UsdaLoanGuarantee.aspx.cs"
    Inherits="LendersOfficeApp.newlos.Forms.UsdaLoanGuarantee"
    TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    EnableViewState="false"
%>

<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml" >
<head  runat="server">
<title>USDA RD3555-21</title>
<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0" />
<meta name="CODE_LANGUAGE" Content="C#" />
<meta name=vs_defaultClientScript content="JavaScript" />
<meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5" />
<link href="<%= AspxTools.HtmlString(StyleSheet) %>" type="text/css" rel="stylesheet" />
<style type="text/css">
    #SectionTitle
    {
    	font-weight: bolder;
    	font-size: larger;
    }
    .ApprovedLenderTable
    {
    	border: solid 1px black;
    	border-collapse: collapse;
    	margin-top: 0.5em;
    	margin-bottom: 0.5em;
    	width: 100%;
    }
    .ApprovedLenderTable td
    {
    	border: solid 1px black;
    	padding: 5px;
    	width: 50%;
    }
    .ApprovedLenderTable input
    {
        width: 100%;
    }
    .ApplicantInfoTable
    {
    	border: solid 1px black;
    	border-collapse: collapse;
    	margin-top: 0.5em;
    	margin-bottom: 0.5em;
    	width: 100%;
    }
    .ApplicantInfoTable td
    {
        border: solid 1px black;
        padding: 5px;
    }
    .ApplicantInfoTable th
    {
        border: solid 1px black;
        text-align: left;
        padding: 5px;
    }
    .ApplicantMainInfo
    {
    	width: 100%;
        border: 0px;
        padding: 0px;
        margin: 0px;
    }
    .ApplicantMainInfo td
    {
        border: 0px;
        padding: 0px;
        margin: 0px;
    }
    .ApplicantTriInfo
    {
    	width: 100%;
    	border: 0px;
        padding: 0px;
        margin: 0px;
    }
    .ApplicantTriInfo td
    {
    	border: 0px;
        padding: 0px;
        margin: 0px;
    }
    .LoanFundsPurposeTable
    {

    }
    .LoanFundsPurposeTable th
    {
    	text-align: right;
    	width: 18em;
    }
    th
    {
        font-size: 8pt;
    }
    .ExtraInformationField
    {
    	margin: 5px 0px 5px 0px;
    	border: 1px solid black;
    	padding: 5px;
    }
    .NoBorder
    {
    	border: 0px;
    }
    .NoBorder td
    {
        border: 0px;
    }
    .TextDescription
    {
        width: 45em;
    }
</style>
</head>

<body class="RightBackground">
<form id="UsdaLoanGuarantee" method="post" runat="server">
<table class="InsetBorder" cellpadding="0" cellspacing="0" width="99%" border="0">
    <tr>
        <td class="FormTableHeader NoBorder">
            REQUEST FOR SINGLE FAMILY HOUSING LOAN GUARANTEE
        </td>
    </tr>
    <tr>
        <td>
            <table class="ApprovedLenderTable">
                <tr>
                    <td>
                        <span class="FieldLabel">
                            Approved Lender:
                        </span>
                        <asp:TextBox runat="server" ID="UsdaGuaranteeApprovedLenderCompanyName"></asp:TextBox>
                    </td>
                    <td>
                        <span class="FieldLabel">
                            Approved Lender Tax ID No.:
                        </span>
                        <asp:TextBox runat="server" ID="UsdaGuaranteeApprovedLenderTaxId"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="FieldLabel">
                            Contact/Authorized Representative:
                        </span>
                        <asp:TextBox runat="server" ID="UsdaGuaranteeApprovedLenderPreparerName"></asp:TextBox>
                    </td>
                    <td>
                        <span class="FieldLabel">
                            Approved Lender E-mail:
                        </span>
                        <asp:TextBox runat="server" ID="UsdaGuaranteeApprovedLenderEmailAddr"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="FieldLabel">
                            Phone Number:
                        </span>
                        <ml:PhoneTextBox preset="phone" runat="server" ID="UsdaGuaranteeApprovedLenderPhoneOfCompany"></ml:PhoneTextBox>
                    </td>
                    <td>
                        <span class="FieldLabel">
                            Fax Number:
                        </span>
                        <ml:PhoneTextBox preset="phone" runat="server" ID="UsdaGuaranteeApprovedLenderFaxOfCompany"></ml:PhoneTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="FieldLabel">
                            Third Party Originator (TPO):
                        </span>
                        <asp:TextBox runat="server" ID="UsdaGuaranteeThirdPartyOriginatorCompanyName"></asp:TextBox>
                    </td>
                    <td>
                        <span class="FieldLabel">
                            TPO Tax ID No:
                        </span>
                        <asp:TextBox runat="server" ID="UsdaGuaranteeThirdPartyOriginatorTaxId"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="FieldLabel">
                            USDA Application Number:
                        </span>
                        <asp:TextBox runat="server" ID="sUsdaGuaranteeNum"></asp:TextBox>
                    </td>
                    <td>
                        <span class="FieldLabel">
                            Lender Loan Reference Number:
                        </span>
                        <asp:TextBox runat="server" ID="sLNm"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="FieldLabel">
                            Title/Company
                        </span>
                        <asp:TextBox runat="server" ID="UsdaGuaranteeApprovedLenderTitle"></asp:TextBox>
                    </td>
                </tr>
            </table>

            <table class="ApplicantInfoTable">
                <tr>
                    <th class="FormTableSubHeader">
                        Applicant Information
                    </th>
                    <th class="FormTableSubHeader">
                        Co-Applicant Information
                    </th>
                </tr>
                <tr>
                    <td>
                        <table class="ApplicantMainInfo">
                            <tr>
                                <td class="FieldLabel" style="width:50%">
                                    First Name:
                                </td>
                                <td>
                                    <asp:textbox id="aBFirstNm" runat="server" maxlength="21" width="127px"></asp:textbox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Middle Name:
                                </td>
                                <td>
                                    <asp:textbox id="aBMidNm" maxlength="21" runat="server" Width="70px"></asp:textbox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Last Name:
                                </td>
                                <td>
                                    <asp:textbox id="aBLastNm" runat="server" maxlength="21" width="127px"></asp:textbox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Suffix:
                                </td>
                                <td>
                                    <ml:ComboBox id="aBSuffix" runat="server" Width="49px"></ml:ComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="FieldLabel">SSN:</span>
                                </td>
                                <td>
                                    <ml:ssntextbox id="aBSsn" runat="server" width="90"></ml:ssntextbox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="FieldLabel">Date of Birth:</span>
                                </td>
                                <td>
                                    <ml:DateTextBox ID="aBDob" runat="server"></ml:DateTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="FieldLabel">U.S. Citizen: (Y/N)</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="aBDecCitizen" runat="server" Width="2em"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="FieldLabel">Permanent Resident/Qualified Alien: (Y/N)</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="aBDecResidency" runat="server" Width="2em"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class="ApplicantMainInfo">
                            <tr>
                                <td class="FieldLabel" style="width: 50%">
                                    First Name:
                                </td>
                                <td>
                                    <asp:textbox id="aCFirstNm" runat="server" maxlength="21" width="127px"></asp:textbox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Middle Name:
                                </td>

                                <td>
                                    <asp:textbox id="aCMidNm" maxlength="21" runat="server" Width="70px"></asp:textbox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Last Name:
                                </td>
                                <td>
                                    <asp:textbox id="aCLastNm" runat="server" maxlength="21" width="127px"></asp:textbox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Suffix:
                                </td>
                                <td>
                                    <ml:ComboBox id="aCSuffix" runat="server" Width="49px"></ml:ComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    SSN:
                                </td>
                                <td>
                                    <ml:ssntextbox id="aCSsn" runat="server" width="90"></ml:ssntextbox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Date of Birth:
                                </td>
                                <td>
                                    <ml:DateTextBox ID="aCDob" runat="server"></ml:DateTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    U.S. Citizen: (Y/N)
                                </td>
                                <td>
                                    <asp:TextBox ID="aCDecCitizen" runat="server" Width="2em"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Permanent Resident/Qualified Alien: (Y/N)
                                </td>
                                <td>
                                    <asp:TextBox ID="aCDecResidency" runat="server" Width="2em"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="ApplicantTriInfo">
                            <tr>
                                <td style="width:50%">
                                    <span class="FieldLabel">Veteran:</span>
                                </td>
                                <td>
                                    <asp:CheckBoxList id="aBUsdaGuaranteeVeteranTri" runat="server" RepeatDirection="Horizontal"></asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="FieldLabel">Disabled:</span>
                                </td>
                                <td>
                                    <asp:CheckBoxList id="aBUsdaGuaranteeDisabledTri" runat="server" RepeatDirection="Horizontal"></asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="FieldLabel">Gender:</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="aBGenderT" runat="server"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="FieldLabel">First Time Homebuyer:</span>
                                </td>
                                <td>
                                    <asp:CheckBoxList id="aBUsdaGuaranteeFirstTimeHomeBuyerTri" runat="server" RepeatDirection="Horizontal"></asp:CheckBoxList>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class="ApplicantTriInfo">
                            <tr>
                                <td style="width:50%">
                                    <span class="FieldLabel">Veteran:</span>
                                </td>
                                <td>
                                    <asp:CheckBoxList id="aCUsdaGuaranteeVeteranTri" runat="server" RepeatDirection="Horizontal"></asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="FieldLabel">Disabled:</span>
                                </td>
                                <td>
                                    <asp:CheckBoxList id="aCUsdaGuaranteeDisabledTri" runat="server" RepeatDirection="Horizontal"></asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="FieldLabel">Gender:</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="aCGenderT" runat="server"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="FieldLabel">First Time Homebuyer:</span>
                                </td>
                                <td>
                                    <asp:CheckBoxList id="aCUsdaGuaranteeFirstTimeHomeBuyerTri" runat="server" RepeatDirection="Horizontal"></asp:CheckBoxList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <span class="FieldLabel" style="width:50%">Ethnicity: </span>
                            <asp:DropDownList ID="aBHispanicT" runat="server"></asp:DropDownList>
                        </div>
                    </td>
                    <td>
                        <div>
                            <span class="FieldLabel" style="width:50%">Ethnicity: </span>
                            <asp:DropDownList ID="aCHispanicT" runat="server"></asp:DropDownList>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <span class="FieldLabel">Race: (check all that apply)</span><br />
                            <asp:CheckBox ID="aBIsAmericanIndian" runat="server" Text="American Indian or Alaska Native"></asp:CheckBox><br />
                            <asp:CheckBox ID="aBIsAsian" runat="server" Text="Asian"></asp:CheckBox><br />
                            <asp:CheckBox ID="aBIsBlack" runat="server" Text="Black or African American"></asp:CheckBox><br />
                            <asp:CheckBox ID="aBIsPacificIslander" runat="server" Text="Native Hawaiian or Other Pacific Islander"></asp:CheckBox><br />
                            <asp:CheckBox ID="aBIsWhite" runat="server" Text="White"></asp:CheckBox><br />
                        </div>
                    </td>
                    <td>
                        <div>
                            <span class="FieldLabel">Race: (check all that apply)</span><br />
                            <asp:CheckBox ID="aCIsAmericanIndian" runat="server" Text="American Indian or Alaska Native"></asp:CheckBox><br />
                            <asp:CheckBox ID="aCIsAsian" runat="server" Text="Asian"></asp:CheckBox><br />
                            <asp:CheckBox ID="aCIsBlack" runat="server" Text="Black or African American"></asp:CheckBox><br />
                            <asp:CheckBox ID="aCIsPacificIslander" runat="server" Text="Native Hawaiian or Other Pacific Islander"></asp:CheckBox><br />
                            <asp:CheckBox ID="aCIsWhite" runat="server" Text="White"></asp:CheckBox><br />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <span class="FieldLabel" style="width:50%">Marital Status:</span>
                            <asp:DropDownList ID="aBMaritalStatT" runat="server"></asp:DropDownList>
                        </div>
                    </td>
                    <td>
                        <div>
                            <span class="FieldLabel" style="width:50%">Marital Status:</span>
                            <asp:DropDownList ID="aCMaritalStatT" runat="server"></asp:DropDownList>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <span class="FieldLabel">
                                The applicant has a relationship with any<br />
                                current Rural Development employee.
                            </span>
                            <asp:CheckBoxList CssClass="NoBorder" ID="aBUsdaGuaranteeEmployeeRelationshipT" runat="server" RepeatDirection="Horizontal"></asp:CheckBoxList>
                            <span class="FieldLabel">
                                Explain.
                            </span>
                            <br />
                            <asp:TextBox ID="aBUsdaGuaranteeEmployeeRelationshipDesc" Width="100%" runat="server" ></asp:TextBox>
                        </div>
                    </td>
                    <td>
                        <div>
                            <span class="FieldLabel">
                                The co-applicant has a relationship with any<br />
                                current Rural Development employee.
                            </span>
                            <asp:CheckBoxList CssClass="NoBorder" ID="aCUsdaGuaranteeEmployeeRelationshipT" runat="server" RepeatDirection="Horizontal"></asp:CheckBoxList>
                            <span class="FieldLabel">
                                Explain.
                            </span>
                            <br />
                            <asp:TextBox ID="aCUsdaGuaranteeEmployeeRelationshipDesc" Width="100%" runat="server"></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <span class="FieldLabel">
                                Applicant's Credit Score:
                            </span>
                            <asp:TextBox ID="aBDecisionCreditScore" runat="server" ReadOnly="true"></asp:TextBox>
                            <asp:CheckBox ID="aBHasHighestScore" runat="server" Disabled="true" ReadOnly="true" /> No Score
                        </div>
                    </td>
                    <td>
                        <div>
                            <span class="FieldLabel">
                                Co-Applicant's Credit Score:
                            </span>
                            <asp:TextBox ID="aCDecisionCreditScore" runat="server" ReadOnly="true"></asp:TextBox>
                            <asp:CheckBox ID="aCHasHighestScore" runat="server" Disabled="true" ReadOnly="true" /> No Score
                        </div>
                    </td>
                </tr>
            </table>

            <div class="ExtraInformation">
                <div class="ExtraInformationField">
                    <table>
                        <tr>
                            <td class="FieldLabel">
                                Property Address:
                            </td>
                            <td>
                                <asp:textbox id=sSpAddr width="359px" maxlength="60" runat="server"></asp:textbox>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">
                                City/State/Zip:
                            </td>
                            <td>
                                <asp:textbox id="sSpCity" maxlength="72" runat="server" Width="258px"></asp:textbox>
                                <ml:statedropdownlist id="sSpState" runat="server" IncludeTerritories="false"></ml:statedropdownlist>
                                <ml:zipcodetextbox id="sSpZip" width="50" runat="server" preset="zipcode"></ml:zipcodetextbox>
                            </td>
                        </tr>

                        <tr>
                            <td class=FieldLabel>County:</td>
                            <td class=FieldLabel>
                                <asp:DropDownList id="sSpCounty" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="ExtraInformationField">
                    <asp:CheckBox runat="server" ID="sUsdaGuaranteeIsRefinanceLoan" Disabled="true"></asp:CheckBox> Is a refinance loan
                    <br />
                    <br />

                    <span class="FieldLabel">
                        If Yes, refinanced loan is an RD Single Family Housing
                    </span>
                    <asp:CheckBoxList id="sUsdaGuaranteeRefinancedLoanT" runat="server" RepeatDirection="Horizontal"/>
                </div>
                <div class="ExtraInformationField">
                    <span class="FieldLabel">
                        1. Number of persons in the household:
                    </span>
                    <asp:TextBox runat="server" ID="sUsdaGuaranteeNumOfPersonInHousehold"></asp:TextBox>
                    <br />
                    <span class="FieldLabel">
                        Number of dependents under Age 18 or Full-time Students:
                    </span>
                    <asp:TextBox runat="server" ID="sUsdaGuaranteeNumOfDependents"></asp:TextBox>
                </div>
                <div class="ExtraInformationField">
                    <span class="FieldLabel">
                        2a. The current annual income for the household is:
                        <ml:MoneyTextBox ID="sLTotAnnualI" runat="server" ReadOnly="true"></ml:MoneyTextBox>
                    </span>
                    <br />
                    <span class="FieldLabel">
                        2b. The current adjusted income for the household is:
                        <ml:MoneyTextBox ID="sUsdaGuaranteeAdjustedAnnualIncome" runat="server"></ml:MoneyTextBox>
                    </span>
                </div>
                <div class="ExtraInformationField">
                    <span class="FieldLabel">
                        3. PITI ratio:
                        <ml:PercentTextBox runat="server" ID="sQualTopR" ReadOnly="true"></ml:PercentTextBox>
                    </span>
                    <span class="FieldLabel">
                        TOTAL DEBT ratio:
                        <ml:PercentTextBox runat="server" ID="sQualBottomR" ReadOnly="true"></ml:PercentTextBox>
                    </span>
                </div>
                <div class="ExtraInformationField">
                    <span class="FieldLabel">
                    4. We propose to loan
                    <ml:MoneyTextBox ID="sLAmtCalc" runat="server" ReadOnly="true"></ml:MoneyTextBox>
                    for 30 years at
                    <ml:PercentTextBox readonly=true ID="sNoteIR" runat="server" onchange="refreshCalculation()"></ml:PercentTextBox>
                    per annum with payments (P&amp;I) of
                    <ml:MoneyTextBox ID="sMonthlyPmt" runat="server" ReadOnly="true"></ml:MoneyTextBox>
                    per month.
                    </span>
                </div>
                <div class="ExtraInformationField">
                    <span class="FieldLabel">
                        5. The interest rate is based on the
                        <asp:CheckBox id="sUsdaGuaranteeIsNoteRateBasedOnFnma" runat="server" Text="Fannie Mae"/>
                        on <ml:DateTextBox runat="server" ID="sUsdaGuaranteeNoteRateBasedOnFnmaD"></ml:DateTextBox>
                        (required).
                    </span> <br />
                    <span class="FieldLabel">
                        <asp:CheckBox Cssclass="FieldLabel" id="sUsdaGuaranteeIsNoteRateLocked" runat="server" Text="The interest rate is locked in"/>
                        until
                        <ml:DateTextBox runat="server" ID="sUsdaGuaranteeNoteRateLockedD"></ml:DateTextBox>
                    </span> <br />
                    <span class="FieldLabel">
                        <asp:CheckBox Cssclass="FieldLabel" id="sUsdaGuaranteeIsNoteRateFloat" runat="server" Text="The interest rate will float until loan closing (documentation of lock
                            date will be required w/ loan closing report)." />
                    </span>
                </div>
                <div class="ExtraInformationField">
                    6. The applicant understands that Rural Development approval of guarantee is
                    required and is subject to the availability of funds.
                </div>
                <div class="ExtraInformationField">
                    7. The applicant is unable to secure the necessary conventional credit without a
                    Rural Development guarantee upon reasonable rates, terms and conditions in
                    which the applicant could reasonably be expected to fulfill.
                </div>
                <div class="ExtraInformationField">
                    8. Loan funds will be used for the following purpose(s): <br />
                    <table class="LoanFundsPurposeTable">
                        <tr>
                            <th>
                                Purchase / Refinance Amt:
                            </th>
                            <td>
                                <asp:TextBox CssClass="TextDescription" runat="server" ID="sUsdaGuaranteePurchaseAmtDesc"></asp:TextBox>
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sUsdaGuaranteePurchaseAmt" runat="server" onchange="refreshCalculation()"></ml:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Financed Loan Closing Costs:
                            </th>
                            <td>
                                <asp:TextBox CssClass="TextDescription" runat="server" ID="sUsdaGuaranteeClosingCostsDesc"></asp:TextBox>
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sUsdaGuaranteeClosingCosts" runat="server" onchange="refreshCalculation()"></ml:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Repairs / Other:
                            </th>
                            <td>
                                <asp:TextBox CssClass="TextDescription" runat="server" ID="sUsdaGuaranteeRepairFeeDesc"></asp:TextBox>
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sUsdaGuaranteeRepairFee" runat="server" onchange="refreshCalculation()"></ml:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Guarantee Fee:
                            </th>
                            <td>
                                <asp:TextBox CssClass="TextDescription" runat="server" ID="sUsdaGuaranteeGuaranteeFeeDesc"></asp:TextBox>
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sUsdaGuaranteeGuaranteeFee" runat="server" onchange="refreshCalculation()"></ml:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Total Request:
                            </th>
                            <td>
                                <!-- Placeholder: empty space -->
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sUsdaGuaranteeTotalRequestFee" runat="server" ReadOnly="true"></ml:MoneyTextBox>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
        </td>
    </tr>
</table>

<uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
</form>

</body>
</html>
