using System;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Constants;
using System.Linq;
using LendersOffice.Admin;
using LendersOfficeApp.los.admin;

namespace LendersOfficeApp.newlos.Forms
{

	public partial class GoodFaithEstimate2010Service : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override bool PerformResultOptimization
        {
            get
            {
                return true;
            }
        }
        protected BrokerDB Broker
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BrokerDB; }
        }
        protected bool IsArchivePage
        {
            get { return Broker.IsGFEandCoCVersioningEnabled && (GetString("hfIsArchivePage") == "true"); }
        }
        protected bool ShowRequireFeesFromDropDownCheckbox
        {
            get
            {
                return Broker.FeeTypeRequirementT != E_FeeTypeRequirementT.Never
                    && BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowEnablingCustomFeeDescriptions);
            }
        }
        private bool m_isSaving = false;
		protected override void Process(string methodName) 
		{
            if (IsArchivePage)
            {
                Tools.LogError("Programming Error: Should not call services on archive page.");
                return;  
            }

            m_isSaving = false;

            switch (methodName) 
			{
				case "SaveData":
                    m_isSaving = true;
					SaveData();
					break;
				case "CalculateData":
					CalculateData();
					break;
                case "AgentPicked":
                    AgentPicked();
                    break;
                case "ClearAgent":
                    ClearAgent();
                    break;
			}
		}

		private void SaveData() 
		{
            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
            bool byPassBgCalcForGfeAsDefault = GetString("ByPassBgCalcForGfeAsDefault", "0") == "1";

            CPageData dataLoan = CreatePageData();
			dataLoan.InitSave(sFileVersion);

            bool sGfeInitialDisclosureDWasEntered = !dataLoan.sGfeInitialDisclosureD.IsValid 
                    && dataLoan.sGfeInitialDisclosureD_rep != GetString("sGfeInitialDisclosureD")
                    && false == dataLoan.BrokerDB.IsProtectDisclosureDates;
            BindData(dataLoan, true);
            if (!IsArchivePage && dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                if (sGfeInitialDisclosureDWasEntered &&
                    ((dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy && dataLoan.GFEArchives.Count() == 1) ||
                     (dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && dataLoan.sClosingCostArchive.Count == 1)))
                {
                //    dataLoan.ArchiveGFE(); // make sure this happens *AFTER* data binding.
                    SetResult("hfDoWindowRefresh", "true"); // refresh the whole page, should update the ddl.
                }
            }

            if (!IsArchivePage && dataLoan.sIsShowsIsPreserveGFEFees && dataLoan.sClosingCostAutomationUpdateT == E_sClosingCostAutomationUpdateT.PreserveFeesOnLoan)
            {
                // OPM 141471.
                dataLoan.RecalculateClosingFeeConditions(true); //Force the calc regardless if revision changed.
                dataLoan.sClosingCostAutomationUpdateT = E_sClosingCostAutomationUpdateT.UpdateOnConditionChange;
                SetResult("sIsShowsIsPreserveGFEFees", false);
            }

            bool oldBypassClosingCostSetDFLPValidation = dataLoan.BypassClosingCostSetDFLPValidation;
            dataLoan.BypassClosingCostSetDFLPValidation = true;
            dataLoan.Save();
            dataLoan.BypassClosingCostSetDFLPValidation = oldBypassClosingCostSetDFLPValidation;

            SetResult("sFileVersion", dataLoan.sFileVersion);
            LoadData(dataLoan);

			BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            SqlParameter[] parameters = {
                                            new SqlParameter("@ByPassBgCalcForGfeAsDefault", byPassBgCalcForGfeAsDefault)
                                            , new SqlParameter("@UserId", principal.UserId)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "SetPassBgCalcForGfeAsDefaultByUserId", 3, parameters); 
        }
        private DateTime GetDateTime(string id)
        {
            DateTime dt = DateTime.MinValue;
            
            string date = GetString(id);
            CDateTime cdateTime = CDateTime.Create(date, null);
            if (cdateTime.IsValid)
            {
                string time = GetString(id + "_Time") + ":" + GetString(id + "_Time_minute") + " " + GetString(id + "_Time_am");
                DateTime.TryParse(cdateTime.ToStringWithDefaultFormatting() + " " + time, out dt);
            }
            else
            {
                // 6/17/2010 dd - Allow user to enter time without date enter.
                string time = GetString(id + "_Time") + ":" + GetString(id + "_Time_minute") + " " + GetString(id + "_Time_am");

                DateTime.TryParse("01/01/1900 " + time, out dt);
            }
            return dt;
        }

        private DateTime? GetDate(string id)
        {
            string date = GetString(id);
            CDateTime cdateTime = CDateTime.Create(date, null);
            if (cdateTime.IsValid)
            {
                return cdateTime.DateTimeForComputation;
            }
            
            return null;
        }
		private bool BindData(CPageData dataLoan, bool isBindAll) 
		{
			bool alertUser = false;
            dataLoan.sEstCloseDLckd = GetBool("sEstCloseDLckd");
			dataLoan.sDue_rep = GetString("sDue") ;
			dataLoan.sTerm_rep = GetString("sTerm") ;
            dataLoan.sSchedDueD1Lckd = GetBool("sSchedDueD1Lckd");
			dataLoan.sNoteIR_rep = GetString("sNoteIR") ;

            DateTime dt = DateTime.MinValue;

            string receivedEstCloseD = GetString("sEstCloseD");
            if (DateTime.TryParse(receivedEstCloseD, out dt))
            {
                dataLoan.sEstCloseD_rep = receivedEstCloseD;
            }
            else if (m_isSaving)
            {
                // We'll throw this exception only if the date fails to be parsed 
                // and the user is attempting to save the loan. We do not need
                // to throw this exception when the page is just calling
                // CalculateData, as the ErrorMsg function in mask.js will
                // have already alerted the user.
                throw new FieldInvalidValueException("sEstCloseD", receivedEstCloseD);
            }

            string receivedSchedDueD1 = GetString("sSchedDueD1");
            if (DateTime.TryParse(receivedSchedDueD1, out dt))
            {
                dataLoan.sSchedDueD1_rep = receivedSchedDueD1;
            }
            else if (m_isSaving)
            {
                // As with sEstCloseD, we'll throw this exception only if the date fails 
                // to be parsed and the user is attempting to save the loan.
                throw new FieldInvalidValueException("sSchedDueD1", receivedSchedDueD1);
            }
            
			if (isBindAll) 
			{
				IPreparerFields gfeTil = dataLoan.GetPreparerOfForm( E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew );
                if (false == Broker.IsRemovePreparedDates)
                {
                    gfeTil.PrepareDate_rep = GetString("GfeTilPrepareDate");
                }
				gfeTil.CompanyName = GetString("GfeTilCompanyName");
				gfeTil.StreetAddr = GetString("GfeTilStreetAddr");
				gfeTil.City = GetString("GfeTilCity");
				gfeTil.State = GetString("GfeTilState");
				gfeTil.Zip = GetString("GfeTilZip");
				gfeTil.PhoneOfCompany = GetString("GfeTilPhoneOfCompany");
                gfeTil.EmailAddr = GetString("GfeTilEmailAddr");
                gfeTil.AgentRoleT = (E_AgentRoleT)GetInt("CFM_AgentRoleT");
                gfeTil.IsLocked = GetBool("CFM_IsLocked");
				gfeTil.Update();
			}

            dataLoan.sFinMethT = (E_sFinMethT)GetInt("sFinMethT");
            dataLoan.sDaysInYr_rep = GetString("sDaysInYr");
            dataLoan.sRAdj1stCapR_rep = GetString("sRAdj1stCapR");
            dataLoan.sRAdjCapR_rep = GetString("sRAdjCapR");
            dataLoan.sRAdjLifeCapR_rep = GetString("sRAdjLifeCapR");
            dataLoan.sRAdjCapMon_rep = GetString("sRAdjCapMon");
            dataLoan.sRAdj1stCapMon_rep = GetString("sRAdj1stCapMon");
            dataLoan.sPmtAdjCapR_rep = GetString("sPmtAdjCapR");
            dataLoan.sPmtAdjMaxBalPc_rep = GetString("sPmtAdjMaxBalPc");
            dataLoan.sPmtAdjRecastStop_rep = GetString("sPmtAdjRecastStop");
            dataLoan.sPmtAdjRecastPeriodMon_rep = GetString("sPmtAdjRecastPeriodMon");
            dataLoan.sPmtAdjCapMon_rep = GetString("sPmtAdjCapMon");
            dataLoan.sIOnlyMon_rep = GetString("sIOnlyMon");
            dataLoan.sIPiaDyLckd = GetBool("sIPiaDyLckd");
            dataLoan.sConsummationD_rep = GetString("sConsummationD");
            dataLoan.sConsummationDLckd = GetBool("sConsummationDLckd");
            dataLoan.sGfeNoteIRAvailTillDLckd = GetBool("sGfeNoteIRAvailTillDLckd");
            dataLoan.sGfeNoteIRAvailTillD = GetDateTime("sGfeNoteIRAvailTillD");
            dataLoan.sGfeEstScAvailTillDLckd = GetBool("sGfeEstScAvailTillDLckd");
            dataLoan.sGfeEstScAvailTillD = GetDateTime("sGfeEstScAvailTillD");
            dataLoan.sGfeRateLockPeriod_rep = GetString("sGfeRateLockPeriod");
            dataLoan.sGfeRateLockPeriodLckd = GetBool("sGfeRateLockPeriodLckd");
            if (false == dataLoan.sIsRateLocked)
            {
                dataLoan.sGfeLockPeriodBeforeSettlement_rep = GetString("sGfeLockPeriodBeforeSettlement");
            }
            dataLoan.sGfeHavePpmtPenalty = GetString("sGfeHavePpmtPenalty") == "1";
            dataLoan.sGfeMaxPpmtPenaltyAmt_rep = GetString("sGfeMaxPpmtPenaltyAmt");
            dataLoan.sMldsHasImpound = GetString("sMldsHasImpound") == "1";
            dataLoan.sClosingCostAutomationUpdateT = (E_sClosingCostAutomationUpdateT)GetInt("sIsPreserveGFEFees");
            dataLoan.sGfeUsePaidToFromOfficialContact = GetBool("sGfeUsePaidToFromOfficialContact");
            dataLoan.sGfeRequirePaidToFromContacts = GetBool("sGfeRequirePaidToFromContacts");
            dataLoan.sGfeNoteIRAvailTillDTimeZoneT = (E_sTimeZoneT)GetInt("sGfeNoteIRAvailTillDTimeZoneT");
            dataLoan.sGfeEstScAvailTillDTimeZoneT = (E_sTimeZoneT)GetInt("sGfeEstScAvailTillDTimeZoneT");
            dataLoan.sIsPrintTimeForGfeNoteIRAvailTillD = GetBool("sIsPrintTimeForGfeNoteIRAvailTillD");
            dataLoan.sIsPrintTimeForsGfeEstScAvailTillD = GetBool("sIsPrintTimeForsGfeEstScAvailTillD");
			dataLoan.s800U5F_rep = GetString("s800U5F") ;

            if (ShowRequireFeesFromDropDownCheckbox)
            {
                dataLoan.sIsRequireFeesFromDropDown = GetBool("sIsRequireFeesFromDropDown");
            }

            if (isBindAll)
            {
                dataLoan.s800U5FDesc = GetString("s800U5FDesc") ;
            }
            dataLoan.s800U4F_rep = GetString("s800U4F");
			if (isBindAll) 
			{
				dataLoan.s800U4FDesc = GetString("s800U4FDesc") ;
			}
			dataLoan.s800U3F_rep = GetString("s800U3F") ;
			if (isBindAll) 
			{
				dataLoan.s800U3FDesc = GetString("s800U3FDesc") ;
			}
			dataLoan.s800U2F_rep = GetString("s800U2F") ;
			if (isBindAll) 
			{
				dataLoan.s800U2FDesc = GetString("s800U2FDesc") ;
			}
			dataLoan.s800U1F_rep = GetString("s800U1F") ;
			if (isBindAll) 
			{
				dataLoan.s800U1FDesc = GetString("s800U1FDesc") ;
			}
			dataLoan.sWireF_rep = GetString("sWireF") ;
			dataLoan.sUwF_rep = GetString("sUwF") ;
			dataLoan.sProcF_rep = GetString("sProcF") ;
			dataLoan.sProcFPaid = GetBool("sProcFProps_ctrl_Paid_chk") ;
			dataLoan.sTxServF_rep = GetString("sTxServF") ;
            dataLoan.sFloodCertificationDeterminationT = (E_FloodCertificationDeterminationT)GetInt("sFloodCertificationDeterminationT");
			dataLoan.sMBrokFMb_rep = GetString("sMBrokFMb") ;
			dataLoan.sMBrokFPc_rep = GetString("sMBrokFPc") ;
            dataLoan.sMBrokFBaseT = (E_PercentBaseT)GetInt("sMBrokFBaseT");
			dataLoan.sInspectF_rep = GetString("sInspectF") ;
			dataLoan.sCrF_rep = GetString("sCrF") ;
            dataLoan.sCrFPaid = GetBool("sCrFProps_ctrl_Paid_chk");
			dataLoan.sApprF_rep = GetString("sApprF") ;
            dataLoan.sApprFPaid = GetBool("sApprFProps_ctrl_Paid_chk");
            dataLoan.sFloodCertificationF_rep = GetString("sFloodCertificationF");
			dataLoan.sLDiscntFMb_rep = GetString("sLDiscntFMb") ;

            dataLoan.sLDiscntPc_rep = GetString("sLDiscntPc");
            dataLoan.sLDiscntBaseT = (E_PercentBaseT)GetInt("sLDiscntBaseT");
            // 9/13/2013 gf - sLastDisclosedGFEArchiveD isn't available for leads.
            if (dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled && ContainsField("sLastDisclosedGFEArchiveD"))
            {
                dataLoan.sLastDisclosedGFEArchiveD_rep = GetString("sLastDisclosedGFEArchiveD");
            }
            dataLoan.sLOrigFMb_rep = GetString("sLOrigFMb");
			dataLoan.sLOrigFPc_rep = GetString("sLOrigFPc") ;
			dataLoan.s900U1Pia_rep = GetString("s900U1Pia") ;
			if (isBindAll) 
			{
				dataLoan.s900U1PiaDesc = GetString("s900U1PiaDesc") ;
				dataLoan.s904PiaDesc = GetString("s904PiaDesc") ;

			}
			dataLoan.s904Pia_rep = GetString("s904Pia") ;
			dataLoan.sHazInsPiaMon_rep = GetString("sHazInsPiaMon") ;
            dataLoan.sProHazInsT = (E_PercentBaseT)GetInt("sProHazInsT");
			dataLoan.sProHazInsR_rep = GetString("sProHazInsR") ;
			dataLoan.sProHazInsMb_rep = GetString("sProHazInsMb") ;
            dataLoan.sIPiaDy_rep = GetString("sIPiaDy") ;
			dataLoan.sIPerDayLckd = GetBool("sIPerDayLckd");
			dataLoan.sIPerDay_rep = GetString("sIPerDay");
			dataLoan.sAggregateAdjRsrv_rep = GetString("sAggregateAdjRsrv") ;
			dataLoan.sAggregateAdjRsrvLckd = GetBool("sAggregateAdjRsrvLckd");
			dataLoan.s1007ProHExp_rep = GetString("s1007ProHExp") ;
            if (dataLoan.s1007RsrvMonLckd) { dataLoan.s1007RsrvMon_rep = GetString("s1007RsrvMon"); }
			if (isBindAll) 
			{
				dataLoan.s1007ProHExpDesc = GetString("s1007ProHExpDesc") ;
			}
			dataLoan.s1006ProHExp_rep = GetString("s1006ProHExp") ;
            if (dataLoan.s1006RsrvMonLckd) { dataLoan.s1006RsrvMon_rep = GetString("s1006RsrvMon"); }
			if (isBindAll) 
			{
				dataLoan.s1006ProHExpDesc = GetString("s1006ProHExpDesc") ;
			}
            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                dataLoan.sProU3Rsrv_rep = GetString("sProU3Rsrv");
                if (dataLoan.sU3RsrvMonLckd) { dataLoan.sU3RsrvMon_rep = GetString("sU3RsrvMon"); }
                if (isBindAll)
                {
                    dataLoan.sU3RsrvDesc = GetString("sU3RsrvDesc");
                }
                dataLoan.sProU4Rsrv_rep = GetString("sProU4Rsrv");
                if (dataLoan.sU4RsrvMonLckd) { dataLoan.sU4RsrvMon_rep = GetString("sU4RsrvMon"); }
                if (isBindAll)
                {
                    dataLoan.sU4RsrvDesc = GetString("sU4RsrvDesc");
                }
            }
			dataLoan.sProFloodIns_rep = GetString("sProFloodIns") ;
            if (dataLoan.sFloodInsRsrvMonLckd) { dataLoan.sFloodInsRsrvMon_rep = GetString("sFloodInsRsrvMon"); }
			dataLoan.sProRealETxMb_rep = GetString("sProRealETxMb") ;
            dataLoan.sProRealETxT = (E_PercentBaseT)GetInt("sProRealETxT");
			dataLoan.sProRealETxR_rep = GetString("sProRealETxR");
            if (dataLoan.sRealETxRsrvMonLckd) { dataLoan.sRealETxRsrvMon_rep = GetString("sRealETxRsrvMon"); }
			dataLoan.sProSchoolTx_rep = GetString("sProSchoolTx") ;
            if (dataLoan.sSchoolTxRsrvMonLckd) { dataLoan.sSchoolTxRsrvMon_rep = GetString("sSchoolTxRsrvMon"); }
            if (dataLoan.sMInsRsrvMonLckd) { dataLoan.sMInsRsrvMon_rep = GetString("sMInsRsrvMon"); }
            if (dataLoan.sHazInsRsrvMonLckd) { dataLoan.sHazInsRsrvMon_rep = GetString("sHazInsRsrvMon"); }
			dataLoan.sU4Tc_rep = GetString("sU4Tc") ;
			if (isBindAll) 
			{
                dataLoan.sU4TcDesc = GetString("sU4TcDesc");
			}
			dataLoan.sU3Tc_rep = GetString("sU3Tc") ;
			if (isBindAll) 
			{
				dataLoan.sU3TcDesc = GetString("sU3TcDesc") ;
			}
			dataLoan.sU2Tc_rep = GetString("sU2Tc") ;
			if (isBindAll) 
			{
				dataLoan.sU2TcDesc = GetString("sU2TcDesc") ;
			}
			dataLoan.sU1Tc_rep = GetString("sU1Tc") ;
			if (isBindAll) 
			{
				dataLoan.sU1TcDesc = GetString("sU1TcDesc") ;
			}
            dataLoan.sOwnerTitleInsF_rep = GetString("sOwnerTitleInsF");
            dataLoan.sTitleInsF_rep = GetString("sTitleInsF") ;
			dataLoan.sTitleInsFTable = GetString("sTitleInsFProps_ctrl_PaidTo_tb") ;
			dataLoan.sAttorneyF_rep = GetString("sAttorneyF") ;
			dataLoan.sNotaryF_rep = GetString("sNotaryF") ;
			dataLoan.sDocPrepF_rep = GetString("sDocPrepF") ;
			dataLoan.sEscrowF_rep = GetString("sEscrowF") ;
            dataLoan.sEscrowFTable = GetString("sEscrowFProps_ctrl_PaidTo_tb");
			dataLoan.sU3GovRtcMb_rep = GetString("sU3GovRtcMb") ;
            dataLoan.sU3GovRtcBaseT = (E_PercentBaseT)GetInt("sU3GovRtcBaseT");
			dataLoan.sU3GovRtcPc_rep = GetString("sU3GovRtcPc") ;
			if (isBindAll) 
			{
				dataLoan.sU3GovRtcDesc = GetString("sU3GovRtcDesc") ;
			}
			dataLoan.sU2GovRtcMb_rep = GetString("sU2GovRtcMb") ;
            dataLoan.sU2GovRtcBaseT = (E_PercentBaseT)GetInt("sU2GovRtcBaseT");
			dataLoan.sU2GovRtcPc_rep = GetString("sU2GovRtcPc") ;
			if (isBindAll) 
			{
				dataLoan.sU2GovRtcDesc = GetString("sU2GovRtcDesc") ;
			}
			dataLoan.sU1GovRtcMb_rep = GetString("sU1GovRtcMb") ;
            dataLoan.sU1GovRtcBaseT = (E_PercentBaseT)GetInt("sU1GovRtcBaseT");
			dataLoan.sU1GovRtcPc_rep = GetString("sU1GovRtcPc") ;
			if (isBindAll) 
			{
				dataLoan.sU1GovRtcDesc = GetString("sU1GovRtcDesc") ;
			}
            dataLoan.sStateRtcDesc = GetString("sStateRtcProps_ctrl_PaidTo_tb");
			dataLoan.sStateRtcMb_rep = GetString("sStateRtcMb") ;
            dataLoan.sStateRtcBaseT = (E_PercentBaseT)GetInt("sStateRtcBaseT");
			dataLoan.sStateRtcPc_rep = GetString("sStateRtcPc") ;
            dataLoan.sCountyRtcDesc = GetString("sCountyRtcProps_ctrl_PaidTo_tb");
			dataLoan.sCountyRtcMb_rep = GetString("sCountyRtcMb") ;
            dataLoan.sCountyRtcBaseT = (E_PercentBaseT)GetInt("sCountyRtcBaseT");
			dataLoan.sCountyRtcPc_rep = GetString("sCountyRtcPc") ;
            dataLoan.sRecFDesc = GetString("sRecFProps_ctrl_PaidTo_tb");
			dataLoan.sRecFMb_rep = GetString("sRecFMb") ;
            dataLoan.sRecBaseT = (E_PercentBaseT)GetInt("sRecBaseT");
			dataLoan.sRecFPc_rep = GetString("sRecFPc") ;
            dataLoan.sRecFLckd = GetBool("sRecFLckd");
            dataLoan.sRecDeed_rep = GetString("sRecDeed");
            dataLoan.sRecMortgage_rep = GetString("sRecMortgage");
            dataLoan.sRecRelease_rep = GetString("sRecRelease");
			dataLoan.sU5Sc_rep = GetString("sU5Sc") ;
			if (isBindAll) 
			{
				dataLoan.sU5ScDesc = GetString("sU5ScDesc") ;
			}
			dataLoan.sU4Sc_rep = GetString("sU4Sc") ;
			if (isBindAll) 
			{
				dataLoan.sU4ScDesc = GetString("sU4ScDesc") ;
			}
			dataLoan.sU3Sc_rep = GetString("sU3Sc") ;
			if (isBindAll) 
			{
				dataLoan.sU3ScDesc = GetString("sU3ScDesc") ;
			}
			dataLoan.sU2Sc_rep = GetString("sU2Sc") ;
			if (isBindAll) 
			{
				dataLoan.sU2ScDesc = GetString("sU2ScDesc") ;
			}
			dataLoan.sU1Sc_rep = GetString("sU1Sc") ;
			if (isBindAll) 
			{
				dataLoan.sU1ScDesc = GetString("sU1ScDesc") ;
			}
			dataLoan.sPestInspectF_rep = GetString("sPestInspectF") ;

			if (isBindAll) 
			{
				dataLoan.sLpTemplateNm = GetString("sLpTemplateNm") ;
				dataLoan.sCcTemplateNm = GetString("sCcTemplateNm") ;
			}
			if (null != GetString("sFfUfmipR", null)) 
			{
				dataLoan.sFfUfmipR_rep = GetString("sFfUfmipR");
			}
            dataLoan.sGfeCreditLenderPaidItemT = (E_CreditLenderPaidItemT)GetInt("sGfeCreditLenderPaidItemT");
            dataLoan.sLOrigFProps = RetrieveItemProps("sLOrigFProps", false, true, true, true, dataLoan.sLOrigFProps);
            //dataLoan.sLDiscntProps = RetrieveItemProps("sLDiscntProps", false, false);
            dataLoan.sGfeOriginatorCompFProps = RetrieveItemProps("sGfeOriginatorCompFProps", dataLoan.sGfeOriginatorCompFProps );
            dataLoan.sGfeLenderCreditFProps = RetrieveItemProps("sGfeLenderCreditFProps", false, false, false, false, dataLoan.sGfeLenderCreditFProps);
            dataLoan.sGfeDiscountPointFProps = RetrieveItemProps("sGfeDiscountPointFProps", false, false, true, true, dataLoan.sGfeDiscountPointFProps);
            dataLoan.sApprFProps = RetrieveItemProps("sApprFProps", true, true, false, false, dataLoan.sApprFProps);
            dataLoan.sCrFProps = RetrieveItemProps("sCrFProps", true, true, false, false, dataLoan.sCrFProps);
            dataLoan.sInspectFProps = RetrieveItemProps("sInspectFProps", true, true, false, false, dataLoan.sInspectFProps);
            dataLoan.sMBrokFProps = RetrieveItemProps("sMBrokFProps", true, true, false, false, dataLoan.sMBrokFProps);
            dataLoan.sTxServFProps = RetrieveItemProps("sTxServFProps", true, true, false, false, dataLoan.sTxServFProps);
            dataLoan.sFloodCertificationFProps = RetrieveItemProps("sFloodCertificationFProps", true, true, false, false, dataLoan.sFloodCertificationFProps);
            dataLoan.sProcFProps = RetrieveItemProps("sProcFProps", true, true, false, false, dataLoan.sProcFProps);
            dataLoan.sUwFProps = RetrieveItemProps("sUwFProps", true, true, false, false, dataLoan.sUwFProps);
            dataLoan.sWireFProps = RetrieveItemProps("sWireFProps", true, true, false, false, dataLoan.sWireFProps);
            dataLoan.s800U1FProps = RetrieveItemProps("s800U1FProps", true, true, false, false, dataLoan.s800U1FProps);
            dataLoan.s800U2FProps = RetrieveItemProps("s800U2FProps", true, true, false, false, dataLoan.s800U2FProps);
            dataLoan.s800U3FProps = RetrieveItemProps("s800U3FProps", true, true, false, false, dataLoan.s800U3FProps);
            dataLoan.s800U4FProps = RetrieveItemProps("s800U4FProps", true, true, false, false, dataLoan.s800U4FProps);
            dataLoan.s800U5FProps = RetrieveItemProps("s800U5FProps", true, true, false, false, dataLoan.s800U5FProps);
            dataLoan.sIPiaProps = RetrieveItemProps("sIPiaProps", true, false, false, false, dataLoan.sIPiaProps);
            dataLoan.sMipPiaProps = RetrieveItemProps902905("sMipPiaProps", dataLoan.sMipPiaProps);
            //dataLoan.sMipPiaProps = RetrieveItemProps("sMipPiaProps", true, false, false, false);
            dataLoan.sHazInsPiaProps = RetrieveItemProps("sHazInsPiaProps", true, false, false, false, dataLoan.sHazInsPiaProps);
            dataLoan.s904PiaProps = RetrieveItemProps("s904PiaProps", true, false, false, false, dataLoan.s904PiaProps);
            dataLoan.sVaFfProps = RetrieveItemProps902905("sVaFfProps", dataLoan.sVaFfProps);
            //dataLoan.sVaFfProps = RetrieveItemProps("sVaFfProps", true, false, false, false);
            dataLoan.s900U1PiaProps = RetrieveItemProps("s900U1PiaProps", true, false, false, false, dataLoan.s900U1PiaProps);
            dataLoan.sHazInsRsrvProps = RetrieveItemProps("sHazInsRsrvProps", true, false, false, false, dataLoan.sHazInsRsrvProps);
            dataLoan.sMInsRsrvProps = RetrieveItemProps("sMInsRsrvProps", true, false, false, false, dataLoan.sMInsRsrvProps);
            dataLoan.sSchoolTxRsrvProps = RetrieveItemProps("sSchoolTxRsrvProps", true, false, false, false, dataLoan.sSchoolTxRsrvProps);
            dataLoan.sRealETxRsrvProps = RetrieveItemProps("sRealETxRsrvProps", true, false, false, false, dataLoan.sRealETxRsrvProps);
            dataLoan.sFloodInsRsrvProps = RetrieveItemProps("sFloodInsRsrvProps", true, false, false, false, dataLoan.sFloodInsRsrvProps);
            dataLoan.s1006RsrvProps = RetrieveItemProps("s1006RsrvProps", true, false, false, false, dataLoan.s1006RsrvProps);
            dataLoan.s1007RsrvProps = RetrieveItemProps("s1007RsrvProps", true, false, false, false, dataLoan.s1007RsrvProps);
            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                dataLoan.sU3RsrvProps = RetrieveItemProps("sU3RsrvProps", true, false, false, false, dataLoan.sU3RsrvProps);
                dataLoan.sU4RsrvProps = RetrieveItemProps("sU4RsrvProps", true, false, false, false, dataLoan.sU4RsrvProps);
            }
            dataLoan.sAggregateAdjRsrvProps = RetrieveItemProps("sAggregateAdjRsrvProps", true, false, false, false, dataLoan.sAggregateAdjRsrvProps);
            
            #region ( 1100 Props )
            dataLoan.sEscrowFProps = RetrieveItemProps("sEscrowFProps", true, true, false, false, true, dataLoan.sEscrowFProps);
            dataLoan.sOwnerTitleInsProps = RetrieveItemProps("sOwnerTitleInsFProps", true, true, false, false, true, dataLoan.sOwnerTitleInsProps);
            dataLoan.sTitleInsFProps = RetrieveItemProps("sTitleInsFProps", true, true, false, false, true, dataLoan.sTitleInsFProps);
            dataLoan.sDocPrepFProps = RetrieveItemProps("sDocPrepFProps", true, true, false, false, true, dataLoan.sDocPrepFProps);
            dataLoan.sNotaryFProps = RetrieveItemProps("sNotaryFProps", true, true, false, false, true, dataLoan.sNotaryFProps);
            dataLoan.sAttorneyFProps = RetrieveItemProps("sAttorneyFProps", true, true, false, false, true, dataLoan.sAttorneyFProps);
            dataLoan.sU1TcProps = RetrieveItemProps("sU1TcProps", true, true, false, false, true, dataLoan.sU1TcProps);
            dataLoan.sU2TcProps = RetrieveItemProps("sU2TcProps", true, true, false, false, true, dataLoan.sU2TcProps);
            dataLoan.sU3TcProps = RetrieveItemProps("sU3TcProps", true, true, false, false, true, dataLoan.sU3TcProps);
            dataLoan.sU4TcProps = RetrieveItemProps("sU4TcProps", true, true, false, false, true, dataLoan.sU4TcProps);
            #endregion 
            #region ( 1200 props ) 
            dataLoan.sRecFProps = RetrieveItemProps("sRecFProps", true, false, false, false, dataLoan.sRecFProps);
            dataLoan.sCountyRtcProps = RetrieveItemProps("sCountyRtcProps", true, false, false, false, dataLoan.sCountyRtcProps);
            dataLoan.sStateRtcProps = RetrieveItemProps("sStateRtcProps", true, false, false, false, dataLoan.sStateRtcProps);
            dataLoan.sU1GovRtcProps = RetrieveItemProps("sU1GovRtcProps", true, false, false, false, dataLoan.sU1GovRtcProps);
            dataLoan.sU2GovRtcProps = RetrieveItemProps("sU2GovRtcProps", true, false, false, false, dataLoan.sU2GovRtcProps);
            dataLoan.sU3GovRtcProps = RetrieveItemProps("sU3GovRtcProps", true, false, false, false, dataLoan.sU3GovRtcProps);
            #endregion 
            #region ( 1300 props )
            dataLoan.sPestInspectFProps = RetrieveItemProps("sPestInspectFProps", true, true, false, false, true, dataLoan.sPestInspectFProps);
            dataLoan.sU1ScProps = RetrieveItemProps("sU1ScProps", true, true, false, false, true, dataLoan.sU1ScProps);
            dataLoan.sU2ScProps = RetrieveItemProps("sU2ScProps", true, true, false, false, true, dataLoan.sU2ScProps);
            dataLoan.sU3ScProps = RetrieveItemProps("sU3ScProps", true, true, false, false, true, dataLoan.sU3ScProps);
            dataLoan.sU4ScProps = RetrieveItemProps("sU4ScProps", true, true, false, false, true, dataLoan.sU4ScProps);
            dataLoan.sU5ScProps = RetrieveItemProps("sU5ScProps", true, true, false, false, true, dataLoan.sU5ScProps);
            #endregion

            dataLoan.sApprFPaidTo = GetString("sApprFProps_ctrl_PaidTo_tb");
            dataLoan.sCrFPaidTo = GetString("sCrFProps_ctrl_PaidTo_tb");
            dataLoan.sTxServFPaidTo = GetString("sTxServFProps_ctrl_PaidTo_tb");
            dataLoan.sFloodCertificationFPaidTo = GetString("sFloodCertificationFProps_ctrl_PaidTo_tb");
            dataLoan.sInspectFPaidTo = GetString("sInspectFProps_ctrl_PaidTo_tb");
            dataLoan.sProcFPaidTo = GetString("sProcFProps_ctrl_PaidTo_tb");
            dataLoan.sUwFPaidTo = GetString("sUwFProps_ctrl_PaidTo_tb");
            dataLoan.sWireFPaidTo = GetString("sWireFProps_ctrl_PaidTo_tb");
            dataLoan.s800U1FPaidTo = GetString("s800U1FProps_ctrl_PaidTo_tb");
            dataLoan.s800U2FPaidTo = GetString("s800U2FProps_ctrl_PaidTo_tb");
            dataLoan.s800U3FPaidTo = GetString("s800U3FProps_ctrl_PaidTo_tb");
            dataLoan.s800U4FPaidTo = GetString("s800U4FProps_ctrl_PaidTo_tb");
            dataLoan.s800U5FPaidTo = GetString("s800U5FProps_ctrl_PaidTo_tb");
            dataLoan.sOwnerTitleInsPaidTo = GetString("sOwnerTitleInsFProps_ctrl_PaidTo_tb");
            dataLoan.sDocPrepFPaidTo = GetString("sDocPrepFProps_ctrl_PaidTo_tb");
            dataLoan.sNotaryFPaidTo = GetString("sNotaryFProps_ctrl_PaidTo_tb");
            dataLoan.sAttorneyFPaidTo = GetString("sAttorneyFProps_ctrl_PaidTo_tb");
            dataLoan.sU1TcPaidTo = GetString("sU1TcProps_ctrl_PaidTo_tb");
            dataLoan.sU2TcPaidTo = GetString("sU2TcProps_ctrl_PaidTo_tb");
            dataLoan.sU3TcPaidTo = GetString("sU3TcProps_ctrl_PaidTo_tb");
            dataLoan.sU4TcPaidTo = GetString("sU4TcProps_ctrl_PaidTo_tb");
            dataLoan.sU1GovRtcPaidTo = GetString("sU1GovRtcProps_ctrl_PaidTo_tb");
            dataLoan.sU2GovRtcPaidTo = GetString("sU2GovRtcProps_ctrl_PaidTo_tb");
            dataLoan.sU3GovRtcPaidTo = GetString("sU3GovRtcProps_ctrl_PaidTo_tb");
            dataLoan.sPestInspectPaidTo = GetString("sPestInspectFProps_ctrl_PaidTo_tb");
            dataLoan.sU1ScPaidTo = GetString("sU1ScProps_ctrl_PaidTo_tb");
            dataLoan.sU2ScPaidTo = GetString("sU2ScProps_ctrl_PaidTo_tb");
            dataLoan.sU3ScPaidTo = GetString("sU3ScProps_ctrl_PaidTo_tb");
            dataLoan.sU4ScPaidTo = GetString("sU4ScProps_ctrl_PaidTo_tb");
            dataLoan.sU5ScPaidTo = GetString("sU5ScProps_ctrl_PaidTo_tb");
            dataLoan.sHazInsPiaPaidTo = GetString("sHazInsPiaProps_ctrl_PaidTo_tb");
            dataLoan.sMipPiaPaidTo = GetString("sMipPiaProps_ctrl_PaidTo_tb");
            dataLoan.sVaFfPaidTo = GetString("sVaFfProps_ctrl_PaidTo_tb");
            
            dataLoan.s800U1FGfeSection = (E_GfeSectionT)GetInt("s800U1FProps_ctrl_Page2");
            dataLoan.s800U2FGfeSection = (E_GfeSectionT)GetInt("s800U2FProps_ctrl_Page2");
            dataLoan.s800U3FGfeSection = (E_GfeSectionT)GetInt("s800U3FProps_ctrl_Page2");
            dataLoan.s800U4FGfeSection = (E_GfeSectionT)GetInt("s800U4FProps_ctrl_Page2");
            dataLoan.s800U5FGfeSection = (E_GfeSectionT)GetInt("s800U5FProps_ctrl_Page2");
            dataLoan.sEscrowFGfeSection = (E_GfeSectionT)GetInt("sEscrowFProps_ctrl_Page2");
            //dataLoan.sTitleInsFGfeSection = (E_GfeSectionT)GetInt("sTitleInsFProps_ctrl_Page2");
            dataLoan.sDocPrepFGfeSection = (E_GfeSectionT)GetInt("sDocPrepFProps_ctrl_Page2");
            dataLoan.sNotaryFGfeSection = (E_GfeSectionT)GetInt("sNotaryFProps_ctrl_Page2");
            dataLoan.sAttorneyFGfeSection = (E_GfeSectionT)GetInt("sAttorneyFProps_ctrl_Page2");
            dataLoan.sU1TcGfeSection = (E_GfeSectionT)GetInt("sU1TcProps_ctrl_Page2");
            dataLoan.sU2TcGfeSection = (E_GfeSectionT)GetInt("sU2TcProps_ctrl_Page2");
            dataLoan.sU3TcGfeSection = (E_GfeSectionT)GetInt("sU3TcProps_ctrl_Page2");
            dataLoan.sU4TcGfeSection = (E_GfeSectionT)GetInt("sU4TcProps_ctrl_Page2");
            dataLoan.sU1GovRtcGfeSection = (E_GfeSectionT)GetInt("sU1GovRtcProps_ctrl_Page2");
            dataLoan.sU2GovRtcGfeSection = (E_GfeSectionT)GetInt("sU2GovRtcProps_ctrl_Page2");
            dataLoan.sU3GovRtcGfeSection = (E_GfeSectionT)GetInt("sU3GovRtcProps_ctrl_Page2");
            dataLoan.sU1ScGfeSection = (E_GfeSectionT)GetInt("sU1ScProps_ctrl_Page2");
            dataLoan.sU2ScGfeSection = (E_GfeSectionT)GetInt("sU2ScProps_ctrl_Page2");
            dataLoan.sU3ScGfeSection = (E_GfeSectionT)GetInt("sU3ScProps_ctrl_Page2");
            dataLoan.sU4ScGfeSection = (E_GfeSectionT)GetInt("sU4ScProps_ctrl_Page2");
            dataLoan.sU5ScGfeSection = (E_GfeSectionT)GetInt("sU5ScProps_ctrl_Page2");
            dataLoan.s904PiaGfeSection = (E_GfeSectionT)GetInt("s904PiaProps_ctrl_Page2");
            dataLoan.s900U1PiaGfeSection = (E_GfeSectionT)GetInt("s900U1PiaProps_ctrl_Page2");

            dataLoan.sGfeTradeOffLowerCCLoanAmt_rep = GetString("sGfeTradeOffLowerCCLoanAmt");
            dataLoan.sGfeTradeOffLowerCCNoteIR_rep = GetString("sGfeTradeOffLowerCCNoteIR");
            dataLoan.sGfeTradeOffLowerCCClosingCost_rep = GetString("sGfeTradeOffLowerCCClosingCost");
            dataLoan.sGfeTradeOffLowerRateLoanAmt_rep = GetString("sGfeTradeOffLowerRateLoanAmt");
            dataLoan.sGfeTradeOffLowerRateNoteIR_rep = GetString("sGfeTradeOffLowerRateNoteIR");
            dataLoan.sGfeTradeOffLowerRateClosingCost_rep = GetString("sGfeTradeOffLowerRateClosingCost");

            E_TriState yes = E_TriState.Yes;
            E_TriState no = E_TriState.No;
            E_TriState blk = E_TriState.Blank;

            dataLoan.sGfeShoppingCartLoan1OriginatorName = GetString("sGfeShoppingCartLoan1OriginatorName");
            dataLoan.sGfeShoppingCartLoan1LoanAmt_rep = GetString("sGfeShoppingCartLoan1LoanAmt");
            dataLoan.sGfeShoppingCartLoan1LoanTerm_rep = GetString("sGfeShoppingCartLoan1LoanTerm");
            dataLoan.sGfeShoppingCartLoan1NoteIR_rep = GetString("sGfeShoppingCartLoan1NoteIR");
            dataLoan.sGfeShoppingCartLoan1InitialPmt_rep = GetString("sGfeShoppingCartLoan1InitialPmt");
            dataLoan.sGfeShoppingCartLoan1RateLockPeriod_rep = GetString("sGfeShoppingCartLoan1RateLockPeriod");
            if (!String.IsNullOrEmpty(dataLoan.sGfeShoppingCartLoan1OriginatorName))
            {
                dataLoan.sGfeShoppingCartLoan1CanRateIncreaseTri = GetBool("sGfeShoppingCartLoan1CanRateIncreaseTri") ? yes : no;
                dataLoan.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri = GetBool("sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri") ? yes : no;
                dataLoan.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri = GetBool("sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri") ? yes : no;
                dataLoan.sGfeShoppingCartLoan1HavePpmtPenaltyTri = GetBool("sGfeShoppingCartLoan1HavePpmtPenaltyTri") ? yes : no;
                dataLoan.sGfeShoppingCartLoan1IsBalloonTri = GetBool("sGfeShoppingCartLoan1IsBalloonTri") ? yes : no;
            }
            else
            {
                dataLoan.sGfeShoppingCartLoan1CanRateIncreaseTri = blk;
                dataLoan.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri = blk;
                dataLoan.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri = blk;
                dataLoan.sGfeShoppingCartLoan1HavePpmtPenaltyTri = blk;
                dataLoan.sGfeShoppingCartLoan1IsBalloonTri = blk;
            }
            dataLoan.sGfeShoppingCartLoan1TotalClosingCost_rep = GetString("sGfeShoppingCartLoan1TotalClosingCost");

            dataLoan.sGfeShoppingCartLoan2OriginatorName = GetString("sGfeShoppingCartLoan2OriginatorName");
            dataLoan.sGfeShoppingCartLoan2LoanAmt_rep = GetString("sGfeShoppingCartLoan2LoanAmt");
            dataLoan.sGfeShoppingCartLoan2LoanTerm_rep = GetString("sGfeShoppingCartLoan2LoanTerm");
            dataLoan.sGfeShoppingCartLoan2NoteIR_rep = GetString("sGfeShoppingCartLoan2NoteIR");
            dataLoan.sGfeShoppingCartLoan2InitialPmt_rep = GetString("sGfeShoppingCartLoan2InitialPmt");
            dataLoan.sGfeShoppingCartLoan2RateLockPeriod_rep = GetString("sGfeShoppingCartLoan2RateLockPeriod");
            if (!String.IsNullOrEmpty(dataLoan.sGfeShoppingCartLoan2OriginatorName))
            {
                dataLoan.sGfeShoppingCartLoan2CanRateIncreaseTri = GetBool("sGfeShoppingCartLoan2CanRateIncreaseTri") ? yes : no;
                dataLoan.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri = GetBool("sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri") ? yes : no;
                dataLoan.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri = GetBool("sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri") ? yes : no;
                dataLoan.sGfeShoppingCartLoan2HavePpmtPenaltyTri = GetBool("sGfeShoppingCartLoan2HavePpmtPenaltyTri") ? yes : no;
                dataLoan.sGfeShoppingCartLoan2IsBalloonTri = GetBool("sGfeShoppingCartLoan2IsBalloonTri") ? yes : no;
            }
            else
            {
                dataLoan.sGfeShoppingCartLoan2CanRateIncreaseTri = blk;
                dataLoan.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri = blk;
                dataLoan.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri = blk;
                dataLoan.sGfeShoppingCartLoan2HavePpmtPenaltyTri = blk;
                dataLoan.sGfeShoppingCartLoan2IsBalloonTri = blk;
            }
            dataLoan.sGfeShoppingCartLoan2TotalClosingCost_rep = GetString("sGfeShoppingCartLoan2TotalClosingCost");

            dataLoan.sGfeShoppingCartLoan3OriginatorName = GetString("sGfeShoppingCartLoan3OriginatorName");
            dataLoan.sGfeShoppingCartLoan3LoanAmt_rep = GetString("sGfeShoppingCartLoan3LoanAmt");
            dataLoan.sGfeShoppingCartLoan3LoanTerm_rep = GetString("sGfeShoppingCartLoan3LoanTerm");
            dataLoan.sGfeShoppingCartLoan3NoteIR_rep = GetString("sGfeShoppingCartLoan3NoteIR");
            dataLoan.sGfeShoppingCartLoan3InitialPmt_rep = GetString("sGfeShoppingCartLoan3InitialPmt");
            dataLoan.sGfeShoppingCartLoan3RateLockPeriod_rep = GetString("sGfeShoppingCartLoan3RateLockPeriod");
            if (!String.IsNullOrEmpty(dataLoan.sGfeShoppingCartLoan3OriginatorName))
            {
                dataLoan.sGfeShoppingCartLoan3CanRateIncreaseTri = GetBool("sGfeShoppingCartLoan3CanRateIncreaseTri") ? yes : no;
                dataLoan.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri = GetBool("sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri") ? yes : no;
                dataLoan.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri = GetBool("sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri") ? yes : no;
                dataLoan.sGfeShoppingCartLoan3HavePpmtPenaltyTri = GetBool("sGfeShoppingCartLoan3HavePpmtPenaltyTri") ? yes : no;
                dataLoan.sGfeShoppingCartLoan3IsBalloonTri = GetBool("sGfeShoppingCartLoan3IsBalloonTri") ? yes : no;
            }
            else
            {
                dataLoan.sGfeShoppingCartLoan3CanRateIncreaseTri = blk;
                dataLoan.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri = blk;
                dataLoan.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri = blk;
                dataLoan.sGfeShoppingCartLoan3HavePpmtPenaltyTri = blk;
                dataLoan.sGfeShoppingCartLoan3IsBalloonTri = blk;
            }
            dataLoan.sGfeShoppingCartLoan3TotalClosingCost_rep = GetString("sGfeShoppingCartLoan3TotalClosingCost");

            dataLoan.sGfeIsTPOTransaction = GetBool("sGfeIsTPOTransaction");
            dataLoan.sIsItemizeBrokerCommissionOnIFW = GetBool("sIsItemizeBrokerCommissionOnIFW");

            E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT = (E_sOriginatorCompensationPaymentSourceT) GetInt("sOriginatorCompensationPaymentSourceT");

            string sGfeOriginatorCompFPc_rep = GetString("sGfeOriginatorCompFPc") ;
            string sGfeOriginatorCompFMb_rep = GetString("sGfeOriginatorCompFMb");
            E_PercentBaseT sGfeOriginatorCompFBaseT = (E_PercentBaseT) GetInt("sGfeOriginatorCompFBaseT");

            dataLoan.SetOriginatorCompensation(sOriginatorCompensationPaymentSourceT, sGfeOriginatorCompFPc_rep, sGfeOriginatorCompFBaseT, sGfeOriginatorCompFMb_rep);

            if (false == dataLoan.BrokerDB.IsProtectDisclosureDates)
            {
                dataLoan.sGfeRedisclosureD_rep = GetString("sGfeRedisclosureD");
                dataLoan.Set_sGfeInitialDisclosureD_rep(GetString("sGfeInitialDisclosureD"), E_GFEArchivedReasonT.InitialDislosureDateSetViaGFE);
            }

            // sGfeInitialDisclosureD_rep should *always* be set last.


            // OPM 170146 - Footer, Details of Transaction tab
            if (!IsArchivePage)
            {
                dataLoan.sAltCostLckd = GetBool("Gfe2010Footer_dot_sAltCostLckd");
                dataLoan.sAltCost_rep = GetString("Gfe2010Footer_dot_sAltCost");
                dataLoan.sFfUfmip1003Lckd = GetBool("Gfe2010Footer_dot_sFfUfmip1003Lckd");
                dataLoan.sFfUfmip1003_rep = GetString("Gfe2010Footer_dot_sFfUfmip1003");
                dataLoan.sLDiscnt1003Lckd = GetBool("Gfe2010Footer_dot_sLDiscnt1003Lckd");
                dataLoan.sLDiscnt1003_rep = GetString("Gfe2010Footer_dot_sLDiscnt1003");
                dataLoan.sLandCost_rep = GetString("Gfe2010Footer_dot_sLandCost");
                dataLoan.sOCredit1Amt_rep = GetString("Gfe2010Footer_dot_sOCredit1Amt");
                dataLoan.sOCredit1Desc = GetString("Gfe2010Footer_dot_sOCredit1Desc");
                dataLoan.sOCredit1Lckd = GetBool("Gfe2010Footer_dot_sOCredit1Lckd");

                if (false == dataLoan.sLoads1003LineLFromAdjustments)
                {
                    dataLoan.sOCredit2Amt_rep = GetString("Gfe2010Footer_dot_sOCredit2Amt");
                    dataLoan.sOCredit2Desc = GetString("Gfe2010Footer_dot_sOCredit2Desc");
                    dataLoan.sOCredit3Amt_rep = GetString("Gfe2010Footer_dot_sOCredit3Amt");
                    dataLoan.sOCredit3Desc = GetString("Gfe2010Footer_dot_sOCredit3Desc");
                    dataLoan.sOCredit4Amt_rep = GetString("Gfe2010Footer_dot_sOCredit4Amt");
                    dataLoan.sOCredit4Desc = GetString("Gfe2010Footer_dot_sOCredit4Desc");
                }

                dataLoan.sPurchPrice_rep = GetString("Gfe2010Footer_dot_sPurchPrice");
                dataLoan.sRefPdOffAmt1003Lckd = GetBool("Gfe2010Footer_dot_sRefPdOffAmt1003Lckd");
                dataLoan.sRefPdOffAmt1003_rep = GetString("Gfe2010Footer_dot_sRefPdOffAmt1003");
                dataLoan.sTotCcPbsLocked = GetBool("Gfe2010Footer_dot_sTotCcPbsLocked");
                dataLoan.sTotCcPbs_rep = GetString("Gfe2010Footer_dot_sTotCcPbs");
                dataLoan.sTotEstCc1003Lckd = GetBool("Gfe2010Footer_dot_sTotEstCc1003Lckd");
                dataLoan.sTotEstCcNoDiscnt1003_rep = GetString("Gfe2010Footer_dot_sTotEstCcNoDiscnt1003");
                dataLoan.sTotEstPp1003Lckd = GetBool("Gfe2010Footer_dot_sTotEstPp1003Lckd");
                dataLoan.sTotEstPp1003_rep = GetString("Gfe2010Footer_dot_sTotEstPp1003");
                dataLoan.sTransNetCashLckd = GetBool("Gfe2010Footer_dot_sTransNetCashLckd");
                dataLoan.sTransNetCash_rep = GetString("Gfe2010Footer_dot_sTransNetCash");

                if (!dataLoan.sIsIncludeONewFinCcInTotEstCc)
                {
                    dataLoan.sONewFinCc_rep = GetString("Gfe2010Footer_dot_sONewFinCc");
                }
                else
                {
                    dataLoan.sONewFinCc_rep = GetString("Gfe2010Footer_dot_sONewFinCc2");
                }

                dataLoan.sLAmtCalc_rep = GetString("Gfe2010Footer_dot_sLAmtCalc");
                dataLoan.sLAmtLckd = GetBool("Gfe2010Footer_dot_sLAmtLckd");
            }

			return alertUser;
		}

		/// <summary>
		/// For performance reason, I only return calculated data. Therefore
		/// DO NOT use this method to return complete GFE data. dd 7/25/2003
		/// </summary>
		/// <param name="dataLoan"></param>
		/// <returns></returns>
		private void LoadData(CPageData dataLoan) 
		{
            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew);
            if (false == Broker.IsRemovePreparedDates)
            {
                SetResult("GfeTilPrepareDate", gfeTil.PrepareDate_rep);
            }
            SetResult("GfeTilCompanyName", gfeTil.CompanyName);
            SetResult("GfeTilStreetAddr", gfeTil.StreetAddr);
            SetResult("GfeTilCity", gfeTil.City);
            SetResult("GfeTilState", gfeTil.State);
            SetResult("GfeTilZip", gfeTil.Zip);
            SetResult("GfeTilPhoneOfCompany", gfeTil.PhoneOfCompany);
            SetResult("GfeTilEmailAddr", gfeTil.EmailAddr);
            SetResult("CFM_AgentRoleT", gfeTil.AgentRoleT);
            SetResult("CFM_IsLocked", gfeTil.IsLocked);

            SetResult("sFinMethT", dataLoan.sFinMethT);
            SetResult("sDaysInYr", dataLoan.sDaysInYr_rep);
            SetResult("sRAdj1stCapR", dataLoan.sRAdj1stCapR_rep);
            SetResult("sRAdjCapR", dataLoan.sRAdjCapR_rep);
            SetResult("sRAdjLifeCapR",dataLoan.sRAdjLifeCapR_rep );
            SetResult("sRAdjCapMon", dataLoan.sRAdjCapMon_rep);
            SetResult("sRAdj1stCapMon", dataLoan.sRAdj1stCapMon_rep);
            SetResult("sPmtAdjCapR", dataLoan.sPmtAdjCapR_rep);
            SetResult("sPmtAdjMaxBalPc", dataLoan.sPmtAdjMaxBalPc_rep);
            SetResult("sPmtAdjRecastStop", dataLoan.sPmtAdjRecastStop_rep);
            SetResult("sPmtAdjRecastPeriodMon", dataLoan.sPmtAdjRecastPeriodMon_rep);
            SetResult("sPmtAdjCapMon", dataLoan.sPmtAdjCapMon_rep);
            SetResult("sIOnlyMon", dataLoan.sIOnlyMon_rep);

            SetResult("sGfeNoteIRAvailTillD", dataLoan.sGfeNoteIRAvailTillD_Date);
            SetResult("sGfeEstScAvailTillD", dataLoan.sGfeEstScAvailTillD_Date);
            SetResult("sGfeRateLockPeriod", dataLoan.sGfeRateLockPeriod_rep);
            SetResult("sGfeRateLockPeriodLckd", dataLoan.sGfeRateLockPeriodLckd);
            SetResult("sGfeLockPeriodBeforeSettlement", dataLoan.sGfeLockPeriodBeforeSettlement_rep);
            SetResult("sGfeCanRateIncrease", dataLoan.sGfeCanRateIncrease ? "1" : "0");
            SetResult("sRLifeCapR", dataLoan.sRLifeCapR_rep);
            SetResult("sGfeFirstInterestChangeIn", dataLoan.sGfeFirstInterestChangeIn);
            SetResult("sGfeCanLoanBalanceIncrease", dataLoan.sGfeCanLoanBalanceIncrease ? "1" : "0");
            SetResult("sGfeMaxLoanBalance", dataLoan.sGfeMaxLoanBalance_rep);
            SetResult("sGfeCanRateIncrease2", dataLoan.sGfeCanRateIncrease ? "1" : "0");
            SetResult("sGfeFirstPaymentChangeIn", dataLoan.sGfeFirstPaymentChangeIn);
            SetResult("sGfeFirstAdjProThisMPmtAndMIns", dataLoan.sGfeFirstAdjProThisMPmtAndMIns_rep);
            SetResult("sGfeMaxProThisMPmtAndMIns", dataLoan.sGfeMaxProThisMPmtAndMIns_rep);
            SetResult("sGfeHavePpmtPenalty", dataLoan.sGfeHavePpmtPenalty ? "1" : "0");
            SetResult("sGfeMaxPpmtPenaltyAmt", dataLoan.sGfeMaxPpmtPenaltyAmt_rep);
            SetResult("sGfeIsBalloon", dataLoan.sGfeIsBalloon ? "1" : "0");
            SetResult("sGfeBalloonPmt", dataLoan.sGfeBalloonPmt_rep);
            SetResult("sGfeBalloonDueInYrs", dataLoan.sGfeBalloonDueInYrs);
            SetResult("sIsPreserveGFEFees", dataLoan.sClosingCostAutomationUpdateT == E_sClosingCostAutomationUpdateT.PreserveFeesOnLoan ? "1" : "0");
            SetResult("sMldsHasImpound", dataLoan.sMldsHasImpound ? "1" : "0");
            SetResult("sGfeUsePaidToFromOfficialContact", dataLoan.sGfeUsePaidToFromOfficialContact);
            SetResult("sGfeRequirePaidToFromContacts", dataLoan.sGfeRequirePaidToFromContacts);
            SetResult("s800U5F", dataLoan.s800U5F_rep);
            SetResult("s800U5FDesc", dataLoan.s800U5FDesc);
            SetResult("s800U4F", dataLoan.s800U4F_rep);
            SetResult("s800U4FDesc", dataLoan.s800U4FDesc);
            SetResult("s800U3F", dataLoan.s800U3F_rep);
            SetResult("s800U3FDesc", dataLoan.s800U3FDesc);
            SetResult("s800U2F", dataLoan.s800U2F_rep);
            SetResult("s800U2FDesc", dataLoan.s800U2FDesc);
            SetResult("s800U1F", dataLoan.s800U1F_rep);
            SetResult("s800U1FDesc", dataLoan.s800U1FDesc);
            SetResult("sWireF", dataLoan.sWireF_rep);
            SetResult("sUwF", dataLoan.sUwF_rep);
            SetResult("sProcF", dataLoan.sProcF_rep);
            SetResult("sTxServF", dataLoan.sTxServF_rep);
            SetResult("sFloodCertificationF", dataLoan.sFloodCertificationF_rep);
            SetResult("sFloodCertificationDeterminationT", dataLoan.sFloodCertificationDeterminationT);
            SetResult("sMBrokFMb", dataLoan.sMBrokFMb_rep);
            SetResult("sMBrokFPc", dataLoan.sMBrokFPc_rep);
            SetResult("sMBrokFBaseT", dataLoan.sMBrokFBaseT);
            SetResult("sCrF", dataLoan.sCrF_rep);
            SetResult("sApprF", dataLoan.sApprF_rep);
            SetResult("sLDiscntFMb", dataLoan.sLDiscntFMb_rep);
            SetResult("sLDiscntPc", dataLoan.sLDiscntPc_rep);
            SetResult("sLDiscntBaseT", dataLoan.sLDiscntBaseT);

            if (ShowRequireFeesFromDropDownCheckbox)
            {
                SetResult("sIsRequireFeesFromDropDown", dataLoan.sIsRequireFeesFromDropDown);
            }

            if (dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                if (dataLoan.sLastDisclosedGFEArchiveD_rep == "")
                {
                    SetResult("sLastDisclosedGFEArchiveD", "0");
                }
                else
                {
                    SetResult("sLastDisclosedGFEArchiveD", dataLoan.sLastDisclosedGFEArchiveD_rep);
                }
            }
            SetResult("sLOrigFMb", dataLoan.sLOrigFMb_rep);
            SetResult("sLOrigFPc", dataLoan.sLOrigFPc_rep);

            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
			SetResult("sMBrokF", dataLoan.sMBrokF_rep);
			SetResult("sInspectF", dataLoan.sInspectF_rep);
			SetResult("sLDiscnt", dataLoan.sLDiscnt_rep);
			SetResult("sLOrigF", dataLoan.sLOrigF_rep);
			SetResult("s900U1Pia", dataLoan.s900U1Pia_rep);
			SetResult("s900U1PiaDesc", dataLoan.s900U1PiaDesc);
            SetResult("sVaFf", dataLoan.sVaFf_rep);
			SetResult("s904Pia", dataLoan.s904Pia_rep);
			SetResult("s904PiaDesc", dataLoan.s904PiaDesc);
            SetResult("sHazInsPia", dataLoan.sHazInsPia_rep);
			SetResult("sHazInsPiaMon", dataLoan.sHazInsPiaMon_rep);
			SetResult("sProHazInsT", dataLoan.sProHazInsT);
			SetResult("sProHazInsR", dataLoan.sProHazInsR_rep);
			SetResult("sProHazInsMb", dataLoan.sProHazInsMb_rep);
			SetResult("sMipPia", dataLoan.sMipPia_rep);
            
			SetResult("sIPia", dataLoan.sIPia_rep);
			SetResult("sIPiaDy", dataLoan.sIPiaDy_rep);
			SetResult("sIPerDayLckd", dataLoan.sIPerDayLckd);
			SetResult("sIPerDay", dataLoan.sIPerDay_rep);
			SetResult("sAggregateAdjRsrv", dataLoan.sAggregateAdjRsrv_rep);
			SetResult("sAggregateAdjRsrvLckd", dataLoan.sAggregateAdjRsrvLckd);
			SetResult("s1007Rsrv", dataLoan.s1007Rsrv_rep);
			SetResult("s1007ProHExp", dataLoan.s1007ProHExp_rep);
			SetResult("s1007RsrvMon", dataLoan.s1007RsrvMon_rep);
			SetResult("s1007ProHExpDesc", dataLoan.s1007ProHExpDesc);
            SetResult("s1006Rsrv", dataLoan.s1006Rsrv_rep);
			SetResult("s1006ProHExp", dataLoan.s1006ProHExp_rep);
			SetResult("s1006RsrvMon", dataLoan.s1006RsrvMon_rep);
            SetResult("s1006ProHExpDesc", dataLoan.s1006ProHExpDesc);
			SetResult("sU3Rsrv", dataLoan.sU3Rsrv_rep);
			SetResult("sProU3Rsrv", dataLoan.sProU3Rsrv_rep);
			SetResult("sU3RsrvMon", dataLoan.sU3RsrvMon_rep);
			SetResult("sU3RsrvDesc", dataLoan.sU3RsrvDesc);
			SetResult("sU4Rsrv", dataLoan.sU4Rsrv_rep);
			SetResult("sProU4Rsrv", dataLoan.sProU4Rsrv_rep);
			SetResult("sU4RsrvMon", dataLoan.sU4RsrvMon_rep);
			SetResult("sU4RsrvDesc", dataLoan.sU4RsrvDesc);
            SetResult("sFloodInsRsrv", dataLoan.sFloodInsRsrv_rep);
			SetResult("sProFloodIns", dataLoan.sProFloodIns_rep);
			SetResult("sFloodInsRsrvMon", dataLoan.sFloodInsRsrvMon_rep);
			SetResult("sRealETxRsrv", dataLoan.sRealETxRsrv_rep);
            SetResult("sProRealETxT", dataLoan.sProRealETxT);
            SetResult("sProRealETxMb", dataLoan.sProRealETxMb_rep);
            SetResult("sProRealETxR", dataLoan.sProRealETxR_rep);
            SetResult("sProRealETx", dataLoan.sProRealETx_rep);
			SetResult("sRealETxRsrvMon", dataLoan.sRealETxRsrvMon_rep);
			SetResult("sSchoolTxRsrv", dataLoan.sSchoolTxRsrv_rep);
			SetResult("sProSchoolTx", dataLoan.sProSchoolTx_rep);
			SetResult("sSchoolTxRsrvMon", dataLoan.sSchoolTxRsrvMon_rep);
			SetResult("sMInsRsrv", dataLoan.sMInsRsrv_rep);
			SetResult("sProMIns", dataLoan.sProMIns_rep);
			SetResult("sMInsRsrvMon", dataLoan.sMInsRsrvMon_rep);
			SetResult("sHazInsRsrv", dataLoan.sHazInsRsrv_rep);
			SetResult("sProHazIns", dataLoan.sProHazIns_rep);
			SetResult("sHazInsRsrvMon", dataLoan.sHazInsRsrvMon_rep);
			SetResult("sTitleInsF", dataLoan.sTitleInsF_rep);
			SetResult("sAttorneyF", dataLoan.sAttorneyF_rep);
			SetResult("sNotaryF", dataLoan.sNotaryF_rep);
			SetResult("sDocPrepF", dataLoan.sDocPrepF_rep);
			SetResult("sEscrowF", dataLoan.sEscrowF_rep);
			SetResult("sU3GovRtc", dataLoan.sU3GovRtc_rep);
			SetResult("sU2GovRtc", dataLoan.sU2GovRtc_rep);
			SetResult("sU1GovRtc", dataLoan.sU1GovRtc_rep);
			SetResult("sStateRtc", dataLoan.sStateRtc_rep);
			SetResult("sCountyRtc", dataLoan.sCountyRtc_rep);
			SetResult("sRecF", dataLoan.sRecF_rep);
			SetResult("sPestInspectF", dataLoan.sPestInspectF_rep);
			SetResult("sTotEstSc", dataLoan.sTotEstSc_rep);
            SetResult("sRecDeed", dataLoan.sRecDeed_rep);
            SetResult("sRecMortgage", dataLoan.sRecMortgage_rep);
            SetResult("sRecRelease", dataLoan.sRecRelease_rep);
            SetResult("sRecFLckd", dataLoan.sRecFLckd);
            SetResult("sU4Tc", dataLoan.sU4Tc_rep);
            SetResult("sU4TcDesc", dataLoan.sU4TcDesc);
            SetResult("sU3Tc", dataLoan.sU3Tc_rep);
            SetResult("sU3TcDesc", dataLoan.sU3TcDesc);
            SetResult("sU2Tc", dataLoan.sU2Tc_rep);
            SetResult("sU2TcDesc", dataLoan.sU2TcDesc);
            SetResult("sU1Tc", dataLoan.sU1Tc_rep);
            SetResult("sU1TcDesc", dataLoan.sU1TcDesc);
            SetResult("sOwnerTitleInsF", dataLoan.sOwnerTitleInsF_rep);

            SetResult("sU3GovRtcMb", dataLoan.sU3GovRtcMb_rep);
            SetResult("sU3GovRtcBaseT", dataLoan.sU3GovRtcBaseT);
            SetResult("sU3GovRtcPc", dataLoan.sU3GovRtcPc_rep);
            SetResult("sU3GovRtcDesc", dataLoan.sU3GovRtcDesc);
            SetResult("sU2GovRtcMb", dataLoan.sU2GovRtcMb_rep);
            SetResult("sU2GovRtcBaseT", dataLoan.sU2GovRtcBaseT);
            SetResult("sU2GovRtcPc", dataLoan.sU2GovRtcPc_rep);
            SetResult("sU2GovRtcDesc", dataLoan.sU2GovRtcDesc);
            SetResult("sU1GovRtcMb", dataLoan.sU1GovRtcMb_rep);
            SetResult("sU1GovRtcBaseT", dataLoan.sU1GovRtcBaseT);
            SetResult("sU1GovRtcPc", dataLoan.sU1GovRtcPc_rep);
            SetResult("sU1GovRtcDesc", dataLoan.sU1GovRtcDesc);
            SetResult("sStateRtcMb", dataLoan.sStateRtcMb_rep);
            SetResult("sStateRtcBaseT", dataLoan.sStateRtcBaseT);
            SetResult("sStateRtcPc", dataLoan.sStateRtcPc_rep);
            SetResult("sCountyRtcMb", dataLoan.sCountyRtcMb_rep);
            SetResult("sCountyRtcBaseT", dataLoan.sCountyRtcBaseT);
            SetResult("sCountyRtcPc", dataLoan.sCountyRtcPc_rep);
            SetResult("sRecFMb", dataLoan.sRecFMb_rep);
            SetResult("sRecBaseT", dataLoan.sRecBaseT);
            SetResult("sRecFPc", dataLoan.sRecFPc_rep);

            SetResult("sU5Sc", dataLoan.sU5Sc_rep);
            SetResult("sU5ScDesc", dataLoan.sU5ScDesc);
            SetResult("sU4Sc", dataLoan.sU4Sc_rep);
            SetResult("sU4ScDesc", dataLoan.sU4ScDesc);
            SetResult("sU3Sc", dataLoan.sU3Sc_rep);
            SetResult("sU3ScDesc", dataLoan.sU3ScDesc);
            SetResult("sU2Sc", dataLoan.sU2Sc_rep);
            SetResult("sU2ScDesc", dataLoan.sU2ScDesc);
            SetResult("sU1Sc", dataLoan.sU1Sc_rep);
            SetResult("sU1ScDesc", dataLoan.sU1ScDesc);
            
            SetResult("sApprFProps_ctrl_PaidTo_tb", dataLoan.sApprFPaidTo);
            SetResult("sCrFProps_ctrl_PaidTo_tb", dataLoan.sCrFPaidTo);
            SetResult("sTxServFProps_ctrl_PaidTo_tb", dataLoan.sTxServFPaidTo);
            SetResult("sFloodCertificationFProps_ctrl_PaidTo_tb", dataLoan.sFloodCertificationFPaidTo);
            SetResult("sInspectFProps_ctrl_PaidTo_tb", dataLoan.sInspectFPaidTo);
            SetResult("sProcFProps_ctrl_PaidTo_tb", dataLoan.sProcFPaidTo);
            SetResult("sUwFProps_ctrl_PaidTo_tb", dataLoan.sUwFPaidTo);
            SetResult("sWireFProps_ctrl_PaidTo_tb", dataLoan.sWireFPaidTo);
            SetResult("s800U1FProps_ctrl_PaidTo_tb", dataLoan.s800U1FPaidTo);
            SetResult("s800U2FProps_ctrl_PaidTo_tb", dataLoan.s800U2FPaidTo);
            SetResult("s800U3FProps_ctrl_PaidTo_tb", dataLoan.s800U3FPaidTo);
            SetResult("s800U4FProps_ctrl_PaidTo_tb", dataLoan.s800U4FPaidTo);
            SetResult("s800U5FProps_ctrl_PaidTo_tb", dataLoan.s800U5FPaidTo);
            SetResult("sEscrowFProps_ctrl_PaidTo_tb", dataLoan.sEscrowFTable);
            SetResult("sTitleInsFProps_ctrl_PaidTo_tb", dataLoan.sTitleInsFTable);
            SetResult("sOwnerTitleInsFProps_ctrl_PaidTo_tb", dataLoan.sOwnerTitleInsPaidTo);
            SetResult("sDocPrepFProps_ctrl_PaidTo_tb", dataLoan.sDocPrepFPaidTo);
            SetResult("sNotaryFProps_ctrl_PaidTo_tb", dataLoan.sNotaryFPaidTo);
            SetResult("sAttorneyFProps_ctrl_PaidTo_tb", dataLoan.sAttorneyFPaidTo);
            SetResult("sU1TcProps_ctrl_PaidTo_tb", dataLoan.sU1TcPaidTo);
            SetResult("sU2TcProps_ctrl_PaidTo_tb", dataLoan.sU2TcPaidTo);
            SetResult("sU3TcProps_ctrl_PaidTo_tb", dataLoan.sU3TcPaidTo);
            SetResult("sU4TcProps_ctrl_PaidTo_tb", dataLoan.sU4TcPaidTo);
            SetResult("sRecFProps_ctrl_PaidTo_tb", dataLoan.sRecFDesc);
            SetResult("sCountyRtcProps_ctrl_PaidTo_tb", dataLoan.sCountyRtcDesc);
            SetResult("sStateRtcProps_ctrl_PaidTo_tb", dataLoan.sStateRtcDesc);
            SetResult("sU1GovRtcProps_ctrl_PaidTo_tb", dataLoan.sU1GovRtcPaidTo);
            SetResult("sU2GovRtcProps_ctrl_PaidTo_tb", dataLoan.sU2GovRtcPaidTo);
            SetResult("sU3GovRtcProps_ctrl_PaidTo_tb", dataLoan.sU3GovRtcPaidTo);
            SetResult("sPestInspectFProps_ctrl_PaidTo_tb", dataLoan.sPestInspectPaidTo);
            SetResult("sU1ScProps_ctrl_PaidTo_tb", dataLoan.sU1ScPaidTo);
            SetResult("sU2ScProps_ctrl_PaidTo_tb", dataLoan.sU2ScPaidTo);
            SetResult("sU3ScProps_ctrl_PaidTo_tb", dataLoan.sU3ScPaidTo);
            SetResult("sU4ScProps_ctrl_PaidTo_tb", dataLoan.sU4ScPaidTo);
            SetResult("sU5ScProps_ctrl_PaidTo_tb", dataLoan.sU5ScPaidTo);
            SetResult("sHazInsPiaProps_ctrl_PaidTo_tb", dataLoan.sHazInsPiaPaidTo);
            SetResult("sMipPiaProps_ctrl_PaidTo_tb", dataLoan.sMipPiaPaidTo);
            SetResult("sVaFfProps_ctrl_PaidTo_tb", dataLoan.sVaFfPaidTo);

            SetResult("sProcFProps_ctrl_Paid_chk", dataLoan.sProcFPaid);
            SetResult("sCrFProps_ctrl_Paid_chk", dataLoan.sCrFPaid);
            SetResult("sApprFProps_ctrl_Paid_chk", dataLoan.sApprFPaid);

            SetResult("sIPiaDyLckd", dataLoan.sIPiaDyLckd);
            SetResult("sEstCloseD", dataLoan.sEstCloseD_rep);
            SetResult("sEstCloseDLckd", dataLoan.sEstCloseDLckd);
            // 9/19/2013 gf - opm 130112 1st pmt date is now calculated field
            SetResult("sSchedDueD1", dataLoan.sSchedDueD1_rep);
            SetResult("sSchedDueD1Lckd", dataLoan.sSchedDueD1Lckd);
            SetResult("sConsummationD", dataLoan.sConsummationD_rep);
            SetResult("sConsummationDLckd", dataLoan.sConsummationDLckd);
            SetResult("sFinalLAmt2", dataLoan.sFinalLAmt_rep);
            SetResult("sNoteIR2", dataLoan.sNoteIR_rep);
            SetResult("sGfeTotalEstimateSettlementCharge", dataLoan.sGfeTotalEstimateSettlementCharge_rep);

            SetResult("sGfeOriginationF", dataLoan.sGfeOriginationF_rep);
            SetResult("sLDiscnt2", dataLoan.sLDiscnt_rep);
            SetResult("sGfeAdjOriginationCharge", dataLoan.sGfeAdjOriginationCharge_rep);
            SetResult("sGfeRequiredServicesTotalFee", dataLoan.sGfeRequiredServicesTotalFee_rep);
            SetResult("sGfeLenderTitleTotalFee", dataLoan.sGfeLenderTitleTotalFee_rep);
            SetResult("sGfeOwnerTitleTotalFee", dataLoan.sGfeOwnerTitleTotalFee_rep);
            SetResult("sGfeServicesYouShopTotalFee", dataLoan.sGfeServicesYouShopTotalFee_rep);
            SetResult("sGfeGovtRecTotalFee", dataLoan.sGfeGovtRecTotalFee_rep);
            SetResult("sGfeTransferTaxTotalFee", dataLoan.sGfeTransferTaxTotalFee_rep);
            SetResult("sGfeInitialImpoundDeposit", dataLoan.sGfeInitialImpoundDeposit_rep);
            SetResult("sGfeDailyInterestTotalFee", dataLoan.sGfeDailyInterestTotalFee_rep);
            SetResult("sGfeHomeOwnerInsuranceTotalFee", dataLoan.sGfeHomeOwnerInsuranceTotalFee_rep);
            SetResult("sGfeTotalOtherSettlementServiceFee", dataLoan.sGfeTotalOtherSettlementServiceFee_rep);
            SetResult("sGfeTotalEstimateSettlementCharge4", dataLoan.sGfeTotalEstimateSettlementCharge_rep);
            SetResult("sGfeNotApplicableF", dataLoan.sGfeNotApplicableF_rep);

            SetResult("sGfeTradeOffLowerCCLoanAmt", dataLoan.sGfeTradeOffLowerCCLoanAmt_rep);
            SetResult("sGfeTradeOffLowerCCNoteIR", dataLoan.sGfeTradeOffLowerCCNoteIR_rep);
            SetResult("sGfeTradeOffLowerCCMPmtAndMInsDiff", dataLoan.sGfeTradeOffLowerCCMPmtAndMInsDiff_rep);
            SetResult("sGfeTradeOffLowerCCClosingCostDiff", dataLoan.sGfeTradeOffLowerCCClosingCostDiff_rep);
            SetResult("sGfeTradeOffLowerCCClosingCost", dataLoan.sGfeTradeOffLowerCCClosingCost_rep);
            SetResult("sGfeTradeOffLowerRateLoanAmt", dataLoan.sGfeTradeOffLowerRateLoanAmt_rep);
            SetResult("sGfeTradeOffLowerRateNoteIR", dataLoan.sGfeTradeOffLowerRateNoteIR_rep);
            SetResult("sGfeTradeOffLowerRateMPmtAndMInsDiff", dataLoan.sGfeTradeOffLowerRateMPmtAndMInsDiff_rep);
            SetResult("sGfeTradeOffLowerRateClosingCostDiff", dataLoan.sGfeTradeOffLowerRateClosingCostDiff_rep);
            SetResult("sGfeTradeOffLowerRateClosingCost", dataLoan.sGfeTradeOffLowerRateClosingCost_rep);
            SetResult("sGfeProThisMPmtAndMIns2", dataLoan.sGfeProThisMPmtAndMIns_rep);
            SetResult("sGfeTradeOffLowerCCMPmtAndMIns", dataLoan.sGfeTradeOffLowerCCMPmtAndMIns_rep);
            SetResult("sGfeTradeOffLowerRateMPmtAndMIns", dataLoan.sGfeTradeOffLowerRateMPmtAndMIns_rep);

            SetResult("GfeTilCompanyName2", gfeTil.CompanyName);
            SetResult("sFinalLAmt3", dataLoan.sFinalLAmt_rep);
            SetResult("sTerm2", dataLoan.sTerm_rep);
            SetResult("sNoteIR3", dataLoan.sNoteIR_rep);
            SetResult("sGfeProThisMPmtAndMIns", dataLoan.sGfeProThisMPmtAndMIns_rep);
            SetResult("sGfeRateLockPeriod2", dataLoan.sGfeRateLockPeriod_rep);
            SetResult("sGfeCanRateIncrease3", dataLoan.sGfeCanRateIncrease);
            SetResult("sGfeCanLoanBalanceIncrease2", dataLoan.sGfeCanLoanBalanceIncrease);
            SetResult("sGfeCanRateIncrease4", dataLoan.sGfeCanRateIncrease);
            SetResult("sGfeHavePpmtPenalty2", dataLoan.sGfeHavePpmtPenalty);
            SetResult("sGfeIsBalloon2", dataLoan.sGfeIsBalloon);
            SetResult("sGfeTotalEstimateSettlementCharge2", dataLoan.sGfeTotalEstimateSettlementCharge_rep);
            SetResult("sGfeTotalEstimateSettlementCharge3", dataLoan.sGfeTotalEstimateSettlementCharge_rep);
            
            E_TriState yes = E_TriState.Yes;
            
            SetResult("sGfeShoppingCartLoan1OriginatorName", dataLoan.sGfeShoppingCartLoan1OriginatorName);
            SetResult("sGfeShoppingCartLoan1LoanAmt", dataLoan.sGfeShoppingCartLoan1LoanAmt_rep);
            SetResult("sGfeShoppingCartLoan1LoanTerm", dataLoan.sGfeShoppingCartLoan1LoanTerm_rep);
            SetResult("sGfeShoppingCartLoan1NoteIR", dataLoan.sGfeShoppingCartLoan1NoteIR_rep);
            SetResult("sGfeShoppingCartLoan1InitialPmt", dataLoan.sGfeShoppingCartLoan1InitialPmt_rep);
            SetResult("sGfeShoppingCartLoan1RateLockPeriod", dataLoan.sGfeShoppingCartLoan1RateLockPeriod_rep);
            SetResult("sGfeShoppingCartLoan1CanRateIncreaseTri", dataLoan.sGfeShoppingCartLoan1CanRateIncreaseTri == yes);
            SetResult("sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri", dataLoan.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri == yes);
            SetResult("sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri", dataLoan.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri == yes);
            SetResult("sGfeShoppingCartLoan1HavePpmtPenaltyTri", dataLoan.sGfeShoppingCartLoan1HavePpmtPenaltyTri == yes);
            SetResult("sGfeShoppingCartLoan1IsBalloonTri", dataLoan.sGfeShoppingCartLoan1IsBalloonTri == yes);
            SetResult("sGfeShoppingCartLoan1TotalClosingCost", dataLoan.sGfeShoppingCartLoan1TotalClosingCost_rep);

            SetResult("sGfeShoppingCartLoan2OriginatorName", dataLoan.sGfeShoppingCartLoan2OriginatorName);
            SetResult("sGfeShoppingCartLoan2LoanAmt", dataLoan.sGfeShoppingCartLoan2LoanAmt_rep);
            SetResult("sGfeShoppingCartLoan2LoanTerm", dataLoan.sGfeShoppingCartLoan2LoanTerm_rep);
            SetResult("sGfeShoppingCartLoan2NoteIR", dataLoan.sGfeShoppingCartLoan2NoteIR_rep);
            SetResult("sGfeShoppingCartLoan2InitialPmt", dataLoan.sGfeShoppingCartLoan2InitialPmt_rep);
            SetResult("sGfeShoppingCartLoan2RateLockPeriod", dataLoan.sGfeShoppingCartLoan2RateLockPeriod_rep);
            SetResult("sGfeShoppingCartLoan2CanRateIncreaseTri", dataLoan.sGfeShoppingCartLoan2CanRateIncreaseTri == yes);
            SetResult("sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri", dataLoan.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri == yes);
            SetResult("sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri", dataLoan.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri == yes);
            SetResult("sGfeShoppingCartLoan2HavePpmtPenaltyTri", dataLoan.sGfeShoppingCartLoan2HavePpmtPenaltyTri == yes);
            SetResult("sGfeShoppingCartLoan2IsBalloonTri", dataLoan.sGfeShoppingCartLoan2IsBalloonTri == yes);
            SetResult("sGfeShoppingCartLoan2TotalClosingCost", dataLoan.sGfeShoppingCartLoan2TotalClosingCost_rep);

            SetResult("sGfeShoppingCartLoan3OriginatorName", dataLoan.sGfeShoppingCartLoan3OriginatorName);
            SetResult("sGfeShoppingCartLoan3LoanAmt", dataLoan.sGfeShoppingCartLoan3LoanAmt_rep);
            SetResult("sGfeShoppingCartLoan3LoanTerm", dataLoan.sGfeShoppingCartLoan3LoanTerm_rep);
            SetResult("sGfeShoppingCartLoan3NoteIR", dataLoan.sGfeShoppingCartLoan3NoteIR_rep);
            SetResult("sGfeShoppingCartLoan3InitialPmt", dataLoan.sGfeShoppingCartLoan3InitialPmt_rep);
            SetResult("sGfeShoppingCartLoan3RateLockPeriod", dataLoan.sGfeShoppingCartLoan3RateLockPeriod_rep);
            SetResult("sGfeShoppingCartLoan3CanRateIncreaseTri", dataLoan.sGfeShoppingCartLoan3CanRateIncreaseTri == yes);
            SetResult("sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri", dataLoan.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri == yes);
            SetResult("sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri", dataLoan.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri == yes);
            SetResult("sGfeShoppingCartLoan3HavePpmtPenaltyTri", dataLoan.sGfeShoppingCartLoan3HavePpmtPenaltyTri == yes);
            SetResult("sGfeShoppingCartLoan3IsBalloonTri", dataLoan.sGfeShoppingCartLoan3IsBalloonTri == yes);
            SetResult("sGfeShoppingCartLoan3TotalClosingCost", dataLoan.sGfeShoppingCartLoan3TotalClosingCost_rep);

            SetResult("sLpTemplateNm", dataLoan.sLpTemplateNm);
            SetResult("sCcTemplateNm", dataLoan.sCcTemplateNm);


            SetResult("sGfeOriginatorCompF", dataLoan.sGfeOriginatorCompF_rep);
            SetResult("sOriginatorCompensationPaymentSourceT", dataLoan.sOriginatorCompensationPaymentSourceT);

            SetResult("sGfeOriginatorCompFPc", dataLoan.sGfeOriginatorCompFPc_rep);
            SetResult("sGfeOriginatorCompFMb", dataLoan.sGfeOriginatorCompFMb_rep);
            SetResult("sGfeOriginatorCompFBaseT", dataLoan.sGfeOriginatorCompFBaseT);

            SetResult("sGfeIsTPOTransaction", dataLoan.sGfeIsTPOTransaction);
            SetResult("sIsItemizeBrokerCommissionOnIFW", dataLoan.sIsItemizeBrokerCommissionOnIFW);
            SetResult("sGfeLenderCreditF", dataLoan.sGfeLenderCreditF_rep);
            SetResult("sGfeLenderCreditFPc", dataLoan.sGfeLenderCreditFPc_rep);
            SetResult("sGfeDiscountPointF", dataLoan.sGfeDiscountPointF_rep);
            SetResult("sGfeDiscountPointFPc", dataLoan.sGfeDiscountPointFPc_rep);
            SetResult("sGfeCreditLenderPaidItemT", dataLoan.sGfeCreditLenderPaidItemT);
            SetResult("sGfeCreditLenderPaidItemF", dataLoan.sGfeCreditLenderPaidItemF_rep);

            SetResult("sGfeInitialDisclosureD", dataLoan.sGfeInitialDisclosureD_rep);
            SetResult("sGfeRedisclosureD", dataLoan.sGfeRedisclosureD_rep);

            // 800 - Items payable in connection with loan
            InitItemProps("sLOrigFProps", dataLoan.sLOrigFProps);
            InitItemProps("sGfeOriginatorCompFProps", dataLoan.sGfeOriginatorCompFProps);
            //InitItemProps("sLDiscntProps", dataLoan.sLDiscntProps);
            InitItemProps("sGfeLenderCreditFProps", dataLoan.sGfeLenderCreditFProps);
            InitItemProps("sGfeDiscountPointFProps", dataLoan.sGfeDiscountPointFProps);
            
            InitItemProps("sApprFProps", dataLoan.sApprFProps);
            InitItemProps("sCrFProps", dataLoan.sCrFProps);
            InitItemProps("sInspectFProps", dataLoan.sInspectFProps);
            InitItemProps("sMBrokFProps", dataLoan.sMBrokFProps);
            InitItemProps("sTxServFProps", dataLoan.sTxServFProps);
            InitItemProps("sFloodCertificationFProps", dataLoan.sFloodCertificationFProps);
            InitItemProps("sProcFProps", dataLoan.sProcFProps);
            InitItemProps("sUwFProps", dataLoan.sUwFProps);
            InitItemProps("sWireFProps", dataLoan.sWireFProps);
            InitItemProps("s800U1FProps", dataLoan.s800U1FProps);
            InitItemProps("s800U2FProps", dataLoan.s800U2FProps);
            InitItemProps("s800U3FProps", dataLoan.s800U3FProps);
            InitItemProps("s800U4FProps", dataLoan.s800U4FProps);
            InitItemProps("s800U5FProps", dataLoan.s800U5FProps);

            // 900 - Items Required by lender to be paid in advance 
            InitItemProps("sIPiaProps", dataLoan.sIPiaProps);
            InitItemProps("sMipPiaProps", dataLoan.sMipPiaProps);
            InitItemProps("sHazInsPiaProps", dataLoan.sHazInsPiaProps);
            InitItemProps("s904PiaProps", dataLoan.s904PiaProps);
            InitItemProps("sVaFfProps", dataLoan.sVaFfProps);
            InitItemProps("s900U1PiaProps", dataLoan.s900U1PiaProps);

            // 1000 - Reserves Deposited with Lender
            InitItemProps("sHazInsRsrvProps", dataLoan.sHazInsRsrvProps);
            InitItemProps("sMInsRsrvProps", dataLoan.sMInsRsrvProps);
            InitItemProps("sSchoolTxRsrvProps", dataLoan.sSchoolTxRsrvProps);
            InitItemProps("sRealETxRsrvProps", dataLoan.sRealETxRsrvProps);
            InitItemProps("sFloodInsRsrvProps", dataLoan.sFloodInsRsrvProps);
            InitItemProps("s1006RsrvProps", dataLoan.s1006RsrvProps);
            InitItemProps("s1007RsrvProps", dataLoan.s1007RsrvProps);
            InitItemProps("sU3RsrvProps", dataLoan.sU3RsrvProps);
            InitItemProps("sU4RsrvProps", dataLoan.sU4RsrvProps);
            InitItemProps("sAggregateAdjRsrvProps", dataLoan.sAggregateAdjRsrvProps);

            // 1100 - Title Charges
            InitItemProps("sEscrowFProps", dataLoan.sEscrowFProps);
            InitItemProps("sOwnerTitleInsFProps", dataLoan.sOwnerTitleInsProps);
            InitItemProps("sDocPrepFProps", dataLoan.sDocPrepFProps);
            InitItemProps("sNotaryFProps", dataLoan.sNotaryFProps);
            InitItemProps("sAttorneyFProps", dataLoan.sAttorneyFProps);
            InitItemProps("sTitleInsFProps", dataLoan.sTitleInsFProps);
            InitItemProps("sU1TcProps", dataLoan.sU1TcProps);
            InitItemProps("sU2TcProps", dataLoan.sU2TcProps);
            InitItemProps("sU3TcProps", dataLoan.sU3TcProps);
            InitItemProps("sU4TcProps", dataLoan.sU4TcProps);

            // 1200 - Government Recording & Transfer Charges
            InitItemProps("sRecFProps", dataLoan.sRecFProps);
            InitItemProps("sCountyRtcProps", dataLoan.sCountyRtcProps);
            InitItemProps("sStateRtcProps", dataLoan.sStateRtcProps);
            InitItemProps("sU1GovRtcProps", dataLoan.sU1GovRtcProps);
            InitItemProps("sU2GovRtcProps", dataLoan.sU2GovRtcProps);
            InitItemProps("sU3GovRtcProps", dataLoan.sU3GovRtcProps);

            // 1300 - Additional Settlement Charges
            InitItemProps("sPestInspectFProps", dataLoan.sPestInspectFProps);
            InitItemProps("sU1ScProps", dataLoan.sU1ScProps);
            InitItemProps("sU2ScProps", dataLoan.sU2ScProps);
            InitItemProps("sU3ScProps", dataLoan.sU3ScProps);
            InitItemProps("sU4ScProps", dataLoan.sU4ScProps);
            InitItemProps("sU5ScProps", dataLoan.sU5ScProps);

            if (!IsArchivePage)
            {
                // OPM 170146 - Footer, Lender and Seller paid totals tab
                SetResult("Gfe2010Footer_sGfeTotalFundByLender", dataLoan.sGfeTotalFundByLender_rep);
                SetResult("Gfe2010Footer_sGfeTotalFundBySeller", dataLoan.sGfeTotalFundBySeller_rep);

                // OPM 170146 - Footer, Details of Transaction tab
                SetResult("Gfe2010Footer_dot_sAltCostLckd", dataLoan.sAltCostLckd);
                SetResult("Gfe2010Footer_dot_sAltCost", dataLoan.sAltCost_rep);
                SetResult("Gfe2010Footer_dot_sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
                SetResult("Gfe2010Footer_dot_sFfUfmip1003Lckd", dataLoan.sFfUfmip1003Lckd);
                SetResult("Gfe2010Footer_dot_sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep);
                SetResult("Gfe2010Footer_dot_sFinalLAmt", dataLoan.sFinalLAmt_rep);
                SetResult("Gfe2010Footer_dot_sLAmt1003", dataLoan.sLAmt1003_rep);
                SetResult("Gfe2010Footer_dot_sLDiscnt1003", dataLoan.sLDiscnt1003_rep);
                SetResult("Gfe2010Footer_dot_sLDiscnt1003Lckd", dataLoan.sLDiscnt1003Lckd);
                SetResult("Gfe2010Footer_dot_sLandCost", dataLoan.sLandCost_rep);
                SetResult("Gfe2010Footer_dot_sOCredit1Amt", dataLoan.sOCredit1Amt_rep);
                SetResult("Gfe2010Footer_dot_sOCredit1Desc", dataLoan.sOCredit1Desc);
                SetResult("Gfe2010Footer_dot_sOCredit1Lckd", dataLoan.sOCredit1Lckd);
                SetResult("Gfe2010Footer_dot_sOCredit2Amt", dataLoan.sOCredit2Amt_rep);
                SetResult("Gfe2010Footer_dot_sOCredit2Desc", dataLoan.sOCredit2Desc);
                SetResult("Gfe2010Footer_dot_sOCredit3Amt", dataLoan.sOCredit3Amt_rep);
                SetResult("Gfe2010Footer_dot_sOCredit3Desc", dataLoan.sOCredit3Desc);
                SetResult("Gfe2010Footer_dot_sOCredit4Amt", dataLoan.sOCredit4Amt_rep);
                SetResult("Gfe2010Footer_dot_sOCredit4Desc", dataLoan.sOCredit4Desc);
                SetResult("Gfe2010Footer_dot_sOCredit5Amt", dataLoan.sOCredit5Amt_rep);
                SetResult("Gfe2010Footer_dot_sONewFinBal", dataLoan.sONewFinBal_rep);
                SetResult("Gfe2010Footer_dot_sPurchPrice", dataLoan.sPurchPrice_rep);
                SetResult("Gfe2010Footer_dot_sRefPdOffAmt1003", dataLoan.sRefPdOffAmt1003_rep);
                SetResult("Gfe2010Footer_dot_sRefPdOffAmt1003Lckd", dataLoan.sRefPdOffAmt1003Lckd);
                SetResult("Gfe2010Footer_dot_sTotCcPbs", dataLoan.sTotCcPbs_rep);
                SetResult("Gfe2010Footer_dot_sTotCcPbsLocked", dataLoan.sTotCcPbsLocked);
                SetResult("Gfe2010Footer_dot_sTotEstCc1003Lckd", dataLoan.sTotEstCc1003Lckd);
                SetResult("Gfe2010Footer_dot_sTotEstCcNoDiscnt1003", dataLoan.sTotEstCcNoDiscnt1003_rep);
                SetResult("Gfe2010Footer_dot_sTotEstPp1003", dataLoan.sTotEstPp1003_rep);
                SetResult("Gfe2010Footer_dot_sTotEstPp1003Lckd", dataLoan.sTotEstPp1003Lckd);
                SetResult("Gfe2010Footer_dot_sTotTransC", dataLoan.sTotTransC_rep);
                SetResult("Gfe2010Footer_dot_sTransNetCash", dataLoan.sTransNetCash_rep);
                SetResult("Gfe2010Footer_dot_sTransNetCashLckd", dataLoan.sTransNetCashLckd);
                SetResult("Gfe2010Footer_dot_sONewFinCc", dataLoan.sONewFinCc_rep);
                SetResult("Gfe2010Footer_dot_sONewFinCc2", dataLoan.sONewFinCc_rep);
                SetResult("Gfe2010Footer_dot_sLAmtCalc", dataLoan.sLAmtCalc_rep);
                SetResult("Gfe2010Footer_dot_sLAmtLckd", dataLoan.sLAmtLckd);
                SetResult("Gfe2010Footer_dot_sTotEstCcNoDiscnt", dataLoan.sTotEstCcNoDiscnt_rep);
                SetResult("Gfe2010Footer_dot_sTotEstPp", dataLoan.sTotEstPp_rep);
                SetResult("Gfe2010Footer_dot_sTotalBorrowerPaidProrations", dataLoan.sTotalBorrowerPaidProrations_rep);
            }
        }

        private int RetrieveItemProps902905(string name, int dflpValueSrc)
        {
            bool apr = GetBool(name + "_ctrl_Apr_chk", false);
            bool fhaAllow = GetBool(name + "_ctrl_Fha_chk", false);
            int payer = GetInt(name + "_ctrl_PdByT_dd", 0);
            bool poc = GetBool(name + "_ctrl_Poc_chk");

            bool trdPty = GetString(name + "_ctrl_TrdPty_hdn", "N") == "Y";
            bool aff = GetString(name + "_ctrl_Aff_hdn", "N") == "Y";
            bool qmWarn = GetString(name + "_ctrl_QmWarn_hdn", "N") == "Y";

            bool dflp = LosConvert.GfeItemProps_Dflp(dflpValueSrc);
            return LosConvert.GfeItemProps_Pack(apr, false, payer, fhaAllow, poc, dflp, trdPty, aff, qmWarn);
        }

        // 2/26/14 gf - opm 150695, Since it is now possible to combine the GFE/SC pages, we
        // maintain some of the settings where they may possibly be set on the SC page.
        // POC only needs to be preserved when it isn't shown. DFLP will need to be preserved as
        // long as POC is false.
        private int RetrieveItemProps(string name, bool hasPOC, bool hasB, bool hasGBF, bool hasBF, bool hasBorr, int originalProps)
        {
            bool apr = GetBool(name + "_ctrl_Apr_chk", false);
            bool fhaAllow = GetBool(name + "_ctrl_Fha_chk", false);
            bool toBr = hasB ? GetBool(name + "_ctrl_ToBrok_chk") : false;
            int payer = GetInt(name + "_ctrl_PdByT_dd", 0);
            bool poc = hasPOC ? GetBool(name + "_ctrl_Poc_chk") : LosConvert.GfeItemProps_Poc(originalProps);

            bool gbf = false;
            // bool gbf = hasGBF ? GetBool(name + "_ctrl_GBF_chk") : false;

            bool bf = hasBF ? GetBool(name + "_ctrl_BF_chk") : false;

            bool borr = hasBorr ? GetBool(name + "_ctrl_Borr_chk") : false;

            bool dflp = false;
            if (!poc)
            {
                dflp = LosConvert.GfeItemProps_Dflp(originalProps);
            }
            bool trdPty = GetString(name + "_ctrl_TrdPty_hdn", "N") == "Y";
            bool aff = GetString(name + "_ctrl_Aff_hdn", "N") == "Y";
            bool qmWarn = GetString(name + "_ctrl_QmWarn_hdn", "N") == "Y";

            return LosConvert.GfeItemProps_Pack(apr, toBr, payer, fhaAllow, poc, dflp, gbf, bf, borr, trdPty, aff, qmWarn);
        }

		private int RetrieveItemProps(string name, bool hasPOC, bool hasB, bool hasGBF, bool hasBF, int originalProps)
		{
            return RetrieveItemProps(name, hasPOC, hasB, hasGBF, hasBF, false /*hasBorr*/, originalProps);
		}
      

        // OPM 126764.  sGfeOriginatorCompFProps is also featured on the Settlement charges page.
        // Use this to make sure we respect existing values for that one.
        // Expect this to be temporary once we have GFE versioning.
        //RetrieveItemProps("sGfeOriginatorCompFProps", false, false, true, true);
        private int RetrieveItemProps(string name, int originalProps)
        {
            bool apr = GetBool(name + "_ctrl_Apr_chk", false);
            bool fhaAllow = GetBool(name + "_ctrl_Fha_chk", false);
            int payer = GetInt(name + "_ctrl_PdByT_dd", 0);
            bool bf = GetBool(name + "_ctrl_BF_chk");

            bool trdPty = GetString(name + "_ctrl_TrdPty_hdn", "N") == "Y";
            bool aff = GetString(name + "_ctrl_Aff_hdn", "N") == "Y";
            bool qmWarn = GetString(name + "_ctrl_QmWarn_hdn", "N") == "Y";
            bool toBr = GetBool(name + "_ctrl_ToBrok_chk");

            bool poc = LosConvert.GfeItemProps_Poc(originalProps);
            bool gbf = LosConvert.GfeItemProps_GBF(originalProps);
            bool borr = LosConvert.GfeItemProps_Borr(originalProps);
            bool dflp = LosConvert.GfeItemProps_Dflp(originalProps);

            return LosConvert.GfeItemProps_Pack(apr, toBr, payer, fhaAllow, poc, dflp, gbf, bf, borr, trdPty, aff, qmWarn);
        }

		private void InitItemProps( string name, int props )
		{
			SetResult(name + "_ctrl_Apr_chk", LosConvert.GfeItemProps_Apr(props));
            SetResult(name + "_ctrl_Fha_chk", LosConvert.GfeItemProps_FhaAllow(props));
            SetResult(name + "_ctrl_ToBrok_chk", LosConvert.GfeItemProps_ToBr(props));
            SetResult(name + "_ctrl_PdByT_dd", LosConvert.GfeItemProps_Payer(props));
            SetResult(name + "_ctrl_Borr_chk", LosConvert.GfeItemProps_Borr(props));
            SetResult(name + "_ctrl_Poc_chk", LosConvert.GfeItemProps_Poc(props));
            SetResult(name + "_ctrl_BF_chk", LosConvert.GfeItemProps_BF(props));
            // SetResult(name + "_ctrl_GBF_chk", LosConvert.GfeItemProps_GBF(props));
            SetResult(name + "_ctrl_TrdPty_hdn", LosConvert.GfeItemProps_PaidToThirdParty(props) ? "Y" : "N");
            SetResult(name + "_ctrl_Aff_hdn", LosConvert.GfeItemProps_ThisPartyIsAffiliate(props) ? "Y" : "N");
            SetResult(name + "_ctrl_QmWarn_hdn", LosConvert.GfeItemProps_ShowQmWarning(props) ? "Y" : "N");
		}
		private void LoadData()
		{
            CPageData dataLoan = CreatePageData();

			dataLoan.InitLoad();
			LoadData(dataLoan);
		}

        private void CalculateData() 
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();
            BindData(dataLoan, true);
            LoadData(dataLoan);
        }
        private CPageData CreatePageData()
        {
            Guid loanID = GetGuid("loanid");
            if (Broker.IsGFEandCoCVersioningEnabled && !IsArchivePage)
            {
                return new CPageData(loanID, CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(GoodFaithEstimate2010Service)).Union(
                    CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release));
            }
            else
            {
                return CPageData.CreateUsingSmartDependency(loanID, typeof(GoodFaithEstimate2010Service));
            }

        }

        private void AgentPicked()
        {
            Guid RecordID = GetGuid("RecordID");
            E_LegacyGfeFieldT LegacyType = (E_LegacyGfeFieldT)GetInt("LegacyType");

            CPageData dataLoan = CreatePageData();
			dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Get Agent from Official Contacts list
            CAgentFields fields = dataLoan.GetAgentFields(RecordID);
            
            // Set Third Party and Affiliates bits (Note. Aff == false if TP == false)
            bool isThirdParty = fields.BrokerLevelAgentID != Guid.Empty; // Is Third party if in Rolodex (ie. BrokerLevelAgentID not Guid.Empty)
            bool isAffiliate = isThirdParty && (fields.IsLenderAffiliate || IsAgentOriginatingCompanyAffiliate(fields, dataLoan));
            bool isQmWarn = !isThirdParty && fields.EmployeeId == Guid.Empty; // Set QM Warning bit if agent not in rolodex (ie. not TP) and is not LQB/PML user (ie. EmployeeId is Guid.Empty).

            SetResult("TP", isThirdParty ? "Y" : "N");
            SetResult("AFF", isAffiliate ? "Y" : "N");
            SetResult("QmWarn", isQmWarn ? "Y" : "N"); 

            // OPM 209868 - Link record to closing cost fee beneficiary on migrated loans
            if (dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy)
            {
                BorrowerClosingCostFee fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(LegacyType, BrokerUserPrincipal.CurrentPrincipal.BrokerId, true);
                fee.BeneficiaryAgentId = RecordID;
                dataLoan.Save();
            }
        }


        /// Separate function from AgentPicked() to aviod DB calls by short-cutting logic 
        /// if Agent is marked as lender affiliate (fields.IsLenderAffiliate)
        private bool IsAgentOriginatingCompanyAffiliate(CAgentFields fields, CPageData dataLoan)
        {
            Guid loanID = GetGuid("loanid");
            PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerByLoanId(Broker.BrokerID, loanID);
            if (pmlBroker != null)
            {
                return pmlBroker.GetAffiliates().Exists(r => r.ID == fields.BrokerLevelAgentID);
            }

            return false;
        }

        // OPM 209868 - Clear closing cost fee beneficiary on migrated loans
        private void ClearAgent()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Skip legacy loans
            if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
            {
                return;
            }

            //string Hudline = GetString("Hudline");
            E_LegacyGfeFieldT LegacyType = (E_LegacyGfeFieldT)GetInt("LegacyType");

            BorrowerClosingCostFee fee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(LegacyType, BrokerUserPrincipal.CurrentPrincipal.BrokerId, true);
            fee.ClearBeneficiary();
            dataLoan.Save();
        }
	}
}
