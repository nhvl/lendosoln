using System;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos.Forms
{

	public partial class GoodFaithEstimateService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
		protected override void Process(string methodName) 
		{
            
			switch (methodName) 
			{
				case "SaveData":
					SaveData();
					break;
				case "CalculateData":
					CalculateData();
					break;
				case "ChecksBrokComp1Pc":
					ChecksBrokComp1Pc();
					break;
			}
		}

		private void SaveData() 
		{
			Guid loanID = GetGuid("loanid");
            bool byPassBgCalcForGfeAsDefault = GetString("ByPassBgCalcForGfeAsDefault", "0") == "1";
            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
			CPageData dataLoan = new CGfeData(loanID);
			dataLoan.InitSave(sFileVersion);
			BindData(dataLoan, true);
			dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
			LoadData(dataLoan);

			BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            SqlParameter[] parameters = {
                                            new SqlParameter("@ByPassBgCalcForGfeAsDefault", byPassBgCalcForGfeAsDefault)
                                            , new SqlParameter("@UserId", principal.UserId)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "SetPassBgCalcForGfeAsDefaultByUserId", 3, parameters); 
        }

		private bool BindData(CPageData dataLoan, bool isBindAll) 
		{
			bool alertUser = false;
			dataLoan.sEstCloseD_rep = GetString("sEstCloseD") ;
			dataLoan.sDue_rep = GetString("sDue") ;
			dataLoan.sTerm_rep = GetString("sTerm") ;
			dataLoan.sSchedDueD1_rep = GetString("sSchedDueD1") ;
			dataLoan.sNoteIR_rep = GetString("sNoteIR") ;
			dataLoan.sDaysInYr_rep = GetString("sDaysInYr") ;

			if (isBindAll) 
			{
				IPreparerFields gfeTil = dataLoan.GetPreparerOfForm( E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew );
				gfeTil.PrepareDate_rep = GetString("GfeTilPrepareDate");
				gfeTil.CompanyName = GetString("GfeTilCompanyName");
				gfeTil.StreetAddr = GetString("GfeTilStreetAddr");
				gfeTil.City = GetString("GfeTilCity");
				gfeTil.State = GetString("GfeTilState");
				gfeTil.Zip = GetString("GfeTilZip");
				gfeTil.PhoneOfCompany = GetString("GfeTilPhoneOfCompany");
                gfeTil.AgentRoleT = (E_AgentRoleT)GetInt("CFM_AgentRoleT");
                gfeTil.IsLocked = GetBool("CFM_IsLocked");
				gfeTil.Update();

			}

			dataLoan.sTotCcPbsLocked = GetBool("sTotCcPbsLocked") ;
			dataLoan.sTotCcPbs_rep = GetString("sTotCcPbs") ;
			dataLoan.sPurchPrice_rep = GetString("sPurchPrice") ;
			dataLoan.s800U5F_rep = GetString("s800U5F") ;
			if (isBindAll) 
			{
				dataLoan.s800U5FDesc = GetString("s800U5FDesc") ;
				dataLoan.s800U5FCode = GetString("s800U5FCode") ;
			}
			dataLoan.s800U4F_rep = GetString("s800U4F") ;
			if (isBindAll) 
			{
				dataLoan.s800U4FDesc = GetString("s800U4FDesc") ;
				dataLoan.s800U4FCode = GetString("s800U4FCode") ;
			}
			dataLoan.s800U3F_rep = GetString("s800U3F") ;
			if (isBindAll) 
			{
				dataLoan.s800U3FDesc = GetString("s800U3FDesc") ;
				dataLoan.s800U3FCode = GetString("s800U3FCode") ;
			}
			dataLoan.s800U2F_rep = GetString("s800U2F") ;
			if (isBindAll) 
			{
				dataLoan.s800U2FDesc = GetString("s800U2FDesc") ;
				dataLoan.s800U2FCode = GetString("s800U2FCode") ;
			}
			dataLoan.s800U1F_rep = GetString("s800U1F") ;
			if (isBindAll) 
			{
				dataLoan.s800U1FDesc = GetString("s800U1FDesc") ;
				dataLoan.s800U1FCode = GetString("s800U1FCode") ;
			}
			dataLoan.sWireF_rep = GetString("sWireF") ;
			dataLoan.sUwF_rep = GetString("sUwF") ;
			dataLoan.sProcF_rep = GetString("sProcF") ;
			dataLoan.sProcFPaid = GetBool("sProcFPaid") ;
			dataLoan.sTxServF_rep = GetString("sTxServF") ;
			dataLoan.sMBrokFMb_rep = GetString("sMBrokFMb") ;
			dataLoan.sMBrokFPc_rep = GetString("sMBrokFPc") ;
			dataLoan.sInspectF_rep = GetString("sInspectF") ;
			dataLoan.sCrF_rep = GetString("sCrF") ;
			dataLoan.sCrFPaid = GetBool("sCrFPaid") ;
			dataLoan.sApprF_rep = GetString("sApprF") ;
			dataLoan.sApprFPaid = GetBool("sApprFPaid") ;
			dataLoan.sLDiscntFMb_rep = GetString("sLDiscntFMb") ;
			dataLoan.sLDiscntPc_rep = GetString("sLDiscntPc") ;
			dataLoan.sLOrigFMb_rep = GetString("sLOrigFMb") ;
			dataLoan.sLOrigFPc_rep = GetString("sLOrigFPc") ;
			dataLoan.s900U1Pia_rep = GetString("s900U1Pia") ;
			if (isBindAll) 
			{
				dataLoan.s900U1PiaDesc = GetString("s900U1PiaDesc") ;
				dataLoan.s900U1PiaCode = GetString("s900U1PiaCode") ;
				dataLoan.s904PiaDesc = GetString("s904PiaDesc") ;

			}
			dataLoan.s904Pia_rep = GetString("s904Pia") ;
			dataLoan.sHazInsPiaMon_rep = GetString("sHazInsPiaMon") ;
            dataLoan.sProHazInsT = (E_PercentBaseT)GetInt("sProHazInsT");
			dataLoan.sProHazInsR_rep = GetString("sProHazInsR") ;
			dataLoan.sProHazInsMb_rep = GetString("sProHazInsMb") ;
            dataLoan.sIPiaDy_rep = GetString("sIPiaDy") ;
			dataLoan.sIPerDayLckd = GetBool("sIPerDayLckd");
			dataLoan.sIPerDay_rep = GetString("sIPerDay");
			dataLoan.sAggregateAdjRsrv_rep = GetString("sAggregateAdjRsrv") ;
			dataLoan.sAggregateAdjRsrvLckd = GetBool("sAggregateAdjRsrvLckd");
			dataLoan.s1007ProHExp_rep = GetString("s1007ProHExp") ;
			dataLoan.s1007RsrvMon_rep = GetString("s1007RsrvMon") ;
			if (isBindAll) 
			{
				dataLoan.s1007ProHExpDesc = GetString("s1007ProHExpDesc") ;
			}
			dataLoan.s1006ProHExp_rep = GetString("s1006ProHExp") ;
			dataLoan.s1006RsrvMon_rep = GetString("s1006RsrvMon") ;
			if (isBindAll) 
			{
				dataLoan.s1006ProHExpDesc = GetString("s1006ProHExpDesc") ;
			}
			dataLoan.sProFloodIns_rep = GetString("sProFloodIns") ;
			dataLoan.sFloodInsRsrvMon_rep = GetString("sFloodInsRsrvMon") ;
			dataLoan.sProRealETxMb_rep = GetString("sProRealETxMb") ;
            dataLoan.sProRealETxT = (E_PercentBaseT)GetInt("sProRealETxT");
			dataLoan.sProRealETxR_rep = GetString("sProRealETxR");
			dataLoan.sRealETxRsrvMon_rep = GetString("sRealETxRsrvMon") ;
			dataLoan.sProSchoolTx_rep = GetString("sProSchoolTx") ;
			dataLoan.sSchoolTxRsrvMon_rep = GetString("sSchoolTxRsrvMon") ;
			dataLoan.sMInsRsrvMon_rep = GetString("sMInsRsrvMon") ;
			dataLoan.sHazInsRsrvMon_rep = GetString("sHazInsRsrvMon") ;
			dataLoan.sU4Tc_rep = GetString("sU4Tc") ;
			if (isBindAll) 
			{
				dataLoan.sU4TcDesc = GetString("sU4TcDesc") ;
				dataLoan.sU4TcCode = GetString("sU4TcCode") ;
			}
			dataLoan.sU3Tc_rep = GetString("sU3Tc") ;
			if (isBindAll) 
			{
				dataLoan.sU3TcDesc = GetString("sU3TcDesc") ;
				dataLoan.sU3TcCode = GetString("sU3TcCode") ;
			}
			dataLoan.sU2Tc_rep = GetString("sU2Tc") ;
			if (isBindAll) 
			{
				dataLoan.sU2TcDesc = GetString("sU2TcDesc") ;
				dataLoan.sU2TcCode = GetString("sU2TcCode") ;
			}
			dataLoan.sU1Tc_rep = GetString("sU1Tc") ;
			if (isBindAll) 
			{
				dataLoan.sU1TcDesc = GetString("sU1TcDesc") ;
				dataLoan.sU1TcCode = GetString("sU1TcCode") ;
			}
			dataLoan.sTitleInsF_rep = GetString("sTitleInsF") ;
			dataLoan.sTitleInsFTable = GetString("sTitleInsFTable") ;
			dataLoan.sAttorneyF_rep = GetString("sAttorneyF") ;
			dataLoan.sNotaryF_rep = GetString("sNotaryF") ;
			dataLoan.sDocPrepF_rep = GetString("sDocPrepF") ;
			dataLoan.sEscrowF_rep = GetString("sEscrowF") ;
			dataLoan.sEscrowFTable = GetString("sEscrowFTable") ;
			dataLoan.sU3GovRtcMb_rep = GetString("sU3GovRtcMb") ;
            dataLoan.sU3GovRtcBaseT = (E_PercentBaseT)GetInt("sU3GovRtcBaseT");
			dataLoan.sU3GovRtcPc_rep = GetString("sU3GovRtcPc") ;
			if (isBindAll) 
			{
				dataLoan.sU3GovRtcDesc = GetString("sU3GovRtcDesc") ;
				dataLoan.sU3GovRtcCode = GetString("sU3GovRtcCode") ;
			}
			dataLoan.sU2GovRtcMb_rep = GetString("sU2GovRtcMb") ;
            dataLoan.sU2GovRtcBaseT = (E_PercentBaseT)GetInt("sU2GovRtcBaseT");
			dataLoan.sU2GovRtcPc_rep = GetString("sU2GovRtcPc") ;
			if (isBindAll) 
			{
				dataLoan.sU2GovRtcDesc = GetString("sU2GovRtcDesc") ;
				dataLoan.sU2GovRtcCode = GetString("sU2GovRtcCode") ;
			}
			dataLoan.sU1GovRtcMb_rep = GetString("sU1GovRtcMb") ;
            dataLoan.sU1GovRtcBaseT = (E_PercentBaseT)GetInt("sU1GovRtcBaseT");
			dataLoan.sU1GovRtcPc_rep = GetString("sU1GovRtcPc") ;
			if (isBindAll) 
			{
				dataLoan.sU1GovRtcDesc = GetString("sU1GovRtcDesc") ;
				dataLoan.sU1GovRtcCode = GetString("sU1GovRtcCode") ;
			}
			dataLoan.sStateRtcDesc = GetString("sStateRtcDesc") ;
			dataLoan.sStateRtcMb_rep = GetString("sStateRtcMb") ;
            dataLoan.sStateRtcBaseT = (E_PercentBaseT)GetInt("sStateRtcBaseT");
			dataLoan.sStateRtcPc_rep = GetString("sStateRtcPc") ;
			dataLoan.sCountyRtcDesc = GetString("sCountyRtcDesc") ;
			dataLoan.sCountyRtcMb_rep = GetString("sCountyRtcMb") ;
            dataLoan.sCountyRtcBaseT = (E_PercentBaseT)GetInt("sCountyRtcBaseT");
			dataLoan.sCountyRtcPc_rep = GetString("sCountyRtcPc") ;
			dataLoan.sRecFDesc = GetString("sRecFDesc") ;
			dataLoan.sRecFMb_rep = GetString("sRecFMb") ;
            dataLoan.sRecBaseT = (E_PercentBaseT)GetInt("sRecBaseT");
			dataLoan.sRecFPc_rep = GetString("sRecFPc") ;
			dataLoan.sU5Sc_rep = GetString("sU5Sc") ;
			if (isBindAll) 
			{
				dataLoan.sU5ScDesc = GetString("sU5ScDesc") ;
				dataLoan.sU5ScCode = GetString("sU5ScCode") ;
			}
			dataLoan.sU4Sc_rep = GetString("sU4Sc") ;
			if (isBindAll) 
			{
				dataLoan.sU4ScDesc = GetString("sU4ScDesc") ;
				dataLoan.sU4ScCode = GetString("sU4ScCode") ;
			}
			dataLoan.sU3Sc_rep = GetString("sU3Sc") ;
			if (isBindAll) 
			{
				dataLoan.sU3ScDesc = GetString("sU3ScDesc") ;
				dataLoan.sU3ScCode = GetString("sU3ScCode") ;
			}
			dataLoan.sU2Sc_rep = GetString("sU2Sc") ;
			if (isBindAll) 
			{
				dataLoan.sU2ScDesc = GetString("sU2ScDesc") ;
				dataLoan.sU2ScCode = GetString("sU2ScCode") ;
			}
			dataLoan.sU1Sc_rep = GetString("sU1Sc") ;
			if (isBindAll) 
			{
				dataLoan.sU1ScDesc = GetString("sU1ScDesc") ;
				dataLoan.sU1ScCode = GetString("sU1ScCode") ;
			}
			dataLoan.sPestInspectF_rep = GetString("sPestInspectF") ;
			dataLoan.sBrokComp2_rep = GetString("sBrokComp2") ;
			if (isBindAll) 
			{
				dataLoan.sBrokComp2Desc = GetString("sBrokComp2Desc") ;
			}
			
			dataLoan.sBrokComp1_rep = GetString("sBrokComp1") ;
			
			if (isBindAll) 
			{
				dataLoan.sBrokComp1Desc = GetString("sBrokComp1Desc") ;
			}

			dataLoan.sBrokComp1Pc_rep = GetString("sBrokComp1Pc");
			
			dataLoan.sBrokComp1Lckd = GetBool("sBrokComp1Lckd");
			dataLoan.sGfeProvByBrok = GetBool("sGfeProvByBrok") ;

			if (isBindAll) 
			{
				dataLoan.sLpTemplateNm = GetString("sLpTemplateNm") ;
				dataLoan.sCcTemplateNm = GetString("sCcTemplateNm") ;
			}
            //if (GetString("sFHAEnergyEffImprov", null) != null) 
            //{
            //    dataLoan.sFHAEnergyEffImprov_rep = GetString("sFHAEnergyEffImprov");
            //}
			if (null != GetString("sFfUfmipR", null)) 
			{
				dataLoan.sFfUfmipR_rep = GetString("sFfUfmipR");
			}

            //if (GetString("sFHAReqCashInv", null) != null) 
            //{
            //    dataLoan.sFHAReqCashInv_rep = GetString("sFHAReqCashInv") ;
            //}
            //if (GetString("sFHASalesConcessions", null) != null) 
            //{
            //    dataLoan.sFHASalesConcessions_rep = GetString("sFHASalesConcessions") ;
            //}


			dataLoan.sLOrigFProps = RetrieveItemProps(dataLoan.m_convertLos,  "sLOrigFProps");
			dataLoan.sLDiscntProps = RetrieveItemProps(dataLoan.m_convertLos,  "sLDiscntProps");
			dataLoan.sApprFProps = RetrieveItemProps(dataLoan.m_convertLos,  "sApprFProps");
			dataLoan.sCrFProps = RetrieveItemProps(dataLoan.m_convertLos,  "sCrFProps");
			dataLoan.sInspectFProps = RetrieveItemProps(dataLoan.m_convertLos,  "sInspectFProps");
			dataLoan.sMBrokFProps = RetrieveItemProps(dataLoan.m_convertLos,  "sMBrokFProps");
			dataLoan.sTxServFProps = RetrieveItemProps(dataLoan.m_convertLos,  "sTxServFProps");
			dataLoan.sProcFProps = RetrieveItemProps(dataLoan.m_convertLos,  "sProcFProps");
			dataLoan.sUwFProps = RetrieveItemProps(dataLoan.m_convertLos,  "sUwFProps");
			dataLoan.sWireFProps = RetrieveItemProps(dataLoan.m_convertLos,  "sWireFProps");
			dataLoan.s800U1FProps = RetrieveItemProps(dataLoan.m_convertLos,  "s800U1FProps");
			dataLoan.s800U2FProps = RetrieveItemProps(dataLoan.m_convertLos,  "s800U2FProps");
			dataLoan.s800U3FProps = RetrieveItemProps(dataLoan.m_convertLos,  "s800U3FProps");	
			dataLoan.s800U4FProps = RetrieveItemProps(dataLoan.m_convertLos,  "s800U4FProps");
			dataLoan.s800U5FProps = RetrieveItemProps(dataLoan.m_convertLos,  "s800U5FProps");
			dataLoan.sIPiaProps = RetrieveItemProps(dataLoan.m_convertLos,  "sIPiaProps");
			dataLoan.sMipPiaProps = RetrieveItemProps(dataLoan.m_convertLos,  "sMipPiaProps");
			dataLoan.sHazInsPiaProps = RetrieveItemProps(dataLoan.m_convertLos,  "sHazInsPiaProps");
			dataLoan.s904PiaProps = RetrieveItemProps(dataLoan.m_convertLos,  "s904PiaProps");
			dataLoan.sVaFfProps = RetrieveItemProps(dataLoan.m_convertLos,  "sVaFfProps");
			dataLoan.s900U1PiaProps = RetrieveItemProps(dataLoan.m_convertLos,  "s900U1PiaProps");
			dataLoan.sHazInsRsrvProps = RetrieveItemProps(dataLoan.m_convertLos,  "sHazInsRsrvProps");
			dataLoan.sMInsRsrvProps = RetrieveItemProps(dataLoan.m_convertLos,  "sMInsRsrvProps");
			dataLoan.sSchoolTxRsrvProps = RetrieveItemProps(dataLoan.m_convertLos,  "sSchoolTxRsrvProps");
			dataLoan.sRealETxRsrvProps = RetrieveItemProps(dataLoan.m_convertLos,  "sRealETxRsrvProps");
			dataLoan.sFloodInsRsrvProps = RetrieveItemProps(dataLoan.m_convertLos,  "sFloodInsRsrvProps");
			dataLoan.s1006RsrvProps = RetrieveItemProps(dataLoan.m_convertLos,  "s1006RsrvProps");
			dataLoan.s1007RsrvProps = RetrieveItemProps(dataLoan.m_convertLos,  "s1007RsrvProps");
			dataLoan.sAggregateAdjRsrvProps = RetrieveItemProps(dataLoan.m_convertLos,  "sAggregateAdjRsrvProps");
			dataLoan.sEscrowFProps = RetrieveItemProps(dataLoan.m_convertLos,  "sEscrowFProps");
			dataLoan.sDocPrepFProps = RetrieveItemProps(dataLoan.m_convertLos,  "sDocPrepFProps");
			dataLoan.sNotaryFProps = RetrieveItemProps(dataLoan.m_convertLos,  "sNotaryFProps");
			dataLoan.sAttorneyFProps = RetrieveItemProps(dataLoan.m_convertLos,  "sAttorneyFProps");
			dataLoan.sTitleInsFProps = RetrieveItemProps(dataLoan.m_convertLos,  "sTitleInsFProps");
			dataLoan.sU1TcProps = RetrieveItemProps(dataLoan.m_convertLos,  "sU1TcProps");
			dataLoan.sU2TcProps = RetrieveItemProps(dataLoan.m_convertLos,  "sU2TcProps");
			dataLoan.sU3TcProps = RetrieveItemProps(dataLoan.m_convertLos,  "sU3TcProps");
			dataLoan.sU4TcProps = RetrieveItemProps(dataLoan.m_convertLos,  "sU4TcProps");
			dataLoan.sRecFProps = RetrieveItemProps(dataLoan.m_convertLos,  "sRecFProps");
			dataLoan.sCountyRtcProps = RetrieveItemProps(dataLoan.m_convertLos,  "sCountyRtcProps");
			dataLoan.sStateRtcProps = RetrieveItemProps(dataLoan.m_convertLos,  "sStateRtcProps");
			dataLoan.sU1GovRtcProps = RetrieveItemProps(dataLoan.m_convertLos,  "sU1GovRtcProps");
			dataLoan.sU2GovRtcProps = RetrieveItemProps(dataLoan.m_convertLos,  "sU2GovRtcProps");
			dataLoan.sU3GovRtcProps = RetrieveItemProps(dataLoan.m_convertLos,  "sU3GovRtcProps");
			dataLoan.sPestInspectFProps = RetrieveItemProps(dataLoan.m_convertLos,  "sPestInspectFProps");
			dataLoan.sU1ScProps = RetrieveItemProps(dataLoan.m_convertLos,  "sU1ScProps");
			dataLoan.sU2ScProps = RetrieveItemProps(dataLoan.m_convertLos,  "sU2ScProps");
			dataLoan.sU3ScProps = RetrieveItemProps(dataLoan.m_convertLos,  "sU3ScProps");
			dataLoan.sU4ScProps = RetrieveItemProps(dataLoan.m_convertLos,  "sU4ScProps");
			dataLoan.sU5ScProps = RetrieveItemProps(dataLoan.m_convertLos,  "sU5ScProps");

            dataLoan.sAltCostLckd = GetBool("sAltCostLckd");
            dataLoan.sAltCost_rep = GetString("sAltCost");
            dataLoan.sLandCost_rep = GetString("sLandCost");
			dataLoan.sRefPdOffAmt1003Lckd = GetBool("sRefPdOffAmt1003Lckd");
			dataLoan.sRefPdOffAmt1003_rep = GetString("sRefPdOffAmt1003");
			dataLoan.sTotEstPp1003Lckd = GetBool("sTotEstPp1003Lckd");
			dataLoan.sTotEstPp1003_rep = GetString("sTotEstPp1003");
			dataLoan.sTotEstCc1003Lckd = GetBool("sTotEstCc1003Lckd");
			dataLoan.sTotEstCcNoDiscnt1003_rep = GetString("sTotEstCcNoDiscnt1003");
			dataLoan.sFfUfmip1003Lckd = GetBool("sFfUfmip1003Lckd");
			dataLoan.sFfUfmip1003_rep = GetString("sFfUfmip1003"); 
			dataLoan.sLDiscnt1003Lckd = GetBool("sLDiscnt1003Lckd");
			dataLoan.sLDiscnt1003_rep = GetString("sLDiscnt1003");
			dataLoan.sOCredit1Desc = GetString("sOCredit1Desc");
			dataLoan.sOCredit1Amt_rep = GetString("sOCredit1Amt");
			dataLoan.sOCredit1Lckd = GetBool("sOCredit1Lckd");
            
            if (false == dataLoan.sLoads1003LineLFromAdjustments)
            {
                dataLoan.sOCredit2Desc = GetString("sOCredit2Desc");
                dataLoan.sOCredit2Amt_rep = GetString("sOCredit2Amt");
                //dataLoan.sOCredit2Lckd = GetBool("sOCredit2Lckd");
                dataLoan.sOCredit3Desc = GetString("sOCredit3Desc");
                dataLoan.sOCredit3Amt_rep = GetString("sOCredit3Amt");
                dataLoan.sOCredit4Desc = GetString("sOCredit4Desc");
                dataLoan.sOCredit4Amt_rep = GetString("sOCredit4Amt");
            }

			dataLoan.sTransNetCashLckd = GetBool("sTransNetCashLckd");
			dataLoan.sTransNetCash_rep = GetString("sTransNetCash");
			dataLoan.sPrintCompensationOnGfe = GetBool("sPrintCompensationOnGfe");
			dataLoan.sONewFinCc_rep = GetString("sONewFinCc");

			dataLoan.sLAmtLckd = GetBool("sLAmtLckd");
			dataLoan.sLAmtCalc_rep = GetString("sLAmtCalc");
            dataLoan.sIPiaDyLckd = GetBool("sIPiaDyLckd");


			return alertUser;
		}

		/// <summary>
		/// For performance reason, I only return calculated data. Therefore
		/// DO NOT use this method to return complete GFE data. dd 7/25/2003
		/// </summary>
		/// <param name="dataLoan"></param>
		/// <returns></returns>
		private void LoadData(CPageData dataLoan) 
		{

			SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
			SetResult("sFinalLAmt2", dataLoan.sFinalLAmt_rep);
			SetResult("sDaysInYr", dataLoan.sDaysInYr_rep);
			SetResult("sTotCcPbsLocked", dataLoan.sTotCcPbsLocked);
			SetResult("sTotCcPbs", dataLoan.sTotCcPbs_rep);
			SetResult("sPurchPrice", dataLoan.sPurchPrice_rep);
			SetResult("sMBrokF", dataLoan.sMBrokF_rep);
			SetResult("sInspectF", dataLoan.sInspectF_rep);
			SetResult("sLDiscnt", dataLoan.sLDiscnt_rep);
			SetResult("sLOrigF", dataLoan.sLOrigF_rep);
			SetResult("s900U1Pia", dataLoan.s900U1Pia_rep);
			SetResult("sVaFf", dataLoan.sVaFf_rep);
			SetResult("s904Pia", dataLoan.s904Pia_rep);
			SetResult("sHazInsPia", dataLoan.sHazInsPia_rep);
			SetResult("sHazInsPiaMon", dataLoan.sHazInsPiaMon_rep);
			SetResult("sProHazInsT", dataLoan.sProHazInsT);
			SetResult("sProHazInsR", dataLoan.sProHazInsR_rep);
			SetResult("sProHazInsMb", dataLoan.sProHazInsMb_rep);
			SetResult("sMipPia", dataLoan.sMipPia_rep);

			SetResult("sIPia", dataLoan.sIPia_rep);
			SetResult("sIPiaDy", dataLoan.sIPiaDy_rep);
			SetResult("sIPerDayLckd", dataLoan.sIPerDayLckd);
			SetResult("sIPerDay", dataLoan.sIPerDay_rep);
			SetResult("sAggregateAdjRsrv", dataLoan.sAggregateAdjRsrv_rep);
			SetResult("sAggregateAdjRsrvLckd", dataLoan.sAggregateAdjRsrvLckd);
			SetResult("s1007Rsrv", dataLoan.s1007Rsrv_rep);
			SetResult("s1007ProHExp", dataLoan.s1007ProHExp_rep);
			SetResult("s1007RsrvMon", dataLoan.s1007RsrvMon_rep);
			SetResult("s1006Rsrv", dataLoan.s1006Rsrv_rep);
			SetResult("s1006ProHExp", dataLoan.s1006ProHExp_rep);
			SetResult("s1006RsrvMon", dataLoan.s1006RsrvMon_rep);
			SetResult("sFloodInsRsrv", dataLoan.sFloodInsRsrv_rep);
			SetResult("sProFloodIns", dataLoan.sProFloodIns_rep);
			SetResult("sFloodInsRsrvMon", dataLoan.sFloodInsRsrvMon_rep);
			SetResult("sRealETxRsrv", dataLoan.sRealETxRsrv_rep);
			SetResult("sProRealETx", dataLoan.sProRealETx_rep);
			SetResult("sRealETxRsrvMon", dataLoan.sRealETxRsrvMon_rep);
			SetResult("sSchoolTxRsrv", dataLoan.sSchoolTxRsrv_rep);
			SetResult("sProSchoolTx", dataLoan.sProSchoolTx_rep);
			SetResult("sSchoolTxRsrvMon", dataLoan.sSchoolTxRsrvMon_rep);
			SetResult("sMInsRsrv", dataLoan.sMInsRsrv_rep);
			SetResult("sProMIns", dataLoan.sProMIns_rep);
			SetResult("sMInsRsrvMon", dataLoan.sMInsRsrvMon_rep);
			SetResult("sHazInsRsrv", dataLoan.sHazInsRsrv_rep);
			SetResult("sProHazIns", dataLoan.sProHazIns_rep);
			SetResult("sHazInsRsrvMon", dataLoan.sHazInsRsrvMon_rep);
			SetResult("sTitleInsF", dataLoan.sTitleInsF_rep);
			SetResult("sAttorneyF", dataLoan.sAttorneyF_rep);
			SetResult("sNotaryF", dataLoan.sNotaryF_rep);
			SetResult("sDocPrepF", dataLoan.sDocPrepF_rep);
			SetResult("sEscrowF", dataLoan.sEscrowF_rep);
			SetResult("sU3GovRtc", dataLoan.sU3GovRtc_rep);
			SetResult("sU2GovRtc", dataLoan.sU2GovRtc_rep);
			SetResult("sU1GovRtc", dataLoan.sU1GovRtc_rep);
			SetResult("sStateRtc", dataLoan.sStateRtc_rep);
			SetResult("sCountyRtc", dataLoan.sCountyRtc_rep);
			SetResult("sRecF", dataLoan.sRecF_rep);
			SetResult("sPestInspectF", dataLoan.sPestInspectF_rep);
			SetResult("sBrokComp2", dataLoan.sBrokComp2_rep);
			SetResult("sBrokComp1", dataLoan.sBrokComp1_rep);
			SetResult("sTotEstSc", dataLoan.sTotEstSc_rep);
			SetResult("sGfeProvByBrok", dataLoan.sGfeProvByBrok);
			//            ret["sFCc"] = dataLoan.sFfUfmipFinanced_rep;
			//            ret["sDisabilityIns"] = dataLoan.sDisabilityIns_rep;

            //SetResult("sFHAEnergyEffImprov", dataLoan.sFHAEnergyEffImprov_rep );
			SetResult("sFfUfmip1003", dataLoan.sFfUfmip1003_rep );
            //SetResult("sFHAReqCashInv", dataLoan.sFHAReqCashInv_rep );
            //SetResult("sFHASalesConcessions", dataLoan.sFHASalesConcessions_rep );
            //SetResult("sFHA203kRehabCost", dataLoan.sFHA203kRehabCost_rep );
            //SetResult("sFHATotEstFntc", dataLoan.sFHATotEstFntc_rep );
            //			ret["sRefPdOffAmtGfe"] = dataLoan.sRefPdOffAmtGfe_rep;
            //			ret["sRefPdOffAmtGfeLckd"] = dataLoan.sRefPdOffAmtGfeLckd.ToString();

            SetResult("sAltCostLckd", dataLoan.sAltCostLckd);
            SetResult("sAltCost", dataLoan.sAltCost_rep );
            SetResult("sLandCost", dataLoan.sLandCost_rep );
			SetResult("sRefPdOffAmt1003Lckd", dataLoan.sRefPdOffAmt1003Lckd );
			SetResult("sRefPdOffAmt1003", dataLoan.sRefPdOffAmt1003_rep );
			SetResult("sTotEstPp1003Lckd", dataLoan.sTotEstPp1003Lckd);
			SetResult("sTotEstPp1003", dataLoan.sTotEstPp1003_rep );
			SetResult("sTotEstCc1003Lckd", dataLoan.sTotEstCc1003Lckd );
			SetResult("sTotEstCcNoDiscnt1003", dataLoan.sTotEstCcNoDiscnt1003_rep );
			SetResult("sFfUfmip1003Lckd", dataLoan.sFfUfmip1003Lckd);
			SetResult("sFfUfmip10032", dataLoan.sFfUfmip1003_rep ); 
			SetResult("sLDiscnt1003Lckd", dataLoan.sLDiscnt1003Lckd );
			SetResult("sLDiscnt1003", dataLoan.sLDiscnt1003_rep );
			SetResult("sTotTransC", dataLoan.sTotTransC_rep );
			SetResult("sONewFinBal", dataLoan.sONewFinBal_rep);
			SetResult("sOCredit1Desc", dataLoan.sOCredit1Desc );
			SetResult("sOCredit1Amt", dataLoan.sOCredit1Amt_rep );
			SetResult("sOCredit1Lckd", dataLoan.sOCredit1Lckd);
			SetResult("sOCredit2Desc", dataLoan.sOCredit2Desc );
			SetResult("sOCredit2Amt", dataLoan.sOCredit2Amt_rep );
			SetResult("sOCredit3Desc", dataLoan.sOCredit3Desc );
			SetResult("sOCredit3Amt", dataLoan.sOCredit3Amt_rep );
			SetResult("sOCredit4Desc", dataLoan.sOCredit4Desc );
			SetResult("sOCredit4Amt", dataLoan.sOCredit4Amt_rep );
			SetResult("sLAmtCalc", dataLoan.sLAmtCalc_rep );
			SetResult("sLAmtLckd", dataLoan.sLAmtLckd);
			SetResult("sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep );
			SetResult("sTransNetCashLckd", dataLoan.sTransNetCashLckd);
			SetResult("sTransNetCash", dataLoan.sTransNetCash_rep );
			SetResult("sPrintCompensationOnGfe", dataLoan.sPrintCompensationOnGfe);
			SetResult("sONewFinCc", dataLoan.sONewFinCc_rep);
            SetResult("sIPiaDyLckd", dataLoan.sIPiaDyLckd);
		}

		private int RetrieveItemProps( LosConvert convert, string name)
		{
			bool apr = GetBool(name + "_ctrl_Apr_chk");
			bool toBr = GetBool(name + "_ctrl_ToBrok_chk");
			int payer = GetInt(name + "_ctrl_PdByT_dd");
			bool fhaAllow = GetBool(name + "_ctrl_Fha_chk");
			bool poc = GetBool(name + "_ctrl_Poc_chk");

			return LosConvert.GfeItemProps_Pack(apr, toBr, payer, fhaAllow, poc );
		}

		private void InitItemProps( LosConvert convert, string name, int props )
		{
			SetResult(name + "_ctrl_Apr_chk", LosConvert.GfeItemProps_Apr(props));
			SetResult(name + "_ctrl_ToBrok_chk", LosConvert.GfeItemProps_ToBr(props));
			SetResult(name + "_ctrl_PdByT_dd", LosConvert.GfeItemProps_Payer(props));
			SetResult(name + "_ctrl_Fha_chk", LosConvert.GfeItemProps_FhaAllow(props));

			SetResult(name + "_ctrl_Poc_ck", LosConvert.GfeItemProps_Poc(props));


		}
		private void LoadData() 
		{
			Guid loanID = GetGuid("loanid");
			CPageData dataLoan = new CGfeData(loanID);
			dataLoan.InitLoad();

			LoadData(dataLoan);
		}

		private void ChecksBrokComp1Pc()
		{
			Guid loanID = GetGuid("loanid");
			CPageData dataLoan = new CGfeData(loanID);
			dataLoan.InitLoad();
			bool throwException = false;
			// 12/27/06 db OPM 8935 - if the rate becomes locked after the user started editing the GFE, send a 
			//message back indicating that the sBrokComp1Pc field cannot be changed
		
			bool ignore = GetBool("ignoreIfNotChanged");
			if(ignore)
			{
				decimal val = -1;
				try
				{
					string rate = GetString("sBrokComp1Pc");
					val = dataLoan.m_convertLos.ToRate(rate);
				}
				catch{}
				
				if(!dataLoan.CanUpdate_sBrokComp1PcIgnoreIfNotChanged(val))
					throwException = true;
			}
			else
			{
				if(!dataLoan.CanUpdate_sBrokComp1Pc())
					throwException = true;
			}
			if(throwException)
			{
				//set the field back to the original value if it's been changed
				SetResult("sBrokComp1Pc", dataLoan.m_convertLos.ToRateString(dataLoan.sBrokComp1Pc));
				CBaseException rebateExc = new CBaseException( ErrorMessages.RebateCannotBeChangedDuringRateLock, "Cannot change the fee/point when there is a rate lock.");
				rebateExc.IsEmailDeveloper = false;
				throw rebateExc;
			}
		}

        private void CalculateData() 
        {
			Guid loanID = GetGuid("loanid");
            CPageData dataLoan = new CGfeData(loanID);
            dataLoan.InitLoad();

            bool alertUser = BindData(dataLoan, false);

            LoadData(dataLoan);

			// 12/27/06 db OPM 8935 - if the rate becomes locked after the user started editing the GFE, send a 
			//message back indicating that the sBrokComp1Pc field cannot be changed

			if(alertUser)
			{
				CBaseException rebateExc = new CBaseException( ErrorMessages.RebateCannotBeChangedDuringRateLock, "Cannot change the fee/point when there is a rate lock.");
				rebateExc.IsEmailDeveloper = false;
				throw rebateExc;
			}
        }
	}
}
