﻿#region Generated Code
namespace LendersOfficeApp.newlos.Forms
#endregion
{
    using System;
    using System.Web.UI;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Loan;

    /// <summary>
    /// Provides a breakdown for the calculation of the loan's qualifying rate.
    /// </summary>
    public partial class QualRateCalculationPopup : UserControl
    {
        /// <summary>
        /// Loads the control data from the service item.
        /// </summary>
        /// <param name="loan">
        /// The loan to load.
        /// </param>
        /// <param name="service">
        /// The service item to send the data through.
        /// </param>
        public static void LoadData(CPageData loan, AbstractBackgroundServiceItem service)
        {
            service.SetResult("HasQualRatePopup", true);
            service.SetResult(nameof(loan.sUseQualRate), loan.sUseQualRate);
            service.SetResult(nameof(loan.sQualRateCalculationT), loan.sQualRateCalculationT);
            service.SetResult(nameof(loan.sQualRateCalculationFieldT1), loan.sQualRateCalculationFieldT1);
            service.SetResult(nameof(loan.sQualRateCalculationAdjustment1), loan.sQualRateCalculationAdjustment1_rep);
            service.SetResult(nameof(loan.sQualRateCalculationFieldT2), loan.sQualRateCalculationFieldT2);
            service.SetResult(nameof(loan.sQualRateCalculationAdjustment2), loan.sQualRateCalculationAdjustment2_rep);

            var calculationOptions = new QualRateCalculationOptions()
            {
                NoteRate = loan.sNoteIR,
                IndexRate = loan.sRAdjIndexR,
                MarginRate = loan.sRAdjMarginR
            };

            var firstCalculationResult = Tools.GetQualRateCalculationValue(calculationOptions, loan.sQualRateCalculationFieldT1, loan.sQualRateCalculationAdjustment1);
            service.SetResult("QualRateCalculationResult1", loan.m_convertLos.ToRateString(firstCalculationResult));

            var secondCalculationResult = Tools.GetQualRateCalculationValue(calculationOptions, loan.sQualRateCalculationFieldT2, loan.sQualRateCalculationAdjustment2);
            service.SetResult("QualRateCalculationResult2", loan.m_convertLos.ToRateString(secondCalculationResult));
        }

        /// <summary>
        /// Binds the control data from the service item to the loan.
        /// </summary>
        /// <param name="loan">
        /// The loan to bind.
        /// </param>
        /// <param name="service">
        /// The service item to bind data from.
        /// </param>
        public static void BindData(CPageData loan, AbstractBackgroundServiceItem service)
        {
            loan.sUseQualRate = service.GetBool(nameof(loan.sUseQualRate));

            loan.sQualRateCalculationFieldT1 = service.GetEnum<QualRateCalculationFieldT>(nameof(loan.sQualRateCalculationFieldT1));
            loan.sQualRateCalculationAdjustment1_rep = service.GetString(nameof(loan.sQualRateCalculationAdjustment1));

            loan.sQualRateCalculationT = service.GetEnum<QualRateCalculationT>(nameof(loan.sQualRateCalculationT));
            if (loan.sQualRateCalculationT == QualRateCalculationT.MaxOf)
            {
                var calculationField2 = service.GetEnum<QualRateCalculationFieldT>(nameof(loan.sQualRateCalculationFieldT2));
                if (loan.sUseQualRate && loan.sQualRateCalculationFieldT1 == calculationField2)
                {
                    throw new CBaseException(ErrorMessages.QualRateFieldsMustBeMutuallyExclusive, ErrorMessages.QualRateFieldsMustBeMutuallyExclusive);
                }

                loan.sQualRateCalculationFieldT2 = calculationField2;
                loan.sQualRateCalculationAdjustment2_rep = service.GetString(nameof(loan.sQualRateCalculationAdjustment2));
            }
        }

        /// <summary>
        /// Loads the calculation options for the control.
        /// </summary>
        /// <param name="loan">
        /// The loan to load data.
        /// </param>
        public void LoadData(CPageData loan)
        {
            var basePage = this.Page as BasePage;
            if (basePage == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Qual rate popup is expected to be hosted on a BasePage.");
            }
            else
            {
                basePage.RegisterCSS("QualRateCalculationPopup.css");

                basePage.RegisterJsScript("QualRateCalculationPopup.js");
                basePage.RegisterJsScript("QualRateCalculation.js");

                basePage.RegisterJsGlobalVariables("FlatValue", QualRateCalculationT.FlatValue.ToString("D"));
                basePage.RegisterJsGlobalVariables("MaxOf", QualRateCalculationT.MaxOf.ToString("D"));
                basePage.RegisterJsObjectWithJsonNetAnonymousSerializer("QualRateFieldIds", this.GetQualRateFieldIds());
            }

            this.sQualIR.Text = loan.sQualIR_rep;
            this.sUseQualRate.Checked = loan.sUseQualRate;

            this.QualRateCalculationFlatValue.Checked = loan.sQualRateCalculationT == QualRateCalculationT.FlatValue;
            this.QualRateCalculationMaxOf.Checked = loan.sQualRateCalculationT == QualRateCalculationT.MaxOf;

            Tools.SetDropDownListValue(this.sQualRateCalculationFieldT1, loan.sQualRateCalculationFieldT1);
            this.sQualRateCalculationAdjustment1.Text = loan.sQualRateCalculationAdjustment1_rep;

            Tools.SetDropDownListValue(this.sQualRateCalculationFieldT2, loan.sQualRateCalculationFieldT2);
            this.sQualRateCalculationAdjustment2.Text = loan.sQualRateCalculationAdjustment2_rep;

            var calculationOptions = new QualRateCalculationOptions()
            {
                NoteRate = loan.sNoteIR,
                IndexRate = loan.sRAdjIndexR,
                MarginRate = loan.sRAdjMarginR
            };

            var calculationResult1 = Tools.GetQualRateCalculationValue(calculationOptions, loan.sQualRateCalculationFieldT1, loan.sQualRateCalculationAdjustment1);
            this.QualRateCalculationResult1.Text = loan.m_convertLos.ToRateString(calculationResult1);

            if (loan.sQualRateCalculationT == QualRateCalculationT.MaxOf)
            {
                var calculationResult2 = Tools.GetQualRateCalculationValue(calculationOptions, loan.sQualRateCalculationFieldT2, loan.sQualRateCalculationAdjustment2);
                this.QualRateCalculationResult2.Text = loan.m_convertLos.ToRateString(calculationResult2);
            }
            else
            {
                this.QualRateCalculationResult2.Text = loan.m_convertLos.ToRateString(0M);
            }
        }

        /// <summary>
        /// Initializes the control.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="args">
        /// The parameter is not used.
        /// </param>
        protected void Page_Init(object sender, EventArgs args)
        {
            Tools.Bind_QualRateCalculationFieldT(this.sQualRateCalculationFieldT1);
            Tools.Bind_QualRateCalculationFieldT(this.sQualRateCalculationFieldT2);
        }

        /// <summary>
        /// Gets the client IDs for the qual rate calculation IDs.
        /// </summary>
        /// <returns>
        /// A complex type with the qual rate field IDs.
        /// </returns>
        private object GetQualRateFieldIds()
        {
            return new
            {
                QualRateCalcPopup = this.QualRateCalcPopup.ClientID,
                QualRateCalculationPopup_Ok = this.QualRateCalculationPopup_Ok.ClientID,
                QualRateCalculationPopup_Close = this.QualRateCalculationPopup_Close.ClientID,
                ShouldUseQualRate = this.sUseQualRate.ClientID,
                QualRate = this.sQualIR.ClientID,
                QualRateCalculationResult1 = this.QualRateCalculationResult1.ClientID,
                QualRateCalculationResult2 = this.QualRateCalculationResult2.ClientID,
                QualRateCalculationFlatValue = this.QualRateCalculationFlatValue.ClientID,
                QualRateCalculationMaxOf = this.QualRateCalculationMaxOf.ClientID,
                QualRateCalculationFieldT1 = this.sQualRateCalculationFieldT1.ClientID,
                QualRateCalculationFieldT2 = this.sQualRateCalculationFieldT2.ClientID,
                QualRateCalculationAdjustment1 = this.sQualRateCalculationAdjustment1.ClientID,
                QualRateCalculationAdjustment2 = this.sQualRateCalculationAdjustment2.ClientID
            };
        }
    }
}