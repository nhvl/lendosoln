<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="FloridaDisclosureEdit.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.FloridaDisclosureEdit" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>FloridaDisclosureEdit</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="FlowLayout" class="RightBackground">
	
    <form id="FloridaDisclosureEdit" method="post" runat="server">
<table id=Table1 cellspacing=0 cellpadding=0 width="100%" border=0 class=FormTable>
  <tr>
    <td class=MainRightHeader colspan=2>Florida-Lender Disclosure</td></tr>
  <tr>
    <td><asp:textbox id=LenderName runat="server" width="247px"></asp:textbox></td>
    <td width="100%"><asp:checkbox id=sIsLicensedLender runat="server" text="licensed mortgage lender"></asp:checkbox></td></tr>
  <tr>
    <td></td>
    <td><asp:checkbox id=sIsCorrespondentLender runat="server" text="correspondent mortgage lender"></asp:checkbox></td></tr>
  <tr>
    <td></td>
    <td><asp:checkbox id=sIsOtherTypeLender runat="server"></asp:checkbox><asp:textbox id=sOtherTypeLenderDesc runat="server"></asp:textbox></td></tr>
  <tr>
    <td></td>
    <td></td></tr>
  <tr>
    <td colspan=2>1) If an application fee, credit report 
      fee, appraisal fee, or any other third-party fee is received by lender, 
      the fee is:</td></tr>
  <tr>
    <td colspan=2><asp:radiobuttonlist id=sRefundabilityOfFeeToLenderT runat="server">
<asp:ListItem Value="0">Leave blank</asp:ListItem>
<asp:ListItem Value="1">nonrefundable</asp:ListItem>
<asp:ListItem Value="2">refundable</asp:ListItem>
<asp:ListItem Value="3">N/A ( No funds collected at or prior to funding)</asp:ListItem>
</asp:radiobuttonlist></td></tr>
  <tr>
    <td colspan=2>2) The issuance of a commitment by the 
      lender following the receipt of the application is estimated at <asp:textbox id=sCommitmentEstimateDays runat="server" width="33px" maxlength="4"></asp:textbox>&nbsp;days</td></tr>
  <tr>
    <td colspan=2>3) The person listed below is designated as being 
      responsible on behalf of the lender to promptly response to written 
      inquiries from borrower regarding questions, comments or complaints</td></tr>
  <tr>
    <td>Name</td>
    <td><asp:textbox id=RepName runat="server" width="331px"></asp:textbox></td></tr>
  <tr>
    <td>Address</td>
    <td><asp:textbox id=RepAddr runat="server" width="332px"></asp:textbox></td></tr>
  <tr>
    <td></td>
    <td><asp:textbox id=RepCity runat="server" width="235px"></asp:textbox><ml:statedropdownlist id=RepState runat="server"></ml:statedropdownlist><ml:ZipcodeTextBox id=RepZipcode runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
</table>

     </form>
	
  </body>
</html>
