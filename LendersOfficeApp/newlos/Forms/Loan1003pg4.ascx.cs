using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{

	/// <summary>
	///		Summary description for Loan1003pg4.
	/// </summary>
	public partial  class Loan1003pg4 : BaseLoanUserControl, IAutoLoadUserControl
	{


		private void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}
        public void SaveData() 
        {
        }
        public void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Loan1003pg4));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            a1003ContEditSheet.Text = dataApp.a1003ContEditSheet;
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

        }
		#endregion
	}
}
