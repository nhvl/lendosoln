﻿<%@ Register TagPrefix="uc1" TagName="GoodFaithEstimate2010RightColumn" Src="../../los/LegalForm/GoodFaithEstimate2010RightColumn.ascx" %>
<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="EscrowImpoundsSetup.aspx.cs" Inherits="LendersOfficeApp.newlos.Forms.EscrowImpoundsSetup" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Escrow / Impounds</title>
    <style type="text/css">
         body { background-color:gainsboro;}
        .EscrowedColumn{ text-align:center;}
        .RsrvMnthsLckdColumn { text-align:right;}
        .RsrvMnthsColumn { text-align:left; }
        .AlignLeft{ text-align:left;}
        .AlignRight { text-align:right; }
        .GFE2010RightColumn { display:none; }
    </style>
    <script>
        function doAfterDateFormat(e)
        {
            refreshCalculation();
        }
    </script>
</head>
<body>
    <script type="text/javascript">
        var eispObj = {}; // page object.
        eispObj.feeTypePropertySetter = new FeeTypePropertiesSetter(false);
        function setFeeTypeProperties(combobox, lineNumber) {
            eispObj.feeTypePropertySetter.setFeeTypeProperties(combobox, lineNumber);
        }
    </script>
    <form id="form1" runat="server">
    <div class="MainRightHeader" noWrap>Escrow / Impounds</div>
    
    <table class="InsetBorder">
    <tbody>
    <tr>
    <td>
        <table id="ReservesTable" class="AlignLeft">
        <thead>
            <tr class="FormTableSubheader">
                <th width="24px"></th>
                <th colspan="2">Description</th>
                <th>Monthly Amt</th>
                <th class="EscrowedColumn">Escrowed</th>
                <th class="RsrvMnthsLckdColumn">Mths</th>
                <th class="RsrvMnthsColumn">Rsv</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody class="InsetBorder">
			<TR>
				<TD class="FieldLabel">1002</TD>
				<TD class="FieldLabel" width="174px">Hazard insurance</TD>
				<TD class="FieldLabel" noWrap>
                    <asp:PlaceHolder runat="server" ID="sProHazInsHolder">
                        <ml:percenttextbox id="sProHazInsR"  runat="server" preset="percent" width="70" DESIGNTIMEDRAGDROP="236"></ml:percenttextbox>
					    of
					    <asp:dropdownlist id="sProHazInsT" runat="server" ></asp:dropdownlist>
					    +
					    <ml:moneytextbox id="sProHazInsMb" runat="server" preset="money" width="60px" decimalDigits="4"></ml:moneytextbox>
                    </asp:PlaceHolder>
				</TD>
				<TD><ml:moneytextbox id="sProHazIns" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
				<TD class="EscrowedColumn"><asp:CheckBox id="sHazInsRsrvEscrowedTri" runat="server" /></TD>
				<TD class="RsrvMnthsLckdColumn"><asp:CheckBox id="sHazInsRsrvMonLckd" runat="server"/></TD>
				<TD class="RsrvMnthsColumn"><asp:textbox id="sHazInsRsrvMon"  runat="server" Width="41" MaxLength="4"></asp:textbox></TD>
				<TD><ml:moneytextbox id="sHazInsRsrv" runat="server" preset="money" decimalDigits="2" ReadOnly="true"/></TD>
			</TR>
			<TR>
				<TD class="FieldLabel">1003</TD>
				<TD class="FieldLabel" colSpan="2"><A href="javascript:linkMe('../LoanInfo.aspx?pg=1');">Mortgage Insurance</A> </TD>
				<TD colSpan="1"><ml:moneytextbox id="sProMIns" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>				
                <TD class="EscrowedColumn">
                    <asp:CheckBox id="sMInsRsrvEscrowedTri" runat="server" />
                </TD>
                <TD class="RsrvMnthsLckdColumn"><asp:CheckBox id="sMInsRsrvMonLckd" runat="server"/></TD>
                <TD class="RsrvMnthsColumn"><asp:textbox id="sMInsRsrvMon"  runat="server" Width="41" MaxLength="4"></asp:textbox></TD>
                <TD><ml:moneytextbox id="sMInsRsrv" runat="server" preset="money" decimalDigits="2" ReadOnly="true"/></TD>
			</TR>
			<TR>
				<TD class="FieldLabel">1004</TD>
				<TD class="FieldLabel" noWrap>Real Estate Taxes </TD>
				<TD class="FieldLabel">
                    <asp:PlaceHolder runat="server" ID="sProRealETxHolder">
                        <ml:percenttextbox id="sProRealETxR" runat="server" preset="percent" width="70" ></ml:percenttextbox>&nbsp;of
					    <asp:dropdownlist id="sProRealETxT" runat="server" ></asp:dropdownlist>
					    +
					    <ml:moneytextbox id="sProRealETxMb" runat="server" preset="money" width="59px" ></ml:moneytextbox>
                    </asp:PlaceHolder>
				</TD>
				<TD><ml:moneytextbox id="sProRealETx" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
				<TD class="EscrowedColumn"><asp:CheckBox id="sRealETxRsrvEscrowedTri" runat="server" /></TD>
				<TD class="RsrvMnthsLckdColumn"><asp:CheckBox id="sRealETxRsrvMonLckd" runat="server"/></TD>
				<TD class="RsrvMnthsColumn"><asp:textbox id="sRealETxRsrvMon"  runat="server" Width="41" MaxLength="4"></asp:textbox></TD>
				<TD><ml:moneytextbox id="sRealETxRsrv" runat="server" preset="money" decimalDigits="2" ReadOnly="true"/></TD>
			</TR>
			<TR>
				<TD class="FieldLabel">1005</TD>
				<TD class="FieldLabel">School taxes</TD>
				<TD class="FieldLabel"></TD>
				<TD><ml:moneytextbox id="sProSchoolTx"  runat="server" preset="money" width="76px"></ml:moneytextbox></TD>
				<TD class="EscrowedColumn"><asp:CheckBox id="sSchoolTxRsrvEscrowedTri" runat="server" /></TD>
			    <TD class="RsrvMnthsLckdColumn"><asp:CheckBox id="sSchoolTxRsrvMonLckd" runat="server"/></TD>
			    <TD class="RsrvMnthsColumn"><asp:textbox id="sSchoolTxRsrvMon"  runat="server" Width="41" MaxLength="4"></asp:textbox></TD>
			    <TD><ml:moneytextbox id="sSchoolTxRsrv" runat="server" preset="money" decimalDigits="2" ReadOnly="true"/></TD>
			</TR>
			<TR>
				<TD class="FieldLabel">1006</TD>
				<TD class="FieldLabel">Flood Insurance</TD>
				<TD class="FieldLabel"></TD>
				<TD><ml:moneytextbox id="sProFloodIns" runat="server" preset="money" width="76px"></ml:moneytextbox></TD>
				<TD class="EscrowedColumn"><asp:CheckBox id="sFloodInsRsrvEscrowedTri" runat="server" /></TD>
				<TD class="RsrvMnthsLckdColumn"><asp:CheckBox id="sFloodInsRsrvMonLckd" runat="server"/></TD>
				<TD class="RsrvMnthsColumn"><asp:textbox id="sFloodInsRsrvMon"  runat="server" Width="41" MaxLength="4"></asp:textbox></TD>
				<TD><ml:moneytextbox id="sFloodInsRsrv" runat="server" preset="money" decimalDigits="2" ReadOnly="true"/></TD>
			</TR>
			<TR>
				<TD class="FieldLabel">1008</TD>
				<TD>
				    <ml:ComboBox id="s1006ProHExpDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1000');" Width="145" MaxLength="21"></ml:ComboBox>
				</TD>
				<TD class="FieldLabel"></TD>
				<TD><ml:moneytextbox id="s1006ProHExp" runat="server" preset="money" width="76px"></ml:moneytextbox></TD>
				<TD class="EscrowedColumn"><asp:CheckBox id="s1006RsrvEscrowedTri" runat="server" /></TD>
				<TD class="RsrvMnthsLckdColumn"><asp:CheckBox id="s1006RsrvMonLckd" runat="server"/></TD>
				<TD class="RsrvMnthsColumn"><asp:textbox id="s1006RsrvMon"  runat="server" Width="41" MaxLength="4"></asp:textbox></TD>
				<TD><ml:moneytextbox id="s1006Rsrv" runat="server" preset="money" decimalDigits="2" ReadOnly="true"/></TD>
				<uc1:GoodFaithEstimate2010RightColumn class="HiddenColumn" id="s1006RsrvProps_ctrl" Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>            
			</TR>
			<TR>
				<TD class="FieldLabel">1009</TD>
				<TD>
				    <ml:ComboBox id="s1007ProHExpDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1000');" Width="145" MaxLength="21" ></ml:ComboBox>
				</TD>
				<TD class="FieldLabel"></TD>
				<TD><ml:moneytextbox id="s1007ProHExp" runat="server" preset="money" width="76px"></ml:moneytextbox></TD>
				<TD class="EscrowedColumn"><asp:CheckBox id="s1007RsrvEscrowedTri" runat="server" /></TD>
				<TD class="RsrvMnthsLckdColumn"><asp:CheckBox id="s1007RsrvMonLckd" runat="server"/></TD>
				<TD class="RsrvMnthsColumn"><asp:textbox id="s1007RsrvMon"  runat="server" Width="41" MaxLength="4"></asp:textbox></TD>
				<TD><ml:moneytextbox id="s1007Rsrv" runat="server" preset="money" decimalDigits="2" ReadOnly="true"/></TD>
				<uc1:GoodFaithEstimate2010RightColumn class="HiddenColumn" id="s1007RsrvProps_ctrl" Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
			</TR>
			<asp:PlaceHolder runat="server" ID="phAdditionalSection1000CustomFees">
				<tr>
					<td class="FieldLabel">1010</td>
					<td>
					    <ml:ComboBox id="sU3RsrvDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1000');" Width="145" MaxLength="21" ></ml:ComboBox>
					</td>
					<td class="FieldLabel"></td>
					<td><ml:moneytextbox id="sProU3Rsrv" runat="server" preset="money" width="76px"></ml:moneytextbox></td>
					<TD class="EscrowedColumn"><asp:CheckBox id="sU3RsrvEscrowedTri" runat="server" /></TD>
				    <TD class="RsrvMnthsLckdColumn"><asp:CheckBox id="sU3RsrvMonLckd" runat="server"/></TD>
				    <TD class="RsrvMnthsColumn"><asp:textbox id="sU3RsrvMon"  runat="server" Width="41" MaxLength="4"></asp:textbox></TD>
				    <TD><ml:moneytextbox id="sU3Rsrv" runat="server" preset="money" decimalDigits="2" ReadOnly="true"/></TD>
				    <uc1:GoodFaithEstimate2010RightColumn class="HiddenColumn" id="sU3RsrvProps_ctrl" Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
				</tr>
				<tr>
					<td class="FieldLabel">1011</td>
					<td>
					    <ml:ComboBox id="sU4RsrvDesc" runat="server" CssClass="FeeTypeComboBox" onchange="setFeeTypeProperties(this, '1000');" Width="145" MaxLength="21"></ml:ComboBox>
					</td>
					<td class="FieldLabel"></td>
					<td><ml:moneytextbox id="sProU4Rsrv" runat="server" preset="money" width="76px"></ml:moneytextbox></td>
					<TD class="EscrowedColumn"><asp:CheckBox id="sU4RsrvEscrowedTri" runat="server" /></TD>
				    <TD class="RsrvMnthsLckdColumn"><asp:CheckBox id="sU4RsrvMonLckd" runat="server"/></TD>
				    <TD class="RsrvMnthsColumn"><asp:textbox id="sU4RsrvMon"  runat="server" Width="41" MaxLength="4"></asp:textbox></TD>
				    <TD><ml:moneytextbox id="sU4Rsrv" runat="server" preset="money" decimalDigits="2" ReadOnly="true"/></TD>
                    <uc1:GoodFaithEstimate2010RightColumn class="HiddenColumn" id="sU4RsrvProps_ctrl" Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>				    
				</tr>
			</asp:PlaceHolder>
		
		</tbody>
        </table>
    </td>
    </tr>
    </tbody>
    </table>
    <table class="InsetBorder">
    <tbody>
    <tr>
    <td>
        <table>
            <thead></thead>
            <tbody>
                <tr>
                    <td width="26px" noWrap></td>
                    <td class="FieldLabel" width="100px" noWrap>
                        1st Payment date 
                    </td>
                    <td class="FieldLabel AlignRight" noWrap>
                        <asp:CheckBox runat="server" ID="sSchedDueD1Lckd" Text="Lock"/>
                    </td>
                    <td class="FieldLabel" noWrap><ml:DateTextBox id="sSchedDueD1" runat="server" width="75" CssClass="mask" preset="date" onchange="date_onblur(null, this);"></ml:DateTextBox></td>
                    <td colspan="2" width="206px" noWrap></td>
                    
                    <td colspan="2" class="FieldLabel" noWrap>
                        Aggregate Adjustment
                    </td>
                    <td class="FieldLabel AlignRight" noWrap>
                        <asp:CheckBox id="sAggregateAdjRsrvLckd" runat="server" Text="Lock" CssClass="FieldLabel"></asp:CheckBox>
                    </td>
                    <td noWrap>
                        <ml:moneytextbox id="sAggregateAdjRsrv" runat="server" preset="money" width="90px"></ml:moneytextbox>
                    </td>
                </tr>
                <tr>
                    <td noWrap></td>
                    <td colspan="3" noWrap>
                        <INPUT class="ButtonStyle" type="button" id="bEscrowAccountSetup" value="Setup Initial Escrow Account..." NoHighlight>
                    </td>
                    <td colspan="2" width="190px" noWrap></td>
                    <td colspan="3" class="FieldLabel" noWrap>
                        Total Escrow Collected at Closing
                    </td>
                    <td>
                        <ml:moneytextbox id="sGfeInitialImpoundDeposit" runat="server" ReadOnly="true" preset="money"></ml:moneytextbox>
                    </td>
                </tr>
                <tr id="aggCalcMode" runat="server">
                    <td nowrap>
                    </td>
                    <td colspan="3" class="FieldLabel">
                        Aggregate Escrow/Impounds Calculation Mode
                    </td>
                    <td>
                        <asp:DropDownList ID="sAggEscrowCalcModeT" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr id="customaryMin" runat="server">
                    <td nowra></td>
                    <td colspan="3" class="FieldLabel">Customary Escrow Impounds Calculation Minimum</td>
                    <td>
                        <asp:DropDownList ID="sCustomaryEscrowImpoundsCalcMinT" runat="server"></asp:DropDownList>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
    </tr>
    </tbody>
    </table>
    
    </form>
    
    <script type="text/javascript">
        $().ready(function() {
            // selector optimization
            var $rsrvsTable = $('#ReservesTable'); 
            var sSchedDueD1LckdElement = <%=AspxTools.JsGetElementById(sSchedDueD1Lckd)%>;
            var sAggregateAdjRsrvLckdElement = <%=AspxTools.JsGetElementById(sAggregateAdjRsrvLckd)%>;
            var sSchedDueD1Id = <%=AspxTools.JsGetClientIdString(sSchedDueD1) %>
            
            // calculations.  note calendar needs exclusion or double calculates.
            $('input,select').not('#'+sSchedDueD1Id).change(function(eventObject){
                refreshCalculation();
            });
            
            // row selection/deselection
            $rsrvsTable.find('input').blur(function() { unhighlightRow(this); }).focus(function() { highlightRow(this); });
            
            // locking
            $rsrvsTable.find('tbody > tr > td.RsrvMnthsLckdColumn input').click(function() {
                lockField(this, $(this).closest('tr').find('td.RsrvMnthsColumn input')[0].id);
            });

            $(sSchedDueD1LckdElement).click(function(){lockField(sSchedDueD1LckdElement, sSchedDueD1Id);});
            $(sAggregateAdjRsrvLckdElement).click(function(){lockField(sAggregateAdjRsrvLckdElement, 'sAggregateAdjRsrv');});
            
            // buttons
            $('#bEscrowAccountSetup').click(f_onsetupClick);        
        });
       function f_onsetupClick() {
          showModal('/newlos/Forms/InitialEscrowAccSetup.aspx?loanid=<%=AspxTools.HtmlString(LoanID) %>', null, null, null, function(args){ 
              if (args.OK) {
                $(<%= AspxTools.JsGetElementById(sSchedDueD1Lckd) %>).prop('checked', args.sSchedDueD1Lckd);
                $(<%= AspxTools.JsGetElementById(sSchedDueD1) %>).val(args.sSchedDueD1);
              }
              refreshCalculation();
          },{ hideCloseButton: true });
        }  
    </script>
</body>
</html>
