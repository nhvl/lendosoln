using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class AggregateEscrowDisclosure : LendersOfficeApp.newlos.BaseLoanPage
    {

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
            UseNewFramework = true;
            this.PageID = "AggregateEscrow";
            this.PageTitle = "Aggregate Escrow Account";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CAggregateEscrowDisclosurePDF);
            AggregateEscrowServicerZip.SmartZipcode(AggregateEscrowServicerCity, AggregateEscrowServicerState);
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(AggregateEscrowDisclosure));
            dataLoan.InitLoad();

            string sEscrowPmt = dataLoan.sEscrowPmt_rep;

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.AggregateEscrowServicer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AggregateEscrowServicerCompanyName.Text = preparer.CompanyName;
            AggregateEscrowServicerStreetAddr.Text = preparer.StreetAddr;
            AggregateEscrowServicerCity.Text = preparer.City;
            AggregateEscrowServicerZip.Text = preparer.Zip;
            AggregateEscrowServicerState.Value = preparer.State;
            AggregateEscrowServicerPhoneOfCompany.Text = preparer.PhoneOfCompany;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            GfeTilPrepareDate.Text = preparer.PrepareDate_rep;

            sSchedDueD1Lckd.Checked = dataLoan.sSchedDueD1Lckd;
            sSchedDueD1.Text = dataLoan.sSchedDueD1_rep;

            AggregateEscrowAccount escrowAccount = dataLoan.sAggregateEscrowAccount;
            AggregateEscrowAdjustment.Text = escrowAccount.AggregateEscrowAdjustment_rep;

            AggregateEscrowItem[] items = escrowAccount.AggregateItems;

            sAggrEscrowInitialDeposit.Text = escrowAccount.InitialDeposit_rep;

            sAggrEscrowMon0.Text = items[0].Mon;
            sEscrowPmt0.Text = items[0].PmtToEscrow_rep;
            sAggrEscrowPmtFromEscrow0.Text = items[0].PmtFromEscrow_rep;
            sAggrEscrowDesc0.Text = items[0].Desc;
            sAggrEscrowBal0.Text = items[0].Bal_rep;

            sAggrEscrowMon1.Text = items[1].Mon;
            sEscrowPmt1.Text = items[1].PmtToEscrow_rep;
            sAggrEscrowPmtFromEscrow1.Text = items[1].PmtFromEscrow_rep;
            sAggrEscrowDesc1.Text = items[1].Desc;
            sAggrEscrowBal1.Text = items[1].Bal_rep;

            sAggrEscrowMon2.Text = items[2].Mon;
            sEscrowPmt2.Text = items[2].PmtToEscrow_rep;
            sAggrEscrowPmtFromEscrow2.Text = items[2].PmtFromEscrow_rep;
            sAggrEscrowDesc2.Text = items[2].Desc;
            sAggrEscrowBal2.Text = items[2].Bal_rep;

            sAggrEscrowMon3.Text = items[3].Mon;
            sEscrowPmt3.Text = items[3].PmtToEscrow_rep;
            sAggrEscrowPmtFromEscrow3.Text = items[3].PmtFromEscrow_rep;
            sAggrEscrowDesc3.Text = items[3].Desc;
            sAggrEscrowBal3.Text = items[3].Bal_rep;

            sAggrEscrowMon4.Text = items[4].Mon;
            sEscrowPmt4.Text = items[4].PmtToEscrow_rep;
            sAggrEscrowPmtFromEscrow4.Text = items[4].PmtFromEscrow_rep;
            sAggrEscrowDesc4.Text = items[4].Desc;
            sAggrEscrowBal4.Text = items[4].Bal_rep;

            sAggrEscrowMon5.Text = items[5].Mon;
            sEscrowPmt5.Text = items[5].PmtToEscrow_rep;
            sAggrEscrowPmtFromEscrow5.Text = items[5].PmtFromEscrow_rep;
            sAggrEscrowDesc5.Text = items[5].Desc;
            sAggrEscrowBal5.Text = items[5].Bal_rep;

            sAggrEscrowMon6.Text = items[6].Mon;
            sEscrowPmt6.Text = items[6].PmtToEscrow_rep;
            sAggrEscrowPmtFromEscrow6.Text = items[6].PmtFromEscrow_rep;
            sAggrEscrowDesc6.Text = items[6].Desc;
            sAggrEscrowBal6.Text = items[6].Bal_rep;

            sAggrEscrowMon7.Text = items[7].Mon;
            sEscrowPmt7.Text = items[7].PmtToEscrow_rep;
            sAggrEscrowPmtFromEscrow7.Text = items[7].PmtFromEscrow_rep;
            sAggrEscrowDesc7.Text = items[7].Desc;
            sAggrEscrowBal7.Text = items[7].Bal_rep;

            sAggrEscrowMon8.Text = items[8].Mon;
            sEscrowPmt8.Text = items[8].PmtToEscrow_rep;
            sAggrEscrowPmtFromEscrow8.Text = items[8].PmtFromEscrow_rep;
            sAggrEscrowDesc8.Text = items[8].Desc;
            sAggrEscrowBal8.Text = items[8].Bal_rep;

            sAggrEscrowMon9.Text = items[9].Mon;
            sEscrowPmt9.Text = items[9].PmtToEscrow_rep;
            sAggrEscrowPmtFromEscrow9.Text = items[9].PmtFromEscrow_rep;
            sAggrEscrowDesc9.Text = items[9].Desc;
            sAggrEscrowBal9.Text = items[9].Bal_rep;

            sAggrEscrowMon10.Text = items[10].Mon;
            sEscrowPmt10.Text = items[10].PmtToEscrow_rep;
            sAggrEscrowPmtFromEscrow10.Text = items[10].PmtFromEscrow_rep;
            sAggrEscrowDesc10.Text = items[10].Desc;
            sAggrEscrowBal10.Text = items[10].Bal_rep;

            sAggrEscrowMon11.Text = items[11].Mon;
            sEscrowPmt11.Text = items[11].PmtToEscrow_rep;
            sAggrEscrowPmtFromEscrow11.Text = items[11].PmtFromEscrow_rep;
            sAggrEscrowDesc11.Text = items[11].Desc;
            sAggrEscrowBal11.Text = items[11].Bal_rep;
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
    }
}
