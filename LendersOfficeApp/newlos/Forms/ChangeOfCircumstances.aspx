﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangeOfCircumstances.aspx.cs"
    Inherits="LendersOfficeApp.newlos.Forms.ChangeOfCircumstances" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>Change Of Circumstances</title>
</head>
<body bgcolor="gainsboro">
<style type="text/css">

/*<%//Get rid of all other padding and margins%>*/
*
{
    margin: 0;
    padding: 0;
}

/*<%//And then add our own%>*/
#MainContent
{
    position: relative;
    left: 10px;
}
.HideBullets
{
    list-style: none;
}
#DateEntry
{
    padding-top: 10px;
}
#DateEntry li
{
    width: 300px;
    padding-bottom: 10px;
}

#DateEntry li label
{
    float: left;
    font-weight: bold;
    width: 150px;
    margin-right: 5px;
}

.AlignedDate
{
    height: 20px;
}

/*<%//The calendar control creates an img that's not well-aligned %>*/
#DateEntry img
{
    vertical-align: bottom;
}

#sCircumstanceChangeExplanation
{
    width: 99.5%;
    height: 260px;
}

#Explanation
{
    width: 550px;
    height: 300px;
}
#Reasons
{
    width: 550px;
}
#Reasons .ReasonAmt
{
    width: 70px;
}
#Reasons .ReasonText
{
    width: 405px;
}
#Reasons .ReasonLink
{
    width: 70px;	
}

#FeeChanges li
{
    margin-bottom: 5px
}
</style>

<script type="text/javascript">
var selectedTabIndex = <%= AspxTools.JsNumeric(selectedTabIndex) %>;
jQuery(function($)
{
    $('#dialog-confirm').dialog({
        modal: true,
        buttons: {
            "Continue": function() {
                $(this).dialog("close");
                saveAndApplyChangeOfCircumstancesForm();
            },
            "Cancel": function() {
                $(this).dialog("close");
            }
        },
        open: function() {
            $(this).parents('.ui-dialog-buttonpane button:eq(1)').focus();
        },
        autoOpen: false,
        closeOnEscape: false,
        width: "400",
        draggable: false,
        dialogClass: "LQBDialogBox"
    });
    
    var ExplanationCharacters = 600;
    var ReasonCharacters = 75;
    $("#ExplanationOfChange").keyup(MakeTextLimiter(ExplanationCharacters));

    $(".ReasonText").keyup(MakeTextLimiter(ReasonCharacters));
    
    //for some reason, print button is always disabled, enabled here
    $(":button").each(function(){
        if(this.value == "Print"){
            this.disabled = false;
            return false;
            }
    });

    function MakeTextLimiter(maxLength)
    {
        return (function()
        {
            var text = $(this).val();
            if (text.length > maxLength)
            {
                $(this).val(text.substr(0, maxLength));
            }
        });
    }
    
    <%if(IsTabCreateNew){ %>
        
        EnableDisableSave();
        window.onunload = saveChangeOfCircumstancesForm;
        
    <%} %>
    
    <%if(IsTabViewPast){ %>
        window.onunload = function(){};
    <%} %>
    
    <%if(IsArchivePage && HasCoCArchives){ %>
    <%} %>
    
    CheckIfApplyIsEnabled();
});

function selectFee(feePosition) {
    // First we open up the GFE fee picker and select a fee
    var path = '/newlos/Forms/GFEFeePicker.aspx';
    var url = path
        + '?loanid=' + <%= AspxTools.JsString(LoanID) %>
    showModal(url, null, null, null, function(modalResult){
        if (modalResult.OK) {
            var data = JSON.parse(modalResult.data);
            
            // Then we call a service that loads the value of that fee for us
            var args = {
                fieldId: data.FieldID,
                sLId: ML.sLId
            };
            var result = gService.loanedit.call("RetrieveGFEFee", args);
      
            // Then populate the appropriate text inputs
            if (!result.error) {
                $('input[id$=sCircumstanceChange' + feePosition + 'Amount_rep]').val(result.value.feeAmount);
                $('input[id$=sCircumstanceChange' + feePosition + 'Reason]').val(data.Name);
                $('input[id$=sCircumstanceChange' + feePosition + 'FeeId]').val(result.value.feeId);
                // alert($('input[id$=sCircumstanceChange' + feePosition + 'FeeId]').val());
                <%if(IsTabCreateNew){ %>
                    EnableDisableSave();
                <%} %>
                // Mark page as dirty
                updateDirtyBit();
            } else {
                alert(result.UserMessage);
            }
        }
    },{ hideCloseButton: true });
}

function clearFees() {
    $('.ReasonAmt').val('');
    $('.ReasonText').val('');
    updateDirtyBit();
    <%if(IsTabCreateNew){ %>
        EnableDisableSave();
    <%} %>
}

function onFeeClick() {
     clearFees();
     var feePosition = 1;
    
     $("input:checkbox:checked.lAmtFeeChk").each (function () {

    if ( feePosition <= 8 )
    {
         $('input[id$=sCircumstanceChange' + feePosition + 'Amount_rep]').val($(this).attr("data1"));
         $('input[id$=sCircumstanceChange' + feePosition + 'Reason]').val($(this).attr("data2"));
         $('input[id$=sCircumstanceChange' + feePosition + 'FeeId]').val($(this).attr("data3"));
        feePosition++;
    }
    });

     $("input:checkbox:checked.totLAmtFeeChk").each (function () {

    if ( feePosition <= 8 )
    {
         $('input[id$=sCircumstanceChange' + feePosition + 'Amount_rep]').val($(this).attr("data1"));
         $('input[id$=sCircumstanceChange' + feePosition + 'Reason]').val($(this).attr("data2"));
         $('input[id$=sCircumstanceChange' + feePosition + 'FeeId]').val($(this).attr("data3"));
        feePosition++;
    }
    });
    
     $("input:checkbox:checked.purchPriceFeeChk").each (function () {

    if ( feePosition <= 8 )
    {
         $('input[id$=sCircumstanceChange' + feePosition + 'Amount_rep]').val($(this).attr("data1"));
         $('input[id$=sCircumstanceChange' + feePosition + 'Reason]').val($(this).attr("data2"));
         $('input[id$=sCircumstanceChange' + feePosition + 'FeeId]').val($(this).attr("data3"));
        feePosition++;
    }
    });

     $("input:checkbox:checked.appraisalFeeChk").each (function () {

    if ( feePosition <= 8 )
    {
         $('input[id$=sCircumstanceChange' + feePosition + 'Amount_rep]').val($(this).attr("data1"));
         $('input[id$=sCircumstanceChange' + feePosition + 'Reason]').val($(this).attr("data2"));
         $('input[id$=sCircumstanceChange' + feePosition + 'FeeId]').val($(this).attr("data3"));
        feePosition++;
    }
    });

    
    $("input:checkbox:checked.feechk").each (function () {
    
    if ( feePosition <= 8 )
    {
       $('input[id$=sCircumstanceChange' + feePosition + 'Amount_rep]').val($(this).attr("data1"));
       $('input[id$=sCircumstanceChange' + feePosition + 'Reason]').val($(this).attr("data2"));
       $('input[id$=sCircumstanceChange' + feePosition + 'FeeId]').val($(this).attr("data3"));
      feePosition++;
    }
    });
}

function onLoanAmtClick()
{
    var setVal = $('#allLAmtFeeChk').prop('checked');
    $("input:checkbox.lAmtFeeChk").prop('checked', setVal);
    
    onFeeClick();
}

function onTotLoanAmtClick()
{
    var setVal = $('#allTotLAmtFeeChk').prop('checked');
    $("input:checkbox.totLAmtFeeChk").prop('checked', setVal);
    
    onFeeClick();
}

function onPurchPriceClick()
{
    var setVal = $('#allPurchPriceFeeChk').prop('checked');
    $("input:checkbox.purchPriceFeeChk").prop('checked', setVal);
    
    onFeeClick();
}

function onAppraisalClick()
{
    var setVal = $('#allAppraisalFeeChk').prop('checked');
    $("input:checkbox.appraisalFeeChk").prop('checked', setVal);
    
    onFeeClick();
}


<%if(IsTabCreateNew){ %>
function DisplayConfirmationDialog()
{
    if(AnyFeeCheckChecked())
    {
        saveAndApplyChangeOfCircumstancesForm();
    }
    else
    {
        $('#dialog-confirm').dialog('open');
    }
}

function saveAndApplyChangeOfCircumstancesForm()
{
    var args = getAllFormValues();
    args.updateAllTotalLoanAmount = $('#allTotLAmtFeeChk').prop('checked') || false;
    args.updateAllLoanAmount = $('#allLAmtFeeChk').prop('checked') || false;
    args.updateAllPurchPrice = $('#allPurchPriceFeeChk').prop('checked') || false;
    args.updateAllAppraisalFeeChk = $('#allAppraisalFeeChk').prop('checked') || false;
    args.sLId = ML.sLId;
    args.sFileVersion = ML.sFileVersion;
    var result = gService.loanedit.call("SaveAndApplyChangeOfCircumstancesForm", args); 
    if (!result.error) {
        // switch to the view past tab if the save succeeded.
        var gfew = window.opener.top.frames["body"];
        if(gfew && gfew.insertAndSelectGFEArchiveDropDown)
        {
            gfew.insertAndSelectGFEArchiveDropDown(result.value.sLastDisclosedGFEArchiveD, gfew);
        }
        window.onunload = function(){}; // don't save the postbacked UI, let it be cleared.
        <%=AspxTools.JsGetElementById(hfSavedCoC) %>.value = "true";
        //set the index to the past changes tab here maintab ref for vision
        __doPostBack('changeTab', '1');
    } else {
        alert(result.UserMessage);
        __doPostBack('clearData','');
    }  
}

function saveChangeOfCircumstancesForm(callback)
{
    var args = getAllFormValues();
    args.sLId = ML.sLId;
    var result = gService.loanedit.call("SaveChangeOfCircumstancesForm", args); 
    if (!result.error) {
        // don't need to do anything.
        
    } else {
        alert(result.UserMessage);
    }  
}

function EnableDisableSave()
{
    var enableApply = true;
   <%=AspxTools.JsGetElementById(bApply) %>.disabled = enableApply ? false : 'disabled';
}
<%} %>

function CheckIfApplyIsEnabled()
{
    var applyBtn = document.getElementById("bApply");
    if(applyBtn)
    {
        applyBtn.disabled = true;
    }
    else
    {
        return;
    }
        
    
    if(document.getElementById("GfePrepareDate").value &&
        document.getElementById("sCircumstanceChangeD").value &&
        <% if(!IsProtectDisclosureDates) { %>
        document.getElementById("sGfeRedisclosureD").value &&
        <% } %>
        document.getElementById("sCircumstanceChangeExplanation").value)
    {
        applyBtn.disabled = false;
    }
}

function AnyFeeCheckChecked()
{
        var numChecked = $("input:checkbox:checked.feechk").length
            + $("input:checkbox:checked.lAmtFeeChk").length
            + $("input:checkbox:checked.totLAmtFeeChk").length
            + $("input:checkbox:checked.purchPriceFeeChk").length
            + $("input:checkbox:checked.appraisalFeeChk").length;

    if(numChecked === 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

</script>

<form id="ChangeOfCircumstances" runat="server">

<div id="MainContent">
    <div class="MainRightHeader">
        <span>Change of Circumstances</span>
    </div>
    <%if(IsArchivePage){ %>
    <asp:HiddenField runat="server" ID="hfSavedCoC" Value="false" />

    <div id="dialog-confirm" title="Continue with no selected fees?">
        <img src="../../images/warn.png">
        <p style="position: absolute; width: 90%">
            No fees were selected for this Change of Circumstance. New disclosures will reflect the fees from the previous disclosure.
        </p>
    </div>
        
    <div class="Tabs">
        <ul class="tabnav">
            <li runat="server" id="tab0"><a href="#" onclick="__doPostBack('changeTab', '0');">Create New Change</a></li>
            <li runat="server" id="tab1"><a href="#" onclick="__doPostBack('changeTab', '1');">View Past Changes</a></li>
        </ul>
    </div>
    
    <%} %>
    <ul id="DateEntry" class="HideBullets">
        <%if (IsArchivePage && IsTabViewPast)
      { %>
        <li>
        <label class="DateEntryText">Select archived change:</label>
        <asp:DropDownList runat="server" ID="CoCArchives" AlwaysEnable onChange="__doPostBack('changeTab', '1');"></asp:DropDownList>
            <script type="text/javascript">
                function openCoCPDF() {
                    var args = encodeURI('ArchiveDate=' + <%=AspxTools.JsGetElementById(CoCArchives) %>.value);
                    var url = gVirtualRoot + '/pdf/Change Of Circumstances.aspx?loanid=' + ML.sLId + "&" + args;
                    LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
                    return false;

                }
		    </script>
        <input type="button" style="padding: 2px;" onclick="openCoCPDF();" value="Print" />
        </li>
    <%} %>
        <li id="GFE"><label class="DateEntryText" for="GfePrepareDate">Date of Good Faith Estimate</label>
            <%//Have to wrap these DateTextBoxes in a div, otherwise the text field 
              //gets separated from the date button %>
            <div class="AlignedDate">
                <ml:DateTextBox ID="GfePrepareDate" runat="server" Width="75" preset="date" CssClass="mask"
                    onblur="CheckIfApplyIsEnabled();" onchange="CheckIfApplyIsEnabled();">
                </ml:DateTextBox>
                <%if (IsTabCreateNew){ %><img src="../../images/require_icon.gif"><%} %>
            </div> 
        </li>
        <li id="Change"><label class="DateEntryText" for="sCircumstanceChangeD">Date of Change</label>
            <div class="AlignedDate">
                <ml:DateTextBox ID="sCircumstanceChangeD" runat="server" Width="75" preset="date"
                    CssClass="mask" onblur="CheckIfApplyIsEnabled();" onchange="CheckIfApplyIsEnabled();">
                </ml:DateTextBox>
                <%if (IsTabCreateNew){ %><img src="../../images/require_icon.gif"><%} %>
            </div>
        </li>
        <li id="GFE_Redisc"><label class="DateEntryText" for="sGfeRedisclosureD">Date of GFE Redisclosure </label>
            <div class="AlignedDate">
                <ml:DateTextBox ID="sGfeRedisclosureD" runat="server" Width="75" preset="date" CssClass="mask"
                    onblur="CheckIfApplyIsEnabled();" onchange="CheckIfApplyIsEnabled();">
                </ml:DateTextBox>
                <%if (IsTabCreateNew && !IsProtectDisclosureDates){ %><img src="../../images/require_icon.gif"><%} %>
            </div>
        </li>
    </ul>
    
    <div id="Explanation">
        <div class="FormTableSubheader">
            <span>Explanation of Change</span>
        </div>
        <textarea id="sCircumstanceChangeExplanation" style="float: left" runat="server"
            onblur="CheckIfApplyIsEnabled();"></textarea>
        <%if (IsTabCreateNew){ %><img style="position: absolute" src="../../images/require_icon.gif"><%} %>
    </div>
    <div id="Reasons">
        <div class="FormTableSubheader">
            <span>Fee changes associated with Changed Circumstances</span>
        </div>
        <ul id="FeeChanges" class="HideBullets" <% if(IsArchivePage && IsTabCreateNew) { %> style="display:none" <% } %> >
            <asp:Repeater ID="FeeChangeReasons" runat="server">
            <ItemTemplate>
            <li>
                <% //Have to put in both a name and an ID for the save routine to work. %>
                <input type="text" 
                    name="<%# AspxTools.HtmlString( "sCircumstanceChange" + (Container.ItemIndex + 1) + "Amount_rep" ) %>"
                    id="<%# AspxTools.HtmlString( "sCircumstanceChange" + (Container.ItemIndex + 1) + "Amount_rep" ) %>" class="ReasonAmt" 
                    value="<%# AspxTools.HtmlString( string.Format("{0:C}", ((CCReason)Container.DataItem).Amount)) %>"
                    readonly="true" />
                <input type="text" 
                    name="<%# AspxTools.HtmlString( "sCircumstanceChange" + (Container.ItemIndex + 1) + "Reason") %>" 
                    id="<%# AspxTools.HtmlString( "sCircumstanceChange" + (Container.ItemIndex + 1) + "Reason") %>" class="ReasonText" 
                    value="<%# AspxTools.HtmlString( ((CCReason)Container.DataItem).Reason) %>"
                    readonly="true" />
                <input type="hidden"
                    name="<%# AspxTools.HtmlString( "sCircumstanceChange" + (Container.ItemIndex + 1) + "FeeId") %>"
                    id="<%# AspxTools.HtmlString( "sCircumstanceChange" + (Container.ItemIndex + 1) + "FeeId") %>"
                    value="<%# AspxTools.HtmlString( ((CCReason)Container.DataItem).FeeID) %>" />
                <%if (!IsArchivePage || IsTabCreateNew)
                  { %>
                <a href="#" onclick="selectFee(<%# AspxTools.HtmlString( (Container.ItemIndex + 1).ToString() )%>);" class="ReasonLink">select fee</a>
                <%} %>
            </li>
            </ItemTemplate>
            </asp:Repeater>
        </ul>
        <%if(!IsArchivePage || IsTabCreateNew){ %>
           <%if(IsArchivePage && IsTabCreateNew){ %>
          <%} else { %>
          <a href="#" onclick="clearFees()">Clear Fees</a>
          <% } %>
        <%} %>
        
        <%if(IsArchivePage && IsTabCreateNew){ %>
        <div id="CalulatedReasons">
            <asp:PlaceHolder id="changeList" runat="server" ></asp:PlaceHolder>
        </div>
        <%} %>
        

        <%if(IsTabCreateNew){ %>
          <div align="center">
            <asp:Button ID="bApply" Style="padding: 2px;" disabled="disabled" runat="server"
                Text="Apply" OnClientClick="DisplayConfirmationDialog(); return false;" />
            <input type="button" style="padding: 2px;" value="Cancel" onclick="onClosePopup();" />
          </div>
        <%} %>
        
    </div>
    <br />
    <br />
</div>
</form>
    
</body>
</html>
