﻿namespace LendersOfficeApp.newlos.Forms
{
    using System;
    using DataAccess;

    public partial class LenderCredits : BaseLoanPage
    {
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            this.Init += new EventHandler(this.Page_Init);
        }

        protected void Page_Init(object sender, System.EventArgs e)
        {
            this.PageTitle = "Lender Credits / Discount";
            this.PageID = "LenderCredits";

            Tools.Bind_sLenderCreditCalculationMethodT(sLenderCreditCalculationMethodT);
            Tools.Bind_sLenderCreditMaxT(sLenderCreditMaxT);
            Tools.Bind_LenderCreditDiscloseLocationT(sLenderPaidFeeDiscloseLocationT);
            Tools.Bind_LenderCreditDiscloseLocationT(sLenderGeneralCreditDiscloseLocationT);
            Tools.Bind_LenderCreditDiscloseLocationT(sLenderCustomCredit1DiscloseLocationT);
            Tools.Bind_LenderCreditDiscloseLocationT(sLenderCustomCredit2DiscloseLocationT);
            Tools.Bind_sToleranceCureCalculationT(sToleranceCureCalculationT);
            Tools.Bind_PercentBaseLoanAmountsT(sLDiscntBaseT);
            Tools.Bind_sGfeCreditLenderPaidItemT(sGfeCreditLenderPaidItemT);
            Tools.Bind_PercentBaseLoanAmountsT(sLDiscntBaseT2);
        }

        protected bool sIsAllowExcludeToleranceCure;

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(LenderCredits));
            dataLoan.InitLoad();

            lnkToleranceCure.Visible = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;
            lnkToleranceCure2.Visible = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;

            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                sGfeCreditLenderPaidItemT.Items.Remove(sGfeCreditLenderPaidItemT.Items.FindByValue(E_CreditLenderPaidItemT.OriginatorCompensationOnly.ToString("D")));
            }

            if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy && Broker.EnableLenderCreditsWhenInLegacyClosingCostMode)
            {
                Tools.ReadonlifyDropDown(sLenderCreditCalculationMethodT);
            }

            bool hasOriginatorComp = dataLoan.sLenderPaidBrokerCompF_rep != "$0.00";

            // 4/1/2015 dd - Per case 209056 hide originator price when no lender paid comp.
            sBrokerLockFinalBrokComp1PcPrice.Visible = hasOriginatorComp;
            sBrokerLockFinalBrokComp1PcAmt.Visible = hasOriginatorComp;
            sLenderPaidBrokerCompPc.Visible = hasOriginatorComp;
            sLenderPaidBrokerCompF.Visible = hasOriginatorComp;
            sLenderPaidBrokerCompCreditF_Neg.Visible = dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID;
            originatorPriceLabel.Visible = hasOriginatorComp;
            lenderPaidCompLabel.Visible = hasOriginatorComp;
            lenderPaidCompCreditLabel.Visible = dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID;

            sBrokerLockFinalBrokComp1PcPrice2.Visible = hasOriginatorComp;
            sBrokerLockFinalBrokComp1PcAmt2.Visible = hasOriginatorComp;
            sLenderPaidBrokerCompPc2.Visible = hasOriginatorComp;
            sLenderPaidBrokerCompF2.Visible = hasOriginatorComp;
            sLenderPaidBrokerCompCreditF_Neg2.Visible = dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID;
            originatorPriceLabel2.Visible = hasOriginatorComp;
            lenderPaidCompLabel2.Visible = hasOriginatorComp;
            lenderPaidCompCreditLabel2.Visible = dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID;

            sLenderPaidBrokerCompF_Neg2_Label.Visible = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;
            sLenderPaidBrokerCompF_Neg2.Visible = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;
            sLenderPaidBrokerCompF_Neg_Label.Visible = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;
            sLenderPaidBrokerCompF_Neg.Visible = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;

            string sLenderCustomCredit1AmountAsCharge_rep = dataLoan.sLenderCustomCredit1AmountAsCharge_rep;
            string sLenderCustomCredit2AmountAsCharge_rep = dataLoan.sLenderCustomCredit2AmountAsCharge_rep;
            string sLenderPaidBrokerCompF_rep = dataLoan.sLenderPaidBrokerCompF_rep;
            string sLenderPaidBrokerCompF_Neg_rep = dataLoan.sLenderPaidBrokerCompF_Neg_rep;
            string sLenderPaidBrokerCompCreditF_Neg2_rep = dataLoan.sLenderPaidBrokerCompCreditF_Neg_rep;
            string sLenderPaidBrokerCompCreditF_Neg_rep = dataLoan.sLenderPaidBrokerCompCreditF_Neg_rep;

            Tools.SetDropDownListValue(sLDiscntBaseT, dataLoan.sLDiscntBaseT);
            sLDiscntBaseAmt.Text = dataLoan.sLDiscntBaseAmt_rep;

            Tools.SetDropDownListValue(sLenderCreditCalculationMethodT, dataLoan.sLenderCreditCalculationMethodT);

            bool oldBypass = dataLoan.ByPassFieldsBrokerLockAdjustXmlContent;
            dataLoan.ByPassFieldsBrokerLockAdjustXmlContent = true;

            sBrokerLockFinalBrokComp1PcPrice.Text = dataLoan.sBrokerLockFinalBrokComp1PcPrice_rep;
            sBrokerLockFinalBrokComp1PcAmt.Text = dataLoan.sBrokerLockFinalBrokComp1PcAmt_rep;
            sBrokerLockOriginatorPriceBrokComp1PcPrice.Text = dataLoan.sBrokerLockOriginatorPriceBrokComp1PcPrice;
            sBrokerLockOriginatorPriceBrokComp1PcAmt.Text = dataLoan.sBrokerLockOriginatorPriceBrokComp1PcAmt_rep;

            dataLoan.ByPassFieldsBrokerLockAdjustXmlContent = oldBypass;

            sLenderPaidBrokerCompF.Text = sLenderPaidBrokerCompF_rep;
            sLenderPaidBrokerCompF_Neg.Text = sLenderPaidBrokerCompF_Neg_rep;
            sLenderPaidBrokerCompF_Neg2.Text = sLenderPaidBrokerCompF_Neg_rep;
            sLenderPaidBrokerCompCreditF_Neg.Text = sLenderPaidBrokerCompCreditF_Neg_rep;
            sLenderPaidBrokerCompPc.Text = dataLoan.sLenderPaidBrokerCompPc_rep;

            Tools.SetDropDownListValue(sLenderCreditMaxT, dataLoan.sLenderCreditMaxT);
            if (dataLoan.sLenderCreditMaxT == E_sLenderCreditMaxT.NoLimit)
            {
                sLenderCreditMaxAmt_Neg.Text = "";
            }
            else
            {
                sLenderCreditMaxAmt_Neg.Text = dataLoan.sLenderCreditMaxAmt_Neg_rep;
            }

            Tools.SetDropDownListValue(sLenderPaidFeeDiscloseLocationT, dataLoan.sLenderPaidFeeDiscloseLocationT);
            Tools.SetDropDownListValue(sLenderGeneralCreditDiscloseLocationT, dataLoan.sLenderGeneralCreditDiscloseLocationT);

            Tools.SetDropDownListValue(sLenderCustomCredit1DiscloseLocationT, dataLoan.sLenderCustomCredit1DiscloseLocationT);
            sLenderCustomCredit1Description.Text = dataLoan.sLenderCustomCredit1Description;
            sLenderCustomCredit1Amount.Text = dataLoan.sLenderCustomCredit1Amount_rep;

            Tools.SetDropDownListValue(sLenderCustomCredit2DiscloseLocationT, dataLoan.sLenderCustomCredit2DiscloseLocationT);
            sLenderCustomCredit2Description.Text = dataLoan.sLenderCustomCredit2Description;
            sLenderCustomCredit2Amount.Text = dataLoan.sLenderCustomCredit2Amount_rep;

            sLenderCreditAvailableAmt_Neg.Text = dataLoan.sLenderCreditAvailableAmt_Neg_rep;
            sLenderPaidFeesAmt_Neg.Text = dataLoan.sLenderPaidFeesAmt_Neg_rep;
            sLenderGeneralCreditAmt_Neg.Text = dataLoan.sLenderGeneralCreditAmt_Neg_rep;

            sLenderTargetDiscountPointAmtAsCreditAmt_Neg.Text = dataLoan.sLenderTargetDiscountPointAmtAsCreditAmt_Neg_rep;
            sLenderInitialCreditAmt_Neg.Text = dataLoan.sLenderInitialCreditAmt_Neg_rep;
            sLenderTargetInitialCreditAmt_Neg.Text = dataLoan.sLenderTargetInitialCreditAmt_Neg_rep;

            sLenderLastDisclosedInitialCredit_Neg.Text = dataLoan.sLenderLastDisclosedInitialCredit_Neg_rep;
            sLenderCreditToleranceCureLckd.Checked = dataLoan.sLenderCreditToleranceCureLckd;
            sLenderCreditToleranceCure_Neg.Text = dataLoan.sLenderCreditToleranceCure_Neg_rep;
            sLenderActualInitialCreditAmt_Neg.Text = dataLoan.sLenderActualInitialCreditAmt_Neg_rep;
            sLenderActualInitialCreditAmt_Neg2.Text = dataLoan.sLenderActualInitialCreditAmt_Neg_rep;
            sLenderTargetClosingCreditAmt_Neg.Text = dataLoan.sLenderTargetClosingCreditAmt_Neg_rep;
            sLenderTargetTotalCreditAmt_Neg.Text = dataLoan.sLenderTargetTotalCreditAmt_Neg_rep;
            sLenderAdditionalCreditAtClosingAmt_Neg.Text = dataLoan.sLenderAdditionalCreditAtClosingAmt_Neg_rep;

            sToleranceZeroPercentCure.Text = dataLoan.sToleranceZeroPercentCure_rep;
            sToleranceZeroPercentCure_Neg.Text = dataLoan.sToleranceZeroPercentCure_Neg_rep;
            sToleranceZeroPercentCure_Neg.ReadOnly = true;
            sToleranceZeroPercentCureLckd.Checked = dataLoan.sToleranceZeroPercentCureLckd;

            sToleranceTenPercentCure.Text = dataLoan.sToleranceTenPercentCure_rep;
            sToleranceTenPercentCure_Neg.Text = dataLoan.sToleranceTenPercentCure_Neg_rep;
            sToleranceTenPercentCure_Neg.ReadOnly = true;
            sToleranceTenPercentCureLckd.Checked = dataLoan.sToleranceTenPercentCureLckd;

            sToleranceCure_Neg.Text = dataLoan.sToleranceCure_Neg_rep;
            sSettlementTotalPaidByLender_Neg.Text = dataLoan.sSettlementTotalPaidByLender_Neg_rep;
            sLenderTargetInitialCreditAmt_Neg2.Text = dataLoan.sLenderTargetInitialCreditAmt_Neg_rep;
            sLenderActualTotalCreditAmt_Neg.Text = dataLoan.sLenderActualTotalCreditAmt_Neg_rep;

            sLenderCreditDifferentFromTargetAmt_Neg.Text = dataLoan.sLenderCreditDifferentFromTargetAmt_Neg_rep;
            //sLenderCreditOverageAmt.Text = dataLoan.sLenderCreditOverageAmt_rep;
            //---- Load when calculation method is set manual.
            sBrokerLockFinalBrokComp1PcPrice2.Text = dataLoan.sBrokerLockFinalBrokComp1PcPrice_rep;
            sBrokerLockFinalBrokComp1PcAmt2.Text = dataLoan.sBrokerLockFinalBrokComp1PcAmt_rep;
            sLenderPaidBrokerCompPc2.Text = dataLoan.sLenderPaidBrokerCompPc_rep;
            sLenderPaidBrokerCompF2.Text = sLenderPaidBrokerCompF_rep;
            sLenderPaidBrokerCompCreditF_Neg2.Text = sLenderPaidBrokerCompCreditF_Neg_rep;
            sBrokerLockOriginatorPriceBrokComp1PcPrice2.Text = dataLoan.sBrokerLockOriginatorPriceBrokComp1PcPrice;
            sBrokerLockOriginatorPriceBrokComp1PcAmt2.Text = dataLoan.sBrokerLockOriginatorPriceBrokComp1PcAmt_rep;
            sLDiscntPc.Text = dataLoan.sLDiscntPc_rep;
            Tools.SetDropDownListValue(sLDiscntBaseT2, dataLoan.sLDiscntBaseT);
            sLDiscntFMb.Text = dataLoan.sLDiscntFMb_rep;
            sLDiscnt.Text = dataLoan.sLDiscnt_rep;
            Tools.SetDropDownListValue(sGfeCreditLenderPaidItemT, dataLoan.sGfeCreditLenderPaidItemT);
            sGfeCreditLenderPaidItemF.Text = dataLoan.sGfeCreditLenderPaidItemF_rep;
            sGfeLenderCreditF.Text = dataLoan.sGfeLenderCreditF_rep;
            sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt.Text = dataLoan.sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt_rep;
            sLenderInitialCreditToBorrowerAmt_Neg.Text = dataLoan.sLenderInitialCreditToBorrowerAmt_Neg_rep;

            sLenderCustomCredit1Description2.Text = dataLoan.sLenderCustomCredit1Description;
            sLenderCustomCredit1Amount2.Text = dataLoan.sLenderCustomCredit1Amount_rep;

            sLenderCustomCredit2Description2.Text = dataLoan.sLenderCustomCredit2Description;
            sLenderCustomCredit2Amount2.Text = dataLoan.sLenderCustomCredit2Amount_rep;
            sGfeDiscountPointFPc.Text = dataLoan.sGfeDiscountPointFPc_rep;
            sGfeDiscountPointF.Text = dataLoan.sGfeDiscountPointF_rep;
            sLenderAdditionalCreditAtClosingManualAmt_Neg.Text = dataLoan.sLenderAdditionalCreditAtClosingManualAmt_Neg_rep;
            sLenderAdditionalCreditAtClosingAmt_Neg2.Text = dataLoan.sLenderAdditionalCreditAtClosingAmt_Neg_rep;

            sToleranceZeroPercentCure2.Text = dataLoan.sToleranceZeroPercentCure_rep;
            sToleranceZeroPercentCure_Neg2.Text = dataLoan.sToleranceZeroPercentCure_Neg_rep;
            sToleranceZeroPercentCure_Neg2.ReadOnly = true;
            sToleranceZeroPercentCureLckd2.Checked = dataLoan.sToleranceZeroPercentCureLckd;

            sToleranceTenPercentCure2.Text = dataLoan.sToleranceTenPercentCure_rep;
            sToleranceTenPercentCure_Neg2.Text = dataLoan.sToleranceTenPercentCure_Neg_rep;
            sToleranceTenPercentCure_Neg2.ReadOnly = true;
            sToleranceTenPercentCureLckd2.Checked = dataLoan.sToleranceTenPercentCureLckd;

            sToleranceCure_Neg2.Text = dataLoan.sToleranceCure_Neg_rep;
            sSettlementTotalPaidByLender_Neg2.Text = dataLoan.sSettlementTotalPaidByLender_Neg_rep;
            sLenderActualTotalCreditAmt_Neg2.Text = dataLoan.sLenderActualTotalCreditAmt_Neg_rep;
            Tools.SetDropDownListValue(sToleranceCureCalculationT, dataLoan.sToleranceCureCalculationT);
            sIsAllowExcludeToleranceCure = dataLoan.sIsAllowExcludeToleranceCure;

            sLenderCustomCredit1AmountAsCharge.Text = sLenderCustomCredit1AmountAsCharge_rep;
            sLenderCustomCredit2AmountAsCharge.Text = sLenderCustomCredit2AmountAsCharge_rep;
            sLenderCustomCredit1AmountAsCharge2.Text = sLenderCustomCredit1AmountAsCharge_rep;
            sLenderCustomCredit2AmountAsCharge2.Text = sLenderCustomCredit2AmountAsCharge_rep;
        }
    }
}
