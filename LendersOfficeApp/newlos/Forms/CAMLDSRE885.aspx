﻿<%@ Register TagPrefix="uc1" TagName="CAMLDSRE885pg1" Src="CAMLDSRE885pg1.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CAMLDSRE885pg2" Src="CAMLDSRE885pg2.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CAMLDSRE885pg3" Src="CAMLDSRE885pg3.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CAMLDSRE885pg4" Src="CAMLDSRE885pg4.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CAMLDSRE885.aspx.cs" Inherits=" LendersOfficeApp.newlos.Forms.CAMLDSRE885" %>
<%@ Register TagPrefix="uc1" TagName="Tabs" Src="~/common/BaseTabPage.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml" >
  <head runat="server">
    <title>CAMLDS</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="FlowLayout" class="RightBackground" scroll="yes">
	<script type="text/javascript">
	    var oRolodex;
	    function _init() {
	        oRolodex = new cRolodex();

	        if (typeof (_initControl) == 'function') _initControl();
	    }
</script>

    <form id="CAMLDS" method="post" runat="server">
<TABLE id=Table1 cellSpacing=0 cellPadding=0 width="100%" border=0>
  <TR>
    <TD noWrap class="Tabs">
        <uc1:Tabs  runat="server" ID="Tabs" />
</TD></TR>
  <TR>
    <TD noWrap>
        <uc1:CAMLDSRE885pg1 id=CAMLDSRE885pg1 runat="server" enableviewstate="False"></uc1:CAMLDSRE885pg1>
        <uc1:CAMLDSRE885pg2 id=CAMLDSRE885pg2 runat="server"></uc1:CAMLDSRE885pg2>
        <uc1:CAMLDSRE885pg3 id=CAMLDSRE885pg3 runat="server"></uc1:CAMLDSRE885pg3>
        <uc1:CAMLDSRE885pg4 id=CAMLDSRE885pg4 runat="server"></uc1:CAMLDSRE885pg4>
    </TD>
  </TR></TABLE>

     </form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
	
  </body>
</html>
