using System;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class CreditDenialService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        private CPageData CreatePageData(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(CreditDenialService));
        }
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "LoadTab":
                    LoadTab();
                    break;
                case "SaveAndLoadTab":
                    SaveAndLoadTab();
                    break;
                case "SaveTab":
                    SaveTab();
                    break;
                case "CopyBorrowerDenialReasons":
                    CopyBorrowerDenialReasons();
                    break;
                case "BindAndLoadTab":
                    BindAndLoadTab();
                    break;
            }

        }
        private void SaveTab()
        {
            Guid sLId = GetGuid("LoanID");
            Guid aAppId = GetGuid("ApplicationID");
            bool isBorrower = GetBool("IsBorrower");
            int sFileVersion = GetInt("sFileVersion");
            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(aAppId);

            SaveTab(dataLoan, dataApp, isBorrower, sFileVersion);
            dataLoan.Save();

        }
        private void SaveAndLoadTab()
        {
            Guid sLId = GetGuid("LoanID");
            Guid aAppId = GetGuid("ApplicationID");
            bool isBorrower = GetBool("IsBorrower");
            bool isPreviousTabBorrower = !isBorrower;// 10/19/2010 dd - This represent previous tab.
            int sFileVersion = GetInt("sFileVersion");
            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(aAppId);

            SaveTab(dataLoan, dataApp, isPreviousTabBorrower, sFileVersion);
            dataLoan.Save();

            LoadTab(dataLoan, dataApp, isBorrower);
        }
        private void LoadTab()
        {
            Guid sLId = GetGuid("LoanID");
            Guid aAppId = GetGuid("ApplicationID");
            bool isBorrower = GetBool("IsBorrower");
            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(aAppId);

            LoadTab(dataLoan, dataApp, isBorrower);
        }

        private void SaveTab(CPageData dataLoan, CAppData dataApp, bool isBorrower, int sFileVersion)
        {
            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditDenialStatement, E_ReturnOptionIfNotExist.CreateNew);
            string sPreparerName = GetString("prepareBy");
            f.PreparerName = sPreparerName;
            f.CompanyName = GetString("BrokerName");
            f.StreetAddr = GetString("BrokerAddress");
            f.City = GetString("BrokerCity");
            f.State = GetString("BrokerState");
            f.Zip = GetString("BrokerZip");
            f.PhoneOfCompany = GetString("BrokerPhone");
            f.Update();

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.CreditReport, E_ReturnOptionIfNotExist.CreateNew);
            agent.CompanyName = GetString("CRAName");
            agent.StreetAddr = GetString("CRAAddress");
            agent.City = GetString("CRACity");
            agent.State = GetString("CRAState");
            agent.Zip = GetString("CRAZipcode");
            agent.Phone = GetString("CRAPhone");
            agent.Update();

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.CreditReportAgency2, E_ReturnOptionIfNotExist.CreateNew);
            agent.CompanyName = GetString("CRA2Name");
            agent.StreetAddr = GetString("CRA2Address");
            agent.City = GetString("CRA2City");
            agent.State = GetString("CRA2State");
            agent.Zip = GetString("CRA2Zipcode");
            agent.Phone = GetString("CRA2Phone");
            agent.Update();

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.CreditReportAgency3, E_ReturnOptionIfNotExist.CreateNew);
            agent.CompanyName = GetString("CRA3Name");
            agent.StreetAddr = GetString("CRA3Address");
            agent.City = GetString("CRA3City");
            agent.State = GetString("CRA3State");
            agent.Zip = GetString("CRA3Zipcode");
            agent.Phone = GetString("CRA3Phone");
            agent.Update(); 
            
            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.ECOA, E_ReturnOptionIfNotExist.CreateNew);
            agent.CompanyName = GetString("ECOA_CompanyName");
            agent.StreetAddr = GetString("ECOA_StreetAddr");
            agent.City = GetString("ECOA_City");
            agent.State = GetString("ECOA_State");
            agent.Zip = GetString("ECOA_Zip");
            agent.Update();

            dataLoan.sRejectD_rep = GetString("sRejectD");
            SaveHMDACreditDenialInfo(dataLoan, sPreparerName);

            bool setManually = false; 
            if (isBorrower)
            {
                dataApp.aDenialNoCreditFile = GetBool("aDenialNoCreditFile");
                dataApp.aDenialInsufficientCreditRef = GetBool("aDenialInsufficientCreditRef");
                dataApp.aDenialInsufficientCreditFile = GetBool("aDenialInsufficientCreditFile");
                dataApp.aDenialUnableVerifyCreditRef = GetBool("aDenialUnableVerifyCreditRef");
                dataApp.aDenialGarnishment = GetBool("aDenialGarnishment");
                dataApp.aDenialExcessiveObligations = GetBool("aDenialExcessiveObligations");
                dataApp.aDenialInsufficientIncome = GetBool("aDenialInsufficientIncome");
                dataApp.aDenialUnacceptablePmtRecord = GetBool("aDenialUnacceptablePmtRecord");
                dataApp.aDenialLackOfCashReserves = GetBool("aDenialLackOfCashReserves");
                dataApp.aDenialDeliquentCreditObligations = GetBool("aDenialDeliquentCreditObligations");
                dataApp.aDenialBankruptcy = GetBool("aDenialBankruptcy");
                dataApp.aDenialInfoFromConsumerReportAgency = GetBool("aDenialInfoFromConsumerReportAgency");
                dataApp.aDenialUnableVerifyEmployment = GetBool("aDenialUnableVerifyEmployment");
                dataApp.aDenialLenOfEmployment = GetBool("aDenialLenOfEmployment");
                dataApp.aDenialTemporaryEmployment = GetBool("aDenialTemporaryEmployment");
                dataApp.aDenialInsufficientIncomeForMortgagePmt = GetBool("aDenialInsufficientIncomeForMortgagePmt");
                dataApp.aDenialUnableVerifyIncome = GetBool("aDenialUnableVerifyIncome");
                dataApp.aDenialTempResidence = GetBool("aDenialTempResidence");
                dataApp.aDenialShortResidencePeriod = GetBool("aDenialShortResidencePeriod");
                dataApp.aDenialUnableVerifyResidence = GetBool("aDenialUnableVerifyResidence");
                dataApp.aDenialByHUD = GetBool("aDenialByHUD");
                dataApp.aDenialByVA = GetBool("aDenialByVA");
                dataApp.aDenialByFedNationalMortAssoc = GetBool("aDenialByFedNationalMortAssoc");
                dataApp.aDenialByFedHomeLoanMortCorp = GetBool("aDenialByFedHomeLoanMortCorp");
                dataApp.aDenialByOther = GetBool("aDenialByOther");
                dataApp.aDenialByOtherDesc = GetString("aDenialByOtherDesc");
                dataApp.aDenialInsufficientFundsToClose = GetBool("aDenialInsufficientFundsToClose");
                dataApp.aDenialCreditAppIncomplete = GetBool("aDenialCreditAppIncomplete");
                dataApp.aDenialInadequateCollateral = GetBool("aDenialInadequateCollateral");
                dataApp.aDenialUnacceptableProp = GetBool("aDenialUnacceptableProp");
                dataApp.aDenialInsufficientPropData = GetBool("aDenialInsufficientPropData");
                dataApp.aDenialUnacceptableAppraisal = GetBool("aDenialUnacceptableAppraisal");
                dataApp.aDenialUnacceptableLeasehold = GetBool("aDenialUnacceptableLeasehold");
                dataApp.aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds = GetBool("aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds");
                dataApp.aDenialWithdrawnByApp = GetBool("aDenialWithdrawnByApp");
                dataApp.aDenialOtherReason1 = GetBool("aDenialOtherReason1");
                dataApp.aDenialOtherReason1Desc = GetString("aDenialOtherReason1Desc");
                dataApp.aDenialOtherReason2 = GetBool("aDenialOtherReason2");
                dataApp.aDenialOtherReason2Desc = GetString("aDenialOtherReason2Desc");
                dataApp.aDenialDecisionBasedOnReportAgency = GetBool("aDenialDecisionBasedOnReportAgency");
                dataApp.aDenialDecisionBasedOnCRA = GetBool("aDenialDecisionBasedOnCRA");
                dataApp.aDenialAdditionalStatement = GetString("aDenialAdditionalStatement");                
                dataApp.aDenialWeObtainedCreditScoreFromCRA = GetBool("aDenialWeObtainedCreditScoreFromCRA");

                setManually = GetBool("aDenialCreditScoreLckd");
                dataApp.aDenialCreditScoreLckd = setManually;

                dataApp.aDenialCreditScore_rep = GetString("aDenialCreditScore");
                dataApp.aDenialCreditScoreD_rep = GetString("aDenialCreditScoreD");
                dataApp.aDenialCreditScoreRangeFrom_rep = GetString("aDenialCreditScoreRangeFrom");
                dataApp.aDenialCreditScoreRangeTo_rep = GetString("aDenialCreditScoreRangeTo");
                dataApp.aDenialCreditScoreFactor1 = GetString("aDenialCreditScoreFactor1");
                dataApp.aDenialCreditScoreFactor2 = GetString("aDenialCreditScoreFactor2");
                dataApp.aDenialCreditScoreFactor3 = GetString("aDenialCreditScoreFactor3");
                dataApp.aDenialCreditScoreFactor4 = GetString("aDenialCreditScoreFactor4");
                dataApp.aDenialIsFactorNumberOfRecentInquiries = GetBool("aDenialIsFactorNumberOfRecentInquiries");
                dataApp.aCrRd_rep = GetString("aCrRd");
            }
            else
            {
                dataApp.aCDenialNoCreditFile = GetBool("aDenialNoCreditFile");
                dataApp.aCDenialInsufficientCreditRef = GetBool("aDenialInsufficientCreditRef");
                dataApp.aCDenialInsufficientCreditFile = GetBool("aDenialInsufficientCreditFile");
                dataApp.aCDenialUnableVerifyCreditRef = GetBool("aDenialUnableVerifyCreditRef");
                dataApp.aCDenialGarnishment = GetBool("aDenialGarnishment");
                dataApp.aCDenialExcessiveObligations = GetBool("aDenialExcessiveObligations");
                dataApp.aCDenialInsufficientIncome = GetBool("aDenialInsufficientIncome");
                dataApp.aCDenialUnacceptablePmtRecord = GetBool("aDenialUnacceptablePmtRecord");
                dataApp.aCDenialLackOfCashReserves = GetBool("aDenialLackOfCashReserves");
                dataApp.aCDenialDeliquentCreditObligations = GetBool("aDenialDeliquentCreditObligations");
                dataApp.aCDenialBankruptcy = GetBool("aDenialBankruptcy");
                dataApp.aCDenialInfoFromConsumerReportAgency = GetBool("aDenialInfoFromConsumerReportAgency");
                dataApp.aCDenialUnableVerifyEmployment = GetBool("aDenialUnableVerifyEmployment");
                dataApp.aCDenialLenOfEmployment = GetBool("aDenialLenOfEmployment");
                dataApp.aCDenialTemporaryEmployment = GetBool("aDenialTemporaryEmployment");
                dataApp.aCDenialInsufficientIncomeForMortgagePmt = GetBool("aDenialInsufficientIncomeForMortgagePmt");
                dataApp.aCDenialUnableVerifyIncome = GetBool("aDenialUnableVerifyIncome");
                dataApp.aCDenialTempResidence = GetBool("aDenialTempResidence");
                dataApp.aCDenialShortResidencePeriod = GetBool("aDenialShortResidencePeriod");
                dataApp.aCDenialUnableVerifyResidence = GetBool("aDenialUnableVerifyResidence");
                dataApp.aCDenialByHUD = GetBool("aDenialByHUD");
                dataApp.aCDenialByVA = GetBool("aDenialByVA");
                dataApp.aCDenialByFedNationalMortAssoc = GetBool("aDenialByFedNationalMortAssoc");
                dataApp.aCDenialByFedHomeLoanMortCorp = GetBool("aDenialByFedHomeLoanMortCorp");
                dataApp.aCDenialByOther = GetBool("aDenialByOther");
                dataApp.aCDenialByOtherDesc = GetString("aDenialByOtherDesc");
                dataApp.aCDenialInsufficientFundsToClose = GetBool("aDenialInsufficientFundsToClose");
                dataApp.aCDenialCreditAppIncomplete = GetBool("aDenialCreditAppIncomplete");
                dataApp.aCDenialInadequateCollateral = GetBool("aDenialInadequateCollateral");
                dataApp.aCDenialUnacceptableProp = GetBool("aDenialUnacceptableProp");
                dataApp.aCDenialInsufficientPropData = GetBool("aDenialInsufficientPropData");
                dataApp.aCDenialUnacceptableAppraisal = GetBool("aDenialUnacceptableAppraisal");
                dataApp.aCDenialUnacceptableLeasehold = GetBool("aDenialUnacceptableLeasehold");
                dataApp.aCDenialNotGrantCreditToAnyAppOnSuchTermsAndConds = GetBool("aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds");
                dataApp.aCDenialWithdrawnByApp = GetBool("aDenialWithdrawnByApp");
                dataApp.aCDenialOtherReason1 = GetBool("aDenialOtherReason1");
                dataApp.aCDenialOtherReason1Desc = GetString("aDenialOtherReason1Desc");
                dataApp.aCDenialOtherReason2 = GetBool("aDenialOtherReason2");
                dataApp.aCDenialOtherReason2Desc = GetString("aDenialOtherReason2Desc");
                dataApp.aCDenialDecisionBasedOnReportAgency = GetBool("aDenialDecisionBasedOnReportAgency");
                dataApp.aCDenialDecisionBasedOnCRA = GetBool("aDenialDecisionBasedOnCRA");
                dataApp.aCDenialAdditionalStatement = GetString("aDenialAdditionalStatement");
                dataApp.aCDenialWeObtainedCreditScoreFromCRA = GetBool("aDenialWeObtainedCreditScoreFromCRA");

                setManually = GetBool("aDenialCreditScoreLckd");
                dataApp.aCDenialCreditScoreLckd = setManually;

                dataApp.aCDenialCreditScore_rep = GetString("aDenialCreditScore");
                dataApp.aCDenialCreditScoreD_rep = GetString("aDenialCreditScoreD");
                dataApp.aCDenialCreditScoreRangeFrom_rep = GetString("aDenialCreditScoreRangeFrom");
                dataApp.aCDenialCreditScoreRangeTo_rep = GetString("aDenialCreditScoreRangeTo");
                dataApp.aCDenialCreditScoreFactor1 = GetString("aDenialCreditScoreFactor1");
                dataApp.aCDenialCreditScoreFactor2 = GetString("aDenialCreditScoreFactor2");
                dataApp.aCDenialCreditScoreFactor3 = GetString("aDenialCreditScoreFactor3");
                dataApp.aCDenialCreditScoreFactor4 = GetString("aDenialCreditScoreFactor4");
                dataApp.aCDenialIsFactorNumberOfRecentInquiries = GetBool("aDenialIsFactorNumberOfRecentInquiries");
                dataApp.aCCrRd_rep = GetString("aCrRd");
            }

            if (setManually)
            {
                IPreparerFields scoreContact = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditDenialScoreContact, E_ReturnOptionIfNotExist.CreateNew);
                scoreContact.CompanyName = GetString("ScoreContactName");
                scoreContact.StreetAddr = GetString("ScoreContactAddress");
                scoreContact.City = GetString("ScoreContactCity");
                scoreContact.State = GetString("ScoreContactState");
                scoreContact.Zip = GetString("ScoreContactZip");
                scoreContact.PhoneOfCompany = GetString("ScoreContactPhone");
                scoreContact.Update();
            }
        }

        private void SaveHMDACreditDenialInfo(CPageData dataLoan, string sPreparerName)
        {
            string sHmdaDeniedFormDoneD_old = dataLoan.sHmdaDeniedFormDoneD_rep;
            dataLoan.sHmdaDeniedFormDoneD_rep = GetString("sHmdaDeniedFormDoneD");
            
            if (!string.Equals(sHmdaDeniedFormDoneD_old, dataLoan.sHmdaDeniedFormDoneD_rep))
            {
                bool bHasMailDate = !string.IsNullOrEmpty(dataLoan.sHmdaDeniedFormDoneD_rep);
                dataLoan.sHmdaDeniedFormDone = bHasMailDate;
                dataLoan.sHmdaDeniedFormDoneBy = bHasMailDate ? sPreparerName : String.Empty;
            }
        }

        private void LoadTab(CPageData dataLoan, CAppData dataApp, bool isBorrower)
        {
            if (isBorrower)
            {
                SetResult("aDenialNoCreditFile", dataApp.aDenialNoCreditFile);
                SetResult("aDenialInsufficientCreditRef", dataApp.aDenialInsufficientCreditRef);
                SetResult("aDenialInsufficientCreditFile", dataApp.aDenialInsufficientCreditFile);
                SetResult("aDenialUnableVerifyCreditRef", dataApp.aDenialUnableVerifyCreditRef);
                SetResult("aDenialGarnishment", dataApp.aDenialGarnishment);
                SetResult("aDenialExcessiveObligations", dataApp.aDenialExcessiveObligations);
                SetResult("aDenialInsufficientIncome", dataApp.aDenialInsufficientIncome);
                SetResult("aDenialUnacceptablePmtRecord", dataApp.aDenialUnacceptablePmtRecord);
                SetResult("aDenialLackOfCashReserves", dataApp.aDenialLackOfCashReserves);
                SetResult("aDenialDeliquentCreditObligations", dataApp.aDenialDeliquentCreditObligations);
                SetResult("aDenialBankruptcy", dataApp.aDenialBankruptcy);
                SetResult("aDenialInfoFromConsumerReportAgency", dataApp.aDenialInfoFromConsumerReportAgency);
                SetResult("aDenialUnableVerifyEmployment", dataApp.aDenialUnableVerifyEmployment);
                SetResult("aDenialLenOfEmployment", dataApp.aDenialLenOfEmployment);
                SetResult("aDenialTemporaryEmployment", dataApp.aDenialTemporaryEmployment);
                SetResult("aDenialInsufficientIncomeForMortgagePmt", dataApp.aDenialInsufficientIncomeForMortgagePmt);
                SetResult("aDenialUnableVerifyIncome", dataApp.aDenialUnableVerifyIncome);
                SetResult("aDenialTempResidence", dataApp.aDenialTempResidence);
                SetResult("aDenialShortResidencePeriod", dataApp.aDenialShortResidencePeriod);
                SetResult("aDenialUnableVerifyResidence", dataApp.aDenialUnableVerifyResidence);
                SetResult("aDenialByHUD", dataApp.aDenialByHUD);
                SetResult("aDenialByVA", dataApp.aDenialByVA);
                SetResult("aDenialByFedNationalMortAssoc", dataApp.aDenialByFedNationalMortAssoc);
                SetResult("aDenialByFedHomeLoanMortCorp", dataApp.aDenialByFedHomeLoanMortCorp);
                SetResult("aDenialByOther", dataApp.aDenialByOther);
                SetResult("aDenialByOtherDesc", dataApp.aDenialByOtherDesc);
                SetResult("aDenialInsufficientFundsToClose", dataApp.aDenialInsufficientFundsToClose);
                SetResult("aDenialCreditAppIncomplete", dataApp.aDenialCreditAppIncomplete);
                SetResult("aDenialInadequateCollateral", dataApp.aDenialInadequateCollateral);
                SetResult("aDenialUnacceptableProp", dataApp.aDenialUnacceptableProp);
                SetResult("aDenialInsufficientPropData", dataApp.aDenialInsufficientPropData);
                SetResult("aDenialUnacceptableAppraisal", dataApp.aDenialUnacceptableAppraisal);
                SetResult("aDenialUnacceptableLeasehold", dataApp.aDenialUnacceptableLeasehold);
                SetResult("aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds", dataApp.aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds);
                SetResult("aDenialWithdrawnByApp", dataApp.aDenialWithdrawnByApp);
                SetResult("aDenialOtherReason1", dataApp.aDenialOtherReason1);
                SetResult("aDenialOtherReason1Desc", dataApp.aDenialOtherReason1Desc);
                SetResult("aDenialOtherReason2", dataApp.aDenialOtherReason2);
                SetResult("aDenialOtherReason2Desc", dataApp.aDenialOtherReason2Desc);
                SetResult("aDenialDecisionBasedOnReportAgency", dataApp.aDenialDecisionBasedOnReportAgency);
                SetResult("aDenialDecisionBasedOnCRA", dataApp.aDenialDecisionBasedOnCRA);
                SetResult("aDenialAdditionalStatement", dataApp.aDenialAdditionalStatement);
                SetResult("aDenialWeObtainedCreditScoreFromCRA", dataApp.aDenialWeObtainedCreditScoreFromCRA);
                SetResult("aDenialCreditScoreLckd", dataApp.aDenialCreditScoreLckd);
                SetResult("aDenialCreditScore", dataApp.aDenialCreditScore_rep);
                SetResult("aDenialCreditScoreD", dataApp.aDenialCreditScoreD_rep);
                SetResult("aDenialCreditScoreRangeFrom", dataApp.aDenialCreditScoreRangeFrom_rep);
                SetResult("aDenialCreditScoreRangeTo", dataApp.aDenialCreditScoreRangeTo_rep);
                SetResult("aDenialCreditScoreFactor1", dataApp.aDenialCreditScoreFactor1);
                SetResult("aDenialCreditScoreFactor2", dataApp.aDenialCreditScoreFactor2);
                SetResult("aDenialCreditScoreFactor3", dataApp.aDenialCreditScoreFactor3);
                SetResult("aDenialCreditScoreFactor4", dataApp.aDenialCreditScoreFactor4);
                SetResult("aDenialIsFactorNumberOfRecentInquiries", dataApp.aDenialIsFactorNumberOfRecentInquiries);
                SetResult("aCrRd", dataApp.aCrRd_rep);

                IPreparerFields f = dataApp.aDenialCreditScoreContact;
                SetResult("ScoreContactName", f.CompanyName);
                SetResult("ScoreContactAddress", f.StreetAddr);
                SetResult("ScoreContactCity", f.City);
                SetResult("ScoreContactState", f.State);
                SetResult("ScoreContactZip", f.Zip);
                SetResult("ScoreContactPhone", f.PhoneOfCompany);
            }
            else
            {
                SetResult("aDenialNoCreditFile", dataApp.aCDenialNoCreditFile);
                SetResult("aDenialInsufficientCreditRef", dataApp.aCDenialInsufficientCreditRef);
                SetResult("aDenialInsufficientCreditFile", dataApp.aCDenialInsufficientCreditFile);
                SetResult("aDenialUnableVerifyCreditRef", dataApp.aCDenialUnableVerifyCreditRef);
                SetResult("aDenialGarnishment", dataApp.aCDenialGarnishment);
                SetResult("aDenialExcessiveObligations", dataApp.aCDenialExcessiveObligations);
                SetResult("aDenialInsufficientIncome", dataApp.aCDenialInsufficientIncome);
                SetResult("aDenialUnacceptablePmtRecord", dataApp.aCDenialUnacceptablePmtRecord);
                SetResult("aDenialLackOfCashReserves", dataApp.aCDenialLackOfCashReserves);
                SetResult("aDenialDeliquentCreditObligations", dataApp.aCDenialDeliquentCreditObligations);
                SetResult("aDenialBankruptcy", dataApp.aCDenialBankruptcy);
                SetResult("aDenialInfoFromConsumerReportAgency", dataApp.aCDenialInfoFromConsumerReportAgency);
                SetResult("aDenialUnableVerifyEmployment", dataApp.aCDenialUnableVerifyEmployment);
                SetResult("aDenialLenOfEmployment", dataApp.aCDenialLenOfEmployment);
                SetResult("aDenialTemporaryEmployment", dataApp.aCDenialTemporaryEmployment);
                SetResult("aDenialInsufficientIncomeForMortgagePmt", dataApp.aCDenialInsufficientIncomeForMortgagePmt);
                SetResult("aDenialUnableVerifyIncome", dataApp.aCDenialUnableVerifyIncome);
                SetResult("aDenialTempResidence", dataApp.aCDenialTempResidence);
                SetResult("aDenialShortResidencePeriod", dataApp.aCDenialShortResidencePeriod);
                SetResult("aDenialUnableVerifyResidence", dataApp.aCDenialUnableVerifyResidence);
                SetResult("aDenialByHUD", dataApp.aCDenialByHUD);
                SetResult("aDenialByVA", dataApp.aCDenialByVA);
                SetResult("aDenialByFedNationalMortAssoc", dataApp.aCDenialByFedNationalMortAssoc);
                SetResult("aDenialByFedHomeLoanMortCorp", dataApp.aCDenialByFedHomeLoanMortCorp);
                SetResult("aDenialByOther", dataApp.aCDenialByOther);
                SetResult("aDenialByOtherDesc", dataApp.aCDenialByOtherDesc);
                SetResult("aDenialInsufficientFundsToClose", dataApp.aCDenialInsufficientFundsToClose);
                SetResult("aDenialCreditAppIncomplete", dataApp.aCDenialCreditAppIncomplete);
                SetResult("aDenialInadequateCollateral", dataApp.aCDenialInadequateCollateral);
                SetResult("aDenialUnacceptableProp", dataApp.aCDenialUnacceptableProp);
                SetResult("aDenialInsufficientPropData", dataApp.aCDenialInsufficientPropData);
                SetResult("aDenialUnacceptableAppraisal", dataApp.aCDenialUnacceptableAppraisal);
                SetResult("aDenialUnacceptableLeasehold", dataApp.aCDenialUnacceptableLeasehold);
                SetResult("aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds", dataApp.aCDenialNotGrantCreditToAnyAppOnSuchTermsAndConds);
                SetResult("aDenialWithdrawnByApp", dataApp.aCDenialWithdrawnByApp);
                SetResult("aDenialOtherReason1", dataApp.aCDenialOtherReason1);
                SetResult("aDenialOtherReason1Desc", dataApp.aCDenialOtherReason1Desc);
                SetResult("aDenialOtherReason2", dataApp.aCDenialOtherReason2);
                SetResult("aDenialOtherReason2Desc", dataApp.aCDenialOtherReason2Desc);
                SetResult("aDenialDecisionBasedOnReportAgency", dataApp.aCDenialDecisionBasedOnReportAgency);
                SetResult("aDenialDecisionBasedOnCRA", dataApp.aCDenialDecisionBasedOnCRA);
                SetResult("aDenialAdditionalStatement", dataApp.aCDenialAdditionalStatement);
                SetResult("aDenialWeObtainedCreditScoreFromCRA", dataApp.aCDenialWeObtainedCreditScoreFromCRA);
                SetResult("aDenialCreditScoreLckd", dataApp.aCDenialCreditScoreLckd);
                SetResult("aDenialCreditScore", dataApp.aCDenialCreditScore_rep);
                SetResult("aDenialCreditScoreD", dataApp.aCDenialCreditScoreD_rep);
                SetResult("aDenialCreditScoreRangeFrom", dataApp.aCDenialCreditScoreRangeFrom_rep);
                SetResult("aDenialCreditScoreRangeTo", dataApp.aCDenialCreditScoreRangeTo_rep);
                SetResult("aDenialCreditScoreFactor1", dataApp.aCDenialCreditScoreFactor1);
                SetResult("aDenialCreditScoreFactor2", dataApp.aCDenialCreditScoreFactor2);
                SetResult("aDenialCreditScoreFactor3", dataApp.aCDenialCreditScoreFactor3);
                SetResult("aDenialCreditScoreFactor4", dataApp.aCDenialCreditScoreFactor4);
                SetResult("aDenialIsFactorNumberOfRecentInquiries", dataApp.aCDenialIsFactorNumberOfRecentInquiries);
                SetResult("aCrRd", dataApp.aCCrRd_rep);

                IPreparerFields f = dataApp.aCDenialCreditScoreContact;
                SetResult("ScoreContactName", f.CompanyName);
                SetResult("ScoreContactAddress", f.StreetAddr);
                SetResult("ScoreContactCity", f.City);
                SetResult("ScoreContactState", f.State);
                SetResult("ScoreContactZip", f.Zip);
                SetResult("ScoreContactPhone", f.PhoneOfCompany);
            }
        }
        private void CopyBorrowerDenialReasons()
        {
            Guid sLId = GetGuid("LoanID");
            Guid aAppId = GetGuid("ApplicationID");
            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(aAppId);

            SetResult("aDenialNoCreditFile", dataApp.aDenialNoCreditFile);
            SetResult("aDenialInsufficientCreditRef", dataApp.aDenialInsufficientCreditRef);
            SetResult("aDenialInsufficientCreditFile", dataApp.aDenialInsufficientCreditFile);
            SetResult("aDenialUnableVerifyCreditRef", dataApp.aDenialUnableVerifyCreditRef);
            SetResult("aDenialGarnishment", dataApp.aDenialGarnishment);
            SetResult("aDenialExcessiveObligations", dataApp.aDenialExcessiveObligations);
            SetResult("aDenialInsufficientIncome", dataApp.aDenialInsufficientIncome);
            SetResult("aDenialUnacceptablePmtRecord", dataApp.aDenialUnacceptablePmtRecord);
            SetResult("aDenialLackOfCashReserves", dataApp.aDenialLackOfCashReserves);
            SetResult("aDenialDeliquentCreditObligations", dataApp.aDenialDeliquentCreditObligations);
            SetResult("aDenialBankruptcy", dataApp.aDenialBankruptcy);
            SetResult("aDenialInfoFromConsumerReportAgency", dataApp.aDenialInfoFromConsumerReportAgency);
            SetResult("aDenialUnableVerifyEmployment", dataApp.aDenialUnableVerifyEmployment);
            SetResult("aDenialLenOfEmployment", dataApp.aDenialLenOfEmployment);
            SetResult("aDenialTemporaryEmployment", dataApp.aDenialTemporaryEmployment);
            SetResult("aDenialInsufficientIncomeForMortgagePmt", dataApp.aDenialInsufficientIncomeForMortgagePmt);
            SetResult("aDenialUnableVerifyIncome", dataApp.aDenialUnableVerifyIncome);
            SetResult("aDenialTempResidence", dataApp.aDenialTempResidence);
            SetResult("aDenialShortResidencePeriod", dataApp.aDenialShortResidencePeriod);
            SetResult("aDenialUnableVerifyResidence", dataApp.aDenialUnableVerifyResidence);
            SetResult("aDenialByHUD", dataApp.aDenialByHUD);
            SetResult("aDenialByVA", dataApp.aDenialByVA);
            SetResult("aDenialByFedNationalMortAssoc", dataApp.aDenialByFedNationalMortAssoc);
            SetResult("aDenialByFedHomeLoanMortCorp", dataApp.aDenialByFedHomeLoanMortCorp);
            SetResult("aDenialByOther", dataApp.aDenialByOther);
            SetResult("aDenialByOtherDesc", dataApp.aDenialByOtherDesc);
            SetResult("aDenialInsufficientFundsToClose", dataApp.aDenialInsufficientFundsToClose);
            SetResult("aDenialCreditAppIncomplete", dataApp.aDenialCreditAppIncomplete);
            SetResult("aDenialInadequateCollateral", dataApp.aDenialInadequateCollateral);
            SetResult("aDenialUnacceptableProp", dataApp.aDenialUnacceptableProp);
            SetResult("aDenialInsufficientPropData", dataApp.aDenialInsufficientPropData);
            SetResult("aDenialUnacceptableAppraisal", dataApp.aDenialUnacceptableAppraisal);
            SetResult("aDenialUnacceptableLeasehold", dataApp.aDenialUnacceptableLeasehold);
            SetResult("aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds", dataApp.aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds);
            SetResult("aDenialWithdrawnByApp", dataApp.aDenialWithdrawnByApp);
            SetResult("aDenialOtherReason1", dataApp.aDenialOtherReason1);
            SetResult("aDenialOtherReason1Desc", dataApp.aDenialOtherReason1Desc);
            SetResult("aDenialOtherReason2", dataApp.aDenialOtherReason2);
            SetResult("aDenialOtherReason2Desc", dataApp.aDenialOtherReason2Desc);
        }

        private void BindAndLoadTab()
        {
            Guid sLId = GetGuid("LoanID");
            Guid aAppId = GetGuid("ApplicationID");
            bool isBorrower = GetBool("IsBorrower");
            int sFileVersion = GetInt("sFileVersion");
            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(aAppId);

            SaveTab(dataLoan, dataApp, isBorrower, sFileVersion);
            LoadTab(dataLoan, dataApp, isBorrower);
        }
    }
}
