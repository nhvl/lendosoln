namespace LendersOfficeApp.newlos.Forms
{
    using System;
    using DataAccess;

    public partial class TruthInLending32 : LendersOfficeApp.newlos.BaseLoanPage
	{

        protected void PageInit(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            this.PageTitle = "Truth In Lending Section 32";
            this.PageID= "TIL32";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CTruthInLending32PDF);

        }

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(TruthInLending32));
            dataLoan.InitLoad();

            sLAmtCalc.Text = dataLoan.sLAmtCalc_rep;
            sApr.Text = dataLoan.sApr_rep;
            sSchedPmt1.Text = dataLoan.sSchedPmt1_rep;
            sFinalBalloonPmt.Text = dataLoan.sFinalBalloonPmt_rep;
            sBalloonPmt.Checked = dataLoan.sBalloonPmt;
            sBalloonPmt.Enabled = dataLoan.sGfeIsBalloonLckd;
            Tools.SetDropDownListValue(sCreditInsIncludedTri, dataLoan.sCreditInsIncludedTri);
            sHasVarRFeature.Checked = dataLoan.sHasVarRFeature;

			MaxPayment.Text = "";
            if (dataLoan.sHasVarRFeature) 
            {
                bool groupAmortTableEntries = true;
                var worstCaseAmort = dataLoan.GetAmortTable(
                    E_AmortizationScheduleT.WorstCase,
                    groupAmortTableEntries);

                decimal maxPayment = 0.0M;

				try
				{
					for (int i = 0; i < worstCaseAmort.nRows; i++) 
					{
						maxPayment = Math.Max(maxPayment, worstCaseAmort.Items[i].Pmt);
					}
					MaxPayment.Text = dataLoan.m_convertLos.ToMoneyString(maxPayment, FormatDirection.ToRep);
				}
				catch
				{}
            } 
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
