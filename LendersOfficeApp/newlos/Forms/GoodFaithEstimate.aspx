<%@ Register TagPrefix="uc1" TagName="GoodFaithEstimateRightColumn" Src="../../los/LegalForm/GoodFaithEstimateRightColumn.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="GoodFaithEstimate.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.GoodFaithEstimate" enableViewState="False"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import namespace="DataAccess"%>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
		<title>GoodFaithEstimate</title>
  </HEAD>
	<body bgcolor="gainsboro" MS_POSITIONING="FlowLayout">
    <script type="text/javascript">
  var bIsFirst = true;
  var oRolodex = null;  
    
  function findCCTemplate() {
    // Before choosing a closing cost template, you always need to save the loan.
    PolyShouldShowConfirmSave(isDirty(), function(){
		showModal('/los/view/closingcostlist.aspx?gfeversion=0&loanid=' + <%= AspxTools.JsString(LoanID)%>, null, null, null, function(args){ 
			if (args.OK) {
				self.location = self.location; // Refresh;
			}
		},{ hideCloseButton: true });
	}, saveMe);
  }
  function _init() {
    if (null == oRolodex)
      oRolodex = new cRolodex();
        
    lockField(<%= AspxTools.JsGetElementById(sTotCcPbsLocked) %>, 'sTotCcPbs');
    lockField(<%= AspxTools.JsGetElementById(sRefPdOffAmt1003Lckd) %>, 'sRefPdOffAmt1003');
    lockField(<%= AspxTools.JsGetElementById(sTotEstPp1003Lckd) %>, 'sTotEstPp1003');
    lockField(<%= AspxTools.JsGetElementById(sTotEstCc1003Lckd) %>, 'sTotEstCcNoDiscnt1003');
    lockField(<%= AspxTools.JsGetElementById(sFfUfmip1003Lckd) %>, 'sFfUfmip1003');
    lockField(<%= AspxTools.JsGetElementById(sLDiscnt1003Lckd) %>, 'sLDiscnt1003');
    lockField(<%= AspxTools.JsGetElementById(sTransNetCashLckd) %>, 'sTransNetCash');
    lockField(<%= AspxTools.JsGetElementById(sOCredit1Lckd) %>, 'sOCredit1Amt');
    lockField(<%= AspxTools.JsGetElementById(sOCredit1Lckd) %>, 'sOCredit1Desc');
    lockField(<%= AspxTools.JsGetElementById(sBrokComp1Lckd) %>, 'sBrokComp1');
    lockField(<%= AspxTools.JsGetElementById(sAggregateAdjRsrvLckd) %>, 'sAggregateAdjRsrv');
    lockField(<%= AspxTools.JsGetElementById(sIPerDayLckd) %>, 'sIPerDay');
    lockField(<%= AspxTools.JsGetElementById(sLAmtLckd) %>, 'sLAmtCalc');
	lockField(<%= AspxTools.JsGetElementById(sIPiaDyLckd) %>, 'sIPiaDy');
    lockField(<%= AspxTools.JsGetElementById(sAltCostLckd) %>, 'sAltCost');
    bIsAutoCalculate = document.forms[0].<%= AspxTools.ClientId(ByPassBgCalcForGfeAsDefault) %>[0].checked;
    document.getElementById("btnCalculate").disabled = bIsAutoCalculate;
     
    if (bIsFirst) {
      // Only init one time.
      bIsFirst = false;
      var length = document.forms[0].<%= AspxTools.ClientId(ByPassBgCalcForGfeAsDefault) %>.length;
      
        for (var i = 0; i < length; i++)
            {
            addEventHandler(document.forms[0].<%= AspxTools.ClientId(ByPassBgCalcForGfeAsDefault) %>[i], "click", backgroundCalculation, false);
        }
    
      <%= AspxTools.JsGetElementById(GfeTilCompanyName) %>.focus();
    }  



  }
 
 
  function OnPaidChecking(obj)
  {
	if ( obj.id == "sApprFPaid" && obj.checked ) 
		document.getElementById("sApprFProps_ctrl_Poc_chk").checked = true;
	if (  obj.id == "sCrFPaid" && obj.checked ) 
		document.getElementById("sCrFProps_ctrl_Poc_chk").checked =  true;
	if (  obj.id == "sProcFPaid" && obj.checked ) 
		document.getElementById("sProcFProps_ctrl_Poc_chk").checked = true;
  }
  
  function OnPocCheck(obj) 
  {
	if( obj.id ==  "sApprFProps_ctrl_Poc_chk"  && obj.checked ) 
		document.getElementById("sApprFPaid").checked = true ;
	if( obj.id == "sCrFProps_ctrl_Poc_chk"  && obj.checked )
		document.getElementById("sCrFPaid").checked = true;
	if( obj.id ==  "sProcFProps_ctrl_Poc_chk"  && obj.checked ) 
		 document.getElementById("sProcFPaid").checked = true;
  }

  var openedDate = <%=AspxTools.JsString(m_openedDate)%>;
  function onDateKeyUp(o, event) {
    if (event.keyCode == 79) {
      o.value = openedDate;
      updateDirtyBit(event);
    }
  }
  

	function _alertUser(obj)
	{
		var control = <%= AspxTools.JsGetElementById(sBrokComp1Pc) %>;
		if((control.readOnly == true) || !obj)
			return;
		
		for (property in obj)
		{
			if(obj[property] == <%=AspxTools.JsString(ErrorMessages.RebateCannotBeChangedDuringRateLock)%>)
			{
				if(control)
				{
					alert(<%=AspxTools.JsString(JsMessages.WarnUserRebateCannotBeChangedDuringRateLock)%>);
					control.style.backgroundColor = "gainsboro";
					control.readOnly = true;
				}
			}
		}
	}
	
	function checksBrokComp1Pc(control)
	{
		//OPM 8935 - if the user clicked in the sBrokComp1Pc textbox, display a warning if there is a rate lock
		//resulting from another user locking the rate while this file was being edited 
		if(control.readOnly == true)
			return;
		var obj = ChecksBrokComp1Pc(false);
		_alertUser(obj);
	}
	</script>
		<form id="GoodFaithEstimate" method="post" runat="server">
			<TABLE id="Table2" cellSpacing="0" cellPadding="0" border="0">
				<TR>
					<TD class="MainRightHeader" noWrap>Good Faith Estimate</TD>
				</TR>
				<TR>
					<TD noWrap>
						<table class="InsetBorder" id="Table6" cellspacing="0" cellpadding="0" width="98%" border="0">
							<tr>
								<td nowrap>
									<table id="Table3" cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td class="FieldLabel" nowrap colspan="6">Perform calculations:<asp:RadioButtonList id="ByPassBgCalcForGfeAsDefault" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
													<asp:ListItem Value="0">automatically</asp:ListItem>
													<asp:ListItem Value="1">manually</asp:ListItem>
												</asp:RadioButtonList>
												<input disabled accesskey="c" onclick="backgroundCalculation();" tabindex="-1" type="button" value="Recalculate  (Alt + C)" name="btnCalculate" id="btnCalculate"></td>
										</tr>
										<tr style="PADDING-TOP:10px">
											<td class=FieldLabel nowrap colspan="7">
												<uc:CFM ID="CFM" runat="server" />
											</td>
										</tr>
										<tr>
											<td class="FieldLabel" nowrap>Prepared By</td>
											<td class="FieldLabel" nowrap colspan="5"><asp:textbox id="GfeTilCompanyName" runat="server" Width="258px" SkipMe="true" EnableViewState="False"></asp:textbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" nowrap>Address</td>
											<td class="FieldLabel" nowrap colspan="5"><asp:textbox id="GfeTilStreetAddr" runat="server" Width="254px" SkipMe="true" EnableViewState="False"></asp:textbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" nowrap></td>
											<td class="FieldLabel" nowrap colspan="5"><asp:textbox id="GfeTilCity" runat="server" SkipMe="true" EnableViewState="False"></asp:textbox><ml:statedropdownlist id="GfeTilState" runat="server" SkipMe="true" EnableViewState="False"></ml:statedropdownlist><ml:zipcodetextbox id="GfeTilZip" runat="server" SkipMe="true" width="50" preset="zipcode" EnableViewState="False"></ml:zipcodetextbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" nowrap>Phone</td>
											<td class="FieldLabel" nowrap colspan="5"><ml:phonetextbox id="GfeTilPhoneOfCompany" runat="server" SkipMe="true" width="120" preset="phone" EnableViewState="False"></ml:phonetextbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" nowrap>Prepared Date</td>
											<td class="FieldLabel" nowrap colspan="5"><ml:datetextbox id="GfeTilPrepareDate" onkeyup="onDateKeyUp(this, event);" runat="server" SkipMe="true" width="75" preset="date" CssClass="mask" EnableViewState="False"></ml:datetextbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" nowrap>Total Loan Amt</td>
											<td nowrap><ml:moneytextbox id="sFinalLAmt" runat="server" width="90" preset="money" ReadOnly="True" EnableViewState="False"></ml:moneytextbox></td>
											<td class="FieldLabel" nowrap>Interest Rate</td>
											<td nowrap><ml:percenttextbox id="sNoteIR" runat="server" width="70" preset="percent" onchange="refreshCalculation();" EnableViewState="False"></ml:percenttextbox></td>
											<td class="FieldLabel" nowrap>1st Payment Date</td>
											<td nowrap><ml:datetextbox id="sSchedDueD1" runat="server" width="75" preset="date" CssClass="mask" EnableViewState="False" onchange="refreshCalculation();"></ml:datetextbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" nowrap>Days in Year</td>
											<td nowrap><asp:textbox id="sDaysInYr" runat="server" Width="54px" onchange="refreshCalculation();" MaxLength="3" EnableViewState="False"></asp:textbox></td>
											<td class="FieldLabel" nowrap>Term/Due In</td>
											<td nowrap><asp:textbox id="sTerm" runat="server" Width="42px" EnableViewState="False"></asp:textbox>
												/
												<asp:textbox id="sDue" runat="server" Width="42px" EnableViewState="False"></asp:textbox>&nbsp;mths</td>
											<td class="FieldLabel" nowrap>Estimated Closing Date</td>
											<td nowrap><ml:datetextbox id="sEstCloseD" runat="server" width="75" preset="date" CssClass="mask" EnableViewState="False"></ml:datetextbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" style="HEIGHT: 18px" nowrap>Loan Program</td>
											<td nowrap colspan="5"><asp:textbox id="sLpTemplateNm" runat="server" Width="432px" SkipMe="true" EnableViewState="False"></asp:textbox></td>
										</tr>
										<tr>
											<td class="FieldLabel" nowrap>CC Template</td>
											<td nowrap colspan="5"><asp:textbox id="sCcTemplateNm" runat="server" Width="432px" SkipMe="true" EnableViewState="False"></asp:textbox>
												<input onclick="findCCTemplate();" type="button" value="Find Closing Cost Template ..."></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</TD>
				</TR>
				<TR>
					<TD noWrap></TD>
				</TR>
				<TR>
					<TD noWrap>
						<table id="Table5" cellspacing="0" cellpadding="0" border="0" class="InsetBorder" width="98%">
							<tr>
								<td>
									<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD class="FormTableHeader"></TD>
											<TD class="FormTableHeader"></TD>
											<TD class="FormTableHeader" colSpan="5">B&nbsp;= Paid to broker&nbsp;&nbsp;&nbsp;A 
												= APR&nbsp;&nbsp;&nbsp;F = FHA Allowable&nbsp;&nbsp;&nbsp;POC = Paid Outside of 
												Closing</TD>
										</TR>
										<TR>
											<TD class="FormTableHeader"></TD>
											<TD class="FormTableHeader">Description of Charge</TD>
											<TD class="FormTableHeader"></TD>
											<TD class="FormTableHeader">Amount</TD>
											<TD class="FormTableHeader">Paid By</TD>
											<TD class="FormTableHeader" style="WIDTH: 44px"></TD>
											<TD class="FormTableHeader"></TD>
										</TR>
										<TR>
											<TD class="FormTableSubheader">800</TD>
											<TD class="FormTableSubheader" colSpan="7">
												ITEMS PAYABLE IN CONNECTION WITH LOAN</TD>
										</TR>
										<TR>
											<TD class="FieldLabel">801</TD>
											<TD class="FieldLabel">Loan origination fee</TD>
											<TD><ml:percenttextbox id="sLOrigFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();" EnableViewState="False"></ml:percenttextbox>
												+
												<ml:moneytextbox id="sLOrigFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();" EnableViewState="False"></ml:moneytextbox></TD>
											<TD><ml:moneytextbox id="sLOrigF" runat="server" preset="money" width="77" ReadOnly="True" EnableViewState="False"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sLOrigFProps_ctrl" onclick="onPocCheck()" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">802</TD>
											<TD class="FieldLabel">Loan discount</TD>
											<TD noWrap><ml:percenttextbox id="sLDiscntPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();" EnableViewState="False"></ml:percenttextbox>
												+
												<ml:moneytextbox id="sLDiscntFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();" EnableViewState="False"></ml:moneytextbox></TD>
											<TD><ml:moneytextbox id="sLDiscnt" runat="server" preset="money" width="76px" ReadOnly="True" EnableViewState="False"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sLDiscntProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">803</TD>
											<TD class="FieldLabel">Appraisal fee</TD>
											<TD align="right"><asp:checkbox id="sApprFPaid" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" onclick="OnPaidChecking(this); refreshCalculation();" runat="server" Width="49px" Text="Paid" EnableViewState="False"></asp:checkbox>&nbsp;</TD>
											<TD><ml:moneytextbox id="sApprF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();" EnableViewState="False"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sApprFProps_ctrl" runat="server"  ></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">804</TD>
											<TD class="FieldLabel">Credit report</TD>
											<TD align="right"><asp:checkbox id="sCrFPaid" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" onclick="OnPaidChecking(this); refreshCalculation();" runat="server" Width="49px" Text="Paid" EnableViewState="False"></asp:checkbox>&nbsp;</TD>
											<TD><ml:moneytextbox id="sCrF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();" EnableViewState="False"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sCrFProps_ctrl" runat="server" ></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">805</TD>
											<TD class="FieldLabel">Lender's inspection fee</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sInspectF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();" EnableViewState="False"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sInspectFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">808</TD>
											<TD class="FieldLabel">Mortgage broker fee</TD>
											<TD><ml:percenttextbox id="sMBrokFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();" EnableViewState="False"></ml:percenttextbox>
												+
												<ml:moneytextbox id="sMBrokFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();" EnableViewState="False"></ml:moneytextbox></TD>
											<TD><ml:moneytextbox id="sMBrokF" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sMBrokFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">809</TD>
											<TD class="FieldLabel">Tax service fee</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sTxServF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sTxServFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">810</TD>
											<TD class="FieldLabel">Processing fee</TD>
											<TD align="right"><asp:checkbox id="sProcFPaid" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" onclick="OnPaidChecking(this); refreshCalculation();" runat="server" Width="49px" Text="Paid"></asp:checkbox>&nbsp;</TD>
											<TD><ml:moneytextbox id="sProcF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sProcFProps_ctrl"   runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">811</TD>
											<TD class="FieldLabel">Underwriting fee</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sUwF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sUwFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">812</TD>
											<TD class="FieldLabel">Wire transfer</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sWireF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sWireFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="s800U1FCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="31" MaxLength="21"></asp:textbox></TD>
											<TD colSpan="2"><asp:textbox id="s800U1FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="95%" MaxLength="100"></asp:textbox></TD>
											<TD><ml:moneytextbox id="s800U1F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s800U1FProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="s800U2FCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="31px" MaxLength="21"></asp:textbox></TD>
											<TD colSpan="2"><asp:textbox id="s800U2FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="95%" MaxLength="100"></asp:textbox></TD>
											<TD><ml:moneytextbox id="s800U2F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s800U2FProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="s800U3FCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="31px" MaxLength="21"></asp:textbox></TD>
											<TD colSpan="2"><asp:textbox id="s800U3FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="95%" MaxLength="100"></asp:textbox></TD>
											<TD><ml:moneytextbox id="s800U3F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s800U3FProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="s800U4FCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="31px" MaxLength="21"></asp:textbox></TD>
											<TD colSpan="2"><asp:textbox id="s800U4FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="95%" MaxLength="100"></asp:textbox></TD>
											<TD><ml:moneytextbox id="s800U4F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s800U4FProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="s800U5FCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="32" MaxLength="21"></asp:textbox></TD>
											<TD colSpan="2"><asp:textbox id="s800U5FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="95%" MaxLength="100"></asp:textbox></TD>
											<TD><ml:moneytextbox id="s800U5F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s800U5FProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FormTableSubheader"><A name="900"></A>900</TD>
											<TD class="FormTableSubheader" colSpan="7">ITEMS REQUIRED BY LENDER TO BE PAID IN 
												ADVANCE</TD>
										</TR>
										<TR>
											<TD class="FieldLabel">901</TD>
											<TD class="FieldLabel">Interest for&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="sIPiaDyLckd" runat="server" Text="Locked" onclick="refreshCalculation();"/></TD>
											<TD class="FieldLabel" noWrap><asp:textbox id="sIPiaDy" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="40px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;days 
												@
												<asp:CheckBox id="sIPerDayLckd" runat="server" onclick="lockField(this, 'sIPerDay'); refreshCalculation();" Text="Lock"></asp:CheckBox>
												<ml:moneytextbox id="sIPerDay" runat="server" preset="money" onchange="refreshCalculation();" decimalDigits="6"></ml:moneytextbox>
												per day</TD>
											<TD><ml:moneytextbox id="sIPia" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sIPiaProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">902</TD>
											<TD class="FieldLabel" noWrap colSpan="2" rowSpan="1"><A href="javascript:linkMe('../LoanInfo.aspx?pg=1');">Mortgage 
													Insurance Premium</A></TD>
											<TD><ml:moneytextbox id="sMipPia" runat="server" preset="money" width="76px" readonly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sMipPiaProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">903</TD>
											<TD class="FieldLabel" noWrap colSpan="2">Haz Ins. @
												<ml:percenttextbox id="sProHazInsR" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();" DESIGNTIMEDRAGDROP="236"></ml:percenttextbox>
												of
												<asp:dropdownlist id="sProHazInsT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
												+
												<ml:moneytextbox id="sProHazInsMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="60px" onchange="refreshCalculation();" decimalDigits="4"></ml:moneytextbox>
												for
												<asp:textbox id="sHazInsPiaMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="30px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>
												mths</TD>
											<TD><ml:moneytextbox id="sHazInsPia" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sHazInsPiaProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">904</TD>
											<TD colSpan="2"><asp:textbox id="s904PiaDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="95%" MaxLength="100" Height="20px"></asp:textbox></TD>
											<TD><ml:moneytextbox id="s904Pia" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s904PiaProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">905</TD>
											<TD class="FieldLabel"><A href="javascript:linkMe('../LoanInfo.aspx?pg=1');">VA Funding 
													Fee</A></TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sVaFf" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" ReadOnly="True" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sVaFfProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="s900U1PiaCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="33" MaxLength="21"></asp:textbox></TD>
											<TD colSpan="2"><asp:textbox id="s900U1PiaDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="95%" MaxLength="100"></asp:textbox></TD>
											<TD><ml:moneytextbox id="s900U1Pia" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s900U1PiaProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FormTableSubheader"><A name="1000"></A>1000</TD>
											<TD class="FormTableSubheader" colSpan="7">RESERVES DEPOSITED WITH LENDER</TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1001</TD>
											<TD class="FieldLabel">Haz ins. reserve</TD>
											<TD class="FieldLabel"><asp:textbox id="sHazInsRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
												@
												<ml:moneytextbox id="sProHazIns" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox>
												/ month</TD>
											<TD><ml:moneytextbox id="sHazInsRsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sHazInsRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1002</TD>
											<TD class="FieldLabel" colSpan="2"><A href="javascript:linkMe('../LoanInfo.aspx?pg=1');">Mtg ins.</A> 
												reserve&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = 
												<ml:moneytextbox id="sProMIns" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox>&nbsp;&nbsp;for
												<asp:textbox id="sMInsRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>
												mths</TD>
											<TD colSpan="1"><ml:moneytextbox id="sMInsRsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sMInsRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1003</TD>
											<TD class="FieldLabel">School taxes</TD>
											<TD class="FieldLabel"><asp:textbox id="sSchoolTxRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
												@
												<ml:moneytextbox id="sProSchoolTx" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>
												/ month</TD>
											<TD><ml:moneytextbox id="sSchoolTxRsrv" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sSchoolTxRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1004</TD>
											<TD class="FieldLabel" colSpan="2">Tax resrv @
												<ml:percenttextbox id="sProRealETxR" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();"></ml:percenttextbox>&nbsp;of
												<asp:dropdownlist id="sProRealETxT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
												+
												<ml:moneytextbox id="sProRealETxMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="59px" onchange="refreshCalculation();"></ml:moneytextbox>
												for
												<asp:textbox id="sRealETxRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="31px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>
												mths</TD>
											<TD><ml:moneytextbox id="sRealETxRsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sRealETxRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1005</TD>
											<TD class="FieldLabel">Flood ins. reserve</TD>
											<TD class="FieldLabel"><asp:textbox id="sFloodInsRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
												@
												<ml:moneytextbox id="sProFloodIns" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>
												/ month</TD>
											<TD><ml:moneytextbox id="sFloodInsRsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sFloodInsRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1006</TD>
											<TD><asp:textbox id="s1006ProHExpDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="135" MaxLength="21" Height="20px"></asp:textbox></TD>
											<TD class="FieldLabel"><asp:textbox id="s1006RsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
												@
												<ml:moneytextbox id="s1006ProHExp" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>
												/ month</TD>
											<TD><ml:moneytextbox id="s1006Rsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s1006RsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1007</TD>
											<TD><asp:textbox id="s1007ProHExpDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="136" MaxLength="21" Height="20px"></asp:textbox></TD>
											<TD class="FieldLabel"><asp:textbox id="s1007RsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
												@
												<ml:moneytextbox id="s1007ProHExp" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>
												/ month</TD>
											<TD><ml:moneytextbox id="s1007Rsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s1007RsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1008</TD>
											<TD class="FieldLabel">Aggregate adjustment</TD>
											<TD align="right"><INPUT type="button" value="Aggregate Escrow" onclick="linkMe('AggregateEscrowDisclosure.aspx');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<asp:CheckBox id="sAggregateAdjRsrvLckd" runat="server" onclick="lockField(this, 'sAggregateAdjRsrv'); refreshCalculation();" Text="Locked" CssClass="FieldLabel"></asp:CheckBox></TD>
											<TD><ml:moneytextbox id="sAggregateAdjRsrv" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sAggregateAdjRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FormTableSubheader">1100</TD>
											<TD class="FormTableSubheader" colSpan="7">TITLE CHARGES</TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1101</TD>
											<TD class="FieldLabel">Closing/Escrow Fee</TD>
											<TD><asp:textbox id="sEscrowFTable" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="212px" MaxLength="21"></asp:textbox></TD>
											<TD><ml:moneytextbox id="sEscrowF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sEscrowFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1105</TD>
											<TD class="FieldLabel">Doc preparation fee</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sDocPrepF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sDocPrepFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1106</TD>
											<TD class="FieldLabel">Notary fees</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sNotaryF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sNotaryFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1107</TD>
											<TD class="FieldLabel">Attorney fees</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sAttorneyF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sAttorneyFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1108</TD>
											<TD class="FieldLabel">Title Insurance</TD>
											<TD><asp:textbox id="sTitleInsFTable" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="212px" MaxLength="36"></asp:textbox></TD>
											<TD><ml:moneytextbox id="sTitleInsF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sTitleInsFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="sU1TcCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="33px" MaxLength="21"></asp:textbox></TD>
											<TD colSpan="2"><asp:textbox id="sU1TcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="95%" MaxLength="100"></asp:textbox></TD>
											<TD><ml:moneytextbox id="sU1Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU1TcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="sU2TcCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="33px" MaxLength="21"></asp:textbox></TD>
											<TD colSpan="2"><asp:textbox id="sU2TcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="95%" MaxLength="100"></asp:textbox></TD>
											<TD><ml:moneytextbox id="sU2Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU2TcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="sU3TcCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="33px" MaxLength="21"></asp:textbox></TD>
											<TD colSpan="2"><asp:textbox id="sU3TcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="95%" MaxLength="100"></asp:textbox></TD>
											<TD><ml:moneytextbox id="sU3Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU3TcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="sU4TcCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="33px" MaxLength="21"></asp:textbox></TD>
											<TD colSpan="2"><asp:textbox id="sU4TcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="95%" MaxLength="100"></asp:textbox></TD>
											<TD><ml:moneytextbox id="sU4Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU4TcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FormTableSubheader">1200</TD>
											<TD class="FormTableSubheader" noWrap colSpan="7">GOVERNMENT RECORDING &amp; 
												TRANSFER CHARGES</TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1201</TD>
											<TD class="FieldLabel" noWrap>Recording fees
												<asp:textbox id="sRecFDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="76px"></asp:textbox></TD>
											<TD class="FieldLabel" noWrap><ml:percenttextbox id="sRecFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51" onchange="refreshCalculation();"></ml:percenttextbox>
												of
												<asp:dropdownlist id="sRecBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
												+
												<ml:moneytextbox id="sRecFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD><ml:moneytextbox id="sRecF" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sRecFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1202</TD>
											<TD class="FieldLabel" noWrap>City tax/stamps
												<asp:textbox id="sCountyRtcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="72px"></asp:textbox></TD>
											<TD class="FieldLabel" noWrap><ml:percenttextbox id="sCountyRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>
												of
												<asp:dropdownlist id="sCountyRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
												+
												<ml:moneytextbox id="sCountyRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD><ml:moneytextbox id="sCountyRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sCountyRtcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1203</TD>
											<TD class="FieldLabel" noWrap>State tax/stamps
												<asp:textbox id="sStateRtcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="68px" onchange="refreshCalculation();"></asp:textbox></TD>
											<TD class="FieldLabel" noWrap><ml:percenttextbox id="sStateRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>
												of
												<asp:dropdownlist id="sStateRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
												+
												<ml:moneytextbox id="sStateRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD><ml:moneytextbox id="sStateRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sStateRtcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="sU1GovRtcCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="33px" MaxLength="21"></asp:textbox></TD>
											<TD><asp:textbox id="sU1GovRtcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="104" MaxLength="21"></asp:textbox></TD>
											<TD class="FieldLabel" noWrap><ml:percenttextbox id="sU1GovRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>
												of
												<asp:dropdownlist id="sU1GovRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
												+
												<ml:moneytextbox id="sU1GovRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD><ml:moneytextbox id="sU1GovRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU1GovRtcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="sU2GovRtcCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="33px" MaxLength="21"></asp:textbox></TD>
											<TD><asp:textbox id="sU2GovRtcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="104px" MaxLength="21" Height="20px"></asp:textbox></TD>
											<TD class="FieldLabel" noWrap><ml:percenttextbox id="sU2GovRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>
												of
												<asp:dropdownlist id="sU2GovRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
												+
												<ml:moneytextbox id="sU2GovRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD><ml:moneytextbox id="sU2GovRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU2GovRtcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="sU3GovRtcCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="33px" MaxLength="21"></asp:textbox></TD>
											<TD><asp:textbox id="sU3GovRtcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="104px" MaxLength="21" Height="20px"></asp:textbox></TD>
											<TD class="FieldLabel" noWrap><ml:percenttextbox id="sU3GovRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>
												of
												<asp:dropdownlist id="sU3GovRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
												+
												<ml:moneytextbox id="sU3GovRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD><ml:moneytextbox id="sU3GovRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU3GovRtcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FormTableSubheader">1300</TD>
											<TD class="FormTableSubheader" colSpan="7">ADDITIONAL SETTLEMENT CHARGES</TD>
										</TR>
										<TR>
											<TD class="FieldLabel">1302</TD>
											<TD class="FieldLabel">Pest Inspection</TD>
											<TD></TD>
											<TD><ml:moneytextbox id="sPestInspectF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sPestInspectFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="sU1ScCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="33px" MaxLength="21"></asp:textbox></TD>
											<TD colSpan="2"><asp:textbox id="sU1ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="95%" MaxLength="100"></asp:textbox></TD>
											<TD><ml:moneytextbox id="sU1Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU1ScProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="sU2ScCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="33px" MaxLength="21"></asp:textbox></TD>
											<TD colSpan="2"><asp:textbox id="sU2ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="95%" MaxLength="100" Height="20px"></asp:textbox></TD>
											<TD><ml:moneytextbox id="sU2Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU2ScProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="sU3ScCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="33px" MaxLength="21"></asp:textbox></TD>
											<TD colSpan="2"><asp:textbox id="sU3ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="95%" MaxLength="100"></asp:textbox></TD>
											<TD><ml:moneytextbox id="sU3Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU3ScProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="sU4ScCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="33px" MaxLength="21"></asp:textbox></TD>
											<TD colSpan="2"><asp:textbox id="sU4ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="95%" MaxLength="100"></asp:textbox></TD>
											<TD><ml:moneytextbox id="sU4Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU4ScProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="sU5ScCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="33px" MaxLength="21"></asp:textbox></TD>
											<TD colSpan="2"><asp:textbox id="sU5ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" Width="95%" MaxLength="100"></asp:textbox></TD>
											<TD><ml:moneytextbox id="sU5Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU5ScProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" colSpan="3">TOTAL ESTIMATED SETTLEMENT CHARGES</TD>
											<TD><ml:moneytextbox id="sTotEstSc" tabIndex="201" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"></TD>
										</TR>
										<TR>
											<TD colSpan="3">&nbsp;</TD>
											<TD></TD>
											<TD noWrap colSpan="3"></TD>
										</TR>
										<TR>
											<TD class="FormTableSubheader" colSpan="7">COMPENSATION TO BROKER (Not Paid Out of 
												Loan Proceeds)</TD>
										</TR>
										<TR>
											<TD colSpan="7" class="FieldLabel"><asp:CheckBox id="sPrintCompensationOnGfe" runat="server" Text="Show compensation to broker on print-out"></asp:CheckBox></TD>
										</TR>
										<TR>
											<TD colSpan="3"><asp:textbox id="sBrokComp1Desc" tabIndex="201" runat="server" SkipMe="true" Width="353px" MaxLength="100"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<ml:PercentTextBox id="sBrokComp1Pc" runat="server" preset="percent" width="70" onclick="checksBrokComp1Pc(this);" onchange="refreshCalculation();"></ml:PercentTextBox></TD>
											<TD><ml:moneytextbox id="sBrokComp1" tabIndex="201" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"><asp:CheckBox id="sBrokComp1Lckd" runat="server" Text="Locked" onclick="lockField(this, 'sBrokComp1'); refreshCalculation();"></asp:CheckBox></TD>
										</TR>
										<TR>
											<TD colSpan="3"><asp:textbox id="sBrokComp2Desc" tabIndex="201" runat="server" SkipMe="true" Width="95%" MaxLength="100"></asp:textbox></TD>
											<TD><ml:moneytextbox id="sBrokComp2" tabIndex="201" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD noWrap colSpan="3"></TD>
										</TR>
										<TR>
											<TD colSpan="3"></TD>
											<TD></TD>
											<TD noWrap colSpan="3"></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE class="InsetBorder" id="Table9" cellSpacing="0" cellPadding="0" border="0" width="98%">
							<TR>
								<TD class="FormTableSubHeader" colSpan="2">&nbsp;Total Estimated Funds Needed to 
									Close</TD>
							</TR>
							<TR>
								<TD colSpan="2">
									<TABLE id="Table10" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<tr>
											<td colSpan="4" style="PADDING-LEFT: 5px">
												<table id="Table4" cellSpacing="0" cellPadding="0" border="0">
													<tr>
														<td></td>
														<td class="FieldLabel" colSpan="2">Locked</td>
														<td width="5"></td>
														<td></td>
														<td class="FieldLabel" colSpan="2">Locked</td>
													</tr>
													<tr>
														<td class="FieldLabel">a. Purchase price</td>
														<td style="WIDTH: 11px"></td>
														<td style="WIDTH: 79px"><ml:moneytextbox id="sPurchPrice" tabIndex="201" runat="server" preset="money" width="86" onchange="refreshCalculation();"></ml:moneytextbox></td>
														<td></td>
														<td class="FieldLabel">j. <a href="javascript:linkMe('../LoanInfo.aspx?pg=2');">Subordinate 
																financing</a></td>
														<td style="WIDTH: 6px"></td>
														<td><ml:moneytextbox id="sONewFinBal" tabIndex="202" runat="server" preset="money" width="86px" onchange="refreshCalculation();" readonly="True"></ml:moneytextbox></td>
													</tr>
													<tr>
														<td class="FieldLabel" title="Alterations, improvements, repairs">b. Alterations</td>
														<td style="WIDTH: 11px">
                                                            <input type="checkbox" id="sAltCostLckd" tabindex="201" runat="server" onclick="refreshCalculation();" />
														</td>
														<td style="WIDTH: 79px"><ml:moneytextbox id="sAltCost" tabIndex="201" runat="server" preset="money" width="86px" onchange="refreshCalculation();"></ml:moneytextbox></td>
														<td></td>
														<td class="FieldLabel">k. Borrower's&nbsp;closing costs paid by Seller</td>
														<td style="WIDTH: 6px"><asp:CheckBox id="sTotCcPbsLocked" tabIndex="202" runat="server" onclick="lockField(this, 'sTotCcPbs'); refreshCalculation();"></asp:CheckBox></td>
														<td><ml:moneytextbox id="sTotCcPbs" tabIndex="202" runat="server" preset="money" width="86px" onchange="refreshCalculation();"></ml:moneytextbox></td>
													</tr>
													<tr>
														<td class="FieldLabel">c. Land (if acquired separately)</td>
														<td style="WIDTH: 11px"></td>
														<td style="WIDTH: 79px"><ml:moneytextbox id="sLandCost" tabIndex="201" runat="server" preset="money" width="86px" onchange="refreshCalculation();"></ml:moneytextbox></td>
														<td></td>
														<td class="FieldLabel">l.
															<ml:combobox id="sOCredit1Desc" tabIndex="202" runat="server" Width="168px"></ml:combobox></td>
														<td><asp:CheckBox id="sOCredit1Lckd" runat="server" onclick="lockField(this, 'sOCredit1Amt'); lockField(this, 'sOCredit1Desc'); refreshCalculation();"></asp:CheckBox></td>
														<td><ml:moneytextbox id="sOCredit1Amt" tabIndex="202" runat="server" preset="money" width="86px" onchange="refreshCalculation();"></ml:moneytextbox></td>
													</tr>
													<tr>
														<td class="FieldLabel">d. Refi (incl. debts to be paid off)&nbsp;</td>
														<td style="WIDTH: 11px"><asp:CheckBox id="sRefPdOffAmt1003Lckd" tabIndex="201" runat="server" onclick="lockField(this, 'sRefPdOffAmt1003'); refreshCalculation();"></asp:CheckBox></td>
														<td style="WIDTH: 79px"><ml:moneytextbox id="sRefPdOffAmt1003" tabIndex="201" runat="server" preset="money" width="86px" onchange="refreshCalculation();"></ml:moneytextbox></td>
														<td></td>
														<td>&nbsp;&nbsp;&nbsp;<ml:combobox id="sOCredit2Desc" tabIndex="202" runat="server" Width="168px"></ml:combobox>
														</td>
														<td></td>
														<td><ml:moneytextbox id="sOCredit2Amt" tabIndex="202" runat="server" preset="money" width="86px" onchange="refreshCalculation();"></ml:moneytextbox></td>
													</tr>
													<tr>
														<td class="FieldLabel">e. Estimated prepaid items&nbsp;</td>
														<td style="WIDTH: 11px"><asp:CheckBox id="sTotEstPp1003Lckd" tabIndex="201" runat="server" onclick="lockField(this, 'sTotEstPp1003'); refreshCalculation();"></asp:CheckBox></td>
														<td style="WIDTH: 79px"><ml:moneytextbox id="sTotEstPp1003" tabIndex="201" runat="server" preset="money" width="86px" onchange="refreshCalculation();"></ml:moneytextbox></td>
														<td></td>
														<td colSpan="2">&nbsp;&nbsp;&nbsp;<ml:combobox id="sOCredit3Desc" tabIndex="202" runat="server" Width="168px"></ml:combobox>
														</td>
														<td><ml:moneytextbox id="sOCredit3Amt" tabIndex="202" runat="server" preset="money" width="86px" onchange="refreshCalculation();"></ml:moneytextbox></td>
													</tr>
													<tr>
														<td></td>
														<td style="WIDTH: 11px"></td>
														<td style="WIDTH: 79px"></td>
														<td></td>
														<td colSpan="2">&nbsp;&nbsp;&nbsp;<ml:combobox id="sOCredit4Desc" tabIndex="202" runat="server" Width="168px"></ml:combobox>
														</td>
														<td><ml:moneytextbox id="sOCredit4Amt" tabIndex="202" runat="server" preset="money" width="86px" onchange="refreshCalculation();"></ml:moneytextbox></td>
													</tr>
													<tr>
														<td></td>
														<td style="WIDTH: 11px"></td>
														<td style="WIDTH: 79px"></td>
														<td></td>
														<td class="FieldLabel">&nbsp;&nbsp;&nbsp;&nbsp;Other financing closing costs</td>
														<td class="FieldLabel" align="right">-&nbsp;</td>
														<td><ml:moneytextbox id="sONewFinCc" tabIndex="202" runat="server" preset="money" width="86px" onchange="refreshCalculation();"></ml:moneytextbox></td>
													</tr>
													<tr>
														<td class="FieldLabel">f. Estimated closing costs</td>
														<td style="WIDTH: 11px"><asp:CheckBox id="sTotEstCc1003Lckd" tabIndex="201" runat="server" onclick="lockField(this, 'sTotEstCcNoDiscnt1003'); refreshCalculation();"></asp:CheckBox></td>
														<td style="WIDTH: 79px"><ml:moneytextbox id="sTotEstCcNoDiscnt1003" tabIndex="201" runat="server" preset="money" width="86px" onchange="refreshCalculation();"></ml:moneytextbox></td>
														<td class="FieldLabel" colSpan="2">m. Loan amount (exclude PMI, MIP, FF financed)</td>
														<td style="WIDTH: 6px"><asp:checkbox id=sLAmtLckd onclick="refreshCalculation();" runat="server" tabIndex="202"></asp:checkbox></td>
														<td><ml:moneytextbox id="sLAmtCalc" tabIndex="202" runat="server" preset="money" width="86px" ReadOnly="True" onchange="refreshCalculation();"></ml:moneytextbox></td>
													</tr>
													<tr>
														<td class="FieldLabel">g. PMI,&nbsp;MIP, Funding Fee</td>
														<td style="WIDTH: 11px"><asp:CheckBox id="sFfUfmip1003Lckd" tabIndex="201" runat="server" onclick="lockField(this, 'sFfUfmip1003'); refreshCalculation();"></asp:CheckBox></td>
														<td style="WIDTH: 79px"><ml:moneytextbox id="sFfUfmip1003" tabIndex="201" runat="server" preset="money" width="86px" onchange="refreshCalculation();"></ml:moneytextbox></td>
														<td></td>
														<td class="FieldLabel">n. PMI, MIP, Funding Fee financed</td>
														<td style="WIDTH: 6px"></td>
														<td><ml:moneytextbox id="sFfUfmipFinanced" tabIndex="202" runat="server" preset="money" width="86px" ReadOnly="True"></ml:moneytextbox></td>
													</tr>
													<tr>
														<td class="FieldLabel">h. Discount (if Borrower will pay)</td>
														<td style="WIDTH: 11px"><asp:CheckBox id="sLDiscnt1003Lckd" tabIndex="201" runat="server" onclick="lockField(this, 'sLDiscnt1003'); refreshCalculation();"></asp:CheckBox></td>
														<td style="WIDTH: 79px"><ml:moneytextbox id="sLDiscnt1003" tabIndex="201" runat="server" preset="money" width="86px" onchange="refreshCalculation();"></ml:moneytextbox></td>
														<td></td>
														<td class="FieldLabel">o. Loan amount (add m &amp; n)</td>
														<td style="WIDTH: 6px"></td>
														<td><ml:moneytextbox id="sFinalLAmt2" tabIndex="202" runat="server" preset="money" width="86px" ReadOnly="True"></ml:moneytextbox></td>
													</tr>
													<tr>
														<td class="FieldLabel">i. Total costs (add items a to h)</td>
														<td style="WIDTH: 11px"></td>
														<td style="WIDTH: 79px"><ml:moneytextbox id="sTotTransC" tabIndex="201" runat="server" preset="money" width="86px" ReadOnly="True"></ml:moneytextbox></td>
														<td></td>
														<td class="FieldLabel">p. Cash from / to Borr (subtract j, k, l &amp; o from 
															i)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
														<td style="WIDTH: 6px"><asp:CheckBox id="sTransNetCashLckd" tabIndex="202" runat="server" onclick="lockField(this, 'sTransNetCash'); refreshCalculation();"></asp:CheckBox></td>
														<td><ml:moneytextbox id="sTransNetCash" tabIndex="202" runat="server" preset="money" width="86px"></ml:moneytextbox></td>
													</tr>
												</table>
											</td>
										</tr>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="FieldLabel" noWrap><asp:checkbox id="sGfeProvByBrok" tabIndex="220" runat="server" Width="393px" Text="This Good Faith Estimate is being provided by broker" Height="15px"></asp:checkbox></TD>
				</TR>
			</TABLE>
			<uc1:cmodaldlg id="CModalDlg1" runat="server" DESIGNTIMEDRAGDROP="5482"></uc1:cmodaldlg></form>
	</body>
</HTML>
