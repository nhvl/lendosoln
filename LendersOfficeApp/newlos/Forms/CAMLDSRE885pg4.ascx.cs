﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using System.Collections;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class CAMLDSRE885pg4 : BaseLoanUserControl, LendersOffice.Common.IAutoLoadUserControl
    {
        protected void PageInit(object sender, System.EventArgs e)
        {
            GfeTilZip.SmartZipcode(GfeTilCity, GfeTilState);
            CFM.Type = "19";
            CFM.CompanyNameField = GfeTilCompanyName.ClientID;
            CFM.StreetAddressField = GfeTilStreetAddr.ClientID;
            CFM.CompanyLicenseField = GfeTilLicenseNumOfCompany.ClientID;
            CFM.CityField = GfeTilCity.ClientID;
            CFM.StateField = GfeTilState.ClientID;
            CFM.ZipField = GfeTilZip.ClientID;
            CFM.AgentNameField = GfeTilPreparerName.ClientID;
            CFM.AgentLicenseField = GfeTilLicenseNumOfAgent.ClientID;
            CFM.IsAllowLockableFeature = true;
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
        }

        public void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(CAMLDSRE885pg4));
            dataLoan.InitLoad();

            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sTermInYr.Text = dataLoan.sTermInYr_rep;

            // ** PrincipalInterest
            sRe885ComparisonPrincipalInterestIsNotOffered.Checked = dataLoan.sRe885ComparisonPrincipalInterestIsNotOffered;

            sRe885ComparisonPrincipalInterestNoteIR.Text = dataLoan.sRe885ComparisonPrincipalInterestNoteIR_rep;

            sRe885ComparisonPrincipalInterestMinPmtFirst5Yrs.Text = dataLoan.sRe885ComparisonPrincipalInterestMinPmtFirst5Yrs_rep;
            sRe885ComparisonPrincipalInterestMPmt6YrsNoRateChange.Text = dataLoan.sRe885ComparisonPrincipalInterestMPmt6YrsNoRateChange_rep;
            sRe885ComparisonPrincipalInterestMPmt6Yrs2PercentRaise.Text = dataLoan.sRe885ComparisonPrincipalInterestMPmt6Yrs2PercentRaise_rep;
            sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaise.Text = dataLoan.sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaise_rep;
            
            sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaiseDiff.Text = dataLoan.sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaiseDiff_rep;
            sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaise.Text = dataLoan.sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaise_rep;
            
            sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaiseDiff.Text = dataLoan.sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaiseDiff_rep;
            
            sRe885ComparisonPrincipalInterestBalanceAfter5Yrs.Text = dataLoan.sRe885ComparisonPrincipalInterestBalanceAfter5Yrs_rep;
            sRe885ComparisonPrincipalInterestBalanceAfter5YrsReduceBy.Text = dataLoan.sRe885ComparisonPrincipalInterestBalanceAfter5YrsReduceBy_rep;

            // ** InterestOnly
            sRe885ComparisonInterestOnlyIsNotOffered.Checked = dataLoan.sRe885ComparisonInterestOnlyIsNotOffered;

            sRe885ComparisonInterestOnlyNoteIR.Text = dataLoan.sRe885ComparisonInterestOnlyNoteIR_rep;

            sRe885ComparisonInterestOnlyMinPmtFirst5Yrs.Text = dataLoan.sRe885ComparisonInterestOnlyMinPmtFirst5Yrs_rep;
            sRe885ComparisonInterestOnlyMPmt6YrsNoRateChange.Text = dataLoan.sRe885ComparisonInterestOnlyMPmt6YrsNoRateChange_rep;
            sRe885ComparisonInterestOnlyMPmt6Yrs2PercentRaise.Text = dataLoan.sRe885ComparisonInterestOnlyMPmt6Yrs2PercentRaise_rep;
            sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaise.Text = dataLoan.sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaise_rep;
            
            sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaiseDiff.Text = dataLoan.sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaiseDiff_rep;
            sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaise.Text = dataLoan.sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaise_rep;
            
            sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaiseDiff.Text = dataLoan.sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaiseDiff_rep;
            
            sRe885ComparisonInterestOnlyBalanceAfter5Yrs.Text = dataLoan.sRe885ComparisonInterestOnlyBalanceAfter5Yrs_rep;

            // ** 5YrsArm
            sRe885Comparison5YrsArmIsNotOffered.Checked = dataLoan.sRe885Comparison5YrsArmIsNotOffered;

            sRe885Comparison5YrsArmInitialNoteIR.Text = dataLoan.sRe885Comparison5YrsArmInitialNoteIR_rep;
            sRe885Comparison5YrsArmMaximumNoteIR.Text = dataLoan.sRe885Comparison5YrsArmMaximumNoteIR_rep;

            sRe885Comparison5YrsArmMinPmtFirst5Yrs.Text = dataLoan.sRe885Comparison5YrsArmMinPmtFirst5Yrs_rep;
            sRe885Comparison5YrsArmMPmt6YrsNoRateChange.Text = dataLoan.sRe885Comparison5YrsArmMPmt6YrsNoRateChange_rep;
            sRe885Comparison5YrsArmMPmt6Yrs2PercentRaise.Text = dataLoan.sRe885Comparison5YrsArmMPmt6Yrs2PercentRaise_rep;
            sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaise.Text = dataLoan.sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaise_rep;
            
            sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaiseDiff.Text = dataLoan.sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaiseDiff_rep;
            sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaise.Text = dataLoan.sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaise_rep;
            
            sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaiseDiff.Text = dataLoan.sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaiseDiff_rep;
            
            sRe885Comparison5YrsArmBalanceAfter5Yrs.Text = dataLoan.sRe885Comparison5YrsArmBalanceAfter5Yrs_rep;
            sRe885Comparison5YrsArmBalanceAfter5YrsReduceBy.Text = dataLoan.sRe885Comparison5YrsArmBalanceAfter5YrsReduceBy_rep;

            // ** 5YrsArmInterestOnly
            sRe885Comparison5YrsArmInterestOnlyIsNotOffered.Checked = dataLoan.sRe885Comparison5YrsArmInterestOnlyIsNotOffered;

            sRe885Comparison5YrsArmInterestOnlyInitialNoteIR.Text = dataLoan.sRe885Comparison5YrsArmInterestOnlyInitialNoteIR_rep;
            sRe885Comparison5YrsArmInterestOnlyMaximumNoteIR.Text = dataLoan.sRe885Comparison5YrsArmInterestOnlyMaximumNoteIR_rep;

            sRe885Comparison5YrsArmInterestOnlyMinPmtFirst5Yrs.Text = dataLoan.sRe885Comparison5YrsArmInterestOnlyMinPmtFirst5Yrs_rep;
            sRe885Comparison5YrsArmInterestOnlyMPmt6YrsNoRateChange.Text = dataLoan.sRe885Comparison5YrsArmInterestOnlyMPmt6YrsNoRateChange_rep;
            sRe885Comparison5YrsArmInterestOnlyMPmt6Yrs2PercentRaise.Text = dataLoan.sRe885Comparison5YrsArmInterestOnlyMPmt6Yrs2PercentRaise_rep;
            sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaise.Text = dataLoan.sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaise_rep;
            
            sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaiseDiff.Text = dataLoan.sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaiseDiff_rep;
            sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaise.Text = dataLoan.sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaise_rep;
            
            sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaiseDiff.Text = dataLoan.sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaiseDiff_rep;
            
            sRe885Comparison5YrsArmInterestOnlyBalanceAfter5Yrs.Text = dataLoan.sRe885Comparison5YrsArmInterestOnlyBalanceAfter5Yrs_rep;
            
            // ** Option Payment
            sRe885ComparisonOptionPaymentIsNotOffered.Checked = dataLoan.sRe885ComparisonOptionPaymentIsNotOffered;

            sRe885ComparisonOptionPaymentFirstMonthNoteIR.Text = dataLoan.sRe885ComparisonOptionPaymentFirstMonthNoteIR_rep;
            sRe885ComparisonOptionPaymentInitialNoteIR.Text = dataLoan.sRe885ComparisonOptionPaymentInitialNoteIR_rep;
            sRe885ComparisonOptionPaymentMaximumNoteIR.Text = dataLoan.sRe885ComparisonOptionPaymentMaximumNoteIR_rep;

            sRe885ComparisonOptionPaymentMinPmtFirst5Yrs.Text = dataLoan.sRe885ComparisonOptionPaymentMinPmtFirst5Yrs_rep;
            sRe885ComparisonOptionPaymentMPmt6YrsNoRateChange.Text = dataLoan.sRe885ComparisonOptionPaymentMPmt6YrsNoRateChange_rep;
            sRe885ComparisonOptionPaymentMPmt6Yrs2PercentRaise.Text = dataLoan.sRe885ComparisonOptionPaymentMPmt6Yrs2PercentRaise_rep;
            sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaise.Text = dataLoan.sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaise_rep;
            
            sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaiseDiff.Text = dataLoan.sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaiseDiff_rep;
            sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaise.Text = dataLoan.sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaise_rep;
            
            sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaiseDiff.Text = dataLoan.sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaiseDiff_rep;
            
            sRe885ComparisonOptionPaymentBalanceAfter5Yrs.Text = dataLoan.sRe885ComparisonOptionPaymentBalanceAfter5Yrs_rep;
            sRe885ComparisonOptionPaymentBalanceAfter5YrsReduceBy.Text = dataLoan.sRe885ComparisonOptionPaymentBalanceAfter5YrsReduceBy_rep;
            
            // ** Proposed Loan
            sRe885ComparisonProposedLoanTypeOfLoan.Text = dataLoan.sRe885ComparisonProposedLoanTypeOfLoan;
            sRe885ComparisonProposedLoanAmortType.Text = dataLoan.sRe885ComparisonProposedLoanAmortType;

            sRe885ComparisonProposedLoanProductDesc.Text = dataLoan.sRe885ComparisonProposedLoanProductDesc;

            sRe885ComparisonProposedLoanMinPmtFirst5Yrs.Text = dataLoan.sRe885ComparisonProposedLoanMinPmtFirst5Yrs_rep;
            sRe885ComparisonProposedLoanMPmt6YrsNoRateChange.Text = dataLoan.sRe885ComparisonProposedLoanMPmt6YrsNoRateChange_rep;
            sRe885ComparisonProposedLoanMPmt6Yrs2PercentRaise.Text = dataLoan.sRe885ComparisonProposedLoanMPmt6Yrs2PercentRaise_rep;
            sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaise.Text = dataLoan.sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaise_rep;

            sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaiseDiff.Text = dataLoan.sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaiseDiff_rep;
            sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaise.Text = dataLoan.sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaise_rep;
            
            sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaiseDiff.Text = dataLoan.sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaiseDiff_rep;
            
            sRe885ComparisonProposedLoanBalanceAfter5Yrs.Text = dataLoan.sRe885ComparisonProposedLoanBalanceAfter5Yrs_rep;
            sRe885ComparisonProposedLoanBalanceAfter5YrsReduceBy.Text = dataLoan.sRe885ComparisonProposedLoanBalanceAfter5YrsReduceBy_rep;

            sRe885ComparisionProposedLoanBalanceReduceYesNoDesc.Text = dataLoan.sRe885ComparisionProposedLoanBalanceReduceYesNoDesc;
            sRe885ComparisionProposedLoanBalanceReduceDesc.Text = dataLoan.sRe885ComparisionProposedLoanBalanceReduceDesc;

            Tools.Bind_TriState(sRe885ComparisonProductsNotOfferedDeemReliableTri);
            Tools.Set_TriState(sRe885ComparisonProductsNotOfferedDeemReliableTri, dataLoan.sRe885ComparisonProductsNotOfferedDeemReliableTri);
            sRe885ComparisonProductsNotOfferedDeemReliableTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);

            // -- Your Gross Income
            sRe885ComparisonPrincipalInterestMinPmt_sLTotI.Text = dataLoan.sLTotI_rep;
            sRe885ComparisonInterestOnlyMinPmt_sLTotI.Text = dataLoan.sLTotI_rep;
            sRe885Comparison5YrsArmMinPmt_sLTotI.Text = dataLoan.sLTotI_rep;
            sRe885Comparison5YrsArmInterestMinPmt_sLTotI.Text = dataLoan.sLTotI_rep;
            sRe885ComparisonOptionPaymentMinPmt_sLTotI.Text = dataLoan.sLTotI_rep;
            sRe885ComparisonProposedLoanMinPmt_sLTotI.Text = dataLoan.sLTotI_rep;

            sRe885ComparisonPrincipalInterestMaxPmt_sLTotI.Text = dataLoan.sLTotI_rep;
            sRe885ComparisonInterestOnlyMaxPmt_sLTotI.Text = dataLoan.sLTotI_rep;
            sRe885Comparison5YrsArmMaxPmt_sLTotI.Text = dataLoan.sLTotI_rep;
            sRe885Comparison5YrsArMaxterestMaxPmt_sLTotI.Text = dataLoan.sLTotI_rep;
            sRe885ComparisonOptionPaymentMaxPmt_sLTotI.Text = dataLoan.sLTotI_rep;
            sRe885ComparisonProposedLoanMaxPmt_sLTotI.Text = dataLoan.sLTotI_rep;

            // ContactFieldMapper
            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            GfeTilCompanyName.Text = gfeTil.CompanyName;
            GfeTilStreetAddr.Text = gfeTil.StreetAddr;
            GfeTilCity.Text = gfeTil.City;
            GfeTilState.Value = gfeTil.State;
            GfeTilZip.Text = gfeTil.Zip;
            GfeTilLicenseNumOfCompany.Text = gfeTil.LicenseNumOfCompany;
            GfeTilLicenseNumOfAgent.Text = gfeTil.LicenseNumOfAgent;
            GfeTilPreparerName.Text = gfeTil.PreparerName;

            CFM.IsLocked = gfeTil.IsLocked;
            CFM.AgentRoleT = gfeTil.AgentRoleT;
        }

        public void SaveData()
        {
        }

        private void ZeroOneCheckBoxList_Load(object sender, System.EventArgs e)
        {
            CheckBoxList cbl = (CheckBoxList)sender;
            ArrayList list = new ArrayList();

            foreach (ListItem o in cbl.Items)
            {
                list.Add("'" + o.Value + "'");
            }

            Page.ClientScript.RegisterArrayDeclaration(cbl.ClientID + "_items", string.Join(",", (string[])list.ToArray(typeof(string))));
            Page.ClientScript.RegisterHiddenField("cbl_" + cbl.ClientID, "");
            ((BasePage)Page).AddInitScriptFunctionWithArgs("_registerCBL", string.Format("'{0}', {1}", cbl.ClientID, cbl.Items.Count));
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }

}