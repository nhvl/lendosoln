<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page language="c#" Codebehind="TruthInLendingTemplateList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.TruthInLendingTemplateList" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>TruthInLendingTemplateList</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </HEAD>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro" scroll="yes">
	<script language=javascript>
<!--
function _init() {
  resize(400, 500);
}
function f_select(id) {
  var arg = window.dialogArguments || {};
  arg.OK = true;
  arg.id = id;
  onClosePopup(arg);
}
//-->
</script>
    <h4 class="page-header">Loan Program Template List</h4>
    <form id="TruthInLendingTemplateList" method="post" runat="server">
      <table class="FormTable" cellspacing="0" cellpadding="0" width="100%" border="0">
        <TR>
          <TD>
            <ml:CommonDataGrid id="m_loanProgramsDG" runat="server">
              <columns>
                <asp:boundcolumn HeaderText="Loan Program Template Name" DataField="lLpTemplateNm" />
                <asp:templatecolumn>
                  <ItemTemplate>
                  <a href="#" onclick=<%#AspxTools.HtmlAttribute("f_select(" + AspxTools.JsString((Guid)DataBinder.Eval(Container.DataItem, "lLpTemplateId")) + ");")%>>select</a>
                  </ItemTemplate>
                </asp:templatecolumn>                
              </columns>
            </ml:CommonDataGrid></TD>
        </TR>
        <TR>
          <TD align="center">
            <INPUT type="button" value="Close" onclick="onClosePopup();"></TD>
        </TR>      
      </table>
     </form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
	
  </body>
</HTML>
