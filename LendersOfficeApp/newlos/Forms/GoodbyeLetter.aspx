﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GoodbyeLetter.aspx.cs" Inherits="LendersOfficeApp.newlos.Forms.GoodbyeLetter" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml" >

<head runat="server">
    <title>Notice of Loan Transfer</title>
    <style type="text/css">
        .wide
        {
            width: 350px;
        }
        .addrSync
        {
            width: 100px;
        }
        .mediumWidth
        {
            width: 250px;
        }
    </style>
</head>
<body bgcolor="gainsboro" style="padding: 5px;">
    <script type="text/javascript">
        // Populate new servicer fields if investor name found in combobox.
        function f_populateInvestorInfo() {
            var investorName = $(this).val();
            var bPopulateData = typeof investorNameMap === 'object' && investorNameMap.hasOwnProperty(investorName);
            if (!bPopulateData) {
                return;
            }

            callWebMethodAsync({
                type: "POST",
                url: "GoodbyeLetter.aspx/GetInvestorInfo",
                data: '{ id :' + investorNameMap[investorName] + '}',
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                success: function(msg) {
                    $('#sGLNewServAddr').val(msg.d.MainAddress);
                    $('#sGLNewServCity').val(msg.d.MainCity);
                    $('#sGLNewServState').val(msg.d.MainState);
                    $('#sGLNewServZip').val(msg.d.MainZip);
                    $('#sGLNewServTFPhone').val(msg.d.MainPhone);
                    $('#sGLNewServContactNm').val(msg.d.MainContactName);
                    $('#sGLNewServContactNum').val(msg.d.MainPhone);
                    $('#sGLNewServPmtAddr').val(msg.d.PaymentToAddress);
                    $('#sGLNewServPmtCity').val(msg.d.PaymentToCity);
                    $('#sGLNewServPmtState').val(msg.d.PaymentToState);
                    $('#sGLNewServPmtZip').val(msg.d.PaymentToZip);
                },
                error: function() {
//                    alert('Could not fetch investor data.'); fail silently
                }
            });
        }
        
        // Update the Accepting Payments date to match the Transfer Effective date
        function f_onchange_sGLServTransEffD() {
            $('#sGLNewServAccPmtD').val(this.value);
            f_updateDateOfFirstPayment();
        }

        function f_updateDateOfFirstPayment() {
            var acceptingPmtD = $('#sGLNewServAccPmtD').val();
            var args = {
                loanId: ML.sLId,
                acceptingD: acceptingPmtD
            };
            var d = JSON.stringify(args);
            callWebMethodAsync({
                type: "POST",
                url: "GoodbyeLetter.aspx/GetSchedDateOfFirstPmt",
                data: JSON.stringify(args),
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                success: function(msg) {
                    // If the result could not be determined, don't wipe out
                    // existing data.
                    if (msg.d !== '') {
                        $('#sGLNewServFirstPmtSchedD').val(msg.d);
                    }
                },
                error: function() {
//                    alert('Could not fetch payment data.'); fail silently
                }
            });
        }

        $(function() {
            // Don't attach events with jquery so they will work with the
            // combobox and datetextbox controls.
            document.getElementById('sGLNewServNm').onchange = f_populateInvestorInfo;
            document.getElementById('sGLServTransEffD').onchange = f_onchange_sGLServTransEffD;
            document.getElementById('sGLNewServAccPmtD').onchange = f_updateDateOfFirstPayment;
        });
    </script>
    <span class="MainRightHeader" style="width: 100%;">Notice of Loan Transfer</span>
    <form id="form1" runat="server">        
        <table class="InsetBorder">
            <tr>
                <td colspan="2">
                    <label class="FieldLabel">Servicing Transfer Effective Date:</label>
                    <ml:DateTextBox ID="sGLServTransEffD" runat="server"/>            
                </td>
            </tr>
            <tr>
                <td class="FieldLabel addrSync">Completed By</td>
                <td>
                    <asp:TextBox id="sGLCompletedBy" runat="server" class="wide"></asp:TextBox>
                    <asp:HiddenField ID="sGLCompletedByBrokerName" runat="server" />
                </td>
            </tr>
        </table>

        <table cellpadding="0" cellspacing="0" border="0">
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td class="FormTableHeader">Previous Servicer Information</td>
        </tr>
        <tr>
            <td>
                <table class="InsetBorder">
                    <tr>
                        <td></td>
                        <td><uc:CFM id="m_prevServCFM" runat="server" 
                                DisableAddTo="true" 
                                CompanyNameField="sGLPrevServNm" 
                                StreetAddressField="sGLPrevServAddr" 
                                CityField="sGLPrevServCity" 
                                StateField="sGLPrevServState" 
                                ZipField="sGLPrevServZip" 
                                CompanyPhoneField="sGLPrevServTFPhone"
                                AgentNameField="sGLPrevServContactNm"
                                PhoneField="sGLPrevServContactNum"></uc:CFM>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Name</td>
                        <td><asp:TextBox ID="sGLPrevServNm" runat="server" class="wide"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel addrSync">Address</td>
                        <td><asp:TextBox ID="sGLPrevServAddr" runat="server" class="wide"></asp:TextBox></td>
                        <tr>
                            <td></td>
                            <td>
                                <asp:TextBox ID="sGLPrevServCity" runat="server" class="mediumWidth"></asp:TextBox>
                                <ml:StateDropDownList ID="sGLPrevServState" runat="server" />
                                <ml:ZipcodeTextBox ID="sGLPrevServZip" runat="server" />
                            </td>
                        </tr>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Toll-Free Phone</td>
                        <td><asp:TextBox ID="sGLPrevServTFPhone" runat="server" preset="phone" class="mediumWidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Contact Name</td>
                        <td><asp:TextBox ID="sGLPrevServContactNm" runat="server" class="wide"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Contact Number</td>
                        <td><asp:TextBox ID="sGLPrevServContactNum" runat="server" preset="phone" class="mediumWidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Business Hours</td>
                        <td>
                            <asp:TextBox ID="sGLPrevServBusHrAM" runat="server" style="width: 40px;"></asp:TextBox>
                            AM &nbsp; To
                            <asp:TextBox ID="sGLPrevServBusHrPM" runat="server" style="width: 40px;"></asp:TextBox>
                            PM
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="FormTableHeader">New Servicer Information</td>
        </tr>
        <tr>
            <td>
                <table class="InsetBorder">
                    <tr>
                        <td></td>
                        <td><uc:CFM id="CFM1" runat="server" 
                                DisableAddTo="true" 
                                CompanyNameField="sGLNewServNm" 
                                StreetAddressField="sGLNewServAddr" 
                                CityField="sGLNewServCity" 
                                StateField="sGLNewServState" 
                                ZipField="sGLNewServZip" 
                                CompanyPhoneField="sGLNewServTFPhone"
                                AgentNameField="sGLNewServContactNm"
                                PhoneField="sGLNewServContactNum"></uc:CFM>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Name</td>
                        <td><ml:ComboBox id="sGLNewServNm" runat="server" IsEditable="True" onchange="" CssClass="wide"></ml:ComboBox></td>
                        
                    </tr>
                    <tr>
                        <td class="FieldLabel">Address</td>
                        <td><asp:TextBox ID="sGLNewServAddr" runat="server" class="wide"></asp:TextBox></td>
                        <tr>
                            <td></td>
                            <td>
                                <asp:TextBox ID="sGLNewServCity" runat="server" class="mediumWidth"></asp:TextBox>
                                <ml:StateDropDownList ID="sGLNewServState" runat="server" />
                                <ml:ZipcodeTextBox ID="sGLNewServZip" runat="server" />
                            </td>
                        </tr>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Toll-Free Phone</td>
                        <td><asp:TextBox ID="sGLNewServTFPhone" runat="server" preset="phone" class="mediumWidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Contact Name</td>
                        <td><asp:TextBox ID="sGLNewServContactNm" runat="server" class="wide"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Contact Number</td>
                        <td><asp:TextBox ID="sGLNewServContactNum" runat="server" preset="phone" class="mediumWidth"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Business Hours</td>
                        <td>
                            <asp:TextBox ID="sGLNewServBusHrAM" runat="server" style="width: 40px;"></asp:TextBox>
                            AM &nbsp; To
                            <asp:TextBox ID="sGLNewServBusHrPM" runat="server" style="width: 40px;"></asp:TextBox>
                            PM
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label class="FieldLabel">New Servicer Accepting Payments Starting</label>
                            <ml:DateTextBox ID="sGLNewServAccPmtD" runat="server"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel addrSync">Payment Address</td>
                        <td><asp:TextBox ID="sGLNewServPmtAddr" runat="server" class="wide"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:TextBox ID="sGLNewServPmtCity" runat="server" class="mediumWidth"></asp:TextBox>
                            <ml:StateDropDownList ID="sGLNewServPmtState" runat="server" />
                            <ml:ZipcodeTextBox ID="sGLNewServPmtZip" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label class="FieldLabel">Scheduled Date of First Payment to New Servicer</label>
                            <ml:DateTextBox ID="sGLNewServFirstPmtSchedD" runat="server"></ml:DateTextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
    </form>
</body>
</html>
