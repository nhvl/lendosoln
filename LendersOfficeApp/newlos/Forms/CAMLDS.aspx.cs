using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Forms
{
	public partial class CAMLDS : BaseLoanPage
	{
        protected MLDSpg1 MLDSpg1;
        protected MLDSpg2 MLDSpg2;

        protected override void OnInit(EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;

            UseNewFramework = true;
            IsAppSpecific = false;
            Tabs.AddToQueryString("loanid", LoanID.ToString());
            Tabs.RegisterControl("Page 1", MLDSpg1);
            Tabs.RegisterControl("Page 2", MLDSpg2);
            base.OnInit(e);
        }




        protected override void OnPreRender(EventArgs e)
        {
            int selectedIndex = Tabs.TabIndex + 1;

            this.PageTitle = "MLDS page " + selectedIndex;
            this.PageID = "CAMLDS_" + selectedIndex;
            switch (selectedIndex)
            {
                case 1:
                    this.PDFPrintClass = typeof(LendersOffice.Pdf.CRe882_2010_1PDF);
                    break;
                case 2:
                    this.PDFPrintClass = typeof(LendersOffice.Pdf.CRe882_2010_2PDF);
                    break;
            }
            base.OnPreRender(e);
        }
	}
}
