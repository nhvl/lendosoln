using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
    public class MortgageLoanCommitmentServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(MortgageLoanCommitmentServiceItem));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            // 4/15/2004 dd - This page doesn't have any calculation field. Don't have to reload.
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.MortgageLoanCommitment, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName     = GetString("MortgageLoanCommitmentCompanyName");
            preparer.StreetAddr      = GetString("MortgageLoanCommitmentStreetAddr");
            preparer.City            = GetString("MortgageLoanCommitmentCity");
            preparer.State           = GetString("MortgageLoanCommitmentState");
            preparer.Zip             = GetString("MortgageLoanCommitmentZip");
            preparer.PrepareDate_rep = GetString("MortgageLoanCommitmentPrepareDate");
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.MortgageLoanCommitmentAlternateLender, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("MortgageLoanCommitmentAlternateLenderCompanyName");
            preparer.StreetAddr  = GetString("MortgageLoanCommitmentAlternateLenderStreetAddr");
            preparer.City        = GetString("MortgageLoanCommitmentAlternateLenderCity");
            preparer.State       = GetString("MortgageLoanCommitmentAlternateLenderState");
            preparer.Zip         = GetString("MortgageLoanCommitmentAlternateLenderZip");
            preparer.Update();

            dataLoan.sCommitReturnToFollowAddr   = GetBool("sCommitReturnToFollowAddr");
            dataLoan.sCommitReturnToAboveAddr    = GetBool("sCommitReturnToAboveAddr");
            dataLoan.sCommitReturnWithinDays_rep = GetString("sCommitReturnWithinDays");
            dataLoan.sCommitTitleEvidence        = GetString("sCommitTitleEvidence");
            dataLoan.sCommitRepayTermsDesc       = GetString("sCommitRepayTermsDesc");

            dataLoan.sNoteIR_rep                 = GetString("sNoteIR");
            dataLoan.sTerm_rep                   = GetString("sTerm");
            dataLoan.sDue_rep                    = GetString("sDue");
            dataLoan.sCommitExpD_rep             = GetString("sCommitExpD");

            dataLoan.sMldsHasImpound             = !GetBool("sMldsHasImpound"); // escrow waived
        }
    }
	/// <summary>
	/// Summary description for MortgageLoanCommitmentService.
	/// </summary>
	public partial class MortgageLoanCommitmentService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new MortgageLoanCommitmentServiceItem());
        }

	}
}
