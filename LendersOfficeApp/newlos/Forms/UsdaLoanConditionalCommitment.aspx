﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UsdaLoanConditionalCommitment.aspx.cs" Inherits="LendersOfficeApp.newlos.Forms.UsdaLoanConditionalCommitment" %>
<%@ Register TagPrefix="uc" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>USDA RD 1980-18</title>
    <style type="text/css">
        body { background-color: gainsboro; }
        span { display: inline-block; font-weight: bold; }
        label { margin-top: 2px; }
        label.block { display: block; }
        label.inline-block { display: inline-block; }
        label input.wide { width: 230px; }
        .column-left { float: left; margin-right: 20px; height: 90px; }
        .column-left label span { width: 90px; }
        .align-left-column { margin-left: 90px; }
        .column-right label span { width: 110px; }
        .column-right label span.special { padding-left: 10px; width: 80px; }
        .lender-address-fields { padding-top: 2px; }
        .clear { clear: both }
        .borrower-info { font-weight: bold; padding-left: 5px; }
        .borrower-info span { width: 80px; }
        .borrower-info label { display: inline-block; padding-left: 2px; padding-right: 2px; }
        .name { width: 130px; }
        .ssn { width: 90px; }
        .suffix { width: 70px; }
        .fees label span { width: 140px; }
        .dates label span { width: 190px; }
        .cushion-top { padding-top: 10px; }
        .cushion-left { padding-left: 5px; }
        .money { width: 90px; text-align: right; }
        .percent { width: 70px; }
        #sUsdaStateCode { width: 38px; }
        #sUsdaCountyCode { width: 50px; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    
    <div class="MainRightHeader">USDA RD 1980-18 - Conditional Commitment for Single Housing Guarantee</div>
    <div class="align-left-column">
            <uc:CFM runat="server" ID="CFM" 
                CompanyNameField="LenderNm" 
                CompanyStreetAddr="LenderAddr" 
                CompanyCity="LenderCity" 
                CompanyState="LenderState"
                CompanyZip="LenderZip" />
    </div>
    
    <div class="column-left cushion-left">
        <label class="block"><span>Lender name</span><asp:TextBox runat="server" ID="LenderNm" CssClass="wide"></asp:TextBox></label>
        <label class="block"><span>Address</span><asp:TextBox runat="server" ID="LenderAddr" CssClass="wide"></asp:TextBox></label>
        <div class="lender-address-fields align-left-column">
            <asp:TextBox runat="server" ID="LenderCity"></asp:TextBox>
            <ml:StateDropDownList runat="server" ID="LenderState" />
            <ml:ZipcodeTextBox runat="server" ID="LenderZip"></ml:ZipcodeTextBox>
        </div>
        
    </div>
    
    <div class="column-right">
        <label class="block"><span>USDA Borrower ID</span><asp:TextBox runat="server" ID="sUsdaBorrowerId" preset="usdaBorrowerId"></asp:TextBox></label>
        <div>
            <label class="inline-block"><span>Property State</span><ml:StateDropDownList runat="server" ID="sSpState" onchange="UpdateCounties(this, document.getElementById('sSpCounty'), event);" /></label>
            <label class="inline-block"><span class="special">County</span><asp:DropDownList runat="server" ID="sSpCounty"></asp:DropDownList></label>
        </div>

        <div>
            <label class="inline-block"><span>State code</span><asp:TextBox runat="server" ID="sUsdaStateCode" MaxLength="2"></asp:TextBox></label>
            <label class="inline-block"><span class="special">County code</span><asp:TextBox runat="server" ID="sUsdaCountyCode" MaxLength="3"></asp:TextBox></label>
        </div>        
        
        <label class="block"><span>Total loan amount</span><asp:TextBox runat="server" ID="sFinalLAmt" ReadOnly="true" class="money"></asp:TextBox></label>
    </div>

    <div class="clear borrower-info">
        <span>Borrower</span>
        <label class="name"><span>First name</span><asp:TextBox runat="server" ID="sCombinedBorFirstNm" ReadOnly="true" class="name"></asp:TextBox></label>
        <label class="name"><span>Middle name</span><asp:TextBox runat="server" ID="sCombinedBorMidNm" ReadOnly="true" class="name"></asp:TextBox></label>
        <label class="name"><span>Last name</span><asp:TextBox runat="server" ID="sCombinedBorLastNm" ReadOnly="true" class="name"></asp:TextBox></label>
        <label class="suffix"><span>Suffix</span><asp:TextBox runat="server" ID="sCombinedBorSuffix" ReadOnly="true" class="suffix"></asp:TextBox></label>
        <label class="ssn"><span>SSN</span><asp:TextBox runat="server" ID="sCombinedBorSsn" ReadOnly="true" class="ssn"></asp:TextBox></label>
    </div>
    
    <div class="fees cushion-top cushion-left">
        <label class="block"><span>Up-front Guarantee fee</span><asp:TextBox runat="server" ID="sFfUfmip1003" ReadOnly="true" class="money"></asp:TextBox></label>
        <label class="block"><span>Note rate</span><asp:TextBox runat="server" ID="sNoteIR" ReadOnly="true" class="percent"></asp:TextBox></label>
        <label class="block"><span>Annual Fee</span><asp:TextBox runat="server" ID="sUsdaAnnualFee" ReadOnly="true" class="money"></asp:TextBox></label>
    </div>
    
    
    <div class="dates cushion-top cushion-left">
        <label class="block"><span>Date of conditional commitment</span><ml:DateTextBox runat="server" ID="sMiCommitmentReceivedD"></ml:DateTextBox></label>
    </div>
    

    </form>
</body>
</html>
