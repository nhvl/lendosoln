﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CAMLDSRE885pg4.ascx.cs" Inherits="LendersOfficeApp.newlos.Forms.CAMLDSRE885pg4" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>

<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<style type="text/css">
    #TypicalMortgageTransactions
    {
        border-collapse:collapse;
    }
    #TypicalMortgageTransactions td.BottomField
    {
    	vertical-align: top;
    	text-align: center;
    }
    #TypicalMortgageTransactions td.MiddleField
    {
    	vertical-align: bottom;
    	text-align: center;
    }
    #TypicalMortgageTransactions td.MiddleFieldDesc
    {
    	text-align: center;
    	font-weight: bold;
    }
    #SectionTitle
    {
    	font-weight: bolder;
    }
    .LoanBalanceReduced
    {
    	position:relative;
    	height: 6em;
    }
    .SubTableHeading
    {
    	font-weight:bold;
    	font-style: italic;
    }
    .ProposedLoanAmount
    {
    	text-align: center;
        font-weight:bold;
    }
    .LoanType
    {
    	font-weight: bold;
        text-decoration: underline;
    }
    .LoanAmortType
    {
    	font-weight: bold;
        font-style: italic;
    }
    .ProposedLoan
    {
    	font-weight: bold;
    }
    .TopDiv
    {
    	font-weight: bold;
    	position:absolute;
    	top: 0px;
    	left: 0px;
    	width: 100%;
    }
    .BottomDiv
    {
    	position:absolute;
    	bottom: 0px;
    	left: 0px;
    	width: 100%;
    }
    .Asterisk
    {
    	text-decoration: none;
    }
    .AsteriskColumn
    {
    	vertical-align: top;
    	width:5%;
    }
</style>
<script type="text/javascript">
<!--
    $(function() {
        $("#TypicalMortgageTransactions td:not(.NoBorder)").css("border", "solid black 1px");
    });
//-->
</script>
<table class="InsetBorder" cellpadding="0" cellspacing="0" width="99%" border="0">
    <tr>
        <td colspan="7" class="FormTableSubheader NoBorder">
            XIX. Comparison of Sample Mortgage Features (One to Four Residential Units)
        </td>
    </tr>
    <tr>
        <td colspan="7" class="NoBorder" id="SectionTitle">
            TYPICAL MORTGAGE TRANSACTIONS
        </td>
    </tr>
    <tr>
        <td>
            <table id="TypicalMortgageTransactions" cellpadding="2px" cellspacing="0" width="100%" border="0">
                <tr>
                    <td rowspan="2" class="NoBorder ProposedLoanAmount">
                        PROPOSED<br />
                        LOAN AMOUNT<br />
                        <ml:MoneyTextBox ReadOnly="true" ID="sFinalLAmt" runat="server"></ml:MoneyTextBox><br />
                        <asp:TextBox ReadOnly="true" Width="38px" ID="sTermInYr" runat="server"></asp:TextBox>-YEAR TERM
                    </td>
                    <td class="MiddleField">
                        <div class="LoanType">
                            Principal and <br />
                            Interest
                        </div><br />
                        <div class="LoanAmortType">Fully Amortizing</div>
                        <asp:CheckBox id="sRe885ComparisonPrincipalInterestIsNotOffered" runat="server" Text="Not Offered" /><a href="#" class="Asterisk" tabindex="-1"  tabindex="-1" title='"Not offered" indicates the broker does not offer the comparison loan product.'>*</a>
                    </td>
                    <td class="MiddleField">
                        <div class="LoanType">Interest Only</div><br />
                        <div class="LoanAmortType">Fully Amortizing</div>
                        <asp:CheckBox id="sRe885ComparisonInterestOnlyIsNotOffered" runat="server" Text="Not Offered" /><a href="#" class="Asterisk" tabindex="-1"  tabindex="-1"  title='"Not offered" indicates the broker does not offer the comparison loan product.'>*</a>
                    </td>
                    <td class="MiddleField">
                        <div class="LoanType">5/1 ARM</div><br />
                        <div class="LoanAmortType">Fully Amortizing</div>
                        <asp:CheckBox id="sRe885Comparison5YrsArmIsNotOffered" runat="server" Text="Not Offered" /><a href="#" class="Asterisk" tabindex="-1"  tabindex="-1"  title='"Not offered" indicates the broker does not offer the comparison loan product.'>*</a>
                    </td>
                    <td class="MiddleField">
                        <div class="LoanType">Interest Only</div><br />
                        <div class="LoanAmortType">Fully Amortizing</div>
                        <asp:CheckBox id="sRe885Comparison5YrsArmInterestOnlyIsNotOffered" runat="server" Text="Not Offered" /><a href="#" class="Asterisk" tabindex="-1"  title='"Not offered" indicates the broker does not offer the comparison loan product.'>*</a>
                    </td>
                    <td class="MiddleField">
                        <div class="LoanType">Option Payment</div><br />
                        <div class="LoanAmortType">Fully Amortizing</div>
                        <asp:CheckBox id="sRe885ComparisonOptionPaymentIsNotOffered" runat="server" Text="Not Offered"/><a href="#" class="Asterisk" tabindex="-1"  title='"Not offered" indicates the broker does not offer the comparison loan product.'>*</a>
                    </td>
                    <td class="MiddleField">
                        <div class="ProposedLoan">
                            Proposed Loan<br />
                            Type of Loan:<br />
                            <asp:TextBox runat="server" ID="sRe885ComparisonProposedLoanTypeOfLoan"></asp:TextBox>
                        </div>
                        <div class="ProposedLoan">
                            Type of<br />
                            Amortization:<br />
                            <asp:TextBox runat="server" ID="sRe885ComparisonProposedLoanAmortType"></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="MiddleFieldDesc">
                        Fixed Rate<br />
                        <ml:PercentTextBox onchange="refreshCalculation();" runat="server" ID="sRe885ComparisonPrincipalInterestNoteIR"></ml:PercentTextBox>
                    </td>
                    <td class="MiddleFieldDesc">
                        Fixed Rate<br />
                        <ml:PercentTextBox onchange="refreshCalculation();" runat="server" ID="sRe885ComparisonInterestOnlyNoteIR" ></ml:PercentTextBox><br />
                        Interest Only for<br />
                        First 5 Years</td>
                    <td class="MiddleFieldDesc">
                        Fixed Rate for<br />
                        First 5 Years;<br />
                        Adjustable Each<br />
                        Year After<br />
                        First 5 Years<br />
                        (Initial rate for 1<br />
                        to 5 is <ml:PercentTextBox onchange="refreshCalculation();" runat="server" ID="sRe885Comparison5YrsArmInitialNoteIR" ></ml:PercentTextBox>;<br />
                        Maximum Rate is<br />
                        <ml:PercentTextBox onchange="refreshCalculation();" runat="server" ID="sRe885Comparison5YrsArmMaximumNoteIR" ></ml:PercentTextBox>)</td>
                    <td class="MiddleFieldDesc">
                        Interest Only<br />
                        and Fixed Rate<br />
                        for First 5 Years;<br />
                        Adjustable Rate<br />
                        Each Year After<br />
                        First 5 Years<br />
                        (Initial rate for 1<br />
                        to 5 is <ml:PercentTextBox onchange="refreshCalculation();" runat="server" ID="sRe885Comparison5YrsArmInterestOnlyInitialNoteIR" ></ml:PercentTextBox>;<br />
                        Maximum Rate is <br />
                        <ml:PercentTextBox onchange="refreshCalculation();" runat="server" ID="sRe885Comparison5YrsArmInterestOnlyMaximumNoteIR" ></ml:PercentTextBox>)</td>
                    <td class="MiddleFieldDesc">
                        Adjustable Rate<br />
                        for Entire Term<br />
                        of the Mortgage<br />
                        (Rate in month<br />
                        1 is <ml:PercentTextBox onchange="refreshCalculation();" runat="server" ID="sRe885ComparisonOptionPaymentFirstMonthNoteIR" ></ml:PercentTextBox>;<br />
                        Rate in month 2<br />
                        through year 5 is <br />
                        <ml:PercentTextBox onchange="refreshCalculation();" runat="server" ID="sRe885ComparisonOptionPaymentInitialNoteIR" ></ml:PercentTextBox>;<br />
                        Maximum Rate is <br />
                        <ml:PercentTextBox onchange="refreshCalculation();" runat="server" ID="sRe885ComparisonOptionPaymentMaximumNoteIR" ></ml:PercentTextBox>)</td>
                    <td class="MiddleFieldDesc">
                        Explanation of<br />
                        Type of Proposed<br />
                        Loan Product:<br />
                        <asp:TextBox runat="server" textmode="MultiLine" ID="sRe885ComparisonProposedLoanProductDesc" Rows="5" Width="10em"></asp:TextBox>

                    </td>
                </tr>
                <tr>
                    <td colspan="7" class="NoBorder SubTableHeading">
                        Payment Scenarios <!-- Empty space -->
                    </td>
                </tr>
                <tr>
                    <td>
                        Minimum Monthly<br />
                        Payment Years 1‑5<br />
                        except as noted
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonPrincipalInterestMinPmtFirst5Yrs" runat="server" /><a href="#" class="Asterisk" tabindex="-1"  title="This illustrates an interest rate and payments that are fixed for the life of the loan.">**</a>
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonInterestOnlyMinPmtFirst5Yrs" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885Comparison5YrsArmMinPmtFirst5Yrs" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885Comparison5YrsArmInterestOnlyMinPmtFirst5Yrs" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonOptionPaymentMinPmtFirst5Yrs" runat="server" /><a href="#" class="Asterisk" tabindex="-1"  title="This illustrates minimum monthly payments that are based on an interest rate that is in effect during the first month only. The payments required during the first year will not be sufficient to cover all of the interest that is due when the rate increases in the second month of the loan. Any unpaid interest amount will be added to the loan balance. Minimum payments for years 2-5 are based on the higher interest rate in effect at the time, subject to any contract limits on payment increases. Minimum payments will be recast (recalculated) after 5 years, or when the loan balance reaches a certain limit, to cover both principal and interest at the applicable rate.">****</a>
                        <br />
                        (1st year only)
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonProposedLoanMinPmtFirst5Yrs" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Monthly Payment in<br />
                        Year 6 with no change<br />
                        in rates
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonPrincipalInterestMPmt6YrsNoRateChange" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonInterestOnlyMPmt6YrsNoRateChange" runat="server" /><a href="#" class="Asterisk" tabindex="-1"  title="This illustrates payments that are fixed after the first five years of the loan at a higher amount because they include both principal and interest.">***</a>
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885Comparison5YrsArmMPmt6YrsNoRateChange" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885Comparison5YrsArmInterestOnlyMPmt6YrsNoRateChange" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonOptionPaymentMPmt6YrsNoRateChange" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonProposedLoanMPmt6YrsNoRateChange" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Monthly Payment in<br />
                        Year 6 with a 2% rise<br />
                        in rates
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonPrincipalInterestMPmt6Yrs2PercentRaise" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonInterestOnlyMPmt6Yrs2PercentRaise" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885Comparison5YrsArmMPmt6Yrs2PercentRaise" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885Comparison5YrsArmInterestOnlyMPmt6Yrs2PercentRaise" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonOptionPaymentMPmt6Yrs2PercentRaise" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonProposedLoanMPmt6Yrs2PercentRaise" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Minimum Monthly<br />
                        Payment
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaise" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaise" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaise" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaise" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaise" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaise" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Your Gross Income
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885ComparisonPrincipalInterestMinPmt_sLTotI" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885ComparisonInterestOnlyMinPmt_sLTotI" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885Comparison5YrsArmMinPmt_sLTotI" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885Comparison5YrsArmInterestMinPmt_sLTotI" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885ComparisonOptionPaymentMinPmt_sLTotI" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885ComparisonProposedLoanMinPmt_sLTotI" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Difference
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885ComparisonPrincipalInterestMinPmt6Yrs2PercentRaiseDiff" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885ComparisonInterestOnlyMinPmt6Yrs2PercentRaiseDiff" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885Comparison5YrsArmMinPmt6Yrs2PercentRaiseDiff" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885Comparison5YrsArmInterestOnlyMinPmt6Yrs2PercentRaiseDiff" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885ComparisonOptionPaymentMinPmt6Yrs2PercentRaiseDiff" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885ComparisonProposedLoanMinPmt6Yrs2PercentRaiseDiff" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Maximum Monthly<br />
                        Payment in Year 6 with<br />
                        a 5% rise in rates
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaise" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaise" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaise" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaise" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaise" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaise" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Your Gross Income
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885ComparisonPrincipalInterestMaxPmt_sLTotI" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885ComparisonInterestOnlyMaxPmt_sLTotI" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885Comparison5YrsArmMaxPmt_sLTotI" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885Comparison5YrsArMaxterestMaxPmt_sLTotI" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885ComparisonOptionPaymentMaxPmt_sLTotI" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885ComparisonProposedLoanMaxPmt_sLTotI" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Difference
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885ComparisonPrincipalInterestMaxPmt6Yrs6PercentRaiseDiff" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885ComparisonInterestOnlyMaxPmt6Yrs6PercentRaiseDiff" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885Comparison5YrsArmMaxPmt6Yrs6PercentRaiseDiff" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885Comparison5YrsArmInterestOnlyMaxPmt6Yrs6PercentRaiseDiff" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885ComparisonOptionPaymentMaxPmt6Yrs6PercentRaiseDiff" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox ReadOnly="true" id="sRe885ComparisonProposedLoanMaxPmt6Yrs6PercentRaiseDiff" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="7" class="NoBorder SubTableHeading">
                        Loan Balance Scenarios
                    </td>
                </tr>
                <tr>
                    <td>
                        How much will be<br />
                        owed after 5 years?
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonPrincipalInterestBalanceAfter5Yrs" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonInterestOnlyBalanceAfter5Yrs" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885Comparison5YrsArmBalanceAfter5Yrs" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885Comparison5YrsArmInterestOnlyBalanceAfter5Yrs" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonOptionPaymentBalanceAfter5Yrs" runat="server" />
                    </td>
                    <td class="MiddleField">
                        <ml:MoneyTextBox onchange="refreshCalculation();" id="sRe885ComparisonProposedLoanBalanceAfter5Yrs" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Has the loan balance<br />
                        been reduced after 5<br />
                        years of payments?
                    </td>
                    <td class="BottomField">
                        <div class="LoanBalanceReduced">
                            <div class="TopDiv">Yes</div>
                            <div class="BottomDiv">
                                The loan balance <br />
                                was reduced <br />
                                by
                                <ml:MoneyTextBox Readonly="true" id="sRe885ComparisonPrincipalInterestBalanceAfter5YrsReduceBy" runat="server" />
                            </div>
                        </div>
                    </td>
                    <td class="BottomField">
                        <div class="LoanBalanceReduced">
                            <div class="TopDiv">No</div>
                            <div class="BottomDiv">
                                The loan balance<br />
                                was not reduced
                            </div>
                        </div>
                    </td>
                    <td class="BottomField">
                        <div class="LoanBalanceReduced">
                            <div class="TopDiv">Yes</div>
                            <div class="BottomDiv">
                                The loan balance<br />
                                was reduced<br />
                                by
                                <ml:MoneyTextBox Readonly="true" id="sRe885Comparison5YrsArmBalanceAfter5YrsReduceBy" runat="server" />
                            </div>
                        </div>
                    </td>
                    <td class="BottomField">
                        <div class="LoanBalanceReduced">
                            <div class="TopDiv">No</div>
                            <div class="BottomDiv">
                                The loan balance<br />
                                was not reduced.
                            </div>
                        </div>
                    </td>
                    <td class="BottomField">
                        <div class="LoanBalanceReduced">
                            <div class="TopDiv">No</div>
                            <div class="BottomDiv">
                                The loan balance<br />
                                increased<br />
                                by
                                <ml:MoneyTextBox Readonly="true" id="sRe885ComparisonOptionPaymentBalanceAfter5YrsReduceBy" runat="server" />
                            </div>
                        </div>
                    </td>
                    <td class="BottomField">
                        <asp:TextBox ReadOnly="true" style="text-align:center; font-weight: bold;" Width="8em" runat="server" ID="sRe885ComparisionProposedLoanBalanceReduceYesNoDesc"></asp:TextBox><br />
                        The loan balance <br />
                        <asp:TextBox ReadOnly="true" style="text-align:center" Width="8em" runat="server" ID="sRe885ComparisionProposedLoanBalanceReduceDesc"></asp:TextBox><br />
                        by <ml:MoneyTextBox ReadOnly="true" id="sRe885ComparisonProposedLoanBalanceAfter5YrsReduceBy" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td class="AsteriskColumn">
                        *
                    </td>
                    <td>
                        <!-- CheckBoxList inserts a new line. Why. -->
                        <span style="float:left;">
                            "Not offered" indicates the broker does not offer the comparison loan product. <br />
                            The information provided for the products not offered was obtained from sources deemed reliable.
                        </span>
                        <span style="float:left;">
                            <asp:CheckBoxList id="sRe885ComparisonProductsNotOfferedDeemReliableTri" runat="server" RepeatDirection="Horizontal"></asp:CheckBoxList>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="AsteriskColumn">
                        **
                    </td>
                    <td>
                        This illustrates an interest rate and payments that are fixed for the life of the loan.
                    </td>
                </tr>
                <tr>
                    <td class="AsteriskColumn">
                        ***
                    </td>
                    <td>
                        This illustrates payments that are fixed after the first five years of the loan at a higher
                        amount because they include both principal and interest.
                    </td>
                </tr>
                <tr>
                    <td class="AsteriskColumn">
                        ****
                    </td>
                    <td>
                        This illustrates minimum monthly payments that are based on an interest rate that is in
                        effect during the first month only. The payments required during the first year will not
                        be sufficient to cover all of the interest that is due when the rate increases in the
                        second month of the loan. Any unpaid interest amount will be added to the loan balance.
                        Minimum payments for years 2‑5 are based on the higher interest rate in effect at the time,
                        subject to any contract limits on payment increases. Minimum payments will be recast
                        (recalculated) after 5 years, or when the loan balance reaches a certain limit, to cover
                        both principal and interest at the applicable rate.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table class="InsetBorder" cellpadding="0" cellspacing="0" width="99%" border="0">
    <tr>
        <td class="FormTableSubheader">XX. NOTICE TO BORROWER:</td>
    </tr>
    <tr>
        <td>
            <table>
                <colgroup></colgroup>
                <colgroup></colgroup>
                <colgroup style="padding-left: 20px;"></colgroup>
                <colgroup></colgroup>
                <tr><td colspan="4"><uc:CFM id="CFM" runat="server"></uc:CFM></td></tr>
                <tr>
                    <td class="FieldLabel">Name of Broker</td>
                    <td><asp:textbox id="GfeTilCompanyName" runat="server" Width="255px"></asp:textbox></td>
                    <td class="FieldLabel">Broker License #</td>
                    <td><asp:textbox id="GfeTilLicenseNumOfCompany" runat="server"  Width="120px"></asp:textbox></td>
                </tr>
                <tr>
                    <td class="FieldLabel">Broker's Address</td>
                    <td>
                        <asp:textbox id="GfeTilStreetAddr" runat="server" Width="253px"></asp:textbox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:textbox id="GfeTilCity" runat="server"></asp:textbox>
                        <ml:statedropdownlist id="GfeTilState" runat="server"></ml:statedropdownlist>
                        <ml:zipcodetextbox id="GfeTilZip" runat="server" width="50px" preset="zipcode"></ml:zipcodetextbox>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">Broker's Representative</td>
                    <td><asp:textbox id="GfeTilPreparerName" runat="server" Width="255px"></asp:textbox></td>
                    <td class="FieldLabel">License #</td>
                    <td><asp:textbox id="GfeTilLicenseNumOfAgent" runat="server"  Width="120px"></asp:textbox></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
