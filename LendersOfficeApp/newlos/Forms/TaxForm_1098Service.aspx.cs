﻿namespace LendersOfficeApp.newlos.Forms
{
    using System;
    using DataAccess;
    using LendersOffice.Common;

    public class TaxForm_1098ServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(TaxForm_1098ServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.s1098ReportingYear_rep = GetString("s1098ReportingYear");
            dataLoan.s1098PointsCalcT = GetEnum<PointCalculation1098T>("s1098PointsCalcT");

            dataLoan.sTax1098IsCorrected = GetBool("sTax1098IsCorrected");
            IPreparerFields tax1098 = dataLoan.GetPreparerOfForm(E_PreparerFormT.Tax1098, E_ReturnOptionIfNotExist.CreateNew);
            tax1098.CompanyName = GetString("Tax1098CompanyName");
            tax1098.StreetAddr = GetString("Tax1098StreetAddr");
            tax1098.City = GetString("Tax1098City");
            tax1098.State = GetString("Tax1098State");
            tax1098.Zip = GetString("Tax1098Zip");
            tax1098.Phone = GetString("Tax1098Phone");
            tax1098.TaxId = GetString("Tax1098TaxID");
            tax1098.AgentRoleT = (E_AgentRoleT)GetInt("CFM_AgentRoleT");
            tax1098.IsLocked = GetBool("CFM_IsLocked");
            tax1098.Update();

            dataLoan.sTax1098AccountNumLckd = GetBool("sTax1098AccountNumLckd");
            dataLoan.sTax1098AccountNum = GetString("sTax1098AccountNum");
            dataLoan.sTax1098PointPaid_rep = GetString("sTax1098PointPaid");
            dataLoan.sTax1098InterestRefund_rep = GetString("sTax1098InterestRefund");
            // 1/16/2014 gf - opm 106154 MIP is only shown for pre-2014 forms (except 2012).
            if (null != GetString("sTax1098MIP", null))
            {
                dataLoan.sTax1098MIP_rep = GetString("sTax1098MIP");
            }
            dataLoan.sTax1098OtherInfo = GetString("sTax1098OtherInfo");
            // 1/16/2014 gf - opm 106154 this is '4. Other Info' for 2014 only.
            // It will be hidden for earlier versions.
            if (null != GetString("sTax1098OtherInfo2", null))
            {
                dataLoan.sTax1098OtherInfo2 = GetString("sTax1098OtherInfo2");
            }

            if(dataLoan.s1098ReportingYear.EqualsOneOf(2016, 2017, 2018))
            {
                dataLoan.sTax1098OutstandingPrincipalLckd = GetBool("sTax1098OutstandingPrincipalLckd");                
                dataLoan.sTax1098OutstandingPrincipal_rep = GetString("sTax1098OutstandingPrincipal");                
                dataLoan.sTax1098OriginationDateLckd = GetBool("sTax1098OriginationDateLckd");
                dataLoan.sTax1098OriginationDate_rep = GetString("sTax1098OriginationDate");

                dataLoan.sTax1098IsPropertyAddrBorrowerAddrLckd = GetBool("sTax1098IsPropertyAddrBorrowerAddrLckd");
                dataLoan.sTax1098IsPropertyAddrBorrowerAddr = GetBool("sTax1098IsPropertyAddrBorrowerAddr");

                if (!dataLoan.sTax1098IsPropertyAddrBorrowerAddr)
                {
                    dataLoan.sSpAddr = GetString("sSpAddr");
                    dataLoan.sSpCity = GetString("sSpCity");
                    dataLoan.sSpState = GetString("sSpState");
                    dataLoan.sSpZip = GetString("sSpZip");
                    dataLoan.sAssessorsParcelId = GetString("sAssessorsParcelId");
                }
            }

            if (dataLoan.s1098ReportingYear.EqualsOneOf(2017, 2018))
            {
                dataLoan.sTax1098NumberOfPropertiesSecuringThisMortgage_rep = GetString("sTax1098NumberOfPropertiesSecuringThisMortgage");
            }

            dataLoan.sTax1098PayerNameLckd = GetBool("sTax1098PayerNameLckd");
            dataLoan.sTax1098PayerName = GetString("sTax1098PayerName");
            dataLoan.sTax1098PayerAddrLckd = GetBool("sTax1098PayerAddrLckd");            
            dataLoan.sTax1098PayerAddr = GetString("sTax1098PayerAddr");
            dataLoan.sTax1098PayerCity = GetString("sTax1098PayerCity");
            dataLoan.sTax1098PayerState = GetString("sTax1098PayerState");
            dataLoan.sTax1098PayerZip = GetString("sTax1098PayerZip");
            dataLoan.sTax1098PayerSsnLckd = GetBool("sTax1098PayerSsnLckd");            
            dataLoan.sTax1098PayerSsn = GetString("sTax1098PayerSsn");
            dataLoan.sTax1098InterestLckd = GetBool("sTax1098InterestLckd");
            dataLoan.sTax1098Interest_rep = GetString("sTax1098Interest");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("s1098ReportingYear", dataLoan.s1098ReportingYear_rep);
            SetResult("s1098PointsCalcT", dataLoan.s1098PointsCalcT);

            SetResult("sTax1098PayerNameLckd", dataLoan.sTax1098PayerNameLckd);
            SetResult("sTax1098PayerAddrLckd", dataLoan.sTax1098PayerAddrLckd);
            SetResult("sTax1098PayerSsnLckd", dataLoan.sTax1098PayerSsnLckd);
            SetResult("sTax1098InterestLckd", dataLoan.sTax1098InterestLckd);
            SetResult("sTax1098AccountNumLckd", dataLoan.sTax1098AccountNumLckd);

            SetResult("sTax1098IsCorrected", dataLoan.sTax1098IsCorrected);
            IPreparerFields tax1098 = dataLoan.GetPreparerOfForm(E_PreparerFormT.Tax1098, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("Tax1098CompanyName", tax1098.CompanyName);
            SetResult("Tax1098StreetAddr", tax1098.StreetAddr);
            SetResult("Tax1098City", tax1098.City);
            SetResult("Tax1098State", tax1098.State);
            SetResult("Tax1098Zip", tax1098.Zip);
            SetResult("Tax1098Phone", tax1098.Phone);
            SetResult("Tax1098TaxID", tax1098.TaxId);

            SetResult("sTax1098PayerName", dataLoan.sTax1098PayerName);
            SetResult("sTax1098PayerAddr", dataLoan.sTax1098PayerAddr);
            SetResult("sTax1098PayerCity", dataLoan.sTax1098PayerCity);
            SetResult("sTax1098PayerState", dataLoan.sTax1098PayerState);
            SetResult("sTax1098PayerZip", dataLoan.sTax1098PayerZip);
            SetResult("sTax1098PayerSsn", dataLoan.sTax1098PayerSsn);

            SetResult("sTax1098AccountNum", dataLoan.sTax1098AccountNum);
            SetResult("sTax1098Interest", dataLoan.sTax1098Interest_rep);
            SetResult("sTax1098PointPaid", dataLoan.sTax1098PointPaid_rep);
            SetResult("sTax1098InterestRefund", dataLoan.sTax1098InterestRefund_rep);
            SetResult("sTax1098MIP", dataLoan.sTax1098MIP_rep);
            SetResult("sTax1098OtherInfo", dataLoan.sTax1098OtherInfo);
            SetResult("sTax1098OtherInfo2", dataLoan.sTax1098OtherInfo2);

            if (dataLoan.s1098ReportingYear.EqualsOneOf(2016, 2017, 2018))
            {
                SetResult("sTax1098OutstandingPrincipalLckd", dataLoan.sTax1098OutstandingPrincipalLckd);
                SetResult("sTax1098OutstandingPrincipal",dataLoan.sTax1098OutstandingPrincipal_rep);
                SetResult("sTax1098OriginationDateLckd",dataLoan.sTax1098OriginationDateLckd);
                SetResult("sTax1098OriginationDate", dataLoan.sTax1098OriginationDate_rep);
                SetResult("sTax1098IsPropertyAddrBorrowerAddrLckd", dataLoan.sTax1098IsPropertyAddrBorrowerAddrLckd);
                SetResult("sTax1098IsPropertyAddrBorrowerAddr", dataLoan.sTax1098IsPropertyAddrBorrowerAddr);

                SetResult("sSpAddr", dataLoan.sSpAddr);
                SetResult("sSpCity", dataLoan.sSpCity);
                SetResult("sSpState", dataLoan.sSpState);
                SetResult("sSpZip", dataLoan.sSpZip);
                SetResult("sAssessorsParcelId", dataLoan.sAssessorsParcelId);
            }

            if(dataLoan.s1098ReportingYear.EqualsOneOf(2017, 2018))
            {
                SetResult("sTax1098NumberOfPropertiesSecuringThisMortgage", dataLoan.sTax1098NumberOfPropertiesSecuringThisMortgage_rep);
            }
        }
    }
    public partial class TaxForm_1098Service : BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new TaxForm_1098ServiceItem());
        }
    }


}
