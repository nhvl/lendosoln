<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Loan1003pg2.ascx.cs" Inherits="LendersOfficeApp.newlos.Forms.Loan1003pg2" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<asp:HiddenField runat="server" ID="aBNm" />
<asp:HiddenField runat="server" ID="aCNm" />
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import namespace="DataAccess" %>
<script type="text/javascript">
function _init() {
  var b = <%= AspxTools.JsGetElementById(aNetRentI1003Lckd) %>.checked;
	<%= AspxTools.JsGetElementById(aBNetRentI1003) %>.readOnly = !b;
	<%= AspxTools.JsGetElementById(aCNetRentI1003) %>.readOnly = !b;
	<%= AspxTools.JsGetElementById(sProOHExp) %>.readOnly = !<%= AspxTools.JsGetElementById(sProOHExpLckd) %>.checked;  
	bindOtherIncomeTable();
}

function f_incomeCalc(bField, cField, desc) {
  var modalArg = new Object();
  modalArg.aBBaseI = document.getElementById(bField).value;
  modalArg.aCBaseI = document.getElementById(cField).value;
  modalArg.aBName  = document.getElementById('<%=AspxTools.ClientId(aBNm)%>').value;
  modalArg.aCName  = document.getElementById('<%=AspxTools.ClientId(aCNm)%>').value;
  modalArg.aDescription = 'Monthly Income: ' + desc;
  
  showModal('/newlos/Forms/IncomeCalculator.aspx', modalArg, null, null, function(wargs){ 
	if (wargs.OK)
	{
		if (wargs.aBBaseI != null)
		{
		document.getElementById(bField).value = wargs.aBBaseI;
		document.getElementById(bField).focus();
		}
		if (wargs.aCBaseI != null)
		{
		document.getElementById(cField).value = wargs.aCBaseI;
		document.getElementById(cField).focus();
		}
		if (typeof(refreshCalculation) == 'function')
		refreshCalculation();
		updateDirtyBit();
	}
  },{ hideCloseButton: true });
  return false;
}
function orderCreditReport()
{
    return linkMe(ML.VirtualRoot + '/newlos/Services/OrderCredit.aspx?islead=' + <%=AspxTools.JsString(RequestHelper.GetQueryString("islead"))%>);
}
function displayCreditReport()
{
    var report_url = gVirtualRoot + "/newlos/services/ViewCreditFrame.aspx?loanid=" + ML.sLId + "&applicationid=" + parent.info.f_getCurrentApplicationID() + "&islead=" + <%=AspxTools.JsString(RequestHelper.GetQueryString("islead"))%>;
    var win = openWindowWithArguments(report_url, "report", "toolbar=no,menubar=no,location=no,status=no,resizable=yes") ;
    if (null != win) win.focus() ;
	return false;
}

function checkForSubsequentBlanks()
{
    // 8/20/2013 EM - If there are only 3 rows, and a row has all subsequent rows as blanks, disable the delete
    var jsonStr = document.getElementById('<%=AspxTools.ClientId(OtherIncomeJson)%>');
    var data = $.parseJSON( jsonStr.value);
    
    if(data.length == 3)
    {
        //work backwards from the last index
        for(var i = 2; i >= 0; i--)
        {
            if(data[i].Desc == "" && (data[i].Amount == "" || data[i].Amount == "0.00"))
            {
                document.getElementById("delete_"+i).removeAttribute("href");
                document.getElementById("delete_"+i).disabled = true;
                jQuery("#delete_"+i).toggleClass("disabled",true);
            }
            else
                break;
        }
    }
}


function bindOtherIncomeTable()
{
    var isReadOnly = <%=AspxTools.JsBool(IsReadOnly) %> || ML.sIsIncomeCollectionEnabled;
    var disabledStr= isReadOnly ? "disabled='disabled'" : "";
    var readOnlyStr = isReadOnly ? "readonly='readonly'" : "";
   
    var data = $.parseJSON( $('#<%=AspxTools.ClientId(OtherIncomeJson)%>').val());
    var h = hypescriptDom;
    
	$("#OtherIncomeContent").empty().append($.map(data, function(item, i) {
		return (h("tr", {className:"IncomeRow"}, [
			h("td", {}, h("select", {id:("s_"+i), onchange:onChangeOtherIncome, disabled:isReadOnly}, [
				h("option", {value:"B", selected:!item.IsForCoBorrower}, "B"),
				h("option", {value:"C", selected: item.IsForCoBorrower}, "C")
			])),
			h("td", {}, [
				h("input", {type:"text", id:("d_"+i), value:item.Desc, style:"width:430px", maxLength:100, 
							onkeydown:function(event){onComboboxKeypress(this, event)}, readOnly:isReadOnly}),
				h("img", {id:("i_"+i), src:(VRoot+"/images/dropdown_arrow.gif"), className:"combobox_img", align:"absbottom"})
			]),
			h("td", {}, h("input", {type:"text", id:("a_"+i), value:item.Amount, attrs:{preset:"money"}, style:"width:90px", onchange:onChangeOtherIncome, readOnly:isReadOnly})),
			h("td", {}, ((!isReadOnly && item.Desc == "" && (item.Amount == "" || item.Amount == "0.00"))
				? h("a", {id:("delete_"+i), href:"javascript:void(0)", onclick:function(){onDeleteClick(i)}}, "delete")
				: h("a", {id:("delete_"+i), attrs:{disabled:"disabled"}, className:"disabled"}, "delete")
				)),
		]))
    }));
    
    $("#OtherIncomeContent").find("select").each( function() { 
    attachCommonEvents(this, false); 

    if ( isReadOnly )
        disableDDL(this, true);
    });
    
    $("#OtherIncomeContent").find("input").each( function() { 
        attachCommonEvents(this, false); 
        
        if( $(this).attr("preset") == "money")
        {
            format_money(this);
            _initMask(this);  
        }
       if ( isReadOnly )
        formatReadonlyField(this); 
    });
    
    $('#<%=AspxTools.ClientId(OtherIncomeCombo)%>_cbl').attr('shared_combo', 'shared_combo');
    $("#OtherIncomeContent").find(".combobox_img").each( function() { 
    
        attachCommonEvents(this, false);
        
        if ( isReadOnly )
            formatReadonlyField(this);
        else
        {
            $(this).on("click", function() { 
                var thisId = $(this).attr('id');
                thisId = thisId.replace('i','d'); 
                $('table[shared_combo]').attr('id', thisId + '_cbl');
                onSelect( thisId );  
            }); 
        }
    });
    checkForSubsequentBlanks()
}

function onDeleteClick(index)
{
    var jsonStr = document.getElementById('<%=AspxTools.ClientId(OtherIncomeJson)%>');
    var data = $.parseJSON( jsonStr.value);
    var dataNew = Array();
    for(var i = 0; i < data.length; i++)
    {
        if(i != index)
            dataNew.push(data[i]);
    }
    jsonStr.value = JSON.stringify(dataNew);
    bindOtherIncomeTable();
    updateDirtyBit();
    //subtract by to ensure you have atleast three since you're deleting an entry as well
    if(data.length - 4 < 0)
        onAddMoreIncomeClick();
    
}

function onAddMoreIncomeClick()
{
    calculateOtherIncomeJson();

    var jsonStr = document.getElementById('<%=AspxTools.ClientId(OtherIncomeJson)%>');
    var data = $.parseJSON( jsonStr.value);
    data.push({"Amount" : 0.00, "Desc":"", "IsForCoBorrower":false});
    jsonStr.value = JSON.stringify(data);
    bindOtherIncomeTable();
    updateDirtyBit();
}

function calculateOtherIncomeJson()
{
    var jsonStr = document.getElementById('<%=AspxTools.ClientId(OtherIncomeJson)%>');
    var data = new Array();

    var count = 0;
    $("#OtherIncomeContent").children("tr").each( function() {
    
    var isForCoborrower = $('#s_' + count).val() == 'C';
    var desc =  $('#d_' + count).val();
    
    format_money( $('#a_' + count)[0] );
    var amt =  $('#a_' + count).val().replace('$','').replace(/,/g,'');
    if (amt == '') amt = '0';
    if (amt.indexOf( "(",0) == 0)
    {
        amt = amt.replace("(","-");
        amt = amt.replace(")","");
    }
    
    data.push({"Amount" : amt, "Desc":desc, "IsForCoBorrower":isForCoborrower});
    
    count++;
    });
    
    jsonStr.value = JSON.stringify(data);
}

function onChangeOtherIncome()
{
    calculateOtherIncomeJson();
    refreshCalculation();
}
</script>
<table id="Table2" cellSpacing="0" cellPadding="0" border="0">
	<tr>
		<td class="LoanFormHeader">V. MONTHLY INCOME AND COMBINED HOUSING EXPENSE 
			INFORMATION</td>
	</tr>
	<tr>
		<td>
			<TABLE class="InsetBorder" id="Table4" cellSpacing="0" cellPadding="0" width="99%" border="0">
				<TR>
					<TD class="FormTableSubHeader" noWrap>Gross Monthly Income</TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD style="WIDTH: 162px" noWrap></TD>
								<TD class="FieldLabel">Borrower</TD>
								<TD class="FieldLabel">Co-borrower</TD>
								<TD class="FieldLabel">Total&nbsp;
								</TD>
							</TR>
							<TR>
								<TD class="FieldLabel income-collection-dependent" style="WIDTH: 162px" noWrap>
                                    <a href="#" ID="BaseIncCalcLink" onclick="return f_incomeCalc( '<%= AspxTools.ClientId(aBBaseI)%>', '<%=AspxTools.ClientId(aCBaseI)%>', 'Base Income' );">Base Income</a>
                                    <label ID="BaseIncLabel">Base Income</label>
                                </TD>
                                <TD style="WIDTH: 104px"><ml:moneytextbox id="aBBaseI" tabIndex="1" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="aCBaseI" tabIndex="9" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="aTotBaseI" tabIndex="17" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel income-collection-dependent" style="WIDTH: 162px" noWrap>
									<a href="#" ID="OvertimeIncCalcLink" onclick="return f_incomeCalc( '<%=AspxTools.ClientId(aBOvertimeI)%>', '<%=AspxTools.ClientId(aCOvertimeI)%>', 'Ovetime' );">Over Time</a>
                                    <label ID="OvertimeIncLabel">Overtime</label>
                                </TD>
								<TD style="WIDTH: 104px"><ml:moneytextbox id="aBOvertimeI" tabIndex="1" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="aCOvertimeI" tabIndex="10" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="aTotOvertimeI" tabIndex="18" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel income-collection-dependent" style="WIDTH: 162px">
									<a href="#" ID="BonusesIncCalcLink" onclick="return f_incomeCalc( '<%=AspxTools.ClientId(aBBonusesI)%>', '<%=AspxTools.ClientId(aCBonusesI)%>', 'Bonuses' );">Bonuses</a>
                                    <label ID="BonusesIncLabel">Bonuses</label>
                                </TD>
                                <TD style="WIDTH: 104px"><ml:moneytextbox id="aBBonusesI" tabIndex="2" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="aCBonusesI" tabIndex="11" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="aTotBonusesI" tabIndex="19" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel income-collection-dependent" style="WIDTH: 162px">
									<a href="#" ID="CommissionIncCalcLink" onclick="return f_incomeCalc( '<%=AspxTools.ClientId(aBCommisionI)%>', '<%=AspxTools.ClientId(aCCommisionI)%>', 'Commissions' );">Commissions</a>
                                    <label ID="CommissionIncLabel">Commission</label>
                                </TD>
								<TD style="WIDTH: 104px"><ml:moneytextbox id="aBCommisionI" tabIndex="3" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="aCCommisionI" tabIndex="12" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="aTotCommisionI" tabIndex="20" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel income-collection-dependent" style="WIDTH: 162px">
									<a href="#" ID="DividendIncCalcLink" onclick="return f_incomeCalc( '<%=AspxTools.ClientId(aBDividendI)%>', '<%=AspxTools.ClientId(aCDividendI)%>', 'Dividends/Interest' );">Dividends/Interest</a>
                                    <label ID="DividendIncLabel">Dividends/Interest</label>
                                </TD>
								<TD style="WIDTH: 104px"><ml:moneytextbox id="aBDividendI" tabIndex="4" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="aCDividendI" tabIndex="13" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="aTotDividendI" tabIndex="21" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" style="WIDTH: 162px" noWrap><A tabIndex="5" onclick="redirectToUladPage('REO');" title="Go to Real Estate Schedule">Net Rental Income:</A>
									<asp:checkbox id="aNetRentI1003Lckd" onclick="refreshCalculation();" tabIndex="5" runat="server" Text="lock"></asp:checkbox></TD>
								<TD style="WIDTH: 104px"><ml:moneytextbox id="aBNetRentI1003" tabIndex="6" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="aCNetRentI1003" tabIndex="14" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="aTotNetRentI1003" tabIndex="22" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
							<tr>
				                <td class="FieldLabel" noWrap>Subject Net Cash</td>
				                <TD noWrap><ml:moneytextbox id="aBSpPosCf" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
					            <TD noWrap><ml:moneytextbox id="aCSpPosCf" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
					            <TD noWrap><ml:moneytextbox id="aTotSpPosCf" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
				            </tr>
							<TR title="(see 'Describe Other Income' below)">
								<TD class="FieldLabel" style="WIDTH: 162px">Other Income:</TD>
								<TD style="WIDTH: 104px"><ml:moneytextbox id="aBTotOI" tabIndex="7" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="aCTotOI" tabIndex="15" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="aTotOI" tabIndex="23" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" style="WIDTH: 162px">Total:</TD>
								<TD style="WIDTH: 104px"><ml:moneytextbox id="aBTotI" tabIndex="8" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="aCTotI" tabIndex="16" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="aTotI" tabIndex="24" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</td>
	</tr>
	<tr>
		<td>
            <input type="hidden" id="OtherIncomeJson" value="" runat ="server" />
			<TABLE class="InsetBorder" id="Table12" cellSpacing="0" cellPadding="0" width="99%" border="0">
				<TR>
					<TD class="FormTableSubHeader" noWrap>Describe Other Income</TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE id="Table13" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FieldLabel">B/C</TD>
								<TD class="FieldLabel">Description</TD>
								<TD class="FieldLabel" noWrap>Monthly Income</TD>
							</TR>
							<tbody id="OtherIncomeContent">
							</tbody>
							<tr><td colspan='3'><input runat="server" id="AddMoreOtherInc" type=button onclick="onAddMoreIncomeClick()" value="Add More" tabindex='28' /></td></tr>
						</TABLE>
						<span style="display:none" ><ml:ComboBox runat="server" id="OtherIncomeCombo" style="display:none" /></span>
					</TD>
				</TR>
			</TABLE>

		</td>
	</tr>
	<tr>
		<td>
			<TABLE class="InsetBorder" id="Table8" cellSpacing="0" cellPadding="0" width="99%" border="0">
				<TR>
					<TD class="FormTableSubheader">Combined Monthly Housing Expense</TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table3" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FormTableSubHeader"></TD>
								<TD class="FormTableSubHeader" vAlign="bottom">Present</TD>
								<TD class="FormTableSubHeader" vAlign="bottom" noWrap colSpan="5">
									<P align="right">Proposed</P>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD></TD>
								<TD class="FieldLabel">Base type</TD>
								<TD class="FieldLabel">Base amount</TD>
								<TD class="FieldLabel" noWrap>Rate</TD>
								<TD class="FieldLabel" noWrap>Mon min. base</TD>
								<TD class="FieldLabel" align="right">Amount</TD>
							</TR>
							<TR>
								<TD class="FieldLabel">Rent</TD>
								<TD><ml:moneytextbox id="aPresRent" tabIndex="34" preset="money" width="71" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD></TD>
								<TD></TD>
								<TD style="WIDTH: 56px" noWrap></TD>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1st Mort (P&amp;I)</TD>
								<TD><ml:moneytextbox id="aPres1stM" tabIndex="35" preset="money" width="71px" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD></TD>
								<TD></TD>
								<TD style="WIDTH: 56px"></TD>
								<TD></TD>
								<TD><ml:moneytextbox id="sProFirstMPmt" tabIndex="44" preset="money" width="81px" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">Other Fin (P&amp;I)</TD>
								<TD><ml:moneytextbox id="aPresOFin" tabIndex="36" preset="money" width="71px" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD></TD>
								<TD></TD>
								<TD style="WIDTH: 56px"></TD>
								<TD class="FieldLabel" align="right"> <asp:HyperLink id="subfinancingLink" Runat="server" Text="Subfinancing" /></TD>
								<TD><ml:moneytextbox id="sProSecondMPmt" tabIndex="46" preset="money" width="81px" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" style="HEIGHT: 16px">Haz Ins / Notes</TD>
								<TD style="HEIGHT: 16px">
									<asp:TextBox id="aPresHazIns" runat="server" Width="71px" style="TEXT-ALIGN:right" onchange="refreshCalculation();"  tabIndex="37"></asp:TextBox>
								</TD>
								<TD style="HEIGHT: 16px">
                                    <asp:PlaceHolder runat="server" ID="sProHazInsTHolder">
                                        <asp:dropdownlist id="sProHazInsT" tabIndex="47" runat="server" onchange="refreshCalculation();" Height="25px"></asp:dropdownlist>
                                    </asp:PlaceHolder>
								</TD>
								<TD style="HEIGHT: 16px">
                                    <asp:PlaceHolder runat="server" ID="sProHazInsBaseAmtHolder">
                                        <ml:moneytextbox id="sProHazInsBaseAmt" tabIndex="48" preset="money" width="80px" runat="server" ReadOnly="True"></ml:moneytextbox>
                                    </asp:PlaceHolder>
								</TD>
								<TD style="HEIGHT: 16px" noWrap>
                                    <asp:PlaceHolder runat="server" ID="sProHazInsRHolder">
                                        <ml:percenttextbox id="sProHazInsR" tabIndex="49" preset="percent" width="60px" runat="server" onchange="refreshCalculation();"></ml:percenttextbox>
                                    </asp:PlaceHolder>
								</TD>
								<TD style="HEIGHT: 16px" noWrap>
                                    <asp:PlaceHolder runat="server" ID="sProHazInsMbHolder">
                                        + <ml:moneytextbox id="sProHazInsMb" tabIndex="50" preset="money" width="81px" runat="server" onchange="refreshCalculation();" decimalDigits="4"></ml:moneytextbox>
                                    </asp:PlaceHolder>
								</TD>
								<TD style="HEIGHT: 16px"><ml:moneytextbox id="sProHazIns" tabIndex="51" preset="money" width="81px" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">RE Tax / Notes</TD>
								<TD>
									<asp:TextBox id="aPresRealETx" runat="server" Width="71px" style="TEXT-ALIGN:right" onchange="refreshCalculation();"  tabIndex="38"></asp:TextBox>
								</TD>
								<TD>
                                    <asp:PlaceHolder runat="server" ID="sProRealETxTHolder">
                                        <asp:dropdownlist id="sProRealETxT" tabIndex="52" runat="server" onchange="refreshCalculation();" Height="25px"></asp:dropdownlist>
                                    </asp:PlaceHolder> 
								</TD>
								<TD>
                                    <asp:PlaceHolder runat="server" ID="sProRealETxBaseAmtPlaceHolder">
                                        <ml:moneytextbox id="sProRealETxBaseAmt" tabIndex="53" preset="money" width="80px" runat="server" ReadOnly="True"></ml:moneytextbox>
                                    </asp:PlaceHolder> 
								</TD>
								<TD>
                                    <asp:PlaceHolder runat="server" ID="sProRealETxRHolder">
                                        <ml:percenttextbox id="sProRealETxR" tabIndex="54" preset="percent" width="60px" runat="server" onchange="refreshCalculation();"></ml:percenttextbox>
                                    </asp:PlaceHolder> 
								</TD>
								<TD nowrap>
                                    <asp:PlaceHolder runat="server" ID="sProRealETxMbHolder">
                                        + <ml:moneytextbox id="sProRealETxMb" tabIndex="55" preset="money" width="81px" runat="server" onchange="refreshCalculation();"></ml:moneytextbox>
                                    </asp:PlaceHolder> 
								</TD>
								<TD>
                                    <ml:moneytextbox id="sProRealETx" tabIndex="56" preset="money" width="81px" runat="server" ReadOnly="True"></ml:moneytextbox>
								</TD>
							</TR>
							<TR>
								<TD class="FieldLabel"><A href="javascript:linkMe('../LoanInfo.aspx?pg=1');" >Mort Ins</A> / Notes</TD>
								<TD colspan="5">
									<asp:TextBox id="aPresMIns" runat="server" Width="71px" style="TEXT-ALIGN:right" onchange="refreshCalculation();" tabIndex="39"></asp:TextBox></TD>
								<TD><ml:moneytextbox id="sProMIns" tabIndex="61" preset="money" width="81px" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">HOA</TD>
								<TD><ml:moneytextbox id="aPresHoAssocDues" tabIndex="40" preset="money" width="71px" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD></TD>
								<TD></TD>
								<TD style="WIDTH: 56px"></TD>
								<TD></TD>
								<TD><ml:moneytextbox id="sProHoAssocDues" tabIndex="62" preset="money" width="81px" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="sProOHExpDesc" tabIndex="41" runat="server" Width="94px"></asp:textbox></TD>
								<TD><ml:moneytextbox id="aPresOHExp" tabIndex="42" preset="money" width="71px" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD></TD>
								<TD></TD>
								<TD style="WIDTH: 56px"></TD>
								<TD align="right" class="FieldLabel"><asp:CheckBox ID="sProOHExpLckd" runat="server" TabIndex="63" onclick="refreshCalculation();" text="Lock"/></TD>
								<TD><ml:moneytextbox id="sProOHExp" tabIndex="63" preset="money" width="81px" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">Total</TD>
								<TD><ml:moneytextbox id="aPresTotHExp" tabIndex="43" preset="money" width="71px" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
								<TD></TD>
								<TD></TD>
								<TD style="WIDTH: 56px"></TD>
								<TD></TD>
								<TD><ml:moneytextbox id="sMonthlyPmt" tabIndex="64" preset="money" width="81px" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</td>
	</tr>
	<tr>
		<td class="LoanFormHeader">VI. ASSETS AND LIABILITIES</td>
	</tr>
	<tr>
		<td class="FieldLabel">Completed&nbsp;&nbsp;&nbsp;&nbsp;
			<asp:radiobuttonlist id="aAsstLiaCompletedNotJointly" tabIndex="65" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
				<asp:ListItem Value="0">Jointly</asp:ListItem>
				<asp:ListItem Value="1">Not Jointly</asp:ListItem>
			</asp:radiobuttonlist>
			&nbsp;&nbsp;
		    <INPUT type="button" value="Order Credit..." onclick="orderCreditReport();" AlwaysEnable=true style="WIDTH: 120px; font-weight:bold;margin-top:5px;margin-bottom:5px;background-color:lightyellow;border:2px outset lightyellow;" size="20">&nbsp;&nbsp;<input type="button" value="View Credit" onclick="displayCreditReport();" AlwaysEnable=true style="WIDTH: 93px; font-weight:bold;margin-top:5px;margin-bottom:5px">
		</td>
	</tr>
	<TR>
		<TD>
			<TABLE id="Table9" cellSpacing="0" cellPadding="0" border="0">
				<TR>
					<TD vAlign="top" noWrap>
						<TABLE class="InsetBorder" id="Table10" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FormTableSubheader" noWrap colSpan="2">Assets</TD>
							</TR>
              <tr>
                <td class=FieldLabel nowrap align=right><a title="Go to Assets" tabindex="68" onclick="redirectToUladPage('Assets');"> Non-Real Estate Assets</a></td>
                <td nowrap></td></tr>
							<TR>
								<TD class="FieldLabel" noWrap align=right> Liquid</TD>
								<TD noWrap><ml:moneytextbox id="aAsstLiqTot" tabIndex="66" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap align=right> Non-liquid</TD>
								<TD noWrap><ml:moneytextbox id="aAsstNonReSolidTot" tabIndex="67" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap align=right><A tabIndex="68" onclick="redirectToUladPage('REO');">Real 
										Estate Owned</A></TD>
								<TD noWrap><ml:moneytextbox id="aReTotVal" tabIndex="69" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap align=right>(a) Total Assets</TD>
								<TD noWrap><ml:moneytextbox id="aAsstValTot" tabIndex="71" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
						</TABLE>
					</TD>
					<TD vAlign="top" noWrap>
						<TABLE class="InsetBorder" id="Table11" cellSpacing="0" cellPadding="0" border="0" width="98%">
							<TR>
								<TD class="FormTableSubheader" noWrap colSpan="2">Liabilities</TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>Monthly Payments</TD>
								<TD noWrap><ml:moneytextbox id="aLiaMonTot" tabIndex="72" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>(b) 
										Total <a tabindex="73" onclick="redirectToUladPage('Liabilities');">Liabilities</a></TD>
								<TD noWrap><ml:moneytextbox id="aLiaBalTot" tabIndex="74" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></TD>
							</TR>
						</TABLE>
            <table class=InsetBorder id=Table7 cellspacing=0 cellpadding=0 
            width="98%" border=0>
              <tr>
                <td nowrap class="FieldLabel">Net Worth (a - b)</td>
                <td class=FieldLabel nowrap align=right>    <ml:moneytextbox id="aNetWorth" tabIndex="75" preset="money" width="90" runat="server" ReadOnly="True"></ml:moneytextbox></td></tr>
              <tr>
                <td nowrap></td>
                <td nowrap></td></tr></table>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</table>
