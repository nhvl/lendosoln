<%@ Page language="c#" Codebehind="MortgageLoanCommitment.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.MortgageLoanCommitment" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>Mortgage Loan Commitment</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%=AspxTools.SafeUrl(StyleSheet)%> type="text/css" rel="Stylesheet">
  </head>
  <body MS_POSITIONING="FlowLayout" class="RightBackground">
	<script type="text/javascript">
  <!--
  var oRolodex = null;  
  function _init() {
    if (null == oRolodex)
      oRolodex = new cRolodex();
  }

  //-->
  </script>

    <form id="MortgageLoanCommitment_v2" method="post" runat="server">
<table id="Table1" cellspacing=0 cellpadding=0 border=0>
  <tr>
    <td nowrap class="MainRightHeader">Mortgage Loan Commitment</td></tr>
  <tr>
    <td nowrap>
      <table class="InsetBorder" id="Table2" cellspacing=0 cellpadding=0 width="98%" border=0>
        <tr>
          <td nowrap>
            <table id="Table3" cellspacing=0 cellpadding=0 border=0>
              <tr>
                <td class="FieldLabel" nowrap></td>
                <td nowrap>
					<uc:CFM id="CFM" runat="server" Type="21" CompanyNameField="MortgageLoanCommitmentCompanyName" StreetAddressField="MortgageLoanCommitmentStreetAddr" CityField="MortgageLoanCommitmentCity" StateField="MortgageLoanCommitmentState" ZipField="MortgageLoanCommitmentZip"></uc:CFM>
				</td></tr>
              <tr>
                <td nowrap class="FieldLabel">Lender Name</td>
                <td nowrap><asp:TextBox id="MortgageLoanCommitmentCompanyName" runat="server" Width="253px"></asp:TextBox></td></tr>
              <tr>
                <td nowrap class="FieldLabel">Address</td>
                <td nowrap><asp:TextBox id="MortgageLoanCommitmentStreetAddr" runat="server" Width="254px"></asp:TextBox></td></tr>
              <tr>
                <td nowrap></td>
                <td nowrap><asp:TextBox id="MortgageLoanCommitmentCity" runat="server"></asp:TextBox><ml:StateDropDownList id=MortgageLoanCommitmentState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=MortgageLoanCommitmentZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
              <tr>
                <td nowrap class="FieldLabel">Prepared Date</td>
                <td nowrap><ml:DateTextBox id="MortgageLoanCommitmentPrepareDate" runat="server" width="75" preset="date"></ml:DateTextBox></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td nowrap>
      <table id="Table7" cellspacing=0 cellpadding=0 width="98%" border=0 class=InsetBorder>
        <tr>
          <td class="FieldLabel">Loan Amount, Terms and Fees</td></tr>
        <tr>
          <td>
            <table id="Table8" cellspacing=0 cellpadding=0 border=0>
              <tr>
                <td class="FieldLabel" nowrap>Loan Amt</td>
                <td nowrap><ml:MoneyTextBox id="sLAmtCalc" runat="server" preset="money" width="90" ReadOnly="True" /></td>
                <td class="FieldLabel" nowrap>Contract Interest Rate</td>
                <td nowrap><ml:PercentTextBox id="sNoteIR" runat="server" preset="percent" width="70"></ml:PercentTextBox></td>
                <td class="FieldLabel" nowrap>LTV</td>
                <td nowrap><ml:PercentTextBox id="sLtvR" runat="server" preset="percent" width="70" ReadOnly="True"></ml:PercentTextBox></td></tr>
              <tr>
                <td class="FieldLabel" nowrap>Terms / Due </td>
                <td nowrap><asp:TextBox id="sTerm" runat="server" Width="38px"></asp:TextBox>&nbsp;/ 
<asp:TextBox id="sDue" runat="server" Width="38px"></asp:TextBox></td>
                <td class="FieldLabel" nowrap>Commitment Expires</td>
                <td nowrap><ml:DateTextBox id="sCommitExpD" runat="server" preset="date" width="75"></ml:DateTextBox></td>
                <td class="FieldLabel" nowrap>CLTV</td>
                <td nowrap><ml:PercentTextBox id="sCltvR" runat="server" preset="percent" width="70" ReadOnly="True"></ml:PercentTextBox></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td nowrap>
      <table id="Table4" cellspacing=0 cellpadding=0 width="98%" border=0 class="InsetBorder">
        <tr>
          <td nowrap class="FieldLabel">Repayment Terms</td></tr>
        <tr>
          <td nowrap><asp:TextBox id="sCommitRepayTermsDesc" runat="server" Width="379px" TextMode="MultiLine" Height="106px"></asp:TextBox></td></tr></table></td></tr>
  <tr>
    <td nowrap class="FieldLabel">Evidence of Title <asp:TextBox id="sCommitTitleEvidence" runat="server" Width="333px"></asp:TextBox></td></tr>
  <tr>
    <td nowrap></td></tr>
  <tr>
    <td nowrap>
      <table id="Table5" cellspacing=0 cellpadding=0 width="98%" border=0 class="InsetBorder">
        <tr>
          <td nowrap>
            <table id="Table6" cellspacing=0 cellpadding=0 border=0>
              <tr>
                <td nowrap colspan=2 class="FieldLabel">Return to lender at: <asp:CheckBox id="sCommitReturnToAboveAddr" runat="server" Text="above address"></asp:CheckBox>&nbsp;<asp:CheckBox id=sCommitReturnToFollowAddr runat="server" Text="following address"></asp:CheckBox>&nbsp;within <asp:TextBox id=sCommitReturnWithinDays runat="server" Width="51px"></asp:TextBox>&nbsp;days 
                </td></tr>
              <tr>
                <td nowrap class="FieldLabel">Lender</td>
                <td nowrap><asp:TextBox id="MortgageLoanCommitmentAlternateLenderCompanyName" runat="server" Width="253px"></asp:TextBox></td></tr>
              <tr>
                <td nowrap class="FieldLabel">Address</td>
                <td nowrap><asp:TextBox id="MortgageLoanCommitmentAlternateLenderStreetAddr" runat="server" Width="253px"></asp:TextBox></td></tr>
              <tr>
                <td nowrap></td>
                <td nowrap><asp:TextBox id="MortgageLoanCommitmentAlternateLenderCity" runat="server"></asp:TextBox><ml:StateDropDownList id=MortgageLoanCommitmentAlternateLenderState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=MortgageLoanCommitmentAlternateLenderZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td>
      <table id="Table9" cellspacing=0 cellpadding=0 border=0 class="InsetBorder">
          <tr>
            <td colspan="4" class="FieldLabel">
                <div style="width=22%; margin-top: 3px; float:left">Mortgage Product</div>
                <div style="width=78%; float:left">
                    <asp:TextBox id="sLpTemplateNm" width="100%" runat="server" ReadOnly="True" />
                </div>
            </td>
          </tr>
          <tr>
            <td class="FieldLabel">Discount</td>
            <td nowrap>
                <asp:TextBox id="sLDiscnt1003" runat="server" width="90" ReadOnly="True" />
            </td>
            <td class="FieldLabel">Rate Lock Expiration Date</td>
            <td nowrap>
                <asp:TextBox id="sRLckdExpiredD" runat="server" width="90" ReadOnly="True" />
            </td>
          </tr>
          <tr>
            <td class="FieldLabel">Principal and Interest Payment</td>
            <td>
                <asp:TextBox id="sProThisMPmt" runat="server" width="90" ReadOnly="True" />
            </td>
            <td class="FieldLabel">Max Qualifying Rate</td>
            <td nowrap>
                <ml:PercentTextBox id="sMaxR" runat="server" width="90" ReadOnly="True" />
            </td>
          </tr>
          <tr>
            <td colspan="4">
                <asp:CheckBox id="sMldsHasImpound" runat="server" Text="Escrows waived"></asp:CheckBox>
            </td>
          </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
		<table id=Table4 cellspacing=0 cellpadding=0 border=0 width="98%" class=InsetBorder>
		    <tr>
		        <td class="FieldLabel">
		            <a runat="server" ID="GotoConditionsLink">
		                Additional Required Items or Conditions
		            </a>
		        </td>
		    </tr>
	    </table>
    </td></tr>
  <tr>
    <td nowrap></td></tr>
  <tr>
    <td nowrap></td></tr></table><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>

     </form>
	
  </body>
</html>
