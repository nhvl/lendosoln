<%@ Import Namespace="DataAccess" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<%@ Page Language="c#" CodeBehind="Transmittal_04_Combined.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.Transmittal_04_Combined" %>

<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Transmittal</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link type="text/css" href="Transmittal_04.css" />
    <link href=<%=AspxTools.SafeUrl(StyleSheet)%> type="text/css" rel="stylesheet">
</head>
<body bgcolor="gainsboro" ms_positioning="FlowLayout">
    <script language="javascript" type="text/javascript">
<!--
        var oRolodex = null;
        function _init() {
            if (null == oRolodex)
            {
                oRolodex = new cRolodex();
            }
      
            enableBorrowerInfo();

            var inputs = <%= AspxTools.JsGetElementById(sBuydown) %>.getElementsByTagName("INPUT");
            var bBuyDown = false;
            for (var i = 0; i < inputs.length; i++)
            {
                if (inputs[i].value == "1" && inputs[i].checked == true)
                {
                    bBuyDown = true;
                    break;
                }
            }
      
            <%= AspxTools.JsGetElementById(sTransmBuydwnTermDesc) %>.readOnly = !bBuyDown;

            <%= AspxTools.JsGetElementById(sPurchPrice) %>.readOnly = <%= AspxTools.JsGetElementById(sLPurposeT) %>.value != <%=AspxTools.JsString(E_sLPurposeT.Purchase)%>;
            lockField(document.getElementById("<%= AspxTools.ClientId(sHcltvRLckd) %>"), 'sHcltvR');
            lockField(document.getElementById("<%= AspxTools.ClientId(sDebtToHousingGapRLckd) %>"), 'sDebtToHousingGapR');
            lockField(document.getElementById("<%= AspxTools.ClientId(sTransmFntcLckd) %>"), 'sTransmFntc');
            lockField(document.getElementById("<%= AspxTools.ClientId(sLenLNumLckd) %>"), 'sLenLNum');
            lockField(<%= AspxTools.JsGetElementById(sInvestLNumLckd) %>, 'sInvestLNum');
    
            var isFirstLien = <%= AspxTools.JsGetElementById(sLienPosT) %>.value == <%=AspxTools.JsString(E_sLienPosT.First)%>;
  
            <%= AspxTools.JsGetElementById(sSubFin) %>.readOnly = !isFirstLien;
            disableDDL(document.getElementById("<%= AspxTools.ClientId(s1stMOwnerT) %>"), isFirstLien);
            <%= AspxTools.JsGetElementById(s1stMtgOrigLAmt) %>.readOnly = isFirstLien;
            on_sFinMethodPrintAsOtherClick();  
            resetAppraisalTypeCheckboxes();

            SetQualTermReadonly();
        }

        function SetQualTermReadonly() {
            var isManual = $(".sQualTermCalculationType").val() == '<%=AspxTools.JsStringUnquoted(QualTermCalculationType.Manual)%>';
            $(".sQualTerm").prop("readonly", !isManual);
        }
  
        function _postSaveMe() 
        {
            updateOccupancyStatusSavedValue();
            onChangeOccupancyStatus();
        }

        //updates the hidden field with the saved value of the occupancy status. 
        function updateOccupancyStatusSavedValue() 
        {
            var dropdown = <%= AspxTools.JsGetElementById(aOccT) %>;
            var selectedText = dropdown.options[dropdown.selectedIndex].text.toLowerCase();
            if (!parent.body.isDirty())
            {
                document.getElementById('savedOccupency').value = selectedText;
            }
        }
  
        // 07/18/07 av disables/enables the anchor around "Borrower's Primary Residence" 
        // when the user changes the occupancy status. 
        function onChangeOccupancyStatus() {
            var initialText	= document.getElementById('savedOccupency').value;
            var dropdown = <%= AspxTools.JsGetElementById(aOccT) %>;
            var selectedText = dropdown.options[dropdown.selectedIndex].text.toLowerCase(); 

            if (initialText == selectedText ||
                (initialText == "secondary residence" && selectedText == "investment") ||
                (initialText == "investment" && selectedText == "secondary residence"))
            {
                document.getElementById("BPRAnchor").setAttribute('href', 'javascript:hrefBorrowPrimaryResidence()');		 
                document.getElementById("BPRAnchor").style.color  = 'blue'; 
            } 
            else 
            {
                document.getElementById("BPRAnchor").removeAttribute('href'); 
                document.getElementById("BPRAnchor").style.color  = 'black'; 
            }	
        }
  
        //called when the user clicks on the Borrower's Primary Residence Anchor tag
        function hrefBorrowPrimaryResidence() 
        {
            var dropdown = <%= AspxTools.JsGetElementById(aOccT) %>;

            if (dropdown.options[dropdown.selectedIndex].text.toLowerCase() == "primary residence") 
            {
                linkMe('../LoanInfo.aspx');
            } 
            else 
            {
                linkMe('../BorrowerInfo.aspx?pg=7');
            }
        }
  
        function on_sFinMethodPrintAsOtherClick() {
          if (<%= AspxTools.JsGetElementById(sFinMethodPrintAsOther) %>.checked) {
              <%= AspxTools.JsGetElementById(sFinMethPrintAsOtherDesc) %>.readOnly = false;
          } else {
              <%= AspxTools.JsGetElementById(sFinMethPrintAsOtherDesc) %>.readOnly = true;
              <%= AspxTools.JsGetElementById(sFinMethPrintAsOtherDesc) %>.value = "";
          }
        }

        function enableBorrowerInfo() {
  
            var b = <%= AspxTools.JsGetElementById(sCombinedBorInfoLckd) %>.checked;
            <%= AspxTools.JsGetElementById(sCombinedBorFirstNm) %>.readOnly = !b;
            <%= AspxTools.JsGetElementById(sCombinedBorMidNm) %>.readOnly = !b;
            <%= AspxTools.JsGetElementById(sCombinedBorLastNm) %>.readOnly = !b;
            <%= AspxTools.JsGetElementById(sCombinedBorSuffix) %>.readOnly = !b;
            <%= AspxTools.JsGetElementById(sCombinedBorSsn) %>.readOnly = !b;
            <%= AspxTools.JsGetElementById(sCombinedCoborFirstNm) %>.readOnly = !b;
            <%= AspxTools.JsGetElementById(sCombinedCoborMidNm) %>.readOnly = !b;
            <%= AspxTools.JsGetElementById(sCombinedCoborLastNm) %>.readOnly = !b;
            <%= AspxTools.JsGetElementById(sCombinedCoborSuffix) %>.readOnly = !b;
            <%= AspxTools.JsGetElementById(sCombinedCoborSsn) %>.readOnly = !b;
            <%= AspxTools.JsGetElementById(sCombinedCoborUsing2ndAppBorr) %>.disabled = b;    
        }

        function f_populateFromAsset() {
            var args = new Object();
            args["LoanID"] = ML.sLId;
 
          var result = gService.loanedit.call("CalculateAssetTotal", args);
          if (!result.error) {
              <%= AspxTools.JsGetElementById(sVerifAssetAmt) %>.value = result.value["VerifiedAssetTotal"];
              updateDirtyBit();
          }
        }
//-->
    </script>
    <form id="Transmittal" method="post" runat="server">
        <table class="FormTable" id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="MainRightHeader">Combined Uniform Underwriting and Transmittal Summary 
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10px">
                    <table id="Table5" cellspacing="0" cellpadding="0" width="770" border="0">
                        <tr>
                            <td class="LoanFormHeader" style="padding-left: 5px">I. Borrower and Property Information</td>
                        </tr>
                        <tr>
                            <td>
                                <table class="InsetBorder" id="Table26" cellspacing="0" cellpadding="0" width="98%" border="0">
                                    <tr>
                                        <td class="no-wrap">
                                            <table id="Table20" cellspacing="0" cellpadding="0" border="0">
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Borrower</td>
                                                    <td class="FieldLabel no-wrap">
                                                        <asp:CheckBox ID="sCombinedBorInfoLckd" runat="server" Text="Lock" onclick="refreshCalculation();"></asp:CheckBox>
                                                    </td>
                                                    <td class="no-wrap" width="20">&nbsp;
                                                    </td>
                                                    <td class="FieldLabel no-wrap">Co-borrower</td>
                                                    <td class="no-wrap"></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap"></td>
                                                    <td class="FieldLabel no-wrap"></td>
                                                    <td class="no-wrap" width="20"></td>
                                                    <td class="FieldLabel no-wrap" colspan="2">
                                                        <asp:CheckBox ID="sCombinedCoborUsing2ndAppBorr" onclick="refreshCalculation();" runat="server" Text="Use 2nd Borrower Info"></asp:CheckBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">First Name</td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="sCombinedBorFirstNm" runat="server"></asp:TextBox></td>
                                                    <td class="no-wrap"></td>
                                                    <td class="FieldLabel no-wrap">First Name</td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="sCombinedCoborFirstNm" TabIndex="5" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Middle Name</td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="sCombinedBorMidNm" runat="server"></asp:TextBox></td>
                                                    <td class="no-wrap"></td>
                                                    <td class="FieldLabel no-wrap">Middle Name</td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="sCombinedCoborMidNm" TabIndex="5" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Last Name</td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="sCombinedBorLastNm" runat="server"></asp:TextBox></td>
                                                    <td class="no-wrap"></td>
                                                    <td class="FieldLabel no-wrap">Last Name</td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="sCombinedCoborLastNm" TabIndex="5" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Suffix</td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="sCombinedBorSuffix" runat="server"></asp:TextBox></td>
                                                    <td class="no-wrap"></td>
                                                    <td class="FieldLabel no-wrap">Suffix
                                                    </td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="sCombinedCoborSuffix" TabIndex="5" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">SSN</td>
                                                    <td class="no-wrap">
                                                        <ml:SSNTextBox ID="sCombinedBorSsn" runat="server" preset="ssn" Width="75px"></ml:SSNTextBox></td>
                                                    <td class="no-wrap"></td>
                                                    <td class="FieldLabel no-wrap">SSN</td>
                                                    <td class="no-wrap">
                                                        <ml:SSNTextBox ID="sCombinedCoborSsn" TabIndex="5" runat="server" preset="ssn" Width="75px"></ml:SSNTextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="InsetBorder" id="Table27" cellspacing="0" cellpadding="0" width="98%" border="0">
                                    <tr>
                                        <td class="no-wrap">
                                            <table id="Table21" cellspacing="0" cellpadding="0" border="0">
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Property Address
                                                    </td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="sSpAddr" TabIndex="6" runat="server" Width="265px"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="no-wrap"></td>
                                                    <td class="no-wrap">
                                                        <asp:TextBox ID="sSpCity" TabIndex="6" runat="server" Width="166px"></asp:TextBox><ml:StateDropDownList ID="sSpState" TabIndex="6" runat="server" IncludeTerritories="false"></ml:StateDropDownList>
                                                        <ml:ZipcodeTextBox ID="sSpZip" TabIndex="6" runat="server" preset="zipcode" Width="50"></ml:ZipcodeTextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="Table2" cellspacing="0" cellpadding="0" width="98%" border="0" class="InsetBorder">
                                    <tr>
                                        <td class="no-wrap">
                                            <table id="Table10" cellspacing="0" cellpadding="0" border="0">
                                                <tr>
                                                    <td class="FieldLabel">Property Type</td>
                                                    <td>
                                                        <asp:DropDownList ID="sGseSpT" TabIndex="10" runat="server"></asp:DropDownList></td>
                                                    <td class="FieldLabel" width="20"></td>
                                                    <td class="FieldLabel">Number of Units</td>
                                                    <td>
                                                        <asp:TextBox ID="sUnitsNum" TabIndex="10" runat="server" Width="82px" MaxLength="4"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel">Occupancy Status</td>
                                                    <td>
                                                        <asp:DropDownList ID="aOccT" TabIndex="10" runat="server" Enabled="False"></asp:DropDownList></td>
                                                    <td class="FieldLabel"></td>
                                                    <td class="FieldLabel">Sales Price</td>
                                                    <td>
                                                        <ml:MoneyTextBox ID="sPurchPrice" TabIndex="10" runat="server" preset="money" Width="90" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td class="FieldLabel">Appraised Value</td>
                                                    <td>
                                                        <ml:MoneyTextBox ID="sApprVal" TabIndex="10" runat="server" preset="money" Width="90" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel">Freddie Mac Project Classification</td>
                                                    <td class="FieldLabel">
                                                        <asp:DropDownList ID="sSpProjectClassFreddieT" TabIndex="10" runat="server"></asp:DropDownList></td>
                                                    <td class="FieldLabel no-wrap"></td>
                                                    <td class="FieldLabel no-wrap"></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Fannie Mae Project Classification
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="sSpProjectClassFannieT" TabIndex="10" runat="server"></asp:DropDownList></td>
                                                    <td></td>
                                                    <td class="FieldLabel"></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel">Project Name</td>
                                                    <td>
                                                        <asp:TextBox ID="sProjNm" TabIndex="10" runat="server" Width="279px" MaxLength="72"></asp:TextBox></td>
                                                    <td class="FieldLabel"></td>
                                                    <td class="FieldLabel">Property Rights</td>
                                                    <td>
                                                        <asp:DropDownList ID="sEstateHeldT" runat="server" TabIndex="10"></asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel">CPM Project ID# (if any)</td>
                                                    <td>
                                                        <asp:TextBox ID="sCpmProjectId" runat="server" TabIndex="10" Width="181px"></asp:TextBox></td>
                                                    <td class="FieldLabel no-wrap"></td>
                                                    <td class="FieldLabel no-wrap"></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="LoanFormHeader" style="padding-left: 5px">II. Mortgage Information</td>
                        </tr>
                        <tr>
                            <td>
                                <table id="Table7" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td class="FieldLabel">Loan Type</td>
                                        <td>
                                            <asp:DropDownList ID="sLT" runat="server" onchange="refreshCalculation();" TabIndex="10"></asp:DropDownList></td>
                                        <td class="FieldLabel">Amortization Type</td>
                                        <td>
                                            <asp:DropDownList ID="sFinMethT" runat="server" onchange="refreshCalculation();" TabIndex="10"></asp:DropDownList></td>
                                        <td class="FieldLabel no-wrap">Amort. Desc.</td>
                                        <td>
                                            <asp:TextBox ID="sFinMethDesc" runat="server" Width="152px" MaxLength="50" TabIndex="10"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel"></td>
                                        <td></td>
                                        <td class="FieldLabel" colspan="3">
                                            <asp:CheckBox ID="sFinMethodPrintAsOther" TabIndex="10" runat="server" Text="Amortization Type Other" onclick="on_sFinMethodPrintAsOtherClick();"></asp:CheckBox>&nbsp;-
                                                <asp:TextBox ID="sFinMethPrintAsOtherDesc" TabIndex="10" runat="server"></asp:TextBox></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel"></td>
                                        <td></td>
                                        <td class="FieldLabel">
                                            <asp:CheckBox ID="sBiweeklyPmt" runat="server" Text="Biweekly Payment" TabIndex="10"></asp:CheckBox>
                                        </td>
                                        <td class="FieldLabel">
                                            <asp:CheckBox ID="sBalloonPmt" runat="server" TabIndex="10"></asp:CheckBox>
                                            Balloon
                                        </td>
                                        <td class="FieldLabel"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Loan Purpose</td>
                                        <td>
                                            <asp:DropDownList ID="sLPurposeT" runat="server" onchange="refreshCalculation();" TabIndex="10"></asp:DropDownList>
                                        </td>
                                        <td class="FieldLabel"></td>
                                        <td></td>
                                        <td class="FieldLabel">Lien Position</td>
                                        <td>
                                            <asp:DropDownList ID="sLienPosT" runat="server" onchange="refreshCalculation();" TabIndex="10"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Purpose of Refi</td>
                                        <td colspan="2">
                                            <asp:DropDownList ID="sGseRefPurposeT" runat="server" TabIndex="10"></asp:DropDownList>
                                        </td>
                                        <td></td>
                                        <td class="FieldLabel">Sub. Fin.</td>
                                        <td>
                                            <ml:MoneyTextBox ID="sSubFin" runat="server" preset="money" Width="90" onchange="refreshCalculation();" TabIndex="10"></ml:MoneyTextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="Table11" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td class="FieldLabel" colspan="2">Note Information</td>
                                        <td></td>
                                        <td></td>
                                        <td class="FieldLabel" colspan="2">If Second Mortgage</td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">Original Loan Amount</td>
                                        <td>
                                            <ml:MoneyTextBox ID="sFinalLAmt" runat="server" preset="money" Width="90" ReadOnly="true" TabIndex="10"></ml:MoneyTextBox>
                                        </td>
                                        <td class="FieldLabel">Mortgage Originator</td>
                                        <td>
                                            <asp:DropDownList ID="sMOrigT" runat="server" onchange="refreshCalculation();" TabIndex="11"></asp:DropDownList>
                                        </td>
                                        <td class="FieldLabel">First Mortgage Owner</td>
                                        <td>
                                            <asp:DropDownList ID="s1stMOwnerT" runat="server" onchange="refreshCalculation();" TabIndex="12"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">Initial P&amp;I Payment</td>
                                        <td>
                                            <ml:MoneyTextBox ID="sProThisMPmt" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                        <td class="FieldLabel" colspan="2">
                                            <asp:CheckBox ID="sIsMOrigBroker" runat="server" Text="Broker" TabIndex="11"></asp:CheckBox><asp:CheckBox ID="sIsMOrigCorrespondent" runat="server" Text="Correspondent" TabIndex="11"></asp:CheckBox></td>
                                        <td class="FieldLabel">Loan Amount</td>
                                        <td>
                                            <ml:MoneyTextBox ID="s1stMtgOrigLAmt" runat="server" preset="money" Width="90" onchange="refreshCalculation();" TabIndex="12"></ml:MoneyTextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">Initial Note Rate</td>
                                        <td>
                                            <ml:PercentTextBox ID="sNoteIR" runat="server" preset="percent" Width="70" onchange="refreshCalculation();" TabIndex="10"></ml:PercentTextBox></td>
                                        <td colspan="3" class="FieldLabel">Broker/Correspondent Name and Company Name</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">Term</td>
                                        <td>
                                            <asp:TextBox ID="sTerm" runat="server" Width="60px" MaxLength="4" onchange="refreshCalculation();" TabIndex="10"></asp:TextBox></td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TransmMOriginatorCompanyName" runat="server" Width="293px" TabIndex="11"></asp:TextBox></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel"></td>
                                        <td></td>
                                        <td class="FieldLabel">Buydown:
                                                <asp:RadioButtonList ID="sBuydown" runat="server" TabIndex="11" onclick="refreshCalculation();" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:RadioButtonList>
                                        </td>
                                        <td class="FieldLabel">Buydown Terms</td>
                                        <td>
                                            <asp:TextBox ID="sTransmBuydwnTermDesc" runat="server" Width="75px" TabIndex="11"></asp:TextBox></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="LoanFormHeader" style="padding-left: 5px">III. Underwriting Information</td>
                        </tr>
                        <tr>
                            <td>
                                <table id="Table15" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td valign="top" class="no-wrap">
                                            <table class="InsetBorder" id="Table16" cellspacing="0" cellpadding="0" width="98%" border="0">
                                                <tr>
                                                    <td class="no-wrap">
                                                        <table id="Table3" cellspacing="0" cellpadding="0" border="0">
                                                            <tr>
                                                                <td class="FieldLabel">Underwriter's Name</td>
                                                                <td>
                                                                    <asp:TextBox ID="sUwerNm" runat="server" Width="220px" MaxLength="56" TabIndex="12"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">Appraiser's Name</td>
                                                                <td>
                                                                    <asp:TextBox ID="sApprerNm" TabIndex="12" runat="server" Width="220px" MaxLength="56"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">Appraiser's License Number</td>
                                                                <td>
                                                                    <asp:TextBox ID="sApprerLicNum" TabIndex="12" runat="server" Width="220px" MaxLength="21"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">Appraisal Company Name</td>
                                                                <td>
                                                                    <asp:TextBox ID="sApprComNm" runat="server" Width="220px" MaxLength="21" TabIndex="12"></asp:TextBox></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table class="InsetBorder" id="Table19" cellspacing="0" cellpadding="0" width="98%" border="0">
                                                <tr>
                                                    <td class="no-wrap">
                                                        <table id="Table13" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td style="width: 91px"></td>
                                                                <td class="FieldLabel">Borrower</td>
                                                                <td class="FieldLabel">Co-borrower</td>
                                                                <td class="FieldLabel">Total</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel" style="width: 91px">Base Income</td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sPrimAppTotBaseI" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sNprimAppsTotBaseI" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sLTotBaseI" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel" style="width: 91px">Other Income</td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sPrimAppTotNonbaseI" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sNprimAppsTotNonbaseI" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sLTotNonbaseI" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel" style="width: 91px">
                                                                    <asp:TextBox ID="sOIFrom1008Desc" runat="server" ReadOnly="True"></asp:TextBox></td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sPrimAppOIFrom1008" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sNprimAppsTotOIFrom1008" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sLTotOIFrom1008" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel" style="width: 91px"><a tabindex="-1" href="javascript:linkMe('../PropertyInfo.aspx?pg=2');" title="Go to Rental Income">Positive 
                                                                            Cash Flow&nbsp;(Subject Property)</a></td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sPrimAppTotSpPosCf" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sNprimAppsTotSpPosCf" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sLTotSpPosCf" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel" style="width: 91px">Total Income</td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sPrimAppTotI" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sNprimAppsTotI" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sLTotI" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table class="InsetBorder" id="Table14" cellspacing="0" cellpadding="0" width="98%" border="0">
                                                <tr>
                                                    <td class="FieldLabel" colspan="2">Qualifying Ratios</td>
                                                </tr>
                                                <tr title="Primary Housing Expense/Income">
                                                    <td class="FieldLabel">Housing Ratio</td>
                                                    <td>
                                                        <ml:PercentTextBox ID="sQualTopR" runat="server" preset="percent" Width="70" ReadOnly="True"></ml:PercentTextBox></td>
                                                </tr>
                                                <tr title="Total Obligations/Income">
                                                    <td class="FieldLabel">Total Debt Ratio</td>
                                                    <td>
                                                        <ml:PercentTextBox ID="sQualBottomR" runat="server" preset="percent" Width="70" ReadOnly="True"></ml:PercentTextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel">Debt-to-Housing Gap Ratio
                                                            <asp:CheckBox ID="sDebtToHousingGapRLckd" onclick="refreshCalculation();" runat="server" Text="Lock" TabIndex="12"></asp:CheckBox></td>
                                                    <td>
                                                        <ml:PercentTextBox ID="sDebtToHousingGapR" runat="server" preset="percent" Width="70" onchange="refreshCalculation();" TabIndex="12"></ml:PercentTextBox></td>
                                                </tr>
                                            </table>
                                            <table id="Table17" cellspacing="0" cellpadding="0" width="98%" border="0">
                                                <tr>
                                                    <td>
                                                        <table class="InsetBorder" id="Table6" cellspacing="5px" cellpadding="0" width="98%" border="0">
                                                            <tr>
                                                                <td class="FieldLabel" style="width: 101px">Qualifying Rate</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <ml:PercentTextBox ID="sQualIR" runat="server" preset="percent" Width="70" ReadOnly="True"></ml:PercentTextBox></td>
                                                                <td>
                                                                    <asp:DropDownList ID="sQualIRDeriveT" TabIndex="12" runat="server"></asp:DropDownList></td>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                        <table class="InsetBorder" id="Table25" cellspacing="5px" cellpadding="0" width="98%" border="0">
                                                            <tr>
                                                                <td class="no-wrap FieldLabel">Escrow (T&amp;I):&nbsp;<asp:RadioButtonList ID="sWillEscrowBeWaived" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" TabIndex="12">
                                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                                </asp:RadioButtonList></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <table class="InsetBorder" id="Table23" cellspacing="0" cellpadding="0" width="98%" border="0">
                                                            <tr>
                                                                <td class="no-wrap" valign="top">
                                                                    <table id="Table24" cellspacing="0" cellpadding="0" border="0">
                                                                        <tr>
                                                                            <td class="FieldLabel no-wrap">Loan-to-Value Ratios</td>
                                                                            <td class="no-wrap"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="FieldLabel no-wrap">LTV</td>
                                                                            <td class="no-wrap">
                                                                                <ml:PercentTextBox ID="sLtvR" runat="server" preset="percent" Width="70" ReadOnly="True"></ml:PercentTextBox></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="FieldLabel no-wrap">CLTV/TLTV</td>
                                                                            <td class="no-wrap">
                                                                                <ml:PercentTextBox ID="sCltvR" runat="server" preset="percent" Width="70" ReadOnly="True"></ml:PercentTextBox></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="FieldLabel no-wrap">HCLTV/HTLTV<asp:CheckBox ID="sHcltvRLckd" onclick="refreshCalculation();" runat="server" Text="Lock" TabIndex="12"></asp:CheckBox>
                                                                            </td>
                                                                            <td class="no-wrap">
                                                                                <ml:PercentTextBox ID="sHcltvR" runat="server" preset="percent" Width="70" onchange="refreshCalculation();" TabIndex="12"></ml:PercentTextBox></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>

                                            <table class="InsetBorder" id="QualTermTable" cellspacing="0" cellpadding="0" width="98%" border="0">
                                                <tr>
                                                    <td class="FieldLabel">
                                                        Qualifying Term
                                                        <asp:DropDownList id="sQualTermCalculationType" runat="server" onchange="refreshCalculation();" CssClass="sQualTermCalculationType" />
                                                        &nbsp
                                                        <input type="text" id="sQualTerm" runat="server" preset="numeric" onchange="refreshCalculation();" class="sQualTerm" />
                                                    </td>
                                                </tr>
                                            </table>

                                            <table class="InsetBorder" id="EscrowDetailsTable" cellspacing="0" cellpadding="0" width="98%" border="0">
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Community Lending/Affordable Housing Initiative:<asp:RadioButtonList ID="sIsCommLen" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" TabIndex="12">
                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                    </asp:RadioButtonList></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Home Buyers/Homeownership Education Certificate in 
                                                        file:<asp:RadioButtonList ID="sIsHOwnershipEdCertInFile" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" TabIndex="12">
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table cellpadding="0" cellspacing="5" border="0" class="InsetBorder" width="98%">
                                                <tr>
                                                    <td class="FieldLabel no-wrap">Approved Date
                                                    </td>
                                                    <td>
                                                        <ml:DateTextBox ID="sApprovD" TabIndex="12" runat="server" Width="75" preset="date"></ml:DateTextBox>
                                                    </td>
                                                    <td class="FieldLabel no-wrap">Underwriting Date
                                                    </td>
                                                    <td>
                                                        <ml:DateTextBox ID="sUnderwritingD" TabIndex="12" runat="server" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top" class="no-wrap">
                                            <table class="InsetBorder" id="Table18" cellspacing="0" cellpadding="0" border="0">
                                                <tr>
                                                    <td class="no-wrap">
                                                        <table id="Table4" cellspacing="0" cellpadding="0" border="0">
                                                            <tr>
                                                                <td class="FieldLabel"><a href="javascript:linkMe('../BorrowerInfo.aspx?pg=7')">Present Housing Payment</a></td>
                                                                <td class="FieldLabel">
                                                                    <ml:MoneyTextBox ID="sPresLTotHExp" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel" colspan="2">Proposed Monthly Payments</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel Underline" colspan="2">
                                                                    <a id="BPRAnchor" href="javascript:hrefBorrowPrimaryResidence()">Borrower's Primary Residence</a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">1st&nbsp;Mortgage P&amp;I</td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sTransmPro1stMPmt" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">2nd Mortgage P&amp;I</td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sTransmPro2ndMPmt" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">Hazard Insurance</td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sTransmProHazIns" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">Taxes</td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sTransmProRealETx" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">Mortgage Insurance</td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sTransmProMIns" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">Homeowners Assoc. Fees</td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sTransmProHoAssocDues" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">Other</td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sTransmOtherProHExp" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">Total Primary Housing Expense</td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sTransmProTotHExp" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel Underline">Other Obligations</td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">Negative Cash Flow</td>
                                                                <td class="FieldLabel">
                                                                    <asp:CheckBox ID="sNegCfLckd" runat="server" Text=" " BackColor="Gainsboro" ForeColor="Silver" Enabled="False"></asp:CheckBox>Locked</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel" align="right"><a tabindex="-1" href="javascript:linkMe('../PropertyInfo.aspx?pg=2');" title="Go to Rental Income">Subject 
                                                                            Property</a></td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sSpNegCf" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel" align="right"><a title="Go to Real Estate Schedule" tabindex="-1" onclick="redirectToUladPage('REO');">Other 
                                                                            Properties</a></td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sOpNegCf" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel" align="left"><a onclick="redirectToUladPage('Liabilities');" tabindex="-1">Non-mortgage 
                                                                            Liabilities</a></td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sTransmOMonPmt" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">Total All Monthly Payments</td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sTransmTotMonPmt" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">Borrower Funds to Close</td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">Required<asp:CheckBox ID="sTransmFntcLckd" onclick="refreshCalculation();" runat="server" Text="Lock" TabIndex="12"></asp:CheckBox></td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sTransmFntc" runat="server" preset="money" Width="90" onchange="refreshCalculation();" TabIndex="12"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel no-wrap">Verified Assets&nbsp;&nbsp;
                                                                    <input style="width: 80px" type="button" value="From Assets" onclick="f_populateFromAsset();" nohighlight></td>
                                                                <td>
                                                                    <ml:MoneyTextBox ID="sVerifAssetAmt" runat="server" preset="money" Width="90" onchange="refreshCalculation();" TabIndex="12"></ml:MoneyTextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">Source of Funds</td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel" colspan="2">
                                                                    <asp:TextBox ID="sFntcSrc" runat="server" Width="250px" TabIndex="12"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">No. of Months Reserves</td>
                                                                <td>
                                                                    <asp:TextBox ID="sRsrvMonNumDesc" runat="server" Width="56px" TabIndex="12"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="FieldLabel">Interested Party Contributions</td>
                                                                <td>
                                                                    <ml:PercentTextBox ID="sInterestedPartyContribR" runat="server" preset="percent" Width="70" TabIndex="12"></ml:PercentTextBox></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="no-wrap" colspan="2">
                                <table class="InsetBorder" id="Table22" cellspacing="0" cellpadding="0" border="0" width="99%">
                                    <tbody runat="server" id="LoanVersion12Section">
                                        <tr>
                                            <td class="FieldLabel no-wrap">Property valuation method</td>
                                            <td colspan="3">
                                                <asp:DropDownList ID="sSpValuationMethodT" runat="server" onchange="onValuationMethodChanged();"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel no-wrap">Appraisal Form Type</td>
                                            <td colspan="3">
                                                <asp:DropDownList ID="sSpAppraisalFormT" runat="server" onchange="onAppraisalFormChanged();"></asp:DropDownList>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Level of Property Review</td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">
                                            <asp:CheckBox ID="sApprFull" runat="server" Text="Interior/Exterior (Full)" onclick="onAppraisalTypeChanged(this)" TabIndex="12"></asp:CheckBox></td>
                                        <td class="FieldLabel no-wrap">
                                            <asp:CheckBox ID="sApprDriveBy" runat="server" Text="Exterior Only" TabIndex="12" onclick="onAppraisalTypeChanged(this)"></asp:CheckBox></td>
                                        <td class="FieldLabel no-wrap">
                                            <asp:CheckBox ID="sIsSpReviewNoAppr" runat="server" Text="No Appraisal" TabIndex="12" onclick="onAppraisalTypeChanged(this)"></asp:CheckBox></td>
                                        <td class="FieldLabel no-wrap">Form Number:
                                                <asp:TextBox ID="sSpReviewFormNum" runat="server" TabIndex="12"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table class="InsetBorder" id="Table8" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td class="FieldLabel no-wrap">Risk Assessment</td>
                                        <td class="no-wrap"></td>
                                        <td width="10"></td>
                                        <td class="FieldLabel no-wrap">DU Case ID</td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sDuCaseId" runat="server" TabIndex="12"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap" colspan="2">
                                            <asp:CheckBox ID="sIsManualUw" runat="server" Text="Manual Underwriting" TabIndex="12"></asp:CheckBox></td>
                                        <td></td>
                                        <td class="FieldLabel no-wrap"><ml:EncodedLiteral id="sLpAusKeyLabel" runat="server">LP AUS Key#</ml:EncodedLiteral></td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sLpAusKey" runat="server" TabIndex="12"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap" colspan="2">
                                            <asp:CheckBox ID="sIsDuUw" runat="server" Text="DU" TabIndex="12"></asp:CheckBox><asp:CheckBox ID="sIsLpUw" runat="server" Text="LP" TabIndex="12"></asp:CheckBox><asp:CheckBox ID="sIsOtherUw" runat="server" Text="Other" TabIndex="12"></asp:CheckBox><asp:TextBox ID="sOtherUwDesc" runat="server" TabIndex="12"></asp:TextBox></td>
                                        <td></td>
                                        <td class="FieldLabel no-wrap"><ml:EncodedLiteral id="sLpDocClassLabel" runat="server">LP Doc Class (Freddie)</ml:EncodedLiteral></td>
                                        <td class="no-wrap">
                                            <ml:ComboBox ID="sLpDocClass" runat="server" TabIndex="12"></ml:ComboBox></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">AUS Recommendation</td>
                                        <td class="no-wrap" colspan="1">
                                            <asp:TextBox ID="sAusRecommendation" runat="server" TabIndex="12"></asp:TextBox></td>
                                        <td></td>
                                        <td class="FieldLabel no-wrap">Representative Credit/Indicator Score</td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sRepCrScore" runat="server" TabIndex="12"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Underwriter Comments</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="sTransmUwerComments" runat="server" Width="100%" Height="136px" TextMode="MultiLine" MaxLength="1000" TabIndex="12"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="LoanFormHeader" style="padding-left: 5px">IV. Seller, Contract, and Contact Information</td>
                        </tr>
                        <tr>
                            <td>
                                <table id="Table9" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td class="FieldLabel no-wrap">Seller</td>
                                        <td class="FieldLabel no-wrap">
                                            <UC:CFM ID="CFM" runat="server" CompanyNameField="sLenNm" AgentNameField="sLenContactNm" StreetAddressField="sLenAddr" AgentTitleField="sLenContactTitle" CityField="sLenCity" StateField="sLenState" ZipField="sLenZip" PhoneField="sLenContactPhone"></UC:CFM>
                                        </td>
                                        <td class="no-wrap"></td>
                                        <td class="no-wrap"></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Seller Name</td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sLenNm" TabIndex="50" runat="server" Width="135px" MaxLength="56"></asp:TextBox></td>
                                        <td class="FieldLabel no-wrap">Contact Name</td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sLenContactNm" TabIndex="60" runat="server" Width="164px" MaxLength="21"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Seller Address</td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sLenAddr" TabIndex="50" runat="server" Width="241px" MaxLength="36"></asp:TextBox></td>
                                        <td class="FieldLabel no-wrap">Contact Title</td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sLenContactTitle" TabIndex="60" runat="server" Width="135px" MaxLength="21"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="no-wrap"></td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sLenCity" TabIndex="50" runat="server" Width="150px" MaxLength="36"></asp:TextBox><ml:StateDropDownList ID="sLenState" TabIndex="50" runat="server"></ml:StateDropDownList>
                                            <ml:ZipcodeTextBox ID="sLenZip" TabIndex="50" runat="server" preset="zipcode" Width="50" CssClass="mask"></ml:ZipcodeTextBox></td>
                                        <td class="FieldLabel no-wrap">Contact Phone</td>
                                        <td class="no-wrap">
                                            <ml:PhoneTextBox ID="sLenContactPhone" TabIndex="60" runat="server" Width="88px" MaxLength="21" preset="phone"></ml:PhoneTextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Seller No.</td>
                                        <td class="FieldLabel no-wrap">
                                            <asp:TextBox ID="sLenNum" TabIndex="50" runat="server" Width="108px" MaxLength="21"></asp:TextBox></td>
                                        <td class="FieldLabel no-wrap">Date</td>
                                        <td class="no-wrap">
                                            <ml:DateTextBox ID="sLenContractD" TabIndex="60" runat="server" preset="date" Width="75"></ml:DateTextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Seller Loan No.&nbsp<asp:CheckBox ID="sLenLNumLckd" Text="Lock" runat="server" onclick="refreshCalculation();" /></td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sLenLNum" TabIndex="50" runat="server" Width="212px" MaxLength="21"></asp:TextBox></td>
                                        <td class="no-wrap"></td>
                                        <td class="no-wrap"></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Investor Loan No.&nbsp;
                                                <asp:CheckBox ID="sInvestLNumLckd" Text="Lock" runat="server" onclick="refreshCalculation();" />&nbsp;
                                        </td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sInvestLNum" TabIndex="10" runat="server" Width="141px" MaxLength="21"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Master Commitment No.</td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sCommitNum" TabIndex="50" runat="server" Width="149px" MaxLength="21"></asp:TextBox></td>
                                        <td class="no-wrap"></td>
                                        <td class="no-wrap"></td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel no-wrap">Contract No.</td>
                                        <td class="no-wrap">
                                            <asp:TextBox ID="sContractNum" TabIndex="50" runat="server" Width="152px" MaxLength="21"></asp:TextBox></td>
                                        <td class="no-wrap"></td>
                                        <td class="no-wrap"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:cModalDlg ID="CModalDlg1" runat="server"></uc1:cModalDlg>
                </td>
            </tr>
        </table>
        <!-- Stores the saved occupancy status value -->
        <input type="hidden" id="savedOccupency" value=<%=AspxTools.HtmlAttribute(aOccT.SelectedItem.Text.ToLower())%>>
    </form>
    <script language="javascript" type="text/javascript" src="Transmittal_04.js"></script>
</body>
</html>
