<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="MLDSpg2.ascx.cs" Inherits="LendersOfficeApp.newlos.Forms.MLDSpg2" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<table id="Table1" cellSpacing="0" cellPadding="0" border="0">
	<tr>
		<td class="FormTableSubheader">Additional Required California Disclosures</td>
	</tr>
	<tr>
		<td>
			<TABLE class="InsetBorder" id="Table8" cellSpacing="0" cellPadding="0" width="99%" border="0">
				<TR>
					<TD noWrap>
						<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="FieldLabel">Proposed Loan Amount</TD>
								<TD></TD>
								<TD><ml:MoneyTextBox id="sFinalLAmt" ReadOnly="True" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">Initial Commissions, Fees, Costs and Expenses Summarized on 
									Page 1</TD>
								<TD vAlign="top"><ml:MoneyTextBox id="sTotEstScMlds" ReadOnly="True" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">Payment of Other Obligations (List):</TD>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" style="PADDING-LEFT:20px" title="The purchase of credit life and/or disability insurance by a borrower is NOT required as a condition of making this proposed loan">Credit Life and/or Disability Insurance</TD>
								<TD><ml:MoneyTextBox id="sDisabilityIns" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" style="PADDING-LEFT:20px">Purchase Price</TD>
								<TD><ml:MoneyTextBox id="sPurchPrice" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></TD>
								<TD></TD> 
							</TR>
							<TR>
								<TD class="FieldLabel" style="PADDING-LEFT:20px"><A title="Go to GFE" href="#" onclick="linkToGFE();">Payoff</A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
								<TD><ml:MoneyTextBox id="sRefPdOffAmtCamlds" ReadOnly="True" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" style="PADDING-LEFT:20px">Paid by&nbsp;Seller and&nbsp;Others (subtract)</TD>
								<TD><ml:MoneyTextBox id="sTotCcPboPbs" ReadOnly="True" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD style="PADDING-LEFT:20px"><asp:TextBox id="sU1FntcDesc" runat="server" MaxLength="21" Width="342px"></asp:TextBox></TD>
								<TD><ml:MoneyTextBox id="sU1Fntc" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">Subtotal of All Deductions</TD>
								<TD></TD>
								<TD><ml:MoneyTextBox id="sTotDeductFromFinalLAmt" ReadOnly="True" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">Estimated Cash at Closing</TD>
								<TD></TD>
								<TD><ml:MoneyTextBox id="sTotEstFntcCamlds" ReadOnly="True" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</td>
	</tr>
	<tr>
		<td>
			<TABLE class="InsetBorder" id="Table9" cellSpacing="0" cellPadding="0" width="99%" border="0">
				<TR>
					<TD noWrap>
						<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="FieldLabel">Interest Rate</TD>
								<TD><ml:PercentTextBox id="sNoteIR" preset="percent" width="70" runat="server" onchange="refreshCalculation();"></ml:PercentTextBox></TD>
								<TD><asp:dropdownlist id="sFinMethT" runat="server" onchange="refreshCalculation();" Width="116px"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">Loan Term / Due (months)</TD>
								<TD><asp:TextBox id="sTerm" runat="server" onchange="refreshCalculation();" MaxLength="4" Width="48"></asp:TextBox>&nbsp;/
									<asp:TextBox id="sDue" runat="server" onchange="refreshCalculation();" MaxLength="4" Width="48px"></asp:TextBox></TD>
								<TD class="FieldLabel" noWrap><asp:CheckBox id="sBalloonPmt" runat="server" onclick="refreshCalculation();"></asp:CheckBox>Balloon &nbsp;&nbsp;&nbsp;&nbsp; Final 
									Balloon Payment
									<ml:MoneyTextBox id="sFinalBalloonPmt" ReadOnly="True" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">Loan Payments</TD>
								<TD><ml:MoneyTextBox id="sProThisMPmt" ReadOnly="True" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
								<TD class="FieldLabel" style="PADDING-LEFT:72px">Estimated Due Date
									<span style="PADDING-LEFT:13px"><asp:TextBox id="sFinalBalloonPmtDueD" runat="server" ReadOnly="True" Width="90px"></asp:TextBox></span></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</td>
	</tr>
  <TR>
    <TD>
      <TABLE id=Table13 cellSpacing=0 cellPadding=0 border=0>
        <TR>
          <TD class=FieldLabel noWrap>This loan is based on limited or no 
            documentation of your income ...</TD>
          <TD noWrap><asp:CheckBoxList id=sMldsIsNoDocTri runat="server" RepeatDirection="Horizontal"></asp:CheckBoxList></TD></TR>
        <TR>
          <TD class=FieldLabel noWrap>Has Late Charges</TD>
          <TD noWrap><asp:CheckBoxList id=sMldsLateChargeTri runat="server" RepeatDirection="Horizontal"></asp:CheckBoxList></TD></TR></TABLE></TD></TR>
	<tr>
		<td>
			<table id="Table4" cellSpacing="0" cellPadding="0" width="99%" border="0" class="InsetBorder">
				<tr>
					<td class="FormTableSubheader">Prepayments</td>
				</tr>
				<tr>
					<td>
						<asp:dropdownlist id="sMldsPpmtT" runat="server" onchange="prepaymentsEnableOptionsDDL();">
							<asp:ListItem Value="0" Selected="True">Leave Blank</asp:ListItem>
							<asp:ListItem Value="3">No prepayment penalty</asp:ListItem>
							<asp:ListItem Value="1">Other (see loan documents for details)</asp:ListItem>
							<asp:ListItem Value="2">Any payment of principal in any calendar year in excess of 20% of ...</asp:ListItem>
						</asp:dropdownlist></td>
				</tr>
        <TR>
          <TD>				Pay prepayment penalty if the loan is paid off or refinanced in 
				the first <asp:TextBox id=sMldsPpmtPeriod runat="server" Width="65px"></asp:TextBox>&nbsp;years.
           </TD>
        </TR>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp; ...
				<asp:dropdownlist id="sMldsPpmtBaseT" runat="server" Width="141px">
					<asp:ListItem Value="0" Selected="True">the unpaid balance</asp:ListItem>
					<asp:ListItem Value="1">the original balance</asp:ListItem>
				</asp:dropdownlist>&nbsp;will include a penalty not to exceed
				<asp:TextBox id="sMldsPpmtMonMax" runat="server" Width="52px" MaxLength="4"></asp:TextBox>&nbsp;months advance interest... The prepayment
			</td>
		</tr>
        <TR>
          <TD>&nbsp;&nbsp;&nbsp;&nbsp; could be as much as <ml:MoneyTextBox id=sMldsPpmtMaxAmt runat="server" width="90" preset="money"></ml:MoneyTextBox>
          </TD>
        </TR>
        <TR>
          <TD>Other Prepayment Penalty Description</TD></TR>
        <TR>
          <TD><asp:TextBox id=sMldsPpmtOtherDetail runat="server" Width="427px" TextMode="MultiLine" Height="52px"></asp:TextBox></TD></TR>
			</table>
			&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
  <TR>
    <TD>
      <TABLE class=InsetBorder id=Table14 cellSpacing=0 cellPadding=0 
        border=0>
        <TR>
          <TD noWrap class=FormTableSubheader>Taxes and Insurance</TD></TR>
        <TR>
          <TD noWrap>There <asp:DropDownList id=sMldsHasImpound runat="server">
<asp:ListItem Value="0">is NO impound (escrow) account, you will have to plan for the payment of</asp:ListItem>
<asp:ListItem Value="1">will be an impound account which will collect</asp:ListItem>
</asp:DropDownList></TD></TR>
        <TR>
          <TD noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
			<asp:CheckBox id=sMldsImpoundIncludeRealETx runat="server" Text="County Property Taxes"></asp:CheckBox>
			<asp:CheckBox id=sMldsImpoundIncludeHazIns runat="server" Text="Haz. Ins."></asp:CheckBox>
			<asp:CheckBox id=sMldsImpoundIncludeMIns runat="server" Text="Mortgage Insurance"></asp:CheckBox>
			<asp:CheckBox id=sMldsImpoundIncludeFloodIns runat="server" Text="Flood Insurance"></asp:CheckBox>
			<asp:CheckBox id=sMldsImpoundIncludeOther runat="server" Text="Other" onclick="enableOtherTextBox();" ></asp:CheckBox>
				<asp:TextBox id=sMldsImpoundOtherDesc runat="server"></asp:TextBox></TD></TR>
        <TR>
          <TD noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; approximately <ml:MoneyTextBox id=sMldsMonthlyImpoundPmt runat="server" width="90" preset="money"></ml:MoneyTextBox>&nbsp;per 
            month.</TD></TR></TABLE></TD></TR>
	<tr>
		<td>
			<TABLE class="InsetBorder" id="Table12" cellSpacing="0" cellPadding="0" width="99%" border="0">
				<TR>
					<TD class="FormTableSubheader" noWrap>Other Liens Currently on this Property (for which the borrower is obligated)</TD></TR>
				<TR>
					<TD noWrap>
						<TABLE id="Table5" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FieldLabel">Lienholder's Name</TD>
								<TD class="FieldLabel">Amount Owing</TD>
								<TD class="FieldLabel">Priority</TD>
							</TR>
							<TR>
								<TD><asp:TextBox id="sLienholder1NmBefore" runat="server" MaxLength="36" Width="232px"></asp:TextBox></TD>
								<TD><ml:MoneyTextBox id="sLien1AmtBefore" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
								<TD><ml:ComboBox id="sLien1PriorityBefore" runat="server" Width="100px"></ml:ComboBox></TD>
							</TR>
							<TR>
								<TD><asp:TextBox id="sLienholder2NmBefore" runat="server" MaxLength="36" Width="232px"></asp:TextBox></TD>
								<TD><ml:MoneyTextBox id="sLien2AmtBefore" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
								<TD><ml:ComboBox id="sLien2PriorityBefore" runat="server" Width="100px"></ml:ComboBox></TD>
							</TR>
							<TR>
								<TD><asp:TextBox id="sLienholder3NmBefore" runat="server" MaxLength="36" Width="232px"></asp:TextBox></TD>
								<TD><ml:MoneyTextBox id="sLien3AmtBefore" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
								<TD><ml:ComboBox id="sLien3PriorityBefore" runat="server" Width="100px"></ml:ComboBox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</td>
	</tr>
	<tr>
		<td>
			<TABLE class="InsetBorder" id="Table11" cellSpacing="0" cellPadding="0" width="99%" border="0">
				<TR>
					<TD class="FormTableSubheader" noWrap>Liens that may Remain on this Property after the Proposed Loan</TD></TR>
				<TR>
					<TD noWrap>
						<table id="Table6" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FieldLabel">Lienholder's Name</TD>
								<TD class="FieldLabel">Amount Owing</TD>
								<TD class="FieldLabel">Priority</TD>
							</TR>
							<TR>
								<TD><asp:TextBox id="sLienholder1NmAfter" runat="server" MaxLength="36" Width="232"></asp:TextBox></TD>
								<TD><ml:MoneyTextBox id="sLien1AmtAfter" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
								<TD><ml:ComboBox id="sLien1PriorityAfter" runat="server" Width="100px"></ml:ComboBox></TD>
							</TR>
							<TR>
								<TD><asp:TextBox id="sLienholder2NmAfter" runat="server" MaxLength="36" Width="232px"></asp:TextBox></TD>
								<TD><ml:MoneyTextBox id="sLien2AmtAfter" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
								<TD><ml:ComboBox id="sLien2PriorityAfter" runat="server" Width="100px"></ml:ComboBox></TD>
							</TR>
							<TR>
								<TD><asp:TextBox id="sLienholder3NmAfter" runat="server" MaxLength="36" Width="232px"></asp:TextBox></TD>
								<TD><ml:MoneyTextBox id="sLien3AmtAfter" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
								<TD><ml:ComboBox id="sLien3PriorityAfter" runat="server" Width="100px"></ml:ComboBox></TD>
							</TR>
						</table>
					</TD>
				</TR>
			</TABLE>
		</td>
	</tr>
	<TR>
		<TD>
			<TABLE class="InsetBorder" id="Table10" cellSpacing="0" cellPadding="0" width="99%" border="0">
				<TR>
					<TD noWrap>
						<TABLE id="Table7" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FormTableSubheader" colspan=4>VIII.&nbsp; Article 7 Compliance</TD></TR>
							<TR>
								<TD class="FieldLabel" colSpan="4">This loan
									<asp:DropDownList id="sBrokControlledFundT" runat="server">
										<asp:ListItem Value="0" Selected="True">leave blank</asp:ListItem>
										<asp:ListItem Value="1">may</asp:ListItem>
										<asp:ListItem Value="2">will</asp:ListItem>
										<asp:ListItem Value="3">will not</asp:ListItem>
									</asp:DropDownList>&nbsp;be made wholly or in part from broker-controlled funds ...</TD>
							</TR>
			  <tr height=5><td class=FieldLabel nowrap></td></tr>
              <tr>
				<td class=FieldLabel nowrap></td>
                <td class="w-400" nowrap>
					<uc:CFM id="CFM" runat="server"></uc:CFM>
                </td></tr>
              <tr>
                <td class=FieldLabel>Broker Name</td>
                <td><asp:TextBox id=GfeTilCompanyName runat="server" Width="248px"></asp:TextBox></td>
                <td class=FieldLabel></td>
                <td></td></tr>
              <tr>
                <td class=FieldLabel>Broker Address</td>
                <td><asp:TextBox id=GfeTilStreetAddr runat="server" Width="252px"></asp:TextBox></td>
                <td class=FieldLabel nowrap>Broker Lic #</td>
                <td><asp:TextBox id=GfeTilLicenseNumOfCompany runat="server" MaxLength="21"></asp:TextBox></td></tr>
              <tr>
                <td class=FieldLabel></td>
                <td nowrap><asp:TextBox id=GfeTilCity runat="server"></asp:TextBox><ml:StateDropDownList id=GfeTilState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=GfeTilZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td>
                <td class=FieldLabel></td>
                <td></td></tr>
							<TR>
								<TD class="FieldLabel">Broker's Representative</TD>
								<TD><asp:TextBox id="GfeTilPreparerName" runat="server" MaxLength="56" Width="198px"></asp:TextBox></TD>
								<TD class="FieldLabel">License #</TD>
								<TD><asp:TextBox id="GfeTilLicenseNumOfAgent" runat="server" MaxLength="21"></asp:TextBox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
	<TR>
		<TD></TD>
	</TR>
</table>

<uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>

<SCRIPT language="javascript">
<!--


function linkToGFE() {
  <% if (sUseObsoleteGfeForm) { %>
    linkMe('GoodFaithEstimateOld.aspx');
  <% } else { %>
    linkMe('GoodFaithEstimate.aspx');
  <% } %>
}

function prepaymentsEnableOptionsDDL()
{
	var selectedValue = <%= AspxTools.JsGetElementById(sMldsPpmtT) %>.value;
	
	//this is the first line
	var sMldsPpmtPeriodTB = <%= AspxTools.JsGetElementById(sMldsPpmtPeriod) %>;
	
	//this is the center
	var sMldsPpmtBaseTDDL = <%= AspxTools.JsGetElementById(sMldsPpmtBaseT) %>;
	var sMldsPpmtMonMaxTB = <%= AspxTools.JsGetElementById(sMldsPpmtMonMax) %>;
	var sMldsPpmtMaxAmtMB = <%= AspxTools.JsGetElementById(sMldsPpmtMaxAmt) %>;

	//this is the textbox
	var sMldsPpmtOtherDetailTB = <%= AspxTools.JsGetElementById(sMldsPpmtOtherDetail) %>;


	if(selectedValue == 0 || selectedValue == 3)
	{		
		sMldsPpmtPeriodTB.readOnly = true;
		sMldsPpmtPeriodTB.value = "";
		
		sMldsPpmtMonMaxTB.readOnly = true;
		sMldsPpmtMaxAmtMB.readOnly = true;
		sMldsPpmtMonMaxTB.value = "0";
		sMldsPpmtMaxAmtMB.value = "$0.00";
		disableDDL(sMldsPpmtBaseTDDL, true);
			
		sMldsPpmtOtherDetailTB.readOnly = true;
		sMldsPpmtOtherDetailTB.value = "";

	
	}
	else if(selectedValue == 1)
	{
		sMldsPpmtPeriodTB.readOnly = false;
	
		sMldsPpmtMonMaxTB.readOnly = true;
		sMldsPpmtMaxAmtMB.readOnly = true;
		sMldsPpmtMonMaxTB.value = "0";
		sMldsPpmtMaxAmtMB.value = "$0.00";
		disableDDL(sMldsPpmtBaseTDDL, true);

		sMldsPpmtOtherDetailTB.readOnly = false;
	}
	else if(selectedValue == 2)
	{
		sMldsPpmtPeriodTB.readOnly = false;
	
		sMldsPpmtMonMaxTB.readOnly = false;
		sMldsPpmtMaxAmtMB.readOnly = false;
		disableDDL(sMldsPpmtBaseTDDL, false);
		
		sMldsPpmtOtherDetailTB.readOnly = true;
		sMldsPpmtOtherDetailTB.value = "";

	}
	
}

function enableOtherTextBox()
{
	var sMldsImpoundIncludeOtherCB = <%= AspxTools.JsGetElementById(sMldsImpoundIncludeOther) %>;
	var sMldsImpoundOtherDescTB = <%= AspxTools.JsGetElementById(sMldsImpoundOtherDesc) %>;
	
	if(sMldsImpoundIncludeOtherCB.checked == true)
	{
		sMldsImpoundOtherDescTB.readOnly = false;
	}
	else
	{
		sMldsImpoundOtherDescTB.value = "";
		sMldsImpoundOtherDescTB.readOnly = true;
	}
}

enableOtherTextBox();
prepaymentsEnableOptionsDDL();

//-->
</SCRIPT>

