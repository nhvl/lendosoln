﻿namespace LendersOfficeApp.newlos.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Web.Services;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Constants;
    using LendersOffice.Rolodex;
    using LendersOffice.Security;

    [DataContract]
    public class SellerClosingCostsView
    {
        public SellerClosingCostsView()
        {
            VRoot = Tools.VRoot;
        }

        [DataMember]
        public E_sClosingCostFeeVersionT sClosingCostFeeVersionT { get; set; }

        [DataMember]
        public decimal TotalAmount
        {
            get
            {
                decimal total = 0;

                if (SectionList != null)
                {
                    foreach (var section in SectionList)
                    {
                        if (section.FilteredClosingCostFeeList != null)
                        {
                            foreach (SellerClosingCostFee fee in section.FilteredClosingCostFeeList)
                            {
                                total += fee.TotalAmount;
                            }
                        }
                    }

                }
                return total;
            }
            private set { }
        }

        [DataMember]
        public IEnumerable<SellerClosingCostFeeSection> SectionList { get; set; }

        [DataMember]
        public bool sIsRequireFeesFromDropDown { get; set; }

        [DataMember]
        public bool sIsManuallySetThirdPartyAffiliateProps { get; set; }

        [DataMember]
        public string VRoot
        {
            get;
            set;
        }


        [DataMember]
        public bool IsArchive { get; set; }

        [DataMember]
        public bool IsReadonly { get; set; }

        [DataMember]
        public string sConsummationD { get; set; }

        [DataMember]
        public bool sIPiaDyLckd { get; set; }
        [DataMember]
        public string sIPiaDy { get; set; }
        [DataMember]
        public bool sIPerDayLckd { get; set; }
        [DataMember]
        public string sIPerDay { get; set; }
        [DataMember]
        public bool CanReadDFLP { get; set; }
        [DataMember]
        public bool CanSetDFLP { get; set; }

        [DataMember]
        public string sLastDisclosedGFEArchiveId { get; set; }
    }

    public partial class SellerClosingCosts : BaseLoanPage
    {        
        protected bool IsArchivePage
        {
            get
            {
                return Broker.IsGFEandCoCVersioningEnabled
                    && RequestHelper.GetSafeQueryString("IsArchivePage") == "true" 
                    && !IsLeadPage;
            }
        }

        public override bool IsReadOnly
        {
            get
            {
                return IsArchivePage || base.IsReadOnly;
            }
        }


        protected override void OnInit(EventArgs e)
        {
            RegisterService("cfpb_utils", "/newlos/Test/RActiveGFEService.aspx");

            base.OnInit(e);
            if (IsArchivePage)
            {
                this.PageID = "SellerClosingCostsArchive";
            }
            else
            {
                this.PageID = "SellerClosingCosts";
            }
        }

        private IEnumerable<KeyValuePair<string, string>> GetGFESectionValues()
        {
            List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();
            // items.Add(new KeyValuePair<string, string>(E_GfeSectionT.LeaveBlank.ToString("d"), ""));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B1.ToString("d"), "A1"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B2.ToString("d"), "A2"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B3.ToString("d"), "A3"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B4.ToString("d"), "B4"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B5.ToString("d"), "B5"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B6.ToString("d"), "B6"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B7.ToString("d"), "B7"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B8.ToString("d"), "B8"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B9.ToString("d"), "B9"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B10.ToString("d"), "B10"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B11.ToString("d"), "B11"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.NotApplicable.ToString("d"), "N/A"));
            return items;
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        private void BindGfeSectionDropDown(Repeater repeater)
        {
            List<ListItem> items = new List<ListItem>();
            items.Add(Tools.CreateEnumListItem("A1", E_GfeSectionT.B1));
            items.Add(Tools.CreateEnumListItem("A2", E_GfeSectionT.B2));
            items.Add(Tools.CreateEnumListItem("B3", E_GfeSectionT.B3));
            items.Add(Tools.CreateEnumListItem("B4", E_GfeSectionT.B4));
            items.Add(Tools.CreateEnumListItem("B5", E_GfeSectionT.B5));
            items.Add(Tools.CreateEnumListItem("B6", E_GfeSectionT.B6));
            items.Add(Tools.CreateEnumListItem("B7", E_GfeSectionT.B7));
            items.Add(Tools.CreateEnumListItem("B8", E_GfeSectionT.B8));
            items.Add(Tools.CreateEnumListItem("B9", E_GfeSectionT.B9));
            items.Add(Tools.CreateEnumListItem("B10", E_GfeSectionT.B10));
            items.Add(Tools.CreateEnumListItem("B11", E_GfeSectionT.B11));
            items.Add(Tools.CreateEnumListItem("N/A", E_GfeSectionT.NotApplicable));
            SectionRepeater.DataSource = items;
            SectionRepeater.DataBind();
        }

        private void BindLoanEstimateSectionDropDown(HtmlSelect ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("A", E_IntegratedDisclosureSectionT.SectionA));
            ddl.Items.Add(Tools.CreateEnumListItem("B", E_IntegratedDisclosureSectionT.SectionB));
            ddl.Items.Add(Tools.CreateEnumListItem("C", E_IntegratedDisclosureSectionT.SectionC));
            ddl.Items.Add(Tools.CreateEnumListItem("E", E_IntegratedDisclosureSectionT.SectionE));
            ddl.Items.Add(Tools.CreateEnumListItem("F", E_IntegratedDisclosureSectionT.SectionF));
            ddl.Items.Add(Tools.CreateEnumListItem("G", E_IntegratedDisclosureSectionT.SectionG));
            ddl.Items.Add(Tools.CreateEnumListItem("H", E_IntegratedDisclosureSectionT.SectionH));
        }

        private void BindBeneficiaryDropDown(Repeater repeater)
        {
            DropDownList dl = new DropDownList();
            RolodexDB.PopulateAgentTypeDropDownList(dl);
            repeater.DataSource = dl.Items;
            repeater.DataBind();
        }

        public void BindPercentRepeaters()
        {
            List<ListItem> Items = new List<ListItem>();

            Items.Add(Tools.CreateEnumListItem("Loan Amount", E_PercentBaseT.LoanAmount));
            Items.Add(Tools.CreateEnumListItem("Purchase Price", E_PercentBaseT.SalesPrice));
            Items.Add(Tools.CreateEnumListItem("Appraisal Value", E_PercentBaseT.AppraisalValue));
            Items.Add(Tools.CreateEnumListItem("Total Loan Amount", E_PercentBaseT.TotalLoanAmount));

            PercentRepeater.DataSource = Items;
            PercentRepeater.DataBind();

            PercentRepeater2.DataSource = Items;
            PercentRepeater2.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Need to do it this way so I dont have the ID element...
            BindGfeSectionDropDown(SectionRepeater);
            BindBeneficiaryDropDown(BeneficiaryRepeater);

            EnableJqueryMigrate = false;
            
            RegisterJsScript("ThirdParty/ractive-legacy.min.js");
            RegisterJsScript("mask.js");

            RegisterCSS("stylesheetnew.css");
            RegisterCSS("GFE2015.css");

            BindPercentRepeaters();

            Response.Headers.Add("X-UA-Compatible", "IE=edge");
            RegisterJsScript("LQBPopup.js");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("Ractive-Common.js");

            CPageData dataLoan;

            if (IsArchivePage)
            {
                dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SellerClosingCosts));
                dataLoan.InitLoad();
                if (dataLoan.sClosingCostArchive.Count != 0)
                {
                    var tridTargetVersion = dataLoan.sTridTargetRegulationVersionT;
                    var latestArchive = dataLoan.sClosingCostArchive.First(a => ClosingCostArchive.IsDisclosedDataArchive(tridTargetVersion, a));
                    dataLoan.ApplyClosingCostArchive(latestArchive);
                }
                else
                {
                    throw new CBaseException("Archive not found", "Archive not found");
                }
            }
            else
            {
                dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SellerClosingCosts));
                dataLoan.InitLoad();
            }


            SellerClosingCostsView viewModel = new SellerClosingCostsView();

            viewModel.sClosingCostFeeVersionT = dataLoan.sClosingCostFeeVersionT;
            viewModel.sIPiaDyLckd = dataLoan.sIPiaDyLckd;
            viewModel.sIPiaDy = dataLoan.sIPiaDy_rep;
            viewModel.sIPerDayLckd = dataLoan.sIPerDayLckd;
            viewModel.sIPerDay = dataLoan.sIPerDay_rep;


            viewModel.SectionList = dataLoan.sSellerResponsibleClosingCostSet.GetViewForSerialization(E_ClosingCostViewT.SellerResponsibleLoanHud1);
            viewModel.sIsRequireFeesFromDropDown = dataLoan.sIsRequireFeesFromDropDown;
            viewModel.sIsManuallySetThirdPartyAffiliateProps = dataLoan.sIsManuallySetThirdPartyAffiliateProps;


            viewModel.sConsummationD = dataLoan.sConsummationD_rep;
            viewModel.IsArchive = IsArchivePage;
            viewModel.IsReadonly = IsReadOnly;

            viewModel.CanReadDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserRead);
            viewModel.CanSetDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserWrite);
            RegisterJsObject("ClosingCostData", viewModel);
        }

        private static void BindToDataLoan(CPageData dataLoan, SellerClosingCostsView viewModel)
        {
            dataLoan.sSellerResponsibleClosingCostSet.UpdateWith((IEnumerable<BaseClosingCostFeeSection>)viewModel.SectionList);
            dataLoan.sIPiaDyLckd = viewModel.sIPiaDyLckd;
            dataLoan.sIPiaDy_rep = viewModel.sIPiaDy;
            dataLoan.sIPerDayLckd = viewModel.sIPerDayLckd;
            dataLoan.sIPerDay_rep = viewModel.sIPerDay;
            if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowEnablingCustomFeeDescriptions))
            {
                dataLoan.sIsRequireFeesFromDropDown = viewModel.sIsRequireFeesFromDropDown;
            }
            dataLoan.sIsManuallySetThirdPartyAffiliateProps = viewModel.sIsManuallySetThirdPartyAffiliateProps;
        }

        private static string CreateViewModel(CPageData dataLoan, bool isArchivePage, string sLastDisclosedGFEArchiveId)
        {

            if (isArchivePage)
            {
                sLastDisclosedGFEArchiveId = sLastDisclosedGFEArchiveId  ?? dataLoan.sLastDisclosedGFEArchiveId.ToString();
                if (dataLoan.sClosingCostArchive.Count != 0)
                {
                    dataLoan.ApplyClosingCostArchive(new Guid(sLastDisclosedGFEArchiveId));
                }
                else
                {
                    throw new CBaseException("Archive not found", "Archive not found");
                }
            }

            SellerClosingCostsView viewModel = new SellerClosingCostsView();

            viewModel.SectionList = dataLoan.sSellerResponsibleClosingCostSet.GetViewForSerialization(E_ClosingCostViewT.SellerResponsibleLoanHud1);

            viewModel.IsArchive = isArchivePage;
            viewModel.sClosingCostFeeVersionT = dataLoan.sClosingCostFeeVersionT;
            viewModel.sIPiaDyLckd = dataLoan.sIPiaDyLckd;
            viewModel.sIPiaDy = dataLoan.sIPiaDy_rep;
            viewModel.sIPerDayLckd = dataLoan.sIPerDayLckd;
            viewModel.sIPerDay = dataLoan.sIPerDay_rep;
            viewModel.sIsRequireFeesFromDropDown = dataLoan.sIsRequireFeesFromDropDown;
            viewModel.sIsManuallySetThirdPartyAffiliateProps = dataLoan.sIsManuallySetThirdPartyAffiliateProps;
            viewModel.sConsummationD = dataLoan.sConsummationD_rep;
            viewModel.CanReadDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserRead);
            viewModel.CanSetDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserWrite);

            return ObsoleteSerializationHelper.JsonSerialize(viewModel);
        }

        [WebMethod]
        public static string GetAvailableClosingCostFeeTypes(Guid loanId, string viewModelJson, string sectionName)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(SellerClosingCosts));
                dataLoan.InitLoad();

                // 4/1/2015 dd - The ClosingCostFeeSection get modify in memory when calling section.ClosingCostFeeList.
                // To discard the BrokerDB from cache we will force to reload from database each time.
                BrokerDB db = BrokerDB.RetrieveByIdForceRefresh(PrincipalFactory.CurrentPrincipal.BrokerId);
                FeeSetupClosingCostSet closingCostSet = db.GetUnlinkedClosingCostSet();
                FeeSetupClosingCostFeeSection section = (FeeSetupClosingCostFeeSection)closingCostSet.GetSection(E_ClosingCostViewT.LenderTypeHud1, sectionName);

                SellerClosingCostsView viewModel = ObsoleteSerializationHelper.JsonDeserialize<SellerClosingCostsView>(viewModelJson);

                List<BaseClosingCostFee> fees = new List<BaseClosingCostFee>();

                foreach (SellerClosingCostFeeSection closingSection in viewModel.SectionList)
                {
                    foreach (SellerClosingCostFee fee in closingSection.FilteredClosingCostFeeList)
                    {
                        fees.Add(fee.ConvertToFeeSetupFee());
                    }
                }

                section.SetExcludeFeeList(fees, dataLoan.sClosingCostFeeVersionT, o => o.CanManuallyAddToEditor == false);

                return ObsoleteSerializationHelper.JsonSerialize(section);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string CalculateData(Guid loanId, string viewModelJson)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(SellerClosingCosts));
                dataLoan.InitLoad();

                SellerClosingCostsView viewModel = ObsoleteSerializationHelper.JsonDeserialize<SellerClosingCostsView>(viewModelJson);
                BindToDataLoan(dataLoan, viewModel);

                return CreateViewModel(dataLoan, viewModel.IsArchive, viewModel.sLastDisclosedGFEArchiveId);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string Save(Guid loanId, string viewModelJson)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(SellerClosingCosts));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                SellerClosingCostsView viewModel = ObsoleteSerializationHelper.JsonDeserialize<SellerClosingCostsView>(viewModelJson);
                BindToDataLoan(dataLoan,viewModel);

                try
                {
                    dataLoan.Save();
                }
                catch (CBaseException e)
                {
                    return SerializationHelper.JsonNetAnonymousSerialize(new
                    {
                        Status = "Error",
                        UserMessage = e.UserMessage,
                        ErrorType = e.GetType().Name
                    });
                }
                return CreateViewModel(dataLoan, viewModel.IsArchive, viewModel.sLastDisclosedGFEArchiveId);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string CreateAgent(Guid loanId, Guid id, bool populateFromRolodex, E_AgentRoleT agentType)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(SellerClosingCosts));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                CAgentFields agent = dataLoan.GetAgentFields(Guid.Empty);
                if (populateFromRolodex)
                {
                    agent.PopulateFromRolodex(id, BrokerUserPrincipal.CurrentPrincipal);
                }
                else
                {
                    CommonFunctions.CopyEmployeeInfoToAgent(PrincipalFactory.CurrentPrincipal.BrokerId, agent, id);
                }

                agent.AgentRoleT = agentType;
                agent.Update();

                dataLoan.Save();

                return "{\"RecordId\": \"" + agent.RecordId.ToString() + "\"}";
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string ClearAgent(Guid loanId, string viewModelJson, Guid recordId)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(SellerClosingCosts));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                // Bind unsaved data to loan
                SellerClosingCostsView viewModel = ObsoleteSerializationHelper.JsonDeserialize<SellerClosingCostsView>(viewModelJson);
                BindToDataLoan(dataLoan, viewModel);

                DataSet ds = dataLoan.sAgentDataSet;
                DataTable table = ds.Tables[0];

                // Find agent to be deleted.
                DataRow toBeDeleted = null;
                foreach (DataRow row in table.Rows)
                {
                    if ((string)row["recordId"] == recordId.ToString())
                    {
                        // I could not invoke the Delete() on row object here.
                        // Doing so will modify the collection of table.Rows which cause
                        // the exception to be throw. dd 4/21/2003
                        toBeDeleted = row;
                        break;
                    }
                }

                // If agent found then delete and save. Otherwise do nothing.
                if (toBeDeleted != null)
                {
                    dataLoan.RecordAuditOnSave(AgentRecordChangeAuditHelper.CreateAuditEvent(toBeDeleted, dataLoan.sSpState, AgentRecordChangeType.DeleteRecord));

                    // OPM 209868 - Clear beneficiary info in any closing cost fee where this agent is the beneficiary.
                    // If we have access to this page/method we can assume that the loan has been migrated.
                    dataLoan.sSellerResponsibleClosingCostSet.ClearBeneficiary(new Guid((string)toBeDeleted["recordId"]));
                    toBeDeleted.Delete();
                }

                dataLoan.sAgentDataSet = ds;
                dataLoan.Save();

                // Return deserialized data set.
                return CreateViewModel(dataLoan, viewModel.IsArchive, viewModel.sLastDisclosedGFEArchiveId);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }
    }
}
