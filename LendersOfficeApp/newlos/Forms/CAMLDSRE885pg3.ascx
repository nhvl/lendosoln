﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CAMLDSRE885pg3.ascx.cs" Inherits="LendersOfficeApp.newlos.Forms.CAMLDSRE885pg3" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>

<table class="InsetBorder" id="Table1" cellSpacing="0" cellPadding="0" width="99%" border="0">
    <TR>
		<TD class="FormTableSubheader" noWrap>XVI. Other Liens Currently on this Property (for which the borrower is obligated)</TD>
    </TR>
	<TR>
		<TD noWrap>
		    <TABLE id="Table2" cellSpacing="0" cellPadding="0" border="0">
			    <TR>
				    <TD class="FieldLabel">Lienholder's Name</TD>
				    <TD class="FieldLabel">Amount Owing</TD>
				    <TD class="FieldLabel">Priority</TD>
			    </TR>
			    <TR>
				    <TD><asp:TextBox id="sLienholder1NmBefore" runat="server" MaxLength="36" Width="232px"></asp:TextBox></TD>
				    <TD><ml:MoneyTextBox id="sLien1AmtBefore" preset="money" width="120" runat="server"></ml:MoneyTextBox></TD>
				    <TD><ml:ComboBox id="sLien1PriorityBefore" runat="server" Width="100px"></ml:ComboBox></TD>
			    </TR>
			    <TR>
				    <TD><asp:TextBox id="sLienholder2NmBefore" runat="server" MaxLength="36" Width="232px"></asp:TextBox></TD>
				    <TD><ml:MoneyTextBox id="sLien2AmtBefore" preset="money" width="120" runat="server"></ml:MoneyTextBox></TD>
				    <TD><ml:ComboBox id="sLien2PriorityBefore" runat="server" Width="100px"></ml:ComboBox></TD>
			    </TR>
			    <TR>
				    <TD><asp:TextBox id="sLienholder3NmBefore" runat="server" MaxLength="36" Width="232px"></asp:TextBox></TD>
				    <TD><ml:MoneyTextBox id="sLien3AmtBefore" preset="money" width="120" runat="server"></ml:MoneyTextBox></TD>
				    <TD><ml:ComboBox id="sLien3PriorityBefore" runat="server" Width="100px"></ml:ComboBox></TD>
			    </TR>
		    </TABLE>
	    </TD>
    </TR>
</table>
<TABLE class="InsetBorder" id="Table11" cellSpacing="0" cellPadding="0" width="99%" border="0">
	<TR>
		<TD class="FormTableSubheader" noWrap>Liens that may Remain on this Property after the Proposed Loan</TD></TR>
	<TR>
		<TD noWrap>
			<table id="Table6" cellSpacing="0" cellPadding="0" border="0">
				<TR>
					<TD class="FieldLabel">Lienholder's Name</TD>
					<TD class="FieldLabel">Amount Owing</TD>
					<TD class="FieldLabel">Priority</TD>
				</TR>
				<TR>
					<TD><asp:TextBox id="sLienholder1NmAfter" runat="server" MaxLength="36" Width="232"></asp:TextBox></TD>
					<TD><ml:MoneyTextBox id="sLien1AmtAfter" preset="money" width="120" runat="server"></ml:MoneyTextBox></TD>
					<TD><ml:ComboBox id="sLien1PriorityAfter" runat="server" Width="100px"></ml:ComboBox></TD>
				</TR>
				<TR>
					<TD><asp:TextBox id="sLienholder2NmAfter" runat="server" MaxLength="36" Width="232px"></asp:TextBox></TD>
					<TD><ml:MoneyTextBox id="sLien2AmtAfter" preset="money" width="120" runat="server"></ml:MoneyTextBox></TD>
					<TD><ml:ComboBox id="sLien2PriorityAfter" runat="server" Width="100px"></ml:ComboBox></TD>
				</TR>
				<TR>
					<TD><asp:TextBox id="sLienholder3NmAfter" runat="server" MaxLength="36" Width="232px"></asp:TextBox></TD>
					<TD><ml:MoneyTextBox id="sLien3AmtAfter" preset="money" width="120" runat="server"></ml:MoneyTextBox></TD>
					<TD><ml:ComboBox id="sLien3PriorityAfter" runat="server" Width="100px"></ml:ComboBox></TD>
				</TR>
			</table>
		</TD>
	</TR>
</TABLE>
<TABLE id="Table7" cellSpacing="0" cellPadding="0" border="0">
    <TR>
	    <TD class="FieldLabel" colspan="2">XVII.&nbsp; Article 7 Compliance</TD>
    </TR>
    <TR>
	    <TD style="padding-left:20px;" colspan="2">This loan
		    <asp:DropDownList id="sBrokControlledFundT" runat="server">
			    <asp:ListItem Value="0" Selected="True">leave blank</asp:ListItem>
			    <asp:ListItem Value="1">may</asp:ListItem>
			    <asp:ListItem Value="2">will</asp:ListItem>
			    <asp:ListItem Value="3">will not</asp:ListItem>
		    </asp:DropDownList>&nbsp;be made wholly or in part from broker-controlled funds ...
		</TD>
    </TR>
    <tr>
        <td class="FieldLabel">
            XVIII. This loan is based on limited or no documentation of your income ...
        </td>
        <td class="FieldLabel"><asp:CheckBoxList id=sMldsIsNoDocTri runat="server" RepeatDirection="Horizontal"></asp:CheckBoxList></td>
    </tr>
</TABLE>
            