namespace LendersOfficeApp.newlos.Forms
{
    using System;
    using System.Web.UI.HtmlControls;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Summary description for LoanSubmission.
    /// </summary>
    public partial class LoanSubmission : BaseLoanPage
	{
        #region "Member variables"
        //01-18-08 opm 18913 Changing county text to ddl
        protected System.Web.UI.WebControls.CheckBoxList sSubmitDocCBL;
        protected System.Web.UI.WebControls.CheckBoxList sSubmitImpoundCBL;
        #endregion 

        protected HtmlControl LabelForsRLckdNumOfDays
        {
            get
            {
                if (this.Broker.HasLenderDefaultFeatures)
                {
                    var a = new HtmlAnchor()
                    {
                        HRef = "javascript:linkMe('../LockDesk/BrokerRateLock.aspx');",
                        Title = "Go to Rate Lock page",
                        InnerText = "# of days"
                    };
                    a.Attributes.Add("tabindex", "-1");
                    return a;
                }

                return new HtmlGenericControl("span") { InnerText = "# of days" };
            }
        }

        protected HtmlControl LabelForsRLckdExpiredD
        {
            get
            {
                if (Broker.HasLenderDefaultFeatures)
                {
                    var a = new HtmlAnchor()
                    {
                        HRef = "javascript:linkMe('../LockDesk/BrokerRateLock.aspx');",
                        Title = "Go to Rate Lock page",
                        InnerText = "Expires"
                    };
                    a.Attributes.Add("tabindex", "-1");
                    return a;
                }

                return new HtmlGenericControl("span") { InnerText = "Expires" };
            }
        }

        internal static void BindDataFromControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem item)
        {
            QualRateCalculationPopup.BindData(loan, item);
        }

        internal static void LoadDataForControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem item)
        {
            QualRateCalculationPopup.LoadData(loan, item);
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            Tools.Bind_aOccT(aOccT);
            Tools.Bind_sLienPosT(sLienPosT);
            Tools.Bind_sLPurposeT(sLPurposeT);

            this.PageTitle = "Loan Submissions";
            this.PageID = "LoanSubmissions";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CLoanSubmissionPDF);

            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterCSS("jquery-ui-1.11.custom.css");

            sAgentLenderZip.SmartZipcode(sAgentLenderCity, sAgentLenderState);
            brokerZipcode.SmartZipcode(brokerCity, brokerState);
            appraisalZipcode.SmartZipcode(appraisalCity, appraisalState);
            escrowZipcode.SmartZipcode(escrowCity, escrowState);
            titleZipcode.SmartZipcode(titleCity, titleState);
			
			//01/18/08 av opm 18913 
			sSpZip.SmartZipcode( this.sSpCity, this.sSpState, this.sSpCounty );


			sSpState.Attributes.Add( "onchange", "javascript:UpdateCounties(this,document.getElementById('" + sSpCounty.ClientID + "'), event);" );

            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);

            Tools.Bind_QualTermCalculationType(sQualTermCalculationType);
        }

        private void BindDataObject(CPageData dataLoan) 
        {
			CAppData dataApp = dataLoan.GetAppData(0);

            this.QualRatePopup.LoadData(dataLoan);

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Processor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            
			processorName.Text = agent.AgentName;

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            loanOfficerPhone.Text		= agent.Phone;
            loanOfficerName.Text		= agent.AgentName;
			brokerCompanyName.Text		= agent.CompanyName;
			brokerAddress.Text			= agent.StreetAddr;
			brokerCity.Text				= agent.City;
			brokerState.Value			= agent.State;			
			brokerZipcode.Text			= agent.Zip;
			
			sSubmitBrokerFax.Text		= agent.FaxNum;

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            appraisalOfficer.Text = agent.AgentName;
            appraisalCompanyName.Text = agent.CompanyName;
            appraisalAddress.Text = agent.StreetAddr;
            appraisalCity.Text = agent.City;
            appraisalState.Value = agent.State;
            appraisalZipcode.Text = agent.Zip;
            appraisalPhone.Text = agent.Phone;
            appraisalFax.Text = agent.FaxNum;
            appraisalLicense.Text = agent.LicenseNumOfAgent;

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Escrow, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            escrowOfficer.Text = agent.AgentName;
            escrowCompanyName.Text = agent.CompanyName;
            escrowAddress.Text = agent.StreetAddr;
            escrowCity.Text = agent.City;
            escrowState.Value = agent.State;
            escrowZipcode.Text = agent.Zip;
            escrowPhone.Text = agent.Phone;
            escrowFax.Text = agent.FaxNum;
            escrowNumber.Text = agent.CaseNum;

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Title, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            titleOfficer.Text = agent.AgentName;
            titleCompanyName.Text = agent.CompanyName;
            titleAddress.Text = agent.StreetAddr;
            titleCity.Text = agent.City;
            titleState.Value = agent.State;
            titleZipcode.Text = agent.Zip;
            titlePhone.Text = agent.Phone;
            titleFax.Text = agent.FaxNum;
            titleNumber.Text = agent.CaseNum;

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject); 
            sAgentLenderAgentName.Text = agent.AgentName;
            sAgentLenderCompanyName.Text = agent.CompanyName;
            sAgentLenderAddress.Text = agent.StreetAddr;
            sAgentLenderCity.Text = agent.City;
            sAgentLenderState.Value = agent.State;
            sAgentLenderZip.Text = agent.Zip;
            sAgentLenderAgentPhone.Text = agent.Phone;
            sAgentLenderAgentFax.Text = agent.FaxNum;

            aBFirstNm.Text = dataApp.aBFirstNm;
            aBMidNm.Text = dataApp.aBMidNm;
            aBLastNm.Text = dataApp.aBLastNm;
            aBSuffix.Text = dataApp.aBSuffix;
            aBSSN.Text = dataApp.aBSsn;
            aCFirstNm.Text = dataApp.aCFirstNm;
            aCMidNm.Text = dataApp.aCMidNm;
            aCLastNm.Text = dataApp.aCLastNm;
            aCSuffix.Text = dataApp.aCSuffix;
            aCSSN.Text = dataApp.aCSsn;
            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Text = dataLoan.sSpZip;

			//01-18-07 av 18913 Needed in order to populate the county ddl 
			Tools.Bind_sSpCounty( dataLoan.sSpState, sSpCounty, true );
			Tools.SetDropDownListCaseInsensitive( sSpCounty, dataLoan.sSpCounty );
	
            
			sLpTemplateNm.Text = dataLoan.sLpTemplateNm;
			// 5/29/07 mf. OPM 12061. There is now a corporate setting for editing
			// the loan program name.
            if (! BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowEditingLoanProgramName))
				sLpTemplateNm.ReadOnly = true;

            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sQualIR.Text = dataLoan.sQualIR_rep;
            this.sQualIRLckd.Checked = dataLoan.sQualIRLckd;
            sTerm.Text = dataLoan.sTerm_rep;
            sTerm.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
			sDue.Text = dataLoan.sDue_rep;
			sDue.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sPurchPrice.Text = dataLoan.sPurchPrice_rep;
            sApprVal.Text = dataLoan.sApprVal_rep;
            sLtvR.Text = dataLoan.sLtvR_rep;
            sCltvR.Text = dataLoan.sCltvR_rep;
            sEquity.Text = dataLoan.sEquityCalc_rep;
            sRAdjCapMon.Text = dataLoan.sRAdjCapMon_rep;
            sRAdjCapR.Text = dataLoan.sRAdjCapR_rep;
            sRAdjIndexR.Text = dataLoan.sRAdjIndexR_rep;
			sRAdjIndexR.ReadOnly = dataLoan.sRAdjWorstIndex || IsReadOnly; // OPM 11220
            sRAdjLifeCapR.Text = dataLoan.sRAdjLifeCapR_rep;
            sRAdjMarginR.Text = dataLoan.sRAdjMarginR_rep;
            sRAdjMarginR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;

            sSubmitRAdjustOtherDesc.Text = dataLoan.sSubmitRAdjustOtherDesc;
            sSubmitInitPmtCapR.Text = dataLoan.sSubmitInitPmtCapR_rep;
            Tools.SetDropDownListValue(sLienPosT, dataLoan.sLienPosT);
            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            Tools.SetDropDownListValue(aOccT, dataApp.aOccT);
            sUnitsNum.Text = dataLoan.sUnitsNum_rep;
            sLOrigF.Text = dataLoan.sLOrigF_rep;
            sLDiscnt.Text = dataLoan.sLDiscnt_rep;
            sApprF.Text = dataLoan.sApprF_rep;
            sCrF.Text = dataLoan.sCrF_rep;
            sProcF.Text = dataLoan.sProcF_rep;
            sSubmitDocFull.Checked = dataLoan.sSubmitDocFull;
            sSubmitDocOther.Checked = dataLoan.sSubmitDocOther;

            sSubmitProgramCode.Text = dataLoan.sSubmitProgramCode;
            sSubmitImpoundOtherDesc.Text = dataLoan.sSubmitImpoundOtherDesc;
            sSubmitImpoundTaxes.Checked = dataLoan.sSubmitImpoundTaxes;
            sSubmitImpoundHazard.Checked = dataLoan.sSubmitImpoundHazard;
            sSubmitImpoundMI.Checked = dataLoan.sSubmitImpoundMI;
            sSubmitImpoundFlood.Checked = dataLoan.sSubmitImpoundFlood;
            sSubmitImpoundOther.Checked = dataLoan.sSubmitImpoundOther;

            sSubmitBuydownDesc.Text = dataLoan.sSubmitBuydownDesc;
            sSubmitAmortDesc.Text = dataLoan.sSubmitAmortDesc;
            sSubmitMIYes.Items[0].Selected = dataLoan.sSubmitMIYes;
            sSubmitMIYes.Items[1].Selected = !dataLoan.sSubmitMIYes;
            sSubmitMITypeDesc.Text = dataLoan.sSubmitMITypeDesc;
            
			//fs opm 9135 06/05/08
			bool bRateLocked = Broker.HasLenderDefaultFeatures ? dataLoan.sIsRateLocked : dataLoan.sRLckdIsLocked;
			sRLckdIsLockedRB.Checked = bRateLocked;
			sRFloatRB.Checked = !bRateLocked;
			sRFloatRB.Enabled = !Broker.HasLenderDefaultFeatures;
			sRLckdIsLockedRB.Enabled = !Broker.HasLenderDefaultFeatures;
			sRLckdNumOfDays.Text = Broker.HasLenderDefaultFeatures ? dataLoan.sRLckdDays_rep : dataLoan.sRLckdNumOfDays_rep;
			//end fs

			// 05/26/06 mf - We do not allow brokers with lender default features
			// to edit the rate lock date on the loan submission form.  We also
			// allow show links to the RateLock page in that case to guide them.

			sRLckdIsLockedRBText.Text = (Broker.HasLenderDefaultFeatures) ? @"<a href=""javascript:linkMe('../LockDesk/BrokerRateLock.aspx');"" tabindex=-1 title=""Go to Rate Lock page"">Locked On:</a>" : "Locked on:";
			sRLckdNumOfDays.ReadOnly = Broker.HasLenderDefaultFeatures || sRLckdNumOfDays.ReadOnly;
            sRLckdExpiredD.Text = dataLoan.sRLckdExpiredD_rep ;
            sRLckdExpiredD.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly || Broker.HasLenderDefaultFeatures;
			sRLckdExpiredD.IsDisplayCalendarHelper = !sRLckdExpiredD.ReadOnly;
			if ( ! sRLckdExpiredD.ReadOnly )
				sRLckdExpiredD.ToolTip = "Hint:  Enter 't' for today's date.";
            sRLckdD.Text = dataLoan.sRLckdD_rep;
            sRLckdD.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly || Broker.HasLenderDefaultFeatures;
			sRLckdD.IsDisplayCalendarHelper = !sRLckdD.ReadOnly;
			if ( ! sRLckdD.ReadOnly )
				sRLckdD.ToolTip = "Hint:  Enter 't' for today's date.";

            sDemandU1LenderDesc.Text = dataLoan.sDemandU1LenderDesc;
            sDemandU1LenderAmt.Text = dataLoan.sDemandU1LenderAmt_rep;
            sDemandU1BrokerPaidAmt.Text = dataLoan.sDemandU1BrokerPaidAmt_rep;
            sDemandU1BrokerDueAmt.Text = dataLoan.sDemandU1BrokerDueAmt_rep;
            sDemandU1BorrowerAmt.Text = dataLoan.sDemandU1BorrowerAmt_rep;

            sDemandU2LenderDesc.Text = dataLoan.sDemandU2LenderDesc;
            sDemandU2LenderAmt.Text = dataLoan.sDemandU2LenderAmt_rep;
            sDemandU2BrokerPaidAmt.Text = dataLoan.sDemandU2BrokerPaidAmt_rep;
            sDemandU2BrokerDueAmt.Text = dataLoan.sDemandU2BrokerDueAmt_rep;
            sDemandU2BorrowerAmt.Text = dataLoan.sDemandU2BorrowerAmt_rep;

            sDemandU3LenderDesc.Text = dataLoan.sDemandU3LenderDesc;
            sDemandU3LenderAmt.Text = dataLoan.sDemandU3LenderAmt_rep;
            sDemandU3BrokerPaidAmt.Text = dataLoan.sDemandU3BrokerPaidAmt_rep;
            sDemandU3BrokerDueAmt.Text = dataLoan.sDemandU3BrokerDueAmt_rep;
            sDemandU3BorrowerAmt.Text = dataLoan.sDemandU3BorrowerAmt_rep;

            sDemandU4LenderDesc.Text = dataLoan.sDemandU4LenderDesc;
            sDemandU4LenderAmt.Text = dataLoan.sDemandU4LenderAmt_rep;
            sDemandU4BrokerPaidAmt.Text = dataLoan.sDemandU4BrokerPaidAmt_rep;
            sDemandU4BrokerDueAmt.Text = dataLoan.sDemandU4BrokerDueAmt_rep;
            sDemandU4BorrowerAmt.Text = dataLoan.sDemandU4BorrowerAmt_rep;

            sDemandU5LenderDesc.Text = dataLoan.sDemandU5LenderDesc;
            sDemandU5LenderAmt.Text = dataLoan.sDemandU5LenderAmt_rep;
            sDemandU5BrokerPaidAmt.Text = dataLoan.sDemandU5BrokerPaidAmt_rep;
            sDemandU5BrokerDueAmt.Text = dataLoan.sDemandU5BrokerDueAmt_rep;
            sDemandU5BorrowerAmt.Text = dataLoan.sDemandU5BorrowerAmt_rep;

            sDemandU6LenderDesc.Text = dataLoan.sDemandU6LenderDesc;
            sDemandU6LenderAmt.Text = dataLoan.sDemandU6LenderAmt_rep;
            sDemandU6BrokerPaidAmt.Text = dataLoan.sDemandU6BrokerPaidAmt_rep;
            sDemandU6BrokerDueAmt.Text = dataLoan.sDemandU6BrokerDueAmt_rep;
            sDemandU6BorrowerAmt.Text = dataLoan.sDemandU6BorrowerAmt_rep;

            sDemandU7LenderDesc.Text = dataLoan.sDemandU7LenderDesc;
            sDemandU7LenderAmt.Text = dataLoan.sDemandU7LenderAmt_rep;
            sDemandU7BrokerPaidAmt.Text = dataLoan.sDemandU7BrokerPaidAmt_rep;
            sDemandU7BrokerDueAmt.Text = dataLoan.sDemandU7BrokerDueAmt_rep;
            sDemandU7BorrowerAmt.Text = dataLoan.sDemandU7BorrowerAmt_rep;
            sHmdaCensusTract.Text = dataLoan.sHmdaCensusTract;

            sDemandApprFeeLenderAmt.Text = dataLoan.sDemandApprFeeLenderAmt_rep;
            sDemandApprFeeBrokerPaidAmt.Text = dataLoan.sDemandApprFeeBrokerPaidAmt_rep;
            sDemandApprFeeBrokerDueAmt.Text = dataLoan.sDemandApprFeeBrokerDueAmt_rep;
            sDemandCreditReportFeeLenderAmt.Text = dataLoan.sDemandCreditReportFeeLenderAmt_rep;
            sDemandCreditReportFeeBrokerPaidAmt.Text = dataLoan.sDemandCreditReportFeeBrokerPaidAmt_rep;
            
            sDemandCreditReportFeeBrokerDueAmt.Text = dataLoan.sDemandCreditReportFeeBrokerDueAmt_rep;
            sDemandProcessingFeeLenderAmt.Text = dataLoan.sDemandProcessingFeeLenderAmt_rep;
            sDemandProcessingFeeBrokerPaidAmt.Text = dataLoan.sDemandProcessingFeeBrokerPaidAmt_rep;
            sDemandProcessingFeeBrokerDueAmt.Text = dataLoan.sDemandProcessingFeeBrokerDueAmt_rep;
            sDemandLoanDocFeeLenderAmt.Text = dataLoan.sDemandLoanDocFeeLenderAmt_rep;
            sDemandLoanDocFeeBrokerPaidAmt.Text = dataLoan.sDemandLoanDocFeeBrokerPaidAmt_rep;
            sDemandLoanDocFeeBorrowerAmt.Text = dataLoan.sDemandLoanDocFeeBorrowerAmt_rep;
            sDemandLoanDocFeeBrokerDueAmt.Text = dataLoan.sDemandLoanDocFeeBrokerDueAmt_rep;
            
            sDemandTotalLenderAmt.Text = dataLoan.sDemandTotalLenderAmt_rep;
            sDemandTotalBrokerPaidAmt.Text = dataLoan.sDemandTotalBrokerPaidAmt_rep;
            sDemandTotalBrokerDueAmt.Text = dataLoan.sDemandTotalBrokerDueAmt_rep; 
            sDemandTotalBorrowerAmt.Text = dataLoan.sDemandTotalBorrowerAmt_rep;
            sDemandLOrigFeeLenderPc.Text = dataLoan.sDemandLOrigFeeLenderPc_rep;
            sDemandLOrigFeeLenderMb.Text = dataLoan.sDemandLOrigFeeLenderMb_rep;
            sDemandLOrigFeeLenderAmt.Text = dataLoan.sDemandLOrigFeeLenderAmt_rep;
            sDemandLOrigFeeBrokerAmt.Text = dataLoan.sDemandLOrigFeeBrokerAmt_rep;

            sDemandLDiscountLenderPc.Text = dataLoan.sDemandLDiscountLenderPc_rep;
            sDemandLDiscountLenderMb.Text = dataLoan.sDemandLDiscountLenderMb_rep;
            sDemandLDiscountLenderAmt.Text = dataLoan.sDemandLDiscountLenderAmt_rep;
            sDemandLDiscountBrokerAmt.Text = dataLoan.sDemandLDiscountBrokerAmt_rep;

            sDemandYieldSpreadPremiumLenderPc.Text = dataLoan.sDemandYieldSpreadPremiumLenderPc_rep;
            sDemandYieldSpreadPremiumLenderMb.Text = dataLoan.sDemandYieldSpreadPremiumLenderMb_rep;
            sDemandYieldSpreadPremiumLenderAmt.Text = dataLoan.sDemandYieldSpreadPremiumLenderAmt_rep;
            sDemandYieldSpreadPremiumBrokerAmt.Text = dataLoan.sDemandYieldSpreadPremiumBrokerAmt_rep;

            sDemandYieldSpreadPremiumBorrowerPc.Text = dataLoan.sDemandYieldSpreadPremiumBorrowerPc_rep;
            sDemandYieldSpreadPremiumBorrowerMb.Text = dataLoan.sDemandYieldSpreadPremiumBorrowerMb_rep;
            sDemandYieldSpreadPremiumBorrowerAmt.Text = dataLoan.sDemandYieldSpreadPremiumBorrowerAmt_rep;

            sDemandNotes.Text = dataLoan.sDemandNotes;

            sSubmitPropTDesc.Text = dataLoan.sSubmitPropTDesc;
            sEstCloseDLckd.Checked = dataLoan.sEstCloseDLckd;
            sEstCloseD.Text = dataLoan.sEstCloseD_rep;
			sEstCloseD.ToolTip = "Hint:  Enter 't' for today's date.";

            Tools.SetDropDownListValue(this.sQualTermCalculationType, dataLoan.sQualTermCalculationType);
            this.sQualTerm.Value = dataLoan.sQualTerm_rep;
        }
        protected override void LoadData() 
        {
            CLoanSubmissionData dataLoan = new CLoanSubmissionData( RequestHelper.LoanID );
            dataLoan.InitLoad();
            BindDataObject(dataLoan);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
            IsAppSpecific = false;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion
	}
}
