﻿#region Generated Code
namespace LendersOfficeApp.newlos.Forms
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.UI.DataContainers;

    /// <summary>
    /// A jquery popup that displays the rate floor calculations.
    /// </summary>
    public partial class RateFloorPopup : System.Web.UI.UserControl
    {
        /// <summary>
        /// The field values passed in from the parent page.
        /// </summary>
        private RateFloorPopupFields rateFloorFields;

        /// <summary>
        /// Sets the field values from the parent page.
        /// </summary>
        /// <param name="fieldValues">The field values.</param>
        public void SetRateLockPopupFieldValues(RateFloorPopupFields fieldValues)
        {
            this.rateFloorFields = fieldValues;
        }

        /// <summary>
        /// PageLoad function.
        /// </summary>
        /// <param name="sender">Controller that called this.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.rateFloorFields == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "RateFloorPopupFields not passed into RateFloorPopup user control.");
            }

            Tools.SetDropDownListValue(this.sRAdjFloorCalcT, this.rateFloorFields.sRAdjFloorCalcT);
            this.sRAdjFloorCalcT_hidden.Value = this.rateFloorFields.sRAdjFloorCalcTLabel;
            this.sRAdjFloorBaseR.Text = this.rateFloorFields.sRAdjFloorBaseR;
            this.sRAdjFloorAddR.Text = this.rateFloorFields.sRAdjFloorAddR;
            this.sRAdjFloorTotalR.Text = this.rateFloorFields.sRAdjFloorTotalR;
            this.sRAdjFloorLifeCapR.Text = this.rateFloorFields.sRAdjFloorLifeCapR;
            this.sRAdjFloorR.Text = this.rateFloorFields.sRAdjFloorR;
            this.sIsRAdjFloorRReadOnly.Value = this.rateFloorFields.sIsRAdjFloorRReadOnly;
            this.sNoteIR_popup.Text = this.rateFloorFields.sNoteIR;
            this.sRAdjLifeCapR_popup.Text = this.rateFloorFields.sRAdjLifeCapR;

            if (this.rateFloorFields.FieldsType == RateFloorPopupFieldsType.Construction)
            {
                this.sRAdjFloorCalcT.Attributes["data-field-id"] = "sConstructionRAdjFloorCalcT";
                this.sRAdjFloorBaseR.Attributes["data-field-id"] = "sConstructionRAdjFloorBaseR";
                this.sRAdjFloorAddR.Attributes["data-field-id"] = "sConstructionRAdjFloorAddR";
                this.sRAdjFloorTotalR.Attributes["data-field-id"] = "sConstructionRAdjFloorTotalR";
                this.sRAdjFloorLifeCapR.Attributes["data-field-id"] = "sConstructionRAdjFloorLifeCapR";
                this.sRAdjFloorR.Attributes["data-field-id"] = "sConstructionRAdjFloorR";
                this.sNoteIR_popup.Attributes["data-field-id"] = "sConstructionPeriodIR";
                this.sRAdjLifeCapR_popup.Attributes["data-field-id"] = "sConstructionRAdjLifeCapR";
            }
        }

        /// <summary>
        /// PageInit function.
        /// </summary>
        /// <param name="sender">Controller that called this.</param>
        /// <param name="e">System event arguments.</param>
        private void Page_Init(object sender, System.EventArgs e)
        {
            Tools.Bind_sRAdjFloorCalcT(sRAdjFloorCalcT);
        }
    }
}