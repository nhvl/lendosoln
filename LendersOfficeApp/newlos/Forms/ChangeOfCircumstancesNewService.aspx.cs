﻿namespace LendersOfficeApp.newlos.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using CommonProjectLib.Common.Lib;
    using ComplianceEagle;
    using DataAccess;
    using LendersOffice.Constants;

    public partial class ChangeOfCircumstancesNewService : LendersOffice.Common.BaseSimpleServiceXmlPage 
    {
        protected override void Initialize() 
        {
            AddBackgroundItem("", new ChangeOfCircumstancesNewServiceItem());
        }
    }

    public class ChangeOfCircumstancesNewServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "SaveAndApplyCoC":
                    SaveAndApplyCoC();
                    break;
            }
        }

        private void SaveAndApplyCoC()
        {
            Guid sLId = GetGuid("sLId");
            
            string selectedFeesStr = GetString("selectedFees");

            string[] selectedFees = selectedFeesStr.Split(',');

            List<Guid> feeIds = new List<Guid>();

            foreach (string feeIdStr in selectedFees)
            {
                if (feeIdStr.Length != 0)
                {
                    feeIds.Add(new Guid(feeIdStr));
                }
            }

            CPageData dataLoan = ConstructPageDataClass(sLId);

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (!dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                return;
            }
            if (string.IsNullOrEmpty(GetString("sCircumstanceChangeD")) ||
                string.IsNullOrEmpty(GetString("GfePrepareDate")) ||
                string.IsNullOrEmpty(GetString("sCircumstanceChangeExplanation")) ||
                (!dataLoan.BrokerDB.IsProtectDisclosureDates && string.IsNullOrEmpty(GetString("sGfeRedisclosureD"))))
            {
                throw new CBaseException("Please fill out all required fields.", "User did not fill out all required fields.");
            }
            
            BindData(dataLoan, null);

            // These being checked means a group of fees were picked
            bool updateAllTotalLoanAmount = GetBool("updateAllTotalLoanAmount");
            bool updateAllLoanAmount = GetBool("updateAllLoanAmount");
            bool updateAllPurchPrice = GetBool("updateAllPurchPrice");
            bool updateAllAppraisalFeeChk = GetBool("updateAllAppraisalFeeChk");
            bool updateAllReserveFeeChk = GetBool("updateAllReserveFeeChk");

            // These being checked means a single loan-value entry was checked
            bool updateOnlyTotalLoanAmount = GetBool("updateOnlyTotalLoanAmount");
            bool updateOnlyLoanAmount = GetBool("updateOnlyLoanAmount");
            bool updateOnlyPurchPrice = GetBool("updateOnlyPurchPrice");
            bool updateOnlyAppraisalFeeChk = GetBool("updateOnlyAppraisalFeeChk");

            List<Tuple<string, bool>> updateFieldList = new List<Tuple<string, bool>>();

            if (updateAllTotalLoanAmount || updateOnlyTotalLoanAmount)
            {
                updateFieldList.Add(Tuple.Create("sFinalLAmt", updateOnlyTotalLoanAmount));
            }

            if (updateAllLoanAmount || updateOnlyLoanAmount)
            {
                updateFieldList.Add(Tuple.Create("sLAmtCalc", updateOnlyLoanAmount));
            }

            if (updateAllPurchPrice || updateOnlyPurchPrice)
            {
                updateFieldList.Add(Tuple.Create("sPurchPrice", updateOnlyPurchPrice));
            }

            if (updateAllAppraisalFeeChk || updateOnlyAppraisalFeeChk)
            {
                updateFieldList.Add(Tuple.Create("sApprVal", updateOnlyAppraisalFeeChk));
            }

            if (updateAllReserveFeeChk)
            {
                updateFieldList.Add(Tuple.Create("sHousingExpenseJsonContent", false));
            }

            var category = this.GetCOCComplianceCategory(dataLoan.sDisclosureRegulationT, dataLoan.BrokerDB.IsEnableComplianceEagleIntegration);

            dataLoan.ArchiveClosingCostCoC(feeIds.Distinct().ToList(), updateFieldList, category);

            SetResult("sLastDisclosedGFEArchiveD", dataLoan.sLastDisclosedGFEArchiveD_rep);

            dataLoan.Save();
        }

        /// <summary>
        /// Get the changed circumstance category. For use with ComplianceEagle.
        /// </summary>
        /// <param name="regulationType">The regulation type of the loan file.</param>
        /// <param name="ComplianceEagleEnabled">True if the lender has the ComplianceEagle integration enabled. Otherwise false.</param>
        /// <returns>The changed circumstance category.</returns>
        private CEagleChangedCircumstanceCategory GetCOCComplianceCategory(E_sDisclosureRegulationT regulationType, bool ComplianceEagleEnabled)
        {
            var category = CEagleChangedCircumstanceCategory.Blank;

            if (ComplianceEagleEnabled && regulationType == E_sDisclosureRegulationT.TRID)
            {
                category = (CEagleChangedCircumstanceCategory)GetInt("ComplianceCOCCategory", Convert.ToInt32(CEagleChangedCircumstanceCategory.Blank));
            }

            return category;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            
            dataLoan.sCircumstanceChangeD_rep = GetString("sCircumstanceChangeD");
            if (false == dataLoan.BrokerDB.IsProtectDisclosureDates)
            {
                dataLoan.sGfeRedisclosureD_rep = GetString("sGfeRedisclosureD");
            }
            dataLoan.sCircumstanceChangeExplanation = GetString("sCircumstanceChangeExplanation");


            IPreparerFields gfe = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            gfe.PrepareDate_rep = GetString("GfePrepareDate");
            gfe.Update();
        }

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, this.GetType());
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }
}
