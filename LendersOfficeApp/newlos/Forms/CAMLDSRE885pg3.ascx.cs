﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using System.Collections;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class CAMLDSRE885pg3 : BaseLoanUserControl, IAutoLoadUserControl
    {
        private void BindLienPositionComboBox(MeridianLink.CommonControls.ComboBox cb)
        {
            cb.Items.Add("1st");
            cb.Items.Add("2nd");
            cb.Items.Add("3rd");
            cb.Items.Add("4th"); // opm 82162
        }
        protected void PageInit(object sender, System.EventArgs e)
        {
            BindLienPositionComboBox(sLien1PriorityBefore);
            BindLienPositionComboBox(sLien1PriorityAfter);
            BindLienPositionComboBox(sLien2PriorityBefore);
            BindLienPositionComboBox(sLien2PriorityAfter);
            BindLienPositionComboBox(sLien3PriorityBefore);
            BindLienPositionComboBox(sLien3PriorityAfter);

            Tools.Bind_TriState(sMldsIsNoDocTri);
            sMldsIsNoDocTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
        }

        public void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(CAMLDSRE885pg3));
            dataLoan.InitLoad();

            sLienholder1NmBefore.Text = dataLoan.sLienholder1NmBefore;
            sLien1AmtBefore.Text = dataLoan.sLien1AmtBefore_rep;
            sLien1PriorityBefore.Text = dataLoan.sLien1PriorityBefore;

            sLienholder2NmBefore.Text = dataLoan.sLienholder2NmBefore;
            sLien2AmtBefore.Text = dataLoan.sLien2AmtBefore_rep;
            sLien2PriorityBefore.Text = dataLoan.sLien2PriorityBefore;

            sLienholder3NmBefore.Text = dataLoan.sLienholder3NmBefore;
            sLien3AmtBefore.Text = dataLoan.sLien3AmtBefore_rep;
            sLien3PriorityBefore.Text = dataLoan.sLien3PriorityBefore;

            sLienholder1NmAfter.Text = dataLoan.sLienholder1NmAfter;
            sLien1AmtAfter.Text = dataLoan.sLien1AmtAfter_rep;
            sLien1PriorityAfter.Text = dataLoan.sLien1PriorityAfter;

            sLienholder2NmAfter.Text = dataLoan.sLienholder2NmAfter;
            sLien2AmtAfter.Text = dataLoan.sLien2AmtAfter_rep;
            sLien2PriorityAfter.Text = dataLoan.sLien2PriorityAfter;

            sLienholder3NmAfter.Text = dataLoan.sLienholder3NmAfter;
            sLien3AmtAfter.Text = dataLoan.sLien3AmtAfter_rep;
            sLien3PriorityAfter.Text = dataLoan.sLien3PriorityAfter;

            Tools.SetDropDownListValue(sBrokControlledFundT, dataLoan.sBrokControlledFundT);
            Tools.Set_TriState(sMldsIsNoDocTri, dataLoan.sMldsIsNoDocTri);
            sMldsIsNoDocTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
        }

        public void SaveData()
        {
        }

        private void ZeroOneCheckBoxList_Load(object sender, System.EventArgs e)
        {
            CheckBoxList cbl = (CheckBoxList)sender;
            ArrayList list = new ArrayList();

            foreach (ListItem o in cbl.Items)
            {
                list.Add("'" + o.Value + "'");
            }

            Page.ClientScript.RegisterArrayDeclaration(cbl.ClientID + "_items", string.Join(",", (string[])list.ToArray(typeof(string))));
            Page.ClientScript.RegisterHiddenField("cbl_" + cbl.ClientID, "");
            ((BasePage)Page).AddInitScriptFunctionWithArgs("_registerCBL", string.Format("'{0}', {1}", cbl.ClientID, cbl.Items.Count));


        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }

}