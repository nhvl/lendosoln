namespace LendersOfficeApp.newlos.Forms
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;

    public class Loan1003Pg1ServiceItem : AbstractBackgroundServiceItem
    {
        protected override void Process(string methodName)
        {
            switch(methodName) {
                case "CalculateAge":
                    CalculateAge();
                    break;
            }
        }
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(Loan1003Pg1ServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.aBAddr = GetString("aBAddr");
            dataApp.aBAddrMail = GetString("aBAddrMail");
            dataApp.aBAddrMailSourceT = (E_aAddrMailSourceT)GetInt("aBAddrMailSourceT");
            dataApp.aBAddrT = (E_aBAddrT)GetInt("aBAddrT");
            dataApp.aBAddrYrs = GetString("aBAddrYrs");
            dataApp.aBAge_rep = GetString("aBAge");
            dataApp.aBBusPhone = GetString("aBBusPhone");
            dataApp.aBCellPhone = GetString("aBCellPhone");
            dataApp.aBCity = GetString("aBCity");
            dataApp.aBCityMail = GetString("aBCityMail");
            dataApp.aBDependAges = GetString("aBDependAges");
            dataApp.aBDependNum_rep = GetString("aBDependNum");
            dataApp.aBDob_rep = GetString("aBDob");
            dataApp.aBEmail = GetString("aBEmail");
            dataApp.aBCoreSystemId = GetString("aBCoreSystemId", "");
            dataApp.aBFirstNm = GetString("aBFirstNm");
            dataApp.aBHPhone = GetString("aBHPhone");
            dataApp.aBLastNm = GetString("aBLastNm");
            dataApp.aBMaritalStatT = (E_aBMaritalStatT)GetInt("aBMaritalStatT");
            dataApp.aBMidNm = GetString("aBMidNm");
            dataApp.aBPrev1Addr = GetString("aBPrev1Addr");
            dataApp.aBPrev1AddrT = (E_aBPrev1AddrT)GetInt("aBPrev1AddrT");
            dataApp.aBPrev1AddrYrs = GetString("aBPrev1AddrYrs");
            dataApp.aBPrev1City = GetString("aBPrev1City");
            dataApp.aBPrev1State = GetString("aBPrev1State");
            dataApp.aBPrev1Zip = GetString("aBPrev1Zip");
            dataApp.aBPrev2Addr = GetString("aBPrev2Addr");
            dataApp.aBPrev2AddrT = (E_aBPrev2AddrT)GetInt("aBPrev2AddrT");
            dataApp.aBPrev2AddrYrs = GetString("aBPrev2AddrYrs");
            dataApp.aBPrev2City = GetString("aBPrev2City");
            dataApp.aBPrev2State = GetString("aBPrev2State");
            dataApp.aBPrev2Zip = GetString("aBPrev2Zip");
            dataApp.aBSchoolYrs_rep = GetString("aBSchoolYrs");
            dataApp.aBSsn = GetString("aBSsn");
            dataApp.aBState = GetString("aBState");
            dataApp.aBStateMail = GetString("aBStateMail");
            dataApp.aBSuffix = GetString("aBSuffix");
            dataApp.aBZip = GetString("aBZip");
            dataApp.aBZipMail = GetString("aBZipMail");
            dataApp.aCAddr = GetString("aCAddr");
            dataApp.aCAddrMail = GetString("aCAddrMail");
            dataApp.aCAddrMailSourceT = (E_aAddrMailSourceT)GetInt("aCAddrMailSourceT");
            dataApp.aCAddrT = (E_aCAddrT)GetInt("aCAddrT");
            dataApp.aCAddrYrs = GetString("aCAddrYrs");
            dataApp.aCAge_rep = GetString("aCAge");
            dataApp.aCBusPhone = GetString("aCBusPhone");
            dataApp.aCCellPhone = GetString("aCCellPhone");
            dataApp.aCCity = GetString("aCCity");
            dataApp.aCCityMail = GetString("aCCityMail");
            dataApp.aCDependAges = GetString("aCDependAges");
            dataApp.aCDependNum_rep = GetString("aCDependNum");
            dataApp.aCDob_rep = GetString("aCDob");
            dataApp.aCEmail = GetString("aCEmail");
            dataApp.aCCoreSystemId = GetString("aCCoreSystemId", "");
            dataApp.aCFirstNm = GetString("aCFirstNm");
            dataApp.aCHPhone = GetString("aCHPhone");
            dataApp.aCLastNm = GetString("aCLastNm");
            dataApp.aCMaritalStatT = (E_aCMaritalStatT)GetInt("aCMaritalStatT");
            dataApp.aCMidNm = GetString("aCMidNm");
            dataApp.aCPrev1Addr = GetString("aCPrev1Addr");
            dataApp.aCPrev1AddrT = (E_aCPrev1AddrT)GetInt("aCPrev1AddrT");
            dataApp.aCPrev1AddrYrs = GetString("aCPrev1AddrYrs");
            dataApp.aCPrev1City = GetString("aCPrev1City");
            dataApp.aCPrev1State = GetString("aCPrev1State");
            dataApp.aCPrev1Zip = GetString("aCPrev1Zip");
            dataApp.aCPrev2Addr = GetString("aCPrev2Addr");
            dataApp.aCPrev2AddrT = (E_aCPrev2AddrT)GetInt("aCPrev2AddrT");
            dataApp.aCPrev2AddrYrs = GetString("aCPrev2AddrYrs");
            dataApp.aCPrev2City = GetString("aCPrev2City");
            dataApp.aCPrev2State = GetString("aCPrev2State");
            dataApp.aCPrev2Zip = GetString("aCPrev2Zip");
            dataApp.aCSchoolYrs_rep = GetString("aCSchoolYrs");
            dataApp.aCSsn = GetString("aCSsn");
            dataApp.aCState = GetString("aCState");
            dataApp.aCStateMail = GetString("aCStateMail");
            dataApp.aCSuffix = GetString("aCSuffix");
            dataApp.aCZip = GetString("aCZip");
            dataApp.aCZipMail = GetString("aCZipMail");
            dataApp.aManner = GetString("aManner");
            dataApp.aOccT = (E_aOccT)GetInt("aOccT");
            dataApp.aSpouseIExcl = GetBool("aSpouseIExcl");
            dataApp.aTitleNm1 = GetString("aTitleNm1");
            dataApp.aTitleNm1Lckd = GetBool("aTitleNm1Lckd");
            dataApp.aTitleNm2 = GetString("aTitleNm2");
            dataApp.aTitleNm2Lckd = GetBool("aTitleNm2Lckd");
            dataLoan.sAgencyCaseNum = GetString("sAgencyCaseNum");
            dataLoan.sDue_rep = GetString("sDue");
            dataLoan.sDwnPmtSrc = GetString("sDwnPmtSrc");
            dataLoan.sDwnPmtSrcExplain = GetString("sDwnPmtSrcExplain");
            dataLoan.sEquityCalc_rep = GetString("sEquity");
            dataLoan.sEstateHeldT = (E_sEstateHeldT)GetInt("sEstateHeldT");
            dataLoan.sFinMethDesc = GetString("sFinMethDesc");
            dataLoan.sFinMethT = (E_sFinMethT)GetInt("sFinMethT");
            dataLoan.sLAmtCalc_rep = GetString("sLAmt");
            dataLoan.sLAmtLckd = GetBool("sLAmtLckd");
            dataLoan.sRefPurpose = GetString("sRefPurpose");
            dataLoan.sLPurposeT = (E_sLPurposeT)GetInt("sLPurposeT");
            dataLoan.sLT = (E_sLT)GetInt("sLT");
            dataLoan.sLTODesc = GetString("sLTODesc");
            dataLoan.sLeaseHoldExpireD_rep = GetString("sLeaseHoldExpireD");
            dataLoan.sLenderCaseNum = GetString("sLenderCaseNum");
            dataLoan.sLenderCaseNumLckd = GetBool("sLenderCaseNumLckd");

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges))
            {
                dataLoan.sLotAcquiredD_rep = GetString("sLotAcquiredD");
            }
            else
            {
                dataLoan.sLotAcqYr = GetString("sLotAcqYr");
                dataLoan.sLotVal_rep = GetString("sLotVal");
            }

            dataLoan.sLotImprovC_rep = GetString("sLotImprovC");
            dataLoan.sLotLien_rep = GetString("sLotLien");
            dataLoan.sLotOrigC_rep = GetString("sLotOrigC");
            dataLoan.sMultiApps = GetBool("sMultiApps");
            dataLoan.sNoteIR_rep = GetString("sNoteIR");
            dataLoan.sOLPurposeDesc = GetString("sOLPurposeDesc");
            dataLoan.sOccR_rep = GetString("sOccR");
            dataLoan.sOccRLckd = GetBool("sOccRLckd");
            dataLoan.sPurchPrice_rep = GetString("sPurchPrice");
            dataLoan.sQualIR_rep = GetString("sQualIR");
            dataLoan.sQualIRLckd = this.GetBool(nameof(dataLoan.sQualIRLckd), dataLoan.sQualIRLckd);
            dataLoan.sSpAcqYr = GetString("sSpAcqYr");
            dataLoan.sSpAddr = GetString("sSpAddr");
            dataLoan.sSpCity = GetString("sSpCity");
            dataLoan.sSpCounty = GetString("sSpCounty");
            dataLoan.sSpGrossRent_rep = GetString("sSpGrossRent");
            dataLoan.sSpImprovC_rep = GetString("sSpImprovC");
            dataLoan.sSpImprovDesc = GetString("sSpImprovDesc");
            dataLoan.sSpImprovTimeFrameT = (E_sSpImprovTimeFrameT)GetInt("sSpImprovTimeFrameT");
            dataLoan.sSpLegalDesc = GetString("sSpLegalDesc");
            dataLoan.sSpLien_rep = GetString("sSpLien");
            dataLoan.sSpOrigC_rep = GetString("sSpOrigC");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sSpZip = GetString("sSpZip");
            dataLoan.sTerm_rep = GetString("sTerm");
            dataLoan.sUnitsNum_rep = GetString("sUnitsNum");
            dataLoan.sYrBuilt = GetString("sYrBuilt");
            dataLoan.sFinMethodPrintAsOther = GetBool("sFinMethodPrintAsOther");
            dataLoan.sFinMethPrintAsOtherDesc = GetString("sFinMethPrintAsOtherDesc");

            dataLoan.sQualTermCalculationType = (QualTermCalculationType)GetInt("sQualTermCalculationType");
            dataLoan.sQualTerm_rep = GetString("sQualTerm");

            Loan1003.BindDataFromQualRatePopup(dataLoan, dataApp, this);
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("aBAddr", dataApp.aBAddr);
            SetResult("aBAddrMail", dataApp.aBAddrMail);
            SetResult("aBAddrMailSourceT", dataApp.aBAddrMailSourceT);
            SetResult("aBAddrT", dataApp.aBAddrT);
            SetResult("aBAddrYrs", dataApp.aBAddrYrs);
            SetResult("aBAge", dataApp.aBAge_rep);
            SetResult("aBBusPhone", dataApp.aBBusPhone);
            SetResult("aBCellphone", dataApp.aBCellPhone);
            SetResult("aBCity", dataApp.aBCity);
            SetResult("aBCityMail", dataApp.aBCityMail);
            SetResult("aBDependAges", dataApp.aBDependAges);
            SetResult("aBDependNum", dataApp.aBDependNum_rep);
            SetResult("aBDob", dataApp.aBDob_rep);
            SetResult("aBEmail", dataApp.aBEmail);
            SetResult("aBFirstNm", dataApp.aBFirstNm);
            SetResult("aBHPhone", dataApp.aBHPhone);
            SetResult("aBLastNm", dataApp.aBLastNm);
            SetResult("aBMaritalStatT", dataApp.aBMaritalStatT);
            SetResult("aBMidNm", dataApp.aBMidNm);
            SetResult("aBPrev1Addr", dataApp.aBPrev1Addr);
            SetResult("aBPrev1AddrT", dataApp.aBPrev1AddrT);
            SetResult("aBPrev1AddrYrs", dataApp.aBPrev1AddrYrs);
            SetResult("aBPrev1City", dataApp.aBPrev1City);
            SetResult("aBPrev1State", dataApp.aBPrev1State);
            SetResult("aBPrev1Zip", dataApp.aBPrev1Zip);
            SetResult("aBPrev2Addr", dataApp.aBPrev2Addr);
            SetResult("aBPrev2AddrT", dataApp.aBPrev2AddrT);
            SetResult("aBPrev2AddrYrs", dataApp.aBPrev2AddrYrs);
            SetResult("aBPrev2City", dataApp.aBPrev2City);
            SetResult("aBPrev2State", dataApp.aBPrev2State);
            SetResult("aBPrev2Zip", dataApp.aBPrev2Zip);
            SetResult("aBSchoolYrs", dataApp.aBSchoolYrs_rep);
            SetResult("aBSsn", dataApp.aBSsn);
            SetResult("aBState", dataApp.aBState);
            SetResult("aBStateMail", dataApp.aBStateMail);
            SetResult("aBSuffix", dataApp.aBSuffix);
            SetResult("aBZip", dataApp.aBZip);
            SetResult("aBZipMail", dataApp.aBZipMail);
            SetResult("aCAddr", dataApp.aCAddr);
            SetResult("aCAddrMail", dataApp.aCAddrMail);
            SetResult("aCAddrMailSourceT", dataApp.aCAddrMailSourceT);
            SetResult("aCAddrT", dataApp.aCAddrT);
            SetResult("aCAddrYrs", dataApp.aCAddrYrs);
            SetResult("aCAge", dataApp.aCAge_rep);
            SetResult("aCBusPhone", dataApp.aCBusPhone);
            SetResult("aCCellphone", dataApp.aCCellPhone);
            SetResult("aCCity", dataApp.aCCity);
            SetResult("aCCityMail", dataApp.aCCityMail);
            SetResult("aCDependAges", dataApp.aCDependAges);
            SetResult("aCDependNum", dataApp.aCDependNum_rep);
            SetResult("aCDob", dataApp.aCDob_rep);
            SetResult("aCEmail", dataApp.aCEmail);
            SetResult("aCFirstNm", dataApp.aCFirstNm);
            SetResult("aCHPhone", dataApp.aCHPhone);
            SetResult("aCLastNm", dataApp.aCLastNm);
            SetResult("aCMaritalStatT", dataApp.aCMaritalStatT);
            SetResult("aCMidNm", dataApp.aCMidNm);
            SetResult("aCPrev1Addr", dataApp.aCPrev1Addr);
            SetResult("aCPrev1AddrT", dataApp.aCPrev1AddrT);
            SetResult("aCPrev1AddrYrs", dataApp.aCPrev1AddrYrs);
            SetResult("aCPrev1City", dataApp.aCPrev1City);
            SetResult("aCPrev1State", dataApp.aCPrev1State);
            SetResult("aCPrev1Zip", dataApp.aCPrev1Zip);
            SetResult("aCPrev2Addr", dataApp.aCPrev2Addr);
            SetResult("aCPrev2AddrT", dataApp.aCPrev2AddrT);
            SetResult("aCPrev2AddrYrs", dataApp.aCPrev2AddrYrs);
            SetResult("aCPrev2City", dataApp.aCPrev2City);
            SetResult("aCPrev2State", dataApp.aCPrev2State);
            SetResult("aCPrev2Zip", dataApp.aCPrev2Zip);
            SetResult("aCSchoolYrs", dataApp.aCSchoolYrs_rep);
            SetResult("aCSsn", dataApp.aCSsn);
            SetResult("aCState", dataApp.aCState);
            SetResult("aCStateMail", dataApp.aCStateMail);
            SetResult("aCSuffix", dataApp.aCSuffix);
            SetResult("aCZip", dataApp.aCZip);
            SetResult("aCZipMail", dataApp.aCZipMail);
            SetResult("aManner", dataApp.aManner);
            SetResult("aOccT", dataApp.aOccT);
            SetResult("aSpouseIExcl", dataApp.aSpouseIExcl);
            SetResult("aTitleNm1", dataApp.aTitleNm1);
            SetResult("aTitleNm1Lckd", dataApp.aTitleNm1Lckd);
            SetResult("aTitleNm2", dataApp.aTitleNm2);
            SetResult("aTitleNm2Lckd", dataApp.aTitleNm2Lckd);
            SetResult("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            SetResult("sDownPmtPc", dataLoan.sDownPmtPc_rep);
            SetResult("sDue", dataLoan.sDue_rep);
            SetResult("sDwnPmtSrc", dataLoan.sDwnPmtSrc);
            SetResult("sDwnPmtSrcExplain", dataLoan.sDwnPmtSrcExplain);
            SetResult("sEquity", dataLoan.sEquityCalc_rep);
            SetResult("sEstateHeldT", dataLoan.sEstateHeldT);
            SetResult("sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep);
            SetResult("sFinMethDesc", dataLoan.sFinMethDesc);
            SetResult("sFinMethT", dataLoan.sFinMethT);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sLAmt", dataLoan.sLAmtCalc_rep);
            SetResult("sLAmtLckd", dataLoan.sLAmtLckd);
            SetResult("sLPurposeT", dataLoan.sLPurposeT);
            SetResult("sLT", dataLoan.sLT);
            SetResult("sLTODesc", dataLoan.sLTODesc);
            SetResult("sLeaseHoldExpireD", dataLoan.sLeaseHoldExpireD_rep);
            SetResult("sLenderCaseNum", dataLoan.sLenderCaseNum);
            SetResult("sLenderCaseNumLckd", dataLoan.sLenderCaseNumLckd);

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges))
            {
                SetResult("sLotAcquiredD", dataLoan.sLotAcquiredD_rep);
            }
            else
            {
                SetResult("sLotAcqYr", dataLoan.sLotAcqYr);
                SetResult("sLotVal", dataLoan.sLotVal_rep);
            }
            
            SetResult("sLotImprovC", dataLoan.sLotImprovC_rep);
            SetResult("sLotLien", dataLoan.sLotLien_rep);
            SetResult("sLotOrigC", dataLoan.sLotOrigC_rep);            
            SetResult("sLotWImprovTot", dataLoan.sLotWImprovTot_rep);
            SetResult("sMultiApps", dataLoan.sMultiApps);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sOLPurposeDesc", dataLoan.sOLPurposeDesc);
            SetResult("sOccRLckd", dataLoan.sOccRLckd);
            SetResult("sOccR", dataLoan.sOccR_rep);
            SetResult("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            SetResult("sPurchPrice", dataLoan.sPurchPrice_rep);
            SetResult("sQualIR", dataLoan.sQualIR_rep);
            this.SetResult(nameof(dataLoan.sQualIRLckd), dataLoan.sQualIRLckd);
            SetResult("sRefPurpose", dataLoan.sRefPurpose);
            SetResult("sSpAcqYr", dataLoan.sSpAcqYr);
            SetResult("sSpAddr", dataLoan.sSpAddr);
            SetResult("sSpCity", dataLoan.sSpCity);
            SetResult("sSpCounty", dataLoan.sSpCounty);
            SetResult("sSpGrossRent", dataLoan.sSpGrossRent_rep);
            SetResult("sSpImprovC", dataLoan.sSpImprovC_rep);
            SetResult("sSpImprovDesc", dataLoan.sSpImprovDesc);
            SetResult("sSpImprovTimeFrameT", dataLoan.sSpImprovTimeFrameT);
            SetResult("sSpLegalDesc", dataLoan.sSpLegalDesc);
            SetResult("sSpLien", dataLoan.sSpLien_rep);
            SetResult("sSpOrigC", dataLoan.sSpOrigC_rep);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sSpZip", dataLoan.sSpZip.TrimWhitespaceAndBOM());
            SetResult("sTerm", dataLoan.sTerm_rep);
            SetResult("sUnitsNum", dataLoan.sUnitsNum_rep);
            SetResult("sYrBuilt", dataLoan.sYrBuilt.TrimWhitespaceAndBOM());
            SetResult("sFinMethPrintAsOtherDesc", dataLoan.sFinMethPrintAsOtherDesc);
            SetResult("sFinMethodPrintAsOther", dataLoan.sFinMethodPrintAsOther);

            SetResult("sQualTermCalculationType", dataLoan.sQualTermCalculationType);
            SetResult("sQualTerm", dataLoan.sQualTerm_rep);

            Loan1003.LoadDataForQualRatePopup(dataLoan, dataApp, this);
        }
        private void CalculateAge()
        {
            try
            {
                string dob = GetString("dob");

                SetResult("age", Tools.CalcAgeForToday(DateTime.Parse(dob)));

            }
            catch (CBaseException)
            {
                // DOB is invalid. Use current age value.
                SetResult("age", GetString("age"));
            }
        }
    }

    public class Loan1003Pg2ServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(Loan1003Pg2ServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.aAsstLiaCompletedNotJointly = GetString("aAsstLiaCompletedNotJointly") == "1";

            if (!dataLoan.sIsIncomeCollectionEnabled)
            {
                dataApp.aBBaseI_rep = GetString("aBBaseI");
                dataApp.aBBonusesI_rep = GetString("aBBonusesI");
                dataApp.aBCommisionI_rep = GetString("aBCommisionI");
                dataApp.aBDividendI_rep = GetString("aBDividendI");
                dataApp.aBOvertimeI_rep = GetString("aBOvertimeI");
                dataApp.aCBaseI_rep = GetString("aCBaseI");
                dataApp.aCBonusesI_rep = GetString("aCBonusesI");
                dataApp.aCCommisionI_rep = GetString("aCCommisionI");
                dataApp.aCDividendI_rep = GetString("aCDividendI");
                dataApp.aCOvertimeI_rep = GetString("aCOvertimeI");
                dataApp.aOtherIncomeList = ObsoleteSerializationHelper.JsonDeserialize<List<OtherIncome>>(GetString("OtherIncomeJson"));
            }

            dataApp.aBNetRentI1003_rep = GetString("aBNetRentI1003");
            dataApp.aCNetRentI1003_rep = GetString("aCNetRentI1003");
            dataApp.aNetRentI1003Lckd = GetBool("aNetRentI1003Lckd");

            dataApp.aPres1stM_rep = GetString("aPres1stM");
            dataApp.aPresHazIns_rep = GetString("aPresHazIns");
            dataApp.aPresHoAssocDues_rep = GetString("aPresHoAssocDues");
            dataApp.aPresMIns_rep = GetString("aPresMIns");
            dataApp.aPresOFin_rep = GetString("aPresOFin");
            dataApp.aPresOHExp_rep = GetString("aPresOHExp");
            dataApp.aPresRealETx_rep = GetString("aPresRealETx");
            dataApp.aPresRent_rep = GetString("aPresRent");
            if (!dataLoan.sHazardExpenseInDisbursementMode)
            {
                dataLoan.sProHazInsMb_rep = GetString("sProHazInsMb");
                dataLoan.sProHazInsR_rep = GetString("sProHazInsR");
                dataLoan.sProHazInsT = (E_PercentBaseT)GetInt("sProHazInsT");
            }
            dataLoan.sProHoAssocDues_rep = GetString("sProHoAssocDues");
            dataLoan.sProOHExpDesc = GetString("sProOHExpDesc");
            dataLoan.sProOHExp_rep = GetString("sProOHExp");
            if (!dataLoan.sRealEstateTaxExpenseInDisbMode)
            {
                dataLoan.sProRealETxMb_rep = GetString("sProRealETxMb");
                dataLoan.sProRealETxR_rep = GetString("sProRealETxR");
                dataLoan.sProRealETxT = (E_PercentBaseT)GetInt("sProRealETxT");
            }
            dataLoan.sProOHExpLckd = GetBool("sProOHExpLckd");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("aAsstLiaCompletedNotJointly", dataApp.aAsstLiaCompletedNotJointly ? "1" : "0");
            SetResult("aAsstLiqTot", dataApp.aAsstLiqTot_rep);
            SetResult("aAsstNonReSolidTot", dataApp.aAsstNonReSolidTot_rep);
            SetResult("aAsstValTot", dataApp.aAsstValTot_rep);
            SetResult("aBBaseI", dataApp.aBBaseI_rep);
            SetResult("aBBonusesI", dataApp.aBBonusesI_rep);
            SetResult("aBCommisionI", dataApp.aBCommisionI_rep);
            SetResult("aBDividendI", dataApp.aBDividendI_rep);
            SetResult("aBNetRentI1003", dataApp.aBNetRentI1003_rep);
            SetResult("aBOvertimeI", dataApp.aBOvertimeI_rep);
            SetResult("aBTotI", dataApp.aBTotI_rep);
            SetResult("aBTotOI", dataApp.aBTotOI_rep);
            SetResult("aBSpPosCf", dataApp.aBSpPosCf_rep);
            SetResult("aCBaseI", dataApp.aCBaseI_rep);
            SetResult("aCBonusesI", dataApp.aCBonusesI_rep);
            SetResult("aCCommisionI", dataApp.aCCommisionI_rep);
            SetResult("aCDividendI", dataApp.aCDividendI_rep);
            SetResult("aCNetRentI1003", dataApp.aCNetRentI1003_rep);
            SetResult("aCOvertimeI", dataApp.aCOvertimeI_rep);
            SetResult("aCTotI", dataApp.aCTotI_rep);
            SetResult("aCTotOI", dataApp.aCTotOI_rep);
            SetResult("aCSpPosCf", dataApp.aCSpPosCf_rep);
            SetResult("aLiaBalTot", dataApp.aLiaBalTot_rep);
            SetResult("aLiaMonTot", dataApp.aLiaMonTot_rep);
            SetResult("aNetRentI1003Lckd", dataApp.aNetRentI1003Lckd);
            SetResult("aNetWorth", dataApp.aNetWorth_rep);
            SetResult("OtherIncomeJson", ObsoleteSerializationHelper.JsonSerialize( dataApp.aOtherIncomeList) );
            SetResult("aPres1stM", dataApp.aPres1stM_rep);
            SetResult("aPresHazIns", dataApp.aPresHazIns_rep);
            SetResult("aPresHoAssocDues", dataApp.aPresHoAssocDues_rep);
            SetResult("aPresMIns", dataApp.aPresMIns_rep);
            SetResult("aPresOFin", dataApp.aPresOFin_rep);
            SetResult("aPresOHExp", dataApp.aPresOHExp_rep);
            SetResult("aPresRealETx", dataApp.aPresRealETx_rep);
            SetResult("aPresRent", dataApp.aPresRent_rep);
            SetResult("aPresTotHExp", dataApp.aPresTotHExp_rep);
            SetResult("aReTotVal", dataApp.aReTotVal_rep);
            SetResult("aTotBaseI", dataApp.aTotBaseI_rep);
            SetResult("aTotBonusesI", dataApp.aTotBonusesI_rep);
            SetResult("aTotCommisionI", dataApp.aTotCommisionI_rep);
            SetResult("aTotDividendI", dataApp.aTotDividendI_rep);
            SetResult("aTotI", dataApp.aTotI_rep);
            SetResult("aTotNetRentI1003", dataApp.aTotNetRentI1003_rep);
            SetResult("aTotOI", dataApp.aTotOI_rep);
            SetResult("aTotSpPosCf", dataApp.aTotSpPosCf_rep);
            SetResult("aTotOvertimeI", dataApp.aTotOvertimeI_rep);
            SetResult("sMonthlyPmt", dataLoan.sMonthlyPmt_rep);
            SetResult("sProFirstMPmt", dataLoan.sProFirstMPmt_rep);
            SetResult("sProHazIns", dataLoan.sProHazIns_rep);
            SetResult("sProHazInsBaseAmt", dataLoan.sProHazInsBaseAmt_rep);
            SetResult("sProHazInsMb", dataLoan.sProHazInsMb_rep);
            SetResult("sProHazInsR", dataLoan.sProHazInsR_rep);
            SetResult("sProHazInsT", dataLoan.sProHazInsT);
            SetResult("sProHoAssocDues", dataLoan.sProHoAssocDues_rep);
            SetResult("sProMIns", dataLoan.sProMIns_rep);
            SetResult("sProOHExp", dataLoan.sProOHExp_rep);
            SetResult("sProOHExpDesc", dataLoan.sProOHExpDesc);
            SetResult("sProRealETx", dataLoan.sProRealETx_rep);
            SetResult("sProRealETxBaseAmt", dataLoan.sProRealETxBaseAmt_rep);
            SetResult("sProRealETxMb", dataLoan.sProRealETxMb_rep);
            SetResult("sProRealETxR", dataLoan.sProRealETxR_rep);
            SetResult("sProRealETxT", dataLoan.sProRealETxT);
            SetResult("sProSecondMPmt", dataLoan.sProSecondMPmt_rep);
            SetResult("sProOHExpLckd", dataLoan.sProOHExpLckd);

        }
    }

    public class Loan1003Pg3ServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        private E_sDisclosureRegulationT disclosureRegulationTBeforeBind;
        private E_sDisclosureRegulationT disclosureRegulationTOnLoad;

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(Loan1003Pg3ServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            this.disclosureRegulationTBeforeBind = dataLoan.sDisclosureRegulationT;

            dataApp.aAltNm1 = GetString("aAltNm1");
            dataApp.aAltNm1AccNum = GetString("aAltNm1AccNum");
            dataApp.aAltNm1CreditorNm = GetString("aAltNm1CreditorNm");
            dataApp.aAltNm2 = GetString("aAltNm2");
            dataApp.aAltNm2AccNum = GetString("aAltNm2AccNum");
            dataApp.aAltNm2CreditorNm = GetString("aAltNm2CreditorNm");
            dataApp.aBDecAlimony = GetString("aBDecAlimony").ToUpper();
            dataApp.aBDecBankrupt = GetString("aBDecBankrupt").ToUpper();
            dataApp.aBDecBorrowing = GetString("aBDecBorrowing").ToUpper();
            dataApp.aBDecCitizen = GetString("aBDecCitizen").ToUpper();
            dataApp.aBDecDelinquent = GetString("aBDecDelinquent").ToUpper();
            dataApp.aBDecEndorser = GetString("aBDecEndorser").ToUpper();
            dataApp.aBDecForeclosure = GetString("aBDecForeclosure").ToUpper();
            dataApp.aBDecJudgment = GetString("aBDecJudgment").ToUpper();
            dataApp.aBDecLawsuit = GetString("aBDecLawsuit").ToUpper();
            dataApp.aBDecObligated = GetString("aBDecObligated").ToUpper();
            dataApp.aBDecOcc = GetString("aBDecOcc").ToUpper();
            dataApp.aBDecPastOwnedPropT = (E_aBDecPastOwnedPropT)GetInt("aBDecPastOwnedPropT");
            dataApp.aBDecPastOwnedPropTitleT = (E_aBDecPastOwnedPropTitleT)GetInt("aBDecPastOwnedPropTitleT");
            dataApp.aBDecPastOwnership = GetString("aBDecPastOwnership").ToUpper();
            dataApp.aBDecResidency = GetString("aBDecResidency").ToUpper();
            
            dataApp.aCDecAlimony = GetString("aCDecAlimony").ToUpper();
            dataApp.aCDecBankrupt = GetString("aCDecBankrupt").ToUpper();
            dataApp.aCDecBorrowing = GetString("aCDecBorrowing").ToUpper();
            dataApp.aCDecCitizen = GetString("aCDecCitizen").ToUpper();
            dataApp.aCDecDelinquent = GetString("aCDecDelinquent").ToUpper();
            dataApp.aCDecEndorser = GetString("aCDecEndorser").ToUpper();
            dataApp.aCDecForeclosure = GetString("aCDecForeclosure").ToUpper();
            dataApp.aCDecJudgment = GetString("aCDecJudgment").ToUpper();
            dataApp.aCDecLawsuit = GetString("aCDecLawsuit").ToUpper();
            dataApp.aCDecObligated = GetString("aCDecObligated").ToUpper();
            dataApp.aCDecOcc = GetString("aCDecOcc").ToUpper();
            dataApp.aCDecPastOwnedPropT = (E_aCDecPastOwnedPropT)GetInt("aCDecPastOwnedPropT");
            dataApp.aCDecPastOwnedPropTitleT = (E_aCDecPastOwnedPropTitleT)GetInt("aCDecPastOwnedPropTitleT");
            dataApp.aCDecPastOwnership = GetString("aCDecPastOwnership").ToUpper();
            dataApp.aCDecResidency = GetString("aCDecResidency").ToUpper();
            
            dataApp.aIntrvwrMethodT = (E_aIntrvwrMethodT)GetInt("aIntrvwrMethodT");
            dataApp.aIntrvwrMethodTLckd = GetBool("aIntrvwrMethodTLckd");
            dataApp.aBDecForeignNational = GetString("aBDecForeignNational").ToUpper();
            dataLoan.sAltCostLckd = GetBool("sAltCostLckd");
            dataLoan.sAltCost_rep = GetString("sAltCost");
            dataLoan.sFfUfmip1003Lckd = GetBool("sFfUfmip1003Lckd");
            dataLoan.sFfUfmip1003_rep = GetString("sFfUfmip1003");
            dataLoan.sLDiscnt1003Lckd = GetBool("sLDiscnt1003Lckd");
            dataLoan.sLDiscnt1003_rep = GetString("sLDiscnt1003");
            dataLoan.sLandCost_rep = GetString("sLandCost");
            dataLoan.sOCredit1Amt_rep = GetString("sOCredit1Amt");
            dataLoan.sOCredit1Desc = GetString("sOCredit1Desc");
            dataLoan.sOCredit1Lckd = GetBool("sOCredit1Lckd");

            if (false == dataLoan.sLoads1003LineLFromAdjustments)
            {
                dataLoan.sOCredit2Amt_rep = GetString("sOCredit2Amt");
                dataLoan.sOCredit2Desc = GetString("sOCredit2Desc");
                dataLoan.sOCredit3Amt_rep = GetString("sOCredit3Amt");
                dataLoan.sOCredit3Desc = GetString("sOCredit3Desc");
                dataLoan.sOCredit4Amt_rep = GetString("sOCredit4Amt");
                dataLoan.sOCredit4Desc = GetString("sOCredit4Desc");
            }

            dataLoan.sPurchPrice_rep = GetString("sPurchPrice");
            dataLoan.sRefPdOffAmt1003Lckd = GetBool("sRefPdOffAmt1003Lckd");
            dataLoan.sRefPdOffAmt1003_rep = GetString("sRefPdOffAmt1003");
            dataLoan.sTotCcPbsLocked = GetBool("sTotCcPbsLocked");
            dataLoan.sTotCcPbs_rep = GetString("sTotCcPbs");
            dataLoan.sTotEstCc1003Lckd = GetBool("sTotEstCc1003Lckd");
            dataLoan.sTotEstCcNoDiscnt1003_rep = GetString("sTotEstCcNoDiscnt1003");
            dataLoan.sTotEstPp1003Lckd = GetBool("sTotEstPp1003Lckd");
            dataLoan.sTotEstPp1003_rep = GetString("sTotEstPp1003");
            dataLoan.sTransNetCashLckd = GetBool("sTransNetCashLckd");
            dataLoan.sTransNetCash_rep = GetString("sTransNetCash");

            if (!dataLoan.sIsIncludeONewFinCcInTotEstCc)
            {
                dataLoan.sONewFinCc_rep = GetString("sONewFinCc");
            }
            else
            {
                dataLoan.sONewFinCc_rep = GetString("sONewFinCc2");
            }

            dataLoan.sLAmtCalc_rep = GetString("sLAmtCalc");
            dataLoan.sLAmtLckd = GetBool("sLAmtLckd");

            Loan1003pg3.BindDataFromControls(dataLoan, dataApp, this);

            dataApp.a1003InterviewD_rep = GetString("a1003InterviewD");
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V17_UpdateApplicationSubmittedCalculation))
            {
                dataApp.a1003InterviewDLckd = GetBool("a1003InterviewDLckd");
            }

            IPreparerFields interviewer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
            interviewer.PreparerName = GetString("LoanOfficerName");
            interviewer.LicenseNumOfAgent = GetString("LoanOfficerLicenseNumber");
            interviewer.LicenseNumOfCompany = GetString("BrokerLicenseNumber");
            interviewer.Phone = GetString("LoanOfficerPhone");
            interviewer.EmailAddr = GetString("LoanOfficerEmail");
            interviewer.CompanyName = GetString("BrokerName");
            interviewer.StreetAddr = GetString("BrokerStreetAddr");
            interviewer.City = GetString("BrokerCity");
            interviewer.State = GetString("BrokerState");
            interviewer.Zip = GetString("BrokerZip");
            interviewer.PhoneOfCompany = GetString("BrokerPhone");
            interviewer.FaxOfCompany = GetString("BrokerFax");
            interviewer.AgentRoleT = (E_AgentRoleT)GetInt("CFM_AgentRoleT");
            interviewer.LoanOriginatorIdentifier = GetString("LoanOfficerLoanOriginatorIdentifier");
            interviewer.CompanyLoanOriginatorIdentifier = GetString("LoanOfficerCompanyLoanOriginatorIdentifier");
            interviewer.IsLocked = GetBool("CFM_IsLocked");
            interviewer.Update();
            
            dataApp.aBDecJudgmentExplanation = GetString("aBDecJudgmentExplanation");
            dataApp.aCDecJudgmentExplanation = GetString("aCDecJudgmentExplanation");
            dataApp.aBDecBankruptExplanation = GetString("aBDecBankruptExplanation");
            dataApp.aCDecBankruptExplanation = GetString("aCDecBankruptExplanation");
            dataApp.aBDecForeclosureExplanation = GetString("aBDecForeclosureExplanation");
            dataApp.aCDecForeclosureExplanation = GetString("aCDecForeclosureExplanation");
            dataApp.aBDecLawsuitExplanation = GetString("aBDecLawsuitExplanation");
            dataApp.aCDecLawsuitExplanation = GetString("aCDecLawsuitExplanation");
            dataApp.aBDecObligatedExplanation = GetString("aBDecObligatedExplanation");
            dataApp.aCDecObligatedExplanation = GetString("aCDecObligatedExplanation");
            dataApp.aBDecDelinquentExplanation = GetString("aBDecDelinquentExplanation");
            dataApp.aCDecDelinquentExplanation = GetString("aCDecDelinquentExplanation");
            dataApp.aBDecAlimonyExplanation = GetString("aBDecAlimonyExplanation");
            dataApp.aCDecAlimonyExplanation = GetString("aCDecAlimonyExplanation");
            dataApp.aBDecBorrowingExplanation = GetString("aBDecBorrowingExplanation");
            dataApp.aCDecBorrowingExplanation = GetString("aCDecBorrowingExplanation");
            dataApp.aBDecEndorserExplanation = GetString("aBDecEndorserExplanation");
            dataApp.aCDecEndorserExplanation = GetString("aCDecEndorserExplanation");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            this.disclosureRegulationTOnLoad = dataLoan.sDisclosureRegulationT;

            SetResult("aAltNm1", dataApp.aAltNm1);
            SetResult("aAltNm1AccNum", dataApp.aAltNm1AccNum.Value);
            SetResult("aAltNm1CreditorNm", dataApp.aAltNm1CreditorNm);
            SetResult("aAltNm2", dataApp.aAltNm2);
            SetResult("aAltNm2AccNum", dataApp.aAltNm2AccNum.Value);
            SetResult("aAltNm2CreditorNm", dataApp.aAltNm2CreditorNm);
            SetResult("aBDecAlimony", dataApp.aBDecAlimony.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecBankrupt", dataApp.aBDecBankrupt.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecBorrowing", dataApp.aBDecBorrowing.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecCitizen", dataApp.aBDecCitizen.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecDelinquent", dataApp.aBDecDelinquent.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecEndorser", dataApp.aBDecEndorser.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecForeclosure", dataApp.aBDecForeclosure.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecJudgment", dataApp.aBDecJudgment.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecLawsuit", dataApp.aBDecLawsuit.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecObligated", dataApp.aBDecObligated.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecOcc", dataApp.aBDecOcc.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecPastOwnedPropT", dataApp.aBDecPastOwnedPropT);
            SetResult("aBDecPastOwnedPropTitleT", dataApp.aBDecPastOwnedPropTitleT);
            SetResult("aBDecPastOwnership", dataApp.aBDecPastOwnership.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecResidency", dataApp.aBDecResidency.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecAlimony", dataApp.aCDecAlimony.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecBankrupt", dataApp.aCDecBankrupt.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecBorrowing", dataApp.aCDecBorrowing.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecCitizen", dataApp.aCDecCitizen.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecDelinquent", dataApp.aCDecDelinquent.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecEndorser", dataApp.aCDecEndorser.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecForeclosure", dataApp.aCDecForeclosure.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecJudgment", dataApp.aCDecJudgment.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecLawsuit", dataApp.aCDecLawsuit.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecObligated", dataApp.aCDecObligated.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecOcc", dataApp.aCDecOcc.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecPastOwnedPropT", dataApp.aCDecPastOwnedPropT);
            SetResult("aCDecPastOwnedPropTitleT", dataApp.aCDecPastOwnedPropTitleT);
            SetResult("aCDecPastOwnership", dataApp.aCDecPastOwnership.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecResidency", dataApp.aCDecResidency.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aIntrvwrMethodT", dataApp.aIntrvwrMethodT);
            SetResult("aIntrvwrMethodTLckd", dataApp.aIntrvwrMethodTLckd);
            SetResult("aReTotGrossRentI", dataApp.aReTotGrossRentI_rep);
            SetResult("aReTotHExp", dataApp.aReTotHExp_rep);
            SetResult("aReTotMAmt", dataApp.aReTotMAmt_rep);
            SetResult("aReTotMPmt", dataApp.aReTotMPmt_rep);
            SetResult("aReTotNetRentI", dataApp.aReTotNetRentI_rep);
            SetResult("aReTotVal", dataApp.aReTotVal_rep);
            SetResult("aBDecForeignNational", dataApp.aBDecForeignNational.ToUpper().TrimWhitespaceAndBOM());
            SetResult("sAltCostLckd", dataLoan.sAltCostLckd);
            SetResult("sAltCost", dataLoan.sAltCost_rep);
            SetResult("sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
            SetResult("sFfUfmip1003Lckd", dataLoan.sFfUfmip1003Lckd);
            SetResult("sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sLAmt1003", dataLoan.sLAmt1003_rep);
            SetResult("sLDiscnt1003", dataLoan.sLDiscnt1003_rep);
            SetResult("sLDiscnt1003Lckd", dataLoan.sLDiscnt1003Lckd);
            SetResult("sLandCost", dataLoan.sLandCost_rep);
            SetResult("sOCredit1Amt", dataLoan.sOCredit1Amt_rep);
            SetResult("sOCredit1Desc", dataLoan.sOCredit1Desc);
            SetResult("sOCredit1Lckd", dataLoan.sOCredit1Lckd);
            SetResult("sOCredit2Amt", dataLoan.sOCredit2Amt_rep);
            SetResult("sOCredit2Desc", dataLoan.sOCredit2Desc);
            SetResult("sOCredit3Amt", dataLoan.sOCredit3Amt_rep);
            SetResult("sOCredit3Desc", dataLoan.sOCredit3Desc);
            SetResult("sOCredit4Amt", dataLoan.sOCredit4Amt_rep);
            SetResult("sOCredit4Desc", dataLoan.sOCredit4Desc);
            SetResult("sOCredit5Amt", dataLoan.sOCredit5Amt_rep);
            SetResult("sONewFinBal", dataLoan.sONewFinBal_rep);
            SetResult("sPurchPrice", dataLoan.sPurchPrice_rep);
            SetResult("sRefPdOffAmt1003", dataLoan.sRefPdOffAmt1003_rep);
            SetResult("sRefPdOffAmt1003Lckd", dataLoan.sRefPdOffAmt1003Lckd);
            SetResult("sTotCcPbs", dataLoan.sTotCcPbs_rep);
            SetResult("sTotCcPbsLocked", dataLoan.sTotCcPbsLocked);
            SetResult("sTotEstCc1003Lckd", dataLoan.sTotEstCc1003Lckd);
            SetResult("sTotEstCcNoDiscnt1003", dataLoan.sTotEstCcNoDiscnt1003_rep);
            SetResult("sTotEstPp1003", dataLoan.sTotEstPp1003_rep);
            SetResult("sTotEstPp1003Lckd", dataLoan.sTotEstPp1003Lckd);
            SetResult("sTotTransC", dataLoan.sTotTransC_rep);
            SetResult("sTransNetCash", dataLoan.sTransNetCash_rep);
            SetResult("sTransNetCashLckd", dataLoan.sTransNetCashLckd);
            SetResult("sONewFinCc", dataLoan.sONewFinCc_rep);
            SetResult("sONewFinCc2", dataLoan.sONewFinCc_rep);
            SetResult("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            SetResult("sLAmtLckd", dataLoan.sLAmtLckd);
            SetResult("sTotEstCcNoDiscnt", dataLoan.sTotEstCcNoDiscnt_rep);
            SetResult("sTotEstPp", dataLoan.sTotEstPp_rep);
            SetResult("sTotalBorrowerPaidProrations", dataLoan.sTotalBorrowerPaidProrations_rep);
            SetResult("a1003InterviewD", dataApp.a1003InterviewD_rep);

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V17_UpdateApplicationSubmittedCalculation))
            {
                SetResult("a1003InterviewDLckd", dataApp.a1003InterviewDLckd);
            }

            IPreparerFields interviewer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("LoanOfficerName", interviewer.PreparerName);
            SetResult("LoanOfficerLicenseNumber", interviewer.LicenseNumOfAgent);
            SetResult("BrokerLicenseNumber", interviewer.LicenseNumOfCompany);
            SetResult("LoanOfficerPhone", interviewer.Phone);
            SetResult("LoanOfficerEmail", interviewer.EmailAddr);
            SetResult("BrokerName", interviewer.CompanyName);
            SetResult("BrokerStreetAddr", interviewer.StreetAddr);
            SetResult("BrokerCity", interviewer.City);
            SetResult("BrokerState", interviewer.State);
            SetResult("BrokerZip", interviewer.Zip);
            SetResult("BrokerPhone", interviewer.PhoneOfCompany);
            SetResult("BrokerFax", interviewer.FaxOfCompany);
            SetResult("LoanOfficerLoanOriginatorIdentifier", interviewer.LoanOriginatorIdentifier);
            SetResult("LoanOfficerCompanyLoanOriginatorIdentifier", interviewer.CompanyLoanOriginatorIdentifier);

            bool refreshNavigation = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                this.disclosureRegulationTBeforeBind != this.disclosureRegulationTOnLoad;
            if (refreshNavigation)
            {
                SetResult("RefreshNavigation", refreshNavigation);
            }

            Loan1003pg3.LoadDataForControls(dataLoan, dataApp, this);
        }
    }

    public class Loan1003Pg4ServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(Loan1003Pg4ServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.a1003ContEditSheet = GetString("a1003ContEditSheet");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }


	public partial class Loan1003Service : LendersOffice.Common.BaseSimpleServiceXmlPage
	{

        protected override void Initialize()
        {
            AddBackgroundItem("Loan1003pg1", new Loan1003Pg1ServiceItem());
            AddBackgroundItem("Loan1003pg2", new Loan1003Pg2ServiceItem());
            AddBackgroundItem("Loan1003pg3", new Loan1003Pg3ServiceItem());
            AddBackgroundItem("Loan1003pg4", new Loan1003Pg4ServiceItem());
        }
	}
}
