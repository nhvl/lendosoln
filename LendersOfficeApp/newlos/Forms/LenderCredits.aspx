﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="LenderCredits.aspx.cs" Inherits="LendersOfficeApp.newlos.Forms.LenderCredits" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Lender Credits / Discount</title>
    <style type="text/css">
        .Indented
        {
            padding-left: 80px;
        }

        .Nowrap
        {
            white-space: nowrap;
        }

        .AlignRight
        {
            text-align: right;
        }
    </style>
</head>
<body bgcolor="gainsboro">
<script type="text/javascript">
function _init() {
    var v = <%= AspxTools.JQuery(sLenderCreditCalculationMethodT) %>.val();
    $('#FrontEndPanel').toggle(v == 1);
    $('#ManualCalculatePanel').toggle(v == 0);
    $('#ManualSummaryPanel').toggle(v == 0);

    $(<%= AspxTools.JsGetElementById(sLenderAdditionalCreditAtClosingAmt_Neg2) %>).toggle(v==1);
    $(<%= AspxTools.JsGetElementById(sLenderAdditionalCreditAtClosingManualAmt_Neg) %>).toggle(v==0);
    
    var sToleranceCureCalculationT = <%= AspxTools.JQuery(sToleranceCureCalculationT) %>.val();
    
    var bDisplayManualToleranceCure = v == 0 && sToleranceCureCalculationT==1;
    $('#ManualTolerancePanel').toggle(bDisplayManualToleranceCure);
    $('#toleranceLabel').toggle(bDisplayManualToleranceCure);
    <%= AspxTools.JQuery(sToleranceCure_Neg2) %>.toggle(bDisplayManualToleranceCure);
    
    var sLenderCreditMaxT = <%= AspxTools.JQuery(sLenderCreditMaxT) %>.val();
    
    <%= AspxTools.JsGetElementById(sLenderCreditMaxAmt_Neg)%>.readOnly = sLenderCreditMaxT != 4;
    
    $('#tolerancesection').toggle(<%= AspxTools.JsBool(sIsAllowExcludeToleranceCure) %>);
    
    lockField(<%= AspxTools.JsGetElementById(sToleranceZeroPercentCureLckd2) %>, 'sToleranceZeroPercentCure2');
    lockField(<%= AspxTools.JsGetElementById(sToleranceTenPercentCureLckd2) %>, 'sToleranceTenPercentCure2');
    lockField(<%= AspxTools.JsGetElementById(sToleranceZeroPercentCureLckd) %>, 'sToleranceZeroPercentCure');
    lockField(<%= AspxTools.JsGetElementById(sToleranceTenPercentCureLckd) %>, 'sToleranceTenPercentCure');
}

function syncCheckboxes(sourceId, targetId)
{
    document.getElementById(targetId).checked = document.getElementById(sourceId).checked;
}

</script>
    <form id="form1" runat="server">
    <div>
    <table>
        <tr><td class="MainRightHeader">Lender Credits / Discount</td></tr>
        <tr>
        <td>
        <table cellspacing="2" cellpadding="0" width="98%" border="0">
            <tr>
                <td colspan="2" class="FieldLabel">Credit/charge calculation method</td>
                <td colspan="6"><asp:DropDownList runat="server" ID="sLenderCreditCalculationMethodT" onchange="refreshCalculation();"/></td>
            </tr>
            <tr>
                <td colspan="8"><hr /></td>
            </tr>
            <tr><td colspan="8">&nbsp;</td></tr>
            <tbody id="FrontEndPanel">
            <tr>
                <td class="FieldLabel" colspan="2">Credit/Charge is based on</td>
                <td class="AlignRight"><asp:DropDownList ID="sLDiscntBaseT2" runat="server"  onchange="refreshCalculation();"/></td>
                <td><ml:MoneyTextBox ID="sLDiscntBaseAmt" runat="server" Width="90" preset="money" ReadOnly="true" /></td>
            </tr>
            <tr><td colspan="8">&nbsp;</td></tr>
            <tr>
                <td class="FieldLabel Indented"><ml:EncodedLiteral ID="originatorPriceLabel" runat="server" Text="Originator Price" /></td>
                <td></td>
                <td class="AlignRight"><ml:PercentTextBox ID="sBrokerLockFinalBrokComp1PcPrice" runat="server" Width="70" preset="percent" ReadOnly="true"/></td>
                <td style="padding-right:15px"><ml:MoneyTextBox ID="sBrokerLockFinalBrokComp1PcAmt" runat="server" width="90" preset="money" ReadOnly="true"/></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="FieldLabel Indented"><ml:EncodedLiteral ID="lenderPaidCompLabel" runat="server" Text="Lender-paid broker compensation" /></td>
                <td></td>
                <td class="AlignRight"><ml:PercentTextBox ID="sLenderPaidBrokerCompPc" runat="server" Width="70" preset="percent" ReadOnly="true" /></td>
                <td><ml:MoneyTextBox ID="sLenderPaidBrokerCompF" runat="server" width="90" preset="money" ReadOnly="true" /></td>

            </tr>

            <tr>
                <td class="FieldLabel Indented">Final price</td>
                <td></td>
                <td class="AlignRight"><ml:PercentTextBox id="sBrokerLockOriginatorPriceBrokComp1PcPrice" runat="server" Width="70" preset="percent" ReadOnly="true" /></td>
                <td style="padding-right:15px"><ml:MoneyTextBox ID="sBrokerLockOriginatorPriceBrokComp1PcAmt" runat="server" width="90" preset="money" ReadOnly="true" /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td class="FieldLabel Indented">General lender credit limit</td>
                <td></td>
                <td><asp:DropDownList runat="server" id="sLenderCreditMaxT" onchange="refreshCalculation();"/></td>
                <td><ml:MoneyTextBox ID="sLenderCreditMaxAmt_Neg" runat="server" width="90" preset="money" onchange="refreshCalculation();"/></td>
            </tr>
            <tr>
                <td class="FieldLabel Indented">Available credit</td>
                <td></td>
                <td></td>
                <td><ml:MoneyTextBox ID="sLenderCreditAvailableAmt_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="FieldLabel">Disclose credit on</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="Indented"><asp:DropDownList runat="server" id="sLenderPaidFeeDiscloseLocationT" onchange="refreshCalculation();"/></td>
                <td class="FieldLabel" colspan="2">Lender paid fees</td>
                <td><ml:MoneyTextBox id="sLenderPaidFeesAmt_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
            </tr>
            <tr>
                <td class="Indented"><asp:DropDownList ID="sLenderGeneralCreditDiscloseLocationT" runat="server"  onchange="refreshCalculation();"/></td>
                <td class="FieldLabel" colspan="2">General lender credit</td>
                <td><ml:MoneyTextBox ID="sLenderGeneralCreditAmt_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
            </tr>
            <tr><td colspan="8">&nbsp;</td></tr> 
            <tr>
                <td class="Indented"><asp:DropDownList ID="sLenderCustomCredit1DiscloseLocationT" runat="server"  onchange="refreshCalculation();"/></td>
                <td class="FieldLabel" colspan="1"><asp:TextBox runat="server" id="sLenderCustomCredit1Description"/></td>
                <td class="AlignRight"><ml:MoneyTextBox ID="sLenderCustomCredit1Amount" runat="server" width="90" preset="money" onchange="refreshCalculation();"/></td>
                <td><ml:MoneyTextBox ID="sLenderCustomCredit1AmountAsCharge" runat="server" Width="90" preset="money" ReadOnly="true" /></td>
            </tr>
            <tr>
                <td class="Indented"><asp:DropDownList ID="sLenderCustomCredit2DiscloseLocationT" runat="server"  onchange="refreshCalculation();"/></td>
                <td colspan="1"><asp:TextBox ID="sLenderCustomCredit2Description" runat="server" /></td>
                <td class="AlignRight"><ml:MoneyTextBox ID="sLenderCustomCredit2Amount" runat="server" width="90" preset="money" onchange="refreshCalculation();"/></td>
                <td><ml:MoneyTextBox ID="sLenderCustomCredit2AmountAsCharge" runat="server" Width="90" preset="money" ReadOnly="true" /></td>
            </tr>
            <tr><td colspan="8">&nbsp;</td></tr> 
            <tr>
                <td class="FieldLabel Indented Nowrap">Closing/HUD-1</td>
                <td class="FieldLabel Nowrap" colspan="1">Tolerance cure for 0% tolerance fees</td>
                <td class="AlignRight">
                    <ml:MoneyTextBox ID="sToleranceZeroPercentCure" runat="server" Width="90" preset="money"
                        onchange="document.getElementById('sToleranceZeroPercentCure2').value = this.value; refreshCalculation();" />
                </td>
                <td>
                    <ml:MoneyTextBox ID="sToleranceZeroPercentCure_Neg" runat="server" Width="90" preset="money" 
                        onchange="document.getElementById('sToleranceZeroPercentCure_Neg2').value = this.value; refreshCalculation();" />
                </td>
                <td>
                    <asp:CheckBox runat="server" onchange="syncCheckboxes('sToleranceZeroPercentCureLckd', 'sToleranceZeroPercentCureLckd2'); refreshCalculation();"
                        ID="sToleranceZeroPercentCureLckd" />Lock
                </td>
            </tr>
            <tr>
                <td class="FieldLabel Indented Nowrap">Closing/HUD-1</td>
                <td class="FieldLabel Nowrap" colspan="1">Tolerance cure for 10% tolerance fees</td>
                <td class="AlignRight">
                    <ml:MoneyTextBox ID="sToleranceTenPercentCure" runat="server" Width="90" preset="money"
                        onchange="document.getElementById('sToleranceTenPercentCure2').value = this.value; refreshCalculation();" />
                </td>
                <td>
                    <ml:MoneyTextBox ID="sToleranceTenPercentCure_Neg" runat="server" Width="90" preset="money" 
                        onchange="document.getElementById('sToleranceTenPercentCure_Neg2').value = this.value; refreshCalculation();" />
                </td>
                <td>
                    <asp:CheckBox runat="server" onchange="syncCheckboxes('sToleranceTenPercentCureLckd', 'sToleranceTenPercentCureLckd2'); refreshCalculation();"
                        ID="sToleranceTenPercentCureLckd" />Lock
                </td>
                <td></td>
                <td class="FieldLabel"></td>
                <td></td>
            </tr>

            <tr id="lnkToleranceCure" runat="server"><td colspan="7" class="FieldLabel Indented"><a href="#" onclick="linkMe('../ToleranceCure.aspx');">See Tolerance Cure Calculation</a></td></tr>

            <tr><td colspan="4"><hr /></td></tr>

            <tr>
             <td class="FieldLabel">Target discount points</td>
             <td style="padding-right:15px"><ml:MoneyTextBox ID="sLenderTargetDiscountPointAmtAsCreditAmt_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
        </tr>
        <tr>
            <td class="FieldLabel"><ml:EncodedLiteral ID="lenderPaidCompCreditLabel" runat="server" Text="Lender paid broker compensation credit" /></td>
                <td><ml:MoneyTextBox ID="sLenderPaidBrokerCompCreditF_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
                <td class="FieldLabel" nowrap="nowrap">Target initial charge (+)/credit (-)</td>
                <td><ml:MoneyTextBox ID="sLenderTargetInitialCreditAmt_Neg2" data-field-id="sLenderTargetInitialCreditAmt_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
        </tr>
        <tr>
            <td class="FieldLabel" style="border-bottom:solid 1px black;padding-bottom:2px">Initial lender credits to borrower</td>
                <td style="border-bottom:solid 1px black;padding-bottom:2px"><ml:MoneyTextBox ID="sLenderInitialCreditAmt_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
                <td class="FieldLabel" style="border-bottom:solid 1px black;padding-bottom:2px">Target lender credits at closing</td>
                <td style="border-bottom:solid 1px black;padding-bottom:2px"><ml:MoneyTextBox ID="sLenderTargetClosingCreditAmt_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
        </tr>
        <tr>
            <td class="FieldLabel">Target initial charge (+)/credit (-)</td>
                <td><ml:MoneyTextBox ID="sLenderTargetInitialCreditAmt_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
                <td class="FieldLabel">Target total charge (+)/credit (-)</td>
                <td><ml:MoneyTextBox ID="sLenderTargetTotalCreditAmt_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
        </tr>
        <tr>
            <td class="FieldLabel">Last disclosed initial credit</td>
                <td><ml:MoneyTextBox ID="sLenderLastDisclosedInitialCredit_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
                <td class="FieldLabel">Actual initial charge (+)/credit (-)</td>
                <td><ml:MoneyTextBox ID="sLenderActualInitialCreditAmt_Neg2" data-field-id="sLenderActualInitialCreditAmt_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
        </tr>
        <tr>
                <td></td>
                <td></td>
                <td class="FieldLabel">Additional credit at closing</td>
                <td><ml:MoneyTextBox ID="sLenderAdditionalCreditAtClosingAmt_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
        </tr>
        <tr>
                <td>
                    <asp:CheckBox runat="server" ID="sLenderCreditToleranceCureLckd" onchange="refreshCalculation();"
                        class="FieldLabel" style="border-bottom:solid 1px black;padding-bottom:2px" Text="Include lender credit tolerance cure" />
                </td>
                <td style="border-bottom:solid 1px black;padding-bottom:2px"><ml:MoneyTextBox ID="sLenderCreditToleranceCure_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
                <td class="FieldLabel" style="border-bottom:solid 1px black;padding-bottom:2px">Tolerance cures</td>
                <td style="border-bottom:solid 1px black;padding-bottom:2px"><ml:MoneyTextBox ID="sToleranceCure_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
        </tr>
        <tr>
            <td class="FieldLabel">Actual initial charge (+)/credit (-)</td>
                <td><ml:MoneyTextBox ID="sLenderActualInitialCreditAmt_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
                <td class="FieldLabel">Actual total charge (+)/credit (-)</td>
                <td><ml:MoneyTextBox ID="sLenderActualTotalCreditAmt_Neg" runat="server" Width="90" preset="money" ReadOnly="true"/></td>
        </tr>
        <tr>
            <td colspan="8">&nbsp;</td>
        </tr>
        <tr>
                <td></td>
                <td></td>
                <td class="FieldLabel"><ml:EncodedLiteral ID="sLenderPaidBrokerCompF_Neg_Label" runat="server" Text="Lender-paid broker compensation" /></td>
                <td><ml:MoneyTextBox ID="sLenderPaidBrokerCompF_Neg" runat="server" width="90" preset="money" ReadOnly="true" /></td>
        </tr>
        <tr>
                <td></td>
                <td></td>
                <td class="FieldLabel">Net charge (+)/credit(-) from lender</td>
                <td><ml:MoneyTextBox ID="sSettlementTotalPaidByLender_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
        </tr>
            <tr><td colspan="8">&nbsp;</td></tr>
        <tr>
                <td></td>
                <td></td>
                <td class="FieldLabel">Difference from target</td>
                <td><ml:MoneyTextBox ID="sLenderCreditDifferentFromTargetAmt_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
        </tr>
            </tbody>
            <tbody id="ManualCalculatePanel">
            <tr>
                <td></td>
                <td class="FieldLabel"><ml:EncodedLiteral ID="originatorPriceLabel2" runat="server" Text="Originator Price" /></td>
                <td class="AlignRight"><ml:PercentTextBox ID="sBrokerLockFinalBrokComp1PcPrice2" runat="server" Width="70" preset="percent" ReadOnly="true"/></td>
                <td style="padding-right:15px" ><ml:MoneyTextBox ID="sBrokerLockFinalBrokComp1PcAmt2" runat="server" width="90" preset="money" ReadOnly="true"/></td>
            </tr>
            <tr>
                <td></td>
                <td class="FieldLabel"><ml:EncodedLiteral ID="lenderPaidCompLabel2" runat="server" Text="Lender-paid broker compensation" /></td>
                <td class="AlignRight"><ml:PercentTextBox ID="sLenderPaidBrokerCompPc2" runat="server" Width="70" preset="percent" ReadOnly="true" /></td>
                <td><ml:MoneyTextBox ID="sLenderPaidBrokerCompF2" runat="server" width="90" preset="money" ReadOnly="true" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">Final price</td>
                <td></td>
                <td class="AlignRight"><ml:PercentTextBox id="sBrokerLockOriginatorPriceBrokComp1PcPrice2" runat="server" Width="70" preset="percent" ReadOnly="true" /></td>
                <td style="padding-right:15px"><ml:MoneyTextBox ID="sBrokerLockOriginatorPriceBrokComp1PcAmt2" runat="server" width="90" preset="money" ReadOnly="true" /></td>
            </tr>
            <tr>
                <td class="FieldLabel" colspan="2">Disclose credit on</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="FieldLabel Indented">Initial/GFE</td>
                <td class="FieldLabel"></td>
                <td colspan="2" class="FieldLabel AlignRight" nowrap="nowrap" style="padding-right:15px">Credit (-) / Charge (+)</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="2" class="Nowrap"><ml:PercentTextBox id="sLDiscntPc" runat="server" Width="70" preset="percent" onchange="refreshCalculation();"/> of <asp:DropDownList id="sLDiscntBaseT" runat="server"  onchange="refreshCalculation();"/> + <ml:MoneyTextBox id="sLDiscntFMb" runat="server" Width="90" preset="money"  onchange="refreshCalculation();"/></td>
                <td><ml:MoneyTextBox ID="sLDiscnt" runat="server" width="90" preset="money" ReadOnly="true"/></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="FieldLabel Indented">Initial/GFE</td>
                <td class="FieldLabel">Credit for lender paid fees </td>
                <td><asp:DropDownList ID="sGfeCreditLenderPaidItemT" runat="server" onchange="refreshCalculation();" /></td>
                <td><ml:MoneyTextBox id="sGfeCreditLenderPaidItemF" runat="server" width="90" preset="money" ReadOnly="true"/></td>
            </tr>
            <tr>
                <td class="FieldLabel Indented">Initial/GFE</td>
                <td class="FieldLabel" colspan="2">General lender credit</td>
                <td><ml:MoneyTextBox ID="sGfeLenderCreditF" runat="server" width="90" preset="money" ReadOnly="true"/></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="FieldLabel Indented Nowrap">Closing/HUD-1</td>
                <td class="FieldLabel" colspan="2">Lender paid items not included in A2/802</td>
                <td><ml:MoneyTextBox id="sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt" runat="server" Width="90" preset="money" ReadOnly="true" /></td>
                <td colspan="4"></td>
            </tr>
            <tr>
                <td class="FieldLabel Indented Nowrap">Closing/HUD-1</td>
                <td><asp:TextBox runat="server" id="sLenderCustomCredit1Description2"/></td>
                <td class="AlignRight"><ml:MoneyTextBox ID="sLenderCustomCredit1Amount2" runat="server" width="90" preset="money" onchange="refreshCalculation();"/></td>
                <td><ml:MoneyTextBox ID="sLenderCustomCredit1AmountAsCharge2" runat="server" Width="90" preset="money" ReadOnly="true" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="FieldLabel Indented Nowrap">Closing/HUD-1</td>
                <td><asp:TextBox ID="sLenderCustomCredit2Description2" runat="server" /></td>
                <td class="AlignRight"><ml:MoneyTextBox ID="sLenderCustomCredit2Amount2" runat="server" width="90" preset="money" onchange="refreshCalculation();"/></td>
                <td><ml:MoneyTextBox ID="sLenderCustomCredit2AmountAsCharge2" runat="server" Width="90" preset="money" ReadOnly="true" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr><td colspan="8">&nbsp;</td></tr> 
            <tr id="tolerancesection">
                <td><asp:DropDownList runat="server" id="sToleranceCureCalculationT" onchange="refreshCalculation();"/></td>
                <td colspan="2" class="FieldLabel">Tolerance cures in the total credit to closing</td>
                <td colspan="5"></td>
            </tr>
            </tbody>
            <tbody id="ManualTolerancePanel">
            <tr>
                <td class="FieldLabel Indented Nowrap">Closing/HUD-1</td>
                <td colspan="1" class="FieldLabel Nowrap">Tolerance cure for 0% tolerance fees</td>
                <td class="AlignRight">
                    <ml:MoneyTextBox ID="sToleranceZeroPercentCure2" runat="server" Width="90" preset="money"
                        onchange="document.getElementById('sToleranceZeroPercentCure').value = this.value; refreshCalculation();" />
                </td>
                <td>
                    <ml:MoneyTextBox onchange="document.getElementById('sToleranceZeroPercentCure_Neg').value = this.value; refreshCalculation();"
                        ID="sToleranceZeroPercentCure_Neg2" runat="server" Width="90" preset="money" />
                </td>
                <td>
                    <asp:CheckBox onchange="syncCheckboxes('sToleranceZeroPercentCureLckd', 'sToleranceZeroPercentCureLckd'); refreshCalculation();"
                        runat="server" ID="sToleranceZeroPercentCureLckd2" />Lock
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="FieldLabel Indented Nowrap">Closing/HUD-1</td>
                <td colspan="1" class="FieldLabel Nowrap">Tolerance cure for 10% tolerance fees</td>
                <td class="AlignRight">
                    <ml:MoneyTextBox ID="sToleranceTenPercentCure2" runat="server" Width="90" preset="money"
                        onchange="document.getElementById('sToleranceTenPercentCure').value = this.value; refreshCalculation();" />
                </td>
                <td>
                    <ml:MoneyTextBox onchange="document.getElementById('sToleranceTenPercentCure_Neg').value = this.value; refreshCalculation();"
                        ID="sToleranceTenPercentCure_Neg2" runat="server" Width="90" preset="money" ></ml:MoneyTextBox>
                </td>
                <td>
                    <asp:CheckBox onchange="syncCheckboxes('sToleranceTenPercentCureLckd2', 'sToleranceTenPercentCureLckd'); refreshCalculation();"
                        runat="server" ID="sToleranceTenPercentCureLckd2" />Lock
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr id="lnkToleranceCure2" runat="server"><td colspan="7" class="FieldLabel Indented"><a href="#" onclick="linkMe('../ToleranceCure.aspx');">See Tolerance Cure Calculation</a></td></tr>
            </tbody>
            <tbody id="ManualSummaryPanel">
                <tr><td colspan="5"><hr /></td></tr>
                <tr>
                    <td class="FieldLabel" nowrap="nowrap"><ml:EncodedLiteral ID="lenderPaidCompCreditLabel2" runat="server" Text="Lender-paid broker compensation credit" /></td>
                        <td></td>
                        <td></td>
                    <td><ml:MoneyTextBox ID="sLenderPaidBrokerCompCreditF_Neg2" data-field-id="sLenderPaidBrokerCompCreditF_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
                </tr>
                <tr>
                    <td class="FieldLabel">Discount points</td>
                        <td></td>
                        <td colspan="2" style="text-align: right; padding-right: 15px;">
                            <ml:PercentTextBox runat="server" ID="sGfeDiscountPointFPc" Width="90" preset="percent" ReadOnly="true"></ml:PercentTextBox>
                            <ml:MoneyTextBox ID="sGfeDiscountPointF" runat="server" Width="90" preset="money" ReadOnly="true" />
                        </td>
                </tr>
                <tr>
                    <td class="FieldLabel">Initial lender credits to borrower</td>
                        <td></td>
                        <td></td>
                        <td><ml:MoneyTextBox ID="sLenderInitialCreditToBorrowerAmt_Neg" runat="server" Width="90" preset="money" ReadOnly="true" /></td>
                </tr>
                <tr>
                    <td class="FieldLabel">Additional credit at closing</td>
                        <td></td>
                        <td></td>
                        <td>
                            <ml:MoneyTextBox ID="sLenderAdditionalCreditAtClosingManualAmt_Neg" runat="server" Width="90" preset="money" ReadOnly="true" />
                            <ml:MoneyTextBox ID="sLenderAdditionalCreditAtClosingAmt_Neg2" runat="server" Width="90" preset="money" ReadOnly="true" />
                        </td>
                </tr>
                <tr>
                    <td class="FieldLabel"><span id="toleranceLabel">Tolerance cures</span></td>
                        <td></td>
                        <td></td>
                        <td><ml:MoneyTextBox ID="sToleranceCure_Neg2" data-field-id="sToleranceCure_Neg" runat="server" Width="90" preset="money" ReadOnly="true" /></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                </tr>
                <tr>
                    <td class="FieldLabel">Total credit (-) or charge (+) to borrower</td>
                        <td></td>
                        <td></td>
                        <td><ml:MoneyTextBox ID="sLenderActualTotalCreditAmt_Neg2" data-field-id="sLenderActualTotalCreditAmt_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                </tr>
                <tr>
                    <td class="FieldLabel"><ml:EncodedLiteral ID="sLenderPaidBrokerCompF_Neg2_Label" runat="server" Text="Lender-paid broker compensation" /></td>
                        <td></td>
                        <td></td>
                        <td><ml:MoneyTextBox ID="sLenderPaidBrokerCompF_Neg2" data-field-id="sLenderPaidBrokerCompF_Neg" runat="server" width="90" preset="money" ReadOnly="true" /></td>
                </tr>
                <tr>
                     <td class="FieldLabel">Net charge (+)/credit(-) from lender</td>
                        <td></td>
                        <td></td>
                        <td><ml:MoneyTextBox ID="sSettlementTotalPaidByLender_Neg2" data-field-id="sSettlementTotalPaidByLender_Neg" runat="server" width="90" preset="money" ReadOnly="true"/></td>
                </tr>
            </tbody>
        </table>
        </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
