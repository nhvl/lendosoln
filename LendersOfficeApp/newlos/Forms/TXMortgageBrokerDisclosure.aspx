<%@ Page language="c#" Codebehind="TXMortgageBrokerDisclosure.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.TXMortgageBrokerDisclosure" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="UC" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
    <title>TXMortgageBrokerDisclosure</title>
  </head>
  
  <body MS_POSITIONING="FlowLayout" class=RightBackground >
	<script type="text/javascript">
	  function _init() {

	  }
    function disabledField(oCb, oTb) {
      oTb.readOnly = !oCb.checked;
    }	  
	</script>
    <form id="TXMortgageBrokerDisclosure" method="post" runat="server">
<table id=Table1 cellspacing=0 cellpadding=0 border=0>
  <tr>
    <td class=MainRightHeader nowrap>Texas Mortgage Broker/Loan Officer Disclosure</td></tr>
  <tr>
    <td nowrap>
      <table id=Table2 cellspacing=0 cellpadding=0 width="98%" border=0 class=InsetBorder>
      
		<tr>
			<td nowrap class=FieldLabel></td>
			<td nowrap><uc:CFM runat="server" Type="19" AgentLicenseField="TXMortgageBrokerDisclosureLicenseNumOfAgent" AgentNameField="TXMortgageBrokerDisclosurePreparerName"></uc:CFM></td>
		</tr>
        
        <tr>
          <td nowrap class=FieldLabel>Mortgage Broker / Loan Officer</td>
          <td nowrap width="100%"><asp:TextBox id=TXMortgageBrokerDisclosurePreparerName runat="server" /></td>
        </tr>
        
        <tr>
          <td nowrap class=FieldLabel>License Number</td>
          <td nowrap width="100%"><asp:TextBox id=TXMortgageBrokerDisclosureLicenseNumOfAgent runat="server" /></td></tr></table></td></tr>
  <tr>
    <td nowrap>
      <table id=Table3 cellspacing=0 cellpadding=0 width="98%" border=0 class=InsetBorder>
        <tr>
          <td nowrap class=FieldLabel>Duties and Nature of Relationship</td></tr>
        <tr>
          <td nowrap>
            <table id=Table5 cellspacing=0 cellpadding=0 width="100%" 
              border=0>
              <tr>
                <td nowrap><asp:CheckBox id=sTexasDiscWillSubmitToLender runat="server" Text="We will submit your loan application to a participating lender" /></td></tr>
              <tr>
                <td nowrap><asp:CheckBox id=sTexasDiscAsIndependentContractor runat="server" Text="We are acting as an independent contractor and not as your agent." /></td></tr>
              <tr>
                <td nowrap><asp:CheckBox id=sTexasDiscWillActAsFollows runat="server" Text="We will be acting as follows:" /></td></tr>
              <tr>
                <td nowrap><asp:TextBox id=sTexasDiscWillActAsFollowsDesc runat="server" textmode="MultiLine" width="443px" /></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td nowrap>
      <table id=Table4 cellspacing=0 cellpadding=0 width="98%" border=0 class=InsetBorder>
        <tr>
          <td nowrap class=FieldLabel>How we will be compensated</td></tr>
        <tr>
          <td nowrap>
            <table id=Table6 cellspacing=0 cellpadding=0 width="100%" 
              border=0>
              <tr>
                <td nowrap><asp:CheckBox id=sTexasDiscCompensationIncluded runat="server" Text="The retail price we offer you - your interest rate, total points, and fees - will include our compensation." /></td></tr>
              <tr>
                <td>
                    <asp:CheckBox id="sTexasDiscChargeVaried" runat="server" Text="Our pricing for your loan is based upon:" />
                    <br />
                    <asp:TextBox id="sTexasDiscChargeVariedDesc" runat="server" TextMode="MultiLine" Width="443px" />
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<UC:cModalDlg id="CModalDlg1" runat="server"></UC:cModalDlg>
</form>	
</body>
</html>
