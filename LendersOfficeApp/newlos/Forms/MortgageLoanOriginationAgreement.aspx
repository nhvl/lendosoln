<%@ Page language="c#" Codebehind="MortgageLoanOriginationAgreement.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.MortgageLoanOriginationAgreement" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>MortgageLoanOriginationAgreement</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
<body bgColor=gainsboro MS_POSITIONING="FlowLayout">
<form id=MortgageLoanOriginationAgreement method=post 
runat="server">
<table id=Table1 cellspacing=0 cellpadding=0 width="100%" border=0>
  <tr>
    <td class=MainRightHeader nowrap style="PADDING-LEFT:5px">Mortgage Loan Origination Agreement</td></tr>
  <tr>
    <td nowrap>
      <table id=Table2 cellspacing=0 cellpadding=0 border=0>
        <tr>
          <td class=FieldLabel nowrap style="PADDING-LEFT:5px">Broker Name</td>
          <td nowrap><asp:TextBox id=MortgageLoanOrigAgreement_CompanyName runat="server" Width="327px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap style="PADDING-LEFT:5px">Broker Address</td>
          <td nowrap><asp:TextBox id=MortgageLoanOrigAgreement_StreetAddr runat="server" Width="327px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap></td>
          <td nowrap><asp:TextBox id=MortgageLoanOrigAgreement_City runat="server" Width="226px"></asp:TextBox><ml:StateDropDownList id=MortgageLoanOrigAgreement_State runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=MortgageLoanOrigAgreement_Zip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap style="PADDING-LEFT:5px">Broker Phone</td>
          <td nowrap><ml:PhoneTextBox id=MortgageLoanOrigAgreement_PhoneOfCompany runat="server" width="120" preset="phone"></ml:PhoneTextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap style="PADDING-LEFT:5px">Broker Fax</td>
          <td nowrap><ml:PhoneTextBox id=MortgageLoanOrigAgreement_FaxOfCompany runat="server" width="120" preset="phone"></ml:PhoneTextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap style="PADDING-LEFT:5px">Name of Law</td>
          <td nowrap><asp:TextBox id=sMBrokerNmOfLaw runat="server" Width="226px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap style="PADDING-LEFT:5px">Date</td>
          <td nowrap><ml:DateTextBox id=MortgageLoanOrigAgreement_PrepareDate runat="server" width="75" preset="date"></ml:DateTextBox></td></tr></table></td></tr></table></form>
	
  </body>
</html>
