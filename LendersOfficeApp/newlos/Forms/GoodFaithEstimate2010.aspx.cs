using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.AntiXss;
using LendersOfficeApp.los.LegalForm;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using LendersOffice.ObjLib.TitleProvider;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class GoodFaithEstimate2010 : BaseLoanPage
    {
        #region "Member's variables"
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sLOrigFProps_ctrl;

        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn sApprFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn sCrFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn sInspectFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn sMBrokFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn sTxServFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn sFloodCertificationFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn sProcFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn sUwFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn sWireFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn s800U1FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn s800U2FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn s800U3FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn s800U4FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn s800U5FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sIPiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sMipPiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sHazInsPiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  s904PiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sVaFfProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  s900U1PiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sHazInsRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sMInsRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sSchoolTxRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sRealETxRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sFloodInsRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  s1006RsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  s1007RsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sAggregateAdjRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn sEscrowFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn sOwnerTitleInsFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sDocPrepFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sNotaryFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sAttorneyFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sTitleInsFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sU1TcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sU2TcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sU3TcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sU4TcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sRecFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sCountyRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sStateRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sU1GovRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sU2GovRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sU3GovRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sPestInspectFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sU1ScProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sU2ScProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sU3ScProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sU4ScProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn  sU5ScProps_ctrl;

        private LosConvert m_convertLos;
        #endregion 


        protected string ClosingCostPageUrl
        {
            get
            {
                BrokerDB db = this.Broker;
                if (db.AreAutomatedClosingCostPagesVisible)
                {
                    return string.Format("/los/Template/ClosingCost/ClosingCostTemplatePicker.aspx?sLId={0}", LoanID.ToString("N"));
                }
                else
                {
                    return string.Format("/los/view/closingcostlist.aspx?loanid={0}", LoanID.ToString("N"));
                }
            }
        }
			
		protected string sLPurposeT = "";

        protected string m_openedDate ;

        protected bool m_byPassBgCalcForGfeAsDefault = false;

        protected bool m_isChannelBrokered = false;

        protected bool m_isChannelCorrespondent = false;

        protected bool m_isVaLoan = false;

        protected bool m_isFfUfMipIsBeingFinanced = false;

        public override bool IsReadOnly
        {
            get
            {
                return Broker.EnableSubFees || IsArchivePage || base.IsReadOnly;
            }
        }

        protected bool IsArchivePage
        {
            get
            {
                return Broker.IsGFEandCoCVersioningEnabled
                    && (RequestHelper.GetSafeQueryString("IsArchivePage") == "true" || RequestHelper.GetSafeQueryString("IsClosingCostMigrationArchivePage") == "true")
                    && !IsLeadPage;
            }
        }

        protected bool IsClosingCostMigrationArchivePage
        {
            get
            {
                return Broker.IsGFEandCoCVersioningEnabled
                    && RequestHelper.GetSafeQueryString("IsClosingCostMigrationArchivePage") == "true"
                    && !IsLeadPage;
            }
        }

        protected string ClosingCostMigrationArchiveTitle
        {
            get
            {
                return string.Format("{0} 2010 Good Faith Estimate Data", RequestHelper.GetSafeQueryString("MigratedOrDiscarded"));
            }
        }

        protected bool ShowGFEArchiveRecorder
        {
            get
            {
                return Broker.IsGFEandCoCVersioningEnabled && !IsArchivePage && !IsLeadPage;
            }
        }

        protected bool HideComboboxArrows
        {
            get
            {
                return IsArchivePage || Broker.FeeTypeRequirementT == E_FeeTypeRequirementT.Never;
            }
        }

        protected bool HidePaidToAgentPicker
        {
            get
            {
                return IsArchivePage;
            }
        }

        protected bool ShowRequireFeesFromDropDownCheckbox
        {
            get
            {
                return Broker.FeeTypeRequirementT != E_FeeTypeRequirementT.Never
                        && BrokerUser.HasPermission(Permission.AllowEnablingCustomFeeDescriptions);
            }
        }

        private void SetByPassBgCalcForGfeAsDefault() 
        {
            SqlParameter[] parameters = { new SqlParameter("@UserId", BrokerUser.UserId) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "RetrieveByPassBgCalcForGfeAsDefaultByUserId", parameters)) 
            {
                if (reader.Read()) 
                {
                    m_byPassBgCalcForGfeAsDefault = (bool) reader["ByPassBgCalcForGfeAsDefault"];
                }
            }
        }

        private void BindDataObject(LendersOffice.Common.SerializationTypes.IGFEArchive gfea)
        {
            string hintForTodaysDate = "Hint:  Enter 't' for today's date.";

            sMipPiaProps_ctrl.LegacyGfeFieldT = gfea.sMipFrequency == E_MipFrequency.AnnualRateXMonths ? E_LegacyGfeFieldT.sMipPia : E_LegacyGfeFieldT.sUpfrontMipPia;

            hfsLPurposeT.Value = gfea.sLPurposeT.ToString("d");
            
            ByPassBgCalcForGfeAsDefault.SelectedIndex = m_byPassBgCalcForGfeAsDefault ? 1 : 0;
            sConsummationDLckd.Checked = gfea.sConsummationDLckd;
            sConsummationD.Text = gfea.sConsummationD_rep;

            sEstCloseDLckd.Checked = gfea.sEstCloseDLckd;
            sEstCloseD.Text = gfea.sEstCloseD_rep;
            sEstCloseD.ToolTip = hintForTodaysDate;
            m_openedDate = gfea.sOpenedD_rep;
            sDue.Text = gfea.sDue_rep;
            sDue.ReadOnly = gfea.sIsRateLocked || IsReadOnly;

            sTerm.Text = gfea.sTerm_rep;
            sTerm.ReadOnly = gfea.sIsRateLocked || IsReadOnly;

            sSchedDueD1Lckd.Checked = gfea.sSchedDueD1Lckd;
            sSchedDueD1.Text = gfea.sSchedDueD1_rep;
            sSchedDueD1.ToolTip = hintForTodaysDate;
            sNoteIR.Text = gfea.sNoteIR_rep;
            sNoteIR.ReadOnly = gfea.sIsRateLocked || IsReadOnly;

            sFinalLAmt.Text = gfea.sFinalLAmt_rep;

            //CPreparerFields gfeTil = origDataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            GfeTilCompanyName.Text = gfea.gfeTil_CompanyName;
            GfeTilStreetAddr.Text = gfea.gfeTil_StreetAddr;
            GfeTilCity.Text = gfea.gfeTil_City;
            GfeTilState.Value = gfea.gfeTil_State;
            GfeTilZip.Text = gfea.gfeTil_Zip;
            GfeTilPhoneOfCompany.Text = gfea.gfeTil_PhoneOfCompany;
            GfeTilEmailAddr.Text = gfea.gfeTil_EmailAddr;
            if (false == Broker.IsRemovePreparedDates)
            {
                GfeTilPrepareDate.Text = gfea.gfeTil_PrepareDate;
                GfeTilPrepareDate.ToolTip = "Hint:  Enter 't' for today's date.  Enter 'o' for opened date.";
            }
            else
            {
                trPreparedDate.Visible = false;
            }
            CFM.IsLocked = gfea.gfeTil_IsLocked;
            CFM.AgentRoleT = gfea.gfeTil_AgentRoleT;

            Tools.SetDropDownListValue(sFinMethT, gfea.sFinMethT);
            sFinMethT.Enabled = !(gfea.sIsRateLocked || IsReadOnly);
            sDaysInYr.Text = gfea.sDaysInYr_rep;

            sRAdj1stCapR.Text = gfea.sRAdj1stCapR_rep;
            sRAdjCapR.Text = gfea.sRAdjCapR_rep;
            sRAdjLifeCapR.Text = gfea.sRAdjLifeCapR_rep;
            sRAdjCapMon.Text = gfea.sRAdjCapMon_rep;
            sRAdj1stCapMon.Text = gfea.sRAdj1stCapMon_rep;
            sPmtAdjCapR.Text = gfea.sPmtAdjCapR_rep;
            sPmtAdjMaxBalPc.Text = gfea.sPmtAdjMaxBalPc_rep;
            sPmtAdjRecastStop.Text = gfea.sPmtAdjRecastStop_rep;
            sPmtAdjRecastPeriodMon.Text = gfea.sPmtAdjRecastPeriodMon_rep;
            sPmtAdjCapMon.Text = gfea.sPmtAdjCapMon_rep;
            sIOnlyMon.Text = gfea.sIOnlyMon_rep;

            sGfeNoteIRAvailTillDLckd.Checked = gfea.sGfeNoteIRAvailTillDLckd;
            sGfeNoteIRAvailTillD.Text = gfea.sGfeNoteIRAvailTillD_Date;
            sGfeNoteIRAvailTillD_Time.Text = gfea.sGfeNoteIRAvailTillD_Time;
            sGfeEstScAvailTillD.Text = gfea.sGfeEstScAvailTillD_Date;
            sGfeEstScAvailTillDLckd.Checked = gfea.sGfeEstScAvailTillDLckd;
            sGfeEstScAvailTillD_Time.Text = gfea.sGfeEstScAvailTillD_Time;
            sGfeRateLockPeriodLckd.Checked = gfea.sGfeRateLockPeriodLckd;
            sGfeRateLockPeriod.Text = gfea.sGfeRateLockPeriod_rep;
            sGfeLockPeriodBeforeSettlement.Text = gfea.sGfeLockPeriodBeforeSettlement_rep;
            sGfeLockPeriodBeforeSettlement.ReadOnly = gfea.sIsRateLocked || IsReadOnly;
            Tools.SetDropDownListValue(sGfeNoteIRAvailTillDTimeZoneT, gfea.sGfeNoteIRAvailTillDTimeZoneT);
            Tools.SetDropDownListValue(sGfeEstScAvailTillDTimeZoneT, gfea.sGfeEstScAvailTillDTimeZoneT);
            sIsPrintTimeForGfeNoteIRAvailTillD.Checked = gfea.sIsPrintTimeForGfeNoteIRAvailTillD;
            sIsPrintTimeForsGfeEstScAvailTillD.Checked = gfea.sIsPrintTimeForsGfeEstScAvailTillD;


            sGfeCanRateIncrease.SelectedIndex = gfea.sGfeCanRateIncrease ? 1 : 0;
            sRLifeCapR.Text = gfea.sRLifeCapR_rep;
            sGfeFirstInterestChangeIn.Text = gfea.sGfeFirstInterestChangeIn;

            sGfeCanLoanBalanceIncrease.SelectedIndex = gfea.sGfeCanLoanBalanceIncrease ? 1 : 0;
            sGfeMaxLoanBalance.Text = gfea.sGfeMaxLoanBalance_rep;

            sGfeCanPaymentIncrease.SelectedIndex = gfea.sGfeCanPaymentIncrease ? 1 : 0;
            sGfeFirstPaymentChangeIn.Text = gfea.sGfeFirstPaymentChangeIn;
            sGfeFirstAdjProThisMPmtAndMIns.Text = gfea.sGfeFirstAdjProThisMPmtAndMIns_rep;
            sGfeMaxProThisMPmtAndMIns.Text = gfea.sGfeMaxProThisMPmtAndMIns_rep;

            sGfeHavePpmtPenalty.SelectedIndex = gfea.sGfeHavePpmtPenalty ? 1 : 0;
            sGfeMaxPpmtPenaltyAmt.Text = gfea.sGfeMaxPpmtPenaltyAmt_rep;

            sGfeIsBalloon.SelectedIndex = gfea.sGfeIsBalloon ? 1 : 0;
            sGfeBalloonPmt.Text = gfea.sGfeBalloonPmt_rep;
            sGfeBalloonDueInYrs.Text = gfea.sGfeBalloonDueInYrs;

            sMldsHasImpound.SelectedIndex = gfea.sMldsHasImpound ? 1 : 0;

            sIsRequireFeesFromDropDown.Checked = gfea.sIsRequireFeesFromDropDown;
            sGfeUsePaidToFromOfficialContact.Checked = gfea.sGfeUsePaidToFromOfficialContact;
            sGfeRequirePaidToFromContacts.Checked = gfea.sGfeRequirePaidToFromContacts;

            s800U5F.Text = gfea.s800U5F_rep;

            s800U5FDesc.Text = gfea.s800U5FDesc;
            s800U4F.Text = gfea.s800U4F_rep;

            s800U4FDesc.Text = gfea.s800U4FDesc;
            s800U3F.Text = gfea.s800U3F_rep;

            s800U3FDesc.Text = gfea.s800U3FDesc;
            s800U2F.Text = gfea.s800U2F_rep;

            s800U2FDesc.Text = gfea.s800U2FDesc;
            s800U1F.Text = gfea.s800U1F_rep;

            s800U1FDesc.Text = gfea.s800U1FDesc;

            sWireF.Text = gfea.sWireF_rep;

            sUwF.Text = gfea.sUwF_rep;

            sProcF.Text = gfea.sProcF_rep;

            sProcFProps_ctrl.Paid = gfea.sProcFPaid;
            sTxServF.Text = gfea.sTxServF_rep;

            sFloodCertificationF.Text = gfea.sFloodCertificationF_rep;
            Tools.SetDropDownListValue(sFloodCertificationDeterminationT, gfea.sFloodCertificationDeterminationT);

            sMBrokF.Text = gfea.sMBrokF_rep;

            sMBrokFMb.Text = gfea.sMBrokFMb_rep;

            sMBrokFPc.Text = gfea.sMBrokFPc_rep;
            Tools.SetDropDownListValue(sMBrokFBaseT, gfea.sMBrokFBaseT);

            sInspectF.Text = gfea.sInspectF_rep;

            sCrF.Text = gfea.sCrF_rep;

            sCrFProps_ctrl.Paid = gfea.sCrFPaid;
            sApprF.Text = gfea.sApprF_rep;

            sApprFProps_ctrl.Paid = gfea.sApprFPaid;

            sLOrigF.Text = gfea.sLOrigF_rep;

            sLOrigFMb.Text = gfea.sLOrigFMb_rep;

            sLOrigFPc.Text = gfea.sLOrigFPc_rep;

            s900U1Pia.Text = gfea.s900U1Pia_rep;

            s900U1PiaDesc.Text = gfea.s900U1PiaDesc;
            sVaFf.Text = gfea.sVaFf_rep;
            s904Pia.Text = gfea.s904Pia_rep;

            s904PiaDesc.Text = gfea.s904PiaDesc;
            sHazInsPia.Text = gfea.sHazInsPia_rep;

            sHazInsPiaMon.Text = gfea.sHazInsPiaMon_rep;

            Tools.SetDropDownListValue(sProHazInsT, gfea.sProHazInsT);
            sProHazInsR.Text = gfea.sProHazInsR_rep;

            sProHazInsMb.Text = gfea.sProHazInsMb_rep;

            sMipPia.Text = gfea.sMipPia_rep;

            sIPia.Text = gfea.sIPia_rep;

            sIPiaDy.Text = gfea.sIPiaDy_rep;
            sIPiaDyLckd.Checked = gfea.sIPiaDyLckd;

            sIPerDayLckd.Checked = gfea.sIPerDayLckd;
            sIPerDay.Text = gfea.sIPerDay_rep;

            sAggregateAdjRsrv.Text = gfea.sAggregateAdjRsrv_rep;
            sAggregateAdjRsrvLckd.Checked = gfea.sAggregateAdjRsrvLckd;
            s1007Rsrv.Text = gfea.s1007Rsrv_rep;

            s1007ProHExp.Text = gfea.s1007ProHExp_rep;

            s1007RsrvMon.Text = gfea.s1007RsrvMon_rep;

            s1007ProHExpDesc.Text = gfea.s1007ProHExpDesc;
            s1006Rsrv.Text = gfea.s1006Rsrv_rep;

            s1006ProHExp.Text = gfea.s1006ProHExp_rep;

            s1006RsrvMon.Text = gfea.s1006RsrvMon_rep;

            s1006ProHExpDesc.Text = gfea.s1006ProHExpDesc;

            sU3Rsrv.Text = gfea.sU3Rsrv_rep;

            sProU3Rsrv.Text = gfea.sProU3Rsrv_rep;

            sU3RsrvMon.Text = gfea.sU3RsrvMon_rep;

            sU3RsrvDesc.Text = gfea.sU3RsrvDesc;

            sU4Rsrv.Text = gfea.sU4Rsrv_rep;

            sProU4Rsrv.Text = gfea.sProU4Rsrv_rep;

            sU4RsrvMon.Text = gfea.sU4RsrvMon_rep;

            sU4RsrvDesc.Text = gfea.sU4RsrvDesc;

            sFloodInsRsrv.Text = gfea.sFloodInsRsrv_rep;

            sProFloodIns.Text = gfea.sProFloodIns_rep;

            sFloodInsRsrvMon.Text = gfea.sFloodInsRsrvMon_rep;

            sRealETxRsrv.Text = gfea.sRealETxRsrv_rep;

            Tools.SetDropDownListValue(sProRealETxT, gfea.sProRealETxT);
            sProRealETxMb.Text = gfea.sProRealETxMb_rep;

            sProRealETxR.Text = gfea.sProRealETxR_rep;

            sRealETxRsrvMon.Text = gfea.sRealETxRsrvMon_rep;

            sSchoolTxRsrv.Text = gfea.sSchoolTxRsrv_rep;

            sProSchoolTx.Text = gfea.sProSchoolTx_rep;
            sSchoolTxRsrvMon.Text = gfea.sSchoolTxRsrvMon_rep;

            sMInsRsrv.Text = gfea.sMInsRsrv_rep;

            sProMIns.Text = gfea.sProMIns_rep;

            sMInsRsrvMon.Text = gfea.sMInsRsrvMon_rep;

            sHazInsRsrv.Text = gfea.sHazInsRsrv_rep;

            sProHazIns.Text = gfea.sProHazIns_rep;

            sHazInsRsrvMon.Text = gfea.sHazInsRsrvMon_rep;

            sU4Tc.Text = gfea.sU4Tc_rep;

            sU4TcDesc.Text = gfea.sU4TcDesc;
            sU3Tc.Text = gfea.sU3Tc_rep;

            sU3TcDesc.Text = gfea.sU3TcDesc;
            sU2Tc.Text = gfea.sU2Tc_rep;

            sU2TcDesc.Text = gfea.sU2TcDesc;
            sU1Tc.Text = gfea.sU1Tc_rep;

            sU1TcDesc.Text = gfea.sU1TcDesc;
            sTitleInsF.Text = gfea.sTitleInsF_rep;

            sTitleInsFProps_ctrl.SetPaidTo(()=> gfea.sTitleInsFTable);
            sAttorneyF.Text = gfea.sAttorneyF_rep;

            sNotaryF.Text = gfea.sNotaryF_rep;

            sDocPrepF.Text = gfea.sDocPrepF_rep;

            sEscrowF.Text = gfea.sEscrowF_rep;

            sOwnerTitleInsF.Text = gfea.sOwnerTitleInsF_rep;

            sEscrowFProps_ctrl.SetPaidTo(()=> gfea.sEscrowFTable);
            sU3GovRtc.Text = gfea.sU3GovRtc_rep;

            sU3GovRtcMb.Text = gfea.sU3GovRtcMb_rep;

            Tools.SetDropDownListValue(sU3GovRtcBaseT, gfea.sU3GovRtcBaseT);
            sU3GovRtcPc.Text = gfea.sU3GovRtcPc_rep;

            sU3GovRtcDesc.Text = gfea.sU3GovRtcDesc;
            sU2GovRtc.Text = gfea.sU2GovRtc_rep;

            sU2GovRtcMb.Text = gfea.sU2GovRtcMb_rep;

            Tools.SetDropDownListValue(sU2GovRtcBaseT, gfea.sU2GovRtcBaseT);
            sU2GovRtcPc.Text = gfea.sU2GovRtcPc_rep;

            sU2GovRtcDesc.Text = gfea.sU2GovRtcDesc;
            sU1GovRtc.Text = gfea.sU1GovRtc_rep;

            sU1GovRtcMb.Text = gfea.sU1GovRtcMb_rep;

            Tools.SetDropDownListValue(sU1GovRtcBaseT, gfea.sU1GovRtcBaseT);
            sU1GovRtcPc.Text = gfea.sU1GovRtcPc_rep;

            sU1GovRtcDesc.Text = gfea.sU1GovRtcDesc;
            sStateRtcProps_ctrl.SetPaidTo(()=> gfea.sStateRtcDesc);
            sStateRtc.Text = gfea.sStateRtc_rep;

            sStateRtcMb.Text = gfea.sStateRtcMb_rep;

            Tools.SetDropDownListValue(sStateRtcBaseT, gfea.sStateRtcBaseT);
            sStateRtcPc.Text = gfea.sStateRtcPc_rep;

            sCountyRtcProps_ctrl.SetPaidTo(()=> gfea.sCountyRtcDesc);
            sCountyRtcProps_ctrl.SetPaidTo(() => gfea.sCountyRtcDesc);
            sCountyRtc.Text = gfea.sCountyRtc_rep;

            sCountyRtcMb.Text = gfea.sCountyRtcMb_rep;

            Tools.SetDropDownListValue(sCountyRtcBaseT, gfea.sCountyRtcBaseT);
            sCountyRtcPc.Text = gfea.sCountyRtcPc_rep;

            sRecFProps_ctrl.SetPaidTo(()=> gfea.sRecFDesc);
            sRecF.Text = gfea.sRecF_rep;

            sRecFMb.Text = gfea.sRecFMb_rep;

            Tools.SetDropDownListValue(sRecBaseT, gfea.sRecBaseT);
            sRecFPc.Text = gfea.sRecFPc_rep;

            sRecFLckd.Checked = gfea.sRecFLckd;
            sRecDeed.Text = gfea.sRecDeed_rep;
            sRecMortgage.Text = gfea.sRecMortgage_rep;
            sRecRelease.Text = gfea.sRecRelease_rep;

            sU5Sc.Text = gfea.sU5Sc_rep;

            sU5ScDesc.Text = gfea.sU5ScDesc;
            sU4Sc.Text = gfea.sU4Sc_rep;

            sU4ScDesc.Text = gfea.sU4ScDesc;
            sU3Sc.Text = gfea.sU3Sc_rep;

            sU3ScDesc.Text = gfea.sU3ScDesc;
            sU2Sc.Text = gfea.sU2Sc_rep;

            sU2ScDesc.Text = gfea.sU2ScDesc;
            sU1Sc.Text = gfea.sU1Sc_rep;

            sU1ScDesc.Text = gfea.sU1ScDesc;
            sPestInspectF.Text = gfea.sPestInspectF_rep;

            sGfeTotalEstimateSettlementCharge3.Text = gfea.sGfeTotalEstimateSettlementCharge_rep;

            sGfeOriginationF.Text = gfea.sGfeOriginationF_rep;
            sLDiscnt2.Text = gfea.sLDiscnt_rep;
            sGfeAdjOriginationCharge.Text = gfea.sGfeAdjOriginationCharge_rep;
            sGfeRequiredServicesTotalFee.Text = gfea.sGfeRequiredServicesTotalFee_rep;
            sGfeLenderTitleTotalFee.Text = gfea.sGfeLenderTitleTotalFee_rep;
            sGfeOwnerTitleTotalFee.Text = gfea.sGfeOwnerTitleTotalFee_rep;
            sGfeServicesYouShopTotalFee.Text = gfea.sGfeServicesYouShopTotalFee_rep;
            sGfeGovtRecTotalFee.Text = gfea.sGfeGovtRecTotalFee_rep;
            sGfeTransferTaxTotalFee.Text = gfea.sGfeTransferTaxTotalFee_rep;
            sGfeInitialImpoundDeposit.Text = gfea.sGfeInitialImpoundDeposit_rep;
            sGfeDailyInterestTotalFee.Text = gfea.sGfeDailyInterestTotalFee_rep;
            sGfeHomeOwnerInsuranceTotalFee.Text = gfea.sGfeHomeOwnerInsuranceTotalFee_rep;
            sGfeTotalOtherSettlementServiceFee.Text = gfea.sGfeTotalOtherSettlementServiceFee_rep;
            sGfeTotalEstimateSettlementCharge4.Text = gfea.sGfeTotalEstimateSettlementCharge_rep;
            sGfeNotApplicableF.Text = gfea.sGfeNotApplicableF_rep;

            sFinalLAmt2.Text = gfea.sFinalLAmt_rep;
            sNoteIR2.Text = gfea.sNoteIR_rep;
            sGfeTotalEstimateSettlementCharge.Text = gfea.sGfeTotalEstimateSettlementCharge_rep;
            sGfeTradeOffLowerCCLoanAmt.Text = gfea.sGfeTradeOffLowerCCLoanAmt_rep;
            sGfeTradeOffLowerCCNoteIR.Text = gfea.sGfeTradeOffLowerCCNoteIR_rep;
            sGfeTradeOffLowerCCMPmtAndMInsDiff.Text = gfea.sGfeTradeOffLowerCCMPmtAndMInsDiff_rep;
            sGfeTradeOffLowerCCClosingCostDiff.Text = gfea.sGfeTradeOffLowerCCClosingCostDiff_rep;
            sGfeTradeOffLowerCCClosingCost.Text = gfea.sGfeTradeOffLowerCCClosingCost_rep;
            sGfeTradeOffLowerRateLoanAmt.Text = gfea.sGfeTradeOffLowerRateLoanAmt_rep;
            sGfeTradeOffLowerRateNoteIR.Text = gfea.sGfeTradeOffLowerRateNoteIR_rep;
            sGfeTradeOffLowerRateMPmtAndMInsDiff.Text = gfea.sGfeTradeOffLowerRateMPmtAndMInsDiff_rep;
            sGfeTradeOffLowerRateClosingCostDiff.Text = gfea.sGfeTradeOffLowerRateClosingCostDiff_rep;
            sGfeTradeOffLowerRateClosingCost.Text = gfea.sGfeTradeOffLowerRateClosingCost_rep;
            sGfeProThisMPmtAndMIns2.Text = gfea.sGfeProThisMPmtAndMIns_rep;
            sGfeTradeOffLowerCCMPmtAndMIns.Text = gfea.sGfeTradeOffLowerCCMPmtAndMIns_rep;
            sGfeTradeOffLowerRateMPmtAndMIns.Text = gfea.sGfeTradeOffLowerRateMPmtAndMIns_rep;

            GfeTilCompanyName2.Text = gfea.gfeTil_CompanyName;
            sFinalLAmt3.Text = gfea.sFinalLAmt_rep;
            sTerm2.Text = gfea.sTerm_rep;
            sNoteIR3.Text = gfea.sNoteIR_rep;
            sGfeProThisMPmtAndMIns.Text = gfea.sGfeProThisMPmtAndMIns_rep;
            sGfeRateLockPeriod2.Text = gfea.sGfeRateLockPeriod_rep;
            sGfeCanRateIncrease3.Checked = gfea.sGfeCanRateIncrease;
            sGfeCanLoanBalanceIncrease2.Checked = gfea.sGfeCanLoanBalanceIncrease;
            sGfeCanRateIncrease4.Checked = gfea.sGfeCanRateIncrease;
            sGfeHavePpmtPenalty2.Checked = gfea.sGfeHavePpmtPenalty;
            sGfeIsBalloon2.Checked = gfea.sGfeIsBalloon;
            sGfeTotalEstimateSettlementCharge2.Text = gfea.sGfeTotalEstimateSettlementCharge_rep;

            E_TriState yes = E_TriState.Yes;

            sGfeShoppingCartLoan1OriginatorName.Text = gfea.sGfeShoppingCartLoan1OriginatorName;
            sGfeShoppingCartLoan1LoanAmt.Text = gfea.sGfeShoppingCartLoan1LoanAmt_rep;
            sGfeShoppingCartLoan1LoanTerm.Text = gfea.sGfeShoppingCartLoan1LoanTerm_rep;
            sGfeShoppingCartLoan1NoteIR.Text = gfea.sGfeShoppingCartLoan1NoteIR_rep;
            sGfeShoppingCartLoan1InitialPmt.Text = gfea.sGfeShoppingCartLoan1InitialPmt_rep;
            sGfeShoppingCartLoan1RateLockPeriod.Text = gfea.sGfeShoppingCartLoan1RateLockPeriod_rep;
            sGfeShoppingCartLoan1CanRateIncreaseTri.Checked = gfea.sGfeShoppingCartLoan1CanRateIncreaseTri == yes;
            sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri.Checked = gfea.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri == yes;
            sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri.Checked = gfea.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri == yes;
            sGfeShoppingCartLoan1HavePpmtPenaltyTri.Checked = gfea.sGfeShoppingCartLoan1HavePpmtPenaltyTri == yes;
            sGfeShoppingCartLoan1IsBalloonTri.Checked = gfea.sGfeShoppingCartLoan1IsBalloonTri == yes;
            sGfeShoppingCartLoan1TotalClosingCost.Text = gfea.sGfeShoppingCartLoan1TotalClosingCost_rep;

            sGfeShoppingCartLoan2OriginatorName.Text = gfea.sGfeShoppingCartLoan2OriginatorName;
            sGfeShoppingCartLoan2LoanAmt.Text = gfea.sGfeShoppingCartLoan2LoanAmt_rep;
            sGfeShoppingCartLoan2LoanTerm.Text = gfea.sGfeShoppingCartLoan2LoanTerm_rep;
            sGfeShoppingCartLoan2NoteIR.Text = gfea.sGfeShoppingCartLoan2NoteIR_rep;
            sGfeShoppingCartLoan2InitialPmt.Text = gfea.sGfeShoppingCartLoan2InitialPmt_rep;
            sGfeShoppingCartLoan2RateLockPeriod.Text = gfea.sGfeShoppingCartLoan2RateLockPeriod_rep;
            sGfeShoppingCartLoan2CanRateIncreaseTri.Checked = gfea.sGfeShoppingCartLoan2CanRateIncreaseTri == yes;
            sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri.Checked = gfea.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri == yes;
            sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri.Checked = gfea.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri == yes;
            sGfeShoppingCartLoan2HavePpmtPenaltyTri.Checked = gfea.sGfeShoppingCartLoan2HavePpmtPenaltyTri == yes;
            sGfeShoppingCartLoan2IsBalloonTri.Checked = gfea.sGfeShoppingCartLoan2IsBalloonTri == yes;
            sGfeShoppingCartLoan2TotalClosingCost.Text = gfea.sGfeShoppingCartLoan2TotalClosingCost_rep;

            sGfeShoppingCartLoan3OriginatorName.Text = gfea.sGfeShoppingCartLoan3OriginatorName;
            sGfeShoppingCartLoan3LoanAmt.Text = gfea.sGfeShoppingCartLoan3LoanAmt_rep;
            sGfeShoppingCartLoan3LoanTerm.Text = gfea.sGfeShoppingCartLoan3LoanTerm_rep;
            sGfeShoppingCartLoan3NoteIR.Text = gfea.sGfeShoppingCartLoan3NoteIR_rep;
            sGfeShoppingCartLoan3InitialPmt.Text = gfea.sGfeShoppingCartLoan3InitialPmt_rep;
            sGfeShoppingCartLoan3RateLockPeriod.Text = gfea.sGfeShoppingCartLoan3RateLockPeriod_rep;
            sGfeShoppingCartLoan3CanRateIncreaseTri.Checked = gfea.sGfeShoppingCartLoan3CanRateIncreaseTri == yes;
            sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri.Checked = gfea.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri == yes;
            sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri.Checked = gfea.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri == yes;
            sGfeShoppingCartLoan3HavePpmtPenaltyTri.Checked = gfea.sGfeShoppingCartLoan3HavePpmtPenaltyTri == yes;
            sGfeShoppingCartLoan3IsBalloonTri.Checked = gfea.sGfeShoppingCartLoan3IsBalloonTri == yes;
            sGfeShoppingCartLoan3TotalClosingCost.Text = gfea.sGfeShoppingCartLoan3TotalClosingCost_rep;

            sHazInsRsrvMon.ReadOnly = (false == gfea.sHazInsRsrvMonLckd) || IsReadOnly;
            sMInsRsrvMon.ReadOnly = (false == gfea.sMInsRsrvMonLckd) || IsReadOnly;
            sRealETxRsrvMon.ReadOnly = (false == gfea.sRealETxRsrvMonLckd) || IsReadOnly;
            sSchoolTxRsrvMon.ReadOnly = (false == gfea.sSchoolTxRsrvMonLckd) || IsReadOnly;
            sFloodInsRsrvMon.ReadOnly = (false == gfea.sFloodInsRsrvMonLckd) || IsReadOnly;
            s1006RsrvMon.ReadOnly = (false == gfea.s1006RsrvMonLckd) || IsReadOnly;
            s1007RsrvMon.ReadOnly = (false == gfea.s1007RsrvMonLckd) || IsReadOnly;
            sU3RsrvMon.ReadOnly = (false == gfea.sU3RsrvMonLckd) || IsReadOnly;
            sU4RsrvMon.ReadOnly = (false == gfea.sU4RsrvMonLckd) || IsReadOnly;

            sLpTemplateNm.Text = gfea.sLpTemplateNm;
            // 5/29/07 mf. OPM 12061. There is now a corporate setting for editing
            // the loan program name.
            if (! BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowEditingLoanProgramName))
                sLpTemplateNm.ReadOnly = true;

            sCcTemplateNm.Text = gfea.sCcTemplateNm;

            sLOrigFProps_ctrl.InitItemProps(gfea.sLOrigFProps);
            sGfeOriginatorCompFProps_ctrl.InitItemProps(gfea.sGfeOriginatorCompFProps);
            sApprFProps_ctrl.InitItemProps(gfea.sApprFProps);
            sCrFProps_ctrl.InitItemProps(gfea.sCrFProps);
            sInspectFProps_ctrl.InitItemProps(gfea.sInspectFProps);
            sMBrokFProps_ctrl.InitItemProps(gfea.sMBrokFProps);
            sTxServFProps_ctrl.InitItemProps(gfea.sTxServFProps);
            sFloodCertificationFProps_ctrl.InitItemProps(gfea.sFloodCertificationFProps);
            sProcFProps_ctrl.InitItemProps(gfea.sProcFProps);
            sUwFProps_ctrl.InitItemProps(gfea.sUwFProps);
            sWireFProps_ctrl.InitItemProps(gfea.sWireFProps);
            s800U1FProps_ctrl.InitItemProps(gfea.s800U1FProps);
            s800U2FProps_ctrl.InitItemProps(gfea.s800U2FProps);
            s800U3FProps_ctrl.InitItemProps(gfea.s800U3FProps);
            s800U4FProps_ctrl.InitItemProps(gfea.s800U4FProps);
            s800U5FProps_ctrl.InitItemProps(gfea.s800U5FProps);
            sIPiaProps_ctrl.InitItemProps(gfea.sIPiaProps);
            sMipPiaProps_ctrl.InitItemProps(gfea.sMipPiaProps);
            sHazInsPiaProps_ctrl.InitItemProps(gfea.sHazInsPiaProps);
            s904PiaProps_ctrl.InitItemProps(gfea.s904PiaProps);
            sVaFfProps_ctrl.InitItemProps(gfea.sVaFfProps);
            s900U1PiaProps_ctrl.InitItemProps(gfea.s900U1PiaProps);
            sHazInsRsrvProps_ctrl.InitItemProps(gfea.sHazInsRsrvProps);
            sMInsRsrvProps_ctrl.InitItemProps(gfea.sMInsRsrvProps);
            sSchoolTxRsrvProps_ctrl.InitItemProps(gfea.sSchoolTxRsrvProps);
            sRealETxRsrvProps_ctrl.InitItemProps(gfea.sRealETxRsrvProps);
            sFloodInsRsrvProps_ctrl.InitItemProps(gfea.sFloodInsRsrvProps);
            s1006RsrvProps_ctrl.InitItemProps(gfea.s1006RsrvProps);
            s1007RsrvProps_ctrl.InitItemProps(gfea.s1007RsrvProps);
            sU3RsrvProps_ctrl.InitItemProps(gfea.sU3RsrvProps);
            sU4RsrvProps_ctrl.InitItemProps(gfea.sU4RsrvProps);
            sAggregateAdjRsrvProps_ctrl.InitItemProps(gfea.sAggregateAdjRsrvProps);
            sEscrowFProps_ctrl.InitItemProps(gfea.sEscrowFProps);
            sOwnerTitleInsFProps_ctrl.InitItemProps(gfea.sOwnerTitleInsProps);
            sDocPrepFProps_ctrl.InitItemProps(gfea.sDocPrepFProps);
            sNotaryFProps_ctrl.InitItemProps(gfea.sNotaryFProps);
            sAttorneyFProps_ctrl.InitItemProps(gfea.sAttorneyFProps);
            sTitleInsFProps_ctrl.InitItemProps(gfea.sTitleInsFProps);
            sU1TcProps_ctrl.InitItemProps(gfea.sU1TcProps);
            sU2TcProps_ctrl.InitItemProps(gfea.sU2TcProps);
            sU3TcProps_ctrl.InitItemProps(gfea.sU3TcProps);
            sU4TcProps_ctrl.InitItemProps(gfea.sU4TcProps);
            sRecFProps_ctrl.InitItemProps(gfea.sRecFProps);
            sCountyRtcProps_ctrl.InitItemProps(gfea.sCountyRtcProps);
            sStateRtcProps_ctrl.InitItemProps(gfea.sStateRtcProps);
            sU1GovRtcProps_ctrl.InitItemProps(gfea.sU1GovRtcProps);
            sU2GovRtcProps_ctrl.InitItemProps(gfea.sU2GovRtcProps);
            sU3GovRtcProps_ctrl.InitItemProps(gfea.sU3GovRtcProps);
            sPestInspectFProps_ctrl.InitItemProps(gfea.sPestInspectFProps);
            sU1ScProps_ctrl.InitItemProps(gfea.sU1ScProps);
            sU2ScProps_ctrl.InitItemProps(gfea.sU2ScProps);
            sU3ScProps_ctrl.InitItemProps(gfea.sU3ScProps);
            sU4ScProps_ctrl.InitItemProps(gfea.sU4ScProps);
            sU5ScProps_ctrl.InitItemProps(gfea.sU5ScProps);

            sApprFProps_ctrl.SetPaidTo(()=> gfea.sApprFPaidTo);
            sCrFProps_ctrl.SetPaidTo(()=> gfea.sCrFPaidTo);
            sTxServFProps_ctrl.SetPaidTo(()=> gfea.sTxServFPaidTo);
            sFloodCertificationFProps_ctrl.SetPaidTo(()=> gfea.sFloodCertificationFPaidTo);
            sInspectFProps_ctrl.SetPaidTo(()=> gfea.sInspectFPaidTo);
            sProcFProps_ctrl.SetPaidTo(()=> gfea.sProcFPaidTo);
            sUwFProps_ctrl.SetPaidTo(()=> gfea.sUwFPaidTo);
            sWireFProps_ctrl.SetPaidTo(()=> gfea.sWireFPaidTo);
            s800U1FProps_ctrl.SetPaidTo(()=> gfea.s800U1FPaidTo);
            s800U2FProps_ctrl.SetPaidTo(()=> gfea.s800U2FPaidTo);
            s800U3FProps_ctrl.SetPaidTo(()=> gfea.s800U3FPaidTo);
            s800U4FProps_ctrl.SetPaidTo(()=> gfea.s800U4FPaidTo);
            s800U5FProps_ctrl.SetPaidTo(()=> gfea.s800U5FPaidTo);
            sOwnerTitleInsFProps_ctrl.SetPaidTo(()=> gfea.sOwnerTitleInsPaidTo);
            sDocPrepFProps_ctrl.SetPaidTo(()=> gfea.sDocPrepFPaidTo);
            sNotaryFProps_ctrl.SetPaidTo(()=> gfea.sNotaryFPaidTo);
            sAttorneyFProps_ctrl.SetPaidTo(()=> gfea.sAttorneyFPaidTo);
            sU1TcProps_ctrl.SetPaidTo(()=> gfea.sU1TcPaidTo);
            sU2TcProps_ctrl.SetPaidTo(()=> gfea.sU2TcPaidTo);
            sU3TcProps_ctrl.SetPaidTo(()=> gfea.sU3TcPaidTo);
            sU4TcProps_ctrl.SetPaidTo(()=> gfea.sU4TcPaidTo);
            sU1GovRtcProps_ctrl.SetPaidTo(()=> gfea.sU1GovRtcPaidTo);
            sU2GovRtcProps_ctrl.SetPaidTo(()=> gfea.sU2GovRtcPaidTo);
            sU3GovRtcProps_ctrl.SetPaidTo(()=> gfea.sU3GovRtcPaidTo);
            sPestInspectFProps_ctrl.SetPaidTo(()=> gfea.sPestInspectPaidTo);
            sU1ScProps_ctrl.SetPaidTo(()=> gfea.sU1ScPaidTo);
            sU2ScProps_ctrl.SetPaidTo(()=> gfea.sU2ScPaidTo);
            sU3ScProps_ctrl.SetPaidTo(()=> gfea.sU3ScPaidTo);
            sU4ScProps_ctrl.SetPaidTo(()=> gfea.sU4ScPaidTo);
            sU5ScProps_ctrl.SetPaidTo(()=> gfea.sU5ScPaidTo);
            sHazInsPiaProps_ctrl.SetPaidTo(()=> gfea.sHazInsPiaPaidTo);
            sMipPiaProps_ctrl.SetPaidTo(()=> gfea.sMipPiaPaidTo);
            sVaFfProps_ctrl.SetPaidTo(()=> gfea.sVaFfPaidTo);

            s800U1FProps_ctrl.Page2Selection = gfea.s800U1FGfeSection;
            s800U2FProps_ctrl.Page2Selection = gfea.s800U2FGfeSection;
            s800U3FProps_ctrl.Page2Selection = gfea.s800U3FGfeSection;
            s800U4FProps_ctrl.Page2Selection = gfea.s800U4FGfeSection;
            s800U5FProps_ctrl.Page2Selection = gfea.s800U5FGfeSection;
            sEscrowFProps_ctrl.Page2Selection = gfea.sEscrowFGfeSection;
            sDocPrepFProps_ctrl.Page2Selection = gfea.sDocPrepFGfeSection;
            sNotaryFProps_ctrl.Page2Selection = gfea.sNotaryFGfeSection;
            sAttorneyFProps_ctrl.Page2Selection = gfea.sAttorneyFGfeSection;
            //sTitleInsFProps_ctrl.Page2Selection = dataLoan.sTitleInsFGfeSection;
            sU1TcProps_ctrl.Page2Selection = gfea.sU1TcGfeSection;
            sU2TcProps_ctrl.Page2Selection = gfea.sU2TcGfeSection;
            sU3TcProps_ctrl.Page2Selection = gfea.sU3TcGfeSection;
            sU4TcProps_ctrl.Page2Selection = gfea.sU4TcGfeSection;
            sU1GovRtcProps_ctrl.Page2Selection = gfea.sU1GovRtcGfeSection;
            sU2GovRtcProps_ctrl.Page2Selection = gfea.sU2GovRtcGfeSection;
            sU3GovRtcProps_ctrl.Page2Selection = gfea.sU3GovRtcGfeSection;
            sU1ScProps_ctrl.Page2Selection = gfea.sU1ScGfeSection;
            sU2ScProps_ctrl.Page2Selection = gfea.sU2ScGfeSection;
            sU3ScProps_ctrl.Page2Selection = gfea.sU3ScGfeSection;
            sU4ScProps_ctrl.Page2Selection = gfea.sU4ScGfeSection;
            sU5ScProps_ctrl.Page2Selection = gfea.sU5ScGfeSection;
            s904PiaProps_ctrl.Page2Selection = gfea.s904PiaGfeSection;
            s900U1PiaProps_ctrl.Page2Selection = gfea.s900U1PiaGfeSection;

            sGfeIsTPOTransaction.Checked = gfea.sGfeIsTPOTransaction;
            sIsItemizeBrokerCommissionOnIFW.Checked = gfea.sIsItemizeBrokerCommissionOnIFW;

            Tools.SetRadioButtonListValue(sOriginatorCompensationPaymentSourceT, gfea.sOriginatorCompensationPaymentSourceT);

            sGfeOriginatorCompF.Text = gfea.sGfeOriginatorCompF_rep;
            sGfeOriginatorCompFPc.Text = gfea.sGfeOriginatorCompFPc_rep;
            sGfeOriginatorCompFMb.Text = gfea.sGfeOriginatorCompFMb_rep;
            Tools.SetDropDownListValue(sGfeOriginatorCompFBaseT, gfea.sGfeOriginatorCompFBaseT);

            if (gfea.sIsOriginationCompensationSourceRequired)
            {
                // 2/22/2011 dd - Need to remove Not specified option.
                foreach (ListItem item in sOriginatorCompensationPaymentSourceT.Items)
                {
                    if (item.Value == E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified.ToString("D"))
                    {
                        sOriginatorCompensationPaymentSourceT.Items.Remove(item);
                        break;
                    }
                }
            }
            m_isChannelBrokered = gfea.sBranchChannelT == E_BranchChannelT.Broker;
            m_isChannelCorrespondent = gfea.sBranchChannelT == E_BranchChannelT.Correspondent;

            // Line 802
            // Credit or charge
            sLDiscnt.Text = gfea.sLDiscnt_rep;
            sLDiscntFMb.Text = gfea.sLDiscntFMb_rep;
            sLDiscntPc.Text = gfea.sLDiscntPc_rep;
            Tools.SetDropDownListValue(sLDiscntBaseT, gfea.sLDiscntBaseT);

            // Credit for lender paid fees
            Tools.SetDropDownListValue(sGfeCreditLenderPaidItemT, gfea.sGfeCreditLenderPaidItemT);
            sGfeCreditLenderPaidItemF.Text = gfea.sGfeCreditLenderPaidItemF_rep;

            // General Lender credit
            sGfeLenderCreditFPc.Text = gfea.sGfeLenderCreditFPc_rep;
            sGfeLenderCreditF.Text = gfea.sGfeLenderCreditF_rep;
            sGfeLenderCreditFProps_ctrl.InitItemProps(gfea.sGfeLenderCreditFProps);

            // Discount points
            sGfeDiscountPointFPc.Text = gfea.sGfeDiscountPointFPc_rep;
            sGfeDiscountPointF.Text = gfea.sGfeDiscountPointF_rep;
            sGfeDiscountPointFProps_ctrl.InitItemProps(gfea.sGfeDiscountPointFProps);

            sGfeInitialDisclosureD.Text = gfea.sGfeInitialDisclosureD_rep;
            sGfeInitialDisclosureD.ToolTip = hintForTodaysDate;
            sGfeRedisclosureD.Text = gfea.sGfeRedisclosureD_rep;
            sGfeRedisclosureD.ToolTip = hintForTodaysDate;
            if (Broker.IsProtectDisclosureDates)
            {
                sGfeInitialDisclosureD.ReadOnly = true;
                sGfeRedisclosureD.ReadOnly = true;
            }

            if (Broker.IsGFEandCoCVersioningEnabled && !IsArchivePage)
            {
                if (gfea.sLastDisclosedGFEArchiveD_rep == "")
                {
                    Tools.SetDropDownListValue(sLastDisclosedGFEArchiveD, "0");
                }
                else
                {
                    Tools.SetDropDownListValue(sLastDisclosedGFEArchiveD, gfea.sLastDisclosedGFEArchiveD_rep);
                }
            }

            m_isVaLoan = gfea.sLT == E_sLT.VA;
            m_isFfUfMipIsBeingFinanced = gfea.sFfUfMipIsBeingFinanced;

            //this part is pretty hardcoded since most of the data needed to make it more dynamic is encapsulated
            if (Broker.IsB4AndB6DisabledIn1100And1300OfGfe && !IsArchivePage)
                hideB4andB6();

            if (gfea.sLenderCreditCalculationMethodT == E_sLenderCreditCalculationMethodT.CalculateFromFrontEndRateLock && !IsArchivePage)
            {
                creditChargeBreakdown.Style["display"] = "none";                
            }

        }

        private void hideB4andB6()
        {
            List<GoodFaithEstimate2010RightColumn> charges1100 = new List<GoodFaithEstimate2010RightColumn>();
            charges1100.Add(sEscrowFProps_ctrl);
            charges1100.Add(sOwnerTitleInsFProps_ctrl);
            charges1100.Add(sTitleInsFProps_ctrl);
            charges1100.Add(sDocPrepFProps_ctrl);
            charges1100.Add(sNotaryFProps_ctrl);
            charges1100.Add(sAttorneyFProps_ctrl);
            charges1100.Add(sU1TcProps_ctrl);
            charges1100.Add(sU2TcProps_ctrl);
            charges1100.Add(sU3TcProps_ctrl);
            charges1100.Add(sU4TcProps_ctrl);

            foreach (GoodFaithEstimate2010RightColumn rightCol in charges1100)
            {
                switch (rightCol.getIndexOfRadioButton("B6"))
                {
                    case 1:
                        rightCol.Page2Option1Text = "";
                        rightCol.Page2A_rbVisible = false;
                        break;
                    case 2:
                        rightCol.Page2Option2Text = "";
                        rightCol.Page2B_rbVisible = false;
                        break;
                    case 3:
                        rightCol.Page2Option3Text = "";
                        rightCol.Page2C_rbVisible = false;
                        break;
                }
            }


            List<GoodFaithEstimate2010RightColumn> charges1300 = new List<GoodFaithEstimate2010RightColumn>();
            charges1300.Add(sU1ScProps_ctrl);
            charges1300.Add(sU2ScProps_ctrl);
            charges1300.Add(sU3ScProps_ctrl);
            charges1300.Add(sU4ScProps_ctrl);
            charges1300.Add(sU5ScProps_ctrl);

            foreach (GoodFaithEstimate2010RightColumn rightCol in charges1300)
            {
                switch (rightCol.getIndexOfRadioButton("B4"))
                {
                    case 1:
                        rightCol.Page2Option1Text = "";
                        rightCol.Page2A_rbVisible = false;
                        break;
                    case 2:
                        rightCol.Page2Option2Text = "";
                        rightCol.Page2B_rbVisible = false;
                        break;
                    case 3:
                        rightCol.Page2Option3Text = "";
                        rightCol.Page2C_rbVisible = false;
                        break;
                }
            }
        }

        protected override void LoadData() 
        {
            m_convertLos = new LosConvert();
            
            CPageData dataLoan = null;
            if (IsArchivePage)
            {
                dataLoan = GetArchiveCPageData(LoanID);
            }
            else
            {
                dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(GoodFaithEstimate2010));
            }
            dataLoan.InitLoad();

            sLPurposeT = dataLoan.sLPurposeT.ToString();
            sIsShowsIsPreserveGFEFees.Value = IsArchivePage && dataLoan.sIsShowsIsPreserveGFEFees ? "1" : "0";
            sIsPreserveGFEFees.SelectedIndex = dataLoan.sClosingCostAutomationUpdateT == E_sClosingCostAutomationUpdateT.PreserveFeesOnLoan ? 1 : 0;

            sGfeIsBalloon.Enabled = dataLoan.sGfeIsBalloonLckd;

            if (IsArchivePage == false)
            {
                if (dataLoan.sGfeIsTPOTransactionIsCalculated
                    && (dataLoan.sBranchChannelT == E_BranchChannelT.Broker
                        || dataLoan.sBranchChannelT == E_BranchChannelT.Retail
                        || dataLoan.sBranchChannelT == E_BranchChannelT.Wholesale))
                {
                    // OPM 198345. It is calculated and readonly.
                    sGfeIsTPOTransaction.Enabled = false;
                }
            }

            if (Broker.ShowGetRecordingChargesTransferTaxesAndTitleQuoteButton)
            {
                GetRecordingChargesTransferTaxesAndTitleQuoteContainer.Visible = true;
            }

            if (IsClosingCostMigrationArchivePage)
            {
                BindDataObject(dataLoan.sClosingCostMigrationGFEArchive);
            }
            else if (IsArchivePage)
            {
                var gfea = (from a in dataLoan.GFEArchives
                            where a.DateArchived == ddlGFEArchives.SelectedValue
                            select a).First();
                BindDataObject(gfea);
            }
            else
            {
                var gfea = dataLoan.ExtractStaticGFEArchive();
                BindDataObject(gfea);
            }

        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);

            // OPM 170146 - Manually insert copyright notice.
            DisplayCopyRight = false;
            RegisterJsScript("FeeTypePropertiesSetter.js");

            // The Record GFE Data to Archive Row is now hidden in PageLoad because it depends on loan data.
            bRecordGFEToArchive.Visible = ShowGFEArchiveRecorder && BrokerUser.HasPermission(Permission.AllowManuallyArchivingGFE);
            phSelectGFERow.Visible = IsArchivePage;
            phAdditionalSection1000CustomFees.Visible = Broker.EnableAdditionalSection1000CustomFees;

            GfeTilZip.SmartZipcode(GfeTilCity, GfeTilState);
            this.PageTitle = "2010 Good Faith Estimate";
            if (IsArchivePage)
            {
                if (IsClosingCostMigrationArchivePage)
                {
                    // We don't want the link to be highlighted in the nav.
                    this.PageID = "2010GFEClosingCostMigrationArchive";
                }
                else
                {
                    this.PageID = "2010GFEArchive";
                }
                // don't print it either, so don't set the pdfprint class.
                hfIsArchivePage.Value = "true";
            }
            else
            {
                this.PageID = "2010GFE";
            
                if (false == IsLeadPage)
                {
                    this.PDFPrintClass = typeof(LendersOffice.Pdf.CGoodFaithEstimate2010PDF);
                }
                else
                {
                    this.PDFPrintClass = typeof(LendersOffice.Pdf.CInitialFeesWorksheetPDF);
                }
            }

            Tools.Bind_sProRealETxT(sProRealETxT);
            Tools.Bind_PercentBaseT(sProHazInsT);
            Tools.Bind_sFinMethT(sFinMethT);
            Tools.Bind_PercentBaseLoanAmountsT(sLDiscntBaseT);
            Tools.Bind_FloodCertificationDeterminationT(sFloodCertificationDeterminationT);
            Tools.Bind_PercentBaseLoanAmountsT(sMBrokFBaseT);
            Tools.Bind_PercentBaseT(sRecBaseT);
            Tools.Bind_PercentBaseT(sCountyRtcBaseT);
            Tools.Bind_PercentBaseT(sStateRtcBaseT);
            Tools.Bind_PercentBaseT(sU1GovRtcBaseT);
            Tools.Bind_PercentBaseT(sU2GovRtcBaseT);
            Tools.Bind_PercentBaseT(sU3GovRtcBaseT);
            Tools.Bind_sTimeZoneT(sGfeNoteIRAvailTillDTimeZoneT);
            Tools.Bind_sTimeZoneT(sGfeEstScAvailTillDTimeZoneT);
            Tools.Bind_sGfeOriginatorCompFBaseT(sGfeOriginatorCompFBaseT);
            Tools.Bind_sGfeCreditLenderPaidItemT(sGfeCreditLenderPaidItemT);

            Tools.Bind_CustomFeeDesc(s800U1FDesc, BrokerID, "800");
            Tools.Bind_CustomFeeDesc(s800U2FDesc, BrokerID, "800");
            Tools.Bind_CustomFeeDesc(s800U3FDesc, BrokerID, "800");
            Tools.Bind_CustomFeeDesc(s800U4FDesc, BrokerID, "800");
            Tools.Bind_CustomFeeDesc(s800U5FDesc, BrokerID, "800");

            Tools.Bind_CustomFeeDesc(s904PiaDesc, BrokerID, "900");
            Tools.Bind_CustomFeeDesc(s900U1PiaDesc, BrokerID, "900");

            Tools.Bind_CustomFeeDesc(s1006ProHExpDesc, BrokerID, "1000");
            Tools.Bind_CustomFeeDesc(s1007ProHExpDesc, BrokerID, "1000");
            Tools.Bind_CustomFeeDesc(sU3RsrvDesc, BrokerID, "1000");
            Tools.Bind_CustomFeeDesc(sU4RsrvDesc, BrokerID, "1000");

            Tools.Bind_CustomFeeDesc(sU1TcDesc, BrokerID, "1100");
            Tools.Bind_CustomFeeDesc(sU2TcDesc, BrokerID, "1100");
            Tools.Bind_CustomFeeDesc(sU3TcDesc, BrokerID, "1100");
            Tools.Bind_CustomFeeDesc(sU4TcDesc, BrokerID, "1100");

            Tools.Bind_CustomFeeDesc(sU1GovRtcDesc, BrokerID, "1200");
            Tools.Bind_CustomFeeDesc(sU2GovRtcDesc, BrokerID, "1200");
            Tools.Bind_CustomFeeDesc(sU3GovRtcDesc, BrokerID, "1200");

            Tools.Bind_CustomFeeDesc(sU1ScDesc, BrokerID, "1300");
            Tools.Bind_CustomFeeDesc(sU2ScDesc, BrokerID, "1300");
            Tools.Bind_CustomFeeDesc(sU3ScDesc, BrokerID, "1300");
            Tools.Bind_CustomFeeDesc(sU4ScDesc, BrokerID, "1300");
            Tools.Bind_CustomFeeDesc(sU5ScDesc, BrokerID, "1300");

            if (ShowGFEArchiveRecorder)
            {
                var dataLoan = new CPageData(LoanID, new string[] { });
                Tools.Bind_GFEArchives(sLastDisclosedGFEArchiveD, dataLoan.GFEArchives);
            }
            if (IsArchivePage && !IsClosingCostMigrationArchivePage)
            {
                var dataLoan = new CPageData(LoanID, new string[] { "sLastDisclosedGFEArchiveD"});
                Tools.Bind_selectedGFEArchives(ddlGFEArchives, dataLoan.GFEArchives);
                if (!IsPostBack)
                {
                    dataLoan.InitLoad();
                    Tools.SetDropDownListValue(ddlGFEArchives, dataLoan.sLastDisclosedGFEArchiveD_rep);
                }
            }
            
            SetByPassBgCalcForGfeAsDefault();

            CFM.Type = "19";
            CFM.CompanyNameField = GfeTilCompanyName.ClientID;
            CFM.StreetAddressField = GfeTilStreetAddr.ClientID;
            CFM.CityField = GfeTilCity.ClientID;
            CFM.StateField = GfeTilState.ClientID;
            CFM.ZipField = GfeTilZip.ClientID;
            CFM.CompanyPhoneField = GfeTilPhoneOfCompany.ClientID;
            CFM.EmailField = GfeTilEmailAddr.ClientID;
            CFM.IsAllowLockableFeature = true;

            Tools.Bind_sOriginatorCompensationPaymentSourceT(sOriginatorCompensationPaymentSourceT);

            TitlePlaceHolder.Visible = TitleProvider.GetAssociations(BrokerID).Count > 0 && BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowOrderingTitleServices);
            if (TitlePlaceHolder.Visible)
            {
                QuoteLog log = TitleService.GetQuoteOrderLog(BrokerID, LoanID);
                if (log == null)
                {
                    QuoteText.Text = "No quote on file";
                }
                else if (log.IsAPolicyOrderLog)
                {
                    QuoteText.Text = "No quote on file";
                }
                else
                {
                    QuoteText.Text = string.Format(@"Quote received {0} from {1} (<a href=""#"" onclick=""return viewStoredQuote()"">view</a>)",
                        AspxTools.HtmlString(Tools.GetDateTimeDescription(log.QuoteOrderedDate)), AspxTools.HtmlString(log.QuoteProviderName));
                }
            }
        }
        
        private CPageData GetArchiveCPageData(Guid LoanID)
        {
            return new CPageData(LoanID, CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(GoodFaithEstimate2010)).Union(
                CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release));
        }

        protected void RecordGFEToArchive(object sender, System.EventArgs e)
        {
            if (ShowGFEArchiveRecorder && BrokerUser.HasPermission(Permission.AllowManuallyArchivingGFE))
            {
                //CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(GoodFaithEstimate2010)); //! this should have been cached on the page probably..
                CPageData dataLoan = GetArchiveCPageData(LoanID);
                dataLoan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);

                dataLoan.ArchiveGFE(E_GFEArchivedReasonT.ManuallyArchived); // updates sLastDisclosedGFEArchiveD
                if (dataLoan.GFEArchives.Count() == 1) // update sGfeInitialDisclosureD iff this the first time archiving the GFE.
                {
                    dataLoan.Set_sGfeInitialDisclosureD(CDateTime.Create(DateTime.Now)
                        , E_GFEArchivedReasonT.InitialDislosureDateSetViaGFE);
                }
                try
                {
                    dataLoan.Save();
                }
                catch (PageDataSaveDenied exc)
                {
                    AddInitScriptFunctionWithArgs("f_displayFieldWriteDenied", AspxTools.JsString(exc.UserMessage)); return;
                }
                Tools.Bind_GFEArchives(sLastDisclosedGFEArchiveD, dataLoan.GFEArchives);
                Tools.SetDropDownListValue(sLastDisclosedGFEArchiveD, dataLoan.sLastDisclosedGFEArchiveD_rep);
                sGfeInitialDisclosureD.Text = dataLoan.sGfeInitialDisclosureD_rep;
                hfDoWindowRefresh.Value = "true";
            }
            else
            {
                var usrMsg = "You do not have permission to manually archive the Good Faith Estimate.";
                throw new AccessDenied(usrMsg, usrMsg);
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            IsAppSpecific = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
            if (IsArchivePage)
            {
                if (IsPostBack)
                {
                    LoadData();
                }
            }

            CPageData dataLoan = new CPageData(LoanID, new string[] {"sClosingCostFeeVersionT", "sIsManuallySetThirdPartyAffiliateProps"});
            dataLoan.InitLoad();
            bool isLegacy = dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy;
            bool ShowRequirePaidToFromOfficialContactListCheckbox = isLegacy && BrokerUser.HasPermission(Permission.AllowEnableGfePaidToManualDesc);
            phGFEArchiveRow.Visible = ShowGFEArchiveRecorder && isLegacy;

            RegisterJsGlobalVariables("isLegacy", isLegacy);
            RegisterJsGlobalVariables("ShowRequirePaidToFromOfficialContactListCheckbox", ShowRequirePaidToFromOfficialContactListCheckbox);
            RegisterJsGlobalVariables("sIsManuallySetThirdPartyAffiliateProps", dataLoan.sIsManuallySetThirdPartyAffiliateProps);
            ReadonlifyAdditionalControlsAsNeeded();

        }
        private void ReadonlifyAdditionalControlsAsNeeded()
        {
            if (IsReadOnly)
            {
                var gfercs = Form.Controls.OfType<GoodFaithEstimate2010RightColumn>();

                foreach (var gferc in gfercs)
                {
                    gferc.Enabled = !IsReadOnly;
                }
            }
        }
        private void BindOtherCreditDescription(MeridianLink.CommonControls.ComboBox cb) 
        {
            cb.Items.Add("Cash Deposit on sales contract");
            cb.Items.Add("Seller Credit");
            cb.Items.Add("Lender Credit");
            cb.Items.Add("Relocation Funds");
            cb.Items.Add("Employer Assisted Housing");
            cb.Items.Add("Lease Purchase Fund");
            cb.Items.Add("Borrower Paid Fees");
            cb.Items.Add("Paid Outside of Closing");
			cb.Items.Add("Broker Credit");
		

        }
    }
}