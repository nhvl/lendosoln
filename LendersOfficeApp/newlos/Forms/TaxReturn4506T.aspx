﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaxReturn4506T.aspx.cs" Inherits="LendersOfficeApp.newlos.Forms.TaxReturn4506T" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">    
     
    <title>Transcript of Tax Return</title>
    <style type="text/css">
        ul li
        {
            list-style: none;
        }
        ul.noindentlist
        {
         padding:0;
         margin:0;         
        }
        ul.noindentlist li
        {
            margin:0;
        }       
        
        span.tab 
        {
          background-color:gray;
          font-family:verdana;
          font-size:8pt;
          color:#ffffff;
          height:21px;
          text-align:center;
          padding-left:5px;
          border-top:outset 1px;
          border-left:outset 2px;
          border-right:outset 2px;
          border-bottom:none;
          padding-right:10px;
          padding-top:2px;
          font-weight:bold;
          cursor:default;
        }
        span.tab_hoover
        {
          background-color:gainsboro;
          color:#003366;
        }
        span.tab_selected
        {
          background-color:gainsboro;
          color:#003366;
          border-bottom:none;
        }
    </style>    
</head>
<body bgcolor="gainsboro">
  <script type="text/javascript">
      var oPreviousSelectedTab = null;
      var bIsInitialize = false; 
    function _init() {

        if (bIsInitialize == false) {
            // 8/1/2013 dd - Only execute this when page first load. Not when page is recalculate.
            $("#tabBorrower,#tabCoborrower").mouseover(f_onTabFocusIn).mouseout(f_onTabFocusOut).click(f_onTabClick);
            oPreviousSelectedTab = $("#tabBorrower");
            oPreviousSelectedTab.addClass('tab_selected');

            // The values from the database will contain the formatting
            // the user previously entered, so only call formatSsnFields
            // when the value changes instead of when the value changes
            // and on page load.
            <%=AspxTools.JQuery(this.aB4506TSsnTinEin, this.aC4506TSsnTinEin)%>.blur(formatSsnFields);

            $('[name=aIs4506TFiledTaxesSeparately]').change(f_onchange_fileMethod);
            f_onchange_fileMethod();
            bIsInitialize = true;
        }
        <%= AspxTools.JsGetElementById(aB4506TNm) %>.readOnly = !<%= AspxTools.JsGetElementById(aB4506TNmLckd) %>.checked;
	    <%= AspxTools.JsGetElementById(aC4506TNm) %>.readOnly = !<%= AspxTools.JsGetElementById(aC4506TNmLckd) %>.checked;
    }

    function formatSsnFields(event) {
        var $input = $(retrieveEventTarget(event));
        var value = $input.val();
        if (value && value.indexOf('-') !== -1) {
            return;
        }

        // The SSN/EIN fields do not define a mask
        // on load since they can contain either
        // SSN-formatted fields (XXX-XX-XXXX) or
        // EIN-formatted fields (XX-XXXXXXX) and
        // we do not want auto-format an EIN field
        // to SSN, losing the user's input. Instead,
        // use the existing mask logic with SSN format
        // when the user has entered a value that does
        // not contain a dash.
        $input.attr('mask', '***-**-####');
        mask_keyup(event);
        $input.removeAttr('mask');
    }

    function f_onchange_fileMethod() {
        var bIsJointly = $('#radioJointly').prop('checked');
        $('.tab').toggle(bIsJointly == false);
        if (bIsJointly) {
            $('#tabBorrower').click();
            $('#BorrowerNmLabel').text('1a. Borrower Name');
            $('#BorrowerSSNLabel').text('1b. Borrower SSN, TIN, or EIN');
            $('#CoborrowerNmLabel').text('2a. Coborrower Name');
            $('#CoborrowerSSNLabel').text('2b. Coborrower SSN, TIN, or EIN');
        }
        else {
            $('#BorrowerNmLabel, #CoborrowerNmLabel').text('1a. Name');
            $('#BorrowerSSNLabel,#CoborrowerSSNLabel').text('1b. SSN, TIN, or EIN');
        }

        var bIsBorrower = $('#tabBorrower').hasClass('tab_selected');
        $('#BorrowerSSN').toggle(bIsJointly || bIsBorrower);
        $('#CoborrowerSSN').toggle(bIsJointly || bIsBorrower == false);
        $('#BorrowerNm').toggle(bIsJointly || bIsBorrower);
        $('#CoborrowerNm').toggle(bIsJointly || bIsBorrower == false);
        
    }

    function f_onTabFocusIn(evt)
    {
      var target = $(evt.target);
      if (target.hasClass('tab_selected') == false)
      {
        target.addClass('tab_hoover');
      }
    }
    function f_onTabFocusOut(evt)
    {
      var target = $(evt.target);
      if (target.hasClass('tab_selected') == false)
      {
        target.removeClass('tab_hoover');
      }    
    }
    function f_onTabClick(evt)
    {
        var target = $(evt.target);
        if (target.hasClass('tab_selected') == false)
        {
            var method = 'LoadTab';
            
            PolyShouldShowConfirmSave(isDirty(), function(){
                if (null != oPreviousSelectedTab)
                {
                    //alert(oPreviousSelectedTab.attr('id') + '-' + oPreviousSelectedTab.hasClass('tab_selected'));
                    oPreviousSelectedTab.removeClass('tab_selected').removeClass('tab_hoover');
                }
                target.addClass('tab_selected');
                oPreviousSelectedTab = target;

                var selectedId = target.attr('id');
                var bIsBorrower = selectedId === 'tabBorrower';
                $('#BorrowerSSN').toggle(bIsBorrower);
                $('#CoborrowerSSN').toggle(bIsBorrower == false);
                $('#BorrowerNm').toggle(bIsBorrower);
                $('#CoborrowerNm').toggle(bIsBorrower == false);
                var args = getAllFormValues();
                args.IsBorrower = bIsBorrower ? 'True' : 'False';
                var result = gService.loanedit.call(method, args);
                if (!result.error) {
                    clearDirty();
                    populateForm(result.value, null);
                    return true;
                } else {
                    updateDirtyBit();
                    if (result.ErrorType === 'VersionMismatchException')
                    {
                        f_displayVersionMismatch();
                    }
                    else if (result.ErrorType === 'LoanFieldWritePermissionDenied') 
                    {
                        f_displayFieldWriteDenied(result.UserMessage);
                    }
                    else
                    {
                        var errMsg = result.UserMessage || 'Unable to save data. Please try again.';
                        alert(errMsg);
                    }
                    return false;
                }
            }, function(){ method =  'SaveAndLoadTab';});
        }    
    }
    
    function saveMe() {
        var selectedId = oPreviousSelectedTab.attr('id');
        var args = getAllFormValues();
        args.IsBorrower = selectedId === 'tabBorrower' ? 'True' : 'False';
        var result = gService.loanedit.call('SaveTab', args);
        if (!result.error) {
          clearDirty();
          populateForm(result.value, null);
          return true;
        } else {
          updateDirtyBit();
          if (result.ErrorType === 'VersionMismatchException')
          {
            f_displayVersionMismatch();
          }
          if (result.ErrorType === 'LoanFieldWritePermissionDenied') {
              f_displayFieldWriteDenied(result.UserMessage);
          }
          else
          {
            var errMsg = result.UserMessage || 'Unable to save data. Please try again.';
            alert(errMsg);
          }
          return false;
        }
      
    }
    
  </script>
    <form id="formTax4506T" runat="server">
     <div class="MainRightHeader">Request for Transcript of Tax Return</div>
     
<table cellSpacing="5" cellPadding="5" border="0">
    <tr>
        <td><b>Spouses filed taxes:</b></td>
        <td>
            <asp:RadioButton ID="radioJointly" GroupName="aIs4506TFiledTaxesSeparately" runat="server" Text="jointly" value="False"/>
        </td>
        <td>
            <asp:RadioButton ID="radioSeparate" GroupName="aIs4506TFiledTaxesSeparately" runat="server" Text="separately" value="True"/>
        </td>
    </tr>
</table>
        
<table cellSpacing="0" cellPadding="0" border="0">
    <tr>
      <td nowrap="nowrap">
      <span class="tab" id="tabBorrower">Borrower</span>
      <span class="tab" id="tabCoborrower">Co-Borrower</span>
      </td>
    </tr>
    <tr>
    <td nowrap="nowrap">
<ul class="noindentlist">
    <li>
        <table class="InsetBorder FormTable" width="99%" cellpadding="0" cellspacing="0">            
            <tr>
                <td>
                    <table>
                        <tr id="BorrowerNm">
                            <td id="BorrowerNmLabel" class="fieldLabel">1a. Borrower Name</td>
                            <td class="fieldLabel"><asp:Checkbox id="aB4506TNmLckd" runat="server" Text="Lock" onclick="refreshCalculation();"/></td>
                            <td><asp:TextBox ID="aB4506TNm" runat="server" /></td>
                        </tr>                       
                        <tr id="BorrowerSSN">
                            <td id="BorrowerSSNLabel" class="fieldLabel">1b. Borrower SSN, TIN, or EIN</td>
                            <td>&nbsp;</td>
                            <td><asp:TextBox ID="aB4506TSsnTinEin" runat="server" /></td>
                        </tr>
                        <tr id="CoborrowerNm">
                            <td id="CoborrowerNmLabel" class="fieldLabel">2a. Coborrower Name</td>
                            <td class="fieldLabel"><asp:Checkbox id="aC4506TNmLckd" runat="server" Text="Lock"  onclick="refreshCalculation();"/></td>
                            <td><asp:TextBox ID="aC4506TNm" runat="server" /></td>
                        </tr>                       
                        <tr id="CoborrowerSSN">
                            <td id="CoborrowerSSNLabel" class="fieldLabel">2b. Coborrower SSN, TIN, or EIN</td>
                            <td>&nbsp;</td>
                            <td><asp:TextBox ID="aC4506TSsnTinEin" runat="server" /></td>
                        </tr>
                    </table>
                </td>                
            </tr>
        </table>
    </li>
    <li>
        <table class="InsetBorder FormTable" width="99%" cellpadding="0" cellspacing="0">            
            <tr>
                <td>
                    <table>                       
                        <tr>
                            <td rowspan="3" valign="top"><b>4. Previous Address</b></td>
                            <td colspan="3"><asp:TextBox ID="a4506TPrevStreetAddr" runat="server" Width="22em" /> </td>
                        </tr>
                        <tr>                            
                            <td><asp:TextBox ID="a4506TPrevCity" runat="server" /> </td>
                            <td><ml:statedropdownlist id="a4506TPrevState" runat="server" IncludeTerritories="false" /> </td>
                            <td><ml:zipcodetextbox id="a4506TPrevZip" runat="server" width="50" preset="zipcode" /></td>
                        </tr>
                        <tr><td colspan="3"><b>(as shown on the last returns filed if different)</b></td></tr>
                    </table>
                </td>                
            </tr>
        </table>
    </li>
    
    
    <li>
     <table class="InsetBorder FormTable" width="99%">         
            <tr>
                <td>
                    <b>5. If transcript or tax information is to be mailed to a third party, enter the third party's information:</b>
                </td>
            </tr>            
            <tr>
                <td>
                 <table>                        
                    <tr>
                            <td></td>
                            <td colspan="3"><UC:CFM ID="CFM" runat="server" /></td>
                    </tr>
	                <tr>
	                    <td><b>Name</b></td>
	                    <td colspan="3">
                            <asp:TextBox ID="a4506TThirdPartyName" runat="server" Width="22em" />
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="2" valign="top"><b>Address</b></td>
                        <td colspan="3"><asp:TextBox ID="a4506TThirdPartyStreetAddr" runat="server" Width="22em" /> </td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="a4506TThirdPartyCity" runat="server" /> </td>
                        <td><ml:statedropdownlist id="a4506TThirdPartyState" runat="server" IncludeTerritories="false" /> </td>
                        <td><ml:zipcodetextbox id="a4506TThirdPartyZip" runat="server" width="50" preset="zipcode" /></td>
                    </tr>
                    <tr>
                        <td><b>Phone Number</b></td>
                        <td><ml:phonetextbox id="a4506TThirdPartyPhone" runat="server" width="120" preset="phone" /></td>
                    </tr>
                </table>
                </td>              
            </tr>
      </table>
    </li>
    
    
    <li>
        <table class="InsetBorder FormTable" width="99%">
            <tr>
                <td>
                    <b>6. Transcript requested.&nbsp;</b>Tax form number(1040, 1065, 1120, etc.):&nbsp;<asp:TextBox ID="a4506TTranscript" runat="server" Width="15em" />
                    <ul style="margin-top:0.5em;margin-bottom:0.5em">
                        <li>
                            <asp:CheckBox ID="a4056TIsReturnTranscript" runat="server"/> <b>a.&nbsp;&nbsp;Return Transcript,</b> which includes most of the line items of a tax return as filed with the IRS
                        </li>
                        <li>
                            <asp:CheckBox ID="a4506TIsAccountTranscript" runat="server"/> <b>b.&nbsp;&nbsp;Account Transcript,</b> which contains information on the financial status of the account
                        </li>
                        <li>
                            <asp:CheckBox ID="a4056TIsRecordAccountTranscript" runat="server"/> <b>c.&nbsp;&nbsp;Record of Account,</b> which is a combination of line item information and later adjustments to the account
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>
                    <b>7.</b>&nbsp;&nbsp;<asp:CheckBox ID="a4506TVerificationNonfiling" runat="server"/> <b>Verification of Nonfiling,</b> which is proof from the IRS that you did not file a return for the year
                </td>
            </tr>
            <tr>
                <td>
                    <b>8.</b>&nbsp;&nbsp;<asp:CheckBox ID="a4506TSeriesTranscript" runat="server"/> <b>Form W-2, Form 1099 series, Form 1098 series, or Form 5498 series transcript</b>
                </td>
            </tr>
        </table>
    </li>
    
    <li>        
        <table class="InsetBorder FormTable" width="99%">
            <tr>
                <td colspan="4">
                        <b>9. &nbsp;Year or period requested</b>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td><ml:DateTextBox id="a4506TYear1" runat="server" preset="date" width="75" /></td>
                <td><ml:DateTextBox id="a4506TYear2" runat="server" preset="date" width="75" /></td>
                <td><ml:DateTextBox id="a4506TYear3" runat="server" preset="date" width="75" /></td>
                <td><ml:DateTextBox id="a4506TYear4" runat="server" preset="date" width="75" /></td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td colspan="4"><b><asp:CheckBox ID="a4506TRequestYrHadIdentityTheft" runat="server" Text="One of the years requested involved identity theft on the federal tax return." /></b></td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td colspan="4"><b><asp:CheckBox ID="a4506TSignatoryAttested" runat="server" Text="Signatory attests that he/she has read the attestation clause and declares that he/she has the authority to sign." /></b></td>
            </tr>
        </table>
    </li>
</ul>    
    </td>
    </tr>
    
</table>

</form>
</body>
</html>
