<%@ Import namespace="DataAccess" %>
<%@ Page language="c#" Codebehind="FloodHazardNotice.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.FloodHazardNotice" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>FloodHazardNotice</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </HEAD>
<body bgColor=gainsboro MS_POSITIONING="FlowLayout">
<script language=javascript>
  <!--
  var oRolodex;  
  function _init() {
    oRolodex = new cRolodex();
  }
  //-->
</script>

<form id=FloodHazardNotice method=post runat="server">
<table id=Table1 cellSpacing=0 cellPadding=0 border=0>
  <tr>
    <td class=MainRightHeader style="PADDING-LEFT: 5px" noWrap>
        Flood Hazard Notice
    </td>
  </tr>
  <tr>
    <td style="PADDING-LEFT: 5px" noWrap>
      <table class=InsetBorder id=Table2 cellSpacing=0 cellPadding=0 width="98%" border=0>
        <tr>
          <td noWrap>
            <table id=Table3 cellSpacing=0 cellPadding=0 border=0>
              <tr>
                <td noWrap></TD>
                <td noWrap>
					<uc:CFM name=CFM runat="server" Type="21" CompanyNameField="FloodHazardNoticeCompanyName" StreetAddressField="FloodHazardNoticeStreetAddr" CityField="FloodHazardNoticeCity" StateField="FloodHazardNoticeState" ZipField="FloodHazardNoticeZip"></uc:CFM>
				</TD>
			  </TR>
              <tr>
                <td class=FieldLabel noWrap>Lender</TD>
                <td noWrap><asp:textbox id=FloodHazardNoticeCompanyName runat="server" Width="264px"></asp:TextBox></TD></TR>
              <tr>
                <td class=FieldLabel noWrap>Address</TD>
                <td noWrap><asp:textbox id=FloodHazardNoticeStreetAddr runat="server" Width="264px"></asp:TextBox></TD></TR>
              <tr>
                <td noWrap></TD>
                <td noWrap><asp:textbox id=FloodHazardNoticeCity runat="server"></asp:TextBox><ml:statedropdownlist id=FloodHazardNoticeState runat="server"></ml:StateDropDownList><ml:zipcodetextbox id=FloodHazardNoticeZip runat="server" preset="zipcode" width="50"></ml:ZipcodeTextBox></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
  <tr>
    <td style="PADDING-LEFT: 5px" noWrap>
      <table class=InsetBorder id=Table4 cellSpacing=0 cellPadding=0 width="98%" 
      border=0>
        <tr>
          <td noWrap>
            <table id=Table5 cellSpacing=0 cellPadding=0 border=0 
            >
              <tr>
                <td class=FieldLabel>NOTICE 1:&nbsp; Notice to 
                  Borrower of Special Flood Hazard Area</TD></TR>
              <tr>
                <td><asp:checkbox id=sFloodHazardBuilding runat="server" Text="The building securing the loan for which you have applied is or will be located in an area with special flood hazards"></asp:CheckBox></TD></TR>
              <tr>
                <td><asp:checkbox id=sFloodHazardMobileHome runat="server" Text="The mobile home securing the loan for which you have applied is or will be located in an area with special flood hazards."></asp:CheckBox></TD></TR>
              <tr style="padding-top:5px">
                <td>The area has been identified by the 
                  Director of the Federal Emergency Management Agency (FEMA) as 
                  a special flood hazard area using FEMA's Flood Insurance Rate 
                  Map or the Flood Hazard Boundary Map for the following 
                  community:</TD></TR>
              <tr>
                <td><asp:textbox id=sFloodHazardCommunityDesc runat="server" Width="264px"></asp:TextBox></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
  <tr>
    <td style="PADDING-LEFT: 5px">
        <table class=InsetBorder id=Table8 cellSpacing=0 cellPadding=0 width="98%" border=0>
        <tr>
          <td noWrap>
            Flood Certification #: <asp:textbox id="sFloodCertId" runat="server"></asp:textbox>
          </td>
        </tr>
        </table>
    </td>
  </tr>
  
  <tr>
    <td style="PADDING-LEFT: 5px" noWrap>
      <table class=InsetBorder id=Table6 cellSpacing=0 cellPadding=0 width="98%" 
      border=0>
        <tr>
          <td>
            <table id=Table7 cellSpacing=0 cellPadding=0 border=0 
            >
              <tr>
                <td class=FieldLabel>NOTICE 2:&nbsp; Notice to 
                  Borrower about Federal Disaster Relief Assistance</TD></TR>
              <tr>
                <td><asp:checkbox id=sFloodHazardFedInsAvail runat="server" Text="The community in which the property securing the loan is located participates in the National Flood Insurance Program (NFIP)"></asp:CheckBox></TD></TR>
              <tr>
                <td><asp:checkbox id=sFloodHazardFedInsNotAvail runat="server" Text="Flood insurance coverage under the NFIP is not available for the property securing the loan because the community in which the property is located does not participate in the NFIP"></asp:CheckBox></TD></TR></TABLE></TD></TR></TABLE></TD></TR></TABLE><uc1:cmodaldlg id=CModalDlg1 runat="server"></uc1:cModalDlg></FORM>
	
  </body>
</HTML>
