﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace LendersOfficeApp.newlos.Forms {
    
    
    public partial class MortgageLoanCommitment {
        
        /// <summary>
        /// MortgageLoanCommitment_v2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm MortgageLoanCommitment_v2;
        
        /// <summary>
        /// CFM control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LendersOfficeApp.newlos.Status.ContactFieldMapper CFM;
        
        /// <summary>
        /// MortgageLoanCommitmentCompanyName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox MortgageLoanCommitmentCompanyName;
        
        /// <summary>
        /// MortgageLoanCommitmentStreetAddr control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox MortgageLoanCommitmentStreetAddr;
        
        /// <summary>
        /// MortgageLoanCommitmentCity control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox MortgageLoanCommitmentCity;
        
        /// <summary>
        /// MortgageLoanCommitmentState control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.StateDropDownList MortgageLoanCommitmentState;
        
        /// <summary>
        /// MortgageLoanCommitmentZip control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.ZipcodeTextBox MortgageLoanCommitmentZip;
        
        /// <summary>
        /// MortgageLoanCommitmentPrepareDate control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox MortgageLoanCommitmentPrepareDate;
        
        /// <summary>
        /// sLAmtCalc control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sLAmtCalc;
        
        /// <summary>
        /// sNoteIR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sNoteIR;
        
        /// <summary>
        /// sLtvR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sLtvR;
        
        /// <summary>
        /// sTerm control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sTerm;
        
        /// <summary>
        /// sDue control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sDue;
        
        /// <summary>
        /// sCommitExpD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sCommitExpD;
        
        /// <summary>
        /// sCltvR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sCltvR;
        
        /// <summary>
        /// sCommitRepayTermsDesc control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sCommitRepayTermsDesc;
        
        /// <summary>
        /// sCommitTitleEvidence control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sCommitTitleEvidence;
        
        /// <summary>
        /// sCommitReturnToAboveAddr control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sCommitReturnToAboveAddr;
        
        /// <summary>
        /// sCommitReturnToFollowAddr control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sCommitReturnToFollowAddr;
        
        /// <summary>
        /// sCommitReturnWithinDays control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sCommitReturnWithinDays;
        
        /// <summary>
        /// MortgageLoanCommitmentAlternateLenderCompanyName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox MortgageLoanCommitmentAlternateLenderCompanyName;
        
        /// <summary>
        /// MortgageLoanCommitmentAlternateLenderStreetAddr control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox MortgageLoanCommitmentAlternateLenderStreetAddr;
        
        /// <summary>
        /// MortgageLoanCommitmentAlternateLenderCity control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox MortgageLoanCommitmentAlternateLenderCity;
        
        /// <summary>
        /// MortgageLoanCommitmentAlternateLenderState control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.StateDropDownList MortgageLoanCommitmentAlternateLenderState;
        
        /// <summary>
        /// MortgageLoanCommitmentAlternateLenderZip control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.ZipcodeTextBox MortgageLoanCommitmentAlternateLenderZip;
        
        /// <summary>
        /// sLpTemplateNm control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sLpTemplateNm;
        
        /// <summary>
        /// sLDiscnt1003 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sLDiscnt1003;
        
        /// <summary>
        /// sRLckdExpiredD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sRLckdExpiredD;
        
        /// <summary>
        /// sProThisMPmt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sProThisMPmt;
        
        /// <summary>
        /// sMaxR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sMaxR;
        
        /// <summary>
        /// sMldsHasImpound control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sMldsHasImpound;
        
        /// <summary>
        /// GotoConditionsLink control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlAnchor GotoConditionsLink;
        
        /// <summary>
        /// CModalDlg1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LendersOfficeApp.common.ModalDlg.cModalDlg CModalDlg1;
    }
}
