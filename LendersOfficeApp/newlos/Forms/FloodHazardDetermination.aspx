<%@ Page language="c#" Codebehind="FloodHazardDetermination.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.FloodHazardDetermination" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>FloodHazardDetermination</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="FlowLayout" class="RightBackground">
	
    <form id="FloodHazardDetermination" method="post" runat="server">
<table id=Table1 cellspacing=0 cellpadding=0 border=0>
  <tr>
    <td class=MainRightHeader nowrap>Standard Flood Hazard Determination</td></tr>
  <tr>
    <td nowrap>
      <table class=InsetBorder id=Table2 cellspacing=0 cellpadding=0 width="98%" 
      border=0>
        <tr>
          <td nowrap>
            <table id=Table3 cellspacing=0 cellpadding=0 border=0>
              <tr>
                <td></td>
                <td></td></tr>
              <tr>
                <td>Lender</td>
                <td><asp:TextBox id=TextBox1 runat="server"></asp:TextBox></td></tr>
              <tr>
                <td>Address</td>
                <td><asp:TextBox id=TextBox2 runat="server" Width="253px"></asp:TextBox></td></tr>
              <tr>
                <td></td>
                <td><asp:TextBox id=TextBox12 runat="server"></asp:TextBox><ml:StateDropDownList id=StateDropDownList2 runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=ZipcodeTextBox1 runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
              <tr>
                <td>Lender ID </td>
                <td><asp:TextBox id=TextBox3 runat="server"></asp:TextBox></td></tr>
              <tr>
                <td>Loan Number</td>
                <td><asp:TextBox id=TextBox4 runat="server"></asp:TextBox></td></tr>
              <tr>
                <td>Amount of Flood Insurance</td>
                <td><ml:MoneyTextBox id=MoneyTextBox1 runat="server" width="90" preset="money"></ml:MoneyTextBox></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td nowrap>
      <table class=InsetBorder id=Table4 cellspacing=0 cellpadding=0 width="98%" 
      border=0>
        <tr>
          <td>
            <table id=Table5 cellspacing=0 cellpadding=0 border=0>
              <tr>
                <td nowrap colspan=2>A. National Flood Insurance Program 
                  (NFIP) Community Jurisdiction</td></tr>
              <tr>
                <td nowrap>NFIP Community Name</td>
                <td nowrap><asp:TextBox id=TextBox5 runat="server"></asp:TextBox></td></tr>
              <tr>
                <td nowrap>County</td>
                <td nowrap><asp:TextBox id=TextBox6 runat="server"></asp:TextBox></td></tr>
              <tr>
                <td nowrap>State</td>
                <td nowrap><ml:StateDropDownList id=StateDropDownList1 runat="server"></ml:StateDropDownList></td></tr>
              <tr>
                <td nowrap>NFIP Community Number</td>
                <td nowrap><asp:TextBox id=TextBox7 runat="server"></asp:TextBox></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td nowrap>
      <table class=InsetBorder id=Table6 cellspacing=0 cellpadding=0 width="98%" 
      border=0>
        <tr>
          <td nowrap>
            <table id=Table7 cellspacing=0 cellpadding=0 border=0>
              <tr>
                <td nowrap colspan=2>B. National Flood&nbsp;Insurance Program 
                  (NFIP) Data Affecting Building/Mobile Home</td></tr>
              <tr>
                <td nowrap>NFIP Map Number (Community name, if not the same as 
                  "A")</td>
                <td nowrap><asp:TextBox id=TextBox8 runat="server"></asp:TextBox></td></tr>
              <tr>
                <td nowrap>NFIP Map Panel Effective/Revised Date</td>
                <td nowrap><asp:TextBox id=TextBox9 runat="server"></asp:TextBox></td></tr>
              <tr>
                <td nowrap>LOMA/LOMR</td>
                <td nowrap><asp:CheckBox id=CheckBox2 runat="server" Text="Yes"></asp:CheckBox>&nbsp;Date 
<ml:DateTextBox id=DateTextBox1 runat="server" width="75" preset="date"></ml:DateTextBox></td></tr>
              <tr>
                <td nowrap>Flood Zone</td>
                <td nowrap><asp:TextBox id=TextBox10 runat="server"></asp:TextBox></td></tr>
              <tr>
                <td nowrap><asp:CheckBox id=CheckBox1 runat="server" Text="No NFIP Map"></asp:CheckBox></td>
                <td nowrap></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td nowrap>
      <table class=InsetBorder id=Table8 cellspacing=0 cellpadding=0 width="98%" 
      border=0>
        <tr>
          <td>
            <table id=Table9 cellspacing=0 cellpadding=0 border=0>
              <tr>
                <td>C. Federal Flood Insurance Availability (Check all that 
                  apply)</td></tr>
              <tr>
                <td><asp:CheckBox id=CheckBox3 runat="server" Text="Federal Flood insurance is available"></asp:CheckBox></td></tr>
              <tr>
                <td><asp:CheckBox id=CheckBox5 runat="server" Text="Regular Program"></asp:CheckBox>&nbsp;<asp:CheckBox id=CheckBox8 runat="server" Text="Emergency Program of NFIP"></asp:CheckBox></td></tr>
              <tr>
                <td><asp:CheckBox id=CheckBox4 runat="server" Text="Federal Flood insurance is not available."></asp:CheckBox></td></tr>
              <tr>
                <td><asp:CheckBox id=CheckBox6 runat="server" Text="Building/Mobile Home is in a Coastal Barrier Resources Area (CBRA)"></asp:CheckBox></td></tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp; CBRA/OPA designation date: <ml:DateTextBox id=DateTextBox2 runat="server" width="75" preset="date"></ml:DateTextBox></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td nowrap>
      <table id=Table10 cellspacing=0 cellpadding=0 width="98%" border=0>
        <tr>
          <td>
            <table id=Table11 cellspacing=0 cellpadding=0 border=0>
              <tr>
                <td>D. Determination</td></tr>
              <tr>
                <td><asp:CheckBox id=CheckBox7 runat="server" Text='Building/Mobile Home in special flood hazard area (Zones containing the letters "A" or "V")'></asp:CheckBox></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td nowrap>
      <table id=Table12 cellspacing=0 cellpadding=0 width="98%" border=0>
        <tr>
          <td nowrap>E. Comments</td></tr>
        <tr>
          <td nowrap><asp:TextBox id=TextBox11 runat="server" TextMode="MultiLine" Width="501px" Height="62px"></asp:TextBox></td></tr></table></td></tr>
  <tr>
    <td nowrap>
      <table class=InsetBorder id=Table13 cellspacing=0 cellpadding=0 
      width="98%" border=0>
        <tr>
          <td nowrap>
            <table id=Table14 cellspacing=0 cellpadding=0 border=0>
              <tr>
                <td>F. Preparer's Information</td>
                <td></td></tr>
              <tr>
                <td>Name (If other than Lender)</td>
                <td><asp:TextBox id=TextBox15 runat="server"></asp:TextBox></td></tr>
              <tr>
                <td>Address</td>
                <td><asp:TextBox id=TextBox14 runat="server" Width="223px"></asp:TextBox></td></tr>
              <tr>
                <td></td>
                <td><asp:TextBox id=TextBox13 runat="server"></asp:TextBox><ml:StateDropDownList id=StateDropDownList3 runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=ZipcodeTextBox2 runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
              <tr>
                <td>Phone</td>
                <td><ml:PhoneTextBox id=PhoneTextBox1 runat="server" width="120" preset="phone"></ml:PhoneTextBox></td></tr>
              <tr>
                <td>Date of Determination</td>
                <td><ml:DateTextBox id=DateTextBox3 runat="server" width="75" preset="date"></ml:DateTextBox></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td nowrap></td></tr></table>

     </form>
	
  </body>
</html>
