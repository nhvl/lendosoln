﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AmortizationTableView.ascx.cs" Inherits="LendersOfficeApp.newlos.Forms.AmortizationTableView" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<table id="AmortTable" cellSpacing="0" cellPadding="0" width="100%" border="0" class="FormTable">
    <tr>
        <td nowrap>
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td width="36" class="GridHeader">No</td>
                    <td width="86" class="GridHeader">Pmt Date</td>
                    <td width="86" class="GridHeader">Int Rate</td>
                    <td width="86" class="GridHeader">Monthly Pmt</td>
                    <td width="86" class="GridHeader">Principal</td>
                    <td width="86" class="GridHeader">Interest</td>
                    <td width="66" class="GridHeader">MI</td>
                    <td width="96" class="GridHeader">Balance</td>
                </tr>
            </table>
            <div style="overflow: auto; height: 350px">
                <asp:DataGrid id="m_amortizationDG" runat="server" AutoGenerateColumns="False" CssClass="DataGrid" ShowHeader="False">
                    <AlternatingItemStyle CssClass="GridAlternatingItem"></AlternatingItemStyle>
                    <ItemStyle CssClass="GridItem"></ItemStyle>
                    <Columns>
                        <asp:BoundColumn DataField="PmtIndex_rep">
                            <ItemStyle Width="30px"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="DueDate_rep">
                            <ItemStyle Width="80px"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Rate_rep">
                            <ItemStyle Width="80px"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Pmt_rep">
                            <ItemStyle Width="80px"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Eq_rep">
                            <ItemStyle Width="80px"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="InterestPd_rep">
                            <ItemStyle Width="80px"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MI_rep">
                            <ItemStyle Width="60px"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Bal_rep">
                            <ItemStyle Width="90px"></ItemStyle>
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td align="center">
            <input type="button" value="Close" onclick="onClosePopup();" NoHighlight>&nbsp;&nbsp;
            <input type="button" value="Print" onclick="f_print(<%=AspxTools.JsString(PdfFileName)%>);" NoHighlight>
        </td>
    </tr>
</table>
