using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Forms
{
    public class LoanSubmissionServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return new CLoanSubmissionData(sLId);
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "CopyFromLoanInfo":
                    CopyFromLoanInfo();
                    break;
                case "CopyFromGFE":
                    CopyFromGFE();
                    break;
            }
        }

        private void CopyFromLoanInfo()
        {
            Guid sLId = GetGuid("LoanId");
            var dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(LoanSubmissionServiceItem));
            dataLoan.InitLoad();
            string sSubmitPropTDesc = dataLoan.sGseSpTFriendlyDisplay;
            SetResult("sSubmitPropTDesc", sSubmitPropTDesc);
        }

        private void CopyFromGFE()
        {
            Guid sLId = GetGuid("LoanId");
            var dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(LoanSubmissionServiceItem));
            dataLoan.InitLoad();
            // Bools get translated to "True" and "False"
            SetResult("sSubmitImpoundTaxes", dataLoan.sRealETxRsrvMon_gt_0);
            SetResult("sSubmitImpoundHazard", dataLoan.sHazInsRsrvMon_gt_0);
            SetResult("sSubmitImpoundMI", dataLoan.sMInsRsrvMon_gt_0);
            SetResult("sSubmitImpoundFlood", dataLoan.sFloodInsRsrvMon_gt_0);
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            // 12/22/2004 dd - Always pull primary record.
            dataApp = dataLoan.GetAppData(0);

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Processor, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = GetString("processorName");
            agent.Update();

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            agent.Phone			= GetString("loanOfficerPhone");
            agent.AgentName		= GetString("loanOfficerName");
            agent.CompanyName	= GetString("brokerCompanyName");
            agent.StreetAddr	= GetString("brokerAddress");
            agent.City			= GetString("brokerCity");
            agent.State			= GetString("brokerState");
            agent.Zip			= GetString("brokerZipcode");
            agent.Update();

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = GetString("appraisalOfficer");
            agent.CompanyName = GetString("appraisalCompanyName");
            agent.StreetAddr = GetString("appraisalAddress");
            agent.City = GetString("appraisalCity");
            agent.State = GetString("appraisalState");
            agent.Zip = GetString("appraisalZipcode");
            agent.Phone = GetString("appraisalPhone");
            agent.FaxNum = GetString("appraisalFax");
            agent.LicenseNumOfAgent = GetString("appraisalLicense");
            agent.Update();

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Escrow, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = GetString("escrowOfficer");
            agent.CompanyName = GetString("escrowCompanyName");
            agent.StreetAddr = GetString("escrowAddress");
            agent.City = GetString("escrowCity");
            agent.State = GetString("escrowState");
            agent.Zip = GetString("escrowZipcode");
            agent.Phone = GetString("escrowPhone");
            agent.FaxNum = GetString("escrowFax");
            agent.CaseNum = GetString("escrowNumber");
            agent.Update();

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Title, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = GetString("titleOfficer");
            agent.CompanyName = GetString("titleCompanyName");
            agent.StreetAddr = GetString("titleAddress");
            agent.City = GetString("titleCity");
            agent.State = GetString("titleState");
            agent.Zip = GetString("titleZipcode");
            agent.Phone = GetString("titlePhone");
            agent.FaxNum = GetString("titleFax");
            agent.CaseNum = GetString("titleNumber");
            agent.Update();

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.CreateNew); 
            agent.AgentName = GetString("sAgentLenderAgentName");
            agent.CompanyName = GetString("sAgentLenderCompanyName");
            agent.StreetAddr = GetString("sAgentLenderAddress");
            agent.City = GetString("sAgentLenderCity");
            agent.State = GetString("sAgentLenderState");
            agent.Zip = GetString("sAgentLenderZip");
            agent.Phone = GetString("sAgentLenderAgentPhone");
            agent.FaxNum = GetString("sAgentLenderAgentFax");
            agent.Update();


            dataApp.aBFirstNm = GetString("aBFirstNm");
            dataApp.aBMidNm = GetString("aBMidNm");
            dataApp.aBLastNm = GetString("aBLastNm");
            dataApp.aBSuffix = GetString("aBSuffix");
            dataApp.aBSsn = GetString("aBSSN");
            dataApp.aCFirstNm = GetString("aCFirstNm");
            dataApp.aCMidNm = GetString("aCMidNm");
            dataApp.aCLastNm = GetString("aCLastNm");
            dataApp.aCSuffix = GetString("aCSuffix");
            dataApp.aCSsn = GetString("aCSSN");
            dataLoan.sSpAddr = GetString("sSpAddr");
            dataLoan.sSpCity = GetString("sSpCity");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sSpZip = GetString("sSpZip");
            dataLoan.sSpCounty = GetString("sSpCounty");
            dataLoan.sUnitsNum_rep = GetString("sUnitsNum");
            dataLoan.sRAdjCapMon_rep = GetString("sRAdjCapMon");
            dataLoan.sRAdjCapR_rep = GetString("sRAdjCapR");
            dataLoan.sRAdjIndexR_rep = GetString("sRAdjIndexR");
            dataLoan.sRAdjLifeCapR_rep = GetString("sRAdjLifeCapR");
            dataLoan.sRAdjMarginR_rep = GetString("sRAdjMarginR");

            dataLoan.sSubmitRAdjustOtherDesc = GetString("sSubmitRAdjustOtherDesc");
            dataLoan.sSubmitInitPmtCapR_rep = GetString("sSubmitInitPmtCapR");

            dataLoan.sApprF_rep  = GetString("sApprF");
            dataLoan.sCrF_rep  = GetString("sCrF");
            dataLoan.sProcF_rep  = GetString("sProcF");
            dataLoan.sLienPosT = (E_sLienPosT) GetInt("sLienPosT");

            dataLoan.sLPurposeT = (E_sLPurposeT) GetInt("sLPurposeT");

            dataApp.aOccT = (E_aOccT) GetInt("aOccT");


            dataLoan.sSubmitProgramCode = GetString("sSubmitProgramCode");

            dataLoan.sSubmitImpoundOtherDesc = GetString("sSubmitImpoundOtherDesc");

            dataLoan.sSubmitImpoundTaxes = GetBool("sSubmitImpoundTaxes");
            dataLoan.sSubmitImpoundHazard = GetBool("sSubmitImpoundHazard");
            dataLoan.sSubmitImpoundMI = GetBool("sSubmitImpoundMI");
            dataLoan.sSubmitImpoundFlood = GetBool("sSubmitImpoundFlood");
            dataLoan.sSubmitImpoundOther = GetBool("sSubmitImpoundOther");
            dataLoan.sSubmitDocFull = GetBool("sSubmitDocFull");
            dataLoan.sSubmitDocOther = GetBool("sSubmitDocOther");
            dataLoan.sSubmitBuydownDesc = GetString("sSubmitBuydownDesc");

            dataLoan.sSubmitAmortDesc = GetString("sSubmitAmortDesc");

            dataLoan.sSubmitMIYes = GetBool("sSubmitMIYes");
            dataLoan.sSubmitMITypeDesc = GetString("sSubmitMITypeDesc");
            dataLoan.sRLckdIsLocked = GetString("sRLckdIsLocked") == "sRLckdIsLockedRB";

            dataLoan.sRLckdNumOfDays_rep = GetString("sRLckdNumOfDays");

            dataLoan.Set_sRLckdExpiredD_Manually(GetString("sRLckdExpiredD"));

            dataLoan.sRLckdD_rep = GetString("sRLckdD");

			// OPM 7049 mf.  We added Due to the form, and corrected
			// bug concerning not saving term and due
			dataLoan.sNoteIR_rep = GetString("sNoteIR");
			dataLoan.sTerm_rep = GetString("sTerm");
			dataLoan.sDue_rep = GetString("sDue");
			dataLoan.sQualIR_rep = GetString("sQualIR");
            dataLoan.sQualIRLckd = this.GetBool(nameof(dataLoan.sQualIRLckd));

            dataLoan.sDemandU1LenderDesc = GetString("sDemandU1LenderDesc");
            dataLoan.sDemandU1LenderAmt_rep = GetString("sDemandU1LenderAmt");
            dataLoan.sDemandU1BrokerPaidAmt_rep = GetString("sDemandU1BrokerPaidAmt");
            dataLoan.sDemandU1BorrowerAmt_rep = GetString("sDemandU1BorrowerAmt");
            dataLoan.sDemandU2LenderDesc = GetString("sDemandU2LenderDesc");
            dataLoan.sDemandU2LenderAmt_rep = GetString("sDemandU2LenderAmt");
            dataLoan.sDemandU2BrokerPaidAmt_rep = GetString("sDemandU2BrokerPaidAmt");
            dataLoan.sDemandU2BorrowerAmt_rep = GetString("sDemandU2BorrowerAmt");
            dataLoan.sDemandU3LenderDesc = GetString("sDemandU3LenderDesc");
            dataLoan.sDemandU3LenderAmt_rep = GetString("sDemandU3LenderAmt");
            dataLoan.sDemandU3BrokerPaidAmt_rep = GetString("sDemandU3BrokerPaidAmt");
            dataLoan.sDemandU3BorrowerAmt_rep = GetString("sDemandU3BorrowerAmt");
            dataLoan.sDemandU4LenderDesc = GetString("sDemandU4LenderDesc");
            dataLoan.sDemandU4LenderAmt_rep = GetString("sDemandU4LenderAmt");
            dataLoan.sDemandU4BrokerPaidAmt_rep = GetString("sDemandU4BrokerPaidAmt");
            dataLoan.sDemandU4BorrowerAmt_rep = GetString("sDemandU4BorrowerAmt");
            dataLoan.sDemandU5LenderDesc = GetString("sDemandU5LenderDesc");
            dataLoan.sDemandU5LenderAmt_rep = GetString("sDemandU5LenderAmt");
            dataLoan.sDemandU5BrokerPaidAmt_rep = GetString("sDemandU5BrokerPaidAmt");
            dataLoan.sDemandU5BorrowerAmt_rep = GetString("sDemandU5BorrowerAmt");
            dataLoan.sDemandU6LenderDesc = GetString("sDemandU6LenderDesc");
            dataLoan.sDemandU6LenderAmt_rep = GetString("sDemandU6LenderAmt");
            dataLoan.sDemandU6BrokerPaidAmt_rep = GetString("sDemandU6BrokerPaidAmt");
            dataLoan.sDemandU6BorrowerAmt_rep = GetString("sDemandU6BorrowerAmt");
            dataLoan.sDemandU7LenderDesc = GetString("sDemandU7LenderDesc");
            dataLoan.sDemandU7LenderAmt_rep = GetString("sDemandU7LenderAmt");
            dataLoan.sDemandU7BrokerPaidAmt_rep = GetString("sDemandU7BrokerPaidAmt");
            dataLoan.sDemandU7BorrowerAmt_rep = GetString("sDemandU7BorrowerAmt");
            dataLoan.sHmdaCensusTract = GetString("sHmdaCensusTract");
            dataLoan.sDemandApprFeeLenderAmt_rep = GetString("sDemandApprFeeLenderAmt");
            dataLoan.sDemandApprFeeBrokerPaidAmt_rep = GetString("sDemandApprFeeBrokerPaidAmt");
            dataLoan.sDemandCreditReportFeeLenderAmt_rep = GetString("sDemandCreditReportFeeLenderAmt");
            dataLoan.sDemandCreditReportFeeBrokerPaidAmt_rep = GetString("sDemandCreditReportFeeBrokerPaidAmt");
            dataLoan.sDemandProcessingFeeLenderAmt_rep = GetString("sDemandProcessingFeeLenderAmt");
            dataLoan.sDemandProcessingFeeBrokerPaidAmt_rep = GetString("sDemandProcessingFeeBrokerPaidAmt");
            dataLoan.sDemandLoanDocFeeLenderAmt_rep = GetString("sDemandLoanDocFeeLenderAmt");
            dataLoan.sDemandLoanDocFeeBrokerPaidAmt_rep = GetString("sDemandLoanDocFeeBrokerPaidAmt");
            dataLoan.sDemandLoanDocFeeBorrowerAmt_rep = GetString("sDemandLoanDocFeeBorrowerAmt");
            dataLoan.sDemandLOrigFeeLenderPc_rep = GetString("sDemandLOrigFeeLenderPc");
            dataLoan.sDemandLOrigFeeLenderMb_rep = GetString("sDemandLOrigFeeLenderMb");
            dataLoan.sDemandLDiscountLenderPc_rep = GetString("sDemandLDiscountLenderPc");
            dataLoan.sDemandLDiscountLenderMb_rep = GetString("sDemandLDiscountLenderMb");
            dataLoan.sDemandYieldSpreadPremiumLenderPc_rep = GetString("sDemandYieldSpreadPremiumLenderPc");
            dataLoan.sDemandYieldSpreadPremiumLenderMb_rep = GetString("sDemandYieldSpreadPremiumLenderMb");
            dataLoan.sDemandYieldSpreadPremiumBorrowerPc_rep = GetString("sDemandYieldSpreadPremiumBorrowerPc");
            dataLoan.sDemandYieldSpreadPremiumBorrowerMb_rep = GetString("sDemandYieldSpreadPremiumBorrowerMb");
            dataLoan.sDemandNotes = GetString("sDemandNotes");
            dataLoan.sSubmitBrokerFax = GetString("sSubmitBrokerFax");
            dataLoan.sSubmitPropTDesc = GetString("sSubmitPropTDesc");
            dataLoan.sEstCloseDLckd = GetBool("sEstCloseDLckd");
            dataLoan.sEstCloseD_rep = GetString("sEstCloseD");

            dataLoan.sQualTermCalculationType = (QualTermCalculationType)GetInt("sQualTermCalculationType");
            dataLoan.sQualTerm_rep = GetString("sQualTerm");

            LoanSubmission.BindDataFromControls(dataLoan, dataApp, this);

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            dataApp = dataLoan.GetAppData(0);

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Processor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("processorName", agent.AgentName);

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("loanOfficerPhone", agent.Phone);
            SetResult("loanOfficerName", agent.AgentName);
            SetResult("brokerCompanyName", agent.CompanyName);
            SetResult("brokerAddress", agent.StreetAddr);
            SetResult("brokerCity", agent.City);
            SetResult("brokerState", agent.State);
            SetResult("brokerZipcode", agent.Zip);

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("appraisalOfficer", agent.AgentName);
            SetResult("appraisalCompanyName", agent.CompanyName);
            SetResult("appraisalAddress", agent.StreetAddr);
            SetResult("appraisalCity", agent.City);
            SetResult("appraisalState", agent.State);
            SetResult("appraisalZipcode", agent.Zip);
            SetResult("appraisalPhone", agent.Phone);
            SetResult("appraisalFax", agent.FaxNum);
            SetResult("appraisalLicense", agent.LicenseNumOfAgent);

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Escrow, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("escrowOfficer", agent.AgentName);
            SetResult("escrowCompanyName", agent.CompanyName);
            SetResult("escrowAddress", agent.StreetAddr);
            SetResult("escrowCity", agent.City);
            SetResult("escrowState", agent.State);
            SetResult("escrowZipcode", agent.Zip);
            SetResult("escrowPhone", agent.Phone);
            SetResult("escrowFax", agent.FaxNum);
            SetResult("escrowNumber", agent.CaseNum);

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Title, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("titleOfficer", agent.AgentName);
            SetResult("titleCompanyName", agent.CompanyName);
            SetResult("titleAddress", agent.StreetAddr);
            SetResult("titleCity", agent.City);
            SetResult("titleState", agent.State);
            SetResult("titleZipcode", agent.Zip);
            SetResult("titlePhone", agent.Phone);
            SetResult("titleFax", agent.FaxNum);
            SetResult("titleNumber", agent.CaseNum);

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject); 
            SetResult("lenderOfficer", agent.AgentName);
            SetResult("lenderCompanyName", agent.CompanyName);
            SetResult("lenderAddress", agent.StreetAddr);
            SetResult("lenderCity", agent.City);
            SetResult("lenderState", agent.State);
            SetResult("lenderZipcode", agent.Zip);
            SetResult("lenderPhone", agent.Phone);
            SetResult("lenderFax", agent.FaxNum);

            SetResult("aBFirstNm", dataApp.aBFirstNm);
            SetResult("aBMidNm", dataApp.aBMidNm);
            SetResult("aBLastNm", dataApp.aBLastNm);
            SetResult("aBSuffix", dataApp.aBSuffix);
            SetResult("aBSSN", dataApp.aBSsn);
            SetResult("aCFirstNm", dataApp.aCFirstNm);
            SetResult("aCMidNm", dataApp.aCMidNm);
            SetResult("aCLastNm", dataApp.aCLastNm);
            SetResult("aCSuffix", dataApp.aCSuffix);
            SetResult("aCSSN", dataApp.aCSsn);
            SetResult("sSpAddr", dataLoan.sSpAddr);
            SetResult("sSpCity", dataLoan.sSpCity);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sSpZip", dataLoan.sSpZip);
            SetResult("sSpCounty", dataLoan.sSpCounty);
            SetResult("sLpTemplateNm", dataLoan.sLpTemplateNm);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sQualIR", dataLoan.sQualIR_rep);
            this.SetResult(nameof(dataLoan.sQualIRLckd), dataLoan.sQualIRLckd);
            SetResult("sTerm", dataLoan.sTerm_rep);
			SetResult("sDue", dataLoan.sDue_rep);
            SetResult("sPurchPrice", dataLoan.sPurchPrice_rep);
            SetResult("sApprVal", dataLoan.sApprVal_rep);
            SetResult("sLtvR", dataLoan.sLtvR_rep);
            SetResult("sCltvR", dataLoan.sCltvR_rep);
            SetResult("sEquity", dataLoan.sEquityCalc_rep);
            SetResult("sRAdjCapMon", dataLoan.sRAdjCapMon_rep);
            SetResult("sRAdjCapR", dataLoan.sRAdjCapR_rep);
            SetResult("sRAdjIndexR", dataLoan.sRAdjIndexR_rep);
            SetResult("sRAdjLifeCapR", dataLoan.sRAdjLifeCapR_rep);
            SetResult("sRAdjMarginR", dataLoan.sRAdjMarginR_rep);
            SetResult("sRLckdIsLocked", dataLoan.sRLckdIsLocked ? "sRLckdIsLockedRB" : "sRFloatRB");

            SetResult("sSubmitMIYes", dataLoan.sSubmitMIYes);

            SetResult("sSubmitRAdjustOtherDesc", dataLoan.sSubmitRAdjustOtherDesc);
            SetResult("sSubmitInitPmtCapR", dataLoan.sSubmitInitPmtCapR_rep);
            SetResult("sLienPosT", dataLoan.sLienPosT);
            SetResult("sLPurposeT", dataLoan.sLPurposeT);
            SetResult("aOccT", dataApp.aOccT);
            SetResult("sUnitsNum", dataLoan.sUnitsNum_rep);
            SetResult("sLOrigF", dataLoan.sLOrigF_rep);
            SetResult("sLDiscnt", dataLoan.sLDiscnt_rep);
            SetResult("sApprF", dataLoan.sApprF_rep);
            SetResult("sCrF", dataLoan.sCrF_rep);
            SetResult("sProcF", dataLoan.sProcF_rep);
            SetResult("sSubmitDocFull", dataLoan.sSubmitDocFull);
            SetResult("sSubmitDocOther", dataLoan.sSubmitDocOther);

            SetResult("sSubmitProgramCode", dataLoan.sSubmitProgramCode);
            SetResult("sSubmitImpoundOtherDesc", dataLoan.sSubmitImpoundOtherDesc);
            SetResult("sSubmitImpoundTaxes", dataLoan.sSubmitImpoundTaxes);
            SetResult("sSubmitImpoundHazard", dataLoan.sSubmitImpoundHazard);
            SetResult("sSubmitImpoundMI", dataLoan.sSubmitImpoundMI);
            SetResult("sSubmitImpoundFlood", dataLoan.sSubmitImpoundFlood);
            SetResult("sSubmitImpoundOther", dataLoan.sSubmitImpoundOther);

            SetResult("sSubmitBuydownDesc", dataLoan.sSubmitBuydownDesc);
            SetResult("sSubmitAmortDesc", dataLoan.sSubmitAmortDesc);

            SetResult("sSubmitMITypeDesc", dataLoan.sSubmitMITypeDesc);

            SetResult("sRLckdNumOfDays", dataLoan.sRLckdNumOfDays_rep);
            SetResult("sRLckdExpiredD", dataLoan.sRLckdExpiredD_rep );
            SetResult("sRLckdD", dataLoan.sRLckdD_rep);

            SetResult("sDemandU1LenderDesc", dataLoan.sDemandU1LenderDesc);
            SetResult("sDemandU1LenderAmt", dataLoan.sDemandU1LenderAmt_rep);
            SetResult("sDemandU1BrokerPaidAmt", dataLoan.sDemandU1BrokerPaidAmt_rep);
            SetResult("sDemandU1BrokerDueAmt", dataLoan.sDemandU1BrokerDueAmt_rep);
            SetResult("sDemandU1BorrowerAmt", dataLoan.sDemandU1BorrowerAmt_rep);

            SetResult("sDemandU2LenderDesc", dataLoan.sDemandU2LenderDesc);
            SetResult("sDemandU2LenderAmt", dataLoan.sDemandU2LenderAmt_rep);
            SetResult("sDemandU2BrokerPaidAmt", dataLoan.sDemandU2BrokerPaidAmt_rep);
            SetResult("sDemandU2BrokerDueAmt", dataLoan.sDemandU2BrokerDueAmt_rep);
            SetResult("sDemandU2BorrowerAmt", dataLoan.sDemandU2BorrowerAmt_rep);

            SetResult("sDemandU3LenderDesc", dataLoan.sDemandU3LenderDesc);
            SetResult("sDemandU3LenderAmt", dataLoan.sDemandU3LenderAmt_rep);
            SetResult("sDemandU3BrokerPaidAmt", dataLoan.sDemandU3BrokerPaidAmt_rep);
            SetResult("sDemandU3BrokerDueAmt", dataLoan.sDemandU3BrokerDueAmt_rep);
            SetResult("sDemandU3BorrowerAmt", dataLoan.sDemandU3BorrowerAmt_rep);

            SetResult("sDemandU4LenderDesc", dataLoan.sDemandU4LenderDesc);
            SetResult("sDemandU4LenderAmt", dataLoan.sDemandU4LenderAmt_rep);
            SetResult("sDemandU4BrokerPaidAmt", dataLoan.sDemandU4BrokerPaidAmt_rep);
            SetResult("sDemandU4BrokerDueAmt", dataLoan.sDemandU4BrokerDueAmt_rep);
            SetResult("sDemandU4BorrowerAmt", dataLoan.sDemandU4BorrowerAmt_rep);

            SetResult("sDemandU5LenderDesc", dataLoan.sDemandU5LenderDesc);
            SetResult("sDemandU5LenderAmt", dataLoan.sDemandU5LenderAmt_rep);
            SetResult("sDemandU5BrokerPaidAmt", dataLoan.sDemandU5BrokerPaidAmt_rep);
            SetResult("sDemandU5BrokerDueAmt", dataLoan.sDemandU5BrokerDueAmt_rep);
            SetResult("sDemandU5BorrowerAmt", dataLoan.sDemandU5BorrowerAmt_rep);

            SetResult("sDemandU6LenderDesc", dataLoan.sDemandU6LenderDesc);
            SetResult("sDemandU6LenderAmt", dataLoan.sDemandU6LenderAmt_rep);
            SetResult("sDemandU6BrokerPaidAmt", dataLoan.sDemandU6BrokerPaidAmt_rep);
            SetResult("sDemandU6BrokerDueAmt", dataLoan.sDemandU6BrokerDueAmt_rep);
            SetResult("sDemandU6BorrowerAmt", dataLoan.sDemandU6BorrowerAmt_rep);

            SetResult("sDemandU7LenderDesc", dataLoan.sDemandU7LenderDesc);
            SetResult("sDemandU7LenderAmt", dataLoan.sDemandU7LenderAmt_rep);
            SetResult("sDemandU7BrokerPaidAmt", dataLoan.sDemandU7BrokerPaidAmt_rep);
            SetResult("sDemandU7BrokerDueAmt", dataLoan.sDemandU7BrokerDueAmt_rep);
            SetResult("sDemandU7BorrowerAmt", dataLoan.sDemandU7BorrowerAmt_rep);
            SetResult("sHmdaCensusTract", dataLoan.sHmdaCensusTract);

            SetResult("sDemandApprFeeLenderAmt", dataLoan.sDemandApprFeeLenderAmt_rep);
            SetResult("sDemandApprFeeBrokerPaidAmt", dataLoan.sDemandApprFeeBrokerPaidAmt_rep);
            SetResult("sDemandApprFeeBrokerDueAmt", dataLoan.sDemandApprFeeBrokerDueAmt_rep);
            SetResult("sDemandCreditReportFeeLenderAmt", dataLoan.sDemandCreditReportFeeLenderAmt_rep);
            SetResult("sDemandCreditReportFeeBrokerPaidAmt", dataLoan.sDemandCreditReportFeeBrokerPaidAmt_rep);
            
            SetResult("sDemandCreditReportFeeBrokerDueAmt", dataLoan.sDemandCreditReportFeeBrokerDueAmt_rep);
            SetResult("sDemandProcessingFeeLenderAmt", dataLoan.sDemandProcessingFeeLenderAmt_rep);
            SetResult("sDemandProcessingFeeBrokerPaidAmt", dataLoan.sDemandProcessingFeeBrokerPaidAmt_rep);
            SetResult("sDemandProcessingFeeBrokerDueAmt", dataLoan.sDemandProcessingFeeBrokerDueAmt_rep);
            SetResult("sDemandLoanDocFeeLenderAmt", dataLoan.sDemandLoanDocFeeLenderAmt_rep);
            SetResult("sDemandLoanDocFeeBrokerPaidAmt", dataLoan.sDemandLoanDocFeeBrokerPaidAmt_rep);
            SetResult("sDemandLoanDocFeeBorrowerAmt", dataLoan.sDemandLoanDocFeeBorrowerAmt_rep);
            SetResult("sDemandLoanDocFeeBrokerDueAmt", dataLoan.sDemandLoanDocFeeBrokerDueAmt_rep);
            
            SetResult("sDemandTotalLenderAmt", dataLoan.sDemandTotalLenderAmt_rep);
            SetResult("sDemandTotalBrokerPaidAmt", dataLoan.sDemandTotalBrokerPaidAmt_rep);
            SetResult("sDemandTotalBrokerDueAmt", dataLoan.sDemandTotalBrokerDueAmt_rep); 
            SetResult("sDemandTotalBorrowerAmt", dataLoan.sDemandTotalBorrowerAmt_rep);
            SetResult("sDemandLOrigFeeLenderPc", dataLoan.sDemandLOrigFeeLenderPc_rep);
            SetResult("sDemandLOrigFeeLenderMb", dataLoan.sDemandLOrigFeeLenderMb_rep);
            SetResult("sDemandLOrigFeeLenderAmt", dataLoan.sDemandLOrigFeeLenderAmt_rep);
            SetResult("sDemandLOrigFeeBrokerAmt", dataLoan.sDemandLOrigFeeBrokerAmt_rep);

            SetResult("sDemandLDiscountLenderPc", dataLoan.sDemandLDiscountLenderPc_rep);
            SetResult("sDemandLDiscountLenderMb", dataLoan.sDemandLDiscountLenderMb_rep);
            SetResult("sDemandLDiscountLenderAmt", dataLoan.sDemandLDiscountLenderAmt_rep);
            SetResult("sDemandLDiscountBrokerAmt", dataLoan.sDemandLDiscountBrokerAmt_rep);

            SetResult("sDemandYieldSpreadPremiumLenderPc", dataLoan.sDemandYieldSpreadPremiumLenderPc_rep);
            SetResult("sDemandYieldSpreadPremiumLenderMb", dataLoan.sDemandYieldSpreadPremiumLenderMb_rep);
            SetResult("sDemandYieldSpreadPremiumLenderAmt", dataLoan.sDemandYieldSpreadPremiumLenderAmt_rep);
            SetResult("sDemandYieldSpreadPremiumBrokerAmt", dataLoan.sDemandYieldSpreadPremiumBrokerAmt_rep);

            SetResult("sDemandYieldSpreadPremiumBorrowerPc", dataLoan.sDemandYieldSpreadPremiumBorrowerPc_rep);
            SetResult("sDemandYieldSpreadPremiumBorrowerMb", dataLoan.sDemandYieldSpreadPremiumBorrowerMb_rep);
            SetResult("sDemandYieldSpreadPremiumBorrowerAmt", dataLoan.sDemandYieldSpreadPremiumBorrowerAmt_rep);

            SetResult("sDemandNotes", dataLoan.sDemandNotes);
            SetResult("sSubmitBrokerFax", dataLoan.sSubmitBrokerFax);
            SetResult("sSubmitPropTDesc", dataLoan.sSubmitPropTDesc);
            SetResult("sEstCloseDLckd", dataLoan.sEstCloseDLckd);
            SetResult("sEstCloseD", dataLoan.sEstCloseD_rep);

            SetResult("sQualTermCalculationType", dataLoan.sQualTermCalculationType);
            SetResult("sQualTerm", dataLoan.sQualTerm_rep);

            LoanSubmission.LoadDataForControls(dataLoan, dataApp, this);
        }

    }
	/// <summary>
	/// Summary description for LoanSubmissionService.
	/// </summary>
	public partial class LoanSubmissionService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new LoanSubmissionServiceItem());
        }

	}
}
