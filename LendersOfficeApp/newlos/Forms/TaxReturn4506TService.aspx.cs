﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class TaxReturn4506TService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        private CPageData CreatePageData(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(TaxReturn4506TService));
        }
        protected override void Process(string methodName)
        {
            switch (methodName) {
                case "CalculateData":
                    CalculateData();
                    break;
                case "LoadTab":
                    LoadTab();
                    break;
                case "SaveAndLoadTab":
                    SaveAndLoadTab();
                    break;
                case "SaveTab":
                    SaveTab();
                    break;
            }

        }
        private void CalculateData()
        {
            Guid sLId = GetGuid("LoanID");
            Guid aAppId = GetGuid("ApplicationID");
            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(aAppId);

            dataApp.aB4506TNm = GetString("aB4506TNm");
            dataApp.aC4506TNm = GetString("aC4506TNm");
            dataApp.aB4506TNmLckd = GetBool("aB4506TNmLckd");
            dataApp.aC4506TNmLckd = GetBool("aC4506TNmLckd");

            SetResult("aB4506TNm", dataApp.aB4506TNm);
            SetResult("aB4506TNmLckd", dataApp.aB4506TNmLckd);
            SetResult("aC4506TNm", dataApp.aC4506TNm);
            SetResult("aC4506TNmLckd", dataApp.aC4506TNmLckd);

        }
        private void SaveTab()
        {
            Guid sLId = GetGuid("LoanID");
            Guid aAppId = GetGuid("ApplicationID");
            bool isBorrower = GetBool("IsBorrower");
            int sFileVersion = GetInt("sFileVersion");
            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(aAppId);

            SaveTab(dataLoan, dataApp, isBorrower, sFileVersion);
            dataLoan.Save();

        }
        private void SaveAndLoadTab()
        {
            Guid sLId = GetGuid("LoanID");
            Guid aAppId = GetGuid("ApplicationID");
            bool isBorrower = GetBool("IsBorrower"); 
            bool isPreviousTabBorrower = !isBorrower;// 10/19/2010 dd - This represent previous tab.
            int sFileVersion = GetInt("sFileVersion");
            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(aAppId);

            SaveTab(dataLoan, dataApp, isPreviousTabBorrower, sFileVersion);
            dataLoan.Save();

            LoadTab(dataLoan, dataApp, isBorrower);
        }
        private void LoadTab()
        {
            Guid sLId = GetGuid("LoanID");
            Guid aAppId = GetGuid("ApplicationID");
            bool isBorrower = GetBool("IsBorrower");
            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(aAppId);

            LoadTab(dataLoan, dataApp, isBorrower);
        }
        private void SaveTab(CPageData dataLoan, CAppData dataApp, bool isBorrower, int sFileVersion)
        {
            dataApp.aIs4506TFiledTaxesSeparately = GetBool("aIs4506TFiledTaxesSeparately");
            dataApp.aB4506TSsnTinEin = GetString("aB4506TSsnTinEin");
            dataApp.aC4506TSsnTinEin = GetString("aC4506TSsnTinEin");
            dataApp.aB4506TNm = GetString("aB4506TNm");
            dataApp.aB4506TNmLckd = GetBool("aB4506TNmLckd");
            dataApp.aC4506TNm = GetString("aC4506TNm");
            dataApp.aC4506TNmLckd = GetBool("aC4506TNmLckd");
            if (isBorrower)
            {
                dataApp.aB4506TPrevStreetAddr = GetString("a4506TPrevStreetAddr");
                dataApp.aB4506TPrevCity = GetString("a4506TPrevCity");
                dataApp.aB4506TPrevState = GetString("a4506TPrevState");
                dataApp.aB4506TPrevZip = GetString("a4506TPrevZip");
                dataApp.aB4506TThirdPartyName = GetString("a4506TThirdPartyName");
                dataApp.aB4506TThirdPartyStreetAddr = GetString("a4506TThirdPartyStreetAddr");
                dataApp.aB4506TThirdPartyCity = GetString("a4506TThirdPartyCity");
                dataApp.aB4506TThirdPartyState = GetString("a4506TThirdPartyState");
                dataApp.aB4506TThirdPartyZip = GetString("a4506TThirdPartyZip");
                dataApp.aB4506TThirdPartyPhone = GetString("a4506TThirdPartyPhone");
                dataApp.aB4506TTranscript = GetString("a4506TTranscript");
                dataApp.aB4056TIsReturnTranscript = GetBool("a4056TIsReturnTranscript");
                dataApp.aB4506TIsAccountTranscript = GetBool("a4506TIsAccountTranscript");
                dataApp.aB4056TIsRecordAccountTranscript = GetBool("a4056TIsRecordAccountTranscript");
                dataApp.aB4506TVerificationNonfiling = GetBool("a4506TVerificationNonfiling");
                dataApp.aB4506TSeriesTranscript = GetBool("a4506TSeriesTranscript");
                dataApp.aB4506TYear1_rep = GetString("a4506TYear1");
                dataApp.aB4506TYear2_rep = GetString("a4506TYear2");
                dataApp.aB4506TYear3_rep = GetString("a4506TYear3");
                dataApp.aB4506TYear4_rep = GetString("a4506TYear4");
                dataApp.aB4506TRequestYrHadIdentityTheft = GetBool("a4506TRequestYrHadIdentityTheft");
                dataApp.aB4506TSignatoryAttested = GetBool("a4506TSignatoryAttested");
            }
            else
            {
                dataApp.aC4506TPrevStreetAddr = GetString("a4506TPrevStreetAddr");
                dataApp.aC4506TPrevCity = GetString("a4506TPrevCity");
                dataApp.aC4506TPrevState = GetString("a4506TPrevState");
                dataApp.aC4506TPrevZip = GetString("a4506TPrevZip");
                dataApp.aC4506TThirdPartyName = GetString("a4506TThirdPartyName");
                dataApp.aC4506TThirdPartyStreetAddr = GetString("a4506TThirdPartyStreetAddr");
                dataApp.aC4506TThirdPartyCity = GetString("a4506TThirdPartyCity");
                dataApp.aC4506TThirdPartyState = GetString("a4506TThirdPartyState");
                dataApp.aC4506TThirdPartyZip = GetString("a4506TThirdPartyZip");
                dataApp.aC4506TThirdPartyPhone = GetString("a4506TThirdPartyPhone");
                dataApp.aC4506TTranscript = GetString("a4506TTranscript");
                dataApp.aC4056TIsReturnTranscript = GetBool("a4056TIsReturnTranscript");
                dataApp.aC4506TIsAccountTranscript = GetBool("a4506TIsAccountTranscript");
                dataApp.aC4056TIsRecordAccountTranscript = GetBool("a4056TIsRecordAccountTranscript");
                dataApp.aC4506TVerificationNonfiling = GetBool("a4506TVerificationNonfiling");
                dataApp.aC4506TSeriesTranscript = GetBool("a4506TSeriesTranscript");
                dataApp.aC4506TYear1_rep = GetString("a4506TYear1");
                dataApp.aC4506TYear2_rep = GetString("a4506TYear2");
                dataApp.aC4506TYear3_rep = GetString("a4506TYear3");
                dataApp.aC4506TYear4_rep = GetString("a4506TYear4");
                dataApp.aC4506TRequestYrHadIdentityTheft = GetBool("a4506TRequestYrHadIdentityTheft");
                dataApp.aC4506TSignatoryAttested = GetBool("a4506TSignatoryAttested");
            }
        }
        private void LoadTab(CPageData dataLoan, CAppData dataApp, bool isBorrower)
        {
            SetResult("aB4506TSsnTinEin", dataApp.aB4506TSsnTinEin);
            SetResult("aC4506TSsnTinEin", dataApp.aC4506TSsnTinEin);
            SetResult("aB4506TNm", dataApp.aB4506TNm);
            SetResult("aB4506TNmLckd", dataApp.aB4506TNmLckd);
            SetResult("aC4506TNm", dataApp.aC4506TNm);
            SetResult("aC4506TNmLckd", dataApp.aC4506TNmLckd);
            SetResult("a4506TPrevStreetAddr", isBorrower ? dataApp.aB4506TPrevStreetAddr : dataApp.aC4506TPrevStreetAddr);
            SetResult("a4506TPrevCity", isBorrower ? dataApp.aB4506TPrevCity : dataApp.aC4506TPrevCity);
            SetResult("a4506TPrevState", isBorrower ? dataApp.aB4506TPrevState : dataApp.aC4506TPrevState);
            SetResult("a4506TPrevZip", isBorrower ? dataApp.aB4506TPrevZip : dataApp.aC4506TPrevZip);
            SetResult("a4506TThirdPartyName", isBorrower ? dataApp.aB4506TThirdPartyName : dataApp.aC4506TThirdPartyName);
            SetResult("a4506TThirdPartyStreetAddr", isBorrower ? dataApp.aB4506TThirdPartyStreetAddr : dataApp.aC4506TThirdPartyStreetAddr);
            SetResult("a4506TThirdPartyCity", isBorrower ? dataApp.aB4506TThirdPartyCity : dataApp.aC4506TThirdPartyCity);
            SetResult("a4506TThirdPartyState", isBorrower ? dataApp.aB4506TThirdPartyState : dataApp.aC4506TThirdPartyState);
            SetResult("a4506TThirdPartyZip", isBorrower ? dataApp.aB4506TThirdPartyZip : dataApp.aC4506TThirdPartyZip);
            SetResult("a4506TThirdPartyPhone", isBorrower ? dataApp.aB4506TThirdPartyPhone : dataApp.aC4506TThirdPartyPhone);
            SetResult("a4506TTranscript", isBorrower ? dataApp.aB4506TTranscript : dataApp.aC4506TTranscript);
            SetResult("a4056TIsReturnTranscript", isBorrower ? dataApp.aB4056TIsReturnTranscript : dataApp.aC4056TIsReturnTranscript);
            SetResult("a4506TIsAccountTranscript", isBorrower ? dataApp.aB4506TIsAccountTranscript : dataApp.aC4506TIsAccountTranscript);
            SetResult("a4056TIsRecordAccountTranscript", isBorrower ? dataApp.aB4056TIsRecordAccountTranscript : dataApp.aC4056TIsRecordAccountTranscript);
            SetResult("a4506TVerificationNonfiling", isBorrower ? dataApp.aB4506TVerificationNonfiling : dataApp.aC4506TVerificationNonfiling);
            SetResult("a4506TSeriesTranscript", isBorrower ? dataApp.aB4506TSeriesTranscript : dataApp.aC4506TSeriesTranscript);
            SetResult("a4506TYear1", isBorrower ? dataApp.aB4506TYear1_rep : dataApp.aC4506TYear1_rep);
            SetResult("a4506TYear2", isBorrower ? dataApp.aB4506TYear2_rep : dataApp.aC4506TYear2_rep);
            SetResult("a4506TYear3", isBorrower ? dataApp.aB4506TYear3_rep : dataApp.aC4506TYear3_rep);
            SetResult("a4506TYear4", isBorrower ? dataApp.aB4506TYear4_rep : dataApp.aC4506TYear4_rep);
            SetResult("a4506TRequestYrHadIdentityTheft", isBorrower ? dataApp.aB4506TRequestYrHadIdentityTheft : dataApp.aC4506TRequestYrHadIdentityTheft);
            SetResult("a4506TSignatoryAttested", isBorrower ? dataApp.aB4506TSignatoryAttested : dataApp.aC4506TSignatoryAttested);
            SetResult("sFileVersion", dataLoan.sFileVersion);
        }

    }
}
