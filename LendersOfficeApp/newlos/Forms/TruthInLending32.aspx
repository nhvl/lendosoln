<%@ Page language="c#" Codebehind="TruthInLending32.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.TruthInLending32" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>TruthInLending32</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="FlowLayout" class="RightBackground">
	
    <form id="TruthInLending32" method="post" runat="server">
<table id=Table1 cellspacing=0 cellpadding=0 width="100%" border=0>
  <tr>
    <td nowrap class=MainRightHeader colspan=2>Truth In Lending Section 32</td></tr>
  <tr>
    <td nowrap class=FieldLabel>Loan Amount</td>
    <td nowrap width="100%"><ml:MoneyTextBox id=sLAmtCalc runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td></tr>
  <tr>
    <td nowrap class=FieldLabel>Optional credit insurance <asp:DropDownList id=sCreditInsIncludedTri runat="server">
<asp:ListItem Value="0">&nbsp;</asp:ListItem>
<asp:ListItem Value="1">is</asp:ListItem>
<asp:ListItem Value="2">is not</asp:ListItem>
</asp:DropDownList>&nbsp;included in the loan amount&nbsp;&nbsp;</td>
    <td nowrap></td></tr>
  <tr>
    <td nowrap class=FieldLabel>APR</td>
    <td nowrap><ml:PercentTextBox id=sApr runat="server" width="70" preset="percent" readOnly="True"></ml:PercentTextBox></td></tr>
  <tr>
    <td nowrap class=FieldLabel>Monthly Payment</td>
    <td nowrap><ml:MoneyTextBox id=sSchedPmt1 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td></tr>
  <tr>
    <td nowrap class=FieldLabel><asp:CheckBox id=sBalloonPmt runat="server" onclick="refreshCalculation();"></asp:CheckBox>At the end of your loan, you will still owe us:</td>
    <td nowrap><ml:MoneyTextBox id=sFinalBalloonPmt runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td></tr>
  <tr>
    <td nowrap class=FieldLabel><asp:CheckBox id=sHasVarRFeature runat="server" Text="Your interest may increase" onclick="refreshCalculation();"></asp:CheckBox></td>
    <td nowrap></td></tr>
  <tr>
    <td nowrap class=FieldLabel>&nbsp;&nbsp;&nbsp;&nbsp; The highest amount your payment could 
      increase to is</td>
    <td nowrap><ml:MoneyTextBox id=MaxPayment runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td></tr></table>

     </form>
	
  </body>
</html>
