<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="QRP" TagName="QualRatePopup" Src="~/newlos/Forms/QualRateCalculationPopup.ascx" %>
<%@ Import Namespace="DataAccess" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page language="c#" Codebehind="LoanSubmission.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.LoanSubmission" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
		<title>LoanSubmission</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema>
  </HEAD>
<body class=RightBackground MS_POSITIONING="FlowLayout">
<script language=javascript>
    <!--
    var oRolodex = null;
    function _init() {
        lockField(document.getElementById('sEstCloseDLckd'), 'sEstCloseD');
        lockField(document.getElementById('sQualIRLckd'), 'sQualIR');
        if (oRolodex == null)
            oRolodex = new cRolodex();

        SetQualTermReadonly();
    }

    function SetQualTermReadonly() {
        var isManual = $(".sQualTermCalculationType").val() == '<%=AspxTools.JsStringUnquoted(QualTermCalculationType.Manual)%>';
        $(".sQualTerm").prop("readonly", !isManual);
    }

    function updateBorrowerName() {
      var name = <%= AspxTools.JsGetElementById(aBLastNm) %>.value + ", " + <%= AspxTools.JsGetElementById(aBFirstNm) %>.value;
      parent.info.f_updateApplicantDDL(name, ML.aAppId);
    }    

    function CopyFromLoanInfo_btn_onClick() {
      var args = {
        LoanId: ML.sLId
      }
      var result = gService.loanedit.call('CopyFromLoanInfo', args);
      if (!result.error) {
        document.getElementById('sSubmitPropTDesc').value = result.value['sSubmitPropTDesc'];
      } else if (null != result.UserMessage) {
        alert(result.UserMessage);
      }
      updateDirtyBit();
    }
    
    function CopyFromGFE_btn_onClick() {
      var args = {
        LoanId: ML.sLId
      }
      var result = gService.loanedit.call('CopyFromGFE', args);
      if (!result.error) {
        if (result.value['sSubmitImpoundTaxes'] == "True") {
          document.getElementById('sSubmitImpoundTaxes').checked = true;
        }
        if (result.value['sSubmitImpoundHazard'] == "True") {
          document.getElementById('sSubmitImpoundHazard').checked = true;
        }
        if (result.value['sSubmitImpoundMI'] == "True") {
          document.getElementById('sSubmitImpoundMI').checked = true;
        }
        if (result.value['sSubmitImpoundFlood'] == "True") {
          document.getElementById('sSubmitImpoundFlood').checked = true;
        }
        updateDirtyBit();
      } else if (null != result.UserMessage) {
        alert(result.UserMessage);
      }
    }
    //-->
</script>

<form id=LoanSubmission method=post runat="server">
<QRP:QualRatePopup ID="QualRatePopup" runat="server" />
<table class=FormTable id=Table1 cellSpacing=0 cellPadding=0 width="100%" 
border=0>
  <tr>
    <td class=MainRightHeader noWrap>Uniform Loan 
      Submission Sheet<uc1:cmodaldlg id=CModalDlg1 runat="server"></uc1:cmodaldlg></TD></TR>
  <tr>
    <td noWrap>
      <table class=InsetBorder id=Table2 cellSpacing=0 cellPadding=0 width="99%" 
      border=0>
        <tr class=FormTableSubheader>
          <td class=FieldLabel>&nbsp; Lender</TD>
          <td noWrap><uc:cfm id=CFM1 runat="server" ZipField="sAgentLenderZip" StateField="sAgentLenderState" CityField="sAgentLenderCity" StreetAddressField="sAgentLenderAddress" AgentNameField="sAgentLenderAgentName" Type="21" FaxField="sAgentLenderAgentFax" PhoneField="sAgentLenderAgentPhone" CompanyNameField="sAgentLenderCompanyName" name="CFM1"></uc:cfm></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>&nbsp; Name</TD>
          <td noWrap width="100%"><asp:textbox id=sAgentLenderCompanyName runat="server" Width="236px"></asp:textbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>&nbsp; Contact</TD>
          <td noWrap><asp:textbox id=sAgentLenderAgentName runat="server" Width="236px"></asp:textbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>&nbsp; 
            Address&nbsp;&nbsp;&nbsp;&nbsp;</TD>
          <td noWrap><asp:textbox id=sAgentLenderAddress runat="server" Width="236"></asp:textbox></TD></TR>
        <tr>
          <td noWrap></TD>
          <td noWrap><asp:textbox id=sAgentLenderCity runat="server" Width="141px"></asp:textbox><ml:statedropdownlist id=sAgentLenderState runat="server"></ml:statedropdownlist><ml:zipcodetextbox id=sAgentLenderZip runat="server" CssClass="mask" preset="zipcode" width="50"></ml:zipcodetextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>&nbsp; Phone</TD>
          <td class=FieldLabel noWrap><ml:phonetextbox id=sAgentLenderAgentPhone runat="server" CssClass="mask" preset="phone" width="120"></ml:phonetextbox>&nbsp;Fax 
<ml:phonetextbox id=sAgentLenderAgentFax runat="server" CssClass="mask" preset="phone" width="120"></ml:phonetextbox></TD></TR></TABLE></TD></TR>
  <tr>
    <td noWrap>
      <table class=InsetBorder id=Table3 cellSpacing=0 cellPadding=0 width="99%" 
      border=0>
        <tr>
          <td noWrap>
            <table id=Table4 cellSpacing=0 cellPadding=0 border=0 
            >
              <tr>
                <td class=FormTableSubheader noWrap 
                  >Submitting Broker/Lender&nbsp;&nbsp; <uc:cfm id=CFM2 runat="server" ZipField="brokerZipcode" StateField="brokerState" CityField="brokerCity" StreetAddressField="brokerAddress" AgentNameField="loanOfficerName" Type="26" FaxField="sSubmitBrokerFax" PhoneField="loanOfficerPhone" CompanyNameField="brokerCompanyName" name="CFM2"></uc:cfm></TD>
                <TD class=FormTableSubHeader noWrap></TD>
                <td class=FormTableSubHeader noWrap 
                  >Appraisal Company&nbsp;&nbsp; <uc:cfm id=CFM3 runat="server" DepartmentNameField="appraisalDepartmentName" ZipField="appraisalZipcode" StateField="appraisalState" CityField="appraisalCity" StreetAddressField="appraisalAddress" AgentNameField="appraisalOfficer" Type="1" FaxField="appraisalFax" PhoneField="appraisalPhone" CompanyNameField="appraisalCompanyName" name="CFM3" AgentLicenseField="appraisalLicense"></uc:cfm></TD></TR>
              <tr>
                <td vAlign=top noWrap>
                  <table id=Table5 cellSpacing=0 cellPadding=0 border=0 
                  >
                    <tr>
                      <td class=FieldLabel noWrap>Name</TD>
                      <td noWrap><asp:textbox id=brokerCompanyName runat="server" Width="236px"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap 
                      >Address</TD>
                      <td noWrap><asp:textbox id=brokerAddress runat="server" Width="236px"></asp:textbox></TD></TR>
                    <tr>
                      <td noWrap></TD>
                      <td noWrap><asp:textbox id=brokerCity runat="server" Width="140px"></asp:textbox><ml:statedropdownlist id=brokerState runat="server"></ml:statedropdownlist><ml:zipcodetextbox id=brokerZipcode runat="server" CssClass="mask" preset="zipcode" width="50"></ml:zipcodetextbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Loan 
                        Officer</TD>
                      <TD class=FieldLabel noWrap><asp:textbox id=loanOfficerName runat="server"></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Agent 
                        Phone</TD>
                      <TD class=FieldLabel noWrap><ml:phonetextbox id=loanOfficerPhone runat="server" CssClass="mask" preset="phone" width="120"></ml:phonetextbox>&nbsp;</TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Agent 
                        Fax</TD>
                      <TD noWrap><ml:phonetextbox id=sSubmitBrokerFax runat="server" CssClass="mask" preset="phone" width="120"></ml:phonetextbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap 
                        >Processor</TD>
                      <TD class=FieldLabel noWrap><asp:textbox id=processorName runat="server"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap></TD>
                      <td noWrap></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap></TD>
                      <td noWrap></TD></TR></TABLE></TD>
                <TD vAlign=top noWrap></TD>
                <td vAlign=top noWrap>
                  <table id=Table6 cellSpacing=0 cellPadding=0 border=0 
                  >
                    <tr>
                      <td class=FieldLabel noWrap>Name</TD>
                      <td noWrap><asp:textbox id=appraisalCompanyName runat="server" Width="236px"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap 
                      >Address</TD>
                      <td noWrap><asp:textbox id=appraisalAddress runat="server" Width="236px"></asp:textbox></TD></TR>
                    <tr>
                      <td noWrap></TD>
                      <td noWrap><asp:textbox id=appraisalCity runat="server" Width="140px"></asp:textbox><ml:statedropdownlist id=appraisalState runat="server"></ml:statedropdownlist><ml:zipcodetextbox id=appraisalZipcode runat="server" CssClass="mask" preset="zipcode" width="50"></ml:zipcodetextbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap>Phone</TD>
                      <td class=FieldLabel noWrap><ml:phonetextbox id=appraisalPhone runat="server" CssClass="mask" preset="phone" width="120"></ml:phonetextbox>&nbsp;Fax 
<ml:phonetextbox id=appraisalFax runat="server" CssClass="mask" preset="phone" width="120"></ml:phonetextbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap 
                        >Appraiser</TD>
                      <td noWrap><asp:textbox id=appraisalOfficer runat="server"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap>License 
                        #</TD>
                      <td noWrap><asp:textbox id=appraisalLicense runat="server"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap></TD>
                      <td noWrap></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap></TD>
                      <td noWrap 
              ></TD></TR></TABLE></TD></TR>
              <tr>
                <td class=FormTableSubHeader noWrap 
                  >Escrow Company&nbsp;&nbsp; <uc:cfm id=CFM4 runat="server" ZipField="escrowZipcode" StateField="escrowState" CityField="escrowCity" StreetAddressField="escrowAddress" AgentNameField="escrowOfficer" Type="2" FaxField="escrowFax" PhoneField="escrowPhone" CompanyNameField="escrowCompanyName" name="CFM4"></uc:cfm></TD>
                <TD class=FormTableSubheader noWrap 
                  >&nbsp;&nbsp;&nbsp;</TD>
                <td class=FormTableSubheader noWrap>Title 
                  Company&nbsp;&nbsp; <uc:cfm id=CFM5 runat="server" ZipField="titleZip" StateField="titleState" CityField="titleCity" StreetAddressField="titleAddress" AgentNameField="titleOfficer" Type="4" FaxField="titleFax" PhoneField="titlePhone" CompanyNameField="titleCompanyName" name="CFM5"></uc:cfm></TD></TR>
              <tr>
                <td vAlign=top noWrap>
                  <table id=Table7 cellSpacing=0 cellPadding=0 border=0 
                  >
                    <tr>
                      <td class=FieldLabel noWrap>Name</TD>
                      <td noWrap><asp:textbox id=escrowCompanyName runat="server" Width="236px"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap 
                      >Address</TD>
                      <td noWrap><asp:textbox id=escrowAddress runat="server" Width="236px"></asp:textbox></TD></TR>
                    <tr>
                      <td noWrap></TD>
                      <td noWrap><asp:textbox id=escrowCity runat="server" Width="140px"></asp:textbox><ml:statedropdownlist id=escrowState runat="server"></ml:statedropdownlist><ml:zipcodetextbox id=escrowZipcode runat="server" CssClass="mask" preset="zipcode" width="50"></ml:zipcodetextbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap>Phone</TD>
                      <td class=FieldLabel noWrap><ml:phonetextbox id=escrowPhone runat="server" CssClass="mask" preset="phone" width="120"></ml:phonetextbox>&nbsp;Fax 
<ml:phonetextbox id=escrowFax runat="server" CssClass="mask" preset="phone" width="120"></ml:phonetextbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap 
                      >Officer</TD>
                      <td noWrap><asp:textbox id=escrowOfficer runat="server"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap>Escrow 
                      #</TD>
                      <td noWrap><asp:textbox id=escrowNumber runat="server"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap colSpan=2>
                          Estimated Close of Escrow&nbsp;&nbsp; 
                          <ml:datetextbox id=sEstCloseD runat="server" CssClass="mask" preset="date" width="75"></ml:datetextbox>
                          <input type="checkbox" runat="server" id="sEstCloseDLckd" onchange="lockField(this, 'sEstCloseD'); refreshCalculation();"/>Lock
                      </td>
                    </tr>
                    <tr>
                      <td class=FieldLabel noWrap></TD>
                      <td noWrap></TD></TR></TABLE></TD>
                <TD vAlign=top noWrap></TD>
                <td vAlign=top noWrap>
                  <table id=Table8 cellSpacing=0 cellPadding=0 border=0 
                  >
                    <tr>
                      <td class=FieldLabel noWrap>Name</TD>
                      <td noWrap><asp:textbox id=titleCompanyName runat="server" Width="236px"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap 
                      >Address</TD>
                      <td noWrap><asp:textbox id=titleAddress runat="server" Width="236px"></asp:textbox></TD></TR>
                    <tr>
                      <td noWrap></TD>
                      <td noWrap><asp:textbox id=titleCity runat="server" Width="140px"></asp:textbox><ml:statedropdownlist id=titleState runat="server"></ml:statedropdownlist><ml:zipcodetextbox id=titleZipcode runat="server" CssClass="mask" preset="zipcode" width="50"></ml:zipcodetextbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap>Phone</TD>
                      <td class=FieldLabel noWrap><ml:phonetextbox id=titlePhone runat="server" CssClass="mask" preset="phone" width="120"></ml:phonetextbox>&nbsp;Fax 
<ml:phonetextbox id=titleFax runat="server" CssClass="mask" preset="phone" width="120"></ml:phonetextbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap 
                      >Officer</TD>
                      <td noWrap><asp:textbox id=titleOfficer runat="server"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap>Title 
                      #</TD>
                      <td noWrap><asp:textbox id=titleNumber runat="server"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap></TD>
                      <td noWrap></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap></TD>
                      <td noWrap 
              ></TD></TR></TABLE></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
  <tr>
    <td noWrap>
      <table class=InsetBorder id=Table9 cellSpacing=0 cellPadding=0 width="99%" 
      border=0>
        <tr>
          <td class=FormTableSubHeader noWrap>Loan 
            Summary and Terms</TD></TR>
        <tr>
          <td noWrap>
            <table id=Table10 cellSpacing=0 cellPadding=0 border=0 
            >
              <tr>
                <td noWrap></TD>
                <td class=FieldLabel noWrap>First 
                  Name&nbsp; </TD>
                <td class=FieldLabel noWrap>Middle 
Name</TD>
                <td class=FieldLabel noWrap>Last Name</TD>
                <td class=FieldLabel noWrap>Suffix</TD>
                <td class=FieldLabel noWrap>SSN</TD></TR>
              <tr>
                <td class=FieldLabel noWrap>Borrower</TD>
                <td noWrap><asp:textbox id=aBFirstNm runat="server" maxlength="21" onchange="updateBorrowerName();"></asp:textbox></TD>
                <td noWrap><asp:textbox id=aBMidNm runat="server" maxlength="21"></asp:textbox></TD>
                <td noWrap><asp:textbox id=aBLastNm runat="server" maxlength="21" onchange="updateBorrowerName();"></asp:textbox></TD>
                <td noWrap><ml:ComboBox id=aBSuffix runat="server" Width="64px" maxlength="21"></ml:ComboBox></TD>
                <td noWrap><ml:ssntextbox id=aBSSN runat="server" CssClass="mask" preset="ssn" width="90"></ml:ssntextbox></TD></TR>
              <tr>
                <td class=FieldLabel noWrap>Co-borrower</TD>
                <td noWrap><asp:textbox id=aCFirstNm runat="server" maxlength="21"></asp:textbox></TD>
                <td noWrap><asp:textbox id=aCMidNm runat="server" maxlength="21"></asp:textbox></TD>
                <td noWrap><asp:textbox id=aCLastNm runat="server" maxlength="21"></asp:textbox></TD>
                <td noWrap><ml:ComboBox id=aCSuffix runat="server" Width="64px" maxlength="21"></ml:ComboBox></TD>
                <td noWrap><ml:ssntextbox id=aCSSN runat="server" CssClass="mask" preset="ssn" width="90"></ml:ssntextbox></TD></TR></TABLE></TD></TR>
        <tr>
          <td noWrap>
            <table id=Table11 cellSpacing=0 cellPadding=0 border=0 
            >
              <tr>
                <td noWrap></TD>
                <td class=FieldLabel noWrap>Property 
                  Address</TD>
                <td noWrap></TD></TR>
              <tr>
                <td noWrap></TD>
                <td class=FieldLabel noWrap>Address</TD>
                <td noWrap><asp:textbox id=sSpAddr runat="server" Width="236px" MaxLength="60"></asp:textbox></TD></TR>
              <tr>
                <td noWrap></TD>
                <td noWrap></TD>
                <td noWrap><asp:textbox id=sSpCity runat="server" Width="140px"></asp:textbox><ml:statedropdownlist id=sSpState runat="server"></ml:statedropdownlist><ml:zipcodetextbox id=sSpZip runat="server" CssClass="mask" preset="zipcode" width="50"></ml:zipcodetextbox></TD></TR>
              <tr>
                <td noWrap></TD>
                <td class=FieldLabel noWrap>County</TD>
                <td class=FieldLabel noWrap><%--01/17/08 OMP 18913 Changing county fields to drop down list.--%><asp:dropdownlist id=sSpCounty runat="server"></asp:dropdownlist>&nbsp;&nbsp;Census 
                  Tract <asp:textbox id=sHmdaCensusTract runat="server"></asp:textbox></TD></TR>
              <tr>
                <td noWrap></TD>
                <td class=FieldLabel noWrap>Loan Program</TD>
                <td class=FieldLabel noWrap><asp:textbox id=sLpTemplateNm runat="server" Width="235px"></asp:textbox>&nbsp;&nbsp;Program 
                  Code: <asp:textbox id=sSubmitProgramCode runat="server" Width="114px"></asp:textbox></TD></TR>
              <tr>
                <td noWrap></TD>
                <td class=FieldLabel noWrap>Property Type</TD>
                <td class=FieldLabel noWrap>
                  <input type="button" id="CopyFromLoanInfo_btn" onclick="CopyFromLoanInfo_btn_onClick();" NoHighlight value="Copy from Loan Info"/>
                  <asp:textbox id=sSubmitPropTDesc runat="server" Width="16em"></asp:textbox>
                  &nbsp;&nbsp;Num of Units
                  <asp:textbox id=sUnitsNum runat="server" Width="36px"></asp:textbox>
                  &nbsp;&nbsp;Doc Type:
                  <asp:checkbox id=sSubmitDocFull runat="server" Text="Full Doc"></asp:checkbox>
                  <asp:checkbox id=sSubmitDocOther runat="server" Text="Other"></asp:checkbox>
                </td>
              </tr>
              <tr>
                <td noWrap></TD>
                <td class=FieldLabel noWrap>Impounds</TD>
                <td class=FieldLabel noWrap>
                  <input type="button" id="CopyFromGFE_btn" onclick="CopyFromGFE_btn_onClick();" NoHighlight value="Copy from GFE"/>
                  <asp:checkbox id=sSubmitImpoundTaxes runat="server" Text="Taxes"></asp:checkbox>
                  <asp:checkbox id=sSubmitImpoundHazard runat="server" Text="Hazard"></asp:checkbox>
                  <asp:checkbox id=sSubmitImpoundMI runat="server" Text="MMI/PMI"></asp:checkbox>
                  <asp:checkbox id=sSubmitImpoundFlood runat="server" Text="Flood"></asp:checkbox>
                  <asp:checkbox id=sSubmitImpoundOther runat="server" Text="Other:"></asp:checkbox>
                  &nbsp;&nbsp;&nbsp;
                  <asp:textbox id=sSubmitImpoundOtherDesc runat="server"></asp:textbox>
                </td>
              </tr>
              <tr>
                <td noWrap></TD>
                <td class=FieldLabel noWrap>Lien Position</TD>
                <td class=FieldLabel noWrap>
                    <asp:dropdownlist id=sLienPosT runat="server"></asp:dropdownlist>
                    &nbsp;&nbsp;Loan Purpose
                    <asp:dropdownlist id=sLPurposeT runat="server"></asp:dropdownlist>
                    &nbsp;&nbsp;Occupancy 
                    <asp:dropdownlist id=aOccT runat="server"></asp:dropdownlist>
                 </td>
              </tr>
           </table>
        </td>
        </tr>
        <tr>
          <td noWrap>
            <table id=Table12 cellSpacing=0 cellPadding=0 border=0 
            >
              <tr>
                <td vAlign=top noWrap>
                  <table id=Table14 cellSpacing=0 cellPadding=0 border=0 
                  >
                    <tr>
                      <td class=FieldLabel noWrap>Type of 
                        Buydown&nbsp;</TD>
                      <td noWrap colSpan=3><asp:textbox id=sSubmitBuydownDesc runat="server"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap>Adj 
                        Period</TD>
                      <td class=FieldLabel noWrap><asp:textbox id=sRAdjCapMon runat="server" Width="36px"></asp:textbox>mths</TD>
                      <td class=FieldLabel noWrap>Adj 
                      Cap</TD>
                      <td noWrap><ml:percenttextbox id=sRAdjCapR runat="server" CssClass="mask" preset="percent" width="70"></ml:percenttextbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap>Index</TD>
                      <td noWrap><ml:percenttextbox id=sRAdjIndexR runat="server" CssClass="mask" preset="percent" width="70"></ml:percenttextbox></TD>
                      <td class=FieldLabel noWrap>Pmt 
                      Cap</TD>
                      <td noWrap><ml:percenttextbox id=sSubmitInitPmtCapR runat="server" CssClass="mask" preset="percent" width="70"></ml:percenttextbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap 
                      >Margin</TD>
                      <td noWrap><ml:percenttextbox id=sRAdjMarginR runat="server" CssClass="mask" preset="percent" width="70"></ml:percenttextbox></TD>
                      <td class=FieldLabel noWrap>Life 
                      Cap</TD>
                      <td noWrap><ml:percenttextbox id=sRAdjLifeCapR runat="server" CssClass="mask" preset="percent" width="70"></ml:percenttextbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap>Other 
                        Desc.</TD>
                      <td noWrap colSpan=3><asp:textbox id=sSubmitRAdjustOtherDesc runat="server"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap><asp:radiobutton id=sRFloatRB runat="server" GroupName="sRLckdIsLocked"></asp:radiobutton><ml:EncodedLabel id=sRFloatRBText runat="server">Float</ml:EncodedLabel></TD>
                      <td noWrap colSpan=3></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap><asp:radiobutton id=sRLckdIsLockedRB runat="server" GroupName="sRLckdIsLocked"></asp:radiobutton><ml:PassThroughLabel id=sRLckdIsLockedRBText runat="server">Locked on:</ml:PassThroughLabel></TD>
                      <td noWrap><ml:datetextbox id=sRLckdD runat="server" CssClass="mask" preset="date" width="75"></ml:datetextbox></TD>
                      <td class=FieldLabel style="PADDING-RIGHT: 5px" noWrap 
                      align=right>MI:</TD>
                      <td class=FieldLabel noWrap><asp:radiobuttonlist id=sSubmitMIYes runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
																<asp:ListItem Value="True">Yes</asp:ListItem>
																<asp:ListItem Value="False">No</asp:ListItem>
															</asp:radiobuttonlist></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap><%= AspxTools.HtmlControl(LabelForsRLckdNumOfDays) %></TD>
                      <td noWrap><asp:textbox id=sRLckdNumOfDays runat="server" Width="36px"></asp:textbox></TD>
                      <td class=FieldLabel style="PADDING-RIGHT: 5px" noWrap 
                      align=right>Type</TD>
                      <td noWrap><asp:textbox id=sSubmitMITypeDesc runat="server"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap><%= AspxTools.HtmlControl(LabelForsRLckdExpiredD) %></TD>
                      <td noWrap colSpan=3><ml:datetextbox id=sRLckdExpiredD runat="server" CssClass="mask" preset="date" width="75"></ml:datetextbox></TD></TR></TABLE></TD>
                <td vAlign=top noWrap 
                  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                </TD>
                <td vAlign=top noWrap>
                  <table id=Table13 cellSpacing=0 cellPadding=0 border=0 
                  >
                    <tr>
                      <td class=FieldLabel noWrap>Loan 
                        Amount</TD>
                      <td noWrap><ml:moneytextbox id=sFinalLAmt runat="server" CssClass="mask" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap>Note 
                        Rate</TD>
                      <td noWrap><ml:percenttextbox id=sNoteIR runat="server" CssClass="mask" preset="percent" width="70" ReadOnly="True" onchange="refreshCalculation();" ></ml:percenttextbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap><a id="QualRateCalcLink">Qual Rate</a></TD>
                      <td noWrap>
                          <ml:percenttextbox id="sQualIR" runat="server" CssClass="mask" preset="percent" width="70"></ml:percenttextbox>
                          <asp:CheckBox ID="sQualIRLckd" runat="server" Text="Lock" onclick="refreshCalculation();" />
                      </TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap>Term / 
                        Due (mths)</TD>
                      <td noWrap><asp:textbox id=sTerm runat="server" Width="36px" ReadOnly="True"></asp:textbox>/<asp:textbox id=sDue runat="server" Width="36px" ReadOnly="True"></asp:textbox></TD>
                    </tr>

                    <tr>
                      <td class=FieldLabel noWrap>Qual Term</td>
                      <td noWrap>
                        <asp:DropDownList id="sQualTermCalculationType" runat="server" onchange="refreshCalculation();" CssClass="sQualTermCalculationType" />
                        &nbsp
                        <input type="text" id="sQualTerm" runat="server" preset="numeric" onchange="refreshCalculation();" class="sQualTerm" />
                      </TD>
                    </tr>

                    <tr>
                      <td class=FieldLabel noWrap>Amort. 
                        Desc.</TD>
                      <td noWrap><asp:textbox id=sSubmitAmortDesc runat="server" Width="88px"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap>Sales 
                        Price</TD>
                      <td noWrap><ml:moneytextbox id=sPurchPrice runat="server" CssClass="mask" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap 
                        >Appraisal</TD>
                      <td noWrap><ml:moneytextbox id=sApprVal runat="server" CssClass="mask" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap>Down 
                        Payment</TD>
                      <td noWrap><ml:moneytextbox id=sEquity runat="server" CssClass="mask" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap>LTV</TD>
                      <td noWrap><ml:percenttextbox id=sLtvR runat="server" CssClass="mask" preset="percent" width="70" ReadOnly="True"></ml:percenttextbox></TD></TR>
                    <tr>
                      <td class=FieldLabel noWrap>CLTV</TD>
                      <td noWrap><ml:percenttextbox id=sCltvR runat="server" CssClass="mask" preset="percent" width="70" ReadOnly="True"></ml:percenttextbox></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
        <tr>
          <td noWrap></TD></TR>
        <tr>
          <td noWrap></TD></TR></TABLE></TD></TR>
  <tr>
    <td noWrap>
      <table class=InsetBorder id=Table15 cellSpacing=0 cellPadding=0 
      width="99%" border=0>
        <tr>
          <td class=FormTableSubHeader noWrap>Demand</TD>
          <td class=FormTableSubHeader noWrap></TD>
          <td class=FormTableSubHeader noWrap>Lender</TD>
          <td class=FormTableSubHeader noWrap width=10 
          >&nbsp;</TD>
          <td class=FormTableSubHeader noWrap>Paid to 
            Broker</TD>
          <td class=FormTableSubHeader noWrap width=10></TD>
          <td class=FormTableSubHeader noWrap>Due to 
            Broker</TD>
          <td class=FormTableSubHeader noWrap></TD>
          <td class=FormTableSubHeader noWrap 
        >Borrower</TD></TR>
        <tr>
          <td class=FieldLabel noWrap>Loan Origination</TD>
          <td noWrap><ml:percenttextbox id=sDemandLOrigFeeLenderPc runat="server" CssClass="mask" preset="percent" width="50px" onchange="refreshCalculation();"></ml:percenttextbox>+ 
<ml:moneytextbox id=sDemandLOrigFeeLenderMb runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td noWrap><ml:moneytextbox id=sDemandLOrigFeeLenderAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandLOrigFeeBrokerAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sLOrigF runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>Loan Discount</TD>
          <td noWrap><ml:percenttextbox id=sDemandLDiscountLenderPc runat="server" CssClass="mask" preset="percent" width="50px" onchange="refreshCalculation();"></ml:percenttextbox>+ 
<ml:moneytextbox id=sDemandLDiscountLenderMb runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td noWrap><ml:moneytextbox id=sDemandLDiscountLenderAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandLDiscountBrokerAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sLDiscnt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>Yield Spread 
          Prem.</TD>
          <td noWrap><ml:percenttextbox id=sDemandYieldSpreadPremiumLenderPc runat="server" CssClass="mask" preset="percent" width="50px" onchange="refreshCalculation();"></ml:percenttextbox>+ 
<ml:moneytextbox id=sDemandYieldSpreadPremiumLenderMb runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td noWrap><ml:moneytextbox id=sDemandYieldSpreadPremiumLenderAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandYieldSpreadPremiumBrokerAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap><ml:percenttextbox id=sDemandYieldSpreadPremiumBorrowerPc runat="server" CssClass="mask" preset="percent" width="50px" onchange="refreshCalculation();"></ml:percenttextbox>+ 
<ml:moneytextbox id=sDemandYieldSpreadPremiumBorrowerMb runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td noWrap><ml:moneytextbox id=sDemandYieldSpreadPremiumBorrowerAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>Appraisal Fee</TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandApprFeeLenderAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandApprFeeBrokerPaidAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td class=FieldLabel noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandApprFeeBrokerDueAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sApprF runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>Credit Report 
Fee</TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandCreditReportFeeLenderAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandCreditReportFeeBrokerPaidAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td class=FieldLabel noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandCreditReportFeeBrokerDueAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sCrF runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>Processing Fee</TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandProcessingFeeLenderAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandProcessingFeeBrokerPaidAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td class=FieldLabel noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandProcessingFeeBrokerDueAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sProcF runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>Loan Document</TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandLoanDocFeeLenderAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandLoanDocFeeBrokerPaidAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td class=FieldLabel noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandLoanDocFeeBrokerDueAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandLoanDocFeeBorrowerAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
        <tr>
          <td noWrap colSpan=2><asp:textbox id=sDemandU1LenderDesc runat="server"></asp:textbox></TD>
          <td noWrap><ml:moneytextbox id=sDemandU1LenderAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU1BrokerPaidAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td class=FieldLabel noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU1BrokerDueAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU1BorrowerAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
        <tr>
          <td noWrap colSpan=2><asp:textbox id=sDemandU2LenderDesc runat="server"></asp:textbox></TD>
          <td noWrap><ml:moneytextbox id=sDemandU2LenderAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU2BrokerPaidAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td class=FieldLabel noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU2BrokerDueAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU2BorrowerAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
        <tr>
          <td noWrap colSpan=2><asp:textbox id=sDemandU3LenderDesc runat="server"></asp:textbox></TD>
          <td noWrap><ml:moneytextbox id=sDemandU3LenderAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU3BrokerPaidAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td class=FieldLabel noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU3BrokerDueAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU3BorrowerAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
        <tr>
          <td noWrap colSpan=2><asp:textbox id=sDemandU4LenderDesc runat="server"></asp:textbox></TD>
          <td noWrap><ml:moneytextbox id=sDemandU4LenderAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU4BrokerPaidAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td class=FieldLabel noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU4BrokerDueAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU4BorrowerAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
        <tr>
          <td style="HEIGHT: 23px" noWrap colSpan=2><asp:textbox id=sDemandU5LenderDesc runat="server"></asp:textbox></TD>
          <td style="HEIGHT: 23px" noWrap><ml:moneytextbox id=sDemandU5LenderAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td style="HEIGHT: 23px" noWrap></TD>
          <td style="HEIGHT: 23px" noWrap><ml:moneytextbox id=sDemandU5BrokerPaidAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td class=FieldLabel style="HEIGHT: 23px" noWrap 
          ></TD>
          <td style="HEIGHT: 23px" noWrap><ml:moneytextbox id=sDemandU5BrokerDueAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td style="HEIGHT: 23px" noWrap></TD>
          <td style="HEIGHT: 23px" noWrap><ml:moneytextbox id=sDemandU5BorrowerAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
        <tr>
          <td noWrap colSpan=2><asp:textbox id=sDemandU6LenderDesc runat="server"></asp:textbox></TD>
          <td noWrap><ml:moneytextbox id=sDemandU6LenderAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU6BrokerPaidAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td class=FieldLabel noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU6BrokerDueAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU6BorrowerAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
        <tr>
          <td noWrap colSpan=2><asp:textbox id=sDemandU7LenderDesc runat="server"></asp:textbox></TD>
          <td noWrap><ml:moneytextbox id=sDemandU7LenderAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU7BrokerPaidAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
          <td class=FieldLabel noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU7BrokerDueAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandU7BorrowerAmt runat="server" CssClass="mask" preset="money" width="70px" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>Total</TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandTotalLenderAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandTotalBrokerPaidAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandTotalBrokerDueAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD>
          <td noWrap></TD>
          <td noWrap><ml:moneytextbox id=sDemandTotalBorrowerAmt runat="server" CssClass="mask" preset="money" width="70px" ReadOnly="True"></ml:moneytextbox></TD></TR></TABLE></TD></TR>
  <tr>
    <td noWrap>
      <table class=InsetBorder id=Table16 cellSpacing=0 cellPadding=0 
      width="99%" border=0>
        <tr>
          <td class=FormTableSubHeader noWrap>Comments / 
            Other Instructions</TD></TR>
        <tr>
          <td noWrap><asp:textbox id=sDemandNotes runat="server" Width="573px" Height="112px" TextMode="MultiLine"></asp:textbox></TD></TR></TABLE></TD></TR></TABLE></FORM>
	</body>
</HTML>
