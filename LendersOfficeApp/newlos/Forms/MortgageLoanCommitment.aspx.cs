using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
	public partial class MortgageLoanCommitment : BaseLoanPage
	{

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.PageID = "MortgageLoanCommitment";
            this.PageTitle = "Mortgage Loan Commitment";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CMortgageLoanCommitment_1PDF);
            MortgageLoanCommitmentZip.SmartZipcode(MortgageLoanCommitmentCity, MortgageLoanCommitmentState);
            MortgageLoanCommitmentAlternateLenderZip.SmartZipcode(MortgageLoanCommitmentAlternateLenderCity, MortgageLoanCommitmentAlternateLenderState);
            RegisterJsScript("LQBPopup.js");
        }
        protected override void LoadData() 
        {
            if (Broker.IsUseNewTaskSystem)
            {
                GotoConditionsLink.HRef = "javascript:linkMe('../Underwriting/Conditions/TaskConditions.aspx')";
            }
            else 
            {
                GotoConditionsLink.HRef = string.Format("javascript:linkMe('{0}')", Broker.HasLenderDefaultFeatures ? "../Underwriting/TaskConditions.aspx" : "../Status/Conditions.aspx");
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(MortgageLoanCommitment));
            dataLoan.InitLoad();

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.MortgageLoanCommitment, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            MortgageLoanCommitmentCompanyName.Text = preparer.CompanyName;
            MortgageLoanCommitmentStreetAddr.Text = preparer.StreetAddr;
            MortgageLoanCommitmentCity.Text = preparer.City;
            MortgageLoanCommitmentState.Value = preparer.State;
            MortgageLoanCommitmentZip.Text = preparer.Zip;
            MortgageLoanCommitmentPrepareDate.Text = preparer.PrepareDate_rep;
			MortgageLoanCommitmentPrepareDate.ToolTip = "Hint:  Enter 't' for today's date.";

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.MortgageLoanCommitmentAlternateLender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            MortgageLoanCommitmentAlternateLenderCompanyName.Text = preparer.CompanyName;
            MortgageLoanCommitmentAlternateLenderStreetAddr.Text = preparer.StreetAddr;
            MortgageLoanCommitmentAlternateLenderCity.Text = preparer.City;
            MortgageLoanCommitmentAlternateLenderState.Value = preparer.State;
            MortgageLoanCommitmentAlternateLenderZip.Text = preparer.Zip;

            sCommitReturnToFollowAddr.Checked = dataLoan.sCommitReturnToFollowAddr;
            sCommitReturnToAboveAddr.Checked = dataLoan.sCommitReturnToAboveAddr;
            sCommitReturnWithinDays.Text = dataLoan.sCommitReturnWithinDays_rep;
            sCommitTitleEvidence.Text = dataLoan.sCommitTitleEvidence;
            sCommitRepayTermsDesc.Text = dataLoan.sCommitRepayTermsDesc;

            sLAmtCalc.Text = dataLoan.sLAmtCalc_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sLtvR.Text = dataLoan.sLtvR_rep;
            sTerm.Text = dataLoan.sTerm_rep;
            sTerm.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sDue.Text = dataLoan.sDue_rep;
            sDue.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sCommitExpD.Text = dataLoan.sCommitExpD_rep;
			sCommitExpD.ToolTip = "Hint:  Enter 't' for today's date.";
            sCltvR.Text = dataLoan.sCltvR_rep;

            sMldsHasImpound.Checked = !dataLoan.sMldsHasImpound; // Escrow waived

            sLpTemplateNm.Text = dataLoan.sLpTemplateNm;
            sLDiscnt1003.Text = dataLoan.sLDiscnt1003_rep;
            sRLckdExpiredD.Text = dataLoan.sRLckdExpiredD_rep;
            sProThisMPmt.Text = dataLoan.sProThisMPmt_rep;
            sMaxR.Text = dataLoan.sMaxR_rep;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
            IsAppSpecific = false;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
