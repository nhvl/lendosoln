using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
    public class TXMortgageBrokerDisclosureServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(TXMortgageBrokerDisclosureServiceItem));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("sTexasDiscWillSubmitToLender", dataLoan.sTexasDiscWillSubmitToLender);
            SetResult("sTexasDiscAsIndependentContractor", dataLoan.sTexasDiscAsIndependentContractor);
            SetResult("sTexasDiscWillActAsFollows", dataLoan.sTexasDiscWillActAsFollows);
            SetResult("sTexasDiscWillActAsFollowsDesc", dataLoan.sTexasDiscWillActAsFollowsDesc);
            SetResult("sTexasDiscCompensationIncluded", dataLoan.sTexasDiscCompensationIncluded);
            SetResult("sTexasDiscChargeVaried", dataLoan.sTexasDiscChargeVaried);
            SetResult("sTexasDiscChargeVariedDesc", dataLoan.sTexasDiscChargeVariedDesc);
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.TXMortgageBrokerDisclosure, E_ReturnOptionIfNotExist.CreateNew);
            preparer.LicenseNumOfAgent = GetString("TXMortgageBrokerDisclosureLicenseNumOfAgent");
            preparer.PreparerName = GetString("TXMortgageBrokerDisclosurePreparerName");
            preparer.Update();

            dataLoan.sTexasDiscWillSubmitToLender       = GetBool("sTexasDiscWillSubmitToLender");
            dataLoan.sTexasDiscAsIndependentContractor  = GetBool("sTexasDiscAsIndependentContractor");
            dataLoan.sTexasDiscWillActAsFollows         = GetBool("sTexasDiscWillActAsFollows");
            dataLoan.sTexasDiscWillActAsFollowsDesc     = GetString("sTexasDiscWillActAsFollowsDesc");
            dataLoan.sTexasDiscCompensationIncluded     = GetBool("sTexasDiscCompensationIncluded");
            dataLoan.sTexasDiscChargeVaried             = GetBool("sTexasDiscChargeVaried");
            dataLoan.sTexasDiscChargeVariedDesc         = GetString("sTexasDiscChargeVariedDesc");
        }
    }
	public partial class TXMortgageBrokerDisclosureService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new TXMortgageBrokerDisclosureServiceItem());
        }

	}
}
