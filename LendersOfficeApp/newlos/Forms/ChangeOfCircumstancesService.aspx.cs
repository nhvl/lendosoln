﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Threading;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class ChangeOfCircumstancesService : LendersOffice.Common.BaseSimpleServiceXmlPage 
    {
        protected override void Initialize() 
        {
            AddBackgroundItem("", new ChangeOfCircumstancesServiceItem());
        }
    }

    public class ChangeOfCircumstancesServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "RetrieveGFEFee":
                    RetrieveGFEFee();
                    break;
                case "SaveChangeOfCircumstancesForm":
                    SaveChangeOfCircumstancesForm();
                    break;
                case "SaveAndApplyChangeOfCircumstancesForm":
                    SaveAndApplyChangeOfCircumstancesForm();
                    break;
            }
        }

        private void RetrieveGFEFee()
        {
            string fieldId = GetString("fieldId");
            Guid sLId = GetGuid("sLId");

            // Load up the fee value
            // For a list of valid fees, see OPM 75461
            var pageData = ConstructPageDataClass(sLId);
            pageData.InitLoad();
            string fieldAmount;
            string feeId;

            // custom feeID as well
            // This solution is really ugly. There has to be a better way to do this.
            switch (fieldId)
            {
                case "sLOrigF":
                    fieldAmount = pageData.sLOrigF_rep;
                    feeId = "LQBGFE801a";
                    break;
                case "sGfeOriginatorCompF":
                    fieldAmount = pageData.sGfeOriginatorCompF_rep;
                    feeId = "LQBGFE801b";
                    break;
                case "sLDiscnt":
                    fieldAmount = pageData.sLDiscnt_rep;
                    feeId = "LQBGFE802";
                    break;
                case "sApprF":
                    fieldAmount = pageData.sApprF_rep;
                    feeId = "LQBGFE804";
                    break;
                case "sCrF":
                    fieldAmount = pageData.sCrF_rep;
                    feeId = "LQBGFE805";
                    break;
                case "sTxServF":
                    fieldAmount = pageData.sTxServF_rep;
                    feeId = "LQBGFE806";
                    break;
                case "sFloodCertificationF":
                    fieldAmount = pageData.sFloodCertificationF_rep;
                    feeId = "LQBGFE807";
                    break;
                case "sMBrokF":
                    fieldAmount = pageData.sMBrokF_rep;
                    feeId = "LQBGFE808";
                    break;
                case "sInspectF":
                    fieldAmount = pageData.sInspectF_rep;
                    feeId = "LQBGFE809";
                    break;
                case "sProcF":
                    fieldAmount = pageData.sProcF_rep;
                    feeId = "LQBGFE810";
                    break;
                case "sUwF":
                    fieldAmount = pageData.sUwF_rep;
                    feeId = "LQBGFE811";
                    break;
                case "sWireF":
                    fieldAmount = pageData.sWireF_rep;
                    feeId = "LQBGFE812";
                    break;
                case "s800U1F":
                    fieldAmount = pageData.s800U1F_rep;
                    feeId = "LQBGFE813";
                    break;
                case "s800U2F":
                    fieldAmount = pageData.s800U2F_rep;
                    feeId = "LQBGFE814";
                    break;
                case "s800U3F":
                    fieldAmount = pageData.s800U3F_rep;
                    feeId = "LQBGFE815";
                    break;
                case "s800U4F":
                    fieldAmount = pageData.s800U4F_rep;
                    feeId = "LQBGFE816";
                    break;
                case "s800U5F":
                    fieldAmount = pageData.s800U5F_rep;
                    feeId = "LQBGFE817";
                    break;
                case "sIPia":
                    fieldAmount = pageData.sIPia_rep;
                    feeId = "LQBGFE901";
                    break;
                case "sMipPia":
                    fieldAmount = pageData.sMipPia_rep;
                    feeId = "LQBGFE902";
                    break;
                case "sHazInsPia":
                    fieldAmount = pageData.sHazInsPia_rep;
                    feeId = "LQBGFE903";
                    break;
                case "s904Pia":
                    fieldAmount = pageData.s904Pia_rep;
                    feeId = "LQBGFE904";
                    break;
                case "sVaFf":
                    fieldAmount = pageData.sVaFf_rep;
                    feeId = "LQBGFE905";
                    break;
                case "s900U1Pia":
                    fieldAmount = pageData.s900U1Pia_rep;
                    feeId = "LQBGFE906";
                    break;
                case "sHazInsRsrv":
                    fieldAmount = pageData.sHazInsRsrv_rep;
                    feeId = "LQBGFE1002";
                    break;
                case "sMInsRsrv":
                    fieldAmount = pageData.sMInsRsrv_rep;
                    feeId = "LQBGFE1003";
                    break;
                case "sRealETxRsrv":
                    fieldAmount = pageData.sRealETxRsrv_rep;
                    feeId = "LQBGFE1004";
                    break;
                case "sSchoolTxRsrv":
                    fieldAmount = pageData.sSchoolTxRsrv_rep;
                    feeId = "LQBGFE1005";
                    break;
                case "sFloodInsRsrv":
                    fieldAmount = pageData.sFloodInsRsrv_rep;
                    feeId = "LQBGFE1006";
                    break;
                case "sAggregateAdjRsrv":
                    fieldAmount = pageData.sAggregateAdjRsrv_rep;
                    feeId = "LQBGFE1007";
                    break;
                case "s1006Rsrv":
                    fieldAmount = pageData.s1006Rsrv_rep;
                    feeId = "LQBGFE1008";
                    break;
                case "s1007Rsrv":
                    fieldAmount = pageData.s1007Rsrv_rep;
                    feeId = "LQBGFE1009";
                    break;
                case "sU3Rsrv":
                    fieldAmount = pageData.sU3Rsrv_rep;
                    feeId = "LQBGFE1010";
                    break;
                case "sU4Rsrv":
                    fieldAmount = pageData.sU4Rsrv_rep;
                    feeId = "LQBGFE1011";
                    break;
                case "sEscrowF":
                    fieldAmount = pageData.sEscrowF_rep;
                    feeId = "LQBGFE1102";
                    break;
                case "sOwnerTitleInsF":
                    fieldAmount = pageData.sOwnerTitleInsF_rep;
                    feeId = "LQBGFE1103";
                    break;
                case "sTitleInsF":
                    fieldAmount = pageData.sTitleInsF_rep;
                    feeId = "LQBGFE1104";
                    break;
                case "sDocPrepF":
                    fieldAmount = pageData.sDocPrepF_rep;
                    feeId = "LQBGFE1109";
                    break;
                case "sNotaryF":
                    fieldAmount = pageData.sNotaryF_rep;
                    feeId = "LQBGFE1110";
                    break;
                case "sAttorneyF":
                    fieldAmount = pageData.sAttorneyF_rep;
                    feeId = "LQBGFE1111";
                    break;
                case "sU1Tc":
                    fieldAmount = pageData.sU1Tc_rep;
                    feeId = "LQBGFE1112";
                    break;
                case "sU2Tc":
                    fieldAmount = pageData.sU2Tc_rep;
                    feeId = "LQBGFE1113";
                    break;
                case "sU3Tc":
                    fieldAmount = pageData.sU3Tc_rep;
                    feeId = "LQBGFE1114";
                    break;
                case "sU4Tc":
                    fieldAmount = pageData.sU4Tc_rep;
                    feeId = "LQBGFE1115";
                    break;
                case "sRecF":
                    fieldAmount = pageData.sRecF_rep;
                    feeId = "LQBGFE1201";
                    break;
                case "sCountyRtc":
                    fieldAmount = pageData.sCountyRtc_rep;
                    feeId = "LQBGFE1204";
                    break;
                case "sStateRtc":
                    fieldAmount = pageData.sStateRtc_rep;
                    feeId = "LQBGFE1205";
                    break;
                case "sU1GovRtc":
                    fieldAmount = pageData.sU1GovRtc_rep;
                    feeId = "LQBGFE1206";
                    break;
                case "sU2GovRtc":
                    fieldAmount = pageData.sU2GovRtc_rep;
                    feeId = "LQBGFE1207";
                    break;
                case "sU3GovRtc":
                    fieldAmount = pageData.sU3GovRtc_rep;
                    feeId = "LQBGFE1208";
                    break;
                case "sPestInspectF":
                    fieldAmount = pageData.sPestInspectF_rep;
                    feeId = "LQBGFE1302";
                    break;
                case "sU1Sc":
                    fieldAmount = pageData.sU1Sc_rep;
                    feeId = "LQBGFE1303";
                    break;
                case "sU2Sc":
                    fieldAmount = pageData.sU2Sc_rep;
                    feeId = "LQBGFE1304";
                    break;
                case "sU3Sc":
                    fieldAmount = pageData.sU3Sc_rep;
                    feeId = "LQBGFE1305";
                    break;
                case "sU4Sc":
                    fieldAmount = pageData.sU4Sc_rep;
                    feeId = "LQBGFE1306";
                    break;
                case "sU5Sc":
                    fieldAmount = pageData.sU5Sc_rep;
                    feeId = "LQBGFE1307";
                    break;
                default:
                    fieldAmount = "";
                    feeId = "";
                    break;
            }

            SetResult("feeAmount", fieldAmount); // Set the fee value
            SetResult("feeId", feeId);
        }

        private CPageData GetArchiveCPageData(Guid LoanID)
        {
            return new CPageData(LoanID, CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(ChangeOfCircumstancesServiceItem)).Union(
                CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release));
        }

        private void SaveChangeOfCircumstancesForm()
        {
            Guid sLId = GetGuid("sLId");
            CPageData dataLoan = GetArchiveCPageData(sLId);

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (!dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                return;
            }

            BindData(dataLoan, null);

            dataLoan.Save();
        }
        private void SaveAndApplyChangeOfCircumstancesForm()
        {
            Guid sLId = GetGuid("sLId");
            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);

            CPageData dataLoan = GetArchiveCPageData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (sFileVersion != dataLoan.sFileVersion)
            {
                CBaseException exp = new CBaseException("The loan was modified since the window was opened. Please redo your changes.", "Loan version changed before new CoC was saved.");
                exp.IsEmailDeveloper = false;
                throw exp;
            }

            if (!dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                return;
            }
            if (string.IsNullOrEmpty(GetString("sCircumstanceChangeD")) ||
                string.IsNullOrEmpty(GetString("GfePrepareDate")) ||
                string.IsNullOrEmpty(GetString("sCircumstanceChangeExplanation")) ||
                (!dataLoan.BrokerDB.IsProtectDisclosureDates && string.IsNullOrEmpty(GetString("sGfeRedisclosureD"))))
            {
                throw new CBaseException("Please fill out all required fields.", "User did not fill out all required fields.");
            }

            BindData(dataLoan, null);

            bool updateAllTotalLoanAmount = GetBool("updateAllTotalLoanAmount");
            bool updateAllLoanAmount = GetBool("updateAllLoanAmount");
            bool updateAllPurchPrice = GetBool("updateAllPurchPrice");
            bool updateAllAppraisalFeeChk = GetBool("updateAllAppraisalFeeChk");

            dataLoan.ArchiveCoC(updateAllTotalLoanAmount, updateAllLoanAmount, updateAllPurchPrice, updateAllAppraisalFeeChk); // will save... (dont' need to call it Save afterwards)

            SetResult("sLastDisclosedGFEArchiveD", dataLoan.sLastDisclosedGFEArchiveD_rep);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sCircumstanceChangeD_rep = GetString("sCircumstanceChangeD");
            if(false == dataLoan.BrokerDB.IsProtectDisclosureDates)
            {
                dataLoan.sGfeRedisclosureD_rep = GetString("sGfeRedisclosureD");
            }
            dataLoan.sCircumstanceChangeExplanation = GetString("sCircumstanceChangeExplanation");
            dataLoan.sCircumstanceChange1Amount_rep = GetString("sCircumstanceChange1Amount_rep");
            dataLoan.sCircumstanceChange1Reason = GetString("sCircumstanceChange1Reason");
            dataLoan.sCircumstanceChange1FeeId = GetString("sCircumstanceChange1FeeId");
            dataLoan.sCircumstanceChange2Amount_rep = GetString("sCircumstanceChange2Amount_rep");
            dataLoan.sCircumstanceChange2Reason = GetString("sCircumstanceChange2Reason");
            dataLoan.sCircumstanceChange2FeeId = GetString("sCircumstanceChange2FeeId");
            dataLoan.sCircumstanceChange3Amount_rep = GetString("sCircumstanceChange3Amount_rep");
            dataLoan.sCircumstanceChange3Reason = GetString("sCircumstanceChange3Reason");
            dataLoan.sCircumstanceChange3FeeId = GetString("sCircumstanceChange3FeeId");
            dataLoan.sCircumstanceChange4Amount_rep = GetString("sCircumstanceChange4Amount_rep");
            dataLoan.sCircumstanceChange4Reason = GetString("sCircumstanceChange4Reason");
            dataLoan.sCircumstanceChange4FeeId = GetString("sCircumstanceChange4FeeId");
            dataLoan.sCircumstanceChange5Amount_rep = GetString("sCircumstanceChange5Amount_rep");
            dataLoan.sCircumstanceChange5Reason = GetString("sCircumstanceChange5Reason");
            dataLoan.sCircumstanceChange5FeeId = GetString("sCircumstanceChange5FeeId");
            dataLoan.sCircumstanceChange6Amount_rep = GetString("sCircumstanceChange6Amount_rep");
            dataLoan.sCircumstanceChange6Reason = GetString("sCircumstanceChange6Reason");
            dataLoan.sCircumstanceChange6FeeId = GetString("sCircumstanceChange6FeeId");
            dataLoan.sCircumstanceChange7Amount_rep = GetString("sCircumstanceChange7Amount_rep");
            dataLoan.sCircumstanceChange7Reason = GetString("sCircumstanceChange7Reason");
            dataLoan.sCircumstanceChange7FeeId = GetString("sCircumstanceChange7FeeId");
            dataLoan.sCircumstanceChange8Amount_rep = GetString("sCircumstanceChange8Amount_rep");
            dataLoan.sCircumstanceChange8Reason = GetString("sCircumstanceChange8Reason");
            dataLoan.sCircumstanceChange8FeeId = GetString("sCircumstanceChange8FeeId");

            IPreparerFields gfe = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            gfe.PrepareDate_rep = GetString("GfePrepareDate");
            gfe.Update();
        }

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, this.GetType());
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }
}
