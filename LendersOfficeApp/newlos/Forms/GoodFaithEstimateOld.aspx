<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="GoodFaithEstimateOld.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.GoodFaithEstimateOld" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="uc1" TagName="GoodFaithEstimateRightColumn" Src="../../los/LegalForm/GoodFaithEstimateRightColumn.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
		<title>GoodFaithEstimate</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
	<body class="RightBackground" MS_POSITIONING="FlowLayout">
		<script language="javascript">
<!--
  var bIsFirst = true;
  
  function findCCTemplate() {
    // Before choosing a closing cost template, you always need to save the loan.
    PolyShouldShowConfirmSave(isDirty(), function(){
		showModal('/los/view/closingcostlist.aspx?loanid=' + ML.sLId, null, null, null, function(args){ 
			if (args.OK) {
				self.location = self.location; // Refresh;
			}
		});
	}, saveMe);
  }
  function _init() {
    lockField(document.getElementById("<%= AspxTools.ClientId(sTotCcPbsLocked) %>"), 'sTotCcPbs');
    lockField(document.getElementById("<%= AspxTools.ClientId(sRefPdOffAmtGfeLckd) %>"), 'sRefPdOffAmtGfe');
    bIsAutoCalculate = document.forms[0].<%= AspxTools.ClientId(ByPassBgCalcForGfeAsDefault) %>[0].checked;
    document.getElementById("btnCalculate").disabled = bIsAutoCalculate;
  
    if (bIsFirst) {
      // Only init one time.
      bIsFirst = false;
      var length = document.forms[0].<%= AspxTools.ClientId(ByPassBgCalcForGfeAsDefault) %>.length;
            for (var i = 0; i < length; i++)
            addEventHandler(document.forms[0].<%= AspxTools.ClientId(ByPassBgCalcForGfeAsDefault) %>[i], "click", backgroundCalculation, false);
    
      <%= AspxTools.JsGetElementById(GfeTilCompanyName) %>.focus();
    }
  }


var openedDate = <%=AspxTools.JsString(m_openedDate)%>;
function onDateKeyUp(o, event) {
  if (event.keyCode == 79) {
    o.value = openedDate;
    updateDirtyBit(event);
  }
}
//-->
		</script>
		<form id="GoodFaithEstimate" method="post" runat="server">
			<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="MainRightHeader" noWrap>Good Faith Estimate</TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE id="Table3" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FieldLabel" noWrap colSpan="6"><asp:RadioButtonList id=ByPassBgCalcForGfeAsDefault runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
<asp:ListItem Value="0">Perform calculation automatically</asp:ListItem>
<asp:ListItem Value="1">Perform calculation manually</asp:ListItem>
</asp:RadioButtonList> 
									 &nbsp; <INPUT disabled accessKey="c" onclick="backgroundCalculation();" tabIndex="-1" type="button" value="Recalculate  (Alt +C)" id="btnCalculate" name="btnCalculate"></TD>
							</TR>
        <tr>
          <td class=FieldLabel nowrap colspan=6></td></tr>
							<tr>
								<td class="FieldLabel" nowrap>Prepared By</td>
								<td class="FieldLabel" nowrap colspan="5"><asp:TextBox id="GfeTilCompanyName" runat="server" Width="258px" SkipMe="true"></asp:TextBox></td>
							</tr>
							<tr>
								<td class="FieldLabel" nowrap>Address</td>
								<td class="FieldLabel" nowrap colspan="5"><asp:TextBox id="GfeTilStreetAddr" runat="server" Width="254px" SkipMe="true"></asp:TextBox></td>
							</tr>
							<tr>
								<td class="FieldLabel" nowrap></td>
								<td class="FieldLabel" nowrap colspan="5"><asp:TextBox id="GfeTilCity" runat="server" SkipMe="true"></asp:TextBox><ml:StateDropDownList id="GfeTilState" runat="server" SkipMe="true"></ml:StateDropDownList><ml:ZipcodeTextBox id="GfeTilZip" runat="server" width="50" preset="zipcode" SkipMe="true"></ml:ZipcodeTextBox></td>
							</tr>
							<tr>
								<td class="FieldLabel" nowrap>Phone</td>
								<td class="FieldLabel" nowrap colspan="5"><ml:PhoneTextBox id="GfeTilPhoneOfCompany" runat="server" width="120" preset="phone" SkipMe="true"></ml:PhoneTextBox></td>
							</tr>
							<TR>
								<TD class="FieldLabel" noWrap>Prepared Date</TD>
								<TD class="FieldLabel" noWrap colSpan="5"><ml:datetextbox id="GfeTilPrepareDate" onkeyup="onDateKeyUp(this, event);" runat="server" CssClass="mask" preset="date" width="75" SkipMe="true"></ml:datetextbox>Shortcut: 
									Enter 't' for today date. Enter 'o' for opened date.</TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>Total Loan Amt</TD>
								<TD noWrap><ml:moneytextbox id="sFinalLAmt" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
								<TD class="FieldLabel" noWrap>Interest Rate</TD>
								<TD noWrap><ml:percenttextbox id="sNoteIR" runat="server" preset="percent" width="70" onchange="refreshCalculation();"></ml:percenttextbox></TD>
								<TD class="FieldLabel" noWrap>1st Payment Date</TD>
								<TD noWrap><ml:datetextbox id="sSchedDueD1" runat="server" CssClass="mask" preset="date" width="75"></ml:datetextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>Days in Year</TD>
								<TD noWrap><asp:textbox id="sDaysInYr" runat="server" onchange="refreshCalculation();" MaxLength="4" Width="54px"></asp:textbox></TD>
								<TD class="FieldLabel" noWrap>Term/Due In</TD>
								<TD noWrap><asp:textbox id="sTerm" runat="server" Width="42px"></asp:textbox>/<asp:textbox id="sDue" runat="server" Width="51px"></asp:textbox>&nbsp;mths</TD>
								<TD class="FieldLabel" noWrap>Estimate Closed Date</TD>
								<TD noWrap><ml:datetextbox id="sEstCloseD" runat="server" CssClass="mask" preset="date" width="75"></ml:datetextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" style="HEIGHT: 18px" noWrap>Loan Program</TD>
								<TD noWrap colSpan="5"><asp:textbox id="sLpTemplateNm" runat="server" SkipMe="true" Width="432px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>CC Template</TD>
								<TD noWrap colSpan="5"><asp:textbox id="sCcTemplateNm" runat="server" SkipMe="true" Width="432px"></asp:textbox><INPUT onclick="findCCTemplate();" type="button" value="Find CC Template..."></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap colSpan="6"><asp:checkbox id="CheckBox1" runat="server" Width="174px" Text="Lender/Exclusive Agent" enabled="False"></asp:checkbox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD noWrap></TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FormTableHeader"></TD>
								<TD class="FormTableHeader"></TD>
								<TD class="FormTableHeader">B&nbsp;= Paid to broker&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A 
									= APR</TD>
								<TD class="FormTableHeader"></TD>
								<TD class="FormTableHeader"></TD>
								<TD class="FormTableHeader" style="WIDTH: 44px"></TD>
								<TD class="FormTableHeader"></TD>
							</TR>
							<TR>
								<TD class="FormTableHeader"></TD>
								<TD class="FormTableHeader">Description of Charge</TD>
								<TD class="FormTableHeader"></TD>
								<TD class="FormTableHeader">Amount</TD>
								<TD class="FormTableHeader">Paid By</TD>
								<TD class="FormTableHeader" style="WIDTH: 44px"></TD>
								<TD class="FormTableHeader"></TD>
							</TR>
							<TR>
								<TD class="FormTableSubheader" colSpan="8">800 ITEMS PAYABLE IN CONNECTION WITH 
									LOAN
								</TD>
							</TR>
							<TR>
								<TD class="FieldLabel">801</TD>
								<TD class="FieldLabel">Loan origination fee</TD>
								<TD><ml:percenttextbox id="sLOrigFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();"></ml:percenttextbox>+
									<ml:moneytextbox id="sLOrigFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="sLOrigF" runat="server" preset="money" width="77" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sLOrigFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">802</TD>
								<TD class="FieldLabel">Loan discount</TD>
								<TD noWrap><ml:percenttextbox id="sLDiscntPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();"></ml:percenttextbox>+
									<ml:moneytextbox id="sLDiscntFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="sLDiscnt" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sLDiscntProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">803</TD>
								<TD class="FieldLabel">Appraisal fee</TD>
								<TD align="right"><asp:checkbox id="sApprFPaid" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" onclick="refreshCalculation();" runat="server" Width="49px" Text="Paid"></asp:checkbox>&nbsp;</TD>
								<TD><ml:moneytextbox id="sApprF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sApprFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">804</TD>
								<TD class="FieldLabel">Credit report</TD>
								<TD align="right"><asp:checkbox id="sCrFPaid" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" onclick="refreshCalculation();" runat="server" Width="49px" Text="Paid"></asp:checkbox>&nbsp;</TD>
								<TD><ml:moneytextbox id="sCrF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sCrFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">805</TD>
								<TD class="FieldLabel">Lender's inspection fee</TD>
								<TD></TD>
								<TD><ml:moneytextbox id="sInspectF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sInspectFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">808</TD>
								<TD class="FieldLabel">Mortgage broker fee</TD>
								<TD><ml:percenttextbox id="sMBrokFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();"></ml:percenttextbox>+
									<ml:moneytextbox id="sMBrokFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="sMBrokF" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sMBrokFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">809</TD>
								<TD class="FieldLabel">Tax service fee</TD>
								<TD></TD>
								<TD><ml:moneytextbox id="sTxServF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sTxServFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">810</TD>
								<TD class="FieldLabel">Processing fee</TD>
								<TD align="right"><asp:checkbox id="sProcFPaid" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" onclick="refreshCalculation();" runat="server" Width="49px" Text="Paid"></asp:checkbox>&nbsp;</TD>
								<TD><ml:moneytextbox id="sProcF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sProcFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">811</TD>
								<TD class="FieldLabel">Underwriting fee</TD>
								<TD></TD>
								<TD><ml:moneytextbox id="sUwF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sUwFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">812</TD>
								<TD class="FieldLabel">Wire transfer</TD>
								<TD></TD>
								<TD><ml:moneytextbox id="sWireF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sWireFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="s800U1FCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="31"></asp:textbox></TD>
								<TD colSpan="2"><asp:textbox id="s800U1FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="100" Width="95%"></asp:textbox></TD>
								<TD><ml:moneytextbox id="s800U1F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s800U1FProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="s800U2FCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="31px"></asp:textbox></TD>
								<TD colSpan="2"><asp:textbox id="s800U2FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="100" Width="95%"></asp:textbox></TD>
								<TD><ml:moneytextbox id="s800U2F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s800U2FProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="s800U3FCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="31px"></asp:textbox></TD>
								<TD colSpan="2"><asp:textbox id="s800U3FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="100" Width="95%"></asp:textbox></TD>
								<TD><ml:moneytextbox id="s800U3F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s800U3FProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="s800U4FCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="31px"></asp:textbox></TD>
								<TD colSpan="2"><asp:textbox id="s800U4FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="100" Width="95%"></asp:textbox></TD>
								<TD><ml:moneytextbox id="s800U4F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s800U4FProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="s800U5FCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="32"></asp:textbox></TD>
								<TD colSpan="2"><asp:textbox id="s800U5FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="100" Width="95%"></asp:textbox></TD>
								<TD><ml:moneytextbox id="s800U5F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s800U5FProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FormTableSubheader" colSpan="8"><A name="900"></A>900 ITEMS REQUIRED BY 
									LENDER TO BE PAID IN ADVANCE</TD>
							</TR>
							<TR>
								<TD class="FieldLabel">901</TD>
								<TD class="FieldLabel">Interest for</TD>
								<TD noWrap class="FieldLabel"><asp:textbox id="sIPiaDy" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();" MaxLength="4" Width="40px"></asp:textbox>&nbsp;days 
									@
									<ml:moneytextbox id="sIPerDay" runat="server" preset="money" decimalDigits="6" ReadOnly="True"></ml:moneytextbox>per 
									day</TD>
								<TD><ml:moneytextbox id="sIPia" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sIPiaProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">902</TD>
								<TD class="FieldLabel" noWrap colSpan="2" rowSpan="1"><a href="javascript:linkMe('../LoanInfo.aspx?pg=1');">Mortgage 
										Insurance Premium</a></TD>
								<TD><ml:moneytextbox id="sMipPia" runat="server" preset="money" width="76px" readonly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sMipPiaProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">903</TD>
								<TD class="FieldLabel" noWrap colSpan="2">Haz Ins. @&nbsp;
									<ml:percenttextbox id="sProHazInsR" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();" DESIGNTIMEDRAGDROP="236"></ml:percenttextbox>of<asp:dropdownlist id="sProHazInsT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();">
										<asp:ListItem Value="0">Loan Amount</asp:ListItem>
										<asp:ListItem Value="1">Purchase Price</asp:ListItem>
										<asp:ListItem Value="2" Selected="True">Appraisal Value</asp:ListItem>
									</asp:dropdownlist>+
									<ml:moneytextbox id="sProHazInsMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="60px" onchange="refreshCalculation();" decimalDigits="4"></ml:moneytextbox>for
									<asp:textbox id="sHazInsPiaMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();" MaxLength="4" Width="30px"></asp:textbox>mths</TD>
								<TD><ml:moneytextbox id="sHazInsPia" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sHazInsPiaProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">904</TD>
								<TD colSpan="2"><asp:textbox id="s904PiaDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="100" Width="95%" Height="20px"></asp:textbox></TD>
								<TD><ml:moneytextbox id="s904Pia" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s904PiaProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">905</TD>
								<TD class="FieldLabel"><a href="javascript:linkMe('../LoanInfo.aspx?pg=1');">VA funding 
										fee</a></TD>
								<TD></TD>
								<TD><ml:moneytextbox id="sVaFf" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sVaFfProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="s900U1PiaCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="33"></asp:textbox></TD>
								<TD colSpan="2"><asp:textbox id="s900U1PiaDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="100" Width="95%"></asp:textbox></TD>
								<TD><ml:moneytextbox id="s900U1Pia" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s900U1PiaProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FormTableSubheader" colSpan="8"><A name="1000"></A>1000 RESERVES 
									DEPOSITED WITH LENDER</TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1001</TD>
								<TD class="FieldLabel">Haz ins. reserve</TD>
								<TD class="FieldLabel"><asp:textbox id="sHazInsRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();" MaxLength="4" Width="41"></asp:textbox>&nbsp;mths 
									@
									<ml:moneytextbox id="sProHazIns" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox>/ 
									month</TD>
								<TD><ml:moneytextbox id="sHazInsRsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sHazInsRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1002</TD>
								<TD class="FieldLabel" colSpan="2">Mtg ins. 
									reserve&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; @
									<ml:percenttextbox id="sProMInsR" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();"></ml:percenttextbox>&nbsp;of
									<asp:dropdownlist id="sProMInsT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();">
										<asp:ListItem Value="0">Loan Amount</asp:ListItem>
										<asp:ListItem Value="1">Purchase Price</asp:ListItem>
										<asp:ListItem Value="2">Appraisal Value</asp:ListItem>
									</asp:dropdownlist>+
									<ml:moneytextbox id="sProMInsMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="60px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"></TD>
							</TR>
							<TR>
								<TD class="FieldLabel"></TD>
								<TD class="FieldLabel" colSpan="2">= 
									<ml:moneytextbox id="sProMIns" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox>&nbsp;&nbsp;for&nbsp;
									<asp:textbox id="sMInsRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();" MaxLength="4" Width="41px"></asp:textbox>
									mths&nbsp;</TD>
								<TD colSpan="1"><ml:moneytextbox id="sMInsRsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sMInsRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1003</TD>
								<TD class="FieldLabel">School taxes</TD>
								<TD class="FieldLabel"><asp:textbox id="sSchoolTxRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();" MaxLength="4" Width="41px"></asp:textbox>&nbsp;mths 
									@
									<ml:moneytextbox id="sProSchoolTx" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>/ 
									month</TD>
								<TD><ml:moneytextbox id="sSchoolTxRsrv" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sSchoolTxRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1004</TD>
								<TD class="FieldLabel" colSpan="2">Tax resrv @
									<ml:percenttextbox id="sProRealETxR" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();"></ml:percenttextbox>&nbsp;of<asp:dropdownlist id="sProRealETxT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();">
										<asp:ListItem Value="0">Loan Amount</asp:ListItem>
										<asp:ListItem Value="1">Purchase Price</asp:ListItem>
										<asp:ListItem Value="2">Appraisal Value</asp:ListItem>
									</asp:dropdownlist>
									+
									<ml:moneytextbox id="sProRealETxMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="59px" onchange="refreshCalculation();"></ml:moneytextbox>for<asp:textbox id="sRealETxRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();" MaxLength="4" Width="31px"></asp:textbox>mths</TD>
								<TD><ml:moneytextbox id="sRealETxRsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sRealETxRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1005</TD>
								<TD class="FieldLabel">Flood ins. reserve</TD>
								<TD class="FieldLabel"><asp:textbox id="sFloodInsRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();" MaxLength="4" Width="41px"></asp:textbox>&nbsp;mths 
									@
									<ml:moneytextbox id="sProFloodIns" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>/ 
									month</TD>
								<TD><ml:moneytextbox id="sFloodInsRsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sFloodInsRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1006</TD>
								<TD><asp:textbox id="s1006ProHExpDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="135" Height="20px"></asp:textbox></TD>
								<TD class="FieldLabel"><asp:textbox id="s1006RsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();" MaxLength="4" Width="41px"></asp:textbox>&nbsp;mths 
									@
									<ml:moneytextbox id="s1006ProHExp" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>/ 
									month</TD>
								<TD><ml:moneytextbox id="s1006Rsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s1006RsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1007</TD>
								<TD><asp:textbox id="s1007ProHExpDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="136" Height="20px"></asp:textbox></TD>
								<TD class="FieldLabel"><asp:textbox id="s1007RsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();" MaxLength="4" Width="41px"></asp:textbox>&nbsp;mths 
									@
									<ml:moneytextbox id="s1007ProHExp" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>/ 
									month</TD>
								<TD><ml:moneytextbox id="s1007Rsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="s1007RsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1008</TD>
								<TD class="FieldLabel">Aggregate adjustment</TD>
								<TD></TD>
								<TD><ml:moneytextbox id="sAggregateAdjRsrv" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sAggregateAdjRsrvProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FormTableSubheader" colSpan="8">1100 TITLE CHARGES</TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1101</TD>
								<TD class="FieldLabel">Closing/Escrow Fee</TD>
								<TD><asp:textbox id="sEscrowFTable" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" MaxLength="21" Width="212px"></asp:textbox></TD>
								<TD><ml:moneytextbox id="sEscrowF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sEscrowFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1105</TD>
								<TD class="FieldLabel">Doc preparation fee</TD>
								<TD></TD>
								<TD><ml:moneytextbox id="sDocPrepF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sDocPrepFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1106</TD>
								<TD class="FieldLabel">Notary fees</TD>
								<TD></TD>
								<TD><ml:moneytextbox id="sNotaryF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sNotaryFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1107</TD>
								<TD class="FieldLabel">Attorney fees</TD>
								<TD></TD>
								<TD><ml:moneytextbox id="sAttorneyF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sAttorneyFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1108</TD>
								<TD class="FieldLabel">Title Insurance</TD>
								<TD><asp:textbox id="sTitleInsFTable" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" MaxLength="36" Width="212px"></asp:textbox></TD>
								<TD><ml:moneytextbox id="sTitleInsF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sTitleInsFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="sU1TcCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="33px"></asp:textbox></TD>
								<TD colSpan="2"><asp:textbox id="sU1TcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="100" Width="95%"></asp:textbox></TD>
								<TD><ml:moneytextbox id="sU1Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU1TcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="sU2TcCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="33px"></asp:textbox></TD>
								<TD colSpan="2"><asp:textbox id="sU2TcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="100" Width="95%"></asp:textbox></TD>
								<TD><ml:moneytextbox id="sU2Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU2TcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="sU3TcCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="33px"></asp:textbox></TD>
								<TD colSpan="2"><asp:textbox id="sU3TcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="100" Width="95%"></asp:textbox></TD>
								<TD><ml:moneytextbox id="sU3Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU3TcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="sU4TcCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="33px"></asp:textbox></TD>
								<TD colSpan="2"><asp:textbox id="sU4TcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="100" Width="95%"></asp:textbox></TD>
								<TD><ml:moneytextbox id="sU4Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU4TcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FormTableSubheader" noWrap colSpan="8">1200 GOVERNMENT RECORDING &amp; 
									TRANSFER CHARGES</TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1201</TD>
								<TD class="FieldLabel" noWrap>Recording fees<asp:textbox id="sRecFDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="76px"></asp:textbox></TD>
								<TD noWrap class="FieldLabel"><ml:percenttextbox id="sRecFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51" onchange="refreshCalculation();"></ml:percenttextbox>of
									<asp:dropdownlist id="sRecBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();">
										<asp:ListItem Value="0">Loan Amount</asp:ListItem>
										<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
										<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
									</asp:dropdownlist>+<ml:moneytextbox id="sRecFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="sRecF" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sRecFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1202</TD>
								<TD class="FieldLabel" noWrap>City tax/stamps<asp:textbox id="sCountyRtcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="72px"></asp:textbox></TD>
								<TD noWrap class="FieldLabel"><ml:percenttextbox id="sCountyRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>of
									<asp:dropdownlist id="sCountyRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();">
										<asp:ListItem Value="0">Loan Amount</asp:ListItem>
										<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
										<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
									</asp:dropdownlist>+<ml:moneytextbox id="sCountyRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="sCountyRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sCountyRtcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1203</TD>
								<TD class="FieldLabel" noWrap>State tax/stamps<asp:textbox id="sStateRtcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();" Width="68px"></asp:textbox></TD>
								<TD noWrap class="FieldLabel"><ml:percenttextbox id="sStateRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>of
									<asp:dropdownlist id="sStateRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();">
										<asp:ListItem Value="0">Loan Amount</asp:ListItem>
										<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
										<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
									</asp:dropdownlist>+<ml:moneytextbox id="sStateRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="sStateRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sStateRtcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="sU1GovRtcCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="33px"></asp:textbox></TD>
								<TD><asp:textbox id="sU1GovRtcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="104"></asp:textbox></TD>
								<TD noWrap class="FieldLabel"><ml:percenttextbox id="sU1GovRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>of
									<asp:dropdownlist id="sU1GovRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();">
										<asp:ListItem Value="0">Loan Amount</asp:ListItem>
										<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
										<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
									</asp:dropdownlist>+<ml:moneytextbox id="sU1GovRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="sU1GovRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU1GovRtcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="sU2GovRtcCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="33px"></asp:textbox></TD>
								<TD><asp:textbox id="sU2GovRtcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="104px" Height="20px"></asp:textbox></TD>
								<TD noWrap class="FieldLabel"><ml:percenttextbox id="sU2GovRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>of
									<asp:dropdownlist id="sU2GovRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();">
										<asp:ListItem Value="0">Loan Amount</asp:ListItem>
										<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
										<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
									</asp:dropdownlist>+<ml:moneytextbox id="sU2GovRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="sU2GovRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU2GovRtcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="sU3GovRtcCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="33px"></asp:textbox></TD>
								<TD><asp:textbox id="sU3GovRtcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="104px" Height="20px"></asp:textbox></TD>
								<TD noWrap class="FieldLabel"><ml:percenttextbox id="sU3GovRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>of
									<asp:dropdownlist id="sU3GovRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();">
										<asp:ListItem Value="0">Loan Amount</asp:ListItem>
										<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
										<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
									</asp:dropdownlist>+<ml:moneytextbox id="sU3GovRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD><ml:moneytextbox id="sU3GovRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU3GovRtcProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FormTableSubheader" colSpan="8">1300 ADDITIONAL SETTLEMENT CHARGES</TD>
							</TR>
							<TR>
								<TD class="FieldLabel">1302</TD>
								<TD class="FieldLabel">Pest Inspection</TD>
								<TD></TD>
								<TD><ml:moneytextbox id="sPestInspectF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sPestInspectFProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="sU1ScCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="33px"></asp:textbox></TD>
								<TD colSpan="2"><asp:textbox id="sU1ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="100" Width="95%"></asp:textbox></TD>
								<TD><ml:moneytextbox id="sU1Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU1ScProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="sU2ScCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="33px"></asp:textbox></TD>
								<TD colSpan="2"><asp:textbox id="sU2ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="100" Width="95%" Height="20px"></asp:textbox></TD>
								<TD><ml:moneytextbox id="sU2Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU2ScProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="sU3ScCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="33px"></asp:textbox></TD>
								<TD colSpan="2"><asp:textbox id="sU3ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="100" Width="95%"></asp:textbox></TD>
								<TD><ml:moneytextbox id="sU3Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU3ScProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="sU4ScCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="33px"></asp:textbox></TD>
								<TD colSpan="2"><asp:textbox id="sU4ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="100" Width="95%"></asp:textbox></TD>
								<TD><ml:moneytextbox id="sU4Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU4ScProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD><asp:textbox id="sU5ScCode" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="21" Width="33px"></asp:textbox></TD>
								<TD colSpan="2"><asp:textbox id="sU5ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" SkipMe="true" MaxLength="100" Width="95%"></asp:textbox></TD>
								<TD><ml:moneytextbox id="sU5Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"><uc1:goodfaithestimaterightcolumn id="sU5ScProps_ctrl" runat="server"></uc1:goodfaithestimaterightcolumn></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" colSpan="3">TOTAL ESTIMATED SETTLEMENT CHARGES</TD>
								<TD><ml:moneytextbox id="sTotEstSc" tabIndex="201" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"></TD>
							</TR>
							<TR>
								<TD colSpan="3">&nbsp;</TD>
								<TD></TD>
								<TD noWrap colSpan="3"></TD>
							</TR>
							<TR>
								<TD class="FormTableSubheader" colSpan="7">COMPENSATION TO BROKER (Not Paid Out of 
									Loan Proceeds)</TD>
							</TR>
							<TR>
								<TD colSpan="3"><asp:textbox id="sBrokComp1Desc" tabIndex="201" runat="server" SkipMe="true" MaxLength="100" Width="95%"></asp:textbox></TD>
								<TD><ml:moneytextbox id="sBrokComp1" tabIndex="201" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"></TD>
							</TR>
							<TR>
								<TD colSpan="3"><asp:textbox id="sBrokComp2Desc" tabIndex="201" runat="server" SkipMe="true" MaxLength="100" Width="95%"></asp:textbox></TD>
								<TD><ml:moneytextbox id="sBrokComp2" tabIndex="201" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap colSpan="3"></TD>
							</TR>
							<TR>
								<TD colSpan="3"></TD>
								<TD></TD>
								<TD noWrap colSpan="3"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE class="InsetBorder" id="Table9" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FormTableSubHeader" colSpan="2">&nbsp;Total Estimated Funds Need To 
									Closed</TD>
							</TR>
        <tr>
          <td colspan=2 class="FieldLabel">
            <asp:checkbox id=sUseObsoleteGfeForm runat="server" text="Use old (obsolete) GFE when printing this loan."></asp:checkbox>
            &nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:linkMe('GoodFaithEstimate.aspx');">Go to new GFE</a>
          </td></tr>							
							<TR>
								<TD colSpan="2">
									<TABLE id="Table10" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD class="FieldLabel">Purchase Price (leave it blank if refinancing)</TD>
											<TD style="WIDTH: 52px" align="right">+</TD>
											<TD><ml:moneytextbox id="sPurchPrice" tabIndex="201" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">Pay Off Amount (<A tabIndex="201" onclick="redirectToUladPage('Liabilities');">liabilities 
													to be paid off with new loan</A>)</TD>
											<TD style="WIDTH: 52px" align="right">&nbsp; +</TD>
											<TD><ml:moneytextbox id="sRefPdOffAmtGfe" tabIndex="201" runat="server" name="sRefPdOffAmtGfe" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox><asp:CheckBox id=sRefPdOffAmtGfeLckd runat="server" Text="Lock" onclick="lockField(this, 'sRefPdOffAmtGfe'); refreshCalculation();"></asp:CheckBox></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>Total Loan Amount (including&nbsp;financed settlement 
												charges&nbsp;of&nbsp;
												<ml:moneytextbox id="sFCc" tabIndex="201" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox>)</TD>
											<TD style="WIDTH: 52px" align="right">-</TD>
											<TD><ml:moneytextbox id="sFinalLAmt2" tabIndex="201" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">Estimated Closing Costs</TD>
											<TD style="WIDTH: 52px" align="right">+</TD>
											<TD><ml:moneytextbox id="sTotEstCc" tabIndex="201" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">Estimated Prepaid Items/Reserves</TD>
											<TD style="WIDTH: 52px" align="right">+</TD>
											<TD><ml:moneytextbox id="sTotEstPp" tabIndex="201" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">Settlement&nbsp;Charges&nbsp;Paid by Seller</TD>
											<TD style="WIDTH: 52px" align="right">-</TD>
											<TD><ml:moneytextbox id="sTotCcPbs" tabIndex="201" runat="server" preset="money" width="90" ReadOnly="True" onchange="refreshCalculation();"></ml:moneytextbox><asp:CheckBox id=sTotCcPbsLocked runat="server" Text="Lock" onclick="lockField(this, 'sTotCcPbs'); refreshCalculation();"></asp:CheckBox></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">Settlement Charges&nbsp;Paid by Others</TD>
											<TD style="WIDTH: 52px" align="right">-</TD>
											<TD><ml:moneytextbox id="sTotCcPbo" tabIndex="201" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">Credit Life and/or Disability Insurance</TD>
											<TD style="WIDTH: 52px" align="right">+</TD>
											<TD><ml:moneytextbox id="sDisabilityIns" tabIndex="201" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="sU1FntcDesc" tabIndex="201" runat="server" MaxLength="50" Width="427px" Height="20px"></asp:textbox></TD>
											<TD style="WIDTH: 52px" align="right">+</TD>
											<TD><ml:moneytextbox id="sU1Fntc" tabIndex="201" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" colSpan="3"></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">Total Estimated Funds to Close&nbsp; (Net Cash from 
												Borrower)</TD>
											<TD style="WIDTH: 52px" align="right">=</TD>
											<TD><ml:moneytextbox id="sTotEstFntc" tabIndex="201" runat="server" preset="money" width="90" ReadOnly="True" DESIGNTIMEDRAGDROP="1429"></ml:moneytextbox></TD>
											<TD></TD>
										</TR>
										<%-- Start FHA section --%>
										<% if (m_isFHALoan) { %>
										<TR>
											<TD class="FieldLabel"></TD>
											<TD style="WIDTH: 52px" align="right"></TD>
											<TD></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD class="FormTableSubHeader" colspan="3">ADDITIONAL FHA/VA RELATED COSTS</TD>
										</TR>
										<TR>
											<TD class="FieldLabel">Upfront Mortgage Insurance Premium / Funding&nbsp;Fee</TD>
											<TD style="WIDTH: 52px" align="right">+</TD>
											<TD><ml:moneytextbox id="sFfUfmip1003" tabIndex="201" runat="server" preset="money" width="90" ReadOnly="True" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">Required Cash Investment (optional)</TD>
											<TD style="WIDTH: 52px" align="right">+</TD>
											<TD><ml:moneytextbox id="sFHAReqCashInv" tabIndex="201" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">Mortgage Insurance Premium Refund / Sale Concessions</TD>
											<TD style="WIDTH: 52px" align="right">-</TD>
											<TD><ml:moneytextbox id="sFHASalesConcessions" tabIndex="201" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">203k Rehabilitation Cost</TD>
											<TD style="WIDTH: 52px" align="right">+</TD>
											<TD><ml:moneytextbox id="sFHA203kRehabCost" tabIndex="201" runat="server" preset="money" width="90" ReadOnly="True" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">Energy Efficient Mortgage Improvements</TD>
											<TD style="WIDTH: 52px" align="right">+</TD>
											<TD><ml:moneytextbox id="sFHAEnergyEffImprov" tabIndex="201" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD></TD>
											<TD style="WIDTH: 52px" align="right"></TD>
											<TD></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD class="FieldLabel">FHA Total Estimated Funds Needed To Close</TD>
											<TD style="WIDTH: 52px" align="right">=</TD>
											<TD><ml:moneytextbox id="sFHATotEstFntc" tabIndex="201" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
											<TD></TD>
										</TR>
										<% } // if (m_isFHALoan) %>
										<%-- End FHA SECTION --%>
										<TR>
											<TD></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="FieldLabel" noWrap><asp:checkbox id="sGfeProvByBrok" tabIndex="201" runat="server" Width="393px" Text="This Good Faith Estimate is being provided by broker" Height="15px"></asp:checkbox></TD>
				</TR>
			</TABLE>
			<uc1:cmodaldlg id="CModalDlg1" runat="server" DESIGNTIMEDRAGDROP="5482"></uc1:cmodaldlg></form>
	</body>
</html>
