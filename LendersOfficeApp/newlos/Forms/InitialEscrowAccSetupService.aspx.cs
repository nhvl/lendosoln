using System;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos.Forms
{
    public class InitialEscrowAccSetupServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(InitialEscrowAccSetupServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            int[,] table = new int[13, 9];
            for (int i = 0; i < 13; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    table[i, j] = GetInt("_" + i + "_" + j, 0);
                }
            }

            dataLoan.sInitialEscrowAcc = table;
            dataLoan.sSchedDueD1Lckd = GetBool("sSchedDueD1Lckd");
            dataLoan.sSchedDueD1_rep = GetString("sSchedDueD1");

            if (GetString("UpdateGFEReserves", "False") == "True")
            {
                dataLoan.PopulateGfeReserveWithRecommendValue();
            }

        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sRealETxRsrvMonRecommended", dataLoan.sRealETxRsrvMonRecommended_rep);
            SetResult("sHazInsRsrvMonRecommended", dataLoan.sHazInsRsrvMonRecommended_rep);
            SetResult("sMInsRsrvMonRecommended", dataLoan.sMInsRsrvMonRecommended_rep);
            SetResult("sFloodInsRsrvMonRecommended", dataLoan.sFloodInsRsrvMonRecommended_rep);
            SetResult("sSchoolTxRsrvMonRecommended", dataLoan.sSchoolTxRsrvMonRecommended_rep);
            SetResult("s1006RsrvMonRecommended", dataLoan.s1006RsrvMonRecommended_rep);
            SetResult("s1007RsrvMonRecommended", dataLoan.s1007RsrvMonRecommended_rep);
            SetResult("sU3RsrvMonRecommended", dataLoan.sU3RsrvMonRecommended_rep);
            SetResult("sU4RsrvMonRecommended", dataLoan.sU4RsrvMonRecommended_rep);
            SetResult("sSchedDueD1Lckd", dataLoan.sSchedDueD1Lckd);
            SetResult("sSchedDueD1", dataLoan.sSchedDueD1_rep);
        }
    }

	public partial class InitialEscrowAccSetupService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize()
        {
            AddBackgroundItem("", new InitialEscrowAccSetupServiceItem());
        }
        //protected override void Process(string methodName) 
        //{
        //    switch (methodName) 
        //    {
        //        case "SaveData":
        //            SaveData();
        //            break;
        //        case "CalculateData":
        //            CalculateData();
        //            break;
        //    }
        //}

        //private void SaveData() 
        //{
        //    int[,] table = new int[13,7];
        //    Guid loanId = GetGuid("LoanID");
        //    int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
        //    CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(InitialEscrowAccSetupService));
        //    dataLoan.InitSave(sFileVersion);

        //    for (int i = 0; i < 13; i++) 
        //    {
        //        for (int j = 0; j < 7; j++) 
        //        {
        //            table[i,j] = GetInt("_" + i + "_" + j, 0);
        //        }
        //    }

        //    dataLoan.sInitialEscrowAcc = table;
        //    dataLoan.sSchedDueD1_rep = GetString("sSchedDueD1");

        //    if (GetString("UpdateGFEReserves") == "True")
        //    {
        //        dataLoan.PopulateGfeReserveWithRecommendValue();
        //    }
        //    dataLoan.Save();

        //}

        //private void CalculateData()
        //{
        //    int[,] table = new int[13, 7];
        //    Guid loanId = GetGuid("LoanID");
        //    CPageBase dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(InitialEscrowAccSetupService));
        //    dataLoan.InitLoad();

        //    for (int i = 0; i < 13; i++)
        //    {
        //        for (int j = 0; j < 7; j++)
        //        {
        //            table[i, j] = GetInt("_" + i + "_" + j, 0);
        //        }
        //    }

        //    dataLoan.sInitialEscrowAcc = table;
        //    dataLoan.sSchedDueD1_rep = GetString("sSchedDueD1");

        //    SetResult("sRealETxRsrvMonRecommended", dataLoan.sRealETxRsrvMonRecommended_rep);
        //    SetResult("sHazInsRsrvMonRecommended", dataLoan.sHazInsRsrvMonRecommended_rep);
        //    SetResult("sMInsRsrvMonRecommended", dataLoan.sMInsRsrvMonRecommended_rep);
        //    SetResult("sFloodInsRsrvMonRecommended", dataLoan.sFloodInsRsrvMonRecommended_rep);
        //    SetResult("sSchoolTxRsrvMonRecommended", dataLoan.sSchoolTxRsrvMonRecommended_rep);
        //    SetResult("s1006RsrvMonRecommended", dataLoan.s1006RsrvMonRecommended_rep);
        //    SetResult("s1007RsrvMonRecommended", dataLoan.s1007RsrvMonRecommended_rep);

        //}

	}
}
