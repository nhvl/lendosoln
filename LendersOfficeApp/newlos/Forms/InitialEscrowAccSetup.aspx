<%@ Page language="c#" Codebehind="InitialEscrowAccSetup.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.InitialEscrowAccSetup" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>Initial Escrow Account Setup</title>
  </HEAD>
<body bgColor=gainsboro leftMargin=0 scroll=no>
<script language=javascript>
<!--
function _init() {
  resize(550, 480);
  lockField(<%=AspxTools.JsGetElementById(sSchedDueD1Lckd)%>, 'sSchedDueD1');
}

function lockField(cb, tbID) {
    var clientID = "";
    if (document.getElementById("_ClientID") != null)
        clientID = document.getElementById("_ClientID").value + "_";

    var item = document.getElementById(clientID + tbID);
    item.readOnly = !cb.checked;
}

function f_save() {
  var args = getAllFormValues();
  args["loanid"] = <%= AspxTools.JsString(LoanID) %>;
  var result = gService.loanedit.call("SaveData", args);

}
function refreshCalculation() {
  var args = getAllFormValues();
  args["loanid"] = <%= AspxTools.JsString(LoanID) %>;
  var result = gService.loanedit.call("CalculateData", args);
  if (!result.error) {
    populateForm(result.value);
  }

}
function f_saveAndClose() {
  f_save();
  var arg = window.dialogArguments || {};
  arg.sSchedDueD1Lckd = <%= AspxTools.JsGetElementById(sSchedDueD1Lckd) %>.checked;
  arg.sSchedDueD1 = <%= AspxTools.JsGetElementById(sSchedDueD1) %>.value;
  arg.OK = true;
  
  f_close(arg);
}
function f_close(args) {
  onClosePopup(args);

}
//-->
</script>
    <h4 class="page-header">Initial Escrow Account Setup</h4>
    <form id="InitialEscrowAccSetup" method="post" runat="server">
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
      <tr>
        <td style="padding-left: 5px"><span class="FieldLabel">First Payment Date</span><asp:CheckBox runat="server" ID="sSchedDueD1Lckd" onclick="lockField(this, 'sSchedDueD1'); refreshCalculation();"/> <ml:DateTextBox ID="sSchedDueD1" runat="server"  onchange="refreshCalculation();" CssClass="mask" preset="date" width="75" /> *Note: First Payment Date is required for the calculated reserves.</td>
      </tr>
      <tr><td>&nbsp;</td></tr>
      <tr>
        <td nowrap style="padding-left: 5px">
          <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
              <td class="FieldLabel" align="center"># of Month</td>
              <td class="FieldLabel" align="center">Real Estate Tax</td>
              <td class="FieldLabel" align="center">Haz Insurance</td>
              <td class="FieldLabel" align="center">Mortgage Insurance</td>
              <td class="FieldLabel" align="center">Flood Insurance</td>
              <td class="FieldLabel" align="center">School Taxes</td>
              <td class="FieldLabel" align="center">User Defined 1</td>
              <td class="FieldLabel" align="center">User Defined 2</td>
              <asp:PlaceHolder runat="server" ID="phAdditionalUserDefinedHeaders">
                  <td class="FieldLabel" align="center">User Defined 3</td>
                  <td class="FieldLabel" align="center">User Defined 4</td>
              </asp:PlaceHolder>
            </tr>
            <tr>
              <td class="FieldLabel" align="center">Cushion</td>
              <td align="center">
                <asp:TextBox ID="_0_0" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_0_1" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_0_2" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_0_3" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_0_4" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_0_5" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_0_6" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <asp:PlaceHolder runat="server" ID="phAdditionalUserDefinedCushion">
                <td align="center">
                  <asp:TextBox ID="_0_7" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
                <td align="center">
                  <asp:TextBox ID="_0_8" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              </asp:PlaceHolder>
            </tr>
            <tr>
              <td class="FieldLabel" align="center">Jan</td>
              <td align="center">
                <asp:TextBox ID="_1_0" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_1_1" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_1_2" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_1_3" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_1_4" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_1_5" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_1_6" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <asp:PlaceHolder runat="server" ID="phAdditionalUserDefinedJan">
                <td align="center">
                  <asp:TextBox ID="_1_7" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
                <td align="center">
                  <asp:TextBox ID="_1_8" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              </asp:PlaceHolder>
            </tr>
            <tr>
              <td class="FieldLabel" align="center">Feb</td>
              <td align="center">
                <asp:TextBox ID="_2_0" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_2_1" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_2_2" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_2_3" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_2_4" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_2_5" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_2_6" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <asp:PlaceHolder runat="server" ID="phAdditionalUserDefinedFeb">
                <td align="center">
                  <asp:TextBox ID="_2_7" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
                <td align="center">
                  <asp:TextBox ID="_2_8" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              </asp:PlaceHolder>
            </tr>
            <tr>
              <td class="FieldLabel" align="center">Mar</td>
              <td align="center">
                <asp:TextBox ID="_3_0" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_3_1" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_3_2" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_3_3" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_3_4" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_3_5" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_3_6" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <asp:PlaceHolder runat="server" ID="phAdditionalUserDefinedMar">
                <td align="center">
                  <asp:TextBox ID="_3_7" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
                <td align="center">
                  <asp:TextBox ID="_3_8" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              </asp:PlaceHolder>
            </tr>
            <tr>
              <td class="FieldLabel" align="center">Apr</td>
              <td align="center">
                <asp:TextBox ID="_4_0" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_4_1" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_4_2" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_4_3" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_4_4" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_4_5" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_4_6" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <asp:PlaceHolder runat="server" ID="phAdditionalUserDefinedApr">
                <td align="center">
                  <asp:TextBox ID="_4_7" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
                <td align="center">
                  <asp:TextBox ID="_4_8" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              </asp:PlaceHolder>
            </tr>
            <tr>
              <td class="FieldLabel" align="center">May</td>
              <td align="center">
                <asp:TextBox ID="_5_0" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_5_1" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_5_2" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_5_3" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_5_4" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_5_5" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_5_6" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <asp:PlaceHolder runat="server" ID="phAdditionalUserDefinedMay">
                <td align="center">
                  <asp:TextBox ID="_5_7" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
                <td align="center">
                  <asp:TextBox ID="_5_8" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              </asp:PlaceHolder>
            </tr>
            <tr>
              <td class="FieldLabel" align="center">Jun</td>
              <td align="center">
                <asp:TextBox ID="_6_0" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_6_1" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_6_2" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_6_3" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_6_4" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_6_5" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_6_6" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <asp:PlaceHolder runat="server" ID="phAdditionalUserDefinedJun">
                <td align="center">
                  <asp:TextBox ID="_6_7" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
                <td align="center">
                  <asp:TextBox ID="_6_8" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              </asp:PlaceHolder>
            </tr>
            <tr>
              <td class="FieldLabel" align="center">Jul</td>
              <td align="center">
                <asp:TextBox ID="_7_0" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_7_1" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_7_2" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_7_3" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_7_4" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_7_5" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_7_6" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <asp:PlaceHolder runat="server" ID="phAdditionalUserDefinedJul">
                <td align="center">
                  <asp:TextBox ID="_7_7" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
                <td align="center">
                  <asp:TextBox ID="_7_8" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              </asp:PlaceHolder>
            </tr>
            <tr>
              <td class="FieldLabel" align="center">Aug</td>
              <td align="center">
                <asp:TextBox ID="_8_0" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_8_1" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_8_2" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_8_3" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_8_4" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_8_5" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_8_6" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <asp:PlaceHolder runat="server" ID="phAdditionalUserDefinedAug">
                <td align="center">
                  <asp:TextBox ID="_8_7" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
                <td align="center">
                  <asp:TextBox ID="_8_8" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              </asp:PlaceHolder>
            </tr>
            <tr>
              <td class="FieldLabel" align="center">Sep</td>
              <td align="center">
                <asp:TextBox ID="_9_0" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_9_1" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_9_2" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_9_3" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_9_4" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_9_5" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_9_6" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <asp:PlaceHolder runat="server" ID="phAdditionalUserDefinedSep">
                <td align="center">
                  <asp:TextBox ID="_9_7" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
                <td align="center">
                  <asp:TextBox ID="_9_8" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              </asp:PlaceHolder>
            </tr>
            <tr>
              <td class="FieldLabel" align="center">Oct</td>
              <td align="center">
                <asp:TextBox ID="_10_0" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_10_1" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_10_2" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_10_3" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_10_4" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_10_5" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_10_6" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <asp:PlaceHolder runat="server" ID="phAdditionalUserDefinedOct">
                <td align="center">
                  <asp:TextBox ID="_10_7" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
                <td align="center">
                  <asp:TextBox ID="_10_8" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              </asp:PlaceHolder>
            </tr>
            <tr>
              <td class="FieldLabel" align="center">Nov</td>
              <td align="center">
                <asp:TextBox ID="_11_0" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_11_1" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_11_2" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_11_3" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_11_4" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_11_5" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_11_6" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <asp:PlaceHolder runat="server" ID="phAdditionalUserDefinedNov">
                <td align="center">
                  <asp:TextBox ID="_11_7" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
                <td align="center">
                  <asp:TextBox ID="_11_8" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              </asp:PlaceHolder>
            </tr>
            <tr>
              <td class="FieldLabel" align="center">Dec</td>
              <td align="center">
                <asp:TextBox ID="_12_0" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_12_1" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_12_2" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_12_3" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_12_4" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_12_5" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <td align="center">
                <asp:TextBox ID="_12_6" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              <asp:PlaceHolder runat="server" ID="phAdditionalUserDefinedDec">
                <td align="center">
                  <asp:TextBox ID="_12_7" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
                <td align="center">
                  <asp:TextBox ID="_12_8" runat="server" Width="37px" MaxLength="2" onchange="refreshCalculation();" /></td>
              </asp:PlaceHolder>
            </tr>
            <tr>
              <td class="FieldLabel" align="center">Calculated Reserves</td>
              <td align="center"><asp:TextBox ID="sRealETxRsrvMonRecommended" runat="server" Width="37px" ReadOnly="true" /></td>
              <td align="center"><asp:TextBox ID="sHazInsRsrvMonRecommended" runat="server" Width="37px" ReadOnly="true" /></td>
              <td align="center"><asp:TextBox ID="sMInsRsrvMonRecommended" runat="server" Width="37px" ReadOnly="true" /></td>
              <td align="center"><asp:TextBox ID="sFloodInsRsrvMonRecommended" runat="server" Width="37px" ReadOnly="true" /></td>
              <td align="center"><asp:TextBox ID="sSchoolTxRsrvMonRecommended" runat="server" Width="37px" ReadOnly="true" /></td>
              <td align="center"><asp:TextBox ID="s1006RsrvMonRecommended" runat="server" Width="37px" ReadOnly="true" /></td>
              <td align="center"><asp:TextBox ID="s1007RsrvMonRecommended" runat="server" Width="37px" ReadOnly="true" /></td>
              <asp:PlaceHolder runat="server" ID="phAdditionalUserDefinedCalcReserves">
                <td align="center"><asp:TextBox ID="sU3RsrvMonRecommended" runat="server" Width="37px" ReadOnly="true" /></td>
                <td align="center"><asp:TextBox ID="sU4RsrvMonRecommended" runat="server" Width="37px" ReadOnly="true" /></td>
              </asp:PlaceHolder>
              
            </tr>
            <tr><td>&nbsp;</td>
            <asp:PlaceHolder runat="server" ID="phUpdateGFEReserveCheckbox" >
            <td colspan="7"><input type="checkbox" id="UpdateGFEReserves" name="UpdateGFEReserves" value="True" checked/><label for="UpdateGFEReserves">Update GFE with calculated reserves</label></td>
            </asp:PlaceHolder>
            </tr>
          </table>
        </td>
      </tr>
      <tr><td></td></tr>
      <tr>
        <td nowrap align="center">
          <input type="button" value="OK" class="ButtonStyle" style="width: 57px" onclick="f_saveAndClose();">&nbsp;&nbsp; 
          <input type="button" value="Cancel" class="ButtonStyle" onclick="f_close();">&nbsp;&nbsp; 
          </td>
      </tr>
    </table>
    <uc1:cModalDlg ID="CModalDlg1" runat="server"></uc1:cModalDlg>
    </form>
	
  </body>
</HTML>
