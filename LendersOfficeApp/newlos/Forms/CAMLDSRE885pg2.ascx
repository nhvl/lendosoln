﻿<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="CAMLDSRE885pg2.ascx.cs" Inherits="LendersOfficeApp.newlos.Forms.CAMLDSRE885pg2" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<table id="Table1" cellSpacing="0" cellPadding="0" border="0">
	<tr>
		<td class="FormTableSubheader">Additional Required California Disclosures</td>
	</tr>
	<tr>
		<td>
			<TABLE class="InsetBorder" id="Table8" cellSpacing="0" cellPadding="0" width="99%" border="0">
				<TR>
					<TD noWrap>
						<TABLE id="Table2" cellSpacing="0" cellPadding="0" border="0">
				    		<colgroup></colgroup>
				            <colgroup style="padding-left:20px;"></colgroup>
				            <colgroup style="padding-left:20px;"></colgroup>
							<TR>
								<TD class="FieldLabel">I. Proposed Loan Amount</TD>
								<TD></TD>
								<TD><ml:MoneyTextBox id="sFinalLAmt" ReadOnly="True" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">Initial Commissions, Fees, Costs and Expenses Summarized on 
									Page 1</TD>
								<TD vAlign="top"><ml:MoneyTextBox id="sTotEstScMlds" ReadOnly="True" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">Payment of Other Obligations (List):</TD>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" style="PADDING-LEFT:20px" title="The purchase of credit life and/or disability insurance by a borrower is NOT required as a condition of making this proposed loan">Credit Life and/or Disability Insurance</TD>
								<TD><ml:MoneyTextBox id="sDisabilityIns" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" style="PADDING-LEFT:20px">Purchase Price / Paid Off</TD>
								<TD><ml:MoneyTextBox id="sPurchPrice_sRefPdOffAmtCamlds" preset="money" width="90" runat="server" readonly="true" onchange="refreshCalculation();"></ml:MoneyTextBox></TD>
								<TD></TD> 
							</TR>
							<TR>
								<TD style="PADDING-LEFT:20px"><asp:TextBox id="sOtherPmtObligationDescMlds" runat="server" MaxLength="21" Width="342px" ReadOnly="true"></asp:TextBox></TD>
								<TD><ml:MoneyTextBox id="sOtherPmtObligationMlds" preset="money" width="90" runat="server" onchange="refreshCalculation();" ReadOnly="true"></ml:MoneyTextBox></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">Subtotal of All Deductions</TD>
								<TD></TD>
								<TD><ml:MoneyTextBox id="sTotDeductFromFinalLAmt" ReadOnly="True" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">
						            Estimated Cash at Closing &nbsp;&nbsp;&nbsp;
						        </TD>
								<TD></TD>
								<TD><ml:MoneyTextBox id="sTotEstFntcCamlds" ReadOnly="True" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</td>
	</tr>
	<tr>
		<td>
			<TABLE class="InsetBorder" id="Table9" cellSpacing="0" cellPadding="0" width="99%" border="0">
				<TR>
					<TD noWrap>
						<TABLE id="Table3" cellSpacing="0" cellPadding="0" border="0">
							<colgroup width="350px"></colgroup>
				            <colgroup></colgroup>
				            <colgroup></colgroup>
							<tr>
							    <TD class="FieldLabel">II. Proposed Loan Term (months)</TD>
								<TD><asp:TextBox id="sTerm" runat="server" onchange="refreshCalculation();" MaxLength="4" Width="48"></asp:TextBox></TD>
							</tr>
							<TR>
								<TD class="FieldLabel">III. Proposed Interest Rate</TD>
								<TD><ml:PercentTextBox id="sNoteIR" preset="percent" width="70" runat="server" onchange="refreshCalculation();"></ml:PercentTextBox></TD>
								<TD><asp:dropdownlist id="sFinMethT" runat="server" onchange="refreshCalculation();" Width="116px"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">IV. Initial Adjustable Rate in effect for</td>
								<td class="FieldLabel"><asp:TextBox id="sMldsRe885InitialAdjRPeriod" runat="server" onchange="refreshCalculation();" MaxLength="3" Width="48"></asp:TextBox> mths</TD>
							</TR>
							<TR>
								<TD class="FieldLabel">V. Fully Indexed Interest Rate</td>
								<td><ml:PercentTextBox id="sMldsRe885FullyIndexR" preset="percent" width="70" runat="server" onchange="refreshCalculation();"></ml:PercentTextBox></td>
							</TR>
							<TR>
								<TD class="FieldLabel">VI. Maximum Interest Rate</TD>
								<td><ml:PercentTextBox id="sMldsRe885MaxLifeCapRAdjustable" preset="percent" width="70" runat="server" onchange="refreshCalculation();"></ml:PercentTextBox></td>
							</TR>
							<TR>
								<TD class="FieldLabel">VII. Proposed Initial (Minimum) Monthly Loan Payment</td>
								<td><ml:MoneyTextBox id="sProThisMPmt" ReadOnly="True" preset="money" width="90" runat="server"></ml:MoneyTextBox></td>
							</TR>
							<tr>
								<td class="FieldLabel">VIII. Interest Rate can Increase</td>
								<td class="FieldLabel" colspan="2">
								    <ml:PercentTextBox id="sRAdjCapRAdjustableMlds" preset="percent" width="70" runat="server" onchange="refreshCalculation();"></ml:PercentTextBox> each 
    								<asp:TextBox id="sRAdjCapMonAdjustableMlds" runat="server" onchange="refreshCalculation();" MaxLength="3" Width="48"></asp:TextBox> mths
    							</td>
							</tr>
							<tr>
								<td class="FieldLabel">IX. Payment Options end after</td>
								<td class="FieldLabel" colspan="2">
								    <asp:TextBox id="sMldsRe885PmtOptionEndMons" runat="server" onchange="refreshCalculation();" MaxLength="3" Width="48"></asp:TextBox> mths or 
								    <ml:PercentTextBox id="sMldsRe885PmtOptionEndOrigBalPc" preset="percent" width="70" runat="server" onchange="refreshCalculation();"></ml:PercentTextBox> of Original Balance, whichever comes first.
    							</td>
							</tr>
							<tr>
								<td class="FieldLabel">X. Negative amortization, minimum and interest only payments no longer allowed after</td>
								<td class="FieldLabel">
								    <asp:TextBox id="sMldsRe885OptionArmMinPayPeriod" runat="server" onchange="refreshCalculation();" MaxLength="3" Width="48"></asp:TextBox> mths
    							</td>
							</tr>
							<tr>
							    <td class="FieldLabel" style="padding-left:20px;">Required P & I payments</td>
							    <td class="FieldLabel" colspan="2">
							        <ml:MoneyTextBox id="sMldsRe885MaxMonthlyPmt" preset="money" width="90" runat="server"></ml:MoneyTextBox> for 
							        <asp:TextBox id="sMldsRe885MaxMonthlyPmtMons" runat="server" onchange="refreshCalculation();" MaxLength="3" Width="48" /> mths
							    </td>
							</tr>
							<tr>
							    <td class="FieldLabel">XI. If loan contains negative amortization, at the time no additional negative amortization will accrue, your loan balance will be</td>
							    <td>
							        <ml:MoneyTextBox id="sMldsRe885NegAmortLoanBalance" preset="money" width="90" runat="server" />
							    </td>
							</tr>
							<tr>
							    <TD class="FieldLabel" noWrap>XII. Balloon? <asp:CheckBox id="sBalloonPmt" runat="server" onclick="refreshCalculation();"></asp:CheckBox>Yes</td>
								<td class="FieldLabel">Final Balloon Payment <ml:MoneyTextBox id="sFinalBalloonPmt" ReadOnly="True" preset="money" width="90" runat="server"></ml:MoneyTextBox></TD>
							    <TD class="FieldLabel">Estimated Due Date
							        <asp:TextBox id="sFinalBalloonPmtDueD" runat="server" ReadOnly="True" Width="90px"></asp:TextBox>
							    </TD>
							</tr>
							</TABLE>
					</TD>
				</TR>
			</TABLE>
		</td>
	</tr>
  <TR>
      <td>
          <table id="Table13" cellspacing="0" cellpadding="0" border="0">
              <tr>
                  <td>
                      <table id="Table4" cellspacing="0" cellpadding="0" width="99%" border="0" class="InsetBorder">
                          <tr>
                              <td class="FormTableSubheader">
                                  XIII. Prepayments
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:DropDownList ID="sMldsPpmtT" runat="server" onchange="prepaymentsEnableOptionsDDL();">
                                      <asp:ListItem Value="0" Selected="True">Leave Blank</asp:ListItem>
                                      <asp:ListItem Value="3">No prepayment penalty</asp:ListItem>
                                      <asp:ListItem Value="1">Other (see loan documents for details)</asp:ListItem>
                                      <asp:ListItem Value="2">Any payment of principal in any calendar year in excess of 20% of ...</asp:ListItem>
                                  </asp:DropDownList>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Pay prepayment penalty if the loan is paid off or refinanced in the first
                                  <asp:TextBox ID="sMldsPpmtPeriod" runat="server" Width="65px"></asp:TextBox>&nbsp;years.
                              </td>
                          </tr>
                          <tr><td>The prepayment penalty could be as much as
                                  <ml:MoneyTextBox id="sMldsPpmtMaxAmt" runat="server" width="90" preset="money">
                                  </ml:MoneyTextBox></td></tr>
                          <tr>
                              <td>
                                  &nbsp;&nbsp;&nbsp;&nbsp; ...
                                  <asp:DropDownList ID="sMldsPpmtBaseT" runat="server" Width="141px">
                                      <asp:ListItem Value="0" Selected="True">the unpaid balance</asp:ListItem>
                                      <asp:ListItem Value="1">the original balance</asp:ListItem>
                                  </asp:DropDownList>
                                  &nbsp;will include a penalty not to exceed
                                  <asp:TextBox ID="sMldsPpmtMonMax" runat="server" Width="52px" MaxLength="4"></asp:TextBox>&nbsp;months
                                  advance interest...
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Other Prepayment Penalty Description
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:TextBox ID="sMldsPpmtOtherDetail" runat="server" Width="427px" TextMode="MultiLine"
                                      Height="52px"></asp:TextBox>
                              </td>
                          </tr>
                      </table>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                  </td>
	</tr>
	</table>
  <tr>
    <TD>
      <TABLE class=InsetBorder id=Table14 cellSpacing=0 cellPadding=0 
        border=0>
        <TR>
          <TD noWrap class=FormTableSubheader>XIV. Taxes and Insurance</TD></TR>
        <TR>
          <TD noWrap>There <asp:DropDownList id=sMldsHasImpound onchange="f_sMldsHasImpound_onchange(true)" runat="server">
<asp:ListItem Value="0">is NO impound (escrow) account, you will have to plan for the payment of</asp:ListItem>
<asp:ListItem Value="1">will be an impound account which will collect</asp:ListItem>
</asp:DropDownList></TD></TR>
        <TR>
          <TD noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
			<asp:CheckBox id=sMldsImpoundIncludeRealETx runat="server" Text="County Property Taxes"></asp:CheckBox>
			<asp:CheckBox id=sMldsImpoundIncludeHazIns runat="server" Text="Haz. Ins."></asp:CheckBox>
			<asp:CheckBox id=sMldsImpoundIncludeMIns runat="server" Text="Mortgage Insurance"></asp:CheckBox>
			<asp:CheckBox id=sMldsImpoundIncludeFloodIns runat="server" Text="Flood Insurance"></asp:CheckBox>
			<asp:CheckBox id=sMldsImpoundIncludeOther runat="server" Text="Other" onclick="enableOtherTextBox();" ></asp:CheckBox>
				<asp:TextBox id=sMldsImpoundOtherDesc runat="server"></asp:TextBox></TD></TR>
        <TR>
          <TD id="NoImpoundTI" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; approximately <ml:MoneyTextBox id=sMldsMonthlyImpoundPmtPerYear_NoImpound runat="server" width="90" preset="money" ReadOnly="true"></ml:MoneyTextBox>&nbsp;per 
              year.</TD>
          <TD id="ImpoundTI" style="display:none" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; approximately <ml:MoneyTextBox id=sMldsMonthlyImpoundPmt_Impound runat="server" width="90" preset="money" ReadOnly="true"></ml:MoneyTextBox>&nbsp;per 
              month.</TD>
              </TR></TABLE></TD></TR>
	<TR>
		<TD></TD>
	</TR>
</table>

<uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>

<SCRIPT language="javascript">
<!--


function linkToGFE() {
  <% if (sUseObsoleteGfeForm) { %>
    linkMe('GoodFaithEstimateOld.aspx');
  <% } else { %>
    linkMe('GoodFaithEstimate.aspx');
  <% } %>
}

function prepaymentsEnableOptionsDDL()
{
	var selectedValue = <%= AspxTools.JsGetElementById(sMldsPpmtT) %>.value;
	
	//this is the first line
	var sMldsPpmtPeriodTB = <%= AspxTools.JsGetElementById(sMldsPpmtPeriod) %>;
	
	//this is the center
	var sMldsPpmtBaseTDDL = <%= AspxTools.JsGetElementById(sMldsPpmtBaseT) %>;
	var sMldsPpmtMonMaxTB = <%= AspxTools.JsGetElementById(sMldsPpmtMonMax) %>;
	var sMldsPpmtMaxAmtMB = <%= AspxTools.JsGetElementById(sMldsPpmtMaxAmt) %>;

	//this is the textbox
	var sMldsPpmtOtherDetailTB = <%= AspxTools.JsGetElementById(sMldsPpmtOtherDetail) %>;


	if(selectedValue == 0 || selectedValue == 3)
	{		
		sMldsPpmtPeriodTB.readOnly = true;
		sMldsPpmtPeriodTB.value = "";
		
		sMldsPpmtMonMaxTB.readOnly = true;
		sMldsPpmtMaxAmtMB.readOnly = true;
		sMldsPpmtMonMaxTB.value = "0";
		sMldsPpmtMaxAmtMB.value = "$0.00";
		disableDDL(sMldsPpmtBaseTDDL, true);
			
		sMldsPpmtOtherDetailTB.readOnly = true;
		sMldsPpmtOtherDetailTB.value = "";

	
	}
	else if(selectedValue == 1)
	{
		sMldsPpmtPeriodTB.readOnly = false;
	
		sMldsPpmtMonMaxTB.readOnly = true;
		sMldsPpmtMaxAmtMB.readOnly = true;
		sMldsPpmtMonMaxTB.value = "0";
		sMldsPpmtMaxAmtMB.value = "$0.00";
		disableDDL(sMldsPpmtBaseTDDL, true);

		sMldsPpmtOtherDetailTB.readOnly = false;
	}
	else if(selectedValue == 2)
	{
		sMldsPpmtPeriodTB.readOnly = false;
	
		sMldsPpmtMonMaxTB.readOnly = false;
		sMldsPpmtMaxAmtMB.readOnly = false;
		disableDDL(sMldsPpmtBaseTDDL, false);
		
		sMldsPpmtOtherDetailTB.readOnly = true;
		sMldsPpmtOtherDetailTB.value = "";

	}
	
}

function enableOtherTextBox()
{
	var sMldsImpoundIncludeOtherCB = <%= AspxTools.JsGetElementById(sMldsImpoundIncludeOther) %>;
	var sMldsImpoundOtherDescTB = <%= AspxTools.JsGetElementById(sMldsImpoundOtherDesc) %>;
	
	if(sMldsImpoundIncludeOtherCB.checked == true)
	{
		sMldsImpoundOtherDescTB.readOnly = false;
	}
	else
	{
		sMldsImpoundOtherDescTB.value = "";
		sMldsImpoundOtherDescTB.readOnly = true;
	}
}
function _initControl() {
enableOtherTextBox();
prepaymentsEnableOptionsDDL();
f_sFinMethT_onchange();
f_sMldsHasImpound_onchange(false);
}
function f_sFinMethT_onchange() {
  var ddl = <%= AspxTools.JsGetElementById(sFinMethT) %>;
  var bIsArm = (ddl.value == <%= AspxTools.JsString(E_sFinMethT.ARM.ToString("D")) %>);
  
  <%=AspxTools.JsGetElementById(sMldsRe885InitialAdjRPeriod) %>.readOnly = !bIsArm;
  <%=AspxTools.JsGetElementById(sMldsRe885FullyIndexR) %>.readOnly = !bIsArm;
  <%=AspxTools.JsGetElementById(sMldsRe885MaxLifeCapRAdjustable) %>.readOnly = !bIsArm;
  <%=AspxTools.JsGetElementById(sRAdjCapRAdjustableMlds) %>.readOnly = !bIsArm;
  <%=AspxTools.JsGetElementById(sRAdjCapMonAdjustableMlds) %>.readOnly = !bIsArm;
  <%=AspxTools.JsGetElementById(sMldsRe885PmtOptionEndMons) %>.readOnly = !bIsArm;
  <%=AspxTools.JsGetElementById(sMldsRe885PmtOptionEndOrigBalPc) %>.readOnly = !bIsArm;
}

function f_sMldsHasImpound_onchange(bRefresh)
{
   var bHasImpound = <%=AspxTools.JsGetElementById(sMldsHasImpound) %>.value == "1";
   document.getElementById('ImpoundTI').style.display = bHasImpound ? 'block' : 'none';
   document.getElementById('NoImpoundTI').style.display = !bHasImpound ? 'block' : 'none';
   
   if (bRefresh) refreshCalculation();
}
//-->
</SCRIPT>

