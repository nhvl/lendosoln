using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{

	public partial class MortgageLoanOriginationAgreement : LendersOfficeApp.newlos.BaseLoanPage
	{

        protected void PageInit(object sender, System.EventArgs e) 
        {

            MortgageLoanOrigAgreement_Zip.SmartZipcode(MortgageLoanOrigAgreement_City, MortgageLoanOrigAgreement_State);
            this.PageTitle = "Mortgage Loan Origination Agreement";
            this.PageID = "MortgageLoanOriginationAgreement";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CMortgageLoanOriginationAgreementPDF);
        }
        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(MortgageLoanOriginationAgreement));
            dataLoan.InitLoad();
            sMBrokerNmOfLaw.Text = dataLoan.sMBrokerNmOfLaw;

            IPreparerFields broker = dataLoan.GetPreparerOfForm( E_PreparerFormT.MortgageLoanOrigAgreement, E_ReturnOptionIfNotExist.ReturnEmptyObject ); 

            MortgageLoanOrigAgreement_CompanyName.Text = broker.CompanyName;
            MortgageLoanOrigAgreement_StreetAddr.Text = broker.StreetAddr;
            MortgageLoanOrigAgreement_City.Text = broker.City;
            MortgageLoanOrigAgreement_State.Value = broker.State;
            MortgageLoanOrigAgreement_Zip.Text = broker.Zip;
            MortgageLoanOrigAgreement_PhoneOfCompany.Text = broker.PhoneOfCompany;
            MortgageLoanOrigAgreement_FaxOfCompany.Text = broker.FaxOfCompany;
            MortgageLoanOrigAgreement_PrepareDate.Text = broker.PrepareDate_rep;

        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
            

        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
