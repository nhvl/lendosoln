using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{

	public partial class InitialEscrowAccSetup : LendersOfficeApp.newlos.BaseLoanPage
	{

        private System.Web.UI.WebControls.TextBox[,] m_controls;
        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.DisplayCopyRight = false;

            m_controls = new System.Web.UI.WebControls.TextBox[,] {
                                                                      {_0_0, _0_1, _0_2, _0_3, _0_4, _0_5, _0_6, _0_7, _0_8},
                                                                      {_1_0, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8},
                                                                      {_2_0, _2_1, _2_2, _2_3, _2_4, _2_5, _2_6, _2_7, _2_8},
                                                                      {_3_0, _3_1, _3_2, _3_3, _3_4, _3_5, _3_6, _3_7, _3_8},
                                                                      {_4_0, _4_1, _4_2, _4_3, _4_4, _4_5, _4_6, _4_7, _4_8},
                                                                      {_5_0, _5_1, _5_2, _5_3, _5_4, _5_5, _5_6, _5_7, _5_8},
                                                                      {_6_0, _6_1, _6_2, _6_3, _6_4, _6_5, _6_6, _6_7, _6_8},
                                                                      {_7_0, _7_1, _7_2, _7_3, _7_4, _7_5, _7_6, _7_7, _7_8},
                                                                      {_8_0, _8_1, _8_2, _8_3, _8_4, _8_5, _8_6, _8_7, _8_8},
                                                                      {_9_0, _9_1, _9_2, _9_3, _9_4, _9_5, _9_6, _9_7, _9_8},
                                                                      {_10_0, _10_1, _10_2, _10_3, _10_4, _10_5, _10_6, _10_7, _10_8},
                                                                      {_11_0, _11_1, _11_2, _11_3, _11_4, _11_5, _11_6, _11_7, _11_8},
                                                                      {_12_0, _12_1, _12_2, _12_3, _12_4, _12_5, _12_6, _12_7, _12_8}

                                                                  };
            this.RegisterService("loanedit", "/newlos/Forms/InitialEscrowAccSetupService.aspx");

            phAdditionalUserDefinedHeaders.Visible =
            phAdditionalUserDefinedCushion.Visible =
            phAdditionalUserDefinedJan.Visible =
            phAdditionalUserDefinedFeb.Visible =
            phAdditionalUserDefinedMar.Visible =
            phAdditionalUserDefinedApr.Visible =
            phAdditionalUserDefinedMay.Visible =
            phAdditionalUserDefinedJun.Visible =
            phAdditionalUserDefinedJul.Visible =
            phAdditionalUserDefinedAug.Visible =
            phAdditionalUserDefinedSep.Visible =
            phAdditionalUserDefinedOct.Visible =
            phAdditionalUserDefinedNov.Visible =
            phAdditionalUserDefinedDec.Visible =
            phAdditionalUserDefinedCalcReserves.Visible = Broker.EnableAdditionalSection1000CustomFees;

        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(InitialEscrowAccSetup));
            dataLoan.InitLoad();

            int[,] table = dataLoan.sInitialEscrowAcc;

            for (int i = 0; i < 13; i++) 
            {
                for (int j = 0; j < 9; j++) 
                {
                    if (table[i,j] != 0) 
                    {
                        m_controls[i, j].Text = table[i,j].ToString();
                    }
                }
            }

            sSchedDueD1Lckd.Checked = dataLoan.sSchedDueD1Lckd;
            sSchedDueD1.Text = dataLoan.sSchedDueD1_rep;
            sRealETxRsrvMonRecommended.Text = dataLoan.sRealETxRsrvMonRecommended_rep;
            sHazInsRsrvMonRecommended.Text = dataLoan.sHazInsRsrvMonRecommended_rep;
            sMInsRsrvMonRecommended.Text = dataLoan.sMInsRsrvMonRecommended_rep;
            sFloodInsRsrvMonRecommended.Text = dataLoan.sFloodInsRsrvMonRecommended_rep;
            sSchoolTxRsrvMonRecommended.Text = dataLoan.sSchoolTxRsrvMonRecommended_rep;
            s1006RsrvMonRecommended.Text = dataLoan.s1006RsrvMonRecommended_rep;
            s1007RsrvMonRecommended.Text = dataLoan.s1007RsrvMonRecommended_rep;
            sU3RsrvMonRecommended.Text = dataLoan.sU3RsrvMonRecommended_rep;
            sU4RsrvMonRecommended.Text = dataLoan.sU4RsrvMonRecommended_rep;

            phUpdateGFEReserveCheckbox.Visible = dataLoan.AreAllVisibleGFE1000RsrvMonthsLocked();

        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
