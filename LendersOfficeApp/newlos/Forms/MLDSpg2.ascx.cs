using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;
using LendersOfficeApp.newlos.Status;



namespace LendersOfficeApp.newlos.Forms
{
    public partial  class MLDSpg2 : BaseLoanUserControl, IAutoLoadUserControl
    {

		#region member variables
		protected ContactFieldMapper CFM;
        protected bool sUseObsoleteGfeForm;
		#endregion


        private void BindLienPositionComboBox(MeridianLink.CommonControls.ComboBox cb) 
        {
            cb.Items.Add("1st");
            cb.Items.Add("2nd");
            cb.Items.Add("3rd");
            cb.Items.Add("4th");
        }
        protected void PageInit(object sender, System.EventArgs e) 
        {
            Tools.Bind_sFinMethT(sFinMethT);
            BindLienPositionComboBox(sLien1PriorityBefore);
            BindLienPositionComboBox(sLien1PriorityAfter);
            BindLienPositionComboBox(sLien2PriorityBefore);
            BindLienPositionComboBox(sLien2PriorityAfter);
            BindLienPositionComboBox(sLien3PriorityBefore);
            BindLienPositionComboBox(sLien3PriorityAfter);
            GfeTilZip.SmartZipcode(GfeTilCity, GfeTilState);

            Tools.Bind_TriState(sMldsIsNoDocTri);
            Tools.Bind_TriState(sMldsLateChargeTri);

            sMldsIsNoDocTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
            sMldsLateChargeTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);


        }
        private void BindDataObject(CPageData dataLoan) 
        {
			IPreparerFields gfeTil = dataLoan.GetPreparerOfForm( E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject );
            GfeTilPreparerName.Text = gfeTil.PreparerName;
            GfeTilLicenseNumOfAgent.Text = gfeTil.LicenseNumOfAgent;
            GfeTilCompanyName.Text = gfeTil.CompanyName;
            GfeTilStreetAddr.Text = gfeTil.StreetAddr;
            GfeTilCity.Text = gfeTil.City;
            GfeTilState.Value = gfeTil.State;
            GfeTilZip.Text = gfeTil.Zip;
            GfeTilLicenseNumOfCompany.Text = gfeTil.LicenseNumOfCompany;
            CFM.IsLocked = gfeTil.IsLocked;
            CFM.AgentRoleT = gfeTil.AgentRoleT;
            
    
            sTerm.Text = dataLoan.sTerm_rep;
            sTerm.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sDue.Text = dataLoan.sDue_rep;
            sDue.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
			sFinMethT.Enabled = ! ( dataLoan.sIsRateLocked || IsReadOnly );
            sBalloonPmt.Checked = dataLoan.sBalloonPmt;
            sBalloonPmt.Enabled = dataLoan.sGfeIsBalloonLckd;
            sMldsPpmtMonMax.Text = dataLoan.sMldsPpmtMonMax_rep;
            sLienholder1NmBefore.Text = dataLoan.sLienholder1NmBefore;
            sLien1PriorityBefore.Text = dataLoan.sLien1PriorityBefore;
            sLienholder2NmBefore.Text = dataLoan.sLienholder2NmBefore;
            sLien2PriorityBefore.Text = dataLoan.sLien2PriorityBefore;
            sLienholder3NmBefore.Text = dataLoan.sLienholder3NmBefore;
            sLien3PriorityBefore.Text = dataLoan.sLien3PriorityBefore;
            sLienholder1NmAfter.Text = dataLoan.sLienholder1NmAfter;
            sLien1PriorityAfter.Text = dataLoan.sLien1PriorityAfter;
            sLienholder2NmAfter.Text = dataLoan.sLienholder2NmAfter;
            sLien2PriorityAfter.Text = dataLoan.sLien2PriorityAfter;
            sLienholder3NmAfter.Text = dataLoan.sLienholder3NmAfter;
            sLien3PriorityAfter.Text = dataLoan.sLien3PriorityAfter;
            Tools.SetDropDownListValue(sBrokControlledFundT, dataLoan.sBrokControlledFundT);
            Tools.SetDropDownListValue(sMldsPpmtT, dataLoan.sMldsPpmtT);
            Tools.SetDropDownListValue(sMldsPpmtBaseT, dataLoan.sMldsPpmtBaseT);

            sU1FntcDesc.Text = dataLoan.sU1FntcDesc;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sTotEstScMlds.Text = dataLoan.sTotEstScMlds_rep;
            sDisabilityIns.Text = dataLoan.sDisabilityIns_rep;
            sPurchPrice.Text = dataLoan.sPurchPrice_rep;
            if (dataLoan.sLPurposeT == E_sLPurposeT.Refin || dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl) 
            {
                // Since user unable to modify sLPurposeT, it is safe to initialize enable state of sPurchPrice here.
                sPurchPrice.ReadOnly = true;
            }
            sRefPdOffAmtCamlds.Text = dataLoan.sRefPdOffAmtCamlds_rep;
            sTotCcPboPbs.Text = "(" + dataLoan.sTotCcPboPbs_rep + ")";
            sU1Fntc.Text = dataLoan.sU1Fntc_rep;
            sTotDeductFromFinalLAmt.Text = dataLoan.sTotDeductFromFinalLAmt_rep;
            sTotEstFntcCamlds.Text = dataLoan.sTotEstFntcCamlds_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sFinalBalloonPmt.Text = dataLoan.sFinalBalloonPmt_rep;
            sProThisMPmt.Text = dataLoan.sProThisMPmt_rep;
            sFinalBalloonPmtDueD.Text = dataLoan.sFinalBalloonPmtDueD_rep;
            sLien1AmtBefore.Text = dataLoan.sLien1AmtBefore_rep;
            sLien2AmtBefore.Text = dataLoan.sLien2AmtBefore_rep;
            sLien3AmtBefore.Text = dataLoan.sLien3AmtBefore_rep;
            sLien1AmtAfter.Text = dataLoan.sLien1AmtAfter_rep;
            sLien2AmtAfter.Text = dataLoan.sLien2AmtAfter_rep;
            sLien3AmtAfter.Text = dataLoan.sLien3AmtAfter_rep;

            sUseObsoleteGfeForm = dataLoan.sUseObsoleteGfeForm;

            Tools.Set_TriState(sMldsIsNoDocTri, dataLoan.sMldsIsNoDocTri);
            Tools.Set_TriState(sMldsLateChargeTri, dataLoan.sMldsLateChargeTri);

            sMldsImpoundOtherDesc.Text = dataLoan.sMldsImpoundOtherDesc;
            sMldsImpoundIncludeOther.Checked = dataLoan.sMldsImpoundIncludeOther;
            sMldsImpoundIncludeFloodIns.Checked = dataLoan.sMldsImpoundIncludeFloodIns;
            sMldsImpoundIncludeMIns.Checked = dataLoan.sMldsImpoundIncludeMIns;
            sMldsImpoundIncludeRealETx.Checked = dataLoan.sMldsImpoundIncludeRealETx;
            sMldsImpoundIncludeHazIns.Checked = dataLoan.sMldsImpoundIncludeHazIns;
            sMldsMonthlyImpoundPmt.Text = dataLoan.sMldsMonthlyImpoundPmt_rep;

            sMldsIsNoDocTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
            sMldsLateChargeTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);

            Tools.SetDropDownListValue(sMldsHasImpound, dataLoan.sMldsHasImpound ? "1" : "0");

            sMldsPpmtPeriod.Text = dataLoan.sMldsPpmtPeriod_rep;
            sMldsPpmtOtherDetail.Text = dataLoan.sMldsPpmtOtherDetail;
            sMldsPpmtMaxAmt.Text = dataLoan.sMldsPpmtMaxAmt_rep;
        }
        public void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency( LoanID , typeof(MLDSpg2));
            dataLoan.InitLoad();

            BindDataObject(dataLoan);

        }
        public void SaveData() 
        {
        }
        /// <summary>
        /// Is there to make this method global? So I would not need to copy-n-paste this methods on every page use Z
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZeroOneCheckBoxList_Load(object sender, System.EventArgs e) 
        {
            CheckBoxList cbl = (CheckBoxList) sender;
            ArrayList list = new ArrayList();

            foreach (ListItem o in cbl.Items) 
            {
                list.Add("'" + o.Value + "'");
            }

            Page.ClientScript.RegisterArrayDeclaration(cbl.ClientID + "_items", string.Join(",", (string[])list.ToArray(typeof(string))));        
            Page.ClientScript.RegisterHiddenField("cbl_" + cbl.ClientID, "");
            ((BasePage) Page).AddInitScriptFunctionWithArgs("_registerCBL", string.Format("'{0}', {1}", cbl.ClientID, cbl.Items.Count));


        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
			// OPM 17652 - Ethan
			CFM.Type				= "19";
			CFM.CompanyNameField	= GfeTilCompanyName.ClientID;
			CFM.StreetAddressField	= GfeTilStreetAddr.ClientID;
			CFM.CompanyLicenseField	= GfeTilLicenseNumOfCompany.ClientID;
			CFM.CityField			= GfeTilCity.ClientID;
			CFM.StateField			= GfeTilState.ClientID;
			CFM.ZipField			= GfeTilZip.ClientID;
			CFM.AgentNameField		= GfeTilPreparerName.ClientID;
			CFM.AgentLicenseField	= GfeTilLicenseNumOfAgent.ClientID;
            CFM.IsAllowLockableFeature = true;
        }

    }
}
