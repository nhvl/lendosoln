using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendersOfficeApp.los.RatePrice;

namespace LendersOfficeApp.newlos.Forms
{

    public class LoanTermsServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "PopulateFromLoanProgramTemplate":
                    PopulateFromLoanProgramTemplate();
                    break;
            }

        }
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(LoanTermsServiceItem));
        }
        protected BrokerUserPrincipal BrokerUser
        {
            get
            {
                BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
                if (null == principal)
                    RequestHelper.Signout();

                return principal;
            }
        }
        protected BrokerDB Broker
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BrokerDB; }
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataLoan.sRAdj1stCapR_rep       = GetString("sRAdj1stCapR");
            dataLoan.sRAdjCapR_rep          = GetString("sRAdjCapR");
            dataLoan.sRAdjLifeCapR_rep      = GetString("sRAdjLifeCapR");
            dataLoan.sRAdjMarginR_rep       = GetString("sRAdjMarginR");
            dataLoan.sRAdjIndexR_rep        = GetString("sRAdjIndexR");
            dataLoan.sIsConvertibleMortgage = GetBool("sIsConvertibleMortgage");
            dataLoan.sRAdjRoundToR_rep      = GetString("sRAdjRoundToR");
            dataLoan.sPmtAdjCapR_rep        = GetString("sPmtAdjCapR");
            dataLoan.sPmtAdjMaxBalPc_rep    = GetString("sPmtAdjMaxBalPc");
            dataLoan.sLateChargePc          = GetString("sLateChargePc");

            dataLoan.sTerm_rep              = GetString("sTerm");
            dataLoan.sDue_rep               = GetString("sDue");
            dataLoan.sProMInsT              = (E_PercentBaseT) GetInt("sProMInsT");
            dataLoan.sFinMethT              = (E_sFinMethT) GetInt("sFinMethT");
            dataLoan.sLateChargeBaseDesc    = GetString("sLateChargeBaseDesc");
            dataLoan.sVarRNotes             = GetString("sVarRNotes");
            dataLoan.sPrepmtRefundT         = (E_sPrepmtRefundT) GetInt("sPrepmtRefundT");
            dataLoan.sPrepmtPenaltyT        = (E_sPrepmtPenaltyT) GetInt("sPrepmtPenaltyT");

            dataLoan.sPrepmtPeriodMonths_rep        = GetString("sPrepmtPeriodMonths");
            dataLoan.sSoftPrepmtPeriodMonths_rep    = GetString("sSoftPrepmtPeriodMonths");
            dataLoan.sGfeMaxPpmtPenaltyAmt_rep = GetString("sGfeMaxPpmtPenaltyAmt");

            dataLoan.sSecurityCurrentOwn    = GetBool("sSecurityCurrentOwn");
            dataLoan.sSecurityPurch         = GetBool("sSecurityPurch");
            dataLoan.sIOnlyMon_rep          = GetString("sIOnlyMon");

            if (dataLoan.sGfeIsBalloonLckd)
            {
                dataLoan.sGfeIsBalloon = this.GetBool("sGfeIsBalloon");
            }

            dataLoan.sProMIns2Mon_rep       = GetString("sProMIns2Mon");
            dataLoan.sProMInsMon_rep        = GetString("sProMInsMon");
            dataLoan.sProMInsT = (E_PercentBaseT)GetInt("sProMInsT");
            dataLoan.sGradPmtR_rep          = GetString("sGradPmtR");
            dataLoan.sGradPmtYrs_rep        = GetString("sGradPmtYrs");
            dataLoan.sRAdjRoundT            = (E_sRAdjRoundT) GetInt("sRAdjRoundT");
            dataLoan.sRAdjCapMon_rep        = GetString("sRAdjCapMon");
            dataLoan.sRAdj1stCapMon_rep     = GetString("sRAdj1stCapMon");
            dataLoan.sAprIncludesReqDeposit = GetBool("sAprIncludesReqDeposit");
            dataLoan.sOnlyLatePmtEstimate   = GetBool("sOnlyLatePmtEstimate");
            dataLoan.sAsteriskEstimate      = GetBool("sAsteriskEstimate");

            dataLoan.sHasDemandFeature          = GetBool("sHasDemandFeature");
            dataLoan.sHasVarRFeature            = GetBool("sHasVarRFeature");
            dataLoan.sIsPropertyBeingSoldByCreditor = GetBool("sIsPropertyBeingSoldByCreditor");
            dataLoan.sPmtAdjRecastStop_rep      = GetString("sPmtAdjRecastStop");
            dataLoan.sPmtAdjRecastPeriodMon_rep = GetString("sPmtAdjRecastPeriodMon");
            dataLoan.sPmtAdjCapMon_rep          = GetString("sPmtAdjCapMon");
            dataLoan.sBuydwnMon5_rep            = GetString("sBuydwnMon5");
            dataLoan.sBuydwnMon4_rep            = GetString("sBuydwnMon4");
            dataLoan.sBuydwnMon3_rep            = GetString("sBuydwnMon3");
            dataLoan.sBuydwnMon2_rep            = GetString("sBuydwnMon2");
            dataLoan.sBuydwnMon1_rep            = GetString("sBuydwnMon1");
            dataLoan.sLateDays                  = GetString("sLateDays");
            dataLoan.sAssumeLT                  = (E_sAssumeLT) GetInt("sAssumeLT");
            dataLoan.sNoteIR_rep                = GetString("sNoteIR");
            dataLoan.sSchedDueD1Lckd            = GetBool("sSchedDueD1Lckd");
            dataLoan.sSchedDueD1_rep            = GetString("sSchedDueD1");

            dataLoan.sBuydwnR1_rep              = GetString("sBuydwnR1");
            dataLoan.sBuydwnR2_rep              = GetString("sBuydwnR2");
            dataLoan.sBuydwnR3_rep              = GetString("sBuydwnR3");
            dataLoan.sBuydwnR4_rep              = GetString("sBuydwnR4");
            dataLoan.sBuydwnR5_rep              = GetString("sBuydwnR5");
            dataLoan.sProMInsCancelLtv_rep      = GetString("sProMInsCancelLtv");
            dataLoan.sProMInsCancelAppraisalLtv_rep      = GetString("sProMInsCancelAppraisalLtv");
            dataLoan.sFilingF                   = GetString("sFilingF");
            dataLoan.sProMInsR_rep              = GetString("sProMInsR");
            dataLoan.sProMInsR2_rep             = GetString("sProMInsR2");
            if (this.GetString("sRAdjWorstIndex", "") != "")
            {
                dataLoan.sRAdjWorstIndex = GetBool("sRAdjWorstIndex");
            }
            dataLoan.sFinMethDesc               = GetString("sFinMethDesc");

            dataLoan.sPpmtAmt_rep	            = GetString("sPpmtAmt");
            dataLoan.sPpmtMon_rep	            = GetString("sPpmtMon");
            dataLoan.sPpmtStartMon_rep          = GetString("sPpmtStartMon");
            dataLoan.sPpmtOneAmt_rep            = GetString("sPpmtOneAmt");
            dataLoan.sPpmtOneMon_rep            = GetString("sPpmtOneMon");

            dataLoan.sInsReqDesc				= GetString("sInsReqDesc");
            dataLoan.sReqCreditLifeIns			= GetBool("sReqCreditLifeIns");
            dataLoan.sReqCreditDisabilityIns	= GetBool("sReqCreditDisabilityIns");
            dataLoan.sReqPropIns				= GetBool("sReqPropIns");
            dataLoan.sReqFloodIns				= GetBool("sReqFloodIns");
            dataLoan.sIfPurchInsFrCreditor		= GetBool("sIfPurchInsFrCreditor");
            dataLoan.sIfPurchPropInsFrCreditor	= GetBool("sIfPurchPropInsFrCreditor");
            dataLoan.sIfPurchFloodInsFrCreditor = GetBool("sIfPurchFloodInsFrCreditor");
            dataLoan.sInsFrCreditorAmt_rep		= GetString("sInsFrCreditorAmt");
            dataLoan.sProMInsMidptCancel		= GetBool("sProMInsMidptCancel");
            dataLoan.sProMInsCancelMinPmts_rep  = GetString("sProMInsCancelMinPmts");

			dataLoan.sIsOptionArm                         = GetBool("sIsOptionArm");
			dataLoan.sOptionArmMinPayPeriod_rep           = GetString("sOptionArmMinPayPeriod");
			dataLoan.sOptionArmMinPayIsIOOnly             = GetBool("sOptionArmMinPayIsIOOnly");
			dataLoan.sOptionArmIntroductoryPeriod_rep     = GetString("sOptionArmIntroductoryPeriod");
			dataLoan.sOptionArmInitialFixMinPmtPeriod_rep = GetString("sOptionArmInitialFixMinPmtPeriod");
			dataLoan.sOptionArmMinPayOptionT              = (E_sOptionArmMinPayOptionT) GetInt("sOptionArmMinPayOptionT");
			dataLoan.sOptionArmPmtDiscount_rep            = GetString("sOptionArmPmtDiscount");
			dataLoan.sOptionArmNoteIRDiscount_rep         = GetString("sOptionArmNoteIRDiscount");
			dataLoan.sIsFullAmortAfterRecast              = GetBool("sIsFullAmortAfterRecast");
			dataLoan.sOptionArmTeaserR_rep                = GetString("sOptionArmTeaserR");
			dataLoan.sDtiUsingMaxBalPc                    = GetBool("sDtiUsingMaxBalPc");

            dataLoan.sLpTemplateNm = GetString("sLpTemplateNm");

            dataLoan.sDaysInYr_rep = GetString("sDaysInYr");
            dataLoan.sIPerDay_rep = GetString("sIPerDay");
            dataLoan.sIPerDayLckd = GetBool("sIPerDayLckd");

            // From RateFloorPopup
            dataLoan.sRAdjFloorCalcT = GetEnum<RateAdjFloorCalcT>("sRAdjFloorCalcT");
            dataLoan.sRAdjFloorAddR_rep = GetString("sRAdjFloorAddR");
            dataLoan.sRAdjFloorR_rep = GetString("sRAdjFloorR");

            if (dataLoan.sIsUseManualApr)
            {
                dataLoan.sApr_rep = GetString("sApr");
            }

            dataLoan.sArmIndexNameVstr = GetString("sArmIndexNameVstr");

            dataLoan.sInitAPR_rep = GetString("sInitAPR");
            dataLoan.sLastDiscAPR_rep = GetString("sLastDiscAPR");

            dataLoan.sOtherLoanFeaturesDescription = GetString("sOtherLoanFeaturesDescription");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("sRAdj1stCapR", dataLoan.sRAdj1stCapR_rep);
            SetResult("sRAdjCapR", dataLoan.sRAdjCapR_rep);
            SetResult("sRAdjMarginR", dataLoan.sRAdjMarginR_rep);
            SetResult("sRAdjIndexR", dataLoan.sRAdjIndexR_rep);
            
            SetResult("sIsConvertibleMortgage", dataLoan.sIsConvertibleMortgage);
            SetResult("sRAdjRoundToR", dataLoan.sRAdjRoundToR_rep);
            SetResult("sPmtAdjCapR", dataLoan.sPmtAdjCapR_rep);
            SetResult("sProMInsBaseAmt", dataLoan.sProMInsBaseAmt_rep);
            SetResult("sPmtAdjMaxBalPc", dataLoan.sPmtAdjMaxBalPc_rep);
            SetResult("sLateChargePc", dataLoan.sLateChargePc.TrimWhitespaceAndBOM());

            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sFinancedAmt", dataLoan.sFinancedAmt_rep);
            SetResult("sTerm", dataLoan.sTerm_rep);
            SetResult("sDue", dataLoan.sDue_rep);
            SetResult("sProMInsT", dataLoan.sProMInsT);
            SetResult("sFinMethT", dataLoan.sFinMethT);
            SetResult("sFinMethDesc", dataLoan.sFinMethDesc);
            SetResult("sFinCharge", dataLoan.sFinCharge_rep);
            SetResult("sLateChargeBaseDesc", dataLoan.sLateChargeBaseDesc);
            SetResult("sVarRNotes", dataLoan.sVarRNotes);
            SetResult("sPrepmtRefundT", dataLoan.sPrepmtRefundT);
            SetResult("sPrepmtPenaltyT", dataLoan.sPrepmtPenaltyT);

            SetResult("sPrepmtPeriodMonths", dataLoan.sPrepmtPeriodMonths_rep);
            SetResult("sSoftPrepmtPeriodMonths", dataLoan.sSoftPrepmtPeriodMonths_rep);
            SetResult("sGfeMaxPpmtPenaltyAmt", dataLoan.sGfeMaxPpmtPenaltyAmt_rep);

            SetResult("sSecurityCurrentOwn", dataLoan.sSecurityCurrentOwn);
            SetResult("sSecurityPurch", dataLoan.sSecurityPurch);
            SetResult("sSchedPmtNumTot", dataLoan.sSchedPmtNumTot_rep);
            SetResult("sIOnlyMon", dataLoan.sIOnlyMon_rep);
            SetResult("sProMIns2Mon", dataLoan.sProMIns2Mon_rep);
            SetResult("sProMInsMon", dataLoan.sProMInsMon_rep);
            SetResult("sGradPmtR", dataLoan.sGradPmtR_rep);
            SetResult("sGradPmtYrs", dataLoan.sGradPmtYrs_rep);
            SetResult("sRAdjRoundT", dataLoan.sRAdjRoundT);
            SetResult("sRAdjCapMon", dataLoan.sRAdjCapMon_rep);
            SetResult("sRAdj1stCapMon", dataLoan.sRAdj1stCapMon_rep);
            SetResult("sAprIncludesReqDeposit", dataLoan.sAprIncludesReqDeposit);
            SetResult("sOnlyLatePmtEstimate", dataLoan.sOnlyLatePmtEstimate);
            SetResult("sAsteriskEstimate", dataLoan.sAsteriskEstimate);


            //ret["sTilPrepByNm"] =dataLoan.sTilPrepByNm;
            SetResult("sHasDemandFeature", dataLoan.sHasDemandFeature);
            SetResult("sHasVarRFeature", dataLoan.sHasVarRFeature);
            SetResult("sIsPropertyBeingSoldByCreditor", dataLoan.sIsPropertyBeingSoldByCreditor);
            SetResult("sPmtAdjRecastStop", dataLoan.sPmtAdjRecastStop_rep);
            SetResult("sPmtAdjRecastPeriodMon", dataLoan.sPmtAdjRecastPeriodMon_rep);
            SetResult("sPmtAdjCapMon", dataLoan.sPmtAdjCapMon_rep);
            SetResult("sBuydwnMon5", dataLoan.sBuydwnMon5_rep);
            SetResult("sBuydwnMon4", dataLoan.sBuydwnMon4_rep);
            SetResult("sBuydwnMon3", dataLoan.sBuydwnMon3_rep);
            SetResult("sBuydwnMon2", dataLoan.sBuydwnMon2_rep);
            SetResult("sBuydwnMon1", dataLoan.sBuydwnMon1_rep);
            SetResult("sSpFullAddr", dataLoan.sSpFullAddr);
            SetResult("sLateDays", dataLoan.sLateDays.TrimWhitespaceAndBOM());
            SetResult("sAssumeLT", dataLoan.sAssumeLT);
            
            SetResult("sSchedDueD1Lckd", dataLoan.sSchedDueD1Lckd);
            // 9/20/2013 gf - opm 130112 makes sSchedDueD1 a calculated field.
            // We want to supply the auto-calculated field for the 1st pmt date
            // that is NOT in the Pmt Schedule table.
            SetResult("sSchedDueD1", dataLoan.sSchedDueD1_rep);

            SetResult("sBuydwnR1", dataLoan.sBuydwnR1_rep);
            SetResult("sBuydwnR2", dataLoan.sBuydwnR2_rep);
            SetResult("sBuydwnR3", dataLoan.sBuydwnR3_rep);
            SetResult("sBuydwnR4", dataLoan.sBuydwnR4_rep);
            SetResult("sBuydwnR5", dataLoan.sBuydwnR5_rep);
            SetResult("sProMInsCancelLtv", dataLoan.sProMInsCancelLtv_rep);
            SetResult("sProMInsCancelAppraisalLtv", dataLoan.sProMInsCancelAppraisalLtv_rep);
            SetResult("sFilingF", dataLoan.sFilingF.TrimWhitespaceAndBOM());

            SetResult("sProMInsR", dataLoan.sProMInsR_rep);
            SetResult("sProMIns", dataLoan.sProMIns_rep);
            SetResult("sProMInsR2", dataLoan.sProMInsR2_rep);
            SetResult("sProMIns2", dataLoan.sProMIns2_rep);

            SetResult("sSchedPmtTot", dataLoan.sSchedPmtTot_rep);
            SetResult("sApr", dataLoan.sApr_rep);
            SetResult("sAprIncludedCc", dataLoan.sAprIncludedCc_rep);
            SetResult("sRAdjWorstIndex", dataLoan.sRAdjWorstIndex);

            SetResult("sPpmtAmt", dataLoan.sPpmtAmt_rep);
            SetResult("sPpmtMon", dataLoan.sPpmtMon_rep);
            SetResult("sPpmtStartMon", dataLoan.sPpmtStartMon_rep);
            SetResult("sPpmtOneAmt", dataLoan.sPpmtOneAmt_rep);
            SetResult("sPpmtOneMon", dataLoan.sPpmtOneMon_rep);

			SetResult("sIsOptionArm", dataLoan.sIsOptionArm);
			SetResult("sOptionArmMinPayPeriod", dataLoan.sOptionArmMinPayPeriod_rep);
			SetResult("sOptionArmMinPayIsIOOnly", dataLoan.sOptionArmMinPayIsIOOnly);
			SetResult("sOptionArmIntroductoryPeriod", dataLoan.sOptionArmIntroductoryPeriod_rep);
			SetResult("sOptionArmInitialFixMinPmtPeriod", dataLoan.sOptionArmInitialFixMinPmtPeriod_rep);
			SetResult("sOptionArmMinPayOptionT", dataLoan.sOptionArmMinPayOptionT);
			SetResult("sOptionArmPmtDiscount", dataLoan.sOptionArmPmtDiscount_rep);
			SetResult("sOptionArmNoteIRDiscount", dataLoan.sOptionArmNoteIRDiscount_rep);
			SetResult("sIsFullAmortAfterRecast", dataLoan.sIsFullAmortAfterRecast);
			SetResult("sOptionArmTeaserR", dataLoan.sOptionArmTeaserR_rep);
			SetResult("sDtiUsingMaxBalPc", dataLoan.sDtiUsingMaxBalPc);

            for (int i = 0; i < 10; i++) 
            {
                SetResult("sSchedIR" + (i + 1), dataLoan.sSchedIR_Rep(i, PmtInfoType.InterestRate));
                // 9/20/2013 gf - opm 130112 made sSchedDueD1 a calculated field.
                // Assign the first pmt date of the Pmt Schedule (sSchedDueD12) 
                // using sSchedIR_rep() because using sSchedDueD1 will populate 
                // a date even if there aren't any pmts.
                var dueDFieldNm = i == 0 ? "sSchedDueD12" : "sSchedDueD" + (i + 1);
                SetResult(dueDFieldNm, dataLoan.sSchedIR_Rep(i, PmtInfoType.DueDate));
                SetResult("sSchedPmtNum" + (i + 1), dataLoan.sSchedIR_Rep(i, PmtInfoType.PmtNumber));
                SetResult("sSchedPmt" + (i + 1), dataLoan.sSchedIR_Rep(i, PmtInfoType.PmtAmount));
                SetResult("sSchedBal" + (i + 1), dataLoan.sSchedIR_Rep(i, PmtInfoType.Bal));
            }

            SetResult("sInsReqDesc", dataLoan.sInsReqDesc);
            SetResult("sReqCreditLifeIns", dataLoan.sReqCreditLifeIns);
            SetResult("sReqCreditDisabilityIns", dataLoan.sReqCreditDisabilityIns);
            SetResult("sReqPropIns", dataLoan.sReqPropIns);
            SetResult("sReqFloodIns", dataLoan.sReqFloodIns);
            SetResult("sIfPurchInsFrCreditor", dataLoan.sIfPurchInsFrCreditor);
            SetResult("sIfPurchPropInsFrCreditor", dataLoan.sIfPurchPropInsFrCreditor);
            SetResult("sIfPurchFloodInsFrCreditor", dataLoan.sIfPurchFloodInsFrCreditor);
            SetResult("sInsFrCreditorAmt", dataLoan.sInsFrCreditorAmt_rep);
            SetResult("sProMInsMidptCancel", dataLoan.sProMInsMidptCancel);
            SetResult("sProMInsCancelMinPmts", dataLoan.sProMInsCancelMinPmts_rep);

            SetResult("sArmIndexNameVstr", dataLoan.sArmIndexNameVstr);
            SetResult("sLpTemplateNm", dataLoan.sLpTemplateNm);

            SetResult("sDaysInYr", dataLoan.sDaysInYr_rep);
            SetResult("sIPerDay", dataLoan.sIPerDay_rep);
            SetResult("sIPerDayLckd", dataLoan.sIPerDayLckd);

            SetResult("sInitAPR", dataLoan.sInitAPR_rep);
            SetResult("sLastDiscAPR", dataLoan.sLastDiscAPR_rep);

            // For RateFloorPopup
            SetResult("sRAdjFloorCalcT", dataLoan.sRAdjFloorCalcT);
            SetResult("sRAdjFloorCalcT_hidden", Tools.GetsRAdjFloorCalcTLabel(dataLoan.sRAdjFloorCalcT));
            SetResult("sRAdjFloorBaseR", dataLoan.sRAdjFloorBaseR_rep);
            SetResult("sRAdjFloorAddR", dataLoan.sRAdjFloorAddR_rep);
            SetResult("sRAdjFloorTotalR", dataLoan.sRAdjFloorTotalR_rep);
            SetResult("sRAdjFloorLifeCapR", dataLoan.sRAdjFloorLifeCapR_rep);
            SetResult("sIsRAdjFloorRReadOnly", dataLoan.sIsRAdjFloorRReadOnly.ToString());
            SetResult("sRAdjFloorR", dataLoan.sRAdjFloorR_rep);
            SetResult("sRAdjFloorR_readonly", dataLoan.sRAdjFloorR_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sNoteIR_popup", dataLoan.sNoteIR_rep);
            SetResult("sRAdjLifeCapR", dataLoan.sRAdjLifeCapR_rep);
            SetResult("sRAdjLifeCapR_popup", dataLoan.sRAdjLifeCapR_rep);

            SetResult("sOtherLoanFeaturesDescription", dataLoan.sOtherLoanFeaturesDescription);
        }

        private void PopulateFromLoanProgramTemplate() 
        {
            Guid lLpTemplateId = GetGuid("lLpTemplateId");
            Guid loanId = GetGuid("loanid");

            CLoanProductData loanProduct = new CLoanProductData(lLpTemplateId);
            loanProduct.InitLoad();

            CPageData dataLoan = ConstructPageDataClass(loanId);
            dataLoan.InitLoad();


            BindData(dataLoan, null);

            #region Bind data from Loan program (exclude term/due and note rate) to CPageData.
            dataLoan.sRAdj1stCapR_rep = loanProduct.lRadj1stCapR_rep;
            dataLoan.sRAdj1stCapMon_rep = loanProduct.lRadj1stCapMon_rep;
            dataLoan.sRAdjCapR_rep = loanProduct.lRAdjCapR_rep;
            dataLoan.sRAdjCapMon_rep = loanProduct.lRAdjCapMon_rep;
            dataLoan.sRAdjLifeCapR_rep = loanProduct.lRAdjLifeCapR_rep;
            dataLoan.sRAdjRoundT = loanProduct.lRAdjRoundT;
            dataLoan.sRAdjRoundToR_rep = loanProduct.lRAdjRoundToR_rep;
            dataLoan.sPmtAdjCapR_rep = loanProduct.lPmtAdjCapR_rep;
            dataLoan.sPmtAdjCapMon_rep = loanProduct.lPmtAdjCapMon_rep;
            dataLoan.sPmtAdjRecastPeriodMon_rep = loanProduct.lPmtAdjRecastPeriodMon_rep;
            dataLoan.sPmtAdjRecastStop_rep = loanProduct.lPmtAdjRecastStop_rep;
            dataLoan.sPmtAdjMaxBalPc_rep = loanProduct.lPmtAdjMaxBalPc_rep;
            dataLoan.sGradPmtYrs_rep = loanProduct.lGradPmtYrs_rep;
            dataLoan.sGradPmtR_rep = loanProduct.lGradPmtR_rep;
            dataLoan.sIOnlyMon_rep = loanProduct.lIOnlyMon_rep;
            dataLoan.sBuydwnR1_rep = loanProduct.lBuydwnR1_rep;
            dataLoan.sBuydwnMon1_rep = loanProduct.lBuydwnMon1_rep;
            dataLoan.sBuydwnR2_rep = loanProduct.lBuydwnR2_rep;
            dataLoan.sBuydwnMon2_rep = loanProduct.lBuydwnMon2_rep;
            dataLoan.sBuydwnR3_rep = loanProduct.lBuydwnR3_rep;
            dataLoan.sBuydwnMon3_rep = loanProduct.lBuydwnMon3_rep;
            dataLoan.sBuydwnR4_rep = loanProduct.lBuydwnR4_rep;
            dataLoan.sBuydwnMon4_rep = loanProduct.lBuydwnMon4_rep;
            dataLoan.sBuydwnR5_rep = loanProduct.lBuydwnR5_rep;
            dataLoan.sBuydwnMon5_rep = loanProduct.lBuydwnMon5_rep;
            dataLoan.sAprIncludesReqDeposit = loanProduct.lAprIncludesReqDeposit;
            dataLoan.sHasDemandFeature = loanProduct.lHasDemandFeature;
            dataLoan.sHasVarRFeature = loanProduct.lHasVarRFeature;
            dataLoan.sVarRNotes = loanProduct.lVarRNotes;
            dataLoan.sFilingF = loanProduct.lFilingF;
            dataLoan.sLateDays = loanProduct.lLateDays;
            dataLoan.sLateChargePc = loanProduct.lLateChargePc;
            dataLoan.sLateChargeBaseDesc = loanProduct.lLateChargeBaseDesc;
            dataLoan.sPrepmtPenaltyT = loanProduct.lPrepmtPenaltyT;
            dataLoan.sPrepmtRefundT = loanProduct.lPrepmtRefundT;
            dataLoan.sArmIndexNameVstr = loanProduct.lArmIndexNameVstr;
            dataLoan.sRAdjIndexR = loanProduct.lRAdjIndexR;
            dataLoan.sRAdjFloorAddR = loanProduct.lRAdjFloorR;
            RateAdjFloorCalcT? floorCalcT = Tools.GetRateAdjFloorCalcFromRateAdjFloorBase(loanProduct.lRAdjFloorBaseT);
            if (floorCalcT.HasValue)
            {
                dataLoan.sRAdjFloorCalcT = floorCalcT.Value;
            }

            // MPTODO: investigate if we should put the prepmtmonths stuff here - loanProduct

            dataLoan.sAssumeLT = loanProduct.lAssumeLT;

            if (!loanProduct.IsLpe && !dataLoan.sIsRateLocked)
            {
                try
                {
                    IRateItem[] rates = loanProduct.GetRawRateSheetWithDeltas();
                    if (rates != null && rates.Length > 0)
                    {
                        dataLoan.sRAdjMarginR = rates[0].Margin;
                    }
                }
                catch (CRateOptionNotFoundException) { }
            }

            #endregion
            LoadData(dataLoan, null);
        }
    }
	/// <summary>
	/// Summary description for LoanTermsService.
	/// </summary>
	public partial class LoanTermsService :LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new LoanTermsServiceItem());
        }
	}
}
