<%@ Page language="c#" Codebehind="TruthInLending.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.TruthInLending" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="Rfp" TagName="RateFloorPopup" Src="~/newlos/Forms/RateFloorPopup.ascx" %>
<%@ Import namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>
<html>
  <head runat="server">
    <title>TruthInLending</title>
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <style type="text/css">
        #InitialApr
        {
            display: inline-block;
            margin-right: 11px;
        }
        #AmortizationTableDiv
        {
            width:630px;
            height:500px;
            display:none;
        }
        #AmortizationTableFrame
        {
            width:100%;
            height:98%;
        }
        .ui-dialog
        {
            border: solid 1px black !important;
        }
        .ui-dialog-titlebar
        {
            background-color:Maroon !important;
            border: 1px solid Maroon !important;
        }
    </style>
  </head>
<body class="RightBackground">
<script type="text/javascript">
<!--
var oRolodex = null;  
function _init() {
    if (null == oRolodex) {
        oRolodex = new cRolodex();
    }

    var sRAdjWorstIndexCheckbox = <%= AspxTools.JsGetElementById(sRAdjWorstIndex) %>;
    if (sRAdjWorstIndexCheckbox) {
        <%= AspxTools.JsGetElementById(sRAdjIndexR) %>.readOnly = sRAdjWorstIndexCheckbox.checked;
    }
    lockField(<%=AspxTools.JsGetElementById(sSchedDueD1Lckd)%>, 'sSchedDueD1');

    f_setAmortizationStatus( false );
    f_setPrepmtPeriodMonthsStatus();
    
    $("#AmortizationTableFrame").attr("src", "../Forms/AmortizationTable.aspx?loanid=" + ML.sLId);

    $("#AmortizationTableFrame").on("load", function() {
        var contents = $("#AmortizationTableFrame").contents();
        var closeBtn = contents.find("input[value='Close']");
        closeBtn.off("click");

        var header = contents.find("h4.page-header");
        header.hide();
        closeBtn.click(closeDialog);
    });

    if (typeof (RateFloorPopupInit) == "function")
    {
        RateFloorPopupInit();
    }
}

function closeDialog() {
    $("#AmortizationTableDiv").dialog("close");
}

function showAmortTable() {
    PolyShouldShowConfirmSave(isDirty(), function(){
      $("#AmortizationTableDiv").dialog({
          width: 630,
          height: 540,
          modal: true,
          title: "Amortization Table",
          resizable: false,
          draggable: false,
      });
    }, saveMe);
}

function updateDisclosedAPR()
{
    location.reload();
}
function f_setPrepmtPeriodMonthsStatus() {
    var ddl = <%= AspxTools.JsGetElementById(sPrepmtPenaltyT) %>;
    var sPrepmtPeriodMonths = <%= AspxTools.JsGetElementById(sPrepmtPeriodMonths) %>;
    var sSoftPrepmtPeriodMonths = <%= AspxTools.JsGetElementById(sSoftPrepmtPeriodMonths) %>;
    if (ddl.options[ddl.selectedIndex].text === "may") {
        sPrepmtPeriodMonths.readOnly = false;
        sSoftPrepmtPeriodMonths.readOnly = false;
    } else {
        sPrepmtPeriodMonths.readOnly = true;
        sPrepmtPeriodMonths.value = "";
        sSoftPrepmtPeriodMonths.readOnly = true;
        sSoftPrepmtPeriodMonths.value = "";
    }
}

var openedDate = <%=AspxTools.JsString(m_openedDate)%>;
function onDateKeyUp(o, event) {
    if (event.keyCode == 79) {
        o.value = openedDate;
        updateDirtyBit(event);
    }
}
function f_setAmortizationStatus()
{
    <% if ( ! IsReadOnly ) { %>
    var scheduleBtn = document.getElementById('AmortizationScheduleBtn');
    var scheduleTbl = document.getElementById('PmtScheduleTable');
    var sIsOptionArm = <%= AspxTools.JsGetElementById(sIsOptionArm) %>;
    var sOptionArmMinPayOptionT = <%= AspxTools.JsGetElementById(sOptionArmMinPayOptionT) %>;
    var sOptionArmNoteIRDiscount = <%= AspxTools.JsGetElementById(sOptionArmNoteIRDiscount) %>;
    var ssOptionArmMinPayOptionT = sOptionArmMinPayOptionT[sOptionArmMinPayOptionT.selectedIndex].value
    var sOptionArmPmtDiscount = <%= AspxTools.JsGetElementById(sOptionArmPmtDiscount) %>;

    sOptionArmMinPayOptionT.disabled = ! sIsOptionArm.checked ;
    sOptionArmPmtDiscount.readOnly = (! sIsOptionArm.checked )  || (sIsOptionArm.checked && ssOptionArmMinPayOptionT != <%=AspxTools.JsString(E_sOptionArmMinPayOptionT.ByDiscountPmt)%>);
    sOptionArmNoteIRDiscount.readOnly = (! sIsOptionArm.checked ) || (sIsOptionArm.checked && ssOptionArmMinPayOptionT != <%=AspxTools.JsString(E_sOptionArmMinPayOptionT.ByDiscountNoteIR)%>);

    <%= AspxTools.JsGetElementById(sOptionArmMinPayPeriod) %>.readOnly = ! sIsOptionArm.checked;
    <%= AspxTools.JsGetElementById(sOptionArmMinPayIsIOOnly) %>.disabled = ! sIsOptionArm.checked;
    <%= AspxTools.JsGetElementById(sOptionArmIntroductoryPeriod) %>.readOnly = ! sIsOptionArm.checked;  
    <%= AspxTools.JsGetElementById(sOptionArmInitialFixMinPmtPeriod) %>.readOnly = ! sIsOptionArm.checked;
    <%= AspxTools.JsGetElementById(sIsFullAmortAfterRecast) %>.disabled = ! sIsOptionArm.checked;
    <%= AspxTools.JsGetElementById(sOptionArmTeaserR) %>.readOnly = (! sIsOptionArm.checked) || <%=AspxTools.JsBool(m_IsRateLocked)%>;
    <% } %>
    /* UNDO COMMENT IF CALCULATION FOR OPTION ARM IS NOT READY.
    if ( sIsOptionArm.checked )
    {
        scheduleBtn.disabled = true;
        scheduleTbl.title = 'This feature is currently not supported for Option ARMs';
    }
    else
    {
        scheduleBtn.disabled = false;
        scheduleTbl.title = '';
    }
    */
  
}
function f_chooseFromTemplate() {
    showModal('/newlos/Forms/TruthInLendingTemplateList.aspx', null, null, null, function(arg){
      if (arg.OK) {
          var args = getAllFormValues(null);
          if(typeof(_postGetAllFormValues) == 'function')
          {
              _postGetAllFormValues(args);
          }
          args["loanid"] = document.getElementById("loanid").value;
          args["applicationid"] = document.getElementById("applicationid").value;    
          args["lLpTemplateId"] = arg.id;
          var result = gService.loanedit.call("PopulateFromLoanProgramTemplate", args);
  
          if (!result.error) {
              populateForm(result.value, null);    
              if(typeof(_postRefreshCalculation) == 'function')
              {
                  _postRefreshCalculation(result.value, null);
              }
              updateDirtyBit();
              _init();
          }
      } 
    },{ hideCloseButton: true });
}

function f_update_sTilRedisclosuresReceivedD() {
    var args = {
        sRedisclosureMethodT : document.getElementById("sRedisclosureMethodT").value,
        sTilRedisclosureD : document.getElementById("sTilRedisclosureD").value,
        loanid : ML.sLId
    }
    
    var result = gService.loanedit.call("CalculateRedisclosuresReceivedD", args);
    
    if (!result.error) {
        if ("True" == result.value.OK) {
            document.getElementById("sTilRedisclosuresReceivedD").value = result.value.sRedisclosuresReceivedD;
            updateDirtyBit();
        }
    } else {
        alert(result.UserMessage);
    }
}

function doAfterDateFormat(e)
{
    refreshCalculation();
}
//-->
</script>

<form id=TruthInLending method=post runat="server">
<div id="AmortizationTableDiv">
    <iframe id="AmortizationTableFrame" src=""></iframe>
</div>
<TABLE class=FormTable id=Table1 cellSpacing=0 cellPadding=0 border=0>
  <TR>
    <TD class=MainRightHeader noWrap>Federal Truth-In-Lending Disclosure Statement</TD></TR>
  <TR>
    <TD noWrap>
      <TABLE class=InsetBorder id=Table12 cellSpacing=0 cellPadding=0 width="99%">
        <TR>
        <td>
        <table id=DisclosureAndRedisclosureDateTable cellSpacing=0 cellPadding=0 border=0 >
        <tr>
            <td>
                <span class=FieldLabel nowrap>
                    Initial TIL Disclosure Date
                    <ml:datetextbox id=sTilInitialDisclosureD runat="server" width="60" preset="date" CssClass="mask" onchange="date_onblur(null, this);" ></ml:datetextbox>
                </span>
                <span class=FieldLabel nowrap>
                    TIL Redisclosure Date
                    <ml:datetextbox id=sTilRedisclosureD runat="server" width="60" preset="date" CssClass="mask" onchange="date_onblur(null, this);" ></ml:datetextbox>
                </span>
                <span class=FieldLabel nowrap>
                    Redisclosure Method
                    <asp:DropDownList ID="sRedisclosureMethodT" runat="server" onchange="f_update_sTilRedisclosuresReceivedD()"></asp:DropDownList>
                </span>
                <span class=FieldLabel nowrap>
                    Redisclosures Received
                    <ml:datetextbox id="sTilRedisclosuresReceivedD" runat="server" width="60" preset="date" CssClass="mask" onchange="date_onblur(null, this);" ></ml:datetextbox>
                </span>
            </td>
            <tr>
                <td>
                    <span class="FieldLabel" id="InitialApr">
                        Initial APR
                            <ml:PercentTextBox ID="sInitAPR" runat="server" preset="percent"></ml:PercentTextBox>
                    </span>
                    <span class="FieldLabel">
                        Last Disclosed APR
                            <ml:PercentTextBox ID="sLastDiscAPR" runat="server" preset="percent"></ml:PercentTextBox>
                    </span>
                </td>
            </tr>
        </tr>
        </table>
        </td>
        </TR>
      </TABLE>
    </TD>
  </TR>
  <TR>
    <TD noWrap>
      <TABLE class=InsetBorder id=Table13 cellSpacing=0 cellPadding=0 width="99%" border=0>
        <TR>
          <TD>
            <TABLE id=Table15 cellSpacing=0 cellPadding=0 border=0>
              <tr>
				<td class=FieldLabel nowrap></td>
                <td nowrap>
                	<uc:CFM ID="CFM1" runat="server" />
                </td>
              </tr>
              <tr>
                <td class=FieldLabel>Prepared By</td>
                <td colSpan=3><asp:textbox id=TilCompanyName runat="server" Width="255px"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel>Address</td>
                <td colSpan=3><asp:textbox id=TilStreetAddr runat="server" Width="253px"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel></td>
                <td colSpan=3><asp:textbox id=TilCity runat="server"></asp:textbox><ml:statedropdownlist id=TilState runat="server"></ml:statedropdownlist><ml:zipcodetextbox id=TilZip runat="server" width="50" preset="zipcode"></ml:zipcodetextbox></td></tr>
              <tr>
                <td class=FieldLabel>Phone</td>
                <td class=FieldLabel colSpan=3><ml:phonetextbox id=TilPhoneOfCompany runat="server" width="120" preset="phone"></ml:phonetextbox>&nbsp; 
                  Fax <ml:phonetextbox id=TilFaxOfCompany runat="server" width="120" preset="phone"></ml:phonetextbox></td></tr>
              <TR runat="server" id="trPreparedDate" visible="true">
                <TD class=FieldLabel>Prepared Date</TD>
                <TD colSpan=3><ml:datetextbox id=TilPrepareDate onkeyup="onDateKeyUp(this, event);" runat="server" width="75" preset="date" CssClass="mask" onchange="date_onblur(null, this);" ></ml:datetextbox></TD></TR>
              <TR>
                <TD class=FieldLabel>Total Loan Amt</TD>
                <TD><ml:moneytextbox id=sFinalLAmt runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD>
                <TD class=FieldLabel style="PADDING-LEFT:10px">Interest Rate</TD>
                <TD><ml:percenttextbox id=sNoteIR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></TD></TR>
              <TR>
                <TD class=FieldLabel>Amort Type</TD>
                <TD><asp:dropdownlist id=sFinMethT runat="server" onchange="refreshCalculation();">
						</asp:dropdownlist></TD>
                <TD class=FieldLabel style="PADDING-LEFT:10px">Term/Due</TD>
                <TD><asp:textbox id=sTerm runat="server" Width="38px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox> / <asp:textbox id=sDue runat="server" Width="38px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox></TD></TR>
              <TR>
                <TD class=FieldLabel>Amort Desc</TD>
                <TD><asp:textbox id=sFinMethDesc runat="server" MaxLength="50"></asp:textbox></TD>
                <TD class=FieldLabel style="PADDING-LEFT:10px; padding-right: 5px;">
                    <label>1st Payment Date</label>
                    <asp:CheckBox runat="server" ID="sSchedDueD1Lckd" onclick="lockField(this, 'sSchedDueD1'); refreshCalculation();"/>
                </TD>
                <TD><ml:datetextbox id=sSchedDueD1 runat="server" width="75" preset="date" CssClass="mask" onchange="date_onblur(null, this);" ></ml:datetextbox></TD></TR>
              </TABLE></TD></TR></TABLE></TD></TR>
  <TR>
    <TD noWrap><INPUT type=button value="Populate from loan program template ..." onclick="f_chooseFromTemplate();" style="WIDTH: 229px"></TD></TR>
  <TR>
    <TD noWrap>
      <table id=Table14 cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TR>
          <TD>
            <TABLE id=Table1 width="100%" border=0>
              <TR>
                <TD vAlign=top>
                  <TABLE id=Table4 cellSpacing=0 cellPadding=0 border=0>
                    <TR>
                      <TD>
                        <TABLE class=InsetBorder id=Table3 cellSpacing=0 cellPadding=0 width=190 border=0>
                          <tr>
                            <td>
                              <table cellSpacing=0 cellPadding=0> <%-- It's tables all the way down! --%>
                                <TR>
                                <td class=FormTableSubHeader colSpan=2 >Adjustable Rate 
                                Mortgage</td></TR>
                                <tr>
                                <td class=FieldLabel colSpan=2 
                                >Rate Adjustment</td></tr>
                                <tr>
                                <td style="WIDTH: 113px" 
                                >1st Adj Cap</td>
                                <td><ml:percenttextbox id=sRAdj1stCapR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
                                <tr>
                                <td style="WIDTH: 113px" 
                                >1st Change</td>
                                <td><asp:textbox id=sRAdj1stCapMon runat="server" Width="42px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox>&nbsp;mths</td></tr>
                                <tr>
                                <td style="WIDTH: 113px" 
                                >Adj Cap</td>
                                <td><ml:percenttextbox id=sRAdjCapR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
                                <tr>
                                <td style="WIDTH: 113px" 
                                >Adj Period</td>
                                <td><asp:textbox id=sRAdjCapMon runat="server" Width="42px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox> mths</td></tr>
                                <tr>
                                <td style="WIDTH: 113px" 
                                >Life Adj Cap</td>
                                <td><ml:percenttextbox id=sRAdjLifeCapR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
                                <tr>
                                <td style="WIDTH: 113px" >Margin</td>
                                <td><ml:percenttextbox id=sRAdjMarginR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
                                <tr>
                                <td style="WIDTH: 113px">Index</td>
                                <td noWrap><asp:CheckBox id=sRAdjWorstIndex runat="server" Text="Worst Case" onclick="refreshCalculation();"></asp:CheckBox></td></tr>
                                <tr>
                                <td><asp:Textbox ToolTip="ARM Index Name" runat="server" ID="sArmIndexNameVstr" onchange="refreshCalculation();"></asp:Textbox></td>
                                <td><ml:percenttextbox id=sRAdjIndexR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
                                <tr>
                                    <td style="WIDTH: 140px" nowrap>
                                        Rate Floor
                                        <a href="javascript:void(0);" id="RateFloorCalcLink">calculate</a>
                                    </td>
                                    <td>
                                        <ml:percenttextbox data-field-id="sRAdjFloorR" id="sRAdjFloorR_readonly" ReadOnly="true" runat="server" width="70" preset="percent"></ml:percenttextbox>
                                    </td>
                                </tr>
                                <tr>
                                <td style="WIDTH: 113px" 
                                >Round&nbsp; <asp:dropdownlist id=sRAdjRoundT runat="server" onchange="refreshCalculation();"></asp:dropdownlist></td>
                                <td><ml:percenttextbox id=sRAdjRoundToR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
                                <td>Convertible Mortgage</td>
                                <td><asp:CheckBox runat="server" id="sIsConvertibleMortgage" onclick="refreshCalculation();" /></td>
                                </tr>
                                
                                </table></td></tr></TABLE></TD></TR>
                    <tr>
                      <td>
                        <table class=InsetBorder id=Table11 cellSpacing=0 
                        cellPadding=0 width=190 border=0>
                          <tr>
                            <td class=FormTablesubheader colSpan=2 
                            >ARM Potential Negative 
                            Amort.</td></tr>
                          <tr>
                            <td class=FieldLabel colSpan=2 
                              >Payment Adjustment</td></tr>
                          <tr>
                            <td>Adj Cap</td>
                            <td><ml:percenttextbox id=sPmtAdjCapR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
                          <tr>
                            <td>Adj Period</td>
                            <td><asp:textbox id=sPmtAdjCapMon runat="server" Width="42px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox>&nbsp;mths</td></tr>
                          <tr>
                            <td>Recast Pd</td>
                            <td><asp:textbox id=sPmtAdjRecastPeriodMon runat="server" Width="42px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox>&nbsp;mths</td></tr>
                          <tr>
                            <td>Recast Stop</td>
                            <td><asp:textbox id=sPmtAdjRecastStop runat="server" Width="42px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox>&nbsp;mths</td></tr>
                          <tr>
                            <td>Max Bal</td>
                            <td><ml:percenttextbox id=sPmtAdjMaxBalPc runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
                          <tr>
                            <td>Use Max Bal for DTI</td>
                            <td><asp:CheckBox runat="server" id="sDtiUsingMaxBalPc" onclick="refreshCalculation();" /></td></tr>
                      </table></td></tr></TABLE></TD>
                <td vAlign=top align=left width="100%">
                  <TABLE class=InsetBorder id=Table10 cellSpacing=0 
                  cellPadding=0 width="99%" border=0>
                    <TR>
                      <TD noWrap>
                        <TABLE id="PmtScheduleTable" cellSpacing=0 cellPadding=0 border=0>
                          <TR>
                            <TD class=FormTableSubheader noWrap colSpan=3>Pmt Schedule</TD>
                            <TD class=FormTableSubheader noWrap align=right colSpan=3>
                                <span title="APR-related closing costs">APR-related CC</span>
                                :
                                <ml:moneytextbox id=sAprIncludedCc runat="server" width="78px" preset="money" ReadOnly="True"></ml:moneytextbox>
                                &nbsp;
                                APR:
                                <ml:percenttextbox id=sApr runat="server" width="60px" preset="percent" ReadOnly="True"></ml:percenttextbox>
                            </TD></TR>
                          <TR>
                            <TD noWrap align=right colSpan=2>
                                # of Pmts
                            </TD>
                            <TD align=center>Pmt Date</TD>
                            <TD nowrap>Interest Rate</TD>
                            <TD noWrap>Monthly Payment</TD>
                            <TD>Balance</TD></TR>
                          <TR>
                            <TD></TD>
                            <TD><asp:textbox id=sSchedPmtNum1 runat="server" Width="35" ReadOnly="True"></asp:textbox></TD>
                            <TD><asp:textbox id=sSchedDueD12 runat="server" Width="62" ReadOnly="True"></asp:textbox></TD>
                            <TD><ml:percenttextbox id=sSchedIR1 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></TD>
                            <TD><ml:moneytextbox id=sSchedPmt1 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD>
                            <TD><ml:moneytextbox id=sSchedBal1 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD></TD>
                            <TD><asp:textbox id=sSchedPmtNum2 runat="server" Width="35px" ReadOnly="True"></asp:textbox></TD>
                            <TD><asp:textbox id=sSchedDueD2 runat="server" Width="62px" ReadOnly="True"></asp:textbox></TD>
                            <TD><ml:percenttextbox id=sSchedIR2 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></TD>
                            <TD><ml:moneytextbox id=sSchedPmt2 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD>
                            <TD><ml:moneytextbox id=sSchedBal2 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD></TD>
                            <TD><asp:textbox id=sSchedPmtNum3 runat="server" Width="35px" ReadOnly="True"></asp:textbox></TD>
                            <TD><asp:textbox id=sSchedDueD3 runat="server" Width="62px" ReadOnly="True"></asp:textbox></TD>
                            <TD><ml:percenttextbox id=sSchedIR3 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></TD>
                            <TD><ml:moneytextbox id=sSchedPmt3 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD>
                            <TD><ml:moneytextbox id=sSchedBal3 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD></TD>
                            <TD><asp:textbox id=sSchedPmtNum4 runat="server" Width="35px" ReadOnly="True"></asp:textbox></TD>
                            <TD><asp:textbox id=sSchedDueD4 runat="server" Width="62px" ReadOnly="True"></asp:textbox></TD>
                            <TD><ml:percenttextbox id=sSchedIR4 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></TD>
                            <TD><ml:moneytextbox id=sSchedPmt4 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD>
                            <TD><ml:moneytextbox id=sSchedBal4 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD></TD>
                            <TD><asp:textbox id=sSchedPmtNum5 runat="server" Width="35px" ReadOnly="True"></asp:textbox></TD>
                            <TD><asp:textbox id=sSchedDueD5 runat="server" Width="62px" ReadOnly="True"></asp:textbox></TD>
                            <TD><ml:percenttextbox id=sSchedIR5 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></TD>
                            <TD><ml:moneytextbox id=sSchedPmt5 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD>
                            <TD><ml:moneytextbox id=sSchedBal5 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD></TD>
                            <TD><asp:textbox id=sSchedPmtNum6 runat="server" Width="35px" ReadOnly="True"></asp:textbox></TD>
                            <TD><asp:textbox id=sSchedDueD6 runat="server" Width="62px" ReadOnly="True"></asp:textbox></TD>
                            <TD><ml:percenttextbox id=sSchedIR6 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></TD>
                            <TD><ml:moneytextbox id=sSchedPmt6 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD>
                            <TD><ml:moneytextbox id=sSchedBal6 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD></TD>
                            <TD><asp:textbox id=sSchedPmtNum7 runat="server" Width="35px" ReadOnly="True"></asp:textbox></TD>
                            <TD><asp:textbox id=sSchedDueD7 runat="server" Width="62px" ReadOnly="True"></asp:textbox></TD>
                            <TD><ml:percenttextbox id=sSchedIR7 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></TD>
                            <TD><ml:moneytextbox id=sSchedPmt7 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD>
                            <TD><ml:moneytextbox id=sSchedBal7 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD></TD>
                            <TD><asp:textbox id=sSchedPmtNum8 runat="server" Width="35px" ReadOnly="True"></asp:textbox></TD>
                            <TD><asp:textbox id=sSchedDueD8 runat="server" Width="62px" ReadOnly="True"></asp:textbox></TD>
                            <TD><ml:percenttextbox id=sSchedIR8 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></TD>
                            <TD><ml:moneytextbox id=sSchedPmt8 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD>
                            <TD><ml:moneytextbox id=sSchedBal8 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD></TD>
                            <TD><asp:textbox id=sSchedPmtNum9 runat="server" Width="35px" ReadOnly="True"></asp:textbox></TD>
                            <TD><asp:textbox id=sSchedDueD9 runat="server" Width="62px" ReadOnly="True"></asp:textbox></TD>
                            <TD><ml:percenttextbox id=sSchedIR9 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></TD>
                            <TD><ml:moneytextbox id=sSchedPmt9 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD>
                            <TD><ml:moneytextbox id=sSchedBal9 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD></TD>
                            <TD><asp:textbox id=sSchedPmtNum10 runat="server" Width="35px" ReadOnly="True"></asp:textbox></TD>
                            <TD><asp:textbox id=sSchedDueD10 runat="server" Width="62px" ReadOnly="True"></asp:textbox></TD>
                            <TD><ml:percenttextbox id=sSchedIR10 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></TD>
                            <TD><ml:moneytextbox id=sSchedPmt10 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD>
                            <TD><ml:moneytextbox id=sSchedBal10 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD>Total&nbsp;</TD>
                            <TD><asp:textbox id=sSchedPmtNumTot runat="server" Width="35px" ReadOnly="True" Height="20px"></asp:textbox></TD>
                            <TD align=right colSpan=2 
                              >Total of Payments&nbsp;</TD>
                            <TD><ml:moneytextbox id=sSchedPmtTot runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD>
                            <TD></TD></TR>
                          <TR>
                            <TD colSpan=4>Amount Financed <ml:moneytextbox id=sFinancedAmt runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD>
                            <TD align=right>Finance Charge&nbsp;</TD>
                            <TD><ml:moneytextbox id=sFinCharge runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD></TR>
                            
                          <TR>
                            <TD colSpan=4><INPUT id="AmortizationScheduleBtn" type=button value="View Amortization Schedule" NoHighlight onclick="showAmortTable();" runat="server"></TD>
                            <TD></TD>
                            <TD></TD></TR></TABLE></TD></TR></TABLE>
                  <table>
                    <tr>
                      <td>
                        <table class=InsetBorder id=Table7 cellSpacing=0 
                        cellPadding=0 width=181 border=0>
                          <TR>
                            <TD class=FormTableSubheader colSpan=2 
                            >Graduated Payment Mortgage</TD></TR>
                          <TR>
                            <TD style="WIDTH: 16px" 
                            >Years</TD>
                            <TD style="WIDTH: 45px"><asp:textbox id=sGradPmtYrs runat="server" Width="81px" onchange="refreshCalculation();" MaxLength="4" Height="22px"></asp:textbox></TD></TR>
                          <TR>
                            <TD style="WIDTH: 16px" 
                            >Rate</TD>
                            <TD style="WIDTH: 45px"><ml:percenttextbox id=sGradPmtR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></TD></TR></table></td>
                      <td vAlign=top>
                        <table class=InsetBorder id=Table9 cellSpacing=0 
                        cellPadding=0 width=181 border=0>
                          <TR>
                            <TD class=FormTablesubheader colSpan=2 
                            >Interest Only</TD></TR>
                          <TR>
                            <TD>Months</TD>
                            <TD noWrap><asp:textbox id=sIOnlyMon runat="server" Width="36" onchange="refreshCalculation();" MaxLength="3"></asp:textbox>&nbsp;mths</TD></TR></table></td></tr></table></td></TR></TABLE>
            <table id=Table5 cellSpacing=0 cellPadding=0 width="100%" border=0 
            >
            <tr><td colspan="2">
              <table class=InsetBorder id=Table2 cellSpacing=0 cellPadding=0 border=0>
                <tr><td class="FormTablesubheader" colspan="4">Option ARM</td></tr>
                <tr>
                  <td>Option ARM</td>
                  <td><asp:CheckBox id="sIsOptionArm" Runat="server" onclick="refreshCalculation();" /></td>
                  <td></td>
                  <td></td>
                <tr>
                  <td>Minimum Payment Period</td>
                  <td><asp:TextBox runat="server" id="sOptionArmMinPayPeriod" onchange="refreshCalculation();" Width="35px" />&nbsp;mths</td>
                  <td style="PADDING-LEFT: 12px; WHITE-SPACE: nowrap">Minimum Payment Calculation Method</td>
                  <td><asp:DropDownList runat="server" id="sOptionArmMinPayOptionT" onchange="refreshCalculation();"></asp:DropDownList></td>
                </tr>
                <tr>
                  <td>Initial Fixed Rate Period</td>
                  <td><asp:TextBox runat="server" id="sOptionArmIntroductoryPeriod" onchange="refreshCalculation();" Width="35px" />&nbsp;mths</td>
                  <td style="PADDING-LEFT: 12px">Teaser Rate</td>
                  <td><ml:percenttextbox runat="server" id="sOptionArmTeaserR" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td>
                </tr>
                <tr>
                  <td style="WHITE-SPACE: nowrap">Initial Fixed Minimum Payment</td>
                  <td><asp:TextBox runat="server" id="sOptionArmInitialFixMinPmtPeriod" onchange="refreshCalculation();" Width="35px" />&nbsp;mths</td>
                  <td style="PADDING-LEFT: 12px">Discount from Note Rate</td>
                  <td><ml:percenttextbox runat="server" id="sOptionArmNoteIRDiscount" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td>
                </tr>
                 <tr>
                  <td style="WHITE-SPACE: nowrap">Is Minimum Payment I/O Only?</td>
                  <td><asp:CheckBox runat="server" id="sOptionArmMinPayIsIOOnly" onclick="refreshCalculation();" /></td>
                  <td style="PADDING-LEFT: 12px; WHITE-SPACE: nowrap">Discount from Fully Amortizing Payment</td>
                  <td><ml:percenttextbox runat="server" id="sOptionArmPmtDiscount" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td>
                </tr>
                <tr>
                  <td>Fully Amortizing at Recast</td>
                  <td><asp:CheckBox runat="server" id="sIsFullAmortAfterRecast" onclick="refreshCalculation();" /></td>
                  <td></td>
                  <td></td>
                </tr>
              </table>
            </td></tr>
              <tr>
                <td vAlign=top>
                  <TABLE class=InsetBorder id=Table2 cellSpacing=0 cellPadding=0 
                  border=0>
                    <TR>
                      <TD class=FormTablesubheader noWrap colSpan=3 
                      >Buydown Mortgage</TD></TR>
                    <TR>
                      <TD noWrap></TD>
                      <TD noWrap>Rate Reduction</TD>
                      <TD noWrap>Term (mths)</TD></TR>
                    <TR>
                      <TD noWrap>1</TD>
                      <TD noWrap><ml:percenttextbox id=sBuydwnR1 runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></TD>
                      <TD noWrap><asp:textbox id=sBuydwnMon1 runat="server" Width="36px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox></TD></TR>
                    <TR>
                      <TD noWrap>2</TD>
                      <TD noWrap><ml:percenttextbox id=sBuydwnR2 runat="server" width="70" preset="percent" onchange="refreshCalculation();" DESIGNTIMEDRAGDROP="3491"></ml:percenttextbox></TD>
                      <TD noWrap><asp:textbox id=sBuydwnMon2 runat="server" Width="36px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox></TD></TR>
                    <TR>
                      <TD noWrap>3</TD>
                      <TD noWrap><ml:percenttextbox id=sBuydwnR3 runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></TD>
                      <TD noWrap><asp:textbox id=sBuydwnMon3 runat="server" Width="36px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox></TD></TR>
                    <TR>
                      <TD noWrap>4</TD>
                      <TD noWrap><ml:percenttextbox id=sBuydwnR4 runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></TD>
                      <TD noWrap><asp:textbox id=sBuydwnMon4 runat="server" Width="36px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox></TD></TR>
                    <TR>
                      <TD noWrap>5</TD>
                      <TD noWrap><ml:percenttextbox id=sBuydwnR5 runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></TD>
                      <TD noWrap><asp:textbox id=sBuydwnMon5 runat="server" Width="36px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox></TD></TR>
                    <TR>
                      <TD noWrap></TD>
                      <TD noWrap></TD>
                      <TD noWrap></TD></TR></TABLE></td>
                <td vAlign=top width="100%">
                  <table class=InsetBorder id=Table8 cellSpacing=0 cellPadding=0 
                  border=0>
                    <tr>
                      <td class=FormTablesubheader colSpan=6 
                      >Mortgage Insurance</td></tr>
                    <tr>
                      <td style="WIDTH: 106px">Base 
                      type:</td>
                      <td>Base amount:</td>
                      <td style="WIDTH: 19px"></td>
                      <td>Rate %</td>
                      <td>Amount</td>
                      <td>Term</td></tr>
                    <tr>
                      <td style="WIDTH: 106px"><asp:dropdownlist id=sProMInsT runat="server" onchange="refreshCalculation();" Height="25px">
									</asp:dropdownlist></td>
                      <td><ml:moneytextbox id=sProMInsBaseAmt runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                      <td style="WIDTH: 19px">1</td>
                      <td><ml:percenttextbox id=sProMInsR runat="server" width="61px" preset="percent" onchange="refreshCalculation();" decimalDigits="6"></ml:percenttextbox></td>
                      <td><ml:moneytextbox id=sProMIns runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                      <td><asp:textbox id=sProMInsMon runat="server" Width="36px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox></td></tr>
                    <tr>
                      <td style="WIDTH: 106px"></td>
                      <td></td>
                      <td style="WIDTH: 19px">2</td>
                      <td><ml:percenttextbox id=sProMInsR2 runat="server" width="61px" preset="percent" onchange="refreshCalculation();"  decimalDigits="6" ></ml:percenttextbox></td>
                      <td><ml:moneytextbox id=sProMIns2 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
                      <td><asp:textbox id=sProMIns2Mon runat="server" Width="36px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox></td></tr>
                    <tr>
                      <td style="WIDTH: 237px" colSpan=3 
                        >Cancel at LTV&nbsp;<ml:percenttextbox id=sProMInsCancelLtv runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox>&nbsp;</td>
                      <td colSpan=3><INPUT 
                        id=sProMInsMidptCancel onclick=refreshCalculation(); 
                        type=checkbox runat="server">Cancel 
                        at midpoint</td></tr>
                        <tr><td colspan="5">Minimum number of payments before cancellation&nbsp;&nbsp;<asp:textbox id="sProMInsCancelMinPmts" runat="server" Width="34px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox>
                        
                        </td>
                        
                        </tr></table>
                  <table class=InsetBorder id=Table6 cellSpacing=0 cellPadding=0 border=0>
                    <TR>
                      <TD class=FormTableSubheader colSpan=2>Prepayment</TD>
                      <TD class=FormTableSubheader></TD>
                      <TD class=FormTableSubheader></TD>
                      <TD class=FormTableSubheader></TD>
                      <TD class=FormTableSubheader></TD></TR>
                    <TR>
                      <TD noWrap>Extra Amt</TD>
                      <TD style="WIDTH: 45px"><ml:moneytextbox id=sPpmtAmt runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></TD>
                      <TD>every</TD>
                      <TD noWrap><asp:textbox id=sPpmtMon runat="server" Width="35px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox> mths</TD>
                      <TD noWrap>start after</TD>
                      <TD noWrap><asp:textbox id=sPpmtStartMon runat="server" Width="34px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox> mths</TD></TR>
                    <TR>
                      <TD noWrap colSpan=3>One Time 
                        Prepayment</TD>
                      <TD style="WIDTH: 45px"><ml:moneytextbox id=sPpmtOneAmt runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></TD>
                      <TD style="WIDTH: 45px">after</TD>
                      <TD><asp:textbox id=sPpmtOneMon runat="server" Width="33px" onchange="refreshCalculation();" MaxLength="3"></asp:textbox> mths</TD></TR></table></td></tr></table></TD></TR>
        <tr>
          <td>
            <table id=Table16 cellSpacing=0 cellPadding=0 width="100%" border=0 
            >
              <tr>
                <td><asp:checkbox id=sAprIncludesReqDeposit runat="server" Text="REQUIRED DEPOSIT:&nbsp; The annual percentage rate does not take into account your required deposit."></asp:checkbox></td></tr>
              <tr>
                <td><asp:checkbox id=sHasDemandFeature runat="server" DESIGNTIMEDRAGDROP="88" Text="DEMAND FEATURE:&nbsp; This obligation has a demand feature."></asp:checkbox></td></tr>
              <tr>
                <td><asp:CheckBox ID="sIsPropertyBeingSoldByCreditor" runat="server" Text="CREDIT SALE: The property for this loan is being sold by the creditor." /></td></tr>
              <tr>
                <td><asp:checkbox id=sHasVarRFeature runat="server" DESIGNTIMEDRAGDROP="87" Text="VARIABLE RATE FEATURE:&nbsp; This loan contains a variable rate feature.&nbsp; A variable rate disclosure has been provided earlier."></asp:checkbox></td></tr>
              <tr>
                <td><asp:textbox id=sVarRNotes runat="server" Width="531px" MaxLength="150" DESIGNTIMEDRAGDROP="398" Rows="3" TextMode="MultiLine"></asp:textbox></td></tr></table></td></tr>
        <tr>
          <td>
            <TABLE class=InsetBorder id=Table18 cellSpacing=0 cellPadding=0 
            width="99%" border=0>
              <TR>
                <TD noWrap>
                  <TABLE id=Table17 cellSpacing=0 cellPadding=0 width="100%" 
                  border=0>
                    <TR>
                      <TD class=FieldLabel vAlign=top noWrap >Insurance</TD>
                      <TD>The following insurance is 
                        required to obtain credit:&nbsp; <asp:textbox id=sInsReqDesc runat="server" Width="340px" MaxLength="21"></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel></TD>
                      <TD><asp:checkbox id=sReqCreditLifeIns runat="server" Text="Credit life insurance"></asp:checkbox><asp:checkbox id=sReqCreditDisabilityIns runat="server" Text="Credit disability"></asp:checkbox><asp:checkbox id=sReqPropIns runat="server" Text="Property insurance"></asp:checkbox><asp:checkbox id=sReqFloodIns runat="server" Text="Flood insurance"></asp:checkbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel></TD>
                      <TD>
                        You may obtain the insurance 
                        from anyone you want that is acceptable to creditor</TD></TR>
                    <TR>
                      <TD class=FieldLabel></TD>
                      <TD><asp:checkbox id=sIfPurchInsFrCreditor runat="server" Text="If you purchase"></asp:checkbox><asp:checkbox id=sIfPurchPropInsFrCreditor runat="server" Text="property"></asp:checkbox><asp:checkbox id=sIfPurchFloodInsFrCreditor runat="server" Text="flood insurance from creditor you will pay"></asp:checkbox>&nbsp;<ml:moneytextbox id=sInsFrCreditorAmt runat="server" width="90" preset="money"></ml:moneytextbox>&nbsp;for 
                        a one year term.</TD></TR>
                    <TR>
                      <TD class=FieldLabel vAlign=top noWrap >Security</TD>
                      <TD>You are giving a security 
                        interest in:&nbsp; <asp:textbox id=sSpFullAddr runat="server" Width="491px" ReadOnly="True"></asp:textbox></TD></TR>
                    <TR>
                      <TD></TD>
                      <TD><asp:checkbox id=sSecurityPurch runat="server" Text="The goods or property being purchased"></asp:checkbox>&nbsp;&nbsp; 
<asp:checkbox id=sSecurityCurrentOwn runat="server" DESIGNTIMEDRAGDROP="387" Text="Real property you already own."></asp:checkbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel vAlign=top noWrap >Filing Fees</TD>
                      <TD><ml:moneytextbox id=sFilingF runat="server" width="90" preset="money"></ml:moneytextbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel vAlign=top noWrap >Late Charges</TD>
                      <TD>If your payment is more than <asp:textbox id=sLateDays runat="server" Width="43px" MaxLength="4" DESIGNTIMEDRAGDROP="17"></asp:textbox>&nbsp;days 
                        late, you will be charged&nbsp;<ml:percenttextbox id=sLateChargePc runat="server" width="70" preset="percent"></ml:percenttextbox>
                        of <asp:textbox id=sLateChargeBaseDesc runat="server" Width="128px" MaxLength="36"></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel vAlign=top noWrap >Prepayment</TD>
                      <TD>If you pay off early, you&nbsp;
                        <asp:dropdownlist id=sPrepmtPenaltyT onclick="f_setPrepmtPeriodMonthsStatus()" runat="server">
                            <asp:ListItem Value="0" Text=""></asp:ListItem>
                            <asp:ListItem Value="1">will not</asp:ListItem>
                            <asp:ListItem Value="2">may</asp:ListItem>
                        </asp:dropdownlist>
                        &nbsp;be required to pay a penalty and
                        <asp:dropdownlist id=sPrepmtRefundT runat="server">
                            <asp:ListItem Value="0" Text=""></asp:ListItem>
                            <asp:ListItem Value="1">will not</asp:ListItem>
                            <asp:ListItem Value="2">may</asp:ListItem>
						</asp:dropdownlist>&nbsp;be entitled to a refund 
                        of part of the finance charge.
                        <br />
                        Prepayment Period <asp:textbox id="sPrepmtPeriodMonths" runat="server" Width="43px"></asp:textbox> months
                        &nbsp;&nbsp;&nbsp;
                        Soft Prepayment Period <asp:textbox id="sSoftPrepmtPeriodMonths" runat="server" Width="43px"></asp:textbox> months
                      </TD></TR>
                    <TR>
                      <TD class=FieldLabel vAlign=top noWrap >Assumption</TD>
                      <TD>Someone buying your property
                        <asp:dropdownlist id=sAssumeLT runat="server">
                            <asp:ListItem Value="0" Text=""></asp:ListItem>
                            <asp:ListItem Value="1">may not</asp:ListItem>
                            <asp:ListItem Value="2">may</asp:ListItem>
                            <asp:ListItem Value="3">may, subject to conditions</asp:ListItem>
						</asp:dropdownlist>&nbsp;assume the remainder of 
                        your loan on the original 
              terms.</TD></TR></TABLE></TD></TR></TABLE></td></tr>
        <tr>
          <td>See your contract documents for any 
            additional information about nonpayment, default, any required repayment 
            in full before the scheduled date and prepayment refunds and 
            penalties.</td></tr>
        <tr>
          <td><asp:checkbox id=sAsteriskEstimate runat="server" Text="* means an estimate"></asp:checkbox></td></tr>
        <tr>
          <td><asp:checkbox id=sOnlyLatePmtEstimate runat="server" Text="all dates and numerical disclosures except the late payment disclosures are estimates."></asp:checkbox></td></tr></table>
    <Rfp:RateFloorPopup runat="server" ID="RateFloorPopup" />
</form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
  </body>
</HTML>
