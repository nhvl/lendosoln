namespace LendersOfficeApp.newlos.Forms
{
    using System;
    using DataAccess;

    public class TruthInLending32ServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(TruthInLending32ServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataLoan.sBalloonPmt = GetBool("sBalloonPmt");
            dataLoan.sCreditInsIncludedTri = (E_TriState) GetInt("sCreditInsIncludedTri");
            dataLoan.sHasVarRFeature = GetBool("sHasVarRFeature");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            SetResult("sApr", dataLoan.sApr_rep);
            SetResult("sSchedPmt1", dataLoan.sSchedPmt1_rep);
            SetResult("sFinalBalloonPmt", dataLoan.sFinalBalloonPmt_rep);
            SetResult("sBalloonPmt", dataLoan.sBalloonPmt);
            SetResult("sCreditInsIncludedTri", dataLoan.sCreditInsIncludedTri.ToString("D"));
            SetResult("sHasVarRFeature", dataLoan.sHasVarRFeature);
            string maxPmtStr = "$0.00";
			
            if (dataLoan.sHasVarRFeature) 
            {
                bool groupAmortTableEntries = true;
                var worstCaseAmort = dataLoan.GetAmortTable(
                    E_AmortizationScheduleT.WorstCase,
                    groupAmortTableEntries);

                decimal maxPayment = 0.0M;

                try
                {
                    for (int i = 0; i < worstCaseAmort.nRows; i++) 
                    {
                        maxPayment = Math.Max(maxPayment, worstCaseAmort.Items[i].Pmt);
                    }
                    maxPmtStr = dataLoan.m_convertLos.ToMoneyString(maxPayment, FormatDirection.ToRep);
                }
                catch{}
            } 
            SetResult("MaxPayment", maxPmtStr);
        }
    }
	public partial class TruthInLending32Service : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new TruthInLending32ServiceItem());
        }
	}
}
