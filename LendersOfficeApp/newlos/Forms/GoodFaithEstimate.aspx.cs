using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOfficeApp.los.LegalForm;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class GoodFaithEstimate : BaseLoanPage
    {
        #region "Member's variables"
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sLOrigFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sLDiscntProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sApprFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sCrFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sInspectFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sMBrokFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sTxServFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sProcFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sUwFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sWireFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  s800U1FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  s800U2FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  s800U3FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  s800U4FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  s800U5FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sIPiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sMipPiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sHazInsPiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  s904PiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sVaFfProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  s900U1PiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sHazInsRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sMInsRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sSchoolTxRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sRealETxRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sFloodInsRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  s1006RsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  s1007RsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sAggregateAdjRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sEscrowFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sDocPrepFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sNotaryFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sAttorneyFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sTitleInsFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sU1TcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sU2TcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sU3TcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sU4TcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sRecFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sCountyRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sStateRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sU1GovRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sU2GovRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sU3GovRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sPestInspectFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sU1ScProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sU2ScProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sU3ScProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sU4ScProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn  sU5ScProps_ctrl;

        private LosConvert m_convertLos;
        #endregion 



			
		protected string sLPurposeT = "";

        protected string m_openedDate ;

        protected bool m_byPassBgCalcForGfeAsDefault = false;


        private void SetByPassBgCalcForGfeAsDefault() 
        {
            SqlParameter[] parameters = { new SqlParameter("@UserId", BrokerUser.UserId) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "RetrieveByPassBgCalcForGfeAsDefaultByUserId", parameters)) 
            {
                if (reader.Read()) 
                {
                    m_byPassBgCalcForGfeAsDefault = (bool) reader["ByPassBgCalcForGfeAsDefault"];

                }
            }
        }
        private void BindDataObject(CPageData dataLoan) 
        {
			ByPassBgCalcForGfeAsDefault.SelectedIndex = m_byPassBgCalcForGfeAsDefault ? 1 : 0;

            sEstCloseD.Text = dataLoan.sEstCloseD_rep;
			sEstCloseD.ToolTip = "Hint:  Enter 't' for today's date.";
            m_openedDate = dataLoan.sOpenedD_rep;
            sDue.Text = dataLoan.sDue_rep;
            sDue.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            LeaveEmptyField(sDue);

            sTerm.Text = dataLoan.sTerm_rep;
            sTerm.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            LeaveEmptyField(sTerm);

            sSchedDueD1.Text = dataLoan.sSchedDueD1_rep;
			sSchedDueD1.ToolTip = "Hint:  Enter 't' for today's date.";
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            LeaveEmptyField(sNoteIR);

            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sFinalLAmt2.Text = dataLoan.sFinalLAmt_rep;
            sDaysInYr.Text = dataLoan.sDaysInYr_rep;
            LeaveEmptyField(sDaysInYr);


            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm( E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject );
            GfeTilCompanyName.Text = gfeTil.CompanyName;
            GfeTilStreetAddr.Text = gfeTil.StreetAddr;
            GfeTilCity.Text = gfeTil.City;
            GfeTilState.Value = gfeTil.State;
            GfeTilZip.Text = gfeTil.Zip;
            GfeTilPhoneOfCompany.Text = gfeTil.PhoneOfCompany;
            GfeTilPrepareDate.Text = gfeTil.PrepareDate_rep;
			GfeTilPrepareDate.ToolTip = "Hint:  Enter 't' for today's date.  Enter 'o' for opened date.";
            CFM.IsLocked = gfeTil.IsLocked;
            CFM.AgentRoleT = gfeTil.AgentRoleT;


            s800U5F.Text = dataLoan.s800U5F_rep;
            LeaveEmptyField(s800U5F);

            s800U5FDesc.Text = dataLoan.s800U5FDesc;
            s800U5FCode.Text = dataLoan.s800U5FCode;
            s800U4F.Text = dataLoan.s800U4F_rep;
            LeaveEmptyField(s800U4F);

            s800U4FDesc.Text = dataLoan.s800U4FDesc;
            s800U4FCode.Text = dataLoan.s800U4FCode;
            s800U3F.Text = dataLoan.s800U3F_rep;
            LeaveEmptyField(s800U3F);

            s800U3FDesc.Text = dataLoan.s800U3FDesc;
            s800U3FCode.Text = dataLoan.s800U3FCode;
            s800U2F.Text = dataLoan.s800U2F_rep;
            LeaveEmptyField(s800U2F);

            s800U2FDesc.Text = dataLoan.s800U2FDesc;
            s800U2FCode.Text = dataLoan.s800U2FCode;
            s800U1F.Text = dataLoan.s800U1F_rep;
            LeaveEmptyField(s800U1F);

            s800U1FDesc.Text = dataLoan.s800U1FDesc;
            s800U1FCode.Text = dataLoan.s800U1FCode;
            sWireF.Text = dataLoan.sWireF_rep;
            LeaveEmptyField(sWireF);

            sUwF.Text = dataLoan.sUwF_rep;
            LeaveEmptyField(sUwF);

            sProcF.Text = dataLoan.sProcF_rep;
            LeaveEmptyField(sProcF);

            sProcFPaid.Checked = dataLoan.sProcFPaid;
            sTxServF.Text = dataLoan.sTxServF_rep;
            LeaveEmptyField(sTxServF);

            sMBrokF.Text = dataLoan.sMBrokF_rep;
            LeaveEmptyField(sMBrokF);

            sMBrokFMb.Text = dataLoan.sMBrokFMb_rep;
            LeaveEmptyField(sMBrokFMb);

            sMBrokFPc.Text = dataLoan.sMBrokFPc_rep;
            LeaveEmptyField(sMBrokFPc);

            sInspectF.Text = dataLoan.sInspectF_rep;
            LeaveEmptyField(sInspectF);

            sCrF.Text = dataLoan.sCrF_rep;
            LeaveEmptyField(sCrF);

            sCrFPaid.Checked = dataLoan.sCrFPaid;
            sApprF.Text = dataLoan.sApprF_rep;
            LeaveEmptyField(sApprF);

            sApprFPaid.Checked = dataLoan.sApprFPaid;
            sLDiscnt.Text = dataLoan.sLDiscnt_rep;
            LeaveEmptyField(sLDiscnt);

            sLDiscntFMb.Text = dataLoan.sLDiscntFMb_rep;
            LeaveEmptyField(sLDiscntFMb);

            sLDiscntPc.Text = dataLoan.sLDiscntPc_rep;
            LeaveEmptyField(sLDiscntPc);

            sLOrigF.Text = dataLoan.sLOrigF_rep;
            LeaveEmptyField(sLOrigF);

            sLOrigFMb.Text = dataLoan.sLOrigFMb_rep;
            LeaveEmptyField(sLOrigFMb);

            sLOrigFPc.Text = dataLoan.sLOrigFPc_rep;
            LeaveEmptyField(sLOrigFPc);

            s900U1Pia.Text = dataLoan.s900U1Pia_rep;
            LeaveEmptyField(s900U1Pia);

            s900U1PiaDesc.Text = dataLoan.s900U1PiaDesc;
            s900U1PiaCode.Text = dataLoan.s900U1PiaCode;
            sVaFf.Text = dataLoan.sVaFf_rep;
            s904Pia.Text = dataLoan.s904Pia_rep;
            LeaveEmptyField(s904Pia);

            s904PiaDesc.Text = dataLoan.s904PiaDesc;
            sHazInsPia.Text = dataLoan.sHazInsPia_rep;
            LeaveEmptyField(sHazInsPia);

            sHazInsPiaMon.Text = dataLoan.sHazInsPiaMon_rep;
            LeaveEmptyField(sHazInsPiaMon);

            Tools.SetDropDownListValue(sProHazInsT, dataLoan.sProHazInsT);
            sProHazInsR.Text = dataLoan.sProHazInsR_rep;
            LeaveEmptyField(sProHazInsR);

            sProHazInsMb.Text = dataLoan.sProHazInsMb_rep;
            LeaveEmptyField(sProHazInsMb);

            sMipPia.Text = dataLoan.sMipPia_rep;
            LeaveEmptyField(sMipPia);

            sIPia.Text = dataLoan.sIPia_rep;
            LeaveEmptyField(sIPia);

            sIPiaDy.Text = dataLoan.sIPiaDy_rep;
            LeaveEmptyField(sIPiaDy);

			sIPerDayLckd.Checked = dataLoan.sIPerDayLckd;
            sIPerDay.Text = dataLoan.sIPerDay_rep;
            LeaveEmptyField(sIPerDay);

            sAggregateAdjRsrv.Text = dataLoan.sAggregateAdjRsrv_rep;
            sAggregateAdjRsrvLckd.Checked = dataLoan.sAggregateAdjRsrvLckd;
            s1007Rsrv.Text = dataLoan.s1007Rsrv_rep;
            LeaveEmptyField(s1007Rsrv);

            s1007ProHExp.Text = dataLoan.s1007ProHExp_rep;
            LeaveEmptyField(s1007ProHExp);

            s1007RsrvMon.Text = dataLoan.s1007RsrvMon_rep;
            LeaveEmptyField(s1007RsrvMon);

            s1007ProHExpDesc.Text = dataLoan.s1007ProHExpDesc;
            s1006Rsrv.Text = dataLoan.s1006Rsrv_rep;
            LeaveEmptyField(s1006Rsrv);

            s1006ProHExp.Text = dataLoan.s1006ProHExp_rep;
            LeaveEmptyField(s1006ProHExp);

            s1006RsrvMon.Text = dataLoan.s1006RsrvMon_rep;
            LeaveEmptyField(s1006RsrvMon);

            s1006ProHExpDesc.Text = dataLoan.s1006ProHExpDesc;
            sFloodInsRsrv.Text = dataLoan.sFloodInsRsrv_rep;
            LeaveEmptyField(sFloodInsRsrv);

            sProFloodIns.Text = dataLoan.sProFloodIns_rep;
            LeaveEmptyField(sProFloodIns);

            sFloodInsRsrvMon.Text = dataLoan.sFloodInsRsrvMon_rep;
            LeaveEmptyField(sFloodInsRsrvMon);

            sRealETxRsrv.Text = dataLoan.sRealETxRsrv_rep;
            LeaveEmptyField(sRealETxRsrv);

            Tools.SetDropDownListValue(sProRealETxT, dataLoan.sProRealETxT);
			sProRealETxMb.Text = dataLoan.sProRealETxMb_rep;
            LeaveEmptyField(sProRealETxMb);

			sProRealETxR.Text = dataLoan.sProRealETxR_rep;
            LeaveEmptyField(sProRealETxR);

            sRealETxRsrvMon.Text = dataLoan.sRealETxRsrvMon_rep;
            LeaveEmptyField(sRealETxRsrvMon);

            sSchoolTxRsrv.Text = dataLoan.sSchoolTxRsrv_rep;
            LeaveEmptyField(sSchoolTxRsrv);

            sProSchoolTx.Text = dataLoan.sProSchoolTx_rep;
            sSchoolTxRsrvMon.Text = dataLoan.sSchoolTxRsrvMon_rep;
            LeaveEmptyField(sSchoolTxRsrvMon);
            LeaveEmptyField(sProSchoolTx);

            sMInsRsrv.Text = dataLoan.sMInsRsrv_rep;
            LeaveEmptyField(sMInsRsrv);

            sProMIns.Text = dataLoan.sProMIns_rep;
            LeaveEmptyField(sProMIns);

            sMInsRsrvMon.Text = dataLoan.sMInsRsrvMon_rep;
            LeaveEmptyField(sMInsRsrvMon);

            sHazInsRsrv.Text = dataLoan.sHazInsRsrv_rep;
            LeaveEmptyField(sHazInsRsrv);

            sProHazIns.Text = dataLoan.sProHazIns_rep;
            LeaveEmptyField(sProHazIns);

            sHazInsRsrvMon.Text = dataLoan.sHazInsRsrvMon_rep;
            LeaveEmptyField(sHazInsRsrvMon);

            sU4Tc.Text = dataLoan.sU4Tc_rep;
            LeaveEmptyField(sU4Tc);

            sU4TcDesc.Text = dataLoan.sU4TcDesc;
            sU4TcCode.Text = dataLoan.sU4TcCode;
            sU3Tc.Text = dataLoan.sU3Tc_rep;
            LeaveEmptyField(sU3Tc);

            sU3TcDesc.Text = dataLoan.sU3TcDesc;
            sU3TcCode.Text = dataLoan.sU3TcCode;
            sU2Tc.Text = dataLoan.sU2Tc_rep;
            LeaveEmptyField(sU2Tc);

            sU2TcDesc.Text = dataLoan.sU2TcDesc;
            sU2TcCode.Text = dataLoan.sU2TcCode;
            sU1Tc.Text = dataLoan.sU1Tc_rep;
            LeaveEmptyField(sU1Tc);

            sU1TcDesc.Text = dataLoan.sU1TcDesc;
            sU1TcCode.Text = dataLoan.sU1TcCode;
            sTitleInsF.Text = dataLoan.sTitleInsF_rep;
            LeaveEmptyField(sTitleInsF);

            sTitleInsFTable.Text = dataLoan.sTitleInsFTable;
            sAttorneyF.Text = dataLoan.sAttorneyF_rep;
            LeaveEmptyField(sAttorneyF);

            sNotaryF.Text = dataLoan.sNotaryF_rep;
            LeaveEmptyField(sNotaryF);

            sDocPrepF.Text = dataLoan.sDocPrepF_rep;
            LeaveEmptyField(sDocPrepF);

            sEscrowF.Text = dataLoan.sEscrowF_rep;
            LeaveEmptyField(sEscrowF);

            sEscrowFTable.Text = dataLoan.sEscrowFTable;
            sU3GovRtc.Text = dataLoan.sU3GovRtc_rep;
            LeaveEmptyField(sU3GovRtc);

            sU3GovRtcMb.Text = dataLoan.sU3GovRtcMb_rep;
            LeaveEmptyField(sU3GovRtcMb);

            Tools.SetDropDownListValue(sU3GovRtcBaseT, dataLoan.sU3GovRtcBaseT);
            sU3GovRtcPc.Text = dataLoan.sU3GovRtcPc_rep;
            LeaveEmptyField(sU3GovRtcPc);

            sU3GovRtcDesc.Text = dataLoan.sU3GovRtcDesc;
            sU3GovRtcCode.Text = dataLoan.sU3GovRtcCode;
            sU2GovRtc.Text = dataLoan.sU2GovRtc_rep;
            LeaveEmptyField(sU2GovRtc);

            sU2GovRtcMb.Text = dataLoan.sU2GovRtcMb_rep;
            LeaveEmptyField(sU2GovRtcMb);

            Tools.SetDropDownListValue(sU2GovRtcBaseT, dataLoan.sU2GovRtcBaseT);
            sU2GovRtcPc.Text = dataLoan.sU2GovRtcPc_rep;
            LeaveEmptyField(sU2GovRtcPc);

            sU2GovRtcDesc.Text = dataLoan.sU2GovRtcDesc;
            sU2GovRtcCode.Text = dataLoan.sU2GovRtcCode;
            sU1GovRtc.Text = dataLoan.sU1GovRtc_rep;
            LeaveEmptyField(sU1GovRtc);

            sU1GovRtcMb.Text = dataLoan.sU1GovRtcMb_rep;
            LeaveEmptyField(sU1GovRtcMb);

            Tools.SetDropDownListValue(sU1GovRtcBaseT, dataLoan.sU1GovRtcBaseT);
            sU1GovRtcPc.Text = dataLoan.sU1GovRtcPc_rep;
            LeaveEmptyField(sU1GovRtcPc);

            sU1GovRtcDesc.Text = dataLoan.sU1GovRtcDesc;
            sU1GovRtcCode.Text = dataLoan.sU1GovRtcCode;
            sStateRtcDesc.Text = dataLoan.sStateRtcDesc;
			sStateRtcDesc.ToolTip = "Enter description";
            sStateRtc.Text = dataLoan.sStateRtc_rep;
            LeaveEmptyField(sStateRtc);

            sStateRtcMb.Text = dataLoan.sStateRtcMb_rep;
            LeaveEmptyField(sStateRtcMb);

            Tools.SetDropDownListValue(sStateRtcBaseT, dataLoan.sStateRtcBaseT);
            sStateRtcPc.Text = dataLoan.sStateRtcPc_rep;
            LeaveEmptyField(sStateRtcPc);

            sCountyRtcDesc.Text = dataLoan.sCountyRtcDesc;
			sCountyRtcDesc.ToolTip = "Enter description";
            sCountyRtc.Text = dataLoan.sCountyRtc_rep;
            LeaveEmptyField(sCountyRtc);

            sCountyRtcMb.Text = dataLoan.sCountyRtcMb_rep;
            LeaveEmptyField(sCountyRtcMb);

            Tools.SetDropDownListValue(sCountyRtcBaseT, dataLoan.sCountyRtcBaseT);
            sCountyRtcPc.Text = dataLoan.sCountyRtcPc_rep;
            LeaveEmptyField(sCountyRtcPc);

            sRecFDesc.Text = dataLoan.sRecFDesc;
			sRecFDesc.ToolTip = "Enter description";
            sRecF.Text = dataLoan.sRecF_rep;
            LeaveEmptyField(sRecF);

            sRecFMb.Text = dataLoan.sRecFMb_rep;
            LeaveEmptyField(sRecFMb);

            Tools.SetDropDownListValue(sRecBaseT, dataLoan.sRecBaseT);
            sRecFPc.Text = dataLoan.sRecFPc_rep;
            LeaveEmptyField(sRecFPc);

            sU5Sc.Text = dataLoan.sU5Sc_rep;
            LeaveEmptyField(sU5Sc);

            sU5ScDesc.Text = dataLoan.sU5ScDesc;
            sU5ScCode.Text = dataLoan.sU5ScCode;
            sU4Sc.Text = dataLoan.sU4Sc_rep;
            LeaveEmptyField(sU4Sc);

            sU4ScDesc.Text = dataLoan.sU4ScDesc;
            sU4ScCode.Text = dataLoan.sU4ScCode;
            sU3Sc.Text = dataLoan.sU3Sc_rep;
            LeaveEmptyField(sU3Sc);

            sU3ScDesc.Text = dataLoan.sU3ScDesc;
            sU3ScCode.Text = dataLoan.sU3ScCode;
            sU2Sc.Text = dataLoan.sU2Sc_rep;
            LeaveEmptyField(sU2Sc);

            sU2ScDesc.Text = dataLoan.sU2ScDesc;
            sU2ScCode.Text = dataLoan.sU2ScCode;
            sU1Sc.Text = dataLoan.sU1Sc_rep;
            LeaveEmptyField(sU1Sc);

            sU1ScDesc.Text = dataLoan.sU1ScDesc;
            sU1ScCode.Text = dataLoan.sU1ScCode;
            sPestInspectF.Text = dataLoan.sPestInspectF_rep;
            LeaveEmptyField(sPestInspectF);

            sBrokComp2.Text = dataLoan.sBrokComp2_rep;
            LeaveEmptyField(sBrokComp2);

            sBrokComp2Desc.Text = dataLoan.sBrokComp2Desc;
            sBrokComp1.Text = dataLoan.sBrokComp1_rep;
            sBrokComp1Pc.Text = dataLoan.sBrokComp1Pc_rep;

            sBrokComp1Pc.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sBrokComp1Lckd.Checked = dataLoan.sBrokComp1Lckd;
            LeaveEmptyField(sBrokComp1);

            sBrokComp1Desc.Text = dataLoan.sBrokComp1Desc;
            sTotEstSc.Text = dataLoan.sTotEstSc_rep;
            sGfeProvByBrok.Checked = dataLoan.sGfeProvByBrok;

            sLpTemplateNm.Text = dataLoan.sLpTemplateNm;
			// 5/29/07 mf. OPM 12061. There is now a corporate setting for editing
			// the loan program name.
            if (! BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowEditingLoanProgramName))
				sLpTemplateNm.ReadOnly = true;

            sCcTemplateNm.Text = dataLoan.sCcTemplateNm;
            sFfUfmip1003.Text = dataLoan.sFfUfmip1003_rep;
            LeaveEmptyField(sFfUfmip1003);



            //sFHAReqCashInv.Text = dataLoan.sFHAReqCashInv_rep;
            //LeaveEmptyField(sFHAReqCashInv);

            //sFHASalesConcessions.Text = dataLoan.sFHASalesConcessions_rep;
            //LeaveEmptyField(sFHASalesConcessions);

            //sFHA203kRehabCost.Text = dataLoan.sFHA203kRehabCost_rep;
            //LeaveEmptyField(sFHA203kRehabCost);

            //sFHATotEstFntc.Text = dataLoan.sFHATotEstFntc_rep;
            //LeaveEmptyField(sFHATotEstFntc);

            //sFHAEnergyEffImprov.Text = dataLoan.sFHAEnergyEffImprov_rep;
            //LeaveEmptyField(sFHAEnergyEffImprov);



            InitItemProps( sLOrigFProps_ctrl, dataLoan.sLOrigFProps );
            InitItemProps( sLDiscntProps_ctrl, dataLoan.sLDiscntProps );
            InitItemProps( sApprFProps_ctrl, dataLoan.sApprFProps );
            InitItemProps( sCrFProps_ctrl, dataLoan.sCrFProps );
            InitItemProps( sInspectFProps_ctrl, dataLoan.sInspectFProps );
            InitItemProps( sMBrokFProps_ctrl, dataLoan.sMBrokFProps );
            InitItemProps( sTxServFProps_ctrl, dataLoan.sTxServFProps );
            InitItemProps( sProcFProps_ctrl, dataLoan.sProcFProps );
            InitItemProps( sUwFProps_ctrl, dataLoan.sUwFProps );
            InitItemProps( sWireFProps_ctrl, dataLoan.sWireFProps );
            InitItemProps( s800U1FProps_ctrl, dataLoan.s800U1FProps );
            InitItemProps( s800U2FProps_ctrl, dataLoan.s800U2FProps );
            InitItemProps( s800U3FProps_ctrl, dataLoan.s800U3FProps );	
            InitItemProps( s800U4FProps_ctrl, dataLoan.s800U4FProps );
            InitItemProps( s800U5FProps_ctrl, dataLoan.s800U5FProps );
            InitItemProps( sIPiaProps_ctrl, dataLoan.sIPiaProps );
            InitItemProps( sMipPiaProps_ctrl, dataLoan.sMipPiaProps );
            InitItemProps( sHazInsPiaProps_ctrl, dataLoan.sHazInsPiaProps );
            InitItemProps( s904PiaProps_ctrl, dataLoan.s904PiaProps );
            InitItemProps( sVaFfProps_ctrl, dataLoan.sVaFfProps );
            InitItemProps( s900U1PiaProps_ctrl, dataLoan.s900U1PiaProps );
            InitItemProps( sHazInsRsrvProps_ctrl, dataLoan.sHazInsRsrvProps );
            InitItemProps( sMInsRsrvProps_ctrl, dataLoan.sMInsRsrvProps );
            InitItemProps( sSchoolTxRsrvProps_ctrl, dataLoan.sSchoolTxRsrvProps );
            InitItemProps( sRealETxRsrvProps_ctrl, dataLoan.sRealETxRsrvProps );
            InitItemProps( sFloodInsRsrvProps_ctrl, dataLoan.sFloodInsRsrvProps );
            InitItemProps( s1006RsrvProps_ctrl, dataLoan.s1006RsrvProps );
            InitItemProps( s1007RsrvProps_ctrl, dataLoan.s1007RsrvProps );
            InitItemProps( sAggregateAdjRsrvProps_ctrl, dataLoan.sAggregateAdjRsrvProps );
            InitItemProps( sEscrowFProps_ctrl, dataLoan.sEscrowFProps );
            InitItemProps( sDocPrepFProps_ctrl, dataLoan.sDocPrepFProps );
            InitItemProps( sNotaryFProps_ctrl, dataLoan.sNotaryFProps );
            InitItemProps( sAttorneyFProps_ctrl, dataLoan.sAttorneyFProps );
            InitItemProps( sTitleInsFProps_ctrl, dataLoan.sTitleInsFProps );
            InitItemProps( sU1TcProps_ctrl, dataLoan.sU1TcProps );
            InitItemProps( sU2TcProps_ctrl, dataLoan.sU2TcProps );
            InitItemProps( sU3TcProps_ctrl, dataLoan.sU3TcProps );
            InitItemProps( sU4TcProps_ctrl, dataLoan.sU4TcProps );
            InitItemProps( sRecFProps_ctrl, dataLoan.sRecFProps );
            InitItemProps( sCountyRtcProps_ctrl, dataLoan.sCountyRtcProps );
            InitItemProps( sStateRtcProps_ctrl, dataLoan.sStateRtcProps );
            InitItemProps( sU1GovRtcProps_ctrl, dataLoan.sU1GovRtcProps );
            InitItemProps( sU2GovRtcProps_ctrl, dataLoan.sU2GovRtcProps );
            InitItemProps( sU3GovRtcProps_ctrl, dataLoan.sU3GovRtcProps );
            InitItemProps( sPestInspectFProps_ctrl, dataLoan.sPestInspectFProps );
            InitItemProps( sU1ScProps_ctrl, dataLoan.sU1ScProps );
            InitItemProps( sU2ScProps_ctrl, dataLoan.sU2ScProps );
            InitItemProps( sU3ScProps_ctrl, dataLoan.sU3ScProps );
            InitItemProps( sU4ScProps_ctrl, dataLoan.sU4ScProps );
            InitItemProps( sU5ScProps_ctrl, dataLoan.sU5ScProps );

            sPurchPrice.Text = dataLoan.sPurchPrice_rep;
            sAltCostLckd.Checked = dataLoan.sAltCostLckd;
            sAltCostLckd.Disabled = !dataLoan.sIsRenovationLoan;
            sAltCost.Text = dataLoan.sAltCost_rep;
            sLandCost.Text = dataLoan.sLandCost_rep;
            sRefPdOffAmt1003Lckd.Checked= dataLoan.sRefPdOffAmt1003Lckd;
            sRefPdOffAmt1003.Text = dataLoan.sRefPdOffAmt1003_rep;
            sTotEstPp1003Lckd.Checked= dataLoan.sTotEstPp1003Lckd;
            sTotEstPp1003.Text = dataLoan.sTotEstPp1003_rep;
            sTotEstCc1003Lckd.Checked= dataLoan.sTotEstCc1003Lckd;
            sTotEstCcNoDiscnt1003.Text = dataLoan.sTotEstCcNoDiscnt1003_rep;
            sFfUfmip1003Lckd.Checked= dataLoan.sFfUfmip1003Lckd;
            sLDiscnt1003Lckd.Checked= dataLoan.sLDiscnt1003Lckd;
            sLDiscnt1003.Text = dataLoan.sLDiscnt1003_rep;
            sTotTransC.Text = dataLoan.sTotTransC_rep;
            sONewFinBal.Text = dataLoan.sONewFinBal_rep;
            sOCredit1Desc.Text = dataLoan.sOCredit1Desc;
            sOCredit1Amt.Text = dataLoan.sOCredit1Amt_rep;
            sOCredit1Lckd.Checked = dataLoan.sOCredit1Lckd;
            sOCredit2Desc.Text = dataLoan.sOCredit2Desc;

            sOCredit2Amt.Text = dataLoan.sOCredit2Amt_rep;
            sOCredit3Desc.Text = dataLoan.sOCredit3Desc;
            sOCredit3Amt.Text = dataLoan.sOCredit3Amt_rep;
            sOCredit4Desc.Text = dataLoan.sOCredit4Desc;
            sOCredit4Amt.Text = dataLoan.sOCredit4Amt_rep;

            if (dataLoan.sLoads1003LineLFromAdjustments)
            {
                sOCredit1Lckd.Enabled = false;
                sOCredit2Amt.ReadOnly = true;
                sOCredit2Desc.ReadOnly = true;
                sOCredit3Amt.ReadOnly = true;
                sOCredit3Desc.ReadOnly = true;
                sOCredit4Amt.ReadOnly = true;
                sOCredit4Desc.ReadOnly = true;
            }

            sLAmtCalc.Text = dataLoan.sLAmtCalc_rep;
			sLAmtLckd.Checked = dataLoan.sLAmtLckd;
            sFfUfmipFinanced.Text = dataLoan.sFfUfmipFinanced_rep;
            sTransNetCashLckd.Checked= dataLoan.sTransNetCashLckd;
            sTransNetCash.Text = dataLoan.sTransNetCash_rep;
            sTotCcPbsLocked.Checked = dataLoan.sTotCcPbsLocked;
            sTotCcPbs.Text = dataLoan.sTotCcPbs_rep;
            sPrintCompensationOnGfe.Checked = dataLoan.sPrintCompensationOnGfe;
			sONewFinCc.Text = dataLoan.sONewFinCc_rep;
            sIPiaDyLckd.Checked = dataLoan.sIPiaDyLckd;
        }

        protected override void LoadData() 
        {
			m_convertLos = new LosConvert();

            CGfeData dataLoan = new CGfeData( LoanID );
            dataLoan.InitLoad();

			sLPurposeT = dataLoan.sLPurposeT.ToString();

            BindDataObject(dataLoan);
            sPurchPrice.ReadOnly = dataLoan.sLPurposeT != E_sLPurposeT.Purchase || IsReadOnly;
            this.sLAmtLckd.Enabled = !IsReadOnly && (dataLoan.sLPurposeT == E_sLPurposeT.Purchase || !dataLoan.sIsRenovationLoan);
        }

        private void InitItemProps( GoodFaithEstimateRightColumn ctrl, int props )
        {
            ctrl.Apr = LosConvert.GfeItemProps_Apr( props );
            ctrl.ToBrok = LosConvert.GfeItemProps_ToBr( props );
            ctrl.PdByT = LosConvert.GfeItemProps_Payer( props );
			ctrl.Fha = LosConvert.GfeItemProps_FhaAllow( props );
            ctrl.Poc = LosConvert.GfeItemProps_Poc(props);
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            GfeTilZip.SmartZipcode(GfeTilCity, GfeTilState);
            BindOtherCreditDescription(sOCredit1Desc);
            BindOtherCreditDescription(sOCredit2Desc);
            BindOtherCreditDescription(sOCredit3Desc);
            BindOtherCreditDescription(sOCredit4Desc);
            this.PageTitle = "Good Faith Estimate";
            this.PageID = "GFE";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CGoodFaithEstimatePDF);

            Tools.Bind_sProRealETxT(sProRealETxT);
            Tools.Bind_PercentBaseT(sProHazInsT);

            Tools.Bind_PercentBaseT(sRecBaseT);
            Tools.Bind_PercentBaseT(sCountyRtcBaseT);
            Tools.Bind_PercentBaseT(sStateRtcBaseT);
            Tools.Bind_PercentBaseT(sU1GovRtcBaseT);
            Tools.Bind_PercentBaseT(sU2GovRtcBaseT);
            Tools.Bind_PercentBaseT(sU3GovRtcBaseT);

            SetByPassBgCalcForGfeAsDefault();

            CFM.Type = "19";
            CFM.CompanyNameField = GfeTilCompanyName.ClientID;
            CFM.StreetAddressField = GfeTilStreetAddr.ClientID;
            CFM.CityField = GfeTilCity.ClientID;
            CFM.StateField = GfeTilState.ClientID;
            CFM.ZipField = GfeTilZip.ClientID;
            CFM.CompanyPhoneField = GfeTilPhoneOfCompany.ClientID;
            CFM.IsAllowLockableFeature = true;

            this.RegisterJsScript("LQBPopup.js");
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            IsAppSpecific = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion

        /// <summary>
        /// Request by AFG users, if GFE in manual calculation mode then they want
        /// $0.00, or 0.000% to be blank. 9/8/03
        /// </summary>
        /// <param name="tb"></param>
        private void LeaveEmptyField(TextBox tb) 
        {

            if (!tb.ReadOnly) 
            {
                if (tb.Text == "$0.00" || tb.Text == "0.000%" || tb.Text == "0") 
                {
                    tb.Text = "";
                }
            }
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
        
        }
        private void BindOtherCreditDescription(MeridianLink.CommonControls.ComboBox cb) 
        {
            cb.Items.Add("Cash Deposit on sales contract");
            cb.Items.Add("Seller Credit");
            cb.Items.Add("Lender Credit");
            cb.Items.Add( "Relocation Funds");
            cb.Items.Add( "Employer Assisted Housing");
            cb.Items.Add( "Lease Purchase Fund");
            cb.Items.Add( "Borrower Paid Fees");
            cb.Items.Add("Paid Outside of Closing");
			cb.Items.Add("Broker Credit");
		

        }
    }
}
