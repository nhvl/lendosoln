<%@ Import namespace="DataAccess" %>
<%@ Page language="c#" Codebehind="AggregateEscrowDisclosure.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Forms.AggregateEscrowDisclosure" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
  <style type="text/css">
    #sSchedDueD1Lckd { float: right; }
    .PlayNiceWithLockCB { display: inline-block; padding-top: 2px; }
  </style>
  </HEAD>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro" leftmargin=0>
	<script language=javascript>
<!--
var oRolodex = null;

function _init() {
  if (null == oRolodex)
    oRolodex = new cRolodex();
  lockField(<%=AspxTools.JsGetElementById(sSchedDueD1Lckd)%>, 'sSchedDueD1');
}
function f_onsetupClick() {
  showModal('/newlos/Forms/InitialEscrowAccSetup.aspx?loanid=' + ML.sLId, null, null, null, function(args){ 
    if (args.OK) {
      <%= AspxTools.JsGetElementById(sSchedDueD1Lckd) %>.checked = args.sSchedDueD1Lckd;
      <%= AspxTools.JsGetElementById(sSchedDueD1) %>.value = args.sSchedDueD1;
    }
    refreshCalculation();
  },{ hideCloseButton: true });
}
function doAfterDateFormat(e)
{
    if(e.id === "sSchedDueD1")
    {
        refreshCalculation();
    }
}
//-->
</script>

    <form id="AggregateEscrowDisclosure" method="post" runat="server">
<TABLE class="FormTable" id=Table1 cellSpacing=0 cellPadding=0 width="100%" border=0>
  <TR>
    <TD noWrap class="MainRightHeader" style="PADDING-LEFT:5px"> Aggregate Escrow Account</TD></TR>
  <TR>
    <TD noWrap style="PADDING-LEFT:5px">
      <TABLE id=Table2 cellSpacing=0 cellPadding=0 border=0 class="InsetBorder">
        <tr>
			<td class="FieldLabel" nowrap></td>
			<td nowrap>
				<uc:CFM name=CFM runat="server" Type="2" CompanyNameField="AggregateEscrowServicerCompanyName" StreetAddressField="AggregateEscrowServicerStreetAddr" CityField="AggregateEscrowServicerCity" StateField="AggregateEscrowServicerState" ZipField="AggregateEscrowServicerZip" CompanyPhoneField="AggregateEscrowServicerPhoneOfCompany"></uc:CFM>
			</td>
		</tr>
        <TR>
          <TD class=FieldLabel noWrap>Servicer Name</TD>
          <TD noWrap><asp:TextBox id=AggregateEscrowServicerCompanyName runat="server" Width="231px"></asp:TextBox></TD></TR>
        <TR>
          <TD class=FieldLabel noWrap>Address</TD>
          <TD noWrap><asp:TextBox id=AggregateEscrowServicerStreetAddr runat="server" Width="231px"></asp:TextBox></TD></TR>
        <TR>
          <TD class=FieldLabel noWrap></TD>
          <TD noWrap><asp:TextBox id=AggregateEscrowServicerCity runat="server" Width="126px"></asp:TextBox><ml:StateDropDownList id=AggregateEscrowServicerState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=AggregateEscrowServicerZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></TD></TR>
        <TR>
          <TD class=FieldLabel noWrap>Phone</TD>
          <TD noWrap><ml:PhoneTextBox id=AggregateEscrowServicerPhoneOfCompany runat="server" width="120" preset="phone"></ml:PhoneTextBox></TD></TR>
        <TR>
          <TD class=FieldLabel noWrap>
              <span class="PlayNiceWithLockCB">First Payment Date</span>
              <asp:CheckBox runat="server" ID="sSchedDueD1Lckd" onclick="lockField(this, 'sSchedDueD1'); refreshCalculation();"/>
          </TD>
          <TD noWrap><ml:DateTextBox id=sSchedDueD1 runat="server" width="75" preset="date" onchange="date_onblur(null, this);" ></ml:DateTextBox>&nbsp;* 
            Note: First Payment Date is required for the calculation.</TD></TR>
        <TR>
          <TD class=FieldLabel noWrap>Date Prepared</TD>
          <TD noWrap><ml:DateTextBox id=GfeTilPrepareDate runat="server" width="75" preset="date"></ml:DateTextBox></TD></TR>
        <TR>
          <TD class=FieldLabel noWrap>Aggr Escrow Adjustment&nbsp; </TD>
          <TD noWrap><ml:MoneyTextBox id=AggregateEscrowAdjustment runat="server" preset="money" width="90" ReadOnly="True"></ml:MoneyTextBox></TD></TR></TABLE></TD></TR>
  <TR>
    <TD style="PADDING-LEFT: 5px" noWrap><INPUT class=ButtonStyle type=button value="Setup Initial Escrow Account ..." onclick="f_onsetupClick();" NoHighlight></TD></TR>
  <TR>
    <TD noWrap style="PADDING-LEFT:5px">
      <table class="InsetBorder"><tr><td>
      <TABLE id=Table3 cellSpacing=0 cellPadding=0 border=0>
        <TR>
          <TD noWrap class=FormTableSubheader align=center>Month</TD>
          <TD noWrap class=FormTableSubheader style="WIDTH: 104px" align=center>Payments to Escrow Account</TD>
          <TD noWrap class=FormTableSubheader style="WIDTH: 103px" align=center>Payments from Escrow Account</TD>
          <TD noWrap class=FormTableSubheader>Description</TD>
          <TD noWrap class=FormTableSubheader style="WIDTH: 110px" align=center>Escrow Account Balance</TD></TR>
        <TR>
          <TD noWrap class=FieldLabel>Initial Deposit</TD>
          <TD noWrap style="WIDTH: 104px"></TD>
          <TD noWrap style="WIDTH: 103px"></TD>
          <TD noWrap></TD>
          <TD noWrap style="WIDTH: 110px"><ml:MoneyTextBox id=sAggrEscrowInitialDeposit runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD></TR>
        <TR>
          <TD noWrap vAlign=top><asp:TextBox id=sAggrEscrowMon0 runat="server" Width="73px" ReadOnly="True"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 104px" vAlign=top><ml:MoneyTextBox id=sEscrowPmt0 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap style="WIDTH: 103px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowPmtFromEscrow0 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap><asp:TextBox id=sAggrEscrowDesc0 runat="server" Width="240px" ReadOnly="True" TextMode="MultiLine" Height="56px"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 110px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowBal0 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD></TR>
        <TR>
          <TD noWrap vAlign=top><asp:TextBox id=sAggrEscrowMon1 runat="server" Width="73px" ReadOnly="True"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 104px" vAlign=top><ml:MoneyTextBox id=sEscrowPmt1 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap style="WIDTH: 103px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowPmtFromEscrow1 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap><asp:TextBox id=sAggrEscrowDesc1 runat="server" Width="240px" ReadOnly="True" TextMode="MultiLine" Height="56px"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 110px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowBal1 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD></TR>
        <TR>
          <TD noWrap vAlign=top><asp:TextBox id=sAggrEscrowMon2 runat="server" Width="73px" ReadOnly="True"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 104px" vAlign=top><ml:MoneyTextBox id=sEscrowPmt2 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap style="WIDTH: 103px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowPmtFromEscrow2 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap><asp:TextBox id=sAggrEscrowDesc2 runat="server" Width="240px" ReadOnly="True" TextMode="MultiLine" Height="56px"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 110px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowBal2 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD></TR>
        <TR>
          <TD noWrap vAlign=top><asp:TextBox id=sAggrEscrowMon3 runat="server" Width="73px" ReadOnly="True"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 104px" vAlign=top><ml:MoneyTextBox id=sEscrowPmt3 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap style="WIDTH: 103px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowPmtFromEscrow3 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap><asp:TextBox id=sAggrEscrowDesc3 runat="server" Width="240px" ReadOnly="True" TextMode="MultiLine" Height="56px"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 110px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowBal3 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD></TR>
        <TR>
          <TD noWrap vAlign=top><asp:TextBox id=sAggrEscrowMon4 runat="server" Width="73px" ReadOnly="True"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 104px" vAlign=top><ml:MoneyTextBox id=sEscrowPmt4 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap style="WIDTH: 103px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowPmtFromEscrow4 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap><asp:TextBox id=sAggrEscrowDesc4 runat="server" Width="240px" ReadOnly="True" TextMode="MultiLine" Height="56px"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 110px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowBal4 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD></TR>
        <TR>
          <TD noWrap vAlign=top><asp:TextBox id=sAggrEscrowMon5 runat="server" Width="73px" ReadOnly="True"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 104px" vAlign=top><ml:MoneyTextBox id=sEscrowPmt5 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap style="WIDTH: 103px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowPmtFromEscrow5 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap><asp:TextBox id=sAggrEscrowDesc5 runat="server" Width="240px" ReadOnly="True" TextMode="MultiLine" Height="56px"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 110px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowBal5 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD></TR>
        <TR>
          <TD noWrap vAlign=top><asp:TextBox id=sAggrEscrowMon6 runat="server" Width="73px" ReadOnly="True"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 104px" vAlign=top><ml:MoneyTextBox id=sEscrowPmt6 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap style="WIDTH: 103px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowPmtFromEscrow6 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap><asp:TextBox id=sAggrEscrowDesc6 runat="server" Width="240px" ReadOnly="True" TextMode="MultiLine" Height="56px"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 110px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowBal6 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD></TR>
        <TR>
          <TD noWrap vAlign=top><asp:TextBox id=sAggrEscrowMon7 runat="server" Width="73px" ReadOnly="True"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 104px" vAlign=top><ml:MoneyTextBox id=sEscrowPmt7 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap style="WIDTH: 103px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowPmtFromEscrow7 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap><asp:TextBox id=sAggrEscrowDesc7 runat="server" Width="240px" ReadOnly="True" TextMode="MultiLine" Height="56px"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 110px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowBal7 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD></TR>
        <TR>
          <TD noWrap vAlign=top><asp:TextBox id=sAggrEscrowMon8 runat="server" Width="73px" ReadOnly="True"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 104px" vAlign=top><ml:MoneyTextBox id=sEscrowPmt8 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap style="WIDTH: 103px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowPmtFromEscrow8 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap><asp:TextBox id=sAggrEscrowDesc8 runat="server" Width="240px" ReadOnly="True" TextMode="MultiLine" Height="56px"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 110px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowBal8 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD></TR>
        <TR>
          <TD noWrap vAlign=top><asp:TextBox id=sAggrEscrowMon9 runat="server" Width="73px" ReadOnly="True"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 104px" vAlign=top><ml:MoneyTextBox id=sEscrowPmt9 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap style="WIDTH: 103px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowPmtFromEscrow9 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap><asp:TextBox id=sAggrEscrowDesc9 runat="server" Width="240px" ReadOnly="True" TextMode="MultiLine" Height="56px"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 110px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowBal9 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD></TR>
        <TR>
          <TD noWrap vAlign=top><asp:TextBox id=sAggrEscrowMon10 runat="server" Width="73px" ReadOnly="True"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 104px" vAlign=top><ml:MoneyTextBox id=sEscrowPmt10 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap style="WIDTH: 103px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowPmtFromEscrow10 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap><asp:TextBox id=sAggrEscrowDesc10 runat="server" Width="240px" ReadOnly="True" TextMode="MultiLine" Height="56px"></asp:TextBox></TD>
          <TD noWrap style="WIDTH: 110px" vAlign=top><ml:MoneyTextBox id=sAggrEscrowBal10 runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD></TR>
        <TR>
          <TD vAlign=top noWrap><asp:TextBox id=sAggrEscrowMon11 runat="server" Width="73px" ReadOnly="True"></asp:TextBox></TD>
          <TD style="WIDTH: 104px" vAlign=top noWrap><ml:MoneyTextBox id=sEscrowPmt11 runat="server" preset="money" width="90" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD style="WIDTH: 103px" vAlign=top noWrap><ml:MoneyTextBox id=sAggrEscrowPmtFromEscrow11 runat="server" preset="money" width="90" ReadOnly="True"></ml:MoneyTextBox></TD>
          <TD noWrap><asp:TextBox id=sAggrEscrowDesc11 runat="server" Width="240px" ReadOnly="True" Height="56px" TextMode="MultiLine"></asp:TextBox></TD>
          <TD style="WIDTH: 110px" vAlign=top noWrap><ml:MoneyTextBox id=sAggrEscrowBal11 runat="server" preset="money" width="90" ReadOnly="True"></ml:MoneyTextBox></TD></TR></TABLE></td></tr>
  <TR>
    <TD noWrap></TD></TR></table>
</TD></TR></TABLE><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
     </form>
	
  </body>
</HTML>
