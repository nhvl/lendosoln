namespace LendersOfficeApp.newlos.Forms
{
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using System;

    public class Transmittal_04_CombinedServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(Transmittal_04_CombinedServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            dataApp = dataLoan.GetAppData(0); // Get Primary Application
            dataApp.aOccT                          = (E_aOccT) GetInt("aOccT");
            dataLoan.s1stMOwnerT                   = (E_s1stMOwnerT) GetInt("s1stMOwnerT");
            dataLoan.s1stMtgOrigLAmt_rep           = GetString("s1stMtgOrigLAmt");
            dataLoan.sApprVal_rep                  = GetString("sApprVal");
            dataLoan.sBuydown                      = GetString("sBuydown") == "1";
            dataLoan.sCombinedBorFirstNm           = GetString("sCombinedBorFirstNm") ;
            dataLoan.sCombinedBorInfoLckd          = GetBool("sCombinedBorInfoLckd") ;
            dataLoan.sCombinedBorLastNm            = GetString("sCombinedBorLastNm") ;
            dataLoan.sCombinedBorMidNm             = GetString("sCombinedBorMidNm") ;
            dataLoan.sCombinedBorSsn               = GetString("sCombinedBorSsn") ;
            dataLoan.sCombinedBorSuffix            = GetString("sCombinedBorSuffix") ;
            dataLoan.sCombinedCoborFirstNm         = GetString("sCombinedCoborFirstNm") ;
            dataLoan.sCombinedCoborLastNm          = GetString("sCombinedCoborLastNm") ;
            dataLoan.sCombinedCoborMidNm           = GetString("sCombinedCoborMidNm") ;
            dataLoan.sCombinedCoborSsn             = GetString("sCombinedCoborSsn") ;
            dataLoan.sCombinedCoborSuffix          = GetString("sCombinedCoborSuffix") ;
            dataLoan.sCombinedCoborUsing2ndAppBorr = GetBool("sCombinedCoborUsing2ndAppBorr");
            dataLoan.sCommitNum                    = GetString("sCommitNum");
            dataLoan.sContractNum                  = GetString("sContractNum");
            dataLoan.sFinMethDesc                  = GetString("sFinMethDesc");
            dataLoan.sFinMethT                     = (E_sFinMethT) GetInt("sFinMethT");
            dataLoan.sInvestLNumLckd               = GetBool("sInvestLNumLckd");
            dataLoan.sInvestLNum                   = GetString("sInvestLNum");
            dataLoan.sLPurposeT                    = (E_sLPurposeT) GetInt("sLPurposeT");
            dataLoan.sLT                           = (E_sLT) GetInt("sLT");
            dataLoan.sLenAddr                      = GetString("sLenAddr");
            dataLoan.sLenCity                      = GetString("sLenCity");
            dataLoan.sLenContactNm                 = GetString("sLenContactNm");
            dataLoan.sLenContactPhone              = GetString("sLenContactPhone");
            dataLoan.sLenContactTitle              = GetString("sLenContactTitle");
            dataLoan.sLenContractD_rep             = GetString("sLenContractD");
            dataLoan.sLenLNumLckd                  = GetBool("sLenLNumLckd");
            dataLoan.sLenLNum                      = GetString("sLenLNum");
            dataLoan.sLenNm                        = GetString("sLenNm");
            dataLoan.sLenNum                       = GetString("sLenNum");
            dataLoan.sLenState                     = GetString("sLenState");
            dataLoan.sLenZip                       = GetString("sLenZip");
            dataLoan.sLienPosT                     = (E_sLienPosT) GetInt("sLienPosT");
            dataLoan.sMOrigT                       = (E_sMOrigT) GetInt("sMOrigT");
            dataLoan.sNoteIR_rep                   = GetString("sNoteIR");
            dataLoan.sProjNm                       = GetString("sProjNm");
            dataLoan.sPurchPrice_rep               = GetString("sPurchPrice");
            dataLoan.sQualIRDeriveT                = (E_sQualIRDeriveT) GetInt("sQualIRDeriveT");
            dataLoan.sSpAddr                       = GetString("sSpAddr");
            dataLoan.sSpCity                       = GetString("sSpCity");
            dataLoan.sSpZip                        = GetString("sSpZip");
            dataLoan.sSubFin_rep                   = GetString("sSubFin");
            dataLoan.sTerm_rep                     = GetString("sTerm");
            dataLoan.sTransmUwerComments           = GetString("sTransmUwerComments");
            dataLoan.sUnitsNum_rep                 = GetString("sUnitsNum");
            dataLoan.sSpProjectClassFreddieT = (E_sSpProjectClassFreddieT)GetInt("sSpProjectClassFreddieT");
            dataLoan.sCpmProjectId = GetString("sCpmProjectId");

            CAgentFields appraiser = dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser,E_ReturnOptionIfNotExist.CreateNew);
            appraiser.CompanyName = GetString("sApprComNm");
            appraiser.LicenseNumOfAgent = GetString("sApprerLicNum") ;
            appraiser.AgentName = GetString("sApprerNm") ;
            appraiser.Update();

            CAgentFields underwritter = dataLoan.GetAgentOfRole( E_AgentRoleT.Underwriter, E_ReturnOptionIfNotExist.CreateNew );
            underwritter.AgentName = GetString("sUwerNm");
            underwritter.Update();

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.TransmMOriginator, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("TransmMOriginatorCompanyName");
            preparer.Update();

            dataLoan.sBiweeklyPmt = GetBool("sBiweeklyPmt");
            dataLoan.sBalloonPmt = GetBool("sBalloonPmt");
            dataLoan.sEstateHeldT = (E_sEstateHeldT) GetInt("sEstateHeldT");
            dataLoan.sGseRefPurposeT = (E_sGseRefPurposeT) GetInt("sGseRefPurposeT");
            dataLoan.sGseSpT = (E_sGseSpT) GetInt("sGseSpT");

            dataLoan.sApprDriveBy = GetBool("sApprDriveBy");
            dataLoan.sApprFull = GetBool("sApprFull");

            dataLoan.sHcltvR_rep = GetString("sHcltvR");
            dataLoan.sHcltvRLckd = GetBool("sHcltvRLckd");
            dataLoan.sDebtToHousingGapR_rep = GetString("sDebtToHousingGapR");
            dataLoan.sDebtToHousingGapRLckd = GetBool("sDebtToHousingGapRLckd");
            dataLoan.sIsSpReviewNoAppr = GetBool("sIsSpReviewNoAppr");
            dataLoan.sSpReviewFormNum = GetString("sSpReviewFormNum");
            dataLoan.sTransmFntc_rep = GetString("sTransmFntc");
            dataLoan.sTransmFntcLckd = GetBool("sTransmFntcLckd");
            dataLoan.sVerifAssetAmt_rep = GetString("sVerifAssetAmt");
            dataLoan.sFntcSrc = GetString("sFntcSrc");
            dataLoan.sRsrvMonNumDesc = GetString("sRsrvMonNumDesc");
            dataLoan.sInterestedPartyContribR_rep = GetString("sInterestedPartyContribR");
            dataLoan.sIsManualUw = GetBool("sIsManualUw");
            dataLoan.sIsDuUw = GetBool("sIsDuUw");
            dataLoan.sIsLpUw = GetBool("sIsLpUw");
            dataLoan.sIsOtherUw = GetBool("sIsOtherUw");
            dataLoan.sOtherUwDesc = GetString("sOtherUwDesc");
            dataLoan.sAusRecommendation = GetString("sAusRecommendation");
            dataLoan.sLpAusKey = GetString("sLpAusKey");
            dataLoan.sDuCaseId = GetString("sDuCaseId");
            dataLoan.sLpDocClass = GetString("sLpDocClass");
            dataLoan.sRepCrScore = GetString("sRepCrScore");
            dataLoan.sIsCommLen = GetString("sIsCommLen") == "1";
            dataLoan.sIsHOwnershipEdCertInFile = GetString("sIsHOwnershipEdCertInFile") == "1";
            dataLoan.sIsMOrigBroker = GetBool("sIsMOrigBroker");
            dataLoan.sIsMOrigCorrespondent = GetBool("sIsMOrigCorrespondent");
            dataLoan.sWillEscrowBeWaived = GetString("sWillEscrowBeWaived") == "0";
            dataLoan.sTransmBuydwnTermDesc = GetString("sTransmBuydwnTermDesc");
            dataLoan.sFinMethodPrintAsOther = GetBool("sFinMethodPrintAsOther");
            dataLoan.sFinMethPrintAsOtherDesc = GetString("sFinMethPrintAsOtherDesc");
            dataLoan.sSpProjectClassFannieT = (E_sSpProjectClassFannieT) GetInt("sSpProjectClassFannieT");
            dataLoan.sApprovD_rep = GetString("sApprovD");
            dataLoan.sUnderwritingD_rep = GetString("sUnderwritingD");

            dataLoan.sQualTermCalculationType = (QualTermCalculationType)GetInt("sQualTermCalculationType");
            dataLoan.sQualTerm_rep = GetString("sQualTerm");

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V12_CalculateLevelOfPropertyReviewSection))
            {
                dataLoan.sSpValuationMethodT = (E_sSpValuationMethodT)GetInt("sSpValuationMethodT");
                dataLoan.sSpAppraisalFormT = (E_sSpAppraisalFormT)GetInt("sSpAppraisalFormT");
            }

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            dataApp = dataLoan.GetAppData(0); // Get Primary Application
            SetResult("aOccT",                         dataApp.aOccT);
            SetResult("s1stMOwnerT",                   dataLoan.s1stMOwnerT);
            SetResult("s1stMtgOrigLAmt",               dataLoan.s1stMtgOrigLAmt_rep);
            SetResult("sApprVal",                      dataLoan.sApprVal_rep);
            SetResult("sBuydown",                      dataLoan.sBuydown ? "1" : "0");
            SetResult("sCltvR",                        dataLoan.sCltvR_rep);
            SetResult("sCombinedBorFirstNm",           dataLoan.sCombinedBorFirstNm );
            SetResult("sCombinedBorInfoLckd",          dataLoan.sCombinedBorInfoLckd );
            SetResult("sCombinedBorLastNm",            dataLoan.sCombinedBorLastNm );
            SetResult("sCombinedBorMidNm",             dataLoan.sCombinedBorMidNm );
            SetResult("sCombinedBorSsn",               dataLoan.sCombinedBorSsn );
            SetResult("sCombinedBorSuffix",            dataLoan.sCombinedBorSuffix );
            SetResult("sCombinedCoborFirstNm",         dataLoan.sCombinedCoborFirstNm );
            SetResult("sCombinedCoborLastNm",          dataLoan.sCombinedCoborLastNm );
            SetResult("sCombinedCoborMidNm",           dataLoan.sCombinedCoborMidNm );
            SetResult("sCombinedCoborSsn",             dataLoan.sCombinedCoborSsn );
            SetResult("sCombinedCoborSuffix",          dataLoan.sCombinedCoborSuffix );
            SetResult("sCombinedCoborUsing2ndAppBorr", dataLoan.sCombinedCoborUsing2ndAppBorr);
            SetResult("sCommitNum",                    dataLoan.sCommitNum);
            SetResult("sContractNum",                  dataLoan.sContractNum);
            SetResult("sFinMethDesc",                  dataLoan.sFinMethDesc);
            SetResult("sFinMethT",                     dataLoan.sFinMethT);
            SetResult("sFinalLAmt",                    dataLoan.sFinalLAmt_rep);
            SetResult("sInvestLNumLckd",               dataLoan.sInvestLNumLckd);
            SetResult("sInvestLNum",                   dataLoan.sInvestLNum);
            SetResult("sLPurposeT",                    dataLoan.sLPurposeT);
            SetResult("sLT",                           dataLoan.sLT);
            SetResult("sLTotBaseI",                    dataLoan.sLTotBaseI_rep);
            SetResult("sLTotI",                        dataLoan.sLTotI_rep);
            SetResult("sLTotNonbaseI",                 dataLoan.sLTotNonbaseI_rep);
            SetResult("sLTotOIFrom1008",               dataLoan.sLTotOIFrom1008_rep);
            SetResult("sLTotSpPosCf",                  dataLoan.sLTotSpPosCf_rep);
            SetResult("sLenAddr",                      dataLoan.sLenAddr);
            SetResult("sLenCity",                      dataLoan.sLenCity);
            SetResult("sLenContactNm",                 dataLoan.sLenContactNm);
            SetResult("sLenContactPhone",              dataLoan.sLenContactPhone);
            SetResult("sLenContactTitle",              dataLoan.sLenContactTitle);
            SetResult("sLenContractD",                 dataLoan.sLenContractD_rep);
            SetResult("sLenLNumLckd",                  dataLoan.sLenLNumLckd);
            SetResult("sLenLNum",                      dataLoan.sLenLNum);
            SetResult("sLenNm",                        dataLoan.sLenNm);
            SetResult("sLenNum",                       dataLoan.sLenNum);
            SetResult("sLenState",                     dataLoan.sLenState);
            SetResult("sLenZip",                       dataLoan.sLenZip);
            SetResult("sLienPosT",                     dataLoan.sLienPosT);
            SetResult("sLtvR",                         dataLoan.sLtvR_rep);
            SetResult("sMOrigT",                       dataLoan.sMOrigT);
            SetResult("sNegCfLckd",                    dataLoan.sNegCfLckd.ToString());
            SetResult("sNoteIR",                       dataLoan.sNoteIR_rep);
            SetResult("sNprimAppsTotI",                dataLoan.sNprimAppsTotI_rep);
            SetResult("sNprimAppsTotSpPosCf",           dataLoan.sNprimAppsTotSpPosCf_rep);
            SetResult("sNprimAppsTotBaseI",            dataLoan.sNprimAppsTotBaseI_rep);
            SetResult("sNprimAppsTotNonbaseI",         dataLoan.sNprimAppsTotNonbaseI_rep);
            SetResult("sNprimAppsTotOIFrom1008",       dataLoan.sNprimAppsTotOIFrom1008_rep);
            SetResult("sOIFrom1008Desc",               dataLoan.sOIFrom1008Desc);
            SetResult("sOpNegCf",                      dataLoan.sOpNegCf_rep);
            SetResult("sPrimAppOIFrom1008",            dataLoan.sPrimAppOIFrom1008_rep);
            SetResult("sPrimAppTotBaseI",              dataLoan.sPrimAppTotBaseI_rep);
            SetResult("sPrimAppTotI",                  dataLoan.sPrimAppTotI_rep);
            SetResult("sPrimAppTotNonbaseI",           dataLoan.sPrimAppTotNonbaseI_rep);
            SetResult("sPrimAppTotSpPosCf",            dataLoan.sPrimAppTotSpPosCf_rep);
            SetResult("sProThisMPmt",                  dataLoan.sProThisMPmt_rep);
            SetResult("sProjNm",                       dataLoan.sProjNm);
            SetResult("sPurchPrice",                   dataLoan.sPurchPrice_rep);
            SetResult("sQualBottomR",                  dataLoan.sQualBottomR_rep);
            SetResult("sQualIR",                       dataLoan.sQualIR_rep);
            SetResult("sQualIRDeriveT",                dataLoan.sQualIRDeriveT );
            SetResult("sQualTopR",                     dataLoan.sQualTopR_rep);
            SetResult("sSpNegCf",                      dataLoan.sSpNegCf_rep);
            SetResult("sSubFin",                       dataLoan.sSubFin_rep);
            SetResult("sTerm",                         dataLoan.sTerm_rep);
            SetResult("sTransmOMonPmt",                dataLoan.sTransmOMonPmt_rep);
            SetResult("sTransmOtherProHExp",           dataLoan.sTransmOtherProHExp_rep);
            SetResult("sTransmPro1stMPmt",             dataLoan.sTransmPro1stMPmt_rep);
            SetResult("sTransmPro2ndMPmt",             dataLoan.sTransmPro2ndMPmt_rep);
            SetResult("sTransmProHazIns",              dataLoan.sTransmProHazIns_rep);
            SetResult("sTransmProHoAssocDues",         dataLoan.sTransmProHoAssocDues_rep);
            SetResult("sTransmProMIns",                dataLoan.sTransmProMIns_rep);
            SetResult("sTransmProRealETx",             dataLoan.sTransmProRealETx_rep);
            SetResult("sTransmProTotHExp",             dataLoan.sTransmProTotHExp_rep);
            SetResult("sTransmTotMonPmt",              dataLoan.sTransmTotMonPmt_rep);
            SetResult("sTransmUwerComments",           dataLoan.sTransmUwerComments);
            SetResult("sUnitsNum",                     dataLoan.sUnitsNum_rep);
            SetResult("sBalloonPmt",                   dataLoan.sBalloonPmt);
            SetResult("sBiweeklyPmt",                  dataLoan.sBiweeklyPmt);
            SetResult("sEstateHeldT",                  dataLoan.sEstateHeldT);
            SetResult("sGseRefPurposeT",               dataLoan.sGseRefPurposeT);
            SetResult("sGseSpT",                       dataLoan.sGseSpT);
            SetResult("sApprDriveBy", dataLoan.sApprDriveBy);
            SetResult("sApprFull", dataLoan.sApprFull);

            CAgentFields appraiser = dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser,E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("sApprComNm", appraiser.CompanyName);
            SetResult("sApprerLicNum", appraiser.LicenseNumOfAgent);
            SetResult("sApprerNm", appraiser.AgentName);

            CAgentFields underwritter = dataLoan.GetAgentOfRole( E_AgentRoleT.Underwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject );
            SetResult("sUwerNm", underwritter.AgentName);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.TransmMOriginator, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("TransmMOriginatorCompanyName", preparer.CompanyName);

            SetResult("sHcltvR", dataLoan.sHcltvR_rep);
            SetResult("sHcltvRLckd", dataLoan.sHcltvRLckd);
            SetResult("sDebtToHousingGapR", dataLoan.sDebtToHousingGapR_rep);
            SetResult("sDebtToHousingGapRLckd", dataLoan.sDebtToHousingGapRLckd);
            SetResult("sIsSpReviewNoAppr", dataLoan.sIsSpReviewNoAppr);
            SetResult("sSpReviewFormNum", dataLoan.sSpReviewFormNum);
            SetResult("sTransmFntc", dataLoan.sTransmFntc_rep);
            SetResult("sTransmFntcLckd", dataLoan.sTransmFntcLckd);
            SetResult("sVerifAssetAmt", dataLoan.sVerifAssetAmt_rep);
            SetResult("sFntcSrc", dataLoan.sFntcSrc);
            SetResult("sRsrvMonNumDesc", dataLoan.sRsrvMonNumDesc);
            SetResult("sInterestedPartyContribR", dataLoan.sInterestedPartyContribR_rep);
            SetResult("sIsManualUw", dataLoan.sIsManualUw);
            SetResult("sIsDuUw", dataLoan.sIsDuUw);
            SetResult("sIsLpUw", dataLoan.sIsLpUw);
            SetResult("sIsOtherUw", dataLoan.sIsOtherUw);
            SetResult("sOtherUwDesc", dataLoan.sOtherUwDesc);
            SetResult("sAusRecommendation", dataLoan.sAusRecommendation);
            SetResult("sLpAusKey", dataLoan.sLpAusKey);
            SetResult("sDuCaseId", dataLoan.sDuCaseId);
            SetResult("sLpDocClass", dataLoan.sLpDocClass);
            SetResult("sRepCrScore", dataLoan.sRepCrScore);
            SetResult("sIsCommLen", dataLoan.sIsCommLen ? "1" : "0");
            SetResult("sIsHOwnershipEdCertInFile", dataLoan.sIsHOwnershipEdCertInFile ? "1" : "0");
            SetResult("sIsMOrigBroker", dataLoan.sIsMOrigBroker);
            SetResult("sIsMOrigCorrespondent", dataLoan.sIsMOrigCorrespondent);
            SetResult("sWillEscrowBeWaived", dataLoan.sWillEscrowBeWaived ? "0" : "1");
            SetResult("sTransmBuydwnTermDesc", dataLoan.sTransmBuydwnTermDesc);
            SetResult("sFinMethPrintAsOtherDesc", dataLoan.sFinMethPrintAsOtherDesc);
            SetResult("sFinMethodPrintAsOther", dataLoan.sFinMethodPrintAsOther);
            SetResult("sSpProjectClassFannieT", dataLoan.sSpProjectClassFannieT);
            SetResult("sSpProjectClassFreddieT", dataLoan.sSpProjectClassFreddieT);
            SetResult("sCpmProjectId", dataLoan.sCpmProjectId);

            SetResult("sQualTermCalculationType", dataLoan.sQualTermCalculationType);
            SetResult("sQualTerm", dataLoan.sQualTerm_rep);

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V12_CalculateLevelOfPropertyReviewSection))
            {
                SetResult("sSpValuationMethodT", dataLoan.sSpValuationMethodT);
                SetResult("sSpAppraisalFormT", dataLoan.sSpAppraisalFormT);
            }
        }


        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "CalculateAssetTotal":
                    CalculateAssetTotal();
                    break;
            }
        }

        private void CalculateAssetTotal() 
        {
            Guid sLId = GetGuid("LoanId");

            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitLoad();

            // 5/10/2012 vm - OPM 25561 - Copy the value from aAsstLiqTot
            int nApps = dataLoan.nApps;

            decimal total = 0.0M;
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = dataLoan.GetAppData(i);
                total += dataApp.aAsstLiqTot;
            }

            SetResult("VerifiedAssetTotal", dataLoan.m_convertLos.ToMoneyString(total, FormatDirection.ToRep));

        }
    }

    public partial class Transmittal_04_CombinedService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize() 
        {
            AddBackgroundItem("", new Transmittal_04_CombinedServiceItem());
        }
    }
}
