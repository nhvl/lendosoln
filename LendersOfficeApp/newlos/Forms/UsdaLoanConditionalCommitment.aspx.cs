﻿using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class UsdaLoanConditionalCommitment : BaseLoanPage
    {
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            LenderZip.SmartZipcode(LenderCity, LenderState);
            base.OnInit(e);
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(UsdaLoanConditionalCommitment));
            dataLoan.InitLoad();

            var lender = dataLoan.GetPreparerOfForm(E_PreparerFormT.UsdaConditionalCommitment, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            LenderNm.Text = lender.CompanyName;
            LenderAddr.Text = lender.StreetAddr;
            LenderCity.Text = lender.City;
            LenderState.Value = lender.State;
            LenderZip.Text = lender.Zip;

            sUsdaBorrowerId.Text = dataLoan.sUsdaBorrowerId;
            sSpState.Value = dataLoan.sSpState;
            Tools.Bind_sSpCounty(dataLoan.sSpState, sSpCounty, true);
            Tools.SetDropDownListCaseInsensitive(sSpCounty, dataLoan.sSpCounty);
            sUsdaStateCode.Text = dataLoan.sUsdaStateCode;
            sUsdaCountyCode.Text = dataLoan.sUsdaCountyCode;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;

            sCombinedBorFirstNm.Text = dataLoan.sCombinedBorFirstNm;
            sCombinedBorMidNm.Text = dataLoan.sCombinedBorMidNm;
            sCombinedBorLastNm.Text = dataLoan.sCombinedBorLastNm;
            sCombinedBorSuffix.Text = dataLoan.sCombinedBorSuffix;
            sCombinedBorSsn.Text = dataLoan.sCombinedBorSsn;

            sFfUfmip1003.Text = dataLoan.sFfUfmip1003_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sUsdaAnnualFee.Text = dataLoan.sUsdaAnnualFee_rep;
            sMiCommitmentReceivedD.Text = dataLoan.sMiCommitmentReceivedD_rep;

        }

    }
}
