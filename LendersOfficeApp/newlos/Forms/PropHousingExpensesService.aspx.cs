﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos.Forms
{
    public class PropHousingExpensesServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(PropHousingExpensesServiceItem));
        }

        protected override void Process(string methodName)
        {
            switch(methodName)
            {
                case "Calculate":
                    Calculate();
                    break;
                case "Save":
                    Save();
                    break;
                default:
                    return;
            }
        }

        protected void Calculate()
        {
            string d = GetString("PageViewModel");
            PageViewModel model = null;
            if (!string.IsNullOrEmpty(d))
            {
                model = ObsoleteSerializationHelper.JsonDeserialize<PageViewModel>(d);
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "PageViewModel JSON string is null or empty");
            }

            CPageData dataLoan = this.ConstructPageDataClass(sLId);
            dataLoan.InitLoad();

            // Do this here so both methods use the same list.
            List<HousingExpenseViewModel> allExp = new List<HousingExpenseViewModel>();
            allExp.AddRange(model.OtherExps);
            allExp.AddRange(model.PropInsExps);
            allExp.AddRange(model.TaxesExps);

            this.BindToDataLoan(dataLoan, model, allExp);

            PageViewModel newModel = this.CreateModel(dataLoan, model, allExp);

            SetResult("PageViewModel", ObsoleteSerializationHelper.JsonSerialize(newModel));
        }

        protected void Save()
        {
            string d = GetString("PageViewModel");
            PageViewModel model = null;
            if (!string.IsNullOrEmpty(d))
            {
                model = ObsoleteSerializationHelper.JsonDeserialize<PageViewModel>(d);
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "PageViewModel JSON string is null or empty");
            }

            CPageData dataLoan = this.ConstructPageDataClass(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Do this here so both methods use the same list.
            List<HousingExpenseViewModel> allExp = new List<HousingExpenseViewModel>();
            allExp.AddRange(model.OtherExps);
            allExp.AddRange(model.PropInsExps);
            allExp.AddRange(model.TaxesExps);

            this.BindToDataLoan(dataLoan, model, allExp);

            dataLoan.Save();

            PageViewModel newModel = this.CreateModel(dataLoan, model, allExp);

            SetResult("PageViewModel", ObsoleteSerializationHelper.JsonSerialize(newModel));
        }


        private void BindToDataLoan(CPageData dataLoan, PageViewModel oldPageModel, List<HousingExpenseViewModel> allExp)
        {
            foreach (HousingExpenseViewModel expModel in allExp)
            {
                BindExpense(dataLoan, expModel);
            }

            // The rest of the fields.
            dataLoan.sEstCloseDLckd = oldPageModel.sEstCloseDLckd;
            dataLoan.sEstCloseD_rep = oldPageModel.sEstCloseD;
            dataLoan.sDocMagicClosingD_rep = oldPageModel.sDocMagicClosingD;
            dataLoan.sSchedDueD1Lckd = oldPageModel.sSchedDueD1Lckd;
            dataLoan.sSchedDueD1_rep = oldPageModel.sSchedDueD1;
            dataLoan.sProMIns_rep = oldPageModel.sProMIns;
            dataLoan.sMInsRsrvEscrowedTri = oldPageModel.sMInsRsrvEscrowedTri ? E_TriState.Yes : E_TriState.No;
            int result;
            dataLoan.sHousingExpenses.MIEscrowSchedule[0] = int.TryParse(oldPageModel.MICush, out result) ? result : 0;
            dataLoan.sMInsRsrvMonLckd = oldPageModel.sMInsRsrvMonLckd;
            dataLoan.sMInsRsrvMon_rep = oldPageModel.sMInsRsrvMon;
            dataLoan.sAggEscrowCalcModeT = oldPageModel.sAggEscrowCalcModeT;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = oldPageModel.sCustomaryEscrowImpoundsCalcMinT;
            dataLoan.sAggregateAdjRsrv_rep = oldPageModel.sAggregateAdjRsrv;
            dataLoan.sAggregateAdjRsrvLckd = oldPageModel.sAggregateAdjRsrvLckd;

            if (oldPageModel.sNonMIHousingExpensesEscrowedReasonT_BorrowerRequested)
            {
                dataLoan.sNonMIHousingExpensesEscrowedReasonT = E_sNonMIHousingExpensesEscrowedReasonT.BorrowerRequested;
            }
            else if (oldPageModel.sNonMIHousingExpensesEscrowedReasonT_LenderRequired)
            {
                dataLoan.sNonMIHousingExpensesEscrowedReasonT = E_sNonMIHousingExpensesEscrowedReasonT.LenderRequired;
            }

            if (oldPageModel.sNonMIHousingExpensesNotEscrowedReasonT_Declined)
            {
                dataLoan.sNonMIHousingExpensesNotEscrowedReasonT = E_sNonMIHousingExpensesNotEscrowedReasonT.BorrowerDeclined;
            }
            else if (oldPageModel.sNonMIHousingExpensesNotEscrowedReasonT_NotOffered)
            {
                dataLoan.sNonMIHousingExpensesNotEscrowedReasonT = E_sNonMIHousingExpensesNotEscrowedReasonT.LenderDidNotOffer;
            }
        }

        private void BindExpense(CPageData dataLoan, HousingExpenseViewModel viewModel)
        {
            BaseHousingExpense expense = dataLoan.GetExpense(viewModel.expenseType, viewModel.customLineNum);

            expense.IsEscrowedAtClosing = viewModel.isEscrowed ? E_TriState.Yes : E_TriState.No;
            expense.AnnualAmtCalcBasePerc_rep = viewModel.annAmtPerc;
            expense.AnnualAmtCalcBaseType = viewModel.annAmtBase;
            expense.MonthlyAmtFixedAmt_rep = viewModel.monAmtFixed;
            expense.IsPrepaid = viewModel.isPrepaid;
            expense.PrepaidMonths_rep = viewModel.prepMnth;
            expense.ReserveMonthsLckd = viewModel.rsrvMonLckd;
            expense.ReserveMonths_rep = viewModel.rsrvMon;
            expense.SetCustomLineNumber(viewModel.customLineNum);
            expense.TaxType = viewModel.taxT;
            expense.ExpenseDescription = viewModel.expDesc;
            expense.DisbursementRepInterval = viewModel.RepInterval;

            int result;
            int cushion = int.TryParse(viewModel.Cush, out result) ? result : 0;
            List<int> disbursementScheduleMonths = new List<int>();
            disbursementScheduleMonths.Add(cushion);
            disbursementScheduleMonths.AddRange(viewModel.DisbSched.Select((value) => int.TryParse(value, out result) ? result : 0));
            expense.DisbursementScheduleMonths = disbursementScheduleMonths.ToArray();

            if (viewModel.flippedOnce)
            {
                expense.ClearDisbursements();
            }

            // If was in Disbursement mode, then bind the disbursements.
            if (viewModel.calcMode == E_AnnualAmtCalcTypeT.Disbursements)
            {
                BindDisbursements(viewModel.Disbursements, expense);
            }

            // Remove any lingering disbursements
            string[] removedDisbs = viewModel.removedDisbs.Split(',');
            RemoveDisbursements(removedDisbs, expense);

            expense.AnnualAmtCalcType = viewModel.annAmtCalcT;

            // Set flipped bit somewhere here

            // Ensure that no actual disbursements exist while in calc mode
            if (expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.LoanValues || viewModel.calcMode == E_AnnualAmtCalcTypeT.LoanValues)
            {
                expense.ClearDisbursements();
            }

            // If switching from calc to disbursement, then transform projected to actual disbursements.
            if (viewModel.calcMode == E_AnnualAmtCalcTypeT.LoanValues && expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                ConvertProjectedToActual(viewModel.Disbursements, expense);
            }
        }

        private void BindDisbursements(List<DisbursementViewModel> disbs, BaseHousingExpense expense)
        {
            foreach (DisbursementViewModel dview in disbs)
            {
                if (dview.DisbT == E_DisbursementTypeT.Projected)
                {
                    continue;
                }

                SingleDisbursement disb = expense.GetDisbursementById(new Guid(dview.DisbId));
                if (disb == null && (dview.DisbT == E_DisbursementTypeT.Actual))
                {
                    disb = new SingleDisbursement();
                    disb.DisbursementType = E_DisbursementTypeT.Actual;
                    disb.DisbursementAmtLckd = true;
                    expense.AddDisbursement(disb);
                }

                disb.DisbPaidDateType = dview.PaidDT;
                if (string.IsNullOrEmpty(dview.DueD))
                {
                    disb.DueDate = expense.GetStartDateWindow();
                }
                else
                {
                    disb.DueDate_rep = dview.DueD;
                }
                disb.DisbursementAmt_rep = dview.DueAmt;
                disb.CoveredMonths_rep = dview.DisbMon;
                disb.PaidDate_rep = dview.DisbPaidD;
                disb.PaidBy = dview.PaySource;
                disb.BillingPeriodStartD_rep = dview.PaidFromD;
                disb.BillingPeriodEndD_rep = dview.PaidToD;
                disb.IsPaidFromEscrowAcc = dview.IsPaidFromEscrow;
            }
        }

        private void RemoveDisbursements(string[] removedDisbs, BaseHousingExpense expense)
        {
            foreach (string disbId in removedDisbs)
            {
                if (string.IsNullOrEmpty(disbId))
                {
                    continue;
                }

                Guid id = new Guid(disbId);
                expense.RemoveDisbursement(id);
            }
        }

        private void ConvertProjectedToActual(List<DisbursementViewModel> disbs, BaseHousingExpense expense)
        {
            foreach (DisbursementViewModel dview in disbs)
            {
                if (dview.DisbT == E_DisbursementTypeT.Actual)
                {
                    continue;
                }

                SingleDisbursement disb = new SingleDisbursement();
                disb.DisbursementType = E_DisbursementTypeT.Actual;
                disb.DisbursementAmtLckd = true;
                expense.AddDisbursement(disb);

                disb.DisbPaidDateType = (E_DisbPaidDateType)dview.PaidDT;
                disb.DueDate_rep = dview.DueD;
                disb.DisbursementAmt_rep = dview.DueAmt;
                disb.CoveredMonths_rep = dview.DisbMon;
                disb.PaidDate_rep = dview.DisbPaidD;
                disb.PaidBy = (E_DisbursementPaidByT)dview.PaySource;
                disb.BillingPeriodStartD_rep = dview.PaidFromD;
                disb.BillingPeriodEndD_rep = dview.PaidToD;
                disb.IsPaidFromEscrowAcc = dview.IsPaidFromEscrow;
            }
        }


        private PageViewModel CreateModel(CPageData dataLoan, PageViewModel oldPageModel, List<HousingExpenseViewModel> allExp)
        {
            PageViewModel newModel = new PageViewModel();
            foreach (HousingExpenseViewModel oldExpModel in allExp)
            {
                BaseHousingExpense expense = dataLoan.GetExpense(oldExpModel.expenseType, oldExpModel.customLineNum);

                bool flipped = oldExpModel.flippedOnce || (oldExpModel.calcMode == E_AnnualAmtCalcTypeT.Disbursements && expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.LoanValues);
                HousingExpenseViewModel newExpViewModel = this.CreateHousingExpenseModel(dataLoan, expense, oldExpModel.removedDisbs, flipped);

                if (newExpViewModel.IsInsurance)
                {
                    newModel.PropInsExps.Add(newExpViewModel);
                    newExpViewModel.Category = "PropInsExps";
                }
                else if (newExpViewModel.IsTax)
                {
                    newModel.TaxesExps.Add(newExpViewModel);
                    newExpViewModel.Category = "TaxesExps";
                }
                else if (newExpViewModel.IsOther)
                {
                    newModel.OtherExps.Add(newExpViewModel);
                    newExpViewModel.Category = "OtherExps";
                }

                if (expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
                {
                    newModel.DisbModeExpenses.Add(expense.DescriptionOrDefault);
                }

                if (expense.MonthlyAmtTotal != 0)
                {
                    if (expense.IsEscrowedAtClosing == E_TriState.Yes)
                    {
                        newModel.EscrowedDescriptions.Add(expense.DescriptionOrDefault);
                    }
                    else
                    {
                        newModel.NonEscrowedDescriptions.Add(expense.DescriptionOrDefault);
                    }
                }
            }

            // Rest of the fields
            newModel.sEstCloseD = dataLoan.sEstCloseD_rep;
            newModel.sEstCloseDLckd = dataLoan.sEstCloseDLckd;
            newModel.sDocMagicClosingD = dataLoan.sDocMagicClosingD_rep;
            newModel.sSchedDueD1 = dataLoan.sSchedDueD1_rep;
            newModel.sSchedDueD1Lckd = dataLoan.sSchedDueD1Lckd;
            newModel.sProMIns = dataLoan.sProMIns_rep;
            newModel.sMInsRsrvEscrowedTri = dataLoan.sMInsRsrvEscrowedTri == E_TriState.Yes;
            newModel.sIssMInsRsrvEscrowedTriReadOnly = dataLoan.sIssMInsRsrvEscrowedTriReadOnly;
            newModel.sMInsRsrv = dataLoan.sMInsRsrv_rep;
            newModel.sMInsRsrvMon = dataLoan.sMInsRsrvMon_rep;
            newModel.MICush = dataLoan.sHousingExpenses.MIEscrowSchedule[0].ToString();
            newModel.sMInsRsrvMonLckd = dataLoan.sMInsRsrvMonLckd;
            newModel.sIsMipPrepaid = dataLoan.sIsMipPrepaid;
            newModel.sMipPiaMon = dataLoan.sMipPiaMon_rep;
            newModel.sMipPia = dataLoan.sMipPia_rep;
            newModel.sRecurringMipPia = dataLoan.sRecurringMipPia_rep;
            newModel.sAggEscrowCalcModeT = dataLoan.sAggEscrowCalcModeT;
            newModel.sCustomaryEscrowImpoundsCalcMinT = dataLoan.sCustomaryEscrowImpoundsCalcMinT;
            newModel.sAggregateAdjRsrv = dataLoan.sAggregateAdjRsrv_rep;
            newModel.sAggregateAdjRsrvLckd = dataLoan.sAggregateAdjRsrvLckd;
            newModel.sGfeInitialImpoundDeposit = dataLoan.sGfeInitialImpoundDeposit_rep;
            newModel.sClosingCostFeeVersionT = dataLoan.sClosingCostFeeVersionT;
            newModel.AnySetToAny = dataLoan.sHousingExpenses.IsAnyExpenseSetToAny() && dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated;
            newModel.sServicingEscrowPmtNonMI = dataLoan.sServicingEscrowPmtNonMI_rep;
            newModel.sMonthlyNonPIExpenseTotalPITI = dataLoan.sMonthlyNonPIExpenseTotalPITI_rep;
            newModel.sServicingEscrowPmt = dataLoan.sServicingEscrowPmt_rep;
            newModel.sTotalPropertyInsurancePITI = dataLoan.sTotalPropertyInsurancePITI_rep;
            newModel.sTotalRealtyTaxesPITI = dataLoan.sTotalRealtyTaxesPITI_rep;
            newModel.sTotalOtherHousingExpensesPITI = dataLoan.sTotalOtherHousingExpensesPITI_rep;
            newModel.sTridEscrowAccountExists = dataLoan.sTridEscrowAccountExists;
            newModel.sTridEscrowAccountExists_True = dataLoan.sTridEscrowAccountExists;
            newModel.sTridEscrowAccountExists_False = !dataLoan.sTridEscrowAccountExists;
            newModel.sNonMIHousingExpensesEscrowedReasonT_LenderRequired = dataLoan.sNonMIHousingExpensesEscrowedReasonT == E_sNonMIHousingExpensesEscrowedReasonT.LenderRequired;
            newModel.sNonMIHousingExpensesEscrowedReasonT_BorrowerRequested = dataLoan.sNonMIHousingExpensesEscrowedReasonT == E_sNonMIHousingExpensesEscrowedReasonT.BorrowerRequested;
            newModel.sClosingDisclosureEscrowedPropertyCostsFirstYear = dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep;
            newModel.sClosingDisclosureNonEscrowedPropertyCostsFirstYear = dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep;
            newModel.sClosingDisclosureMonthlyEscrowPayment = dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep;
            newModel.sNonMIHousingExpensesNotEscrowedReasonT_Declined = dataLoan.sNonMIHousingExpensesNotEscrowedReasonT == E_sNonMIHousingExpensesNotEscrowedReasonT.BorrowerDeclined;
            newModel.sNonMIHousingExpensesNotEscrowedReasonT_NotOffered = dataLoan.sNonMIHousingExpensesNotEscrowedReasonT == E_sNonMIHousingExpensesNotEscrowedReasonT.LenderDidNotOffer;
            newModel.sClosingDisclosurePropertyCostsFirstYear = dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep;
            newModel.sTRIDEscrowWaiverFee = dataLoan.sTRIDEscrowWaiverFee_rep;

            return newModel;
        }

        private HousingExpenseViewModel CreateHousingExpenseModel(CPageData dataLoan, BaseHousingExpense exp, string remDisbs, bool flippedOnce)
        {
            HousingExpenseViewModel model = new HousingExpenseViewModel();

            model.IsInsurance = exp.IsInsurance;
            model.IsTax = exp.IsTax;
            model.IsOther = !exp.IsTax && !exp.IsInsurance;
            model.expenseType = exp.HousingExpenseType;
            model.taxT = exp.TaxType;
            model.isPrepaid = exp.IsPrepaid;
            model.IsCustom = exp.CanHaveCustomLineNum;
            model.removedDisbs = remDisbs;
            model.customLineNumAsString = exp.LineNumAsString;
            model.customLineNum = exp.CustomExpenseLineNum;
            model.calcMode = exp.AnnualAmtCalcType;
            model.flippedOnce = flippedOnce;
            model.expName = exp.DefaultExpenseDescription;
            model.expDesc = exp.ExpenseDescription;
            model.expPrefix = exp.ExpensePrefix;
            model.isEscrowed = exp.IsEscrowedAtClosing == E_TriState.Yes;
            model.annAmtCalcT = exp.AnnualAmtCalcType;
            model.annAmtTot = exp.AnnualAmt_rep;
            model.monTotPITI = exp.MonthlyAmtTotal_rep;
            model.prepMnth = exp.PrepaidMonths_rep;
            model.prepAmt = exp.PrepaidAmt_rep;
            model.monTotServ = exp.MonthlyAmtServicing_rep;
            model.rsrvMon = exp.ReserveMonths_rep;
            model.rsrvMonLckd = exp.ReserveMonthsLckd;
            model.rsrvAmt = exp.ReserveAmt_rep;
            model.annAmtPerc = exp.AnnualAmtCalcBasePerc_rep;
            model.annAmtBase = exp.AnnualAmtCalcBaseType;
            model.monAmtFixed = exp.MonthlyAmtFixedAmt_rep;
            model.RepInterval = exp.DisbursementRepInterval;
            model.firstDisbD = exp.FirstDisbDate_rep;

            model.DisbSched = exp.DisbursementScheduleMonths.Skip(1).Select((value) => value.ToString()).ToArray();
            model.Cush = exp.DisbursementScheduleMonths[0].ToString();

            IEnumerable<SingleDisbursement> disbList;
            if (exp.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.LoanValues)
            {
                disbList = exp.ProjectedDisbursements.OrderBy(o => o.DueDate);
            }
            else
            {
                disbList = exp.ActualDisbursementsList;
            }

            model.Disbursements = new List<DisbursementViewModel>();
            foreach (SingleDisbursement disb in disbList)
            {
                model.Disbursements.Add(CreateDisbursementModel(disb));
            }
            return model;
        }

        private DisbursementViewModel CreateDisbursementModel(SingleDisbursement disb)
        {
            DisbursementViewModel model = new DisbursementViewModel();

            model.DisbT = disb.DisbursementType;
            model.PaidDT = disb.DisbPaidDateType;
            model.DueD = disb.DueDate_rep;
            model.DueAmt = disb.DisbursementAmt_rep;
            model.DisbMon = disb.CoveredMonths_rep;
            model.DisbPaidD = disb.PaidDate_rep;
            model.PaySource = disb.PaidBy;
            model.PaidFromD = disb.BillingPeriodStartD_rep;
            model.PaidToD = disb.BillingPeriodEndD_rep;
            model.DisbId = disb.DisbId.ToString();
            model.IsPaidFromEscrow = disb.IsPaidFromEscrowAcc;

            return model;
        }

        // Not used
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            throw new NotImplementedException();
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            throw new NotImplementedException();
        }
    }

    public partial class PropHousingExpensesService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new PropHousingExpensesServiceItem());
        }
    }
}
