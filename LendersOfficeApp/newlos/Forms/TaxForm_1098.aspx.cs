﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.Migration;

namespace LendersOfficeApp.newlos.Forms
{
    public partial class TaxForm_1098 : BaseLoanPage
    {
        private CPageData dataLoan;

        private int? selectedYear;

        protected override void LoadData()
        {
            this.RegisterJsScript("LQBPopup.js");

            sTax1098IsCorrected.Checked = this.dataLoan.sTax1098IsCorrected;
            IPreparerFields tax1098 = this.dataLoan.GetPreparerOfForm(E_PreparerFormT.Tax1098, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            Tax1098CompanyName.Text = tax1098.CompanyName;
            Tax1098StreetAddr.Text = tax1098.StreetAddr;
            Tax1098City.Text = tax1098.City;
            Tax1098State.Value = tax1098.State;
            Tax1098Zip.Text = tax1098.Zip;
            Tax1098Phone.Text = tax1098.Phone;

            Tax1098TaxID.Text = tax1098.TaxId;
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.dataLoan.sLoanVersionT, LoanVersionT.V18_ConsolidateTaxIdFields))
            {
                Tax1098TaxID.Attributes.Add("preset", "employerIdentificationNumber");
            }

            CFM.IsLocked = tax1098.IsLocked;
            CFM.AgentRoleT = tax1098.AgentRoleT;

            sTax1098PayerName.Text = this.dataLoan.sTax1098PayerName;
            sTax1098PayerAddr.Text = this.dataLoan.sTax1098PayerAddr;
            sTax1098PayerCity.Text = this.dataLoan.sTax1098PayerCity;
            sTax1098PayerState.Value = this.dataLoan.sTax1098PayerState;
            sTax1098PayerZip.Text = this.dataLoan.sTax1098PayerZip;
            sTax1098PayerSsn.Text = this.dataLoan.sTax1098PayerSsn;

            sTax1098AccountNum.Text = this.dataLoan.sTax1098AccountNum;
            sTax1098Interest.Text = this.dataLoan.sTax1098Interest_rep;
            sTax1098PointPaid.Text = this.dataLoan.sTax1098PointPaid_rep;
            sTax1098InterestRefund.Text = this.dataLoan.sTax1098InterestRefund_rep;
            sTax1098MIP.Text = this.dataLoan.sTax1098MIP_rep;
            sTax1098OtherInfo.Text = this.dataLoan.sTax1098OtherInfo;
            sTax1098OtherInfo2.Text = this.dataLoan.sTax1098OtherInfo2;

            sTax1098OutstandingPrincipal.Text = this.dataLoan.sTax1098OutstandingPrincipal_rep;
            sTax1098OriginationDate.Text = this.dataLoan.sTax1098OriginationDate_rep;
            sTax1098OriginationDateLckd.Checked = this.dataLoan.sTax1098OriginationDateLckd;
            sTax1098IsPropertyAddrBorrowerAddr.Checked = this.dataLoan.sTax1098IsPropertyAddrBorrowerAddr;
            sTax1098IsPropertyAddrBorrowerAddrLckd.Checked = this.dataLoan.sTax1098IsPropertyAddrBorrowerAddrLckd;
            sTax1098NumberOfPropertiesSecuringThisMortgage.Text = this.dataLoan.sTax1098NumberOfPropertiesSecuringThisMortgage_rep;

            sSpAddr.Text = this.dataLoan.sSpAddr;
            sSpCity.Text = this.dataLoan.sSpCity;
            sSpState.Value = this.dataLoan.sSpState;
            sSpZip.Text = this.dataLoan.sSpZip;
            sSpZip.SmartZipcode(sSpCity, sSpState);
            sAssessorsParcelId.Text = this.dataLoan.sAssessorsParcelId;

            ClientScript.RegisterHiddenField("version", this.selectedYear.ToString());

            Tools.Bind_1098FilingYearT(s1098ReportingYear, this.dataLoan.sCreatedD.DateTimeForComputation.Year);            

            Tools.SetDropDownListValue(s1098ReportingYear, this.dataLoan.s1098ReportingYear.HasValue ? this.dataLoan.s1098ReportingYear.ToString() : string.Empty);
            Tools.SetDropDownListValue(s1098PointsCalcT, this.dataLoan.s1098PointsCalcT);

            sTax1098PayerNameLckd.Checked = this.dataLoan.sTax1098PayerNameLckd;
            sTax1098PayerAddrLckd.Checked = this.dataLoan.sTax1098PayerAddrLckd;
            sTax1098PayerSsnLckd.Checked = this.dataLoan.sTax1098PayerSsnLckd;
            sTax1098InterestLckd.Checked = this.dataLoan.sTax1098InterestLckd;
            sTax1098OutstandingPrincipalLckd.Checked = this.dataLoan.sTax1098OutstandingPrincipalLckd;
            sTax1098AccountNumLckd.Checked = this.dataLoan.sTax1098AccountNumLckd;
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            this.PageTitle = "1098 Tax Form";
            this.PageID = "Tax1098";

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);

            this.dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(TaxForm_1098));
            this.dataLoan.InitLoad();

            this.selectedYear = this.dataLoan.s1098ReportingYear;

            switch (this.selectedYear)
            {
                case 2012:
                    this.PDFPrintClass = typeof(LendersOffice.Pdf.CTaxForm_1098_2012PDF);
                    break;
                case 2013: // 1/17/2014 tj - opm 106154 For 2013, form is virtually same as 2011
                    this.PDFPrintClass = typeof(LendersOffice.Pdf.CTaxForm_1098_2013PDF);
                    break;
                case 2014:
                    this.PDFPrintClass = typeof(LendersOffice.Pdf.CTaxForm_1098_2014PDF);
                    break;
                case 2016:
                    this.PDFPrintClass = typeof(LendersOffice.Pdf.CTaxForm_1098_2016PDF);
                    break;
                case 2017:
                    this.PDFPrintClass = typeof(LendersOffice.Pdf.CTaxForm_1098_2017PDF);
                    break;
                case 2018:
                    this.PDFPrintClass = typeof(LendersOffice.Pdf.CTaxForm_1098_2018PDF);
                    break;
                default:
                    this.PDFPrintClass = typeof(LendersOffice.Pdf.CTaxForm_1098_2011PDF);
                    break;
            }            

            //CFM.Type = "19";
            CFM.CompanyNameField = Tax1098CompanyName.ClientID;
            CFM.StreetAddressField = Tax1098StreetAddr.ClientID;
            CFM.CityField = Tax1098City.ClientID;
            CFM.StateField = Tax1098State.ClientID;
            CFM.ZipField = Tax1098Zip.ClientID;
            CFM.PhoneField = Tax1098Phone.ClientID;
            CFM.TaxIdField = Tax1098TaxID.ClientID;
            CFM.IsAllowLockableFeature = true;

            Tax1098Zip.SmartZipcode(Tax1098City, Tax1098State);
            sTax1098PayerZip.SmartZipcode(sTax1098PayerCity, sTax1098PayerState);

            Tools.Bind_1098PointsCalcT(s1098PointsCalcT);

            RegisterJsGlobalVariables("appid", this.dataLoan.GetAppData(0).aAppId);
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
