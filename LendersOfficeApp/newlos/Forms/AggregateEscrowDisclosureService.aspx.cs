using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos.Forms
{
    public class AggregateEscrowDisclosureServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(AggregateEscrowDisclosureServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sSchedDueD1Lckd = GetBool("sSchedDueD1Lckd");
            dataLoan.sSchedDueD1_rep = GetString("sSchedDueD1");

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.AggregateEscrowServicer, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("AggregateEscrowServicerCompanyName");
            preparer.StreetAddr = GetString("AggregateEscrowServicerStreetAddr");
            preparer.City = GetString("AggregateEscrowServicerCity");
            preparer.State = GetString("AggregateEscrowServicerState");
            preparer.Zip = GetString("AggregateEscrowServicerZip");
            preparer.PhoneOfCompany = GetString("AggregateEscrowServicerPhoneOfCompany");
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew);
            preparer.PrepareDate_rep = GetString("GfeTilPrepareDate");
            preparer.Update();
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            AggregateEscrowAccount escrowAccount = dataLoan.sAggregateEscrowAccount;
            SetResult("sAggrEscrowInitialDeposit", escrowAccount.InitialDeposit_rep);
            SetResult("AggregateEscrowAdjustment", escrowAccount.AggregateEscrowAdjustment_rep);

            string sEscrowPmt = dataLoan.sEscrowPmt_rep;

            for (int i = 0; i < 12; i++)
            {
                SetResult("sAggrEscrowMon" + i, escrowAccount.AggregateItems[i].Mon);
                SetResult("sEscrowPmt" + i, sEscrowPmt);
                SetResult("sAggrEscrowPmtFromEscrow" + i, escrowAccount.AggregateItems[i].PmtFromEscrow_rep);
                SetResult("sAggrEscrowDesc" + i, escrowAccount.AggregateItems[i].Desc);
                SetResult("sAggrEscrowBal" + i, escrowAccount.AggregateItems[i].Bal_rep);
            }

            SetResult("sSchedDueD1Lckd", dataLoan.sSchedDueD1Lckd);
            SetResult("sSchedDueD1", dataLoan.sSchedDueD1_rep);
        }
    }

	public partial class AggregateEscrowDisclosureService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize()
        {
            AddBackgroundItem("", new AggregateEscrowDisclosureServiceItem());
        }
        //protected override void Process(string methodName) 
        //{
        //    switch (methodName) 
        //    {
        //        case "SaveData":
        //            SaveData();
        //            break;
        //        case "CalculateData":
        //            CalculateData();
        //            break;
        //    }
        //}
        //private void SaveData() 
        //{
        //    Guid loanId = GetGuid("LoanID");
        //    int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
        //    CPageBase dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(AggregateEscrowDisclosureService));
        //    dataLoan.InitSave(sFileVersion);

        //    dataLoan.sSchedDueD1_rep = GetString("sSchedDueD1");

        //    CPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.AggregateEscrowServicer, E_ReturnOptionIfNotExist.CreateNew);
        //    preparer.CompanyName = GetString("AggregateEscrowServicerCompanyName");
        //    preparer.StreetAddr = GetString("AggregateEscrowServicerStreetAddr");
        //    preparer.City = GetString("AggregateEscrowServicerCity");
        //    preparer.State = GetString("AggregateEscrowServicerState");
        //    preparer.Zip = GetString("AggregateEscrowServicerZip");
        //    preparer.PhoneOfCompany = GetString("AggregateEscrowServicerPhoneOfCompany");
        //    preparer.Update();

        //    preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew);
        //    preparer.PrepareDate_rep = GetString("GfeTilPrepareDate");
        //    preparer.Update();

        //    dataLoan.Save();

        //}
        //private void CalculateData() 
        //{

        //    Guid loanId = GetGuid("LoanID");
        //    CPageBase dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(AggregateEscrowDisclosureService));
        //    dataLoan.InitLoad();

        //    dataLoan.sSchedDueD1_rep = GetString("sSchedDueD1");
        //    AggregateEscrowAccount escrowAccount = dataLoan.sAggregateEscrowAccount;
        //    SetResult("sAggrEscrowInitialDeposit", escrowAccount.InitialDeposit_rep);
        //    SetResult("AggregateEscrowAdjustment", escrowAccount.AggregateEscrowAdjustment_rep);

        //    string sEscrowPmt = dataLoan.sEscrowPmt_rep;

        //    for (int i = 0; i < 12; i++) 
        //    {
        //        SetResult("sAggrEscrowMon" + i, escrowAccount.AggregateItems[i].Mon);
        //        SetResult("sEscrowPmt" + i, sEscrowPmt);
        //        SetResult("sAggrEscrowPmtFromEscrow" + i, escrowAccount.AggregateItems[i].PmtFromEscrow_rep);
        //        SetResult("sAggrEscrowDesc" + i, escrowAccount.AggregateItems[i].Desc);
        //        SetResult("sAggrEscrowBal" + i, escrowAccount.AggregateItems[i].Bal_rep);
        //    }
        //}
	}
}
