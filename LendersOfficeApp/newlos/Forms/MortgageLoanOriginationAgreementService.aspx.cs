using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Forms
{
    public class MortgageLoanOriginationAgreementServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(MortgageLoanOriginationAgreementServiceItem));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.MortgageLoanOrigAgreement, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("MortgageLoanOrigAgreement_CompanyName");
            preparer.StreetAddr = GetString("MortgageLoanOrigAgreement_StreetAddr");
            preparer.City = GetString("MortgageLoanOrigAgreement_City");
            preparer.State = GetString("MortgageLoanOrigAgreement_State");
            preparer.Zip = GetString("MortgageLoanOrigAgreement_Zip");
            preparer.PhoneOfCompany = GetString("MortgageLoanOrigAgreement_PhoneOfCompany");
            preparer.FaxOfCompany = GetString("MortgageLoanOrigAgreement_FaxOfCompany");
            preparer.PrepareDate_rep = GetString("MortgageLoanOrigAgreement_PrepareDate");
            preparer.Update();
            dataLoan.sMBrokerNmOfLaw = GetString("sMBrokerNmOfLaw");
        }
    }
	/// <summary>
	/// Summary description for MortgageLoanOriginationAgreementService.
	/// </summary>
	public partial class MortgageLoanOriginationAgreementService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new MortgageLoanOriginationAgreementServiceItem());

        }
	}
}
