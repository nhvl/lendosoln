using System;
using System.Collections.Generic;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Migration;
using LendersOfficeApp.newlos.Status;

namespace LendersOfficeApp.newlos.Forms
{
    public partial  class Loan1003pg3 : BaseLoanUserControl, IAutoLoadUserControl
	{
        /// <summary>
        /// Saves data to the loan from this page's controls.
        /// </summary>
        /// <param name="dataLoan">The loan to bind to.</param>
        /// <param name="dataApp">The app to bind to.</param>
        /// <param name="serviceItem">The service item calling this method.</param>
        internal static void BindDataFromControls(CPageData dataLoan, CAppData dataApp, AbstractBackgroundServiceItem serviceItem)
        {
            HmdaRaceEthnicityData.BindHmdaRaceEthnicityData(dataApp, serviceItem, $"{nameof(HREData)}_");
        }

        /// <summary>
        /// Loads the data from the loan to this page's controls.
        /// </summary>
        /// <param name="dataLoan">The loan to load from.</param>
        /// <param name="dataApp">The app to load from.</param>
        /// <param name="serviceItem">The service item.</param>
        internal static void LoadDataForControls(CPageData dataLoan, CAppData dataApp, AbstractBackgroundServiceItem serviceItem)
        {
            HmdaRaceEthnicityData.LoadHmdaRaceEthnicityData(dataApp, serviceItem, $"{nameof(HREData)}_");
        }

        #region "Member variables"
        protected MeridianLink.CommonControls.MoneyTextBox sConcurSubFin;
		protected ContactFieldMapper CFM;
        protected bool m_isPurchase = false;

        #endregion 

        private void BindOtherCreditDescription(MeridianLink.CommonControls.ComboBox cb) 
        {
            cb.Items.Add("Cash Deposit on sales contract");
            cb.Items.Add("Seller Credit");
            cb.Items.Add("Lender Credit");
            cb.Items.Add("Relocation Funds");
            cb.Items.Add("Employer Assisted Housing");
            cb.Items.Add("Lease Purchase Fund");
            cb.Items.Add("Borrower Paid Fees");
            cb.Items.Add("Paid Outside of Closing");
			cb.Items.Add("Broker Credit");

        }
        protected void PageInit(object sender, System.EventArgs e) 
        {
            Tools.Bind_aIntrvwrMethodT(aIntrvwrMethodT);
            Tools.Bind_aDecPastOwnedPropT(aBDecPastOwnedPropT);
            Tools.Bind_aDecPastOwnedPropTitleT(aBDecPastOwnedPropTitleT);
            Tools.Bind_aDecPastOwnedPropT(aCDecPastOwnedPropT);
            Tools.Bind_aDecPastOwnedPropTitleT(aCDecPastOwnedPropTitleT);
            BindOtherCreditDescription(sOCredit1Desc);
            BindOtherCreditDescription(sOCredit2Desc);
            BindOtherCreditDescription(sOCredit3Desc);
            BindOtherCreditDescription(sOCredit4Desc);
            BrokerZip.SmartZipcode(BrokerCity, BrokerState);


			// OPM 17652 - Ethan
            CFM.IsAllowLockableFeature = true;
			CFM.Type				="19";
			CFM.AgentNameField		= LoanOfficerName.ClientID;			
			CFM.PhoneField			= LoanOfficerPhone.ClientID;
            CFM.EmailField          = LoanOfficerEmail.ClientID;
            CFM.StreetAddressField  = BrokerStreetAddr.ClientID;
            CFM.CityField           = BrokerCity.ClientID;
            CFM.StateField          = BrokerState.ClientID;
            CFM.ZipField            = BrokerZip.ClientID;
            CFM.CompanyNameField	= BrokerName.ClientID;
            CFM.CompanyStreetAddr   = BrokerStreetAddr.ClientID;
            CFM.CompanyCity         = BrokerCity.ClientID;
            CFM.CompanyState        = BrokerState.ClientID;
            CFM.CompanyZip          = BrokerZip.ClientID;
			CFM.CompanyPhoneField	= BrokerPhone.ClientID;
			CFM.CompanyFaxField		= BrokerFax.ClientID;
            CFM.LoanOriginatorIdentifierField = LoanOfficerLoanOriginatorIdentifier.ClientID;
            CFM.CompanyLoanOriginatorIdentifierField = LoanOfficerCompanyLoanOriginatorIdentifier.ClientID;
        }


        public void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Loan1003pg3));
            dataLoan.InitLoad();

            // ****************************************************************
            // dataApp
            // ****************************************************************
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            
            aReTotVal.Text = dataApp.aReTotVal_rep;
            aReTotMAmt.Text = dataApp.aReTotMAmt_rep;
            aReTotGrossRentI.Text = dataApp.aReTotGrossRentI_rep;
            aReTotMPmt.Text = dataApp.aReTotMPmt_rep;
            aReTotHExp.Text = dataApp.aReTotHExp_rep;
            aReTotNetRentI.Text = dataApp.aReTotNetRentI_rep;

            aBDecJudgment.Text = dataApp.aBDecJudgment.ToUpper().TrimWhitespaceAndBOM() ;
            aCDecJudgment.Text = dataApp.aCDecJudgment.ToUpper().TrimWhitespaceAndBOM() ;
            aBDecBankrupt.Text = dataApp.aBDecBankrupt.ToUpper().TrimWhitespaceAndBOM() ;
            aBDecForeclosure.Text = dataApp.aBDecForeclosure.ToUpper().TrimWhitespaceAndBOM() ;
            aCDecForeclosure.Text = dataApp.aCDecForeclosure.ToUpper().TrimWhitespaceAndBOM() ;
            aBDecLawsuit.Text = dataApp.aBDecLawsuit.ToUpper().TrimWhitespaceAndBOM() ;
            aCDecLawsuit.Text = dataApp.aCDecLawsuit.ToUpper().TrimWhitespaceAndBOM() ;
            aCDecBankrupt.Text = dataApp.aCDecBankrupt.ToUpper().TrimWhitespaceAndBOM() ;
            aBDecDelinquent.Text = dataApp.aBDecDelinquent.ToUpper().TrimWhitespaceAndBOM() ;
            aBDecAlimony.Text = dataApp.aBDecAlimony.ToUpper().TrimWhitespaceAndBOM() ;
            aCDecAlimony.Text = dataApp.aCDecAlimony.ToUpper().TrimWhitespaceAndBOM() ;
            aBDecBorrowing.Text = dataApp.aBDecBorrowing.ToUpper().TrimWhitespaceAndBOM() ;
            aCDecBorrowing.Text = dataApp.aCDecBorrowing.ToUpper().TrimWhitespaceAndBOM() ;
            aCDecDelinquent.Text = dataApp.aCDecDelinquent.ToUpper().TrimWhitespaceAndBOM() ;
            aCDecObligated.Text = dataApp.aCDecObligated.ToUpper().TrimWhitespaceAndBOM() ;
            aBDecObligated.Text = dataApp.aBDecObligated.ToUpper().TrimWhitespaceAndBOM() ;
            aCDecPastOwnership.Text = dataApp.aCDecPastOwnership.ToUpper().TrimWhitespaceAndBOM() ;
            aBDecEndorser.Text = dataApp.aBDecEndorser.ToUpper().TrimWhitespaceAndBOM() ;
            aBDecCitizen.Text = dataApp.aBDecCitizen.ToUpper().TrimWhitespaceAndBOM() ;
            aCDecCitizen.Text = dataApp.aCDecCitizen.ToUpper().TrimWhitespaceAndBOM() ;
            aBDecResidency.Text = dataApp.aBDecResidency.ToUpper().TrimWhitespaceAndBOM() ;
            aCDecResidency.Text = dataApp.aCDecResidency.ToUpper().TrimWhitespaceAndBOM() ;
            aBDecOcc.Text = dataApp.aBDecOcc.ToUpper().TrimWhitespaceAndBOM() ;
            aCDecOcc.Text = dataApp.aCDecOcc.ToUpper().TrimWhitespaceAndBOM() ;
            aCDecEndorser.Text = dataApp.aCDecEndorser.ToUpper().TrimWhitespaceAndBOM() ;
            aBDecPastOwnership.Text = dataApp.aBDecPastOwnership.ToUpper().TrimWhitespaceAndBOM() ;
			aBDecForeignNational.Text = dataApp.aBDecForeignNational.ToUpper().TrimWhitespaceAndBOM() ;

            Tools.SetDropDownListValue(aBDecPastOwnedPropT, dataApp.aBDecPastOwnedPropT);
            Tools.SetDropDownListValue(aCDecPastOwnedPropT, dataApp.aCDecPastOwnedPropT);
            Tools.SetDropDownListValue(aBDecPastOwnedPropTitleT, dataApp.aBDecPastOwnedPropTitleT);
            Tools.SetDropDownListValue(aCDecPastOwnedPropTitleT, dataApp.aCDecPastOwnedPropTitleT);

            Tools.SetDropDownListValue(aIntrvwrMethodT, dataApp.aIntrvwrMethodT);
            aIntrvwrMethodTLckd.Checked = dataApp.aIntrvwrMethodTLckd;

            aAltNm1.Text = dataApp.aAltNm1;
            aAltNm1CreditorNm.Text = dataApp.aAltNm1CreditorNm;
            aAltNm1AccNum.Text = dataApp.aAltNm1AccNum.Value;
            aAltNm2.Text = dataApp.aAltNm2;
            aAltNm2CreditorNm.Text = dataApp.aAltNm2CreditorNm;
            aAltNm2AccNum.Text = dataApp.aAltNm2AccNum.Value;

            a1003InterviewD.Text = dataApp.a1003InterviewD_rep;
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V17_UpdateApplicationSubmittedCalculation))
            {
                a1003InterviewDLckd.Checked = dataApp.a1003InterviewDLckd;
            }
            else
            {
                a1003InterviewDLckd.Visible = false;
            }

            IPreparerFields interviewer = dataLoan.GetPreparerOfForm( E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject );
            LoanOfficerName.Text		= interviewer.PreparerName;
            LoanOfficerPhone.Text		= interviewer.Phone;
            LoanOfficerEmail.Text       = interviewer.EmailAddr;
			LoanOfficerLicenseNumber.Text = interviewer.LicenseNumOfAgent; // 11/15/06 mf. OPM 7970
            LoanOfficerCompanyLoanOriginatorIdentifier.Text = interviewer.CompanyLoanOriginatorIdentifier;
            LoanOfficerLoanOriginatorIdentifier.Text = interviewer.LoanOriginatorIdentifier;
			BrokerLicenseNumber.Text    = interviewer.LicenseNumOfCompany;
            BrokerName.Text				= interviewer.CompanyName;
            BrokerStreetAddr.Text		= interviewer.StreetAddr;
            BrokerCity.Text				= interviewer.City;
            BrokerState.Value			= interviewer.State;
            BrokerZip.Text				= interviewer.Zip;
            BrokerPhone.Text			= interviewer.PhoneOfCompany;
            BrokerFax.Text				= interviewer.FaxOfCompany;
            CFM.IsLocked = interviewer.IsLocked;
            CFM.AgentRoleT = interviewer.AgentRoleT;

            // ****************************************************************
            // dataLoan
            // ****************************************************************
            var isConstruction = dataLoan.sLPurposeT == E_sLPurposeT.Construct || dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm;
            bool areConstructionLoanDataPointsMigrated = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints);

            ((BasePage)this.Page).RegisterJsGlobalVariables("IsConstruction", isConstruction);
            ((BasePage)this.Page).RegisterJsGlobalVariables("AreConstructionLoanDataPointsMigrated", areConstructionLoanDataPointsMigrated);

            sPurchPrice.Text = dataLoan.sPurchPrice_rep;

            if (areConstructionLoanDataPointsMigrated)
            {
                sPurchPrice.ReadOnly = dataLoan.sLPurposeT != E_sLPurposeT.Purchase;
            }
            else
            {
                sPurchPrice.ReadOnly = dataLoan.sLPurposeT != E_sLPurposeT.Purchase && dataLoan.sLPurposeT != E_sLPurposeT.Construct && dataLoan.sLPurposeT != E_sLPurposeT.ConstructPerm;
            }

            sPurchasePrice1003.Text = dataLoan.sPurchasePrice1003_rep;
            m_isPurchase = dataLoan.sLPurposeT != E_sLPurposeT.Purchase && dataLoan.sLPurposeT != E_sLPurposeT.Construct && dataLoan.sLPurposeT != E_sLPurposeT.ConstructPerm;
            sAltCost.Text = dataLoan.sAltCost_rep;
            sAltCost.ReadOnly = areConstructionLoanDataPointsMigrated && isConstruction;
            sAltCostLckd.Checked = dataLoan.sAltCostLckd;
            sAltCostLckd.Disabled = !dataLoan.sIsRenovationLoan || (areConstructionLoanDataPointsMigrated && isConstruction);
            sLandCost.Text = dataLoan.sLandCost_rep;
            sLandIfAcquiredSeparately1003.Text = dataLoan.sLandIfAcquiredSeparately1003_rep;
            sRefPdOffAmt1003.Text = dataLoan.sRefPdOffAmt1003_rep;
            sRefPdOffAmt1003.ReadOnly = areConstructionLoanDataPointsMigrated && isConstruction;
            sRefPdOffAmt1003Lckd.Checked = dataLoan.sRefPdOffAmt1003Lckd;
            sRefPdOffAmt1003Lckd.Enabled = !areConstructionLoanDataPointsMigrated || !isConstruction;
            sTotEstPp1003.Text = dataLoan.sTotEstPp1003_rep;

            if (dataLoan.sIsIncludeProrationsIn1003Details && dataLoan.sIsIncludeProrationsInTotPp)
            {
                this.sTotEstPp.Text = dataLoan.sTotEstPp_rep;
                this.sTotalBorrowerPaidProrations.Text = dataLoan.sTotalBorrowerPaidProrations_rep;
            }
            else
            {
                this.PrepaidsAndReservesLabel.Visible = false;
                this.sTotEstPp.Visible = false;
                this.ProrationsPaidByBorrowerLabel.Visible = false;
                this.sTotalBorrowerPaidProrations.Visible = false;
            }

            sTotEstCc1003Lckd.Checked = dataLoan.sTotEstCc1003Lckd;
            sTotEstCcNoDiscnt1003.Text = dataLoan.sTotEstCcNoDiscnt1003_rep;

            if (dataLoan.sIsIncludeONewFinCcInTotEstCc)
            {
                sTotEstCcNoDiscnt.Text = dataLoan.sTotEstCcNoDiscnt_rep;
                sONewFinCc2.Text = dataLoan.sONewFinCc_rep;
                this.HideIfOtherFinancingCcIncludedInCc.Visible = false;

                var cells = new List<System.Web.UI.HtmlControls.HtmlTableCell>
                {
                    this.LabelCellM,
                    this.LockedCellM,
                    this.AmountCellM
                };

                foreach (var cell in cells)
                {
                    cell.Attributes["rowspan"] = "3";
                    cell.Attributes["class"] = cell.Attributes["class"] + " align-bottom";
                }
            }
            else
            {
                this.DisplayIfOtherFinancingCcIncludedInCc.Visible = false;
            }

            sLDiscnt1003.Text = dataLoan.sLDiscnt1003_rep;
            sLDiscnt1003Lckd.Checked = dataLoan.sLDiscnt1003Lckd;
            sFfUfmip1003Lckd.Checked = dataLoan.sFfUfmip1003Lckd;
            sFfUfmip1003.Text = dataLoan.sFfUfmip1003_rep;
            sTotTransC.Text = dataLoan.sTotTransC_rep;
            sONewFinBal.Text = dataLoan.sONewFinBal_rep;
            sTotCcPbs.Text = dataLoan.sTotCcPbs_rep;
            sTotCcPbsLocked.Checked = dataLoan.sTotCcPbsLocked;
            sOCredit1Lckd.Checked = dataLoan.sOCredit1Lckd;
            sOCredit1Amt.Text = dataLoan.sOCredit1Amt_rep;
            sOCredit1Desc.Text = dataLoan.sOCredit1Desc;
            sOCredit2Amt.Text = dataLoan.sOCredit2Amt_rep ;
            sOCredit2Desc.Text = dataLoan.sOCredit2Desc ;

            sOCredit3Amt.Text = dataLoan.sOCredit3Amt_rep ;
            sOCredit3Desc.Text = dataLoan.sOCredit3Desc ;
            sOCredit4Amt.Text = dataLoan.sOCredit4Amt_rep ;
            sOCredit4Desc.Text = dataLoan.sOCredit4Desc ;

            if (dataLoan.sLoads1003LineLFromAdjustments)
            {
                sOCredit1Lckd.Enabled = false;
                sOCredit2Amt.ReadOnly = true;
                sOCredit2Desc.ReadOnly = true;
                sOCredit3Amt.ReadOnly = true;
                sOCredit3Desc.ReadOnly = true;
                sOCredit4Amt.ReadOnly = true;
                sOCredit4Desc.ReadOnly = true;
            }

            sOCredit5Amt.Text = dataLoan.sOCredit5Amt_rep;
            sLAmtCalc.Text = dataLoan.sLAmtCalc_rep;
			sLAmtLckd.Checked = dataLoan.sLAmtLckd;
            sFfUfmipFinanced.Text = dataLoan.sFfUfmipFinanced_rep;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sTransNetCash.Text = dataLoan.sTransNetCash_rep;
            sTransNetCashLckd.Checked = dataLoan.sTransNetCashLckd;
			sONewFinCc.Text = dataLoan.sONewFinCc_rep;

            //OPM 43665 : Populate the state license fields only when the subject property state in the file is not empty
            if (!string.IsNullOrEmpty(dataLoan.sSpState))
            {
                CFM.AgentLicenseField = LoanOfficerLicenseNumber.ClientID;
                CFM.CompanyLicenseField = BrokerLicenseNumber.ClientID;
            }

            this.HREData.ShouldLoad = true;
            this.HREData.RaceEthnicityData = dataApp.ConstructHmdaRaceEthnicityData();

            var decExplnDataContainer = dataApp.ConstructDeclarationExplanationData();
            var serializedDecExplnData = SerializationHelper.JsonNetSerialize(decExplnDataContainer);
            this.Parent.Page.ClientScript.RegisterHiddenField("serializedDecExplnData", serializedDecExplnData);
        }
        public void SaveData() 
        {

        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

	}
}
