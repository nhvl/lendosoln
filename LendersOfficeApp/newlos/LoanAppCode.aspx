<%@ Page language="c#" Codebehind="LoanAppCode.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.LoanAppCode" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>LoanAppCode</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">

    <script type="text/javascript">
    function f_isLoanWindowOpen(sLId) {
      var winOpener = parent.window.opener;
      
      if (null != winOpener && !winOpener.closed) {
        if (typeof(winOpener.f_isLoanWindowOpen) != "undefined")
          return winOpener.f_isLoanWindowOpen(sLId);
      } 
      return false;      
    }
    function _onbeforeunload() {
      var winOpener = parent.window.opener;
      
      // MULTI-EDIT
      if (null != winOpener && !winOpener.closed) {
          if (typeof(winOpener.removeFromCurrentLoanList) != "undefined") {
              winOpener.removeFromCurrentLoanList(<%=AspxTools.JsString(RequestHelper.LoanID)%>);
          }
      }

      if (typeof(parent.body.isDirty) == 'function' && parent.body.isDirty()) {
          return UnloadMessage;
      }
    }
    
    // Workaround implemented to preserve window management
    // when switching to and from the new PML UI
    var winOpener = parent.window.opener;
    if (null != winOpener && !winOpener.closed) {
        winOpener.gSkipWindowManagement = false;
    }
    window.onbeforeunload = _onbeforeunload;
    </script>
</head>
    <script language=vbscript src=<%=AspxTools.SafeUrl(DataAccess.Tools.VRoot + "/inc/common.vbs")%> ></script>
    <script type="text/javascript" src=<%=AspxTools.SafeUrl(DataAccess.Tools.VRoot + "/inc/vbsfix.js")%> ></script>
  <body MS_POSITIONING="FlowLayout">
	
    <form id="LoanAppCode" method="post" runat="server">
     </form>
	
  </body>
</html>
