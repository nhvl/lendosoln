namespace LendersOfficeApp.newlos
{
    using System;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using System.Web;

    public partial class AuditTrail : LendersOfficeApp.newlos.BaseLoanPage
	{
		protected bool IsLpeEnabled
		{
			get{ return BrokerUser.HasFeatures( E_BrokerFeatureT.PricingEngine ); }
		}

		protected bool IsPmlEnabled
		{
			get{ return BrokerUser.HasFeatures( E_BrokerFeatureT.PriceMyLoan ); }
		}

        protected bool HasLenderDefaultFeatures
        {
            get { return Broker.HasLenderDefaultFeatures; }
        }

        protected bool IsInternalUser
        {
            get { return BrokerUser.HasPermission(Permission.CanModifyLoanPrograms); }
        }

		protected Boolean IsRateLockedAtSubmission
		{
			get{ return BrokerUser.IsRateLockedAtSubmission; }
		}

        protected bool HasAccountantReadPermission
        {
            get { return BrokerUser.HasPermission(Permission.AllowAccountantRead); }
        }

        protected bool HasInvestorViewPermission
        {
            get { return BrokerUser.HasPermission(Permission.AllowViewingInvestorInformation); }
        }

        protected bool HasQualityControlAccess
        {
            get { return BrokerUser.HasPermission(Permission.AllowQualityControlAccess); }
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            if ((HasAccountantReadPermission && HasInvestorViewPermission) || HasQualityControlAccess)
            {
                AuditItems.DataSource = AuditManager.RetrieveAuditList(LoanID);
                AuditItems.DataBind();
            }

        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            RegisterJsScript("jquery.tablesorter.dev.js");
            this.EnableJqueryMigrate = false;
            this.PageID = "AuditHistory";
            this.PageTitle = "Audit History";
            BindAuditCategories();

            String category = RequestHelper.GetSafeQueryString("category");
            if (category == "trailingDocs" || !HasQualityControlAccess)
            {
                int i = 0;
                foreach(ListItem item in m_categoryDDL.Items)
                {
                    if (item.Value == E_AuditItemCategoryT.TrailingDocument.ToString())
                    {
                        m_categoryDDL.SelectedIndex = i;
                    }
                    i++;
                }
                m_categoryDDL.Enabled = false;
            }
        }

        protected string RenderDate(LightWeightAuditItem audit)
        {
            return audit.Timestamp.ToShortDateString();
        }

        protected HtmlControl RenderDetailsLink(LightWeightAuditItem audit)
        {
            if (audit.HasDetails)
            {
                //<a href="#" onclick='return f_viewAuditDetail("e73eab24-a32d-4072-a555-d4577af27fc6");'>view detail</a>
                var anchor = new HtmlAnchor();
                anchor.InnerText = "view detail";
                anchor.HRef = "#";
                if(audit.AuditSeq == 0)
                {
                    anchor.Attributes.Add("onclick", $"return f_viewAuditDetail(\"{audit.ID}\");");
                }
                else
                {
                    string data = $"{LoanID}/{audit.AuditSeq}";
                    string key = LendersOffice.Drivers.Encryption.EncryptionHelper.EncryptAndEncode(EncryptionKeyIdentifier.AuditKeyPassword, data);

                    anchor.Attributes.Add("onclick", $"return f_viewAuditDetailVer2(\"{HttpUtility.UrlEncode(key)}\");");
                }

                return anchor;
            }

            return null;
        }

        protected void AuditItems_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            var auditItem = args.Item.DataItem as LightWeightAuditItem;
            if (auditItem == null) return;

            var row = args.Item.FindControl("Row") as HtmlTableRow;
            var category = args.Item.FindControl("Category") as Literal;
            var desc = args.Item.FindControl("Event") as Literal;
            var userName = args.Item.FindControl("UserName") as Literal;
            var timestamp = args.Item.FindControl("Timestamp") as Literal;

            row.Attributes["class"] = String.Format("{0} {1}", row.Attributes["class"], auditItem.CategoryT);
            category.Text = auditItem.Category;
            desc.Text = auditItem.Description;
            userName.Text = auditItem.UserName;
            timestamp.Text = auditItem.TimestampDescription;
        }

        private void BindAuditCategories()
        {
            m_categoryDDL.Items.Add(new ListItem("All", "All"));
            m_categoryDDL.Items.Add(new ListItem("Field Change", E_AuditItemCategoryT.FieldChange.ToString()));
            m_categoryDDL.Items.Add(new ListItem("Role Assignment", E_AuditItemCategoryT.RoleAssignment.ToString()));
            m_categoryDDL.Items.Add(new ListItem("Rate Lock", E_AuditItemCategoryT.RateLock.ToString()));
            m_categoryDDL.Items.Add(new ListItem("Loan Status", E_AuditItemCategoryT.LoanStatus.ToString()));
            m_categoryDDL.Items.Add(new ListItem("Email", E_AuditItemCategoryT.Email.ToString()));
            m_categoryDDL.Items.Add(new ListItem("Credit Report", E_AuditItemCategoryT.CreditReport.ToString()));
            m_categoryDDL.Items.Add(new ListItem("Disclosure/E-Sign", E_AuditItemCategoryT.DisclosureESign.ToString()));
            m_categoryDDL.Items.Add(new ListItem("Integration", E_AuditItemCategoryT.Integration.ToString()));
            m_categoryDDL.Items.Add(new ListItem("Trailing Document", E_AuditItemCategoryT.TrailingDocument.ToString()));
            m_categoryDDL.Items.Add(new ListItem("Disclosures Archived", E_AuditItemCategoryT.DisclosuresArchived.ToString()));
            m_categoryDDL.Items.Add(new ListItem("Fee Service", E_AuditItemCategoryT.FeeService.ToString()));
            m_categoryDDL.Items.Add(new ListItem("Agent Contact Record Change", E_AuditItemCategoryT.AgentContactRecordChange.ToString()));
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
