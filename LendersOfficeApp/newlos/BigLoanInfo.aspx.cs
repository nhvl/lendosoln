﻿namespace LendersOfficeApp.newlos
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using DataAccess.Core.Construction;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Migration;
    using LendersOffice.Security;
    using LendersOfficeApp.newlos.Forms;

    public partial class BigLoanInfo : BaseLoanPage
    {
        private CPageData dataLoan;
        private CAppData dataApp;

        protected String HeaderDisplay
        {
            get
            {
                if (IsLeadPage)
                {
                    return "This Lead Info";
                }
                else
                {
                    return "This Loan Info";
                }
            }
        }

        public bool CanEditLoanProgramName
        {
            // 5/29/07 mf. The LoanInfo page is very commonly viewed, so we expose this
            // setting for the child loaninfo control. This way we will not load the broker
            // twice every time. This property must be accessed after the Init when we retrieve.
            get { return BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowEditingLoanProgramName); }
        }

        #region Borrower Info

        protected bool isPurchase;

        private void InitBorrowerInfo()
        {
            RegularExpressionValidator1.ValidationExpression = ConstApp.EmailValidationExpression;

            Tools.Bind_aAddrT(aBAddrT);
            Tools.Bind_aAddrT(aCAddrT);

            Tools.Bind_aAddrMailSourceT(aBAddrMailSourceT);
            Tools.Bind_aAddrMailSourceT(aCAddrMailSourceT);
            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);
            
            aBZip.SmartZipcode(aBCity, aBState);
            aBZipMail.SmartZipcode(aBCityMail, aBStateMail);
            aCZip.SmartZipcode(aCCity, aCState);
            aCZipMail.SmartZipcode(aCCityMail, aCStateMail);
        }

        private void LoadBorrowerInfo()
        {
            if (dataApp == null)
            {
                throw new CBaseException(ErrorMessages.LoadLoanFailed, ErrorMessages.LoadLoanFailed);
            }
            else
            {
                // Alias / POA

                var bAliases = new List<string>(dataApp.aBAliases);
                var cAliases = new List<string>(dataApp.aCAliases);

                aBAlias.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(bAliases);
                aCAlias.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(cAliases);

                aBPowerOfAttorneyNm.Value = dataApp.aBPowerOfAttorneyNm;
                aCPowerOfAttorneyNm.Value = dataApp.aCPowerOfAttorneyNm;

                // All the rest
                aBFirstNm.Text = dataApp.aBFirstNm;
                aBPreferredNm.Text = dataApp.aBPreferredNm;
                aBPreferredNmLckd.Checked = dataApp.aBPreferredNmLckd;
                aBMidNm.Text = dataApp.aBMidNm;
                aBLastNm.Text = dataApp.aBLastNm;
                aBSuffix.Text = dataApp.aBSuffix;
                aCFirstNm.Text = dataApp.aCFirstNm;
                aCPreferredNm.Text = dataApp.aCPreferredNm;
                aCPreferredNmLckd.Checked = dataApp.aCPreferredNmLckd;
                aCMidNm.Text = dataApp.aCMidNm;
                aCLastNm.Text = dataApp.aCLastNm;
                aCSuffix.Text = dataApp.aCSuffix;
                aBSsn.Text = dataApp.aBSsn;
                aCSsn.Text = dataApp.aCSsn;
                aBDob.Text = dataApp.aBDob_rep;
                aCDob.Text = dataApp.aCDob_rep;
                aBHPhone.Text = dataApp.aBHPhone;
                aCHPhone.Text = dataApp.aCHPhone;
                aBCellphone.Text = dataApp.aBCellPhone;
                aCCellphone.Text = dataApp.aCCellPhone;
                aBBusPhone.Text = dataApp.aBBusPhone;
                aCBusPhone.Text = dataApp.aCBusPhone;
                aBEmail.Text = dataApp.aBEmail;
                aCEmail.Text = dataApp.aCEmail;
                aBAddr.Text = dataApp.aBAddr;
                aCAddr.Text = dataApp.aCAddr;
                aBCity.Text = dataApp.aBCity;
                aBState.Value = dataApp.aBState;
                aBZip.Text = dataApp.aBZip;
                aBAddrYrs.Text = dataApp.aBAddrYrs;
                aCAddrYrs.Text = dataApp.aCAddrYrs;
                aCZip.Text = dataApp.aCZip;
                aCState.Value = dataApp.aCState;
                aCCity.Text = dataApp.aCCity;
                aBAddrMail.Text = dataApp.aBAddrMail;
                aCAddrMail.Text = dataApp.aCAddrMail;
                aBCityMail.Text = dataApp.aBCityMail;
                aCCityMail.Text = dataApp.aCCityMail;
                aBStateMail.Value = dataApp.aBStateMail;
                aCStateMail.Value = dataApp.aCStateMail;
                aBZipMail.Text = dataApp.aBZipMail;
                aCZipMail.Text = dataApp.aCZipMail;

                aBFax.Text = dataApp.aBFax;
                aCFax.Text = dataApp.aCFax;

                Tools.SetDropDownListValue(aBAddrT, dataApp.aBAddrT);
                Tools.SetDropDownListValue(aCAddrT, dataApp.aCAddrT);
                
                Tools.SetDropDownListValue(aBAddrMailSourceT, dataApp.aBAddrMailSourceT);
                Tools.SetDropDownListValue(aCAddrMailSourceT, dataApp.aCAddrMailSourceT);

                isPurchase = dataLoan.sLPurposeT == E_sLPurposeT.Purchase;
            }
        }
        
        #endregion

        #region Loan Info Old

        protected bool m_IsRateLocked = false;
        protected bool m_showCreditLineFields
        {
            get
            {
                // 10/22/2013 dd - Only show the Is Credit Line check box for broker with HELOC feature and Loan TEmplate
                BrokerDB broker = this.Broker;
                //return broker.IsEnableHELOC && m_isTemplate;
                // 5/11/2014 dd - OPM 179885 - Allow the user to change the product type on a 2nd lien from HELOC to CLOSED-END or vice-versa
                return broker.IsEnableHELOC;
                //return BrokerUser.BrokerId == new Guid("e9f006c9-cb83-4531-8925-08fccf4add63")
                //    || BrokerUser.BrokerId == new Guid("D116B2B3-D3CA-4181-BAF6-B663815DB67D"); // David W Test (Dev)
            }
        }
        protected bool m_showCRMID
        {
            get
            {
                //ir OPM 169478 - Only show if broker bit is enabled
                BrokerDB broker = BrokerDB.RetrieveById(BrokerUser.BrokerId);
                return broker.ShowCRMID && !m_isTemplate;  //Loan Templates should not have visible CRM ID
            }
        }
        protected string ClosingCostPageUrl
        {
            get
            {
                BrokerDB db = this.Broker;
                if (db.AreAutomatedClosingCostPagesVisible)
                {
                    return string.Format("/los/Template/ClosingCost/ClosingCostTemplatePicker.aspx?sLId={0}", LoanID.ToString("N"));
                }
                else
                {
                    return string.Format("/los/view/closingcostlist.aspx?loanid={0}", LoanID.ToString("N"));
                }
            }
        }

        protected string m_lineAmtDisplay
        {
            get
            {
                if (this.Broker.IsEnableHELOC && sIsLineOfCredit.Checked)
                    return "";
                return "none";
            }
        }

        protected bool m_isInterestOnly = false;
        protected bool m_isPmlEnabled = false;
        protected bool m_cannotModifyLoanName = false;
        protected bool m_isTemplate = false;

        private void SetIsPMLEnable()
        {
            m_isPmlEnabled = BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
        }

        private void BindLeadSource()
        {
            // 5/5/14 gf - opm 172380, always add a blank option for the default
            // value. We do not want the UI to display misleading data before the
            // page is saved because there are now pricing rules and fee service
            // rules which depend on this field.
            sLeadSrcDesc.Items.Add(new ListItem("", ""));

            foreach (LeadSource desc in this.Broker.LeadSources)
            {
                sLeadSrcDesc.Items.Add(new ListItem(desc.Name, desc.Name));
            }
        }

        private void InitLoanInfo()
        {
            SetIsPMLEnable();
            Tools.Bind_sLienPosT(sLienPosT);
            Tools.Bind_sLPurposeT(sLPurposeT);
            Tools.Bind_sLT(sLT);
            Tools.Bind_sFinMethT(sFinMethT);
            Tools.Bind_aOccT(aOccT);
            Tools.Bind_sProRealETxT(sProRealETxT);
            Tools.Bind_PercentBaseT(sProHazInsT);
            Tools.Bind_sGseSpT(sGseSpT);
            Tools.Bind_TriState(this.sHomeIsMhAdvantageTri);
            Tools.Bind_QualTermCalculationType(sQualTermCalculationType);

            m_cannotModifyLoanName = !BrokerUser.HasPermission(Permission.CanModifyLoanName);
            BindLeadSource();
        }

        private void LoadLoanInfo()
        {
            bool oldBypass = dataLoan.ByPassFieldSecurityCheck;
            dataLoan.ByPassFieldSecurityCheck = true;

            this.QualRatePopup.LoadData(dataLoan);

            m_isTemplate = dataLoan.IsTemplate;

            bool isRealEstateTaxCalcVisible = !this.dataLoan.sRealEstateTaxExpenseInDisbMode;
            bool isHazInsCalcVisible = !this.dataLoan.sHazardExpenseInDisbursementMode;

            Tools.SetDropDownListValue(aOccT, dataApp.aOccT);
            Tools.SetDropDownListValue(sLT, dataLoan.sLT);
            Tools.SetDropDownListValue(sLienPosT, dataLoan.sLienPosT);
            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
            Tools.SetDropDownListValue(sGseSpT, dataLoan.sGseSpT);
            Tools.SetDropDownListValue(this.sHomeIsMhAdvantageTri, dataLoan.sHomeIsMhAdvantageTri);
            sFinMethT.Enabled = !(dataLoan.sIsRateLocked || IsReadOnly);

            if (isRealEstateTaxCalcVisible)
            {
                Tools.SetDropDownListValue(sProRealETxT, dataLoan.sProRealETxT);
            }
            else
            {
                this.sProRealETxTHolder.Visible = false;
            }

            if (isHazInsCalcVisible)
            {
                Tools.SetDropDownListValue(sProHazInsT, dataLoan.sProHazInsT);
            }
            else
            {
                this.sProHazInsTHolder.Visible = false;
            }

            // 7/15/2005 dd - If current description no longer in description, just add to the drop downlist instead of clear out.
            if (dataLoan.sLeadSrcDesc.Length > 0 && sLeadSrcDesc.Items.FindByText(dataLoan.sLeadSrcDesc) == null)
                sLeadSrcDesc.Items.Add(new ListItem(dataLoan.sLeadSrcDesc, dataLoan.sLeadSrcDesc));

            Tools.SetDropDownListValue(sLeadSrcDesc, dataLoan.sLeadSrcDesc);


            sLNm.Text = dataLoan.sLNm;

            // 5/25/2005 dd - Disable sLNm either cannotModifyLoanName or Loan in ReadOnly.

            // 5/25/2005 kb - Per case 1058, we are enforcing locking
            // the loan name.  We shall put all permission controlled
            // visibility/access control to ui elements here.
            sLNm.ReadOnly = m_cannotModifyLoanName || IsReadOnly;
            if (m_cannotModifyLoanName)
                sLNm.ToolTip = "You do not have permission to modify the loan number.";

            sLRefNm.Text = dataLoan.sLRefNm;

            sCrmNowLeadId.Text = dataLoan.sCrmNowLeadId;

            sLpTemplateNm.Text = dataLoan.sLpTemplateNm;
            if (!this.Broker.ShowLoanProductIdentifier)
            {
                this.sLoanProductIdentifierRow.Visible = false;
            }
            else
            {
                this.sLoanProductIdentifier.Text = dataLoan.sLoanProductIdentifier;
            }

            // 5/29/07 mf. OPM 12061. There is now a corporate setting for editing
            // the loan program name.
            if (!CanEditLoanProgramName)
                sLpTemplateNm.ReadOnly = true;

            sCcTemplateNm.Text = dataLoan.sCcTemplateNm;

            sIsStudentLoanCashoutRefi.Checked = dataLoan.sIsStudentLoanCashoutRefi;

            sFinMethDesc.Text = dataLoan.sFinMethDesc;
            sPurchPrice.Text = dataLoan.sPurchPrice_rep;
            sEquityCalc.Text = dataLoan.sEquityCalc_rep;
            sDownPmtPc.Text = dataLoan.sDownPmtPc_rep;

            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sQualIR.Text = dataLoan.sQualIR_rep;
            this.sQualIRLckd.Checked = dataLoan.sQualIRLckd;

            sTerm.Text = dataLoan.sTerm_rep;
            sTerm.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sDue.Text = dataLoan.sDue_rep;
            sDue.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sApprVal.Text = dataLoan.sApprVal_rep;
            sSpState.Value = dataLoan.sSpState;
            sProRealETxMb.Text = dataLoan.sProRealETxMb_rep;
            sProRealETxMbHolder.Visible = isRealEstateTaxCalcVisible;
            sProRealETxR.Text = dataLoan.sProRealETxR_rep;
            sProRealETxRHolder.Visible = isRealEstateTaxCalcVisible;
            sProRealETx.Text = dataLoan.sProRealETx_rep;
            sProRealETxBaseAmt.Text = dataLoan.sProRealETxBaseAmt_rep;
            sProRealETxBaseAmtHolder.Visible = isRealEstateTaxCalcVisible;

            sLAmtCalc.Text = dataLoan.sLAmtCalc_rep;
            sLAmtLckd.Checked = dataLoan.sLAmtLckd;
            sFfUfmipFinanced.Text = dataLoan.sFfUfmipFinanced_rep;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sMonthlyPmt.Text = dataLoan.sMonthlyPmt_rep;
            sLTotI.Text = dataLoan.sLTotI_rep;
            sLiaMonLTot.Text = dataLoan.sLiaMonLTot_rep;
            sPresLTotPersistentHExp.Text = dataLoan.sPresLTotPersistentHExp_rep;
            sProThisMPmt.Text = dataLoan.sProThisMPmt_rep;
            sProOFinPmt.Text = dataLoan.sProOFinPmt_rep;
            sProHazInsBaseAmt.Text = dataLoan.sProHazInsBaseAmt_rep;
            sProHazInsBaseAmtHolder.Visible = isHazInsCalcVisible;
            sProHazInsR.Text = dataLoan.sProHazInsR_rep;
            sProHazInsRHolder.Visible = isHazInsCalcVisible;
            sProHazInsMb.Text = dataLoan.sProHazInsMb_rep;
            sProHazInsMbHolder.Visible = isHazInsCalcVisible;
            sProHazIns.Text = dataLoan.sProHazIns_rep;
            sProMIns.Text = dataLoan.sProMIns_rep;
            sProMInsLckd.Checked = dataLoan.sProMInsLckd;
            sProHoAssocDues.Text = dataLoan.sProHoAssocDues_rep;
            sProHoAssocDues.ReadOnly = this.dataLoan.sHoaDuesExpenseInDisbMode;
            sProOHExp.Text = dataLoan.sProOHExp_rep;
            sProOHExpLckd.Checked = dataLoan.sProOHExpLckd;
            sLtvR.Text = dataLoan.sLtvR_rep;
            sCltvR.Text = dataLoan.sCltvR_rep;
            sQualTopR.Text = dataLoan.sQualTopR_rep;
            sQualBottomR.Text = dataLoan.sQualBottomR_rep;

            sOptionArmTeaserR.Text = dataLoan.sOptionArmTeaserR_rep;
            sOptionArmTeaserR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sIsOptionArm.Checked = dataLoan.sIsOptionArm;
            sIsOptionArm.Enabled = !(dataLoan.sIsRateLocked || IsReadOnly);
            sIsQRateIOnly.Checked = dataLoan.sIsQRateIOnly;
            sOriginalAppraisedValue.Text = dataLoan.sOriginalAppraisedValue_rep;
            sFHAPurposeIsStreamlineRefiWithoutAppr.Value = dataLoan.sFHAPurposeIsStreamlineRefiWithoutAppr ? "1" : "0";

            m_IsRateLocked = dataLoan.sIsRateLocked;
            sIsEmployeeLoan.Checked = dataLoan.sIsEmployeeLoan;

            sIsNewConstruction.Checked = dataLoan.sIsNewConstruction;

            sHighPricedMortgageTLckd.Checked = dataLoan.sHighPricedMortgageTLckd;

            Tools.Bind_sHighPricedMortgageT(sHighPricedMortgageT);
            Tools.SetDropDownListValue(sHighPricedMortgageT, dataLoan.sHighPricedMortgageT);

            sIsLineOfCredit.Checked = dataLoan.sIsLineOfCredit;
            sCreditLineAmt.Text = dataLoan.sCreditLineAmt_rep;

            // 6/4/2004 dd - Kevin Bagley requested display Interest Only indicator when sIOnlyMon is not zero.
            m_isInterestOnly = dataLoan.sIOnlyMon_rep != "0";
            sIsIOnlyForSubFin.Visible = dataLoan.sIsIOnlyForSubFin;
            sConstructionPeriodMon.Text = dataLoan.sConstructionPeriodMon_rep;
            sConstructionPeriodIR.Text = dataLoan.sConstructionPeriodIR_rep;
            sConstructionImprovementAmt.Text = dataLoan.sConstructionImprovementAmt_rep;
            sLandCost.Text = dataLoan.sLandCost_rep;

            Tools.SetDropDownListValue(this.sQualTermCalculationType, dataLoan.sQualTermCalculationType);
            this.sQualTerm.Value = dataLoan.sQualTerm_rep;

            // OPM 235326, ML, 1/14/2016
            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                MortgagePILink.HRef = "javascript:linkMe('Forms/LoanTerms.aspx');";
                MortgagePILink.Title = "Go to Loan Terms";
            }
            else
            {
                MortgagePILink.HRef = "javascript:linkMe('Forms/TruthInLending.aspx');";
                MortgagePILink.Title = "Go to Truth-In-Lending";
            }

            #region OPM 3297. Add details of other loan next to "Other Financing"
            if (dataLoan.sLienPosT == E_sLienPosT.First)
            {
                // 12/7/2005 dd - Only display the 2nd loan information if current loan is first lien.
                if (dataLoan.sConcurSubFin_rep != "$0.00" && dataLoan.sSubFinIR_rep != "0.000%" && dataLoan.sSubFinPmtLckd == false)
                {
                    OtherFinancingDescription.Text = string.Format("({0} @ {1})", dataLoan.sConcurSubFin_rep, dataLoan.sSubFinIR_rep);
                }
            }

            #endregion

            LoanValueEvaluator evaluator = new LoanValueEvaluator(BrokerUser.ConnectionInfo, BrokerUser.BrokerId, LoanID, WorkflowOperations.UseOldSecurityModelForPml, WorkflowOperations.RunPmlToStep3, WorkflowOperations.RunPmlForAllLoans, WorkflowOperations.RunPmlForRegisteredLoans);
            evaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(BrokerUser));
            bool useOldEngine = LendingQBExecutingEngine.CanPerform(WorkflowOperations.UseOldSecurityModelForPml, evaluator);

            if (false == useOldEngine)
            {
                btnRunLPE.Disabled = false == LendingQBExecutingEngine.CanPerformAny(evaluator, WorkflowOperations.RunPmlToStep3, WorkflowOperations.RunPmlForRegisteredLoans, WorkflowOperations.RunPmlForAllLoans);
            }
            else
            {
                btnRunLPE.Disabled = IsReadOnly;
            }

            if (dataLoan.sIsRefinancing)
            {
                sDownOrEquityLabel.InnerText = "Equity";
                sDownOrEquityPcLabel.InnerText = "Equity (%)";
            }
            else
            {
                sDownOrEquityLabel.InnerText = "Down Payment";
                sDownOrEquityPcLabel.InnerText = "Down Payment (%)";
            }

            sTransmProTotHExp_pop.Text = dataLoan.sTransmProTotHExp_rep;
            sLTotI_pop.Text = dataLoan.sLTotI_rep;
            sQualTopR_pop.Text = dataLoan.sQualTopR_rep;

            sMonthlyPmt_pop.Text = dataLoan.sMonthlyPmt_rep;
            sPresLTotPersistentHExp_pop.Text = dataLoan.sPresLTotPersistentHExp_rep;
            sLiaMonLTot_pop.Text = dataLoan.sLiaMonLTot_rep;
            sLTotI_pop2.Text = dataLoan.sLTotI_rep;
            sQualBottomR_pop.Text = dataLoan.sQualBottomR_rep;

            dataLoan.ByPassFieldSecurityCheck = oldBypass;

            this.RegisterJsGlobalVariables("IsEnableRenovationCheckboxInPML", this.Broker.IsEnableRenovationCheckboxInPML);
            this.RegisterJsGlobalVariables("EnableRenovationLoanSupport", this.Broker.EnableRenovationLoanSupport);

            sIsRenovationLoan.Checked = dataLoan.sIsRenovationLoan;

            if (dataLoan.sIsRenovationLoan)
            {
                sTotalRenovationCostsLckd.Checked = dataLoan.sTotalRenovationCostsLckd;
                sFHASpAsIsVal.Text = dataLoan.sFHASpAsIsVal_rep;
                sTotalRenovationCosts.Text = dataLoan.sTotalRenovationCosts_rep;
                sInducementPurchPrice.Text = dataLoan.sInducementPurchPrice_rep;
            }
        }

        #endregion

        #region Monthly Income

        protected string m_sBName = string.Empty;
        protected string m_sCName = string.Empty;

        private void LoadMonthlyIncome()
        {
            m_sBName = dataApp.aBNm;
            m_sCName = dataApp.aCNm;

            var incomeFields = new[]
            {
                aBBaseI,
                aBOvertimeI,
                aBBonusesI,
                aBCommisionI,
                aBDividendI,
                aCBaseI,
                aCOvertimeI,
                aCBonusesI,
                aCCommisionI,
                aCDividendI,
            };

            foreach (var incomeField in incomeFields)
            {
                incomeField.ReadOnly = dataLoan.sIsIncomeCollectionEnabled;
            }

            if(dataLoan.sIsIncomeCollectionEnabled)
            {
                this.Form.Attributes.Add("class", "is-income-collection-enabled");
            }

            aBBaseI.Text = dataApp.aBBaseI_rep;
            aBOvertimeI.Text = dataApp.aBOvertimeI_rep;
            aBBonusesI.Text = dataApp.aBBonusesI_rep;
            aBCommisionI.Text = dataApp.aBCommisionI_rep;
            aBDividendI.Text = dataApp.aBDividendI_rep;
            aBNetRentI1003.Text = dataApp.aBNetRentI1003_rep;
            aCBaseI.Text = dataApp.aCBaseI_rep;
            aCOvertimeI.Text = dataApp.aCOvertimeI_rep;
            aCBonusesI.Text = dataApp.aCBonusesI_rep;
            aCCommisionI.Text = dataApp.aCCommisionI_rep;
            aCDividendI.Text = dataApp.aCDividendI_rep;
            aCNetRentI1003.Text = dataApp.aCNetRentI1003_rep;
            aNetRentI1003Lckd.Checked = dataApp.aNetRentI1003Lckd;

            aBSpPosCf.Text = dataApp.aBSpPosCf_rep;
            aCSpPosCf.Text = dataApp.aCSpPosCf_rep;
            aTotSpPosCf.Text = dataApp.aTotSpPosCf_rep;

            aBTotI.Text = dataApp.aBTotI_rep;
            aCTotI.Text = dataApp.aCTotI_rep;

            aTotI.Text = dataApp.aTotI_rep;
            aBTotOI.Text = dataApp.aBTotOI_rep;
            aCTotOI.Text = dataApp.aCTotOI_rep;
            aTotOI.Text = dataApp.aTotOI_rep;

            aTotNetRentI1003.Text = dataApp.aTotNetRentI1003_rep;
            aTotDividendI.Text = dataApp.aTotDividendI_rep;
            aTotCommisionI.Text = dataApp.aTotCommisionI_rep;
            aTotBonusesI.Text = dataApp.aTotBonusesI_rep;
            aTotBaseI.Text = dataApp.aTotBaseI_rep;
            aTotOvertimeI.Text = dataApp.aTotOvertimeI_rep;

            sLTotI_mon.Text = dataLoan.sLTotI_rep;
        }

        #endregion

        #region Credit Score

        private void LoadCreditScore()
        {
            aBExperianScore.Text   = dataApp.aBExperianScore_rep;
            aBTransUnionScore.Text = dataApp.aBTransUnionScore_rep;
            aBEquifaxScore.Text    = dataApp.aBEquifaxScore_rep;
            aCExperianScore.Text   = dataApp.aCExperianScore_rep;
            aCTransUnionScore.Text = dataApp.aCTransUnionScore_rep;
            aCEquifaxScore.Text    = dataApp.aCEquifaxScore_rep;

            sEstCloseDLckd.Checked = dataLoan.sEstCloseDLckd;
            sEstCloseD.Text = dataLoan.sEstCloseD_rep;
        }

        #endregion

        #region Subj Prop Info

        private void InitSubjPropInfo()
        {
            sSpZip_spi.SmartZipcode(sSpCity_spi, sSpState_spi);
        }

        private void LoadSubjPropInfo()
        {
            sSpCity_spi.Text = dataLoan.sSpCity;
            sSpState_spi.Value = dataLoan.sSpState;
            sSpZip_spi.Text = dataLoan.sSpZip.TrimWhitespaceAndBOM();
            sSpAddr_spi.Text = dataLoan.sSpAddr;
        }

        #endregion

        #region MIP

        private void LoadMIP()
        {
            Tools.Bind_MipCalcType_rep(sProMInsT, dataLoan.sLT);
            Tools.Bind_sMiInsuranceT(sMiInsuranceT);
            Tools.Bind_sMiCompanyNmT(sMiCompanyNmT, dataLoan.sLT);

            Tools.SetDropDownListValue(sProMInsT, dataLoan.sProMInsT);
            Tools.SetDropDownListValue(sMiInsuranceT, dataLoan.sMiInsuranceT);
            Tools.SetDropDownListValue(sMiCompanyNmT, dataLoan.sMiCompanyNmT);

            sFfUfmipR.Text = dataLoan.sFfUfmipR_rep;
            sUfCashPd.Text = dataLoan.sUfCashPd_rep;
            sFfUfmipFinanced_mip.Text = dataLoan.sFfUfmipFinanced_rep;
            sFfUfmip1003.Text = dataLoan.sFfUfmip1003_rep;
            sFfUfmip1003Lckd.Checked = dataLoan.sFfUfmip1003Lckd;

            sMipFrequency_0.Checked = sMipFrequency_0.Value == dataLoan.sMipFrequency.ToString("D");
            sMipFrequency_1.Checked = sMipFrequency_1.Value == dataLoan.sMipFrequency.ToString("D");
            if (dataLoan.sFfUfMipIsBeingFinanced)
                sFfUfMipIsBeingFinanced_0.Checked = true;
            else
                sFfUfMipIsBeingFinanced_1.Checked = true;
            sMipPiaMon.Text = dataLoan.sMipPiaMon_rep;
            sUfCashPdLckd.Checked = dataLoan.sUfCashPdLckd;

            sProMInsR.Text = dataLoan.sProMInsR_rep;
            sProMInsBaseAmt.Text = dataLoan.sProMInsBaseAmt_rep;
            sProMInsBaseMonthlyPremium.Text = dataLoan.sProMInsBaseMonthlyPremium_rep;
            sProMInsMb.Text = dataLoan.sProMInsMb_rep;
            sProMIns_mip.Text = dataLoan.sProMIns_rep;
            sProMInsLckd_mip.Checked = dataLoan.sProMInsLckd;

            sMiLenderPaidCoverage.Text = dataLoan.sMiLenderPaidCoverage_rep;
            sMiCommitmentRequestedD.Text = dataLoan.sMiCommitmentRequestedD_rep;
            sMiCommitmentReceivedD.Text = dataLoan.sMiCommitmentReceivedD_rep;
            sMiCommitmentExpirationD.Text = dataLoan.sMiCommitmentExpirationD_rep;
            sMiCertId.Text = dataLoan.sMiCertId;
            sProMInsMon.Text = dataLoan.sProMInsMon_rep;
            sProMIns2Mon.Text = dataLoan.sProMIns2Mon_rep;
            sProMInsR2.Text = dataLoan.sProMInsR2_rep;
            sProMIns2.Text = dataLoan.sProMIns2_rep;
            sProMInsCancelLtv.Text = dataLoan.sProMInsCancelLtv_rep;
            sProMInsCancelAppraisalLtv.Text = dataLoan.sProMInsCancelAppraisalLtv_rep;
            sProMInsCancelMinPmts.Text = dataLoan.sProMInsCancelMinPmts_rep;
            sProMInsMidptCancel.Checked = dataLoan.sProMInsMidptCancel;
            sLenderUfmipR.Text = dataLoan.sLenderUfmipR_rep;
            sLenderUfmip.Text = dataLoan.sLenderUfmip_rep;
            sLenderUfmipLckd.Checked = dataLoan.sLenderUfmipLckd;
            sUfmipIsRefundableOnProRataBasis.Checked = dataLoan.sUfmipIsRefundableOnProRataBasis;
        }

        private void InitMIP()
        {
            RegisterJsObject("MipCalcTypeNonFha_map", DataAccess.CPageBase.MipCalcTypeNonFha_map);
            RegisterJsObject("MipCalcTypeFha_map", DataAccess.CPageBase.MipCalcTypeFha_map);
        }

        #endregion

        #region Misc

        private void InitMisc()
        {
            Tools.Bind_sFHAHousingActSection(sFHAHousingActSection);
            Tools.Bind_aVaEntitleCode(aVaEntitleCode);
            Tools.Bind_aVaServiceBranchT(aVaServiceBranchT);
            Tools.Bind_aVaMilitaryStatT(aVaMilitaryStatT);
        }

        private void LoadMisc()
        {
            sAgencyCaseNum.Text = dataLoan.sAgencyCaseNum;
            sCaseAssignmentD.Text = dataLoan.sCaseAssignmentD_rep;
            sFHAHousingActSection.Text = dataLoan.sFHAHousingActSection;

            aVaEntitleCode.Text = dataApp.aVaEntitleCode;
            aVaEntitleAmt.Text = dataApp.aVaEntitleAmt_rep;
            Tools.SetDropDownListValue(aVaServiceBranchT, dataApp.aVaServiceBranchT);
            Tools.SetDropDownListValue(aVaMilitaryStatT, dataApp.aVaMilitaryStatT);
        }

        #endregion

        #region Footer

        protected bool m_isPurchase = false;

        private void BindOtherCreditDescription(MeridianLink.CommonControls.ComboBox cb)
        {
            cb.Items.Add("Cash Deposit on sales contract");
            cb.Items.Add("Seller Credit");
            cb.Items.Add("Lender Credit");
            cb.Items.Add("Relocation Funds");
            cb.Items.Add("Employer Assisted Housing");
            cb.Items.Add("Lease Purchase Fund");
            cb.Items.Add("Borrower Paid Fees");
            cb.Items.Add("Paid Outside of Closing");
            cb.Items.Add("Broker Credit");
        }

        private void LoadFooter()
        {
            sPurchPrice_foot.Text = dataLoan.sPurchPrice_rep;
            sPurchPrice.ReadOnly = dataLoan.sLPurposeT != E_sLPurposeT.Purchase && dataLoan.sLPurposeT != E_sLPurposeT.Construct && dataLoan.sLPurposeT != E_sLPurposeT.ConstructPerm;
            sAltCost.Text = dataLoan.sAltCost_rep;
            sAltCostLckd.Checked = dataLoan.sAltCostLckd;
            sLandCost_foot.Text = dataLoan.sLandCost_rep;
            sRefPdOffAmt1003.Text = dataLoan.sRefPdOffAmt1003_rep;
            sRefPdOffAmt1003Lckd.Checked = dataLoan.sRefPdOffAmt1003Lckd;
            sTotEstPp1003.Text = dataLoan.sTotEstPp1003_rep;
            sTotEstPp1003Lckd.Checked = dataLoan.sTotEstPp1003Lckd;

            if (dataLoan.sIsIncludeProrationsIn1003Details && dataLoan.sIsIncludeProrationsInTotPp)
            {
                sTotEstPp.Text = dataLoan.sTotEstPp_rep;
                sTotalBorrowerPaidProrations.Text = dataLoan.sTotalBorrowerPaidProrations_rep;
            }
            else
            {
                this.PrepaidsAndReservesLabel.Visible = false;
                this.sTotEstPp.Visible = false;
                this.ProrationsPaidByBorrowerLabel.Visible = false;
                this.sTotalBorrowerPaidProrations.Visible = false;
            }

            sTotEstCc1003Lckd.Checked = dataLoan.sTotEstCc1003Lckd;
            sTotEstCcNoDiscnt1003.Text = dataLoan.sTotEstCcNoDiscnt1003_rep;

            if (dataLoan.sIsIncludeONewFinCcInTotEstCc)
            {
                sTotEstCcNoDiscnt.Text = dataLoan.sTotEstCcNoDiscnt_rep;
                sONewFinCc2.Text = dataLoan.sONewFinCc_rep;
                this.HideIfOtherFinancingCcIncludedInCc.Visible = false;

                var cells = new List<HtmlTableCell>
                {
                    this.LabelCellM,
                    this.LockedCellM,
                    this.AmountCellM
                };

                foreach (var cell in cells)
                {
                    cell.Attributes["rowspan"] = "3";
                    cell.Attributes["class"] = cell.Attributes["class"] + " align-bottom";
                }
            }
            else
            {
                this.DisplayIfOtherFinancingCcIncludedInCc.Visible = false;
            }

            sLDiscnt1003.Text = dataLoan.sLDiscnt1003_rep;
            sLDiscnt1003Lckd.Checked = dataLoan.sLDiscnt1003Lckd;
            sFfUfmip1003Lckd_foot.Checked = dataLoan.sFfUfmip1003Lckd;
            sFfUfmip1003_foot.Text = dataLoan.sFfUfmip1003_rep;
            sTotTransC.Text = dataLoan.sTotTransC_rep;
            sONewFinBal.Text = dataLoan.sONewFinBal_rep;
            sTotCcPbs.Text = dataLoan.sTotCcPbs_rep;
            sTotCcPbsLocked.Checked = dataLoan.sTotCcPbsLocked;
            sOCredit1Lckd.Checked = dataLoan.sOCredit1Lckd;
            sOCredit1Amt.Text = dataLoan.sOCredit1Amt_rep;
            footer_sOCredit1Desc.Text = dataLoan.sOCredit1Desc;
            sOCredit2Amt.Text = dataLoan.sOCredit2Amt_rep;
            footer_sOCredit2Desc.Text = dataLoan.sOCredit2Desc;

            sOCredit3Amt.Text = dataLoan.sOCredit3Amt_rep;
            footer_sOCredit3Desc.Text = dataLoan.sOCredit3Desc;
            sOCredit4Amt.Text = dataLoan.sOCredit4Amt_rep;
            footer_sOCredit4Desc.Text = dataLoan.sOCredit4Desc;

            if(dataLoan.sLoads1003LineLFromAdjustments)
            {
                sOCredit1Lckd.Enabled = false;
                sOCredit2Amt.ReadOnly = true;
                footer_sOCredit2Desc.ReadOnly = true;
                sOCredit3Amt.ReadOnly = true;
                footer_sOCredit3Desc.ReadOnly = true;
                sOCredit4Amt.ReadOnly = true;
                footer_sOCredit4Desc.ReadOnly = true;
            }

            sOCredit5Amt.Text = dataLoan.sOCredit5Amt_rep;
            sLAmtCalc_foot.Text = dataLoan.sLAmtCalc_rep;
            sLAmtLckd_foot.Checked = dataLoan.sLAmtLckd;
            sFfUfmipFinanced_foot.Text = dataLoan.sFfUfmipFinanced_rep;
            sFinalLAmt_foot.Text = dataLoan.sFinalLAmt_rep;
            sTransNetCash.Text = dataLoan.sTransNetCash_rep;
            sTransNetCashLckd.Checked = dataLoan.sTransNetCashLckd;
            sONewFinCc.Text = dataLoan.sONewFinCc_rep;
        }

        private void InitFooter()
        {
            BindOtherCreditDescription(footer_sOCredit1Desc);
            BindOtherCreditDescription(footer_sOCredit2Desc);
            BindOtherCreditDescription(footer_sOCredit3Desc);
            BindOtherCreditDescription(footer_sOCredit4Desc);
        }

        #endregion

        internal static void BindDataFromControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem item)
        {
            QualRateCalculationPopup.BindData(loan, item);
        }

        internal static void LoadDataForControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem item)
        {
            QualRateCalculationPopup.LoadData(loan, item);
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            DisplayCopyRight = false;
            dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(BigLoanInfo));
            dataLoan.InitLoad();
            sFileVersion = dataLoan.sFileVersion;
            dataApp = dataLoan.GetAppData(ApplicationID);

            this.RegisterJsGlobalVariables("IsConstructionAndLotPurchase",
                dataLoan.sConstructionPurposeT == ConstructionPurpose.ConstructionAndLotPurchase);

            this.RegisterJsGlobalVariables("AreConstructionLoanCalcsMigrated",
                LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges));

            this.RegisterJsGlobalVariables("IsConstructionPurposeRefi", dataLoan.sConstructionPurposeT.EqualsOneOf(ConstructionPurpose.ConstructionAndLotRefinance, ConstructionPurpose.ConstructionOnOwnedLot));

            if (this.m_showCreditLineFields)
            {
                pageBody.Attributes.Add("class", "FTFCU");
            }

            InitBorrowerInfo();

            InitLoanInfo();

            InitSubjPropInfo();

            InitMIP();

            InitMisc();

            InitFooter();

            this.RegisterJsScript("LQBPopup.js");	
        }

        protected override void LoadData()
        {
            LoadBorrowerInfo();

            LoadLoanInfo();
            
            LoadMonthlyIncome();

            LoadCreditScore();

            LoadSubjPropInfo();

            LoadMIP();

            LoadMisc();

            var isUlad2019 = dataLoan.sGseTargetApplicationT == GseTargetApplicationT.Ulad2019;
            this.RegisterJsGlobalVariables("isUlad2019", isUlad2019);

            if(isUlad2019)
            {
                QualifyingBorrower.Init_QualifyingBorrower(this, dataLoan);
            }
            else
            {
                LoadFooter();
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            this.PageTitle = "Loan Information";
            this.PageID = "BigLoanInformation";
            base.OnPreRender(e);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            IsAppSpecific = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
