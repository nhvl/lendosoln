<%@ Register TagPrefix="uc" TagName="DocTypePicker" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx"%>
<%@ Page language="c#" Codebehind="PrintList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.PrintList" enableViewState="False"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!doctype html>
<HTML>
  <HEAD runat="server">
    <title>PrintList</title>
<style>
  .HelpStyle {
      width: 230px;
      border: black 3px solid;
      padding: 5px;
      position: absolute;
      background-color: whitesmoke;
  }
  .LoadingPopupContent {
      text-align: center;
  }
  .LoadingPopupContent img {
      margin-top: 72px;<%-- img is 54x55, popup is 200, so ~72 should vertically center the image --%>
  }
  .LoadingPopupContent div {
      padding-top: 30px;<%-- Splitting vertical division at 36px, but then -6 to account for font-size 11 --%>
  }
  </style>
</HEAD>
<body onresize=updatePrintFrame(); bgColor=gainsboro scroll=no ms_positioning="FlowLayout">

<script type="text/javascript">
    <!--
    var gProcessingLabel;
    var gDotCount = 0;
    var gSelectedColor = 'skyblue';
    var gProcessingIntervalID = null;
    var gSelectedCount = 0;
    var gCustomFormList = null;
    var gHostUrl = <%=AspxTools.JsString(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("/", 8)) + VirtualRoot)%>;
    
    function updatePrintFrame() {
      var h;
      if(document.documentElement.clientHeight > 0){
        h = document.documentElement.clientHeight;
      }
      else{
        h = document.body.clientHeight;
      }
      document.getElementById("PrintFrame").style.height=(h - 195) + "px";    
    }
    function updateCountLabel() {
      document.getElementById("countLabel").innerHTML = "<b>" + gSelectedCount + "</b> item(s) selected.";
      disabledButtons(gSelectedCount === 0);
    }
    function _init() {
      createProcessingLabel();
      updateCountLabel();
      updatePrintFrame();
      f_refresh_PdfEncryptionOptions();

      hookAHref();
      hookCb();
      selectDefault();
    }
    function linkMouseOver(event) { highlightRow( retrieveEventTarget(event)); }
    function linkMouseOut(event)  { unhighlightRow( retrieveEventTarget(event)); }
    function hookAHref() {
      var coll = document.getElementsByTagName("A");
      var length = coll.length;
      for (var i = 0; i < length; i++) {
        var o = coll[i];
        if (o.id == 'help') continue;
        addEventHandler(o, "mouseover", linkMouseOver, false);
        addEventHandler(o, "mouseout", linkMouseOut, false);
      }
    }
    function clickCb(event) {
      genericOnclick( retrieveEventTarget(event));
    }
    function hookCb() {
      var coll = document.getElementsByTagName("INPUT");
      var length = coll.length;
      for (var i = 0; i < length; i++) {
        var o = coll[i];
        if (o.type == "checkbox" && o.getAttribute("parentLink") == null && o.id != 'IsPdfEncryption') {
          addEventHandler(o, "click", clickCb, false);
        }
      }
    }
    function selectDefault() {

      var isFirstScroll = true;
      var coll = document.getElementsByTagName("INPUT");
      var length = coll.length;

      for (var i = 0; i < length; i++)
      {
        var o = coll[i];
        if (o.type === 'checkbox' && o.disabled === false)
        {
          if (o.getAttribute("pdf") === <%= AspxTools.JsString(m_defaultPrintID) %> && (o.getAttribute("primaryOnly") != null || o.getAttribute("appid") === <%= AspxTools.JsString(ApplicationID) %>)) 
          {
            if (isFirstScroll) 
          {
              scrollTo(o);
              isFirstScroll = false;
          }
            o.click();
          }
        }
      }

      //these 2 exist on the same page, so we have to make sure if we are looking for one, we are also
      //looking for the other
      if(<%= AspxTools.JsString(m_defaultPrintID) %> === <%= AspxTools.JsString((typeof(LendersOffice.Pdf.CVOMPDF)).Name) %>)
      {
		for (var i = 0; i < length; i++) 
		{
			var o = coll[i];
			if (o.type == 'checkbox') 
			{
				if (o.getAttribute("pdf") == <%=AspxTools.JsString((typeof(LendersOffice.Pdf.CVORPDF)).Name)%> && (o.getAttribute("primaryOnly") != null || o.getAttribute("appid") === <%= AspxTools.JsString(ApplicationID) %>)) 
				{
					if (isFirstScroll) 
					{
						scrollTo(o);
						isFirstScroll = false;
					}
					o.click();
				}
			}
		}
      }
      
      return;
    }
    
    function scrollTo(o) {
      if (o.offsetParent.offsetTop < PrintFrame.scrollTop || o.offsetParent.offsetTop > PrintFrame.clientHeight + PrintFrame.scrollTop)
        PrintFrame.scrollTop = o.offsetParent.offsetTop - 20;
    }

    function selectAll(b) {

      var coll = document.getElementsByTagName("INPUT");
      var length = coll.length;
      gSelectedCount = 0;
      for (i = 0; i < length; i++) {
        var o = coll[i];      
        if (o.type === 'checkbox' && o.disabled === false) {
          if (f_isInIgnoreList(o.id)) {
              continue;
          }
            o.checked = b;
            if (o.getAttribute("excludeFromBatch") != null || o.getAttribute("excludefrombatch") != null) gSelectedCount--;

            genericOnclick(o);
        }
      }
      
      if (!b) gSelectedCount = 0;
      updateCountLabel();
          
    }
    function selectWhole() {
      var count = arguments.length - 1;
      var o = arguments[0];
      highlightRowByCheckbox(o, gSelectedColor);      
      for (var i = 0; i < count; i++) {
        var cb = document.forms[0][arguments[i + 1]];
        if (null != cb) {
          if (cb.checked != o.checked) {
            if (o.checked) gSelectedCount++;
            else gSelectedCount--;
          }
          cb.checked = o.checked;
          highlightRowByCheckbox(cb, gSelectedColor);
        }
      }
      updateCountLabel();
      
    }
    
    function _link(href, id) {
      var o = document.forms[0][id];
      var appid = o.getAttribute("appid");
      var recordid = o.getAttribute("recordid");
      var borrtype = o.getAttribute("borrtype");
      linkMe(href, appid, recordid, borrtype);
    }
    function linkMe(href, appid, recordid, borrtype) {
      clearDirty();
    
      if (appid != null) parent.body.f_setCurrentApplicationID(appid);
      var ch = href.indexOf('?') > 0 ? '&' : '?';
      var extraArgs = "";
      if (null != recordid)
        extraArgs = "&recordid=" + recordid;
      if (null != borrtype)
        extraArgs = extraArgs + "&borrtype=" + borrtype;
      
      parent.body.f_load(href + ch + 'loanid=' + ML.sLId + extraArgs);
    }
        
    function _preview(file, id) {
      var passwordId = "";
      
      var _args = new Object();
      _args["loanid"] = ML.sLId;
      if (f_pdfEncryptionOptions(_args)) {
  	    var result = gService.print.call("HidePassword", _args);    
	      var passwordId = "";
	      if (!result.error) {
	        passwordId = result.value["ID"];
	      }
      }
    
      var o = document.forms[0][id];
      var appid = o.getAttribute("appid");
      var recordid = o.getAttribute("recordid");
      var borrtype = o.getAttribute("borrtype");
      var isAddUli = o.getAttribute("isadduli");
      var args = "applicationid=" + appid + "&passwordid=" + passwordId;
      if (recordid != null)
        args += "&recordid=" + recordid;
      if (borrtype != null)
          args += "&borrtype=" + borrtype; 
      if(isAddUli != null)
      {
            args += "&isadduli=" + isAddUli;
      }

         
      downloadFile(file, args)
    }

  
    function createProcessingLabel() {
      gProcessingLabel = document.createElement("div");
      gProcessingLabel.id = "ProcessingLabel";
      gProcessingLabel.style.position = 'absolute';
      gProcessingLabel.style.width = "250px";
      gProcessingLabel.style.visibility = 'hidden';
      gProcessingLabel.style.backgroundColor = 'white';
      gProcessingLabel.style.color = "black";
      gProcessingLabel.style.border = "1px solid black";
      gProcessingLabel.style.padding = "10px";
      
      document.body.appendChild(gProcessingLabel);      
    }

    function clearMainForm(o, id) {
      highlightRowByCheckbox(o, gSelectedColor);
          
      document.forms[0][id].checked = false;
      highlightRowByCheckbox(document.forms[0][id]);
      updateCountLabel();
    }    
    function genericOnclick(o) {
      highlightRowByCheckbox(o, gSelectedColor);
      if (o.checked) gSelectedCount++;
      else gSelectedCount--;
      updateCountLabel();
      
    }
    function updateMessage() {
      var str = "Processing. Please wait ";
      for (var i = 0; i < gDotCount; i++)
        str += ".";
        
      gProcessingLabel.innerHTML = str;
      gProcessingLabel.style.left = (document.body.clientWidth / 2 - 150) + "px";
      gProcessingLabel.style.top = (document.body.scrollTop + (document.body.clientHeight / 2)) + "px";
      gDotCount = ++gDotCount % 10;
    }
    function displayProcessingMessage() {
	    gProcessingLabel.style.visibility = "visible";
	    gProcessingLabel.style.display = "";
	    updateMessage();
	    gProcessingIntervalID = setInterval(updateMessage, 400);
    }
    function hideProcessingMessage() {
	    gProcessingLabel.style.visibility = "hidden";
	    gProcessingLabel.style.display = "none";
	    clearInterval(gProcessingIntervalID);
    
    }

    function getPrintOptions() {
      <%-- 0 - Normal, form with data, 1 - Blank form., 2 - test Form --%> 
      var o = document.getElementById("printWithTestData");
      if (null != o && o.checked) {
        return 2;
      } else {
      return document.getElementById("printWithData").checked ? 0 : 1;
      }
    }
    
    function downloadFile(file, args) {
        var options = getPrintOptions();
        var url = gVirtualRoot + "/pdf/" + file + "?loanid=" + ML.sLId + "&printoptions=" + options + "&crack=" + new Date() + "&" + args;
        LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
        return false;
    }
    
    function downloadBatchPrint(id) {
        var url = gVirtualRoot + "/pdf/BatchPDF.aspx?id=" + id + "&crack=" + new Date();
        LqbAsyncPdfDownloadHelper.SubmitBatchPdfDownload(url);
        return false;
    }
    
	function downloadCustomForm(id) {
	    var options = getPrintOptions();	  
	    window.open(gVirtualRoot + '/newlos/DownloadCustomForm.aspx?loanid=' + ML.sLId + '&appid=' + ML.appId + '&id=' + id + '&printoptions=' + options + '&crack=' + new Date());
	}        

	// PageSize = 0 - LetterSize, 1 - LegalSize
	function downloadCustomPdf(id, pageSize) {
	    var options = getPrintOptions();
	    
	    var obj = document.getElementById(id);
	    
        var args = "&custompdfid=" + obj.getAttribute("custompdfid") + "&applicationid=" + obj.getAttribute("appid");
      
        var url = gVirtualRoot + "/pdf/Custom.aspx?loanid=" + ML.sLId + args + "&pagesize=" + pageSize + "&printoptions=" + options + "&crack=" + new Date();
        LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
	    return false;
	}
	
    function testBatchPrint() {
      selectAll(true);
      var tmp = batchWordPreview;
      batchWordPreview = function () { return null; } // temporarily disable this function for testing
      document.getElementById("btnDownload").click();
      batchWordPreview = tmp;
    }

	function hasCheck(event) {
	  var b = false;
	  var coll = document.getElementsByTagName("INPUT");
      var length = coll.length;
      if (length > 0) {
        for (var i = 0; i < length; i++) {
          var o = coll[i];
          if (o.type == 'checkbox' && o.checked) {
            b = true;
            break;
          }
        }
      } // end if (length > 0)
      if (!b) {
        alert('No form is selected to print.');
      }
      event.returnValue = false;
      return b;
	}

      function disabledButtons(b) {
        document.getElementById('btnDownload').disabled = b;
        var ePrintBtn = document.getElementById('btnEprint');
        if(ePrintBtn != null)
          ePrintBtn.disabled = b;
        document.getElementById('btnPrintToEDocs').disabled = b;
      }

	  function f_isInIgnoreList(id) {
	    if (id == 'IsPdfEncryption' ||
	        id == 'PdfPassword_sSpZip' ||
	        id == 'PdfPassword_Other' ||
	        id == 'printBlankForms' ||
	        id == 'printWithData' ||
	        id == 'printWithTestData')
	        return true;
	    return false;
	  }
	  
	  function generatePrintList() {
	    gCustomFormList = new Array();
	    var coll = document.getElementsByTagName("input");
	    var length = coll.length;
	    var args = new Object();
	    args["loanid"] = <%= AspxTools.JsString(LoanID) %>;
	    var currentIndex = 0;
	    for (var i = 0; i < length; i++) {
	      var o = coll[i];

	      if (f_isInIgnoreList(o.id)) continue;

	      if (o.checked && o.getAttribute("excludefrombatch") == null) {
	        if (o.getAttribute("customform") == "t") {
	          gCustomFormList.push(o.getAttribute('customletterid'));
	        } else if (o.getAttribute("custompdfform") == "t") {
	          args["item_" + currentIndex] = o.getAttribute("custompdfid");
	          args["type_" + currentIndex] = "custompdf";
	          args["pagesize_" + currentIndex] = o.getAttribute("pageSize");
	          args["appid_" + currentIndex] = o.getAttribute("appid");
	          args["isadduli" + currentIndex] = o.getAttribute("isadduli");
	          
	          currentIndex++;
	        } else {
	          args["item_" + currentIndex] = o.getAttribute("pdf");
	          args["appid_" + currentIndex] = o.getAttribute("appid");
	          args["pagesize_" + currentIndex] = o.getAttribute("pageSize");	          
	          if (o.getAttribute("recordid") != null)
	            args["recordid_" + currentIndex] = o.getAttribute("recordid");
	          if (o.getAttribute("borrtype") != null)
	              args["borrtype_" + currentIndex] = o.getAttribute("borrtype");
	          if (o.getAttribute("isadduli") != null)
	              args["isadduli" + currentIndex] = o.getAttribute("isadduli");
	            
	          currentIndex++;
	        }
	      }
	    }
	    args["count"] = currentIndex + "";
	    return args;
	  }

	  function batchPrint(event) {
	    if (!hasCheck(event))
	      return;

        displayProcessingMessage();
        disabledButtons(true);

	    var options = getPrintOptions();
	    var args = generatePrintList();
	    args["printoptions"] = options;
	    f_pdfEncryptionOptions(args);
	    if (args["count"] != "0") {
	      var result = gService.print.call("BatchPrint", args);
	      if (!result.error) {
	        downloadBatchPrint(result.value.PrintID);
	      }
	    }
	    
	    if (gCustomFormList.length > 0) 
	      batchWordPreview(event);
	      
	    hideProcessingMessage();
	    disabledButtons(false);
	  }
	  
	  function f_pdfEncryptionOptions(args) {
	    if (document.getElementById("IsPdfEncryption").checked) {
	      args["PdfPasswordOption"] = "Other";
	      args["PdfOtherPassword"] = <%= AspxTools.JsGetElementById(PdfOtherPassword) %>.value;
	      return true;
	    }
	    return false;
	  }
	  
    function batchWordPreview(event) {
        if (!hasCheck(event))
            return;

        for (var i = 0; i < gCustomFormList.length; ++i) {
            var customFormId = gCustomFormList[i];
            downloadCustomForm(customFormId);
        }
    }

    function eprintClick(event) {
        if (!hasCheck(event))
            return;
        var options = getPrintOptions();
        var args = generatePrintList();

        if (gCustomFormList.length > 0) {
            batchWordPreview(event);
            return;
        }

        args["printoptions"] = options;
        var callback =  function(isOk) {
            if (isOk) {
      
                var printerName = GetPrinterName();            
                if (args["count"] != 0) {
                    var isLqbPrinter = printerName == 'LendingQB';
                    var result = gService.print.call("BatchEPrint&IsLqbPrinter=" + isLqbPrinter, args);
                    if (!result.error)
                        batchEprint(result.value.LegalSizeID, result.value.LetterSizeID);
                }
            }
        }
        
        ShowPrintSetup(callback);
    }

    function f_getPrintId() {
        var result = gService.print.call("GetNewPrintId");
        if (!result.error)
            return result.value.PrintId;
        return '';
    }

    function batchEprint(legalSizeID, letterSizeID) {
        var hasLegal = legalSizeID != null;
        var hasLetter = letterSizeID != null;

        if (!hasLegal && !hasLetter) {
            return;
        }

        var printArray = [];
        var printSizeArray = [];

        var invokePrintControl = function () {
            var masterPrintCtrl = new ePrintCtrl();
            masterPrintCtrl.BatchPrint(printArray, printSizeArray, false, f_getPrintId, ML.sLNm);
        };

        if (ML.UseSynchronousPdfGenerationFallback)
        {
            if (hasLegal) {
                var url = gHostUrl + "/pdf/BatchPDF.aspx?cmd=submit&jobType=1&id=" + legalSizeID + '&' + ML.SynchronousPdfFallbackQueryStringParameterName + '=true&crack=' + new Date();
                printArray.push(url);
                printSizeArray.push('1'); // Legal size paper
            }

            if (hasLetter) {
                var url = gHostUrl + "/pdf/BatchPDF.aspx?cmd=submit&jobType=1&id=" + letterSizeID + '&' + ML.SynchronousPdfFallbackQueryStringParameterName + '=true&crack=' + new Date();
                printArray.push(url);
                printSizeArray.push('0'); // Letter size paper
            }

            invokePrintControl();
        }
        else if (hasLegal && hasLetter) {
            submitLetterLegalBatchEPrint(legalSizeID, letterSizeID, printArray, printSizeArray, invokePrintControl);
        }
        else if (hasLegal) {
            submitLegalBatchEPrint(legalSizeID, printArray, printSizeArray, invokePrintControl);
        }
        else if (hasLetter) {
            submitLetterBatchEPrint(letterSizeID, printArray, printSizeArray, invokePrintControl);
        }
    }

    function submitLetterLegalBatchEPrint(legalSizeID, letterSizeID, printArray, printSizeArray, callback) {
        submitLegalBatchEPrint(legalSizeID, printArray, printSizeArray, function () {
            submitLetterBatchEPrint(letterSizeID, printArray, printSizeArray, callback);
        });
    }

    function submitLegalBatchEPrint(legalSizeID, printArray, printSizeArray, callback) {
        var url = gVirtualRoot + "/pdf/BatchPDF.aspx?id=" + legalSizeID + "&crack=" + new Date();
        LqbAsyncPdfDownloadHelper.SubmitBatchPdfGeneration(url, function (fileDbKey) {
            printArray.push(gHostUrl + '/pdf/BatchPDF.aspx?cmd=retrieve&jobType=1&fileDbKey=' + fileDbKey);
            printSizeArray.push('1'); // Legal size paper
            callback();
        });
    }

    function submitLetterBatchEPrint(letterSizeID, printArray, printSizeArray, callback) {
        var url = gVirtualRoot + "/pdf/BatchPDF.aspx?id=" + letterSizeID + "&crack=" + new Date();
        LqbAsyncPdfDownloadHelper.SubmitBatchPdfGeneration(url, function (fileDbKey) {
            printArray.push(gHostUrl + '/pdf/BatchPDF.aspx?cmd=retrieve&jobType=1&fileDbKey=' + fileDbKey);
            printSizeArray.push('0'); // Letter size paper
            callback();
        });
    }
	  
	  function f_refresh_PdfEncryptionOptions() {

	    var isPdfEncryption = document.getElementById("IsPdfEncryption").checked;
	    document.getElementById("PdfOtherPassword").readOnly = !isPdfEncryption;
	    if (!isPdfEncryption)
	      document.getElementById("PdfOtherPassword").value = "";
	    
	  }
    function f_closeHelp() {
      document.getElementById("PdfEncryptionHelp").style.display = "none";
    }
    function f_showHelp(event) {
      var msg = document.getElementById("PdfEncryptionHelp");
      msg.style.display = "";
      msg.style.top = (event.clientY  )+ "px";
      msg.style.left = (event.clientX  ) + "px";
    }

    function openPrintToEDocs(event) {
        if (!hasCheck(event)) {
            return;
        }
        var printArgs = generatePrintList();
        printArgs["printoptions"] = getPrintOptions();
        if (gCustomFormList.length > 0) {
            alert('Custom Word Forms are not supported by Print to EDocs');
            return;
        } else if (document.getElementById("IsPdfEncryption").checked && !confirm('Print to EDocs is not compatible with password protected PDFs.  Continue without password?')) {
            return;
        }

        var edocMetadata = null;

        var args = {
            'setEDocMetadata': function(d) { edocMetadata = d; }
        };

        var settings = {
            'hideCloseButton': true,
            'height': 415,
            'width': 400,
            'onReturn': function() { returnFromPrintToEDocsEDocSettings(printArgs, edocMetadata); }
        };
        LQBPopup.Show(gVirtualRoot + '/newlos/ElectronicDocs/CreateEDocOptions.aspx?loanid=' + ML.sLId, settings, args);
    }

    function returnFromPrintToEDocsEDocSettings(printArgs, edocMetadata) {
        printArgs['EDoc_aAppId'] = edocMetadata.appId;
        printArgs['EDoc_DocTypeId'] = edocMetadata.docType;
        printArgs['EDoc_Description'] = edocMetadata.description;
        printArgs['EDoc_InternalComments'] = edocMetadata.internalComments;

        var onSuccess = function (result) { returnFromPrintToEDocsAction(printArgs, result); };
        var onError = function () { alert('System error. Please contact us if this happens again.') };
        window.setTimeout(function(){ LQBPopup.ShowElement($('#LoadingPopup'), { 'hideCloseButton':true, 'height': 200, 'width': 300 }); }, 0);
        gService.print.callAsyncBypassOverlay('PrintToEDocs', printArgs, onSuccess, onError);
    }

    var printToEdocsPollingHandle = null;
    var printToEdocsPollingAttempts = 0;
    function returnFromPrintToEDocsAction(printArgs, result) {
        if (ML.UseSynchronousPdfGenerationFallback) {
            pollForPrintToEdocsCallback(result);
            return;
        }

        if (result.value.Error) {
            window.setTimeout(LQBPopup.Hide, 0);
            alert(result.value.ErrorMessage);
            return;
        }

        printArgs['JobId'] = result.value.JobId;

        printToEdocsPollingAttempts = 0;

        var pollingIntervalMillis = /*ML.PdfGenerationBackgroundJobPollingIntervalSeconds*/10 * 1000;
        printToEdocsPollingHandle = window.setInterval(pollForPrintToEdocs, pollingIntervalMillis, printArgs);
    }

    function pollForPrintToEdocs(printArgs) {
        var onSuccess = pollForPrintToEdocsCallback;
        var onError = function () {
            window.setTimeout(LQBPopup.Hide, 0);
            alert('System error. Please contact us if this happens again.');
        };

        gService.print.callAsyncBypassOverlay('PrintToEDocs_Poll', printArgs, onSuccess, onError);
    }

    function pollForPrintToEdocsCallback(result) {
        if (result.value.Error) {
            window.setTimeout(LQBPopup.Hide, 0);
            window.clearInterval(printToEdocsPollingHandle);
            alert(result.value.ErrorMessage);
        }
        else if (result.value.Complete == "True") {
            window.setTimeout(LQBPopup.Hide, 0);
            window.clearInterval(printToEdocsPollingHandle);
            alert('Document(s) successfully printed to EDocs.');
        }
        else {
            ++printToEdocsPollingAttempts;

            if (printToEdocsPollingAttempts > ML.PdfGenerationBackgroundJobMaxPollingAttempts) {
                window.setTimeout(LQBPopup.Hide, 0);
                window.clearInterval(printToEdocsPollingHandle);
                alert('System error. Please contact us if this happens again.');
            }
        }
    }
        //-->
</script>

<form id=PrintList method=post runat="server">
<table id=Table1 cellSpacing=0 cellPadding=0 width="100%" border=0>
  <tr>
    <td class=MainRightHeader colSpan=2>Print</td></tr>
  <tr>
    <td class=FieldLabel style="PADDING-LEFT: 5px" noWrap >Print:</td>
    <td class=FieldLabel width="100%">
      <input id="printBlankForms" NoHighlight="True" NotForEdit="True" name="printoption" type="radio" value="1">blank forms &nbsp;
      <input id="printWithData" NoHighlight="True" NotForEdit="True" checked  name="printoption" type="radio" value="0">forms with data
      <% if (AllowPrintTestPdf) { %>
      &nbsp;<input id="printWithTestData" NoHighlight="True" NotForEdit="True" name="printoption" type="radio" value="2" />forms with test data
      <% } %>
      </td></tr>
  <tr style="PADDING-TOP: 2px">
    <td class=FieldLabel style="PADDING-LEFT: 5px" noWrap >Print Group:</td>
    <td style="padding-left:5px"><asp:dropdownlist id=m_printGroupDDL runat="server" AutoPostBack="True" enableviewstate="False" AlwaysEnable="True"></asp:dropdownlist></td></tr>
  <tr>
    <td colSpan=2>
      <hr>
    </td></tr>
  <tr>
    <td colspan=2>
      <table cellpadding=0 cellspacing=0 width="100%">
        <tr>
          <td align=center><img src="~/images/lock_email.gif" runat="server"></td>
          <td><input type=checkbox id="IsPdfEncryption" NotForEdit=True onclick="f_refresh_PdfEncryptionOptions();"> <label for="IsPdfEncryption"><b>Require this password to open your PDF:</b></label>&nbsp; <asp:textbox id=PdfOtherPassword runat="server" NotForEdit=True AlwaysEnable="True"></asp:textbox> &nbsp;(<a href="#" onclick="f_showHelp(event);" >why use a password?</a>)</td>
        </tr>
        <tr>
          <td colspan=2><hr></td>
        </tr>
        <tr style="padding-bottom:8px">
          <td align=center><IMG src="../images/print.gif"  <td td<IMG src="../images/print.gif"  <td td<IMG >
          <td style="PADDING-LEFT:4px">
              <input id=btnDownload onclick=batchPrint(event); style="width:222px" type=button value="Preview Selected Forms to Print or Email">
              <input id=btnEprint runat="server" onclick=eprintClick(event); style="width:139px" type=button value="Print Selected Forms ...">
              <input id="btnPrintToEDocs" onclick="openPrintToEDocs(event);" type="button" value="Print to EDocs" runat="server" />
      &nbsp;&nbsp;&nbsp;<span id=countLabel><b>0</b> 
      item(s) selected.</span>&nbsp;&nbsp;&nbsp;<input onclick=selectAll(true); type=button value="Select All" AlwaysEnable="True">&nbsp;<input onclick=selectAll(false); type=button value="Deselect All" AlwaysEnable="True"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table>
</table>
<div id="PrintFrame" style="border-right: 2px inset; border-top: 2px inset; margin: 0px; overflow: auto; border-left: 2px inset; border-bottom: 2px inset; padding-left: 3px">
  <ml:PassthroughLiteral ID="m_main" runat="server" EnableViewState="False"></ml:PassthroughLiteral>
</div>
<div id="PdfEncryptionHelp" class="HelpStyle" style="display: none">
  <table width="100%">
    <tr>
      <td><b><u>Secure your PDF by setting a password</u></b><br><br>You can use this option to set a password to protect your PDF.&nbsp; This password will be required to open the file -- a convenient security feature when emailing documents to your external contacts.</td>
    </tr>
    <tr>
      <td align="center">[<a id="help" href="#" onclick='f_closeHelp();'>Close</a>]</td>
    </tr>
  </table>
</div>
<div id="LoadingPopup" style="display:none;">
    <div class="LoadingPopupContent">
        <img alt="Loading" src="../images/loading.gif" />
        <div>Please wait while printing to EDocs...</div>
    </div>
</div>
</form>
	
  </body>
</HTML>
