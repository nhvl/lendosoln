﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using System.Collections.Generic;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos
{
    public partial class DetailsofTransactionServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override void Process(string methodName)
        {
            switch(methodName)
            {
                case "Migrate1003ToAdjustments":
                    this.Migrate1003ToAdjustments();
                    break;

            }
        }
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(DetailsofTransactionServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sAltCostLckd = GetBool("dot_sAltCostLckd");
            dataLoan.sAltCost_rep = GetString("dot_sAltCost");
            dataLoan.sFfUfmip1003Lckd = GetBool("dot_sFfUfmip1003Lckd");
            dataLoan.sFfUfmip1003_rep = GetString("dot_sFfUfmip1003");
            dataLoan.sLDiscnt1003Lckd = GetBool("dot_sLDiscnt1003Lckd");
            dataLoan.sLDiscnt1003_rep = GetString("dot_sLDiscnt1003");
            dataLoan.sLandCost_rep = GetString("dot_sLandCost");
            dataLoan.sOCredit1Amt_rep = GetString("dot_sOCredit1Amt");
            dataLoan.sOCredit1Desc = GetString("dot_sOCredit1Desc");
            dataLoan.sOCredit1Lckd = GetBool("dot_sOCredit1Lckd");

            if (false == dataLoan.sLoads1003LineLFromAdjustments)
            {
                dataLoan.sOCredit2Amt_rep = GetString("dot_sOCredit2Amt");
                dataLoan.sOCredit2Desc = GetString("dot_sOCredit2Desc");
                dataLoan.sOCredit3Amt_rep = GetString("dot_sOCredit3Amt");
                dataLoan.sOCredit3Desc = GetString("dot_sOCredit3Desc");
                dataLoan.sOCredit4Amt_rep = GetString("dot_sOCredit4Amt");
                dataLoan.sOCredit4Desc = GetString("dot_sOCredit4Desc");
            }

            dataLoan.sPurchPrice_rep = GetString("dot_sPurchPrice");
            dataLoan.sRefPdOffAmt1003Lckd = GetBool("dot_sRefPdOffAmt1003Lckd");
            dataLoan.sRefPdOffAmt1003_rep = GetString("dot_sRefPdOffAmt1003");
            dataLoan.sTotCcPbsLocked = GetBool("dot_sTotCcPbsLocked");
            dataLoan.sTotCcPbs_rep = GetString("dot_sTotCcPbs");
            dataLoan.sTotEstCc1003Lckd = GetBool("dot_sTotEstCc1003Lckd");
            dataLoan.sTotEstCcNoDiscnt1003_rep = GetString("dot_sTotEstCcNoDiscnt1003");
            dataLoan.sTotEstPp1003Lckd = GetBool("dot_sTotEstPp1003Lckd");
            dataLoan.sTotEstPp1003_rep = GetString("dot_sTotEstPp1003");
            dataLoan.sTransNetCashLckd = GetBool("dot_sTransNetCashLckd");
            dataLoan.sTransNetCash_rep = GetString("dot_sTransNetCash");

            if (!dataLoan.sIsIncludeONewFinCcInTotEstCc)
            {
                dataLoan.sONewFinCc_rep = GetString("dot_sONewFinCc");
            }
            else
            {
                dataLoan.sONewFinCc_rep = GetString("dot_sONewFinCc2");
            }

            dataLoan.sLAmtCalc_rep = GetString("dot_sLAmtCalc");
            dataLoan.sLAmtLckd = GetBool("dot_sLAmtLckd");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("dot_sAltCostLckd", dataLoan.sAltCostLckd);
            SetResult("dot_sAltCost", dataLoan.sAltCost_rep);
            SetResult("dot_sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
            SetResult("dot_sFfUfmip1003Lckd", dataLoan.sFfUfmip1003Lckd);
            SetResult("dot_sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep);
            SetResult("dot_sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("dot_sLAmt1003", dataLoan.sLAmt1003_rep);
            SetResult("dot_sLDiscnt1003", dataLoan.sLDiscnt1003_rep);
            SetResult("dot_sLDiscnt1003Lckd", dataLoan.sLDiscnt1003Lckd);
            SetResult("dot_sLandCost", dataLoan.sLandCost_rep);
            SetResult("dot_sOCredit1Amt", dataLoan.sOCredit1Amt_rep);
            SetResult("dot_sOCredit1Desc", dataLoan.sOCredit1Desc);
            SetResult("dot_sOCredit1Lckd", dataLoan.sOCredit1Lckd);
            SetResult("dot_sOCredit2Amt", dataLoan.sOCredit2Amt_rep);
            SetResult("dot_sOCredit2Desc", dataLoan.sOCredit2Desc);
            SetResult("dot_sOCredit3Amt", dataLoan.sOCredit3Amt_rep);
            SetResult("dot_sOCredit3Desc", dataLoan.sOCredit3Desc);
            SetResult("dot_sOCredit4Amt", dataLoan.sOCredit4Amt_rep);
            SetResult("dot_sOCredit4Desc", dataLoan.sOCredit4Desc);
            SetResult("dot_sOCredit5Amt", dataLoan.sOCredit5Amt_rep);
            SetResult("dot_sONewFinBal", dataLoan.sONewFinBal_rep);
            SetResult("dot_sPurchPrice", dataLoan.sPurchPrice_rep);
            SetResult("dot_sRefPdOffAmt1003", dataLoan.sRefPdOffAmt1003_rep);
            SetResult("dot_sRefPdOffAmt1003Lckd", dataLoan.sRefPdOffAmt1003Lckd);
            SetResult("dot_sTotCcPbs", dataLoan.sTotCcPbs_rep);
            SetResult("dot_sTotCcPbsLocked", dataLoan.sTotCcPbsLocked);
            SetResult("dot_sTotEstCc1003Lckd", dataLoan.sTotEstCc1003Lckd);
            SetResult("dot_sTotEstCcNoDiscnt1003", dataLoan.sTotEstCcNoDiscnt1003_rep);
            SetResult("dot_sTotEstPp1003", dataLoan.sTotEstPp1003_rep);
            SetResult("dot_sTotEstPp1003Lckd", dataLoan.sTotEstPp1003Lckd);
            SetResult("dot_sTotTransC", dataLoan.sTotTransC_rep);
            SetResult("dot_sTransNetCash", dataLoan.sTransNetCash_rep);
            SetResult("dot_sTransNetCashLckd", dataLoan.sTransNetCashLckd);
            SetResult("dot_sONewFinCc", dataLoan.sONewFinCc_rep);
            SetResult("dot_sONewFinCc2", dataLoan.sONewFinCc_rep);
            SetResult("dot_sLAmtCalc", dataLoan.sLAmtCalc_rep);
            SetResult("dot_sLAmtLckd", dataLoan.sLAmtLckd);
            SetResult("dot_sLoads1003LineLFromAdjustments", dataLoan.sLoads1003LineLFromAdjustments);
            SetResult("dot_sTotEstCcNoDiscnt", dataLoan.sTotEstCcNoDiscnt_rep);
            SetResult("dot_sTotEstPp", dataLoan.sTotEstPp_rep);
            SetResult("dot_sTotalBorrowerPaidProrations", dataLoan.sTotalBorrowerPaidProrations_rep);
        }

        private void Migrate1003ToAdjustments()
        {
            if (ConstStage.DisableUserMigrationToAdjustmentsMode)
            {
                return;
            }

            Guid loanId = this.GetGuid("loanId");

            var loan = CPageData.CreateUsingSmartDependency(loanId, typeof(DetailsofTransactionServiceItem));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (loan.sLoads1003LineLFromAdjustments)
            {
                Tools.LogError("Programming error.  Somehow Migrate1003ToAdjustments was called when sLoads1003LineLFromAdjustments was already true.");
                return;
            }

            BrokerDB db = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
            if (db.DontLink1003ToAdjustmentsOnPull)
            {
                loan.MigrateExisting1003ToAdjustments();
            }
            else
            {
                loan.sLoads1003LineLFromAdjustments = true;
            }

            loan.Save();
        }
    }

    public partial class DetailsofTransactionService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {

        protected override void Initialize()
        {
            AddBackgroundItem("", new DetailsofTransactionServiceItem());
        }
    }
}
