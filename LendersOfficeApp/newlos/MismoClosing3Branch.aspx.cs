﻿// <copyright file="MismoClosing33.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Justin Lara
//  Date:   November 18, 2015
// </summary>
namespace LendersOfficeApp.newlos
{
    using System;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Conversions.Mismo3.Version3;
    using LendersOffice.Conversions.Mismo3.Version4;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Security;

    public partial class MismoClosing3Branch : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool isUsing34 = LendersOffice.Common.RequestHelper.GetSafeQueryString("sub") == "f";

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(MismoClosing3Branch));
            dataLoan.InitLoad();
            
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.BufferOutput = true;
            Response.AddHeader("Content-Disposition", $"attachment; filename=\"{dataLoan.sLNm}.xml\"");

            var disclosedArchives = dataLoan.sClosingCostArchive.ToDictionary(
                a => a.Id,
                a => a.IsDisclosed);

            bool validClosingDisclosureOnFile =
                dataLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.Any(
                    cd => cd.ArchiveId == Guid.Empty
                    || (disclosedArchives.ContainsKey(cd.ArchiveId) && disclosedArchives[cd.ArchiveId]));

            VendorConfig.DocumentPackage documentPackage = VendorConfig.DocumentPackage.InvalidPackage;
            if (validClosingDisclosureOnFile)
            {
                documentPackage = new VendorConfig.DocumentPackage(
                    "Generic Document Package",
                    VendorConfig.PackageType.Closing,
                    hasGFE: false,
                    hasTIL: false,
                    hasLoanEstimate: false,
                    hasClosingDisclosure: true,
                    triggersPtmBilling: false);
            }
            else
            {
                bool validLoanEstimateOnFile =
                    dataLoan.sLoanEstimateDatesInfo.LoanEstimateDatesList.Any(
                        le => le.ArchiveId == Guid.Empty
                        || (disclosedArchives.ContainsKey(le.ArchiveId) && disclosedArchives[le.ArchiveId]));

                documentPackage = new VendorConfig.DocumentPackage(
                    "Generic Document Package",
                    validLoanEstimateOnFile ? VendorConfig.PackageType.Redisclosure : VendorConfig.PackageType.Initial,
                    hasGFE: false,
                    hasTIL: false,
                    hasLoanEstimate: true,
                    hasClosingDisclosure: false,
                    triggersPtmBilling: false);
            }

            string xml = string.Empty;
            if (!isUsing34)
            {
                xml = Mismo33RequestProvider.SerializeMessage(
                loanIdentifier: LoanID,
                vendorAccountID: null,
                transactionID: Guid.NewGuid().ToString(),
                docPackage: documentPackage,
                includeIntegratedDisclosureExtension: true,
                principal: PrincipalFactory.CurrentPrincipal,
                vendor: this.Broker.Mismo33SidebarLinkDocumentVendor,
                useMismo3DefaultNamespace: true);
            }
            else
            {
                Mismo34ExporterOptions exporterOptions = new Mismo34ExporterOptions()
                {
                    LoanId = LoanID,
                    TransactionId = Guid.NewGuid().ToString(),
                    DocumentPackage = documentPackage,
                    IncludeIntegratedDisclosureExtension = true,
                    Principal = PrincipalFactory.CurrentPrincipal,
                    DocumentVendor = this.Broker.Mismo33SidebarLinkDocumentVendor,
                    UseMismo3DefaultNamespace = true
                };

                Mismo34Exporter exporter = new Mismo34Exporter(exporterOptions);
                xml = exporter.SerializePayload();
            }

            Response.Write(xml);
            Response.Flush();
            Response.End();
        }
    }
}