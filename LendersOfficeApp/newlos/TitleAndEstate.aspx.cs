namespace LendersOfficeApp.newlos
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using DataAccess.Sellers;
    using LendersOffice.Common;
    using MeridianLink.CommonControls;

    public partial class TitleAndEstate : BaseLoanPage
    {
        protected bool useNewNonPurchaseBorrower = false;

        private CPageData dataLoan;

        private ListItem[] SellerTypes = new ListItem[]
        {
            new ListItem("Individual", E_SellerType.Individual.ToString()),
            new ListItem("Entity", E_SellerType.Entity.ToString())
        };
        private ListItem[] SellerEntityTypes = new ListItem[]
        {
            new ListItem("-- Select Entity Type --", E_SellerEntityType.Undefined.ToString()),
            new ListItem("Company", E_SellerEntityType.Company.ToString()),
            new ListItem("Corporation", E_SellerEntityType.Corporation.ToString()),
            new ListItem("General Partnership", E_SellerEntityType.GeneralPartnership.ToString()),
            new ListItem("Limited Liability Company", E_SellerEntityType.LimitedLiabilityCompany.ToString()),
            new ListItem("Limited Liability Corporation", E_SellerEntityType.LimitedLiabilityCorporation.ToString()),
            new ListItem("Limited Partnership", E_SellerEntityType.LimitedPartnership.ToString()),
            new ListItem("Sole Proprietor", E_SellerEntityType.SoleProprietor.ToString())
        };
       
        protected void Page_Load(object sender, EventArgs e)
        {
            dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(TitleAndEstate));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            this.aBNm.Text = dataApp.aBNm;
            this.aCNm.Text = dataApp.aCNm;

            this.RegisterJsGlobalVariables("IsTargeting2019Ulad", dataLoan.sIsTargeting2019Ulad);
            this.RegisterJsGlobalVariables("HasCoborrower", dataApp.aHasCoborrower);
            this.RegisterJsGlobalVariables("BorrowerIsCurrentTitleOnly", dataApp.aBTypeT == E_aTypeT.CurrentTitleOnly);
            this.RegisterJsGlobalVariables("CoborrowerIsCurrentTitleOnly", dataApp.aCTypeT == E_aTypeT.CurrentTitleOnly);
            this.RegisterJsGlobalVariables("EmailValidationExpression", LendersOffice.Constants.ConstApp.EmailValidationExpression);

            aTitleNm1.Text = dataApp.aTitleNm1;
            aTitleNm1Lckd.Checked = dataApp.aTitleNm1Lckd;
            aTitleNm2.Text = dataApp.aTitleNm2;
            aTitleNm2Lckd.Checked = dataApp.aTitleNm2Lckd;

            Tools.SetDropDownListValue(this.aBIsCurrentlyOnTitle, dataApp.aBIsCurrentlyOnTitle);
            Tools.SetDropDownListValue(this.aCIsCurrentlyOnTitle, dataApp.aCIsCurrentlyOnTitle);

            this.aBCurrentTitleName.Text = dataApp.aBCurrentTitleName;
            this.aBCurrentTitleNameLckd.Checked = dataApp.aBCurrentTitleNameLckd;

            this.aCCurrentTitleName.Text = dataApp.aCCurrentTitleName;
            this.aCCurrentTitleNameLckd.Checked = dataApp.aCCurrentTitleNameLckd;

            aManner.Text = dataApp.aManner;

            Tools.SetDropDownListValue(sEstateHeldT, dataLoan.sEstateHeldT);
            sLeaseHoldExpireD.Text = dataLoan.sLeaseHoldExpireD_rep;

            sDwnPmtSrc.Text = dataLoan.sDwnPmtSrc;
            sDwnPmtSrcExplain.Text = dataLoan.sDwnPmtSrcExplain;

            Tools.SetDropDownListValue(this.sNativeAmericanLandsT, dataLoan.sNativeAmericanLandsT);

            sSellerVesting.Text = dataLoan.sSellerVesting;

            if (dataLoan.sSellerCollection.IsEmpty())
            {
                NumberOfSellers.SelectedValue = "0";
            }
            else
            {
                NumberOfSellers.SelectedValue = dataLoan.sSellerCollection.ListOfSellers.Count.ToString();
                Sellers.DataSource = dataLoan.sSellerCollection.ListOfSellers;
                Sellers.DataBind();
            }

            SellerCollection sellersFromAgents = new SellerCollection();
            sellersFromAgents.PopulateFromAgents(dataLoan.GetAgentsOfRole(E_AgentRoleT.Seller, E_ReturnOptionIfNotExist.ReturnEmptyObject));
            RegisterJsObjectArray("AgentsOfRoleSeller", sellersFromAgents.IsEmpty() ? null : sellersFromAgents.ListOfSellers);
            
            List<TitleBorrower> borrowers = dataLoan.sTitleBorrowers;

            TitleOnlyBorrowers.DataSource = borrowers;
            TitleOnlyBorrowers.DataBind();
            TitleOnlyBorrowerJSON.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(dataLoan.sTitleBorrowers);

            useNewNonPurchaseBorrower = dataLoan.BrokerDB.IsUseNewNonPurchaseSpouseFeature;
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Title and Estate";
            this.PageID = "TitleAndEstate";

            Tools.Bind_TriState(this.aBIsCurrentlyOnTitle);
            Tools.Bind_TriState(this.aCIsCurrentlyOnTitle);
            Tools.BindComboBox_aManner(aManner);
            Tools.BindComboBox_DownPaymentSource(sDwnPmtSrc);
            Tools.Bind_sEstateHeldT(sEstateHeldT);
            Tools.Bind_sNativeAmericanLandsT(this.sNativeAmericanLandsT);

            for (int i = 0; i <= 10; i++)
                NumberOfSellers.Items.Add(i.ToString());

            SellerTemplateSellerType.Items.AddRange(SellerTypes);
            SellerTemplateSellerEntityType.Items.AddRange(SellerEntityTypes);

            this.RegisterJsScript("TitleOnlyBorrowerUtils.js");
        }


        protected void TitleOnlyBorrowers_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType == ListItemType.Item || 
                args.Item.ItemType == ListItemType.AlternatingItem ||
                args.Item.ItemType == ListItemType.Header)
            {
                TitleBorrower borrower = (TitleBorrower)args.Item.DataItem;
                DropDownList titleRelationshipDDL = args.Item.FindControl("TitleRelationshipTitleT") as DropDownList;
                Tools.Bind_aBRelationshipTitleT(titleRelationshipDDL);

                DropDownList associatedApplicationDDL = args.Item.FindControl("TitleAssociatedApplicationId") as DropDownList;
                dataLoan.LoadAppNames(associatedApplicationDDL);
                associatedApplicationDDL.Items.Insert(0, new ListItem("-- NONE --", Guid.Empty.ToString()));

                if (args.Item.ItemType != ListItemType.Header)
                {
                    DateTextBox dob = args.Item.FindControl("TitleDob") as DateTextBox;
                    dob.Text = borrower.Dob_rep;

                    Tools.SetDropDownListValue(titleRelationshipDDL, borrower.RelationshipTitleT);

                    if (borrower.AssociatedApplicationId != Guid.Empty)
                    {
                        Tools.SetDropDownListValue(associatedApplicationDDL, borrower.AssociatedApplicationId.ToString());
                    }

                    HiddenField aliases = (HiddenField)args.Item.FindControl("TitleAliases");
                    aliases.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(borrower.Aliases);
                }
            }
        }

        protected void SellerList_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            var seller = (Seller)args.Item.DataItem;
            TextBox name = (TextBox)args.Item.FindControl("Name");
            HtmlInputHidden agentId = (HtmlInputHidden)args.Item.FindControl("AgentId");
            TextBox streetAddress = (TextBox)args.Item.FindControl("StreetAddress");
            TextBox city = (TextBox)args.Item.FindControl("City");
            StateDropDownList state = (StateDropDownList)args.Item.FindControl("State");
            TextBox zipcode = (TextBox)args.Item.FindControl("Zipcode");
            DropDownList sellerType = (DropDownList)args.Item.FindControl("SellerType");
            DropDownList sellerEntityType = (DropDownList)args.Item.FindControl("SellerEntityType");

            name.Text = seller.Name;
            agentId.Value = seller.AgentId.ToString();
            streetAddress.Text = seller.Address.StreetAddress;
            city.Text = seller.Address.City;
            state.Value = seller.Address.State;
            zipcode.Text = seller.Address.PostalCode;

            sellerType.Items.AddRange(SellerTypes);
            sellerType.SelectedValue = seller.Type.ToString();

            sellerEntityType.Items.AddRange(SellerEntityTypes);
            if (seller.Type == E_SellerType.Entity)
            {
                sellerEntityType.SelectedValue = seller.EntityType.ToString();
            }
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
