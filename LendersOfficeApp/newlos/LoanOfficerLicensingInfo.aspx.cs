﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Migration;
using LendersOfficeApp.newlos.Status;

namespace LendersOfficeApp.newlos
{
    public partial class LoanOfficerLicensingInfo : BaseLoanPage
    {
        protected ContactFieldMapper CFM;

        protected void Page_Load(object sender, EventArgs e)
        {

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(LoanOfficerLicensingInfo));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            Tools.SetDropDownListValue(aIntrvwrMethodT, dataApp.aIntrvwrMethodT);
            aIntrvwrMethodTLckd.Checked = dataApp.aIntrvwrMethodTLckd;
            a1003InterviewD.Text = dataApp.a1003InterviewD_rep;
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V17_UpdateApplicationSubmittedCalculation))
            {
                a1003InterviewDLckd.Checked = dataApp.a1003InterviewDLckd;
            }
            else
            {
                a1003InterviewDLckd.Visible = false;
            }

            IPreparerFields interviewer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            LoanOfficerName.Text = interviewer.PreparerName;
            LoanOfficerPhone.Text = interviewer.Phone;
            LoanOfficerEmail.Text = interviewer.EmailAddr;
            LoanOfficerLicenseNumber.Text = interviewer.LicenseNumOfAgent; // 11/15/06 mf. OPM 7970
            LoanOfficerCompanyLoanOriginatorIdentifier.Text = interviewer.CompanyLoanOriginatorIdentifier;
            LoanOfficerLoanOriginatorIdentifier.Text = interviewer.LoanOriginatorIdentifier;
            BrokerLicenseNumber.Text = interviewer.LicenseNumOfCompany;
            BrokerName.Text = interviewer.CompanyName;
            BrokerStreetAddr.Text = interviewer.StreetAddr;
            BrokerCity.Text = interviewer.City;
            BrokerState.Value = interviewer.State;
            BrokerZip.Text = interviewer.Zip;
            BrokerPhone.Text = interviewer.PhoneOfCompany;
            BrokerFax.Text = interviewer.FaxOfCompany;
            CFM.IsLocked = interviewer.IsLocked;
            CFM.AgentRoleT = interviewer.AgentRoleT;

            //OPM 43665 : Populate the state license fields only when the subject property state in the file is not empty
            if (!string.IsNullOrEmpty(dataLoan.sSpState))
            {
                CFM.AgentLicenseField = LoanOfficerLicenseNumber.ClientID;
                CFM.CompanyLicenseField = BrokerLicenseNumber.ClientID;
            }
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Loan Officer Licensing Info";
            this.PageID = "LoanOfficerLicensingInfo";

            Tools.Bind_aIntrvwrMethodT(aIntrvwrMethodT);

            CFM.IsAllowLockableFeature = true;
            CFM.Type = "19";
            CFM.AgentNameField = LoanOfficerName.ClientID;
            CFM.PhoneField = LoanOfficerPhone.ClientID;
            CFM.EmailField = LoanOfficerEmail.ClientID;
            CFM.StreetAddressField = BrokerStreetAddr.ClientID;
            CFM.CityField = BrokerCity.ClientID;
            CFM.StateField = BrokerState.ClientID;
            CFM.ZipField = BrokerZip.ClientID;
            CFM.CompanyNameField = BrokerName.ClientID;
            CFM.CompanyStreetAddr = BrokerStreetAddr.ClientID;
            CFM.CompanyCity = BrokerCity.ClientID;
            CFM.CompanyState = BrokerState.ClientID;
            CFM.CompanyZip = BrokerZip.ClientID;
            CFM.CompanyPhoneField = BrokerPhone.ClientID;
            CFM.CompanyFaxField = BrokerFax.ClientID;
            CFM.LoanOriginatorIdentifierField = LoanOfficerLoanOriginatorIdentifier.ClientID;
            CFM.CompanyLoanOriginatorIdentifierField = LoanOfficerCompanyLoanOriginatorIdentifier.ClientID;

            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
