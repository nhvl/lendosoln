using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web.Services;
using DataAccess;
using DataAccess.GFE;
using LendersOffice.AntiXss;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendersOfficeApp.los.admin;
using System.Xml;
using LendersOffice.Migration;

namespace LendersOfficeApp.newlos
{
	public partial class LeftSummaryFrameService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        private Guid sLId { get { return GetGuid("loanid"); } }


        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "RefreshInfo":
                    RefreshInfo();
                    break;
                case "UpdateLoanEditorMenuTreeState":
                    UpdateLoanEditorMenuTreeState();
                    break;
                case "RefreshAppList":
                    RefreshAppList();
                    break;
                case "UpdatePendingDirtyBit":
                    UpdatePendingDirtyBit();
                    break;
            }
        }

        private void UpdatePendingDirtyBit()
        {
            AbstractUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal as AbstractUserPrincipal;
            if (null != principal)
            {
                SqlParameter[] parameters = {
                                                        new SqlParameter("@sLId", sLId),
                                                        new SqlParameter("@UserId", principal.UserId),
                                                        new SqlParameter("@UserName", principal.DisplayNameForAuditRecentModification),
                                                        new SqlParameter("@UserType", principal.Type),
                                                        new SqlParameter("@Action", 2) // 2 - For Pending IsDirty
                                                    };
                StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LoanModification_Insert", 3, parameters);
            }
        }

        private void RefreshInfo() 
        {
            CPageData dataLoan = new CLeftSummaryData(sLId);
            dataLoan.InitLoad();

            SetResult("sStatusT", dataLoan.sStatusT_rep);
            SetResult("sLNm", dataLoan.sLNm);
            SetResult("LoanOfficer", dataLoan.sEmployeeLoanRepName);
            SetResult("sQualTopR", dataLoan.sQualTopR_rep);
            SetResult("sQualBottomR", dataLoan.sQualBottomR_rep);
            SetResult("sLtvR", dataLoan.sLtvR_rep);
            SetResult("sCltvR", dataLoan.sCltvR_rep);
            SetResult("sHcltvR", dataLoan.sHcltvR_rep);
            SetResult("sRateLockStatusT", dataLoan.sRateLockStatusT_rep);
            SetResult("sLT", dataLoan.sLT_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sDocMagicPlanCodeId", dataLoan.sDocMagicPlanCodeId);
            SetResult("sClosingCostFeeVersionT", dataLoan.sClosingCostFeeVersionT.ToString());

            // OPM 236812 - TRID changes to Disclosure Pipeline Trigger for APR.
            if (dataLoan.sIsAprOutOfTolerence)
            {
                var APRDelta = Math.Abs(dataLoan.sApr - dataLoan.sLastDiscAPR);
                SetResult("APRDelta", APRDelta);
                SetResult("NeedsAPRAlert", "1");
            }

            LoanVersionT currentVersion = dataLoan.sLoanVersionT;
            LoanVersionT latestVersion = LoanDataMigrationUtils.GetLatestVersion();
            bool isLoanVersionTUpToDate = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(currentVersion, latestVersion);
            SetResult("LoanVersionTUpToDate", isLoanVersionTUpToDate.ToString());
            if (!isLoanVersionTUpToDate)
            {
                SetResult("LoanVersionTCurrent", (int)currentVersion);
                SetResult("LoanVersionTLatest", (int)latestVersion);
            }
        }

        private void UpdateLoanEditorMenuTreeState() 
        {
            // EM - OPM 2108863 - Saving the Tree State now merges the old and new states.
            string xml = GetString("xml");
            string oldXml = GetString("loanEditorMenuTreeStateXmlContent");

            if (oldXml != null && oldXml != "" && xml != null && xml != "")
            {
                XmlDocument doc = DataAccess.Tools.CreateXmlDoc(xml);
                XmlDocument oldDoc = DataAccess.Tools.CreateXmlDoc(oldXml);

                foreach (XmlNode node in oldDoc.DocumentElement.ChildNodes)
                {
                    string attr = node.Attributes["path"].Value;
                    if (doc.SelectNodes("/root/folder[@path=\"" + node.Attributes["path"].Value + "\"]").Count <= 0)
                    {
                        var importedNode = doc.ImportNode(node, true);
                        doc.DocumentElement.AppendChild(importedNode);
                    }
                }

                xml = doc.OuterXml;
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@UserID", BrokerUserPrincipal.CurrentPrincipal.UserId),
                                            new SqlParameter("@LoanEditorMenuTreeStateXmlContent", xml)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(BrokerUserPrincipal.CurrentPrincipal.BrokerId, "UpdateLoanEditorMenuTreeState", 5, parameters);
        }

        private void RefreshAppList()
        {
            CAppData dataApp = null;
            List<CAppData> list = new List<CAppData>();

            CPageData dataLoan = new CLeftSummaryData(sLId);
            dataLoan.InitLoad();

            for (int i = 0; i < dataLoan.nApps; i++)
            {
                dataApp = dataLoan.GetAppData(i);
                list.Add(dataApp);
            }
            string result = ToJson(list);
            SetResult("appList", result);
        }

        private string ToJson(List<CAppData> list)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("({");
            sb.Append("'length':");
            sb.Append(list.Count);
            sb.Append(",");
            sb.Append("'values':");
            sb.Append("[");
            for (int i = 0; i < list.Count; i++)
            {
                if (i != 0)
                    sb.Append(",");
                sb.Append(ToJson(list[i]));
            }

            sb.Append("]");
            sb.Append("})");

            return sb.ToString();
        }

        private string ToJson(CAppData ap)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{");

            sb.Append("'id':");
            sb.Append(AspxTools.JsString(ap.aAppId));
            sb.Append(",");

            sb.Append("'name':");
            sb.Append(AspxTools.JsString(ap.aAppNm));
            sb.Append("}");

            return sb.ToString();
        }
        [WebMethod]
        public static object GetComplianceInfo(Guid sLId, bool checkingError)
        {
            // 2015-10-14 - dd - This method get call A LOT on production environment. Therefore to save network bandwith 
            // to SQL server, I used NotEnforceAccessControlPageData which completely bypass workflow check.
            // This method is not crucial the make sure the user has access to the file.

            // 2016-11-18 - mf - This method is still the source of a lot of pain to prod servers.  Since we do not 
            // care about access control for this data, just pull it directly from the DB for speed below.
            // InitLoad() still does a lot of expensive extra work we do not care about.

            var status = Tools.GetLoanComplianceEaseStatus(sLId, PrincipalFactory.CurrentPrincipal.BrokerId);
            var sComplianceEaseStatusT = Tools.GetComplianceEaseStatusT(status);

            var error = sComplianceEaseStatusT == E_sComplianceEaseStatusT.Errors;
            var updating = sComplianceEaseStatusT == E_sComplianceEaseStatusT.Updating;

            if (checkingError && !error)
            {
                Tools.LogBug("User was presented with the option of viewing ComplianceEase errors, when there were no errors in the db. LoanId: '" + sLId + "'");
            }

            return new
            {
                IsUpdating = updating,
                IsError = error,
                Status = status,
                StatusClass = status,
                RefreshTimeSeconds = ConstStage.ComplianceEaseIndicatorRefreshSeconds
            };
        }
        
        [WebMethod]
        public static object GetGfeToleranceInfo(Guid sLId)
        {
            try
            {
                var gfeToleranceStatus = GfeToleranceViolationCalculator.GetGfeToleranceInfo(sLId, BrokerUserPrincipal.CurrentPrincipal);
                return new
                {
                    NeedsGfeZeroAlert = gfeToleranceStatus.NeedsGfeZeroAlert,
                    NeedsGfeTenAlert = gfeToleranceStatus.NeedsGfeTenAlert,
                    MinimumSecondsBetweenGfeToleranceRefreshes = ConstStage.MinimumSecondsBetweenGfeToleranceRefreshes,
                    RedisclosingPendingArchiveWillClearZeroPercentCure = gfeToleranceStatus.RedisclosingPendingArchiveWillClearZeroPercentCure,
                    RedisclosingPendingArchiveWillClearTenPercentCure = gfeToleranceStatus.RedisclosingPendingArchiveWillClearTenPercentCure
                };
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }
	}
}
