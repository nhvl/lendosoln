<%@ Page language="c#" Codebehind="BorrowerREOList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.BorrowerREOList" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>BorrowerREOList</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="FlowLayout"  scroll="yes" leftmargin=0 bgcolor="gainsboro" style="CURSOR: default">
    <script type="text/javascript">
    function _init() {
      list_oTable = document.getElementById("<%= AspxTools.ClientId(m_dg) %>");
      parent.parent_initScreen(2);
    }
    function constructEmptyRow(tr) {
      // Construct empty row.
      var td = tr.insertCell();
      td.innerText = " ";
      td = tr.insertCell();
      td.innerText = " ";
      td = tr.insertCell();
      td.innerText = " ";
      td = tr.insertCell();
      td.innerText = " ";
    }    
        function refreshCalculation() {
            callFrameMethod(parent, "SingleIFrame", "refreshCalculation");
        }
    </script>	
    <form id="BorrowerREOList" method="post" runat="server">
      <asp:DataGrid id=m_dg AutoGenerateColumns="False" Width="100%" runat="server" BorderColor="Gainsboro" enableviewstate="False">
<alternatingitemstyle cssclass="GridItem">
</AlternatingItemStyle>

<itemstyle cssclass="GridItem">
</ItemStyle>

<headerstyle cssclass="GridHeader">
</HeaderStyle>

<footerstyle cssclass="GridFooter">
</FooterStyle>

<columns>
<asp:TemplateColumn SortExpression="Addr" HeaderText="Address">
<itemtemplate>
<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Addr").ToString())%>, <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "City").ToString())%>, <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "State").ToString())%> <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Zip").ToString())%>
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn HeaderText="Is Subj Prop?">
<headerstyle width="90px">
</HeaderStyle>

<itemstyle width="90px">
</ItemStyle>

<itemtemplate>
<%# AspxTools.HtmlString(displayBool(DataBinder.Eval(Container.DataItem, "IsSubjectProp").ToString()))%>
</ItemTemplate>
</asp:TemplateColumn>
<asp:BoundColumn DataField="Stat" SortExpression="Stat" HeaderText="Status">
<headerstyle width="50px">
</HeaderStyle>

<itemstyle width="50px">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="NetRentI" SortExpression="NetRentI" HeaderText="Cash Flow">
<headerstyle width="90px">
</HeaderStyle>

<itemstyle width="90px">
</ItemStyle>
</asp:BoundColumn>
</Columns>
      </asp:DataGrid>
     </form>
	
  </body>
</html>
