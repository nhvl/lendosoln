<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="LoanProspectorSendConfirmation.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.LoanProspectorSendConfirmation" %>
<%@ Import namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>LoanProspectorSendConfirmation</title>
    <link href="<%=AspxTools.SafeUrl(StyleSheet)%>" rel="stylesheet" type="text/css">    
  </HEAD>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro">
	<script language=javascript>
<!--
function _init()
{
  resize( 600 , 200 );
}
function f_choice(choice) {    
  var args = window.dialogArguments || {};
  args.Choice = choice;
  onClosePopup(args);
}
//-->
</script>
    <h4 class="page-header">Send To Loan Prospector</h4>
    <form id="LoanProspectorSendConfirmation" method="post" runat="server">
<table width="100%" cellpadding=10>
<tr>
<td>
    <ml:EncodedLiteral id="descriptionLabel" runat="server">This loan file was previously send to Loan Product Advisor and did not receive Send To Your System from Loan Product Advisor. Choose Go to Loan Product Advisor Main Menu option to search for existing loan. Send to Loan Product Advisor will result a new file.</ml:EncodedLiteral>
</td>
</tr>
<tr><td align=center>
<input id="btnGoToLp" type=button value="Go To Loan Product Advisor Main Menu" onclick="f_choice(0);" style="WIDTH:245px" runat="server">
<input id="btnSendToLp" type=button value="Send To Loan Product Advisor" onclick="f_choice(1);" style="WIDTH:180px" runat="server">
<input type=button value="Cancel" onclick="f_choice(2);">
</td></tr>
</table>
     </form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
	
  </body>
</HTML>
