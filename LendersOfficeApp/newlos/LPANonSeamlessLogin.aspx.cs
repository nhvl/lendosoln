﻿#region Auto-generated code
namespace LendersOfficeApp.newlos
#endregion
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Integration.LoanProductAdvisor;
    using LendersOffice.ObjLib.Conversions.Aus;
    using LendersOffice.Security;
    using static LendersOffice.ObjLib.Conversions.Aus.AusCreditOrderingInfo;

    /// <summary>
    /// LPA non-seamless login page.
    /// </summary>
    public partial class LPANonSeamlessLogin : BaseLoanPage
    {
        /// <summary>
        /// Gets JQuery version.
        /// </summary>
        /// <returns>JQuery 1.11.2.</returns>
        protected override E_JqueryVersion GetJQueryVersion()
        {
            return E_JqueryVersion._1_11_2;
        }

        /// <summary>
        /// Workflow operations to check on loading the page.
        /// </summary>
        /// <returns>The "Run LP" workflow operation.</returns>
        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[] { WorkflowOperations.RunLp };
        }

        /// <summary>
        /// Page initialization function.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.Load += this.PageLoad;
            this.EnableJquery = true;
            this.DisplayCopyRight = false;
            this.UseNewFramework = true;

            this.RegisterCSS("stylesheetnew.css");
            this.RegisterJsScript("LQBPopup.js");

            Tools.Bind_IPair(sFredProcPointT, Tools.sFredProcPointT_pairs);

            MainCreditReportingCompanies.DataSource = Tools.FreddieMacCreditReportingCompanyMapping;
            MainCreditReportingCompanies.DataBind();
            TechnicalAffiliates.DataSource = Tools.FreddieMacTechnicalAffiliateMapping;
            TechnicalAffiliates.DataBind();

            base.OnInit(e);
        }

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void PageLoad(object sender, EventArgs e)
        {
            var loan = CPageData.CreateUsingSmartDependencyForLoad(this.LoanID, typeof(LPANonSeamlessLogin));

            var craInfo = AusCreditOrderingInfo.LoadCraFromCreditReportForFreddie(loan, this.BrokerID);
            string cra = craInfo.Item1;
            List<TmpBorrower> borrowerList = craInfo.Item2;
            if (string.IsNullOrEmpty(cra))
            {
                cra = LoanSubmitRequest.GetTemporaryCraInformation(loan.sLId)?.CraProviderId;
            }

            this.borrowerCreditReferenceRepeater.DataSource = borrowerList;
            this.borrowerCreditReferenceRepeater.DataBind();
            this.borrowerCreditReorderRepeater.DataSource = borrowerList;
            this.borrowerCreditReorderRepeater.DataBind();

            this.orderInfileCredit.Checked = loan.sHasFreddieInfileCredit;
            this.orderMergedCredit.Checked = !loan.sHasFreddieInfileCredit;

            Tools.SetDropDownListValue(sFredProcPointT, loan.sFredProcPointT);
            sFreddieLoanId.Text = loan.sFreddieLoanId;
            sFreddieTransactionId.Text = loan.sFreddieTransactionId;
            sLpAusKey.Text = loan.sLpAusKey;
            sFreddieSellerNum.Text = loan.sFreddieSellerNum;
            sFreddieTpoNum.Text = loan.sFreddieTpoNum;
            sFreddieNotpNum.Text = loan.sFreddieNotpNum;
            sFreddieLpPassword.Text = loan.sFreddieLpPassword.Value;
            sFreddieLenderBranchId.Text = loan.sFreddieLenderBranchId;

            this.RegisterJsGlobalVariables("cra", cra);
            this.RegisterJsGlobalVariables("Constant_PendingLoanTransaction", ConstAppDavid.LoanProspector_PendingLoanTransaction);
            this.RegisterJsGlobalVariables("Constant_LPAGoToMain", ConstAppDavid.LoanProspector_EntryPoint_GoToMain);
            this.RegisterJsGlobalVariables("Constant_SendToLPA", ConstAppDavid.LoanProspector_EntryPoint_SendLp);
            this.RegisterJsGlobalVariables("hasLpPassword", loan.sFreddieLpPassword.Value != string.Empty);
            this.RegisterJsGlobalVariables("pollingInterval", 1000);
        }
    }
}