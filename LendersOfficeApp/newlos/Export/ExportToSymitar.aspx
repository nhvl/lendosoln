﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExportToSymitar.aspx.cs" Inherits="LendersOfficeApp.newlos.Export.ExportToSymitar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Export to Symitar</title>
</head>
<body>
    <div ng-app="ExportToSymitar">
        <script type="text/ng-template" id="ExportDataSection">
        <button id="Section{{$id}}" type="button" class="accordion active" ng-click="toggleSection($id, table.Name)">{{table.Name}} &#x25ba;</button>
        <div class="panel show">
            <table class="FullWidth BottomMargin">
                <tr class="LoanFormHeader" ng-hide="{{table.Rows.length == 0}}">
                    <th>Description</th>
                    <th>Mnemonic</th>
                    <th>Value</th>
                </tr>
                <tr ng-repeat="row in table.Rows" class="{{$index % 2 == 0 ? 'GridItem' : 'GridAlternatingItem'}}">
                    <td>{{row.Description}}</td>
                    <td>{{row.Mnemonic}}</td>
                    <td>
                        {{row.Value}} 
                        <img src="../../images/fail.png" class="ExportErrorImage" ng-show="row.HasError" data-toggle="tooltip" data-placement="right"
                            title="The value '{{row.Value}}' did not parse into a valid value for a {{row.ValueDataType}}." />
                    </td>
                </tr>
                <tr class="Center" ng-show="{{table.Rows.length == 0}}">
                    <td colspan="3">No mappings at this record level</td>
                </tr>
            </table>
            <div class="Indent BottomMargin" ng-repeat="table in table.Subsections" ng-include="'ExportDataSection'"></div>
        </div>
        </script>

        <div ng-controller="ExportToSymitarController">
            <form id="form1" runat="server">
                <table id="HeaderTable">
		            <tr>
			            <td class="MainRightHeader" colspan="6">Export Data to Symitar</td>
		            </tr>
                </table>

                <table class="InsetSpacer">
		            <tr>
						<td class="FieldLabel">Borrower Member ID</td>
						<td class="Center">
							<input id="aBCoreSystemId" runat="server" type="text" size="11" maxlength="15" NoHighlight="true" />
							<img src="~/images/require_icon.gif" runat="server" />
						</td>
						<td colspan="2">&nbsp;</td>
						<td class="FieldLabel">Co-Borrower Member ID</td>
						<td class="Center">
							<input id="aCCoreSystemId" runat="server" type="text" size="11" maxlength="15" NoHighlight="true" />
						</td>
					</tr>
					<tr>
						<td colspan="6"><p id="ErrorMessage" class="Center"></p></td>
					</tr>
                </table>

                <fieldset class="ExportFieldset">
                    <legend>Booking Info</legend>
                    <table class="FullWidth">
                        <tr>
                            <td colspan="3">&nbsp;</td>
						    <td colspan="3">
							    <input id="LoadSymitarDataButton" type="button" value="Load Data from Symitar" ng-click="loadSymitarData();" NoHighlight="true" />
						    </td>
					    </tr>
                        <tr>
                            <td class="FieldLabel">
                                <span class="BookingInfoSpacer">Symitar Loan ID</span>
                                <input id="sCoreLoanId" runat="server" type="text" size="6" maxlength="4" NoHighlight="true" />
                                <input id="UseNextAvailableIdButton" type="button" value="Use Next Available" ng-click="calculateNextLoanId()" 
                                    NoHighlight="true" class="Indent" />
                            </td>
                            <td class="BookingInfoSpacer">&nbsp;</td>
                            <td class="FieldLabel">Existing Loan IDs in Symitar</td>
                            <td>
                                <select id="ExistingLoanIds">
                                    <option id="NoLoansFoundOption" value="0">&nbsp;</option>
                                    <option ng-repeat="loan in ExistingSymitarLoans" value="{{loan.Id}}">{{loan.Id}} - {{loan.Description}}</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </fieldset>

                <br />

                <table class="ExtraLeftPadding">
                    <tr>
                        <td>
                            <input id="LoadExportDataButton" type="button" value="Load Export Data" ng-click="loadExportData();" NoHighlight="true" class="Left" />
                        </td>
                        <td><label class="Spacer">&nbsp;</label></td>
                        <td>
                            <input id="SubmitButton" type="button" value="Submit to Symitar" ng-click="submit();" NoHighlight="true" />
                            <label ng-show="HasExportError">
                                <img src="../../images/fail.png" class="ExportErrorImage" data-toggle="tooltip" data-placement="right" 
                                    title="Unable to submit: error in export data." />
                            </label>
                            <label>
                                <img id="SubmitButtonDirtyErrorImg" src="../../images/fail.png" class="ExportErrorImage" data-placement="right" 
                                    title="Data may have changed. Please re-load the export data." />
                            </label>
                        </td>
                    </tr>
                </table>

                <br />

                <fieldset class="ExportFieldset Clear">
                    <legend>Export Data</legend>
                    <br />
                    <div class="BottomMargin" ng-repeat="table in ExportDataSections" ng-include="'ExportDataSection'"></div>
                </fieldset>
            </form>
        </div>
    </div>
</body>
</html>
