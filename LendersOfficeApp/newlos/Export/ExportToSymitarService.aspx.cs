﻿// <copyright file="ExportToSymitarService.aspx.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Michael Leinweaver
//  Date:   2016-5-26
// </summary>
#region Generated Code
namespace LendersOfficeApp.newlos.Export
#endregion
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.Symitar;
    using LendersOffice.Integration.Symitar.Export;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a service for the "Export to Symitar" page.
    /// </summary>
    public partial class ExportToSymitarService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Initializes the service by adding a new <see cref="ExportToSymitarServiceItem"/>.
        /// </summary>
        protected override void Initialize()
        {
            AddBackgroundItem(string.Empty, new ExportToSymitarServiceItem());
        }
    }

    /// <summary>
    /// Provides a service item for the "Export to Symitar" page.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed.")]
    public class ExportToSymitarServiceItem : AbstractBackgroundServiceItem
    {
        /// <summary>
        /// Constructs a new <see cref="CPageData"/> object using the
        /// specific <paramref name="loanId"/>.
        /// </summary>
        /// <param name="loanId">
        /// The <see cref="Guid"/> for the loan.
        /// </param>
        /// <returns>
        /// A <see cref="CPageData"/> instance with the specified
        /// loan id.
        /// </returns>
        protected override CPageData ConstructPageDataClass(Guid loanId)
        {
            return CPageData.CreateUsingSmartDependency(loanId, typeof(ExportToSymitarServiceItem));
        }

        /// <summary>
        /// Binds data received by the service to the loan.
        /// </summary>
        /// <param name="dataLoan">
        /// The <see cref="CPageData"/> to bind to.
        /// </param>
        /// <param name="dataApp">
        /// The <see cref="CAppData"/> to bind to.
        /// </param>
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.aBCoreSystemId = this.GetString("aBCoreSystemId");
            dataApp.aCCoreSystemId = this.GetString("aCCoreSystemId");
            dataLoan.sCoreLoanId = this.GetString("sCoreLoanId");
        }

        /// <summary>
        /// Loads data for the export page.
        /// </summary>
        /// <param name="dataLoan">
        /// The <see cref="CPageData"/> to load from.
        /// </param>
        /// <param name="dataApp">
        /// The <see cref="CAppData"/> to load from.
        /// </param>
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            this.SetResult("aBCoreSystemId", dataApp.aBCoreSystemId);
            this.SetResult("aCCoreSystemId", dataApp.aCCoreSystemId);
            this.SetResult("sCoreLoanId", dataLoan.sCoreLoanId);
        }

        /// <summary>
        /// Processes the method with the specified <paramref name="methodName"/>.
        /// </summary>
        /// <param name="methodName">
        /// The method to process.
        /// </param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "LoadDataFromSymitar":
                    LoadDataFromSymitar();
                    break;
                case "LoadExportData":
                    LoadExportData();
                    break;
                case "SubmitToSymitar":
                    SubmitToSymitar();
                    break;
                default:
                    throw new GenericUserErrorMessageException(
                        "Unhandled export from Symitar service method: " + methodName);
            }
        }

        /// <summary>
        /// Loads Symitar loan information to display on the page.
        /// </summary>
        private void LoadDataFromSymitar()
        {
            var borrowerCoreSystemId = this.GetString("aBCoreSystemId");
            var coborrowerCoreSystemId = this.GetString("aCCoreSystemId");

            if (string.IsNullOrWhiteSpace(borrowerCoreSystemId))
            {
                throw new CBaseException(
                    "Invalid Member ID. Please verify the borrower’s member ID.",
                    "Invalid Symitar borrower member ID: " + borrowerCoreSystemId);
            }

            var currentPrincipal = PrincipalFactory.CurrentPrincipal;
            var lenderConfig = currentPrincipal.BrokerDB.GetEnabledSymitarLenderConfig(RetrieveLoanType());
            var existingLoanIds = SymitarManager.GetLoans(lenderConfig, borrowerCoreSystemId);

            this.SetResult("Status", "OK");
            this.SetResult("ExistingLoanIdJson", SerializationHelper.JsonNetSerialize(existingLoanIds));
        }

        /// <summary>
        /// Loads the loan data for the export to Symitar.
        /// </summary>
        private void LoadExportData()
        {
            var borrowerCoreSystemId = this.GetString("aBCoreSystemId");
            var coborrowerCoreSystemId = this.GetString("aCCoreSystemId");

            if (string.IsNullOrWhiteSpace(borrowerCoreSystemId))
            {
                throw new CBaseException(
                    "Invalid Member ID. Please verify the borrower’s member ID.",
                    "Invalid Symitar borrower member ID: " + borrowerCoreSystemId);
            }

            var currentPrincipal = PrincipalFactory.CurrentPrincipal;
            var lenderConfig = currentPrincipal.BrokerDB.GetEnabledSymitarLenderConfig(RetrieveLoanType());

            var export = SymitarManager.LoadExportToSymitar(
                lenderConfig,
                this.sLId,
                this.aAppId,
                currentPrincipal);
            string cacheKey = null;
            var exportDataSections = new List<SymitarExportDataSection>(1);
            if (!export.HasError)
            {
                cacheKey = export.SaveCopy(currentPrincipal);
                exportDataSections.Add(SymitarManager.MapToDisplayLogic(export.TableToExport, new LosConvert()));
            }

            this.SetResult("Status", "OK");
            this.SetResult("ExportDataSectionsJson", SerializationHelper.JsonNetSerialize(exportDataSections));
            this.SetResult("ExportCacheKey", cacheKey);
        }

        /// <summary>
        /// Submits loan data to Symitar.
        /// </summary>
        private void SubmitToSymitar()
        {
            var borrowerCoreSystemId = this.GetString("aBCoreSystemId");
            var coborrowerCoreSystemId = this.GetString("aCCoreSystemId");
            var symitarLoanId = this.GetString("sCoreLoanId");
            var exportCacheKey = this.GetString("ExportCacheKey");
            var isNewSymitarLoan = this.GetBool("isNewSymitarLoan");

            if (string.IsNullOrWhiteSpace(borrowerCoreSystemId))
            {
                throw new CBaseException(
                    "Invalid Member ID. Please verify the borrower’s member ID.",
                    "Invalid Symitar borrower member ID: " + borrowerCoreSystemId);
            }
            else if (string.IsNullOrWhiteSpace(symitarLoanId))
            {
                throw new CBaseException(
                    "You must specify a valid loan id.",
                    "An invalid Symitar loan id was specific for export: " + symitarLoanId);
            }

            var currentPrincipal = PrincipalFactory.CurrentPrincipal;
            var export = SymitarDataExport.ConstructFromSavedCopy(currentPrincipal, exportCacheKey);

            var lenderConfig = currentPrincipal.BrokerDB.GetEnabledSymitarLenderConfig(RetrieveLoanType());
            bool success = SymitarManager.SaveToSymitar(currentPrincipal, export, lenderConfig, borrowerCoreSystemId, symitarLoanId, isNewSymitarLoan);

            this.SetResult("Status", success ? "OK" : "ERROR");
        }

        /// <summary>
        /// Retrieves the file type of the current loan via the loan object.
        /// </summary>
        /// <returns>The loan file type.</returns>
        private E_sLoanFileT RetrieveLoanType()
        {
            var loan = new CFullAccessPageData(this.sLId, new[] { nameof(CPageData.sLoanFileT) });
            loan.InitLoad();
            return loan.sLoanFileT;
        }
    }
}
