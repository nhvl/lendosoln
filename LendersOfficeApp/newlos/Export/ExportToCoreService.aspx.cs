﻿#region Auto Generated code
namespace LendersOfficeApp.newlos.Export
#endregion
{
    using LendersOffice.Common;
    using LendersOffice.Integration.Kiva;

    /// <summary>
    /// Provides the web service functionality for <see cref="ExportToCore"/>.
    /// </summary>
    public partial class ExportToCoreService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Processes the specified method.
        /// </summary>
        /// <param name="methodName">The name of the method being called.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "Export":
                    Export();
                    break;
                default:
                    throw DataAccess.CBaseException.GenericException("Unexpected methodName: " + methodName);
            }
        }

        /// <summary>
        /// Handles the export method.
        /// </summary>
        private void Export()
        {
            var exporter = new KivaExportProcessor();
            KivaExportResultData result = exporter.ExportLoan(LendersOffice.Security.PrincipalFactory.CurrentPrincipal, this.GetGuid("LoanId"));
            this.SetResult("Successful", result.Successful);
            this.SetResult("sCoreLoanId", result.AccountNumber);
            this.SetResult("UserErrorMessages", SerializationHelper.JsonNetSerialize(result.UserErrorMessages));
        }
    }
}