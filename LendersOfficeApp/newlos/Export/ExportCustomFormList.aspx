﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExportCustomFormList.aspx.cs" Inherits="LendersOfficeApp.newlos.Export.ExportCustomFormList" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
    <div>
    <table cellpadding="0" cellspacing="0" width="100%" border="0">
        <tr><td class="MainRightHeader">Export Custom Form</td></tr>
        <tr>
            <td>
                        <ul>
                <asp:Repeater runat="server" ID="m_exportFormats">
                    <ItemTemplate>
                        <li>
                            <a href="ExportCustomFormList.aspx?Name=<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Name").ToString()) %>&cmd=download&loanid=<%= AspxTools.HtmlString(LoanID) %>" target="_parent">
                                <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Description").ToString()) %>
                            </a>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:PlaceHolder runat="server" ID="m_noReportFormats" Visible="false">
                    <li>
                        No Available Formats
                    </li>
                </asp:PlaceHolder>
            </ul>
            </td>
        </tr>
    </table>
    
    </div>
    </form>
</body>
</html>
