﻿#region Auto Generated code
namespace LendersOfficeApp.newlos.Export
#endregion
{
    using System;
    using LendersOffice.ConfigSystem.Operations;

    /// <summary>
    /// Handles exports to the core banking system, specifically for the integration with <c>KIVA</c> group's export.
    /// </summary>
    public partial class ExportToCore : BaseLoanPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExportToCore"/> class.
        /// </summary>
        public ExportToCore()
        {
            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;
        }

        /// <summary>
        /// Handles the <c>Load</c> event of the Page control.
        /// </summary>
        /// <param name="sender">The control/object that raised the event.</param>
        /// <param name="e">The event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var loan = DataAccess.CPageData.CreateUsingSmartDependency(this.LoanID, typeof(ExportToCore));
            loan.InitLoad();
            this.sCoreLoanId.Value = loan.sCoreLoanId;

            if (!this.UserHasWorkflowPrivilege(WorkflowOperations.ExportToCoreSystem))
            {
                this.RegisterJsGlobalVariables("ExportToCoreSystemBlockMessage", this.GetReasonUserCannotPerformPrivilege(WorkflowOperations.ExportToCoreSystem));
            }

            this.RegisterJsScript("SimplePopups.js");
            this.RegisterJsScript("jquery.tmpl.js");
        }

        /// <summary>
        /// Gets the extra workflow operations to check.
        /// </summary>
        /// <returns>The extra workflow operations to check.</returns>
        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new[] { WorkflowOperations.ExportToCoreSystem };
        }

        /// <summary>
        /// Gets the reasons for privileges that aren't allowed.
        /// </summary>
        /// <returns>The reasons for privileges that aren't allowed.</returns>
        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return this.GetExtraOpsToCheck();
        }
    }
}