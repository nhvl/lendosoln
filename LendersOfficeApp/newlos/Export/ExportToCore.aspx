﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExportToCore.aspx.cs" Inherits="LendersOfficeApp.newlos.Export.ExportToCore" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Export Data to Core</title>
    <style>
        body {
            background-color: gainsboro;
        }
        .PageRow {
            padding: 10px;
        }
        .PageRow > * {
            vertical-align: middle;
        }
        #ErrorMessage {
            color: red;
            font-weight: bold;
        }
    </style>
    <script>
        function _init() {
            var exportBtn = document.getElementById('ExportBtn');
            var exportBtnWarning = document.getElementById('ExportBtnWarning');
            var errorMessageDiv = document.getElementById('ErrorMessage');
            var sCoreLoanIdInput = document.getElementById('sCoreLoanId');
            if (ML['ExportToCoreSystemBlockMessage'] !== undefined) {
                exportBtn.disabled = true;
                exportBtnWarning.title = ML['ExportToCoreSystemBlockMessage'];
            } else {
                exportBtnWarning.style.display = 'none';
            }

            addEventHandler(exportBtn, 'click', function () {
                var args = {
                    'LoanId': ML.sLId
                };
                gService.loanedit.callAsyncSimple('Export', args, function (result) {
                    var headerText = "Export Results";
                    if (result.error) {
                        var errorPopup = SimplePopups.CreateErrorPopup(result.UserMessage, null, headerText);
                        showPopup(errorPopup);
                        return;
                    } else if (result.value.Successful !== 'True') {
                        var errorPopup = SimplePopups.CreateErrorPopup("Unable to export to Core system.", JSON.parse(result.value.UserErrorMessages), headerText);
                        showPopup(errorPopup);
                        return;
                    } else if (result.value.sCoreLoanId) {
                        sCoreLoanIdInput.value = result.value.sCoreLoanId;
                    }
                });
            });

            function showPopup(popup) {
                LQBPopup.ShowElement($('<div>').append(popup), {
                    width: 350,
                    height: 200,
                    hideCloseButton: true,
                    elementClasses: 'FullWidthHeight'
                });
            }
        }
    </script>
</head>
<body>
    <div class="MainRightHeader">
        Export Data to Core
    </div>
    <form id="form1" runat="server">
        <div class="PageRow">
            <label for="sCoreLoanId" class="FieldLabel">Core Account Number</label>
            <input type="text" id="sCoreLoanId" name="sCoreLoanId" runat="server" disabled="disabled" />
        </div>
        <div class="PageRow">
            <input id="ExportBtn" type="button" value="Export" />
            <img id="ExportBtnWarning" src="<%= AspxTools.SafeUrl(this.VirtualRoot + "/images/warning25x25.png") %>" alt="Warning" />
        </div>
    </form>
</body>
</html>