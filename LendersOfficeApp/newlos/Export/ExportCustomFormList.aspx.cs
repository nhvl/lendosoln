﻿using System;
using System.Linq;
using LendersOffice.XsltExportReport;
using LendersOffice.Common;
using System.Text;
using DataAccess;
using LendersOffice.HttpModule;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Export
{
    public partial class ExportCustomFormList : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string cmd = Request.QueryString["cmd"];

            if (cmd == "download")
            {
                DownloadReport();
            }
            else
            {
                var reports = XsltMap.ListActiveForSingleLoan(Broker).OrderBy(o => o.Description);
                m_exportFormats.DataSource = reports;
                m_exportFormats.DataBind();
                m_noReportFormats.Visible = (!reports.Any());
            }
        }

        private void DownloadReport()
        {
            string reportName = RequestHelper.GetSafeQueryString("name");

            XsltExportRequest request = new XsltExportRequest(this.BrokerUser.UserId, new Guid[] { this.LoanID }, reportName, null, null);

            XsltExportWorker worker = new XsltExportWorker(request, false, PrincipalFactory.CurrentPrincipal);

            XsltExportResult result = worker.Execute();

            if (result.HasError)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var o in result.ErrorList)
                {
                    sb.AppendLine(o);
                }

                // 6/3/2014 dd - TODO: Handle error better.
                throw CBaseException.GenericException(sb.ToString());
            }
            else
            {
                var item = PerformanceMonitorItem.Current;
                if (item != null)
                {
                    item.IsEnabledDebugOutput = false;
                }

                XsltMapItem mapItem = XsltMap.Get(reportName);
                Response.Clear();
                if (string.IsNullOrEmpty(mapItem.ExportContentType) == false)
                {
                    Response.ContentType = mapItem.ExportContentType;
                }

                Response.AddHeader("Content-Disposition", $"attachment; filename=\"{mapItem.OutputFileName}\"");
                Response.WriteFile(result.OutputFileLocation);
                Response.End();
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.PageTitle = "Export To Custom Form";
            this.PageID = "ExportCustomForm";
        }
    }
}
