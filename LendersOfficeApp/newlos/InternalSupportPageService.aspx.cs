﻿using System;
using System.Collections.Generic;
using System.Threading;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Common.SerializationTypes;
using LendersOffice.Constants;
using LendersOffice.Migration;
using LendersOffice.Security;
using LendersOffice.ObjLib.StatusEvents;

namespace LendersOfficeApp.newlos
{
    public class InternalSupportPageServiceItem : AbstractBackgroundServiceItem
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "Migrate":
                    Migrate();
                    break;
                case "CreateClosingDisclosureArchive":
                    CreateClosingDisclosureArchive();
                    break;
                case "FixLEAndCDDates":
                    FixLEAndCDDates();
                    break;
                case "RollbackLoanVersion":
                    this.RollbackLoanVersion();
                    break;
                case "RunAutoPriceEligibilityProcess":
                    this.RunAutoPriceEligibilityProcess();
                    break;
                case "ResetTouchEvents":
                    this.ResetTouchEvents();
                    break;
                case "MigrateToLatestUladDataLayer":
                    this.MigrateToLatestUladDataLayer();
                    break;
            }
        }

        protected override DataAccess.CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(InternalSupportPageServiceItem));
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sIsAllowExcludeToleranceCure", dataLoan.sIsAllowExcludeToleranceCure);
            SetResult("sClosingCostFeeVersionT", dataLoan.sClosingCostFeeVersionT);
            SetResult("sIsHousingExpenseMigrated", dataLoan.sIsHousingExpenseMigrated);
            SetResult("sIsManuallySetThirdPartyAffiliateProps", dataLoan.sIsManuallySetThirdPartyAffiliateProps);
            SetResult("sLoads1003LineLFromAdjustments", dataLoan.sLoads1003LineLFromAdjustments);
            SetResult("sLoanFileT", dataLoan.sLoanFileT);

            if (dataLoan.sHasLastDisclosedLoanEstimateArchive)
            {
                SetResult("sTRIDLoanEstimateCashToCloseCalcMethodT", dataLoan.LastDisclosedLoanEstimateArchive.GetCountValue("sTRIDLoanEstimateCashToCloseCalcMethodT").ToString());
            }

            SetResult("sUse2016ComboAndProrationUpdatesTo1003Details", dataLoan.sUse2016ComboAndProrationUpdatesTo1003Details);
            SetResult("sBlockComplianceEaseIdFromRequest", dataLoan.sBlockComplianceEaseIdFromRequest);

            SetResult("sRespa6D", dataLoan.sRespa6D_rep);
            SetResult("sRespa6DLckd", dataLoan.sRespa6DLckd);

            var preparer = dataLoan.GetPreparerOfForm(
                E_PreparerFormT.App1003Interviewer,
                E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("App1003PrepareDate", preparer.PrepareDate_rep);

            SetResult("sCalculateInitAprAndLastDiscApr", dataLoan.sCalculateInitAprAndLastDiscApr);

            SetResult("sLegalEntityIdentifier", dataLoan.sLegalEntityIdentifier);

            SetResult("sBorrowerApplicationCollectionT", dataLoan.sBorrowerApplicationCollectionT);
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sIsAllowExcludeToleranceCure = GetBool("sIsAllowExcludeToleranceCure");
            dataLoan.sClosingCostFeeVersionT = GetEnum<E_sClosingCostFeeVersionT>("sClosingCostFeeVersionT");
            dataLoan.sIsHousingExpenseMigrated = GetBool("sIsHousingExpenseMigrated");
            dataLoan.sIsManuallySetThirdPartyAffiliateProps = GetBool("sIsManuallySetThirdPartyAffiliateProps");

            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                dataLoan.sLoads1003LineLFromAdjustments = GetBool("sLoads1003LineLFromAdjustments");
            }

            if (dataLoan.sHasLastDisclosedLoanEstimateArchive)
            {
                dataLoan.LastDisclosedLoanEstimateArchive.OverrideValue("sTRIDLoanEstimateCashToCloseCalcMethodT", GetString("sTRIDLoanEstimateCashToCloseCalcMethodT"));
                dataLoan.FlushClosingCostArchives();
            }

            dataLoan.sUse2016ComboAndProrationUpdatesTo1003Details = GetBool("sUse2016ComboAndProrationUpdatesTo1003Details");
            dataLoan.sBlockComplianceEaseIdFromRequest = GetBool("sBlockComplianceEaseIdFromRequest");

            dataLoan.sRespa6D_rep = GetString("sRespa6D");
            dataLoan.sRespa6DLckd = GetBool("sRespa6DLckd");


            var app1003PrepareDate = this.GetString("App1003PrepareDate");
            // If we are going to be setting the value to blank, we only want to do that
            // for an existing preparer. If we're setting the value to a non-blank value
            // we will create a new one if the file doesn't have one.
            var returnOption = string.IsNullOrWhiteSpace(app1003PrepareDate)
                ? E_ReturnOptionIfNotExist.ReturnEmptyObject
                : E_ReturnOptionIfNotExist.CreateNew;

            var preparer = dataLoan.GetPreparerOfForm(
                E_PreparerFormT.App1003Interviewer,
                returnOption);

            if (preparer.IsValid)
            {
                preparer.PrepareDate_rep = app1003PrepareDate;
                preparer.Update();
            }

            dataLoan.sCalculateInitAprAndLastDiscApr = this.GetBool("sCalculateInitAprAndLastDiscApr");

            dataLoan.sLegalEntityIdentifier = GetString("sLegalEntityIdentifier");
        }

        private void ResetTouchEvents()
        {
            Guid sLId = GetGuid("loanid");
            StatusEvent.RemoveAllStatusEvents(sLId, PrincipalFactory.CurrentPrincipal.BrokerId);

            var dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(InternalSupportPageServiceItem));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sIsStatusEventMigrated = false;
            dataLoan.sStatusEventMigrationVersion = StatusEventMigrationVersion.NotMigrated;
            dataLoan.Save();

        }

        private void Migrate()
        {
            Guid sLId = GetGuid("loanid");
            var version = (E_sClosingCostFeeVersionT) GetInt("version");

            AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;

            ClosingCostFeeMigration.Migrate(principal, sLId, version);
            
            SetResult("sIsHousingExpenseMigrated", true);
            SetResult("sClosingCostFeeVersionT", version);
        }

        private void MigrateToLatestUladDataLayer()
        {
            var loanId = this.GetGuid("loanid");
            var loan = CPageData.CreateUsingSmartDependency(loanId, typeof(InternalSupportPageServiceItem));
            loan.InitSave();

            loan.MigrateToLatestUladCollectionVersion();

            loan.Save();

            this.SetResult(nameof(CPageData.sBorrowerApplicationCollectionT), loan.sBorrowerApplicationCollectionT.ToString());
        }

        private void FixLEAndCDDates()
        {
            Guid sLId = GetGuid("loanid");
            LEAndCDDatesUniqueIdMigration.AddUniqueIds(sLId);
        }

        private void CreateClosingDisclosureArchive()
        {
            // This is here so closing disclosure archive can be tested without generating docs.
            
            Guid sLId = GetGuid("loanid");

            string msg;
            // BrokerUser.HasPermission(Permission.AllowManuallyArchivingGFE)
            CPageData dataLoan = new CPageData(sLId, new List<string> { "sfArchiveClosingCosts", "sDisclosureRegulationT", "sClosingCostFeeVersionT" });
            dataLoan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);

            if (dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                dataLoan.ArchiveClosingCosts(
                    ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure,
                    E_GFEArchivedReasonT.ManuallyArchived,
                    ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed);
                dataLoan.Save();

                msg = "Closing Disclosure Archive Created.";
            }
            else
            {
                msg = "ERROR: Must be in new fee type mode and TRID status to archive Closing Disclosure.";
            }

            SetResult("ResultMsg", msg);
        }

        private void RunAutoPriceEligibilityProcess()
        {
            // OPM 247399. These are so this can be tested without waiting
            // for the overnight process.  Should not be a normal part of flow.

            Guid loanId = GetGuid("loanid");
            var action = GetString("action");
            bool loanSkipped = false;

            switch (action)
            {
                case "runnow":
                    AutoPriceEligibility.RunPriceAndEligibilityCheck(loanId, out loanSkipped);
                    break;
                case "enqueueloan":
                    AutoPriceEligibility.EnqueueLoan(loanId);
                    break;
                case "enqueuebroker":
                    AutoPriceEligibility.EnqueueApplicableLoansForBroker(PrincipalFactory.CurrentPrincipal.BrokerId);
                    break;

                default:
                    throw new GenericUserErrorMessageException("Unknown action: " + action);
            }


            SetResult("ResultMsg", "Done." + ( loanSkipped ? " Loan was skipped due to being unchanged." : string.Empty));
        }

        private void RollbackLoanVersion()
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            if (!principal.HasPermission(Permission.CanModifyLoanPrograms))
            {
                throw new AccessDenied();
            }

            Guid loanId = this.GetGuid("loanid");
            int targetVersionAsInt = GetInt("rollbackTarget");

            if (!Enum.IsDefined(typeof(LoanVersionT), targetVersionAsInt))
            {
                SetResult("IsSuccessful", false);
                SetResult("ErrorMessage", "Invalid version. Unable to rollback.");
                return;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(InternalSupportPageServiceItem));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (targetVersionAsInt >= (int)dataLoan.sLoanVersionT)
            {
                SetResult("IsSuccessful", false);
                SetResult("ErrorMessage", "Invalid version. Version must be below the current loan version.");
            }
            else
            {
                dataLoan.sLoanVersionT = (LoanVersionT)targetVersionAsInt;
                dataLoan.Save();

                SetResult("IsSuccessful", true);
            }
        }

    }

    public partial class InternalSupportPageService : BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new InternalSupportPageServiceItem());
        }
    }
}
