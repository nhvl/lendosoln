<%@ Register TagPrefix="UC" TagName="CFM" Src="../newlos/Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="EmploymentRecord.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.EmploymentRecord" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Borrower Employments</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
        <style>
            .SelfEmployedCheckboxContainer .FieldLabel {
                display: inline-block;
                max-width: 300px;           
            }

            .SelfEmployedDataContainer {
                white-space: nowrap;
            }

            .SelfEmployedDataContainer div:not(:last-child) {
                padding-bottom: 3px;
            }

            .SelfEmployedDataContainer div + div {
                padding-top: 3px;
            }

            .self-ownership-share {
                margin-left: 10px;
            }
        </style>
  </HEAD>
	<body leftmargin=0 bgcolor="gainsboro" MS_POSITIONING="FlowLayout">
	<script type="text/javascript">
    var oRolodex = null;  

        function _init() {
            parent.parent_initScreen(1);

            if (null == oRolodex)
                oRolodex = new cRolodex();


            var $selfEmployedCheckbox = $('.SelfEmployedCheckboxContainer input');
            $selfEmployedCheckbox.change(determineShowSelfEmploymentSection);
            determineShowSelfEmploymentSection();
        }

    function determineShowSelfEmploymentSection(event) {
        var $selfOwnershipShare = $('.self-ownership-share');
        var isSelfEmployed = $('.SelfEmployedCheckboxContainer input').is(':checked');

        $selfOwnershipShare.prop('disabled', !isSelfEmployed);
        if (!isSelfEmployed) {
            $selfOwnershipShare.val('0');
            $('.is-special-borrower-employer-relationship').prop('checked', false);
        }
    }

    function getSpecialValues(args) {
      var obj = parent.parent_getSpecialValues();
      for (p in obj) args[p] = obj[p];
    }
    function setSpecialValues(args) {
      parent.parent_updatePrimaryRecord(args);
    }
  
    function refreshUI(isFocus) {
      if (document.getElementById("MainEdit").style.display != "none" && isFocus)
            document.getElementById("<%= AspxTools.ClientId(EmplrNm) %>").focus();    
    }
    
    function updateDirtyBit_callback() { parent.parent_disableSaveBtn(false); }
    
    function updateListIFrame(iRowIndex) {
        var list_updateRow = retrieveFrameProperty(parent, "ListIFrame", "list_updateRow");
        if (list_updateRow) {
            list_updateRow(iRowIndex, document.getElementById("RecordID").value, document.getElementById("<%= AspxTools.ClientId(EmplrNm) %>").value, document.getElementById("<%= AspxTools.ClientId(MonI) %>").value);
        }
    }
    
    function f_getPrimaryBusinessPhone(args) {
      return gService.singleedit.call("GetPrimaryBusinessPhone", args);
    }    
    
    function doAfterDateFormat(e)
    {   
        try
        {
            var startDateStr = document.getElementById("EmplmtStartD");
            var endDateStr = document.getElementById("EmplmtEndD");
            
           // if(e != endDateStr)
           //     return;
            
            var partsS = startDateStr.value.split('/');
            var mmS = eval(partsS[0]);
            var ddS = eval(partsS[1]);
            var yyyyS = eval(partsS[2]);
            
            var partsE = endDateStr.value.split('/');
            var mmE = eval(partsE[0]);
            var ddE = eval(partsE[1]);
            var yyyyE = eval(partsE[2]);
            
            if((yyyyE < yyyyS) || (yyyyE == yyyyS && mmE < mmS) || (yyyyE == yyyyS && mmS == mmE && ddE < ddS))
            {
                alert("Warning : Employment start date is more recent than the employment end date.  Please try again.");
                e.value = "";
                
            }
                
        }
        catch(e)
        {
        }
    }
    function checkboxIsCurrentClicked() {
        var args = getAllFormValues();
        var results = gService.loanedit.call("IsCurrentClicked", args);

        if (!results.error) {
            populateForm(results.value, null);
            postPopulateForm(results);
        }
    }
    function postPopulateForm(results) {
        document.getElementById("EmplmtEndD").disabled = document.getElementById("IsCurrent").checked;
        determineShowSelfEmploymentSection();
    }
    </script>
		<form id="EmploymentRecord" method="post" runat="server">
		  <div id="MainEdit">
			<table class="FormTable" id="Table2" cellSpacing="0" cellPadding="0" border="0">
				<tr>
					<td>
						<table class="insetborder" cellSpacing="0" cellPadding="0" width="99%" border="0">
							<tr>
								<td>
									<table cellSpacing="0" cellPadding="0">
										<tr>
											<td>&nbsp;</td>
											<td class=FieldLabel colspan=3><asp:checkbox CssClass="SelfEmployedCheckboxContainer" id="IsSelfEmplmt" runat="server" Text="Self-employed" tabIndex=20></asp:checkbox></td>
										</tr>
										<tr>
										    <td></td>
										    <td class=FieldLabel colspan=3>
					                            <asp:checkbox id="IsCurrent" runat="server" onclick="checkboxIsCurrentClicked();" Text="Current employment" tabIndex="20"></asp:checkbox>
										    </td>
										</tr>
                                        <tr class="SelfEmployedDataContainer">
                                            <td></td>
                                            <td colspan="4">
                                                <div>
                                                    <label class="FieldLabel" for="IsSpecialBorrowerEmployerRelationship">Employed by a family member, property seller, real estate agent, or other party to the transaction?</label>
                                                    <input runat="server" type="checkbox" class="is-special-borrower-employer-relationship" id="IsSpecialBorrowerEmployerRelationship" />
                                                </div>
                                                <div>
                                                    <span class="FieldLabel">Ownership share of</span>
                                                    <asp:DropDownList runat="server" CssClass="self-ownership-share" ID="SelfOwnershipShareT"></asp:DropDownList>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
											<td></td>
											<td>
												<uc:CFM id="CFM" runat="server" CompanyNameField="EmplrNm" StreetAddressField="EmplrAddr" CityField="EmplrCity" StateField="EmplrState" ZipField="EmplrZip" CompanyPhoneField="EmplrBusPhone" AgentTitleField="JobTitle" CompanyFaxField="EmplrFax"></uc:CFM>
											</td>
											<td colspan="2">&nbsp;</td>
										</tr>
										<tr>
											<td class="FieldLabel">Employer Name</td>
											<td class="FieldLabel"><asp:textbox id="EmplrNm" runat="server" Width="220px" tabIndex=20></asp:textbox></td>
											<td class="FieldLabel" colspan=2 style="PADDING-LEFT:10px; white-space: nowrap;">
											Dates:&nbsp; from 
											<ml:datetextbox id="EmplmtStartD" runat="server" CssClass="mask" IsDisplayCalendarHelper="false" preset="date" width="75" tabIndex=20></ml:datetextbox>
												&nbsp;to
											<ml:datetextbox id="EmplmtEndD" runat="server" CssClass="mask" IsDisplayCalendarHelper="false" preset="date" width="75" tabIndex=20></ml:datetextbox>
											</td>
											
											
										</tr>
										<tr>
											<td class="FieldLabel">Employer Address</td>
											<td><asp:textbox id="EmplrAddr" runat="server" Width="256px" tabIndex=20></asp:textbox></td>
							                <td class=FieldLabel colspan=2 style="PADDING-LEFT:10px">Monthly Income Desc:&nbsp; <asp:textbox id="MonI" style="TEXT-ALIGN: right" runat="server" width="113" type="text" tabIndex=20></asp:textbox></td>
										</tr>
										<tr>
											<td class="FieldLabel">Employer City</td>
											<td><asp:textbox id="EmplrCity" runat="server" Width="173px" tabIndex=20></asp:textbox><ml:statedropdownlist id="EmplrState" runat="server" tabIndex=20></ml:statedropdownlist><ml:zipcodetextbox id="EmplrZip" runat="server" CssClass="mask" preset="zipcode" width="50" tabIndex=20></ml:zipcodetextbox></td>
											<td class=FieldLabel style="PADDING-LEFT:10px">Phone</td>
											<td><ml:phonetextbox id=EmplrBusPhone runat="server" width="120" preset="phone" CssClass="mask" tabIndex=25></ml:phonetextbox></td>
                						</tr>
										<TR>
											<TD class="FieldLabel" noWrap>Position/Title</TD>
											<TD><asp:textbox id="JobTitle" runat="server" Width="223px" tabIndex=20></asp:textbox></TD>
											<td class=FieldLabel style="PADDING-LEFT:10px">Fax</td>
											<td><ml:phonetextbox id=EmplrFax runat="server" width="120" preset="phone" CssClass="mask" tabIndex=25></ml:phonetextbox></td>
                                            
										</TR>
                                        <tr>
                                            <td title="Identifies an employee on The Work Number database" class="FieldLabel" noWrap>Employee ID (VOE)</td>
                                            <td title="Identifies an employee on The Work Number database"><asp:TextBox ID="EmployeeIdVoe" runat="server" preset="custom" mask="###########"></asp:TextBox></td>
                                            <td title="Identifies an employer on The Work Number database" class="FieldLabel">Employer Code (VOE)</td>
                                            <td title="Identifies an employer on The Work Number database"><asp:TextBox ID="EmployerCodeVoe" runat="server" preset="custom" mask="##########"></asp:TextBox></td>
                                        </tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="FieldLabel">
						<table class="InsetBorder" id="Table1" cellSpacing="0" cellPadding="0" width="99%" border="0">
							<tr>
								<td class="FieldLabel">Verif Sent</td>
								<td noWrap><ml:datetextbox id="VerifSentD" runat="server" CssClass="mask" preset="date" width="75" tabIndex=30></ml:datetextbox></td>
								<td class="FieldLabel">Re-order</td>
								<td noWrap><ml:datetextbox id="VerifReorderedD" runat="server" CssClass="mask" preset="date" width="75" tabIndex=30></ml:datetextbox></td>
								<td class="FieldLabel">Received</td>
								<td noWrap><ml:datetextbox id="VerifRecvD" runat="server" CssClass="mask" preset="date" width="75" tabIndex=30></ml:datetextbox></td>
								<td class="FieldLabel">Expected</td>
								<td noWrap><ml:datetextbox id="VerifExpD" runat="server" CssClass="mask" preset="date" width="75" tabIndex=30></ml:datetextbox></td>
							</tr>
						</table>
					</td>
				</tr>
			</table></TD></TR></TABLE>
      </div>			
			</form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>

	</body>
</HTML>
