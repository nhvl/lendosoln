﻿using System;
using DataAccess;
using LendersOffice.Conversions.GenericMismoClosing26;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos
{
    public partial class GenericMismoClosing26 : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(GenericMismoClosing26));
            dataLoan.InitLoad();
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.BufferOutput = true;
            Response.AddHeader("Content-Disposition", $"attachment; filename=\"{dataLoan.sLNm}.xml\"");
            string xml = null;
            try
            {
                xml = GenericMismoClosing26Exporter.Export(LoanID, PrincipalFactory.CurrentPrincipal);
            }
            catch (LendersOffice.Conversions.FeeDiscrepancyException exc)
            {
                xml = exc.UserMessage;
            }
            Response.Write(xml);
            Response.Flush();
            Response.End();
        }
    }
}
