﻿namespace LendersOfficeApp.Integration.SeamlessDU
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.DU;
    using LendersOffice.ObjLib.Conversions.Templates;
    using LendersOffice.ObjLib.DU.Seamless;
    using LendersOffice.ObjLib.DU.Seamless.Login;
    using LendersOffice.Security;

    /// <summary>
    /// Defines the server-side service methods for the Seamless DU submission dialog.
    /// </summary>
    public partial class SeamlessDuService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Process the method called from AJAX.
        /// </summary>
        /// <param name="methodName">The name of the method to execute.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(this.RunAudit):
                    this.RunAudit();
                    break;
                case nameof(this.GetLoginModel):
                    this.GetLoginModel();
                    break;
                case nameof(this.Submit):
                    this.Submit();
                    break;
                case nameof(this.UpdateLoan):
                    this.UpdateLoan();
                    break;
                default:
                    throw new ArgumentException($"Unknown method: {methodName}.");
            }
        }

        /// <summary>
        /// Run an audit on the loan file to determine if it's suitable for submission to DU.
        /// </summary>
        protected void RunAudit()
        {
            string auditResult =
                SerializationHelper.JsonNetAnonymousSerialize(
                    LoanDataAuditor.RunAudit(
                        this.GetGuid("LoanId"),
                        DuSeamlessAuditConditions.Singleton,
                        isPml: false));
            this.SetResult("AuditResult", auditResult);
        }

        /// <summary>
        /// Retrieves the necessary data points for displaying the Seamless DU login page.
        /// </summary>
        protected void GetLoginModel()
        {
            var loanData = CPageData.CreateUsingSmartDependency(this.GetGuid("LoanId"), typeof(SeamlessDuService));
            loanData.InitLoad();
            this.SetResult("LoginModel", DuSeamlessLoginModel.GenerateLoginModelJson(PrincipalFactory.CurrentPrincipal, loanData, isPml: false));
        }

        /// <summary>
        /// Submits a DU underwriting request to the DU system, using data from the DU seamless login page.
        /// </summary>
        protected void Submit()
        {
            string fannieId = GetString("FannieId");
            var model = SerializationHelper.JsonNetDeserialize<DuSeamlessLoginModel>(GetString("model"));
            if (string.IsNullOrEmpty(fannieId))
            {
                var saveLoanData = CPageData.CreateUsingSmartDependency(this.GetGuid("LoanId"), typeof(SeamlessDuService));
                saveLoanData.InitSave(ConstAppDavid.SkipVersionCheck);
                model.ApplyToLoanFile(saveLoanData);
                saveLoanData.Save();
            }

            var loadLoanData = CPageData.CreateUsingSmartDependency(this.GetGuid("LoanId"), typeof(SeamlessDuService));
            loadLoanData.InitLoad();

            string duResponseData = DuRequestSubmission.SubmitAndCreateViewModel(
                PrincipalFactory.CurrentPrincipal,
                loadLoanData.sLId,
                model.ConvertToDuUnderwritingRequest(PrincipalFactory.CurrentPrincipal, loadLoanData, fannieId),
                model.CreditInfo.CreditReportOption);
            this.SetResult("DuResponseData", duResponseData);
        }

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Updates the loan file with information from the last successful DU response.
        /// </summary>
        private void UpdateLoan()
        {
            Guid loanId = GetGuid("LoanId");
            FnmaXisDuUnderwriteResponse response = new FnmaXisDuUnderwriteResponse(loanId);
            response.ImportToLoanFile(loanId, PrincipalFactory.CurrentPrincipal, this.GetBool("ImportAusFindings"), this.GetBool("ImportCreditReport"), this.GetBool("ImportLiabilities"));
        }
    }
}