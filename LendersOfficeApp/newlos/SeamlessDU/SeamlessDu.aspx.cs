﻿namespace LendersOffice.Integration.SeamlessDU
{
    using System;
    using LendersOffice.Common;

    /// <summary>
    /// Page for the (new - 2017) Seamless DU popup dialog.
    /// </summary>
    public partial class SeamlessDu : BaseServicePage
    {
        /// <summary>
        /// Page initialization event handler.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.RegisterJsScript("angular-1.5.5.min.js");
            this.RegisterJsScript("angular-route.1.4.8.min.js");
            this.RegisterJsScript("SeamlessDu.js");
            this.RegisterJsScript("angularModules/LqbForms.module.js");
            this.RegisterJsScript("angularModules/LqbAudit.module.js");

            this.RegisterCSS("SeamlessDu.css");
            this.RegisterCSS("bootstrap.min.css");
            this.RegisterCSS("font-awesome.css");
            base.OnInit(e);
        }
    }
}