﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SeamlessDu.aspx.cs" Inherits="LendersOffice.Integration.SeamlessDU.SeamlessDu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Seamless DU Interface</title>
    <script type="text/javascript">
        var seamlessFolder = gVirtualRoot + '/newlos/SeamlessDU';
        var virtualRoot = gVirtualRoot;
    </script>
</head>
<body id="app" ng-controller="SeamlessDUController as seamless">
    <h4 class="page-header ng-cloak">{{pageTitle}}</h4>
    <span ng-show="!loaded">Loading {{pageTitle}}...<span class="fa fa-spin fa-spinner fa-fw"></span></span>
    <div class="ng-cloak" ng-show="loaded && !loadSuccess" >
        Something went wrong communicating with the server:
        <br />{{errorMessage}}
        <div class="bottomButtons"><button ng-click="closeWindow()">Close</button></div>
    </div>
    <div ng-view></div>
    <script type="text/javascript">
        // Because of the multiple angular apps on the page, bootstrap explicitly.
        angular.bootstrap(document.getElementById('app'), ['SeamlessDU']);
    </script>
</body>
</html>
