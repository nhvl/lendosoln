<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BorrowerREOFrame.ascx.cs" Inherits="LendersOfficeApp.newlos.BorrowerREOFrame" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<script type="text/javascript">

function _init() {
  <% if (IsReadOnly) { %>
    document.getElementById("btnPrev").disabled = true;
    document.getElementById("btnNext").disabled = true;
    document.getElementById("btnInsert").disabled = true;
    document.getElementById("btnAdd").disabled = true;
    document.getElementById("btnSave").disabled = true;
    document.getElementById("btnUp").disabled = true;    
    document.getElementById("btnDown").disabled = true;
    document.getElementById("btnDelete").disabled = true;    
  <% } %>
}

    function refreshCalculation() {
        callFrameMethod(window, "SingleIFrame", "refreshCalculation");
    }

function saveMe() {
    var ret = false;
    var single_saveMe = retrieveFrameProperty(window, "SingleIFrame", "single_saveMe");
    if (single_saveMe) {
        var result = single_saveMe();        
        if(ret = !result.error)
        {   
            var f_refreshInfoFromDb = retrieveFrameProperty(parent, "info", "f_refreshInfoFromDB");
            if(f_refreshInfoFromDb) {
                f_refreshInfoFromDb();//update loan summary
            }
            else {
                callFrameMethod(parent, "info", "f_refreshInfo");//update lead summary
            }
        }
        else
        {
            clearDirty();
        }
      }
      return ret;
}
 
function updateTotal(aReTotVal, aReTotMAmt, aReTotGrossRentI, aReTotMPmt, aReTotHExp, aReTotNetRentI,
aReRetainedTotGrossRentI, aReRetainedTotMPmt, aReRetainedTotHExp, aReRetainedTotNetRentI) {
  <%= AspxTools.JsGetElementById(aReTotVal) %>.value = aReTotVal;
  <%= AspxTools.JsGetElementById(aReTotMAmt) %>.value = aReTotMAmt;
  
  <%= AspxTools.JsGetElementById(aReRentalTotGrossRentI) %>.value = aReTotGrossRentI;
  <%= AspxTools.JsGetElementById(aReRentalTotMPmt) %>.value =  aReTotMPmt;
  <%= AspxTools.JsGetElementById(aReRentalTotHExp) %>.value =  aReTotHExp;
  <%= AspxTools.JsGetElementById(aReRentalTotNetRentI) %>.value =  aReTotNetRentI;
  
  <%= AspxTools.JsGetElementById(aReRetainedTotGrossRentI) %>.value = aReRetainedTotGrossRentI;
  <%= AspxTools.JsGetElementById(aReRetainedTotMPmt) %>.value =  aReRetainedTotMPmt;
  <%= AspxTools.JsGetElementById(aReRetainedTotHExp) %>.value =  aReRetainedTotHExp;
  <%= AspxTools.JsGetElementById(aReRetainedTotNetRentI) %>.value =  aReRetainedTotNetRentI;
    
}

function getRecordID()
{
   return callFrameMethod(parent, "SingleIFrame", "getRecordID");
}

function GoToLiabilities(liabilityID)
{
    var link = document.getElementById("GoToLiabilities");
    link.href = "javascript:linkMe('BorrowerInfo.aspx?pg=4&RecordID="+liabilityID+"');"
    link.click();
}

</script>

<a href="javascript:linkMe('BorrowerInfo.aspx?pg=4');" id="GoToLiabilities" title="Go to Liabilities" visible="false">Liabilities</a>
<table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
    <tr>
        <td></td>
        <td class="FieldLabel" style="padding-left:3px">Rental Properties</td>
        <td class="FieldLabel" style="padding-left:3px">Retained Properties</td>
    </tr>
  <tr>
    <td nowrap>
        <table cellspacing="0" cellpadding="0" border="0" class="InsetBorder" style="width:100%">
            <tr>
                <td nowrap></td>
                <td class="FieldLabel" nowrap>Market Value</td>
                <td class="FieldLabel" nowrap>Mtg Amount</td>
            </tr>
            <tr>
                <td nowrap class="FieldLabel">Total</td>
                <td nowrap><ml:MoneyTextBox ID="aReTotVal" ReadOnly="True" style="width:98%; min-width:80px;" preset="money" runat="server" /></td>
                <td nowrap><ml:MoneyTextBox ID="aReTotMAmt" ReadOnly="True" style="width:98%; min-width:80px;" preset="money" runat="server" /></td>
            </tr>
        </table>
   </td>
   <td>
        <table cellspacing="0" cellpadding="0" border="0" class="InsetBorder" style="width:100%">  
            <tr>
              <td class="FieldLabel" nowrap>Gross Rent</td>
              <td class="FieldLabel" nowrap>Mtg Payment</td>
              <td class="FieldLabel" nowrap>Ins/Maint/Taxes</td>
              <td class="FieldLabel" nowrap>Cash Flow</td>
            </tr>
            <tr>
              <td nowrap><ml:MoneyTextBox ID="aReRentalTotGrossRentI" ReadOnly="True" style="width:98%; min-width:80px;" preset="money" runat="server" /></td>
              <td nowrap><ml:MoneyTextBox ID="aReRentalTotMPmt" ReadOnly="True" style="width:98%; min-width:80px;" preset="money" runat="server" /></td>
              <td nowrap><ml:MoneyTextBox ID="aReRentalTotHExp" ReadOnly="True" style="width:98%; min-width:80px;" preset="money" runat="server" /></td>
              <td nowrap><ml:MoneyTextBox ID="aReRentalTotNetRentI" ReadOnly="True" style="width:98%; min-width:80px;" preset="money" runat="server" /></td>
            </tr>
        </table>
   </td>
   <td>
        <table cellspacing="0" cellpadding="0" border="0" class="InsetBorder" style="width:100%">
            <tr>
              <td class="FieldLabel" nowrap>Gross Rent</td>
              <td class="FieldLabel" nowrap>Mtg Payment</td>
              <td class="FieldLabel" nowrap>Ins/Maint/Taxes</td>
              <td class="FieldLabel" nowrap>Cash Flow</td>
            </tr>
            <tr>
              <td nowrap><ml:MoneyTextBox ID="aReRetainedTotGrossRentI" ReadOnly="True" style="width:98%; min-width:80px;" preset="money" runat="server" /></td>
              <td nowrap><ml:MoneyTextBox ID="aReRetainedTotMPmt" ReadOnly="True" style="width:98%; min-width:80px;" preset="money" runat="server" /></td>
              <td nowrap><ml:MoneyTextBox ID="aReRetainedTotHExp" ReadOnly="True" style="width:98%; min-width:80px;" preset="money" runat="server" /></td>
              <td nowrap><ml:MoneyTextBox ID="aReRetainedTotNetRentI" ReadOnly="True" style="width:98%; min-width:80px;" preset="money" runat="server" /></td>
            </tr>
        </table>
   </td>
  </tr>
  <tr>
    <td nowrap colspan="3">
      <iframe id="ListIFrame" width="100%" height="150" tabindex="-1" src=<%= AspxTools.SafeUrl(ListUrl) %>></iframe>
    </td>
  </tr>
  <tr>
    <td nowrap  colspan="3">
    <input id="btnPrev" onclick="runListIFrameMethod('list_go', -1); refreshCalculation();" type="button" value="<< Prev" tabindex="10" accesskey="P" NoHighlight="true" />
    <input id="btnNext" onclick="runListIFrameMethod('list_go', 1); refreshCalculation();" type="button" value="Next >>" tabindex="10" accesskey="N" NoHighlight="true" />
    <input id="btnInsert" onclick="parent_onInsert(); refreshCalculation();" type="button" value="Insert" tabindex="10" NoHighlight="true" />
    <input id="btnAdd" type="button" value="Add" onclick="parent_onAdd(); refreshCalculation();" tabindex="10" NoHighlight="true" />
    <input id="btnSave" onclick="saveMe();" type="button" value="Save" tabindex="10" NoHighlight="true" />
    <input id="btnUp" onclick="runListIFrameMethod('list_move', 1);" type="button" value="Move Up" tabindex="10" NoHighlight="true" />
    <input id="btnDown" onclick="runListIFrameMethod('list_move', -1);" type="button" value="Move Down" tabindex="10" NoHighlight="true" />
    <input id="btnDelete" onclick="runListIFrameMethod('list_deleteRecord'); refreshCalculation();" type="button" value="Delete" tabindex="10" NoHighlight="true"/>
    </td>
  </tr>
  <tr>
    <td nowrap valign="top" colspan="3">
      <iframe id="SingleIFrame" width="100%" height="220" tabindex="100" src=<%= AspxTools.SafeUrl(EditUrl) %>></iframe>
    </td>
  </tr>
</table>
