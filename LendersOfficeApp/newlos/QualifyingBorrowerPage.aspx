﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QualifyingBorrowerPage.aspx.cs" Inherits="LendersOfficeApp.newlos.QualifyingBorrowerPage" %>
<%@ Register TagPrefix="uc1" TagName="QualifyingBorrower" Src="~/newlos/QualifyingBorrower.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Qualifying Borrower</title>
    <script>

        function createUpdateFunction(isSave) {
            return function (refreshScreen) {
                var model = retrieveQualifyingBorrowerModelForUpdate();
                model.loanid = ML.sLId;
                if (refreshScreen) {
                    gService.loanedit.callAsyncSimple(isSave ? "SaveData" : "CalculateData", model, function (result) {
                        populateQualifyingBorrower(result.value);
                        if (isSave) {
                            clearDirty();
                        }
                    }, function (result) {
                        LQBPopup.ShowString(result.UserMessage);
                    });
                }
                else {
                    var result = gService.loanedit.call(isSave ? "SaveData" : "CalculateData", model);
                    if (result.error) {
                        LQBPopup.ShowString(result.UserMessage);
                        return false;
                    }

                    if (isSave) {
                        clearDirty();
                    }

                    return true;
                }
            }
        }

        $(function () {
            setTimeout(function () {
                window.refreshCalculation = createUpdateFunction(false);
                window.f_saveMe = createUpdateFunction(true);
                window.saveMe = window.f_saveMe;

                window.loadData = function () {
                    var model = { loanid: ML.sLId };
                    gService.loanedit.callAsyncSimple("LoadData", model, function (result) {
                        populateQualifyingBorrower(result.value);
                    }, function (result) {
                        LQBPopup.ShowString(result.UserMessage);
                    });
                };

                initializeQualifyingBorrower(false, refreshCalculation);
                loadData();
            });
        });
    </script>
</head>
<body class="EditBackground">
    <form id="form1" runat="server">
        <uc1:QualifyingBorrower runat="server" ID="QualifyingBorrowerControl" />
    </form>
</body>
</html>
