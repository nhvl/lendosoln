﻿#region Generated Code
namespace LendersOfficeApp.newlos
#endregion
{
    using System;

    /// <summary>
    /// The QualifyingBorrowerPage.  All the logic is contained within QualifyingBorrower.ascx.
    /// </summary>
    public partial class QualifyingBorrowerPage : BaseLoanPage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;
            base.OnInit(e);
        }
    }
}