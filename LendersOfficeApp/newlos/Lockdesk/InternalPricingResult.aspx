﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InternalPricingResult.aspx.cs" Inherits="LendersOfficeApp.newlos.Lockdesk.InternalPricingResult" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Pricing Result</title>
    <style>
        #WaitingPanel
        {
            width: 100%;
            height: 200px;
        }
        #WaitingPanel div
        {
            width: 100px;
            margin-top: 20px;
            margin-left: auto;
            margin-right: auto;
        }
        td.header
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: xx-small;
            font-weight: bold;
            color: #666;
            background-color: #e2e2e2;
            text-align: center;
            vertical-align: bottom;
            width: 50px;
            padding: 3px;
            border-bottom: 1px solid #666;
            border-left: 1px solid #666;
        }
        .program_name
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            font-weight: bold;
            color: #666;
            letter-spacing: -0.1em;
            line-height: normal;
            padding: 3px 5px 2px 5px;
        }
        .program_name_desc
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            color: #666;
            letter-spacing: -0.1em;
            line-height: normal;
            padding: 3px 5px 2px 5px;
        }
        .item_font
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: xx-small;
            line-height: normal;
            color: #666;
        }
        .rate_item
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            letter-spacing: -0.1em;
            font-size: xx-small;
            color: #666;
            padding-left: 10px;
            padding-right: 5px;
            text-align: right;
            border-left: 1px solid #666;
        }
        .rate_item_expired
        {
            color: red;
        }
        .result_category
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            color: #f93;
            font-size: 12px;
            font-weight: bold;
            vertical-align: bottom;
            padding: 0px 10px 3px 3px;
            border-bottom: 1px solid #666;
        }
        .alt_bg
        {
            background-color: #def;
        }
        .ineligible_alt_bg
        {
            background-color: #ffd7d7;
        }
        .rt_border
        {
            border-right: 1px solid #666;
        }
        .dti_item
        {
            text-align: right;
            font-weight: bold;
            color: #666;
        }
        #SelectTooltip
        {
            z-index: 900;
            width: 200px;
            border-right: black 3px solid;
            padding-right: 5px;
            border-top: black 3px solid;
            display: none;
            padding-left: 5px;
            padding-bottom: 5px;
            border-left: black 3px solid;
            padding-top: 5px;
            border-bottom: black 3px solid;
            position: absolute;
            background-color: whitesmoke;
            overflow: auto;
            
        }
        
        #SelectTooltip .links {
            text-align:center;
        }
    </style>
</head>
<body>
	<script id="EligibilityHeaderTemplate" type="text/x-jquery-tmpl">
	<table cellpadding="0" cellspacing="0">
	<tbody class="eligible_header">
	<tr>
		<td class="result_category" colspan="2">Eligible Loan Programs</td>
		{{each Headers}}<td class="header">${$value}</td>{{/each}}
	</tr>
	</tbody>
	{{if ResultMode === 'RateMerge'}}
		{{tmpl(EligiblePrograms) "#RateMergeTemplate"}}
	{{else}}
		{{tmpl(EligiblePrograms) "#RegularTemplate"}}
	{{/if}}
	</table>
	<hr color="#ff99933" noShade SIZE="1"/><br/>
	<table cellpading="0" cellspacing="0">
	  <tr>
	    <td class="result_category" colspan="2">Ineligible Loan Programs</td>
	    {{if oRateMergeResult.IneligibleHeaders.length > 0}}
		    {{each IneligibleHeaders}}<td class="header">${$value}</td>{{/each}}
		  {{else}}
		    <td class="header">&nbsp;</td>
		  {{/if}}
	  </tr>
	  {{if oRateMergeResult.IneligibleHeaders.length > 0}}
	    {{tmpl(IneligiblePrograms) "#IneligibleWithRateOptionTemplate"}}
	  {{else}}
	    {{tmpl(IneligiblePrograms) "#IneligibleTemplate"}}
	  {{/if}}
	</table>
	</script>
	
	<script id="IneligibleTemplate" type="text/x-jquery-tmpl">
	<tbody class="ineligible_program_group">
	  <tr>
	    <td class="program_name" colspan="3">${lLpTemplateNm}</td>
	  </tr>
	  <tr>
	    <td colspan="3">${lLpInvestorNm} ${ProductCode}</td>
	  </tr>
	  <tr>
      <td colspan="3">
	      {{each DisqualifiedRules}}
	      ${$value}<br/>
	      {{/each}}
	    </td>
	  </tr>
    <tr>
      <td colspan="3">
			  <a href="#" class="preview-link">preview</a>
			</td>
    </tr>
	</tbody>
	</script>
	<script id="RateMergeTemplate" type="text/x-jquery-tmpl">
	<tbody class="program_group">
		<tr>
			<td class="program_name" colspan="${oRateMergeResult.Headers.length + 2}">${lLpTemplateNm}</td>
		</tr>
		
		{{each(index, rate) RateOptions}}
		<tr class="rate_group">
			<td>&nbsp;</td>
			<td align="right">
			    {{if rate.slice(-1)[0].substring(0,2) != 'd:'}}
			        <a href="#" class="preview-link" rateindex="${index}">preview</a>&nbsp;&nbsp;
			        <a href="#" class="select-link" rateindex="${index}">select option</a>
                {{else}}
                    <a href="#" class="preview-link" rateindex="${index}">preview</a>&nbsp;&nbsp;
                    <a href="#" class="unavailable-link" rateindex="${index}">unavailable</a> 
                {{/if}}
            </td>
			{{each rate}}
			    {{if $index < oRateMergeResult.Headers.length}}
			        <td class="rate_item  ${AreRatesExpired ? 'rate_item_expired' : ''}">${$value}</td>
                {{/if}}
            {{/each}}
		</tr>
		{{/each}}
		
		<tr>
			<td colspan="${oRateMergeResult.Headers.length + 2}"><hr /></td>
		</tr>
	</tbody>
	</script>
	
	<script id="RegularTemplate" type="text/x-jquery-tmpl">
	<tbody class="program_group">
		<tr>
			<td class="program_name" colspan="${oRateMergeResult.Headers.length - 1}">${lLpTemplateNm}</td>
			<td colspan="2" class="dti_item">Max DTI:</td>
			<td class="dti_item">${MaxDti}</td>
		</tr>
		<tr>
		  <td class="program_name_desc" colspan="${oRateMergeResult.Headers.length + 2}">${lLpInvestorNm} ${ProductCode}</td>
		</tr>
		<tr class="rate_group">
			<td>
			  <a href="#" class="preview-link">preview</a>
			  {{if RateOptions}}
			    <label for="__m_${lLpTemplateId}" class="item_font">View More</label><input type="checkbox" id="__m_${lLpTemplateId}">
			  {{/if}}
			</td>
			
			<td>
			    {{if RepresentativeRate.slice(-1)[0].substring(0,2) != 'd:'}}
			        <a href="#" class="select-link" rateindex="-1">select option</a>
                {{else}}
                      <a href="#" class="unavailable-link" rateindex="-1">unavailable</a> 
                {{/if}}
            </td>
			{{each RepresentativeRate}}
			    {{if $value.substring(0,2) != 'd:'}}
			        <td class="rate_item ${AreRatesExpired ? 'rate_item_expired' : ''}">    
                        ${$value}
                    </td>
                {{/if}}
            {{/each}}
		</tr>
		{{if RateOptions}}
		{{each(index, rate) RateOptions}}
			<tr class="rate_group" style="display:none">
				<td>
				  {{if index == 0}}
				    <a href="#" class="preview-link">preview</a>
				    <label for="__l_${lLpTemplateId}" class="item_font">View More</label><input type="checkbox" checked="checked" id="__l_${lLpTemplateId}">
				  {{/if}}

				</td>
				<td>
				    {{if rate.slice(-1)[0].substring(0,2) != 'd:'}}
				        <a href="#" class="select-link" rateindex="${index}">select option</a>
				    {{else}}
				        <a href="#" class="unavailable-link" rateindex="${index}">unavailable</a> 
				    {{/if}}
				</td>
				{{each rate}}{{if $index < oRateMergeResult.Headers.length}}<td class="rate_item  ${AreRatesExpired ? 'rate_item_expired' : ''}">${$value}</td>{{/if}}{{/each}}
			</tr>
		{{/each}}
		{{/if}}
	</tbody>
	</script>	
	
	<script id="IneligibleWithRateOptionTemplate" type="text/x-jquery-tmpl">
	<tbody class="ineligible_program_group">
		<tr>
			<td class="program_name" colspan="${oRateMergeResult.IneligibleHeaders.length - 1}">${lLpTemplateNm}</td>
			<td colspan="2" class="dti_item">Max DTI:</td>
			<td class="dti_item">${MaxDti}</td>
		</tr>
	  <tr>
	    <td colspan="${oRateMergeResult.IneligibleHeaders.length + 2}">${lLpInvestorNm} ${ProductCode}</td>
	  </tr>		
	  <tr>
      <td colspan="${oRateMergeResult.IneligibleHeaders.length + 2}">
	      {{each DisqualifiedRules}}
	      ${$value}<br/>
	      {{/each}}
	    </td>
	  </tr>
		<tr class="rate_group">
			<td>
			  <a href="#" class="preview-link">preview</a>
			  {{if RateOptions}}
			    <label for="__m_${lLpTemplateId}" class="item_font">View More</label><input type="checkbox" id="__m_${lLpTemplateId}">
			  {{/if}}
			</td>
			{{if RepresentativeRate}}
			<td>
			    {{if RepresentativeRate.slice(-1)[0].substring(0,2) != 'd:'}}
			        <a href="#" class="select-link" rateindex="-1">select option</a>
                {{else}}
                      <a href="#" class="unavailable-link" rateindex="-1">unavailable</a> 
                {{/if}}
            </td>
			{{each RepresentativeRate}}
			    {{if $value.substring(0,2) != 'd:'}}
			        <td class="rate_item ${AreRatesExpired ? 'rate_item_expired' : ''}">${$value}</td>
			    {{else}}
			        {{if $index < oRateMergeResult.Headers.length}}
				        <td class="rate_item ${AreRatesExpired ? 'rate_item_expired' : ''}">${$value}</td>
				    {{/if}}
                {{/if}}
            {{/each}}
            {{else}}
			  <td colspan="${oRateMergeResult.IneligibleHeaders.length + 1}">&nbsp;</td>
			{{/if}}
		</tr>
		{{if RateOptions}}
		{{each(index, rate) RateOptions}}
			<tr class="rate_group" style="display:none">
				<td>
				  {{if index == 0}}
				    <a href="#" class="preview-link">preview</a>
				    <label for="__l_${lLpTemplateId}" class="item_font">View More</label><input type="checkbox" checked="checked" id="__l_${lLpTemplateId}">
				  {{/if}}

				</td>
				<td>
				    {{if rate.slice(-1)[0].substring(0,2) != 'd:'}}
				        <a href="#" class="select-link" rateindex="${index}">select option</a>
                    {{else}}
                        <a href="#" class="unavailable-link" rateindex="${index}">unavailable</a> 
                    {{/if}}
                </td>
				{{each rate}}
				    {{if $index < oRateMergeResult.Headers.length}}
				        <td class="rate_item ${AreRatesExpired ? 'rate_item_expired' : ''}">${$value}</td>
				    {{/if}}
                {{/each}}
			</tr>
		{{/each}}
		{{/if}}
	</tbody>
	</script>	
	
	<div id="SelectTooltip">
	    Rate is unavailable because: <br />
	    <span id="UnavailableMsg"></span>
	    <br />
	    <br />
	    <div class="links">
	    <a href="#" class="close" >close</a>&nbsp; &nbsp;
	    <a href="#" class="select" id="selectoption"  runat="server" >select option</a>
	    </div>
	</div>
  <script type="text/javascript">

    var gRequestId = null;
    var g_iPollingInterval = 1000;
    var oRateMergeResult = null;
    function _init()
    {
      f_requestPricing();
    }
    function switchApplicant() {
        // No-op. There are no borrower-specific fields on this page.
    }
    
    function f_requestPricing()
    {
      var args = {
        LoanId: ML.sLId,
        sPricingModeT: ML.sPricingModeT,
        historicalLoanProgramTemplate: ML.historicalLoanProgramTemplate,
        historicalPolicy: ML.historicalPolicy,
        historicalRateOption: ML.historicalRateOption
      };
      
        
      var result = gService.main.call('RequestPricing', args);
      if (!result.error)
      {
        gRequestId = result.value["RequestId"];
        setTimeout(f_isAvailable, g_iPollingInterval);
      } 
      else
      {
        f_displayError('Unable to get pricing please try again.');
      }
    }
    function f_isAvailable()
    {
      var args = { RequestId: gRequestId };

      var result = gService.main.call('IsResultReady', args);
      if (!result.error)
      {
        var isReady = result.value["IsReady"] === 'True';
        if (isReady)
        {
          f_displayResult();
        } 
        else
        {
          setTimeout(f_isAvailable, g_iPollingInterval);
        }
      }
      else
      {
        f_displayError('Unable to get pricing please try again.');
      }

    }
    function f_displayError(errMsg)
    {
      alert(errMsg);
      linkMe('RunInternalPricing.aspx');
    }
    function f_displayResult()
    {
      var args = {
      RequestId: gRequestId,
      LoanId: ML.sLId,
      sPricingModeT: ML.sPricingModeT   ,
      StartTicks:ML.StartTicks   
        };

      var result = gService.main.call('GetResult', args);

      if (!result.error)
      {
        var json = result.value["json"];

        oRateMergeResult = $.parseJSON(json);

        $('#EligibilityHeaderTemplate').tmpl(oRateMergeResult).appendTo('#ResultPanel');

        if (oRateMergeResult.ResultMode === 'Regular')
        {
          $('#ResultPanel .program_group:odd').addClass('alt_bg');
          $('#ResultPanel .program_group .rate_group').on('click', '[type=checkbox]', f_expand);
        }
        else
        {
          $('#ResultPanel .program_group .rate_group:nth-child(odd)').addClass('alt_bg');
          $('#ResultPanel .eligible_header .header:last').css('width', 'auto');
          $('#ResultPanel .program_group .rate_group td:last-child').css('text-align', 'left');
        }
        $('#ResultPanel .ineligible_program_group:odd').addClass('ineligible_alt_bg');
        $('#ResultPanel .ineligible_program_group .rate_group').on('click', '[type=checkbox]', f_expandIneligible);
        
        $('#ResultPanel').on('click', '.preview-link', f_preview)
            .on('click', '.unavailable-link', f_unavailable);
        $('#ResultPanel').on('click', 'tbody.program_group a.select-link', f_selectOption);
        $('#ResultPanel').on('click', 'tbody.ineligible_program_group a.select-link', f_selectIneligibleOption);
        $('#SelectTooltip').on('click', 'a.close', function () { Modal.Hide(); return false; })
            .on('click', '.select', f_unavailableSelect);
        
        $('#WaitingPanel').hide();
        $('#RegularPanel').show();        
      } 
      else
      {
        f_displayError('Unable to get pricing please try again.');
        return;
      }

    }
    
    function f_getSelectedRate(o)
    {
      var oProgram = $(o).tmplItem().data;
      var rateIndex = parseInt($(o).attr('rateindex'));

      var productId = null;
      var uniqueChecksum = null;
      var rateOption = rateIndex >= 0 ? oProgram.RateOptions[rateIndex] : oProgram.RepresentativeRate;
      if (oRateMergeResult.ResultMode === 'RateMerge' && oProgram.lLpTemplateId === '00000000-0000-0000-0000-000000000000')
      {
        var baseIndex = oRateMergeResult.Headers.length;
        productId = rateOption[baseIndex + 0]; // ProductId is the first item after the last visible header data.
        uniqueChecksum = rateOption[baseIndex + 1]; // 
      }
      else
      {
        productId = oProgram.lLpTemplateId; // Result is in regular mode.
        uniqueChecksum = oProgram.UniqueChecksum;
      }

      return { ProductId: productId, RateOption: rateOption, UniqueChecksum : uniqueChecksum };    
    }
    function f_preview()
    {
      
      var selectedObject = f_getSelectedRate(this);

      var obj = {
        lienQualifyModeT: '0',
        version: '',
        productId: selectedObject.ProductId,
        loanid: ML.sLId,
        sPricingModeT: ML.sPricingModeT,
        UniqueChecksum : selectedObject.UniqueChecksum,
        historicalPolicy: ML.historicalPolicy,
        historicalRateOption: ML.historicalRateOption,
        rate: selectedObject.RateOption[0]
      };
      var url = '/newlos/lockdesk/CertificateMainFrame.aspx?' + f_generateQueryString(obj);
      
      showModal(url, null, 'dialogWidth:780px;dialogHeight:580px;center:yes;resizable:yes;scroll:yes;status=yes;help=no;');
      return false;
  }
  var _selectedRate = null;
  function f_unavailable(e) {
      
      var selectedObject = f_getSelectedRate(this);
      _selectedRate = selectedObject;
      var msg = selectedObject.RateOption.slice(-1)[0].substring(3);
      $('#UnavailableMsg').text(msg);
      Modal.ShowPopup('SelectTooltip', null, e);
      
      return false;
  }
  function f_unavailableSelect(){
    f_selectOptionImp(_selectedRate);
}
  function f_selectIneligibleOption() {
      var selectedObject = f_getSelectedRate(this);
      f_selectOptionImp(selectedObject, 2);
  }
  function f_selectOption() {
      var selectedObject = f_getSelectedRate(this);
      f_selectOptionImp(selectedObject, 3);
  }
    function f_selectOptionImp(selectedObject, feeIndex) {
      feeIndex = ML.sPricingModeT == 3 ? feeIndex : 2;
      var obj = {
      loanid: ML.sLId,
        productId: selectedObject.ProductId,
        lienQualifyModeT: '0',
        sPricingModeT : ML.sPricingModeT,
        version: '',
        rate : selectedObject.RateOption[0],
        fee: selectedObject.RateOption[feeIndex],
        UniqueChecksum: selectedObject.UniqueChecksum,
        historicalPolicy: ML.historicalPolicy,
        historicalRateOption: ML.historicalRateOption                
      };

      var url = '/newlos/lockdesk/InternalPricingSelectionOption.aspx?' + f_generateQueryString(obj);
      showModal(url, null, null, null, null, {hideCloseButton: true});
    }
    function f_generateQueryString(obj)
    {
      var o = obj || '';
      var str = '';
      for (var p in o)
      {
        if (o.hasOwnProperty(p))
        {
          str += p + '=' + o[p] + '&';
        }
      }
      return str;
    }
    function f_expand()
    {
      $(this).closest('.program_group').children('.rate_group').toggle();
      return false;
    }
    function f_expandIneligible()
    {
      $(this).closest('.ineligible_program_group').children('.rate_group').toggle();
      return false;
    }
    function f_goBack()
    {
      linkMe('RunInternalPricing.aspx?sPricingModeT=' + ML.sPricingModeT );
    }
  </script>
    <form id="form1" runat="server">
    <div class="LegendSectionHeader">Results</div>
    <div id="WaitingPanel">
      <div>
      <span style="font-weight:bold;font-size:12px">Please wait ...</span>
      <br /><br /><img src="~/images/status.gif" runat="server" />
      </div>
    </div>
    <div id="RegularPanel" style="display:none">
      <div id="ResultPanel"></div>
      <input type="button" value="<< Go back" onclick="f_goBack();"/>
    </form>
</body>
</html>
