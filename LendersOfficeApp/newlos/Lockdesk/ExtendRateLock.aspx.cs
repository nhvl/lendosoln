﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Lockdesk
{
    public partial class ExtendRateLock : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DisplayCopyRight = false;
            RegisterService("BrokerRateLock", "/newlos/Lockdesk/BrokerRateLockService.aspx");
            if (string.IsNullOrEmpty(RequestHelper.GetSafeQueryString("load")))
            {
                return;
            }

            LoadFromArgs.Value = "False";
            CPageData data = CPageData.CreateUsingSmartDependency(LoanID, typeof(ExtendRateLock));
            data.InitLoad();

            var pageData = new
            {
                sNoteIR = data.sNoteIR_rep,
                sRLckdExpiredD = data.sRLckdExpiredD_rep,
                sBrokComp1Pc = data.sBrokComp1Pc_rep,
                sRAdjMarginR = data.sRAdjMarginR_rep,
                sOptionArmTeaserR = data.sIsOptionArm ? data.sOptionArmTeaserR_rep : "0.000%",
                sBrokerLockFinalBrokComp1PcPrice = data.sBrokerLockFinalBrokComp1PcPrice,
                bIsOptionArm = data.sIsOptionArm,
                slId = RequestHelper.LoanID
            };

            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            ClientScript.RegisterStartupScript(typeof(ExtendRateLock), "PageData", "var PageData = " + serializer.Serialize(pageData) + ";", true);
        }
    }
}
