﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.Security;
using DataAccess;
using DataAccess.LoanComparison;
using LendersOffice.RatePrice;
using LendersOffice.ObjLib.HistoricalPricing;

namespace LendersOfficeApp.newlos.Lockdesk
{
    public partial class CertificateWaitService : BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName) 
            {
                case "RequestCertificate":
                    RequestCertificate();
                    break;
                case "IsResultReady":
                    IsResultReady();
                    break;
            }
        }
        private void RequestCertificate()
        {
            Guid sLId = GetGuid("loanID");
            Guid productID = GetGuid("productID");
            DuplicateFinder.SetSkipLoanSubmissionCheck(sLId, true);

            string requestedRate = GetString("Rate", "") + ":" + GetString("Fee", "") + ":0.000,0.000";
            E_sLienQualifyModeT sLienQualifyModeT = (E_sLienQualifyModeT) GetInt("sLienQualifyModeT");
            E_RenderResultModeT renderResultModeT = E_RenderResultModeT.Regular;

            E_sPricingModeT sPricingModeT = (E_sPricingModeT)GetInt("sPricingModeT");

            string version = string.Empty;
            string uniqueChecksum = GetString("UniqueChecksum", "");
            AbstractUserPrincipal principal = this.User as AbstractUserPrincipal;


            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(CertificateWaitService));
            dataLoan.InitLoad();
            dataLoan.sPricingModeT = sPricingModeT;

            Guid lpePriceGroupId = dataLoan.sInternalPricingLpePriceGroupId;
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            HybridLoanProgramSetOption historicalOptions = options;

            BrokerDB broker = BrokerDB.RetrieveById(principal.BrokerId);
            var isHistorical = this.GetBool("isHistorical");
            if (isHistorical)
            {
                var snapshotJson = HttpUtility.HtmlDecode(this.GetString("snapshotJson"));
                var snapshot = SerializationHelper.JsonNetDeserialize<SubmissionSnapshot>(snapshotJson);
                var lenderSettings = principal.BrokerDB.HistoricalPricingSettings;
                historicalOptions = HybridLoanProgramSetOption.CreateHistoricalPricingOption(lenderSettings, snapshot);
            }

            Guid requestId = DistributeUnderwritingEngine.SubmitToEngineForRenderCertificate(principal,
            sLId, productID, requestedRate, sLienQualifyModeT, lpePriceGroupId, "" /*sTempLpeTaskMessageXml*/, version,
            renderResultModeT, sPricingModeT, uniqueChecksum, GetString("parId", ""), historicalOptions);

            SetResult("RequestID", requestId);
        }

        private void IsResultReady()
        {
            Guid requestId = GetGuid("requestid");

            bool isReady = LpeDistributeResults.IsResultAvailable(requestId);

            SetResult("IsReady", isReady);
        }
    }
}
