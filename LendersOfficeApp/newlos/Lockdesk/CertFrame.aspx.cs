﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Lockdesk
{
    public partial class CertFrame : BaseLoanPage
    {
#if LQB_NET45
        protected global::System.Web.UI.HtmlControls.HtmlIframe Certificate;
#else
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl Certificate;
#endif

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowLockDeskRead };
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DisplayCopyRight = false;
            CPageData data = CPageData.CreateUsingSmartDependency(LoanID, typeof(CertFrame));
            data.InitLoad();
            if (RequestHelper.GetInt("viewcert", -1) != -1)
            {
                Response.Clear();
                System.Xml.Xsl.XsltArgumentList arg = new System.Xml.Xsl.XsltArgumentList();
                arg.AddParam("currentYear", "", DateTime.Now.Year);
                XslTransformHelper.TransformFromEmbeddedResource("LendersOffice.ObjLib.Stylesheets.InvestorLoanSummary.xslt.config", InvestorLoanSummaryData.GenerateXml(LoanID), Response.OutputStream, arg);

                Response.Flush();
                Response.End();
            }

            this.RegisterJsScript("LQBPrintFix.js");
        
            Certificate.Attributes.Add("src", "CertFrame.aspx?viewcert=1&loanid=" + LoanID);
            Certificate.Attributes.Add("onload", "f_resizeIframe('Certificate',10);");
        }
    }
}
