<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BrokerRateLock.aspx.cs" Inherits="LendersOfficeApp.newlos.Lockdesk.BrokerRateLock" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="DataAccess" %>

<!--[if lt IE 7 ]> <html xmlns="http://www.w3.org/1999/xhtml" class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html xmlns="http://www.w3.org/1999/xhtml" class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html xmlns="http://www.w3.org/1999/xhtml" class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html xmlns="http://www.w3.org/1999/xhtml" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html xmlns="http://www.w3.org/1999/xhtml" > <!--<![endif]-->
<head runat="server">
    <title>Broker Rate Lock</title>

    <style type="text/css">
    input[type="checkbox"] { vertical-align: bottom; } 
    fieldset { clear: both; display: block; }
.ie7 input[type="checkbox"] { vertical-align: baseline; }
.ie6 input { vertical-align: text-bottom; }
        .hidden { display: none; }
        body { background-color: gainsboro; }
        .modalbox { line-height: 1em; height: 100px; border:3px inset black; top: 150px;  margin-left: 200px; left: 0; right: 0;  width: 425px; display: none; position: absolute; background-color : whitesmoke;}
                .RateLockTable
            {
                margin-top: 10px;
                border-collapse: collapse;
                table-layout: fixed;
            }
   
            table.RateLockTable td
            {
                border: 1px solid #EEEEEE;
            }
            
            td.date 
            {
                width:150px;
            }
            td.action 
            {
                width: 100px;
            }
            
            td.by 
            {
                width: 150px;
            }
            
            td.lockExpiration{
                width: 60px;
            }
            td.rate, td.price, td.fee, td.margin, td.trate, td.lockfee {
                width: 50px;
            }
            
            .moneyField {
                width: 70px;
            }
            
            td.reason {
                width: 200px;
            }
          
            
            .RateLockHeader
            {
                font-weight: bold;
                font-size: 11px;
                color: white;
                font-family: Arial, Helvetica, sans-serif;
                background-color: #999999;
            }
            .RateLockAlternatingItem
            {
                font-size: 11px;
                color: black;
                font-family: Arial, Helvetica, sans-serif;
                background-color: #cccccc;
            }
            .RateLockItem
            {
                font-size: 11px;
                color: black;
                font-family: Arial, Helvetica, sans-serif;
                background-color: white;
            }
            
            img { vertical-align: bottom; }
        .CenteringDiv input { margin: 0 auto; text-align: center; }
	    .descriptionField { width: 330px; }
	    .rateField { width: 50px; }
	    .noteField { width: 300px; }
	    .bigNoteField { width: 400px; height: 200px; }
	   
	    .modalPadding { padding: 5px; }
	    .dayField { width: 30px; }
	    .dateField { width: 60px; }
	    .timeField { width: 80px; }
	    .firstCol { width: 150px; }
	    .secondCol { width: 100px;}
	    .ButtonStyle { overflow: visible; width: auto; padding: 2px;}
	    .bottomMargin { margin-bottom: 10px; }
	    .padding { padding: 5px; }
	    #PricingTable { margin-left: 183px; }
	    fieldset { width: 810px; }
	    .wrapper { padding: 5px; }
	    #AdjustmentTable, .AdjustmentTable { }
	    img { vertical-align: bottom; }
	    #OriginatorCompLenderDetails { height: auto;  width: 390px; padding-top: 5px; padding-bottom: 5px; }
	    #OriginatorCompLenderDetails  table { margin: 0 auto; }
        #CloseOrigDetails { display: block; margin: 0 auto; text-align: center; }
        #ClearReportBtn { margin-left: 4px; margin-bottom: 4px}

        #ConfSaveWarn { display:none; }
    </style>
       
    <script type="text/javascript" src="../../inc/utilities.js"></script>
    <script type="text/javascript" src="../../inc/json.js"></script>
    <script type="text/javascript" src="../../inc/mask.js"></script>
    <link type="text/css" href="../../css/calendar-win2k-cold-2.css" rel="Stylesheet" />
    
</head>
<body>
    <form id="form1" runat="server" onreset="return false;">
        <asp:HiddenField runat="server" ID="LinkedLoanId" />
        <asp:HiddenField runat="server" ID="sIsRateLocked" />        
        <asp:HiddenField runat="server" ID="sIsOptionArm" />
        
        <div id="ConfSaveWarn">
            <h4 class="page-header">Save</h4>
            <br />
            <div>
                You must save your changes before generating lock confirmation. Do you want to save your changes?
            </div>
            <br />
            <input type="button" id="ConfSaveWarn_Yes" value="Yes" onclick="confSaveWarn_Yes_click();"/>
            <input type="button" id="ConfSaveWarn_No" value="No" onclick="confSaveWarn_No_click();"/>
        </div>

        <div class="MainRightHeader">Front-end Rate Lock</div>
        <div class="wrapper">
        <fieldset class="bottomMargin">
        
            <table cellpadding="0" cellspacing="2" style="table-layout:fixed;" >
                <tbody>
                    <tr>
                        <td class="firstCol">
                            <ml:EncodedLabel AssociatedControlID="sLpTemplateNm" runat="server" CssClass="FieldLabel">Loan Program</ml:EncodedLabel>
                        </td>
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="sLpTemplateNm" CssClass="noteField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ml:EncodedLabel AssociatedControlID="sLpTemplateNmSubmitted" runat="server" CssClass="FieldLabel">Registered Loan Program</ml:EncodedLabel>
                        </td>
                        <td colspan="3">
                            <asp:TextBox runat="server" CssClass="noteField" ID="sLpTemplateNmSubmitted" ReadOnly="true"
                                LockedField="LockedField"></asp:TextBox>
                            <input class="ButtonStyle" type="button" id="AllowSelectNewProgram" runat="server"
                                value="Clear Registered Program" onclick="f_allowSelectNewProgram()" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ml:EncodedLabel AssociatedControlID="sStatusT" runat="server" CssClass="FieldLabel">Loan Status</ml:EncodedLabel>
                        </td>
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="sStatusT" ReadOnly="true" LockedField="LockedField"
                                SkipMe="SkipMe"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Registered
                        </td>
                        <td class="secondCol" colspan="3">
                            <table cellpadding="0" cellspacing="0" width="500px">
                                <tr>
                                    <td>
                                        <span class="FieldLabel">Date </span>
                                    </td>
                                    <td>
                                        <span class="FieldLabel">&nbsp;Comments </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ml:DateTextBox runat="server" CssClass="dateField" ID="sSubmitD"> </ml:DateTextBox>
                                    </td>
                                    <td>
                                        &nbsp;<asp:TextBox runat="server" ID="sSubmitN" CssClass="noteField"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>
        
        <div class="bottomMargin">
            <input class="ButtonStyle" type="button" id="EmailRateLockConf"  value="Generate New Rate Lock Confirmation..." onclick="f_emailRateLockConf();" />          
            <input class="ButtonStyle" type="button" id="ViewLinkedLoan" value="Go To Linked Loan" runat="server" onclick="f_OpenLinkedLoan();"  />
            <input class="ButtonStyle" type="button" id="RunInternalPricing" value="Run Internal Pricing" onclick="f_runInternalPricing();" runat="server" />
            <br /><br />
            <ml:PassThroughLabel ID="storedRateLockCertInfo" class="FieldLabel" runat="server" Text="No rate lock confirmation on file"></ml:PassThroughLabel>
            
        </div>
        
        
        
        <fieldset class="bottomMargin">
            <div class="bottomMargin">  <!-- Start Rate Lock Info Section -->
                <div class="GridHeader padding bottomMargin">
                    <input  class="ButtonStyle" type="button" id="LockRateBtn" value="Lock Rate..." onclick="f_lockRateBtn_onClick();" />
                    <input  class="ButtonStyle" type="button" id="SuspendLockBtn" value="Resume Lock with Modifications..." onclick="f_suspendLockBtn_onClick();" />
                </div>   
                <table>
                <tr>
                <td>
                    <table cellpadding="0" cellspacing="2" border="0">
                    <tbody>
                    <tr>
                        <td class="FieldLabel firstCol">
                            Rate Lock Status
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="sRateLockStatusT" LockedField="LockedField"  ReadOnly="true" SkipMe="SkipMe"></asp:TextBox>
                        </td>
                        <td>
                            <input  class="ButtonStyle" type="button" id="ClearRateLockBtn" value="Clear Rate Lock Request" onclick="f_removeRequestedRate();"  />
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            <ml:EncodedLabel ID="Label1" AssociatedControlID="sRLckdDays" runat="server">Lock Period</ml:EncodedLabel>
                        </td>
                        <td class="FieldLabel">
                            <asp:TextBox MaxLength="4" CssClass="dayField" runat="server" ID="sRLckdDays" onblur="f_validateNumber(this)"></asp:TextBox>
                            days
                        </td>
                        <td colspan="3">
                            <input class="ButtonStyle" type="button" id="ExtendRateLock"  value="Extend Lock Period..." onclick="f_ExtendLockPeriod()" />
                        </td>
                    </tr>
                </tbody>
                </table>
                
                </td>
                
                <td>
                    <table style="width:300px;" id="CountTable">
                    <tr>
                        <td class="FieldLabel">
                            # Of Rate Renegotiations On File
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="sFERateRenegotiationCount" style="width:30px;"></asp:TextBox>
                        </td>
                        <td class="FieldLabel">
                            <asp:CheckBox ID="sFERateRenegotiationCountLckd" runat="server" /> Lock
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            # Of Extensions (Current Lock)
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="sFECurrentLockExtensionCount" style="width:30px;"></asp:TextBox>
                        </td>
                        <td class="FieldLabel">
                            <asp:CheckBox ID="sFECurrentLockExtensionCountLckd" runat="server" /> Lock
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            # Of Extensions (All Locks On File)
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="sFETotalLockExtensionCount" style="width:30px;"></asp:TextBox>
                        </td>
                        <td class="FieldLabel">
                            <asp:CheckBox ID="sFETotalLockExtensionCountLckd" runat="server" /> Lock
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            # Of Rate Re-Locks On File
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="sFETotalRateReLockCount" style="width:30px;"></asp:TextBox>
                        </td>
                        <td class="FieldLabel">
                            <asp:CheckBox ID="sFETotalRateReLockCountLckd" runat="server" /> Lock
                        </td>
                    </tr>
                </table>
                </td>
                </tr>
                </table>
                
                    <table cellpadding="0" cellspacing="2">
                        <thead>
                            <tr>
                                <td class="firstCol">
                                    &nbsp;
                                </td>
                                <td class="FieldLabel">
                                    Date
                                </td>
                                <td>
                                    <span class="FieldLabel">Time </span>
                                </td>
                                <td class="FieldLabel">
                                    Comments
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="FieldLabel">
                                    <ml:EncodedLabel AssociatedControlID="sRLckdD" runat="server">Rate Lock</ml:EncodedLabel>
                                </td>
                                <td>
                                    <asp:TextBox CssClass="dateField" runat="server" ID="sRLckdD" ReadOnly="true" SkipMe="SkipMe"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox CssClass="timeField" runat="server" ID="sRLckdDTime" ReadOnly="true" SkipMe="SkipMe"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="sRLckdN" CssClass="noteField"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>                                
                                <td>
                                    <ml:EncodedLabel runat="server" AssociatedControlID="sRLckdExpiredD" class="FieldLabel">Rate Lock Expiration</ml:EncodedLabel>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" CssClass="dateField" ID="sRLckdExpiredD" ReadOnly="true"
                                        SkipMe="SkipMe"></asp:TextBox>
                                </td>
                                <td></td>
                                <td>
                                    <asp:TextBox runat="server" ID="sRLckdExpiredN" CssClass="noteField"></asp:TextBox>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <hr />
                <asp:PlaceHolder ID="AutoPriceProcessResultPh" runat="server" Visible="false">
                  <div style="height: 170px">
                      <div>
                          <span class="FieldLabel">Overnight Process Report for Pricing:</span>
                          <asp:TextBox id="sAutoPriceProcessResult" ReadOnly="true" runat="server" Width="150px" />
                          <asp:TextBox id="sAutoPriceProcessResultD" ReadOnly="true" runat="server" Width="170px" />
                          <input class="ButtonStyle" type="button" id="ClearReportBtn" runat="server"
                                value="Clear Report" onclick="f_clearReport()" />
                      </div>
                 </asp:PlaceHolder>
                <table cellpadding="0" cellspacing="0" id="PricingTable" class="bottomMargin">
                    <thead>
                        <tr>
                            <td align="center" style="width:175px" class="FieldLabel">
                                &nbsp;
                            </td>
                            <td align="center" class="FieldLabel">
                                Rate
                            </td>
                            <td align="center" class="FieldLabel">
                                Price
                            </td>
                            <td align="center" class="FieldLabel">
                                Fee
                            </td>
                            <td align="center" class="FieldLabel">
                                Margin
                            </td>
                       
                            <td align="center" class="FieldLabel">
                                Teaser<br />Rate
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="FieldLabel">Base Price</td>
                            <td><asp:TextBox runat="server" onchange="f_UpdatePricing();" CssClass="rateField" preset="percent" ID="sBrokerLockBaseNoteIR"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" onchange="f_PriceFeeConversion( this, 'sBrokerLockBaseBrokComp1PcFee','P', 'Pricing');f_UpdatePricing();" CssClass="rateField" preset="percent" ID="sBrokerLockBaseBrokComp1PcPrice"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" onchange="f_PriceFeeConversion( this, 'sBrokerLockBaseBrokComp1PcPrice','F', 'Pricing');f_UpdatePricing();" CssClass="rateField" preset="percent"  ID="sBrokerLockBaseBrokComp1PcFee"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" onchange="f_UpdatePricing();" CssClass="rateField" preset="percent" ID="sBrokerLockBaseRAdjMarginR"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" onchange="f_UpdatePricing();" CssClass="rateField" preset="percent" ID="sBrokerLockBaseOptionArmTeaserR" ></asp:TextBox> </td>
                            <td class="FieldLabel"><asp:RadioButton runat="server"  CssClass="FieldLabel" ID="sBrokerLockPriceRowLockedT_Base"  GroupName="PricingLock"  onclick="f_PricingTableUpdateReadStatus(this);"/> Edit </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Total Hidden Adjustments</td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent"  ID="sBrokerLockTotHiddenAdjNoteIR" ReadOnly="true"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent"  ID="sBrokerLockTotHiddenAdjBrokComp1PcPrice" ReadOnly="true"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent"  ID="sBrokerLockTotHiddenAdjBrokComp1PcFee" ReadOnly="true"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent"  ID="sBrokerLockTotHiddenAdjRAdjMarginR" ReadOnly="true"></asp:TextBox> </td>
                            <td colspan="2"> <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sBrokerLockTotHiddenAdjOptionArmTeaserR" ReadOnly="true"></asp:TextBox> </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Originator Base Price</td>
                            <td><asp:TextBox runat="server" onchange="f_UpdatePricing();" CssClass="rateField"  preset="percent"  ID="sBrokerLockBrokerBaseNoteIR"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" onchange="f_PriceFeeConversion( this, 'sBrokerLockBrokerBaseBrokComp1PcFee','P', 'Pricing');f_UpdatePricing();" CssClass="rateField" preset="percent" ID="sBrokerLockBrokerBaseBrokComp1PcPrice"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" onchange="f_PriceFeeConversion( this, 'sBrokerLockBrokerBaseBrokComp1PcPrice','F', 'Pricing');f_UpdatePricing();" CssClass="rateField" preset="percent" ID="sBrokerLockBrokerBaseBrokComp1PcFee"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" onchange="f_UpdatePricing();" CssClass="rateField" preset="percent"  ID="sBrokerLockBrokerBaseRAdjMarginR"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" onchange="f_UpdatePricing();" CssClass="rateField" preset="percent"   ID="sBrokerLockBrokerBaseOptionArmTeaserR"></asp:TextBox> </td>
                            <td class="FieldLabel"><asp:RadioButton runat="server" CssClass="FieldLabel" ID="sBrokerLockPriceRowLockedT_BrokerBase"  GroupName="PricingLock"  onclick="f_PricingTableUpdateReadStatus(this);"/> Edit </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Total Visible Adjustments</td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sBrokerLockTotVisibleAdjNoteIR" ReadOnly="true"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sBrokerLockTotVisibleAdjBrokComp1PcPrice" ReadOnly="true"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sBrokerLockTotVisibleAdjBrokComp1PcFee" ReadOnly="true"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sBrokerLockTotVisibleAdjRAdjMarginR" ReadOnly="true"></asp:TextBox> </td>
                            <td colspan="2"><asp:TextBox runat="server" preset="percent" CssClass="rateField"   ID="sBrokerLockTotVisibleAdjOptionArmTeaserR" ReadOnly="true"></asp:TextBox> </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" id="finalPriceLabel">Originator Price</td> <%-- Final Price when the "Add lender paid originator compensation to fees" is unchecked --%>
                            <td><asp:TextBox runat="server" onchange="f_UpdatePricing();"   CssClass="rateField"  preset="percent" ID="sNoteIR"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" onchange="f_PriceFeeConversion( this, 'sBrokComp1Pc','P', 'Pricing');f_UpdatePricing();"   CssClass="rateField" preset="percent" ID="sBrokerLockFinalBrokComp1PcPrice"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" onchange="f_PriceFeeConversion( this, 'sBrokerLockFinalBrokComp1PcPrice','F', 'Pricing');f_UpdatePricing();"   CssClass="rateField" preset="percent" ID="sBrokComp1Pc"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" onchange="f_UpdatePricing();"   CssClass="rateField" preset="percent" ID="sRAdjMarginR"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" onchange="f_UpdatePricing();"   CssClass="rateField" preset="percent"  ID="sOptionArmTeaserR" ></asp:TextBox> </td>
                            <td class="FieldLabel"><asp:RadioButton runat="server" CssClass="FieldLabel" ID="sBrokerLockPriceRowLockedT_BrokerFinal"  GroupName="PricingLock" onclick="f_PricingTableUpdateReadStatus(this);"/> Edit </td>
                        </tr>  
                        <tr id="originatorAdjRow" style="display:none">
                            <td class="FieldLabel">Originator Comp Adjustments</td> <%-- Hidden when the "Add lender paid originator compensation to fees" is unchecked --%>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ReadOnly="true" ID="sBrokerLockOriginatorCompAdjNoteIR"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ReadOnly="true" ID="sBrokerLockOriginatorCompAdjBrokComp1PcPrice"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ReadOnly="true" ID="sBrokerLockOriginatorCompAdjBrokComp1PcFee"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ReadOnly="true" ID="sBrokerLockOriginatorCompAdjRAdjMarginR"></asp:TextBox> </td>
                            <td colspan="2"><asp:TextBox runat="server" CssClass="rateField" ReadOnly="true" preset="percent"  ID="sBrokerLockOriginatorCompAdjOptionArmTeaserR" ></asp:TextBox> </td>
                        </tr>
                        <tr id="originatorPriceRow" style="display:none">
                            <td class="FieldLabel" id="originatorPriceLabel">Final Price</td> <%-- Hidden when the "Add lender paid originator compensation to fees" is unchecked --%>
                            <td><asp:TextBox runat="server" onchange="f_UpdatePricing();"   CssClass="rateField"  preset="percent" ID="sBrokerLockOriginatorPriceNoteIR"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" onchange="f_PriceFeeConversion( this, 'sBrokerLockOriginatorPriceBrokComp1PcFee','P', 'Pricing');f_UpdatePricing();"   CssClass="rateField" preset="percent" ID="sBrokerLockOriginatorPriceBrokComp1PcPrice"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" onchange="f_PriceFeeConversion( this, 'sBrokerLockOriginatorPriceBrokComp1PcPrice','F', 'Pricing');f_UpdatePricing();"   CssClass="rateField" preset="percent" ID="sBrokerLockOriginatorPriceBrokComp1PcFee"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" onchange="f_UpdatePricing();"   CssClass="rateField" preset="percent" ID="sBrokerLockOriginatorPriceRAdjMarginR"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" onchange="f_UpdatePricing();"   CssClass="rateField" preset="percent"  ID="sBrokerLockOriginatorPriceOptionArmTeaserR" ></asp:TextBox> </td>
                            <td class="FieldLabel"><asp:RadioButton runat="server" CssClass="FieldLabel"  ID="sBrokerLockPriceRowLockedT_OriginatorPrice"  GroupName="PricingLock" onclick="f_PricingTableUpdateReadStatus(this);"/> Edit </td>
                        </tr>
                    </tbody>
                </table>
                </div>
                
                <div class="FieldLabel"><br />Adjustments</div>
                <table cellpadding="0" cellspacing="0" id="AdjustmentTable">
                    <thead>
                        <tr class="GridHeader">
                            <th><input type="checkbox" id="SelectAllAdjustments" /></th>
                            <th style="width:330px !important">Description</th>
                            <th style="width:56px">Rate</th>
                            <th style="width:56px">Price</th>
                            <th style="width:56px">Fee</th>
                            <th style="width:56px">Margin</th>
                            <th style="width:56px">Teaser<br />Rate</th>
                            <th style="width:70px">Hidden</th>
                            <th style="width:56px">Lender</th>
                            <th style="width:56px">Persist</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <input class="ButtonStyle" type="button" value="Add New Adjustment" id="AddNewAdjustment" onclick="AdjustmentTable.AddEmptyAdjustment();" />
                <input class="ButtonStyle"  type="button" value="Delete Selected Adjustments" id="DeleteSelectedAdjustments" disabled="disabled" />
     
        </fieldset>
                
                <fieldset>
                <table>
                    <tr>
                        <td class="FieldLabel">Originator Compensation</td>
                        <td class="FieldLabel">Amount</td>
                        <td class="FieldLabel" colspan="2">Net Points</td>
                    </tr>
                    <tr>
                        <td class="FieldLabel"> <ml:EncodedLabel runat="server" ID="sOriginatorCompensationPaymentSourceT"></ml:EncodedLabel> &nbsp;&nbsp;&nbsp;<a href="#" id="OrigCompLenderDetailsLink"  visible="false" runat="server" >details</a> </td>
                        <td><asp:TextBox runat="server" ID="sOriginatorCompensationAmount"></asp:TextBox></td>
                        <td><asp:TextBox runat="server" ID="sOriginatorCompNetPoints" preset="percent" class="rateField" ></asp:TextBox></td>
                        <td class="FieldLabel"><asp:CheckBox runat="server" ID="sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees" Visible="false" onclick="hideCompRows(this); f_UpdatePricing();"  Text="Add lender paid originator compensation to fees"/> </td>
                    </tr>
                </table>
                </fieldset>
          </div> <!-- End Rate Lock Info Section -->
        
        <table cellpadding="0" cellspacing="2" class="bottomMargin">
            <thead>
                <tr>
                    <th align="left">Internal Notes</th>
                    <th align="right"><input type="button" class="ButtonStyle"  value="Date &amp; Time Stamp" id="DateAndAmpTimeStamp" onclick="f_WriteDateAndTimeStamp()" /></th>
                    <th align="left" ><ml:EncodedLabel runat="server" ID="MessageToLenderHeader">Message to Lender</ml:EncodedLabel> </th>
                    <th align="right" >&nbsp;<input class="ButtonStyle" runat="server" id="MessageToLenderPrint" type="button"  value="Print..." onclick="f_viewBrokerNotes()" /></th>
                </tr>
            </thead>
            <tbody >
                <tr>
                    <td colspan="2">
                        <asp:TextBox TextMode="MultiLine" CssClass="bigNoteField" runat="server" ID="sTrNotes" ></asp:TextBox>
                    </td>
                    <td colspan="2">
                        <asp:TextBox TextMode="MultiLine" CssClass="bigNoteField" ReadOnly="true" runat="server" ID="sLpeNotesFromBrokerToUnderwriterHistory" ></asp:TextBox>
                    </td>
                </tr>
            </tbody>
        </table>

        <div>
            <div class="FieldLabel">Rate Lock History</div>
            <div id="RateLockHistory">
                <ml:PassthroughLiteral runat="server" ID="sRateLockHistoryXmlContent"></ml:PassthroughLiteral>
            </div>
        </div>

        <asp:Panel runat="server" ID="OriginatorCompLenderDetails" Visible="false" CssClass="modalbox">
        <table cellspacing="0">
            <tr>
                <td>
                    <table class="OriginatorCompensation_InnerInfoTable" cellspacing="0">
                        <tr>
                            <td class="FormTableHeader" colspan="4"> 
                              <ml:EncodedLiteral runat="server" ID="OriginatorPlanDetails"></ml:EncodedLiteral>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" colspan="2">
                                Originator compensation set
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="sOriginatorCompensationPlanAppliedD"  CssClass="dateField" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" colspan="2">
                                Effective date of compensation plan
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="sOriginatorCompensationEffectiveD" CssClass="dateField" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" colspan="3">
                                <asp:CheckBox id="sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo" runat="server" Enabled="false" Text="Compensation only paid for the 1st lien of a combo" />                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" colspan="3">
                            
                                <asp:TextBox runat="server" ID="sOriginatorCompensationPercent" CssClass="rateField" ReadOnly="true"></asp:TextBox>
                                of the
                                <asp:DropDownList runat="server" ID="sOriginatorCompensationBaseT" Enabled="false"></asp:DropDownList>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="sOriginatorCompensationAdjBaseAmount" CssClass="moneyField" ReadOnly="true"></asp:TextBox>
                            
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" style="width: 140px">
                             &nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" ID="sOriginatorCompensationHasMinAmount" Enabled="false" />
                                Minimum
                            </td>
                            <td>
                       
                             <asp:TextBox runat="server" ID="sOriginatorCompensationMinAmount" CssClass="moneyField" ReadOnly="true"></asp:TextBox>
                       
                            </td>
                            <td colspan="2" rowspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">
                              &nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" ID="sOriginatorCompensationHasMaxAmount" CssClass="moneyField" Enabled="false" />
                                Maximum
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="sOriginatorCompensationMaxAmount" CssClass="moneyField" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" colspan="3">
                                Fixed amount
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="sOriginatorCompensationFixedAmount" CssClass="moneyField" ReadOnly="true"></asp:TextBox>
                            
                                
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <hr style="height: 1px; color: black;" />
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" colspan="3" >
                                Total lender-paid compensation
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="sOriginatorCompensationTotalAmount" CssClass="moneyField" ReadOnly="true"></asp:TextBox>
                            
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <a href="#" id="CloseOrigDetails"  >[close]</a>
    </asp:Panel>
        <script type="text/javascript" >
     
            function _init(){
                var bIsOptionArm = document.getElementById('sIsOptionArm').value === 'True';
                AdjustmentTable.EnableDeleteEntries('SelectAllAdjustments','DeleteSelectedAdjustments');              
                AdjustmentTable.InstantiateTable('AdjustmentTable', PricingAdjustments, bIsOptionArm, false, true, true, true );
                UpdateRateLockStatus();
                
                var chb = $('#sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees'); 
                if( chb.length > 0  ){
                    hideCompRows(chb[0]);
                }
                
                $('#OrigCompLenderDetailsLink').click(function(e){
                    e.preventDefault();
                    Modal.ShowPopup('OriginatorCompLenderDetails', null, e);
                });
                
                $('#CloseOrigDetails').click(function(e){
                    e.preventDefault();
                    Modal.Hide();
                });
                
                checkLockStatus();
                $("#sFERateRenegotiationCountLckd, #sFECurrentLockExtensionCountLckd, #sFETotalLockExtensionCountLckd, #sFETotalRateReLockCountLckd").change(function(){checkLockStatus(); f_UpdatePricing();});
            }
            
            function checkLockStatus()
            {
                $("#sFERateRenegotiationCount").prop("readonly", !$("#sFERateRenegotiationCountLckd").prop("checked"));
                $("#sFECurrentLockExtensionCount").prop("readonly", !$("#sFECurrentLockExtensionCountLckd").prop("checked"));
                $("#sFETotalLockExtensionCount").prop("readonly", !$("#sFETotalLockExtensionCountLckd").prop("checked"));
                $("#sFETotalRateReLockCount").prop("readonly", !$("#sFETotalRateReLockCountLckd").prop("checked"));
                
            }
            
            function hideCompRows(chb) {
                var show = chb.checked;
                if( !show && $('#sBrokerLockPriceRowLockedT_OriginatorPrice').is(':checked') ) {
                    var $sBrokerLockPriceRowLockedT_Base = $('#sBrokerLockPriceRowLockedT_Base');
                    var sBrokerLockPriceRowLockedT_Base = $sBrokerLockPriceRowLockedT_Base.get(0);
                    // See OPM 116086 - If the rate is locked and the radios are disabled,
                    // make sure that this value still gets checked. Used plain js for this
                    // to avoid an issue with .attr()/.prop() if the jQuery version gets bumped.
                    var disabled = sBrokerLockPriceRowLockedT_Base.disabled;
                    if (disabled) {
                        sBrokerLockPriceRowLockedT_Base.checked = true;
                    }
                    else {
                        $sBrokerLockPriceRowLockedT_Base.click();
                    }
                }
                $('#originatorPriceRow,#originatorAdjRow').toggle(show);
                $('#finalPriceLabel').text( show ? 'Originator Price' : 'Final Price');
                $('#originatorPriceLabel').text( show ? 'Final Price' : 'Originator Price');
            }
            
            function f_runInternalPricing() {
              linkMe(<%= AspxTools.JsString(this.GetInternalPricerUrl()) %>, null, <%= AspxTools.JsBool(this.UseInternalPricerV2) %>);
            }
            function linkMe(href, extraArgs, useFullEditor) {
              var ch = href.indexOf('?') > 0 ? '&' : '?';
              var loanID = document.getElementById("loanid").value;
              parent.body.f_load(href + ch + "loanid=" + loanID + (extraArgs == null ? "" : "&" + extraArgs), useFullEditor);
            }            
            function f_removeRequestedRate(){
                if(!confirm(<%= AspxTools.JsString(JsMessages.RateLock_ConfirmClearRequestedRate)%>)){
                    return;
                }
            
                var args = new Object();
                args["loanid"] = <%=AspxTools.JsString(LoanID) %>;
                args["sFileVersion"] = getFileVersion();
                var result = gService.loanedit.call("RemoveRequestedRate", args);
      
                if( f_handleResponse(result) == false ) {
                    return;
                }
            
                if (result.value["HasError"] === "True"){
                    alert("ERROR: Unable to remove requested rate.\n\nMessage: " + result.value["ErrorMessage"]);      
                } 
                else{    
                    <%= AspxTools.JsGetElementById(sRateLockStatusT) %>.value = result.value["sRateLockStatusT"]; 
                    document.getElementById('ClearRateLockBtn').disabled = true;
                }
        }
 
        function getFileVersion() {
            return document.getElementById('sFileVersion').value;
        }
        
        function updateFileVersion(fileVersion){
            document.getElementById('sFileVersion').value = fileVersion;
        }
        
        function f_handleResponse(response) {
        
            if( response.error ) {
              
                if( response.ErrorType == "VersionMismatchException" ) {
                    f_displayVersionMismatch();
                }
                else if( response.ErrorType == "LoanFieldWritePermissionDenied" ) {
                     f_displayFieldWriteDenied(response.UserMessage);
                }
                else {
                    alert(response.UserMessage);
                }
                return false;
            }
            else {
                updateFileVersion(response.value["sFileVersion"]);
                return true;
            }
        }
        
        function f_lockRateBtn_onClick() {
            var obj = document.getElementById('sRateLockStatusT');
            
            if (obj.value === 'Locked' || obj.value === 'Lock Suspended') {
                f_breakRateLock();
            }
            else if (obj.value === 'Not Locked' || obj.value === 'Lock Requested') {
                f_lockRate();
            }
        }
        
        function f_suspendLockBtn_onClick(){
            var obj = document.getElementById('sRateLockStatusT');
            
            if(obj.value === 'Lock Suspended' ){
                f_resumeLock();
            }
            else if(obj.value === 'Locked'){
                f_suspendLock();
            }
        }

            function f_suspendLock() {
                var modalArg = new Object();
                modalArg.sRLckdDays = document.getElementById('sRLckdDays').value;
        
                showModal('/newlos/Underwriting/SuspendLock.aspx', modalArg, null, null, function(wargs){
                    var reason = "";
                    if (wargs.OK) {
                        reason = wargs.tbReason;
                    }
                    else{
                        return;
                    }
                    
                    if (!confirm(<%= AspxTools.JsString(JsMessages.RateLock_ConfirmSuspendLock) %>)) {
                        return; 
                    } 
                    
                    var data = {};
                    data["loanid"] = $( '#loanid' ).val();
                    data["Reason"] = reason;
                    data["sFileVersion"] = getFileVersion();
                    
                    
                    var result = gService.loanedit.call('SuspendLock', data);
                    
                    if( f_handleResponse(result) == false ){
                        return;
                    }
                    
                    if (result.value.HasError === 'True'){
                        alert( result.value.ErrorMessage );
                    }
                    if (result.value.HasWarning === 'True'){ 
                        alert( result.value.WarningMessage );
                    }
                    
                    $("#sFECurrentLockExtensionCount").val(result.value.sFECurrentLockExtensionCount);
                    $("#sFERateRenegotiationCount").val(result.value.sFERateRenegotiationCount);
                    $("#sFETotalLockExtensionCount").val(result.value.sFETotalLockExtensionCount);
                    $("#sFETotalRateReLockCount").val(result.value.sFETotalRateReLockCount);
                   
                    document.getElementById('<%= AspxTools.ClientId(sIsRateLocked) %>').value = result.value.sIsRateLocked;
                    document.getElementById('<%= AspxTools.ClientId(sRLckdD) %>').value = result.value.sRLckdD;
                    document.getElementById('<%= AspxTools.ClientId(sRLckdDTime) %>').value = result.value.sRLckdDTime;
                    document.getElementById('<%= AspxTools.ClientId(sRLckdExpiredD) %>').value = result.value.sRLckdExpiredD;
                    document.getElementById('<%= AspxTools.ClientId(sStatusT) %>').value = result.value.sStatusT;
                    document.getElementById('<%= AspxTools.ClientId(sRateLockStatusT) %>').value = result.value.sRateLockStatusT;
                    document.getElementById('RateLockHistory').innerHTML = result.value.RateLockHistory;
                    aReasonsNotAllow = JSON.parse(result.value.sReasonForNotAllowingResubmission);
                    UpdateRateLockStatus();
                    f_refreshLoanSummary();
                },{hideCloseButton: true});
            }

          function f_resumeLock() {
            var modalArg = new Object();
            modalArg.sRLckdDays = <%= AspxTools.JsGetElementById(sRLckdDays) %>.value;
        
            showModal('/newlos/Underwriting/ResumeLock.aspx?loanid=' + $('#loanid').val(), modalArg, null, null, function(wargs){
                if (!wargs.OK) { return; }
                
                if ( false == f_saveMe() ) {
                    return;
                }
                var args = new Object();
              
                args["loanid"] = $('#loanid').val();
                args["Reason"] = wargs.tbReason; 
                    
                args["sRLckdD"] = $('#<%= AspxTools.ClientId(sRLckdD) %>').val();
                args["sRLckdExpiredD"] = $('#<%= AspxTools.ClientId(sRLckdExpiredD) %>').val();  
                args[<%=AspxTools.JsGetClientIdString(sRLckdDays)%>] = $('#<%= AspxTools.ClientId(sRLckdDays) %>').val();
    
                args[<%=AspxTools.JsGetClientIdString(sNoteIR)%>]          = $('#<%= AspxTools.ClientId(sNoteIR) %>').val();
                args[<%=AspxTools.JsGetClientIdString(sBrokComp1Pc)%>]      = $('#<%= AspxTools.ClientId(sBrokComp1Pc) %>').val();
                args[<%=AspxTools.JsGetClientIdString(sRAdjMarginR)%>]      = $('#<%= AspxTools.ClientId(sRAdjMarginR) %>').val();
                
                var inAdditionTo = $('#sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees');
                if( inAdditionTo.length != 0 ) {
                    args.sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees = inAdditionTo[0].checked;
                }
                
                var pricing = GetLockedRow();
                
                for(var attr in pricing){
                    if(pricing.hasOwnProperty(attr)){
                        args[attr] = pricing[attr];
                    }
                }
                    
                args["sFileVersion"] = getFileVersion();
            
                var result = gService.loanedit.call("ResumeLock", args);
                
                if(result.value["HasDuplicateError"] == "True")
                {
                    f_VerifyOKToLockInspiteOfDuplicate(result.value["Duplicates"], function(itsOKToLockInspiteOfDuplicate){
                        if(itsOKToLockInspiteOfDuplicate)
                        {
                            args["SkipLoanDuplicationCheck"] = true;
                            result = gService.loanedit.call("ResumeLock", args);
                            f_ResumeLockCallback(result, wargs);
                        }
                        else{
                            return;
                        }
                    });
                }
                else {
                    f_ResumeLockCallback(result, wargs);
                }

            },{hideCloseButton: true});
        }
        
            function f_ResumeLockCallback(result, wargs){
                if( f_handleResponse(result) == false) {
                    return;
                }
                
                if (result.value["HasError"] == "True") {
                    alert("ERROR: Unable to lock rate.\n\nMessage: " + result.value["ErrorMessage"]);
                }
                 
                else {  
                    if (result.value["HasWarning"] == "True") {
                        alert("WARNING: " + result.value["WarningMessage"]);
                    }
                
                    document.getElementById("sIsRateLocked").value = result.value["sIsRateLocked"];
                    <%= AspxTools.JsGetElementById(sRLckdD) %>.value = result.value["sRLckdD"];
                    <%= AspxTools.JsGetElementById(sRLckdDTime) %>.value = result.value["sRLckdDTime"];
                    <%= AspxTools.JsGetElementById(sRLckdExpiredD) %>.value = result.value["sRLckdExpiredD"];       
                    <%= AspxTools.JsGetElementById(sStatusT) %>.value = result.value["sStatusT"];     
                    <%= AspxTools.JsGetElementById(sRateLockStatusT) %>.value = result.value["sRateLockStatusT"];
                    document.getElementById(<%=AspxTools.JsGetClientIdString(sRLckdDays)%>).value = result.value['sRLckdDays'];   
                    
                    $("#sFECurrentLockExtensionCount").val(result.value.sFECurrentLockExtensionCount);
                    $("#sFERateRenegotiationCount").val(result.value.sFERateRenegotiationCount);
                    $("#sFETotalLockExtensionCount").val(result.value.sFETotalLockExtensionCount);
                    $("#sFETotalRateReLockCount").val(result.value.sFETotalRateReLockCount);
                    
                    document.getElementById("RateLockHistory").innerHTML = result.value["RateLockHistory"]; 
                     
                     if ( wargs.emailCert ) 
                        f_emailRateLockConf();
                        
                    aReasonsNotAllow = JSON.parse(result.value["sReasonForNotAllowingResubmission"]);
                    UpdateRateLockStatus();
                    f_refreshLoanSummary();
                    }
            }
        
            function f_PriceFeeConversion(field, oppositeFieldId, type, conversion){
            
                var otherField = document.getElementById(oppositeFieldId);
                
                var data = {
                    'type' : type,
                    'value' : field.value, 
                    'conversion' : conversion
                };
                
                var results = gService.loanedit.call('PriceFeeConversion',data); 
                
                if(results.error){
                    alert('Error calculation P/F');
                    return;
                }
                
                otherField.value = results.value.result; 
                field.value = results.value.value;
            }
            
            function f_updateAdjustmentPriceFee(sourceId,destinationId,sourceType){
                var source = document.getElementById(sourceId);
                f_PriceFeeConversion(source, destinationId, sourceType, 'Adjustment');
                
            }
            
            function f_adjustmentChange(update){
                if(update !== 'description'){
                    f_UpdatePricing();
                }
            }

            function f_allowSelectNewProgram(){
                if ( typeof(f_prepareLoanForResubmission) === "function" ) {
                    f_prepareLoanForResubmission();
                } 
            }
        
            function f_emailRateLockConf() {
                var rateLockStatus = document.getElementById('<%= AspxTools.ClientId(sRateLockStatusT) %>');
                var bIsRateLocked = rateLockStatus.value === 'Locked';
                var bIsSuspended = rateLockStatus.value === "Lock Suspended";

                var bDisplayWindow = true;
                if (!bIsRateLocked) {
                    bDisplayWindow = confirm('The loan\'s rate is not locked. Are you sure you want to continue?');
                }

                if (bIsSuspended) {
                    alert('Lock confirmation cannot be generated when the lock is suspended.');
                    bDisplayWindow = false;
                }

                if (!bDisplayWindow) {
                    return;
                }

                if (isDirty()) {
                    LQBPopup.ShowElement($('#ConfSaveWarn'), { 'hideCloseButton': true, 'height': 50, 'width': 300 });
                } else {
                    generateRateLockConf();
                }
            }

            function confSaveWarn_Yes_click() {
                LQBPopup.Hide();
                f_saveMe();
                generateRateLockConf();
            }

            function confSaveWarn_No_click() {
                LQBPopup.Hide();
            }

            function generateRateLockConf() {
                var body_url = <%= AspxTools.JsString(VirtualRoot) %> + '/newlos/Underwriting/RateLockConfirmation_Frame.aspx?loanid=' + $( '#loanid' ).val();
                var win = window.open(body_url, "", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=780,height=600");
      
                 win.focus();
                 
                var args = new Object();
                args["loanid"] = <%=AspxTools.JsString(LoanID) %>;                
                var result = gService.loanedit.call("SetLockDesk", args);
                if(result.error){
                    alert(result.UserMessage);
                    return;
                }
            }
        
            function f_validateNumber(obj){
                var re = /^\d+$/
                if(!re.test(obj.value)){
                    obj.value = '';
                }
            }

            function f_WriteDateAndTimeStamp(){
                var user = <%= AspxTools.JsString(BrokerUser.FirstName + " " + BrokerUser.LastName) %>;
                
                var timeStamp = getTimeStamp(user);

                var notes = <%= AspxTools.JsGetElementById(sTrNotes) %>;

                notes.value = timeStamp + notes.value;

                updateDirtyBit();
            }

            function f_viewBrokerNotes() {
                //var body_url = <%= AspxTools.JsString(VirtualRoot) %> + '/newlos/underwriting/BrokerToUnderwritingNotes.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %>;
                var body_url = <%=AspxTools.JsNumeric(E_UrlOption.BrokerToUnderwritingNotes) %>;
                var params = encodeURIComponent("loanid=<%=AspxTools.HtmlString(LoanID)%>");
                window.open(<%= AspxTools.JsString(VirtualRoot) %> + '/common/PrintView_Frame.aspx?body_url=' + body_url + '&menu_param=print&params=' + params, '', 'toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=650px');
                return false;
            }

            function f_OpenLinkedLoan(){
                var item = $('#<%= AspxTools.ClientId( LinkedLoanId )%>');
                if ( item.length > 0  && item.val().length > 0 ) {
                    f_openLoan( item.val() );
                }
            }
            function f_PricingTableUpdateReadStatus(field){
                    var bIsOptionArm = document.getElementById("<%= AspxTools.ClientId(sIsOptionArm)%>").value === "True";
                    
                    var basePriceFieldsIds      = [ 'sBrokerLockBaseNoteIR', 'sBrokerLockBaseBrokComp1PcPrice', 'sBrokerLockBaseBrokComp1PcFee', 'sBrokerLockBaseRAdjMarginR' ];
                    var brokerBasePriceFieldIds = [ 'sBrokerLockBrokerBaseNoteIR', 'sBrokerLockBrokerBaseBrokComp1PcPrice', 'sBrokerLockBrokerBaseBrokComp1PcFee', 'sBrokerLockBrokerBaseRAdjMarginR']
                    var finalPriceFieldIds      = [ 'sNoteIR', 'sBrokerLockFinalBrokComp1PcPrice', 'sBrokerLockFinalBrokComp1PcPrice', 'sBrokComp1Pc', 'sRAdjMarginR'];
                    var originatorFieldIds      = [ 'sBrokerLockOriginatorPriceNoteIR', 'sBrokerLockOriginatorPriceBrokComp1PcFee', 'sBrokerLockOriginatorPriceBrokComp1PcPrice', 'sBrokerLockOriginatorPriceRAdjMarginR' ] ;
                    if(bIsOptionArm){
                        basePriceFieldsIds.push('sBrokerLockBaseOptionArmTeaserR');
                        brokerBasePriceFieldIds.push('sBrokerLockBrokerBaseOptionArmTeaserR');
                        finalPriceFieldIds.push('sOptionArmTeaserR');
                        originatorFieldIds.push('sBrokerLockOriginatorPriceOptionArmTeaserR' );
                    }
                    
                    f_UpdateReadOnlyBit(basePriceFieldsIds, true);
                    f_UpdateReadOnlyBit(brokerBasePriceFieldIds, true);
                    f_UpdateReadOnlyBit(finalPriceFieldIds, true);
                    f_UpdateReadOnlyBit(originatorFieldIds, true);
                    
                    if(field.id === 'sBrokerLockPriceRowLockedT_Base' ) { 
                        f_UpdateReadOnlyBit(basePriceFieldsIds, false);
                    }
                    else if(field.id === 'sBrokerLockPriceRowLockedT_BrokerBase' ) { 
                        f_UpdateReadOnlyBit(brokerBasePriceFieldIds, false);
                    }
                    else if ( field.id === 'sBrokerLockPriceRowLockedT_OriginatorPrice' ) {
                        f_UpdateReadOnlyBit(originatorFieldIds, false);
                    }
                    else{
                      f_UpdateReadOnlyBit(finalPriceFieldIds, false);
                    }
            }
        
            function f_UpdateReadOnlyBit(fieldIds, isReadOnly){
                var g = function(id){ return document.getElementById(id);}; 

                for(var i = 0;i < fieldIds.length; i++){
                    g(fieldIds[i]).readOnly = isReadOnly; 
                }
            }
            
       
            
            function f_UpdatePricing() {
                var data = {};
                data.LoanId = $( '#loanid' ).val();
                data.PricingAdjustments = AdjustmentTable.Serialize(); 
                
                var pricing = GetLockedRow();
                
                for(var attr in pricing){
                    if(pricing.hasOwnProperty(attr)){                
                        data[attr] = pricing[attr];
                    }
                }
                
                var inAdditionTo = $('#sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees');
                if( inAdditionTo.length != 0 ) {
                    data.sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees = inAdditionTo[0].checked;
                } 
                
                $("#CountTable input").each(function(){
                    var elem = $(this);
                    if(elem.attr("type") == "checkbox")
                    {
                        data[elem.attr("id")] = elem.prop("checked");
                    }
                    else
                    {
                        data[elem.attr("id")] = elem.val();
                    }
                });
                
                var results = gService.loanedit.call('CalculateData', data);
                
                if(results.error){
                    alert(results.UserMessage);
                    return;
                }
                
                for(var attr in results.value){
                    if(results.value.hasOwnProperty(attr)){
                        var element = document.getElementById(attr); 
                        element.value = results.value[attr]; 
                    }
                }
            }
            function f_openAuditItem(eventId){
                var body_url = <%= AspxTools.JsString(VirtualRoot) %> + '/newlos/LockDesk/RateLockEventInfo.aspx?loanid=' + $( '#loanid' ).val() + '&eventid='+eventId
                 var win = window.open(body_url, "", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=600,height=620");
      
                 win.focus();  
                 return false;
            }
            
            function GetLockedRow() {
                var g = function(id){ return document.getElementById(id);};
                var data = {};
                  switch($('input[name=PricingLock]:checked').get(0).id) {
                    case 'sBrokerLockPriceRowLockedT_Base':
                        data.Rate = g('sBrokerLockBaseNoteIR').value;
                        data.Price = g('sBrokerLockBaseBrokComp1PcPrice').value;
                        data.Fee = g('sBrokerLockBaseBrokComp1PcFee').value;
                        data.Margin = g('sBrokerLockBaseRAdjMarginR').value;
                        data.TRate = g('sBrokerLockBaseOptionArmTeaserR').value;
                        data.Lock = 'BasePrice';
                        break;
                    case  'sBrokerLockPriceRowLockedT_BrokerBase':
                        data.Rate = g('sBrokerLockBrokerBaseNoteIR').value;
                        data.Price = g('sBrokerLockBrokerBaseBrokComp1PcPrice').value;
                        data.Fee = g('sBrokerLockBrokerBaseBrokComp1PcFee').value;
                        data.Margin = g('sBrokerLockBrokerBaseRAdjMarginR').value;
                        data.TRate = g('sBrokerLockBrokerBaseOptionArmTeaserR').value;
                        data.Lock = 'BrokerBasePrice';
                        break;
                    case 'sBrokerLockPriceRowLockedT_BrokerFinal':
                        data.Rate = g('sNoteIR').value;
                        data.Price = g('sBrokerLockFinalBrokComp1PcPrice').value;
                        data.Fee = g('sBrokComp1Pc').value;
                        data.Margin = g('sRAdjMarginR').value;
                        data.TRate = g('sOptionArmTeaserR').value;
                        data.Lock = 'BrokerFinalPrice';
                        break;
                     case 'sBrokerLockPriceRowLockedT_OriginatorPrice':
                        data.Rate = g('sBrokerLockOriginatorPriceNoteIR').value; 
                        data.Price = g('sBrokerLockOriginatorPriceBrokComp1PcPrice').value; 
                        data.Fee = g('sBrokerLockOriginatorPriceBrokComp1PcFee').value; 
                        data.Margin = g('sBrokerLockOriginatorPriceRAdjMarginR').value; 
                        data.TRate = g('sBrokerLockOriginatorPriceOptionArmTeaserR').value; 
                        data.Lock = 'OriginatorPrice'; 
                        break; 
                }
                
                return data;
            }
        
            function f_save() {
                f_saveMe();
                return true;
            }
            
            function saveMe() {
                f_saveMe();
                return true;
            }
            function f_saveMe() {
                var g = function(id){ return document.getElementById(id);};
                
                var data = {};
                
                data.LoanId             = g( 'loanid' ).value;
                data.PricingAdjustments = AdjustmentTable.Serialize(); 
                data.sLpTemplateNm      = g('sLpTemplateNm').value;
                data.sRLckdN            = g('sRLckdN').value;  
                data.sRLckdExpiredN     = g('sRLckdExpiredN').value;
                data.sSubmitD           = g('sSubmitD').value;
                data.sTrNotes           = g('sTrNotes').value;
                data.sSubmitN           = g('sSubmitN').value;
                data.sRLckdDays         = g('sRLckdDays').value;
                data.sFileVersion       = getFileVersion();
                
                $("#CountTable input").each(function(){
                    var elem = $(this);
                    if(elem.attr("type") == "checkbox")
                    {
                        data[elem.attr("id")] = elem.prop("checked");
                    }
                    else
                    {
                        data[elem.attr("id")] = elem.val();
                    }
                });
                
                
                var inAdditionTo = $('#sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees');
                
                if( inAdditionTo.length != 0 ) {
                    data.sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees = inAdditionTo[0].checked;
                } 
                
                var pricing = GetLockedRow();
                
                for(var attr in pricing){
                    if(pricing.hasOwnProperty(attr)){
                        data[attr] = pricing[attr];
                    }
                }
                
                var result = gService.loanedit.call('SaveData', data);
                var answer = f_handleResponse(result) ;
                if (  answer) {
                    clearDirty();
                }       
                
                return answer;
            }
        
            function f_breakRateLock() {
                var modalArg = new Object();
                modalArg.sRLckdDays = document.getElementById('sRLckdDays').value;
        
                showModal('/newlos/Underwriting/BreakRateLock.aspx', modalArg, null, null, function(wargs){

                    var reason = "";
                    if (wargs.OK) {
                        reason = wargs.tbReason;
                    }
                    else{
                        return;
                    }
                    
                    if (!confirm(<%= AspxTools.JsString(JsMessages.RateLock_ConfirmBreakRateLock) %>)) {
                        return; 
                    } 
                    
                    var data = {};
                    data["loanid"] = $( '#loanid' ).val();
                    data["Reason"] = reason;
                    data["sFileVersion"] = getFileVersion();
                    
                    
                    var result = gService.loanedit.call('BreakRateLock', data);
                    
                    if( f_handleResponse(result) == false ){
                        return;
                    }
                    
                    if (result.value.HasError === 'True'){
                        alert( result.value.ErrorMessage );
                    }
                    if (result.value.HasWarning === 'True'){ 
                        alert( result.value.WarningMessage );
                    }
                    
                    $("#sFECurrentLockExtensionCount").val(result.value.sFECurrentLockExtensionCount);
                    $("#sFERateRenegotiationCount").val(result.value.sFERateRenegotiationCount);
                    $("#sFETotalLockExtensionCount").val(result.value.sFETotalLockExtensionCount);
                    $("#sFETotalRateReLockCount").val(result.value.sFETotalRateReLockCount);
                   
                    document.getElementById('<%= AspxTools.ClientId(sIsRateLocked) %>').value = result.value.sIsRateLocked;
                    document.getElementById('<%= AspxTools.ClientId(sRLckdD) %>').value = result.value.sRLckdD;
                    document.getElementById('<%= AspxTools.ClientId(sRLckdDTime) %>').value = result.value.sRLckdDTime;
                    document.getElementById('<%= AspxTools.ClientId(sRLckdExpiredD) %>').value = result.value.sRLckdExpiredD;
                    document.getElementById('<%= AspxTools.ClientId(sStatusT) %>').value = result.value.sStatusT;
                    document.getElementById('<%= AspxTools.ClientId(sRateLockStatusT) %>').value = result.value.sRateLockStatusT;
                    document.getElementById('RateLockHistory').innerHTML = result.value.RateLockHistory;
                    aReasonsNotAllow = JSON.parse(result.value.sReasonForNotAllowingResubmission);
                    UpdateRateLockStatus();
                    f_refreshLoanSummary();
                },{hideCloseButton: true});
                
            }
        
            function UpdateRateLockStatus() {
                var g = function(id){ return document.getElementById(id);};
                
                var bIsRateLocked   = g("sIsRateLocked").value === "True";
                var bIsReadOnly     = g("_ReadOnly").value === "True"; 
                var status          = g("sRateLockStatusT").value;
                
                var bIsRequested    = status === "Lock Requested";
                var bIsReallyLocked = status === "Locked";
                var bIsSuspended    = status === "Lock Suspended";
                var bIsNotLocked    = status === "Not Locked";
                var bIsOptionArm    = g("sIsOptionArm").value === "True";
                
                parent.frames['body'].ML.sRateLockStatusT = status;
                
                g('sBrokerLockBaseNoteIR').readOnly                 = bIsRateLocked;
                g('sBrokerLockBaseBrokComp1PcPrice').readOnly       = bIsRateLocked;
                g('sBrokerLockBaseBrokComp1PcFee').readOnly         = bIsRateLocked;
                g('sBrokerLockBaseRAdjMarginR').readOnly            = bIsRateLocked;
                g('sBrokerLockBrokerBaseNoteIR').readOnly           = bIsRateLocked;
                
                g('sBrokerLockBrokerBaseBrokComp1PcPrice').readOnly = bIsRateLocked;
                g('sBrokerLockBrokerBaseBrokComp1PcFee').readOnly   = bIsRateLocked;
                g('sBrokerLockBrokerBaseRAdjMarginR').readOnly      = bIsRateLocked;
                
                g('sNoteIR').readOnly                               = bIsRateLocked;
                g('sBrokComp1Pc').readOnly                          = bIsRateLocked;
                g('sRAdjMarginR').readOnly                          = bIsRateLocked;
                g('sBrokerLockFinalBrokComp1PcPrice').readOnly      = bIsRateLocked;
                
                
                g('sBrokerLockBaseOptionArmTeaserR').readOnly            = bIsRateLocked || !bIsOptionArm;
                g('sBrokerLockBrokerBaseOptionArmTeaserR').readOnly      = bIsRateLocked || !bIsOptionArm;
                g('sOptionArmTeaserR').readOnly                          = bIsRateLocked || !bIsOptionArm;
                g('sBrokerLockOriginatorPriceOptionArmTeaserR').readOnly = bIsRateLocked || !bIsOptionArm;
                

                g('sRLckdDays').readOnly                            = bIsRateLocked || bIsSuspended; 
                
                g('sBrokerLockOriginatorPriceNoteIR').readOnly = bIsRateLocked; 
                g('sBrokerLockOriginatorPriceBrokComp1PcPrice').readOnly = bIsRateLocked;
                g('sBrokerLockOriginatorPriceBrokComp1PcFee').readOnly = bIsRateLocked;
                g('sBrokerLockOriginatorPriceRAdjMarginR').readOnly = bIsRateLocked;

                $('#sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees').prop('disabled', bIsRateLocked);
                
                var lockBtn = g("LockRateBtn");
                var clear = g("ClearRateLockBtn");
                var suspendBtn = g("SuspendLockBtn");
                var generateCertBtn = g("EmailRateLockConf");
                
                if ( bIsNotLocked || bIsRequested )
                {
                    suspendBtn.style.visibility = 'hidden';
                }
                else
                {
                    suspendBtn.style.visibility = 'visible';
                    suspendBtn.value = ( bIsReallyLocked ) ? 'Suspend Rate Lock...' : 'Resume Lock with Modifications...';
                }
                
                lockBtn.disabled = bIsReadOnly;
                if(bIsReallyLocked){
                    lockBtn.value = 'Break Rate Lock...';
                }else{
                    lockBtn.value = 'Lock Rate...';
                    lockBtn.disabled = bIsReadOnly || bIsSuspended;
                }
                clear.disabled = !bIsRequested || bIsReadOnly ;
                
                generateCertBtn.disabled = bIsSuspended || bIsReadOnly;
                
                g("ExtendRateLock").disabled                         =  !bIsRateLocked || bIsReadOnly  || bIsSuspended;
                g('sSubmitD').readOnly                               = bIsRateLocked;
                //hide the calendar img when the page is readonly
                g('sSubmitD').nextSibling.style.visibility           = g('sSubmitD').readOnly ? 'hidden' : 'visible'; 
                g('sBrokerLockPriceRowLockedT_BrokerFinal').disabled = bIsRateLocked;
                g('sBrokerLockPriceRowLockedT_Base'       ).disabled = bIsRateLocked;
                g('sBrokerLockPriceRowLockedT_BrokerBase' ).disabled = bIsRateLocked;
                g('sBrokerLockPriceRowLockedT_OriginatorPrice').disabled= bIsRateLocked;
                g('sOriginatorCompensationAmount').disabled = bIsRateLocked;
                g('sOriginatorCompNetPoints').disabled = bIsRateLocked;
                
                //make sure the pricing table is enabled correctly, dont touch when the page is in readonly
                if(!bIsRateLocked && !bIsReadOnly){
                    $('input[name=PricingLock]:checked').triggerHandler('click');
                }
                if(!!g('AllowSelectNewProgram')){
                    g('AllowSelectNewProgram').disabled = bIsRateLocked || aReasonsNotAllow.length == 0 || bIsReadOnly;
                }
                g('AddNewAdjustment').disabled = bIsRateLocked || bIsReadOnly;
        
                AdjustmentTable.SetReadOnly(bIsRateLocked || bIsReadOnly);
          }
          
        
          function f_ExtendLockPeriod() {
            var modalArg = new Object();
            var g = function(id){ return document.getElementById(id);};
            
            modalArg.sRLckdExpiredD = g('sRLckdExpiredD').value;
            modalArg.sNoteIR = g('sNoteIR').value; 
            modalArg.sBrokerLockFinalBrokComp1PcPrice = g('sBrokerLockFinalBrokComp1PcPrice').value;       
            modalArg.sBrokComp1Pc = g('sBrokComp1Pc').value;
            modalArg.sRAdjMarginR = g('sRAdjMarginR').value;
            modalArg.sOptionArmTeaserR = g('sOptionArmTeaserR').value;
            modalArg.slId = g('loanid').value;
            modalArg.bIsOptionArm = g('sIsOptionArm').value === 'True';
            
            showModal('/newlos/Lockdesk/ExtendRateLock.aspx?loanid=' + modalArg.slId , modalArg, null, null, function(wargs){

                if(typeof(wargs.Status) === 'undefined'){
                    return;
                }
                if(wargs.Status !== 'OK'){
                    return;
                }
                
                var args = {};        
                args.sLId = wargs.sLId;
                args.Reason = wargs.Reason;
                args.sRLckdDays = wargs.sRLckdDays;
                args.IsAdjustPrice = wargs.IsAdjustPrice;
            args.calendarDays = wargs.calendarDays;
                
                args.AdjustmentRate = wargs.AdjustmentRate;
                args.AdjustmentPrice = wargs.AdjustmentPrice;
                args.AdjustmentFee = wargs.AdjustmentFee; 
                args.AdjustmentMargin = wargs.AdjustmentMargin;
                args.AdjustmentTRate = wargs.AdjustmentTRate;
                args.AdjustmentDesc = wargs.AdjustmentDesc;
                args.sFileVersion = getFileVersion();
                
                var result = gService.loanedit.call("ExtendRateLock", args);
                if( f_handleResponse(result) == false) {
                    return false;
                }
    
                if (result.value.HasError === "True"){
                    alert("ERROR: Unable to extend lock period.\n\nMessage: " + result.value["ErrorMessage"]);
                    return;      
                }
                    
                for(var attr in result.value){
                    if (result.value.hasOwnProperty(attr)) {
                        if(attr === 'RateLockHistory'){
                            g('RateLockHistory').innerHTML = result.value["RateLockHistory"]; 
                            continue;
                        }
                        g(attr).value = result.value[attr];
                    }
                }
                
                if(args.IsAdjustPrice){
                    var data = {};
                    data.Description     = wargs.AdjustmentDesc;
                    data.Rate            = wargs.AdjustmentRate;
                    data.Price           = wargs.AdjustmentPrice;
                    data.Fee             = wargs.AdjustmentFee;
                    data.Margin          = wargs.AdjustmentMargin;
                    data.TeaserRate      = wargs.AdjustmentTRate;
                    
                    data.Hidden          = false;
                    data.IsLenderAdjustment = true;
                    data.IsSRPAdjustment = false;
                    data.IsPersist = true;
                    
                    AdjustmentTable.AddAdjustment(data, true); 
                }
            },{hideCloseButton: true});
          }
          
          function f_VerifyOKToLockInspiteOfDuplicate(duplicates, callback)
          {
              setTimeout(function(){
                  var modalArg = new Object();
                  modalArg.duplicates = JSON.parse(duplicates);
                  showModal('/newlos/LockDesk/VerifyOKToLockInspiteOfDuplicate.aspx?loanid=' + $('#loanid').val(), modalArg, null, null, function(wargs){
                      callback(wargs.OK);
                  },{hideCloseButton: true});}, 0);
          } 
      
          function f_lockRate() {
            var modalArg = new Object();
            modalArg.sRLckdDays = <%= AspxTools.JsGetElementById(sRLckdDays) %>.value;
        
            showModal('/newlos/Underwriting/RateLockConfirmation.aspx?loanid=' + $('#loanid').val(), modalArg, null, null, function(wargs){                 
                if (!wargs.OK) { return; }
                
                if ( false == f_saveMe() ) {
                    return;
                }
                var args = new Object();
              
                args["loanid"] = $('#loanid').val();
                args["Reason"] = wargs.tbReason; 
                args["sRLckdD"] = wargs.sRLckdD;
                args["sRLckdExpiredD"] = wargs.sRLckExpiration;   
                args[<%=AspxTools.JsGetClientIdString(sRLckdDays)%>]        = wargs.sRLckdDays;
                    
                args[<%=AspxTools.JsGetClientIdString(sNoteIR)%>]          = $('#<%= AspxTools.ClientId(sNoteIR) %>').val();
                args[<%=AspxTools.JsGetClientIdString(sBrokComp1Pc)%>]      = $('#<%= AspxTools.ClientId(sBrokComp1Pc) %>').val();
                args[<%=AspxTools.JsGetClientIdString(sRAdjMarginR)%>]      = $('#<%= AspxTools.ClientId(sRAdjMarginR) %>').val();
                
                var inAdditionTo = $('#sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees');
                if( inAdditionTo.length != 0 ) {
                    args.sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees = inAdditionTo[0].checked;
                }
                
                var pricing = GetLockedRow();
                
                for(var attr in pricing){
                    if(pricing.hasOwnProperty(attr)){
                        args[attr] = pricing[attr];
                    }
                }
                    
                args["sFileVersion"] = getFileVersion();
            
                var result = gService.loanedit.call("LockRate", args);
                
                if(result.value["HasDuplicateError"] == "True")
                {
                    f_VerifyOKToLockInspiteOfDuplicate(result.value["Duplicates"], function (itsOKToLockInspiteOfDuplicate){
                        if(itsOKToLockInspiteOfDuplicate)
                        {
                            args["SkipLoanDuplicationCheck"] = true;
                            result = gService.loanedit.call("LockRate", args);
                            f_LockResponseCallback(result, wargs);
                        }
                        else{
                            return;
                        }
                    });
                }
                else {
                    f_LockResponseCallback(result, wargs);
                }
            },{hideCloseButton: true});
          }

            function f_LockResponseCallback(result, wargs)
            {
                if( f_handleResponse(result) == false) {
                    return;
                }
                
                if (result.value["HasError"] == "True") {
                    alert("ERROR: Unable to lock rate.\n\nMessage: " + result.value["ErrorMessage"]);
                }
                 
                else {  
                    if (result.value["HasWarning"] == "True") {
                        alert("WARNING: " + result.value["WarningMessage"]);
                    }
                
                    document.getElementById("sIsRateLocked").value = result.value["sIsRateLocked"];
                    <%= AspxTools.JsGetElementById(sRLckdD) %>.value = result.value["sRLckdD"];
                    <%= AspxTools.JsGetElementById(sRLckdDTime) %>.value = result.value["sRLckdDTime"];
                    <%= AspxTools.JsGetElementById(sRLckdExpiredD) %>.value = result.value["sRLckdExpiredD"];       
                    <%= AspxTools.JsGetElementById(sStatusT) %>.value = result.value["sStatusT"];     
                    <%= AspxTools.JsGetElementById(sRateLockStatusT) %>.value = result.value["sRateLockStatusT"];
                    document.getElementById(<%=AspxTools.JsGetClientIdString(sRLckdDays)%>).value = result.value['sRLckdDays'];   
                    
                    $("#sFECurrentLockExtensionCount").val(result.value.sFECurrentLockExtensionCount);
                    $("#sFERateRenegotiationCount").val(result.value.sFERateRenegotiationCount);
                    $("#sFETotalLockExtensionCount").val(result.value.sFETotalLockExtensionCount);
                    $("#sFETotalRateReLockCount").val(result.value.sFETotalRateReLockCount);
                    
                    document.getElementById("RateLockHistory").innerHTML = result.value["RateLockHistory"]; 
                     
                    if ( wargs.emailCert ) document.getElementById('EmailRateLockConf').click();
                     
                    aReasonsNotAllow = JSON.parse(result.value["sReasonForNotAllowingResubmission"]);
                    UpdateRateLockStatus();
                    f_refreshLoanSummary();
                }
            }

            function f_clearReport(){
                if (confirm("Please confirm that you would like to clear the pricing report on the file."))
                {
                    var args = new Object();
                    args["loanid"] = <%= AspxTools.JsString(LoanID) %>;

                    var result = gService.loanedit.call('ClearAutoPriceProcessReport',args); 
                
                    if( f_handleResponse(result) == false) {
                        return;
                    }

                    document.getElementById('sAutoPriceProcessResult').value = result.value.sAutoPriceProcessResult;
                    document.getElementById('sAutoPriceProcessResultD').value = result.value.sAutoPriceProcessResultD;
                }
            }
            

        </script>
      
      
        <asp:PlaceHolder runat="server" ID="Prepare1stLoanForResubmissions" Visible="false">
        <script type="text/javascript">
        
            function f_prepareLoanForResubmission() {
                var nCount = aReasonsNotAllow.length;
                var msg = 'The following action(s) will be performed on the current loan:\n\n';
                for (var i = 0; i < nCount; i++) {
                    msg += '    * ' + aReasonsNotAllow[i] + '\n\n';
                }
                msg += 'Do you want to continue?';
                if (nCount == 0) {
                    msg = 'Do you want to continue?';
                }
                if (!confirm(msg)) {
                    return;
                }
                
                var args = new Object();
                args["loanid"] = <%= AspxTools.JsString(LoanID) %>;
                
                var result = gService.loanedit.call("PrepareLoanForResubmission", args);
                if (!result.error) {
                    if (result.value["HasError"] == "True") {
                        alert("ERROR: " + result.value["ErrorMessage"]);
                    } else {
                    self.location = self.location;
                    }
                }
            }
            
        </script>
        </asp:PlaceHolder>
      
        <asp:PlaceHolder runat="server" ID="Prepare2ndLoanForResubmissionLinked" Visible="false">
            <script type="text/javascript">
                function f_prepareLoanForResubmission() {
                    alert( <%= AspxTools.JsString(JsMessages.RateLock_2ndLienNotSupportWithLinkedLoan) %> );
                }
            </script>
      </asp:PlaceHolder>
        
        <asp:PlaceHolder runat="server" ID="Prepare2ndLoanForResubmissionNotLinked" Visible="false">
            <script type="text/javascript">
                function f_prepareLoanForResubmission() {
                    alert( <%= AspxTools.JsString(JsMessages.RateLock_2ndLienNotSupport) %> );
                }
            </script>
      </asp:PlaceHolder>

                        
      <uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg>
      </div>
    </form>
    <!--[if lte IE 6.5]><iframe id="selectmask" src="javascript:void(0)" style="display:none" ></iframe><![endif]-->
</body>
</html>
