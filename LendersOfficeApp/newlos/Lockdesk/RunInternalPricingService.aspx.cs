﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;

namespace LendersOfficeApp.newlos.Lockdesk
{
    public class RunInternalPricingServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(RunInternalPricingServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            // Set value from webform to CPageData
            dataLoan.sProdFilterDisplayrateMerge = GetBool("sProdFilterDisplayrateMerge");
            dataLoan.sProdFilterDisplayUsingCurrentNoteRate = GetBool("sProdFilterDisplayUsingCurrentNoteRate");
            dataLoan.sProdFilterRestrictResultToCurrentRegistered = GetBool("sProdFilterRestrictResultToCurrentRegistered");
            dataLoan.sProdFilterMatchCurrentTerm = GetBool("sProdFilterMatchCurrentTerm");
            dataLoan.sProdRLckdDays = GetInt("sProdRLckdDays");
            bool bProdFilterDueOther = dataLoan.sProdFilterDueOther = GetBool("sProdFilterDueOther", false);
            dataLoan.sProdFilterDue10Yrs = GetBool("sProdFilterDue10Yrs", bProdFilterDueOther);
            dataLoan.sProdFilterDue15Yrs = GetBool("sProdFilterDue15Yrs", bProdFilterDueOther);
            dataLoan.sProdFilterDue20Yrs = GetBool("sProdFilterDue20Yrs", bProdFilterDueOther);
            dataLoan.sProdFilterDue25Yrs = GetBool("sProdFilterDue25Yrs", bProdFilterDueOther);
            dataLoan.sProdFilterDue30Yrs = GetBool("sProdFilterDue30Yrs", bProdFilterDueOther);
            bool bProdFilterFinMethOther = dataLoan.sProdFilterFinMethOther = GetBool("sProdFilterFinMethOther", false);
            dataLoan.sProdFilterFinMethFixed = GetBool("sProdFilterFinMethFixed", bProdFilterFinMethOther);
            dataLoan.sProdFilterFinMeth3YrsArm = GetBool("sProdFilterFinMeth3YrsArm", bProdFilterFinMethOther);
            dataLoan.sProdFilterFinMeth5YrsArm = GetBool("sProdFilterFinMeth5YrsArm", bProdFilterFinMethOther);
            dataLoan.sProdFilterFinMeth7YrsArm = GetBool("sProdFilterFinMeth7YrsArm", bProdFilterFinMethOther);
            dataLoan.sProdFilterFinMeth10YrsArm = GetBool("sProdFilterFinMeth10YrsArm", bProdFilterFinMethOther);
            dataLoan.sProdFilterPmtTPI = GetBool("sProdFilterPmtTPI");
            dataLoan.sProdFilterPmtTIOnly = GetBool("sProdFilterPmtTIOnly");
            dataLoan.sInvestorLockLpePriceGroupId = GetGuid("sInvestorLockLpePriceGroupId");
            dataLoan.sProdInvestorRLckdDays_rep = GetString("sProdInvestorRLckdDays");
            dataLoan.sProdInvestorRLckdExpiredD_rep = GetString("sProdInvestorRLckdExpiredD");
            dataLoan.sProdInvestorRLckdModeT = (E_sProdInvestorRLckdModeT) GetInt("sProdInvestorRLckdModeT");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sProdFilterDisplayrateMerge", dataLoan.sProdFilterDisplayrateMerge);
            SetResult("sProdFilterDisplayUsingCurrentNoteRate", dataLoan.sProdFilterDisplayUsingCurrentNoteRate);
            SetResult("sProdFilterRestrictResultToCurrentRegistered", dataLoan.sProdFilterRestrictResultToCurrentRegistered);
            SetResult("sProdFilterMatchCurrentTerm", dataLoan.sProdFilterMatchCurrentTerm);
            SetResult("sProdFilterDue10Yrs", dataLoan.sProdFilterDue10Yrs);
            SetResult("sProdFilterDue15Yrs", dataLoan.sProdFilterDue15Yrs);
            SetResult("sProdFilterDue20Yrs", dataLoan.sProdFilterDue20Yrs);
            SetResult("sProdFilterDue25Yrs", dataLoan.sProdFilterDue25Yrs);
            SetResult("sProdFilterDue30Yrs", dataLoan.sProdFilterDue30Yrs);            
            SetResult("sProdFilterDueOther", dataLoan.sProdFilterDueOther);
            SetResult("sProdFilterFinMethFixed", dataLoan.sProdFilterFinMethFixed);            
            SetResult("sProdFilterFinMeth3YrsArm", dataLoan.sProdFilterFinMeth3YrsArm);
            SetResult("sProdFilterFinMeth5YrsArm", dataLoan.sProdFilterFinMeth5YrsArm);
            SetResult("sProdFilterFinMeth7YrsArm", dataLoan.sProdFilterFinMeth7YrsArm);
            SetResult("sProdFilterFinMeth10YrsArm", dataLoan.sProdFilterFinMeth10YrsArm);
            SetResult("sProdFilterFinMethOther", dataLoan.sProdFilterFinMethOther);
            SetResult("sProdFilterPmtTPI", dataLoan.sProdFilterPmtTPI);
            SetResult("sProdFilterPmtTIOnly", dataLoan.sProdFilterPmtTIOnly);
            SetResult("sInvestorLockLpePriceGroupId", dataLoan.sInvestorLockLpePriceGroupId);
            SetResult("sProdInvestorRLckdDays", dataLoan.sProdInvestorRLckdDays_rep);
            SetResult("sProdInvestorRLckdExpiredD", dataLoan.sProdInvestorRLckdExpiredD_rep);
            SetResult("sProdRLckdDays", dataLoan.sProdRLckdDays_rep);
        }
    }
    public partial class RunInternalPricingService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new RunInternalPricingServiceItem());
        }
    }


}
