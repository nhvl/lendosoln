﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LendersOfficeApp.newlos.Lockdesk
{
    public partial class ExtendInvestorLock : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterService("InvestorRateLock", "/newlos/Lockdesk/InvestorRateLockService.aspx");
            base.DisplayCopyRight = false;
        }
    }
}
