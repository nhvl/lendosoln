﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvestorRateLock.aspx.cs" Inherits="LendersOfficeApp.newlos.Lockdesk.InvestorRateLock" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %> 
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head id="Head1" runat="server">
        <title>Investor Rate Lock</title>
        <meta http-equiv="X-UA-Compatible" content="IE=8"/>        

        <link type="text/css" href="../../css/calendar-win2k-cold-2.css" rel="Stylesheet" />
        <link href="../../css/stylesheetnew.css" rel="stylesheet" type="text/css" />
        <style type="text/css" >
            body
            {
                background-color: gainsboro;
            }
            #wrapper
            {
                padding: 5px;
            }
            .rateField
            {
                width: 50px;
            }
            .narrowMoneyField
            {
                width: 70px;
            }
            .bigNoteField
            {
                width: 400px;
                height: 200px;
            }
            .bigNoteField td{
                line-height:44px;
            }
            .isborder
            {
                border-style: ridge;
                border-width: medium;
            }
            .RateLockTable
            {
                margin-top: 10px;
                border-collapse: collapse;
                table-layout: fixed;
            }
   
            table.RateLockTable td
            {
                border: 1px solid #EEEEEE;
            }
            
            td.date 
            {
                width:150px;
            }
            td.action 
            {
                width: 100px;
            }
            
            td.by 
            {
                width: 80px;
            }
            
            td.lockExpiration{
                width: 60px;
            }
            td.rate, td.price, td.fee, td.margin, td.trate, td.lockfee {
                width: 50px;
            }
            
            td.reason {
                widith: 200px;
            }
          
            
            .RateLockHeader
            {
                font-weight: bold;
                font-size: 11px;
                color: white;
                font-family: Arial, Helvetica, sans-serif;
                background-color: #999999;
            }
            .RateLockAlternatingItem
            {
                font-size: 11px;
                color: black;
                font-family: Arial, Helvetica, sans-serif;
                background-color: #cccccc;
            }
            .RateLockItem
            {
                font-size: 11px;
                color: black;
                font-family: Arial, Helvetica, sans-serif;
                background-color: white;
            }
            .dayField
            {
                width: 40px;
            }
            .dateField
            {
                width: 70px;
            }
            #MainTable
            {
                width: 100%;
            }
            fieldset
            {
                width: 850px;
                margin-top: 5px;
            }
            div.GridHeader
            {
                padding: 4px;
            }
            table.Pricing
            {
                margin-left: 227px;
            }
            table.Adjustments
            {
                margin-bottom: 0px;
            }
            div.AdjustmentHeader
            {
            
                clear: both;
                margin-bottom: 5px;
            }
            div.AdjustmentHeader, table.Adjustments
            {
               width: 665px; 
            }
            .descriptionField
            {
                width: 300px;
            }
            .fieldsetWrapper
            {
                padding: 3px;
            }
            
            .ButtonStyle 
            {
                padding:2px;
                width: auto;
                overflow: visible; 
            }
            #CustomFields .action
            {
                cursor: pointer;
            }
            
            #CustomFields .open
            {
                display: none;
            }
            
            #CustomFields.open span.open
            {
                display: inline;
            }
            
            #CustomFields.open div.open
            {
                display: block;
            }
            
            #CustomFields.open .closed
            {
                display: none;
            }
            #CustomFields input[preset=money]
            {
                width: 90px;
            }
            #CustomFields input[preset=percent]
            {
                width: 60px;
            }
        </style>
    </head>
<body >
    <form id="form1" runat="server" onreset="return false;">
    <asp:HiddenField runat="server" ID="LoanId" />
    <asp:HiddenField runat="server" ID="sIsOptionArm" />
    <asp:HiddenField runat="server" ID="mVRoot" />   
    <asp:HiddenField runat="server" ID="IsPageReadOnly" />
    <asp:HiddenField runat="server" ID="CanWrite" />
    <asp:HiddenField runat="server" ID="sInvestorLockCheckBrokerExtendByDefault" />
    <div class="MainRightHeader">
        Back-end Rate Lock
    </div>
      <div id="wrapper">
    <div>
        <input type="button" class="ButtonStyle" value="Show Loan Summary" runat="server" id="ShowLoanSummaryBtn" />            <input class="ButtonStyle" type="button" id="RunInternalPricing" value="Run Internal Pricing" onclick="f_runInternalPricing();" runat="server"/>
    </div>
    
    <fieldset>
        <div class="FieldLabel GridHeader">
            <input type="button" id="RateLockBtn" class="ButtonStyle" value="RateLock" runat="server"  />
            <input class="ButtonStyle" type="button" id="SuspendLockBtn" value="Resume Lock with Modifications..." runat="server" />
        </div>
        <div class="fieldsetWrapper">
        
        <input type="button" id="PopulateButton" value="Populate Fields From Front-end Lock Request..." class="ButtonStyle" runat="server" />
        <table cellpadding="1" cellspacing="1" border="0" id="MainTable">
            <tbody id="OldInvestorNameRow" runat="server">
                            <tr style="background-color:Yellow;margin:5px;border:solid 1px black">
                    <td>
                    <ml:EncodedLabel ID="Label4" AssociatedControlID="sInvestorLockLpInvestorNm" CssClass="FieldLabel" runat="server">Investor Name</ml:EncodedLabel>
                        
                    </td>
                    <td>&nbsp;</td>
                    <td colspan="3">
                         <asp:TextBox runat="server" ID="sInvestorLockLpInvestorNm" width="250"/> <span class="FieldLabel">* NOTE - Must pick investor name from pre-define list.</span>
                    </td>

                   
                </tr>
                <tr><td colspan="5">&nbsp;</td></tr>
            </tbody>
            <tbody>

                <tr>
                    <td><ml:EncodedLabel ID="Label3" runat="server" AssociatedControlID="sInvestorRolodexId" CssClass="FieldLabel" Text="Investor" /></td>
                    <td>&nbsp;</td>
                    <td>
                        <asp:DropDownList runat="server" ID="sInvestorRolodexId" />
                       
                    </td>
                    <td><ml:EncodedLabel AssociatedControlID="sInvestorLockLoanNum" runat="server" CssClass="FieldLabel">Investor Loan #</ml:EncodedLabel></td>
                    <td><asp:TextBox runat="server" ID="sInvestorLockLoanNum" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td><ml:EncodedLabel runat="server" AssociatedControlID="sInvestorLockLpTemplateNm" CssClass="FieldLabel"  >Investor Program Description</ml:EncodedLabel></td>
                    <td>&nbsp;</td>
                    <td><asp:TextBox runat="server" ID="sInvestorLockLpTemplateNm" Width="240px"></asp:TextBox></td>
                    <td><ml:EncodedLabel runat="server" AssociatedControlID="sInvestorLockProgramId" CssClass="FieldLabel" >Investor Program Identifier</ml:EncodedLabel></td>
                    <td><asp:TextBox runat="server" ID="sInvestorLockProgramId"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><ml:EncodedLabel AssociatedControlID="sInvestorLockRateLockStatusT" runat="server" CssClass="FieldLabel" >Investor Lock Status</ml:EncodedLabel></td>
                    <td>&nbsp;</td>                    
                    <td><asp:TextBox runat="server" ID="sInvestorLockRateLockStatusT" ReadOnly="true" ></asp:TextBox></td>
                    <td><ml:EncodedLabel runat="server" AssociatedControlID="sInvestorLockConfNum" CssClass="FieldLabel" >Investor Lock Confirmation #</ml:EncodedLabel></td>
                    <td><asp:TextBox runat="server" ID="sInvestorLockConfNum"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><ml:EncodedLabel AssociatedControlID="sInvestorLockRLckdDays" runat="server" CssClass="FieldLabel">Investor Lock Period</ml:EncodedLabel></td>
                    <td> &nbsp;</td>
                    <td class="FieldLabel">
                        <asp:TextBox runat="server" ID="sInvestorLockRLckdDays" CssClass="dayField" MaxLength="4"></asp:TextBox> days 
                        <input type="button" class="ButtonStyle" runat="server" id="ExtendRateLockBtn" value="Extend Rate Lock..." />
                    </td>
                    <td> <ml:EncodedLabel runat="server" AssociatedControlID="sInvestorLockRateSheetID" CssClass="FieldLabel" >Investor Rate Sheet ID </ml:EncodedLabel></td>
                    <td><asp:TextBox runat="server" ID="sInvestorLockRateSheetID" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td><ml:EncodedLabel runat="server"  AssociatedControlID="sInvestorLockRLckdD" CssClass="FieldLabel">Investor Rate Lock Date</ml:EncodedLabel></td>
                    <td>&nbsp;</td>
                    
                    <td><asp:TextBox runat="server" ID="sInvestorLockRLckdD" ReadOnly="true" CssClass="dateField"></asp:TextBox></td>
                    <td>
                        <ml:EncodedLabel runat="server" AssociatedControlID="sInvestorLockRateSheetEffectiveTime" CssClass="FieldLabel"> Inv. Rate Sheet Effective Time</ml:EncodedLabel>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="sInvestorLockRateSheetEffectiveTime" Width="200px" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                     <td><ml:EncodedLabel  runat="server"  AssociatedControlID="sInvestorLockRLckExpiredD"  CssClass="FieldLabel">Investor Rate Lock Expiration</ml:EncodedLabel></td>
                    <td><asp:CheckBox runat="server" ID="sInvestorLockRLckExpiredDLckd" Enabled="false" /></td>                     
                    <td>
                        <asp:TextBox runat="server" ID="sInvestorLockRLckExpiredD" ReadOnly="true" CssClass="dateField"></asp:TextBox>
                    </td>
                    <td><ml:EncodedLabel runat="server" AssociatedControlID="sInvestorLockLockFee" CssClass="FieldLabel rate" >Investor Lock Fee</ml:EncodedLabel></td>
                    <td><asp:TextBox runat="server" ID="sInvestorLockLockFee" preset="money"  CssClass="rateField" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td><ml:EncodedLabel ID="Label1"  runat="server"  AssociatedControlID="sInvestorLockDeliveryExpiredD" CssClass="FieldLabel">Investor Delivery Expiration</ml:EncodedLabel></td>
                    <td><asp:CheckBox runat="server" ID="sInvestorLockDeliveryExpiredDLckd" Enabled="false" /></td>
                    <td>
                    <asp:TextBox runat="server" ID="sInvestorLockDeliveryExpiredD" ReadOnly="true" CssClass="dateField"></asp:TextBox>
                    </td>
                    <td><ml:EncodedLabel ID="Label2" runat="server" AssociatedControlID="sInvestorLockCommitmentT" CssClass="FieldLabel">Investor Commitment Type</ml:EncodedLabel></td>
                    <td>
                        <asp:DropDownList runat="server" ID="sInvestorLockCommitmentT"></asp:DropDownList>
                    </td>
                </tr>
            </tbody>
        </table>
        <table id="CountTable">
            <tr>
                <td class="FieldLabel"># Of Investor Extensions (Current Lock)</td>
                <td><asp:TextBox runat="server" ID="sBECurrentLockExtensionCount" style="width:40px;"></asp:TextBox></td>
                <td class="FieldLabel"><asp:CheckBox runat="server" ID="sBECurrentLockExtensionCountLckd" /> Lock</td>
            </tr>
            <tr>
                <td class="FieldLabel" nowrap># Of Investor Extensions (All Locks On File)</td>
                <td><asp:TextBox runat="server" ID="sBETotalLockExtensionCount" style="width:40px;"></asp:TextBox></td>
                <td class="FieldLabel"><asp:CheckBox runat="server" ID="sBETotalLockExtensionCountLckd" /> Lock</td>
            </tr>
            <tr>
                <td class="FieldLabel"># Of Investor Rate Re-Locks On File</td>
                <td><asp:TextBox runat="server" ID="sBETotalRateReLockCount" style="width:40px;"></asp:TextBox></td>
                <td class="FieldLabel"><asp:CheckBox runat="server" ID="sBETotalRateReLockCountLckd" /> Lock</td>
            </tr>
        </table>
  <br />
    <hr />
    <table cellpadding="0" class="Pricing" cellspacing="0" id="PricingTable">
        <thead>
            <tr>
                <td class="FieldLabel">
                    &nbsp;
                </td>
                <td  align="center" class="FieldLabel">
                    Rate
                </td>
                <td align="center" class="FieldLabel">
                    Price
                </td>
                <td align="center" class="FieldLabel">
                    Fee
                </td>
                <td align="center" class="FieldLabel">
                    Margin
                </td>
         
                <td align="center" class="FieldLabel">
                    Teaser<br />Rate
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="FieldLabel">
                    Base Price
                </td>
                <td>
                    <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sInvestorLockBaseNoteIR"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sInvestorLockBaseBrokComp1PcPrice"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sInvestorLockBaseBrokComp1PcFee"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sInvestorLockBaseRAdjMarginR"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" CssClass="rateField" preset="percent" ReadOnly="true" ID="sInvestorLockBaseOptionArmTeaserR"></asp:TextBox>
                </td>
                <td>
                    <asp:RadioButton runat="server" CssClass="FieldLabel" ID="sInvestorLockPriceRowLockedT_Base"
                        GroupName="PricingLock" />
                    <ml:EncodedLabel runat="server" AssociatedControlID="sInvestorLockPriceRowLockedT_Base" CssClass="FieldLabel" >Edit</ml:EncodedLabel>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Total Adjustments
                </td>
                <td>
                    <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sInvestorLockTotAdjNoteIR"
                        ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sInvestorLockTotAdjBrokComp1PcPrice"
                        ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sInvestorLockTotAdjBrokComp1PcFee"
                        ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sInvestorLockTotAdjRAdjMarginR"
                        ReadOnly="true"></asp:TextBox>
                </td>
  
                <td colspan="2">
                    <asp:TextBox runat="server" CssClass="rateField" preset="percent"  ID="sInvestorLockTotAdjOptionArmTeaserR"
                        ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Adjusted Price
                </td>
                <td>
                    <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sInvestorLockNoteIR"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sInvestorLockBrokComp1PcPrice" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sInvestorLockBrokComp1Pc"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sInvestorLockRAdjMarginR"></asp:TextBox>
                </td>
         
                <td>
                    <asp:TextBox runat="server" CssClass="rateField" preset="percent" ReadOnly="true" ID="sInvestorLockOptionArmTeaserR"></asp:TextBox>
                </td>
                <td>
                    <asp:RadioButton runat="server" CssClass="FieldLabel"  ID="sInvestorLockPriceRowLockedT_Adjusted" 
                        GroupName="PricingLock" />
                    <ml:EncodedLabel runat="server" AssociatedControlID="sInvestorLockPriceRowLockedT_Adjusted" CssClass="FieldLabel" >Edit</ml:EncodedLabel>
                </td>
            </tr>
        </tbody>
    </table>
    
    <div class="AdjustmentHeader FieldLabel">
        Adjustments
    </div>
    <table id="AdjustmentTable" cellpadding="0" cellspacing="0" class="Adjustments">
        <thead>
        <tr class="GridHeader">
            <th>
                <input type="checkbox" id="SelectAllAdjustments" />
            </th>
            <th style="width:306px" >
                Description
            </th>
            <th style="width:56px">
                Rate
            </th>
            <th style="width:56px">
                Price
            </th>
            <th style="width:56px">
                Fee
            </th>
            <th style="width:56px">
                Margin
            </th>
  
            <th style="width:56px">
                Teaser<br />Rate
            </th>
            <th style="width:56px">
                Persist
            </th>
        </tr>
        </thead>
        <tbody>
            
        </tbody>
    </table>
        <div class="AdjustmentHeader">
        <input type="button" runat="server" class="ButtonStyle" id="AddAdjustment" value="Add New Adjustment" />
        <input type="button" runat="server" class="ButtonStyle" id="DeleteSelectedAdjustments" value="Delete Selected Adjustments" />
            </div>
        </div>    
    </fieldset>
    <br />
        <div id="CustomFields">
            <span id="CustomFieldsLabel"><span class="closed">&#9658;</span><span class="open">&#9660;</span> Rate Lock Custom Fields</span>
            <div id="CustomRateLockFields" class="open">
                <table>
                    <thead>
                        <tr>
                            <th>
                                <!--Blank -->
                            </th>
                            <th>
                                Item Description
                            </th>
                            <th>
                                Date
                            </th>
                            <th>
                                Amount
                            </th>
                            <th>
                                Percentage
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="GridItem">
                            <td> 
                                Lock Field Set 1
                            </td>
                            <td>
                                <input type='text' id='sU1LockFieldDesc' runat='server' class='mask' />
                            </td>
                            <td>
                                <ml:DateTextBox ID='sU1LockFieldD' runat='server' CssClass='mask' Width='75' preset='date'></ml:DateTextBox>
                            </td>
                            <td>
                                <input type='text' id='sU1LockFieldAmt' runat='server' class='mask' preset='money' />
                            </td>
                            <td>
                                <input type='text' id='sU1LockFieldPc' runat='server' class='mask' preset='percent' />
                            </td>
                        </tr>
                        <tr class="GridAlternatingItem">
                            <td> 
                                Lock Field Set 2
                            </td>
                            <td>
                                <input type='text' id='sU2LockFieldDesc' runat='server' class='mask' />
                            </td>
                            <td>
                                <ml:DateTextBox ID='sU2LockFieldD' runat='server' CssClass='mask' Width='75' preset='date'></ml:DateTextBox>
                            </td>
                            <td>
                                <input type='text' id='sU2LockFieldAmt' runat='server' class='mask' preset='money' />
                            </td>
                            <td>
                                <input type='text' id='sU2LockFieldPc' runat='server' class='mask' preset='percent' />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <table cellpadding="2" cellspacing="0">
            <thead>
                <tr>
                    <td class="FieldLabel" >Internal Notes</td>
                    <td align="right"><input type="button" class="ButtonStyle" runat="server" id="DateTimeStampNoteBtn" value="Date &amp; Time Stamp" onclick="f_WriteDateAndTimeStamp();"/></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                <td colspan="2">
                    <asp:TextBox runat="server" CssClass="bigNoteField" ID="sTrNotes" TextMode="MultiLine"></asp:TextBox>
                </td>
                <td colspan="2">
                    <table cellpadding="1" cellspacing="1"  style="padding:5px;" class="isborder bigNoteField" >
                        <thead>
                            <tr>
                                <td>&nbsp;</td>
                                <td  class="FieldLabel">Rate</td>
                                <td  class="FieldLabel">Price</td>
                                <td  class="FieldLabel">Amount</td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">Back-end Rate Lock Pricing</td>
                                <td><asp:TextBox runat="server" ReadOnly="true" CssClass="rateField" ID="sInvestorLockNoteIRQS"></asp:TextBox></td>
                                <td><asp:TextBox runat="server" ReadOnly="true" CssClass="rateField" ID="InvestorRateLockPricingP"></asp:TextBox></td>
                                <td>&nbsp;</td>                            
                            </tr>
                            <tr>
                                <td  class="FieldLabel">Front-end Rate Lock Pricing</td>
                                <td><asp:TextBox runat="server" ReadOnly="true" CssClass="rateField" ID="sNoteIR"></asp:TextBox></td>
                                <td><asp:TextBox runat="server" ReadOnly="true" CssClass="rateField" ID="sBrokerLockFinalBrokComp1PcPrice"></asp:TextBox></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">Gross Projected Profit / Loss</td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:TextBox runat="server"  CssClass="rateField" ID="sInvestorLockProjectedProfit" ReadOnly="true"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" CssClass="narrowMoneyField" ID="sInvestorLockProjectedProfitAmt" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
      
                        </thead>
                    </table>
                </td>
                </tr>
            </tbody>
        
        </table>
        <br />
        <div class="FieldLabel">Audit History</div>
        <div id="RateLockHistory" runat="server">
            <ml:PassthroughLiteral runat="server" ID="RateLockHistoryData"></ml:PassthroughLiteral>
        </div>
        <uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
        
        <script type="text/javascript">
            jQuery(function($){
                var $customFields = $('#CustomFields');
                $('#CustomFieldsLabel').click(function(){
                    $customFields.toggleClass('open');
                }).addClass("action");
                
                if($('#ShowCustomFields').length){
                    $customFields.addClass('open');
                }
                
                checkLockStatus();
                $("#sBECurrentLockExtensionCountLckd").change(function(){checkLockStatus();});
                $("#sBETotalLockExtensionCountLckd").change(function(){checkLockStatus();});
                $("#sBETotalRateReLockCountLckd").change(function(){checkLockStatus();});
            });
            
            function checkLockStatus()
            {
                $("#sBECurrentLockExtensionCount").prop("readonly", !$("#sBECurrentLockExtensionCountLckd").prop("checked"));
                $("#sBETotalLockExtensionCount").prop("readonly", !$("#sBETotalLockExtensionCountLckd").prop("checked"));
                $("#sBETotalRateReLockCount").prop("readonly", !$("#sBETotalRateReLockCountLckd").prop("checked"));
                
            }
            
            function f_WriteDateAndTimeStamp() 
            {
                var user = <%= AspxTools.JsString(BrokerUser.FirstName + " " + BrokerUser.LastName) %>;
                
                var timeStamp = getTimeStamp(user);

                var notes = <%= AspxTools.JsGetElementById(sTrNotes) %>;

                notes.value = timeStamp + notes.value;

                updateDirtyBit();
            }
            function f_runInternalPricing()
            {
              linkMe(<%= AspxTools.JsString(this.GetInternalPricerUrl()) %>, null, <%= AspxTools.JsBool(this.UseInternalPricerV2) %>);
            }
            function linkMe(href, extraArgs, useFullEditor) {
              var ch = href.indexOf('?') > 0 ? '&' : '?';
              var loanID = document.getElementById("loanid").value;
              parent.body.f_load(href + ch + "loanid=" + loanID + (extraArgs == null ? "" : "&" + extraArgs), useFullEditor);
            }
            function saveMe() {
                return IRLPage.save();
            }

            function f_openAuditItem(id){
               return IRLPage.openAuditItem(id);
            }
        </script>
    </div>
    </form>
</body>
</html>
