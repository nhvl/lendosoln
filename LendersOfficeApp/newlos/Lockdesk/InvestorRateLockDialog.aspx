﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvestorRateLockDialog.aspx.cs" Inherits="LendersOfficeApp.newlos.Lockdesk.InvestorRateLockDialog" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %> 
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Investor Rate Lock</title>
    <link href="../../css/stylesheetnew.css" type="text/css" rel="Stylesheet" />
    <style type="text/css">
        body { background-color:gainsboro; }
        #wrapper { padding: 5px; }
	    .dateField { width: 60px;   }
	    .dayField { width: 20px; }
        .descField { width: 250px; }
        .centeringDiv { margin-right: auto; margin-left : auto; text-align: center;  padding-top: 40px; }
    </style>
    
    <link type="text/css" href="../../css/calendar-win2k-cold-2.css" rel="Stylesheet" />

</head>
<body>
    <h4 class="page-header">Lock Investor Rate</h4>
    <form id="form1" runat="server" onreset="return false;">
    <div id="wrapper">
        <table cellpadding="1" cellspacing="1" border="0">
            <tbody>
            <tr>
                <td>
                    <ml:EncodedLabel runat="server" CssClass="FieldLabel"  AssociatedControlID="sInvestorLockRLckdD">Investor Rate Lock Date</ml:EncodedLabel>
                </td>
                <td>
                &nbsp;
                </td>
                <td>
                    <ml:DateTextBox runat="server" ID="sInvestorLockRLckdD" preset="date" CssClass="dateField" onchange="f_UpdateLockExpireDate();_validatePage();" onkeyup="f_UpdateLockExpireDate();_validatePage();" ></ml:DateTextBox>
                    <img alt="Required" src="../../images/error_icon.gif" id="sInvestorLockRLckdD_R" />
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel runat="server"  CssClass="FieldLabel" AssociatedControlID="sInvestorLockRLckdDays" >Investor Lock Period</ml:EncodedLabel>
                </td>
                <td>
                &nbsp;
                </td>
                <td class="FieldLabel">
                    <asp:TextBox runat="server" ID="sInvestorLockRLckdDays" onchange="f_UpdateLockExpireDate();_validatePage();"  CssClass="dayField" MaxLength="4" onkeyup="f_UpdateLockExpireDate();_validatePage();"></asp:TextBox>
                    days
                    <img alt="Required" src="../../images/error_icon.gif" id="sInvestorLockRLckdDays_R" />
                    
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel runat="server"  CssClass="FieldLabel" AssociatedControlID="sInvestorLockRLckExpiredD">Investor Rate Lock Expiration</ml:EncodedLabel>
                </td>
                <td align="right">
                    <asp:CheckBox runat="server" ID="sInvestorLockRLckExpiredDLckd" onclick="lockClick(this,'sInvestorLockRLckExpiredD');" />       
                </td>
                <td>         
                    <ml:DateTextBox CssClass="dateField" runat="server" preset="date" ID="sInvestorLockRLckExpiredD"   onkeyup="_validatePage()" onchange="_validatePage()"></ml:DateTextBox>
 
                    <img alt="Required" src="../../images/error_icon.gif" id="sInvestorLockRLckExpiredD_R" />
                    
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel runat="server" CssClass="FieldLabel" AssociatedControlID="sInvestorLockDeliveryExpiredD">Investor Delivery Expiration</ml:EncodedLabel>
                </td>
                <td align="right">
                    <asp:CheckBox runat="server" ID="sInvestorLockDeliveryExpiredDLckd" onclick="lockClick(this,'sInvestorLockDeliveryExpiredD');" />
                </td>
                <td>
                    <ml:DateTextBox runat="server" CssClass="dateField" preset="date" ReadOnly="true" ID="sInvestorLockDeliveryExpiredD"  onkeyup="_validatePage()" onchange="_validatePage()"></ml:DateTextBox>
                    <img alt="Required" src="../../images/error_icon.gif" id="sInvestorLockDeliveryExpiredD_R" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <ml:EncodedLabel runat="server" CssClass="FieldLabel"  AssociatedControlID="Reason">Reason</ml:EncodedLabel>
                    <asp:TextBox runat="server" CssClass="descField" ID="Reason" ></asp:TextBox>
                </td>
               
            </tr>
                      </tbody>
        </table>
         

        <div class="centeringDiv">
                    <input type="button" id="LockRateBtn" value="Lock Rate" class="ButtonStyle"  onclick="LockRate()" />
                    <input type="button" id="Cancel" value="Cancel" class="ButtonStyle" onclick="onClosePopup()" />

  </div>
    </div>
      <uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg>
    
    </form>
        
        <script type="text/javascript">
        
        function doAfterDateFormat(){
            _validatePage();
        }
    
            function lockClick(obj, fieldId){
 
                var field = document.getElementById(fieldId);
                
                field.readOnly = !obj.checked;
                
                if(!obj.checked){
                    field.nextSibling.style.visibility = 'hidden';
                    f_UpdateLockExpireDate();
                }
                else {
                    field.nextSibling.style.visibility = 'visible';
                }
                _validatePage();
                
            }
            function f_toDtString(dt) {
                return (dt.getMonth() + 1) + '/' + dt.getDate() + '/' + dt.getFullYear();
            }
            
            function f_UpdateLockExpireDate(){
                var sInvestorLockRLckdDays = document.getElementById('sInvestorLockRLckdDays');
                var sInvestorLockRLckdD = document.getElementById('sInvestorLockRLckdD');
                var sInvestorLockRLckExpiredD = document.getElementById('sInvestorLockRLckExpiredD');
                var sInvestorLockDeliveryExpiredD = document.getElementById('sInvestorLockDeliveryExpiredD');
                var sInvestorLockRLckExpiredDLckd = document.getElementById('sInvestorLockRLckExpiredDLckd');
                var sInvestorLockDeliveryExpiredDLckd = document.getElementById('sInvestorLockDeliveryExpiredDLckd');
                var sInvestorLockDeliveryExpiredD = document.getElementById('sInvestorLockDeliveryExpiredD');
                
                var re = /^\d+$/
                if(!re.test(sInvestorLockRLckdDays.value)) {
                    sInvestorLockRLckdDays.value = '';
                } 
                  
                var days = parseInt(sInvestorLockRLckdDays.value), sInvestorLockRLckDate = new Date(sInvestorLockRLckdD.value);
                       
                
                if(sInvestorLockRLckExpiredDLckd.checked && sInvestorLockDeliveryExpiredDLckd.checked){
                    return;
                }
                
       
                
                if( sInvestorLockRLckdDays.value.length === 0 || isNaN(days) || days < 0){
                    if(!sInvestorLockRLckExpiredDLckd.checked){
                        sInvestorLockRLckExpiredD.value = '';
                    }
                    if(!sInvestorLockDeliveryExpiredDLckd.checked){
                        sInvestorLockDeliveryExpiredD.value = '';
                    }
                    return;
                }
            
                sInvestorLockRLckDate.setDate(sInvestorLockRLckDate.getDate()+days);
                if(isNaN(sInvestorLockRLckDate.getMonth())){
                    if(!sInvestorLockRLckExpiredDLckd.checked){
                        sInvestorLockRLckExpiredD.value = '';
                    }
                    if(!sInvestorLockDeliveryExpiredDLckd.checked){
                        sInvestorLockDeliveryExpiredD.value = '';
                    }
                    return;
                }
                var month = sInvestorLockRLckDate.getMonth() + 1;
                var day = sInvestorLockRLckDate.getDate();
                var year = sInvestorLockRLckDate.getFullYear();
                var newDate = month + '/' + day + '/' + year;
                
                if(!sInvestorLockRLckExpiredDLckd.checked){
                    sInvestorLockRLckExpiredD.value = newDate;
                }
                
                if(!sInvestorLockDeliveryExpiredDLckd.checked){
                    sInvestorLockDeliveryExpiredD.value = newDate;
                }
            }
            
            function _validatePage(){

                var g = function(id) { return document.getElementById(id); }; 
                var sInvestorLockRLckdD = g('sInvestorLockRLckdD');
                var sInvestorLockRLckdDays = g('sInvestorLockRLckdDays');
                var sInvestorLockRLckExpiredD = g('sInvestorLockRLckExpiredD');
                var sInvestorLockDeliveryExpiredD = g('sInvestorLockDeliveryExpiredD');
                var Reason= g('Reason');
                var LockRateBtn = g('LockRateBtn');
                
                var sInvestorLockRLckdD_R = g('sInvestorLockRLckdD_R');
                var sInvestorLockRLckdDays_R = g('sInvestorLockRLckdDays_R');
                var sInvestorLockRLckExpiredD_R = g('sInvestorLockRLckExpiredD_R');
                var sInvestorLockDeliveryExpiredD_R = g('sInvestorLockDeliveryExpiredD_R');
                
                
                var pageIsValid = true;
                
                sInvestorLockRLckdD_R.style.visibility = trim(sInvestorLockRLckdD.value).length === 0 ? 'visible' : 'hidden';
                if( trim(sInvestorLockRLckdD.value).length === 0 || isNaN(Date.parse(sInvestorLockRLckdD.value)) ){
                    sInvestorLockRLckdD_R.style.visibility =   'visible';
                    pageIsValid = false;
                }
                else{
                    sInvestorLockRLckdD_R.style.visibility =   'hidden';                
                }
                
                
                if( trim(sInvestorLockRLckdDays.value).length === 0){
                    sInvestorLockRLckdDays_R.style.visibility = 'visible';
                        pageIsValid = false;
                    
                }
                else {
                    var days = parseInt(sInvestorLockRLckdDays.value)
                    if( isNaN(days) || days < 0 ){
                        sInvestorLockRLckdDays_R.style.visibility = 'visible';    
                        pageIsValid = false;
                                        
                    }                
                    else {
                         sInvestorLockRLckdDays_R.style.visibility = 'hidden';  
                         
                    }
                }
                
                if( trim(sInvestorLockRLckExpiredD.value).length === 0 || isNaN(Date.parse(sInvestorLockRLckExpiredD.value)) ){
                    sInvestorLockRLckExpiredD_R.style.visibility =   'visible';
                    pageIsValid = false;
                }
                else{
                    sInvestorLockRLckExpiredD_R.style.visibility =   'hidden';                
                }
                
                if(trim(sInvestorLockDeliveryExpiredD.value).length === 0 || isNaN(Date.parse(sInvestorLockDeliveryExpiredD.value))){
                    sInvestorLockDeliveryExpiredD_R.style.visibility  = 'visible';   
                    pageIsValid = false;
                }
                else {
                    sInvestorLockDeliveryExpiredD_R.style.visibility  = 'hidden';   
                }
   
                
                LockRateBtn.disabled = !pageIsValid;
                
            }
        
            function _init(){
                resizeForIE6And7(425,350);
                var g = function(id){ return document.getElementById(id);}, a = getModalArgs(), dt = new Date();
                  
                var sInvestorLockRLckExpiredDLckd = g('sInvestorLockRLckExpiredDLckd');
                var sInvestorLockDeliveryExpiredDLckd = g('sInvestorLockDeliveryExpiredDLckd');
                var sInvestorLockRLckdD = g('sInvestorLockRLckdD'); 
                var sInvestorLockRLckdDays = g('sInvestorLockRLckdDays');
                var sInvestorLockRLckExpiredD = g('sInvestorLockRLckExpiredD');
                var sInvestorLockDeliveryExpiredD = g('sInvestorLockDeliveryExpiredD');
                
                sInvestorLockRLckdD.checked = a.sInvestorLockRLckdD;
                sInvestorLockRLckdDays.value = a.sInvestorLockRLckdDays;
                
                sInvestorLockRLckdD.value = f_toDtString(dt);
                
                
                lockClick(sInvestorLockRLckExpiredDLckd, 'sInvestorLockRLckExpiredD');
                lockClick(sInvestorLockDeliveryExpiredDLckd, 'sInvestorLockDeliveryExpiredD');
                
                f_UpdateLockExpireDate();
                _validatePage();
            }
            
            function LockRate() {
                var sInvestorLockRLckdD = document.getElementById('sInvestorLockRLckdD');
                var sInvestorLockRLckExpiredD = document.getElementById('sInvestorLockRLckExpiredD');
                var sInvestorLockDeliveryExpiredD = document.getElementById('sInvestorLockDeliveryExpiredD');
                
                if(isNaN(Date.parse(sInvestorLockRLckdD.value))){
                    alert('Invalid Investor Rate Lock Date');
                    return;
                }
                
                if(isNaN(Date.parse(sInvestorLockRLckExpiredD.value))){
                    alert('Invalid Investor Rate Lock Expiration Date');
                    return;
                }
                
                if(isNaN(Date.parse(sInvestorLockDeliveryExpiredD.value))){
                    alert('Invalid Investor Delivery Expiration Date');
                    return;
                }
                
                var arg = window.dialogArguments || {};
                arg.OK = 'OK';
                arg.sInvestorLockRLckdDays            = document.getElementById('sInvestorLockRLckdDays').value;
                arg.sInvestorLockRLckdD               = sInvestorLockRLckdD.value;
                arg.sInvestorLockRLckExpiredDLckd     = document.getElementById('sInvestorLockRLckExpiredDLckd').checked;
                arg.sInvestorLockRLckExpiredD         = sInvestorLockRLckExpiredD.value;
                arg.sInvestorLockDeliveryExpiredDLckd = document.getElementById('sInvestorLockDeliveryExpiredDLckd').checked;
                arg.sInvestorLockDeliveryExpiredD     = sInvestorLockDeliveryExpiredD.value;
                arg.Reason = document.getElementById('Reason').value;
                onClosePopup(arg);

            }
    </script>
</body>
</html>
