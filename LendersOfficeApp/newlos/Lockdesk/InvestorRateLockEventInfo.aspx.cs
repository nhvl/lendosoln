﻿namespace LendersOfficeApp.newlos.Lockdesk
{
    using System;
    using DataAccess;
    using LendersOffice.Common;

    public partial class InvestorRateLockEventInfo : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DisplayCopyRight = false;
            CPageData loanData = CPageData.CreateUsingSmartDependency(LoanID, typeof(InvestorRateLockEventInfo));
            loanData.InitLoad();

            Guid eventId = RequestHelper.GetGuid("eventid");

            InvestorRateLockHistoryItem item = InvestorRateLockHistoryItem.GetRateLockEventFromXml(loanData.sInvestorLockHistoryXmlContent, eventId);

            By.Text = item.DoneBy;
            sInvestorLockLpInvestorNm.Text = item.sInvestorLockLpInvestorNm;
            sInvestorLockLpTemplateNm.Text = item.sInvestorLockLpTemplateNm;
            sInvestorLockRLckdDays.Text = item.sInvestorLockRLckdDays;
            sInvestorLockRLckdD.Text = item.sInvestorLockRLckdD;
            sInvestorLockRLckExpiredD.Text = item.sInvestorLockRLckExpiredD;
            sInvestorLockDeliveryExpiredD.Text = item.sInvestorLockDeliveryExpiredD;
            sInvestorLockLoanNum.Text = item.sInvestorLockLoanNum;
            sInvestorLockProgramId.Text = item.sInvestorLockProgramId;
            sInvestorLockConfNum.Text = item.sInvestorLockConfNum;
            sInvestorLockRateSheetEffectiveTime.Text = item.sInvestorLockRateSheetEffectiveTime;
            sInvestorLockLockFee.Text = item.sInvestorLockLockFee;
            sInvestorLockCommitmentT.Text = item.sInvestorLockCommitmentT;
            sInvestorLockNoteIR.Text = item.sInvestorLockNoteIR;
            sInvestorLockBrokComp1Pc.Text = item.sInvestorLockBrokComp1Pc;
            sInvestorLockBrokComp1PcPrice.Text = item.sInvestorLockBrokComp1PcPrice;
            sInvestorLockRAdjMarginR.Text = item.sInvestorLockRAdjMarginR;
            sInvestorLockOptionArmTeaserR.Text = item.sInvestorLockOptionArmTeaserR;
            sInvestorLockBaseNoteIR.Text = item.sInvestorLockBaseNoteIR;
            sInvestorLockBaseBrokComp1PcFee.Text = item.sInvestorLockBaseBrokComp1PcFee;
            sInvestorLockBaseBrokComp1PcPrice.Text = item.sInvestorLockBaseBrokComp1PcPrice;
            sInvestorLockBaseRAdjMarginR.Text = item.sInvestorLockBaseRAdjMarginR;
            sInvestorLockBaseOptionArmTeaserR.Text = item.sInvestorLockBaseOptionArmTeaserR;
            sInvestorLockTotAdjNoteIR.Text = item.sInvestorLockTotAdjNoteIR;
            sInvestorLockTotAdjBrokComp1PcFee.Text = item.sInvestorLockTotAdjBrokComp1PcFee;
            sInvestorLockTotAdjBrokComp1PcPrice.Text = item.sInvestorLockTotAdjBrokComp1PcPrice;
            sInvestorLockTotAdjRAdjMarginR.Text = item.sInvestorLockTotAdjRAdjMarginR;
            sInvestorLockTotAdjOptionArmTeaserR.Text = item.sInvestorLockTotAdjOptionArmTeaserR;

            EventType.Text = item.Action;
            CausedOn.Text = item.EventDate;

            AdjustmentDataRepeater.DataSource = item.pricingAdjustments;
            AdjustmentDataRepeater.DataBind();

            if (item.pricingAdjustments.Count == 0)
            {
                AdjustmentDataRepeater.Visible = false;
            }
        }
    }
}