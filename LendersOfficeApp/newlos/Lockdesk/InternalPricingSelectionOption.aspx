﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InternalPricingSelectionOption.aspx.cs" Inherits="LendersOfficeApp.newlos.Lockdesk.InternalPricingSelectionOption" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style>
        html, body {
            height: 90%;
        }

        #mainFrm {
            width: 100%;
            height: 600px;
        }

        .main-container {
            padding:5px; 
            font-weight:bold; 
            height: 100%
        }
    </style>
</head>
<body>
  <script type="text/javascript">
    function _init()
    {
      resize(780, 700);
    }
          var g_checkBoxOptionList = [
      <%=AspxTools.JsGetClientIdString(m_cbSetInvestorRateLock) %>, 
      <%=AspxTools.JsGetClientIdString(m_cbAddProgramConditions) %>
      ];
    function f_onOkClick()
    {
      var args = {};
      args.LoanId = ML.sLId;
      args.ProductId = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("productid")) %>;
      args.sPricingModeT = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("sPricingModeT")) %>;
      args.Rate = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("rate")) %>;
      args.Fee = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("fee")) %>;
      args.historicalPolicy = <%= AspxTools.JsString(RequestHelper.GetInt("historicalPolicy",0)) %>;
      args.historicalRateOption = <%= AspxTools.JsString(RequestHelper.GetInt("historicalRateOption",0)) %>;
      args.isHistorical = <%= AspxTools.JsBool(RequestHelper.GetBool("isHistorical"))%>;
      args.snapshotJson = <%= AspxTools.JsString(HttpUtility.HtmlDecode(RequestHelper.GetSafeQueryString("snapshotJson"))) %>;

      for (var i = 0; i < g_checkBoxOptionList.length; i++)
      {
        var cb = document.getElementById(g_checkBoxOptionList[i]);
        if (null != cb)
        {
          args[cb.id] = cb.checked ? 'True' : 'False';
        }
      }
      var result = gService.loanedit.call("PerformAction", args);
      if (!result.error)
      {
          var arg = {};
          arg.isSubmit = true;
          onClosePopup(arg);
      }
      else
      {
        alert('Unable to complete selected action. Please try again.');
      }
    }
    function f_close()
    {
      onClosePopup();
    }
    function f_onCheckboxClick()
    {

      
      for (var i = 0; i < g_checkBoxOptionList.length; i++)
      {
        var cb = document.getElementById(g_checkBoxOptionList[i]);
        if (null != cb && cb.checked)
        {
          document.getElementById("btnOk").disabled = false;
          return;
        }
        
      }
      document.getElementById("btnOk").disabled = true;
    }
  </script>
    <form id="form1" runat="server">
    <div class="main-container">
      <asp:CheckBox ID="m_cbSetInvestorRateLock" Text="Set investor rate lock pricing" runat="server" onclick="f_onCheckboxClick();"/>
      <br />
      <asp:CheckBox ID="m_cbAddProgramConditions" Text="Add program conditions" runat="server" onclick="f_onCheckboxClick();"/>
      <br />
      <iframe id="mainFrm" src=<%= AspxTools.SafeUrl(m_frmBodyUrl) %> ></iframe>
    </div>
    <table width="100%">
      <tr><td align="center"><input type="button" value="OK" onclick="f_onOkClick();" id="btnOk" disabled="disabled"/>&nbsp;&nbsp;<input type="button" value="Cancel" onclick="f_close();" /></td></tr>
    </table>
    </form>
</body>
</html>
