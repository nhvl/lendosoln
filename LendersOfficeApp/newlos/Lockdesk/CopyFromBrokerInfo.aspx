﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CopyFromBrokerInfo.aspx.cs"
    Inherits="LendersOfficeApp.newlos.Lockdesk.CopyFromBrokerInfo" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Populate fields from Broker Rate Lock</title>

    <script type="text/javascript" src="../../inc/json.js"></script>

    <style type="text/css">
        body
        {
            background-color: gainsboro;
        }
        .rateField, .dayField
        {
            width: 50px;
        }
        .pname
        {
            width: 300px;
        }
        .col1
        {
            width: 50px;
        }
        .col2
        {
            width: 175px;
        }
        .wrapper
        {
            padding: 5px;
        }
        .centeringDiv
        {
            margin-right: auto;
            margin-left: auto;
            text-align: center;
        }
    </style>
</head>
<body>
    <h4 class="page-header">Populate fields from Front-end Rate Lock</h4>
    <form id="form1" runat="server" onreset="return false;">
    <div class="wrapper">
        <table cellpadding="1" cellspacing="1">
            <thead>
                <tr>
                    <td class="FieldLabel col1">
                        Include?
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </thead>
            <tbody>
<%--                <asp:PlaceHolder runat="server" ID="InvestorSection">
                    <tr>
                        <td align="right">
                            <asp:CheckBox runat="server" Checked="true" ID="InvestorNmCb" />
                        </td>
                        <td class="FieldLabel col2 ">
                            Investor
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="sLpInvestorNm" ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                </asp:PlaceHolder>--%>
                <tr>
                    <td align="right">
                        <asp:CheckBox runat="server" ID="sLpTemplateNmCB" />
                    </td>
                    <td class="FieldLabel">
                        Program Name
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="sLpTemplateNm" CssClass="pname" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <asp:PlaceHolder runat="server" ID="LockBufferSection">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td class="FieldLabel">
                            Front-end Lock Period
                        </td>
                        <td class="FieldLabel">
                            <asp:TextBox CssClass="dayField" runat="server" ID="sRLckdDays" ReadOnly="true"></asp:TextBox>days
                            <span style="padding-left: 20px">Lock Buffer</span>
                            <asp:TextBox runat="server" ID="sInvestorLockLockBuffer" CssClass="dayField" ReadOnly="true"></asp:TextBox>days
                        </td>
                    </tr>
                </asp:PlaceHolder>
                <tr>
                    <td align="right">
                        <asp:CheckBox runat="server" Checked="true" ID="InvestorLockPeriodCb" />
                    </td>
                    <td class="FieldLabel">
                        Investor Lock Period
                    </td>
                    <td>
                        <asp:TextBox CssClass="dayField" runat="server" ID="InvestorLockPeriod" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <asp:PlaceHolder runat="server" ID="RateSheetEffTimeSection">
                    <tr>
                        <td align="right">
                            <asp:CheckBox runat="server" Checked="true" ID="sBrokerLockRateSheetEffectiveDCB" />
                        </td>
                        <td class="FieldLabel">
                            Rate Sheet Effective Time
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="sBrokerLockRateSheetEffectiveD" ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                </asp:PlaceHolder>
            </tbody>
        </table>
        <br />
        <br />
        <table cellpadding="1" cellspacing="1">
            <tbody>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td class="FieldLabel">
                        Rate
                    </td>
                    <td class="FieldLabel">
                        Price
                    </td>
                    <td class="FieldLabel">
                        Fee
                    </td>
                    <td class="FieldLabel">
                        Margin
                    </td>
                    <td class="FieldLabel">
                        Teaser<br />
                        Rate
                    </td>
                </tr>
                <tr>
                    <td align="right" class="col1">
                        <asp:CheckBox runat="server" ID="BasePricingCB" Checked="true" />
                    </td>
                    <td class="FieldLabel col2 ">
                        Base Pricing
                    </td>
                    <td>
                        <asp:TextBox runat="server" CssClass="rateField" ReadOnly="true" preset="percent"
                            ID="sBrokerLockBaseNoteIR"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" CssClass="rateField" ReadOnly="true" preset="percent"
                            ID="sBrokerLockBaseBrokComp1PcPrice"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" CssClass="rateField" ReadOnly="true" preset="percent"
                            ID="sBrokerLockBaseBrokComp1PcFee"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" CssClass="rateField" ReadOnly="true" preset="percent"
                            ID="sBrokerLockBaseRAdjMarginR"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" CssClass="rateField" ReadOnly="true" preset="percent"
                            ID="sBrokerLockBaseOptionArmTeaserR"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td class="FieldLabel">
                        Total Included Adjustments
                    </td>
                    <td>
                        <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sIncludedAdjustmentRate"
                            ReadOnly="true"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sIncludedAdjustmentPrice"
                            ReadOnly="true"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sIncludedAdjustmentFee"
                            ReadOnly="true"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sIncludedAdjustmentMargin"
                            ReadOnly="true"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sIncludedAdjustmentTRate"
                            ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td class="FieldLabel">
                        Total
                    </td>
                    <td>
                        <asp:TextBox runat="server" CssClass="rateField" ReadOnly="true" preset="percent"
                            ID="sInvestorTotalRate"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" CssClass="rateField" ReadOnly="true" preset="percent"
                            ID="sInvestorTotalPrice"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" CssClass="rateField" ReadOnly="true" preset="percent"
                            ID="sInvestorTotalFee"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" CssClass="rateField" ReadOnly="true" preset="percent"
                            ID="sInvestorTotalMargin"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" CssClass="rateField" ReadOnly="true" preset="percent"
                            ID="sInvestorTotalTRate"></asp:TextBox>
                    </td>
                </tr>
            </tbody>
        </table>
        <br />
        <table cellpadding="1" cellspacing="1">
            <!-- For layout purposes -->
            <tr>
                <td>
                    <asp:Repeater runat="server" ID="BrokerAdjustments" OnItemDataBound="BrokerAdjustments_DataBind">
                        <HeaderTemplate>
                            <div class="FieldLabel">
                                Front-end Adjustment
                            </div>
                            <table cellpadding="1" cellspacing="0" id="BrokerAdjustmentTable">
                                <thead>
                                    <tr class="GridHeader">
                                        <td class="FieldLabel rateField">
                                            Include?
                                        </td>
                                        <td class="FieldLabel">
                                            Description
                                        </td>
                                        <td class="FieldLabel rateField">
                                            Rate
                                        </td>
                                        <td class="FieldLabel rateField">
                                            Price
                                        </td>
                                        <td class="FieldLabel rateField">
                                            Fee
                                        </td>
                                        <td class="FieldLabel rateField">
                                            Margin
                                        </td>
                                        <td class="FieldLabel rateField">
                                            Teaser<br />
                                            Rate
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="GridItem">
                                <td align="center">
                                    <asp:CheckBox runat="server" ID="AdjustmentCB" />
                                </td>
                                <td>
                                    <%# AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Description) %>
                                </td>
                                <td>
                                    <%# AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Rate) %>
                                </td>
                                <td>
                                    <%# AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Price) %>
                                </td>
                                <td>
                                    <%# AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Fee) %>
                                </td>
                                <td>
                                    <%# AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Margin) %>
                                </td>
                                <td>
                                    <%# AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).TeaserRate) %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="GridAlternatingItem">
                                <td align="center">
                                    <asp:CheckBox runat="server" ID="AdjustmentCB" />
                                </td>
                                <td>
                                    <%# AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Description) %>
                                </td>
                                <td>
                                    <%# AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Rate) %>
                                </td>
                                <td>
                                    <%# AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Price) %>
                                </td>
                                <td>
                                    <%# AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Fee) %>
                                </td>
                                <td>
                                    <%# AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Margin) %>
                                </td>
                                <td>
                                    <%# AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).TeaserRate) %>
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
        <br />
        <div class="centeringDiv">
            <input type="button" runat="server" class="ButtonStyle" id="Button1" value="Import"
                onclick="ImportFields()" />
            <input type="button" runat="server" class="ButtonStyle" id="Import" value="Cancel"
                onclick="cancel()" />
        </div>
    </div>
    <uc1:cModalDlg runat="server" ID="uc1" />

    <script type="text/javascript">
        function _init(){
            if (Adjustments.length <= 4) {
                var newHeight = 400 + (Adjustments.length*36);
                resizeForIE6And7(600, newHeight);
            } else {
                resizeForIE6And7(600,544);
            }
        }
        
        function refreshPricing(){
            trace('Running Refresh Pricing');
            var adjustments = getSelectedAdjustments();
            adjustments = JSON.stringify(adjustments);
            trace(adjustments);
            
            var basePricingFields = [ 
                'sBrokerLockBaseNoteIR', 
                'sBrokerLockBaseBrokComp1PcPrice', 
                'sBrokerLockBaseBrokComp1PcFee', 
                'sBrokerLockBaseRAdjMarginR', 
                'sBrokerLockBaseOptionArmTeaserR' ];
                
            var data = {};
            data.loanId = document.getElementById('loanid').value;
            data.includedAdjustments = adjustments; 
             
            for( var i = 0; i<basePricingFields.length; i++){
                var field = basePricingFields[i];
                data[field] = document.getElementById(field).value;
            }
            
                trace('Begin service call');
            
            var result = gService.loanedit.call("PopulateCalcRefresh", data);
                trace('End service call');
            
            if(result.error){
                alert('Error: ' + result.UserMessage);
                return;
            }
            
            var totalPricingFields = [
                'sInvestorTotalRate',
                'sInvestorTotalPrice',
                'sInvestorTotalFee',
                'sInvestorTotalMargin',
                'sInvestorTotalTRate',
                'sIncludedAdjustmentRate',
                'sIncludedAdjustmentPrice',
                'sIncludedAdjustmentFee',
                'sIncludedAdjustmentMargin',
                'sIncludedAdjustmentTRate'
            ];
            
            for( var i = 0; i<totalPricingFields.length; i++){
                var field = totalPricingFields[i];
                document.getElementById(field).value = result.value[field];
            } 
            
            trace('Pricing Update Finish');
            
        }
        
        function trace(s) {
        };
        
        
        function cancel(){
            onClosePopup();
        }
        
        function getSelectedAdjustments(){
            trace('GetSelected Adjustments');
            if( Adjustments.length === 0 ){
                return [];
            }
            var table = document.getElementById('BrokerAdjustmentTable');
            var body = table.tBodies[0];
            var rows = body.rows;
            var includedAdjustments = [];
            
            trace(rows.length + ' rows found.');
            for(var i = 0; i < rows.length; i++){
                trace('Row ' + i);
                var row = rows[i];
                
                var inputs = row.getElementsByTagName('input');
                trace(inputs.length + ' inputs found.');
                for( var y = 0; y < inputs.length; y++){
                    var input = inputs[y];
                    trace('Input ' + y + ' Type name: ' + input.type + ' ID ' + input.id);                    
                    if( input.id.indexOf('AdjustmentCB') >= 0 ){
                        if( input.checked ){
                            trace('input is checked');
                            includedAdjustments.push(Adjustments[i]);
                            
                            break;
                        }
                    }
                }
            }

            return includedAdjustments;
        }
        
        
        function ImportFields(){
            var args = getModalArgs() || {};
            var g = function(id) { return document.getElementById(id); };
            
//            args.UseInvestorNm = g('InvestorNmCb') != null && g('InvestorNmCb').checked;
//            if(args.UseInvestorNm){
//                args.InvestorNm = g('sLpInvestorNm').value;
//            }
            
            args.UsesLpTemplateNm = g('sLpTemplateNmCB').checked;
            if(args.UsesLpTemplateNm){
                args.sLpTemplateNm = g('sLpTemplateNm').value;
            }
            
            args.UseInvestorLockPeriod = g('InvestorLockPeriodCb').checked;
            if(args.UseInvestorLockPeriod){
                args.InvestorLockPeriod= g('InvestorLockPeriod').value;
            }
            
            args.UsesBrokerLockRateSheetEffectiveD = g('sBrokerLockRateSheetEffectiveDCB') != null && g('sBrokerLockRateSheetEffectiveDCB').checked;
            if(args.UsesBrokerLockRateSheetEffectiveD){
                args.sBrokerLockRateSheetEffectiveD = g('sBrokerLockRateSheetEffectiveD').value;
            }
            
            args.UseBasePricingCB = g('BasePricingCB').checked;
            if(args.UseBasePricingCB){
                args.rate = g('sBrokerLockBaseNoteIR').value;
                args.price = g('sBrokerLockBaseBrokComp1PcPrice').value;
                args.fee = g('sBrokerLockBaseBrokComp1PcFee').value;
                args.margin = g('sBrokerLockBaseRAdjMarginR').value;
                args.trate = g('sBrokerLockBaseOptionArmTeaserR').value;
            }
            
            args.adjustments = getSelectedAdjustments();
            
            args.OK = true;
            
            onClosePopup(args);   
        }


    </script>

    </form>
</body>
</html>
