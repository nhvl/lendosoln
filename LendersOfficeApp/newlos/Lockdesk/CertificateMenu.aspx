﻿<%@ Import Namespace="LendersOffice.Common" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CertificateMenu.aspx.cs" Inherits="LendersOfficeApp.newlos.Lockdesk.CertificateMenu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
  <script type="text/javascript">
      function f_printMe() {
          lqbPrintByFrame(parent.frmBody);
      }
      function f_email() {
          window.parent.openEmail();
      }
  </script>
    <form id="form1" runat="server">
    <div>

    <%if(  RequestHelper.GetBool("detailCert") ) {%>
    <input type="button" id="btnNormalCert" value="Show Normal Certificate" onclick="parent.showNormalCert();"  class="ButtonStyle" nohighlight/>
    &nbsp;&nbsp;
    <%}%>

    <input type="button" id="btnPrint" value="Print ..." onclick="f_printMe();" class="ButtonStyle" NoHighlight />
    &nbsp;&nbsp;
    <input type="button" id="btnEmail" value="Email ..." onclick="f_email();" class="ButtonStyle" NoHighlight />
    &nbsp;&nbsp;
    <input type="button" id="btnClose" value="Close" onclick="parent.onClosePopup();" class="ButtonStyle" NoHighlight />
    </div>
    </form>
</body>
</html>
