﻿namespace LendersOfficeApp.newlos.Lockdesk
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Security;
    using LendersOfficeApp.newlos.Underwriting;

    public partial class BrokerRateLock : BaseLoanPage
    {
        protected bool UseInternalPricerV2
        {
            get
            {
                return this.BrokerUser.UseInternalPricerV2;
            }
        }

        protected string GetInternalPricerUrl()
        {
            var baseUrl = this.UseInternalPricerV2
                ? "RunInternalPricingV2.aspx"
                : "RunInternalPricing.aspx";

            return baseUrl + $"?sPricingModeT={E_sPricingModeT.InternalBrokerPricing.ToString("D")}";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageID = "BrokerRateLock";   
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowLockDeskRead };
            }
        }

        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowLockDeskWrite };
            }
        }

        protected override void LoadData()
        {
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;

            CPageData pageData = CPageData.CreateUsingSmartDependency(LoanID, typeof(BrokerRateLock));
            pageData.InitLoad();

            pageData.LoadBrokerLockData();
            sLpTemplateNm.Text = pageData.sLpTemplateNm;
            sStatusT.Text = pageData.sStatusT_rep;
            sSubmitD.Text = pageData.sSubmitD_rep;
            sSubmitN.Text = pageData.sSubmitN;
            sRateLockStatusT.Text = pageData.sRateLockStatusT_rep;
            sRLckdDays.Text = pageData.sRLckdDays_rep;
            sRLckdD.Text = pageData.sRLckdD_rep;
            sRLckdDTime.Text = pageData.sRLckdDTime_rep;
            sRLckdN.Text = pageData.sRLckdN;
            sRLckdExpiredD.Text = pageData.sRLckdExpiredD_rep;
            sRLckdExpiredN.Text = pageData.sRLckdExpiredN;
            sTrNotes.Text = pageData.sTrNotes;
            sLpTemplateNmSubmitted.Text = pageData.sLpTemplateNmSubmitted;

            sBrokerLockBaseNoteIR.Text = pageData.sBrokerLockBaseNoteIR_rep;
            sBrokerLockBaseBrokComp1PcPrice.Text = pageData.sBrokerLockBaseBrokComp1PcPrice;
            sBrokerLockBaseBrokComp1PcFee.Text = pageData.sBrokerLockBaseBrokComp1PcFee_rep;
            sBrokerLockBaseRAdjMarginR.Text = pageData.sBrokerLockBaseRAdjMarginR_rep;
            sBrokerLockBaseOptionArmTeaserR.Text = pageData.sBrokerLockBaseOptionArmTeaserR_rep;

            sBrokerLockBrokerBaseNoteIR.Text = pageData.sBrokerLockBrokerBaseNoteIR_rep;
            sBrokerLockBrokerBaseBrokComp1PcPrice.Text = pageData.sBrokerLockBrokerBaseBrokComp1PcPrice;
            sBrokerLockBrokerBaseBrokComp1PcFee.Text = pageData.sBrokerLockBrokerBaseBrokComp1PcFee_rep;
            sBrokerLockBrokerBaseRAdjMarginR.Text = pageData.sBrokerLockBrokerBaseRAdjMarginR_rep;
            sBrokerLockBrokerBaseOptionArmTeaserR.Text = pageData.sBrokerLockBrokerBaseOptionArmTeaserR_rep;

            sBrokerLockTotHiddenAdjNoteIR.Text = pageData.sBrokerLockTotHiddenAdjNoteIR_rep;
            sBrokerLockTotHiddenAdjBrokComp1PcPrice.Text = pageData.sBrokerLockTotHiddenAdjBrokComp1PcPrice;
            sBrokerLockTotHiddenAdjBrokComp1PcFee.Text = pageData.sBrokerLockTotHiddenAdjBrokComp1PcFee_rep;
            sBrokerLockTotHiddenAdjRAdjMarginR.Text = pageData.sBrokerLockTotHiddenAdjRAdjMarginR_rep;
            sBrokerLockTotHiddenAdjOptionArmTeaserR.Text = pageData.sBrokerLockTotHiddenAdjOptionArmTeaserR_rep;

            sBrokerLockTotVisibleAdjNoteIR.Text = pageData.sBrokerLockTotVisibleAdjNoteIR_rep;
            sBrokerLockTotVisibleAdjBrokComp1PcPrice.Text = pageData.sBrokerLockTotVisibleAdjBrokComp1PcPrice;
            sBrokerLockTotVisibleAdjBrokComp1PcFee.Text = pageData.sBrokerLockTotVisibleAdjBrokComp1PcFee_rep;
            sBrokerLockTotVisibleAdjRAdjMarginR.Text = pageData.sBrokerLockTotVisibleAdjRAdjMarginR_rep;
            sBrokerLockTotVisibleAdjOptionArmTeaserR.Text = pageData.sBrokerLockTotVisibleAdjOptionArmTeaserR_rep;
            sBrokerLockFinalBrokComp1PcPrice.Text = pageData.sBrokerLockFinalBrokComp1PcPrice_rep;

          
            sBrokerLockOriginatorPriceNoteIR.Text = pageData.sBrokerLockOriginatorPriceNoteIR_rep;
            sBrokerLockOriginatorPriceBrokComp1PcFee.Text = pageData.sBrokerLockOriginatorPriceBrokComp1PcFee_rep;
            sBrokerLockOriginatorPriceBrokComp1PcPrice.Text = pageData.sBrokerLockOriginatorPriceBrokComp1PcPrice;
            sBrokerLockOriginatorPriceRAdjMarginR.Text = pageData.sBrokerLockOriginatorPriceRAdjMarginR_rep;
            sBrokerLockOriginatorPriceOptionArmTeaserR.Text = pageData.sBrokerLockOriginatorPriceOptionArmTeaserR_rep;


            sBrokerLockOriginatorCompAdjNoteIR.Text = pageData.sBrokerLockOriginatorCompAdjNoteIR_rep;
            sBrokerLockOriginatorCompAdjBrokComp1PcFee.Text = pageData.sBrokerLockOriginatorCompAdjBrokComp1PcFee_rep;
            sBrokerLockOriginatorCompAdjBrokComp1PcPrice.Text = pageData.sBrokerLockOriginatorCompAdjBrokComp1PcPrice;
            sBrokerLockOriginatorCompAdjRAdjMarginR.Text = pageData.sBrokerLockOriginatorCompAdjRAdjMarginR_rep;
            sBrokerLockOriginatorCompAdjOptionArmTeaserR.Text = pageData.sBrokerLockOriginatorCompAdjOptionArmTeaserR_rep;
            bool ByPassFieldSecurityCheckTmp = pageData.ByPassFieldSecurityCheck;
            pageData.ByPassFieldSecurityCheck = true;

            sOriginatorCompensationAmount.Text = pageData.sOriginatorCompensationAmount_rep;
            sOriginatorCompensationAmount.ReadOnly = true;
            sOriginatorCompNetPoints.Text = pageData.sOriginatorCompNetPoints_rep;
            sOriginatorCompNetPoints.ReadOnly = true;
            Tools.Bind_PercentBaseLoanAmountsT(sOriginatorCompensationBaseT);

            switch ( pageData.sOriginatorCompensationPaymentSourceT )
            {
                case E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified:
                    sOriginatorCompensationPaymentSourceT.Text = "Payment source not specified";
                    break;
                case E_sOriginatorCompensationPaymentSourceT.BorrowerPaid:
                    sOriginatorCompensationPaymentSourceT.Text = "Borrower paid";
                    break;
                case E_sOriginatorCompensationPaymentSourceT.LenderPaid:
                    sOriginatorCompensationPaymentSourceT.Text = "Lender paid";
                    sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees.Visible = true;
                    sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees.Checked = pageData.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;

                    if (pageData.sHasOriginatorCompensationPlan)
                    {
                        OriginatorPlanDetails.Text = Tools.GetOrigCompPlanDesc(pageData.sOriginatorCompensationPlanT, pageData.sOriginatorCompensationAppliedBy, pageData.sOriginatorCompensationPlanSourceEmployeeName);
                        sOriginatorCompensationPlanAppliedD.Text = pageData.sOriginatorCompensationPlanAppliedD_rep;
                        sOriginatorCompensationEffectiveD.Text = pageData.sOriginatorCompensationEffectiveD_rep;
                        sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo.Checked = pageData.sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo;
                        sOriginatorCompensationPercent.Text = pageData.sOriginatorCompensationPercent_rep;

                        sOriginatorCompensationAdjBaseAmount.Text = pageData.sOriginatorCompensationAdjBaseAmount_rep;
                        sOriginatorCompensationMinAmount.Text = pageData.sOriginatorCompensationMinAmount_rep;
                        sOriginatorCompensationMaxAmount.Text = pageData.sOriginatorCompensationMaxAmount_rep;
                        sOriginatorCompensationFixedAmount.Text = pageData.sOriginatorCompensationFixedAmount_rep;
                        sOriginatorCompensationTotalAmount.Text = pageData.sOriginatorCompensationTotalAmount_rep;
                        sOriginatorCompensationHasMinAmount.Checked = pageData.sOriginatorCompensationHasMinAmount;
                        sOriginatorCompensationHasMaxAmount.Checked = pageData.sOriginatorCompensationHasMaxAmount;
                        OrigCompLenderDetailsLink.Visible = true;
                        Tools.SetDropDownListValue(sOriginatorCompensationBaseT, pageData.sOriginatorCompensationBaseT);
                        OriginatorCompLenderDetails.Visible = true;
                    }
                    break;
                default:
                    break;
            }
            pageData.ByPassFieldSecurityCheck = ByPassFieldSecurityCheckTmp;

            sIsRateLocked.Value = pageData.sIsRateLocked.ToString();
            sIsOptionArm.Value = pageData.sIsOptionArm.ToString();

            sNoteIR.Text = pageData.sNoteIR_rep;
            sRAdjMarginR.Text = pageData.sRAdjMarginR_rep;
            sOptionArmTeaserR.Text = !pageData.sIsOptionArm ? "0.000%" : pageData.sOptionArmTeaserR_rep; 
            sBrokComp1Pc.Text = pageData.sBrokComp1Pc_rep;

            switch (pageData.sBrokerLockPriceRowLockedT)
            {
                case E_sBrokerLockPriceRowLockedT.Base:
                    sBrokerLockPriceRowLockedT_Base.Checked = true;
                    break;
                case E_sBrokerLockPriceRowLockedT.BrokerBase:
                    sBrokerLockPriceRowLockedT_BrokerBase.Checked = true;
                    break;
                case E_sBrokerLockPriceRowLockedT.BrokerFinal:
                    sBrokerLockPriceRowLockedT_BrokerFinal.Checked = true;
                    break;
                case E_sBrokerLockPriceRowLockedT.OriginatorPrice:
                    sBrokerLockPriceRowLockedT_OriginatorPrice.Checked = true;
                    break;
                default:
                    throw new UnhandledEnumException(pageData.sBrokerLockPriceRowLockedT);
            }

            bool isSecondLien = pageData.sLienPosT == E_sLienPosT.Second;
            bool hasLinkedLoan = pageData.sLinkedLoanInfo.IsLoanLinked;

            LinkedLoanId.Visible = hasLinkedLoan;
            ViewLinkedLoan.Visible = hasLinkedLoan;

            //From obsolete rate lock page js
            if (hasLinkedLoan)
            {
                LinkedLoanId.Value = pageData.sLinkedLoanInfo.LinkedLId.ToString();
            }

            if (!isSecondLien || isSecondLien && !hasLinkedLoan)
            {
                Prepare1stLoanForResubmissions.Visible = true;
            }
            else if (hasLinkedLoan)
            {
                Prepare2ndLoanForResubmissionLinked.Visible = true;
            }
            else
            {
                Prepare2ndLoanForResubmissionNotLinked.Visible = true;
            }

            sLpeNotesFromBrokerToUnderwriterHistory.Text = pageData.sLpeNotesFromBrokerToUnderwriterHistory;
            sRateLockHistoryXmlContent.Text = RateLockHistoryTable.BuildBrokerRateLockHistoryTable(pageData.sRateLockHistoryXmlContent);

            // 6/05/07 mf. OPM 12061. There is now a corporate setting for editing
            // the loan program name.
            if (! BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowEditingLoanProgramName))
            {
                sLpTemplateNm.ReadOnly = true;
            }

            if (BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) == false)
            {
                AllowSelectNewProgram.Visible = false;
                MessageToLenderPrint.Visible = false;
                MessageToLenderHeader.Text = "&nbsp;";
                sLpeNotesFromBrokerToUnderwriterHistory.Visible = false;
            }

            List<string> reasonNotAllowList = new List<string>();
            foreach (E_sReasonNotAllowingResubmission reason in pageData.sReasonForNotAllowingResubmission)
            {
                reasonNotAllowList.Add(Tools.Get_sReasonNotAllowingResubmission_Action(reason));
            }

            if (pageData.sHasRateLockConfirmationPdf)
            {
                var pdf = new LendersOffice.Pdf.CRateLockConfirmationPDF();
                storedRateLockCertInfo.Text = "Rate lock confirmation issued " + pageData.sRateLockConfirmationPdfLastSavedDFriendlyDisplay + " ";
                string link = string.Format(
                    "LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload('{0}/pdf/{1}.aspx?loanid={2}&crack={3}');",
                    VirtualRoot,
                    pdf.Name,
                    LoanID,
                    DateTime.Now);
                storedRateLockCertInfo.Text += string.Format("(<a onclick=\"{0}\">view</a>)", link);
                // (view) links to the saved certificate PDF
            }

            RegisterJsObject("PricingAdjustments", pageData.sBrokerLockAdjustments);
            RegisterJsObject("aReasonsNotAllow", reasonNotAllowList);

            RegisterJsScript("AdjustmentTable.js");

            sFERateRenegotiationCount.Text = "" + pageData.sFERateRenegotiationCount;
            sFECurrentLockExtensionCount.Text = "" + pageData.sFECurrentLockExtensionCount;
            sFETotalLockExtensionCount.Text = "" + pageData.sFETotalLockExtensionCount;
            sFETotalRateReLockCount.Text = "" + pageData.sFETotalRateReLockCount;

            sFERateRenegotiationCountLckd.Checked = pageData.sFERateRenegotiationCountLckd;
            sFECurrentLockExtensionCountLckd.Checked = pageData.sFECurrentLockExtensionCountLckd;
            sFETotalLockExtensionCountLckd.Checked = pageData.sFETotalLockExtensionCountLckd;
            sFETotalRateReLockCountLckd.Checked = pageData.sFETotalRateReLockCountLckd;

            if ( pageData.BrokerDB.EnableAutoPriceEligibilityProcess)
            {
                AutoPriceProcessResultPh.Visible = true;
                sAutoPriceProcessResult.Text = pageData.sAutoPriceProcessResultT_rep;
                sAutoPriceProcessResultD.Text = pageData.sAutoPriceProcessResultD_rep;
            }
            else
            {
                AutoPriceProcessResultPh.Visible = false;
            }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            // Init your user controls here, i.e: bind drop down list.
            this.PageTitle = "Broker Rate Lock";
            this.PageID = "BrokerRateLock";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CRateLockConfirmationPDF);

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);
        }

        override protected void OnInit(EventArgs e)
        {
            int l = VirtualRoot.Length;
            string url = Request.Path.Substring(l, Request.Path.Length - l - 5) + "Service.aspx";
            RegisterService("loanedit", url);

            ClientScript.RegisterHiddenField("loanid", LoanID.ToString());

            IsAppSpecific = false;
            base.OnInit(e);
        }
    }
}
