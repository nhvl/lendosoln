﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" EnableViewState="false" CodeBehind="RateLockEventInfo.aspx.cs" Inherits="LendersOfficeApp.newlos.Lockdesk.RateLockEventInfo" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Audit History Details - Broker Rate Lock</title>
    <style type="text/css">
        body { background-color: gainsboro; padding: 5px;}
       input { margin-left: 250px; }
              .rate { width: 50px; }
       .desc { width: 195px; }
    </style>
</head>
<body>
    <form id="form1" runat="server" onreset="return false;">
    <div>
        <br />
        <ml:EncodedLabel runat="server" ID="EventType" CssClass="FieldLabel"></ml:EncodedLabel>
        <br />
        <br />
        <span class="FieldLabel">Date: </span><ml:EncodedLabel runat="server" ID="CausedOn"></ml:EncodedLabel>
        <br />
        <span class="FieldLabel">By: </span><ml:EncodedLabel runat="server" ID="By"></ml:EncodedLabel>
        <br />
        <span class="FieldLabel">Program Name: </span><ml:EncodedLabel runat="server" ID="ProgramName"></ml:EncodedLabel>
        <br />
        <span class="FieldLabel">Lock Period: </span><ml:EncodedLabel runat="server" ID="LockPeriod"></ml:EncodedLabel>
        <br />
        <span class="FieldLabel">Rate Lock Date: </span><ml:EncodedLabel runat="server" ID="RateLockDate"></ml:EncodedLabel>
        <br />
        <span class="FieldLabel">Rate Lock Expiration: </span><ml:EncodedLabel runat="server" ID="RateLockExpiration"></ml:EncodedLabel>
        <br />
        <span class="FieldLabel">Prepayment Penalty Period: </span><ml:EncodedLabel runat="server" ID="PrepaymentPenalty"></ml:EncodedLabel>
        <br />
        <span class="FieldLabel">Rate Sheet Effective Time: </span><ml:EncodedLabel runat="server" ID="RateSheetEffectiveTime"></ml:EncodedLabel>
        <br />
        <br />
        <table cellpadding="2" cellspacing="0" border="1">
            <tbody>
                <tr>
                    <td  class="desc">&nbsp;</td>
                    <td class="FieldLabel rate" align="center">Rate</td>
                    <td class="FieldLabel rate" align="center">Price</td>
                    <td class="FieldLabel rate" align="center">Fee</td>
                    <td class="FieldLabel rate" align="center">Margin</td>
                    <td class="FieldLabel rate" align="center">Teaser<br />Rate</td>
                </tr>
                <tr>
                    <td class="FieldLabel"> Base Price </td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockBaseNoteIR"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockBaseBrokComp1PcPrice"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockBaseBrokComp1PcFee"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockBaseRAdjMarginR"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockBaseOptionArmTeaserR"></ml:EncodedLiteral></td>
                </tr>
                <tr>
                    <td class="FieldLabel"> Total Hidden Adjustments </td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockTotHiddenAdjNoteIR"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockTotHiddenAdjBrokComp1PcPrice"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockTotHiddenAdjBrokComp1PcFee"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockTotHiddenAdjRAdjMarginR"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockTotHiddenAdjOptionArmTeaserR"></ml:EncodedLiteral></td>
                </tr>
                <tr>
                    <td class="FieldLabel"> Broker Base Price </td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockBrokerBaseNoteIR"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockBrokerBaseBrokComp1PcPrice"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockBrokerBaseBrokComp1PcFee"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockBrokerBaseRAdjMarginR"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockBrokerBaseOptionArmTeaserR"></ml:EncodedLiteral></td>
                </tr>
                <tr>
                    <td class="FieldLabel"> Total  Visible Adjustments </td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockTotVisibleAdjNoteIR"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockTotVisibleAdjBrokComp1PcPrice"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockTotVisibleAdjBrokComp1PcFee"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockTotVisibleAdjRAdjMarginR"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockTotVisibleAdjOptionArmTeaserR"></ml:EncodedLiteral></td>
                </tr>
                <tr>
                    <td class="FieldLabel"> Broker Final Price </td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sNoteIR"></ml:EncodedLiteral> </td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokerLockFinalBrokComp1PcPrice"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sBrokComp1Pc"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sRAdjMarginR"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sOptionArmTeaserR"></ml:EncodedLiteral></td>
                </tr>
            </tbody>
        </table>

        <br />
        <br />

        <asp:Repeater runat="server" ID="AdjustmentDataRepeater">
            <HeaderTemplate>
                <table cellpadding="2" cellspacing="0" border="1" >
                    <thead>
                        <tr>
                            <th class="FieldLabel desc" align="center">
                                Adjustments
                            </th>
                            <th class="FieldLabel rate" align="center">
                                Rate
                            </th>
                            <th class="FieldLabel rate" align="center">
                                Price
                            </th>
                            <th class="FieldLabel rate" align="center">
                                Fee
                            </th>
                            <th class="FieldLabel rate" align="center">
                                Margin
                            </th>
                            <th class="FieldLabel rate" align="center">
                                Teaser<br />
                                Rate
                            </th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td >
                        <%# LendersOffice.AntiXss.AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Description )%>
                    </td>
                    <td align="right">
                        <%# LendersOffice.AntiXss.AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Rate )%>
                    </td>
                    <td align="right">
                        <%# LendersOffice.AntiXss.AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Price)%>
                    </td>
                    <td align="right">
                        <%# LendersOffice.AntiXss.AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Fee)%>
                    </td>
                    <td align="right">
                        <%# LendersOffice.AntiXss.AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Margin)%>
                    </td>
                    <td align="right">
                        <%# LendersOffice.AntiXss.AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).TeaserRate )%>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
            </table>
            </FooterTemplate>
        </asp:Repeater>
        <br />
        <br />
                <div clas="center">
            <input type="button" value="Close"  class="ButtonStyle"  onclick="onClosePopup();" />
        </div>
    </div>
    </form>
</body>
</html>
