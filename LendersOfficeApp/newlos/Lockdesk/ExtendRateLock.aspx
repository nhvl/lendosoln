﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" EnableViewState="true" CodeBehind="ExtendRateLock.aspx.cs" Inherits="LendersOfficeApp.newlos.Lockdesk.ExtendRateLock" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Extend Broker Rate Lock</title>
    <style type="text/css">
        .hidden { display: none; }
        body { background-color: gainsboro; }
        .modalbox { line-height: 1em; height: 100px; border:3px inset black; top: 150px;  margin-left: 200px; left: 0; right: 0;  width: 425px; display: none; position: absolute; background-color : whitesmoke;}
        fielset { width: auto; }
	    .CenteringDiv input { margin: 0 auto; text-align: center; }
	    .descriptionField { width: 190px; }
	    .rateField { width: 50px; }
	    .noteField { width: 400px; }
	    .bigNoteField { width: 400px; height: 200px; }
	    #AdjustmentTable { width:800px; }
	    #ExtendLockPeriodModal { width: 700px; height: 250px; }
	    .modalPadding { padding: 5px; }
	    .dayField { width: 30px; }
	    .dateField { width: 70px;   }
	    #Extend_AdjustPriceTable { border: 1px solid black; padding: 10px; margin-left: 10px; }
	    .ButtonStyle { width: auto; overflow: visible; padding: 2px; }
	    .bottomMargin { margin-bottom: 10px; }
	    .CenteringDiv { margin-right: auto; margin-left: auto; text-align: center; margin-top: 10px;}
	    .CenteringDiv input { margin-right: 5px;  }
	    
    </style>
    <script type="text/javascript" src="../../inc/mask.js"></script>
    
</head>
<body>
    <h4 class="page-header">Extend Front-end Rate Lock</h4>
    <form id="form1" runat="server" onreset="return false;">
        <input type="hidden" id="loanid" name="loanid" />
        <asp:HiddenField runat="server" ID="LoadFromArgs" Value="True" />
            <div class="modalPadding">
                <table cellpadding="4" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td class="FieldLabel">
                                 Current Lock Expiration Date
                            </td>
                            <td>
                                <input type="text" id="sRLckdExpiredD" readonly="readonly" class="dateField" name="sRLckdExpiredD" SkipMe="SkipMe" class="dateField" />
                            </td>
                            <td class="FieldLabel">
                                New Lock Expiration Date
                            </td>
                            <td class="FieldLabel"> 
                                <input type="text" id="sRLckdExpiredDNew" readonly="readonly" class="dateField" name="sRLckdExpiredDNew" SkipMe="SkipMe"  class="dateField"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">
                                <label for="Extend_sRLckdDays">Extend Lock Period by</label>
                            </td>
                            <td class="FieldLabel" >
                                <input type="text" id="Extend_sRLckdDays" onchange="f_ExtendUpdate(this);" onkeyup="f_ExtendUpdate(this);" name="Extend_sRLckdDays" class="dayField" /> days
                                <img alt="Required" src="../../images/require_icon.gif" id="Extend_sRLckdDays_Required" />
                            </td>
                            <td class="FieldLabel">
                                Calendar Extension Days
                            </td>
                            <td class="FieldLabel">
                                <input type="text" id="Extend_sTrueRLckedDays" class="dayField" readonly="readonly"/>days
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" colspan="4">
                                <label for="Extend_Reason">Reason</label>
                                <input type="text" id="Extend_Reason" name="Extend_Reason" class="noteField"  onchange="f_validate();" onkeyup="f_validate()" />
                                <img alt="Required" src="../../images/require_icon.gif" id="Extend_Reason_Required" />
                                
                            </td>
                        </tr>
                    </tbody>
                </table>
                <input type="checkbox" id="Extend_AdjustPrice" onclick="f_ToggleAdjustprice(this);" checked="checked" name="Extend_AdjustPrice" /> <label class="FieldLabel" for="Extend_AdjustPrice" >Adjust Price</label>
                <div id="Extend_AdjustPriceTable">
                <table cellpadding="0" cellspacing="0" class="bottomMargin">
                    <thead>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="FieldLabel">Rate</td>
                            <td class="FieldLabel">Price</td>
                            <td class="FieldLabel">Fee</td>
                            <td class="FieldLabel">Margin</td>
                            <td class="FieldLabel">Teaser<br />Rate</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="FieldLabel" align="right"> Current Price </td>
                            <td><input type="text" class="rateField" readonly="readonly"   preset="percent" ID="sNoteIR" name="sNoteIR"/></td>
                            <td><input type="text" class="rateField" readonly="readonly"  preset="percent" ID="sBrokerLockFinalBrokComp1PcPrice" name="sBrokerLockFinalBrokComp1PcPrice" /></td>
                            <td><input type="text" class="rateField" readonly="readonly"  preset="percent" ID="sBrokComp1Pc" name="sBrokComp1Pc"/></td>
                            <td><input type="text" class="rateField" readonly="readonly"  preset="percent" ID="sRAdjMarginR" name="sRAdjMarginR"/> </td>
                            <td><input type="text" class="rateField" readonly="readonly" preset="percent" ID="sOptionArmTeaserR" name="newTeaserRate"/>  </td>
                    
                        </tr>  
                        <tr>
                            <td class="FieldLabel" align="right"> 
                                <input type="checkbox" id="Extend_LockAdjustmentDesc" onclick="f_lockDescription(this);" name="Extend_LockAdjustmentDesc" /> 
                                <input type="text" id="Extend_AdjustmentDescription" class="descriptionField" name="Extend_AdjustmentDescription" readonly="readonly"  /> 
                            </td>
                            <td><input onchange="f_UpdatePricing()" type="text" class="rateField" preset="percent" value="0.000%" id="adjRate" name="adjRate"/></td>
                            <td><input onchange="f_PriceFeeConversion( this, 'adjFee','P', 'Adjustment'); f_UpdatePricing();" type="text" class="rateField" preset="percent" value="0.000%" ID="adjPrice" name="adjPrice"/> </td>
                            <td><input onchange="f_PriceFeeConversion( this, 'adjPrice','F', 'Adjustment'); f_UpdatePricing();" type="text" class="rateField" preset="percent" value="0.000%" ID="adjFee" name="adjFee"/></td>
                            <td><input onchange="f_UpdatePricing()" type="text" class="rateField" preset="percent" value="0.000%" id="adjMargin" name="adjMargin"/> </td>
                            <td><input onchange="f_UpdatePricing()" type="text" class="rateField" readonly="readonly" preset="percent"  value="0.000%" ID="adjTRate" name="adjTRate"/>  </td>
                        </tr>  
                        
                        <tr>
                            <td class="FieldLabel" align="right"> New Price </td>
                            <td><input type="text" class="rateField" readonly="readonly" preset="percent" ID="newsNoteIR" name="newsNoteIR"/></td>
                            <td><input type="text" class="rateField" readonly="readonly" preset="percent" ID="newsBrokerLockFinalBrokComp1PcPrice" name="newsBrokerLockFinalBrokComp1PcPrice"/> </td>
                            <td><input type="text" class="rateField" readonly="readonly" preset="percent" ID="newsBrokComp1Pc" name="newsBrokComp1Pc"/></td>
                            <td><input type="text" class="rateField" readonly="readonly" preset="percent" ID="newsRAdjMarginR" name="newsRAdjMarginR"/> </td>
                            <td><input type="text" class="rateField" readonly="readonly" preset="percent" ID="newsOptionArmTeaserR" name="newsOptionArmTeaserR"/>  </td>
                        </tr>  
                    </tbody>
                </table>
                </div>
                <div class="CenteringDiv">
                    <input type="button" class="ButtonStyle" id="ExtendBrokerRateLock" value="Extend Front-end Rate Lock" onclick="f_ExtendRateLock()"/>
                    <img id="blockRateLockImg" src="../../images/warning25x25.png" class="WarningIcon hidden" alt="The current input days to extend Lock Expiration Date does not result in a new date. Please enter a different value."/>
                    <input type="button" class="ButtonStyle" id="CancelExtendButton" value="Cancel" onclick="onClosePopup();" />
                </div>
            </div>
      <uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg>
                
    </form>
    
    <script type="text/javascript">
    
        function _init(){
            resizeForIE6And7(610,350);
            var args =  getModalArgs() || {};
            var data = document.getElementById('LoadFromArgs').value === 'True' ? args : PageData; 
            var sRLckdExpiredD = document.getElementById('sRLckdExpiredD');
            
            var sNoteIR = document.getElementById('sNoteIR');
            var sBrokComp1Pc = document.getElementById('sBrokComp1Pc');
            var sRAdjMarginR = document.getElementById('sRAdjMarginR');
            var sOptionArmTeaserR = document.getElementById('sOptionArmTeaserR');
            var sBrokerLockFinalBrokComp1PcPrice = document.getElementById('sBrokerLockFinalBrokComp1PcPrice');
            var newsBrokerLockFinalBrokComp1PcPrice = document.getElementById('newsBrokerLockFinalBrokComp1PcPrice');
            var newsNoteIR = document.getElementById('newsNoteIR');
            var newsBrokComp1Pc = document.getElementById('newsBrokComp1Pc');
            var newsRAdjMarginR = document.getElementById('newsRAdjMarginR');
            var newsOptionArmTeaserR = document.getElementById('newsOptionArmTeaserR');
            var loanid = document.getElementById('loanid');
            var adjTRate = document.getElementById('adjTRate');
            
            
            sRLckdExpiredD.value = data.sRLckdExpiredD;
            sNoteIR.value = data.sNoteIR;
            sBrokComp1Pc.value = data.sBrokComp1Pc;
            sRAdjMarginR.value = data.sRAdjMarginR;
            sOptionArmTeaserR.value = data.sOptionArmTeaserR;
            sBrokerLockFinalBrokComp1PcPrice.value = data.sBrokerLockFinalBrokComp1PcPrice;
            adjTRate.readOnly = !data.bIsOptionArm;
            
            newsNoteIR.value = data.sNoteIR;
            newsBrokComp1Pc.value = data.sBrokComp1Pc;
            newsRAdjMarginR.value = data.sRAdjMarginR;
            newsOptionArmTeaserR.value = data.sOptionArmTeaserR;
            loanid.value = data.slId;
            newsBrokerLockFinalBrokComp1PcPrice.value = data.sBrokerLockFinalBrokComp1PcPrice;            
            f_validate();
        }
        
        function f_ToggleAdjustprice(obj){
            var pricingTable = document.getElementById('Extend_AdjustPriceTable');
            if (obj.checked){
                pricingTable.style.visibility = 'visible';
            }
            else {
                pricingTable.style.visibility = 'hidden';
            }
        }

        function f_ExtendUpdate(obj){
            var re = /^\d+$/
            if (re.test(obj.value) || obj.value.length === 0){
                f_UpdateAdjDescription(obj);
                f_UpdateLockExpireDate(obj);
                f_validate();
            } else {
                obj.value = '';
                f_validate();
                return false;
            }
            
        }
        
        function f_validate(){
            
            var days = document.getElementById('Extend_sRLckdDays');
            var reason = document.getElementById('Extend_Reason');
            var daysR = document.getElementById('Extend_sRLckdDays_Required');
            var reasonR = document.getElementById('Extend_Reason_Required');
            var calendarDays = document.getElementById('Extend_sTrueRLckedDays');

            var btn = document.getElementById('ExtendBrokerRateLock');
            
            
            var isValid = true;
            if(days.value.length === 0){
                isValid = false;
                daysR.style.display = '';
            } else {
                daysR.style.display = 'none';
            }
            if(trim(reason.value).length === 0){
                isValid = false;
                reasonR.style.display = '';
            }
            else{
                reasonR.style.display = 'none';
            }
            
            btn.disabled = !isValid || calendarDays.value === "0";
            
        }
        
        
        function f_UpdatePricing(){
            var data = {};
            var transferFields = [ "loanid", "adjPrice", "adjRate", "adjFee", "adjMargin",  "adjTRate"];
            
            for(var i=0; i<transferFields.length; i++){
                var field = transferFields[i]; 
                data[field] = document.getElementById(field).value; 
            }   
            
            var results = gService.BrokerRateLock.call('ExtendRateLockPriceUpdate', data);
           
           if (results.error){
                alert( results.UserMessage );
                return;
           }
           
           for(var field in results.value){
                if( results.value.hasOwnProperty(field)){
                    document.getElementById(field).value = results.value[field];
                }
           }
        }
        
        function f_PriceFeeConversion(field, oppositeFieldId, type, conversion){
            
            var otherField = document.getElementById(oppositeFieldId);
            
            var data = {
                'type' : type,
                'value' : field.value, 
                'conversion' : conversion
            };
            
            var results = gService.BrokerRateLock.call('PriceFeeConversion',data); 
            
            if(results.error){
                alert('Error calculation P/F');
                return;
            }
            
            otherField.value = results.value.result; 
            field.value = results.value.value;
        }
        
        
        function f_UpdateLockExpireDate(obj){
            var sRLckdExpiredD = document.getElementById('sRLckdExpiredD');
            var sRLckdExpiredDNew = document.getElementById('sRLckdExpiredDNew');
            var Extend_sTrueRLckedDays = document.getElementById('Extend_sTrueRLckedDays');
            <%-- //fs opm 21179 06/06/08 --%>
            var days = parseInt(obj.value);
            if(obj.value.length === 0 || isNaN(days)){
                sRLckdExpiredDNew.value = '';
                Extend_sTrueRLckedDays.value = '';
                return;
            }
            
            var args = new Object();  
            args["loanid"] = document.getElementById('loanid').value;
            args["sRLckdD"] = sRLckdExpiredD.value;
            args["sRLckdDays"] = days;
            var result = gService.BrokerRateLock.call("ExtendRateLockCalculateExpiration", args);
            
            if (!result.error) {
                sRLckdExpiredDNew.value = result.value.sRLckdExpiredD;
                Extend_sTrueRLckedDays.value = result.value.Extend_sTrueRLckedDays;
                if (Extend_sTrueRLckedDays.value === "0") {
                    $("#blockRateLockImg").removeClass("hidden");
                }
                else {
                    $("#blockRateLockImg").addClass("hidden");
                }
            }
        }
        
        function f_UpdateAdjDescription(obj){
            var alcb = document.getElementById('Extend_LockAdjustmentDesc');
            var ald = document.getElementById('Extend_AdjustmentDescription');
            
            if(alcb.checked){
                return;
            }
            
            if(obj.value.length === 0){
                ald.value = '';
            }
            else {
                // Get todays date.
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yy = today.getFullYear().toString().substr(2, 2);;

                if (dd < 10) {
                    dd = '0' + dd
                }

                if (mm < 10) {
                    mm = '0' + mm
                }

                today = mm + '/' + dd + '/' + yy;

                ald.value = "LOCK EXTENSION " + today + " - " + obj.value + (obj.value == 1 ? " DAY" : " DAYS"); 
            }
        }
        
        function f_lockDescription(obj){
            var ald = document.getElementById('Extend_AdjustmentDescription');        
            if(obj.checked){
                ald.readOnly = false;
            }
            else{
                var rld = document.getElementById('Extend_sRLckdDays');
                f_UpdateAdjDescription(rld);
                ald.readOnly = true;
            }
        }   
        
        
        function f_ExtendRateLock(){
            var args = window.dialogArguments || {};
            var g = function(id){ return document.getElementById(id);};
            
            args.sLId = g('loanid').value;
            args.Reason = g('Extend_Reason').value;
            args.sRLckdDays = g('Extend_sRLckdDays').value;
            args.IsAdjustPrice = g('Extend_AdjustPrice').checked;
            args.calendarDays = g('Extend_sTrueRLckedDays').value;
        
            args.AdjustmentRate = g('adjRate').value;
            args.AdjustmentPrice = g('adjPrice').value;
            args.AdjustmentFee = g('adjFee').value;
            args.AdjustmentMargin = g('adjMargin').value;
            args.AdjustmentTRate = g('adjTRate').value;
            args.AdjustmentDesc = g('Extend_AdjustmentDescription').value;

            args.Status = 'OK';
 
            onClosePopup(args);
        }
    </script>
</body>
</html>
