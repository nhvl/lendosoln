﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExtendInvestorLock.aspx.cs" Inherits="LendersOfficeApp.newlos.Lockdesk.ExtendInvestorLock" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <script type="text/javascript" src="../../inc/mask.js"></script>
    <link type="text/css" href="../../css/calendar-win2k-cold-2.css" rel="Stylesheet" />
        <style type="text/css">
        .hidden { display: none; }
        body { background-color: gainsboro; }
        .modalbox { line-height: 1em; height: 100px; border:3px inset black; top: 150px;  margin-left: 200px; left: 0; right: 0;  width: 425px; display: none; position: absolute; background-color : whitesmoke;}
        fielset { width: auto; }
	    div.CenteringDiv{
	        margin-left: auto ;
            margin-right: auto ;
            text-align: center ;
            clear :both;
        }
        div.CenteringDiv input {
            margin-left: 10px;
        }
	    .descriptionField { width: 240px; }
	    .rateField { width: 50px; }
	    .noteField { width: 400px; }
	    .bigNoteField { width: 400px; height: 200px; }
	    #AdjustmentTable { width:800px; }
	    #ExtendLockPeriodModal { width: 700px; height: 245px; }
	    .modalPadding { padding: 5px; }
	    .dayField { width: 30px; }
	    .dateField { width: 60px;   }
	    .Wrapper { padding: 2.5px; }
	    .lefty {  margin-left: 20px; border: 1px solid black; padding: 4px;}
        .ButtonStyle {

           width:auto;
           overflow:visible;
           padding-left: 5px;
           padding-right: 5px;
        }

        img { vertical-align: bottom !important; }

    </style>
</head>
<body>
    <h4 class="page-header">Extend Back-end Rate Lock</h4>
    <form id="form1" runat="server" onreset="return false;">
        <input type="hidden" id="loanid" name="loanid" />
        <div class="Wrapper">
            <table cellpadding="1" cellspacing="1">
            <tbody>
                <tr>
                    <td class="FieldLabel">Current Lock Expiration Date</td>
                    <td><asp:TextBox runat="server" ID="sInvestorLockRLckExpiredD" ReadOnly="true" CssClass="dateField"></asp:TextBox></td>
                    <td class="FieldLabel">New Lock Expiration Date</td>
                    <td><asp:CheckBox runat="server" ID="newsInvestorLockRLckExpiredDLckd"  onclick="lckOnClick(this,'newsInvestorLockRLckExpiredD');"/></td>
                    <td><ml:DateTextBox runat="server" preset="date"  id="newsInvestorLockRLckExpiredD"  onkeyup="f_validate()" ReadOnly="true" CssClass="dateField"></ml:DateTextBox>
                                <img alt="Required" src="../../images/error_icon.gif" id="newsInvestorLockRLckExpiredD_Required" />

                    </td>
                </tr>
                 <tr>
                    <td class="FieldLabel">Current Delivery Expiration Date</td>
                    <td><asp:TextBox runat="server" ReadOnly="true"  CssClass="dateField" id="sInvestorLockDeliveryExpiredD"></asp:TextBox></td>
                    <td class="FieldLabel">New Delivery Expiration Date</td>
                    <td><asp:CheckBox runat="server" ID="newsInvestorLockDeliveryExpiredDLckd" onclick="lckOnClick(this,'newsInvestorLockDeliveryExpiredD');" /></td>
                    <td><ml:DateTextBox runat="server"  CssClass="dateField" preset="date"  id="newsInvestorLockDeliveryExpiredD" onkeyup="f_validate()" ReadOnly="true"></ml:DateTextBox>
                                <img alt="Required" src="../../images/error_icon.gif" id="newsInvestorLockDeliveryExpiredD_Required" />

                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">Extend Lock Period By</td>
                    <td class="FieldLabel"><asp:TextBox MaxLength="4" runat="server" ID="ExtendPeriod" CssClass="dayField" onchange="f_ExtendUpdate(this)" onkeyup="f_ExtendUpdate(this, true)"></asp:TextBox> days
                                <img alt="Required" src="../../images/error_icon.gif" id="ExtendPeriod_Required" />

                    </td>
                    <td class="FieldLabel">Lock Fee</td>
                    <td>&nbsp;</td>
                    <td><asp:TextBox runat="server" ID="sInvestorLockLockFee" preset="money" Width="80px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="5" class="FieldLabel">
                        Reason
                        <asp:TextBox Width="390px" runat="server" ID="ExtendReason" onchange="f_validate();" onkeyup="f_validate();"></asp:TextBox>
                                <img alt="Required" src="../../images/error_icon.gif" id="ExtendReason_Required" />


                    </td>
                </tr>
            </tbody>
        </table>
        <br />
            <input type="checkbox" id="Extend_AdjustPrice" onclick="f_ToggleAdjustprice(this)" style=" text-align:center; " checked="checked" name="Extend_AdjustPrice" /> <label class="FieldLabel" for="Extend_AdjustPrice" >Adjust Price</label>
            <table class="lefty" id="Extend_AdjustPriceTable" cellpadding="1" cellspacing="1">
                    <thead>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="FieldLabel">Rate</td>
                            <td class="FieldLabel">Price</td>
                            <td class="FieldLabel">Fee</td>
                            <td class="FieldLabel">Margin</td>
                            <td class="FieldLabel">Teaser<br />Rate</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="FieldLabel" align="right"> Current Price </td>
                            <td><input type="text" class="rateField" readonly="readonly"   preset="percent" ID="sInvestorLockNoteIR" name="sInvestorLockNoteIR"/></td>
                            <td><input type="text" class="rateField" readonly="readonly"  preset="percent" ID="sInvestorLockBrokComp1PcPrice" name="sInvestorLockBrokComp1PcPrice" /></td>
                            <td><input type="text" class="rateField" readonly="readonly"  preset="percent" ID="sInvestorLockBrokComp1Pc" name="sInvestorLockBrokComp1Pc"/></td>
                            <td><input type="text" class="rateField" readonly="readonly"  preset="percent" ID="sInvestorLockRAdjMarginR" name="sInvestorLockRAdjMarginR"/> </td>
                            <td><input type="text" class="rateField" readonly="readonly" preset="percent" ID="sInvestorLockOptionArmTeaserR" name="sInvestorLockOptionArmTeaserR"/>  </td>

                        </tr>
                        <tr>
                            <td class="FieldLabel" align="right">
                                <input type="checkbox" id="Extend_LockAdjustmentDesc" onclick="f_lockDescription(this);" name="Extend_LockAdjustmentDesc" />
                                <input type="text" id="Extend_AdjustmentDescription" class="descriptionField" name="Extend_AdjustmentDescription" readonly="readonly"  />
                            </td>
                            <td><input type="text" class="rateField" preset="percent" value="0.000%" id="adjRate" name="adjRate" onchange="f_UpdatePricing()"/></td>
                            <td><input type="text" class="rateField" onchange="f_PriceFeeConversion( this, 'adjFee','P', 'Adjustment');f_UpdatePricing()"   preset="percent" value="0.000%" ID="adjPrice" name="adjPrice"/> </td>
                            <td><input type="text" class="rateField" onchange="f_PriceFeeConversion( this, 'adjPrice','F', 'Adjustment');f_UpdatePricing()" preset="percent" value="0.000%" ID="adjFee" name="adjFee"/></td>
                            <td><input type="text" class="rateField" preset="percent" value="0.000%" id="adjMargin" name="adjMargin" onchange="f_UpdatePricing();"/> </td>
                            <td><input type="text" class="rateField" readonly="readonly" preset="percent"  value="0.000%" ID="adjTRate" name="adjTRate" onchange="f_UpdatePricing();"/>  </td>
                        </tr>

                        <tr>
                            <td class="FieldLabel" align="right"> New Price </td>
                            <td><input type="text" class="rateField" readonly="readonly" preset="percent" ID="newsInvestorLockNoteIR" name="newsInvestorLockNoteIR"/></td>
                            <td><input type="text" class="rateField" readonly="readonly" preset="percent" ID="newsInvestorLockBrokComp1PcPrice" name="newsInvestorLockBrokComp1PcPrice"/> </td>
                            <td><input type="text" class="rateField" readonly="readonly" preset="percent" ID="newsInvestorLockBrokComp1Pc" name="newsInvestorLockBrokComp1Pc"/></td>
                            <td><input type="text" class="rateField" readonly="readonly" preset="percent" ID="newsInvestorLockRAdjMarginR" name="newsInvestorLockRAdjMarginR"/> </td>
                            <td><input type="text" class="rateField" readonly="readonly" preset="percent" ID="newsInvestorLockOptionArmTeaserR" name="newsInvestorLockOptionArmTeaserR"/>  </td>
                        </tr>
                    </tbody>
                </table>


            <div class="CenteringDiv">
            <br />
                <asp:CheckBox runat="server" ID="ExtendBrokerRateLock"  /> <ml:EncodedLabel CssClass="FieldLabel" AssociatedControlID="ExtendBrokerRateLock" runat="server" >Extend front-end rate lock after extending the back-end rate lock</ml:EncodedLabel>
            </div>
            <br />
            <div class="CenteringDiv">
                <input type="button" id="ExtendInvestorRateLock" class="ButtonStyle" onclick="f_extendRate()" value="Extend Back-end Rate Lock" />
                <input type="button" id="CancelExtendButton" class="ButtonStyle" value="Cancel" onclick="onClosePopup();" />
            </div>
        </div>
      <uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg>
    </form>
</body>

<script type="text/javascript" >

    function doAfterDateFormat(){
        f_validate();
    }
    /* document.getElementById short cut with caching */
    function g(id) {
        return g.cache[id] = g.cache[id] ||
            document.getElementById(id);
    }

    g.cache = {};

    function _init() {
        var args = getModalArgs();
        resizeForIE6And7(670,400);


        /* New Lock Expiration Dates */
        g('newsInvestorLockRLckExpiredDLckd').checked = args.sInvestorLockRLckExpiredDLckd;
        g('newsInvestorLockDeliveryExpiredDLckd').checked = args.sInvestorLockDeliveryExpiredDLckd;
        if( args.sInvestorLockDeliveryExpiredDLckd ) {
            g('newsInvestorLockDeliveryExpiredD').value = args.sInvestorLockDeliveryExpiredD;
        }
        if( args.sInvestorLockRLckExpiredDLckd ) {
            g('newsInvestorLockRLckExpiredD').value = args.sInvestorLockRLckExpiredD;
        }
        /* Existing Dates  */
        g('sInvestorLockRLckExpiredD').value = args.sInvestorLockRLckExpiredD;
        g('sInvestorLockDeliveryExpiredD').value = args.sInvestorLockDeliveryExpiredD;



        /* Adjustment Info */
        g('sInvestorLockNoteIR').value = args.sInvestorLockNoteIR;
        g('newsInvestorLockNoteIR').value = args.sInvestorLockNoteIR;
        g('sInvestorLockBrokComp1PcPrice').value = args.sInvestorLockBrokComp1PcPrice;
        g('newsInvestorLockBrokComp1PcPrice').value = args.sInvestorLockBrokComp1PcPrice;
        g('sInvestorLockBrokComp1Pc').value = args.sInvestorLockBrokComp1Pc;
        g('newsInvestorLockBrokComp1Pc').value = args.sInvestorLockBrokComp1Pc;
        g('sInvestorLockRAdjMarginR').value = args.sInvestorLockRAdjMarginR;
        g('newsInvestorLockRAdjMarginR').value = args.sInvestorLockRAdjMarginR;
        g('sInvestorLockOptionArmTeaserR').value = args.sInvestorLockOptionArmTeaserR;
        g('newsInvestorLockOptionArmTeaserR').value = args.sInvestorLockOptionArmTeaserR;


        g('sInvestorLockOptionArmTeaserR').readOnly = !args.isOptionArm;

        g('sInvestorLockLockFee').value = args.sInvestorLockLockFee;
        g('loanid').value = args.loanid;

        g('newsInvestorLockRLckExpiredD').readOnly = !g('newsInvestorLockRLckExpiredDLckd').checked;
        g('newsInvestorLockDeliveryExpiredD').readOnly = !g('newsInvestorLockDeliveryExpiredDLckd').checked;

        g('ExtendBrokerRateLock').checked = args.sInvestorLockCheckBrokerExtendByDefault;


        f_validate();

    }

    function f_extendRate() {
        if(isNaN(Date.parse(g('newsInvestorLockRLckExpiredD').value))){
            alert('Invalid New Lock Expiration Date, please use a valid date.');
            return;
        }
        if(isNaN(Date.parse(g('newsInvestorLockDeliveryExpiredD').value))){
            alert('Invalid New Delivery Expiration Date, please use a valid date.');
            return;
        }

        var data = window.dialogArguments || {};

        data.adjPrice = g('adjPrice').value;
        data.adjDesc = g('Extend_AdjustmentDescription').value;
        data.adjRate = g('adjRate').value;
        data.adjFee = g('adjFee').value;
        data.adjMargin = g('adjMargin').value;
        data.adjTRate = g('adjTRate').value;
        data.sInvestorLockLockFee = g('sInvestorLockLockFee').value;
        data.AddAdjustment = g('Extend_AdjustPrice').checked  ? 'True' : 'False';

        data.newsInvestorLockRLckExpiredDLckd    = g('newsInvestorLockRLckExpiredDLckd').checked ? 'True' : 'False';
        data.newsInvestorLockRLckExpiredD        = g('newsInvestorLockRLckExpiredD').value;

        data.newsInvestorLockDeliveryExpiredDLckd  = g('newsInvestorLockDeliveryExpiredDLckd').checked ? 'True' : 'False';
        data.newsInvestorLockDeliveryExpiredD      = g('newsInvestorLockDeliveryExpiredD').value;


        data.Reason = g('ExtendReason').value;
        data.ExtendPeriod = g('ExtendPeriod').value;
        data.ExtendBroker = g('ExtendBrokerRateLock').checked;
        data.OK = 'OK';
        onClosePopup(data);
    }

    function f_updateDates(){
        var sInvestorLockDeliveryExpiredD       = g('newsInvestorLockDeliveryExpiredD');
        var sInvestorLockDeliveryExpiredDLckd   = g('newsInvestorLockDeliveryExpiredDLckd');

        var sInvestorLockRLckExpiredDLckd       = g('newsInvestorLockRLckExpiredDLckd');
        var sInvestorLockRLckExpiredD           = g('newsInvestorLockRLckExpiredD');

        var ExtendPeriod = g('ExtendPeriod');

        if( ExtendPeriod.value.length == 0 ){
           if(false === sInvestorLockDeliveryExpiredDLckd.checked ) {
                sInvestorLockDeliveryExpiredD.value = '';
           }
           if(false === sInvestorLockRLckExpiredDLckd.checked ){
                sInvestorLockRLckExpiredD.value = '';
           }
           return;
        }

        if(sInvestorLockDeliveryExpiredDLckd.checked && sInvestorLockRLckExpiredDLckd.checked) {
            return; //nothing to update both checked.
        }
        var data = {};

        data.loanid                             = g('loanid').value;
        data.ExtendPeriod                       = ExtendPeriod.value;
        data.sInvestorLockRLckExpiredDLckd      = sInvestorLockRLckExpiredDLckd.checked ? 'True' : 'False';
        data.sInvestorLockDeliveryExpiredDLckd  = sInvestorLockDeliveryExpiredDLckd.checked ? 'True' : 'False';
        data.sInvestorLockRLckExpiredD          = sInvestorLockRLckExpiredD.value;
        data.sInvestorLockDeliveryExpiredD      = sInvestorLockDeliveryExpiredD.value;


        var results = gService.InvestorRateLock.call('PreviewRateLockExtendDates', data);

        if(results.error){
            alert(results.UserMessage);
            return;
        }

        sInvestorLockRLckExpiredD.value = results.value.sInvestorLockRLckExpiredD;
        sInvestorLockDeliveryExpiredD.value = results.value.sInvestorLockDeliveryExpiredD;

    }

    function f_UpdatePricing(){
        var data = {};

            data.loanid = g('loanid').value;
            data.adjPrice = g('adjPrice').value;
            data.adjRate = g('adjRate').value;
            data.adjFee = g('adjFee').value;
            data.adjMargin = g('adjMargin').value;
            data.adjTRate = g('adjTRate').value;

            var results = gService.InvestorRateLock.call('ExtendAdjustmentPricing', data);

           if (results.error){
                alert( results.UserMessage );
                return;
           }

           g('newsInvestorLockNoteIR').value = results.value.sInvestorLockNoteIR;
           g('newsInvestorLockBrokComp1PcPrice').value = results.value.sInvestorLockBrokComp1PcPrice;
           g('newsInvestorLockBrokComp1Pc').value = results.value.sInvestorLockBrokComp1Pc;
           g('newsInvestorLockRAdjMarginR').value = results.value.sInvestorLockRAdjMarginR;
           g('newsInvestorLockOptionArmTeaserR').value = results.value.sInvestorLockOptionArmTeaserR;
        }

    function f_ToggleAdjustprice(obj){
        var pricingTable = document.getElementById('Extend_AdjustPriceTable');
        if (obj.checked){
            pricingTable.style.visibility = 'visible';
        }
        else {
            pricingTable.style.visibility = 'hidden';
        }
    }

    function f_ExtendUpdate(obj, dontUpdateDates){

        ///We dont wan to update dates on every keypress, slower.
        if(typeof(dontUpdateDates) === 'undefined'){
            dontUpdateDates = false;
        }

        var re = /^\d+$/
        if(re.test(obj.value) || obj.value.length === 0){
                f_UpdateAdjDescription(obj);
                if(!dontUpdateDates){f_updateDates();}
            }
            else{
                obj.value = '';
            }
            f_validate();
        }

        function lckOnClick(obj, fieldID)
        {
            var textField = document.getElementById(fieldID);
            lckOnHandler(obj, textField);
        }

        function lckOnHandler(checkbox, textField){
            textField.readOnly = !checkbox.checked;
            if(!checkbox.checked){
                f_ExtendUpdate(document.getElementById('ExtendPeriod'));
            }
        }

        function f_UpdateAdjDescription(obj){
            var alcb = document.getElementById('Extend_LockAdjustmentDesc');
            var ald = document.getElementById('Extend_AdjustmentDescription');

            if(alcb.checked){
                return;
            }

            if(obj.value.length === 0){
                ald.value = '';
            }
            else {
                // Get todays date.
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yy = today.getFullYear().toString().substr(2, 2);;

                if (dd < 10) {
                    dd = '0' + dd
                }

                if (mm < 10) {
                    mm = '0' + mm
                }

                today = mm + '/' + dd + '/' + yy;

                ald.value = "LOCK EXTENSION " + today + " - " + obj.value + (obj.value == 1 ? " DAY" : " DAYS");
            }
        }

        function f_lockDescription(obj){
            var ald = document.getElementById('Extend_AdjustmentDescription');
            if(obj.checked){
                ald.readOnly = false;
            }
            else{
                var rld = document.getElementById('ExtendPeriod');
                f_UpdateAdjDescription(rld);
                ald.readOnly = true;
            }
        }




    function f_PriceFeeConversion(field, oppositeFieldId, type, conversion){

       var otherField = document.getElementById(oppositeFieldId);

       var data = {
           'type' : type,
           'value' : field.value,
           'conversion' : conversion
       };

       var results = gService.InvestorRateLock.call('PriceFeeConversion',data);

       if(results.error){
           alert('Error calculation P/F');
           return;
       }

       otherField.value = results.value.result;
       field.value = results.value.value;
    }



        function f_validate(){

            var days = document.getElementById('ExtendPeriod');
            var reason = document.getElementById('ExtendReason');



            var daysR = document.getElementById('ExtendPeriod_Required');
            var reasonR = document.getElementById('ExtendReason_Required');

            var d1 = document.getElementById('newsInvestorLockRLckExpiredD');
            var d2 = document.getElementById('newsInvestorLockDeliveryExpiredD');

            var d1r = document.getElementById('newsInvestorLockRLckExpiredD_Required');
            var d2r = document.getElementById('newsInvestorLockDeliveryExpiredD_Required');

            var btn = document.getElementById('ExtendInvestorRateLock');


            var isValid = true;
            if(days.value.length === 0){
                isValid = false;
                daysR.style.visibility = 'visible';
            }else{
                daysR.style.visibility = 'hidden';
            }
            if(trim(reason.value).length === 0){
                isValid = false;
                reasonR.style.visibility = 'visible';
            }
            else{
                reasonR.style.visibility = 'hidden';
            }
            if(d1.value.length === 0 || isNaN(Date.parse(d1.value)) ){
                isValid = false;
                d1r.style.visibility = 'visible';
            }
            else{
                d1r.style.visibility = 'hidden';
            }
            if(d2.value.length == 0 || isNaN(Date.parse(d2.value))){
                isValid = false;
                d2r.style.visibility = 'visible';
            }
            else{
                d2r.style.visibility = 'hidden';
            }
            btn.disabled = !isValid;

        }

</script>
</html>
