﻿using System;
using System.Collections;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Xml;
using System.Xml.Xsl;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Audit;
using LendersOffice.Common;
using LendersOffice.DistributeUnderwriting;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.AntiXss;
using LendersOffice.Constants;
using LendersOfficeApp.los.common;
using LendersOffice.Security;
using LendersOffice.RatePrice;
using LendersOffice.ObjLib.Resource;

namespace LendersOfficeApp.newlos.Lockdesk
{

    public partial class EmailCertificateService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        private string m_pmlLenderSiteId = "";
        private string XsltFileLocation
        {
            get { return ResourceManager.Instance.GetResourcePath(ResourceType.PmlCertificateXslt, BrokerID); }
        }

        private Guid BrokerID
        {
            get { return ((AbstractUserPrincipal)Page.User).BrokerId; }
        }

        public XsltArgumentList XsltParams(string notes)
        {
            string css =  ResourceManager.Instance.GetResourceContents(ResourceType.CertificateCss, this.BrokerID); 

            XsltArgumentList args = new XsltArgumentList();
            args.AddParam("VirtualRoot", "", Tools.VRoot);
            args.AddParam("LenderPmlSiteId", "", m_pmlLenderSiteId);
            args.AddParam("GenericStyle", "", css);
            args.AddParam("Email", "", "True");
            args.AddParam("IsMoreDetailCert", "", "True");
            args.AddParam("Notes", "", notes);
            if (PrincipalFactory.CurrentPrincipal.BrokerDB.HidePmlCertSensitiveFields)
            {
                args.AddParam("HideSensitiveFields", "", "True");
            }
            return args;
        }
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "SendEmail":
                    SendEmail();
                    break;
            }
        }
        private void SendEmail()
        {

            Guid sLId = GetGuid("loanID");
            Guid productID = GetGuid("productID");
            string recipient = GetString("EmailAddr").Replace(" ", "");
            string ccrecipent = GetString("CCAddr").Replace(" ", "");
            string subject = GetString("Subject");
            string fromEmailAddr = GetString("FromEmailAddr");
            string note = AspxTools.HtmlString(GetString("Note", ""));
            string requestedRate = GetString("Rate", "") + ":" + GetString("Fee", "") + ":0.000,0.000";
            string version = string.Empty;
            string uniqueChecksum = GetString("UniqueChecksum", "");
            E_sLienQualifyModeT sLienQualifyModeT = (E_sLienQualifyModeT)GetInt("sLienQualifyModeT");
            E_RenderResultModeT renderResultModeT = E_RenderResultModeT.Regular;
            E_sPricingModeT sPricingModeT = (E_sPricingModeT)GetInt("sPricingModeT");
            AbstractUserPrincipal principal = this.User as AbstractUserPrincipal;

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(CertificateWaitService));
            dataLoan.InitLoad();
            dataLoan.sPricingModeT = sPricingModeT;
            Guid lpePriceGroupId = dataLoan.sInternalPricingLpePriceGroupId;
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            HybridLoanProgramSetOption historicalOptions = options;

            BrokerDB broker = BrokerDB.RetrieveById(principal.BrokerId);
            if (broker.IsEnableHistoricalPricing)
            {
                historicalOptions = new HybridLoanProgramSetOption();
                historicalOptions.ApplicablePolicies = (E_LoanProgramSetPricingT)GetInt("historicalPolicy", 0);
                historicalOptions.RateOptions = (E_LoanProgramSetPricingT)GetInt("historicalRateOption", 0);
            }

            Guid requestId = DistributeUnderwritingEngine.SubmitToEngineForRenderCertificate(principal, 
                sLId, productID, requestedRate, sLienQualifyModeT, lpePriceGroupId, "" /*sTempLpeTaskMessageXml*/, version, 
                renderResultModeT, sPricingModeT, uniqueChecksum, GetString("parId", ""),
                historicalOptions);
            UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultsSynchronous(requestId);

            if (note != "")
            {
                note = string.Format("Note from {0}:<br><br>{1}", principal.DisplayName, note.Replace(Environment.NewLine, "<br>"));
            }
            SetPmlLenderSiteId(sLId);

            Guid sBranchId = Guid.Empty;

            bool hasLogo = AttachLogo(sLId, out sBranchId);

            m_pmlLenderSiteId = Tools.GetFileDBKeyForPmlLogo(new Guid(m_pmlLenderSiteId), sBranchId);

            string logoId = $"_logo_{DateTime.Now.Ticks}";
            byte[] logo = null;
            if (hasLogo)
            {
                try
                {
                    logo = FileDBTools.ReadData(E_FileDB.Normal, m_pmlLenderSiteId.ToString().ToLower() + ".logo.gif");
                }
                catch (FileNotFoundException)
                {
                    hasLogo = false;
                }
            }

            string bodyContent = "";
            string auditContent = "";

            ArrayList productIDList = new ArrayList();
            productIDList.Add(productID);


            if (resultItem.IsErrorMessage)
            {
                SetResult("ErrMsg", resultItem.ErrorMessage);

                return;
            }
            ArrayList results = resultItem.Results;

            if (null == results)
                throw new CBaseException(ErrorMessages.Generic, "DistributeUnderwritingEngine.RetrieveResults return null for RequestID = " + requestId);

            XmlDocument _doc = (XmlDocument)results[0];
            _doc = UnderwritingResultItem.DedupAdjustmentDescription(_doc);
            if (hasLogo)
            {
                bodyContent = GenerateContent(_doc, $"cid:{logoId}", note);
                auditContent = GenerateContent(_doc, "LogoPL.aspx?id=" + m_pmlLenderSiteId, note);
            }
            else
            {
                bodyContent = auditContent = GenerateContent(_doc, null, note);
            }

            var cbe = new LendersOffice.Email.CBaseEmail(BrokerID)
            {
                DisclaimerType = E_DisclaimerType.NORMAL,
                From = fromEmailAddr,
                To = recipient,
                Subject = subject,
                Message = bodyContent,
                IsHtmlEmail = true,
                Bcc = ccrecipent
            };
            if (hasLogo && logo.Length > 0)
            {
                cbe.AddAttachment(logoId, logo, true);
            }
            cbe.Send();
            AbstractAuditItem audit = new PmlCertificateEmailAuditItem((LendersOffice.Security.AbstractUserPrincipal)Page.User, fromEmailAddr, recipient, subject, auditContent);
            AuditManager.RecordAudit(sLId, audit);

        }

        private string GenerateContent(XmlDocument doc, string logoSource, string notes)
        {
            using (MemoryStream stream = new MemoryStream(10000))
            {
                XmlWriterSettings writerSettings = new XmlWriterSettings();
                writerSettings.Encoding = new UTF8Encoding(false); // Skip BOM
                writerSettings.OmitXmlDeclaration = true;

                XmlWriter writer = XmlWriter.Create(stream, writerSettings);

                doc.WriteTo(writer);

                writer.Flush();

                stream.Seek(0, SeekOrigin.Begin);

                XsltArgumentList xsltParams = XsltParams(notes);
                if (false == string.IsNullOrEmpty(logoSource))
                {
                    xsltParams.AddParam("LogoSource", "", logoSource);
                }
                return XslTransformHelper.Transform(XsltFileLocation, stream, xsltParams);
            }
        }

        private void SetPmlLenderSiteId(Guid sLId)
        {
            // 11/10/2004 dd - Theoretically, PmlLenderSiteId should be in loan product.
            // This way we allow independent broker to run qualification against multiple lender.
            // However we are not there yet. Retrieve from current broker.

            SqlParameter[] parameters = { new SqlParameter("@BrokerID", BrokerID) };
            Guid brokerPmlSiteId = Guid.Empty;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerID, "RetrieveLenderSiteIDByBrokerID", parameters))
            {
                if (reader.Read())
                {
                    brokerPmlSiteId = (Guid)reader["BrokerPmlSiteID"];
                }
            }
            m_pmlLenderSiteId = Tools.GetFileDBKeyForPmlLogoWithLoanId(brokerPmlSiteId, sLId);
        }

        /// <summary>
        /// Determines if we need to show the logo on the email cert based on   IsDisplayNmModified
        /// We need to check outside because we dont want to attach the logo to the email if its not going to display it.
        /// </summary>
        /// <param name="sLId"></param>
        /// <returns></returns>
        private bool AttachLogo(Guid sLId, out Guid sBranchId)
        {
            CPageData data = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(EmailCertificateService));
            data.InitLoad();

            sBranchId = data.sBranchId;

            BranchDB db = new BranchDB(data.sBranchId, data.sBrokerId);
            db.Retrieve();

            return !db.IsDisplayNmModified;
        }
    }
}
