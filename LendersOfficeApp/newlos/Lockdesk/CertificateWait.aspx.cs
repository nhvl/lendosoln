﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Lockdesk
{
    public partial class CertificateWait : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterJsGlobalVariables("ProductId", RequestHelper.GetGuid("productid"));
            RegisterJsGlobalVariables("sLienQualifyModeT", RequestHelper.GetInt("lienqualifymodet"));
            RegisterJsGlobalVariables("sPricingModeT", RequestHelper.GetInt("sPricingModeT"));
            RegisterJsGlobalVariables("Rate", RequestHelper.GetSafeQueryString("rate"));
            RegisterJsGlobalVariables("Fee", RequestHelper.GetSafeQueryString("fee"));
            RegisterJsGlobalVariables("UniqueChecksum", RequestHelper.GetSafeQueryString("uniquechecksum"));
            RegisterJsGlobalVariables("historicalRateOption", RequestHelper.GetInt("historicalRateOption", 0));
            RegisterJsGlobalVariables("historicalPolicy", RequestHelper.GetInt("historicalPolicy", 0));
            RegisterJsGlobalVariables("snapshotJson", HttpUtility.HtmlDecode(RequestHelper.GetSafeQueryString("snapshotJson")));
            RegisterJsGlobalVariables("isHistorical", RequestHelper.GetBool("isHistorical"));

        }
        protected override void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;
            this.DisplayCopyRight = false;
            base.OnInit(e);
        }
    }
}
