﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RunInternalPricingV2.aspx.cs" Inherits="LendersOfficeApp.newlos.Lockdesk.RunInternalPricingV2" %>
<%@ Register TagPrefix="PCF" TagName="ProductCodeFilterPopup" Src="~/newlos/ProductCodeFilterPopup.ascx" %>
<%@ Register TagPrefix="uc1" TagName="QualifyingBorrower" Src="~/newlos/QualifyingBorrower.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Run Internal Pricing</title>
</head>
<body>
    <form class="InternalPricingV2" id="InternalPricingV2" runat="server">
        <PCF:ProductCodeFilterPopup ID="ProductCodeFilterPopup" runat="server" ShouldLoadJqueryUi="false" />
        <div class="outermost-container">
        <div class="header">
            <div>
                <div class="loan-officer">
                    Loan Officer: <span id="sEmployeeLoanRepFullName" runat="server"></span>
                <!-- These tags are touching to prevent the browser from inserting whitespace, which messes
                    with the styling. -->
                </div><div class="go-back">
                    <img src="../../images/GoBackIcon_transparent.png" alt="Go back to LendingQB" class="back-button" />
                    <a id="backButton" href="#" runat="server">Go back to LendingQB</a>
                </div>
            </div>
            <table>
                <thead>
                    <tr>
                        <th>Loan Number</th>
                        <th>Loan Status</th>
                        <th>Rate Lock Status</th>
                        <th>Borrower Name</th>
                        <th>Lowest Middle Credit Score</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><span id="sLNm" runat="server"></span></td>
                        <td><span id="sStatusT" runat="server"></span></td>
                        <td><span id="sRateLockStatusT" runat="server"></span></td>
                        <td><span id="aBNm" runat="server"></span></td>
                        <td class="center"><span id="sCreditScoreType2" runat="server"></span></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="left-column">
            <div class="mask-when-pricing"></div>
            <div class="container loan-info">
                <h4>Loan Information</h4>
                <div class="row">
                    <div>
                        <label for="sLpTemplateNm">Loan Program</label>
                    </div>
                    <div>
                        <input type="text" id="sLpTemplateNm" class="text-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sLpProductType">Registered Product Type</label>
                    </div>
                    <div>
                        <input type="text" id="sLpProductType" class="text-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div class="row" runat="server" id="sLpInvestorNmRow">
                    <div>
                        <label for="sLpInvestorNm">Investor</label>
                    </div>
                    <div>
                        <input type="text" id="sLpInvestorNm" class="text-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sTerm">Term / Due (months)</label>
                    </div>
                    <div>
                        <input type="text" id="sTerm" class="count-field" readonly="readonly" runat="server" />
                        /
                    <input type="text" id="sDue" class="count-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sFinMethT">Amortization Type</label>
                    </div>
                    <div>
                        <select id="sFinMethT" class="disabled-select" runat="server"></select>
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sNoteIR">Note Rate</label>
                    </div>
                    <div>
                        <input type="text" id="sNoteIR" class="percent-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sRLckdD">Loan Officer Rate Lock Date</label>
                    </div>
                    <div>
                        <input type="text" id="sRLckdD" class="date-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sRLckdDays">Rate Lock Period</label>
                    </div>
                    <div>
                        <input type="text" id="sRLckdDays" class="count-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sRLckdExpiredD">Loan Officer Rate Lock Expiration</label>
                    </div>
                    <div>
                        <input type="text" id="sRLckdExpiredD" class="date-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sRLckdExpiredInDays">Rate Lock Expires in</label>
                    </div>
                    <div>
                        <input type="text" id="sRLckdExpiredInDays" class="count-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div id="IsRenovationRow" class="row" runat="server">
                    <div>
                        <label for="sIsRenovationLoan">Is Renovation Loan?</label>
                    </div>
                    <div>
                        <input type="checkbox" id="sIsRenovationLoan" disabled="disabled" runat="server" />
                        Yes
                    </div>
                </div>
                <div id="TotalRenovationCostsRow" class="row" runat="server">
                    <div>
                        <label for="sTotalRenovationCosts">Total Renovation Costs</label>
                    </div>
                    <div>
                        <input type="text" id="sTotalRenovationCosts" class="money-field" readonly="readonly" runat="server"  />
                    </div>
                </div>
                <div id="RefinanceTypeRow" class="row" runat="server">
                    <div>
                        <label for="sLPurposeTPe">Refinance Type</label>
                    </div>
                    <div>
                        <select id="sLPurposeTPe" class="disabled-select" runat="server"></select>
                    </div>
                </div>
                <div id="Texas50a6Row" class="row" runat="server">
                    <div>
                        <label for="sProdIsTexas50a6Loan">New Loan is Texas 50(a)(6)?</label>
                    </div>
                    <div>
                        <input type="checkbox" id="sProdIsTexas50a6Loan" disabled="disabled" runat="server" />
                        Yes
                    </div>
                </div>
                <div id="sPreviousLoanIsTexas50a6LoanRow" class="row" runat="server">
                    <div>
                        <label for="sPreviousLoanIsTexas50a6Loan">Prior Loan is Texas 50(a)(6)?</label>
                    </div>
                    <div>
                        <input type="checkbox" id="sPreviousLoanIsTexas50a6Loan" disabled="disabled" runat="server" />
                        Yes
                    </div>
                </div>
                <div id="EndorsedRow" class="row" runat="server">
                    <div>
                        <label for="sProdIsLoanEndorsedBeforeJune09">Was existing loan endorsed on or before May 31, 2009?</label>
                    </div>
                    <div>
                        <input type="checkbox" id="sProdIsLoanEndorsedBeforeJune09" disabled="disabled" runat="server" />
                        Yes
                    </div>
                </div>
                <div id="CashoutAmountRow" class="row" runat="server">
                    <div>
                        <label for="sProdCashoutAmt">Cashout Amount</label>
                    </div>
                    <div>
                        <input type="text" id="sProdCashoutAmt" class="money-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <asp:PlaceHolder ID="CurrentLoanPiPaymentCurrentMipPerMonth" runat="server">
                <div class="row">
                    <div>
                        <label for="sProdCurrPIPmt">Current Loan P&amp;I Payment</label>
                    </div>
                    <div>
                        <input type="text" id="sProdCurrPIPmt" class="money-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sProdCurrMIPMo">Current MIP/Month</label>
                    </div>
                    <div>
                        <input type="text" id="sProdCurrMIPMo" class="money-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="FhaStreamlineRefiVaIrrrlFields" runat="server">
                <div class="row">
                    <div>
                        <label for="sIsCreditQualifying">Credit Qualifying?</label>
                    </div>
                    <div>
                        <input type="checkbox" id="sIsCreditQualifying" disabled="disabled" runat="server" />
                        Yes
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sHasAppraisal">Appraisal?</label>
                    </div>
                    <div>
                        <input type="checkbox" id="sHasAppraisal" disabled="disabled" runat="server" />
                        Yes
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sSpLien">Outstanding Principal Balance</label>
                    </div>
                    <div>
                        <input type="text" id="sSpLien" class="money-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                </asp:PlaceHolder>
                <div id="UpfrontMipRefundRow" class="row" runat="server">
                    <div>
                        <label for="sFHASalesConcessions">Upfront MIP Refund</label>
                    </div>
                    <div>
                        <input type="text" id="sFHASalesConcessions" class="money-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div id="SellerCreditRow" class="row" runat="server">
                    <div>
                        <label for="sSellerCreditT">Seller Credit</label>
                    </div>
                    <div>
                        <select id="sSellerCreditT" class="disabled-select" runat="server"></select>
                    </div>
                </div>
                <asp:PlaceHolder ID="StandaloneSecondLienFields" runat="server">
                <div class="row">
                    <div>
                        <label for="sLpIsNegAmortOtherLien">1st Lien has Negative Amort.</label>
                    </div>
                    <div>
                        <input type="checkbox" id="sLpIsNegAmortOtherLien" disabled="disabled" runat="server" />
                        Yes
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sOtherLFinMethT">1st Lien Amort. Type</label>
                    </div>
                    <div>
                        <select id="sOtherLFinMethT" class="disabled-select" runat="server"></select>
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sProOFinPmtPe">1st Lien Payment</label>
                    </div>
                    <div>
                        <input type="text" id="sProOFinPmtPe" class="money-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                </asp:PlaceHolder>
                <div class="row">
                    <div>
                        <label for="sProdImpound">Impound?</label>
                    </div>
                    <div>
                        <input type="checkbox" id="sProdImpound" disabled="disabled" runat="server" />
                        Yes
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sProdDocT">Doc Type</label>
                    </div>
                    <div>
                        <select id="sProdDocT" class="disabled-select" runat="server"></select>
                    </div>
                </div>
                <div id="OriginalAppraisedValueRow" class="row" runat="server">
                    <div>
                        <label for="sOriginalAppraisedValue">Original Appraised Value</label>
                    </div>
                    <div>
                        <input type="text" id="sOriginalAppraisedValue" class="money-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div id="AppraisedValueRow" class="row" runat="server">
                    <div>
                        <label id="sApprValPeLabel" for="sApprValPe" runat="server">Appraised Value</label>
                    </div>
                    <div>
                        <input type="text" id="sApprValPe" class="money-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div id="HomeValueRow" class="row" runat="server">
                    <div>
                        <label id="sHouseValPeLabel" for="sHouseValPe" runat="server">Sales Price</label>
                    </div>
                    <div>
                        <input type="text" id="sHouseValPe" class="money-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div id="AsIsValueRow" class="row" runat="server">
                    <div>
                        <label for="sAsIsAppraisedValuePeval">As-Is Value</label>
                    </div>
                    <div>
                        <input type="text" id="sAsIsAppraisedValuePeval" class="money-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label id="DownPaymentLabel" for="sDownPmtPcPe" runat="server">Down Payment</label>
                    </div>
                    <div>
                        <input type="text" id="sDownPmtPcPe" class="percent-field" readonly="readonly" runat="server" /><input type="text" id="sEquityPe" class="money-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sLtvRPe">1st Lien</label>
                    </div>
                    <div>
                        <asp:PlaceHolder ID="FirstLienLtv" runat="server">
                        <input type="text" id="sLtvRPe" class="percent-field" readonly="readonly" runat="server" />
                        <input type="text" id="sLAmtCalcPe" class="money-field" readonly="readonly" runat="server" />
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="StandaloneSecondLienLtv" runat="server">
                        <input type="text" id="sLtvROtherFinPe2" data-field-id="sLtvROtherFinPe" class="percent-field" readonly="readonly" runat="server" />
                        <input type="text" id="sProOFinBalPe2"   data-field-id="sProOFinBalPe" class="money-field" readonly="readonly" runat="server" />
                        </asp:PlaceHolder>
                    </div>
                </div>
                <div id="LineAmountRow" class="row" runat="server">
                    <div>
                        <label for="sCreditLineAmt">Line Amount</label>
                    </div>
                    <div>
                        <input type="text" id="sCreditLineAmt" class="money-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label>2nd Financing?</label>
                    </div>
                    <div>
                        <input type="radio" id="No2ndFinancing" name="sHasSecondFinancingT" disabled="disabled" runat="server" />
                        <label for="No2ndFinancing">No</label>
                        <input type="radio" id="Yes2ndFinancing" name="sHasSecondFinancingT" disabled="disabled" runat="server" />
                        <label for="Yes2ndFinancing">Yes</label>
                    </div>
                </div>
                <div id="SecondFinancingTypeRow" class="row" runat="server">
                    <div>
                        <label>2nd Financing Type</label>
                    </div>
                    <div>
                        <input type="radio" id="ClosedEnd2ndFinancing" name="sSubFinT" disabled="disabled" runat="server" />
                        <label for="ClosedEnd2ndFinancing">Closed-end</label>
                        <input type="radio" id="Heloc2ndFinancing" name="sSubFinT" disabled="disabled" runat="server" />
                        <label for="Heloc2ndFinancing">HELOC</label>
                    </div>
                </div>
                <asp:PlaceHolder ID="SecondFinancingFields" runat="server">
                <div class="row">
                    <div>
                        <label id="SecondFinancingAmountLabel" for="sLtvROtherFinPe" runat="server">Initial Draw Amount</label>
                    </div>
                    <div>
                        <asp:PlaceHolder ID="FirstLienOtherFinancing" runat="server">
                        <input type="text" id="sLtvROtherFinPe" class="percent-field" readonly="readonly" runat="server" />
                        <input type="text" id="sProOFinBalPe" class="money-field" readonly="readonly" runat="server" />
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="StandaloneSecondLienOtherFinancing" runat="server">
                        <input type="text" id="sLtvRPe2" data-field-id="sLtvRPe" class="percent-field" readonly="readonly" runat="server" />
                        <input type="text" id="sLAmtCalcPe2" data-field-id="sLAmtCalcPe" class="money-field" readonly="readonly" runat="server" />
                        </asp:PlaceHolder>
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sCltvRPe" class="singleLine">CLTV</label>
                    </div>
                    <div>
                        <input type="text" id="sCltvRPe" class="percent-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                </asp:PlaceHolder>

                <asp:PlaceHolder ID="SecondFinancingHelocFields" runat="server">
                <div class="row">
                    <div>
                        <label for="sSubFinToPropertyValue">Line Amount</label>
                    </div>
                    <div>
                        <input type="text" id="sSubFinToPropertyValue" class="percent-field" readonly="readonly" runat="server" />
                        <input type="text" id="sSubFinPe" class="money-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sHCLTVRPe">HCLTV</label>
                    </div>
                    <div>
                        <input type="text" id="sHCLTVRPe" class="percent-field" readonly="readonly" runat="server" />
                    </div>
                </div>
                </asp:PlaceHolder>
            </div>

            <div class="pricing-options-container">
            <div class="container pricing-options">
                <h4>Pricing Options</h4>
                <div class="row">
                    <div>
                        <input type="radio" id="frontEndPricing" name="pricingOption" value="FrontEnd" runat="server" />
                        <label for="frontEndPricing">Front-end Pricing</label>
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sProdRLckdDays">Front-end Rate Lock Period</label>
                    </div>
                    <div>
                        <input type="text" id="sProdRLckdDays" class="count-field disable-for-investor-pricing" runat="server" />
                        days
                    </div>
                </div>
                <div class="row" id="FrontEndFinalPriceContainer" runat="server">
                    <div>
                        <label for="sBrokerLockOriginatorPriceBrokComp1PcPrice">Front-end Final Price</label>
                    </div>
                    <div>
                        <input runat="server" id="sBrokerLockOriginatorPriceBrokComp1PcPrice"  readonly="readonly"/>
                        <input runat="server" id="sBrokerLockFinalBrokComp1PcPrice"  readonly="readonly"/>
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sProdLpePriceGroupNm">Price Group</label>
                    </div>
                    <div>
                        <input type="text" id="sProdLpePriceGroupNm" readonly="readonly" runat="server" />
                    </div>
                </div>
                <div class="row">
                    <div>
                        <input type="radio" id="investorPricing" name="pricingOption" value="Investor" runat="server" />
                        <label for="investorPricing">Investor Pricing</label>
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sProdInvestorRLckdDays">Investor Rate Lock Period</label>
                    </div>
                    <div>
                        <input type="text" id="sProdInvestorRLckdDays" class="count-field disable-for-front-end-pricing data-refresh" runat="server" />
                        days
                    </div>
                    <div>
                        <!-- Value comes from code behind -->
                        <input type="radio" id="editInvestorRateLockPeriod" name="sProdInvestorRLckdModeT" class="disable-for-front-end-pricing" runat="server" />
                        <label for="editInvestorRateLockPeriod">Edit</label>
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sProdInvestorRLckdExpiredD">Investor Rate Lock Expiration</label>
                    </div>
                    <div>
                        <input type="text" id="sProdInvestorRLckdExpiredD" class="date-field disable-for-front-end-pricing data-refresh" preset="date" runat="server" /><img src="../../images/pdate.gif" id="InvestorRateLockCalendar" title="Open calendar" />
                    </div>
                    <div>
                        <!-- Value comes from code behind -->
                        <input type="radio" id="editInvestorRateExpiration" name="sProdInvestorRLckdModeT" class="disable-for-front-end-pricing" runat="server" />
                        <label for="editInvestorRateExpiration">Edit</label>
                    </div>
                </div>
                <div class="row" runat="server" id="InvestorFinalPriceContainer">
                    <div>
                        <label for="sInvestorLockBrokComp1PcPrice">Investor Final Price</label>
                    </div>
                    <div>
                        <input runat="server" id="sInvestorLockBrokComp1PcPrice" readonly="readonly" />
                    </div>
                </div>
                <div class="row">
                    <div>
                        <label for="sInvestorLockLpePriceGroupId">Price Group</label>
                    </div>
                    <div>
                        <select id="sInvestorLockLpePriceGroupId" class="disable-for-front-end-pricing" runat="server"></select>
                    </div>
                </div>
            </div>

            <asp:PlaceHolder ID="GetResultsUsing" runat="server">
            <div class="container get-results-using">
                <div class="row">
                    <div class="sub-header">Get Results Using</div>
                </div>
                <div id="HistoricalPricingRow" class="row">
                    <div>
                        <!-- Value for the radio is set by the server -->
                        <input type="radio" id="HistoricalPricing" name="GetResultsUsing" runat="server" />
                        <label for="HistoricalPricing">Historical Pricing</label>
                        <a id="explainHistorical" href="#" title="When pricing with the Historical Pricing option, results will be returned as if pricing off of the effective ratesheet of the values entered for the date and time.">explain</a>
                    </div>
                </div>
                <div id="WorstCasePricingRow" class="row" runat="server">
                    <div>
                        <!-- Value for the radio is set by the server -->
                        <input type="radio" id="WorstCasePricing" name="GetResultsUsing" runat="server" />
                        <label for="WorstCasePricing">Worst-Case Pricing</label>
                        <a id="explainWorstCase" href="#" title="Worst-Case pricing for a loan program is determined by comparing the final price of rate options between the inputted historical date and time and current pricing. The lower priced rate option will be considered worse.">explain</a>
                    </div>
                </div>
                <div class="row">
                    <div>
                        <!-- Value for the radio is set by the server -->
                        <input type="radio" id="CurrentPricing" name="GetResultsUsing" runat="server" />
                        <label for="CurrentPricing">Current Pricing</label>
                    </div>
                </div>
                <div class="row">
                    <div id="HistoricalFields">
                        <span id="HistoricalPricingTopLabel">Compare the results from the date and time of</span><br />
                        <select id="HistoricalSubmissions" runat="server"></select><br />
                        <input type="text" id="HistoricalDate" readonly="readonly" runat="server" /><br />
                        <span id="HistoricalPricingBottomLabel"></span>
                    </div>
                </div>
            </div>
            </asp:PlaceHolder>

            <div class="price">
                <div>
                    <input type="button" class="price-button" value="Price" />
                </div>
            </div>
            </div>
        </div>

        <div class="right-column">
            <asp:PlaceHolder ID="ResultsFilter" runat="server">
            <div class="results-filter">
                <div class="mask-when-pricing"></div>
                <h4>Results Filter and Display Options</h4>
                <div class="filters">
                    <div class="internal-filters">
                        <div>
                            <input type="checkbox" id="sProdFilterRestrictResultToRegisteredProgram" runat="server" />
                            <label for="sProdFilterRestrictResultToRegisteredProgram">Restrict results to registered program?</label>
                        </div>
                        <div>
                            <input type="checkbox" id="sProdFilterRestrictResultToCurrentRegistered" class="disable-for-registered-program" runat="server" />
                            <label for="sProdFilterRestrictResultToCurrentRegistered">Restrict results to registered product type?</label>
                        </div>
                        <div>
                            <input type="checkbox" id="sProdFilterRestrictResultToCurrentInvestor" class="disable-for-registered-program" runat="server" />
                            <label for="sProdFilterRestrictResultToCurrentInvestor">Restrict results to current investor?</label>
                        </div>
                        <div>
                            <input type="checkbox" id="sProdFilterMatchCurrentTerm" class="disable-for-registered-program" runat="server" />
                            <label for="sProdFilterMatchCurrentTerm">Restrict results to current amortization type and term?</label>
                        </div>
                        <div>
                            <input type="checkbox" id="sProdFilterDisplayUsingCurrentNoteRate" runat="server" />
                            <label for="sProdFilterDisplayUsingCurrentNoteRate">Restrict results to current note rate?</label>
                        </div>
                        <div>
                            <input type="checkbox" id="sProdFilterDisplayrateMerge" class="disable-for-registered-program" runat="server" />
                            <label for="sProdFilterDisplayrateMerge">Display best prices per program?</label>
                        </div>
                        <div>
                            <span class="red">*Current Pricing is expired when displayed in red text.</span>
                        </div>
                    </div>
                    <div class="term-amort-prod-filters">
                        <table id="term-filters">
                            <thead>
                                <tr>
                                    <th colspan="2">Term</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input id="sProdFilterDue10Yrs" type="checkbox" runat="server" />
                                        <label for="sProdFilterDue10Yrs">10yr</label>
                                    </td>
                                    <td>
                                        <input id="sProdFilterDue25Yrs" type="checkbox" runat="server" />
                                        <label for="sProdFilterDue25Yrs">25yr</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="sProdFilterDue15Yrs" type="checkbox" runat="server" />
                                        <label for="sProdFilterDue15Yrs">15yr</label>
                                    </td>
                                    <td>
                                        <input id="sProdFilterDue30Yrs" type="checkbox" runat="server" />
                                        <label for="sProdFilterDue30Yrs">30yr</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="sProdFilterDue20Yrs" type="checkbox" runat="server" />
                                        <label for="sProdFilterDue20Yrs">20yr</label>
                                    </td>
                                    <td>
                                        <input id="sProdFilterDueOther" type="checkbox" runat="server" />
                                        <label for="sProdFilterDueOther">Other</label>
                                    </td>
                                </tr>
                            </tbody>

                            <thead>
                                <tr>
                                    <th colspan="2">Payment Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input id="sProdFilterPmtTPI" type="checkbox" runat="server" />
                                        <label for="sProdFilterPmtTPI">P&I</label>
                                    </td>
                                    <td>
                                        <input id="sProdFilterPmtTIOnly" type="checkbox" runat="server" />
                                        <label for="sProdFilterPmtTIOnly">I/O</label>
                                    </td>
                                </tr>
                            </tbody>

                        </table>
                        <table id="amort-filters">
                            <thead>
                                <tr>
                                    <th colspan="2">Amortization Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input id="sProdFilterFinMethFixed" type="checkbox" runat="server" />
                                        <label for="sProdFilterFinMethFixed">Fixed</label>
                                    </td>
                                    <td>
                                        <input id="sProdFilterFinMeth7YrsArm" type="checkbox" runat="server" />
                                        <label for="sProdFilterFinMeth7YrsArm">7yr ARM</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="sProdFilterFinMeth3YrsArm" type="checkbox" runat="server" />
                                        <label for="sProdFilterFinMeth3YrsArm">3yr ARM</label>
                                    </td>
                                    <td>
                                        <input id="sProdFilterFinMeth10YrsArm" type="checkbox" runat="server" />
                                        <label for="sProdFilterFinMeth10YrsArm">10yr ARM</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="sProdFilterFinMeth5YrsArm" type="checkbox" runat="server" />
                                        <label for="sProdFilterFinMeth5YrsArm">5yr ARM</label>
                                    </td>
                                    <td>
                                        <input id="sProdFilterFinMethOther" type="checkbox" runat="server" />
                                        <label for="sProdFilterFinMethOther">Other</label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table id="prod-filters">
                            <thead>
                                <tr>
                                    <th>Product Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input id="sProdIncludeNormalProc" type="checkbox" runat="server" />
                                        <label for="sProdIncludeNormalProc">Conventional</label>
                                    </td>
                                    <td>
                                        <input id="sProdIncludeFHATotalProc" type="checkbox" runat="server" />
                                        <label for="sProdIncludeFHATotalProc">FHA</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="sProdIncludeMyCommunityProc" type="checkbox" runat="server" />
                                        <label for="sProdIncludeMyCommunityProc">HomeReady</label>
                                    </td>
                                    <td>
                                        <input id="sProdIncludeVAProc" type="checkbox" runat="server" />
                                        <label for="sProdIncludeVAProc">VA</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="sProdIncludeHomePossibleProc" type="checkbox" runat="server" />
                                        <label for="sProdIncludeHomePossibleProc">Home Possible</label>
                                    </td>
                                    <td>
                                        <input id="sProdIncludeUSDARuralProc" type="checkbox" runat="server" />
                                        <label for="sProdIncludeUSDARuralProc">USDA</label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div>
                            <a runat="server" id="advancedFilterBtn">Advanced Filter Options</a>
                        </div>
                    </div>
                    <div class="price">
                        <div>
                            <input type="button" class="price-button" value="Price" />
                        </div>
                        <div><span id="NumLPs" class="sub-header">Number of Programs:</span></div>
                    </div>
                </div>
            </div>
            </asp:PlaceHolder>

            <div id="PricingResultsContainer" ng-app="pricingResults" ng-controller="pricingResultsController">
                <div id="LoadingMessage">
                    <img id="resultsLoadingGif" src="../../images/loading.gif" />
                    <p>Loading...</p>
                </div>
                <div id="ResultsContainer"></div>
            </div>
        </div>
        </div>
        <div class="Hidden">
            <uc1:QualifyingBorrower runat="server" ID="QualifyingBorrower" />
        </div>
    </form>
</body>
</html>
