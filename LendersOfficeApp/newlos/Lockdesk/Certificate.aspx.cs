﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;
using System.Xml.Xsl;
using DataAccess;
using System.Xml;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.ObjLib.Resource;

namespace LendersOfficeApp.newlos.Lockdesk
{
    public partial class Certificate : BaseXsltPage
    {
        private AbstractUserPrincipal BrokerUser
        {
            get { return (AbstractUserPrincipal)Page.User; }
        }
        protected override string XsltFileLocation
        {
            get
            {
                bool forceNoCustomCert = RequestHelper.GetBool("detailCert") && (Page.User as AbstractUserPrincipal)?.CanViewDualCertificate == true;
                return ResourceManager.Instance.GetResourcePath(ResourceType.PmlCertificateXslt, this.BrokerUser.BrokerId, forceNoCustomCert);
            }
        }
        protected override System.Xml.Xsl.XsltArgumentList XsltParams
        {
            get
            {
                E_sPricingModeT sPricingModeT = (E_sPricingModeT)RequestHelper.GetInt("sPricingModeT", 0);
                AbstractUserPrincipal principal = BrokerUser;

                Guid sLId = RequestHelper.GetGuid("loanid", Guid.Empty);
                BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);
                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("LenderPmlSiteId", "", Tools.GetFileDBKeyForPmlLogoWithLoanId(brokerDB.PmlSiteID, sLId));
                args.AddParam("VirtualRoot", "", Tools.VRoot);
                args.AddParam("IsMoreDetailCert", "", "True");
                args.AddParam("ShowDetermination", "", "False"); // 1/17/2011 dd - OPM 30649 - We do not display Determination section.
                if (sPricingModeT == E_sPricingModeT.EligibilityBrokerPricing || sPricingModeT == E_sPricingModeT.EligibilityInvestorPricing)
                {
                    // 1/26/2011 dd - OPM 30649 - We do not display pricing section in Eligibility mode.
                    args.AddParam("ShowPricing", "", "False");
                }
                if(brokerDB.HidePmlCertSensitiveFields)
                {
                    args.AddParam("HideSensitiveFields", "", "True");
                }
                args.AddParam("Timing", "", "0"); // 1/24/2011 dd - We current do not support debug timing for internal pricing.
                // OPM 3825 - Allow users with Admin & LockDesk role to see hidden adjustments
                if (principal.HasPermission(Permission.CanModifyLoanPrograms) ||
                    principal.HasAtLeastOneRole(new E_RoleT[] { E_RoleT.Administrator, E_RoleT.LockDesk }))
                {
                    args.AddParam("ShowHiddenAdj", "", "True");
                }
                // 10/11/07 mf. OPM 18269 - Users with new permission can see hidden stips (conditions)
                if (principal.HasPermission(Permission.CanViewHiddenInformation) ||
                    principal.HasPermission(Permission.CanModifyLoanPrograms))
                {
                    args.AddParam("ShowHiddenConditions", "", "True");
                }

                if (ConstSite.DisplayPolicyAndRuleIdOnLpeResult)
                {
                    args.AddParam("ShowDebugInfo", "", "True");
                }

                bool IsNewPmlUIEnabled;
                BrokerDB CurrentBroker = BrokerDB.RetrieveById(BrokerUser.BrokerId);
                if (CurrentBroker.IsNewPmlUIEnabled)
                {
                    IsNewPmlUIEnabled = true;
                }
                else
                {
                    var empDB = EmployeeDB.RetrieveById(BrokerUser.BrokerId, BrokerUser.EmployeeId);
                    IsNewPmlUIEnabled = empDB.IsNewPmlUIEnabled;
                }
                args.AddParam("IsNewPmlUIEnabled", "", IsNewPmlUIEnabled.ToString());

                return args;

            }
        }

        protected override void GenerateXmlData(XmlWriter writer)
        {

            bool hasInvalidRateOptionError = RequestHelper.GetBool("hasInvalidRateOptionError");
            if (hasInvalidRateOptionError)
            {
                XmlDocument errorDoc = new XmlDocument();
                XmlElement root = errorDoc.CreateElement("InvalidRateOptionVersion");
                errorDoc.AppendChild(root);

                errorDoc.WriteTo(writer);

                return;
            }

            UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultItem(RequestHelper.GetGuid("requestid"));

            if (null == resultItem || resultItem.IsErrorMessage)
            {
                if (null != resultItem && resultItem.ErrorMessage == ErrorMessages.InvalidRateOptionVersion)
                {
                    XmlDocument errorDoc = new XmlDocument();
                    XmlElement root = errorDoc.CreateElement("InvalidRateOptionVersion");
                    errorDoc.AppendChild(root);

                    errorDoc.WriteTo(writer);
                }
                else
                {
                    ErrorUtilities.DisplayErrorPage(new CBaseException(null == resultItem ? ErrorMessages.Generic : resultItem.ErrorMessage,
                        null == resultItem ? ErrorMessages.Generic : resultItem.ErrorMessage), false, Guid.Empty, Guid.Empty);
                }
                return;
            }
            XmlDocument doc = resultItem.CertificateXmlDocument;

            if (null == doc)
                throw new GenericUserErrorMessageException("DistributeUnderwritingEngine.RetrieveResults return null for RequestID = " + RequestHelper.GetGuid("requestid"));

            doc.WriteTo(writer);

        }
    }
}
