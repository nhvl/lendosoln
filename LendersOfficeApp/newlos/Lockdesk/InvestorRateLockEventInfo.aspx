﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvestorRateLockEventInfo.aspx.cs" Inherits="LendersOfficeApp.newlos.Lockdesk.InvestorRateLockEventInfo" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Audit History Details - Investor Rate Lock</title>
    <style type="text/css">
        body { padding-left: 5px; background-color: gainsboro; padding: 5px;}
       .rate { width: 50px; }
       .desc { width: 195px; }
    </style>
</head>
<body>
    <form id="form1" runat="server" onreset="return false;">
    <div>
        <br />
        <ml:EncodedLabel CssClass="FieldLabel" runat="server" ID="EventType"></ml:EncodedLabel>
        <br />
        <br />
        <span class="FieldLabel"> Date: </span><ml:EncodedLabel runat="server" ID="CausedOn"></ml:EncodedLabel>
        <br />
        <span class="FieldLabel"> By: </span> <ml:EncodedLabel runat="server" ID="By"></ml:EncodedLabel>
        <br />
        <span class="FieldLabel">Investor Name: </span><ml:EncodedLabel runat="server" ID="sInvestorLockLpInvestorNm"></ml:EncodedLabel>
        <br />
        <span class="FieldLabel">Investor Program Description: </span><ml:EncodedLabel runat="server" ID="sInvestorLockLpTemplateNm"></ml:EncodedLabel>
        <br />
        <span class="FieldLabel">Investor Lock Period: </span><ml:EncodedLabel runat="server" ID="sInvestorLockRLckdDays"></ml:EncodedLabel>
        <br />
        <span class="FieldLabel">Investor Rate Lock Date: </span><ml:EncodedLabel runat="server" ID="sInvestorLockRLckdD"></ml:EncodedLabel>
        <br />
        <span class="FieldLabel">Investor Rate Lock Expiration: </span><ml:EncodedLabel runat="server" ID="sInvestorLockRLckExpiredD"></ml:EncodedLabel>
        <br />
        <span class="FieldLabel">Investor Delivery Lock Expiration: </span><ml:EncodedLabel runat="server" ID="sInvestorLockDeliveryExpiredD"></ml:EncodedLabel>
        <br />
        <span class="FieldLabel">Investor Loan #: </span><ml:EncodedLabel runat="server" ID="sInvestorLockLoanNum"></ml:EncodedLabel>
        <br />
        <span class="FieldLabel">Investor Program Identifier: </span><ml:EncodedLabel runat="server" ID="sInvestorLockProgramId"></ml:EncodedLabel>
        <br />
        <span class="FieldLabel">Investor Lock Confirmation #: </span><ml:EncodedLabel runat="server" ID="sInvestorLockConfNum"></ml:EncodedLabel>
            
        <br />
        <span class="FieldLabel">Investor Rate Sheet Effective Time: </span><ml:EncodedLabel runat="server" ID="sInvestorLockRateSheetEffectiveTime"></ml:EncodedLabel>
        <br />
        
        <span class="FieldLabel">Investor Lock Fee: </span><ml:EncodedLabel runat="server" ID="sInvestorLockLockFee"></ml:EncodedLabel>
        <br />
    
        <span class="FieldLabel">Investor Commitment Type: </span><ml:EncodedLabel runat="server" ID="sInvestorLockCommitmentT"></ml:EncodedLabel>
        <br />
        <br />
        <table cellpadding="2" cellspacing="0" border="1" >
            <tbody>
                <tr>
                    <td class="desc">&nbsp;</td>
                    <td class="FieldLabel rate" align="center" >Rate</td>
                    <td class="FieldLabel rate" align="center" >Price</td>
                    <td class="FieldLabel rate" align="center" >Fee</td>
                    <td class="FieldLabel rate" align="center" >Margin</td>
                    <td class="FieldLabel rate" align="center" >Teaser<br />Rate</td>
                </tr>
                <tr>
                    <td class="FieldLabel"> Base Price </td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sInvestorLockBaseNoteIR"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sInvestorLockBaseBrokComp1PcPrice"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sInvestorLockBaseBrokComp1PcFee"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sInvestorLockBaseRAdjMarginR"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sInvestorLockBaseOptionArmTeaserR" ></ml:EncodedLiteral></td>
                </tr>
                <tr>
                    <td class="FieldLabel"> Total Adjustments </td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sInvestorLockTotAdjNoteIR" ></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sInvestorLockTotAdjBrokComp1PcPrice" ></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sInvestorLockTotAdjBrokComp1PcFee" ></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sInvestorLockTotAdjRAdjMarginR" ></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sInvestorLockTotAdjOptionArmTeaserR" ></ml:EncodedLiteral></td>
                </tr>
                <tr>
                    <td class="FieldLabel"> Adjusted Price </td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sInvestorLockNoteIR"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sInvestorLockBrokComp1PcPrice"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sInvestorLockBrokComp1Pc"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sInvestorLockRAdjMarginR"></ml:EncodedLiteral></td>
                    <td align="right"><ml:EncodedLiteral runat="server" ID="sInvestorLockOptionArmTeaserR" ></ml:EncodedLiteral></td>
                </tr>
            </tbody>
        </table>

        <br />
        <br />

        <asp:Repeater runat="server" ID="AdjustmentDataRepeater">
            <HeaderTemplate>
                <table cellpadding="2" cellspacing="0" border="1" >
                    <thead>
                        <tr>
                            <th class="FieldLabel desc" align="center">
                                Adjustments
                            </th>
                            <th class="FieldLabel rate" align="center">
                                Rate
                            </th>
                            <th class="FieldLabel rate" align="center">
                                Price
                            </th>
                            <th class="FieldLabel rate" align="center">
                                Fee
                            </th>
                            <th class="FieldLabel rate" align="center">
                                Margin
                            </th>
                            <th class="FieldLabel rate" align="center">
                                Teaser<br />
                                Rate
                            </th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td >
                        <%# LendersOffice.AntiXss.AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Description )%>
                    </td>
                    <td align="right">
                        <%# LendersOffice.AntiXss.AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Rate )%>
                    </td>
                    <td align="right">
                        <%# LendersOffice.AntiXss.AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Price)%>
                    </td>
                    <td align="right">
                        <%# LendersOffice.AntiXss.AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Fee)%>
                    </td>
                    <td align="right">
                        <%# LendersOffice.AntiXss.AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).Margin)%>
                    </td>
                    <td align="right">
                        <%# LendersOffice.AntiXss.AspxTools.HtmlString(((DataAccess.PricingAdjustment)Container.DataItem).TeaserRate )%>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
            </table>
            </FooterTemplate>
        </asp:Repeater>
        

        <br />
        <br />
        <div>
       <input style="margin-left: 271px" type="button" id="cls" value="Close" class="ButtonStyle" onclick="onClosePopup();" />
        </div>
    </div>
    </form>
</body>
</html>
