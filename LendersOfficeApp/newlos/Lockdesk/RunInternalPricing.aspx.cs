﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Security;
using DataAccess.LoanValidation;
using LendersOffice.Common;
using LendersOffice.RatePrice;

namespace LendersOfficeApp.newlos.Lockdesk
{
    public partial class RunInternalPricing : BaseLoanPage
    {
        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowLockDeskRead };
            }
        }

        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowLockDeskWrite };
            }
        }

        private E_sPricingModeT sPricingModeT
        {
            get { return (E_sPricingModeT)RequestHelper.GetInt("sPricingModeT", 0); }
        }
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(RunInternalPricing));
            dataLoan.InitLoad();
            // OPM 237202.  To allow the PG data to flow like standard PML.
            dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);

            var resultList = Tools.RunInternalPricingValidation(dataLoan, this.BrokerUser);
            if (resultList.ErrorCount > 0)
            {
                Response.Redirect("~/newlos/InternalPricingValidationErrorPage.aspx?loanid=" + LoanID);
                return;
            }

            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
            Tools.SetDropDownListValue(sInvestorLockLpePriceGroupId
                    , (   dataLoan.sInvestorLockLpePriceGroupId == Guid.Empty 
                        ? dataLoan.sProdLpePriceGroupId
                        : dataLoan.sInvestorLockLpePriceGroupId
                      ).ToString() 
                );

            sLpTemplateNm.Text = dataLoan.sLpTemplateNm;
            sLpProductType.Text = dataLoan.sLpProductType;
            sTerm.Text = dataLoan.sTerm_rep;
            sDue.Text = dataLoan.sDue_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sRLckdD.Text = dataLoan.sRLckdD_rep;
            sRLckdDays.Text = dataLoan.sRLckdDays_rep;
            sRLckdExpiredD.Text = dataLoan.sRLckdExpiredD_rep;
            sRLckdExpiredInDays.Text = dataLoan.sRLckdExpiredInDays_rep;
            sProdRLckdDays.Text = dataLoan.sProdRLckdDays_rep;
            sProdFilterDisplayrateMerge.Checked = dataLoan.sProdFilterDisplayrateMerge;
            sProdFilterDisplayUsingCurrentNoteRate.Checked = dataLoan.sProdFilterDisplayUsingCurrentNoteRate;
            sProdFilterRestrictResultToCurrentRegistered.Checked = dataLoan.sProdFilterRestrictResultToCurrentRegistered;
            sProdFilterMatchCurrentTerm.Checked = dataLoan.sProdFilterMatchCurrentTerm;
            sProdFilterDue10Yrs.Checked = dataLoan.sProdFilterDue10Yrs;
            sProdFilterDue15Yrs.Checked = dataLoan.sProdFilterDue15Yrs;
            sProdFilterDue20Yrs.Checked = dataLoan.sProdFilterDue20Yrs;
            sProdFilterDue25Yrs.Checked = dataLoan.sProdFilterDue25Yrs;
            sProdFilterDue30Yrs.Checked = dataLoan.sProdFilterDue30Yrs;            
            sProdFilterDueOther.Checked = dataLoan.sProdFilterDueOther;
            sProdFilterFinMethFixed.Checked = dataLoan.sProdFilterFinMethFixed;            
            sProdFilterFinMeth3YrsArm.Checked = dataLoan.sProdFilterFinMeth3YrsArm;
            sProdFilterFinMeth5YrsArm.Checked = dataLoan.sProdFilterFinMeth5YrsArm;
            sProdFilterFinMeth7YrsArm.Checked = dataLoan.sProdFilterFinMeth7YrsArm;
            sProdFilterFinMeth10YrsArm.Checked = dataLoan.sProdFilterFinMeth10YrsArm;
            sProdFilterFinMethOther.Checked = dataLoan.sProdFilterFinMethOther;
            sProdFilterPmtTPI.Checked = dataLoan.sProdFilterPmtTPI;
            sProdFilterPmtTIOnly.Checked = dataLoan.sProdFilterPmtTIOnly;

            // We want to retrieve the name regardless of the lender setting for
            // displaying the price group in embedded PML. gf opm 450583
            sProdLpePriceGroupNm.Text = dataLoan.RetrievePriceGroupNameWithBypass();

            sProdInvestorRLckdDays.Text = dataLoan.sProdInvestorRLckdDays_rep;
            sProdInvestorRLckdExpiredD.Text = dataLoan.sProdInvestorRLckdExpiredD_rep;

            if (dataLoan.sProdInvestorRLckdModeT == E_sProdInvestorRLckdModeT.RateLockPeriod)
            {
                m_editRatePeriod.Checked = true;
            }
            else if (dataLoan.sProdInvestorRLckdModeT == E_sProdInvestorRLckdModeT.RateLockExpiredDate)
            {
                m_editRateDate.Checked = true;
            }
            if (dataLoan.sNoteIR_rep == "0.000%")
            {
                // 1/20/2011 dd - If note rate is 0 then disable "Restrict to current note rate" option.
                sProdFilterDisplayUsingCurrentNoteRate.Enabled = false;
                sProdFilterDisplayUsingCurrentNoteRate.Checked = false;
            }

            if (string.IsNullOrEmpty(dataLoan.sLpProductType))
            {
                sProdFilterRestrictResultToCurrentRegistered.Checked = false;
                sProdFilterRestrictResultToCurrentRegistered.Enabled = false;
            }

            switch (sPricingModeT)
            {
                case E_sPricingModeT.Undefined:
                case E_sPricingModeT.RegularPricing:
                case E_sPricingModeT.InternalBrokerPricing:
                    m_pricingOptionBroker.Checked = true;
                    break;
                case E_sPricingModeT.InternalInvestorPricing:
                    m_pricingOptionInvestor.Checked = true;
                    break;
                default:
                    break;
            }

            if (IsTestHistorical)
            {
                if (dataLoan.sHistoricalPricingVersionD == DateTime.MinValue)
                {
                    sHistoricalPricingVersionD.Text = string.Empty;

                    historicalPolicy.Items.RemoveAt(1); // Remove Historical option.
                    historicalRateOption.Items.RemoveAt(1); // Remove Historical option.
                }
                else
                {
                    sHistoricalPricingVersionD.Text = dataLoan.sHistoricalPricingVersionD.ToString("MM-dd-yyyy hh:mm:ss tt");
                }
            }
        }

        private void TempSetupHistoricalPricing()
        {
            tmpHistoricalTable.Visible = true;
            BindHistoricalDropDown(historicalPolicy);
            BindHistoricalDropDown(historicalRateOption);
        }
        private void BindHistoricalDropDown(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("Current Snapshot", E_LoanProgramSetPricingT.Snapshot.ToString("D")));
            ddl.Items.Add(new ListItem("Historical", E_LoanProgramSetPricingT.Historical.ToString("D")));
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Run Internal Pricing";
            this.PageID = "RunInternalPricing";
            this.DisplayCopyRight = false;

            Tools.Bind_sFinMethT(sFinMethT);

            foreach (var priceGroup in Broker.ListEnablePriceGroups())
            {
                sInvestorLockLpePriceGroupId.Items.Add(new ListItem(priceGroup.Value, priceGroup.Key.ToString()));
            }

            if (IsTestHistorical)
            {
                TempSetupHistoricalPricing();
            }
        }

        protected bool IsTestHistorical
        {
            get { return this.Broker.IsEnableHistoricalPricing; }
        }
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
