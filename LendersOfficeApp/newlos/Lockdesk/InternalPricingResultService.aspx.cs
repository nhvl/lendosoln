﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.Security;
using DataAccess;
using System.Runtime.Serialization;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.RatePrice;


namespace LendersOfficeApp.newlos.Lockdesk
{
    enum E_JsonResultModeT
    {
        Undefined,

        RateMerge,

        Regular
    }
    [DataContract]
    class JsonResult
    {
        public E_sPricingModeT sPricingModeT { get; private set; }
        public AbstractUserPrincipal CurrentPrincipal { get; private set; }
        public bool IncludeOriginatorComp { get; private set; }

        public JsonResult(AbstractUserPrincipal principal, E_sPricingModeT _sPricingModeT, bool includeOriginatorComp)
        {
            sPricingModeT = _sPricingModeT;
            CurrentPrincipal = principal;
            IncludeOriginatorComp = includeOriginatorComp;
        }
        [DataMember]
        public string[] Headers
        {
            get
            {
                switch (ResultMode)
                {
                    case E_JsonResultModeT.RateMerge:
                        if (sPricingModeT == E_sPricingModeT.InternalInvestorPricing)
                        {
                            return new string[] { "NOTE RATE", "PRICE ADJ", "PRICE", "FEE", "PAYMENT", "QUAL RATE", "MARGIN", "DTI", "MAX DTI", "", "" };
                        }
                        else
                        {
                            return new string[] { "NOTE RATE", "PRICE", "FEE", "PAYMENT", "QUAL RATE", "MARGIN", "DTI", "MAX DTI", "", "" };
                        }

                    case E_JsonResultModeT.Undefined:
                    case E_JsonResultModeT.Regular:
                        if (sPricingModeT == E_sPricingModeT.EligibilityInvestorPricing || sPricingModeT == E_sPricingModeT.EligibilityBrokerPricing)
                        {
                            // 1/17/2011 dd - We do not display PRICE and FEE in Check Eligibility result.
                            return new string[] { "NOTE RATE", "PAYMENT", "QUAL RATE", "MARGIN", "DTI" };
                        }
                        else
                        {
                            if (sPricingModeT == E_sPricingModeT.InternalBrokerPricing)
                            {
                                return new string[] { "NOTE RATE", "PRICE", "FEE", "PAYMENT", "QUAL RATE", "MARGIN", "DTI" };
                            }
                            else if (sPricingModeT == E_sPricingModeT.InternalInvestorPricing)
                            {
                                return new string[] { "NOTE RATE", "PRICE ADJ", "PRICE", "FEE", "PAYMENT", "QUAL RATE", "MARGIN", "DTI" };

                            }
                            else
                            {
                                throw CBaseException.GenericException("Unhandle sPricingModeT=" + sPricingModeT);
                            }
                        }

                    default:
                        throw new UnhandledEnumException(ResultMode);
                }
            }
        }
        [DataMember]
        public string[] IneligibleHeaders
        {
            get
            {
                if (CurrentPrincipal.HasPermission(Permission.CanApplyForIneligibleLoanPrograms) == false)
                {
                    return new string[0];
                }
                switch (sPricingModeT)
                {
                    case E_sPricingModeT.InternalBrokerPricing:
                    case E_sPricingModeT.InternalInvestorPricing:
                        return new string[] { "NOTE RATE", "PRICE", "FEE", "PAYMENT", "QUAL RATE", "MARGIN", "DTI" };
                    case E_sPricingModeT.EligibilityBrokerPricing:
                    case E_sPricingModeT.EligibilityInvestorPricing:
                        // 1/17/2011 dd - We do not display PRICE and FEE in Check Eligibility result.
                        return new string[] { "NOTE RATE", "PAYMENT", "QUAL RATE", "MARGIN", "DTI" };
                    default:
                        throw new UnhandledEnumException(ResultMode);
                }
            }
        }
        [DataMember]
        private List<JsonResultLoanProgram> EligiblePrograms
        {
            get
            {
                return m_eligiblePrograms;
            }
        }

        [DataMember(Name = "ResultMode")]
        private string ResultModeFriendlyName
        {
            get
            {
                // 1/8/2011 dd - Need this property because .NET framework does not use enum name when performing JSON serialization.
                return ResultMode.ToString();
            }
            set { }
        }
        private E_JsonResultModeT ResultMode = E_JsonResultModeT.Undefined;

        [DataMember]
        private List<JsonResultIneligibleLoanProgram> IneligiblePrograms = new List<JsonResultIneligibleLoanProgram>();


        public void Add(CApplicantPriceXml program)
        {
            if (ResultMode != E_JsonResultModeT.Undefined && ResultMode != E_JsonResultModeT.Regular)
            {
                throw new GenericUserErrorMessageException("You cannot mix and match between CApplicantPriceXml and RateMergeGroup");
            }
            if (program == null)
            {
                throw new GenericUserErrorMessageException("program is null in InternalPricingResultService.JsonResult");
            }
            ResultMode = E_JsonResultModeT.Regular;
            switch (program.Status)
            {
                case E_EvalStatus.Eval_Eligible:
                    m_eligiblePrograms.Add(new JsonResultLoanProgram(program, sPricingModeT, IncludeOriginatorComp));
                    break;
                case E_EvalStatus.Eval_Ineligible:
                    break;
                case E_EvalStatus.Eval_InsufficientInfo:
                    break;
                case E_EvalStatus.Eval_HideFromResult:
                    break;
                default:
                    throw new UnhandledEnumException(program.Status);
            }

        }

        private List<JsonResultLoanProgram> m_eligiblePrograms = new List<JsonResultLoanProgram>();

        public void Add(RateMergeGroup rateMergeGroup)
        {
            if (ResultMode != E_JsonResultModeT.Undefined && ResultMode != E_JsonResultModeT.RateMerge)
            {
                throw new GenericUserErrorMessageException("You cannot mix and match between CApplicantPriceXml and RateMergeGroup");
            }
            ResultMode = E_JsonResultModeT.RateMerge;
            m_eligiblePrograms.Add(new JsonResultLoanProgram(rateMergeGroup, sPricingModeT, IncludeOriginatorComp));
        }

        public void AddIneligibleLoanProgram(CApplicantPriceXml program)
        {
            IneligiblePrograms.Add(new JsonResultIneligibleLoanProgram(CurrentPrincipal, program, sPricingModeT, IncludeOriginatorComp));
        }

    }

    [DataContract]
    class JsonResultIneligibleLoanProgram
    {
        [DataMember]
        private Guid lLpTemplateId { get; set; }

        [DataMember]
        private string lLpTemplateNm { get; set; }

        [DataMember]
        private string lLpInvestorNm { get; set; }

        [DataMember]
        private string ProductCode { get; set; }

        [DataMember]
        private string MaxDTI { get; set; }

        [DataMember]
        private string UniqueChecksum { get; set; }

        [DataMember]
        private bool AreRatesExpired
        {
            get;
            set;
        }

        [DataMember]
        private List<string> DisqualifiedRules { get; set; }

        private bool IsHeloc { get; set; }

        public JsonResultIneligibleLoanProgram(AbstractUserPrincipal principal, CApplicantPriceXml program, E_sPricingModeT sPricingModeT, bool applyOriginatorComp)
        {
            if (program.Status != E_EvalStatus.Eval_Ineligible)
            {
                throw new GenericUserErrorMessageException("Only accept ineligible loan program");
            }
            lLpTemplateId = program.lLpTemplateId;
            lLpTemplateNm = program.lLpTemplateNm;
            MaxDTI = program.MaxDti_rep;
            UniqueChecksum = program.UniqueChecksum;
            AreRatesExpired = program.AreRatesExpired;
            lLpInvestorNm = program.lLpInvestorNm;
            ProductCode = program.ProductCode;
            IsHeloc = program.lLpProductType == "HELOC";

            DisqualifiedRules = new List<string>();
            foreach (var disqualifiedRule in program.DisqualifiedRuleList)
            {
                DisqualifiedRules.Add(disqualifiedRule);
            }
            if (principal.HasPermission(Permission.CanApplyForIneligibleLoanPrograms))
            {
                // 4/18/2011 dd - Only return ineligible rate when user has permission to apply for ineligible.
                if (program.ApplicantRateOptions.Length > 1)
                {
                    m_rateOptions = new List<List<string>>();
                    foreach (var rate in program.ApplicantRateOptions)
                    {
                        m_rateOptions.Add(GetRateOption(rate, E_JsonResultModeT.Regular, sPricingModeT, applyOriginatorComp));
                    }
                }
                if (program.RepresentativeRateOption != null && program.RepresentativeRateOption.Length > 0)
                {
                    m_representativeRate = GetRateOption(program.RepresentativeRateOption[0], E_JsonResultModeT.Regular, sPricingModeT, applyOriginatorComp);
                }
                else if (program.ApplicantRateOptions.Length > 0)
                {
                    // OPM 175387 - If no representative rate, set representative rate to first rate option (if it exists)
                    m_representativeRate = GetRateOption(program.ApplicantRateOptions[0], E_JsonResultModeT.Regular, sPricingModeT, applyOriginatorComp);
                }
            }
        }
        private List<string> m_representativeRate;

        [DataMember]
        private List<string> RepresentativeRate
        {
            get
            {
                return m_representativeRate;
            }
            set
            {
                // Need to have empty setter otherwise Json Serializer will complain on null.
            }

        }
        private List<List<string>> m_rateOptions;

        [DataMember]
        private List<List<string>> RateOptions
        {
            get
            {
                return m_rateOptions;
            }
            set 
            {
                // Need to have empty setter otherwise Json Serializer will complain on null.
            }
        }

        private List<string> GetRateOption(CApplicantRateOption rate, E_JsonResultModeT resultMode, E_sPricingModeT sPricingModeT, bool applyOriginatorComp)
        {
            if (null == rate)
            {
                return null;
            }
            List<string> list = new List<string>();

            // 1/7/2011 dd - The order here must match the order of column headers.
            list.Add(rate.Rate_rep);

            if (sPricingModeT != E_sPricingModeT.EligibilityBrokerPricing && sPricingModeT != E_sPricingModeT.EligibilityInvestorPricing)
            {
                if (sPricingModeT == E_sPricingModeT.InternalBrokerPricing && applyOriginatorComp)
                {
                    // OPM 114645
                    list.Add(rate.PointIncludingOriginatorCompIn100_rep);
                    list.Add(rate.PointIncludingOriginatorComp_rep);
                }
                else
                {
                    list.Add(rate.PointIn100_rep);
                    list.Add(rate.Point_rep);
                }
            }
            list.Add(rate.FirstPmtAmt_rep);
            list.Add(IsHeloc ? "" : rate.QRate_rep);
            list.Add(rate.Margin_rep);
            list.Add(rate.BottomRatio_rep);


            if (rate.IsDisqualified)
            {
                list.Add("d:" + rate.DisqualReason);
            }

            return list;
        }
    }
    [DataContract]
    class JsonResultLoanProgram
    {
        [DataMember]
        private Guid lLpTemplateId { get; set; }

        [DataMember]
        private string lLpTemplateNm { get; set; }

        [DataMember]
        private string lLpInvestorNm { get; set; }

        [DataMember]
        private string ProductCode { get; set; }

        [DataMember]
        private string MaxDti { get; set; }

        [DataMember]
        private string UniqueChecksum { get; set; }

        [DataMember]
        private bool AreRatesExpired
        {
            get;
            set;
        }

        private List<List<string>> m_rateOptions;
        [DataMember]
        private List<List<string>> RateOptions
        {
            get
            {
                return m_rateOptions;
            }
            set
            {
                // Need to have empty setter otherwise Json Serializer will complain on null.
            }
        }

        private List<string> m_representativeRate;

        [DataMember]
        private List<string> RepresentativeRate
        {
            get
            {
                return m_representativeRate;
            }
            set
            {
                // Need to have empty setter otherwise Json Serializer will complain on null.
            }

        }

        private bool IsHeloc { get; set; }

        public JsonResultLoanProgram(CApplicantPriceXml applicantPrice, E_sPricingModeT sPricingModeT, bool applyOriginatorComp)
        {
            lLpTemplateId = applicantPrice.lLpTemplateId;
            lLpTemplateNm = applicantPrice.lLpTemplateNm;
            lLpInvestorNm = applicantPrice.lLpInvestorNm;
            ProductCode = applicantPrice.ProductCode;
            UniqueChecksum = applicantPrice.UniqueChecksum;
            AreRatesExpired = applicantPrice.AreRatesExpired;
            IsHeloc = applicantPrice.lLpProductType == "HELOC";

            MaxDti = ParseDti(applicantPrice.MaxDti_rep);

            if (applicantPrice.ApplicantRateOptions.Length > 1)
            {
                // 1/17/2011 dd - Only return this list if there are more than 1 rate option. If there is one rate option then it will use
                // the representative rate.
                m_rateOptions = new List<List<string>>();
                foreach (var rate in applicantPrice.ApplicantRateOptions)
                {
                    rate.SetApplicantPrice(applicantPrice);
                    m_rateOptions.Add(GetRateOption(rate, E_JsonResultModeT.Regular, sPricingModeT, applyOriginatorComp));
                }
            }

            if (applicantPrice.RepresentativeRateOption != null && applicantPrice.RepresentativeRateOption.Length > 0)
            {
                applicantPrice.RepresentativeRateOption[0].SetApplicantPrice(applicantPrice);
                m_representativeRate = GetRateOption(applicantPrice.RepresentativeRateOption[0], E_JsonResultModeT.Regular, sPricingModeT, applyOriginatorComp);
            }
        }
        public JsonResultLoanProgram(RateMergeGroup rateMergeGroup, E_sPricingModeT sPricingModeT, bool applyOriginatorComp)
        {
            lLpTemplateId = Guid.Empty;
            lLpTemplateNm = rateMergeGroup.Name;

            IsHeloc = rateMergeGroup.lProductType == "HELOC";

            m_rateOptions = new List<List<string>>();
            foreach (CApplicantRateOption rate in rateMergeGroup.RateOptions)
            {
                m_rateOptions.Add(GetRateOption(rate, E_JsonResultModeT.RateMerge, sPricingModeT, applyOriginatorComp));
                AreRatesExpired = rate.IsRateExpired;

            }
        }
        private string ParseDti(string dti)
        {
            decimal value = 0;
            if (string.IsNullOrEmpty(dti) == false)
            {
                if (decimal.TryParse(dti.Replace("%", ""), out value) == false)
                {
                    value = -1;
                }
            }

            if (value > 0)
            {
                return value.ToString();
            }
            else
            {
                return "N/A";
            }


        }


        private List<string> GetRateOption(CApplicantRateOption rate, E_JsonResultModeT resultMode, E_sPricingModeT sPricingModeT, bool applyOriginatorComp)
        {

            if (null == rate)
            {
                return null;
            }
            List<string> list = new List<string>();

            // 1/7/2011 dd - The order here must match the order of column headers.
            list.Add(rate.Rate_rep);

            if (sPricingModeT == E_sPricingModeT.InternalInvestorPricing)
            {
                // 7/8/2011 dd - OPM 67783 - Add Total Adjustments to internal pricing investor mode results screen.
                list.Add(rate.TotalPriceAdj_rep);
            }
            if (sPricingModeT != E_sPricingModeT.EligibilityBrokerPricing && sPricingModeT != E_sPricingModeT.EligibilityInvestorPricing)
            {
                if (sPricingModeT == E_sPricingModeT.InternalBrokerPricing && applyOriginatorComp)
                {
                    // OPM 114645
                    list.Add(rate.PointIncludingOriginatorCompIn100_rep);
                    list.Add(rate.PointIncludingOriginatorComp_rep);
                }
                else
                {
                    list.Add(rate.PointIn100_rep);
                    list.Add(rate.Point_rep);
                }
            }
            list.Add(rate.FirstPmtAmt_rep);
            list.Add(IsHeloc ? "" : rate.QRate_rep);
            list.Add(rate.Margin_rep);
            list.Add(rate.BottomRatio_rep);

            if (resultMode == E_JsonResultModeT.RateMerge)
            {
                list.Add(ParseDti(rate.MaxDti_rep));
                // 1/8/2011 dd - For rate merge, we need loan program name and program id since each rate in the group can come from different loan program.
                // 1/21/2011 dd - DO NOT MODIFY THE ORDER OF THESE INSERT. MODIFY THE ORDER OF INSERT REQUIRE UPDATE THE JS
                list.Add(rate.LpTemplateNm);
                list.Add(rate.LpInvestorNm + " " + rate.ApplicantPrice.ProductCode);
                list.Add(rate.LpTemplateId.ToString());
                list.Add(rate.ApplicantPrice.UniqueChecksum);
            }

            if (rate.IsDisqualified)
            {
                list.Add("d:" + rate.DisqualReason);
            }
            return list;
        }
    }
    public partial class InternalPricingResultService : BaseSimpleServiceXmlPage
    {


        protected override void Process(string methodName)
        {
            switch (methodName) 
            {
                case "RequestPricing":
                    RequestPricing();
                    break;
                case "IsResultReady":
                    IsResultReady();
                    break;
                case "GetResult":
                    GetResult();
                    break;
            }
        }

        private void RequestPricing()
        {
            Guid sLId = GetGuid("loanid");
            E_sPricingModeT sPricingModeT = (E_sPricingModeT)GetInt("sPricingModeT");
            AbstractUserPrincipal principal = this.User as AbstractUserPrincipal;
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            HybridLoanProgramSetOption historicalOption = options;

            BrokerDB broker = BrokerDB.RetrieveById(principal.BrokerId);

            if (broker.IsEnableHistoricalPricing)
            {
                historicalOption = new HybridLoanProgramSetOption();
                historicalOption.ApplicablePolicies = (E_LoanProgramSetPricingT)GetInt("historicalPolicy", 0);
                historicalOption.RateOptions = (E_LoanProgramSetPricingT)GetInt("historicalRateOption", 0);
            }

            Guid requestId = DistributeUnderwritingEngine.RequestInternalPricingResult(principal, sLId, sPricingModeT, historicalOption);

            SetResult("RequestId", requestId);
        }

        private void IsResultReady()
        {
            Guid requestId = GetGuid("requestid");

            bool isReady = LpeDistributeResults.IsResultAvailable(requestId);

            SetResult("IsReady", isReady);
        }

        private void GetResult()
        {


            Guid requestId = GetGuid("requestid");

            UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultItem(requestId);

            if (null == resultItem)
            {
                throw new CBaseException(ErrorMessages.Generic, "UnderwritingResultItem is null");
            }
            if (resultItem.IsErrorMessage)
            {
                throw new CBaseException(resultItem.ErrorMessage, resultItem.ErrorMessage);
            }
            AbstractUserPrincipal principal = this.User as AbstractUserPrincipal;

            Guid sLId = GetGuid("LoanID");
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(InternalPricingResultService));
            dataLoan.InitLoad();
            dataLoan.sPricingModeT = (E_sPricingModeT)GetInt("sPricingModeT");

            bool addOriginatorComp = (dataLoan.BrokerDB.IsUsePriceIncludingCompensationInPricingResult
                && dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees);

            JsonResult result = new JsonResult(principal, dataLoan.sPricingModeT, addOriginatorComp);
            bool isRateMerge = dataLoan.sProdFilterDisplayrateMerge;

            if (isRateMerge)
            {
                BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);
                foreach (var rateMergeGroup in resultItem.GetRateMergeResult(brokerDB))
                {
                    result.Add(rateMergeGroup);
                }
            }
            else
            {
                foreach (var program in resultItem.GetEligibleLoanProgramList())
                {
                    result.Add(program);
                }
            }

            foreach (var ineligibleProgram in resultItem.GetIneligibleLoanProgramList())
            {
                result.AddIneligibleLoanProgram(ineligibleProgram);
            }
            string s = ObsoleteSerializationHelper.JsonSerialize(result);
            //Tools.LogInfo(s);
            long startTicks = GetLong("StartTicks", 0);
            if (startTicks > 0)
            {
                int duration = (int)(DateTime.Now.Ticks - startTicks) / 10000;
                DistributeUnderwritingEngine.RecordPricingTotalTime(requestId, dataLoan.sPricingModeT.ToString(), duration);
            }
            SetResult("json", s);
        }
    }
}
