﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Lockdesk
{
    public partial class InternalPricingResult : BaseLoanPage
    {
        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowLockDeskRead };
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            int sPricingModeT = RequestHelper.GetInt("sPricingModeT");

            int historicalLoanProgramTemplate = RequestHelper.GetInt("historicalLoanProgramTemplate", 0);
            int historicalPolicy = RequestHelper.GetInt("historicalPolicy", 0);
            int historicalRateOption = RequestHelper.GetInt("historicalRateOption", 0);

            RegisterJsGlobalVariables("sPricingModeT", sPricingModeT);
            RegisterJsGlobalVariables("historicalLoanProgramTemplate", historicalLoanProgramTemplate);
            RegisterJsGlobalVariables("historicalPolicy", historicalPolicy);
            RegisterJsGlobalVariables("historicalRateOption", historicalRateOption);

            selectoption.Visible = BrokerUser.HasPermission(Permission.CanApplyForIneligibleLoanPrograms);
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new EventHandler(PageInit);
            base.OnInit(e);
        }

        void PageInit(object sender, EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("utilities.js");
            this.RegisterService("main", "/newlos/Lockdesk/InternalPricingResultService.aspx");

            this.DisplayCopyRight = false;
            this.IncludeStyleSheet("~/css/quickpricer.css");
        }
    }
}
