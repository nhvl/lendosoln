﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.Admin;

namespace LendersOfficeApp.newlos.Lockdesk
{
    public partial class CopyFromBrokerInfo : BaseLoanPage
    {
        private static ListItemType[] x_interestingItems = new ListItemType[] { ListItemType.Item, ListItemType.AlternatingItem };

        private List<PricingAdjustment> selectedAdjustments = new List<PricingAdjustment>();

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowLockDeskRead };
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterService("loanedit","/newlos/LockDesk/InvestorRateLockService.aspx");
            DisplayCopyRight = false;
        }

        protected override void  LoadData()
        {
            CPageData data = CPageData.CreateUsingSmartDependency(LoanID, typeof(CopyFromBrokerInfo));
            data.InitLoad();

            //sLpInvestorNm.Text = data.sLpInvestorNm;
            data.LoadBrokerLockData();
            sRLckdDays.Text = data.sRLckdDays_rep;
            sBrokerLockBaseNoteIR.Text = data.sBrokerLockBaseNoteIR_rep;
            sBrokerLockBaseBrokComp1PcPrice.Text = data.sBrokerLockBaseBrokComp1PcPrice;
            sBrokerLockBaseBrokComp1PcFee.Text = data.sBrokerLockBaseBrokComp1PcFee_rep;
            sBrokerLockBaseOptionArmTeaserR.Text = data.sBrokerLockBaseOptionArmTeaserR_rep;
            sLpTemplateNm.Text = data.sLpTemplateNm;
            sBrokerLockRateSheetEffectiveD.Text = data.sBrokerLockRateSheetEffectiveD_rep;
            sInvestorLockLockBuffer.Text = data.sInvestorLockLockBuffer_rep;
            sBrokerLockBaseRAdjMarginR.Text = data.sBrokerLockBaseRAdjMarginR_rep;
            InvestorLockPeriod.Text = (data.sRLckdDays + data.sInvestorLockLockBuffer).ToString();
            var adjustments = data.sBrokerLockAdjustments;
            adjustments.ForEach((adj) => adj.IsHidden = false);
            BrokerAdjustments.DataSource = adjustments;
            BrokerAdjustments.DataBind();

            BrokerAdjustments.Visible = adjustments.Count > 0;

            var convert = data.m_convertLos;

            decimal rate = 0M;
            decimal price = 0M;
            decimal fee = 0M; 
            decimal margin  = 0M;
            decimal trate = 0M;



            foreach (var adjustment in selectedAdjustments)
            {
                rate += convert.ToRate(adjustment.Rate);
                price += convert.ToRate(adjustment.Price);
                fee += convert.ToRate(adjustment.Fee);
                margin += convert.ToRate(adjustment.Margin);
                trate += convert.ToRate(adjustment.TeaserRate);
            }

            sIncludedAdjustmentRate.Text = convert.ToRateString(rate);
            sIncludedAdjustmentPrice.Text = convert.ToRateString(price);
            sIncludedAdjustmentFee.Text = convert.ToRateString(fee);
            sIncludedAdjustmentMargin.Text = convert.ToRateString(margin);
            sIncludedAdjustmentTRate.Text = convert.ToRateString(trate);

            rate += convert.ToRate(data.sBrokerLockBaseNoteIR_rep);
            price += convert.ToRate(data.sBrokerLockBaseBrokComp1PcPrice);
            fee += convert.ToRate(data.sBrokerLockBaseBrokComp1PcFee_rep);
            margin += convert.ToRate(data.sBrokerLockBaseRAdjMarginR_rep);
            trate += convert.ToRate(data.sBrokerLockBaseOptionArmTeaserR_rep);

            sInvestorTotalRate.Text = convert.ToRateString(rate);
            sInvestorTotalPrice.Text = convert.ToRateString(price);
            sInvestorTotalFee.Text = convert.ToRateString(fee);
            sInvestorTotalMargin.Text = convert.ToRateString(margin);
            sInvestorTotalTRate.Text = convert.ToRateString(trate);

            ClientScript.RegisterHiddenField("loanid", LoanID.ToString());
            RegisterJsObject("Adjustments", adjustments);

            if (BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) == false)
            {
                //InvestorSection.Visible = false;
                RateSheetEffTimeSection.Visible = false;
                LockBufferSection.Visible = false;
            }
        }


        protected void BrokerAdjustments_DataBind(object ender, RepeaterItemEventArgs args)
        {
            if (!x_interestingItems.Contains(args.Item.ItemType))
            {
                return;
            }

            CheckBox cb = args.Item.FindControl("AdjustmentCB") as CheckBox;
            PricingAdjustment adjustment = args.Item.DataItem as PricingAdjustment;

            cb.Checked = !adjustment.IsLenderAdjustment;
            cb.Attributes.Add("onclick", "refreshPricing()");
            if (cb.Checked)
            {
                selectedAdjustments.Add(adjustment);
            }
        }
    }
}
