﻿namespace LendersOfficeApp.newlos.Lockdesk
{
    using System;
    using LendersOffice.Common;

    public partial class CertificateMenu : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterJsScript("LQBPrintFix.js");
        }
    }
}
