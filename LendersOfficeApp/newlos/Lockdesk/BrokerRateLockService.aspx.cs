﻿namespace LendersOfficeApp.newlos.Lockdesk
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using DataAccess.LoanComparison;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Events;
    using LendersOffice.ObjLib.LockPolicies;
    using LendersOffice.Security;
    using LendersOfficeApp.newlos.Underwriting;

    /// <summary>
    /// READ ME: 
    /// Any changes you make here that are not solely for the new broker rate lock page must be done also on RateLockService in underwriting folder. 
    /// I copied pasted. 
    /// </summary>
    public partial class BrokerRateLockService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override bool PerformResultOptimization
        {
            get
            {
                return false;
            }
        }
 
        protected BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private CPageData CreatePageData(Guid loanID)
        {
            return CPageData.CreateUsingSmartDependency(loanID, typeof(BrokerRateLockService));
        }

        protected override void Process(string methodName)
        {
            if (!BrokerUser.HasPermission(Permission.AllowLockDeskRead) || !BrokerUser.HasPermission(Permission.AllowLockDeskWrite))
            {
                throw new CBaseException(ErrorMessages.Generic, "BrokerRateLock Service Requires Both Read and Write on LockDesk");
            }
            switch (methodName)
            {
                case "LockRate":
                    LockRate();
                    break;
                case "BreakRateLock":
                    BreakRateLock();
                    break;
                case "PrepareLoanForResubmission":
                    PrepareLoanForResubmission();
                    break;
                case "ExtendRateLock":
                    ExtendRateLock();
                    break;
                case "RemoveRequestedRate":
                    RemoveRequestedRate();
                    break;
                case "CalculateData":
                    CalculateData();
                    break;
                case "SaveData":
                    Save();
                    break;
                case "ExtendRateLockPriceUpdate":
                    ExtendRateLockPriceUpdate();
                    break;
                case "PriceFeeConversion":
                    PriceFeeConversion();
                    break;
                case "CalculateExpiration":
                    CalculateExpiration();
                    break;
                case "ExtendRateLockCalculateExpiration":
                    ExtendRateLockCalculateExpiration();
                    break;
                case "SetLockDesk":
                    SetLockDesk();
                    break;
                case "SuspendLock":
                    SuspendLock();
                    break;
                case "ResumeLock":
                    ResumeLock();
                    break;
                case "ClearAutoPriceProcessReport":
                    ClearAutoPriceProcessReport();
                    break;

            }
        }

        private void SetLockDesk()
        {
            CPageData dataLoan = new CEmployeeData(GetGuid("loanid"));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            //OPM 7762 - If no lock desk is currently assigned to the loan file then assign the default lock desk.
            if (dataLoan.sEmployeeLockDeskId == Guid.Empty)
            {
                if (dataLoan.BrokerDB.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
                {
                    if (dataLoan.sLockPolicy.DefaultLockDeskIDOrInheritedID != Guid.Empty)
                    {
                        dataLoan.sEmployeeLockDeskId = dataLoan.sLockPolicy.DefaultLockDeskIDOrInheritedID;
                        dataLoan.Save();
                    }
                }
                else
                {
                    var defaultLockPolicy = LockPolicy.Retrieve(dataLoan.sBrokerId, dataLoan.BrokerDB.DefaultLockPolicyID.Value);
                    if (defaultLockPolicy.DefaultLockDeskIDOrInheritedID != Guid.Empty)
                    {
                        dataLoan.sEmployeeLockDeskId = defaultLockPolicy.DefaultLockDeskID;
                        dataLoan.Save();
                    }
                }
            }
        }
        private void CalculateExpiration()
        {
            string date = GetString("sRLckdD");
            string days = GetString("sRLckdDays");
            Guid loanID = GetGuid("loanid");
            CPageData dataLoan = CreatePageData(loanID);
            dataLoan.InitLoad();
            string expirationDate = dataLoan.CalculateRateLockExpirationDate_rep(date, days);
            SetResult("sRLckdExpiredD", expirationDate);
        }

        private void ExtendRateLockCalculateExpiration()
        {
            string date = GetString("sRLckdD");
            string days = GetString("sRLckdDays");
            Guid loanID = GetGuid("loanid");
            CPageData dataLoan = CreatePageData(loanID);
            dataLoan.InitLoad();

            CDateTime lockedDays = CDateTime.Create(date, dataLoan.m_convertLos);
            CDateTime expirationDate = dataLoan.CalculateRateLockExtensionExpirationDate(lockedDays, int.Parse(days));

            int extendDays = expirationDate.DateTimeForComputation.Subtract(lockedDays.DateTimeForComputation).Days;

            SetResult("sRLckdExpiredD", dataLoan.m_convertLos.ToDateTimeString(expirationDate).ToString());
            SetResult("Extend_sTrueRLckedDays", extendDays);
        }

        private void ExtendRateLockPriceUpdate()
        {
            Guid loanID = GetGuid("loanid");
            
            CPageData pageData = CreatePageData(loanID);
            pageData.InitLoad();

            // Getting current base data
            pageData.LoadBrokerLockData();

            // These can depend on the adjustments. Get their values before we
            // update the adjustments.
            string baseRate = pageData.sBrokerLockBaseNoteIR_rep;
            string baseFee = pageData.sBrokerLockBaseBrokComp1PcFee_rep;
            string baseMargin = pageData.sBrokerLockBaseRAdjMarginR_rep;
            string baseTeaserRate = pageData.sBrokerLockBrokerBaseOptionArmTeaserR_rep;

            var adjustmets = pageData.sBrokerLockAdjustments;

            var newAdjustment = new PricingAdjustment()
            {
                Description = "",

                Fee = GetString("AdjFee"),
                IsHidden = false,
                IsLenderAdjustment = false,
                IsSRPAdjustment = false,
                Margin = GetString("AdjMargin"),
                Rate = GetString("AdjRate"),
                TeaserRate = GetString("AdjTRate"),
                IsPersist = true
            };

            adjustmets.Add(newAdjustment);

            pageData.sBrokerLockAdjustments = adjustmets;

            pageData.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.Base;
            pageData.LoadBrokerLockData(baseRate, baseFee,
                baseMargin, baseTeaserRate);
                
            SetResult("newsNoteIR", pageData.sBrokerLockFinalNoteIR_rep);
            SetResult("newsRAdjMarginR", pageData.sBrokerLockFinalRAdjMarginR_rep);
            SetResult("newsOptionArmTeaserR", pageData.sBrokerLockFinalOptionArmTeaserR_rep);
            SetResult("newsBrokComp1Pc", pageData.sBrokerLockFinalBrokComp1PcFee_rep);
            SetResult("newsBrokerLockFinalBrokComp1PcPrice", pageData.sBrokerLockFinalBrokComp1PcPrice);



        }

        private void RemoveRequestedRate()
        {
            try
            {
                Guid loanID = GetGuid("loanid");

                CPageData dataLoan = CreatePageData(loanID);
                dataLoan.InitSave(GetInt("sFileVersion"));

                AbstractUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
                //opm 19109 fs 06/16/08
                AbstractAuditItem auditItem = new RemoveRequestedRateAuditItem(principal, dataLoan.sNoteIRSubmitted_rep, dataLoan.sLOrigFPcSubmitted_rep, dataLoan.sRAdjMarginRSubmitted_rep, dataLoan.sQualIRSubmitted_rep, dataLoan.sOptionArmTeaserRSubmitted_rep, dataLoan.sIsOptionArmSubmitted, dataLoan.sTermSubmitted_rep, dataLoan.sDueSubmitted_rep, dataLoan.sFinMethTSubmitted.ToString());

                dataLoan.RemoveRequestedRate();
                dataLoan.Save();
                SetResult("sFileVersion", dataLoan.sFileVersion);
                AuditManager.RecordAudit(dataLoan.sLId, auditItem);

                SetResult("sNoteIRSubmitted", dataLoan.sNoteIRSubmitted_rep);
                SetResult("sLOrigFPcSubmitted", dataLoan.sLOrigFPcSubmitted_rep);
                SetResult("sRAdjMarginRSubmitted", dataLoan.sRAdjMarginRSubmitted_rep);
                SetResult("sIsOptionArmSubmitted", dataLoan.sIsOptionArmSubmitted);
                SetResult("sOptionArmTeaserRSubmitted", dataLoan.sOptionArmTeaserRSubmitted_rep);
                SetResult("sRateLockStatusT", dataLoan.sRateLockStatusT_rep);

            }
            catch (CBaseException exc)
            {
                SetResult("HasError", true);
                SetResult("ErrorMessage", exc.UserMessage);
            }
            catch (Exception exc)
            {
                SetResult("HasError", true);
                SetResult("ErrorMessage", exc.Message);

            }
        }

        private void LockRate()
        {
            Guid loanID = GetGuid("loanid");
            string reason = GetString("Reason");
            try
            {
                var rateLockDate = GetString("sRLckdD");

                DateTime testingDateTime;

                if (DateTime.TryParse(rateLockDate, out testingDateTime) == false)
                {
                    var devMsg = string.Format(
                        "Rate lock date '{0}' for loan '{1}' is not valid.",
                        rateLockDate,
                        loanID);

                    // If the user corrects the rate lock date and then attempts
                    // to lock the rate again, the Save() method will expect the 
                    // file version to have been specified, which won't happen if
                    // we throw the exception here. We'll use the current version 
                    // since no changes were made.
                    SetResult("sFileVersion", GetInt("sFileVersion"));

                    throw new CBaseException(ErrorMessages.InvalidRateLockDate, devMsg);
                }

                CPageData dataLoan = CreatePageData(loanID);
                dataLoan.InitSave(GetInt("sFileVersion"));

                string rateLockExpirationDate = GetString("sRLckdExpiredD", string.Empty);
                dataLoan.sNoteIR_rep = GetString("sNoteIR");

                // Update the BrokerRateTable
                bool ByPassFieldSecurityCheckTmp = dataLoan.ByPassFieldSecurityCheck;
                dataLoan.ByPassFieldSecurityCheck = true;
                if (dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                {
                    if (GetBool("sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees"))
                    {
                        dataLoan.sOriginatorCompensationLenderFeeOptionT = E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;
                    }
                    else
                    {
                        dataLoan.sOriginatorCompensationLenderFeeOptionT = E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees;
                    }
                }
                dataLoan.ByPassFieldSecurityCheck = ByPassFieldSecurityCheckTmp;

                string rowLock = GetString("Lock");

                switch (rowLock)
                {
                    case "BasePrice":
                        dataLoan.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.Base;
                        break;
                    case "BrokerBasePrice":
                        dataLoan.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.BrokerBase;
                        break;
                    case "BrokerFinalPrice":
                        dataLoan.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.BrokerFinal;
                        break;
                    case "OriginatorPrice":
                        dataLoan.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.OriginatorPrice;
                        break;
                    default:
                        throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Invalid E_sBrokerLockPriceRowLockedT : " + rowLock);
                }

                dataLoan.LoadBrokerLockData(GetRate("Rate")
                    , GetRate("Fee")
                    , GetRate("Margin")
                    , GetRate("TRate")
                );

                dataLoan.sRAdjMarginR_rep = GetString("sRAdjMarginR");
                dataLoan.sRLckdD_rep = rateLockDate;
                dataLoan.sRLckdDays_rep = GetString("sRLckdDays");

                DuplicateFinder.SetSkipLoanSubmissionCheck(dataLoan.sLId, GetBool("SkipLoanDuplicationCheck", false));
                try
                {
                    dataLoan.LockRate(BrokerUser.DisplayName, reason, false /* bChangeStatus */ );
                }
                catch (DuplicatePMLSubmissionException e)
                {
                    SetResult("HasDuplicateError", true);
                    SetResult("ErrorMessage", e.Message);
                    var js = new System.Web.Script.Serialization.JavaScriptSerializer();
                    SetResult("Duplicates", js.Serialize(e.Duplicates));
                    return;
                }

                dataLoan.Save();

                SetResult("sFileVersion", dataLoan.sFileVersion);
                // 10/7/2004 dd - Rate lock successful. Email to PML Broker indicate this.
                //
                // 05/25/2005 dd - Only send email to loan rep if there is one assign to loan.
                //
                // 8/26/2005 kb - For now, we send to all assigned
                // employees to make sure all are covered.  A new
                // notification subscription framework that's in
                // the works will obsolete this.  This loop will
                // get obsoleted by a revised notification subscription
                // config module (see case 1800).
                string rate;
                string price;
                if (dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees)
                {
                    // When "in addition to lender fees", use these values. OPM 75717
                    rate = dataLoan.sBrokerLockOriginatorPriceNoteIR_rep;
                    price = dataLoan.sBrokerLockOriginatorPriceBrokComp1PcFee_rep;
                }
                else
                {
                    rate = dataLoan.sNoteIR_rep;
                    price = dataLoan.sBrokComp1Pc_rep;
                }

                bool allNotificationsSuccessful = LoanRateLocked.Send(
                    BrokerUser.BrokerId,
                    loanID,
                    rate,
                    price,
                    rateLockExpirationDate,
                    BrokerUser.ApplicationType,
                    excludedRoles: new List<E_RoleT> { E_RoleT.Administrator },
                    continueOnException: true);

                if (!allNotificationsSuccessful)
                {
                    SetResult("WarningMessage", "Failed to send notifications.");
                    SetResult("HasWarning", true);
                }

                SetResult("sIsRateLocked", dataLoan.sIsRateLocked);
                SetResult("sRLckdD", dataLoan.sRLckdD_rep);
                SetResult("sRLckdDTime", dataLoan.sRLckdDTime_rep);
                SetResult("sRLckdDays", dataLoan.sRLckdDays_rep);
                SetResult("sRLckdExpiredD", dataLoan.sRLckdExpiredD_rep);
                SetResult("RateLockHistory", RateLockHistoryTable.BuildBrokerRateLockHistoryTable(dataLoan.sRateLockHistoryXmlContent));
                SetResult("sStatusT", dataLoan.sStatusT_rep);
                SetResult("sRateLockStatusT", dataLoan.sRateLockStatusT_rep);


                List<string> noResubmissionReasons = new List<string>();
                foreach (E_sReasonNotAllowingResubmission o in dataLoan.sReasonForNotAllowingResubmission)
                {
                    noResubmissionReasons.Add(Tools.Get_sReasonNotAllowingResubmission_Action(o));
                }

                SetResult("sReasonForNotAllowingResubmission", LendersOffice.Common.ObsoleteSerializationHelper.JsonSerialize(noResubmissionReasons));

                SetResult("sFECurrentLockExtensionCount", dataLoan.sFECurrentLockExtensionCount);
                SetResult("sFERateRenegotiationCount", dataLoan.sFERateRenegotiationCount);
                SetResult("sFETotalLockExtensionCount", dataLoan.sFETotalLockExtensionCount);
                SetResult("sFETotalRateReLockCount", dataLoan.sFETotalRateReLockCount);
            }

            catch (CBaseException excc)
            {
                Tools.LogError(excc);
                SetResult("HasError", true);
                SetResult("ErrorMessage", excc.UserMessage);
            }

            catch (Exception exc)
            {
                Tools.LogError(exc);
                SetResult("HasError", true);
                SetResult("ErrorMessage", exc.Message);
            }
        }

        private void ResumeLock()
        {
            Guid loanID = GetGuid("loanid");
            string reason = GetString("Reason");
            CPageData dataLoan = CreatePageData(loanID);
            dataLoan.InitSave(GetInt("sFileVersion"));

            string rateLockExpirationDate = GetString("sRLckdExpiredD", string.Empty);
            dataLoan.sNoteIR_rep = GetString("sNoteIR");

            // Update the BrokerRateTable
            bool ByPassFieldSecurityCheckTmp = dataLoan.ByPassFieldSecurityCheck;
            dataLoan.ByPassFieldSecurityCheck = true;
            if (dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
            {
                if (GetBool("sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees"))
                {
                    dataLoan.sOriginatorCompensationLenderFeeOptionT = E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;
                }
                else
                {
                    dataLoan.sOriginatorCompensationLenderFeeOptionT = E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees;
                }
            }
            dataLoan.ByPassFieldSecurityCheck = ByPassFieldSecurityCheckTmp;

            string rowLock = GetString("Lock");

            switch (rowLock)
            {
                case "BasePrice":
                    dataLoan.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.Base;
                    break;
                case "BrokerBasePrice":
                    dataLoan.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.BrokerBase;
                    break;
                case "BrokerFinalPrice":
                    dataLoan.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.BrokerFinal;
                    break;
                case "OriginatorPrice":
                    dataLoan.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.OriginatorPrice;
                    break;
                default:
                    throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Invalid E_sBrokerLockPriceRowLockedT : " + rowLock);
            }

            dataLoan.LoadBrokerLockData(GetRate("Rate")
                , GetRate("Fee")
                , GetRate("Margin")
                , GetRate("TRate")
            );

            dataLoan.sRAdjMarginR_rep = GetString("sRAdjMarginR");
            dataLoan.sRLckdD_rep = GetString("sRLckdD");
            dataLoan.sRLckdDays_rep = GetString("sRLckdDays");

            DuplicateFinder.SetSkipLoanSubmissionCheck(dataLoan.sLId, GetBool("SkipLoanDuplicationCheck", false));
            try
            {
                dataLoan.ResumeLock(BrokerUser.DisplayName, reason);
            }
            catch (DuplicatePMLSubmissionException e)
            {
                SetResult("HasDuplicateError", true);
                SetResult("ErrorMessage", e.Message);
                var js = new System.Web.Script.Serialization.JavaScriptSerializer();
                SetResult("Duplicates", js.Serialize(e.Duplicates));
                return;
            }

            dataLoan.Save();

            SetResult("sFileVersion", dataLoan.sFileVersion);
            // 10/7/2004 dd - Rate lock successful. Email to PML Broker indicate this.
            //
            // 05/25/2005 dd - Only send email to loan rep if there is one assign to loan.
            //
            // 8/26/2005 kb - For now, we send to all assigned
            // employees to make sure all are covered.  A new
            // notification subscription framework that's in
            // the works will obsolete this.  This loop will
            // get obsoleted by a revised notification subscription
            // config module (see case 1800).

            var empList = GetLoanAssigments(loanID, E_RoleT.Administrator);

            if (dataLoan.BrokerDB.IsNotifyUponRateLockModified)
            {
                foreach (Guid sendEmployeeId in empList)
                {
                    try
                    {
                        LoanLockModified loanEvt = new LoanLockModified();
                        string rate;
                        string price;
                        if (dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees)
                        {
                            // When "in addition to lender fees", use these values. OPM 75717
                            rate = dataLoan.sBrokerLockOriginatorPriceNoteIR_rep;
                            price = dataLoan.sBrokerLockOriginatorPriceBrokComp1PcFee_rep;
                        }
                        else
                        {
                            rate = dataLoan.sNoteIR_rep;
                            price = dataLoan.sBrokComp1Pc_rep;
                        }
                        loanEvt.Initialize(rate, price, sendEmployeeId, dataLoan.sLId, rateLockExpirationDate, BrokerUser.ApplicationType);

                        loanEvt.Send();
                    }
                    catch (EventNotifyException e)
                    {
                        SetResult("WarningMessage", "Failed to send notifications.");

                        SetResult("HasWarning", true);

                        Tools.LogError(e);
                    }
                }
            }

            SetResult("sIsRateLocked", dataLoan.sIsRateLocked);
            SetResult("sRLckdD", dataLoan.sRLckdD_rep);
            SetResult("sRLckdDTime", dataLoan.sRLckdDTime_rep);
            SetResult("sRLckdDays", dataLoan.sRLckdDays_rep);
            SetResult("sRLckdExpiredD", dataLoan.sRLckdExpiredD_rep);
            SetResult("RateLockHistory", RateLockHistoryTable.BuildBrokerRateLockHistoryTable(dataLoan.sRateLockHistoryXmlContent));
            SetResult("sStatusT", dataLoan.sStatusT_rep);
            SetResult("sRateLockStatusT", dataLoan.sRateLockStatusT_rep);


            List<string> noResubmissionReasons = new List<string>();
            foreach (E_sReasonNotAllowingResubmission o in dataLoan.sReasonForNotAllowingResubmission)
            {
                noResubmissionReasons.Add(Tools.Get_sReasonNotAllowingResubmission_Action(o));
            }

            SetResult("sReasonForNotAllowingResubmission", LendersOffice.Common.ObsoleteSerializationHelper.JsonSerialize(noResubmissionReasons));

            SetResult("sFECurrentLockExtensionCount", dataLoan.sFECurrentLockExtensionCount);
            SetResult("sFERateRenegotiationCount", dataLoan.sFERateRenegotiationCount);
            SetResult("sFETotalLockExtensionCount", dataLoan.sFETotalLockExtensionCount);
            SetResult("sFETotalRateReLockCount", dataLoan.sFETotalRateReLockCount);
        }

        private void BreakRateLock()
        {
            Guid loanID = GetGuid("loanid");
            string reason = GetString("Reason");

            try
            {
                CPageData dataLoan = CreatePageData(loanID);

                dataLoan.InitSave(GetInt("sFileVersion"));

                dataLoan.BreakRateLock(BrokerUser.DisplayName, reason);

                dataLoan.Save();
                SetResult("sFileVersion", dataLoan.sFileVersion);

                bool allNotificationsSucceeded = RateLockBroken.Send(BrokerUser.BrokerId, loanID, reason, BrokerUser.ApplicationType);
                if (!allNotificationsSucceeded)
                {
                    SetResult("WarningMessage", "Failed to send notifications.");
                    SetResult("HasWarning", true);
                }

                List<string> noResubmissionReasons = new List<string>();
                foreach (E_sReasonNotAllowingResubmission o in dataLoan.sReasonForNotAllowingResubmission)
                {
                    noResubmissionReasons.Add(Tools.Get_sReasonNotAllowingResubmission_Action(o));
                }

                SetResult("sReasonForNotAllowingResubmission", LendersOffice.Common.ObsoleteSerializationHelper.JsonSerialize(noResubmissionReasons));

                SetResult("sIsRateLocked", dataLoan.sIsRateLocked);
                SetResult("sRLckdD", dataLoan.sRLckdD_rep);
                SetResult("sRLckdDTime", dataLoan.sRLckdDTime_rep);
                SetResult("sRLckdExpiredD", dataLoan.sRLckdExpiredD_rep);
                SetResult("RateLockHistory", RateLockHistoryTable.BuildBrokerRateLockHistoryTable(dataLoan.sRateLockHistoryXmlContent));
                SetResult("sStatusT", dataLoan.sStatusT_rep);
                SetResult("sRateLockStatusT", dataLoan.sRateLockStatusT_rep);

                SetResult("sFECurrentLockExtensionCount", dataLoan.sFECurrentLockExtensionCount);
                SetResult("sFERateRenegotiationCount", dataLoan.sFERateRenegotiationCount);
                SetResult("sFETotalLockExtensionCount", dataLoan.sFETotalLockExtensionCount);
                SetResult("sFETotalRateReLockCount", dataLoan.sFETotalRateReLockCount);
            }
            catch (VersionMismatchException )
            {
                throw;
            }
            catch (CBaseException exc)
            {
                SetResult("HasError", true);
                SetResult("ErrorMessage", exc.UserMessage);
            }
            catch (Exception exc)
            {
                SetResult("HasError", true);
                SetResult("ErrorMessage", exc.Message);

            }
        }

        private void SuspendLock()
        {
            Guid loanID = GetGuid("loanid");
            string reason = GetString("Reason");

            try
            {
                CPageData dataLoan = CreatePageData(loanID);

                dataLoan.InitSave(GetInt("sFileVersion"));

                dataLoan.SuspendLock(BrokerUser.DisplayName, reason);

                dataLoan.Save();
                SetResult("sFileVersion", dataLoan.sFileVersion);

                List<string> noResubmissionReasons = new List<string>();
                foreach (E_sReasonNotAllowingResubmission o in dataLoan.sReasonForNotAllowingResubmission)
                {
                    noResubmissionReasons.Add(Tools.Get_sReasonNotAllowingResubmission_Action(o));
                }

                SetResult("sReasonForNotAllowingResubmission", LendersOffice.Common.ObsoleteSerializationHelper.JsonSerialize(noResubmissionReasons));

                SetResult("sIsRateLocked", dataLoan.sIsRateLocked);
                SetResult("sRLckdD", dataLoan.sRLckdD_rep);
                SetResult("sRLckdDTime", dataLoan.sRLckdDTime_rep);
                SetResult("sRLckdExpiredD", dataLoan.sRLckdExpiredD_rep);
                SetResult("RateLockHistory", RateLockHistoryTable.BuildBrokerRateLockHistoryTable(dataLoan.sRateLockHistoryXmlContent));
                SetResult("sStatusT", dataLoan.sStatusT_rep);
                SetResult("sRateLockStatusT", dataLoan.sRateLockStatusT_rep);

                SetResult("sFECurrentLockExtensionCount", dataLoan.sFECurrentLockExtensionCount);
                SetResult("sFERateRenegotiationCount", dataLoan.sFERateRenegotiationCount);
                SetResult("sFETotalLockExtensionCount", dataLoan.sFETotalLockExtensionCount);
                SetResult("sFETotalRateReLockCount", dataLoan.sFETotalRateReLockCount);
            }
            catch (VersionMismatchException)
            {
                throw;
            }
            catch (CBaseException exc)
            {
                SetResult("HasError", true);
                SetResult("ErrorMessage", exc.UserMessage);
            }
        }


        private void ExtendRateLock()
        {
            Guid loanID = GetGuid("sLId");
            string reason = GetString("Reason");
            int days = GetInt("sRLckdDays");
            int calendarDays = GetInt("calendarDays");

            if (calendarDays == 0)
            {
                throw new AutoRateLock.NonCriticalAutoRateLockException("Calendar Extension Days cannot be 0");
            }

            if (reason == string.Empty)
            {
                throw new AutoRateLock.NonCriticalAutoRateLockException("Reason cannot be blank");
            }

            bool addAdjustment = GetBool("IsAdjustPrice");
            PricingAdjustment newAdjustment = null;

            if (addAdjustment)
            {
                newAdjustment = new PricingAdjustment()
                {
                    Description = GetString("AdjustmentDesc"),
                    Fee = GetString("AdjustmentFee"),
                    IsHidden = false,
                    IsLenderAdjustment = true,
                    IsSRPAdjustment = false,
                    Margin = GetString("AdjustmentMargin"),
                    Rate = GetString("AdjustmentRate"),
                    TeaserRate = GetString("AdjustmentTRate"),
                    IsPersist = true
                };
            }

            try
            {
                CPageData pageData = CreatePageData(loanID);

                pageData.InitSave(GetInt("sFileVersion"));
                pageData.LoadBrokerLockData();
                pageData.ExtendRateLock(BrokerUser.DisplayName, reason, days, newAdjustment);
                pageData.Save();
                SetResult("sFileVersion", pageData.sFileVersion);
                SetResult("sRLckdExpiredD", pageData.sRLckdExpiredD_rep);
                SetResult("sRLckdDays", pageData.sRLckdDays_rep);

                SetResult("sBrokerLockBaseNoteIR", pageData.sBrokerLockBaseNoteIR_rep);
                SetResult("sBrokerLockBaseBrokComp1PcPrice", pageData.sBrokerLockBaseBrokComp1PcPrice);
                SetResult("sBrokerLockBaseBrokComp1PcFee", pageData.sBrokerLockBaseBrokComp1PcFee_rep);
                SetResult("sBrokerLockBaseRAdjMarginR", pageData.sBrokerLockBaseRAdjMarginR_rep);
                SetResult("sBrokerLockBaseOptionArmTeaserR", pageData.sBrokerLockBaseOptionArmTeaserR_rep);

                SetResult("sBrokerLockBrokerBaseNoteIR", pageData.sBrokerLockBrokerBaseNoteIR_rep);
                SetResult("sBrokerLockBrokerBaseBrokComp1PcPrice", pageData.sBrokerLockBrokerBaseBrokComp1PcPrice);
                SetResult("sBrokerLockBrokerBaseBrokComp1PcFee", pageData.sBrokerLockBrokerBaseBrokComp1PcFee_rep);
                SetResult("sBrokerLockBrokerBaseRAdjMarginR", pageData.sBrokerLockBrokerBaseRAdjMarginR_rep);
                SetResult("sBrokerLockBrokerBaseOptionArmTeaserR", pageData.sBrokerLockBrokerBaseOptionArmTeaserR_rep);

                SetResult("sNoteIR", pageData.sNoteIR_rep);
                SetResult("sRAdjMarginR", pageData.sRAdjMarginR_rep);
                SetResult("sOptionArmTeaserR", !pageData.sIsOptionArm ? "0.000%" : pageData.sOptionArmTeaserR_rep);
                SetResult("sBrokComp1Pc", pageData.sBrokComp1Pc_rep);
                SetResult("sBrokerLockFinalBrokComp1PcPrice", pageData.sBrokerLockFinalBrokComp1PcPrice);

                SetResult("sBrokerLockTotHiddenAdjNoteIR", pageData.sBrokerLockTotHiddenAdjNoteIR_rep);
                SetResult("sBrokerLockTotHiddenAdjBrokComp1PcPrice", pageData.sBrokerLockTotHiddenAdjBrokComp1PcPrice);
                SetResult("sBrokerLockTotHiddenAdjBrokComp1PcFee", pageData.sBrokerLockTotHiddenAdjBrokComp1PcFee_rep);
                SetResult("sBrokerLockTotHiddenAdjRAdjMarginR", pageData.sBrokerLockTotHiddenAdjRAdjMarginR_rep);
                SetResult("sBrokerLockTotHiddenAdjOptionArmTeaserR", pageData.sBrokerLockTotHiddenAdjOptionArmTeaserR_rep);

                SetResult("sBrokerLockTotVisibleAdjNoteIR", pageData.sBrokerLockTotVisibleAdjNoteIR_rep);
                SetResult("sBrokerLockTotVisibleAdjBrokComp1PcPrice", pageData.sBrokerLockTotVisibleAdjBrokComp1PcPrice);
                SetResult("sBrokerLockTotVisibleAdjBrokComp1PcFee", pageData.sBrokerLockTotVisibleAdjBrokComp1PcFee_rep);
                SetResult("sBrokerLockTotVisibleAdjRAdjMarginR", pageData.sBrokerLockTotVisibleAdjRAdjMarginR_rep);
                SetResult("sBrokerLockTotVisibleAdjOptionArmTeaserR", pageData.sBrokerLockTotVisibleAdjOptionArmTeaserR_rep);
                SetResult("RateLockHistory", RateLockHistoryTable.BuildBrokerRateLockHistoryTable(pageData.sRateLockHistoryXmlContent));

                SetResult("sBrokerLockOriginatorPriceNoteIR", pageData.sBrokerLockOriginatorPriceNoteIR_rep);
                SetResult("sBrokerLockOriginatorPriceBrokComp1PcFee", pageData.sBrokerLockOriginatorPriceBrokComp1PcFee_rep);
                SetResult("sBrokerLockOriginatorPriceBrokComp1PcPrice", pageData.sBrokerLockOriginatorPriceBrokComp1PcPrice);
                SetResult("sBrokerLockOriginatorPriceRAdjMarginR", pageData.sBrokerLockOriginatorPriceRAdjMarginR_rep);
                SetResult("sBrokerLockOriginatorPriceOptionArmTeaserR", pageData.sBrokerLockOriginatorPriceOptionArmTeaserR_rep);

                SetResult("sBrokerLockOriginatorCompAdjNoteIR", pageData.sBrokerLockOriginatorCompAdjNoteIR_rep);
                SetResult("sBrokerLockOriginatorCompAdjBrokComp1PcFee", pageData.sBrokerLockOriginatorCompAdjBrokComp1PcFee_rep);
                SetResult("sBrokerLockOriginatorCompAdjBrokComp1PcPrice", pageData.sBrokerLockOriginatorCompAdjBrokComp1PcPrice);
                SetResult("sBrokerLockOriginatorCompAdjRAdjMarginR", pageData.sBrokerLockOriginatorCompAdjRAdjMarginR_rep);
                SetResult("sBrokerLockOriginatorCompAdjOptionArmTeaserR", pageData.sBrokerLockOriginatorCompAdjOptionArmTeaserR_rep);

                SetResult("sFECurrentLockExtensionCount", pageData.sFECurrentLockExtensionCount);
                SetResult("sFERateRenegotiationCount", pageData.sFERateRenegotiationCount);
                SetResult("sFETotalLockExtensionCount", pageData.sFETotalLockExtensionCount);
                SetResult("sFETotalRateReLockCount", pageData.sFETotalRateReLockCount);

            }
            catch (VersionMismatchException)
            {
                throw;
            }
            catch (CBaseException exc)
            {
                SetResult("HasError", true);
                SetResult("ErrorMessage", exc.UserMessage);
                Tools.LogError(exc);
            }
            catch (Exception exc)
            {
                SetResult("HasError", true);
                SetResult("ErrorMessage", exc.Message);
                Tools.LogError(exc);
            }
        }

        private void PrepareLoanForResubmission()
        {
            try
            {
                Guid loanID = GetGuid("loanid");
                Tools.PrepareLoanForResubmission(loanID);

                CPageData dataLoan = CreatePageData(loanID);
                dataLoan.InitLoad();

                AbstractUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
                //opm 42830 db 11/24/09, opm 19109 fs 06/16/08
                AbstractAuditItem auditItem = new RemoveRequestedRateAuditItem(principal, dataLoan.sNoteIRSubmitted_rep, dataLoan.sLOrigFPcSubmitted_rep, dataLoan.sRAdjMarginRSubmitted_rep, dataLoan.sQualIRSubmitted_rep, dataLoan.sOptionArmTeaserRSubmitted_rep, dataLoan.sIsOptionArmSubmitted, dataLoan.sTermSubmitted_rep, dataLoan.sDueSubmitted_rep, dataLoan.sFinMethTSubmitted.ToString());
                AuditManager.RecordAudit(dataLoan.sLId, auditItem);
            }
            catch (CBaseException exc)
            {
                SetResult("HasError", true);
                SetResult("ErrorMessage", exc.UserMessage);
            }
            catch (Exception exc)
            {
                SetResult("HasError", true);
                SetResult("ErrorMessage", exc.Message);

            }
        }

        private IList<Guid> GetLoanAssigments(Guid loanId, E_RoleT fileOutRole)
        {
            // 8/26/2005 kb - For now, we send to all assigned
            // employees to make sure all are covered.  A new
            // notification subscription framework that's in
            // the works will obsolete this.  This loop will
            // get obsoleted by a revised notification subscription
            // config module (see case 1800).

            LoanAssignmentContactTable loanAssignmentTable = new LoanAssignmentContactTable(BrokerUser.BrokerId, loanId);
            List<Guid> empList = new List<Guid>();

            foreach (var assignedEmployee in loanAssignmentTable.Items)
            {
                if (assignedEmployee.RoleT != fileOutRole)
                {
                    if (empList.Contains(assignedEmployee.EmployeeId) == false)
                    {
                        empList.Add(assignedEmployee.EmployeeId);
                    }
                }
            }

            return empList;
        }

        private void Save()
        {
            Guid loanID = GetGuid("LoanID");


            string serializedAdjustments = GetString("PricingAdjustments");
            List<PricingAdjustment> adjustments = LendersOffice.Common.ObsoleteSerializationHelper.JsonDeserialize<List<PricingAdjustment>>(serializedAdjustments);

            CPageData pageData = CreatePageData(loanID);
            pageData.InitSave(GetInt("sFileVersion"));
            pageData.sLpTemplateNm = GetString("sLpTemplateNm");
            pageData.sRLckdN = GetString("sRLckdN");
            pageData.sRLckdExpiredN = GetString("sRLckdExpiredN");
            pageData.sSubmitD_rep = GetString("sSubmitD");
            pageData.sSubmitN = GetString("sSubmitN");
            pageData.sTrNotes = GetString("sTrNotes");
            pageData.sBrokerLockAdjustments = adjustments;
            pageData.sRLckdDays_rep = GetString("sRLckdDays");

            pageData.sFECurrentLockExtensionCount = GetInt("sFECurrentLockExtensionCount");
            pageData.sFECurrentLockExtensionCountLckd = GetBool("sFECurrentLockExtensionCountLckd");

            pageData.sFERateRenegotiationCount = GetInt("sFERateRenegotiationCount");
            pageData.sFERateRenegotiationCountLckd = GetBool("sFERateRenegotiationCountLckd");

            pageData.sFETotalLockExtensionCount = GetInt("sFETotalLockExtensionCount");
            pageData.sFETotalLockExtensionCountLckd = GetBool("sFETotalLockExtensionCountLckd");

            pageData.sFETotalRateReLockCount = GetInt("sFETotalRateReLockCount");
            pageData.sFETotalRateReLockCountLckd = GetBool("sFETotalRateReLockCountLckd");

            bool ByPassFieldSecurityCheckTmp = pageData.ByPassFieldSecurityCheck;
            pageData.ByPassFieldSecurityCheck = true;
            if (pageData.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
            {
                if (GetBool("sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees"))
                {
                    pageData.sOriginatorCompensationLenderFeeOptionT = E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;
                }
                else
                {
                    pageData.sOriginatorCompensationLenderFeeOptionT = E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees;
                }
            }
            pageData.ByPassFieldSecurityCheck = ByPassFieldSecurityCheckTmp;

            string rowLock = GetString("Lock");


            switch (rowLock)
            {
                case "BasePrice":
                    pageData.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.Base;
                    break;
                case "BrokerBasePrice":
                    pageData.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.BrokerBase;
                    break;
                case "BrokerFinalPrice":
                    pageData.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.BrokerFinal;
                    break;
                case "OriginatorPrice":
                    pageData.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.OriginatorPrice;
                    break;
                default:
                    throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Invalid E_sBrokerLockPriceRowLockedT : " + rowLock);
            }

            pageData.LoadBrokerLockData(GetRate("Rate")
                , GetRate("Fee")
                , GetRate("Margin")
                , GetRate("TRate")
            );


            pageData.Save();
            SetResult("sFileVersion", pageData.sFileVersion);
        }

        private void CalculateData()
        {
            Guid loanID = GetGuid("LoanID");
            string rowLock = GetString("Lock");

            string serializedAdjustments = GetString("PricingAdjustments");
            List<PricingAdjustment> adjustments = LendersOffice.Common.ObsoleteSerializationHelper.JsonDeserialize<List<PricingAdjustment>>(serializedAdjustments);

            CPageData pageData = CreatePageData(loanID);
            pageData.InitLoad();


            switch (rowLock)
            {
                case "BasePrice":
                    pageData.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.Base;
                    break;
                case "BrokerBasePrice":
                    pageData.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.BrokerBase;
                    break;
                case "BrokerFinalPrice":
                    pageData.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.BrokerFinal;
                    break;
                case "OriginatorPrice":
                    pageData.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.OriginatorPrice;
                    break;
                default:
                    throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Invalid E_sBrokerLockPriceRowLockedT : " + rowLock);
            }

            pageData.sBrokerLockAdjustments = adjustments;

            var sInAdditionToLenderFees = GetString("sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees", "");
            if (false == string.IsNullOrEmpty(sInAdditionToLenderFees))
            {
                bool ByPassFieldSecurityCheckTmp = pageData.ByPassFieldSecurityCheck;
                pageData.ByPassFieldSecurityCheck = true;
                pageData.sOriginatorCompensationLenderFeeOptionT = GetBool("sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees") ? E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees : E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees;
                pageData.ByPassFieldSecurityCheck = ByPassFieldSecurityCheckTmp;
            }
            pageData.LoadBrokerLockData(
                 GetRate("Rate")
                , GetRate("Fee")
                , GetRate("Margin")
                , GetRate("TRate")
                );

            pageData.sFECurrentLockExtensionCountLckd = GetBool("sFECurrentLockExtensionCountLckd");
            pageData.sFERateRenegotiationCountLckd = GetBool("sFERateRenegotiationCountLckd");
            pageData.sFETotalLockExtensionCountLckd = GetBool("sFETotalLockExtensionCountLckd");
            pageData.sFETotalRateReLockCountLckd = GetBool("sFETotalRateReLockCountLckd");

            pageData.sFECurrentLockExtensionCount = GetInt("sFECurrentLockExtensionCount");
            pageData.sFERateRenegotiationCount = GetInt("sFERateRenegotiationCount");
            pageData.sFETotalLockExtensionCount = GetInt("sFETotalLockExtensionCount");
            pageData.sFETotalRateReLockCount = GetInt("sFETotalRateReLockCount");

            SetResult("sFECurrentLockExtensionCount", pageData.sFECurrentLockExtensionCount);
            SetResult("sFECurrentLockExtensionCountLckd", pageData.sFECurrentLockExtensionCountLckd);

            SetResult("sFERateRenegotiationCount", pageData.sFERateRenegotiationCount);
            SetResult("sFERateRenegotiationCountLckd", pageData.sFERateRenegotiationCountLckd);

            SetResult("sFETotalLockExtensionCount", pageData.sFETotalLockExtensionCount);
            SetResult("sFETotalLockExtensionCountLckd", pageData.sFETotalLockExtensionCountLckd);

            SetResult("sFETotalRateReLockCount", pageData.sFETotalRateReLockCount);
            SetResult("sFETotalRateReLockCountLckd", pageData.sFETotalRateReLockCountLckd);

            SetResult("sBrokerLockBaseNoteIR", pageData.sBrokerLockBaseNoteIR_rep);
            SetResult("sBrokerLockBaseBrokComp1PcPrice", pageData.sBrokerLockBaseBrokComp1PcPrice);
            SetResult("sBrokerLockBaseBrokComp1PcFee", pageData.sBrokerLockBaseBrokComp1PcFee_rep);
            SetResult("sBrokerLockBaseRAdjMarginR", pageData.sBrokerLockBaseRAdjMarginR_rep);
            SetResult("sBrokerLockBaseOptionArmTeaserR", pageData.sBrokerLockBaseOptionArmTeaserR_rep);

            SetResult("sBrokerLockBrokerBaseNoteIR", pageData.sBrokerLockBrokerBaseNoteIR_rep);
            SetResult("sBrokerLockBrokerBaseBrokComp1PcPrice", pageData.sBrokerLockBrokerBaseBrokComp1PcPrice);
            SetResult("sBrokerLockBrokerBaseBrokComp1PcFee", pageData.sBrokerLockBrokerBaseBrokComp1PcFee_rep);
            SetResult("sBrokerLockBrokerBaseRAdjMarginR", pageData.sBrokerLockBrokerBaseRAdjMarginR_rep);
            SetResult("sBrokerLockBrokerBaseOptionArmTeaserR", pageData.sBrokerLockBrokerBaseOptionArmTeaserR_rep);

            SetResult("sNoteIR", pageData.sNoteIR_rep);
            SetResult("sRAdjMarginR", pageData.sRAdjMarginR_rep);
            SetResult("sOptionArmTeaserR", !pageData.sIsOptionArm ? "0.000%" : pageData.sOptionArmTeaserR_rep);
            SetResult("sBrokComp1Pc", pageData.sBrokComp1Pc_rep);

            SetResult("sBrokerLockTotHiddenAdjNoteIR", pageData.sBrokerLockTotHiddenAdjNoteIR_rep);
            SetResult("sBrokerLockTotHiddenAdjBrokComp1PcPrice", pageData.sBrokerLockTotHiddenAdjBrokComp1PcPrice);
            SetResult("sBrokerLockTotHiddenAdjBrokComp1PcFee", pageData.sBrokerLockTotHiddenAdjBrokComp1PcFee_rep);
            SetResult("sBrokerLockTotHiddenAdjRAdjMarginR", pageData.sBrokerLockTotHiddenAdjRAdjMarginR_rep);
            SetResult("sBrokerLockTotHiddenAdjOptionArmTeaserR", pageData.sBrokerLockTotHiddenAdjOptionArmTeaserR_rep);
            SetResult("sBrokerLockFinalBrokComp1PcPrice", pageData.sBrokerLockFinalBrokComp1PcPrice_rep);

            SetResult("sBrokerLockTotVisibleAdjNoteIR", pageData.sBrokerLockTotVisibleAdjNoteIR_rep);
            SetResult("sBrokerLockTotVisibleAdjBrokComp1PcPrice", pageData.sBrokerLockTotVisibleAdjBrokComp1PcPrice);
            SetResult("sBrokerLockTotVisibleAdjBrokComp1PcFee", pageData.sBrokerLockTotVisibleAdjBrokComp1PcFee_rep);
            SetResult("sBrokerLockTotVisibleAdjRAdjMarginR", pageData.sBrokerLockTotVisibleAdjRAdjMarginR_rep);
            SetResult("sBrokerLockTotVisibleAdjOptionArmTeaserR", pageData.sBrokerLockTotVisibleAdjOptionArmTeaserR_rep);


            SetResult("sBrokerLockOriginatorPriceNoteIR", pageData.sBrokerLockOriginatorPriceNoteIR_rep);
            SetResult("sBrokerLockOriginatorPriceBrokComp1PcFee", pageData.sBrokerLockOriginatorPriceBrokComp1PcFee_rep);
            SetResult("sBrokerLockOriginatorPriceBrokComp1PcPrice", pageData.sBrokerLockOriginatorPriceBrokComp1PcPrice);
            SetResult("sBrokerLockOriginatorPriceRAdjMarginR", pageData.sBrokerLockOriginatorPriceRAdjMarginR_rep);
            SetResult("sBrokerLockOriginatorPriceOptionArmTeaserR", pageData.sBrokerLockOriginatorPriceOptionArmTeaserR_rep);

            SetResult("sBrokerLockOriginatorCompAdjNoteIR", pageData.sBrokerLockOriginatorCompAdjNoteIR_rep);
            SetResult("sBrokerLockOriginatorCompAdjBrokComp1PcFee", pageData.sBrokerLockOriginatorCompAdjBrokComp1PcFee_rep);
            SetResult("sBrokerLockOriginatorCompAdjBrokComp1PcPrice", pageData.sBrokerLockOriginatorCompAdjBrokComp1PcPrice);
            SetResult("sBrokerLockOriginatorCompAdjRAdjMarginR", pageData.sBrokerLockOriginatorCompAdjRAdjMarginR_rep);
            SetResult("sBrokerLockOriginatorCompAdjOptionArmTeaserR", pageData.sBrokerLockOriginatorCompAdjOptionArmTeaserR_rep);

        }

        private void PriceFeeConversion()
        {
            string conversion = GetString("conversion");
            if (conversion == "Adjustment")
            {
                ConvertPriceOrFeeForAdjustment();
            }
            else if (conversion == "Pricing")
            {
                ConvertPriceOrFeeForPricing();
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Dev Bug - Missing Price fee conversion variable");
            }
        }
        private void ConvertPriceOrFeeForPricing()
        {
            try
            {
                decimal value = GetRate("Value");
                decimal otherValue = 100 - value;
                SetResult("result", otherValue.ToString("0.000\\%"));
                SetResult("value", value.ToString("0.000\\%"));
            }
            catch (GenericUserErrorMessageException)
            {
                SetResult("result", "0.000%");
                SetResult("value", "0.000%");
            }
        }
        private void ConvertPriceOrFeeForAdjustment()
        {
            try
            {
                decimal value = GetRate("value");
                decimal otherValue = -1 * value;
                SetResult("result", otherValue.ToString("0.000\\%"));
                SetResult("value", value.ToString("0.000\\%"));
            }
            catch (GenericUserErrorMessageException)
            {
                SetResult("result", "0.000%");
                SetResult("value", "0.000%");

            }
        }

        private void ClearAutoPriceProcessReport()
        {
            // OPM 247399.

            var loanId = GetGuid("loanid");
            CPageData dataLoan = CreatePageData(loanId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sAutoPriceProcessResultT = E_AutoPriceProcessResultT.ReportCleared;
            dataLoan.sAutoPriceProcessResultD = CDateTime.Create(DateTime.Now);
            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            SetResult("sAutoPriceProcessResult", dataLoan.sAutoPriceProcessResultT_rep);
            SetResult("sAutoPriceProcessResultD", dataLoan.sAutoPriceProcessResultD_rep);
        }
    }
}
