﻿namespace LendersOfficeApp.newlos.Lockdesk
{
    using System;
    using System.Web;
    using LendersOffice.Common;
    using DataAccess;

    public partial class InternalPricingSelectionOption : BaseLoanPage
    {
        protected string m_frmBodyUrl = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            
            this.DisplayCopyRight = false;
            Guid sLId = RequestHelper.LoanID;
            string lienQualifyModeT = RequestHelper.GetSafeQueryString("lienqualifymodet");
            string productId = RequestHelper.GetSafeQueryString("productid");
            string version = RequestHelper.GetSafeQueryString("version");
            E_sPricingModeT sPricingModeT = (E_sPricingModeT) RequestHelper.GetInt("sPricingModeT", 0);
            string selectRate = RequestHelper.GetSafeQueryString("rate");
            string selectPoint = RequestHelper.GetSafeQueryString("fee");
            bool isHistorical = RequestHelper.GetBool("isHistorical");
            string snapshotJson = HttpUtility.HtmlDecode( RequestHelper.GetSafeQueryString("snapshotJson"));
            string pricingType = string.IsNullOrEmpty(snapshotJson) ? "" : isHistorical ? "historical " : "current ";

            int historicalPolicy = RequestHelper.GetInt("historicalPolicy", 0);
            int historicalRateOption = RequestHelper.GetInt("historicalRateOption", 0);

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(InternalPricingSelectionOption));
            dataLoan.InitLoad();
            switch (sPricingModeT)
            {
                case E_sPricingModeT.EligibilityBrokerPricing:
                case E_sPricingModeT.EligibilityInvestorPricing:
                    m_cbSetInvestorRateLock.Visible = false;
                    break;
                case E_sPricingModeT.InternalBrokerPricing:
                    m_cbSetInvestorRateLock.Text = "Set Front-end rate lock with " + pricingType + "pricing";
                    m_cbSetInvestorRateLock.Enabled = dataLoan.sRateLockStatusT != E_sRateLockStatusT.Locked;
                    break;
                case E_sPricingModeT.InternalInvestorPricing:
                    m_cbSetInvestorRateLock.Text = "Set investor rate lock with " + pricingType + "pricing";
                    m_cbSetInvestorRateLock.Enabled = dataLoan.sInvestorLockRateLockStatusT != E_sInvestorLockRateLockStatusT.Locked;
                    break;
                default:
                    throw new UnhandledEnumException(sPricingModeT);
            }

            string requestString = string.Format("loanid={0}&productid={1}&lienqualifymodet={2}&version={3}&spricingmodet={4}&rate={5}&fee={6}&historicalPolicy={7}&historicalRateOption={8}&isHistorical={9}&snapshotJson={10}",
                sLId, productId, lienQualifyModeT, version, sPricingModeT.ToString("D"), selectRate, selectPoint, historicalPolicy, historicalRateOption, isHistorical.ToString().ToLower(), snapshotJson);
            m_frmBodyUrl = "CertificateWait.aspx?" + requestString;


        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.RegisterService("loanedit", "/newlos/LockDesk/InternalPricingSelectionOptionService.aspx");
        }
    }
}
