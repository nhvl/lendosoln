﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.Security;
using LendersOffice.RatePrice;
using LendersOfficeApp.los.admin;
using LendersOffice.Constants;
using LendersOffice.ObjLib.HistoricalPricing;

namespace LendersOfficeApp.newlos.Lockdesk
{
    public partial class InternalPricingSelectionOptionService : BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName) 
            {
                case "PerformAction":
                    PerformAction();
                    break;
            }
        }
        private void PerformAction()
        {
            Guid sLId = GetGuid("loanid");
            Guid productId = GetGuid("ProductId");
            E_sPricingModeT sPricingModeT = (E_sPricingModeT)GetInt("sPricingModeT");
            decimal selectRate = GetRate("Rate");
            decimal selectPoint = GetRate("Fee");
            AbstractUserPrincipal principal = (AbstractUserPrincipal)this.User;
            bool isSetRateLockPricingOption = GetBool("m_cbSetInvestorRateLock", false);
            bool isAddConditionOption = GetBool("m_cbAddProgramConditions", false);
            bool isHistorical = GetBool("isHistorical");
            string snapshotJson = GetString("snapshotJson");
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            HybridLoanProgramSetOption historicalOption = options;

            if(isHistorical)
            {
                var snapshot = SerializationHelper.JsonNetDeserialize<SubmissionSnapshot>(snapshotJson);
                var lenderSettings = principal.BrokerDB.HistoricalPricingSettings;
                historicalOption = HybridLoanProgramSetOption.CreateHistoricalPricingOption(lenderSettings, snapshot);
            }

            DistributeUnderwritingEngine.PerformInternalPricingAction(principal, sLId, productId, 
                sPricingModeT, selectRate, selectPoint, isSetRateLockPricingOption, isAddConditionOption,
                historicalOption);

            // This needs to be done, and doesn't come from UI data, so might as well give it full access.
            var dataLoan = new CFullAccessPageData(sLId, new string[] { "sfUpdate_sProMInsFieldsPer110559" });
            dataLoan.ByPassFieldSecurityCheck = true;  // might as well make it faster.
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.Update_sProMInsFieldsPer110559();
            if (dataLoan.sLT == E_sLT.FHA)
            {
                dataLoan.Save();
            }

        }
    }
}
