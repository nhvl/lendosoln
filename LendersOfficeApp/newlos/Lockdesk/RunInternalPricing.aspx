﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="RunInternalPricing.aspx.cs" Inherits="LendersOfficeApp.newlos.Lockdesk.RunInternalPricing" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %> 
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <link type="text/css" href="~/css/quickpricer.css" rel="Stylesheet" runat="server"/>
</head>
<body>
    <form id="form1" runat="server">
        <script type="text/javascript">
            <!--
            function switchApplicant() {
                // No-op. There are no borrower-specific fields on this page.
            }
            function sProdFilterMatchCurrentTerm_onclick()
            {
              var cb = <%= AspxTools.JsGetElementById(sProdFilterMatchCurrentTerm) %>;
              
              var bVisible = cb.checked == false;
              
              var list = ['termFilterPanel', 'amortFilterPanel'];
                          
              for (var i = 0; i < list.length; i++)
              {
                document.getElementById(list[i]).style.visibility = bVisible ? 'visible' : 'hidden';
              }
            }
          function btnGetResults_onclick()
          {
            f_saveMe();
            var sPricingModeT = '';
            if (<%= AspxTools.JsGetElementById(m_pricingOptionBroker) %>.checked)
            {
              sPricingModeT = <%= AspxTools.JsString(E_sPricingModeT.InternalBrokerPricing) %>;
            }
            else if (<%= AspxTools.JsGetElementById(m_pricingOptionInvestor) %>.checked)
            {
              sPricingModeT = <%= AspxTools.JsString(E_sPricingModeT.InternalInvestorPricing) %>;
            }
            
            var extraQuery = '';
            <% if (IsTestHistorical) { %>

             extraQuery += '&historicalPolicy=' + encodeURIComponent(<%= AspxTools.JsGetElementById(historicalPolicy) %>.value);
             extraQuery += '&historicalRateOption=' + encodeURIComponent(<%= AspxTools.JsGetElementById(historicalRateOption) %>.value);
            <% } %>
            location.href = 'InternalPricingResult.aspx?loanid=' + ML.sLId + '&sPricingModeT=' + sPricingModeT + extraQuery;
          }
            function investorPricingOptionChanged() {
                var rateLockDate = document.getElementById('m_editRateDate').checked;
                <%= AspxTools.JsGetElementById(sProdInvestorRLckdDays) %>.readOnly = rateLockDate;
                <%= AspxTools.JsGetElementById(sProdInvestorRLckdExpiredD) %>.readOnly = !rateLockDate;            
            }
            
            function pricingOptionChanged() {
                var brokerPricing = document.getElementById('m_pricingOptionBroker').checked;
                document.getElementById('m_editRatePeriod').disabled = brokerPricing;
                document.getElementById('m_editRateDate').disabled = brokerPricing;
                document.getElementById('sProdRLckdDays').readOnly = !brokerPricing;
                <%= AspxTools.JsGetElementById(sProdInvestorRLckdDays) %>.disabled = brokerPricing;
                <%= AspxTools.JsGetElementById(sProdInvestorRLckdExpiredD) %>.disabled = brokerPricing;

                if (!brokerPricing)
                    investorPricingOptionChanged();
            }
            
            function _init() {                
                document.getElementById('sLpTemplateNm').readOnly = true;
                document.getElementById('sLpProductType').readOnly = true;
                document.getElementById('sFinMethT').disabled = true;
                document.getElementById('sTerm').readOnly = true;
                document.getElementById('sDue').readOnly = true;
                document.getElementById('sNoteIR').readOnly = true;
                document.getElementById('sRLckdD').readOnly = true;
                document.getElementById('sRLckdDays').readOnly = true;
                document.getElementById('sRLckdExpiredD').readOnly = true;
                document.getElementById('sRLckdExpiredInDays').readOnly = true;

                pricingOptionChanged();
                sProdFilterMatchCurrentTerm_onclick();
            }
            //-->
        </script>
        <fieldset id="loanInfo" class="QuickPricerFieldSet">
            <legend class="LegendSectionHeader">Loan Information</legend>
            <table>
                <tr>
                    <td>
                        <ml:EncodedLabel ID="lbl_sLpTemplateNm" AssociatedControlID="sLpTemplateNm" runat="server" Text="Loan Program"></ml:EncodedLabel>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="sLpTemplateNm" Width="405px" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ml:EncodedLabel ID="lbl_sLpProductType" AssociatedControlID="sLpProductType" runat="server" Text="Registered Product Type"></ml:EncodedLabel>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="sLpProductType" Width="405px" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ml:EncodedLabel ID="lbl_sFinMethT" AssociatedControlID="sFinMethT" runat="server" Text="Amortization Type"></ml:EncodedLabel>
                    </td>
                    <td>
                        <asp:DropDownList ID="sFinMethT" runat="server"></asp:DropDownList>
                    </td>
                    <td>
                        <ml:EncodedLabel ID="lbl_sTerm" AssociatedControlID="sTerm" runat="server" Text="Term/Due (mnths)"></ml:EncodedLabel>                        
                    </td>
                    <td>
                        <asp:TextBox ID="sTerm" Width="50" runat="server"></asp:TextBox> /
                        <asp:TextBox ID="sDue" Width="50" runat="server"></asp:TextBox>                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <ml:EncodedLabel ID="lbl_sNoteIR" AssociatedControlID="sNoteIR" runat="server" Text="Note Rate"></ml:EncodedLabel>
                    </td>
                    <td>
                        <asp:TextBox ID="sNoteIR" Width="50" runat="server"></asp:TextBox>
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td>
                        <ml:EncodedLabel ID="lbl_sRLckdD" AssociatedControlID="sRLckdD" runat="server" Text="Loan officer rate lock date"></ml:EncodedLabel>
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" CssClass="dateField" ID="sRLckdD"></ml:DateTextBox>
                    </td>
                    <td>
                        <ml:EncodedLabel ID="lbl_sRLckdDays" AssociatedControlID="sRLckdDays" runat="server" Text="Lock period at rate lock"></ml:EncodedLabel>                        
                    </td>
                    <td>
                        <asp:TextBox ID="sRLckdDays" Width="50" runat="server"></asp:TextBox> days
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <ml:EncodedLabel ID="lbl_sRLckdExpiredD" AssociatedControlID="sRLckdExpiredD" runat="server" Text="Loan officer rate lock expiration" />
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" CssClass="dateField" ID="sRLckdExpiredD" />
                    </td>
                    <td>
                        <ml:EncodedLabel ID="lbl_sRLckdExpiredD1" AssociatedControlID="sRLckdExpiredInDays" runat="server" Text="Rate lock expires in" />                      
                    </td>
                    <td>
                        <asp:TextBox ID="sRLckdExpiredInDays" Width="50" runat="server" /> days
                    </td>  
                </tr>
            </table>
        </fieldset>
        <fieldset id="pricingOptions" class="QuickPricerFieldSet">
            <legend class="LegendSectionHeader">Pricing Options</legend>
            <table id="tmpHistoricalTable" runat="server" visible="false" style="border:solid 2px red">
            <tr>
                <td colspan="2" style="color:Red; font-weight:bold">HISTORICAL PRICING EXPERIMENT</td>
            </tr>
            <tr>
                <td>Pricing Data Version</td>
                <td><asp:TextBox ID="sHistoricalPricingVersionD" runat=server ReadOnly=true /></td>
            </tr>
            <tr>
            </tr>
            <tr>
            <td>Policy</td>
            <td><asp:DropDownList ID="historicalPolicy" runat="server" /></td>
            </tr>
            <tr>
            <td>Rate Option</td>
            <td><asp:DropDownList ID="historicalRateOption" runat="server" /></td>
            </tr>
            </table>
            <table>
                <tr>
                    <td colspan="4">
                        <asp:RadioButton ID="m_pricingOptionBroker" GroupName="pricingOption" onclick="pricingOptionChanged()" runat="server" Checked="true" Text="Front-end pricing" />
                    </td>
                </tr>
                <tr>
                    <td style="width:30px"></td>
                    <td>
                        <ml:EncodedLabel ID="lbl_sProdRLckdDays" AssociatedControlID="sProdRLckdDays" runat="server" Text="Front-end rate lock period" />
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="sProdRLckdDays" Width="50" runat="server" /> days
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <ml:EncodedLabel ID="lbl_tbd" runat="server" Text="Price group" AssociatedControlID="sProdLpePriceGroupNm" />                        
                    </td>
                    <td colspan="2">
                      <asp:TextBox ID="sProdLpePriceGroupNm" runat="server" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:RadioButton ID="m_pricingOptionInvestor" GroupName="pricingOption" onclick="pricingOptionChanged()" runat="server" Text="Investor pricing" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <ml:EncodedLabel ID="lbl_sProdInvestorRLckdDays" AssociatedControlID="sProdInvestorRLckdDays" runat="server" Text="Investor rate lock period" />                        
                    </td> 
                    <td>
                        <asp:TextBox ID="sProdInvestorRLckdDays" Width="50" runat="server" onchange="refreshCalculation();" /> days
                    </td>
                    <td>
                        <asp:RadioButton ID="m_editRatePeriod" GroupName="sProdInvestorRLckdModeT" Checked="true" onclick="investorPricingOptionChanged()" runat="server" Text="Edit" value="0" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <label for="sProdInvestorRLckdExpiredD">
                            Investor rate lock would <br />
                            expire on
                        </label>                      
                    </td> 
                    <td>
                        <ml:DateTextBox runat="server" CssClass="dateField" ID="sProdInvestorRLckdExpiredD" onchange="refreshCalculation();" />
                    </td>
                    <td>
                        <asp:RadioButton ID="m_editRateDate" GroupName="sProdInvestorRLckdModeT" onclick="investorPricingOptionChanged()" runat="server" Text="Edit" value="1"/>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <ml:EncodedLabel ID="lbl_tbd3" AssociatedControlID="sInvestorLockLpePriceGroupId" runat="server" Text="Price group" />                        
                    </td> 
                    <td colspan="2">
                        <asp:DropDownList ID="sInvestorLockLpePriceGroupId" runat="server" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <fieldset id="filters" class="QuickPricerFieldSet">
            <legend class="LegendSectionHeader">Filters</legend>
            <table>
                <tr>
                    <td valign="top">
                        <asp:CheckBox ID="sProdFilterDisplayrateMerge" runat="server" Text="Display best prices per program?" /><br />
                        <asp:CheckBox ID="sProdFilterDisplayUsingCurrentNoteRate" runat="server" Text="Restrict results to current note rate?" /><br />
                        <asp:CheckBox ID="sProdFilterRestrictResultToCurrentRegistered" runat="server" Text="Restrict results to registered product type?" /><br />
                        <asp:CheckBox ID="sProdFilterMatchCurrentTerm" runat="server" Text="Match current Amortization Type, Term, and Due" onclick="sProdFilterMatchCurrentTerm_onclick();"/>
                    </td>
                    <td valign="top">
                      <span id="termFilterPanel">
                        Term<br />
                        <asp:CheckBox ID="sProdFilterDue10Yrs" runat="server" Text="10yr" /><br />
                        <asp:CheckBox ID="sProdFilterDue15Yrs" runat="server" Text="15yr" /><br />
                        <asp:CheckBox ID="sProdFilterDue20Yrs" runat="server" Text="20yr" /><br />
                        <asp:CheckBox ID="sProdFilterDue25Yrs" runat="server" Text="25yr" /><br />
                        <asp:CheckBox ID="sProdFilterDue30Yrs" runat="server" Text="30yr" /><br />                        
                        <asp:CheckBox ID="sProdFilterDueOther" runat="server" Text="Other" />
                        </span>
                    </td>
                    <td valign="top">
                      <span id="amortFilterPanel">
                        Amortization Type<br />
                        <asp:CheckBox ID="sProdFilterFinMethFixed" runat="server" Text="Fixed" /><br />                        
                        <asp:CheckBox ID="sProdFilterFinMeth3YrsArm" runat="server" Text="3yr ARM" /><br />
                        <asp:CheckBox ID="sProdFilterFinMeth5YrsArm" runat="server" Text="5yr ARM" /><br />
                        <asp:CheckBox ID="sProdFilterFinMeth7YrsArm" runat="server" Text="7yr ARM" /><br />
                        <asp:CheckBox ID="sProdFilterFinMeth10YrsArm" runat="server" Text="10yr ARM" /><br />
                        <asp:CheckBox ID="sProdFilterFinMethOther" runat="server" Text="Other" />
                        </span>
                    </td>

                    <td valign="top">
                      <span id="amortFilterPanel">
                        Payment Type<br />
                        <asp:CheckBox ID="sProdFilterPmtTPI" runat="server" Text="P&I" /><br />                        
                        <asp:CheckBox ID="sProdFilterPmtTIOnly" runat="server" Text="I/O" /><br />
                        </span>
                    </td>
                </tr>
            </table>
        </fieldset>
        <input type="button" id="btnGetResults" value="Get Results >>" onclick="btnGetResults_onclick();" />
    </form>
</body>
</html>
