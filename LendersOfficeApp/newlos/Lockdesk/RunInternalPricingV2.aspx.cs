﻿#region Generated Code -- lie to hide newlos from the merciless StyleCop
namespace LendersOfficeApp.newlos.Lockdesk
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using DataAccess.LoanValidation;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// Updated version of the internal pricer to support historical pricing.
    /// </summary>
    public partial class RunInternalPricingV2 : BaseLoanPage
    {
        /// <summary>
        /// The mode the page will render in.
        /// </summary>
        private enum PageMode
        {
            /// <summary>
            /// Render the page for the Internal Pricer.
            /// </summary>
            InternalPricing,

            /// <summary>
            /// Render the page for check eligibility.
            /// </summary>
            CheckEligibility
        }

        /// <summary>
        /// Gets the required permissions to read this page.
        /// </summary>
        /// <value>The required permissions to read this page.</value>
        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                // The permission checks for CheckEligibility occur in PageInit.
                return this.Mode == PageMode.InternalPricing
                    ? new[] { Permission.AllowLockDeskRead }
                    : new Permission[] { };
            }
        } 

        /// <summary>
        /// Gets the required permissions to write to this page.
        /// </summary>
        /// <value>The required permissions to write to this page.</value>
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                // The CheckEligibility mode does not have write permission checks.
                return this.Mode == PageMode.InternalPricing
                    ? new[] { Permission.AllowLockDeskWrite }
                    : new Permission[] { };
            }
        } 

        /// <summary>
        /// Gets the mode of the page.
        /// </summary>
        /// <value>The mode of the page.</value>
        private PageMode Mode
        {
            get
            {
                switch (this.PricingMode)
                {
                    case E_sPricingModeT.EligibilityBrokerPricing:
                    case E_sPricingModeT.EligibilityInvestorPricing:
                        return PageMode.CheckEligibility;
                    case E_sPricingModeT.Undefined:
                    case E_sPricingModeT.RegularPricing:
                    case E_sPricingModeT.InternalBrokerPricing:
                    case E_sPricingModeT.InternalInvestorPricing:
                        return PageMode.InternalPricing;
                    default:
                        throw new UnhandledEnumException(this.PricingMode);
                }
            }
        }

        /// <summary>
        /// Gets the pricing mode of the page, which will affect how the page
        /// initially renders and which options are selected by default.
        /// </summary>
        /// <value>The pricing mode of the page.</value>
        private E_sPricingModeT PricingMode => (E_sPricingModeT)RequestHelper.GetInt("sPricingModeT", 0);

        /// <summary>
        /// Gets a value indicating whether the advanced pricing filters are enabled.
        /// </summary>
        private bool IsAdvancedFiltersEnabled => Broker.IsAdvancedFilterOptionsForPricingEnabled(PrincipalFactory.CurrentPrincipal);

        /// <summary>
        /// Gets the pricing mode to use when loading up the filters on page load.
        /// </summary>
        private E_sPricingModeT PricingModeForFilters => this.PricingMode == E_sPricingModeT.RegularPricing || this.PricingMode == E_sPricingModeT.Undefined ? E_sPricingModeT.InternalBrokerPricing : this.PricingMode;

        /// <summary>
        /// Gets the mode for the page to render in.
        /// </summary>
        /// <returns>The mode for the page to render in.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode() => E_XUAComaptibleValue.IE10;

        /// <summary>
        /// Handles the page initialization event.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }

        /// <summary>
        /// Performs page initialization that doesn't require a loaded loan.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void PageInit(object sender, EventArgs e)
        {
            if (this.Mode == PageMode.CheckEligibility
                && !BrokerUser.HasPermission(Permission.AllowUnderwritingAccess)
                && !BrokerUser.IsInEmployeeGroup(ConstAppDavid.CheckEligibilityEmployeeGroup))
            {
                // Taken straight from the check eligibility page.
                if (Broker.IsEnableBigLoanPage)
                {
                    Response.Redirect("~/newlos/BigLoanInfo.aspx?loanid=" + this.LoanID + "&appid=" + this.ApplicationID, true);
                }
                else
                {
                    Response.Redirect("~/newlos/LoanInfo.aspx?loanid=" + this.LoanID + "&appid=" + this.ApplicationID, true);
                }

                return;
            }

            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("LQBPopup.js");
            this.DisplayCopyRight = false;

            if (this.Mode == PageMode.CheckEligibility)
            {
                this.Title = "Check Eligibility";
                this.PageTitle = "Check Eligibility";
            }
            else
            {
                this.Title = "Run Internal Pricing";
                this.PageTitle = "Run Internal Pricing";
            }

            this.RegisterJsScript("mask.js");
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterJsScript("RunInternalPricingV2.js");

            this.RegisterCSS("bootstrap.min.css");
            this.RegisterCSS("RunInternalPricingV2.css");
            this.RegisterCSS("PricingResults.css");
            this.RegisterCSS("jquery-ui-1.11.custom.css");
            this.RegisterCSS("ProductCodeFilterPopupForIP2.css");

            this.RegisterJsScript("angular-1.5.5.min.js");
            this.RegisterJsScript("PricingResults.v3.js");

            Tools.Bind_sFinMethT(this.sFinMethT);
            Tools.Bind_sLPurposeTPe(this.sLPurposeTPe);
            Tools.Bind_sSellerCreditT(this.sSellerCreditT);
            Tools.Bind_sFinMethT(this.sOtherLFinMethT);

            foreach (var priceGroup in this.Broker.ListEnablePriceGroups())
            {
                this.sInvestorLockLpePriceGroupId.Items.Add(new ListItem(priceGroup.Value, priceGroup.Key.ToString()));
            }

            this.HistoricalPricing.Value = PricingResultsType.Historical.ToString("D");
            this.WorstCasePricing.Value = PricingResultsType.WorstCase.ToString("D");
            this.CurrentPricing.Value = PricingResultsType.Current.ToString("D");
            this.RegisterJsEnumByName(typeof(PricingResultsType));
            this.RegisterJsEnumByName(typeof(E_sPricingModeT));
            ClientScript.RegisterHiddenField("IsCheckEligibility", (this.Mode == PageMode.CheckEligibility).ToString());
        }

        /// <summary>
        /// Loads a loan and initializes the page.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var loan = this.GetInitializedLoan();
            this.RegisterJsGlobalVariables("IsUlad2019", loan.sGseTargetApplicationT == GseTargetApplicationT.Ulad2019);

            if (!this.AllowRunningInternalPricer(loan))
            {
                Response.Redirect($"~/newlos/LoanApp.aspx?loanid={LoanID}&highURL={E_UrlOption.Page_InternalPricingValidationError.ToString("D")}");
                return;
            }

            Tools.Bind_sProdDocT(this.sProdDocT, this.Broker, loan.sProdDocT);

            bool isBreakEvenMonths = loan.sLPurposeT != E_sLPurposeT.Purchase && (!Broker.EnableCashToCloseAndReservesDebugInPML || Broker.DisplayBreakEvenMthsInsteadOfReserveMthsInPML2ForNonPurLoans);

            RegisterJsGlobalVariables("showQm", PrincipalFactory.CurrentPrincipal.BrokerDB.ShowQMStatusInPml2 && loan.BranchChannelT != E_BranchChannelT.Correspondent);
            RegisterJsGlobalVariables("IsDebugColumns", PrincipalFactory.CurrentPrincipal.BrokerDB.EnableCashToCloseAndReservesDebugInPML);
            RegisterJsGlobalVariables("IsBreakEvenMonths", isBreakEvenMonths);
            RegisterJsGlobalVariables("sDisclosureRegulationT", (int)loan.sDisclosureRegulationT);
            RegisterJsGlobalVariables("IsAdvancedFiltersEnabled", this.IsAdvancedFiltersEnabled);

            this.LoadHeader(loan);
            this.LoadLoanInfo(loan);
            this.LoadFilters(loan);
            this.LoadPricingOptions(loan);
        }

        /// <summary>
        /// Gets a loaded loan.
        /// </summary>
        /// <returns>A loaded loan.</returns>
        private CPageData GetInitializedLoan()
        {
            var loan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(RunInternalPricingV2));
            loan.InitLoad();

            // OPM 237202.  To allow the PG data to flow like standard PML.
            loan.TransformDataToPml(E_TransformToPmlT.FromScratch);

            return loan;
        }

        /// <summary>
        /// Verifies whether the file can be run through the internal pricer.
        /// </summary>
        /// <param name="loan">The loan file.</param>
        /// <returns>True if the file can run be run through internal pricing. Otherwise, false.</returns>
        private bool AllowRunningInternalPricer(CPageData loan)
        {
            var resultList = Tools.RunInternalPricingValidation(loan, this.BrokerUser);
            return resultList.ErrorCount == 0;
        }

        /// <summary>
        /// Loads the header section.
        /// </summary>
        /// <param name="loan">The loan file.</param>
        private void LoadHeader(CPageData loan)
        {
            this.backButton.HRef = Tools.VRoot + $"/newlos/loanapp.aspx?loanid={this.LoanID}";

            this.sEmployeeLoanRepFullName.InnerText = loan.sEmployeeLoanRep.FullName;
            this.sLNm.InnerText = loan.sLNm;
            this.sStatusT.InnerText = loan.sStatusT_rep;
            this.sRateLockStatusT.InnerText = loan.sRateLockStatusT_rep;
            this.aBNm.InnerText = loan.GetAppData(0).aBNm;
            this.sCreditScoreType2.InnerText = loan.sCreditScoreType2_rep;
        }

        /// <summary>
        /// Loads the Loan Info section.
        /// </summary>
        /// <param name="loan">The loan file.</param>
        private void LoadLoanInfo(CPageData loan)
        {
            this.sLpTemplateNm.Value = loan.sLpTemplateNm;
            this.sLpTemplateNm.Attributes["title"] = loan.sLpTemplateNm;
            this.sLpProductType.Value = loan.sLpProductType;
            this.sLpProductType.Attributes["title"] = loan.sLpProductType;

            // sLpInvestorNm is protected by the Lock Desk read permission.
            // We don't want to require this permission for the Check Eligibility
            // page, so don't access the field.
            if (this.Mode == PageMode.CheckEligibility)
            {
                this.sLpInvestorNmRow.Visible = false;
            }
            else
            {
                this.sLpInvestorNm.Value = loan.sLpInvestorNm;
                this.sLpInvestorNm.Attributes["title"] = loan.sLpInvestorNm;
            }

            this.sTerm.Value = loan.sTerm_rep;
            this.sDue.Value = loan.sDue_rep;
            this.sFinMethT.Value = loan.sFinMethT.ToString("D");
            this.sNoteIR.Value = loan.sNoteIR_rep;
            this.sRLckdD.Value = loan.sRLckdD_rep;
            this.sRLckdDays.Value = loan.sRLckdDays_rep;
            this.sRLckdExpiredD.Value = loan.sRLckdExpiredD_rep;
            this.sRLckdExpiredInDays.Value = loan.sRLckdExpiredInDays_rep;

            this.IsRenovationRow.Visible = this.Broker.IsEnableRenovationCheckboxInPML || this.Broker.EnableRenovationLoanSupport;
            this.sIsRenovationLoan.Checked = loan.sIsRenovationLoan;
            this.TotalRenovationCostsRow.Visible = loan.sIsRenovationLoan && (this.Broker.IsEnableRenovationCheckboxInPML || this.Broker.EnableRenovationLoanSupport);
            this.sTotalRenovationCosts.Value = loan.sTotalRenovationCosts_rep;
            this.RefinanceTypeRow.Visible = loan.sLPurposeT != E_sLPurposeT.Purchase && !loan.sIsStandAlone2ndLien;
            this.sLPurposeTPe.Value = loan.sLPurposeTPe.ToString("D");

            bool showTexas50a6Row = loan.sIsStandAlone2ndLien
                ? loan.sSpStatePe == "TX"
                : loan.sSpStatePe == "TX" && loan.sLPurposeTPe != E_sLPurposeT.Purchase;
            this.Texas50a6Row.Visible = showTexas50a6Row;
            this.sPreviousLoanIsTexas50a6LoanRow.Visible = showTexas50a6Row;

            this.sProdIsTexas50a6Loan.Checked = loan.sProdIsTexas50a6Loan;
            this.sPreviousLoanIsTexas50a6Loan.Checked = loan.sPreviousLoanIsTexas50a6Loan;

            this.EndorsedRow.Visible = loan.sLPurposeTPe == E_sLPurposeT.FhaStreamlinedRefinance;
            this.sProdIsLoanEndorsedBeforeJune09.Checked = loan.sProdIsLoanEndorsedBeforeJune09;
            this.CashoutAmountRow.Visible = loan.sIsStandAlone2ndLien || loan.sLPurposeTPe == E_sLPurposeT.RefinCashout;
            this.sProdCashoutAmt.Value = loan.sProdCashoutAmt_rep;

            this.CurrentLoanPiPaymentCurrentMipPerMonth.Visible = loan.sLPurposeTPe != E_sLPurposeT.Purchase
                && !loan.sIsStandAlone2ndLien;
            this.sProdCurrPIPmt.Value = loan.sProdCurrPIPmt_rep;
            this.sProdCurrMIPMo.Value = loan.sProdCurrMIPMo_rep;

            this.FhaStreamlineRefiVaIrrrlFields.Visible = loan.sLPurposeTPe == E_sLPurposeT.FhaStreamlinedRefinance
                || loan.sLPurposeTPe == E_sLPurposeT.VaIrrrl;
            this.sIsCreditQualifying.Checked = loan.sIsCreditQualifying;
            this.sHasAppraisal.Checked = loan.sHasAppraisal;
            this.sSpLien.Value = loan.sSpLien_rep;

            this.UpfrontMipRefundRow.Visible = loan.sLPurposeTPe == E_sLPurposeT.FhaStreamlinedRefinance;
            this.sFHASalesConcessions.Value = loan.sFHASalesConcessions_rep;
            this.SellerCreditRow.Visible = loan.sLPurposeTPe == E_sLPurposeT.Purchase;
            this.sSellerCreditT.Value = loan.sSellerCreditT.ToString("D");

            this.StandaloneSecondLienFields.Visible = loan.sIsStandAlone2ndLien;
            this.sLpIsNegAmortOtherLien.Checked = loan.sLpIsNegAmortOtherLien;
            this.sOtherLFinMethT.Value = loan.sOtherLFinMethT.ToString("D");
            this.sProOFinPmtPe.Value = loan.sProOFinPmtPe_rep;

            this.sProdImpound.Checked = loan.sProdImpound;

            this.sProdDocT.Value = loan.sProdDocT.ToString("D");

            this.OriginalAppraisedValueRow.Visible = loan.sLPurposeTPe == E_sLPurposeT.FhaStreamlinedRefinance
                && loan.sHasAppraisal;
            this.sOriginalAppraisedValue.Value = loan.sOriginalAppraisedValue_rep;

            this.AppraisedValueRow.Visible = loan.sLPurposeTPe == E_sLPurposeT.Purchase;
            this.sApprValPeLabel.InnerText = loan.sIsRenovationLoan && (this.Broker.IsEnableRenovationCheckboxInPML || this.Broker.EnableRenovationLoanSupport)
                ? "As-Completed Value"
                : "Appraised Value";
            this.sApprValPe.Value = loan.sApprValPe_rep;

            this.sHouseValPe.Value = loan.sHouseValPe_rep;
            if (loan.sIsLineOfCredit)
            {
                this.sHouseValPeLabel.InnerText = "Home Value";
            }
            else if (loan.sLPurposeTPe == E_sLPurposeT.Purchase)
            {
                this.sHouseValPeLabel.InnerText = "Sales Price";
            }
            else if (loan.sLPurposeTPe == E_sLPurposeT.FhaStreamlinedRefinance
                && loan.sHasAppraisal
                && !loan.sIsStandAlone2ndLien)
            {
                this.sHouseValPeLabel.InnerText = "Appraised Value";
            }
            else if (loan.sLPurposeTPe == E_sLPurposeT.FhaStreamlinedRefinance
                && !loan.sHasAppraisal
                && !loan.sIsStandAlone2ndLien)
            {
                this.HomeValueRow.Visible = false;
            }
            else if (loan.sLPurposeTPe != E_sLPurposeT.Purchase
                && (this.Broker.IsEnableRenovationCheckboxInPML || this.Broker.EnableRenovationLoanSupport)
                && loan.sIsRenovationLoan
                && !loan.sIsStandAlone2ndLien)
            {
                this.sHouseValPeLabel.InnerText = "As-Completed Value";
            }
            else
            {
                this.sHouseValPeLabel.InnerText = "Home Value";
            }

            this.AsIsValueRow.Visible = loan.sIsRenovationLoan && 
                (this.Broker.IsEnableRenovationCheckboxInPML || this.Broker.EnableRenovationLoanSupport);
            this.sAsIsAppraisedValuePeval.Value = loan.sAsIsAppraisedValuePeval_rep;

            this.DownPaymentLabel.InnerText = loan.sLPurposeT == E_sLPurposeT.Purchase
                ? "Down Payment"
                : "Equity";
            this.sDownPmtPcPe.Value = loan.sDownPmtPcPe_rep;
            this.sEquityPe.Value = loan.sEquityPe_rep;
            this.FirstLienLtv.Visible = !loan.sIsStandAlone2ndLien;
            this.sLtvRPe.Value = loan.sLtvRPe_rep;
            this.sLAmtCalcPe.Value = loan.sLAmtCalcPe_rep;
            this.StandaloneSecondLienLtv.Visible = loan.sIsStandAlone2ndLien;
            this.sLtvROtherFinPe2.Value = loan.sLtvROtherFinPe_rep;
            this.sProOFinBalPe2.Value = loan.sProOFinBalPe_rep;

            this.LineAmountRow.Visible = !loan.sIsStandAlone2ndLien
                && loan.sIsLineOfCredit;
            this.sCreditLineAmt.Value = loan.sCreditLineAmt_rep;

            bool has2nd = loan.sHas2ndFinPe
                || (loan.sLienPosT == E_sLienPosT.First
                        && loan.sSubFinT == E_sSubFinT.Heloc
                        && (loan.sSubFin > 0 || loan.sConcurSubFin > 0)); // OPM 145405 Helocs can have $0 Other Balance
            this.No2ndFinancing.Checked = !has2nd;
            this.Yes2ndFinancing.Checked = has2nd;

            this.SecondFinancingTypeRow.Visible = has2nd;
            this.ClosedEnd2ndFinancing.Checked = loan.sSubFinT == E_sSubFinT.CloseEnd;
            this.Heloc2ndFinancing.Checked = loan.sSubFinT == E_sSubFinT.Heloc;

            this.SecondFinancingFields.Visible = has2nd;
            this.SecondFinancingAmountLabel.InnerText = loan.sSubFinT == E_sSubFinT.CloseEnd
                ? "2nd Financing"
                : "Initial Draw Amount";
            this.FirstLienOtherFinancing.Visible = !loan.sIsStandAlone2ndLien;
            this.sLtvROtherFinPe.Value = loan.sLtvROtherFinPe_rep;
            this.sProOFinBalPe.Value = loan.sProOFinBalPe_rep;
            this.StandaloneSecondLienOtherFinancing.Visible = loan.sIsStandAlone2ndLien;
            this.sLtvRPe2.Value = loan.sLtvRPe_rep;
            this.sLAmtCalcPe2.Value = loan.sLAmtCalcPe_rep;
            this.sCltvRPe.Value = loan.sCltvRPe_rep;

            this.SecondFinancingHelocFields.Visible = has2nd
                && loan.sSubFinT == E_sSubFinT.Heloc;
            this.sSubFinToPropertyValue.Value = loan.sSubFinToPropertyValue_rep;
            this.sSubFinPe.Value = loan.sSubFinPe_rep;
            this.sHCLTVRPe.Value = loan.sHCLTVRPe_rep;

            if (loan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees)
            {
                this.sBrokerLockOriginatorPriceBrokComp1PcPrice.Value = loan.sBrokerLockOriginatorPriceBrokComp1PcPrice;
                this.sBrokerLockFinalBrokComp1PcPrice.Visible = false;
            }
            else
            {
                this.sBrokerLockFinalBrokComp1PcPrice.Value = loan.sBrokerLockFinalBrokComp1PcPrice_rep;
                this.sBrokerLockOriginatorPriceBrokComp1PcPrice.Visible = false;
            }

            // sInvestorLockBrokComp1PcPrice is protected by the Lock Desk read permission.
            // We don't want to require this permission for the Check Eligibility
            // page, so don't access the field for that mode. The row is hidden in LoadPricingOptions.
            if (this.Mode != PageMode.CheckEligibility)
            {
                this.sInvestorLockBrokComp1PcPrice.Value = loan.sInvestorLockBrokComp1PcPrice_rep;
            }
        }

        /// <summary>
        /// Loads the Results Filter section.
        /// </summary>
        /// <param name="loan">The loan file.</param>
        private void LoadFilters(CPageData loan)
        {
            this.ResultsFilter.Visible = this.PricingMode != E_sPricingModeT.EligibilityBrokerPricing
                && this.PricingMode != E_sPricingModeT.EligibilityInvestorPricing;

            if (!this.ResultsFilter.Visible)
            {
                return;
            }

            bool isAdvancedFiltersEnabled = this.IsAdvancedFiltersEnabled;
            advancedFilterBtn.Visible = isAdvancedFiltersEnabled;
            advancedFilterBtn.Disabled = loan.sProdFilterRestrictResultToRegisteredProgram;
            ProductCodeFilterPopup.Visible = isAdvancedFiltersEnabled;
            ProductCodeFilterPopup.EnableAdvancedFilterOptionsForPriceEngineResults = isAdvancedFiltersEnabled;
            if (isAdvancedFiltersEnabled)
            {
                var oldPricingMode = loan.sPricingModeT;
                loan.sPricingModeT = this.PricingModeForFilters; 
                ProductCodeFilterPopup.SelectedProductCodes = loan.sSelectedProductCodeFilter;
                ProductCodeFilterPopup.AvailableProductCodes = loan.sAvailableProductCodeFilter;
                ProductCodeFilterPopup.ProductCodesByFileType = loan.sProductCodesByFileType;
                loan.sPricingModeT = oldPricingMode;
            }

            this.sProdFilterRestrictResultToRegisteredProgram.Checked = loan.sProdFilterRestrictResultToRegisteredProgram;
            this.sProdFilterRestrictResultToCurrentRegistered.Checked = loan.sProdFilterRestrictResultToCurrentRegistered;
            this.sProdFilterRestrictResultToCurrentInvestor.Checked = loan.sProdFilterRestrictResultToCurrentInvestor;
            this.sProdFilterMatchCurrentTerm.Checked = loan.sProdFilterMatchCurrentTerm;
            this.sProdFilterDisplayUsingCurrentNoteRate.Checked = loan.sProdFilterDisplayUsingCurrentNoteRate;
            this.sProdFilterDisplayrateMerge.Checked = loan.sProdFilterDisplayrateMerge;

            this.sProdFilterDue10Yrs.Checked = loan.sProdFilterDue10Yrs;
            this.sProdFilterDue15Yrs.Checked = loan.sProdFilterDue15Yrs;
            this.sProdFilterDue20Yrs.Checked = loan.sProdFilterDue20Yrs;
            this.sProdFilterDue25Yrs.Checked = loan.sProdFilterDue25Yrs;
            this.sProdFilterDue30Yrs.Checked = loan.sProdFilterDue30Yrs;
            this.sProdFilterDueOther.Checked = loan.sProdFilterDueOther;
            this.sProdFilterFinMethFixed.Checked = loan.sProdFilterFinMethFixed;
            this.sProdFilterFinMeth3YrsArm.Checked = loan.sProdFilterFinMeth3YrsArm;
            this.sProdFilterFinMeth5YrsArm.Checked = loan.sProdFilterFinMeth5YrsArm;
            this.sProdFilterFinMeth7YrsArm.Checked = loan.sProdFilterFinMeth7YrsArm;
            this.sProdFilterFinMeth10YrsArm.Checked = loan.sProdFilterFinMeth10YrsArm;
            this.sProdFilterFinMethOther.Checked = loan.sProdFilterFinMethOther;
            this.sProdIncludeNormalProc.Checked = loan.sProdIncludeNormalProc;
            this.sProdIncludeMyCommunityProc.Checked = loan.sProdIncludeMyCommunityProc;
            this.sProdIncludeHomePossibleProc.Checked = loan.sProdIncludeHomePossibleProc;
            this.sProdIncludeFHATotalProc.Checked = loan.sProdIncludeFHATotalProc;
            this.sProdIncludeVAProc.Checked = loan.sProdIncludeVAProc;
            this.sProdIncludeUSDARuralProc.Checked = loan.sProdIncludeUSDARuralProc;
            this.sProdFilterPmtTPI.Checked = loan.sProdFilterPmtTPI;
            this.sProdFilterPmtTIOnly.Checked = loan.sProdFilterPmtTIOnly;

            if (string.IsNullOrEmpty(loan.sLpProductType))
            {
                this.sProdFilterRestrictResultToCurrentRegistered.Attributes["disabled"] = "disabled";
                this.sProdFilterRestrictResultToCurrentRegistered.Attributes["class"] += " perma-disable";
            }

            if (string.IsNullOrEmpty(loan.sLpTemplateNm))
            {
                this.sProdFilterRestrictResultToRegisteredProgram.Attributes["disabled"] = "disabled";
                this.sProdFilterRestrictResultToRegisteredProgram.Attributes["class"] += " perma-disable";
            }
        }

        /// <summary>
        /// Loads the Pricing Options section.
        /// </summary>
        /// <param name="loan">The loan file.</param>
        private void LoadPricingOptions(CPageData loan)
        {
            Bind_HistoricalSubmissions(this.HistoricalSubmissions, loan);

            switch (this.PricingMode)
            {
                case E_sPricingModeT.Undefined:
                case E_sPricingModeT.RegularPricing:
                case E_sPricingModeT.InternalBrokerPricing:
                case E_sPricingModeT.EligibilityBrokerPricing:
                    this.frontEndPricing.Checked = true;
                    break;
                case E_sPricingModeT.InternalInvestorPricing:
                case E_sPricingModeT.EligibilityInvestorPricing:
                    this.investorPricing.Checked = true;
                    break;
                default:
                    throw new UnhandledEnumException(this.PricingMode);
            }

            this.sProdRLckdDays.Value = loan.sProdRLckdDays_rep;

            // The CheckEligibility page will only display the price group name
            // if the lender is configured to display the price group in Embedded 
            // PML. For Internal Pricing, we want to display the value regardless
            // of the setting. gf opm 450583
            string priceGroupName = this.Mode == PageMode.CheckEligibility
                ? loan.sProdLpePriceGroupNm
                : loan.RetrievePriceGroupNameWithBypass();
            this.sProdLpePriceGroupNm.Value = priceGroupName;
            this.sProdLpePriceGroupNm.Attributes["title"] = priceGroupName;

            this.editInvestorRateLockPeriod.Value = E_sProdInvestorRLckdModeT.RateLockPeriod.ToString("D");
            this.editInvestorRateLockPeriod.Checked = loan.sProdInvestorRLckdModeT == E_sProdInvestorRLckdModeT.RateLockPeriod;
            this.sProdInvestorRLckdDays.Value = loan.sProdInvestorRLckdDays_rep;

            this.editInvestorRateExpiration.Value = E_sProdInvestorRLckdModeT.RateLockExpiredDate.ToString("D");
            this.editInvestorRateExpiration.Checked = loan.sProdInvestorRLckdModeT == E_sProdInvestorRLckdModeT.RateLockExpiredDate;
            this.sProdInvestorRLckdExpiredD.Value = loan.sProdInvestorRLckdExpiredD_rep;

            this.sInvestorLockLpePriceGroupId.Value = loan.sInvestorLockLpePriceGroupId == Guid.Empty
                ? loan.sProdLpePriceGroupId.ToString()
                : loan.sInvestorLockLpePriceGroupId.ToString();

            this.GetResultsUsing.Visible = this.BrokerUser.UseHistoricalPricingAndUiEnhancements;
            SetResultsType(loan);

            if (this.Mode == PageMode.CheckEligibility)
            {
                this.WorstCasePricingRow.Visible = false;
                this.HistoricalSubmissions.Attributes["class"] += " disabled-select";
                this.FrontEndFinalPriceContainer.Visible = false;
                this.InvestorFinalPriceContainer.Visible = false;
            }
        }

        /// <summary>
        /// Binds the historical submissions dropdown.
        /// </summary>
        /// <param name="select">The dropdown.</param>
        /// <param name="loan">The loan file.</param>
        private void Bind_HistoricalSubmissions(HtmlSelect select, CPageData loan)
        {
            var snapshots = loan.SubmissionSnapshots;
            var numStandardSubmissions = 0;
            var numFrontEndPricingSubmissions = 0;
            var numInvestorPricingSubmissions = 0;

            var snapshotByDescription = new Dictionary<string, string>();

            foreach (var snapshot in snapshots)
            {
                string description;

                switch (snapshot.SubmissionType)
                {
                    case E_sPricingModeT.Undefined:
                    case E_sPricingModeT.RegularPricing:
                        numStandardSubmissions++;
                        description = $"{numStandardSubmissions}{Tools.ComputeOrdinal(numStandardSubmissions)} PML Pricing Submission";
                        break;
                    case E_sPricingModeT.InternalBrokerPricing:
                        numFrontEndPricingSubmissions++;
                        description = $"{numFrontEndPricingSubmissions}{Tools.ComputeOrdinal(numFrontEndPricingSubmissions)} Front-end Internal Pricing Submission";
                        break;
                    case E_sPricingModeT.InternalInvestorPricing:
                        numInvestorPricingSubmissions++;
                        description = $"{numInvestorPricingSubmissions}{Tools.ComputeOrdinal(numInvestorPricingSubmissions)} Investor Internal Pricing Submission";
                        break;
                    default:
                        throw new UnhandledEnumException(snapshot.SubmissionType);
                }

                snapshotByDescription.Add(description, SerializationHelper.JsonNetAnonymousSerialize(snapshot));
            }

            this.RegisterJsObject("HistoricalSubmissionOptions", snapshotByDescription);
        }

        /// <summary>
        /// Sets the default results type.
        /// </summary>
        /// <param name="loan">The loan file.</param>
        private void SetResultsType(CPageData loan)
        {
            if (this.Mode == PageMode.InternalPricing)
            {
                if (loan.SubmissionSnapshots.Any()
                    && loan.sRateLockStatusT != E_sRateLockStatusT.NotLocked
                    && loan.sStatusT != E_sStatusT.Loan_Canceled
                    && loan.sStatusT != E_sStatusT.Loan_Rejected)
                {
                    if (loan.sIsRateLockExpired)
                    {
                        this.WorstCasePricing.Checked = true;
                    }
                    else
                    {
                        this.HistoricalPricing.Checked = true;
                    }
                }
                else
                {
                    this.CurrentPricing.Checked = true;
                }
            }
            else
            {
                if (loan.SubmissionSnapshots.Any()
                    && loan.sIsRateLocked
                    && !loan.sIsRateLockExpired)
                {
                    this.HistoricalPricing.Checked = true;
                }
                else
                {
                    this.CurrentPricing.Checked = true;
                }
            }
        }
    }
}