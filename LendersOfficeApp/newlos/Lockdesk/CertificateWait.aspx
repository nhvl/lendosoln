﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CertificateWait.aspx.cs" Inherits="LendersOfficeApp.newlos.Lockdesk.CertificateWait" %>
<%@ Import namespace="LendersOffice.Common"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
  <script type="text/javascript">
    var gRequestId;
    var g_iPollingInterval = 1000;
    function _init()
    {
      setTimeout(f_requestCertificate, 300);
    }
    function f_requestCertificate()
    {
      var args = {
        LoanId: ML.sLId,
        ProductId: ML.ProductId,
        sLienQualifyModeT: ML.sLienQualifyModeT,
        sPricingModeT: ML.sPricingModeT,
        historicalPolicy: ML.historicalPolicy,
        historicalRateOption: ML.historicalRateOption,
        snapshotJson: ML.snapshotJson,
        isHistorical: ML.isHistorical,
        Rate: ML.Rate,
        Fee: ML.Fee,
        UniqueChecksum: ML.UniqueChecksum
      };


      var result = gService.loanedit.call('RequestCertificate', args);
      if (!result.error)
      {
        gRequestId = result.value["RequestID"];
        setTimeout(f_isAvailable, g_iPollingInterval);
      }
      else {
          f_displayError(result.UserMessage);
      }
      
    }
    function f_isAvailable()
    {
      var args = { RequestId: gRequestId };

      var result = gService.loanedit.call('IsResultReady', args);
      if (!result.error)
      {
        var isReady = result.value["IsReady"] === 'True';
        if (isReady)
        {
          f_displayResult();
        }
        else
        {
          setTimeout(f_isAvailable, g_iPollingInterval);
        }
      }
      else
      {
        alert('Error. Unable to render certificate.');
      }

    }
    function f_displayError(msg)
    {
      alert(msg);
    }
    function f_displayResult()
    {
      var extraArg = "";
      <% if (RequestHelper.GetBool("detailCert")) {%>
        extraArg = "&detailCert=1";
      <%}%>
      
        self.location = "Certificate.aspx?loanid=" + ML.sLId + "&requestid=" + encodeURIComponent(gRequestId) + "&sPricingModeT=" + ML.sPricingModeT + extraArg;
    }
  </script>
    <form id="form1" runat="server">
    <div>
        <table width="100%" height="100%" border="0" id="WaitTable">
	<tr>
		<td align=center valign=middle class="WaitMessageLabel">Please Wait ...</td>
	</tr>
</table>
    </div>
    </form>
</body>
</html>
