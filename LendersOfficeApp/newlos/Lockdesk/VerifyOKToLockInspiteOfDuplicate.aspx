﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="VerifyOKToLockInspiteOfDuplicate.aspx.cs" Inherits="LendersOfficeApp.newlos.Lockdesk.VerifyOKToLockInspiteOfDuplicate" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
  <HEAD runat="server">
    <title>Duplicate Loan Submission Alert</title>
  </HEAD>
<body bgColor=gainsboro margin="0" MS_POSITIONING="FlowLayout" scroll="no">
    <form id="form1" runat="server">
    <script type="text/javascript">
        function f_close(lockIt) {
            var arg = window.dialogArguments || {};
            arg.OK = lockIt;
            onClosePopup(arg);
        }
        function _init() {
            resizeForIE6And7(400, 200);
            var dlgArgs = getModalArgs();
            if (dlgArgs != null) {
                var dups = dlgArgs.duplicates;
                for (var i = 0; i < dups.length; i++) {
                    createAndAddLink(dups[i].LoanID, dups[i].LoanName);
                }
                $('a').after('  ');
            }
        }
        function createAndAddLink(loanID, loanName) {
            var s = document.getElementById('spanForDuplicateLoanLinks');

            $('<a>', {
                text: loanName,
                href: '#',
                click: function() { editLoan(loanID, loanName); }
            }).appendTo(s);
        }
        function editLoan(loanID, loanName) {
            if(window.opener != null)
            {
                var o = window.opener.top; // lock desk page window
                if(o.opener != null)
                {
                    var p = o.opener;  // pipeline
                    if(typeof(p.editLoan) == 'object')
                    {
                        return p.editLoan(loanID);
                    }
                }
            }
            lw_url = ML.VirtualRoot + '/newlos/loanapp.aspx?loanid=' + loanID;
            return __openLoanWindow(loanID, lw_url);

        } 
        function generateWindowName(sLId) {
            return 'LoanEdit_' + sLId.replace(/-/g, '');
        }
        function __openLoanWindow(sLId, url) {
            name = generateWindowName(sLId)
            var w = screen.availWidth - 10;
            var h = screen.availHeight - 50;

            var win = wmOpen(url, 'width=' + w + 'px,height=' + h + 'px,left=0px,top=0px,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes');

            win.focus();
            return win;
        }
        
        function wmOpen(url, options) {
          if (wmOpen.arguments.length < 3) options = "resizable=yes,scrollbars=yes,menubar=no,toolbar=no";
          return window.open(url,'_blank', options);
        }

        
    </script>
    <h4 class="page-header">Duplicate Loan Submission Alert</h4>
<TABLE id=Table1 style="MARGIN: 0px" cellSpacing=0 cellPadding=0 width="100%" border=0>
  <tr>
    <td>
        <div style="padding-top: 10px; padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
            <div style="color:Red;text-align:left;font-weight:bold">
              *A loan file submission with duplicate borrower and property information has already been received.
              The previously submitted file has loan number: <span id="spanForDuplicateLoanLinks"></span>
             </div>
             <br />
             <div>
                <input type="button" onclick="f_close(true);" value="Ignore Alert, Proceed with Rate Lock" style="font-weight:bold" />
                <input type="button" onclick="f_close(false);" value="Close" style="font-weight:bold" />
             </div>
        </div>
    </td>
 </tr>
 </TABLE>
    </form>
</body>
</html>
