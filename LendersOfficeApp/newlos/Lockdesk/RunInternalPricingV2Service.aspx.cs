﻿#region Generated Code -- lie to hide newlos from the merciless StyleCop
namespace LendersOfficeApp.newlos.Lockdesk
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.ObjLib.HistoricalPricing;
    using LendersOffice.RatePrice;
    using LendersOffice.Security;
    using LendersOffice.RatePrice.Model;

    /// <summary>
    /// The service page for RunInternalPricingV2.
    /// </summary>
    public partial class RunInternalPricingV2ServiceItem : AbstractBackgroundServiceItem
    {
        /// <summary>
        /// Routes incoming methods to their implementations.
        /// </summary>
        /// <param name="methodName">The method.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(this.GetNumberOfLoanPrograms):
                    this.GetNumberOfLoanPrograms();
                    break;
                case nameof(this.SubmitInternalPricingRequest):
                    this.SubmitInternalPricingRequest();
                    break;
                case nameof(this.IsResultReady):
                    this.IsResultReady();
                    break;
                default:
                    // Custom StyleCop rules require an exception to be thrown here.
                    throw new CBaseException(ErrorMessages.Generic, "Unhandled method");
            }
        }

        /// <summary>
        /// Binds the data from the request to the loan file.
        /// </summary>
        /// <param name="dataLoan">The loan file.</param>
        /// <param name="dataApp">The application fo the loan file.</param>
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            if (!this.GetBool("IsCheckEligibility"))
            {
                bool frontEndPricing = this.GetString("PricingOption") == "FrontEnd";
                E_sPricingModeT mode = this.GetPricingMode(frontEndPricing, checkEligibility: false);
                dataLoan.sPricingModeT = mode;
                dataLoan.sInvestorLockLpePriceGroupId = this.GetGuid("sInvestorLockLpePriceGroupId");

                BindFilters(dataLoan);
                BindPricingOptions(dataLoan);
            }
        }

        /// <summary>
        /// Gets a loan instance for use in this class.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <returns>The loan instance.</returns>
        protected override CPageData ConstructPageDataClass(Guid loanId)
        {
            return CPageData.CreateUsingSmartDependency(loanId, typeof(RunInternalPricingV2ServiceItem));
        }

        /// <summary>
        /// Sends the data to the client.
        /// </summary>
        /// <param name="dataLoan">The loan file.</param>
        /// <param name="dataApp">The application of the loan file.</param>
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            // Filters
            this.SetResult(nameof(CPageData.sProdFilterRestrictResultToRegisteredProgram), dataLoan.sProdFilterRestrictResultToRegisteredProgram);
            this.SetResult(nameof(CPageData.sProdFilterRestrictResultToCurrentRegistered), dataLoan.sProdFilterRestrictResultToCurrentRegistered);
            this.SetResult(nameof(CPageData.sProdFilterRestrictResultToCurrentInvestor), dataLoan.sProdFilterRestrictResultToCurrentInvestor);
            this.SetResult(nameof(CPageData.sProdFilterMatchCurrentTerm), dataLoan.sProdFilterMatchCurrentTerm);
            this.SetResult(nameof(CPageData.sProdFilterDisplayUsingCurrentNoteRate), dataLoan.sProdFilterDisplayUsingCurrentNoteRate);
            this.SetResult(nameof(CPageData.sProdFilterDisplayrateMerge), dataLoan.sProdFilterDisplayrateMerge);
            this.SetResult(nameof(CPageData.sProdFilterDue10Yrs), dataLoan.sProdFilterDue10Yrs);
            this.SetResult(nameof(CPageData.sProdFilterDue15Yrs), dataLoan.sProdFilterDue15Yrs);
            this.SetResult(nameof(CPageData.sProdFilterDue20Yrs), dataLoan.sProdFilterDue20Yrs);
            this.SetResult(nameof(CPageData.sProdFilterDue25Yrs), dataLoan.sProdFilterDue25Yrs);
            this.SetResult(nameof(CPageData.sProdFilterDue30Yrs), dataLoan.sProdFilterDue30Yrs);
            this.SetResult(nameof(CPageData.sProdFilterDueOther), dataLoan.sProdFilterDueOther);
            this.SetResult(nameof(CPageData.sProdFilterFinMethFixed), dataLoan.sProdFilterFinMethFixed);
            this.SetResult(nameof(CPageData.sProdFilterFinMeth3YrsArm), dataLoan.sProdFilterFinMeth3YrsArm);
            this.SetResult(nameof(CPageData.sProdFilterFinMeth5YrsArm), dataLoan.sProdFilterFinMeth5YrsArm);
            this.SetResult(nameof(CPageData.sProdFilterFinMeth7YrsArm), dataLoan.sProdFilterFinMeth7YrsArm);
            this.SetResult(nameof(CPageData.sProdFilterFinMeth10YrsArm), dataLoan.sProdFilterFinMeth10YrsArm);
            this.SetResult(nameof(CPageData.sProdFilterFinMethOther), dataLoan.sProdFilterFinMethOther);
            this.SetResult(nameof(CPageData.sProdIncludeNormalProc), dataLoan.sProdIncludeNormalProc);
            this.SetResult(nameof(CPageData.sProdIncludeMyCommunityProc), dataLoan.sProdIncludeMyCommunityProc);
            this.SetResult(nameof(CPageData.sProdIncludeHomePossibleProc), dataLoan.sProdIncludeHomePossibleProc);
            this.SetResult(nameof(CPageData.sProdIncludeFHATotalProc), dataLoan.sProdIncludeFHATotalProc);
            this.SetResult(nameof(CPageData.sProdIncludeVAProc), dataLoan.sProdIncludeVAProc);
            this.SetResult(nameof(CPageData.sProdIncludeUSDARuralProc), dataLoan.sProdIncludeUSDARuralProc);
            this.SetResult(nameof(CPageData.sProdFilterPmtTPI), dataLoan.sProdFilterPmtTPI);
            this.SetResult(nameof(CPageData.sProdFilterPmtTIOnly), dataLoan.sProdFilterPmtTIOnly);
            ProductCodeFilterPopup.LoadData(this, dataLoan, PrincipalFactory.CurrentPrincipal);

            // Pricing Options
            this.SetResult(nameof(CPageData.sProdRLckdDays), dataLoan.sProdRLckdDays_rep);
            this.SetResult("sProdInvestorRLckdDays", dataLoan.sProdInvestorRLckdDays_rep);
            this.SetResult("sProdInvestorRLckdExpiredD", dataLoan.sProdInvestorRLckdExpiredD_rep);
            this.SetResult(nameof(CPageData.sInvestorLockLpePriceGroupId), dataLoan.sInvestorLockLpePriceGroupId);
            this.SetResult(nameof(CPageData.sProdInvestorRLckdModeT), dataLoan.sProdInvestorRLckdModeT);
        }

        /// <summary>
        /// Binds the filter data from the request to the loan.
        /// </summary>
        /// <param name="dataLoan">The loan file.</param>
        private void BindFilters(CPageData dataLoan)
        {
            dataLoan.sProdFilterRestrictResultToRegisteredProgram = this.GetBool(nameof(CPageData.sProdFilterRestrictResultToRegisteredProgram));
            dataLoan.sProdFilterRestrictResultToCurrentRegistered = this.GetBool(nameof(CPageData.sProdFilterRestrictResultToCurrentRegistered));
            dataLoan.sProdFilterRestrictResultToCurrentInvestor = this.GetBool(nameof(CPageData.sProdFilterRestrictResultToCurrentInvestor));
            dataLoan.sProdFilterMatchCurrentTerm = this.GetBool(nameof(CPageData.sProdFilterMatchCurrentTerm));
            dataLoan.sProdFilterDisplayUsingCurrentNoteRate = this.GetBool(nameof(CPageData.sProdFilterDisplayUsingCurrentNoteRate));
            dataLoan.sProdFilterDisplayrateMerge = this.GetBool(nameof(CPageData.sProdFilterDisplayrateMerge));

            dataLoan.sProdFilterDue10Yrs = this.GetBool(nameof(CPageData.sProdFilterDue10Yrs));
            dataLoan.sProdFilterDue15Yrs = this.GetBool(nameof(CPageData.sProdFilterDue15Yrs));
            dataLoan.sProdFilterDue20Yrs = this.GetBool(nameof(CPageData.sProdFilterDue20Yrs));
            dataLoan.sProdFilterDue25Yrs = this.GetBool(nameof(CPageData.sProdFilterDue25Yrs));
            dataLoan.sProdFilterDue30Yrs = this.GetBool(nameof(CPageData.sProdFilterDue30Yrs));
            dataLoan.sProdFilterDueOther = this.GetBool(nameof(CPageData.sProdFilterDueOther));
            dataLoan.sProdFilterFinMethFixed = this.GetBool(nameof(CPageData.sProdFilterFinMethFixed));
            dataLoan.sProdFilterFinMeth3YrsArm = this.GetBool(nameof(CPageData.sProdFilterFinMeth3YrsArm));
            dataLoan.sProdFilterFinMeth5YrsArm = this.GetBool(nameof(CPageData.sProdFilterFinMeth5YrsArm));
            dataLoan.sProdFilterFinMeth7YrsArm = this.GetBool(nameof(CPageData.sProdFilterFinMeth7YrsArm));
            dataLoan.sProdFilterFinMeth10YrsArm = this.GetBool(nameof(CPageData.sProdFilterFinMeth10YrsArm));
            dataLoan.sProdFilterFinMethOther = this.GetBool(nameof(CPageData.sProdFilterFinMethOther));
            dataLoan.sProdIncludeNormalProc = this.GetBool(nameof(CPageData.sProdIncludeNormalProc));
            dataLoan.sProdIncludeMyCommunityProc = this.GetBool(nameof(CPageData.sProdIncludeMyCommunityProc));
            dataLoan.sProdIncludeHomePossibleProc = this.GetBool(nameof(CPageData.sProdIncludeHomePossibleProc));
            dataLoan.sProdIncludeFHATotalProc = this.GetBool(nameof(CPageData.sProdIncludeFHATotalProc));
            dataLoan.sProdIncludeVAProc = this.GetBool(nameof(CPageData.sProdIncludeVAProc));
            dataLoan.sProdIncludeUSDARuralProc = this.GetBool(nameof(CPageData.sProdIncludeUSDARuralProc));
            dataLoan.sProdFilterPmtTPI = this.GetBool(nameof(CPageData.sProdFilterPmtTPI));
            dataLoan.sProdFilterPmtTIOnly = this.GetBool(nameof(CPageData.sProdFilterPmtTIOnly));
            ProductCodeFilterPopup.BindData(this, dataLoan, PrincipalFactory.CurrentPrincipal);
        }

        /// <summary>
        /// Binds the pricing options from the request to the loan.
        /// </summary>
        /// <param name="dataLoan">The loan file.</param>
        private void BindPricingOptions(CPageData dataLoan)
        {
            dataLoan.sProdRLckdDays_rep = this.GetString(nameof(CPageData.sProdRLckdDays));
            dataLoan.sProdInvestorRLckdDays_rep = this.GetString("sProdInvestorRLckdDays");
            dataLoan.sProdInvestorRLckdExpiredD_rep = this.GetString("sProdInvestorRLckdExpiredD");
            dataLoan.sInvestorLockLpePriceGroupId = this.GetGuid(nameof(CPageData.sInvestorLockLpePriceGroupId));
            dataLoan.sProdInvestorRLckdModeT = (E_sProdInvestorRLckdModeT)this.GetInt(nameof(CPageData.sProdInvestorRLckdModeT));
        }

        /// <summary>
        /// Retrieves the number of loan programs that will be evaluated.
        /// </summary>
        private void GetNumberOfLoanPrograms()
        {
            bool frontEndPricing = this.GetString("PricingOption") == "FrontEnd";

            // We do not display the number of loan programs for Check Eligibility mode.
            E_sPricingModeT mode = this.GetPricingMode(frontEndPricing, checkEligibility: false);

            var dataLoan = CPageData.CreateUsingSmartDependency(this.sLId, typeof(RunInternalPricingV2ServiceItem));
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();
            dataLoan.sPricingModeT = mode;
            dataLoan.sInvestorLockLpePriceGroupId = this.GetGuid("sInvestorLockLpePriceGroupId");
            this.BindFilters(dataLoan);

            try
            {
                AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
                var loanPrograms = dataLoan.GetLoanProgramsToRunForHistoricalPricing(principal);
                this.SetResult("NumLPs", loanPrograms.LoanProgramsCount);
                ProductCodeFilterPopup.LoadData(this, dataLoan, principal);
            }
            catch (CLPERunModeException e)
            {
                this.SetResult("ErrorMessage", e.UserMessage);
                return;
            }
        }

        /// <summary>
        /// Submits the internal pricing request and sends back the request id.
        /// </summary>
        private void SubmitInternalPricingRequest()
        {
            bool frontEndPricing = this.GetString("PricingOption") == "FrontEnd";
            bool checkEligibility = this.GetBool("IsCheckEligibility");
            E_sPricingModeT mode = this.GetPricingMode(frontEndPricing, checkEligibility);
            var getResultsUsing = PricingResultsType.Current;
            var principal = PrincipalFactory.CurrentPrincipal;
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);

            if (principal.UseHistoricalPricingAndUiEnhancements)
            {
                getResultsUsing = this.GetEnum<PricingResultsType>("GetResultsUsing");

                if (getResultsUsing == PricingResultsType.Historical || getResultsUsing == PricingResultsType.WorstCase)
                {
                    var snapshot = SerializationHelper.JsonNetDeserialize<SubmissionSnapshot>(this.GetString("SnapshotJson"));
                    var lenderSettings = principal.BrokerDB.HistoricalPricingSettings;
                    options = HybridLoanProgramSetOption.CreateHistoricalPricingOption(lenderSettings, snapshot);
                }
            }

            var requestId = DistributeUnderwritingEngine.RequestInternalPricingResult(
                    principal,
                    this.sLId,
                    mode,
                    options);
            var name = getResultsUsing == PricingResultsType.Current
                ? "CurrentRequestId"
                : "HistoricalRequestId";
            this.SetResult(name, requestId);

            if (getResultsUsing == PricingResultsType.WorstCase)
            {
                var currentRequestId = DistributeUnderwritingEngine.RequestInternalPricingResult(
                        principal,
                        this.sLId,
                        mode,
                        HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB));

                this.SetResult("CurrentRequestId", currentRequestId);
            }
        }

        /// <summary>
        /// Gets the result using the request Id, and serializes it for the UI.
        /// </summary>
        /// <param name="currentRequestId">
        /// The request Id for the current pricing result. This should have a
        /// value when running Current or Worst-Case pricing.
        /// </param>
        /// <param name="historicalRequestId">
        /// The request Id for the historical pricing result. This should have a
        /// value when running Historical or Worst-Case pricing.
        /// </param>
        private void GetResults(Guid currentRequestId, Guid historicalRequestId)
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            bool frontEndPricing = this.GetString("PricingOption") == "FrontEnd";
            bool checkEligibility = this.GetBool("IsCheckEligibility");

            // Default to false for Check Eligibility, which does not display the filters.
            bool prodFilterDisplayrateMerge = this.GetBool("sProdFilterDisplayrateMerge", defaultValue: false);

            E_sPricingModeT mode = this.GetPricingMode(frontEndPricing, checkEligibility);
            SetResult("pricingMode", mode);

            var getResultsUsing = PricingResultsType.Current;

            if (principal.UseHistoricalPricingAndUiEnhancements)
            {
                getResultsUsing = this.GetEnum<PricingResultsType>("GetResultsUsing");
            }

            UnderwritingResultItem resultItem = null;
            UnderwritingResultItem historicalResultItem = null;

            if (currentRequestId != Guid.Empty)
            {
                resultItem = LpeDistributeResults.RetrieveResultItem(currentRequestId);

                if (resultItem == null)
                {
                    throw new CBaseException(ErrorMessages.Generic, "UnderwritingResultItem is null");
                }

                if (resultItem.IsErrorMessage)
                {
                    throw new CBaseException(resultItem.ErrorMessage, resultItem.ErrorMessage);
                }
            }

            if (historicalRequestId != Guid.Empty)
            {
                historicalResultItem = LpeDistributeResults.RetrieveResultItem(historicalRequestId);
                if (historicalResultItem == null)
                {
                    throw new CBaseException(ErrorMessages.Generic, "UnderwritingResultItem is null");
                }

                if (historicalResultItem.IsErrorMessage)
                {
                    throw new CBaseException(historicalResultItem.ErrorMessage, historicalResultItem.ErrorMessage);
                }
            }

            Guid loanId = GetGuid("LoanID");
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(InternalPricingResultService));
            dataLoan.InitLoad();
            dataLoan.sPricingModeT = mode;

            var resultFormatter = ResultPricingFormatter.GetFormatter(
                dataLoan.BrokerDB,
                principal,
                dataLoan.sPricingModeT,
                dataLoan.sOriginatorCompensationPaymentSourceT,
                dataLoan.sOriginatorCompensationLenderFeeOptionT);

            ResultPricing result = null;

            switch (getResultsUsing)
            {
                case PricingResultsType.Current:
                    result = prodFilterDisplayrateMerge ? resultFormatter.GetMergedSingleResult(resultItem) : resultFormatter.GetSingleResult(resultItem);
                    break;
                case PricingResultsType.Historical:
                    result = prodFilterDisplayrateMerge ? resultFormatter.GetMergedSingleResult(historicalResultItem) : resultFormatter.GetSingleResult(historicalResultItem);
                    break;
                case PricingResultsType.WorstCase:
                    result = resultFormatter.GetCombinedResult(resultItem, historicalResultItem);
                    break;
                default:
                    throw new UnhandledEnumException(getResultsUsing);
            }

            SetResult("PricingResults", ObsoleteSerializationHelper.JsonSerialize(result));
        }

        /// <summary>
        /// Gets the pricing mode to use.
        /// </summary>
        /// <param name="frontEndPricing">A value indicating whether it is for front-end pricing.</param>
        /// <param name="checkEligibility">A value indicating whether it is for the check eligibility page.</param>
        /// <returns>The pricing mode.</returns>        
        private E_sPricingModeT GetPricingMode(bool frontEndPricing, bool checkEligibility)
        {
            E_sPricingModeT mode = E_sPricingModeT.Undefined;

            if (checkEligibility)
            {
                mode = frontEndPricing
                    ? E_sPricingModeT.EligibilityBrokerPricing
                    : E_sPricingModeT.EligibilityInvestorPricing;
            }
            else
            {
                mode = frontEndPricing
                    ? E_sPricingModeT.InternalBrokerPricing
                    : E_sPricingModeT.InternalInvestorPricing;
            }

            return mode;
        }

        /// <summary>
        /// Gets a value indicating whether the pricing result is ready.
        /// </summary>
        private void IsResultReady()
        {
            Guid currentRequestId = GetGuid("CurrentRequestId", defaultValue: Guid.Empty);
            Guid historicalRequestId = GetGuid("HistoricalRequestId", defaultValue: Guid.Empty);

            bool isReady = (currentRequestId == Guid.Empty || LpeDistributeResults.IsResultAvailable(currentRequestId))
                && (historicalRequestId == Guid.Empty || LpeDistributeResults.IsResultAvailable(historicalRequestId));

            SetResult("IsReady", isReady);

            if (isReady)
            {
                GetResults(currentRequestId, historicalRequestId);
            }
        }
    }

    /// <summary>
    /// The service page for the second version of the internal pricer.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Using the AbstractBackgroundServiceItem approach.")]
    public partial class RunInternalPricingV2Service : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Adds the background service item.
        /// </summary>
        protected override void Initialize()
        {
            this.AddBackgroundItem(string.Empty, new RunInternalPricingV2ServiceItem());
        }
    }
}