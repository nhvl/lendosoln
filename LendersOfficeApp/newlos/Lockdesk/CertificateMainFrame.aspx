﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CertificateMainFrame.aspx.cs" Inherits="LendersOfficeApp.newlos.Lockdesk.CertificateMainFrame" %>
<%@ Import Namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="LendersOffice.AntiXss" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>

<script type="text/javascript" src=<%=AspxTools.SafeUrl(DataAccess.Tools.VRoot + "/common/ModalDlg/CModalDlg.js")%> ></script>
<script type="text/javascript">
    var VRoot = <%=AspxTools.JsString(DataAccess.Tools.VRoot)%>;
	function openEmail()
	{
        var sLId = <%= AspxTools.JsString(RequestHelper.LoanID) %>;
        var productId = <%= AspxTools.JsString(RequestHelper.GetGuid("productid")) %>;
        var lienqualifymodet = escape(<%= AspxTools.JsString(RequestHelper.GetSafeQueryString("lienqualifymodet")) %>);
        var version = escape(<%= AspxTools.JsString(RequestHelper.GetSafeQueryString("version")) %>);
        var uniquechecksum = escape(<%= AspxTools.JsString(RequestHelper.GetSafeQueryString("uniquechecksum")) %>);
        var sPricingModeT = escape(<%= AspxTools.JsString(RequestHelper.GetInt("sPricingModeT")) %>);
        var historicalPolicy = escape(<%= AspxTools.JsString(RequestHelper.GetInt("historicalPolicy", 0)) %>);
        var historicalRateOption = escape(<%= AspxTools.JsString(RequestHelper.GetInt("historicalRateOption", 0)) %>);
        var rate = escape(<%= AspxTools.JsString(RequestHelper.GetSafeQueryString("rate")) %>);
        <%--
        //since the frmBody destination url doesn't have LQBPopup due to being an XSLT document, we have to
        //implement our own.
        //It would probably be a better idea to re-write this page to make it capable of showing an LQB Popup, 
        //but that would require re-doing the frameset, since frameset replaces a <body> tag.
        --%>
        
	    var emailUrl = 'EmailCertificate.aspx?loanid=' + sLId + '&productid=' + productId + '&lienqualifymodet=' + lienqualifymodet + '&version=' + version + '&uniquechecksum=' + uniquechecksum + '&sPricingModeT=' + sPricingModeT + '&historicalPolicy=' + historicalPolicy + '&historicalRateOption=' + historicalRateOption + '&rate=' + rate;
        $('#frmPopup').attr('src', emailUrl);
        $('#Main').attr('rows', '0, 0, *');
	}
	
	function closeEmail()
	{
        $('#Main').attr('rows', '50, *');	    
        $('#frmPopup').removeAttr('src');
	}

    function showNormalCert() {

        <%if(  this.IsDisplayDetailCert ) {%>
        showModal(<%= AspxTools.SafeUrl("/newlos/lockdesk/" + m_frmBodyUrl + "&detailCert=t")%>, null, 'dialogWidth:780px;dialogHeight:600px;center:yes;resizable:yes;scroll:yes;status=yes;help=no;');
        <%}%>
	}

</script>
<body class="body-iframe">
    <form runat="server">
        <div class="top">
            <iframe name="frmMenu" src=<%= AspxTools.SafeUrl(m_frmMenuUrl) %> scrolling="no"></iframe>
        </div>
        <div class="bottom bottom-certificate-main-frame">
            <iframe name="frmBody" src=<%= AspxTools.SafeUrl(m_frmBodyUrl) %>></iframe>
        </div>
        <iframe id='frmPopup' name='frmPopup'></iframe>
    </form>
</body>
</html>
