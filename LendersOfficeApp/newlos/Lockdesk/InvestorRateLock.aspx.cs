﻿namespace LendersOfficeApp.newlos.Lockdesk
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Rolodex;
    using LendersOffice.Security;

    public partial class InvestorRateLock : BaseLoanPage
    {

        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowLockDeskWrite };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowLockDeskRead };
            }
        }

        protected bool UseInternalPricerV2
        {
            get
            {
                return this.BrokerUser.UseInternalPricerV2;
            }
        }

        protected string GetInternalPricerUrl()
        {
            var baseUrl = this.UseInternalPricerV2
                ? "RunInternalPricingV2.aspx"
                : "RunInternalPricing.aspx";

            return baseUrl + $"?sPricingModeT={E_sPricingModeT.InternalInvestorPricing.ToString("D")}";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            EnableJqueryMigrate = false;
            PageID = "InvestorRateLock";

            mVRoot.Value = DataAccess.Tools.VRoot;
            LoanId.Value = LoanID.ToString();

            RegisterService("loanedit", "/newlos/LockDesk/InvestorRateLockService.aspx");
            RegisterService("BrokerRateLock", "/newlos/LockDesk/BrokerRateLockService.aspx");
            this.RegisterJsScript("LQBPopup.js");	
        }

        public void BindInvestorNames(int selected)
        {
            List<InvestorRolodexEntry> entries = InvestorRolodexEntry.GetAll(PrincipalFactory.CurrentPrincipal.BrokerId, null).ToList();
            sInvestorRolodexId.DataSource = entries.Where(p => (p.Status == E_InvestorStatus.Active || p.Id == selected) && (p.InvestorRolodexType == E_InvestorRolodexType.Investor));
            sInvestorRolodexId.DataTextField = "InvestorName";
            sInvestorRolodexId.DataValueField = "Id";
            sInvestorRolodexId.DataBind();

            sInvestorRolodexId.Items.Insert(0, new ListItem("<-- Select an Investor -->", "-1"));
            Tools.SetDropDownListValue(sInvestorRolodexId, selected);
        }

        protected override void LoadData()
        {
            CPageData data = CPageData.CreateUsingSmartDependency(LoanID, typeof(InvestorRateLock));
            data.InitLoad();

            if (data.sCreatedD.IsValid)
            {
                // 6/13/2014 dd - Turn on obsolete mode for loan crate before 6/20/2014.
                OldInvestorNameRow.Visible = data.sCreatedD.DateTimeForComputation < new DateTime(2014, 6, 20);
            }

            IsPageReadOnly.Value = IsReadOnly.ToString();
            BindInvestorNames(data.sInvestorRolodexId);
            sInvestorLockLpInvestorNm.Text = data.sInvestorLockLpInvestorNm;
            sInvestorLockLpInvestorNm.ReadOnly = true;
            sInvestorLockRateLockStatusT.Text = data.sInvestorLockRateLockStatusT_rep;
            sInvestorLockRLckdDays.Text = data.sInvestorLockRLckdDays_rep;
            sInvestorLockRLckdD.Text = data.sInvestorLockRLckdD_rep;
            sInvestorLockRLckExpiredD.Text = data.sInvestorLockRLckExpiredD_rep;
            sInvestorLockRLckExpiredDLckd.Checked = data.sInvestorLockRLckExpiredDLckd;
            sInvestorLockRLckExpiredD.Text = data.sInvestorLockRLckExpiredD_rep;

            sInvestorLockDeliveryExpiredDLckd.Checked = data.sInvestorLockDeliveryExpiredDLckd;
            sInvestorLockDeliveryExpiredD.Text = data.sInvestorLockDeliveryExpiredD_rep;

            sInvestorLockLoanNum.Text = data.sInvestorLockLoanNum;
            sInvestorLockProgramId.Text = data.sInvestorLockProgramId;
            sInvestorLockConfNum.Text = data.sInvestorLockConfNum;
            sInvestorLockRateSheetID.Text = data.sInvestorLockRateSheetID;
            sInvestorLockRateSheetEffectiveTime.Text = data.sInvestorLockRateSheetEffectiveTime;
            sInvestorLockLockFee.Text = data.sInvestorLockLockFee_rep;
            sInvestorLockLpTemplateNm.Text = data.sInvestorLockLpTemplateNm;

            sInvestorLockBaseNoteIR.Text = data.sInvestorLockBaseNoteIR_rep;
            sInvestorLockBaseBrokComp1PcPrice.Text = data.sInvestorLockBaseBrokComp1PcPrice;
            sInvestorLockBaseBrokComp1PcFee.Text = data.sInvestorLockBaseBrokComp1PcFee_rep;
            sInvestorLockBaseRAdjMarginR.Text = data.sInvestorLockBaseRAdjMarginR_rep;
            sInvestorLockBaseOptionArmTeaserR.Text = data.sInvestorLockBaseOptionArmTeaserR_rep;


            sInvestorLockTotAdjNoteIR.Text = data.sInvestorLockTotAdjNoteIR_rep;
            sInvestorLockTotAdjBrokComp1PcPrice.Text = data.sInvestorLockTotAdjBrokComp1PcPrice;
            sInvestorLockTotAdjBrokComp1PcFee.Text = data.sInvestorLockTotAdjBrokComp1PcFee_rep;
            sInvestorLockTotAdjRAdjMarginR.Text = data.sInvestorLockTotAdjRAdjMarginR_rep;
            sInvestorLockTotAdjOptionArmTeaserR.Text = data.sInvestorLockTotAdjOptionArmTeaserR_rep;

            sInvestorLockProjectedProfit.Text = data.sInvestorLockProjectedProfit_rep;
            sInvestorLockProjectedProfitAmt.Text = data.sInvestorLockProjectedProfitAmt_rep;

            sInvestorLockNoteIR.Text = data.sInvestorLockNoteIR_rep;

            sInvestorLockNoteIRQS.Text = data.sInvestorLockNoteIR_rep;
            InvestorRateLockPricingP.Text = data.sInvestorLockBrokComp1PcPrice_rep;

            sInvestorLockBrokComp1PcPrice.Text = data.sInvestorLockBrokComp1PcPrice_rep;
            sInvestorLockBrokComp1Pc.Text = data.sInvestorLockBrokComp1Pc_rep;
            sInvestorLockRAdjMarginR.Text = data.sInvestorLockRAdjMarginR_rep;
            sInvestorLockOptionArmTeaserR.Text = data.sInvestorLockOptionArmTeaserR_rep;

            sTrNotes.Text = data.sTrNotes;
            sNoteIR.Text = data.sNoteIR_rep;
            sBrokerLockFinalBrokComp1PcPrice.Text = data.sBrokerLockFinalBrokComp1PcPrice_rep;

            sU1LockFieldDesc.Value = data.sU1LockFieldDesc;
            sU1LockFieldD.Text = data.sU1LockFieldD_rep;
            sU1LockFieldAmt.Value = data.sU1LockFieldAmt_rep;
            sU1LockFieldPc.Value = data.sU1LockFieldPc_rep;
            sU2LockFieldDesc.Value = data.sU2LockFieldDesc;
            sU2LockFieldD.Text = data.sU2LockFieldD_rep;
            sU2LockFieldAmt.Value = data.sU2LockFieldAmt_rep;
            sU2LockFieldPc.Value = data.sU2LockFieldPc_rep;

            if(!FieldsEmpty(new[]{
                data.sU1LockFieldDesc,
                data.sU1LockFieldD_rep,
                data.sU1LockFieldAmt_rep,
                data.sU1LockFieldPc_rep,
                data.sU2LockFieldDesc,
                data.sU2LockFieldD_rep,
                data.sU2LockFieldAmt_rep,
                data.sU2LockFieldPc_rep
            }))
            {
                ClientScript.RegisterHiddenField("ShowCustomFields", "true");
            }


            sInvestorLockCheckBrokerExtendByDefault.Value = data.sInvestorLockCheckBrokerExtendByDefault.ToString();
            RateLockHistoryData.Text = RateLockHistoryTable.BuildInvestorRateLockHistoryTable(data.sInvestorLockHistoryXmlContent);

            sBECurrentLockExtensionCount.Text = "" + data.sBECurrentLockExtensionCount;
            sBETotalLockExtensionCount.Text = "" + data.sBETotalLockExtensionCount;
            sBETotalRateReLockCount.Text = "" + data.sBETotalRateReLockCount;

            sBECurrentLockExtensionCountLckd.Checked = data.sBECurrentLockExtensionCountLckd;
            sBETotalLockExtensionCountLckd.Checked = data.sBETotalLockExtensionCountLckd;
            sBETotalRateReLockCountLckd.Checked = data.sBETotalRateReLockCountLckd;

            switch (data.sInvestorLockPriceRowLockedT)
            {
                case E_sInvestorLockPriceRowLockedT.Base:
                    sInvestorLockPriceRowLockedT_Base.Checked = true;
                    break;
                case E_sInvestorLockPriceRowLockedT.Adjusted:
                    sInvestorLockPriceRowLockedT_Adjusted.Checked = true;
                    break;
                default:
                    throw new UnhandledEnumException(data.sInvestorLockPriceRowLockedT);
            }


            Tools.Bind_sInvestorLockCommitmentT(sInvestorLockCommitmentT);
            Tools.SetDropDownListValue(sInvestorLockCommitmentT, data.sInvestorLockCommitmentT);


            CanWrite.Value = BrokerUser.HasPermission(Permission.AllowLockDeskWrite).ToString();

            RegisterJsObject("PricingAdjustments", data.sInvestorLockAdjustments);
            RegisterJsScript("utilities.js");
            RegisterJsScript("json.js");
            RegisterJsScript("mask.js");
            RegisterJsScript("AdjustmentTable.js");
            RegisterJsScript("InvestorRateLock.js");
            RegisterJsScript("ModelessDlg.js");
        }

        private bool FieldsEmpty(IEnumerable<string> check)
        {
            return check.All(a => string.IsNullOrEmpty(a) || a.Equals("$0.00") || a.Equals("0.000%"));
        }

        protected override void OnPreRender(EventArgs e)
        {

            ClientScript.RegisterClientScriptBlock(typeof(InvestorRateLock), "PageFields", "var PageFields = " + ObsoleteSerializationHelper.JavascriptJsonSerialize(GetClientIDs()) + ";", true);
            base.OnPreRender(e);
        }

        public void AddControl(Control control, Dictionary<string, string> ids)
        {
            if (!string.IsNullOrEmpty(control.ID))
            {
                ids.Add(control.ID, control.ClientID);
            }

            if (control.HasControls())
            {
                foreach (Control childControl in control.Controls)
                {
                    AddControl(childControl, ids);
                }
            }
        }

        public Dictionary<string, string> GetClientIDs()
        {
            Dictionary<string, string> fields = new Dictionary<string, string>();

            foreach (Control control in this.Controls)
            {
                AddControl(control, fields);
            }

            return fields;
        }
    }
}
