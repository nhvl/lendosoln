﻿namespace LendersOfficeApp.newlos.Lockdesk
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;

    public partial class InvestorRateLockService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override bool PerformResultOptimization
        {
            get
            {
                return false;
            }
        }
        protected BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private CPageData CreatePageData()
        {
            var loanID = GetGuid("loanid");
            return CPageData.CreateUsingSmartDependency(loanID, typeof(InvestorRateLockService));
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "PopulateCalcRefresh":
                    CalculatePricingInfoFromPopulate();
                    break;
                case "Save":
                    Save();
                    break;
                case "PricingUpdate":
                    InvestorPagePricingUpdate();
                    break;
                case "PriceFeeConversion":
                    PriceFeeConversion();
                    break;
                case "LockRate":
                    LockRate();
                    break;
                case "BreakRateLock":
                    BreakInvestorRateLock();
                    break;
                case "ExtendRateLock":
                    ExtendRateLock();
                    break;
                case "ExtendAdjustmentPricing":
                    ExtendRateLockForPricing();
                    break;
                case "PreviewResumeLock":
                    PreviewResumeLock();
                    break;
                case "PreviewRateLockExtendDates":
                    PreviewRateLockExtendDates();
                    break;
                case "ResumeLock":
                    ResumeLock();
                    break;
                case "SuspendLock":
                    SuspendInvestorRateLock();
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, methodName + " does not exist in service page.");

            }
        }

        private void PreviewResumeLock()
        {
            int sFileVersion = GetInt("sFileVersion");
            CPageData data = CreatePageData();
            data.InitLoad();
            this.GetInvestorLockDates(data);
            this.SetInvestorLockDates(data);
            SetResult("sFileVersion", sFileVersion);
        }

        private void PreviewRateLockExtendDates()
        {
            CPageData data = CreatePageData();
            data.InitLoad();

            string newExpirationDate = GetBool("sInvestorLockRLckExpiredDLckd") ? GetString("sInvestorLockRLckExpiredD") : null;
            string newDeliveryDate = GetBool("sInvestorLockDeliveryExpiredDLckd") ? GetString("sInvestorLockDeliveryExpiredD") : null;

            data.ExtendInvestorRateLock(BrokerUser.LoginNm, "", GetInt("ExtendPeriod"), newExpirationDate, newDeliveryDate, null, "$0.00");

            SetResult("sInvestorLockDeliveryExpiredD", data.sInvestorLockDeliveryExpiredD_rep);
            SetResult("sInvestorLockRLckExpiredD", data.sInvestorLockRLckExpiredD_rep);
            SetResult("sInvestorLockRLckExpiredDLckd", data.sInvestorLockRLckExpiredDLckd);
            SetResult("sInvestorLockDeliveryExpiredDLckd", data.sInvestorLockDeliveryExpiredDLckd);

        }


        private void ExtendRateLockForPricing()
        {
            CPageData data = CreatePageData();
            data.InitLoad();

            PricingAdjustment adjustment = new PricingAdjustment()
            {
                Fee = GetString("adjFee"),
                Price = GetString("adjPrice"),
                Rate = GetString("adjRate"),
                Margin = GetString("adjMargin"),
                TeaserRate = GetString("adjTRate"),
                IsPersist = true // OPM 460584 - default to true;
            };

            data.ExtendInvestorRateLockForPricing(adjustment);

            SetResult("sInvestorLockNoteIR", data.sInvestorLockNoteIR_rep);
            SetResult("sInvestorLockBrokComp1PcPrice", data.sInvestorLockBrokComp1PcPrice_rep);
            SetResult("sInvestorLockBrokComp1Pc", data.sInvestorLockBrokComp1Pc_rep);
            SetResult("sInvestorLockRAdjMarginR", data.sInvestorLockRAdjMarginR_rep);
            SetResult("sInvestorLockOptionArmTeaserR", data.sInvestorLockOptionArmTeaserR_rep);
        }

        private void ExtendRateLock()
        {
            CPageData data = CreatePageData();
            data.InitSave(GetInt("sFileVersion"));

            PricingAdjustment adjustment = null;

            if (GetBool("AddAdjustment"))
            {
                adjustment = new PricingAdjustment();
                adjustment.Description = GetString("adjDesc");
                adjustment.Fee = GetString("adjFee");
                adjustment.Price = GetString("adjPrice");
                adjustment.Rate = GetString("adjRate");
                adjustment.Margin = GetString("adjMargin");
                adjustment.TeaserRate = GetString("adjTRate");
                adjustment.IsPersist = true; // OPM 460584 - Default to true.
            }

            string newExpirationDate = GetBool("newsInvestorLockRLckExpiredDLckd") ? GetString("newsInvestorLockRLckExpiredD") : null;
            string newDeliveryDate = GetBool("newsInvestorLockDeliveryExpiredDLckd") ? GetString("newsInvestorLockDeliveryExpiredD") : null;

            data.ExtendInvestorRateLock(BrokerUser.LoginNm, GetString("Reason"), GetInt("ExtendPeriod"), newExpirationDate, newDeliveryDate, adjustment, GetString("sInvestorLockLockFee"));

            SetResult("sInvestorLockNoteIR", data.sInvestorLockNoteIR_rep);
            SetResult("sInvestorLockBrokComp1PcPrice", data.sInvestorLockBrokComp1PcPrice_rep);
            SetResult("sInvestorLockBrokComp1Pc", data.sInvestorLockBrokComp1Pc_rep);
            SetResult("sInvestorLockRAdjMarginR", data.sInvestorLockRAdjMarginR_rep);
            SetResult("sInvestorLockOptionArmTeaserR", data.sInvestorLockOptionArmTeaserR_rep);

            SetResult("sInvestorLockTotAdjNoteIR", data.sInvestorLockTotAdjNoteIR_rep);
            SetResult("sInvestorLockTotAdjBrokComp1PcPrice", data.sInvestorLockTotAdjBrokComp1PcPrice);
            SetResult("sInvestorLockTotAdjBrokComp1PcFee", data.sInvestorLockTotAdjBrokComp1PcFee_rep);
            SetResult("sInvestorLockTotAdjRAdjMarginR", data.sInvestorLockTotAdjRAdjMarginR_rep);
            SetResult("sInvestorLockTotAdjOptionArmTeaserR", data.sInvestorLockTotAdjOptionArmTeaserR_rep);
            SetResult("sInvestorLockDeliveryExpiredD", data.sInvestorLockDeliveryExpiredD_rep);
            SetResult("sInvestorLockRLckExpiredD", data.sInvestorLockRLckExpiredD_rep);
            SetResult("sInvestorLockRLckExpiredDLckd", data.sInvestorLockRLckExpiredDLckd);
            SetResult("sInvestorLockDeliveryExpiredDLckd", data.sInvestorLockDeliveryExpiredDLckd);
            SetResult("sInvestorLockRLckdDays", data.sInvestorLockRLckdDays);

            SetResult("sInvestorLockLockFee", data.sInvestorLockLockFee);

            data.Save();
            SetResult("sInvestorLockHistoryXmlContent", RateLockHistoryTable.BuildInvestorRateLockHistoryTable(data.sInvestorLockHistoryXmlContent));
            SetResult("sInvestorLockProjectedProfit", data.sInvestorLockProjectedProfit_rep);
            SetResult("sInvestorLockProjectedProfitAmt", data.sInvestorLockProjectedProfitAmt_rep);
            SetResult("sFileVersion", data.sFileVersion);

            SetResult("sBECurrentLockExtensionCount", data.sBECurrentLockExtensionCount);
            SetResult("sBETotalLockExtensionCount", data.sBETotalLockExtensionCount);
            SetResult("sBETotalRateReLockCount", data.sBETotalRateReLockCount);
        }

        private CPageData PartialSave(bool attemptingRateLock = false)
        {
            string adjustmentData = GetString("Adjustments");
            List<PricingAdjustment> adjustments = LendersOffice.Common.ObsoleteSerializationHelper.JsonDeserialize<List<PricingAdjustment>>(adjustmentData);

            CPageData data = CreatePageData();
            data.InitSave(GetInt("sFileVersion"));

            data.sInvestorLockAdjustments = adjustments;
            data.sInvestorRolodexId = GetInt("sInvestorRolodexId");
            //data.sInvestorLockLpInvestorNm = GetString("sInvestorLockLpInvestorNm");
            data.sInvestorLockLpTemplateNm = GetString("sInvestorLockLpTemplateNm");
            data.sInvestorLockRLckdDays_rep = GetString("sInvestorLockRLckdDays");
            data.sInvestorLockLoanNum = GetString("sInvestorLockLoanNum");
            data.sInvestorLockProgramId = GetString("sInvestorLockProgramId");
            data.sInvestorLockConfNum = GetString("sInvestorLockConfNum");
            data.sInvestorLockRateSheetID = GetString("sInvestorLockRateSheetID");
            data.sInvestorLockRateSheetEffectiveTime = GetString("sInvestorLockRateSheetEffectiveTime");
            data.sInvestorLockLockFee_rep = GetString("sInvestorLockLockFee");
            data.sInvestorLockCommitmentT = (E_sInvestorLockCommitmentT)GetInt("sInvestorLockCommitmentT");
            data.sTrNotes = GetString("sTrNotes");
            data.sU1LockFieldDesc = GetString("sU1LockFieldDesc");
            data.sU1LockFieldD_rep = GetString("sU1LockFieldD");
            data.sU1LockFieldAmt_rep = GetString("sU1LockFieldAmt");
            data.sU1LockFieldPc_rep = GetString("sU1LockFieldPc");
            data.sU2LockFieldDesc = GetString("sU2LockFieldDesc");
            data.sU2LockFieldD_rep = GetString("sU2LockFieldD");
            data.sU2LockFieldAmt_rep = GetString("sU2LockFieldAmt");
            data.sU2LockFieldPc_rep = GetString("sU2LockFieldPc");

            data.sBECurrentLockExtensionCountLckd = GetBool("sBECurrentLockExtensionCountLckd");
            data.sBETotalLockExtensionCountLckd = GetBool("sBETotalLockExtensionCountLckd");
            data.sBETotalRateReLockCountLckd = GetBool("sBETotalRateReLockCountLckd");

            data.sBECurrentLockExtensionCount = GetInt("sBECurrentLockExtensionCount");
            data.sBETotalLockExtensionCount = GetInt("sBETotalLockExtensionCount");
            data.sBETotalRateReLockCount = GetInt("sBETotalRateReLockCount");

            string option = GetString("xPricingLock");

            switch (option)
            {
                case "BASE":
                    data.sInvestorLockPriceRowLockedT = E_sInvestorLockPriceRowLockedT.Base;
                    data.LoadInvestorLockData(
                        GetString("sInvestorLockBaseNoteIR"),
                        GetString("sInvestorLockBaseBrokComp1PcFee"),
                        GetString("sInvestorLockBaseRAdjMarginR"),
                        GetString("sInvestorLockBaseOptionArmTeaserR"));
                    break;
                case "ADJUSTED":
                    data.sInvestorLockPriceRowLockedT = E_sInvestorLockPriceRowLockedT.Adjusted;
                    data.LoadInvestorLockData(
                        GetString("sInvestorLockNoteIR"),
                        GetString("sInvestorLockBrokComp1Pc"),
                        GetString("sInvestorLockRAdjMarginR"),
                        GetString("sInvestorLockOptionArmTeaserR"));
                    break;

                default:
                    throw new CBaseException(ErrorMessages.Generic, "Invalid Pricing Row Lock In background save.");
            }

            return data;

        }

        private void Save()
        {
            CPageData data = PartialSave();
            data.Save();
            SetResult("sFileVersion", data.sFileVersion);
        }

        private void GetInvestorLockDates(CPageData data)
        {
            data.sInvestorLockRLckdD_rep = GetString("sInvestorLockRLckdD");
            data.sInvestorLockRLckdDays_rep = GetString("sInvestorLockRLckdDays");

            data.sInvestorLockRLckExpiredDLckd = GetBool("sInvestorLockRLckExpiredDLckd");
            data.sInvestorLockRLckExpiredD_rep = GetString("sInvestorLockRLckExpiredD");

            data.sInvestorLockDeliveryExpiredDLckd = GetBool("sInvestorLockDeliveryExpiredDLckd");
            data.sInvestorLockDeliveryExpiredD_rep = GetString("sInvestorLockDeliveryExpiredD");
        }

        private void SetInvestorLockDates(CPageData data)
        {
            SetResult("sInvestorLockRLckdDays", data.sInvestorLockRLckdDays_rep);
            SetResult("sInvestorLockRLckExpiredDLckd", data.sInvestorLockRLckExpiredDLckd);
            SetResult("sInvestorLockDeliveryExpiredDLckd", data.sInvestorLockDeliveryExpiredDLckd);
            SetResult("sInvestorLockRLckdD", data.sInvestorLockRLckdD_rep);
            SetResult("sInvestorLockRLckExpiredD", data.sInvestorLockRLckExpiredD_rep);
            SetResult("sInvestorLockDeliveryExpiredD", data.sInvestorLockDeliveryExpiredD_rep);
        }

        private void LockRate()
        {
            var rateLockDate = GetString("sInvestorLockRLckdD");

            DateTime testingDateTime;

            if (DateTime.TryParse(rateLockDate, out testingDateTime) == false)
            {
                var devMsg = string.Format(
                        "Investor rate lock date '{0}' for loan '{1}' is not valid.",
                        rateLockDate,
                        GetGuid("loanid"));

                // As with BrokerRateLockService, we need to set the
                // file version to the current version to allow the 
                // rate to be locked once the user corrects the date.
                SetResult("sFileVersion", GetInt("sFileVersion"));

                throw new CBaseException(ErrorMessages.InvalidInvestorRateLockDate, devMsg);
            }

            CPageData data = PartialSave(attemptingRateLock: true);
            this.GetInvestorLockDates(data);
            data.LockInvestorRate(BrokerUser.LoginNm, GetString("Reason"));
            data.Save();

            SetResult("sFileVersion", data.sFileVersion);

            SetResult("sInvestorLockRateLockStatusT", data.sInvestorLockRateLockStatusT_rep);
            SetResult("sInvestorLockHistoryXmlContent", RateLockHistoryTable.BuildInvestorRateLockHistoryTable(data.sInvestorLockHistoryXmlContent));
            SetResult("sInvestorLockProjectedProfit", data.sInvestorLockProjectedProfit_rep);
            SetResult("sInvestorLockProjectedProfitAmt", data.sInvestorLockProjectedProfitAmt_rep);
            this.SetInvestorLockDates(data);
            SetResult("sBECurrentLockExtensionCount", data.sBECurrentLockExtensionCount);
            SetResult("sBETotalLockExtensionCount", data.sBETotalLockExtensionCount);
            SetResult("sBETotalRateReLockCount", data.sBETotalRateReLockCount);
        }

        private void ResumeLock()
        {
            CPageData data = PartialSave();
            this.GetInvestorLockDates(data);
            data.ResumeInvestorRateLock(BrokerUser.LoginNm, GetString("Reason"));
            data.Save();

            SetResult("sFileVersion", data.sFileVersion);
            this.SetInvestorLockDates(data);
            SetResult("sInvestorLockRateLockStatusT", data.sInvestorLockRateLockStatusT_rep);
            SetResult("sInvestorLockHistoryXmlContent", RateLockHistoryTable.BuildInvestorRateLockHistoryTable(data.sInvestorLockHistoryXmlContent));
            SetResult("sInvestorLockProjectedProfit", data.sInvestorLockProjectedProfit_rep);
            SetResult("sInvestorLockProjectedProfitAmt", data.sInvestorLockProjectedProfitAmt_rep);

            SetResult("sBECurrentLockExtensionCount", data.sBECurrentLockExtensionCount);
            SetResult("sBETotalLockExtensionCount", data.sBETotalLockExtensionCount);
            SetResult("sBETotalRateReLockCount", data.sBETotalRateReLockCount);
        }

        private void SuspendInvestorRateLock()
        {
            CPageData data = CreatePageData();
            data.InitSave(GetInt("sFileVersion"));
            data.SuspendInvestorRateLock(BrokerUser.LoginNm, GetString("Reason"));

            data.Save();
            this.SetInvestorLockDates(data);
            SetResult("sInvestorLockRateLockStatusT", data.sInvestorLockRateLockStatusT_rep);
            SetResult("sInvestorLockHistoryXmlContent", RateLockHistoryTable.BuildInvestorRateLockHistoryTable(data.sInvestorLockHistoryXmlContent));
            SetResult("sFileVersion", data.sFileVersion);

            SetResult("sBECurrentLockExtensionCount", data.sBECurrentLockExtensionCount);
            SetResult("sBETotalLockExtensionCount", data.sBETotalLockExtensionCount);
            SetResult("sBETotalRateReLockCount", data.sBETotalRateReLockCount);
        }

        private void BreakInvestorRateLock()
        {
            CPageData data = CreatePageData();
            data.InitSave(GetInt("sFileVersion"));
            data.BreakInvestorRateLock(BrokerUser.LoginNm, GetString("Reason"));

            data.Save();
            this.SetInvestorLockDates(data);
            SetResult("sInvestorLockRateLockStatusT", data.sInvestorLockRateLockStatusT_rep);
            SetResult("sInvestorLockHistoryXmlContent", RateLockHistoryTable.BuildInvestorRateLockHistoryTable(data.sInvestorLockHistoryXmlContent));
            SetResult("sFileVersion", data.sFileVersion);

            SetResult("sBECurrentLockExtensionCount", data.sBECurrentLockExtensionCount);
            SetResult("sBETotalLockExtensionCount", data.sBETotalLockExtensionCount);
            SetResult("sBETotalRateReLockCount", data.sBETotalRateReLockCount);
        }

        private void InvestorPagePricingUpdate()
        {
            string adjustmentData = GetString("Adjustments");
            List<PricingAdjustment> adjustments = LendersOffice.Common.ObsoleteSerializationHelper.JsonDeserialize<List<PricingAdjustment>>(adjustmentData);
            CPageData data = CreatePageData();
            data.InitLoad();
            data.sInvestorLockAdjustments = adjustments;

            string option = GetString("xPricingLock");

            switch (option)
            {
                case "BASE":
                    data.sInvestorLockPriceRowLockedT = E_sInvestorLockPriceRowLockedT.Base;
                    data.LoadInvestorLockData(
                        GetString("sInvestorLockBaseNoteIR"),
                        GetString("sInvestorLockBaseBrokComp1PcFee"),
                        GetString("sInvestorLockBaseRAdjMarginR"),
                        GetString("sInvestorLockBaseOptionArmTeaserR"));
                    break;
                case "ADJUSTED":
                    data.sInvestorLockPriceRowLockedT = E_sInvestorLockPriceRowLockedT.Adjusted;
                    data.LoadInvestorLockData(
                        GetString("sInvestorLockNoteIR"),
                        GetString("sInvestorLockBrokComp1Pc"),
                        GetString("sInvestorLockRAdjMarginR"),
                        GetString("sInvestorLockOptionArmTeaserR"));
                    break;

                default:
                    throw new CBaseException(ErrorMessages.Generic, "Invalid Pricing Row Lock In background save.");
            }

            data.sBECurrentLockExtensionCountLckd = GetBool("sBECurrentLockExtensionCountLckd");
            data.sBETotalLockExtensionCountLckd = GetBool("sBETotalLockExtensionCountLckd");
            data.sBETotalRateReLockCountLckd = GetBool("sBETotalRateReLockCountLckd");

            data.sBECurrentLockExtensionCount = GetInt("sBECurrentLockExtensionCount");
            data.sBETotalLockExtensionCount = GetInt("sBETotalLockExtensionCount");
            data.sBETotalRateReLockCount = GetInt("sBETotalRateReLockCount");

            SetResult("sBECurrentLockExtensionCount", data.sBECurrentLockExtensionCount);
            SetResult("sBECurrentLockExtensionCountLckd", data.sBECurrentLockExtensionCountLckd);

            SetResult("sBETotalLockExtensionCount", data.sBETotalLockExtensionCount);
            SetResult("sBETotalLockExtensionCountLckd", data.sBETotalLockExtensionCountLckd);

            SetResult("sBETotalRateReLockCount", data.sBETotalRateReLockCount);
            SetResult("sBETotalRateReLockCountLckd", data.sBETotalRateReLockCountLckd);

            SetResult("sInvestorLockBaseNoteIR", data.sInvestorLockBaseNoteIR_rep);
            SetResult("sInvestorLockBaseBrokComp1PcPrice", data.sInvestorLockBaseBrokComp1PcPrice);
            SetResult("sInvestorLockBaseBrokComp1PcFee", data.sInvestorLockBaseBrokComp1PcFee_rep);
            SetResult("sInvestorLockBaseRAdjMarginR", data.sInvestorLockBaseRAdjMarginR_rep);
            SetResult("sInvestorLockBaseOptionArmTeaserR", data.sInvestorLockBaseOptionArmTeaserR_rep);

            SetResult("sInvestorLockNoteIR", data.sInvestorLockNoteIR_rep);
            SetResult("sInvestorLockBrokComp1PcPrice", data.sInvestorLockBrokComp1PcPrice_rep);
            SetResult("sInvestorLockBrokComp1Pc", data.sInvestorLockBrokComp1Pc_rep);
            SetResult("sInvestorLockRAdjMarginR", data.sInvestorLockRAdjMarginR_rep);
            SetResult("sInvestorLockOptionArmTeaserR", data.sInvestorLockOptionArmTeaserR_rep);

            SetResult("sInvestorLockTotAdjNoteIR", data.sInvestorLockTotAdjNoteIR_rep);
            SetResult("sInvestorLockTotAdjBrokComp1PcPrice", data.sInvestorLockTotAdjBrokComp1PcPrice);
            SetResult("sInvestorLockTotAdjBrokComp1PcFee", data.sInvestorLockTotAdjBrokComp1PcFee_rep);
            SetResult("sInvestorLockTotAdjRAdjMarginR", data.sInvestorLockTotAdjRAdjMarginR_rep);
            SetResult("sInvestorLockTotAdjOptionArmTeaserR", data.sInvestorLockTotAdjOptionArmTeaserR_rep);

            SetResult("sInvestorLockProjectedProfit", data.sInvestorLockProjectedProfit_rep);
            SetResult("sInvestorLockProjectedProfitAmt", data.sInvestorLockProjectedProfitAmt_rep);

        }

        #region price/fee conversion copied from brokerratelockservice.aspx.cs
        private void PriceFeeConversion()
        {
            string conversion = GetString("conversion");
            if (conversion == "Adjustment")
            {
                ConvertPriceOrFeeForAdjustment();
            }
            else if (conversion == "Pricing")
            {
                ConvertPriceOrFeeForPricing();
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Dev Bug - Missing Price fee conversion variable");
            }
        }


        private void ConvertPriceOrFeeForPricing()
        {
            try
            {
                decimal value = GetRate("Value");
                decimal otherValue = 100 - value;
                SetResult("result", otherValue.ToString("0.000\\%"));
                SetResult("value", value.ToString("0.000\\%"));
            }
            catch (GenericUserErrorMessageException)
            {
                SetResult("result", "0.000%");
                SetResult("value", "0.000%");
            }
        }
        private void ConvertPriceOrFeeForAdjustment()
        {
            try
            {
                decimal value = GetRate("value");
                decimal otherValue = -1 * value;
                SetResult("result", otherValue.ToString("0.000\\%"));
                SetResult("value", value.ToString("0.000\\%"));
            }
            catch (GenericUserErrorMessageException)
            {
                SetResult("result", "0.000%");
                SetResult("value", "0.000%");

            }
        }

        #endregion

        /// <summary>
        /// This method is used by the Copy Info From Broker Lock Request
        /// </summary>
        private void CalculatePricingInfoFromPopulate()
        {
            Guid loanId = GetGuid("loanid");
            LosConvert conv = new LosConvert();

            decimal baseRate = conv.ToRate(GetString("sBrokerLockBaseNoteIR"));
            decimal basePrice = conv.ToRate(GetString("sBrokerLockBaseBrokComp1PcPrice"));
            decimal baseFee = conv.ToRate(GetString("sBrokerLockBaseBrokComp1PcFee"));
            decimal baseMargin = conv.ToRate(GetString("sBrokerLockBaseRAdjMarginR"));
            decimal baseTRate = conv.ToRate(GetString("sBrokerLockBaseOptionArmTeaserR"));

            string sadjustments = GetString("includedAdjustments");

            List<PricingAdjustment> adjustments = LendersOffice.Common.ObsoleteSerializationHelper.JsonDeserialize<List<PricingAdjustment>>(sadjustments);

            decimal aRate = 0M;
            decimal aPrice = 0M;
            decimal aFee = 0M;
            decimal aMargin = 0M;
            decimal aTRate = 0M;

            foreach (var adjustment in adjustments)
            {
                aRate += conv.ToRate(adjustment.Rate);
                aPrice += conv.ToRate(adjustment.Price);
                aFee += conv.ToRate(adjustment.Fee);
                aMargin += conv.ToRate(adjustment.Margin);
                aTRate += conv.ToRate(adjustment.TeaserRate);
            }

            baseRate += aRate;
            basePrice += aPrice;
            baseFee += aFee;
            baseMargin += aMargin;
            baseTRate += aTRate;

            SetResult("sInvestorTotalRate", conv.ToRateString(baseRate));
            SetResult("sInvestorTotalPrice", conv.ToRateString(basePrice));
            SetResult("sInvestorTotalFee", conv.ToRateString(baseFee));
            SetResult("sInvestorTotalMargin", conv.ToRateString(baseMargin));
            SetResult("sInvestorTotalTRate", conv.ToRateString(baseTRate));


            SetResult("sIncludedAdjustmentRate", conv.ToRateString(aRate));
            SetResult("sIncludedAdjustmentPrice", conv.ToRateString(aPrice));
            SetResult("sIncludedAdjustmentFee", conv.ToRateString(aFee));
            SetResult("sIncludedAdjustmentMargin", conv.ToRateString(aMargin));
            SetResult("sIncludedAdjustmentTRate", conv.ToRateString(aTRate));
        }

    }
}
