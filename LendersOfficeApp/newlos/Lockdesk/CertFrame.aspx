﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CertFrame.aspx.cs" Inherits="LendersOfficeApp.newlos.Lockdesk.CertFrame" %>
<%@ Register TagPrefix="ML" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Investor Loan Summary</title>
</head>
<body bgcolor="gainsboro" class="body-iframe">
    <script type="text/javascript">
       window.onresize = function() { f_resizeIframe('Certificate',10); };       
        function f_print() {
            lqbPrintByFrame(window.frames['Certificate']);
       }
       function _init(){
        resizeForIE6And7( 780, 600 );
        f_resizeIframe('Certificate',10); 
        }
    </script>
    <h4 class="page-header">Investor Loan Summary</h4>
    <form id="form1" runat="server">
    <table border="0" cellspacing="0" cellpadding="5" width="100%">
        <tr>
            <td align="right" valign="top">
                
                <input type="button" onclick="f_print();" value="Print" />
                <input type="button" onclick="onClosePopup()" value="Close" />
            </td>
        </tr>
    </table>
    
     <iframe runat="server" id="Certificate" width="100%"></iframe>
      <ML:CModalDlg id="m_ModalDlg" runat="server"></ML:CModalDlg>
     
    </form>
</body>
</html>
