﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Lockdesk
{
    public partial class RateLockEventInfo : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DisplayCopyRight = false;
            CPageData loanData = CPageData.CreateUsingSmartDependency(LoanID, typeof(RateLockEventInfo));
            loanData.InitLoad();

            Guid eventId = RequestHelper.GetGuid("eventid");
            
            RateLockHistoryItem item = RateLockHistoryItem.GetRateLockEventFromXml(loanData.sRateLockHistoryXmlContent, eventId);

            EventType.Text = item.Action;
            CausedOn.Text = item.EventDate;
            ProgramName.Text = item.sLpTemplateNm;
            LockPeriod.Text = item.sRLckdDays;
            RateLockDate.Text = item.sRLckdD;
            RateLockExpiration.Text = item.sRLckdExpiredD;
            PrepaymentPenalty.Text = item.sPpmtPenaltyMon_rep;
            RateSheetEffectiveTime.Text = item.sBrokerLockRateSheetEffectiveD;

            sBrokerLockBaseNoteIR.Text = item.sBrokerLockBaseNoteIR;
            sBrokerLockBaseBrokComp1PcPrice.Text = item.sBrokerLockBaseBrokComp1PcPrice;
            sBrokerLockBaseBrokComp1PcFee.Text = item.sBrokerLockBaseBrokComp1PcFee;
            sBrokerLockBaseRAdjMarginR.Text = item.sBrokerLockBaseRAdjMarginR;
            sBrokerLockBaseOptionArmTeaserR.Text = item.sBrokerLockBaseOptionArmTeaserR;
           
            sBrokerLockTotHiddenAdjNoteIR.Text = item.sBrokerLockTotHiddenAdjNoteIR;
            sBrokerLockTotHiddenAdjBrokComp1PcPrice.Text = item.sBrokerLockTotHiddenAdjBrokComp1PcPrice;
            sBrokerLockTotHiddenAdjBrokComp1PcFee.Text = item.sBrokerLockTotHiddenAdjBrokComp1PcFee;
            sBrokerLockTotHiddenAdjRAdjMarginR.Text = item.sBrokerLockTotHiddenAdjRAdjMarginR;
            sBrokerLockTotHiddenAdjOptionArmTeaserR.Text = item.sBrokerLockTotHiddenAdjOptionArmTeaserR;

            sBrokerLockBrokerBaseNoteIR.Text = item.sBrokerLockBrokerBaseNoteIR;
            sBrokerLockBrokerBaseBrokComp1PcPrice.Text = item.sBrokerLockBrokerBaseBrokComp1PcPrice;
            sBrokerLockBrokerBaseBrokComp1PcFee.Text = item.sBrokerLockBrokerBaseBrokComp1PcFee;
            sBrokerLockBrokerBaseRAdjMarginR.Text = item.sBrokerLockBrokerBaseRAdjMarginR;
            sBrokerLockBrokerBaseOptionArmTeaserR.Text = item.sBrokerLockBrokerBaseOptionArmTeaserR;

            sBrokerLockTotVisibleAdjNoteIR.Text = item.sBrokerLockTotVisibleAdjNoteIR;
            sBrokerLockTotVisibleAdjBrokComp1PcPrice.Text = item.sBrokerLockTotVisibleAdjBrokComp1PcPrice;
            sBrokerLockTotVisibleAdjBrokComp1PcFee.Text = item.sBrokerLockTotVisibleAdjBrokComp1PcFee;
            sBrokerLockTotVisibleAdjRAdjMarginR.Text = item.sBrokerLockTotVisibleAdjRAdjMarginR;
            sBrokerLockTotVisibleAdjOptionArmTeaserR.Text = item.sBrokerLockTotVisibleAdjOptionArmTeaserR;

            By.Text = item.DoneBy; 

            sNoteIR.Text = item.sNoteIR;
            sBrokerLockFinalBrokComp1PcPrice.Text = item.sBrokerLockFinalBrokComp1PcPrice;
            sBrokComp1Pc.Text = item.sBrokComp1Pc;
            sRAdjMarginR.Text = item.sRAdjMarginR;
            sOptionArmTeaserR.Text = item.sOptionArmTeaserR;
            AdjustmentDataRepeater.DataSource = item.pricingAdjustments;
            AdjustmentDataRepeater.DataBind();

            if (item.pricingAdjustments.Count == 0)
            {
                AdjustmentDataRepeater.Visible = false;
            }

        }
    }
}
