﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Lockdesk
{
    public partial class CertificateMainFrame : BasePage
    {
        protected string m_frmMenuUrl = "";
        protected string m_frmBodyUrl = "";
        protected bool IsDisplayDetailCert { get; private set; } = false; // opm 458165

        protected void Page_Load(object sender, EventArgs e)
        {
            Guid sLId = RequestHelper.LoanID;
            string lienQualifyModeT = RequestHelper.GetSafeQueryString("lienqualifymodet");
            string productId = RequestHelper.GetSafeQueryString("productid");
            string version = RequestHelper.GetSafeQueryString("version");
            int sPricingModeT = RequestHelper.GetInt("sPricingModeT");
            string uniqueChecksum = RequestHelper.GetSafeQueryString("uniquechecksum");
            int historicalPolicy = RequestHelper.GetInt("historicalPolicy", 0);
            int historicalRateOption = RequestHelper.GetInt("historicalRateOption", 0);
            string rate = RequestHelper.GetSafeQueryString("rate");
            string requestString = string.Format("loanid={0}&productid={1}&lienqualifymodet={2}&version={3}&sPricingModeT={4}&uniqueChecksum={5}&historicalPolicy={6}&historicalRateOption={7}&rate={8}", 
                sLId, productId, lienQualifyModeT, version, sPricingModeT, uniqueChecksum, historicalPolicy, historicalRateOption, rate);

            IsDisplayDetailCert = BrokerUserPrincipal.CurrentPrincipal?.CanViewDualCertificate == true;
            m_frmMenuUrl = "CertificateMenu.aspx?" + requestString + (IsDisplayDetailCert ? "&detailCert=t" : "");
            m_frmBodyUrl = "CertificateWait.aspx?" + requestString;


            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }
    }
}
