﻿namespace LendersOfficeApp.newlos
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using DataAccess.Core.Construction;
    using LendersOffice.Migration;
    using LendersOffice.UI.DataContainers;
    using MeridianLink.CommonControls;

    public partial class ConstructionLoanInfo : BaseLoanPage
    {
        

        private void Bind_TimingDDL(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_Timing.Blank));
            ddl.Items.Add(Tools.CreateEnumListItem("Before Closing", E_Timing.Before_Closing));
            ddl.Items.Add(Tools.CreateEnumListItem("At Closing", E_Timing.At_Closing));
            ddl.Items.Add(Tools.CreateEnumListItem("After Closing", E_Timing.After_Closing));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterJsGlobalVariables("ConstructionAmortTAdjustable", ((int)E_sFinMethT.ARM).ToString());
            RegisterJsGlobalVariables("ConstructionPurposeConstructionAndLotPurchase", ((int)ConstructionPurpose.ConstructionAndLotPurchase).ToString());

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ConstructionLoanInfo));
            dataLoan.InitLoad();
            List<DrawSchedule> drawSchedules = dataLoan.sDrawSchedules;

            Tools.Bind_sBuildingStatusT(sBuildingStatusT);
            Tools.SetDropDownListValue(sBuildingStatusT, dataLoan.sBuildingStatusT);

            Tools.Bind_sFinMethT(sConstructionAmortT);
            Tools.SetDropDownListValue(sConstructionAmortT, dataLoan.sConstructionAmortT);

            Tools.Bind_ConstructionIntCalcType(sConstructionIntCalcT);
            Tools.SetDropDownListValue(sConstructionIntCalcT, dataLoan.sConstructionIntCalcT);

            Tools.Bind_ConstructionMethod(sConstructionMethodT);
            Tools.SetDropDownListValue(sConstructionMethodT, dataLoan.sConstructionMethodT);

            Tools.Bind_ConstructionPhaseIntAccrual(sConstructionPhaseIntAccrualT);
            Tools.SetDropDownListValue(sConstructionPhaseIntAccrualT, dataLoan.sConstructionPhaseIntAccrualT);

            Tools.Bind_ConstructionPurpose(sConstructionPurposeT);
            Tools.SetDropDownListValue(sConstructionPurposeT, dataLoan.sConstructionPurposeT);

            Tools.Bind_LotOwnerType(sLotOwnerT);
            Tools.SetDropDownListValue(sLotOwnerT, dataLoan.sLotOwnerT);

            sConstructionPeriodMon.Value = dataLoan.sConstructionPeriodMon_rep;
            sConstructionPeriodIR.Text = dataLoan.sConstructionPeriodIR_rep;

            Tools.Bind_sRAdjRoundT(sConstructionRAdjRoundT);
            Tools.SetDropDownListValue(sConstructionRAdjRoundT, dataLoan.sConstructionRAdjRoundT);

            Tools.Bind_sLPurposeT(this.sLPurposeT);
            Tools.SetDropDownListValue(this.sLPurposeT, dataLoan.sLPurposeT);

            sLotImprovC.Text = dataLoan.sLotImprovC_rep;
            sLandCost.Text = dataLoan.sLandCost_rep;
            sLotVal.Text = dataLoan.sLotVal_rep;
            sSubsequentlyPaidFinanceChargeAmt.Text = dataLoan.sSubsequentlyPaidFinanceChargeAmt_rep;
            sConstructionInitialAdvanceAmt.Text = dataLoan.sConstructionInitialAdvanceAmt_rep;
            sConstructionLoanDLckd.Checked = dataLoan.sConstructionLoanDLckd;
            sConstructionLoanD.Text = dataLoan.sConstructionLoanD_rep;
            sConstructionIntAccrualDLckd.Checked = dataLoan.sConstructionIntAccrualDLckd;
            sConstructionIntAccrualD.Text = dataLoan.sConstructionIntAccrualD_rep;
            sConstructionFirstPaymentDLckd.Checked = dataLoan.sConstructionFirstPaymentDLckd;
            sConstructionFirstPaymentD.Text = dataLoan.sConstructionFirstPaymentD_rep;
            sIsIntReserveRequired.Checked = dataLoan.sIsIntReserveRequired;
            sIntReserveAmt.Text = dataLoan.sIntReserveAmt_rep;

            sConstructionRAdj1stCapR.Value = dataLoan.sConstructionRAdj1stCapR_rep;
            sConstructionRAdj1stCapMon.Value = dataLoan.sConstructionRAdj1stCapMon_rep;
            sConstructionRAdjCapR.Value = dataLoan.sConstructionRAdjCapR_rep;
            sConstructionRAdjCapMon.Value = dataLoan.sConstructionRAdjCapMon_rep;
            sConstructionRAdjLifeCapR.Value = dataLoan.sConstructionRAdjLifeCapR_rep;
            sConstructionRAdjMarginR.Value = dataLoan.sConstructionRAdjMarginR_rep;
            sConstructionArmIndexNameVstr.Value = dataLoan.sConstructionArmIndexNameVstr;
            sConstructionRAdjIndexR.Value = dataLoan.sConstructionRAdjIndexR_rep;
            sConstructionRAdjFloorR.Value = dataLoan.sConstructionRAdjFloorR_rep;
            sConstructionRAdjRoundToR.Value = dataLoan.sConstructionRAdjRoundToR_rep;
            sConstructionIntAmount.Text = dataLoan.sConstructionIntAmount_rep;
            sApr.Text = dataLoan.sApr_rep;

            RateFloorPopupFields popupFields = new RateFloorPopupFields();
            popupFields.FieldsType = RateFloorPopupFieldsType.Construction;
            popupFields.sIsRAdjFloorRReadOnly = dataLoan.sIsConstructionRAdjFloorRReadOnly.ToString();
            popupFields.sNoteIR = dataLoan.sConstructionPeriodIR_rep;
            popupFields.sRAdjFloorAddR = dataLoan.sConstructionRAdjFloorAddR_rep;
            popupFields.sRAdjFloorBaseR = dataLoan.sConstructionRAdjFloorBaseR_rep;
            popupFields.sRAdjFloorCalcT = dataLoan.sConstructionRAdjFloorCalcT;
            popupFields.sRAdjFloorCalcTLabel = Tools.GetsRAdjFloorCalcTLabel(dataLoan.sConstructionRAdjFloorCalcT);
            popupFields.sRAdjFloorLifeCapR = dataLoan.sConstructionRAdjFloorLifeCapR_rep;
            popupFields.sRAdjFloorR = dataLoan.sConstructionRAdjFloorR_rep;
            popupFields.sRAdjFloorTotalR = dataLoan.sConstructionRAdjFloorTotalR_rep;
            popupFields.sRAdjLifeCapR = dataLoan.sConstructionRAdjLifeCapR_rep;

            RateFloorPopup.SetRateLockPopupFieldValues(popupFields);


            for (int i = 0; i < drawSchedules.Count; i++)
            {
                DrawSchedule schedule = drawSchedules[i];

                HtmlTableRow row = new HtmlTableRow();
                row.Attributes.Add("index", ""+i);
                TextBox condition = new TextBox();
                HtmlTableCell cell = new HtmlTableCell();

                CheckBox checkbox = new CheckBox();
                checkbox.ID = "deleteCheckbox" + i;
                cell.Controls.Add(checkbox);
                row.Controls.Add(cell);

                cell = new HtmlTableCell();
                condition.ID = "condition" + i;
                condition.Style.Add("width", "400px");
                condition.TextMode = TextBoxMode.MultiLine;
                condition.Text = schedule.Condition;
                cell.Controls.Add(condition);
                row.Controls.Add(cell);

                cell = new HtmlTableCell();
                DropDownList timing = new DropDownList();
                Bind_TimingDDL(timing);
                timing.ID = "timing" + i;
                timing.SelectedValue = schedule.Timing;
                cell.Controls.Add(timing);
                row.Controls.Add(cell);

                cell = new HtmlTableCell();
                PercentTextBox percent = new PercentTextBox();
                percent.ID = "percent" + i;
                percent.Attributes.Add("onchange", "recalculateTotal("+i+");");
                percent.Text = schedule.Percent;
                cell.Controls.Add(percent);
                row.Controls.Add(cell);

                cell = new HtmlTableCell();
                DropDownList basis = new DropDownList();
                Tools.Bind_sProRealETxT(basis);
                basis.Attributes.Add("onchange", "recalculateTotal(" + i + ");");
                basis.ID = "basis" + i;
                cell.Controls.Add(basis);
                basis.SelectedValue = schedule.Basis;
                row.Controls.Add(cell);

                cell = new HtmlTableCell();
                MoneyTextBox fixedAmount = new MoneyTextBox();
                fixedAmount.ID = "fixedAmount" + i;
                fixedAmount.Attributes.Add("onchange", "recalculateTotal("+i+");");
                fixedAmount.Text = schedule.FixedAmount;
                cell.Controls.Add(fixedAmount);
                row.Controls.Add(cell);

                cell = new HtmlTableCell();
                MoneyTextBox total = new MoneyTextBox();
                total.ID = "total" + i;
                total.ReadOnly = true;
                total.Text = schedule.Total;
                cell.Controls.Add(total);
                row.Controls.Add(cell);


                if (i % 2 == 0)
                    row.Attributes.Add("class", "GridAlternatingItem");
                else
                    row.Attributes.Add("class", "GridItem");
                
                DrawScheduleTable.Controls.Add(row);
            }

            this.RegisterJsGlobalVariables("AreConstructionLoanDataPointsMigrated",
                LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints));
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Construction Loan Info";
            this.PageID = "ConstructionLoanInfo";
            IncludeStyleSheet(StyleSheet);
            RegisterJsScript("json.js");
            RegisterJsScript("mask.js");
            RegisterJsScript("LQBPopup.js");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
