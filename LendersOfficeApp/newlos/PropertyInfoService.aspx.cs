namespace LendersOfficeApp.newlos
{
    using System;
    using DataAccess;
    using LendersOffice.Migration;

    public class PropertyInfoServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(PropertyInfoServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataLoan.sSpCity       = GetString("sSpCity");
            dataLoan.sSpState      = GetString("sSpState");
            dataLoan.sSpZip        = GetString("sSpZip");
            dataLoan.sSpCounty     = GetString("sSpCounty");
            dataLoan.sSpLegalDesc  = GetString("sSpLegalDesc");
            dataLoan.sSpAddr       = GetString("sSpAddr");
            dataLoan.sUnitsNum_rep = GetString("sUnitsNum");
            dataLoan.sYrBuilt      = GetString("sYrBuilt");
            dataLoan.sGseSpT = (E_sGseSpT)GetInt("sGseSpT");
            dataLoan.sHomeIsMhAdvantageTri = this.GetEnum<E_TriState>("sHomeIsMhAdvantageTri");
            dataLoan.sIsNewConstruction = GetBool("sIsNewConstruction");
            dataLoan.sSpIsMixedUse = GetBool("sSpIsMixedUse");
            dataLoan.sSpIsInPud = GetBool("sSpIsInPud");
            dataLoan.sSubjPropertyMineralAbbrLegalDesc = GetString("sSubjPropertyMineralAbbrLegalDesc");
            dataLoan.sProjNm       = GetString("sProjNm");
            dataLoan.sNfipFloodZoneId = GetString("sNfipFloodZoneId");
            dataLoan.sAssessorsParcelId = GetString("sAssessorsParcelId");
            dataLoan.sHasMultipleParcels = GetBool("sHasMultipleParcels");
            dataLoan.sFloodCertificationIsInSpecialArea = GetBool("sFloodCertificationIsInSpecialArea");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            this.SetResult("sHomeIsMhAdvantageTri", dataLoan.sHomeIsMhAdvantageTri, forDropdown: true);
        }
    }
    public class SubjPropInvestmentServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(SubjPropInvestmentServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataLoan.sSpGrossRent_rep = GetString("sSpGrossRent");
            dataLoan.sOccRLckd = GetBool("sOccRLckd");
            dataLoan.sOccR_rep = GetString("sOccR");
            dataLoan.sSpCountRentalIForPrimaryResidToo = GetBool("sSpCountRentalIForPrimaryResidToo");
            dataApp.aOccT = (E_aOccT) GetInt("aOccT");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("sSpGrossRent", dataLoan.sSpGrossRent_rep);
            SetResult("sOccRLckd", dataLoan.sOccRLckd);
            SetResult("sOccR", dataLoan.sOccR_rep);
            SetResult("aOccT", dataApp.aOccT);
            SetResult("sSpCountRentalIForPrimaryResidToo", dataLoan.sSpCountRentalIForPrimaryResidToo);
        }
    }

    public class PropertyDetailsServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(PropertyDetailsServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sSpAddr = GetString("sSpAddr");
            dataLoan.sSpCity = GetString("sSpCity");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sSpZip = GetString("sSpZip");
            dataLoan.sGseSpT = (E_sGseSpT)GetInt("sGseSpT");
            dataLoan.sHomeIsMhAdvantageTri = this.GetEnum<E_TriState>("sHomeIsMhAdvantageTri");
            dataLoan.sIsNewConstruction = GetBool("sIsNewConstruction");
            dataLoan.sUnitsNum = GetInt("sUnitsNum");
            dataLoan.sProjNm = GetString("sProjNm");
            dataLoan.sCpmProjectId = GetString("sCpmProjectId");
            dataLoan.sSpProjectClassFreddieT = (E_sSpProjectClassFreddieT)GetInt("sSpProjectClassFreddieT");
            dataLoan.sSpProjectClassFannieT = (E_sSpProjectClassFannieT)GetInt("sSpProjectClassFannieT");
            dataLoan.sSpProjectStatusT = (E_sSpProjectStatusT)GetInt("sSpProjectStatusT");
            dataLoan.sSpProjectAttachmentT = (E_sSpProjectAttachmentT)GetInt("sSpProjectAttachmentT");
            dataLoan.sSpProjectDesignT = (E_sSpProjectDesignT)GetInt("sSpProjectDesignT");
            dataLoan.sSpValuationMethodT = (E_sSpValuationMethodT)GetInt("sSpValuationMethodT");
            dataLoan.sSpAppraisalFormT = (E_sSpAppraisalFormT)GetInt("sSpAppraisalFormT");
            dataLoan.sSpCuScore_rep = GetString(nameof(CPageData.sSpCuScore));
            dataLoan.sSpOvervaluationRiskT = (E_CuRiskT)GetInt("sSpOvervaluationRiskT");
            dataLoan.sSpPropertyEligibilityRiskT = (E_CuRiskT)GetInt("sSpPropertyEligibilityRiskT");
            dataLoan.sSpAppraisalQualityRiskT = (E_CuRiskT)GetInt("sSpAppraisalQualityRiskT");
            dataLoan.sSpAvmModelT = (E_sSpAvmModelT)GetInt("sSpAvmModelT");
            dataLoan.sSpGseRefiProgramT = (E_sSpGseRefiProgramT)GetInt("sSpGseRefiProgramT");
            dataLoan.sSpGseCollateralProgramT = (E_sSpGseCollateralProgramT)GetInt("sSpGseCollateralProgramT");
            dataLoan.sSpInvestorCurrentLoanT = (E_sSpInvestorCurrentLoanT)GetInt("sSpInvestorCurrentLoanT");
            dataLoan.sSpUnit1BedroomsCount_rep = GetString("sSpUnit1BedroomsCount");
            dataLoan.sSpUnit2BedroomsCount_rep = GetString("sSpUnit2BedroomsCount");
            dataLoan.sSpUnit3BedroomsCount_rep = GetString("sSpUnit3BedroomsCount");
            dataLoan.sSpUnit4BedroomsCount_rep = GetString("sSpUnit4BedroomsCount");
            dataLoan.sSpProjectDwellingUnitCount_rep = GetString("sSpProjectDwellingUnitCount");
            dataLoan.sSpProjectDwellingUnitSoldCount_rep = GetString("sSpProjectDwellingUnitSoldCount");
            dataLoan.sSpUnit1EligibleRent_rep = GetString("sSpUnit1EligibleRent");
            dataLoan.sSpUnit2EligibleRent_rep = GetString("sSpUnit2EligibleRent");
            dataLoan.sSpUnit3EligibleRent_rep = GetString("sSpUnit3EligibleRent");
            dataLoan.sSpUnit4EligibleRent_rep = GetString("sSpUnit4EligibleRent");
            dataLoan.sSpValuationEffectiveD_rep = GetString("sSpValuationEffectiveD");
            dataLoan.sSpAppraisalId = GetString("sSpAppraisalId");
            dataLoan.sSpSubmittedToUcdpD_rep = GetString("sSpSubmittedToUcdpD");

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V11_ConsolidateFHACaseNumberFields_Opm186897))
            {
                dataLoan.sFHAPreviousCaseNum = GetString("sFHAPreviousCaseNum");
            }
            else
            {
                dataLoan.sSpFhaCaseNumCurrentLoan = GetString("sSpFhaCaseNumCurrentLoan");
            }

            dataLoan.sSpGseLoanNumCurrentLoan = GetString("sSpGseLoanNumCurrentLoan");
            dataLoan.sFhaUfmipAmt_rep = GetString("sFhaUfmipAmt");
            dataLoan.sFhaEndorsementD_rep = GetString("sFhaEndorsementD");

            dataLoan.sManufacturedHomeMake = GetString("sManufacturedHomeMake");
            dataLoan.sManufacturedHomeModel = GetString("sManufacturedHomeModel");
            dataLoan.sManufacturedHomeYear = GetString("sManufacturedHomeYear");
            dataLoan.sManufacturedHomeConditionT = (E_ManufacturedHomeConditionT)GetInt("sManufacturedHomeConditionT");
            dataLoan.sManufacturedHomeLengthFeet_rep = GetString("sManufacturedHomeLengthFeet");
            dataLoan.sManufacturedHomeWidthFeet_rep = GetString("sManufacturedHomeWidthFeet");
            dataLoan.sManufacturedHomeManufacturer = GetString("sManufacturedHomeManufacturer");
            dataLoan.sManufacturedHomeSerialNumber = GetString("sManufacturedHomeSerialNumber");
            dataLoan.sManufacturedHomeAttachedToFoundation = (E_TriState)GetInt("sManufacturedHomeAttachedToFoundation");
            dataLoan.sManufacturedHomeHUDLabelNumber = GetString("sManufacturedHomeHUDLabelNumber");
            dataLoan.sManufacturedHomeCertificateofTitle = GetString("sManufacturedHomeCertificateofTitle");
            dataLoan.sManufacturedHomeCertificateofTitleT = (E_ManufacturedHomeCertificateofTitleT)GetInt("sManufacturedHomeCertificateofTitleT");
            dataLoan.sCooperativeApartmentNumber = GetString("sCooperativeApartmentNumber");
            dataLoan.sCooperativeNumberOfShares_rep = GetString("sCooperativeNumberOfShares");
            dataLoan.sCooperativeLeaseD_rep = GetString("sCooperativeLeaseD");
            dataLoan.sCooperativeLeaseAssignD_rep = GetString("sCooperativeLeaseAssignD");
            dataLoan.sCooperativeLienSearchD_rep = GetString("sCooperativeLienSearchD");
            dataLoan.sCooperativeLienSearchNumber = GetString("sCooperativeLienSearchNumber");
            dataLoan.sCooperativeFormOfOwnershipT = (E_CooperativeFormOfOwnershipT)GetInt("sCooperativeFormOfOwnershipT");
            dataLoan.sCooperativeStockCertNumber = GetString("sCooperativeStockCertNumber");
            dataLoan.sLeaseholdLeaseD_rep = GetString("sLeaseholdLeaseD");
            dataLoan.sLeaseHoldExpireD_rep = GetString("sLeaseHoldExpireD");
            dataLoan.sLeaseholdRecordD_rep = GetString("sLeaseholdRecordD");
            dataLoan.sLeaseHoldInstrumentNumber = GetString("sLeaseHoldInstrumentNumber");
            dataLoan.sLeaseholdHolderName = GetString("sLeaseholdHolderName");
            dataLoan.sPurchaseContractDate_rep = GetString("sPurchaseContractDate");
            dataLoan.sFinancingContingencyExpD_rep = GetString("sFinancingContingencyExpD");
            dataLoan.sFinancingContingencyExtensionExpD_rep = GetString("sFinancingContingencyExtensionExpD");
            dataLoan.sDeedGeneratedD_rep = GetString("sDeedGeneratedD");

            if (dataLoan.sLT == E_sLT.FHA)
            {
                dataLoan.sApprVal_rep = GetString("sApprVal");
                dataLoan.sSpFhaDocId = GetString("sSpFhaDocId");
                dataLoan.sSpSubmittedToFhaD_rep = GetString("sSpSubmittedToFhaD");
            }

            dataLoan.sLoanBeingRefinancedAmortizationT = GetEnum<E_sLoanBeingRefinancedAmortizationT>("sLoanBeingRefinancedAmortizationT");
            dataLoan.sLoanBeingRefinancedLienPosT = GetEnum<E_sLoanBeingRefinancedLienPosT>("sLoanBeingRefinancedLienPosT");
            dataLoan.sLoanBeingRefinancedInterestRate_rep = GetString("sLoanBeingRefinancedInterestRate");
            dataLoan.sLoanBeingRefinancedRemainingTerm_rep = GetString("sLoanBeingRefinancedRemainingTerm");
            dataLoan.sLoanBeingRefinancedTotalOfPayments_rep = GetString("sLoanBeingRefinancedTotalOfPayments");
            dataLoan.sLoanBeingRefinancedLtvR_rep = GetString("sLoanBeingRefinancedLtvR");
            dataLoan.sLoanBeingRefinancedUsedToConstructAlterRepair = GetBool("sLoanBeingRefinancedUsedToConstructAlterRepair");

            ValidateData(dataLoan);
        }

        private void ValidateData(CPageData dataLoan)
        {
            string userMsg, devMsg;
            switch(dataLoan.sSpGseRefiProgramT)
            {
                case E_sSpGseRefiProgramT.DURefiPlus:
                case E_sSpGseRefiProgramT.RefiPlus:
                    if (E_sSpInvestorCurrentLoanT.FannieMae != dataLoan.sSpInvestorCurrentLoanT)
                    {
                        userMsg = "If refinance program is DU Refinance Plus or Refinance Plus, investor must be Fannie Mae";
                        devMsg = "If sSpGseRefiProgramT is DU Refinance Plus or Refinance Plus, sSpInvestorCurrentLoanT must be Fannie Mae";
                        throw new CBaseException(userMsg, devMsg);
                    }
                    break;
                case E_sSpGseRefiProgramT.ReliefRefiOpenAccess:
                case E_sSpGseRefiProgramT.ReliefRefiSameServicer:
                    if (E_sSpInvestorCurrentLoanT.FreddieMac != dataLoan.sSpInvestorCurrentLoanT)
                    {
                        userMsg = "If refinance program is a relief program, investor must be Freddie Mac";
                        devMsg = "If sSpGseRefiProgramT is a relief program, sSpInvestorCurrentLoanT must be Freddie Mac";
                        throw new CBaseException(userMsg, devMsg);
                    }    
                    break;
                default:
                    break;
            }
            
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sSpAddr", dataLoan.sSpAddr);
            SetResult("sSpCity", dataLoan.sSpCity);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sSpZip", dataLoan.sSpZip);
            SetResult("sGseSpT", dataLoan.sGseSpT);
            this.SetResult("sHomeIsMhAdvantageTri", dataLoan.sHomeIsMhAdvantageTri, forDropdown: true);
            SetResult("sIsNewConstruction", dataLoan.sIsNewConstruction);
            SetResult("sSpIsMixedUse", dataLoan.sSpIsMixedUse);
            SetResult("sSpIsInPud", dataLoan.sSpIsInPud);
            SetResult("sUnitsNum", dataLoan.sUnitsNum);
            SetResult("sProjNm", dataLoan.sProjNm);
            SetResult("sCpmProjectId", dataLoan.sCpmProjectId);
            SetResult("sSpProjectClassFreddieT", dataLoan.sSpProjectClassFreddieT);
            SetResult("sSpProjectClassFannieT", dataLoan.sSpProjectClassFannieT);
            SetResult("sSpUnit1BedroomsCount", dataLoan.sSpUnit1BedroomsCount_rep);
            SetResult("sSpUnit2BedroomsCount", dataLoan.sSpUnit2BedroomsCount_rep);
            SetResult("sSpUnit3BedroomsCount", dataLoan.sSpUnit3BedroomsCount_rep);
            SetResult("sSpUnit4BedroomsCount", dataLoan.sSpUnit4BedroomsCount_rep);
            SetResult("sSpUnit1EligibleRent", dataLoan.sSpUnit1EligibleRent_rep);
            SetResult("sSpUnit2EligibleRent", dataLoan.sSpUnit2EligibleRent_rep);
            SetResult("sSpUnit3EligibleRent", dataLoan.sSpUnit3EligibleRent_rep);
            SetResult("sSpUnit4EligibleRent", dataLoan.sSpUnit4EligibleRent_rep);
            SetResult("sSpProjectStatusT", dataLoan.sSpProjectStatusT);
            SetResult("sSpProjectAttachmentT", dataLoan.sSpProjectAttachmentT);
            SetResult("sSpProjectDesignT", dataLoan.sSpProjectDesignT);
            SetResult("sSpProjectDwellingUnitCount", dataLoan.sSpProjectDwellingUnitCount_rep);
            SetResult("sSpProjectDwellingUnitSoldCount", dataLoan.sSpProjectDwellingUnitSoldCount_rep);
            SetResult("sSpValuationMethodT", dataLoan.sSpValuationMethodT);
            SetResult("sSpAppraisalFormT", dataLoan.sSpAppraisalFormT);
            SetResult("sSpCuScore", dataLoan.sSpCuScore_rep);
            SetResult("sSpOvervaluationRiskT", dataLoan.sSpOvervaluationRiskT);
            SetResult("sSpPropertyEligibilityRiskT", dataLoan.sSpPropertyEligibilityRiskT);
            SetResult("sSpAppraisalQualityRiskT", dataLoan.sSpAppraisalQualityRiskT);
            SetResult("sSpAvmModelT", dataLoan.sSpAvmModelT);
            SetResult("sSpValuationEffectiveD", dataLoan.sSpValuationEffectiveD);
            SetResult("sSpAppraisalId", dataLoan.sSpAppraisalId);
            SetResult("sSpSubmittedToUcdpD", dataLoan.sSpSubmittedToUcdpD_rep);

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V11_ConsolidateFHACaseNumberFields_Opm186897))
            {
                SetResult("sFHAPreviousCaseNum", dataLoan.sFHAPreviousCaseNum);
            }
            else
            {
                SetResult("sSpFhaCaseNumCurrentLoan", dataLoan.sSpFhaCaseNumCurrentLoan);
            }

            SetResult("sSpGseLoanNumCurrentLoan", dataLoan.sSpGseLoanNumCurrentLoan);
            SetResult("sSpGseRefiProgramT", dataLoan.sSpGseRefiProgramT);
            SetResult("sSpInvestorCurrentLoanT", dataLoan.sSpInvestorCurrentLoanT);
            SetResult("sSpGseCollateralProgramT", dataLoan.sSpGseCollateralProgramT);            

            SetResult("sManufacturedHomeMake", dataLoan.sManufacturedHomeMake);
            SetResult("sManufacturedHomeModel", dataLoan.sManufacturedHomeModel);
            SetResult("sManufacturedHomeYear", dataLoan.sManufacturedHomeYear);
            SetResult("sManufacturedHomeConditionT", dataLoan.sManufacturedHomeConditionT);
            SetResult("sManufacturedHomeLengthFeet", dataLoan.sManufacturedHomeLengthFeet_rep);
            SetResult("sManufacturedHomeWidthFeet", dataLoan.sManufacturedHomeWidthFeet_rep);
            SetResult("sManufacturedHomeManufacturer", dataLoan.sManufacturedHomeManufacturer);
            SetResult("sManufacturedHomeSerialNumber", dataLoan.sManufacturedHomeSerialNumber);
            SetResult("sManufacturedHomeAttachedToFoundation", dataLoan.sManufacturedHomeAttachedToFoundation);
            SetResult("sManufacturedHomeHUDLabelNumber", dataLoan.sManufacturedHomeHUDLabelNumber);
            SetResult("sManufacturedHomeCertificateofTitle", dataLoan.sManufacturedHomeCertificateofTitle);
            SetResult("sManufacturedHomeCertificateofTitleT", dataLoan.sManufacturedHomeCertificateofTitleT);
            SetResult("sCooperativeApartmentNumber", dataLoan.sCooperativeApartmentNumber);
            SetResult("sCooperativeNumberOfShares", dataLoan.sCooperativeNumberOfShares_rep);
            SetResult("sCooperativeLeaseD", dataLoan.sCooperativeLeaseD_rep);
            SetResult("sCooperativeLeaseAssignD", dataLoan.sCooperativeLeaseAssignD_rep);
            SetResult("sCooperativeLienSearchD", dataLoan.sCooperativeLienSearchD_rep);
            SetResult("sCooperativeLienSearchNumber", dataLoan.sCooperativeLienSearchNumber);
            SetResult("sCooperativeFormOfOwnershipT", dataLoan.sCooperativeFormOfOwnershipT);
            SetResult("sCooperativeStockCertNumber", dataLoan.sCooperativeStockCertNumber);
            SetResult("sLeaseholdLeaseD", dataLoan.sLeaseholdLeaseD_rep);
            SetResult("sLeaseHoldExpireD", dataLoan.sLeaseHoldExpireD_rep);
            SetResult("sLeaseholdRecordD", dataLoan.sLeaseholdRecordD_rep);
            SetResult("sLeaseHoldInstrumentNumber", dataLoan.sLeaseHoldInstrumentNumber);
            SetResult("sLeaseholdHolderName", dataLoan.sLeaseholdHolderName);
            SetResult("sPurchaseContractDate", dataLoan.sPurchaseContractDate_rep);
            SetResult("sFinancingContingencyExpD", dataLoan.sFinancingContingencyExpD_rep);
            SetResult("sFinancingContingencyExtensionExpD", dataLoan.sFinancingContingencyExtensionExpD_rep);
            SetResult("sDeedGeneratedD", dataLoan.sDeedGeneratedD_rep);

            if (dataLoan.sLT == E_sLT.FHA)
            {
                SetResult("sApprVal", dataLoan.sApprVal_rep);
                SetResult("sSpFhaDocId", dataLoan.sSpFhaDocId);
                SetResult("sSpSubmittedToFhaD", dataLoan.sSpSubmittedToFhaD_rep);
            }

            SetResult("sLoanBeingRefinancedAmortizationT", dataLoan.sLoanBeingRefinancedAmortizationT);
            SetResult("sLoanBeingRefinancedLienPosT", dataLoan.sLoanBeingRefinancedLienPosT);
            SetResult("sLoanBeingRefinancedInterestRate", dataLoan.sLoanBeingRefinancedInterestRate_rep);
            SetResult("sLoanBeingRefinancedRemainingTerm", dataLoan.sLoanBeingRefinancedRemainingTerm_rep);
            SetResult("sLoanBeingRefinancedTotalOfPayments", dataLoan.sLoanBeingRefinancedTotalOfPayments_rep);
            SetResult("sLoanBeingRefinancedLtvR", dataLoan.sLoanBeingRefinancedLtvR_rep);
            SetResult("sLoanBeingRefinancedUsedToConstructAlterRepair", dataLoan.sLoanBeingRefinancedUsedToConstructAlterRepair);
        }
    }

    public partial class PropertyInfoService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize() 
        {
            AddBackgroundItem("PropertyInfoUserControl", new PropertyInfoServiceItem());
            AddBackgroundItem("SubjPropInvestment", new SubjPropInvestmentServiceItem());
            AddBackgroundItem("PropertyDetailsUserControl", new PropertyDetailsServiceItem());
        }

    }
}
