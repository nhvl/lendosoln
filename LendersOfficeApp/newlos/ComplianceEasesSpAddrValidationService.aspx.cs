﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ComplianceEase;
using DataAccess;

namespace LendersOfficeApp.newlos
{
    public partial class ComplianceEasesSpAddrValidationService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "ValidatesSpAddr":
                    ValidatesSpAddr();
                    break;
            }
        }
        protected void ValidatesSpAddr()
        {
            var address = GetString("sSpAddr");
            ParsedStreetAddress _ParsedStreetAddress = new ParsedStreetAddress();
            CommonLib.Address addr = new CommonLib.Address();
            try
            {
                addr.ParseStreetAddress(address);
                SetResult("sSpAddrIsNotValid", "false");
            }
            catch(CBaseException)
            {
                SetResult("sSpAddrIsNotValid", "true");
            }
            catch(Exception)
            {
                SetResult("sSpAddrIsNotValid", "true");
            }
            
        }
    }
}
