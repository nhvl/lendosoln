using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;


namespace LendersOfficeApp.newlos
{
	/// <summary>
	/// Summary description for EmploymentRecord.
	/// </summary>
	public partial class EmploymentRecord : BaseSingleEditPage2
	{

        protected bool m_isBorrower
        {
            get { return RequestHelper.GetBool("isborrower"); }
        }


        protected void PageInit(object sender, System.EventArgs e)
        {
            EmplrZip.SmartZipcode(EmplrCity, EmplrState);
            Tools.Bind_SelfOwnershipShare(SelfOwnershipShareT);
//            CPageData dataLoan = new CBorrowerInfoData( LoanID  );
//            dataLoan.InitLoad();
//            m_dataApp = dataLoan.GetAppData( ApplicationID );

            ClientScript.RegisterHiddenField("OwnerT", m_isBorrower ? "0" : "1");
            this.PageTitle = "Employment record";
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
            UseNewFramework = true;
            InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion

	}
}
