﻿using System;
using DataAccess;
using LendersOffice.Common;
using Toolbox;

namespace LendersOfficeApp.newlos
{
    public partial class GovernmentMonitoringData : BaseLoanPage
    {
        /// <summary>
        /// Saves data to the loan from this page's controls.
        /// </summary>
        /// <param name="dataLoan">The loan to bind to.</param>
        /// <param name="dataApp">The app to bind to.</param>
        /// <param name="serviceItem">The service item calling this method.</param>
        internal static void BindDataFromControls(CPageData dataLoan, CAppData dataApp, AbstractBackgroundServiceItem serviceItem)
        {
            HmdaRaceEthnicityData.BindHmdaRaceEthnicityData(dataApp, serviceItem, $"{nameof(HREData)}_");
        }

        /// <summary>
        /// Loads the data from the loan to this page's controls.
        /// </summary>
        /// <param name="dataLoan">The loan to load from.</param>
        /// <param name="dataApp">The app to load from.</param>
        /// <param name="serviceItem">The service item.</param>
        internal static void LoadDataForControls(CPageData dataLoan, CAppData dataApp, AbstractBackgroundServiceItem serviceItem)
        {
            HmdaRaceEthnicityData.LoadHmdaRaceEthnicityData(dataApp, serviceItem, $"{nameof(HREData)}_");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(GovernmentMonitoringData));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            this.HREData.ShouldLoad = true;
            this.HREData.RaceEthnicityData = dataApp.ConstructHmdaRaceEthnicityData();
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Government Monitoring Data";
            this.PageID = "GovernmentMonitoringData";
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
