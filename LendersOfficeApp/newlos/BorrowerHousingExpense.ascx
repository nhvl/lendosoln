<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BorrowerHousingExpense.ascx.cs" Inherits="LendersOfficeApp.newlos.BorrowerHousingExpense" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

			<table class="InsetBorder" id="Table1" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td class="LoanFormHeader" nowrap colspan="2">Present Housing Expense (Borrower's Current Residence)</td>
				</tr>
				<tr>
					<td nowrap></td>
					<td class="FieldLabel" nowrap>Amount</td>
				</tr>
				<tr>
					<td class="FieldLabel" nowrap>Rent</td>
					<td nowrap><ml:moneytextbox id="aPresRent" runat="server" onchange="refreshCalculation();" preset="money" width="90" /></td>
				</tr>
				<tr>
					<td class="FieldLabel" nowrap>1st Mortgage (P &amp; I)</td>
					<td nowrap><ml:moneytextbox id="aPres1stM" runat="server" onchange="refreshCalculation();" preset="money" width="90" /></td>
				</tr>
				<tr>
					<td class="FieldLabel" nowrap>Other Financing (P &amp; I)</td>
					<td nowrap><ml:moneytextbox id="aPresOFin" runat="server" onchange="refreshCalculation();" preset="money" width="90" /></td>
				</tr>
				<tr>
					<td class="FieldLabel" nowrap>Hazard Insurance&nbsp;Amount or Notes</td>
					<td nowrap><asp:textbox id="aPresHazIns" style="TEXT-ALIGN: right" runat="server" onchange="refreshCalculation();" width="90px" /></td>
				</tr>
				<tr>
					<td class="FieldLabel" nowrap>Real Estate Taxes Amount or Notes</td>
					<td nowrap><asp:textbox id="aPresRealETx" style="TEXT-ALIGN: right" runat="server" onchange="refreshCalculation();"  width="90px" /></td>
				</tr>
				<tr>
					<td class="FieldLabel" nowrap>Mortgage Insurance Amount or Notes</td>
					<td nowrap><asp:textbox id="aPresMIns" style="TEXT-ALIGN: right" runat="server" onchange="refreshCalculation();" width="90px" /></td>
				</tr>
				<tr>
					<td class="FieldLabel" nowrap>Homeowner Association Dues</td>
					<td nowrap><ml:moneytextbox id="aPresHoAssocDues" runat="server" onchange="refreshCalculation();" preset="money" width="90" /></td>
				</tr>
				<tr>
					<td nowrap><asp:textbox id="aPresOHExpDesc" runat="server" width="210px" /></td>
					<td nowrap><ml:moneytextbox id="aPresOHExp" runat="server" onchange="refreshCalculation();" preset="money" width="90" /></td>
				</tr>
				<tr>
					<td class="FieldLabel" nowrap>Total</td>
					<td nowrap><ml:moneytextbox id="aPresTotHExp" runat="server" preset="money" width="90" readonly="True" /></td>
				</tr>
				<tr>
					<td class="FieldLabel" nowrap>Subject property occupancy type</td>
					<td nowrap><asp:dropdownlist id="aOccT" runat="server" onchange="refreshCalculation();" /></td>
				</tr>
				<tr>
					<td class="FieldLabel" nowrap>Present Housing Expense Included in Ratios</td>
					<td nowrap><ml:moneytextbox id="sPresLTotPersistentHExp" runat="server" preset="money" width="90" readonly="True" /></td>
				</tr>
				<tr>
					<td nowrap></td>
					<td nowrap></td>
				</tr>
			</table>
