﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WarehouseLenderRolodexInfo.aspx.cs" Inherits="LendersOfficeApp.newlos.WarehouseLenderRolodexInfo" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    
        <style type="text/css">
        #form1 table, #form1 fieldset { width: 700px; }
        #form1 fieldset, #form1 table.last  { margin-top: 10px; display: block;}
        #form1 fieldset table { margin-left: 7px; margin-top: 5px;  }
        #form1 table.first { margin-left: 20px; table-layout: fixed; }
        .MainRightHeader { margin-bottom: 10px; }
        body { font-weight: bold; background-color: gainsboro;}
        legend { font-weight: bolder;  }
        input.address, input.attention, input.company { width: 250px; }
        input.zip { width: 70px; }
        input.email { width: 150px; }
        input.contactname {width: 225px; }
        input.investor { width: 200px; }
        input.mers, input.payee { width: 95px; }
        span.usecb input { margin-left: -3px; position: relative;}
    </style>
</head>
<body>
    <script type="text/javascript">

            function onLenderChange() {
                if ($('#sWarehouseLenderRolodexEntries').val() == -2) {
                    document.getElementById("sWarehouseFunderDescTD1").style.display = "";
                    document.getElementById("sWarehouseFunderDescTD2").style.display = "";
                    document.getElementById("sWarehouseFunderDesc").value = "";

                    document.getElementById("AdvancedClassificationTD1").style.display = "none";
                    document.getElementById("AdvancedClassificationTD2").style.display = "none";
                }
                else {
                    document.getElementById("sWarehouseFunderDescTD1").style.display = "none";
                    document.getElementById("sWarehouseFunderDescTD2").style.display = "none";

                    document.getElementById("AdvancedClassificationTD1").style.display = "";
                    document.getElementById("AdvancedClassificationTD2").style.display = "";
                }
            }
            
            function f_save() {
                return f_saveMe();
            }
            
            function saveMe() {
                return f_saveMe();
            }
            
            function f_saveMe() {
                var saved = false;
                var description;
                if ($('#sWarehouseLenderRolodexEntries').val() == -2)
                    description = $('#sWarehouseFunderDesc').val();
                else
                    description = $('#sWarehouseLenderRolodexEntries option:selected').text();
                
                var data = { 
                    sLId : ML.sLId,
                    id: $('#sWarehouseLenderRolodexEntries').val(),
                    desc: description,
                    advClass: $('#sAdvanceClassification').val(),
                    maxFundPc: $('#sWarehouseMaxFundPc').val(),
                    fileVersion : $('#sFileVersion').val()
                };
                 callWebMethodAsync({
                    type: "POST",
                    async: false,
                    url: "WarehouseLenderRolodexInfo.aspx/Save",
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    success: function(msg){
                        saved = true;
                    },
                    error: function(){
                        alert('Error saving.');
                        saved = false;
                    }
                });
                
                return saved;
            }
            $(function() {
                $('#sWarehouseLenderRolodexEntries').change(function() {
                    callWebMethodAsync({
                        type: "POST",
                        url: "WarehouseLenderRolodexInfo.aspx/GetWarehouseLenderInfo",
                        data: '{ id :' + $(this).val() + '}',
                        contentType: 'application/json; charset=utf-8',
                        dataType: "json",
                        success: function(msg) {
                            $('#WarehouseLenderRolodexId').val(msg.d.WarehouseLenderRolodexId);
                            $('#BrokerId').val(msg.d.BrokerId);
                            $('#WarehouseLenderName').val(msg.d.WarehouseLenderName);
                            $('#Status').val(msg.d.Status);
                            $('#sWarehouseLenderFannieMaePayeeId').val(msg.d.FannieMaePayeeId);
                            $('#sWarehouseLenderFreddieMacPayeeId').val(msg.d.FreddieMacPayeeId);
                            $('#sWarehouseLenderFannieMaeId').val(msg.d.WarehouseLenderFannieMaeId);
                            $('#sWarehouseLenderFreddieMacId').val(msg.d.WarehouseLenderFreddieMacId);
                            $('#sWarehouseLenderMersOrganizationId').val(msg.d.MersOrganizationId);
                            $('#sWarehouseLenderPayToBankName').val(msg.d.PayToBankName);
                            $('#sWarehouseLenderPayToCity').val(msg.d.PayToCity);
                            $('#sWarehouseLenderPayToState').val(msg.d.PayToState);
                            $('#sWarehouseLenderPayToABANum').val(msg.d.PayToABANum.Value);
                            $('#sWarehouseLenderPayToAccountName').val(msg.d.PayToAccountName);
                            $('#sWarehouseLenderPayToAccountNum').val(msg.d.PayToAccountNum.Value);
                            $('#sWarehouseLenderFurtherCreditToAccountName').val(msg.d.FurtherCreditToAccountName);
                            $('#sWarehouseLenderFurtherCreditToAccountNum').val(msg.d.FurtherCreditToAccountNum.Value);
                            $('#sWarehouseLenderMainCompanyName').val(msg.d.MainCompanyName);
                            $('#sWarehouseLenderMainContactName').val(msg.d.MainContactName);
                            $('#sWarehouseLenderMainAttention').val(msg.d.MainAttention);
                            $('#sWarehouseLenderMainEmail').val(msg.d.MainEmail);
                            $('#sWarehouseLenderMainAddress').val(msg.d.MainAddress);
                            $('#sWarehouseLenderMainCity').val(msg.d.MainCity);
                            $('#sWarehouseLenderMainState').val(msg.d.MainState);
                            $('#sWarehouseLenderMainZip').val(msg.d.MainZip);
                            $('#sWarehouseLenderMainPhone').val(msg.d.MainPhone);
                            $('#sWarehouseLenderMainFax').val(msg.d.MainFax);
                            $('#sWarehouseLenderCollateralPackageToAttention').val(msg.d.CollateralPackageToAttention);
                            $('#sWarehouseLenderCollateralPackageToContactName').val(msg.d.CollateralPackageToContactName);
                            $('#sWarehouseLenderCollateralPackageToUseMainAddress').val(msg.d.CollateralPackageToUseMainAddress);
                            $('#sWarehouseLenderCollateralPackageToEmail').val(msg.d.CollateralPackageToEmail);
                            $('#sWarehouseLenderCollateralPackageToAddress').val(msg.d.CollateralPackageToAddress);
                            $('#sWarehouseLenderCollateralPackageToCity').val(msg.d.CollateralPackageToCity);
                            $('#sWarehouseLenderCollateralPackageToState').val(msg.d.CollateralPackageToState);
                            $('#sWarehouseLenderCollateralPackageToZip').val(msg.d.CollateralPackageToZip);
                            $('#sWarehouseLenderCollateralPackageToPhone').val(msg.d.CollateralPackageToPhone);
                            $('#sWarehouseLenderCollateralPackageToFax').val(msg.d.CollateralPackageToFax);
                            $('#sWarehouseLenderClosingPackageToAttention').val(msg.d.ClosingPackageToAttention);
                            $('#sWarehouseLenderClosingPackageToContactName').val(msg.d.ClosingPackageToContactName);
                            $('#sWarehouseLenderClosingPackageToUseMainAddress').val(msg.d.ClosingPackageToUseMainAddress);
                            $('#sWarehouseLenderClosingPackageToEmail').val(msg.d.ClosingPackageToEmail);
                            $('#sWarehouseLenderClosingPackageToAddress').val(msg.d.ClosingPackageToAddress);
                            $('#sWarehouseLenderClosingPackageToCity').val(msg.d.ClosingPackageToCity);
                            $('#sWarehouseLenderClosingPackageToState').val(msg.d.ClosingPackageToState);
                            $('#sWarehouseLenderClosingPackageToZip').val(msg.d.ClosingPackageToZip);
                            $('#sWarehouseLenderClosingPackageToPhone').val(msg.d.ClosingPackageToPhone);
                            $('#sWarehouseLenderClosingPackageToFax').val(msg.d.ClosingPackageToFax);
                            $('#sAdvanceClassification').empty().append("<option></option>");
                            $(msg.d.AdvanceClassifications).each(function(index, item) {
                                var value = item.Description + " - " + item.MaxFundingPercent_rep;
                                $('#sAdvanceClassification').append(
                                    $("<option></option>").val(value).text(value)
                                );
                            });
                        },
                        error: function() {
                            alert('Could not fetch warehouse lender data.');
                        }
                    });
                });
            });
            $(function() {

            if ($('#sWarehouseLenderRolodexEntries').val() == -2) {
                document.getElementById("sWarehouseFunderDescTD1").style.display = "";
                document.getElementById("sWarehouseFunderDescTD2").style.display = "";

                document.getElementById("AdvancedClassificationTD1").style.display = "none";
                document.getElementById("AdvancedClassificationTD2").style.display = "none";
                }
                $('#sAdvanceClassification').change(function() {
                    var regex = /\d+(\.\d+)?%(\s+)?$/
                    var arr = regex.exec(this.value);
                    if (arr != null)
                        $('#sWarehouseMaxFundPc').val(arr[0]);
                });
            });
    </script>
    <form id="form1" runat="server">
    <div class="MainRightHeader">
        <ml:EncodedLiteral runat="server" ID="HeaderText">Warehouse Lender Info</ml:EncodedLiteral>
    </div>
    <table cellpadding="0" cellspacing="0" class="first">
        <tr>
            <td width="130px" >
                <label for="sWarehouseLenderRolodexEntries">Warehouse Lender</label>
            </td>
            <td width="230px">
                <asp:DropDownList runat="server" ID="sWarehouseLenderRolodexEntries" onchange="onLenderChange();" Width="200px"></asp:DropDownList>
            </td><td>
                <label for="sWarehouseLenderMersOrganizationId">MERS Organization ID</label>
            </td>
            <td >
                <asp:TextBox runat="server" ID="sWarehouseLenderMersOrganizationId" preset="mersorgid" ReadOnly="true" CssClass="mers"></asp:TextBox>
            </td>
        </tr>
        <tr>
        
            <td id="sWarehouseFunderDescTD1" style="display:none;">
                <label for="sWarehouseFunderDesc">Warehouse Lender Other Desc</label>
            </td>
            <td id="sWarehouseFunderDescTD2" style="display:none;">
                <asp:TextBox runat="server" ID="sWarehouseFunderDesc" style="width: 197px"></asp:TextBox>
            </td>
            
            <td id="AdvancedClassificationTD1">
                <label for="sAdvanceClassification">Advance classification</label>
            </td>
            <td id="AdvancedClassificationTD2">
                <asp:DropDownList runat="server" ID="sAdvanceClassification" Width="200px"></asp:DropDownList>
            </td>
            <td>
                <label for="sWarehouseLenderFannieMaePayeeId">Fannie Mae Payee ID</label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="sWarehouseLenderFannieMaePayeeId" CssClass="payee" ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                <label for="sWarehouseLenderFannieMaeId">Fannie Mae Warehouse Lender ID</label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="sWarehouseLenderFannieMaeId" CssClass="payee" ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                <label for="sWarehouseLenderFreddieMacPayeeId">Freddie Mac Payee ID</label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="sWarehouseLenderFreddieMacPayeeId" CssClass="payee" ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                <label for="sWarehouseLenderFreddieMacId">Freddie Mac Warehouse Lender ID</label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="sWarehouseLenderFreddieMacId" CssClass="payee" ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <label for="sWarehouseMaxFundPc">Max Funding %</label>
            </td>
            <td>
                <ml:PercentTextBox runat="server" ID="sWarehouseMaxFundPc" Width="90px"></ml:PercentTextBox>
            </td>
        </tr>
    </table>
    <fieldset style="margin-left: 10px">
        <legend>Payment Instructions</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    Pay to
                </td>
                <td>
                    <label for="sWarehouseLenderPayToBankName">Bank Name</label>
                </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="sWarehouseLenderPayToBankName" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <label for="sWarehouseLenderPayToCity">City / State</label>
                </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="sWarehouseLenderPayToCity" ReadOnly="true"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="sWarehouseLenderPayToState" disabled="disabled"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <label for="sWarehouseLenderPayToABANum">ABA Number</label>
                </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="sWarehouseLenderPayToABANum" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <label for="sWarehouseLenderPayToAccountName">Account Name</label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="sWarehouseLenderPayToAccountName" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <label for="sWarehouseLenderPayToAccountNum">Account #</label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="sWarehouseLenderPayToAccountNum" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Further credit to
                </td>
                <td >
                    <label for="sWarehouseLenderFurtherCreditToAccountName">Account Name</label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="sWarehouseLenderFurtherCreditToAccountName" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <label for="sWarehouseLenderFurtherCreditToAccountNum">Account #</label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="sWarehouseLenderFurtherCreditToAccountNum" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset style="margin-left: 10px">
        <legend>Main Address</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <label for="sWarehouseLenderMainCompanyName">Company Name</label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="sWarehouseLenderMainCompanyName" CssClass="company" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <label for="sWarehouseLenderMainContactName">Contact Name</label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="sWarehouseLenderMainContactName" CssClass="contactname" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="sWarehouseLenderMainAttention">Attention</label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="sWarehouseLenderMainAttention" CssClass="attention" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <label for="sWarehouseLenderMainEmail">Email</label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="sWarehouseLenderMainEmail" CssClass="email" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="sWarehouseLenderMainAddress">Address</label>
                </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="sWarehouseLenderMainAddress" CssClass="address" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="sWarehouseLenderMainCity" CssClass="city" ReadOnly="true"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="sWarehouseLenderMainState"  CssClass="state" disabled="disabled"/>
                    <asp:TextBox runat="server" ID="sWarehouseLenderMainZip" preset="longzipcode" CssClass="zip" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <label for="sWarehouseLenderMainPhone">Phone</label>
                </td>
                <td>
                    <ml:PhoneTextBox runat="server" ID="sWarehouseLenderMainPhone" CssClass="phone" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                
            </tr>
            <tr>
                <td>
                    <label for="sWarehouseLenderMainFax">Fax</label>
                </td>
                <td colspan="3">
                    <ml:PhoneTextBox runat="server" ID="sWarehouseLenderMainFax" CssClass="fax" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset style="margin-left: 10px">
        <legend>Collateral package to</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <label for="sWarehouseLenderCollateralPackageToAttention">Attention</label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="sWarehouseLenderCollateralPackageToAttention" CssClass="attention" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <label for="sWarehouseLenderCollateralPackageToContactName">Contact Name</label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="sWarehouseLenderCollateralPackageToContactName" CssClass="contactname" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="sWarehouseLenderCollateralPackageToAddress">Address</label>
                </td>
                <td >
                    <asp:TextBox runat="server" ID="sWarehouseLenderCollateralPackageToAddress" CssClass="address" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <label for="sWarehouseLenderCollateralPackageToEmail">Email</label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="sWarehouseLenderCollateralPackageToEmail" CssClass="email" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="sWarehouseLenderCollateralPackageToCity" CssClass="city" ReadOnly="true"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="sWarehouseLenderCollateralPackageToState" CssClass="state" disabled="disabled" />
                    <asp:TextBox runat="server" ID="sWarehouseLenderCollateralPackageToZip" preset="longzipcode" CssClass="zip" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <label for="sWarehouseLenderCollateralPackageToPhone">Phone</label>
                </td>
                <td colspan="3">
                    <ml:PhoneTextBox runat="server" ID="sWarehouseLenderCollateralPackageToPhone" CssClass="phone" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="sWarehouseLenderCollateralPackageToFax">Fax</label>
                </td>
                <td >
                    <ml:PhoneTextBox runat="server" ID="sWarehouseLenderCollateralPackageToFax" CssClass="fax" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset style="margin-left: 10px">
        <legend>Closing package to</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <label for="sWarehouseLenderClosingPackageToAttention">Attention</label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="sWarehouseLenderClosingPackageToAttention" CssClass="attention" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <label for="sWarehouseLenderClosingPackageToContactName">Contact Name</label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="sWarehouseLenderClosingPackageToContactName" CssClass="contactname" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>            
                <td>
                    <label for="sWarehouseLenderClosingPackageToAddress">Address</label>
                </td>
                <td >
                    <asp:TextBox runat="server" ID="sWarehouseLenderClosingPackageToAddress" CssClass="address" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <label for="sWarehouseLenderClosingPackageToEmail">Email</label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="sWarehouseLenderClosingPackageToEmail" CssClass="email" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="sWarehouseLenderClosingPackageToCity" CssClass="city" ReadOnly="true"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="sWarehouseLenderClosingPackageToState" CssClass="state" disabled="disabled" />
                    <asp:TextBox runat="server" ID="sWarehouseLenderClosingPackageToZip" preset="longzipcode" CssClass="zip" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <label for="sWarehouseLenderClosingPackageToPhone">Phone</label>
                </td>
                <td colspan="3">
                    <ml:PhoneTextBox runat="server" ID="sWarehouseLenderClosingPackageToPhone" CssClass="phone" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="sWarehouseLenderClosingPackageToFax">Fax</label>
                </td>
                <td >
                    <ml:PhoneTextBox runat="server" ID="sWarehouseLenderClosingPackageToFax" CssClass="fax" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </fieldset>
    </form>
</body>
</html>
