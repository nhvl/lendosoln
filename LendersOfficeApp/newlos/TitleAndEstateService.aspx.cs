﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using System.Collections.Generic;
using DataAccess.Sellers;

namespace LendersOfficeApp.newlos
{
    public class TitleAndEstateServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(TitleAndEstateServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.aTitleNm1 = GetString("aTitleNm1");
            dataApp.aTitleNm1Lckd = GetBool("aTitleNm1Lckd");
            dataApp.aTitleNm2 = GetString("aTitleNm2");
            dataApp.aTitleNm2Lckd = GetBool("aTitleNm2Lckd");

            dataApp.aBIsCurrentlyOnTitle = this.GetEnum<E_TriState>("aBIsCurrentlyOnTitle");
            dataApp.aCIsCurrentlyOnTitle = this.GetEnum<E_TriState>("aCIsCurrentlyOnTitle");

            dataApp.aBCurrentTitleName = this.GetString("aBCurrentTitleName");
            dataApp.aBCurrentTitleNameLckd = this.GetBool("aBCurrentTitleNameLckd");

            dataApp.aCCurrentTitleName = this.GetString("aCCurrentTitleName");
            dataApp.aCCurrentTitleNameLckd = this.GetBool("aCCurrentTitleNameLckd");

            dataApp.aManner = GetString("aManner");

            dataLoan.sEstateHeldT = (E_sEstateHeldT)GetInt("sEstateHeldT");
            dataLoan.sNativeAmericanLandsT = this.GetEnum<NativeAmericanLandsT>("sNativeAmericanLandsT");
            dataLoan.sLeaseHoldExpireD_rep = GetString("sLeaseHoldExpireD");

            dataLoan.sDwnPmtSrc = GetString("sDwnPmtSrc");
            dataLoan.sDwnPmtSrcExplain = GetString("sDwnPmtSrcExplain");

            var jsonSerializedCollection = GetString("SellerInfoHolder");
            SellerCollection sellColl = ObsoleteSerializationHelper.JavascriptJsonDeserializer<SellerCollection>(jsonSerializedCollection);
            if (sellColl != null) dataLoan.sSellerCollection = sellColl;

            dataLoan.sSellerVesting = GetString("sSellerVesting");


            var jsonSerializedTitleOnlyBorrower = GetString("TitleOnlyBorrowerJSON");
            var titleBorrowers = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<TitleBorrower>>(jsonSerializedTitleOnlyBorrower);

            List<TitleBorrower> toRemvoe = new List<TitleBorrower>();

            titleBorrowers.ForEach(p =>
            {
                if (p.Id == Guid.Empty)
                {
                    throw CBaseException.GenericException("Something is blanking out the ID.");
                }
                if (string.IsNullOrEmpty(p.Email) && string.IsNullOrEmpty(p.FirstNm) && string.IsNullOrEmpty(p.MidNm) && string.IsNullOrEmpty(p.SSN))
                {
                    toRemvoe.Add(p);
                }
            });

            toRemvoe.ForEach(p => titleBorrowers.Remove(p));
            dataLoan.sTitleBorrowers = titleBorrowers; 
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("aTitleNm1", dataApp.aTitleNm1);
            SetResult("aTitleNm1Lckd", dataApp.aTitleNm1Lckd);
            SetResult("aTitleNm2", dataApp.aTitleNm2);
            SetResult("aTitleNm2Lckd", dataApp.aTitleNm2Lckd);

            this.SetResult("aBCurrentTitleName", dataApp.aBCurrentTitleName);
            this.SetResult("aCCurrentTitleName", dataApp.aCCurrentTitleName);

            this.SetResult("TitleOnlyBorrowerJSON", ObsoleteSerializationHelper.JavascriptJsonSerialize(dataLoan.sTitleBorrowers));
        }
    }

    public partial class TitleAndEstateService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new TitleAndEstateServiceItem());
        }
    }
}
