using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Reminders;
using LendersOffice.Security;
using MeridianLink.CommonControls;
using System.Linq;
using System.Linq.Expressions;
using CommonProjectLib.Common.Lib;

namespace LendersOfficeApp.newlos.Underwriting
{
	public partial class TaskConditions : BaseLoanPage
	{

		private TaskCondCache m_Cache = new TaskCondCache();
		private ChoiceList    m_Conds = new ChoiceList();

        // OPM 211800, 8/19/2015, ML
        // Closers will now be able to view the Conditions page without 
        // needing the AllowUnderwritingAccess permission. To support this, we
        // determine the required read/write permissions to have BaseLoanPage 
        // check based on the permissions that the user currently has. If the
        // user has AllowCloserRead, AllowCloserWrite, or AllowUnderwritingAccess,
        // they are authorized and BaseLoanPage does not have to check the permissions
        // again. If not, we return a default of AllowUnderwritingAccess and the check 
        // will fail.
        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                if (BrokerUser.HasPermission(Permission.AllowCloserRead) ||
                    BrokerUser.HasPermission(Permission.AllowUnderwritingAccess))
                {
                    return ConstApp.SkipConditionBasePagePermissionCheck;
                }

                return ConstApp.DefaultConditionPagePermission;
            }
        }

        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                if (BrokerUser.HasPermission(Permission.AllowCloserWrite) ||
                    BrokerUser.HasPermission(Permission.AllowUnderwritingAccess))
                {
                    return ConstApp.SkipConditionBasePagePermissionCheck;
                }

                return ConstApp.DefaultConditionPagePermission;
            }
        }

        public override bool IsReadOnly
        {
            get
            {
                return !(BrokerUser.HasPermission(Permission.AllowCloserWrite) ||
                    BrokerUser.HasPermission(Permission.AllowUnderwritingAccess));
            }
        }

		public bool CanViewHiddenInformation
		{
			get { return BrokerUser.HasPermission( Permission.CanViewHiddenInformation )
					  || BrokerUser.HasPermission( Permission.CanModifyLoanPrograms); }
		}
		protected bool m_Dirty
		{
			// Tell base that we need to set the page dirty.

			set
			{
				// The functions added through this interface
				// are written out during the base page's pre-
				// render method (which is called before ours).

				ViewState.Add( "Dirty" , value );
			}
			get
			{
				// Report what we last recorded.

				if( ViewState[ "Dirty" ] != null )
				{
					return Convert.ToBoolean( ViewState[ "Dirty" ] );
				}

				return false;
			}
		}

		private string ErrorMessage
		{
			set { Page.ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." ); }
		}

		/// <summary>
		/// Initialize this page.
		/// </summary>

		protected void PageInit( object sender, System.EventArgs a )
		{
			// Set printing framework entries.
			PageTitle = "Conditions";
			PageID = "UnderwritingConditions";
			PDFPrintClass = typeof( LendersOffice.Pdf.CConditionPDF );
			EnableJqueryMigrate = false;
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
        	this.RegisterCSS("jquery-ui-1.11.custom.css");

			// 06/27/06 mf - For batch adding at the client
			Page.ClientScript.RegisterHiddenField("BatchAddList", String.Empty);
            IsAlwaysSave = true;
			this.WarningLabel.Text=""; 
			this.WarningLabel.CssClass="hidden";



		}

		/// <summary>
		/// Render this page.
		/// </summary>

		protected void PagePreRender( object sender , System.EventArgs a )
		{

            CPageData d = CPageData.CreateUsingSmartDependency(LoanID, typeof(TaskConditions));
            d.InitLoad();

            ConditionChangeWarning.Visible = d.IsConditionLastModifiedDAfterApprovalCertLastSentD;

            if (d.IsTemplate == false)
            {
                CAppData data = d.GetAppData(0);
                Header.Title = string.Format("{0} - {1} - Conditions", data.aBLastFirstNm, d.sLNm); 
            }
			// Pump out the array of preset condition descriptions.
			try
			{
				// 12/22/2004 kb - Bind again before rendering because sorting events may have been handled
				// by the grid.  Remember to get the view.
                DataView view = ViewGenerator.Generate( m_Cache.Set , "IsValid = 1" );
                int maxCount = view.Count + 1;
				m_Grid.DataSource = view;
				m_Grid.DataBind();


                m_ConditionLength.Text = maxCount.ToString();
                m_InsertMultipleAt.Value = maxCount.ToString();

				// 12/14/2004 kb - Added delete command for batch
				// deleting so now we want to hide it when read
				// only.

				if( IsReadOnly == true )
				{
					m_DeletePanel.Visible = false;
				}
				else
				{
					m_DeletePanel.Visible = true;
				}

				// Inject the default condition choices for this broker into script so we can auto-init.
				if( m_Conds.Choices.Count == 0 )
				{
					m_EmptyMessage.Visible = true;
				}

				m_ChoiceList.DataSource = m_Conds.Choices;
				
				SortedList list = new SortedList();
				foreach (ChoiceItem item in m_Conds.Choices) 
				{
					if (item.Category != "") 
					{
						string key = item.Category.ToUpper().TrimWhitespaceAndBOM();
						if (!list.Contains(key))  
						{
							list.Add(key, item.Category);
						}
					}
                    
				}
				m_ConditionChoicesCategoryFilter.Items.Clear();
				m_ConditionChoicesCategoryFilter.Items.Add(new ListItem("All", "{ALL}"));
				m_ConditionChoicesCategoryFilter.Items.Add(new ListItem("(blank)", ""));
				foreach (string key in list.Keys) 
				{
					m_ConditionChoicesCategoryFilter.Items.Add(new ListItem((string) list[key], key));
				}
				m_ChoiceList.DataBind();

				// 10/10/07 mf. OPM 17983 Users who cannot see hidden conditions should not
				// toggle the hidden setting.
				m_Grid.Columns[7].Visible =  CanViewHiddenInformation;
                m_Grid.Columns[2].Visible = !IsReadOnly;
			}
			catch( Exception e )
			{
				// Oops!
				Tools.LogError( "Failed to render conditions page." , e );
				ErrorMessage = "Failed to prerender page.";
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Init += new System.EventHandler(this.PageInit);
			this.PreRender += new System.EventHandler(this.PagePreRender);

		}
		#endregion

		/// <summary>
		/// Handle viewstate for cached variables.
		/// </summary>

		protected override void LoadViewState( object oState )
		{
			// Get the view state of a previous postback and restore
			// the data grid's state prior to loading form variables.
			base.LoadViewState( oState );

			try
			{
				if( ViewState[ "Cache" ] != null )
				{
					//m_Cache = TaskCondCache.ToObject( ViewState[ "Cache" ].ToString() );
                    m_Cache = (TaskCondCache)ViewState["Cache"];
				}

				if( ViewState[ "Conds" ] != null )
				{
					//m_Conds = ChoiceList.ToObject( ViewState[ "Conds" ].ToString() );
                    m_Conds = (ChoiceList)ViewState["Conds"];
				}

				if( ViewState[ "Dirty" ] != null )
				{
					m_Dirty = Convert.ToBoolean( ViewState[ "Dirty" ] );
				}

                //note this is needed to handle any initial events such as delete if you dont bind here there will be nothing in the event
                m_Grid.DataSource = ViewGenerator.Generate( m_Cache.Set , "IsValid = 1" );
                m_Grid.DataBind();

			}
			catch( Exception e )
			{
				// Oops!
				Tools.LogError( "Failed to load viewstate." , e );
			}
		}

		/// <summary>
		/// Handle viewstate for cached variables.
		/// </summary>

		protected override object SaveViewState()
		{
			// Store what we have as latest in the viewstate so we can stay current between postbacks without
			// hitting the database.
            //ViewState.Add("Cache", m_Cache.ToString());
            //ViewState.Add("Conds", m_Conds.ToString());

            ViewState.Add("Cache", m_Cache);
            ViewState.Add("Conds", m_Conds);

			return base.SaveViewState();
		}

		/// <summary>
		/// Overload framework event for loading.
		/// </summary>

		protected override void LoadData() 
		{
			// Get the initial state of our conditions grid from the data base.
			LoanConditionSetObsolete ldSet = new LoanConditionSetObsolete();

			ldSet.Retrieve( LoanID, true, CanViewHiddenInformation, false );

			m_Cache.Set.Clear();

			foreach( CLoanConditionObsolete dCond in ldSet )
			{
				// Convert the discussion log to our condition descriptor.
				TaskCondDesc tcDesc = new TaskCondDesc();

				tcDesc.Category    = dCond.CondCategoryDesc;
				tcDesc.OrderRank   = dCond.CondOrderedPosition;
				tcDesc.Description = dCond.CondDesc;
				tcDesc.Notes       = dCond.CondNotes;
				tcDesc.IsHidden    = dCond.CondIsHidden;
				tcDesc.CreatedD    = Tools.GetDateTimeDescription( dCond.CreatedD );
                tcDesc.DoneBy      = dCond.CondCompletedByUserNm;

				if( dCond.CondStatus == E_CondStatus.Done )
				{
					tcDesc.DateDone = dCond.CondStatusDate.ToShortDateString();
					tcDesc.IsDone   = true;
				}

				tcDesc.Id = dCond.CondId;

				tcDesc.IsSaved = true;
				tcDesc.IsValid = true;


				m_Cache.Add( tcDesc );
			}

			// Get the current set of condition choices.
			// When we save, we will merge with the current
			// set so that we don't blow away any.  If any
			// body is editing in the admin inteface, then
			// we will probably lose some entries.

			m_Conds = Broker.ConditionChoices;

			// Bind what was retrieved -- whether cached or
			// loaded.

			m_Grid.DataSource = ViewGenerator.Generate( m_Cache.Set , "IsValid = 1" );
			m_Grid.DataBind();

		}

        private void SortConditions(List<string> keyOrder)
        {
            int count = keyOrder.Count;
            foreach (TaskCondDesc tc in m_Cache.Set)
            {
                int index = keyOrder.IndexOf(tc.Id.ToString());
                if (index != -1)
                {
                    tc.OrderRank = index;
                }
                else
                {
                    tc.OrderRank = count;
                    count++;
                }

                tc.IsDirty = true;
            }

        }


        /// <summary>
        /// Overload framework event for posting.
        /// </summary>

        protected override void PostData() 
		{
			// Write back the ui's latest state to our cached
			// list of descriptors.  Any changes to the text
			// or other controls is saved here.  We assume that
			// the form variables containing all the text data
			// were already applied to the grid.


			foreach( DataGridItem dgItem in m_Grid.Items )
			{
				EncodedLabel idKey = dgItem.FindControl( "Key" ) as EncodedLabel;
				if( idKey != null )
				{
					try
					{
						// Load current state from the posted-back text fields
						// of our data grid.

						Control c; 
                        TaskCondDesc tcItem = m_Cache[ new Guid( idKey.Text ) ];

						c = dgItem.FindControl( "Category" );

						if( c is TextBox == true && tcItem.Category != ( c as TextBox ).Text.TrimWhitespaceAndBOM() )
						{
							tcItem.Category = ( c as TextBox ).Text.TrimWhitespaceAndBOM();
							tcItem.IsDirty = true;
						}

						c = dgItem.FindControl( "Description" );

						if( c is TextBox == true && tcItem.Description != ( c as TextBox ).Text.TrimWhitespaceAndBOM() )
						{
							tcItem.Description = ( c as TextBox ).Text.TrimWhitespaceAndBOM();
							tcItem.IsDirty = true;
						}

						c = dgItem.FindControl( "Notes" );

						if( c is TextBox == true && tcItem.Notes != ( c as TextBox ).Text.TrimWhitespaceAndBOM() )
						{
							tcItem.Notes = ( c as TextBox ).Text.TrimWhitespaceAndBOM();
							tcItem.IsDirty = true;
						}

						c = dgItem.FindControl( "DateDone" );

						if( c is TextBox == true && tcItem.DateDone != ( c as TextBox ).Text.TrimWhitespaceAndBOM() )
						{
							tcItem.DateDone = ( c as TextBox ).Text.TrimWhitespaceAndBOM();
							tcItem.IsDirty = true;
						}

                        c = dgItem.FindControl("DoneBy");

                        if (c is TextBox == true && tcItem.DoneBy != (c as TextBox).Text.TrimWhitespaceAndBOM())
                        {
                            tcItem.DoneBy = (c as TextBox).Text.TrimWhitespaceAndBOM();
                            tcItem.IsDirty = true;
                        }

						c = dgItem.FindControl( "IsDone" );

						if( c is CheckBox == true && tcItem.IsDone != ( c as CheckBox ).Checked )
						{
							tcItem.IsDone = ( c as CheckBox ).Checked;
							tcItem.IsDirty = true;
						}

						c = dgItem.FindControl( "IsHidden" );

						if( c is CheckBox == true && tcItem.IsHidden != ( c as CheckBox ).Checked )
						{
							tcItem.IsHidden = ( c as CheckBox ).Checked;
							tcItem.IsDirty = true;
						}

						c = dgItem.FindControl( "CreatedD" );

						if( c is TextBox == true )
						{
							tcItem.CreatedD = ( c as TextBox ).Text.TrimWhitespaceAndBOM();

						}
					}
					catch( Exception e )
					{
						// Oops!

                        Tools.LogError("Failed to post rows to cached descriptors.", e);
                    }


                    //always sort on post data, the sorting is now its own event and it comes after this.
                    if (string.IsNullOrEmpty(m_ConditionOrder.Value) == false)
                    {
                        SortConditions(ObsoleteSerializationHelper.JsonDeserialize<List<string>>(m_ConditionOrder.Value));
                    }

				}
			}


			// 2/11/2005 kb - We're now dirty every time we post
			// back (because sort order is now persisted).

			m_Dirty = true;
		}

		/// <summary>
		/// Overload framework event for saving.
		/// </summary>


		protected override void SaveData()
		{
			// Write back the current state of the cached entries,
			// with the application of the form variables, to the
			// data base as discussion instances.
			//
			// 10/8/2004 kb - Save the condition choices first.
			// If we fail here, move on.
			//
			// 12/15/2004 kb - Skip saving because we are posting
			// back in response to a click on this page.  If Dave
			// ever passed in a special keyword in the postback
			// then I can key off that.  Until then, maintain all
			// the possible postback reasons here.

			if( Request.Form[ "__EVENTTARGET" ] != null && Request.Form[ "__EVENTTARGET" ] != "" )
			{
				return;
			}
			else if( Request.Form[ "m_Add" ] != null )
			{
				return;
			}
			else if( Request.Form[ "m_Del" ] != null )
			{
				return;
			}
			else if ( Request.Form[ "m_AddMultiple" ] != null )
			{
				return;
			}
			else if ( Request.Form[ "m_ImportFromTemplate" ] != null ) 
			{
				return;
			}
			else if ( Request.Form[ "m_RestoreDeleted" ] != null ) 
			{
				return;
			}

            Tools.LogInfo("Saving conditions. Condition choices: " + m_Conds.ToString());

            LoanConditionSetObsolete lcs = new LoanConditionSetObsolete();
            lcs.RetrieveAll(LoanID, true);
            Dictionary<Guid, CLoanConditionObsolete> conditions = new Dictionary<Guid,CLoanConditionObsolete>();
            List<Tuple<CLoanConditionObsolete, TaskCondDesc>> conditionsToSave = new List<Tuple<CLoanConditionObsolete,TaskCondDesc>>();

            foreach (CLoanConditionObsolete condition in lcs)
            {
                conditions.Add(condition.CondId, condition);
            }


            // 2/1/2005 kb - We update the database using the flags
            // we stored for each task.  This table shows what we'll
            // do for each case (note that is-saved denotes that the
            // instance was pulled from the database, not added during
            // this client session, and is-valid denotes an un-deleted
            // instance):
            //
            // 08/26/09 mf.  From user perspective, deleteing is a single action--
            // It does not matter if it was ever in DB or not.  So when something
            // was created and deleted in the same session, upon save, commit the
            // deleted to DB.
            //
            // IsSaved? IsValid? Action-to-do
            // ------------------------------
            //  0        0       create new (as deleted)
            //  0        1       create new
            //  1        0       remove
            //  1        1       update
            //
            // We don't bother handling the added instance that was
            // never saved, rather was deleted.  We should probably
            // remove them from the set on delete.

            DateTime dNow = DateTime.Now;



            foreach (TaskCondDesc tcItem in m_Cache.Set)
            {
                // 4/26/2005 kb - Per case #1653, we now only save
                // the tasks that are actually dirty.

                if (tcItem.IsDirty == false)
                {
                    continue;
                }

                // Create new task and prepare it for update.
                // If new, we make it and then update the
                // details using what was last cached from
                // the ui.  Only valid conditions are updated.
                //
                // 10/7/2004 kb - TODO: Consolidate the saves
                // in here to use a dicussion task specific
                // collection interface -- help keep this new
                // condition code colocated.
                //
                // 2/15/2005 kb - We no longer track updates
                // here.  We let the data layer manager tracking
                // updates and notifying employees.

                bool conditionIsSaved = tcItem.IsSaved;
                bool conditionIsValid = tcItem.IsValid;

                // If we are update existing, load it up
                // If we are new, create it
                CLoanConditionObsolete cdItem;
                if (conditionIsSaved)
                {
                    if (conditions.TryGetValue(tcItem.Id, out cdItem) == false)
                    {
                        throw new CBaseException(ErrorMessages.FailedToSave, "Failed to retrieve task condition '" + tcItem.Description + "'.");
                    }
                }
                else
                {
                    cdItem = new CLoanConditionObsolete(this.BrokerID);
                    cdItem.LoanId = LoanID;
                }

                // Set condition values from current UI state.
                cdItem.CondCategoryDesc = tcItem.Category;
                cdItem.CondDesc = tcItem.Description;
                cdItem.CondNotes = tcItem.Notes;
                cdItem.CondIsHidden = tcItem.IsHidden;
                cdItem.CondCompletedByUserNm = tcItem.DoneBy;
                cdItem.CondOrderedPosition = tcItem.OrderRank;
                cdItem.CondIsValid = conditionIsValid;

                if (tcItem.IsDone != true)
                {
                    cdItem.CondStatus = E_CondStatus.Active;
                }
                else
                {
                    cdItem.CondStatus = E_CondStatus.Done;

                    if (tcItem.DateDone != "")
                    {
                        cdItem.CondStatusDate = DateTime.Parse(tcItem.DateDone);
                    }
                    else
                    {
                        cdItem.CondStatusDate = DateTime.Now.Date;
                    }
                }

                if (!conditionIsValid)
                {
                    // 8/24/09 mf. OPM 32217.  If user has a deleted condition in cache,
                    // then they are the one who committed the delete.  Mark the condition.
                    cdItem.CondDeleteByUserNm = BrokerUser.DisplayName;
                    cdItem.CondDeleteD = dNow;
                }

                TaskCondDesc toSave = null;

                // Update client for new Conditions
                if (!conditionIsSaved)
                {
                    toSave = tcItem;
                }

                conditionsToSave.Add(Tuple.Create(cdItem, toSave));
            }


            using (CStoredProcedureExec exec = new CStoredProcedureExec(BrokerUser.BrokerId))
            {
                exec.BeginTransactionForWrite();
                try
                {
                    foreach (Tuple<CLoanConditionObsolete, TaskCondDesc> saveItem in conditionsToSave)
                    {
                        saveItem.Item1.Save(exec);
                        if (saveItem.Item2 != null)
                        {
                            saveItem.Item2.Id = saveItem.Item1.CondId;
                            saveItem.Item2.CreatedD = Tools.GetDateTimeDescription(dNow);
                        }
                    }

                    exec.CommitTransaction();
                }
                catch (Exception e)
                {
                    Tools.LogErrorWithCriticalTracking("Failed to save conditions", e);// because zombies
                    // also, always log exception first thing in catch block.

                    exec.RollbackTransaction();
                    ErrorMessage = "Failed to save.";
                    throw;
                }
            }

            foreach (Tuple<CLoanConditionObsolete, TaskCondDesc> saveItem in conditionsToSave)
            {
                if (saveItem.Item1.CondIsValid)
                {
                    saveItem.Item1.Signal(BrokerUser.DisplayName, dNow);
                }
            }


            // 07/27/07 av Added an array list in order to remove the conditions that were 
            // deleted because they still persisted after a save which would case the import 
            // to not work as it should. OPM 6489
            ArrayList list = new ArrayList(m_Cache.Set.Count);
            foreach (TaskCondDesc tcItem in m_Cache.Set)
            {
                tcItem.IsDirty = false;
                tcItem.IsSaved = true;
                if (tcItem.IsSaved && !tcItem.IsValid)
                    list.Add(tcItem);

            }
            // 07/27/07 av Remove deleted conditions from the cache.  OPM 6489
            foreach (TaskCondDesc task in list)
            {
                m_Cache.Remove(task);
            }

            m_Dirty = false;

        }

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void GridItemBound(object sender, DataGridItemEventArgs a)
		{
			// Bind the task condition descriptor.  We filter out
			// any guys that were already deleted.
			//
			// 1/6/2005 kb - We previously used task conditions
			// to show the grid, but we now convert to a data
			// view.  Use the right item.

			DataRowView dvItem = a.Item.DataItem as DataRowView;

			if(dvItem == null)
			{
				return;
			}

            ((CheckBox)a.Item.FindControl("IsDone")).Checked = (bool)dvItem["IsDone"];
            ((DateTextBox)a.Item.FindControl("DateDone")).Text = dvItem["DateDone"]?.ToString() ?? string.Empty;
            ((TextBox)a.Item.FindControl("DoneBy")).Text = dvItem["DoneBy"]?.ToString() ?? string.Empty;
            ((TextBox)a.Item.FindControl("Category")).Text = dvItem["Category"]?.ToString() ?? string.Empty;
            ((TextBox)a.Item.FindControl("Description")).Text = dvItem["Description"]?.ToString() ?? string.Empty;
            ((TextBox)a.Item.FindControl("Notes")).Text = dvItem["Notes"]?.ToString() ?? string.Empty;
            ((CheckBox)a.Item.FindControl("IsHidden")).Checked = (bool)dvItem["IsHidden"];
            ((TextBox)a.Item.FindControl("CreatedD")).Text = dvItem["CreatedD"]?.ToString() ?? string.Empty;

            // 2/24/2005 kb - We now support the concept of note-
            // task-conditions.  We key off the category.  If we
            // bind to one, we are to hide the date done and done
            // check box elements (the first 2 columns of the
            // grid).

            if ((bool)dvItem["IsNote"])
			{
				Control cHide;

				cHide = a.Item.FindControl("DateDone");
				if(cHide != null)
				{
					cHide.Visible = false;
				}

				cHide = a.Item.FindControl("IsDone");
				if(cHide != null)
				{
					cHide.Visible = false;
				}

                cHide = a.Item.FindControl("DoneBy");
                if (cHide != null)
                {
                    cHide.Visible = false;
                }
			} 
			// 08-07-07 av Disables the DateTextBox associated with a IsDone checkbox if IsDone is not true.
			// If IsDone is true the box is left alone. In case there was previous data and the IsDone is not set
			// DateBox text is set to empty. OPM 3317
			else 
			{
				CheckBox box = a.Item.FindControl("IsDone") as CheckBox;
                if (box != null)
                {
                    // 12/30/08 mf. OPM 26539. Here we must use the readonly client attribute
                    // instead of server ReadOnly property because we cannot use posted
                    // ReadOnly values in 2.0.
                   
                    // 05/22/09 mf. Per OPM 4425, DateBox is disabled at every render.  User can only change when
                    // client allows it (the item is newly marked as done), then it is locked until they remove it.
                    DateTextBox DateBox = a.Item.FindControl("DateDone") as DateTextBox;
                    if ( DateBox != null )
                        DateBox.Attributes.Add("readonly", "readonly");
                   
                    // Its read-only even when checked.  We lock the DateDone after the first time it is entered.
                    // User must uncheck and re-check if they want to changed it from this state.
                    if (!box.Checked)
                    {
                        DateBox.Text = String.Empty;
                    }
                    else
                    {
                        // For done conditions we disable Category, Description, and the "Choose" Link.
                        // Notes can still be edited.
                        WebControl c = a.Item.FindControl("Category") as TextBox;
                        if (c != null) c.Attributes.Add("readonly", "readonly");
                        c = a.Item.FindControl("Description") as TextBox;
                        if (c != null) c.Attributes.Add("readonly", "readonly");

                        HtmlControl hc = a.Item.FindControl("choose") as HtmlAnchor;
                        if (hc != null)
                        {
                            hc.Attributes.Add("disabled", "disabled");
                            hc.Attributes.Remove("onclick");
                        }

                    }
                }

			}


            // DoneBy and Created Date are always readonly
			TextBox tB = a.Item.FindControl("CreatedD") as TextBox;
			if (tB != null)
				tB.Attributes.Add("readonly", "readonly");

            tB = a.Item.FindControl("DoneBy") as TextBox;
            if (tB != null)
            {
                tB.Attributes.Add("readonly", "readonly"); 
            }
			

			// 12/15/2004 kb - Mark items as disabled if readonly is
			// in effect.

			Panel lPanel = a.Item.FindControl( "Links" ) as Panel;

			if( lPanel != null )
			{
				if( IsReadOnly == true )
				{
					lPanel.Visible = false;
				}
				else
				{
					lPanel.Visible = true;
				}
			}

            // 2/1/2005 kb - Associate the id as a key for the row
            // so we can pull it back out on grid item events.

            EncodedLabel lKey = a.Item.FindControl("Key") as EncodedLabel;

            if (lKey != null)
            {
                lKey.Text = dvItem["Id"].ToString();
            }

			
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void GridItemClick( object sender , System.Web.UI.WebControls.DataGridCommandEventArgs a )
		{

		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void DeleteClick( object sender , System.EventArgs a )
		{
			// Walk the grid and find all the selected elements
			// and delete the members.  The entry's key is stashed
			// in a named label that we hide in the html.

			foreach( DataGridItem dgItem in m_Grid.Items )
			{
				CheckBox isSelected = dgItem.FindControl( "Selected" ) as CheckBox;
                EncodedLabel idKey = dgItem.FindControl( "Key"      ) as EncodedLabel;

				if( isSelected == null || idKey == null )
				{
					continue;
				}

				if( isSelected.Checked == true )
				{
					// Remove the entry by marking it invalid.  We
					// want to keep it in the cache list so we can
					// delete it on save (and not before).  However,
					// the grid is coloring funny by hiding rows on
					// invalid items and making same colored rows
					// adjacent in the grid.  Best to move the marked
					// items to the end.

					try
					{
						TaskCondDesc tcItem = m_Cache[ new Guid( idKey.Text ) ];

						if( tcItem != null )
						{
							tcItem.IsValid = false;
							tcItem.IsDirty = true;
							m_Dirty = true;
						}
					}
					catch( Exception e )
					{
						// Oops!

						Tools.LogError( "Unable to delete selected condition." , e );
					}
				}
			}
		}

		protected void AddBatchClick( object sender, System.EventArgs a)
		{
			// 06/27/06 mf - OPM 3950.  We now allow the user to add multiple
			// conditions.  We maintain a list of the conditions and catagories
			// in a hidden field on the client.

			// Note that this only adds them to cache.  The user still has to
			// choose to save them or they will be lost.

            int maxSize = m_Cache.Set.Where(p => p.IsValid).Count() + 1;

            int insertPosition;
               
            if (false == Int32.TryParse(m_InsertMultipleAt.Value, out insertPosition) || insertPosition < 1 || insertPosition > maxSize)
            {
                ErrorMessage = "Invalid insert position.";
                return;
            }

            insertPosition -= 1;   //client only to used 1 index not zero
			if (Request.Form["BatchAddList"] != null)
			{
                //It looks like in the javascript the batch list is generated each condition takes up 2 spots in the array. 
                //They are comma seperated and commans are escaped.
				string[] listToAdd = ((string) Request.Form["BatchAddList"]).Split(',');

                if (listToAdd.Length % 2 != 0)
                {
                    ErrorMessage = "Page error - Invalid Batch Add Data";
                    return;
                }

                int entriesToAdd = listToAdd.Length / 2;


                foreach (TaskCondDesc task in m_Cache.Set)
                {
                    //since we are inserting at the position we need to push that appears at that position up by the number of items we need to add 
                    if (task.OrderRank >= insertPosition)
                    {
                        task.OrderRank += entriesToAdd; 
                    }
                }

				for( int i = 1; i < listToAdd.Length; i+=2)
				{
					TaskCondDesc tcD = new TaskCondDesc();

					tcD.Id = Guid.NewGuid();
					tcD.Category = listToAdd[i-1].Replace("&comma;", ",");
					tcD.Description = listToAdd[i].Replace("&comma;", ",");
					tcD.IsDone     = false;
                    tcD.DoneBy = string.Empty;
					tcD.IsSaved = false;
					tcD.IsDirty = true;
					tcD.IsValid = true;
                    tcD.OrderRank = insertPosition;
                    insertPosition++;
                    m_Cache.Add( tcD );

				}
				
				// We currently set as dirty on every postback, but
				// in case that changes, we insure setting here.
				m_Dirty = m_Dirty || ( listToAdd.Length > 1 );

			}
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void AddClick( object sender , System.EventArgs a )
		{
			// Create empty, default task condition and add it to
			// our cache.

			TaskCondDesc tcD = new TaskCondDesc();

			tcD.Id = Guid.NewGuid();
			tcD.Category = "";
			tcD.IsDone     = false;
            tcD.DoneBy = "";
			tcD.IsSaved = false;
			tcD.IsDirty = true;
			tcD.IsValid = true;
            tcD.OrderRank = m_Cache.Set.Where(p => p.IsValid == true).Count();
			m_Cache.Add( tcD );

			m_Dirty = true;
		}

		// 01/23/07 mf. OPM 7325
		protected void RestoreDeletedClick( object sender, System.EventArgs a) 
		{
			// Load Condition.  Put values into cache item.

			foreach (string id in m_RestoreConditions.Value.Split(',') )
			{
				CLoanConditionObsolete condition = new CLoanConditionObsolete( this.BrokerID, new Guid(id) );
				condition.Retrieve();
				
				TaskCondDesc tcD = new TaskCondDesc();
				tcD.Id = new Guid(id);
				tcD.Category = condition.CondCategoryDesc;
				tcD.Description = condition.CondDesc;
				tcD.Notes = condition.CondNotes;
				tcD.IsHidden = condition.CondIsHidden;
				tcD.IsDone     = condition.CondStatus == E_CondStatus.Done;
				tcD.DateDone = (tcD.IsDone) ?  condition.CondStatusDate.ToShortDateString() : "";
                tcD.DoneBy = condition.CondCompletedByUserNm;
				tcD.CreatedD = Tools.GetDateTimeDescription( condition.CreatedD );
				tcD.IsSaved = true;
				tcD.IsDirty = true;
				tcD.IsValid = true;
                tcD.OrderRank = m_Cache.Set.Count;
				m_Cache.Add(tcD);
			}

		}

        protected void Grid_OnSortCondition(object sender, DataGridSortCommandEventArgs args)
        {
            List<TaskCondDesc> items = m_Cache.Set.ToList();
            List<string> sortedList = new List<string>(items.Count);
         

            switch (args.SortExpression)
            {
                case "Condition":
                    if (ConditionSortOrder.Value == "ASC")
                    {
                        items = items.OrderBy(p => p.Description).ToList();
                        ConditionSortOrder.Value = "DESC";
                    }
                    else
                    {
                        items = items.OrderByDescending(p => p.Description).ToList();
                        ConditionSortOrder.Value = "ASC";
                    }

                    break;
                case "Category":
                    if (CategorySortOrder.Value == "ASC")
                    {
                        items = items.OrderBy(p => p.Category).ToList();
                        CategorySortOrder.Value = "DESC";
                    }
                    else
                    {
                        items = items.OrderByDescending(p => p.Category).ToList();
                        CategorySortOrder.Value = "ASC";
                    }
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Did not handle a sort type");
            }

            foreach (TaskCondDesc d in items)
            {
                sortedList.Add(d.Id.ToString());
            }

            SortConditions(sortedList);
        }

		/// <summary>
		/// ImportClick is called after the user choses a template from another dialog. 
		/// Added for OPM 6489 av
		/// </summary>
		protected void ImportClick( object sender, System.EventArgs a) 
		{
			if ( Request.Params["m_ImportTemplateId"] == null ) 
				return; 

			LoanConditionSetObsolete templateLdSet = new LoanConditionSetObsolete();
			try 
			{
				// 10/12/07 mf. OPM 18269. Users can only pull condtitions from template
				// that they would see in the template.
				templateLdSet.Retrieve( new Guid(Request.Params["m_ImportTemplateId"]), true, CanViewHiddenInformation, false);
			} 
			catch ( Exception e ) 
			{
				Tools.LogError("Could not retrieve Loan ", e);
				WarningLabel.Text = ErrorMessages.FailedToLoadTemplateConditions(Request.Params["m_ImportTemplateName"]);
				WarningLabel.CssClass = "warning";
				return;
			}
						
			LoanConditionSetObsolete tempConditionSet = new LoanConditionSetObsolete(); 
			bool duplicatesExist = false;
            int count = m_Cache.Set.Count;
			foreach ( CLoanConditionObsolete dCond in templateLdSet ) 
			{
				if (! this.m_Cache.Contains(dCond.CondCategoryDesc, dCond.CondDesc ) ) 
				{
					TaskCondDesc tcD = new TaskCondDesc();
					tcD.Id = Guid.NewGuid();
					tcD.Category = dCond.CondCategoryDesc;
					tcD.Description = dCond.CondDesc;
					tcD.IsHidden = dCond.CondIsHidden;
					tcD.IsDone     = false;
                    tcD.DoneBy = string.Empty;
					tcD.IsSaved = false;
					tcD.IsDirty = true;
					tcD.IsValid = true;
                    if (tcD.Category.Equals("warning", StringComparison.InvariantCultureIgnoreCase))
                    {
                        tcD.OrderRank = -1;
                    }
                    else
                    {
                        tcD.OrderRank = count;
                        count += 1;
                    }
                    tempConditionSet.Add(tcD);
				}
				else 
				{
					duplicatesExist = true;
				
				}
			}
			if ( duplicatesExist ) 
			{
				WarningLabel.Text="Duplicate conditions were not imported!<br/>";	
			}
			
			WarningLabel.Text = 
				String.Format("{0}{1} out of {3} conditions were imported from {2}.",
				WarningLabel.Text,
				tempConditionSet.Count, 
				Request.Params["m_ImportTemplateName"], 
				templateLdSet.Count);
			WarningLabel.CssClass="warning"; 
			foreach ( TaskCondDesc temp in tempConditionSet ) 
			{
				m_Cache.Add(temp);
			}
			m_Dirty = true;
		}
	}
	


	/// <summary>
	/// Store condition details.  We map these members onto the task
	/// instance when we save.
	/// </summary>
	[ XmlRoot ]
    [Serializable]
	public class TaskCondDesc
	{
		/// <summary>
		/// Store condition details.  We map these members onto
		/// the task instance when we save.
		/// </summary>

		private string    m_Category = "";
		private string m_Description = "";
		private string       m_Notes = "";
		private string    m_DateDone = "";
		private bool     m_IsDone = false;
		private bool    m_IsValid = false;
		private bool    m_IsSaved = false;
		private bool    m_IsDirty = false;
		private Int32    m_OrderRank = 0;
		private Guid            m_Id = Guid.Empty;
		private bool m_IsHidden = false;
		private string    m_CreatedD = "";
        private string m_DoneBy = "";

        

		#region ( Condition properties )

		[ XmlIgnore ]
		public Boolean IsNote
		{
			get
			{
				if( m_Category.ToLower().TrimWhitespaceAndBOM() == "notes" )
				{
					return true;
				}

				return false;
			}
		}

		[ XmlAttribute ]
		public String Category
		{
			set { m_Category = value; }
			get { return m_Category; }
		}

		[ XmlElement ]
		public String Description
		{
            set { m_Description = Tools.ReplaceInvalidUnicodeChars(value); }
			get { return m_Description; }
		}

		[ XmlElement ]
		public String Notes
		{
			set { m_Notes = value; }
			get { return m_Notes; }
		}

		[ XmlElement ]
		public String DateDone
		{
			set { m_DateDone = value; }
			get { return m_DateDone; }
		}

        [XmlIgnore]
        public DateTime SortDateDone
        {
            get
            {
                try
                {
                    return DateTime.Parse(m_DateDone);
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
        }

        [ XmlElement ]
		public Boolean IsDone
		{
			set { m_IsDone = value; }
			get { return m_IsDone; }
		}

		[ XmlAttribute ]
		public Boolean IsValid
		{
			set { m_IsValid = value; }
			get { return m_IsValid; }
		}

		[ XmlAttribute ]
		public Boolean IsSaved
		{
			set { m_IsSaved = value; }
			get { return m_IsSaved; }
		}

		[ XmlAttribute ]
		public Boolean IsDirty
		{
			set { m_IsDirty = value; }
			get { return m_IsDirty; }
		}

		[ XmlAttribute ]
		public Int32 OrderRank
		{
			set { m_OrderRank = value; }
			get { return m_OrderRank; }
		}

		[ XmlAttribute ]
		public Guid Id
		{
			set { m_Id = value; }
			get { return m_Id; }
		}

		[ XmlAttribute ]
		public Boolean IsHidden
		{
			set { m_IsHidden = value; }
			get { return m_IsHidden; }
		}

		[ XmlAttribute ]
		public String CreatedD
		{
			set { m_CreatedD = value; }
			get { return m_CreatedD; }
		}

        [XmlAttribute]
        public String DoneBy
        {
            set { m_DoneBy = value; }
            get { return m_DoneBy; }
        }


        [XmlIgnore]
        public DateTime SortCreatedD
        {
            get
            {
                try
                {
                    return DateTime.Parse(m_CreatedD);
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
        }


		#endregion

	}

	/// <summary>
	/// Keep a list of the condition task descriptors that we
	/// maintain during the lifetime of this page.
	/// </summary>

	[ XmlRoot ]
    [Serializable]
	public class TaskCondCache
	{
		/// <summary>
		/// Keep a list of the condition task descriptors
		/// that we maintain during the lifetime of this
		/// page.
		/// </summary>

		private List<TaskCondDesc> m_Set = new List<TaskCondDesc>();

        [NonSerialized]
        private Dictionary<Guid, TaskCondDesc> x_taskCondDescDictionary = new Dictionary<Guid, TaskCondDesc>();

        private Dictionary<Guid, TaskCondDesc> TaskCondDescDictionary
        {
            get
            {
                if (x_taskCondDescDictionary == null)
                {
                    x_taskCondDescDictionary = new Dictionary<Guid, TaskCondDesc>();
                    foreach (TaskCondDesc taskCondDesc in m_Set)
                    {
                        x_taskCondDescDictionary.Add(taskCondDesc.Id, taskCondDesc);
                    }
                }
                return x_taskCondDescDictionary;
            }
        }
        private void ClearTaskCondDescDictionary()
        {
            x_taskCondDescDictionary = null;
        }

		[ XmlIgnore ]
		public TaskCondDesc this[ Guid taskId ]
		{
			get
			{
                return TaskCondDescDictionary[taskId];
			}
		}

		[ XmlArray ]
		[ XmlArrayItem( typeof( TaskCondDesc ) ) ]
		public List<TaskCondDesc> Set
		{
			get
			{
                return m_Set.OrderBy(p => p.OrderRank).ThenBy(p =>p.Category.ToLower()).ThenBy(p=>p.Description.ToLower()).ToList();
			}
		}

		/// <summary>
		/// Remove the instance from our set and table.
		/// </summary>

		public void Remove( TaskCondDesc tDesc )
		{
			// Remove the instance from our set and table.
            TaskCondDescDictionary.Remove(tDesc.Id);
			m_Set.Remove( tDesc );
		}

		/// <summary>
		/// Append new entry onto our set.  Only unique entries
		/// get appended.
		/// </summary>

		public void Add( TaskCondDesc tDesc )
		{
            if (tDesc == null || TaskCondDescDictionary.ContainsKey(tDesc.Id) == true)
            {
                return;
            }
            TaskCondDescDictionary.Add(tDesc.Id, tDesc);

			m_Set.Add( tDesc );
		}


		/// <summary>
		/// Checks to see if the given task description and category already exist in the cache. 
		/// Since deleted items still remain in the cache Contains only checks undeleted items or items 
		/// that are no longer valid
		/// 07/27/07 av opm  
		/// </summary>
		/// <param name="sCategory">The category of the condition.</param>
		/// <param name="sDescription">String represetation of the description </param>
		/// <returns>Whether there exist a condition with the same name  and description which is not mark for deletion.</returns>
		public bool Contains( string sCategory, string sDescription ) 
		{
			bool conditionExist =false;
			foreach ( TaskCondDesc tDesc in m_Set ) 
			{
				if ( tDesc.Category.ToLower() == sCategory.ToLower() && tDesc.Description.ToLower() == sDescription.ToLower() && tDesc.IsValid) 
				{
					conditionExist= true;
					break;
				}
			}
			return conditionExist; 
		}

	}
}
