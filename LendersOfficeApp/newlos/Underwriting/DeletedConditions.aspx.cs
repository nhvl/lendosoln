using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Reminders;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Underwriting
{
	/// <summary>
	/// Summary description for DeletedConditions.
	/// </summary>
	public partial class DeletedConditions : LendersOffice.Common.BasePage
	{

		public bool CanViewHiddenInformation
		{
			get 
			{
				return BrokerUserPrincipal.CurrentPrincipal.HasPermission( Permission.CanViewHiddenInformation)
					  || BrokerUserPrincipal.CurrentPrincipal.HasPermission( Permission.CanModifyLoanPrograms ); }
		}

		
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here


			// Would be a good idea here to double-check that the user has the
			// right to do this.

			BindConditionGrid();

			m_GridPanel.Visible = m_DeletedConditionsDG.Items.Count > 0;
			m_EmptyPanel.Visible = !m_GridPanel.Visible;

			m_DeletedConditionsDG.Columns[4].Visible = CanViewHiddenInformation;
			
		}

		protected void PagePreRender(object sender, System.EventArgs e)
		{
			// If event occured, grid must be re-bound to reflect latest status.

			if( Request["m_RemoveSelected"] != null )
				BindConditionGrid();
		}

		private void BindConditionGrid()
		{

			LoanConditionSetObsolete deletedLoans = new LoanConditionSetObsolete();
			deletedLoans.Retrieve( 
				new Guid( RequestHelper.GetSafeQueryString("loanid") )
				, false
				, CanViewHiddenInformation
				, true
				);

			m_DeletedConditionsDG.DataSource = LendersOffice.Common.ViewGenerator.Generate( deletedLoans.Digest );//conditionSet.Tables[0].DefaultView;
			m_DeletedConditionsDG.DataBind();

		}
		
		
		protected void RemoveConditionsClick( object sender , System.EventArgs a )
		{	
			// We would need a safeway to handle this.
			// Trouble would occur if a person restored a condition (to cache in Condition editor)
			// Then deleted here.  If they re-saved the cached condition, we would fail.
			// We might want some way to communicate what is in cache, and not
			// show it here.

			using( CStoredProcedureExec spExec = new CStoredProcedureExec(BrokerUserPrincipal.CurrentPrincipal.BrokerId) )
			{
				spExec.BeginTransactionForWrite();
				
				foreach( string id in m_Selected.Value.Split(',') )
				{
					CLoanConditionObsolete.DeleteLoanCondition( spExec, new Guid(id), new Guid( RequestHelper.GetSafeQueryString("loanid") ) );
				}
				
				spExec.CommitTransaction(); // Remember that SqlTransaction will Rollback anything uncommited on dispose.
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.PreRender += new System.EventHandler(this.PagePreRender);
		}
		#endregion
	}
}
