﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using DataAccess;
using DataAccess.LoanValidation;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.RatePrice;

namespace LendersOfficeApp.newlos.Underwriting
{
    public partial class CheckEligibility : BaseLoanPage
    {
        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { }; // See the PageInit method. [BB] 11/4/13 
            }
        }

        private E_sPricingModeT sPricingModeT
        {
            get { return (E_sPricingModeT)RequestHelper.GetInt("sPricingModeT", 0); }
        }
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(CheckEligibility));
            dataLoan.InitLoad();

            var resultList = Tools.RunInternalPricingValidation(dataLoan, this.BrokerUser);
            if (resultList.ErrorCount > 0)
            {
                Response.Redirect("~/newlos/InternalPricingValidationErrorPage.aspx?loanid=" + LoanID);
                return;
            }
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
            Tools.SetDropDownListValue(sInvestorLockLpePriceGroupId
                    , (dataLoan.sInvestorLockLpePriceGroupId == Guid.Empty
                        ? dataLoan.sProdLpePriceGroupId
                        : dataLoan.sInvestorLockLpePriceGroupId
                      ).ToString()
                );            

            sLpTemplateNm.Text = dataLoan.sLpTemplateNm;
            sLpProductType.Text = dataLoan.sLpProductType;
            sTerm.Text = dataLoan.sTerm_rep;
            sDue.Text = dataLoan.sDue_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sRLckdD.Text = dataLoan.sRLckdD_rep;
            sRLckdDays.Text = dataLoan.sRLckdDays_rep;
            sRLckdExpiredD.Text = dataLoan.sRLckdExpiredD_rep;
            sRLckdExpiredInDays.Text = dataLoan.sRLckdExpiredInDays_rep;
            sProdRLckdDays.Text = dataLoan.sProdRLckdDays_rep;

            // This will only display the value if the lender is configured to
            // display the price group in Embedded PML.
            sProdLpePriceGroupNm.Text = dataLoan.sProdLpePriceGroupNm;

            sProdInvestorRLckdDays.Text = dataLoan.sProdInvestorRLckdDays_rep;
            sProdInvestorRLckdExpiredD.Text = dataLoan.sProdInvestorRLckdExpiredD_rep;

            if (dataLoan.sProdInvestorRLckdModeT == E_sProdInvestorRLckdModeT.RateLockPeriod)
            {
                m_editRatePeriod.Checked = true;
            }
            else if (dataLoan.sProdInvestorRLckdModeT == E_sProdInvestorRLckdModeT.RateLockExpiredDate)
            {
                m_editRateDate.Checked = true;
            }

            switch (sPricingModeT)
            {
                case E_sPricingModeT.Undefined:
                case E_sPricingModeT.RegularPricing:
                case E_sPricingModeT.InternalBrokerPricing:
                case E_sPricingModeT.EligibilityBrokerPricing:
                    m_pricingOptionBroker.Checked = true;
                    break;
                case E_sPricingModeT.InternalInvestorPricing:
                case E_sPricingModeT.EligibilityInvestorPricing:
                    m_pricingOptionInvestor.Checked = true;
                    break;
                default:
                    break;
            }
            //.Text = dataLoan.;
            if (IsTestHistorical)
            {
                if (dataLoan.sHistoricalPricingVersionD == DateTime.MinValue)
                {
                    sHistoricalPricingVersionD.Text = string.Empty;

                    historicalPolicy.Items.RemoveAt(1); // Remove Historical option.
                    historicalRateOption.Items.RemoveAt(1); // Remove Historical option.
                }
                else
                {
                    sHistoricalPricingVersionD.Text = dataLoan.sHistoricalPricingVersionD.ToString("MM-dd-yyyy hh:mm:ss tt");
                }
            }
        }

        private void TempSetupHistoricalPricing()
        {
            tmpHistoricalTable.Visible = true;
            BindHistoricalDropDown(historicalPolicy);
            BindHistoricalDropDown(historicalRateOption);
        }
        private void BindHistoricalDropDown(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("Current Snapshot", E_LoanProgramSetPricingT.Snapshot.ToString("D")));
            ddl.Items.Add(new ListItem("Historical", E_LoanProgramSetPricingT.Historical.ToString("D")));
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Check Eligibility";
            this.PageID = "CheckEligibility";
            this.DisplayCopyRight = false;

            Tools.Bind_sFinMethT(sFinMethT);

            foreach (var priceGroup in Broker.ListEnablePriceGroups())
            {
                sInvestorLockLpePriceGroupId.Items.Add(new ListItem(priceGroup.Value, priceGroup.Key.ToString()));
            }

            if (IsTestHistorical)
            {
                TempSetupHistoricalPricing();
            }
            // Permission checks are made here rather than in the base loan so that the U/W folder access permission can be OR'd with employee group membership.
            // Members of the Check Eligibility group (such as processors) can access the page w/o accessing the entire U/W folder.
            if (!BrokerUser.HasPermission(Permission.AllowUnderwritingAccess)
                && !BrokerUser.IsInEmployeeGroup(ConstAppDavid.CheckEligibilityEmployeeGroup))
            {
                if (Broker.IsEnableBigLoanPage)
                {
                    Response.Redirect("~/newlos/BigLoanInfo.aspx?loanid=" + LoanID + "&appid=" + ApplicationID, true);
                }
                else
                {
                    Response.Redirect("~/newlos/LoanInfo.aspx?loanid=" + LoanID + "&appid=" + ApplicationID, true);
                }
                return;
            }
        }
        protected bool IsTestHistorical
        {
            get { return this.Broker.IsEnableHistoricalPricing; }
        }
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
