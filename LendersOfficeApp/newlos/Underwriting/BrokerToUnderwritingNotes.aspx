<%@ Page language="c#" Codebehind="BrokerToUnderwritingNotes.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Underwriting.BrokerToUnderwritingNotes" %>
<%@ Import Namespace="LendersOffice.AntiXss"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>BrokerToUnderwritingNotes</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
<link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </HEAD>
  <body MS_POSITIONING="FlowLayout" leftmargin=10>

    <form id="BrokerToUnderwritingNotes" method="post" runat="server">
<TABLE id=Table1 cellSpacing=0 cellPadding=0 width="100%" border=0 style="padding-left:10px;padding-top:10px;table-layout: fixed">
  <TR>
    <TD class=FieldLabel style="font-size:18px" align=center>Message to Lender</TD></TR>
    <tr><td>&nbsp;</td></tr>
  <TR>
    <TD style="white-space: pre-wrap; word-wrap: break-word;"><ml:EncodedLiteral id=sLpeNotesFromBrokerToUnderwriterHistory runat="server" EnableViewState="False" ></ml:EncodedLiteral></TD></TR></TABLE>
     </form>
	
  </body>
</HTML>
