<%@ Page language="c#" Codebehind="DeletedConditions.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Underwriting.DeletedConditions" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML> 
<html>
  <head>
    <title>Deleted Conditions</title>
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro" margin=0 scroll=no>
<script type="text/javascript">
function _init() {
  <% if( !Page.IsPostBack ) { %>
  resize(750, 550);
  <% } %>
  f_calculateSelected();
}
function f_close(bOk) {
    var arg = {};
  arg.OK = bOk;
  
  if (bOk) {
    arg.Ids = document.getElementById('m_Selected').value;
  }
 
  onClosePopup(arg);
}
function f_calculateSelected()
{
    var table = <%= AspxTools.JsGetElementById(m_DeletedConditionsDG) %>;
    var ids = '';
    var selectionList = document.getElementById('m_Selected');
    if (table)
    {
      var inputs = table.getElementsByTagName('input');
      for(var i = 0; i < inputs.length; i++)
      {
        input = inputs[i];    
        if (input.type && input.type == 'checkbox' && input.id != 'GroupSelectCB' && !input.disabled && input.checked)
        {
          ids += ((ids.length == 0) ? "" : ",") + input.id;
        }
      }
    }
    selectionList.value = ids;
    document.getElementById('RestoreBtn').disabled = ids == '';
}
function f_onSelectAll( bSet )
{
    var table = <%= AspxTools.JsGetElementById(m_DeletedConditionsDG) %>;
    var inputs = table.getElementsByTagName('input');
    for(var i = 0; i < inputs.length; i++)
    {
      input = inputs[i];    
      if (input.type && input.type == 'checkbox' && !input.disabled )
      {
        input.checked = bSet;        
      }
    }
    f_calculateSelected();
}
</script>
    <h4 class="page-header">Deleted Conditions</h4>
    <form id="DeletedConditions" method="post" runat="server">
    <input id="m_Selected" runat="server" type="hidden" value="" />
  <table id=Table1 cellSpacing=0 cellPadding=3 width="100%" border=0  style="MARGIN:0px">
  <tr>
    <td noWrap>
    <asp:Panel ID="m_GridPanel" Runat="server" style="HEIGHT: 400px; overflow-y: scroll">
    <ml:CommonDataGrid ID="m_DeletedConditionsDG" Runat="server" AutoGenerateColumns="False" Width="100%">
    <Columns>
      <asp:TemplateColumn>
        <HeaderTemplate>
          <input type='checkbox' id='GroupSelectCB' onclick='f_onSelectAll(this.checked);'>
        </HeaderTemplate>
        <ItemTemplate>
          <input type="checkbox" id=<%#AspxTools.HtmlAttribute(Eval("Id").ToString())%> onclick="f_calculateSelected();" >
        </ItemTemplate>
      </asp:TemplateColumn>
      <asp:TemplateColumn HeaderText="Date Done" SortExpression="IsDone, DateDone" >
        <ItemStyle Wrap="false" />
        <ItemTemplate>
          <input type="checkbox" disabled="disabled" <%#AspxTools.HtmlString((bool)Eval("IsDone") ? "checked" : "")%> >
          <input type="text" readonly="true" style="WIDTH:78px" value=<%#AspxTools.HtmlAttribute((string)Eval("DateDone"))%> >
        </ItemTemplate>
      </asp:TemplateColumn>
      <asp:BoundColumn HeaderText="Category" DataField="Category" SortExpression="Category" />
      <asp:BoundColumn HeaderText="Condition" DataField="Description" SortExpression="Description" />
      <asp:TemplateColumn HeaderText="Hidden" SortExpression="IsHidden" >
        <ItemTemplate>
          <input type="checkbox" disabled="disabled" <%#AspxTools.HtmlString((bool)Eval("IsHidden") ? "checked" : "")%> >
        </ItemTemplate>
      </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Deleted Date" >
            <ItemTemplate>
            <div style="width:165px">
            <input type="text" readonly="readonly" style="WIDTH:150px" value=<%#AspxTools.HtmlAttribute((string)Eval("CondDeleteD"))%> /> 
            <input type="text" readonly="readonly" style="WIDTH:150px" value=<%#AspxTools.HtmlAttribute((string)Eval("CondDeleteByUserNm"))%> />
            </div>
            </ItemTemplate>
        </asp:TemplateColumn>
    </Columns>
    </ml:CommonDataGrid>
    </asp:Panel>
    <asp:Panel ID="m_EmptyPanel" Runat="server" HorizontalAlign="Center" style="HEIGHT: 400px; overflow-y: scroll" >No Deleted Conditions.</asp:Panel>
    </td>
  </tr>
  <tr>
    <td noWrap align=center>
      <input id="RestoreBtn" class=ButtonStyle onclick=f_close(true); type=button value="Restore Selected Conditions">&nbsp; 
      <%--<asp:Button CssClass="ButtonStyle" ID="m_RemoveSelected" Runat="server" Text="Remove Selected Conditions" OnClick="RemoveConditionsClick" />--%>
      <input class=ButtonStyle style="WIDTH: 55px" onclick=f_close(false); type=button value=" Cancel ">
    </td>
  </tr>
  </table>
	<uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
     </form>
  </body>
</html>
