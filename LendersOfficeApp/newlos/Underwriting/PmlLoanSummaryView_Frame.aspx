<%@ Page language="c#" Codebehind="PmlLoanSummaryView_Frame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Underwriting.PmlLoanSummaryView_Frame"  EnableEventValidation="false" enableViewState="False"%>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<html>
	<head>
		<title>PML Summary</title>
		<link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet"/>
		<style type="text/css">
		    body { background-color: #DCDCDC; font-weight:bold; overflow: hidden;  } 
		    .ButtonStyle { margin-right: 5px; width: 150px; }
		    #buttonSection { padding: 5px 0 20px 5px; }
		    p{ margin:0; padding:0; }
		    ul { margin: 0; padding: 10px 20px 10px 0; list-style-type: none; } 
		    li { display: inline; }
		    .header { font-size: 1.2em; }
		</style>
		<script type="text/javascript">
		        var certificateSaved = false;
		        function f_save() {
		            
		            var args = new Object();
		            args.LoanID = <%= AspxTools.JsString(RequestHelper.LoanID) %>;
		            args.DisplaySubmissionAgreement = <%= AspxTools.JsGetElementById(DisplaySubmissionAgreement) %>.checked ? "True" : "False";
			        args.DisplayMessageToUnderwriter = <%= AspxTools.JsGetElementById(DisplayMessageToUnderwriter) %>.checked ? "True" : "False";
			        args.DisplayPricing = "False";
			        args.DisplayUnderwritingSignature = <%= AspxTools.JsGetElementById(DisplayUnderwriterSignature) %>.checked ? "True" : "False";
		            args.FormType = "pmlsummary";
		            args.Notes = "";
		            
		            var result = gService.main.call("Save", args);
			        if (result.error) {
			            alert('Unable to save form. \n\n' + result.UserMessage);
			        }
			        
			        certificateSaved = true;
			        var saveBtn = document.getElementById("saveBtn");
			        saveBtn.disabled = true;
			        saveBtn.blur();
			        // perhaps we should display some kind of message that it has been saved
			        document.getElementById("printBtn").value = "Print...";
			        document.getElementById("emailBtn").value = "Email...";
			        
			    }
			    
			    function f_print() {
			        if (!certificateSaved) {
			            f_save();
			        }
			        
			        lqbPrintByFrame(frmBody);
			    }
			   
			    function f_email() {
			        if (!certificateSaved) {
                        f_save();
                    }

			        var args = new Object();
			        args.DisplaySubmissionAgreement = <%= AspxTools.JsGetElementById(DisplaySubmissionAgreement) %>.checked ? "True" : "False";
			        args.DisplayMessageToUnderwriter = <%= AspxTools.JsGetElementById(DisplayMessageToUnderwriter) %>.checked ? "True" : "False";
			        args.DisplayPricing = "False";
			        args.DisplayUnderwritingSignature = <%= AspxTools.JsGetElementById(DisplayUnderwriterSignature) %>.checked ? "True" : "False";
			        args.FormType = "pmlsummary";

			        showModal(<%= AspxTools.JsString("/newlos/Underwriting/PmlLoanSummaryEmailWithComment.aspx?loanid=" + RequestHelper.LoanID) %>, args, null, null, null,{hideCloseButton:true});
			    }

			    function f_onChangeDisplay() {
			        var submission = <%= AspxTools.JsGetElementById(DisplaySubmissionAgreement) %>.checked ? "t" : "f";
			        var note = <%= AspxTools.JsGetElementById(DisplayMessageToUnderwriter) %>.checked ? "t" : "f";
			        var pricing = "f";
			        var signature = <%= AspxTools.JsGetElementById(DisplayUnderwriterSignature) %>.checked ? "t" : "f";
			        frmBody.location = <%= AspxTools.JsString("PmlLoanSummaryView.aspx?loanid=" + RequestHelper.LoanID + "&submission=") %> + encodeURIComponent(submission) + "&note=" + encodeURIComponent(note) + "&pricing=" + encodeURIComponent(pricing) + "&signature=" + encodeURIComponent(signature);

			    }
                
                window.onresize = function() { 
                    f_resizeIframe('frmBody',10); 
                };

//-->
		</script>
	</head>
	<body>  
		<form id="PmlLoanSummaryView_Frame" method="post" runat="server">
		    <div style="float:left" style="padding-left: 5px" class="FieldLabel header"> PML Summary Options </div> 
		    <div style="float:right"> <input type="button" onclick="onClosePopup();" value="Close" style="margin:5px 5px 0 0" class="ButtonStyle" /></div>
		    <div style="clear:both" id="buttonSection">
		        <input NoHighlight="true" id="saveBtn" type="button" onclick="f_save();" runat="server" value="Save to loan file" class="ButtonStyle"/>
		        <input NoHighlight="true" id="printBtn" type="button" onclick="f_print();" runat="server" value="Save and Print..." class="ButtonStyle"/> 
		        <input NoHighlight="true" id="emailBtn" type="button" onclick="f_email();" runat="server" value="Save and Email..." class="ButtonStyle" />(SSN's will be shown as ***-**-**** in email)
		    </div>
		    <div  style="padding-left: 5px">
		        Include these section in the print-out and the email: 
		        <ul>
		            <li>
		                <asp:CheckBox id="DisplaySubmissionAgreement" runat="server" style="PADDING-LEFT: 10px" Text="Loan Submission Agreement" Checked="True" onclick="f_onChangeDisplay();"></asp:CheckBox>
		            </li>
		            <li>
		            	<asp:CheckBox id="DisplayMessageToUnderwriter" runat="server" style="PADDING-LEFT: 10px" Text="Message to Lender" onclick="f_onChangeDisplay();"></asp:CheckBox>
		            </li>
		            <li>
		                <asp:CheckBox id="DisplayUnderwriterSignature" style="PADDING-LEFT: 10px" onclick="f_onChangeDisplay();" runat="server" Text="Underwriter's Signature Line"></asp:CheckBox>
		            </li>
		        </ul>
		    </div>

            <iframe onload="f_resizeIframe('frmBody', 10);" src=<%= AspxTools.SafeUrl("PmlLoanSummaryView.aspx?loanid=" + RequestHelper.LoanID + "&submission=t&note=f&pricing=f&signature=f") %> style="width:100%; background-color:White;"  frameborder="1" id="frmBody"></iframe>

			<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
		</form>
	</body>
</html>
