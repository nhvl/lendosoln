﻿#region Generated code
namespace LendersOfficeApp.newlos.Underwriting
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Integration.Underwriting;
    using MeridianLink.CommonControls;

    /// <summary>
    /// This page holds metadata on each AUS run associated with this loan.
    /// </summary>
    public partial class AusOrderingDashboard : BaseLoanPage
    {
        /// <summary>
        /// Loan data object for this page.
        /// </summary>
        private CPageData loanData;

        /// <summary>
        /// Gets the loan data object for the page.
        /// </summary>
        private CPageData LoanData
        {
            get
            {
                if (loanData == null)
                {
                    loanData = CPageData.CreateUsingSmartDependencyForLoad(this.LoanID, typeof(AusOrderingDashboard));
                }

                return loanData;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show the "Reported As" column.
        /// </summary>
        private bool ShowReportedAsColumn { get; set; }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">The calling object.</param>
        /// <param name="e">A set of event arguments.</param>
        protected void PageInit(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.PageTitle = "AUS Ordering Dashboard";
            this.PageID = "AusOrderingDashboard";
            Tools.Bind_UnderwritingService(this.AusForNewRow);
        }

        /// <summary>
        /// Makes sure the DU workflow privilege is checked.
        /// </summary>
        /// <returns>A collection of workflow operations.</returns>
        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return new WorkflowOperation[]
            {
                WorkflowOperations.RunDu
            };
        }

        /// <summary>
        /// Makes sure the DU workflow privilege is checked.
        /// </summary>
        /// <returns>A collection of workflow operations.</returns>
        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[]
            {
                WorkflowOperations.RunDu
            };
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">The sending object.</param>
        /// <param name="e">A set of event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                var orders = AusResultHandler.RetrieveOrdersForLoan(this.LoanID, this.BrokerID);
                this.BindAusRepeater(orders);
                this.PageIsDirty.Checked = false;
                this.DuSeamlessButton.Visible = this.Broker.GetSeamlessDuSettings().SeamlessDuEnabled && this.UserHasWorkflowPrivilege(WorkflowOperations.RunDu);
                this.LpaSeamlessButton.Visible = this.Broker.IsSeamlessLpaEnabled;
                this.RunAusSection.Visible = this.DuSeamlessButton.Visible;
                this.sReportFhaGseAusRunsAsTotalRun.Checked = this.LoanData.sReportFhaGseAusRunsAsTotalRun;
            }
            else
            {
                string action = Request["__EVENTTARGET"];
                if (string.IsNullOrEmpty(action))
                {
                    return;
                }

                this.PageIsDirty.Checked = true;

                string parameter = Request["__EVENTARGUMENT"];
                var orderList = SerializationHelper.JsonNetDeserialize<List<AusResult>>(this.AusOrderListJson.Value);

                if (action == "add")
                {
                    var underwritingService = UnderwritingService.DU;
                    Enum.TryParse(this.AusForNewRow.SelectedValue, out underwritingService);
                    var newAus = new AusResult()
                    {
                        LoanId = this.LoanID,
                        BrokerId = this.BrokerID,
                        IsManual = true,
                        Timestamp = DateTime.Now,
                        UnderwritingService = underwritingService,
                        LoanIsFha = this.LoanData.sLT == E_sLT.FHA
                    };

                    orderList.Add(newAus);
                    this.BindAusRepeater(orderList);
                }
                else if (action == "remove")
                {
                    int index;
                    if (int.TryParse(parameter, out index) && index < orderList.Count)
                    {
                        orderList.RemoveAt(index);
                        this.BindAusRepeater(orderList);
                    }
                }
                else if (action == "move")
                {
                    try
                    {
                        var intParams = parameter.Split('|').Select(int.Parse).ToArray();
                        if (intParams.Count() >= 2)
                        {
                            var sourceIndex = intParams[0];
                            var destIndex = intParams[1];

                            var sourceItem = orderList[sourceIndex];
                            var destItem = orderList[destIndex];

                            if (sourceItem.Timestamp.Date == destItem.Timestamp.Date)
                            {
                                int tempPosition = sourceItem.CustomPosition;
                                sourceItem.CustomPosition = destItem.CustomPosition;
                                destItem.CustomPosition = tempPosition;
                            }

                            this.BindAusRepeater(orderList);
                        }
                    }
                    catch (FormatException)
                    {
                        return;
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        return;
                    }
                }
                else if (action == "update")
                {
                    this.BindAusRepeater(orderList);
                }
            }
        }

        /// <summary>
        /// Binds an AUS order to the table.
        /// </summary>
        /// <param name="sender">The sending object.</param>
        /// <param name="args">A set of event arguments.</param>
        protected void OnAusOrderBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            var result = args.Item.DataItem as AusResult;

            var ausOrderId = args.Item.FindControl("AusOrderId") as HiddenField;
            ausOrderId.Value = result.AusOrderId?.ToString() ?? string.Empty;

            var customPosition = args.Item.FindControl("CustomPosition") as HiddenField;
            customPosition.Value = result.CustomPosition.ToString();

            var brokerId = args.Item.FindControl("AusBrokerId") as HiddenField;
            brokerId.Value = result.BrokerId.ToString();

            var loanId = args.Item.FindControl("AusLoanId") as HiddenField;
            loanId.Value = result.LoanId.ToString();

            var isManual = args.Item.FindControl("IsManual") as CheckBox;
            isManual.Checked = result.IsManual;

            var underwritingServiceFriendlyName = args.Item.FindControl("UnderwritingServiceFriendlyName") as TextBox;
            underwritingServiceFriendlyName.Enabled = false; // The user should not change the AUS service for an existing row.
            underwritingServiceFriendlyName.Text = this.GetUnderwritingServiceFriendlyName(result.UnderwritingService);

            var underwritingServiceValue = args.Item.FindControl("UnderwritingServiceValue") as HiddenField;
            underwritingServiceValue.Value = result.UnderwritingService.ToString("D");

            var loanIsFha = args.Item.FindControl("LoanIsFha") as HiddenField;
            loanIsFha.Value = result.LoanIsFha.ToString();

            var otherUnderwritingService = args.Item.FindControl("OtherUnderwritingService");
            otherUnderwritingService.Visible = result.UnderwritingService == UnderwritingService.Other;

            var ausOtherDescription = args.Item.FindControl("UnderwritingServiceOtherDescription") as TextBox;
            ausOtherDescription.Enabled = result.IsManual;
            ausOtherDescription.Text = result.UnderwritingServiceOtherDescription;

            if (ShowReportedAsColumn)
            {
                var reportAsField = args.Item.FindControl("ReportAs") as EncodedLiteral;
                reportAsField.Text = this.GetUnderwritingServiceFriendlyName(result.ReportAs(this.LoanData.sReportFhaGseAusRunsAsTotalRun));
            }
            else
            {
                args.Item.FindControl("ReportAsColumn").Visible = false;
            }

            args.Item.FindControl("DuDescriptions").Visible = result.UnderwritingService.Equals(UnderwritingService.DU);
            args.Item.FindControl("DuControls").Visible = result.UnderwritingService.Equals(UnderwritingService.DU);

            var duRecommendation = args.Item.FindControl("DuRecommendationDropdown") as DropDownList;
            duRecommendation.Enabled = result.IsManual;
            Tools.Bind_DuRecommendation(duRecommendation);
            Tools.SetDropDownListValue(duRecommendation, result.DuRecommendation);

            args.Item.FindControl("LpDescriptions").Visible = result.UnderwritingService == UnderwritingService.LP;
            args.Item.FindControl("LpControls").Visible = result.UnderwritingService == UnderwritingService.LP;

            var lpRiskClass = args.Item.FindControl("LpRiskClassDropdown") as DropDownList;
            lpRiskClass.Enabled = result.IsManual;
            Tools.Bind_LpRiskClass(lpRiskClass);
            Tools.SetDropDownListValue(lpRiskClass, result.LpRiskClass);

            var lpPurchaseEligibility = args.Item.FindControl("lpPurchaseEligibilityDropdown") as DropDownList;
            lpPurchaseEligibility.Enabled = result.IsManual;
            Tools.Bind_LpPurchaseEligibility(lpPurchaseEligibility);
            Tools.SetDropDownListValue(lpPurchaseEligibility, result.LpPurchaseEligibility);

            var lpFeeLevel = args.Item.FindControl("LpFeeLevelDropdown") as TextBox;
            lpFeeLevel.Enabled = result.IsManual;
            lpFeeLevel.Text = result.LpFeeLevel;
            args.Item.FindControl("LpFeeLevelControl").Visible = result.LpPurchaseEligibility == LpPurchaseEligibility._500FreddieMacEligibleLPAMinusOffering;
            args.Item.FindControl("LpFeeLevelDescription").Visible = result.LpPurchaseEligibility == LpPurchaseEligibility._500FreddieMacEligibleLPAMinusOffering;

            var lpStatus = args.Item.FindControl("LpStatusDropdown") as DropDownList;
            lpStatus.Enabled = result.IsManual;
            Tools.Bind_LpStatus(lpStatus);
            Tools.SetDropDownListValue(lpStatus, result.LpStatus);

            args.Item.FindControl("TotalDescriptions").Visible = result.UnderwritingService == UnderwritingService.TOTAL;
            args.Item.FindControl("TotalControls").Visible = result.UnderwritingService == UnderwritingService.TOTAL;

            var totalRiskClass = args.Item.FindControl("TotalRiskClassDropdown") as DropDownList;
            totalRiskClass.Enabled = result.IsManual;
            Tools.Bind_TotalRiskClass(totalRiskClass);
            Tools.SetDropDownListValue(totalRiskClass, result.TotalRiskClass);

            var totalEligibilityAssessment = args.Item.FindControl("TotalEligibilityAssessmentDropdown") as DropDownList;
            totalEligibilityAssessment.Enabled = result.IsManual;
            Tools.Bind_TotalEligibilityAssessment(totalEligibilityAssessment);
            Tools.SetDropDownListValue(totalEligibilityAssessment, result.TotalEligibilityAssessment);

            args.Item.FindControl("GusDescriptions").Visible = result.UnderwritingService == UnderwritingService.GUS;
            args.Item.FindControl("GusControls").Visible = result.UnderwritingService == UnderwritingService.GUS;

            var gusRecommendation = args.Item.FindControl("GusRecommendationDropdown") as DropDownList;
            Tools.Bind_GusRecommendation(gusRecommendation);
            Tools.SetDropDownListValue(gusRecommendation, result.GusRecommendation);

            var gusRiskEvaluation = args.Item.FindControl("GusRiskEvaluationDropdown") as DropDownList;
            Tools.Bind_GusRiskEvaluation(gusRiskEvaluation);
            Tools.SetDropDownListValue(gusRiskEvaluation, result.GusRiskEvaluation);

            args.Item.FindControl("OtherAusDescriptions").Visible = result.UnderwritingService == UnderwritingService.Other;
            args.Item.FindControl("OtherAusControls").Visible = result.UnderwritingService == UnderwritingService.Other;

            var resultOtherDescription = args.Item.FindControl("ResultOtherDescription") as TextBox;
            resultOtherDescription.Text = result.ResultOtherDescription;

            var caseId = args.Item.FindControl("CaseId") as TextBox;
            caseId.Enabled = result.IsManual;
            caseId.Text = result.CaseId;

            var orderRunBy = args.Item.FindControl("OrderRunBy") as TextBox;
            orderRunBy.Enabled = result.IsManual;
            orderRunBy.Text = result.OrderPlacedBy;

            var manualTimestamp = args.Item.FindControl("ManualTimeStamp") as TextBox;
            manualTimestamp.Text = result.Timestamp.ToShortDateString();
            manualTimestamp.Visible = result.IsManual;

            var timestamp = args.Item.FindControl("Timestamp") as TextBox;
            timestamp.Enabled = false;
            timestamp.Text = Tools.GetDateTimeDescription(result.Timestamp);
            if (result.IsManual)
            {
                // We use this as a cached fallback value when the user enters
                // an invalid date in a manual row, so we don't want to use
                // Visible = false as that would remove it from the DOM altogether.
                timestamp.Style.Add("display", "none");
            }

            var notes = args.Item.FindControl("Notes") as TextBox;
            notes.Text = result.Notes;

            var removeButton = args.Item.FindControl("RemoveAusRun") as Button;
            removeButton.Enabled = result.IsManual;
            removeButton.OnClientClick = "return onRemoveClick(" + args.Item.ItemIndex + ");";

            var moveUpArrow = args.Item.FindControl("upArrow") as HyperLink;
            var moveDownArrow = args.Item.FindControl("downArrow") as HyperLink;

            var resultList = (AusOrderList.DataSource as IEnumerable<AusResult>).ToList();
            bool isAlone = (args.Item.ItemIndex == 0 || resultList[args.Item.ItemIndex - 1]?.Timestamp.Date != result.Timestamp.Date) && result.CustomPosition == int.MaxValue;

            moveUpArrow.Visible = !isAlone && result.CustomPosition > 0 && result.IsManual;
            moveDownArrow.Visible = result.CustomPosition < int.MaxValue && result.IsManual;
        }

        /// <summary>
        /// Gets a friendly display name for an Underwriting Service value.
        /// </summary>
        /// <param name="service">The service value.</param>
        /// <returns>A friendly display name.</returns>
        protected string GetUnderwritingServiceFriendlyName(UnderwritingService service)
        {
            if (service == UnderwritingService.LP)
            {
                return "LPA";
            }
            else
            {
                return service.ToString();
            }
        }

        /// <summary>
        /// Bind repeater from list of AUS runner.
        /// </summary>
        /// <param name="resultList">List of AUS runner.</param>
        protected void BindAusRepeater(List<AusResult> resultList)
        {
            var ausResultsByDate = resultList.GroupBy(result => result.Timestamp.Date).OrderByDescending(group => group.Key);
            List<AusResult> reorderedList = new List<AusResult>();
            foreach (var group in ausResultsByDate)
            {
                int index = 0;
                var groupCount = group.Count();
                foreach (var result in group.OrderBy(r => r.CustomPosition).ToList())
                {
                    result.CustomPosition = index + 1 < groupCount ? index++ : int.MaxValue;
                    reorderedList.Add(result);
                }
            }

            this.ShowReportedAsColumn = resultList.Any(result => result.UnderwritingService != result.ReportAs(this.LoanData.sReportFhaGseAusRunsAsTotalRun));
            ReportedAsColumnHeader.Visible = ShowReportedAsColumn;

            AusOrderList.DataSource = reorderedList;
            AusOrderList.DataBind();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            RegisterJsScript("LQBPopup.js");
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
