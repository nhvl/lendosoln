﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Common;
using LendersOfficeApp.los.admin;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Underwriting.QM
{
    public partial class QM2015_PointAndFeesServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "ChangeViewType":
                    ChangeViewType();
                    break;
                default:
                    break;
            }
        }

        protected override DataAccess.CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(QM2015_PointAndFeesServiceItem));
        }

        protected override void BindData(DataAccess.CPageData dataLoan, DataAccess.CAppData dataApp)
        {
            // OPM 198065 - DO NOTHING
        }

        protected override void LoadData(DataAccess.CPageData dataLoan, DataAccess.CAppData dataApp)
        {
            SetResult("sQMTotFeeAmount", dataLoan.sQMTotFeeAmount_rep);
            SetResult("sQMTotFinFeeAmount", dataLoan.sQMTotFinFeeAmount_rep);
        }

        public void ChangeViewType()
        {
            string viewModelJson = GetString("viewModelJson");
            PointAndFeesViewModel viewModel = ObsoleteSerializationHelper.JsonDeserialize<PointAndFeesViewModel>(viewModelJson);

            SetResult("viewModelJson", ObsoleteSerializationHelper.JsonSerialize(viewModel));
        }
    }

	public sealed class QM2015_CalculationsServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(QM2015_CalculationsServiceItem));
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sRLckdD", dataLoan.sRLckdD);
            SetResult("sInterestRateSetDLckd", dataLoan.sInterestRateSetDLckd);
            SetResult("sInterestRateSetD", dataLoan.sInterestRateSetD_rep);
            SetResult("sDocMagicClosingD", dataLoan.sDocMagicClosingD_rep);
            SetResult("sQMAveragePrimeOfferR", dataLoan.sQMAveragePrimeOfferR_rep);
            SetResult("sQMAveragePrimeOfferRLck", dataLoan.sQMAveragePrimeOfferRLck);
            SetResult("sQMParR", dataLoan.sQMParR_rep);
            SetResult("sQMTotFeeAmount", dataLoan.sQMTotFeeAmount_rep);
            SetResult("sQMTotFinFeeAmount", dataLoan.sQMTotFinFeeAmount_rep);
            SetResult("sQMExclDiscntPnt", dataLoan.sQMExclDiscntPnt_rep);
            SetResult("sQMExclDiscntPntLck", dataLoan.sQMExclDiscntPntLck);
            SetResult("sQMFhaUfmipAtClosingLck", dataLoan.sQMFhaUfmipAtClosingLck);
            SetResult("sQMFhaUfmipAtClosing", dataLoan.sQMFhaUfmipAtClosing_rep);
            SetResult("sQMNonExcludableDiscountPointsPc", dataLoan.sQMNonExcludableDiscountPointsPc_rep);

            SetResult("sQMMaxPrePmntPenalty", dataLoan.sQMMaxPrePmntPenalty_rep);
            SetResult("sQMQualR", dataLoan.sQMQualR_rep);
            SetResult("sQMQualBottom", dataLoan.sQMQualBottom_rep);
            SetResult("sQMAprRSpread", dataLoan.sQMAprRSpread_rep);
            SetResult("sQMParRSpread", dataLoan.sQMParRSpread_rep);

            SetResult("sQMParRSpreadCalcDesc", dataLoan.sQMParRSpreadCalcDesc);
            SetResult("sQMAprRSpreadCalcDesc", dataLoan.sQMAprRSpreadCalcDesc);
            //SetResult("sQMExcessUpfrontMIP", dataLoan.sQMExcessUpfrontMIP_rep);
            SetResult("sQMExcessUpfrontMIP_1", dataLoan.sQMExcessUpfrontMIP_rep);
            SetResult("sQMExcessUpfrontMIPCalcDesc", dataLoan.sQMExcessUpfrontMIPCalcDesc);
            SetResult("sQMExcessDiscntFCalcDesc", dataLoan.sQMExcessDiscntFCalcDesc);

            //SetResult("sQMTotFeeAmount", dataLoan.sQMTotFeeAmount_rep);
            SetResult("sQMTotFeeAmount_1", dataLoan.sQMTotFeeAmount_rep);
            SetResult("sQMExcessDiscntF", dataLoan.sQMExcessDiscntF_rep);
            SetResult("sQMExcessUpfrontMIP", dataLoan.sQMExcessUpfrontMIP_rep);
            //SetResult("sQMMaxPrePmntPenalty", dataLoan.sQMMaxPrePmntPenalty_rep);
            SetResult("sQMMaxPrePmntPenalty_1", dataLoan.sQMMaxPrePmntPenalty_rep);
            SetResult("sQMDiscntBuyDownR", dataLoan.sQMDiscntBuyDownR_rep);
            SetResult("sQMDiscntBuyDownRCalcDesc", dataLoan.sQMDiscntBuyDownRCalcDesc);
            SetResult("sQMLAmt", dataLoan.sQMLAmt_rep);
            SetResult("sQMLAmt_1", dataLoan.sQMLAmt_rep);
            SetResult("sQMLAmtCalcDesc", dataLoan.sQMLAmtCalcDesc);
            SetResult("sQMTotFeeAmt", dataLoan.sQMTotFeeAmt_rep);
            SetResult("sQMTotFeePc", dataLoan.sQMTotFeePc_rep);
            SetResult("sQMExcessDiscntFPc", dataLoan.sQMExcessDiscntFPc_rep);
            SetResult("sQMMaxPointAndFeesAllowedAmt", dataLoan.sQMMaxPointAndFeesAllowedAmt_rep);
            SetResult("sQMMaxPointAndFeesAllowedPc", dataLoan.sQMMaxPointAndFeesAllowedPc_rep);
            SetResult("sQMLoanPassesPointAndFeesTest", dataLoan.sQMLoanPassesPointAndFeesTest);
            SetResult("sQMLoanDoesNotHaveNegativeAmort", dataLoan.sQMLoanDoesNotHaveNegativeAmort);
            SetResult("sQMLoanDoesNotHaveAmortTermOver30Yr", dataLoan.sQMLoanDoesNotHaveAmortTermOver30Yr);
            SetResult("sQMLoanDoesNotHaveBalloonFeature", dataLoan.sQMLoanDoesNotHaveBalloonFeature);
            SetResult("sQMLoanHasDtiLessThan43Pc", dataLoan.sQMLoanHasDtiLessThan43Pc);
            SetResult("sQMIsVerifiedIncomeAndAssets", dataLoan.sQMIsVerifiedIncomeAndAssets);
            SetResult("sQMIsEligibleByLoanPurchaseAgency", dataLoan.sQMIsEligibleByLoanPurchaseAgency);
            SetResult("sQMLoanPassedComplianceChecks", dataLoan.sQMLoanPassedComplianceChecks);
            SetResult("sQMStatusT", dataLoan.sQMStatusT);
            SetResult("sQMStatusTCombo", CPageBase.sQMStatusT_map_rep(dataLoan.sQMStatusT));
            SetResult("sQMLoanHasComplianceChecksOnFile", dataLoan.sQMLoanHasComplianceChecksOnFile);
            SetResult("sQMLoanPurchaseAgency", dataLoan.sQMLoanPurchaseAgency);
            //SetResult("sQMLoanPassesPointAndFeesTest", dataLoan.sQMLoanPassesPointAndFeesTest);
            SetResult("sQMLoanPurchaseAgencyIsDisabled", dataLoan.sQMLoanPurchaseAgencyIsDisabled);

            SetResult("sHighPricedMortgageT", dataLoan.sHighPricedMortgageT);
            SetResult("sHighPricedMortgageTLckd", dataLoan.sHighPricedMortgageTLckd);

            SetResult("sIsExemptFromAtr", dataLoan.sIsExemptFromAtr);
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sInterestRateSetDLckd = GetBool("sInterestRateSetDLckd");
            dataLoan.sInterestRateSetD_rep = GetString("sInterestRateSetD");
            dataLoan.sQMAveragePrimeOfferRLck = GetBool("sQMAveragePrimeOfferRLck");
            dataLoan.sQMAveragePrimeOfferR_rep = GetString("sQMAveragePrimeOfferR");
            dataLoan.sQMParR_rep = GetString("sQMParR");
            dataLoan.sQMExclDiscntPnt_rep = GetString("sQMExclDiscntPnt");
            dataLoan.sQMExclDiscntPntLck = GetBool("sQMExclDiscntPntLck");
            dataLoan.sQMIsVerifiedIncomeAndAssets = GetBool("sQMIsVerifiedIncomeAndAssets");
            dataLoan.sQMLoanPurchaseAgency = GetEnum<E_sQMLoanPurchaseAgency>("sQMLoanPurchaseAgency");
            dataLoan.sQMIsEligibleByLoanPurchaseAgency = GetBool("sQMIsEligibleByLoanPurchaseAgency");
            dataLoan.sQMNonExcludableDiscountPointsPc_rep = GetString("sQMNonExcludableDiscountPointsPc");

            dataLoan.sHighPricedMortgageTLckd = GetBool("sHighPricedMortgageTLckd");
            dataLoan.sHighPricedMortgageT = GetEnum<E_HighPricedMortgageT>("sHighPricedMortgageT");

            dataLoan.sIsExemptFromAtr = GetBool("sIsExemptFromAtr");

            dataLoan.sQMFhaUfmipAtClosing_rep = GetString("sQMFhaUfmipAtClosing");
            dataLoan.sQMFhaUfmipAtClosingLck = GetBool("sQMFhaUfmipAtClosingLck");
            dataLoan.sQMMaxPrePmntPenalty_rep = GetString("sQMMaxPrePmntPenalty");
        }
    }

    public partial class QM2015Service : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                default:
                    break;
            }
        }

        protected override void Initialize()
        {
            AddBackgroundItem("QM2015_Calculations", new QM2015_CalculationsServiceItem());
            AddBackgroundItem("QM2015_PointAndFees", new QM2015_PointAndFeesServiceItem());
        }
    }
}
