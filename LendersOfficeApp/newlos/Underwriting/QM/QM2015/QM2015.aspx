﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QM2015.aspx.cs" Inherits="LendersOfficeApp.newlos.Underwriting.QM.QM2015" %>
<%@ Register TagPrefix="uc1" TagName="Tabs" Src="~/common/BaseTabPage.ascx" %>
<%@ Register TagPrefix="uc1" TagName="QM2015_Calculations" Src="~/newlos/Underwriting/QM/QM2015/QM2015_Calculations.ascx" %>
<%@ Register TagPrefix="uc1" TagName="QM2015_PointAndFees" Src="~/newlos/Underwriting/QM/QM2015/QM2015_PointAndFees.ascx" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>QM</title>
    <style type="text/css">
        body { background-color: gainsboro; } 
        table { margin-left: 10px; }
        table.calc td { padding: 4px; }
        ul.nob {list-style: none; margin: 10px; padding: 0; }
        ul.nob li { padding-bottom: 4px; }
        .QmFail { color: Red; }
        .QmFail img { display: inline; }
        .QmSuccess img { display: none;  }
    </style>
    <script type="text/javascript">
        //change the checkbox icons
        //should be called in the aspx file)
        function displayAutomatedCheckBoxes() {
            var checkboxList = document.getElementsByTagName("input");
            for (var i = 0; i < checkboxList.length; i++) {
                var inputElement = checkboxList[i];
                if (inputElement.disabled && inputElement.type == "checkbox") {
                    inputElement.style.display = "none";
                    var src;
                    if (inputElement.checked)
                        src = ML.VirtualRoot + "/images/pass.png";
                    else
                        src = ML.VirtualRoot + "/images/fail.png";

                    if (inputElement.previousSibling == null) {
                        var img = document.createElement("img");
                        img.style.paddingLeft = "3px";
                        img.style.marginRight = "2px";
                        img.src = src;
                        inputElement.parentNode.insertBefore(img, inputElement);
                    }
                    else if ((inputElement.previousSibling.nameProp !== "pass.png" || inputElement.previousSibling.nameProp !== "fail.png")) {
                        inputElement.previousSibling.src = src;
                    }
                }
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="Tabs">
        <uc1:Tabs runat="server" ID="Tabs" />
    </div>
    <uc1:QM2015_Calculations runat="server" ID="QM2015_Calculations"></uc1:QM2015_Calculations>
    <uc1:QM2015_PointAndFees runat="server" ID="QM2015_PointAndFees"></uc1:QM2015_PointAndFees>
    </form>
</body>
</html>
