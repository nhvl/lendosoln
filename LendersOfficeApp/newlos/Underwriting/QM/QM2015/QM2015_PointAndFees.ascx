﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QM2015_PointAndFees.ascx.cs" Inherits="LendersOfficeApp.newlos.Underwriting.QM.QM2015_PointAndFees" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<style type="text/css">
    .sectionTable 
    {
        border-collapse: separate;
        border: 1px solid black;
        margin-top: 10px;
    }
    
    .sectionTable tr td
    {
        padding-left: 4px;
        padding-right: 4px;
    }
</style>

<%--<script type="text/javascript" src='http://cdn.ractivejs.org/latest/ractive.js'></script> --%>

<script id='QMTemplate' type='text/ractive'>
<br/>
<div style="padding-left:10px">
    Use sections from the:
    <select value='{{ViewT}}' on-change="changeViewType" NotForEdit="true">
        <option value="<%= AspxTools.HtmlString(E_ClosingCostViewT.LoanHud1.ToString("D")) %>"><%= AspxTools.HtmlString("HUD-1") %></option>
        <option value="<%= AspxTools.HtmlString(E_ClosingCostViewT.LoanClosingCost.ToString("D")) %>"><%= AspxTools.HtmlString("Loan Estimate") %></option>
    </select>
</div>
{{#SectionList:i}}
{{#if ClosingCostFeeList.length > 0 }}
<table class="sectionTable" cellspacing="0" cellpadding="0" border="0" width="780px" >
    <tr  class="GridHeader">
        <td colspan="15">
          {{SectionName}}
        </td>
    </tr>
    <tr class="FormTableHeader">
        <td style="width:70px;">HUD-1 Line</td>
        <td>Description</td>
        <td style="text-align:right;width:70px;">Amount</td>
        <td style="width:50px;padding-left:10px;">Paid By</td>
        <td style="width:25px;">APR</td>
        <td style="width:170px;">Paid To</td>
        <td style="width:25px;">TP</td>
        <td style="width:25px;">AFF</td>
        <td style="width:25px;"></td>
        <td style="text-align:right;width:70px; padding-right: 4px; ">QM Amount</td>
    </tr>
    {{#ClosingCostFeeList:j}}
        {{>ClosingCostFeeTemplate}}
    {{/#ClosingCostFeeList}}
</table>
{{/if}}
{{/SectionList}}
</script>

<script id='ClosingCostFeeTemplate' type='text/ractive'>
    <tr class="{{ j % 2 == 0 ? "GridItem" : "GridAlternatingItem"}}">
        <td class="FieldLabel">{{hudline}}</td>
        <td class="FieldLabel">{{desc}}
            {{#if (typeid == <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId) %>) ||
                  (typeid == <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId) %>) || 
                  (typeid == <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId) %>) ||
                  (typeid == <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId) %>) ||
                  (typeid == <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId) %>)}}
                * 
            {{/if}}
            {{#if typeid == <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId) %>}}
                **
            {{/if}}
        </td>
        <td align="right">{{total}}</td>
        
        <td style="padding-left:10px;">
            {{#if (pmts && pmts.length == 1)}} 
                {{#if pmts[0].paid_by == 1}}
                    borr pd
                {{/if}}
                {{#if pmts[0].paid_by == 2}}
                    seller
                {{/if}}
                {{#if pmts[0].paid_by == 3}}
                    borr fin
                {{/if}}
                {{#if pmts[0].paid_by == 4}}
                    lender
                {{/if}}
                {{#if pmts[0].paid_by == 5}}
                    broker
                {{/if}}
                {{#if pmts[0].paid_by == 6}}
                    other
                {{/if}}
            {{else}}
                --split--
            {{/if}}
        </td> 

        <td><input type="checkbox" checked="{{apr}}" disabled="true" /></td>
        <td><input type="textbox" style="width:170px;" ReadOnly="true" value={{bene_desc}} /> </td>
        <td><input type="checkbox" checked="{{tp}}" disabled="true" /></td>
        <td><input type="checkbox" checked="{{aff}}" disabled="true" /></td>
        
        <td>
            {{#if show_qm_warning}}
                <img id="QmWarning" runat="server" style="padding-left:10px" src="../../../../images/warn.png" />
            {{else}}
                &nbsp;
            {{/if}}
        </td>
        
        <td align="right">
            {{#if (pmts && pmts.length == 1)}}
                <input type="textbox" style="text-align:right;width:70px;" ReadOnly="true" value={{qm_amount}} />
            {{else}}
                &nbsp;
            {{/if}}
        </td>
    </tr>
    {{#if pmts.length > 1}} 
    {{#pmts:k}}
        {{>ClosingCostFeePaymentTemplate}}
    {{/pmts}}
    {{/if}}
</script>

<script id='ClosingCostFeePaymentTemplate' type='text/ractive'>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
        <td align="right">
            {{#if (k == (pmts.length - 1))}}+{{/if}}
            {{amt}}
        </td>
        <td style="padding-left:10px;">
            {{#if paid_by == 1}}
                borr pd
            {{/if}}
            {{#if paid_by == 2}}
                seller
            {{/if}}
            {{#if paid_by == 3}}
                borr fin
            {{/if}}
            {{#if paid_by == 4}}
                lender
            {{/if}}
            {{#if paid_by == 5}}
                broker
            {{/if}}
            {{#if paid_by == 6}}
                other
            {{/if}}
        </td>
        <td colspan="4">
            &nbsp;
        </td>
        <td align="right">
            {{#if (is_qm && (!apr || paid_by == 1 || paid_by == 3))}}
                <input type="textbox" style="text-align:right;width:70px;" ReadOnly="true" value={{amt}} />
            {{else}}
                <input type="textbox" style="text-align:right;width:70px;" ReadOnly="true" value="$0.00" />
            {{/if}}
        </td>
    </tr>
</script>

<table width="350px">
    <tr>
        <td>
            *Fee is handled separately
        </td>
        <td>
            **Fee is excluded per regulation
        </td>
    </tr>
</table>
<div id="divWarning" runat="server">
    <img style="padding-left:10px" src="../../../../images/warn.png" /> Unable to determine if the fee recipient is a third-party or affiliate, please verify
</div>
<div id='container'></div>
<div style="width:600px; margin-top:10px">
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td align="right" style="padding-right:5px;">Total QM Fee Amount</td>
            <td align="right" width="70px"><asp:TextBox id="sQMTotFeeAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
        </tr>
        <tr>
            <td align="right" style="padding-right:5px;">Financed QM Fee Amount</td>
            <td align="right" width="70px"><asp:TextBox id="sQMTotFinFeeAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
        </tr>
    </table>
</div>

<script type="text/javascript">
    $(function($) {
        var ractive = new Ractive({
            // The `el` option can be a node, an ID, or a CSS selector.
            el: 'container',
            append: false,

            template: '#QMTemplate',
            partials: '#ClosingCostFeeTemplate,#ClosingCostFeePaymentTemplate',

            data: ClosingCostData
        });

        ractive.on('changeViewType', function(event) {
            //An on-change event from a select doesn't seem to update the model in time, so it's always showing the last selected value.
            //Using a setTimeout allows the model to update before using its data.
            setTimeout(function() {
                var args = new Object();
                //args["loanid"] = ML.sLId;
                args['_ClientID'] = ML._ClientID;
                args["viewModelJson"] = JSON.stringify(event.context);

                var result = gService.loanedit.call("ChangeViewType", args);

                if (!result.error) {
                    ractive.set(JSON.parse(result.value.viewModelJson));
                }
            }, 0);
        });
    });
</script>
