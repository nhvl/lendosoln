﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using LendersOfficeApp.los.admin;
using LendersOffice.Security;
using LendersOfficeApp.newlos.Test;

namespace LendersOfficeApp.newlos.Underwriting.QM
{
    [DataContract]
    public class PointAndFeesViewModel
    {
        [DataMember]
        public E_ClosingCostViewT ViewT { get; set; }

        [DataMember]
        public IEnumerable<BorrowerClosingCostFeeSection> SectionList
        {
            get
            {
                return this.ClosingCostSet.GetViewForSerialization(this.ViewT);
            }

            set
            {
                this.ClosingCostSet = new BorrowerClosingCostSet(string.Empty);
                if (value != null)
                {
                    foreach (BorrowerClosingCostFeeSection section in value)
                    {
                        if (section.FilteredClosingCostFeeList != null)
                        {
                            foreach (BorrowerClosingCostFee fee in section.FilteredClosingCostFeeList)
                            {
                                this.ClosingCostSet.AddOrUpdate(fee);
                            }
                        }
                    }
                }
            }
        }

        public BorrowerClosingCostSet ClosingCostSet { get; set; }
    }

    public partial class QM2015_PointAndFees : BaseLoanUserControl, IAutoLoadUserControl
    {
        #region IAutoLoadUserControl Members

        public void LoadData()
        {
        }

        public void SaveData()
        {
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Headers.Add("X-UA-Compatible", "IE=edge");

            CPageData dataLoan;
            dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(QM2015_PointAndFees));
            dataLoan.InitLoad();

            divWarning.Visible = dataLoan.sQMShowWarning;
            sQMTotFeeAmount.Text = dataLoan.sQMTotFeeAmount_rep;
            sQMTotFinFeeAmount.Text = dataLoan.sQMTotFinFeeAmount_rep;

            PointAndFeesViewModel viewModel = new PointAndFeesViewModel();
            viewModel.ViewT = dataLoan.sIsInTRID2015Mode ? E_ClosingCostViewT.LoanClosingCost : E_ClosingCostViewT.LoanHud1;

            viewModel.ClosingCostSet = dataLoan.sClosingCostSet;

            ((BasePage)Page).RegisterJsScript("ThirdParty/ractive-legacy.min.js");
            ((BasePage)Page).RegisterJsObject("ClosingCostData", viewModel);
        }
    }
}