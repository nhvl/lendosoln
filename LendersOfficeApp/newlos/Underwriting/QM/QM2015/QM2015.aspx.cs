﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Underwriting.QM
{
    public partial class QM2015 : BaseLoanPage
    {
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            EnableJqueryMigrate = false;

            int page = RequestHelper.GetInt("pg", 0);
            if (page == 1)
            {
                PDFPrintClass = typeof(LendersOffice.Pdf.CQMPointsAndFeesPDF);
            }
            else
            {
                PDFPrintClass = typeof(LendersOffice.Pdf.CQMSummaryFormPDF);
            }

            Tabs.AddToQueryString("loanid", LoanID.ToString());
            Tabs.RegisterControl("QM Calculations", QM2015_Calculations);
            Tabs.RegisterControl("QM Point and Fees", QM2015_PointAndFees);
            PageID = "QM2015";

            base.OnInit(e);
           
        }
    }
}
