﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="LendersOfficeApp.newlos.Underwriting.QM.Main" %>
<%@ Register TagPrefix="uc1" TagName="Tabs" Src="~/common/BaseTabPage.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Calculations" Src="~/newlos/Underwriting/QM/Calculations.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PointAndFees" Src="~/newlos/Underwriting/QM/PointAndFees.ascx" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>QM</title>
    <style type="text/css">
        body { background-color: gainsboro; } 
        table { margin-left: 10px; }
        table.calc td { padding: 4px; }
        ul.nob {list-style: none; margin: 10px; padding: 0; }
        
        .QmFail { color: Red; }
        .QmFail img { display: inline; }
        .QmSuccess img { display: none;  }
    </style>
    <script>

        //change the checkbox icons
        //should be called in the aspx file)
        function displayAutomatedCheckBoxes() {
            var checkboxList = document.getElementsByTagName("input");
            for (var i = 0; i < checkboxList.length; i++) {
                var inputElement = checkboxList[i];
                if (inputElement.disabled && inputElement.type == "checkbox") {
                    inputElement.style.display = "none";
                    var src;
                    if (inputElement.checked)
                        src = (document.getElementById("VirtualRoot") == null ? "" : document.getElementById("VirtualRoot").value) + "/images/pass.png";
                    else
                        src = (document.getElementById("VirtualRoot") == null ? "" : document.getElementById("VirtualRoot").value) + "/images/fail.png";

                    if (inputElement.previousSibling == null) {
                        var img = document.createElement("img");
                        img.style.paddingLeft = "3px";
                        img.style.marginRight = "2px";
                        img.src = src;
                        inputElement.parentNode.insertBefore(img, inputElement);
                    }
                    else if ((inputElement.previousSibling.nameProp !== "pass.png" || inputElement.previousSibling.nameProp !== "fail.png")) {
                        inputElement.previousSibling.src = src;
                    }



                }
            }
        }

        function SetTpAffEdit( bEnable ) {
            var checkboxList = document.getElementsByTagName("input");
            for (var i = 0; i < checkboxList.length; i++) {
                var inputElement = checkboxList[i];
                if (inputElement.type == "checkbox") {
                    if (inputElement.id &&
                        (inputElement.id.indexOf("TrdPty_chk") != -1 || inputElement.id.indexOf("Aff_chk") != -1)) {
                        inputElement.disabled = !bEnable;
                        inputElement.checked = false;
                    }
                }
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="Tabs">
        <uc1:Tabs runat="server" ID="Tabs" />
    </div>
    <uc1:Calculations runat="server" ID="Calculations"></uc1:Calculations>
    <uc1:PointAndFees runat="server" ID="PointAndFees"></uc1:PointAndFees>
    </form>
</body>
</html>
