﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PointAndFees.ascx.cs" Inherits="LendersOfficeApp.newlos.Underwriting.QM.PointAndFees" %>
<%@ Register TagPrefix="uc1" TagName="QMItemProps" Src="QMItemProps.ascx" %>

<table width="350px">
    <tr>
        <td>
            *Fee is handled separately
        </td>
        <td>
            **Fee is excluded per regulation
        </td>
    </tr>
</table>
<div id="divWarning" runat="server">
    <img style="padding-left:10px" src="../../../images/warn.png" /> Unable to determine if the fee recipient is a third-party or affiliate, please verify
</div>
<table cellspacing="0" cellpadding="0" border="0" width="600px" style="border-style:groove;border-width:2px;padding-left:3px;padding-right:3px;" >
    <tr class="GridHeader">
		<td>HUD-1 Line</td>
		<td>Description</td>
		<td  align="right" width="77px">Amount</td>
		<td>Paid By</td>
		<td width="30px">APR</td>
		<td width="30px">Third-party</td>
		<td width="30px">Affiliate</td>
		<td width="30px"></td>
		<td align="right" width="77px">QM Amount</td>
	</tr>
	<tr class="GridItem">
	    <td class="FieldLabel">801</td>
	    <td class="FieldLabel">Loan origination fee</td>
	    <td align="right"><ml:EncodedLiteral id="sQMLOrigF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMLOrigFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMLOrigF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel"></td>
	    <td class="FieldLabel">Originator compensation*</td>
	    <td align="right"><ml:EncodedLiteral id="sQMGfeOriginatorCompF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMGfeOriginatorCompFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMGfeOriginatorCompF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">802</td>
	    <td class="FieldLabel">Discount points*</td>
	    <td align="right"><ml:EncodedLiteral id="sQMGfeDiscountPointF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMGfeDiscountPointFProps" runat="server" PaidToThirdPartyVisible="false" />
	    <td align="right"><asp:TextBox id="sQMGfeDiscountPointF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">804</td>
	    <td class="FieldLabel">Appraisal fee</td>
	    <td align="right"><ml:EncodedLiteral id="sQMApprF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMApprFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMApprF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">805</td>
	    <td class="FieldLabel">Credit report</td>
	    <td align="right"><ml:EncodedLiteral id="sQMCrF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMCrFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMCrF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">806</td>
	    <td class="FieldLabel">Tax service fee</td>
	    <td align="right"><ml:EncodedLiteral id="sQMTxServF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMTxServFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMTxServF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">807</td>
	    <td class="FieldLabel">Flood Certification</td>
	    <td align="right"><ml:EncodedLiteral id="sQMFloodCertificationF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMFloodCertificationFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMFloodCertificationF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">808</td>
	    <td class="FieldLabel">Mortgage broker fee</td>
	    <td align="right"><ml:EncodedLiteral id="sQMMBrokF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMMBrokFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMMBrokF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">809</td>
	    <td class="FieldLabel">Lender's inspection fee</td>
	    <td align="right"><ml:EncodedLiteral id="sQMInspectF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMInspectFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMInspectF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">810</td>
	    <td class="FieldLabel">Processing fee</td>
	    <td align="right"><ml:EncodedLiteral id="sQMProcF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMProcFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMProcF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">811</td>
	    <td class="FieldLabel">Underwriting fee</td>
	    <td align="right"><ml:EncodedLiteral id="sQMUwF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMUwFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMUwF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">812</td>
	    <td class="FieldLabel">Wire transfer</td>
	    <td align="right"><ml:EncodedLiteral id="sQMWireF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMWireFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMWireF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">813</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQM800U1FDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQM800U1F" runat="server" /></td>
	    <uc1:QMItemProps id="sQM800U1FProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQM800U1F_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">814</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQM800U2FDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQM800U2F" runat="server" /></td>
	    <uc1:QMItemProps id="sQM800U2FProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQM800U2F_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">815</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQM800U3FDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQM800U3F" runat="server" /></td>
	    <uc1:QMItemProps id="sQM800U3FProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQM800U3F_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">816</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQM800U4FDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQM800U4F" runat="server" /></td>
	    <uc1:QMItemProps id="sQM800U4FProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQM800U4F_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">817</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQM800U5FDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQM800U5F" runat="server" /></td>
	    <uc1:QMItemProps id="sQM800U5FProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQM800U5F_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">901</td>
	    <td class="FieldLabel">Prepaid interest**</td>
	    <td align="right"><ml:EncodedLiteral id="sQMIPia" runat="server" /></td>
	    <uc1:QMItemProps id="sQMIPiaProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMIPia_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">902</td>
	    <td class="FieldLabel">Mortgage Insurance Premium*</td>
	    <td align="right"><ml:EncodedLiteral id="sQMMipPia" runat="server" /></td>
	    <uc1:QMItemProps id="sQMMipPiaProps" runat="server" PaidToThirdPartyVisible="false" />
	    <td align="right"><asp:TextBox id="sQMMipPia_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">903</td>
	    <td class="FieldLabel">Hazard Insurance</td>
	    <td align="right"><ml:EncodedLiteral id="sQMHazInsPia" runat="server" /></td>
	    <uc1:QMItemProps id="sQMHazInsPiaProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMHazInsPia_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">904</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQM904PiaDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQM904Pia" runat="server" /></td>
	    <uc1:QMItemProps id="sQM904PiaProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQM904Pia_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">905</td>
	    <td class="FieldLabel">VA Funding Fee*</td>
	    <td align="right"><ml:EncodedLiteral id="sQMVaFf" runat="server" /></td>
	    <uc1:QMItemProps id="sQMVaFfProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMVaFf_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">906</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQM900U1PiaDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQM900U1Pia" runat="server" /></td>
	    <uc1:QMItemProps id="sQM900U1PiaProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQM900U1Pia_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">1002</td>
	    <td class="FieldLabel">Hazard insurance reserve</td>
	    <td align="right"><ml:EncodedLiteral id="sQMHazInsRsrv" runat="server" /></td>
	    <uc1:QMItemProps id="sQMHazInsRsrvProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMHazInsRsrv_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">1003</td>
	    <td class="FieldLabel">Mortgage insurance reserve</td>
	    <td align="right"><ml:EncodedLiteral id="sQMMInsRsrv" runat="server" /></td>
	    <uc1:QMItemProps id="sQMMInsRsrvProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMMInsRsrv_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">1004</td>
	    <td class="FieldLabel">Tax reserve</td>
	    <td align="right"><ml:EncodedLiteral id="sQMRealETxRsrv" runat="server" /></td>
	    <uc1:QMItemProps id="sQMRealETxRsrvProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMRealETxRsrv_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">1005</td>
	    <td class="FieldLabel">School Tax reserve</td>
	    <td align="right"><ml:EncodedLiteral id="sQMSchoolTxRsrv" runat="server" /></td>
	    <uc1:QMItemProps id="sQMSchoolTxRsrvProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMSchoolTxRsrv_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">1006</td>
	    <td class="FieldLabel">Flood insurance reserve</td>
	    <td align="right"><ml:EncodedLiteral id="sQMFloodInsRsrv" runat="server" /></td>
	    <uc1:QMItemProps id="sQMFloodInsRsrvProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMFloodInsRsrv_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">1007</td>
	    <td class="FieldLabel">Aggregate adjustment</td>
	    <td align="right"><ml:EncodedLiteral id="sQMAggregateAdjRsrv" runat="server" /></td>
	    <uc1:QMItemProps id="sQMAggregateAdjRsrvProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMAggregateAdjRsrv_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">1008</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQM1006ProHExpDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQM1006Rsrv" runat="server" /></td>
	    <uc1:QMItemProps id="sQM1006RsrvProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQM1006Rsrv_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">1009</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQM1007ProHExpDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQM1007Rsrv" runat="server" /></td>
	    <uc1:QMItemProps id="sQM1007RsrvProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQM1007Rsrv_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <asp:PlaceHolder runat="server" ID="phAdditionalSection1000CustomFees">
        <tr class="GridAlternatingItem">
    	    <td class="FieldLabel">1010</td>
    	    <td class="FieldLabel"><ml:EncodedLiteral id="sQMU3RsrvDesc" runat="server" /></td>
    	    <td align="right"><ml:EncodedLiteral id="sQMU3Rsrv" runat="server" /></td>
    	    <uc1:QMItemProps id="sQMU3RsrvProps" runat="server" />
    	    <td align="right"><asp:TextBox id="sQMU3Rsrv_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
        </tr>
        <tr class="GridItem">
    	    <td class="FieldLabel">1011</td>
    	    <td class="FieldLabel"><ml:EncodedLiteral id="sQMU4RsrvDesc" runat="server" /></td>
    	    <td align="right"><ml:EncodedLiteral id="sQMU4Rsrv" runat="server" /></td>
    	    <uc1:QMItemProps id="sQMU4RsrvProps" runat="server" />
    	    <td align="right"><asp:TextBox id="sQMU4Rsrv_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
        </tr>
    </asp:PlaceHolder>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">1102</td>
	    <td class="FieldLabel">Closing/Escrow Fee</td>
	    <td align="right"><ml:EncodedLiteral id="sQMEscrowF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMEscrowFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMEscrowF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">1103</td>
	    <td class="FieldLabel">Owner's title Insurance</td>
	    <td align="right"><ml:EncodedLiteral id="sQMOwnerTitleInsF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMOwnerTitleInsFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMOwnerTitleInsF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">1104</td>
	    <td class="FieldLabel">Lender's title Insurance</td>
	    <td align="right"><ml:EncodedLiteral id="sQMTitleInsF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMTitleInsFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMTitleInsF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">1109</td>
	    <td class="FieldLabel">Doc preparation fee</td>
	    <td align="right"><ml:EncodedLiteral id="sQMDocPrepF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMDocPrepFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMDocPrepF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">1110</td>
	    <td class="FieldLabel">Notary fees</td>
	    <td align="right"><ml:EncodedLiteral id="sQMNotaryF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMNotaryFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMNotaryF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">1111</td>
	    <td class="FieldLabel">Attorney fees</td>
	    <td align="right"><ml:EncodedLiteral id="sQMAttorneyF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMAttorneyFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMAttorneyF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">1112</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQMU1TcDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQMU1Tc" runat="server" /></td>
	    <uc1:QMItemProps id="sQMU1TcProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMU1Tc_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">1113</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQMU2TcDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQMU2Tc" runat="server" /></td>
	    <uc1:QMItemProps id="sQMU2TcProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMU2Tc_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">1114</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQMU3TcDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQMU3Tc" runat="server" /></td>
	    <uc1:QMItemProps id="sQMU3TcProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMU3Tc_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">1115</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQMU4TcDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQMU4Tc" runat="server" /></td>
	    <uc1:QMItemProps id="sQMU4TcProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMU4Tc_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">1201</td>
	    <td class="FieldLabel">Recording fees</td>
	    <td align="right"><ml:EncodedLiteral id="sQMRecF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMRecFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMRecF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">1204</td>
	    <td class="FieldLabel">City/County tax stamps</td>
	    <td align="right"><ml:EncodedLiteral id="sQMCountyRtc" runat="server" /></td>
	    <uc1:QMItemProps id="sQMCountyRtcProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMCountyRtc_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">1205</td>
	    <td class="FieldLabel">State tax/stamps</td>
	    <td align="right"><ml:EncodedLiteral id="sQMStateRtc" runat="server" /></td>
	    <uc1:QMItemProps id="sQMStateRtcProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMStateRtc_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">1206</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQMU1GovRtcDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQMU1GovRtc" runat="server" /></td>
	    <uc1:QMItemProps id="sQMU1GovRtcProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMU1GovRtc_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">1207</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQMU2GovRtcDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQMU2GovRtc" runat="server" /></td>
	    <uc1:QMItemProps id="sQMU2GovRtcProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMU2GovRtc_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">1208</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQMU3GovRtcDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQMU3GovRtc" runat="server" /></td>
	    <uc1:QMItemProps id="sQMU3GovRtcProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMU3GovRtc_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">1302</td>
	    <td class="FieldLabel">Pest Inspection</td>
	    <td align="right"><ml:EncodedLiteral id="sQMPestInspectF" runat="server" /></td>
	    <uc1:QMItemProps id="sQMPestInspectFProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMPestInspectF_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">1303</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQMU1ScDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQMU1Sc" runat="server" /></td>
	    <uc1:QMItemProps id="sQMU1ScProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMU1Sc_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">1304</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQMU2ScDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQMU2Sc" runat="server" /></td>
	    <uc1:QMItemProps id="sQMU2ScProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMU2Sc_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">1305</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQMU3ScDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQMU3Sc" runat="server" /></td>
	    <uc1:QMItemProps id="sQMU3ScProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMU3Sc_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridAlternatingItem">
	    <td class="FieldLabel">1306</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQMU4ScDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQMU4Sc" runat="server" /></td>
	    <uc1:QMItemProps id="sQMU4ScProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMU4Sc_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
    <tr class="GridItem">
	    <td class="FieldLabel">1307</td>
	    <td class="FieldLabel"><ml:EncodedLiteral id="sQMU5ScDesc" runat="server" /></td>
	    <td align="right"><ml:EncodedLiteral id="sQMU5Sc" runat="server" /></td>
	    <uc1:QMItemProps id="sQMU5ScProps" runat="server" />
	    <td align="right"><asp:TextBox id="sQMU5Sc_QMAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
    </tr>
</table>
<div style="width:605px">
    <table cellspacing="0" cellpadding="0" border="0" style="float:right">
        <tr>
            <td style="padding-right:5px;">Total QM Fee Amount</td>
            <td align="right"><asp:TextBox id="sQMTotFeeAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
        </tr>
        <tr>
            <td style="padding-right:5px;">Financed QM Fee Amount</td>
            <td align="right"><asp:TextBox id="sQMTotFinFeeAmount" runat="server" style="text-align:right;" Width="70px" ReadOnly="true" /></td>
        </tr>
    </table>
</div>