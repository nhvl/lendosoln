﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos.Underwriting.QM
{
    public partial class PointAndFees : BaseLoanUserControl, IAutoLoadUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            phAdditionalSection1000CustomFees.Visible = BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId).EnableAdditionalSection1000CustomFees;
        }

        #region IAutoLoadUserControl Members

        public void LoadData()
        {
            CPageData loanData = CPageData.CreateUsingSmartDependency(LoanID, typeof(PointAndFees));
            loanData.InitLoad();

            divWarning.Visible = loanData.sQMShowWarning;

            bool _sIsManuallySetThirdPartyAffiliateProps = loanData.sIsManuallySetThirdPartyAffiliateProps;
            
            sQMTotFeeAmount.Text = loanData.sQMTotFeeAmount_rep;
            sQMTotFinFeeAmount.Text = loanData.sQMTotFinFeeAmount_rep;

            sQMLOrigF.Text = loanData.sQMLOrigF_rep;
            sQMLOrigF_QMAmount.Text = loanData.sQMLOrigF_QMAmount_rep;
            sQMLOrigFProps.InitItemProps(loanData.sQMLOrigFProps);

            sQMGfeOriginatorCompF.Text = loanData.sQMGfeOriginatorCompF_rep;
            sQMGfeOriginatorCompF_QMAmount.Text = loanData.sQMGfeOriginatorCompF_QMAmount_rep;
            sQMGfeOriginatorCompFProps.InitItemProps(loanData.sQMGfeOriginatorCompFProps);

            sQMGfeDiscountPointF.Text = loanData.sQMGfeDiscountPointF_rep;
            sQMGfeDiscountPointF_QMAmount.Text = loanData.sQMGfeDiscountPointF_QMAmount_rep;
            sQMGfeDiscountPointFProps.InitItemProps(loanData.sQMGfeDiscountPointFProps);

            sQMApprF.Text = loanData.sQMApprF_rep;
            sQMApprF_QMAmount.Text = loanData.sQMApprF_QMAmount_rep;
            sQMApprFProps.InitItemProps(loanData.sQMApprFProps);
            ProcessAffiliateEditing(sQMApprFProps, loanData, 804);

            sQMCrF.Text = loanData.sQMCrF_rep;
            sQMCrF_QMAmount.Text = loanData.sQMCrF_QMAmount_rep;
            sQMCrFProps.InitItemProps(loanData.sQMCrFProps);
            ProcessAffiliateEditing(sQMCrFProps, loanData, 805);

            sQMTxServF.Text = loanData.sQMTxServF_rep;
            sQMTxServF_QMAmount.Text = loanData.sQMTxServF_QMAmount_rep;
            sQMTxServFProps.InitItemProps(loanData.sQMTxServFProps);
            ProcessAffiliateEditing(sQMTxServFProps, loanData, 806);

            sQMFloodCertificationF.Text = loanData.sQMFloodCertificationF_rep;
            sQMFloodCertificationF_QMAmount.Text = loanData.sQMFloodCertificationF_QMAmount_rep;
            sQMFloodCertificationFProps.InitItemProps(loanData.sQMFloodCertificationFProps);
            ProcessAffiliateEditing(sQMFloodCertificationFProps, loanData, 807);

            sQMMBrokF.Text = loanData.sQMMBrokF_rep;
            sQMMBrokF_QMAmount.Text = loanData.sQMMBrokF_QMAmount_rep;
            sQMMBrokFProps.InitItemProps(loanData.sQMMBrokFProps);
            ProcessAffiliateEditing(sQMMBrokFProps, loanData, 808);

            sQMInspectF.Text = loanData.sQMInspectF_rep;
            sQMInspectF_QMAmount.Text = loanData.sQMInspectF_QMAmount_rep;
            sQMInspectFProps.InitItemProps(loanData.sQMInspectFProps);
            ProcessAffiliateEditing(sQMInspectFProps, loanData, 809);

            sQMProcF.Text = loanData.sQMProcF_rep;
            sQMProcF_QMAmount.Text = loanData.sQMProcF_QMAmount_rep;
            sQMProcFProps.InitItemProps(loanData.sQMProcFProps);
            ProcessAffiliateEditing(sQMProcFProps, loanData, 810);

            sQMUwF.Text = loanData.sQMUwF_rep;
            sQMUwF_QMAmount.Text = loanData.sQMUwF_QMAmount_rep;
            sQMUwFProps.InitItemProps(loanData.sQMUwFProps);
            ProcessAffiliateEditing(sQMUwFProps, loanData, 811);

            sQMWireF.Text = loanData.sQMWireF_rep;
            sQMWireF_QMAmount.Text = loanData.sQMWireF_QMAmount_rep;
            sQMWireFProps.InitItemProps(loanData.sQMWireFProps);
            ProcessAffiliateEditing(sQMWireFProps, loanData, 812);

            sQM800U1FDesc.Text = loanData.sQM800U1FDesc;
            sQM800U1F.Text = loanData.sQM800U1F_rep;
            sQM800U1F_QMAmount.Text = loanData.sQM800U1F_QMAmount_rep;
            sQM800U1FProps.InitItemProps(loanData.sQM800U1FProps);
            ProcessAffiliateEditing(sQM800U1FProps, loanData, 813);

            sQM800U2FDesc.Text = loanData.sQM800U2FDesc;
            sQM800U2F.Text = loanData.sQM800U2F_rep;
            sQM800U2F_QMAmount.Text = loanData.sQM800U2F_QMAmount_rep;
            sQM800U2FProps.InitItemProps(loanData.sQM800U2FProps);
            ProcessAffiliateEditing(sQM800U2FProps, loanData, 814);

            sQM800U3FDesc.Text = loanData.sQM800U3FDesc;
            sQM800U3F.Text = loanData.sQM800U3F_rep;
            sQM800U3F_QMAmount.Text = loanData.sQM800U3F_QMAmount_rep;
            sQM800U3FProps.InitItemProps(loanData.sQM800U3FProps);
            ProcessAffiliateEditing(sQM800U3FProps, loanData, 815);

            sQM800U4FDesc.Text = loanData.sQM800U4FDesc;
            sQM800U4F.Text = loanData.sQM800U4F_rep;
            sQM800U4F_QMAmount.Text = loanData.sQM800U4F_QMAmount_rep;
            sQM800U4FProps.InitItemProps(loanData.sQM800U4FProps);
            ProcessAffiliateEditing(sQM800U4FProps, loanData, 816);

            sQM800U5FDesc.Text = loanData.sQM800U5FDesc;
            sQM800U5F.Text = loanData.sQM800U5F_rep;
            sQM800U5F_QMAmount.Text = loanData.sQM800U5F_QMAmount_rep;
            sQM800U5FProps.InitItemProps(loanData.sQM800U5FProps);
            ProcessAffiliateEditing(sQM800U5FProps, loanData, 817);

            sQMIPia.Text = loanData.sQMIPia_rep;
            sQMIPia_QMAmount.Text = loanData.sQMIPia_QMAmount_rep;
            sQMIPiaProps.InitItemProps(loanData.sQMIPiaProps);

            sQMMipPia.Text = loanData.sQMMipPia_rep;
            sQMMipPia_QMAmount.Text = loanData.sQMMipPia_QMAmount_rep;
            sQMMipPiaProps.InitItemProps(loanData.sQMMipPiaProps);
            ProcessAffiliateEditing(sQMMipPiaProps, loanData, 902);

            sQMHazInsPia.Text = loanData.sQMHazInsPia_rep;
            sQMHazInsPia_QMAmount.Text = loanData.sQMHazInsPia_QMAmount_rep;
            sQMHazInsPiaProps.InitItemProps(loanData.sQMHazInsPiaProps);
            ProcessAffiliateEditing(sQMHazInsPiaProps, loanData, 903);

            sQM904PiaDesc.Text = loanData.sQM904PiaDesc;
            sQM904Pia.Text = loanData.sQM904Pia_rep;
            sQM904Pia_QMAmount.Text = loanData.sQM904Pia_QMAmount_rep;
            sQM904PiaProps.InitItemProps(loanData.sQM904PiaProps);
            ProcessAffiliateEditing(sQM904PiaProps, loanData, 80);

            sQMVaFf.Text = loanData.sQMVaFf_rep;
            sQMVaFf_QMAmount.Text = loanData.sQMVaFf_QMAmount_rep;
            sQMVaFfProps.InitItemProps(loanData.sQMVaFfProps);
            ProcessAffiliateEditing(sQMVaFfProps, loanData, 905);

            sQM900U1PiaDesc.Text = loanData.sQM900U1PiaDesc;
            sQM900U1Pia.Text = loanData.sQM900U1Pia_rep;
            sQM900U1Pia_QMAmount.Text = loanData.sQM900U1Pia_QMAmount_rep;
            sQM900U1PiaProps.InitItemProps(loanData.sQM900U1PiaProps);

            sQMHazInsRsrv.Text = loanData.sQMHazInsRsrv_rep;
            sQMHazInsRsrv_QMAmount.Text = loanData.sQMHazInsRsrv_QMAmount_rep;
            sQMHazInsRsrvProps.InitItemProps(loanData.sQMHazInsRsrvProps);

            sQMMInsRsrv.Text = loanData.sQMMInsRsrv_rep;
            sQMMInsRsrv_QMAmount.Text = loanData.sQMMInsRsrv_QMAmount_rep;
            sQMMInsRsrvProps.InitItemProps(loanData.sQMMInsRsrvProps);

            sQMRealETxRsrv.Text = loanData.sQMRealETxRsrv_rep;
            sQMRealETxRsrv_QMAmount.Text = loanData.sQMRealETxRsrv_QMAmount_rep;
            sQMRealETxRsrvProps.InitItemProps(loanData.sQMRealETxRsrvProps);

            sQMSchoolTxRsrv.Text = loanData.sQMSchoolTxRsrv_rep;
            sQMSchoolTxRsrv_QMAmount.Text = loanData.sQMSchoolTxRsrv_QMAmount_rep;
            sQMSchoolTxRsrvProps.InitItemProps(loanData.sQMSchoolTxRsrvProps);

            sQMFloodInsRsrv.Text = loanData.sQMFloodInsRsrv_rep;
            sQMFloodInsRsrv_QMAmount.Text = loanData.sQMFloodInsRsrv_QMAmount_rep;
            sQMFloodInsRsrvProps.InitItemProps(loanData.sQMFloodInsRsrvProps);

            sQMAggregateAdjRsrv.Text = loanData.sQMAggregateAdjRsrv_rep;
            sQMAggregateAdjRsrv_QMAmount.Text = loanData.sQMAggregateAdjRsrv_QMAmount_rep;
            sQMAggregateAdjRsrvProps.InitItemProps(loanData.sQMAggregateAdjRsrvProps);

            sQM1006ProHExpDesc.Text = loanData.sQM1006ProHExpDesc;
            sQM1006Rsrv.Text = loanData.sQM1006Rsrv_rep;
            sQM1006Rsrv_QMAmount.Text = loanData.sQM1006Rsrv_QMAmount_rep;
            sQM1006RsrvProps.InitItemProps(loanData.sQM1006RsrvProps);

            sQM1007ProHExpDesc.Text = loanData.sQM1007ProHExpDesc;
            sQM1007Rsrv.Text = loanData.sQM1007Rsrv_rep;
            sQM1007Rsrv_QMAmount.Text = loanData.sQM1007Rsrv_QMAmount_rep;
            sQM1007RsrvProps.InitItemProps(loanData.sQM1007RsrvProps);

            sQMU3RsrvDesc.Text = loanData.sQMU3RsrvDesc;
            sQMU3Rsrv.Text = loanData.sQMU3Rsrv_rep;
            sQMU3Rsrv_QMAmount.Text = loanData.sQMU3Rsrv_QMAmount_rep;
            sQMU3RsrvProps.InitItemProps(loanData.sQMU3RsrvProps);

            sQMU4RsrvDesc.Text = loanData.sQMU4RsrvDesc;
            sQMU4Rsrv.Text = loanData.sQMU4Rsrv_rep;
            sQMU4Rsrv_QMAmount.Text = loanData.sQMU4Rsrv_QMAmount_rep;
            sQMU4RsrvProps.InitItemProps(loanData.sQMU4RsrvProps);

            sQMEscrowF.Text = loanData.sQMEscrowF_rep;
            sQMEscrowF_QMAmount.Text = loanData.sQMEscrowF_QMAmount_rep;
            sQMEscrowFProps.InitItemProps(loanData.sQMEscrowFProps);
            ProcessAffiliateEditing(sQMEscrowFProps, loanData, 1102);

            sQMOwnerTitleInsF.Text = loanData.sQMOwnerTitleInsF_rep;
            sQMOwnerTitleInsF_QMAmount.Text = loanData.sQMOwnerTitleInsF_QMAmount_rep;
            sQMOwnerTitleInsFProps.InitItemProps(loanData.sQMOwnerTitleInsFProps);
            ProcessAffiliateEditing(sQMOwnerTitleInsFProps, loanData, 1103);

            sQMTitleInsF.Text = loanData.sQMTitleInsF_rep;
            sQMTitleInsF_QMAmount.Text = loanData.sQMTitleInsF_QMAmount_rep;
            sQMTitleInsFProps.InitItemProps(loanData.sQMTitleInsFProps);
            ProcessAffiliateEditing(sQMTitleInsFProps, loanData, 1104);

            sQMDocPrepF.Text = loanData.sQMDocPrepF_rep;
            sQMDocPrepF_QMAmount.Text = loanData.sQMDocPrepF_QMAmount_rep;
            sQMDocPrepFProps.InitItemProps(loanData.sQMDocPrepFProps);
            ProcessAffiliateEditing(sQMDocPrepFProps, loanData, 1109);

            sQMNotaryF.Text = loanData.sQMNotaryF_rep;
            sQMNotaryF_QMAmount.Text = loanData.sQMNotaryF_QMAmount_rep;
            sQMNotaryFProps.InitItemProps(loanData.sQMNotaryFProps);
            ProcessAffiliateEditing(sQMNotaryFProps, loanData, 1110);

            sQMAttorneyF.Text = loanData.sQMAttorneyF_rep;
            sQMAttorneyF_QMAmount.Text = loanData.sQMAttorneyF_QMAmount_rep;
            sQMAttorneyFProps.InitItemProps(loanData.sQMAttorneyFProps);
            ProcessAffiliateEditing(sQMAttorneyFProps, loanData, 1111);

            sQMU1TcDesc.Text = loanData.sQMU1TcDesc;
            sQMU1Tc.Text = loanData.sQMU1Tc_rep;
            sQMU1Tc_QMAmount.Text = loanData.sQMU1Tc_QMAmount_rep;
            sQMU1TcProps.InitItemProps(loanData.sQMU1TcProps);
            ProcessAffiliateEditing(sQMU1TcProps, loanData, 1112);

            sQMU2TcDesc.Text = loanData.sQMU2TcDesc;
            sQMU2Tc.Text = loanData.sQMU2Tc_rep;
            sQMU2Tc_QMAmount.Text = loanData.sQMU2Tc_QMAmount_rep;
            sQMU2TcProps.InitItemProps(loanData.sQMU2TcProps);
            ProcessAffiliateEditing(sQMU2TcProps, loanData, 1113);

            sQMU3TcDesc.Text = loanData.sQMU3TcDesc;
            sQMU3Tc.Text = loanData.sQMU3Tc_rep;
            sQMU3Tc_QMAmount.Text = loanData.sQMU3Tc_QMAmount_rep;
            sQMU3TcProps.InitItemProps(loanData.sQMU3TcProps);
            ProcessAffiliateEditing(sQMU3TcProps, loanData, 1114);

            sQMU4TcDesc.Text = loanData.sQMU4TcDesc;
            sQMU4Tc.Text = loanData.sQMU4Tc_rep;
            sQMU4Tc_QMAmount.Text = loanData.sQMU4Tc_QMAmount_rep;
            sQMU4TcProps.InitItemProps(loanData.sQMU4TcProps);
            ProcessAffiliateEditing(sQMU4TcProps, loanData, 1115);

            sQMRecF.Text = loanData.sQMRecF_rep;
            sQMRecF_QMAmount.Text = loanData.sQMRecF_QMAmount_rep;
            sQMRecFProps.InitItemProps(loanData.sQMRecFProps);
            ProcessAffiliateEditing(sQMRecFProps, loanData, 1201);

            sQMCountyRtc.Text = loanData.sQMCountyRtc_rep;
            sQMCountyRtc_QMAmount.Text = loanData.sQMCountyRtc_QMAmount_rep;
            sQMCountyRtcProps.InitItemProps(loanData.sQMCountyRtcProps);
            ProcessAffiliateEditing(sQMCountyRtcProps, loanData, 1204);

            sQMStateRtc.Text = loanData.sQMStateRtc_rep;
            sQMStateRtc_QMAmount.Text = loanData.sQMStateRtc_QMAmount_rep;
            sQMStateRtcProps.InitItemProps(loanData.sQMStateRtcProps);
            ProcessAffiliateEditing(sQMStateRtcProps, loanData, 1205);

            sQMU1GovRtcDesc.Text = loanData.sQMU1GovRtcDesc;
            sQMU1GovRtc.Text = loanData.sQMU1GovRtc_rep;
            sQMU1GovRtc_QMAmount.Text = loanData.sQMU1GovRtc_QMAmount_rep;
            sQMU1GovRtcProps.InitItemProps(loanData.sQMU1GovRtcProps);
            ProcessAffiliateEditing(sQMU1GovRtcProps, loanData, 1206);

            sQMU2GovRtcDesc.Text = loanData.sQMU2GovRtcDesc;
            sQMU2GovRtc.Text = loanData.sQMU2GovRtc_rep;
            sQMU2GovRtc_QMAmount.Text = loanData.sQMU2GovRtc_QMAmount_rep;
            sQMU2GovRtcProps.InitItemProps(loanData.sQMU2GovRtcProps);
            ProcessAffiliateEditing(sQMU2GovRtcProps, loanData, 1207);

            sQMU3GovRtcDesc.Text = loanData.sQMU3GovRtcDesc;
            sQMU3GovRtc.Text = loanData.sQMU3GovRtc_rep;
            sQMU3GovRtc_QMAmount.Text = loanData.sQMU3GovRtc_QMAmount_rep;
            sQMU3GovRtcProps.InitItemProps(loanData.sQMU3GovRtcProps);
            ProcessAffiliateEditing(sQMU3GovRtcProps, loanData, 1208);

            sQMPestInspectF.Text = loanData.sQMPestInspectF_rep;
            sQMPestInspectF_QMAmount.Text = loanData.sQMPestInspectF_QMAmount_rep;
            sQMPestInspectFProps.InitItemProps(loanData.sQMPestInspectFProps);
            ProcessAffiliateEditing(sQMPestInspectFProps, loanData, 1302);

            sQMU1ScDesc.Text = loanData.sQMU1ScDesc;
            sQMU1Sc.Text = loanData.sQMU1Sc_rep;
            sQMU1Sc_QMAmount.Text = loanData.sQMU1Sc_QMAmount_rep;
            sQMU1ScProps.InitItemProps(loanData.sQMU1ScProps);
            ProcessAffiliateEditing(sQMU1ScProps, loanData, 1303);

            sQMU2ScDesc.Text = loanData.sQMU2ScDesc;
            sQMU2Sc.Text = loanData.sQMU2Sc_rep;
            sQMU2Sc_QMAmount.Text = loanData.sQMU2Sc_QMAmount_rep;
            sQMU2ScProps.InitItemProps(loanData.sQMU2ScProps);
            ProcessAffiliateEditing(sQMU2ScProps, loanData, 1304);

            sQMU3ScDesc.Text = loanData.sQMU3ScDesc;
            sQMU3Sc.Text = loanData.sQMU3Sc_rep;
            sQMU3Sc_QMAmount.Text = loanData.sQMU3Sc_QMAmount_rep;
            sQMU3ScProps.InitItemProps(loanData.sQMU3ScProps);
            ProcessAffiliateEditing(sQMU3ScProps, loanData, 1305);

            sQMU4ScDesc.Text = loanData.sQMU4ScDesc;
            sQMU4Sc.Text = loanData.sQMU4Sc_rep;
            sQMU4Sc_QMAmount.Text = loanData.sQMU4Sc_QMAmount_rep;
            sQMU4ScProps.InitItemProps(loanData.sQMU4ScProps);
            ProcessAffiliateEditing(sQMU4ScProps, loanData, 1306);

            sQMU5ScDesc.Text = loanData.sQMU5ScDesc;
            sQMU5Sc.Text = loanData.sQMU5Sc_rep;
            sQMU5Sc_QMAmount.Text = loanData.sQMU5Sc_QMAmount_rep;
            sQMU5ScProps.InitItemProps(loanData.sQMU5ScProps);
            ProcessAffiliateEditing(sQMU5ScProps, loanData, 1307);

            if (loanData.sGfeUsePaidToFromOfficialContact)
            {
                sQMApprFProps.PaidToThirdPartyDisabled = true;
                sQMApprFProps.ThisPartyIsAffiliateDisabled = true;

                sQMCrFProps.PaidToThirdPartyDisabled = true;
                sQMCrFProps.ThisPartyIsAffiliateDisabled = true;

                sQMFloodCertificationFProps.PaidToThirdPartyDisabled = true;
                sQMFloodCertificationFProps.ThisPartyIsAffiliateDisabled = true;

                sQMEscrowFProps.PaidToThirdPartyDisabled = true;
                sQMEscrowFProps.ThisPartyIsAffiliateDisabled = true;

                sQMTitleInsFProps.PaidToThirdPartyDisabled = true;
                sQMTitleInsFProps.ThisPartyIsAffiliateDisabled = true;

                sQMAttorneyFProps.PaidToThirdPartyDisabled = true;
                sQMAttorneyFProps.ThisPartyIsAffiliateDisabled = true;

                if (loanData.sLPurposeT == E_sLPurposeT.Purchase)
                {
                    sQMOwnerTitleInsFProps.PaidToThirdPartyDisabled = true;
                    sQMOwnerTitleInsFProps.ThisPartyIsAffiliateDisabled = true;
                }
            }
        }

        private void ProcessAffiliateEditing(QMItemProps props, CPageData dataLoan, int hudLine)
        {
            if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy 
                || dataLoan.BrokerDB.IsEnableGfe2015 == false
                || dataLoan.sIsManuallySetThirdPartyAffiliateProps )
            {
                return; // Allow normal edit.
            }

            // We are in a migrated mode with no bypass.  If there is an agent record attached, do not allow edit.
            Func<BaseClosingCostFee, bool> setFilter = bFee => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)bFee, dataLoan.sOriginatorCompensationPaymentSourceT, dataLoan.sDisclosureRegulationT, dataLoan.sGfeIsTPOTransaction);

            BorrowerClosingCostFee thisMigratedFee = null;
            foreach (BorrowerClosingCostFee fee in dataLoan.sClosingCostSet.GetFees(setFilter))
            {
                if (fee.HudLine == hudLine)
                {
                    thisMigratedFee = fee;
                    break;
                }
            }

            if (thisMigratedFee != null)
            {
                if (thisMigratedFee.BeneficiaryAgentId != Guid.Empty)
                {
                    // There is an agent assigned.  Use its calculation.
                    props.ThisPartyIsAffiliateDisabled = true;
                    props.PaidToThirdPartyDisabled = true;
                }
            }
        }

        public void SaveData()
        {
        }

        #endregion
    }
}