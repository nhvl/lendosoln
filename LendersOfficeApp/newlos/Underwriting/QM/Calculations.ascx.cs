﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;
using DataAccess;

namespace LendersOfficeApp.newlos.Underwriting.QM
{
    public partial class Calculations : BaseLoanUserControl, IAutoLoadUserControl
    {
        protected bool AllowNonExcludableDiscount
        {
            get
            {
                BrokerDB broker = BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                return broker.AllowNonExcludableDiscountPoints;
            }
        }
        

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region IAutoLoadUserControl Members

        public void LoadData()
        {
            CPageData loanData = CPageData.CreateUsingSmartDependency(LoanID, typeof(Calculations));
            loanData.InitLoad();

            divWarning.Visible = loanData.sQMShowWarning;

            sRLckdD.Text = loanData.sRLckdD_rep;
            sInterestRateSetD.Text = loanData.sInterestRateSetD_rep;
            sInterestRateSetDLckd.Checked = loanData.sInterestRateSetDLckd;
            sDocMagicClosingD.Text = loanData.sDocMagicClosingD_rep;
            sQMAveragePrimeOfferR.Text = loanData.sQMAveragePrimeOfferR_rep;
            sQMAveragePrimeOfferRLck.Checked = loanData.sQMAveragePrimeOfferRLck;
            sQMParR.Text = loanData.sQMParR_rep;
            sQMTotFeeAmount.Text = loanData.sQMTotFeeAmount_rep;
            sQMTotFinFeeAmount.Text = loanData.sQMTotFinFeeAmount_rep;
            sQMExclDiscntPnt.Text = loanData.sQMExclDiscntPnt_rep;
            sQMExclDiscntPntLck.Checked = loanData.sQMExclDiscntPntLck;
            sQMFhaUfmipAtClosingLck.Checked = loanData.sQMFhaUfmipAtClosingLck;
            sQMFhaUfmipAtClosing.Text = loanData.sQMFhaUfmipAtClosing_rep;
            sInterestRateSetDLckd.InputAttributes.Add("data-lckid", sInterestRateSetD.ClientID);
            sQMAveragePrimeOfferRLck.InputAttributes.Add("data-lckid", sQMAveragePrimeOfferR.ClientID);
            sQMExclDiscntPntLck.InputAttributes.Add("data-lckid", sQMExclDiscntPnt.ClientID);
            sQMFhaUfmipAtClosingLck.InputAttributes.Add("data-lckid", sQMFhaUfmipAtClosing.ClientID);
            sQMMaxPrePmntPenalty.Text = loanData.sQMMaxPrePmntPenalty_rep;
            sQMQualR.Text = loanData.sQMQualR_rep;
            sQMQualBottom.Text = loanData.sQMQualBottom_rep;
            sQMAprRSpread.Text = loanData.sQMAprRSpread_rep;
            sQMNonExcludableDiscountPointsPc.Text = loanData.sQMNonExcludableDiscountPointsPc_rep;

            sQMParRSpread.Text = loanData.sQMParRSpread_rep;
            sQMParRSpreadCalcDesc.Text = loanData.sQMParRSpreadCalcDesc; 
            sQMAprRSpreadCalcDesc.Text = loanData.sQMAprRSpreadCalcDesc;
            sQMExcessUpfrontMIP.Text = loanData.sQMExcessUpfrontMIP_rep;
            sQMExcessUpfrontMIPCalcDesc.Text = loanData.sQMExcessUpfrontMIPCalcDesc;
            sQMExcessDiscntFCalcDesc.Text = loanData.sQMExcessDiscntFCalcDesc;

            sQMTotFeeAmount_1.Text = loanData.sQMTotFeeAmount_rep;
            sQMExcessDiscntF.Text = loanData.sQMExcessDiscntF_rep;
            sQMExcessUpfrontMIP_1.Text = loanData.sQMExcessUpfrontMIP_rep;
            sQMMaxPrePmntPenalty_1.Text = loanData.sQMMaxPrePmntPenalty_rep;
            sQMDiscntBuyDownR.Text = loanData.sQMDiscntBuyDownR_rep;
            sQMDiscntBuyDownRCalcDesc.Text = loanData.sQMDiscntBuyDownRCalcDesc;
            sQMLAmt.Text = loanData.sQMLAmt_rep;
            sQMLAmt_1.Text = loanData.sQMLAmt_rep;
            sQMLAmtCalcDesc.Text = loanData.sQMLAmtCalcDesc;
            sQMTotFeeAmt.Text = loanData.sQMTotFeeAmt_rep;
            sQMTotFeePc.Text = loanData.sQMTotFeePc_rep;
            sQMExcessDiscntFPc.Text = loanData.sQMExcessDiscntFPc_rep;
            sQMMaxPointAndFeesAllowedAmt.Text = loanData.sQMMaxPointAndFeesAllowedAmt_rep;
            sQMMaxPointAndFeesAllowedPc.Text = loanData.sQMMaxPointAndFeesAllowedPc_rep;
            sQMLoanPassesPointAndFeesTest.Checked = loanData.sQMLoanPassesPointAndFeesTest;
            sQMLoanDoesNotHaveNegativeAmort.Checked = loanData.sQMLoanDoesNotHaveNegativeAmort;
            sQMLoanDoesNotHaveAmortTermOver30Yr.Checked = loanData.sQMLoanDoesNotHaveAmortTermOver30Yr;
            sQMLoanDoesNotHaveBalloonFeature.Checked = loanData.sQMLoanDoesNotHaveBalloonFeature;
            sQMLoanHasDtiLessThan43Pc.Checked = loanData.sQMLoanHasDtiLessThan43Pc;
            sQMIsVerifiedIncomeAndAssets.Checked = loanData.sQMIsVerifiedIncomeAndAssets;
            sQMIsEligibleByLoanPurchaseAgency.Checked = loanData.sQMIsEligibleByLoanPurchaseAgency;
            sQMLoanPassedComplianceChecks.Checked = loanData.sQMLoanPassedComplianceChecks;

            sGfeOriginatorCompF.Text = loanData.sGfeOriginatorCompF_rep;

            Tools.Bind_sQMStatusT(sQMStatusT);
            Tools.SetDropDownListValue(sQMStatusT, loanData.sQMStatusT);

            sQMStatusTCombo.Text = sQMStatusT.Items[sQMStatusT.SelectedIndex].Text;

            sHighPricedMortgageTLckd.Checked = loanData.sHighPricedMortgageTLckd;
            sHighPricedMortgageTLckd.InputAttributes.Add("data-lckid", sHighPricedMortgageT.ClientID);

            sIsExemptFromAtr.Checked = loanData.sIsExemptFromAtr;

            Tools.Bind_sHighPricedMortgageT(sHighPricedMortgageT);
            Tools.SetDropDownListValue(sHighPricedMortgageT, loanData.sHighPricedMortgageT);

            if (!loanData.sQMLoanHasComplianceChecksOnFile)
            {
                ComplianceChecks.Style.Add("display", "none");
            }

            ComplianceChecks.Visible = false;

            Tools.Bind_sQMLoanPurchaseAgency(sQMLoanPurchaseAgency);
            Tools.SetDropDownListValue(sQMLoanPurchaseAgency, loanData.sQMLoanPurchaseAgency);

            sQMLoanPurchaseAgency.Enabled = !loanData.sQMLoanPurchaseAgencyIsDisabled;

            sQMLoanPurchaseAgencyCombo.Text = sQMLoanPurchaseAgency.Items[sQMLoanPurchaseAgency.SelectedIndex].Text;

            if (loanData.sQMLoanPurchaseAgencyIsDisabled)
            {
                sQMLoanPurchaseAgency.Style.Add("display", "none");
                sQMLoanPurchaseAgencyCombo.Attributes.Add("ReadOnly", "");
            }
            else
                sQMLoanPurchaseAgencyCombo.Visible = false;
            if (loanData.sQMLoanPassesPointAndFeesTest)
            {
                StatusRow.Attributes.Add("class", "QmSuccess");
            }
            else
            {
                StatusRow.Attributes.Add("class", "QmFail");
            }
            
        }

        public void SaveData()
        {
        }

        #endregion
    }
}