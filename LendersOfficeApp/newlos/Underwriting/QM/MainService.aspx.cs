﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Underwriting.QM
{
    public partial class PointAndFeesServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        

        protected override DataAccess.CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(PointAndFeesServiceItem));
        }

        protected override void BindData(DataAccess.CPageData dataLoan, DataAccess.CAppData dataApp)
        {
            dataLoan.sQMLOrigFProps_Payer = GetInt("sQMLOrigFProps_PdByT_dd");
            dataLoan.sQMLOrigFProps_Apr = GetBool("sQMLOrigFProps_Apr_chk");
            dataLoan.sQMLOrigFProps_PaidToThirdParty = GetBool("sQMLOrigFProps_TrdPty_chk");
            dataLoan.sQMLOrigFProps_ThisPartyIsAffiliate = GetBool("sQMLOrigFProps_Aff_chk");

            dataLoan.sQMGfeOriginatorCompFProps_Payer = GetInt("sQMGfeOriginatorCompFProps_PdByT_dd");
            dataLoan.sQMGfeOriginatorCompFProps_Apr = GetBool("sQMGfeOriginatorCompFProps_Apr_chk");
            dataLoan.sQMGfeOriginatorCompFProps_PaidToThirdParty = GetBool("sQMGfeOriginatorCompFProps_TrdPty_chk");
            dataLoan.sQMGfeOriginatorCompFProps_ThisPartyIsAffiliate = GetBool("sQMGfeOriginatorCompFProps_Aff_chk");

            dataLoan.sQMGfeDiscountPointFProps_Payer = GetInt("sQMGfeDiscountPointFProps_PdByT_dd");
            dataLoan.sQMGfeDiscountPointFProps_Apr = GetBool("sQMGfeDiscountPointFProps_Apr_chk");
            //dataLoan.sQMGfeDiscountPointFProps_PaidToThirdParty = GetBool("sQMGfeDiscountPointFProps_TrdPty_chk");
            //dataLoan.sQMGfeDiscountPointFProps_ThisPartyIsAffiliate = GetBool("sQMGfeDiscountPointFProps_Aff_chk");

            dataLoan.sQMApprFProps_Payer = GetInt("sQMApprFProps_PdByT_dd");
            dataLoan.sQMApprFProps_Apr = GetBool("sQMApprFProps_Apr_chk");
            dataLoan.sQMApprFProps_PaidToThirdParty = GetBool("sQMApprFProps_TrdPty_chk");
            dataLoan.sQMApprFProps_ThisPartyIsAffiliate = GetBool("sQMApprFProps_Aff_chk");

            dataLoan.sQMCrFProps_Payer = GetInt("sQMCrFProps_PdByT_dd");
            dataLoan.sQMCrFProps_Apr = GetBool("sQMCrFProps_Apr_chk");
            dataLoan.sQMCrFProps_PaidToThirdParty = GetBool("sQMCrFProps_TrdPty_chk");
            dataLoan.sQMCrFProps_ThisPartyIsAffiliate = GetBool("sQMCrFProps_Aff_chk");

            dataLoan.sQMTxServFProps_Payer = GetInt("sQMTxServFProps_PdByT_dd");
            dataLoan.sQMTxServFProps_Apr = GetBool("sQMTxServFProps_Apr_chk");
            dataLoan.sQMTxServFProps_PaidToThirdParty = GetBool("sQMTxServFProps_TrdPty_chk");
            dataLoan.sQMTxServFProps_ThisPartyIsAffiliate = GetBool("sQMTxServFProps_Aff_chk");

            dataLoan.sQMFloodCertificationFProps_Payer = GetInt("sQMFloodCertificationFProps_PdByT_dd");
            dataLoan.sQMFloodCertificationFProps_Apr = GetBool("sQMFloodCertificationFProps_Apr_chk");
            dataLoan.sQMFloodCertificationFProps_PaidToThirdParty = GetBool("sQMFloodCertificationFProps_TrdPty_chk");
            dataLoan.sQMFloodCertificationFProps_ThisPartyIsAffiliate = GetBool("sQMFloodCertificationFProps_Aff_chk");

            dataLoan.sQMMBrokFProps_Payer = GetInt("sQMMBrokFProps_PdByT_dd");
            dataLoan.sQMMBrokFProps_Apr = GetBool("sQMMBrokFProps_Apr_chk");
            dataLoan.sQMMBrokFProps_PaidToThirdParty = GetBool("sQMMBrokFProps_TrdPty_chk");
            dataLoan.sQMMBrokFProps_ThisPartyIsAffiliate = GetBool("sQMMBrokFProps_Aff_chk");

            dataLoan.sQMInspectFProps_Payer = GetInt("sQMInspectFProps_PdByT_dd");
            dataLoan.sQMInspectFProps_Apr = GetBool("sQMInspectFProps_Apr_chk");
            dataLoan.sQMInspectFProps_PaidToThirdParty = GetBool("sQMInspectFProps_TrdPty_chk");
            dataLoan.sQMInspectFProps_ThisPartyIsAffiliate = GetBool("sQMInspectFProps_Aff_chk");

            dataLoan.sQMProcFProps_Payer = GetInt("sQMProcFProps_PdByT_dd");
            dataLoan.sQMProcFProps_Apr = GetBool("sQMProcFProps_Apr_chk");
            dataLoan.sQMProcFProps_PaidToThirdParty = GetBool("sQMProcFProps_TrdPty_chk");
            dataLoan.sQMProcFProps_ThisPartyIsAffiliate = GetBool("sQMProcFProps_Aff_chk");

            dataLoan.sQMUwFProps_Payer = GetInt("sQMUwFProps_PdByT_dd");
            dataLoan.sQMUwFProps_Apr = GetBool("sQMUwFProps_Apr_chk");
            dataLoan.sQMUwFProps_PaidToThirdParty = GetBool("sQMUwFProps_TrdPty_chk");
            dataLoan.sQMUwFProps_ThisPartyIsAffiliate = GetBool("sQMUwFProps_Aff_chk");

            dataLoan.sQMWireFProps_Payer = GetInt("sQMWireFProps_PdByT_dd");
            dataLoan.sQMWireFProps_Apr = GetBool("sQMWireFProps_Apr_chk");
            dataLoan.sQMWireFProps_PaidToThirdParty = GetBool("sQMWireFProps_TrdPty_chk");
            dataLoan.sQMWireFProps_ThisPartyIsAffiliate = GetBool("sQMWireFProps_Aff_chk");

            dataLoan.sQM800U1FProps_Payer = GetInt("sQM800U1FProps_PdByT_dd");
            dataLoan.sQM800U1FProps_Apr = GetBool("sQM800U1FProps_Apr_chk");
            dataLoan.sQM800U1FProps_PaidToThirdParty = GetBool("sQM800U1FProps_TrdPty_chk");
            dataLoan.sQM800U1FProps_ThisPartyIsAffiliate = GetBool("sQM800U1FProps_Aff_chk");

            dataLoan.sQM800U2FProps_Payer = GetInt("sQM800U2FProps_PdByT_dd");
            dataLoan.sQM800U2FProps_Apr = GetBool("sQM800U2FProps_Apr_chk");
            dataLoan.sQM800U2FProps_PaidToThirdParty = GetBool("sQM800U2FProps_TrdPty_chk");
            dataLoan.sQM800U2FProps_ThisPartyIsAffiliate = GetBool("sQM800U2FProps_Aff_chk");

            dataLoan.sQM800U3FProps_Payer = GetInt("sQM800U3FProps_PdByT_dd");
            dataLoan.sQM800U3FProps_Apr = GetBool("sQM800U3FProps_Apr_chk");
            dataLoan.sQM800U3FProps_PaidToThirdParty = GetBool("sQM800U3FProps_TrdPty_chk");
            dataLoan.sQM800U3FProps_ThisPartyIsAffiliate = GetBool("sQM800U3FProps_Aff_chk");

            dataLoan.sQM800U4FProps_Payer = GetInt("sQM800U4FProps_PdByT_dd");
            dataLoan.sQM800U4FProps_Apr = GetBool("sQM800U4FProps_Apr_chk");
            dataLoan.sQM800U4FProps_PaidToThirdParty = GetBool("sQM800U4FProps_TrdPty_chk");
            dataLoan.sQM800U4FProps_ThisPartyIsAffiliate = GetBool("sQM800U4FProps_Aff_chk");

            dataLoan.sQM800U5FProps_Payer = GetInt("sQM800U5FProps_PdByT_dd");
            dataLoan.sQM800U5FProps_Apr = GetBool("sQM800U5FProps_Apr_chk");
            dataLoan.sQM800U5FProps_PaidToThirdParty = GetBool("sQM800U5FProps_TrdPty_chk");
            dataLoan.sQM800U5FProps_ThisPartyIsAffiliate = GetBool("sQM800U5FProps_Aff_chk");

            dataLoan.sQMIPiaProps_Payer = GetInt("sQMIPiaProps_PdByT_dd");
            dataLoan.sQMIPiaProps_Apr = GetBool("sQMIPiaProps_Apr_chk");
            dataLoan.sQMIPiaProps_PaidToThirdParty = GetBool("sQMIPiaProps_TrdPty_chk");
            dataLoan.sQMIPiaProps_ThisPartyIsAffiliate = GetBool("sQMIPiaProps_Aff_chk");

            dataLoan.sQMMipPiaProps_Payer = GetInt("sQMMipPiaProps_PdByT_dd");
            dataLoan.sQMMipPiaProps_Apr = GetBool("sQMMipPiaProps_Apr_chk");
            //dataLoan.sQMMipPiaProps_PaidToThirdParty = GetBool("sQMMipPiaProps_TrdPty_chk");
            //dataLoan.sQMMipPiaProps_ThisPartyIsAffiliate = GetBool("sQMMipPiaProps_Aff_chk");

            dataLoan.sQMHazInsPiaProps_Payer = GetInt("sQMHazInsPiaProps_PdByT_dd");
            dataLoan.sQMHazInsPiaProps_Apr = GetBool("sQMHazInsPiaProps_Apr_chk");
            dataLoan.sQMHazInsPiaProps_PaidToThirdParty = GetBool("sQMHazInsPiaProps_TrdPty_chk");
            dataLoan.sQMHazInsPiaProps_ThisPartyIsAffiliate = GetBool("sQMHazInsPiaProps_Aff_chk");

            dataLoan.sQM904PiaProps_Payer = GetInt("sQM904PiaProps_PdByT_dd");
            dataLoan.sQM904PiaProps_Apr = GetBool("sQM904PiaProps_Apr_chk");
            dataLoan.sQM904PiaProps_PaidToThirdParty = GetBool("sQM904PiaProps_TrdPty_chk");
            dataLoan.sQM904PiaProps_ThisPartyIsAffiliate = GetBool("sQM904PiaProps_Aff_chk");

            dataLoan.sQMVaFfProps_Payer = GetInt("sQMVaFfProps_PdByT_dd");
            dataLoan.sQMVaFfProps_Apr = GetBool("sQMVaFfProps_Apr_chk");
            dataLoan.sQMVaFfProps_PaidToThirdParty = GetBool("sQMVaFfProps_TrdPty_chk");
            dataLoan.sQMVaFfProps_ThisPartyIsAffiliate = GetBool("sQMVaFfProps_Aff_chk");

            dataLoan.sQM900U1PiaProps_Payer = GetInt("sQM900U1PiaProps_PdByT_dd");
            dataLoan.sQM900U1PiaProps_Apr = GetBool("sQM900U1PiaProps_Apr_chk");
            dataLoan.sQM900U1PiaProps_PaidToThirdParty = GetBool("sQM900U1PiaProps_TrdPty_chk");
            dataLoan.sQM900U1PiaProps_ThisPartyIsAffiliate = GetBool("sQM900U1PiaProps_Aff_chk");

            dataLoan.sQMHazInsRsrvProps_Payer = GetInt("sQMHazInsRsrvProps_PdByT_dd");
            dataLoan.sQMHazInsRsrvProps_Apr = GetBool("sQMHazInsRsrvProps_Apr_chk");
            dataLoan.sQMHazInsRsrvProps_PaidToThirdParty = GetBool("sQMHazInsRsrvProps_TrdPty_chk");
            dataLoan.sQMHazInsRsrvProps_ThisPartyIsAffiliate = GetBool("sQMHazInsRsrvProps_Aff_chk");

            dataLoan.sQMMInsRsrvProps_Payer = GetInt("sQMMInsRsrvProps_PdByT_dd");
            dataLoan.sQMMInsRsrvProps_Apr = GetBool("sQMMInsRsrvProps_Apr_chk");
            dataLoan.sQMMInsRsrvProps_PaidToThirdParty = GetBool("sQMMInsRsrvProps_TrdPty_chk");
            dataLoan.sQMMInsRsrvProps_ThisPartyIsAffiliate = GetBool("sQMMInsRsrvProps_Aff_chk");

            dataLoan.sQMRealETxRsrvProps_Payer = GetInt("sQMRealETxRsrvProps_PdByT_dd");
            dataLoan.sQMRealETxRsrvProps_Apr = GetBool("sQMRealETxRsrvProps_Apr_chk");
            dataLoan.sQMRealETxRsrvProps_PaidToThirdParty = GetBool("sQMRealETxRsrvProps_TrdPty_chk");
            dataLoan.sQMRealETxRsrvProps_ThisPartyIsAffiliate = GetBool("sQMRealETxRsrvProps_Aff_chk");

            dataLoan.sQMSchoolTxRsrvProps_Payer = GetInt("sQMSchoolTxRsrvProps_PdByT_dd");
            dataLoan.sQMSchoolTxRsrvProps_Apr = GetBool("sQMSchoolTxRsrvProps_Apr_chk");
            dataLoan.sQMSchoolTxRsrvProps_PaidToThirdParty = GetBool("sQMSchoolTxRsrvProps_TrdPty_chk");
            dataLoan.sQMSchoolTxRsrvProps_ThisPartyIsAffiliate = GetBool("sQMSchoolTxRsrvProps_Aff_chk");

            dataLoan.sQMFloodInsRsrvProps_Payer = GetInt("sQMFloodInsRsrvProps_PdByT_dd");
            dataLoan.sQMFloodInsRsrvProps_Apr = GetBool("sQMFloodInsRsrvProps_Apr_chk");
            dataLoan.sQMFloodInsRsrvProps_PaidToThirdParty = GetBool("sQMFloodInsRsrvProps_TrdPty_chk");
            dataLoan.sQMFloodInsRsrvProps_ThisPartyIsAffiliate = GetBool("sQMFloodInsRsrvProps_Aff_chk");

            dataLoan.sQMAggregateAdjRsrvProps_Payer = GetInt("sQMAggregateAdjRsrvProps_PdByT_dd");
            dataLoan.sQMAggregateAdjRsrvProps_Apr = GetBool("sQMAggregateAdjRsrvProps_Apr_chk");
            dataLoan.sQMAggregateAdjRsrvProps_PaidToThirdParty = GetBool("sQMAggregateAdjRsrvProps_TrdPty_chk");
            dataLoan.sQMAggregateAdjRsrvProps_ThisPartyIsAffiliate = GetBool("sQMAggregateAdjRsrvProps_Aff_chk");

            dataLoan.sQM1006RsrvProps_Payer = GetInt("sQM1006RsrvProps_PdByT_dd");
            dataLoan.sQM1006RsrvProps_Apr = GetBool("sQM1006RsrvProps_Apr_chk");
            dataLoan.sQM1006RsrvProps_PaidToThirdParty = GetBool("sQM1006RsrvProps_TrdPty_chk");
            dataLoan.sQM1006RsrvProps_ThisPartyIsAffiliate = GetBool("sQM1006RsrvProps_Aff_chk");

            dataLoan.sQM1007RsrvProps_Payer = GetInt("sQM1007RsrvProps_PdByT_dd");
            dataLoan.sQM1007RsrvProps_Apr = GetBool("sQM1007RsrvProps_Apr_chk");
            dataLoan.sQM1007RsrvProps_PaidToThirdParty = GetBool("sQM1007RsrvProps_TrdPty_chk");
            dataLoan.sQM1007RsrvProps_ThisPartyIsAffiliate = GetBool("sQM1007RsrvProps_Aff_chk");

            if (BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId).EnableAdditionalSection1000CustomFees)
            {
                dataLoan.sQMU3RsrvProps_Payer = GetInt("sQMU3RsrvProps_PdByT_dd");
                dataLoan.sQMU3RsrvProps_Apr = GetBool("sQMU3RsrvProps_Apr_chk");
                dataLoan.sQMU3RsrvProps_PaidToThirdParty = GetBool("sQMU3RsrvProps_TrdPty_chk");
                dataLoan.sQMU3RsrvProps_ThisPartyIsAffiliate = GetBool("sQMU3RsrvProps_Aff_chk");

                dataLoan.sQMU4RsrvProps_Payer = GetInt("sQMU4RsrvProps_PdByT_dd");
                dataLoan.sQMU4RsrvProps_Apr = GetBool("sQMU4RsrvProps_Apr_chk");
                dataLoan.sQMU4RsrvProps_PaidToThirdParty = GetBool("sQMU4RsrvProps_TrdPty_chk");
                dataLoan.sQMU4RsrvProps_ThisPartyIsAffiliate = GetBool("sQMU4RsrvProps_Aff_chk");
            }

            dataLoan.sQMEscrowFProps_Payer = GetInt("sQMEscrowFProps_PdByT_dd");
            dataLoan.sQMEscrowFProps_Apr = GetBool("sQMEscrowFProps_Apr_chk");
            dataLoan.sQMEscrowFProps_PaidToThirdParty = GetBool("sQMEscrowFProps_TrdPty_chk");
            dataLoan.sQMEscrowFProps_ThisPartyIsAffiliate = GetBool("sQMEscrowFProps_Aff_chk");

            dataLoan.sQMOwnerTitleInsFProps_Payer = GetInt("sQMOwnerTitleInsFProps_PdByT_dd");
            dataLoan.sQMOwnerTitleInsFProps_Apr = GetBool("sQMOwnerTitleInsFProps_Apr_chk");
            dataLoan.sQMOwnerTitleInsFProps_PaidToThirdParty = GetBool("sQMOwnerTitleInsFProps_TrdPty_chk");
            dataLoan.sQMOwnerTitleInsFProps_ThisPartyIsAffiliate = GetBool("sQMOwnerTitleInsFProps_Aff_chk");

            dataLoan.sQMTitleInsFProps_Payer = GetInt("sQMTitleInsFProps_PdByT_dd");
            dataLoan.sQMTitleInsFProps_Apr = GetBool("sQMTitleInsFProps_Apr_chk");
            dataLoan.sQMTitleInsFProps_PaidToThirdParty = GetBool("sQMTitleInsFProps_TrdPty_chk");
            dataLoan.sQMTitleInsFProps_ThisPartyIsAffiliate = GetBool("sQMTitleInsFProps_Aff_chk");

            dataLoan.sQMDocPrepFProps_Payer = GetInt("sQMDocPrepFProps_PdByT_dd");
            dataLoan.sQMDocPrepFProps_Apr = GetBool("sQMDocPrepFProps_Apr_chk");
            dataLoan.sQMDocPrepFProps_PaidToThirdParty = GetBool("sQMDocPrepFProps_TrdPty_chk");
            dataLoan.sQMDocPrepFProps_ThisPartyIsAffiliate = GetBool("sQMDocPrepFProps_Aff_chk");

            dataLoan.sQMNotaryFProps_Payer = GetInt("sQMNotaryFProps_PdByT_dd");
            dataLoan.sQMNotaryFProps_Apr = GetBool("sQMNotaryFProps_Apr_chk");
            dataLoan.sQMNotaryFProps_PaidToThirdParty = GetBool("sQMNotaryFProps_TrdPty_chk");
            dataLoan.sQMNotaryFProps_ThisPartyIsAffiliate = GetBool("sQMNotaryFProps_Aff_chk");

            dataLoan.sQMAttorneyFProps_Payer = GetInt("sQMAttorneyFProps_PdByT_dd");
            dataLoan.sQMAttorneyFProps_Apr = GetBool("sQMAttorneyFProps_Apr_chk");
            dataLoan.sQMAttorneyFProps_PaidToThirdParty = GetBool("sQMAttorneyFProps_TrdPty_chk");
            dataLoan.sQMAttorneyFProps_ThisPartyIsAffiliate = GetBool("sQMAttorneyFProps_Aff_chk");

            dataLoan.sQMU1TcProps_Payer = GetInt("sQMU1TcProps_PdByT_dd");
            dataLoan.sQMU1TcProps_Apr = GetBool("sQMU1TcProps_Apr_chk");
            dataLoan.sQMU1TcProps_PaidToThirdParty = GetBool("sQMU1TcProps_TrdPty_chk");
            dataLoan.sQMU1TcProps_ThisPartyIsAffiliate = GetBool("sQMU1TcProps_Aff_chk");

            dataLoan.sQMU2TcProps_Payer = GetInt("sQMU2TcProps_PdByT_dd");
            dataLoan.sQMU2TcProps_Apr = GetBool("sQMU2TcProps_Apr_chk");
            dataLoan.sQMU2TcProps_PaidToThirdParty = GetBool("sQMU2TcProps_TrdPty_chk");
            dataLoan.sQMU2TcProps_ThisPartyIsAffiliate = GetBool("sQMU2TcProps_Aff_chk");

            dataLoan.sQMU3TcProps_Payer = GetInt("sQMU3TcProps_PdByT_dd");
            dataLoan.sQMU3TcProps_Apr = GetBool("sQMU3TcProps_Apr_chk");
            dataLoan.sQMU3TcProps_PaidToThirdParty = GetBool("sQMU3TcProps_TrdPty_chk");
            dataLoan.sQMU3TcProps_ThisPartyIsAffiliate = GetBool("sQMU3TcProps_Aff_chk");

            dataLoan.sQMU4TcProps_Payer = GetInt("sQMU4TcProps_PdByT_dd");
            dataLoan.sQMU4TcProps_Apr = GetBool("sQMU4TcProps_Apr_chk");
            dataLoan.sQMU4TcProps_PaidToThirdParty = GetBool("sQMU4TcProps_TrdPty_chk");
            dataLoan.sQMU4TcProps_ThisPartyIsAffiliate = GetBool("sQMU4TcProps_Aff_chk");

            dataLoan.sQMRecFProps_Payer = GetInt("sQMRecFProps_PdByT_dd");
            dataLoan.sQMRecFProps_Apr = GetBool("sQMRecFProps_Apr_chk");
            dataLoan.sQMRecFProps_PaidToThirdParty = GetBool("sQMRecFProps_TrdPty_chk");
            dataLoan.sQMRecFProps_ThisPartyIsAffiliate = GetBool("sQMRecFProps_Aff_chk");

            dataLoan.sQMCountyRtcProps_Payer = GetInt("sQMCountyRtcProps_PdByT_dd");
            dataLoan.sQMCountyRtcProps_Apr = GetBool("sQMCountyRtcProps_Apr_chk");
            dataLoan.sQMCountyRtcProps_PaidToThirdParty = GetBool("sQMCountyRtcProps_TrdPty_chk");
            dataLoan.sQMCountyRtcProps_ThisPartyIsAffiliate = GetBool("sQMCountyRtcProps_Aff_chk");

            dataLoan.sQMStateRtcProps_Payer = GetInt("sQMStateRtcProps_PdByT_dd");
            dataLoan.sQMStateRtcProps_Apr = GetBool("sQMStateRtcProps_Apr_chk");
            dataLoan.sQMStateRtcProps_PaidToThirdParty = GetBool("sQMStateRtcProps_TrdPty_chk");
            dataLoan.sQMStateRtcProps_ThisPartyIsAffiliate = GetBool("sQMStateRtcProps_Aff_chk");

            dataLoan.sQMU1GovRtcProps_Payer = GetInt("sQMU1GovRtcProps_PdByT_dd");
            dataLoan.sQMU1GovRtcProps_Apr = GetBool("sQMU1GovRtcProps_Apr_chk");
            dataLoan.sQMU1GovRtcProps_PaidToThirdParty = GetBool("sQMU1GovRtcProps_TrdPty_chk");
            dataLoan.sQMU1GovRtcProps_ThisPartyIsAffiliate = GetBool("sQMU1GovRtcProps_Aff_chk");

            dataLoan.sQMU2GovRtcProps_Payer = GetInt("sQMU2GovRtcProps_PdByT_dd");
            dataLoan.sQMU2GovRtcProps_Apr = GetBool("sQMU2GovRtcProps_Apr_chk");
            dataLoan.sQMU2GovRtcProps_PaidToThirdParty = GetBool("sQMU2GovRtcProps_TrdPty_chk");
            dataLoan.sQMU2GovRtcProps_ThisPartyIsAffiliate = GetBool("sQMU2GovRtcProps_Aff_chk");

            dataLoan.sQMU3GovRtcProps_Payer = GetInt("sQMU3GovRtcProps_PdByT_dd");
            dataLoan.sQMU3GovRtcProps_Apr = GetBool("sQMU3GovRtcProps_Apr_chk");
            dataLoan.sQMU3GovRtcProps_PaidToThirdParty = GetBool("sQMU3GovRtcProps_TrdPty_chk");
            dataLoan.sQMU3GovRtcProps_ThisPartyIsAffiliate = GetBool("sQMU3GovRtcProps_Aff_chk");

            dataLoan.sQMPestInspectFProps_Payer = GetInt("sQMPestInspectFProps_PdByT_dd");
            dataLoan.sQMPestInspectFProps_Apr = GetBool("sQMPestInspectFProps_Apr_chk");
            dataLoan.sQMPestInspectFProps_PaidToThirdParty = GetBool("sQMPestInspectFProps_TrdPty_chk");
            dataLoan.sQMPestInspectFProps_ThisPartyIsAffiliate = GetBool("sQMPestInspectFProps_Aff_chk");

            dataLoan.sQMU1ScProps_Payer = GetInt("sQMU1ScProps_PdByT_dd");
            dataLoan.sQMU1ScProps_Apr = GetBool("sQMU1ScProps_Apr_chk");
            dataLoan.sQMU1ScProps_PaidToThirdParty = GetBool("sQMU1ScProps_TrdPty_chk");
            dataLoan.sQMU1ScProps_ThisPartyIsAffiliate = GetBool("sQMU1ScProps_Aff_chk");

            dataLoan.sQMU2ScProps_Payer = GetInt("sQMU2ScProps_PdByT_dd");
            dataLoan.sQMU2ScProps_Apr = GetBool("sQMU2ScProps_Apr_chk");
            dataLoan.sQMU2ScProps_PaidToThirdParty = GetBool("sQMU2ScProps_TrdPty_chk");
            dataLoan.sQMU2ScProps_ThisPartyIsAffiliate = GetBool("sQMU2ScProps_Aff_chk");

            dataLoan.sQMU3ScProps_Payer = GetInt("sQMU3ScProps_PdByT_dd");
            dataLoan.sQMU3ScProps_Apr = GetBool("sQMU3ScProps_Apr_chk");
            dataLoan.sQMU3ScProps_PaidToThirdParty = GetBool("sQMU3ScProps_TrdPty_chk");
            dataLoan.sQMU3ScProps_ThisPartyIsAffiliate = GetBool("sQMU3ScProps_Aff_chk");

            dataLoan.sQMU4ScProps_Payer = GetInt("sQMU4ScProps_PdByT_dd");
            dataLoan.sQMU4ScProps_Apr = GetBool("sQMU4ScProps_Apr_chk");
            dataLoan.sQMU4ScProps_PaidToThirdParty = GetBool("sQMU4ScProps_TrdPty_chk");
            dataLoan.sQMU4ScProps_ThisPartyIsAffiliate = GetBool("sQMU4ScProps_Aff_chk");

            dataLoan.sQMU5ScProps_Payer = GetInt("sQMU5ScProps_PdByT_dd");
            dataLoan.sQMU5ScProps_Apr = GetBool("sQMU5ScProps_Apr_chk");
            dataLoan.sQMU5ScProps_PaidToThirdParty = GetBool("sQMU5ScProps_TrdPty_chk");
            dataLoan.sQMU5ScProps_ThisPartyIsAffiliate = GetBool("sQMU5ScProps_Aff_chk");
        }

        protected override void LoadData(DataAccess.CPageData dataLoan, DataAccess.CAppData dataApp)
        {
            SetResult("sQMTotFeeAmount", dataLoan.sQMTotFeeAmount_rep);
            SetResult("sQMTotFinFeeAmount", dataLoan.sQMTotFinFeeAmount_rep);

            SetResult("sQMLOrigFProps_PdByT_dd", dataLoan.sQMLOrigFProps_Payer);
            SetResult("sQMLOrigFProps_Apr_chk", dataLoan.sQMLOrigFProps_Apr);
            SetResult("sQMLOrigFProps_TrdPty_chk", dataLoan.sQMLOrigFProps_PaidToThirdParty);
            SetResult("sQMLOrigFProps_Aff_chk", dataLoan.sQMLOrigFProps_ThisPartyIsAffiliate);
            SetResult("sQMLOrigF_QMAmount", dataLoan.sQMLOrigF_QMAmount_rep);

            SetResult("sQMGfeOriginatorCompFProps_PdByT_dd", dataLoan.sQMGfeOriginatorCompFProps_Payer);
            SetResult("sQMGfeOriginatorCompFProps_Apr_chk", dataLoan.sQMGfeOriginatorCompFProps_Apr);
            SetResult("sQMGfeOriginatorCompFProps_TrdPty_chk", dataLoan.sQMGfeOriginatorCompFProps_PaidToThirdParty);
            SetResult("sQMGfeOriginatorCompFProps_Aff_chk", dataLoan.sQMGfeOriginatorCompFProps_ThisPartyIsAffiliate);
            SetResult("sQMGfeOriginatorCompF_QMAmount", dataLoan.sQMGfeOriginatorCompF_QMAmount_rep);

            SetResult("sQMGfeDiscountPointFProps_PdByT_dd", dataLoan.sQMGfeDiscountPointFProps_Payer);
            SetResult("sQMGfeDiscountPointFProps_Apr_chk", dataLoan.sQMGfeDiscountPointFProps_Apr);
            //SetResult("sQMGfeDiscountPointFProps_TrdPty_chk", dataLoan.sQMGfeDiscountPointFProps_PaidToThirdParty);
            //SetResult("sQMGfeDiscountPointFProps_Aff_chk", dataLoan.sQMGfeDiscountPointFProps_ThisPartyIsAffiliate);
            SetResult("sQMGfeDiscountPointF_QMAmount", dataLoan.sQMGfeDiscountPointF_QMAmount_rep);

            SetResult("sQMApprFProps_PdByT_dd", dataLoan.sQMApprFProps_Payer);
            SetResult("sQMApprFProps_Apr_chk", dataLoan.sQMApprFProps_Apr);
            SetResult("sQMApprFProps_TrdPty_chk", dataLoan.sQMApprFProps_PaidToThirdParty);
            SetResult("sQMApprFProps_Aff_chk", dataLoan.sQMApprFProps_ThisPartyIsAffiliate);
            SetResult("sQMApprF_QMAmount", dataLoan.sQMApprF_QMAmount_rep);

            SetResult("sQMCrFProps_PdByT_dd", dataLoan.sQMCrFProps_Payer);
            SetResult("sQMCrFProps_Apr_chk", dataLoan.sQMCrFProps_Apr);
            SetResult("sQMCrFProps_TrdPty_chk", dataLoan.sQMCrFProps_PaidToThirdParty);
            SetResult("sQMCrFProps_Aff_chk", dataLoan.sQMCrFProps_ThisPartyIsAffiliate);
            SetResult("sQMCrF_QMAmount", dataLoan.sQMCrF_QMAmount_rep);

            SetResult("sQMTxServFProps_PdByT_dd", dataLoan.sQMTxServFProps_Payer);
            SetResult("sQMTxServFProps_Apr_chk", dataLoan.sQMTxServFProps_Apr);
            SetResult("sQMTxServFProps_TrdPty_chk", dataLoan.sQMTxServFProps_PaidToThirdParty);
            SetResult("sQMTxServFProps_Aff_chk", dataLoan.sQMTxServFProps_ThisPartyIsAffiliate);
            SetResult("sQMTxServF_QMAmount", dataLoan.sQMTxServF_QMAmount_rep);

            SetResult("sQMFloodCertificationFProps_PdByT_dd", dataLoan.sQMFloodCertificationFProps_Payer);
            SetResult("sQMFloodCertificationFProps_Apr_chk", dataLoan.sQMFloodCertificationFProps_Apr);
            SetResult("sQMFloodCertificationFProps_TrdPty_chk", dataLoan.sQMFloodCertificationFProps_PaidToThirdParty);
            SetResult("sQMFloodCertificationFProps_Aff_chk", dataLoan.sQMFloodCertificationFProps_ThisPartyIsAffiliate);
            SetResult("sQMFloodCertificationF_QMAmount", dataLoan.sQMFloodCertificationF_QMAmount_rep);

            SetResult("sQMMBrokFProps_PdByT_dd", dataLoan.sQMMBrokFProps_Payer);
            SetResult("sQMMBrokFProps_Apr_chk", dataLoan.sQMMBrokFProps_Apr);
            SetResult("sQMMBrokFProps_TrdPty_chk", dataLoan.sQMMBrokFProps_PaidToThirdParty);
            SetResult("sQMMBrokFProps_Aff_chk", dataLoan.sQMMBrokFProps_ThisPartyIsAffiliate);
            SetResult("sQMMBrokF_QMAmount", dataLoan.sQMMBrokF_QMAmount_rep);

            SetResult("sQMInspectFProps_PdByT_dd", dataLoan.sQMInspectFProps_Payer);
            SetResult("sQMInspectFProps_Apr_chk", dataLoan.sQMInspectFProps_Apr);
            SetResult("sQMInspectFProps_TrdPty_chk", dataLoan.sQMInspectFProps_PaidToThirdParty);
            SetResult("sQMInspectFProps_Aff_chk", dataLoan.sQMInspectFProps_ThisPartyIsAffiliate);
            SetResult("sQMInspectF_QMAmount", dataLoan.sQMInspectF_QMAmount_rep);

            SetResult("sQMProcFProps_PdByT_dd", dataLoan.sQMProcFProps_Payer);
            SetResult("sQMProcFProps_Apr_chk", dataLoan.sQMProcFProps_Apr);
            SetResult("sQMProcFProps_TrdPty_chk", dataLoan.sQMProcFProps_PaidToThirdParty);
            SetResult("sQMProcFProps_Aff_chk", dataLoan.sQMProcFProps_ThisPartyIsAffiliate);
            SetResult("sQMProcF_QMAmount", dataLoan.sQMProcF_QMAmount_rep);

            SetResult("sQMUwFProps_PdByT_dd", dataLoan.sQMUwFProps_Payer);
            SetResult("sQMUwFProps_Apr_chk", dataLoan.sQMUwFProps_Apr);
            SetResult("sQMUwFProps_TrdPty_chk", dataLoan.sQMUwFProps_PaidToThirdParty);
            SetResult("sQMUwFProps_Aff_chk", dataLoan.sQMUwFProps_ThisPartyIsAffiliate);
            SetResult("sQMUwF_QMAmount", dataLoan.sQMUwF_QMAmount_rep);

            SetResult("sQMWireFProps_PdByT_dd", dataLoan.sQMWireFProps_Payer);
            SetResult("sQMWireFProps_Apr_chk", dataLoan.sQMWireFProps_Apr);
            SetResult("sQMWireFProps_TrdPty_chk", dataLoan.sQMWireFProps_PaidToThirdParty);
            SetResult("sQMWireFProps_Aff_chk", dataLoan.sQMWireFProps_ThisPartyIsAffiliate);
            SetResult("sQMWireF_QMAmount", dataLoan.sQMWireF_QMAmount_rep);

            SetResult("sQM800U1FProps_PdByT_dd", dataLoan.sQM800U1FProps_Payer);
            SetResult("sQM800U1FProps_Apr_chk", dataLoan.sQM800U1FProps_Apr);
            SetResult("sQM800U1FProps_TrdPty_chk", dataLoan.sQM800U1FProps_PaidToThirdParty);
            SetResult("sQM800U1FProps_Aff_chk", dataLoan.sQM800U1FProps_ThisPartyIsAffiliate);
            SetResult("sQM800U1F_QMAmount", dataLoan.sQM800U1F_QMAmount_rep);

            SetResult("sQM800U2FProps_PdByT_dd", dataLoan.sQM800U2FProps_Payer);
            SetResult("sQM800U2FProps_Apr_chk", dataLoan.sQM800U2FProps_Apr);
            SetResult("sQM800U2FProps_TrdPty_chk", dataLoan.sQM800U2FProps_PaidToThirdParty);
            SetResult("sQM800U2FProps_Aff_chk", dataLoan.sQM800U2FProps_ThisPartyIsAffiliate);
            SetResult("sQM800U2F_QMAmount", dataLoan.sQM800U2F_QMAmount_rep);

            SetResult("sQM800U3FProps_PdByT_dd", dataLoan.sQM800U3FProps_Payer);
            SetResult("sQM800U3FProps_Apr_chk", dataLoan.sQM800U3FProps_Apr);
            SetResult("sQM800U3FProps_TrdPty_chk", dataLoan.sQM800U3FProps_PaidToThirdParty);
            SetResult("sQM800U3FProps_Aff_chk", dataLoan.sQM800U3FProps_ThisPartyIsAffiliate);
            SetResult("sQM800U3F_QMAmount", dataLoan.sQM800U3F_QMAmount_rep);

            SetResult("sQM800U4FProps_PdByT_dd", dataLoan.sQM800U4FProps_Payer);
            SetResult("sQM800U4FProps_Apr_chk", dataLoan.sQM800U4FProps_Apr);
            SetResult("sQM800U4FProps_TrdPty_chk", dataLoan.sQM800U4FProps_PaidToThirdParty);
            SetResult("sQM800U4FProps_Aff_chk", dataLoan.sQM800U4FProps_ThisPartyIsAffiliate);
            SetResult("sQM800U4F_QMAmount", dataLoan.sQM800U4F_QMAmount_rep);

            SetResult("sQM800U5FProps_PdByT_dd", dataLoan.sQM800U5FProps_Payer);
            SetResult("sQM800U5FProps_Apr_chk", dataLoan.sQM800U5FProps_Apr);
            SetResult("sQM800U5FProps_TrdPty_chk", dataLoan.sQM800U5FProps_PaidToThirdParty);
            SetResult("sQM800U5FProps_Aff_chk", dataLoan.sQM800U5FProps_ThisPartyIsAffiliate);
            SetResult("sQM800U5F_QMAmount", dataLoan.sQM800U5F_QMAmount_rep);

            SetResult("sQMIPiaProps_PdByT_dd", dataLoan.sQMIPiaProps_Payer);
            SetResult("sQMIPiaProps_Apr_chk", dataLoan.sQMIPiaProps_Apr);
            SetResult("sQMIPiaProps_TrdPty_chk", dataLoan.sQMIPiaProps_PaidToThirdParty);
            SetResult("sQMIPiaProps_Aff_chk", dataLoan.sQMIPiaProps_ThisPartyIsAffiliate);
            SetResult("sQMIPia_QMAmount", dataLoan.sQMIPia_QMAmount_rep);

            SetResult("sQMMipPiaProps_PdByT_dd", dataLoan.sQMMipPiaProps_Payer);
            SetResult("sQMMipPiaProps_Apr_chk", dataLoan.sQMMipPiaProps_Apr);
            //SetResult("sQMMipPiaProps_TrdPty_chk", dataLoan.sQMMipPiaProps_PaidToThirdParty);
            //SetResult("sQMMipPiaProps_Aff_chk", dataLoan.sQMMipPiaProps_ThisPartyIsAffiliate);
            SetResult("sQMMipPia_QMAmount", dataLoan.sQMMipPia_QMAmount_rep);

            SetResult("sQMHazInsPiaProps_PdByT_dd", dataLoan.sQMHazInsPiaProps_Payer);
            SetResult("sQMHazInsPiaProps_Apr_chk", dataLoan.sQMHazInsPiaProps_Apr);
            SetResult("sQMHazInsPiaProps_TrdPty_chk", dataLoan.sQMHazInsPiaProps_PaidToThirdParty);
            SetResult("sQMHazInsPiaProps_Aff_chk", dataLoan.sQMHazInsPiaProps_ThisPartyIsAffiliate);
            SetResult("sQMHazInsPia_QMAmount", dataLoan.sQMHazInsPia_QMAmount_rep);

            SetResult("sQM904PiaProps_PdByT_dd", dataLoan.sQM904PiaProps_Payer);
            SetResult("sQM904PiaProps_Apr_chk", dataLoan.sQM904PiaProps_Apr);
            SetResult("sQM904PiaProps_TrdPty_chk", dataLoan.sQM904PiaProps_PaidToThirdParty);
            SetResult("sQM904PiaProps_Aff_chk", dataLoan.sQM904PiaProps_ThisPartyIsAffiliate);
            SetResult("sQM904Pia_QMAmount", dataLoan.sQM904Pia_QMAmount_rep);

            SetResult("sQMVaFfProps_PdByT_dd", dataLoan.sQMVaFfProps_Payer);
            SetResult("sQMVaFfProps_Apr_chk", dataLoan.sQMVaFfProps_Apr);
            SetResult("sQMVaFfProps_TrdPty_chk", dataLoan.sQMVaFfProps_PaidToThirdParty);
            SetResult("sQMVaFfProps_Aff_chk", dataLoan.sQMVaFfProps_ThisPartyIsAffiliate);
            SetResult("sQMVaFf_QMAmount", dataLoan.sQMVaFf_QMAmount_rep);

            SetResult("sQM900U1PiaProps_PdByT_dd", dataLoan.sQM900U1PiaProps_Payer);
            SetResult("sQM900U1PiaProps_Apr_chk", dataLoan.sQM900U1PiaProps_Apr);
            SetResult("sQM900U1PiaProps_TrdPty_chk", dataLoan.sQM900U1PiaProps_PaidToThirdParty);
            SetResult("sQM900U1PiaProps_Aff_chk", dataLoan.sQM900U1PiaProps_ThisPartyIsAffiliate);
            SetResult("sQM900U1Pia_QMAmount", dataLoan.sQM900U1Pia_QMAmount_rep);

            SetResult("sQMHazInsRsrvProps_PdByT_dd", dataLoan.sQMHazInsRsrvProps_Payer);
            SetResult("sQMHazInsRsrvProps_Apr_chk", dataLoan.sQMHazInsRsrvProps_Apr);
            SetResult("sQMHazInsRsrvProps_TrdPty_chk", dataLoan.sQMHazInsRsrvProps_PaidToThirdParty);
            SetResult("sQMHazInsRsrvProps_Aff_chk", dataLoan.sQMHazInsRsrvProps_ThisPartyIsAffiliate);
            SetResult("sQMHazInsRsrv_QMAmount", dataLoan.sQMHazInsRsrv_QMAmount_rep);

            SetResult("sQMMInsRsrvProps_PdByT_dd", dataLoan.sQMMInsRsrvProps_Payer);
            SetResult("sQMMInsRsrvProps_Apr_chk", dataLoan.sQMMInsRsrvProps_Apr);
            SetResult("sQMMInsRsrvProps_TrdPty_chk", dataLoan.sQMMInsRsrvProps_PaidToThirdParty);
            SetResult("sQMMInsRsrvProps_Aff_chk", dataLoan.sQMMInsRsrvProps_ThisPartyIsAffiliate);
            SetResult("sQMMInsRsrv_QMAmount", dataLoan.sQMMInsRsrv_QMAmount_rep);

            SetResult("sQMRealETxRsrvProps_PdByT_dd", dataLoan.sQMRealETxRsrvProps_Payer);
            SetResult("sQMRealETxRsrvProps_Apr_chk", dataLoan.sQMRealETxRsrvProps_Apr);
            SetResult("sQMRealETxRsrvProps_TrdPty_chk", dataLoan.sQMRealETxRsrvProps_PaidToThirdParty);
            SetResult("sQMRealETxRsrvProps_Aff_chk", dataLoan.sQMRealETxRsrvProps_ThisPartyIsAffiliate);
            SetResult("sQMRealETxRsrv_QMAmount", dataLoan.sQMRealETxRsrv_QMAmount_rep);

            SetResult("sQMSchoolTxRsrvProps_PdByT_dd", dataLoan.sQMSchoolTxRsrvProps_Payer);
            SetResult("sQMSchoolTxRsrvProps_Apr_chk", dataLoan.sQMSchoolTxRsrvProps_Apr);
            SetResult("sQMSchoolTxRsrvProps_TrdPty_chk", dataLoan.sQMSchoolTxRsrvProps_PaidToThirdParty);
            SetResult("sQMSchoolTxRsrvProps_Aff_chk", dataLoan.sQMSchoolTxRsrvProps_ThisPartyIsAffiliate);
            SetResult("sQMSchoolTxRsrv_QMAmount", dataLoan.sQMSchoolTxRsrv_QMAmount_rep);

            SetResult("sQMFloodInsRsrvProps_PdByT_dd", dataLoan.sQMFloodInsRsrvProps_Payer);
            SetResult("sQMFloodInsRsrvProps_Apr_chk", dataLoan.sQMFloodInsRsrvProps_Apr);
            SetResult("sQMFloodInsRsrvProps_TrdPty_chk", dataLoan.sQMFloodInsRsrvProps_PaidToThirdParty);
            SetResult("sQMFloodInsRsrvProps_Aff_chk", dataLoan.sQMFloodInsRsrvProps_ThisPartyIsAffiliate);
            SetResult("sQMFloodInsRsrv_QMAmount", dataLoan.sQMFloodInsRsrv_QMAmount_rep);

            SetResult("sQMAggregateAdjRsrvProps_PdByT_dd", dataLoan.sQMAggregateAdjRsrvProps_Payer);
            SetResult("sQMAggregateAdjRsrvProps_Apr_chk", dataLoan.sQMAggregateAdjRsrvProps_Apr);
            SetResult("sQMAggregateAdjRsrvProps_TrdPty_chk", dataLoan.sQMAggregateAdjRsrvProps_PaidToThirdParty);
            SetResult("sQMAggregateAdjRsrvProps_Aff_chk", dataLoan.sQMAggregateAdjRsrvProps_ThisPartyIsAffiliate);
            SetResult("sQMAggregateAdjRsrv_QMAmount", dataLoan.sQMAggregateAdjRsrv_QMAmount_rep);

            SetResult("sQM1006RsrvProps_PdByT_dd", dataLoan.sQM1006RsrvProps_Payer);
            SetResult("sQM1006RsrvProps_Apr_chk", dataLoan.sQM1006RsrvProps_Apr);
            SetResult("sQM1006RsrvProps_TrdPty_chk", dataLoan.sQM1006RsrvProps_PaidToThirdParty);
            SetResult("sQM1006RsrvProps_Aff_chk", dataLoan.sQM1006RsrvProps_ThisPartyIsAffiliate);
            SetResult("sQM1006Rsrv_QMAmount", dataLoan.sQM1006Rsrv_QMAmount_rep);

            SetResult("sQM1007RsrvProps_PdByT_dd", dataLoan.sQM1007RsrvProps_Payer);
            SetResult("sQM1007RsrvProps_Apr_chk", dataLoan.sQM1007RsrvProps_Apr);
            SetResult("sQM1007RsrvProps_TrdPty_chk", dataLoan.sQM1007RsrvProps_PaidToThirdParty);
            SetResult("sQM1007RsrvProps_Aff_chk", dataLoan.sQM1007RsrvProps_ThisPartyIsAffiliate);
            SetResult("sQM1007Rsrv_QMAmount", dataLoan.sQM1007Rsrv_QMAmount_rep);

            SetResult("sQMU3RsrvProps_PdByT_dd", dataLoan.sQMU3RsrvProps_Payer);
            SetResult("sQMU3RsrvProps_Apr_chk", dataLoan.sQMU3RsrvProps_Apr);
            SetResult("sQMU3RsrvProps_TrdPty_chk", dataLoan.sQMU3RsrvProps_PaidToThirdParty);
            SetResult("sQMU3RsrvProps_Aff_chk", dataLoan.sQMU3RsrvProps_ThisPartyIsAffiliate);
            SetResult("sQMU3Rsrv_QMAmount", dataLoan.sQMU3Rsrv_QMAmount_rep);

            SetResult("sQMU4RsrvProps_PdByT_dd", dataLoan.sQMU4RsrvProps_Payer);
            SetResult("sQMU4RsrvProps_Apr_chk", dataLoan.sQMU4RsrvProps_Apr);
            SetResult("sQMU4RsrvProps_TrdPty_chk", dataLoan.sQMU4RsrvProps_PaidToThirdParty);
            SetResult("sQMU4RsrvProps_Aff_chk", dataLoan.sQMU4RsrvProps_ThisPartyIsAffiliate);
            SetResult("sQMU4Rsrv_QMAmount", dataLoan.sQMU4Rsrv_QMAmount_rep);

            SetResult("sQMEscrowFProps_PdByT_dd", dataLoan.sQMEscrowFProps_Payer);
            SetResult("sQMEscrowFProps_Apr_chk", dataLoan.sQMEscrowFProps_Apr);
            SetResult("sQMEscrowFProps_TrdPty_chk", dataLoan.sQMEscrowFProps_PaidToThirdParty);
            SetResult("sQMEscrowFProps_Aff_chk", dataLoan.sQMEscrowFProps_ThisPartyIsAffiliate);
            SetResult("sQMEscrowF_QMAmount", dataLoan.sQMEscrowF_QMAmount_rep);

            SetResult("sQMOwnerTitleInsFProps_PdByT_dd", dataLoan.sQMOwnerTitleInsFProps_Payer);
            SetResult("sQMOwnerTitleInsFProps_Apr_chk", dataLoan.sQMOwnerTitleInsFProps_Apr);
            SetResult("sQMOwnerTitleInsFProps_TrdPty_chk", dataLoan.sQMOwnerTitleInsFProps_PaidToThirdParty);
            SetResult("sQMOwnerTitleInsFProps_Aff_chk", dataLoan.sQMOwnerTitleInsFProps_ThisPartyIsAffiliate);
            SetResult("sQMOwnerTitleInsF_QMAmount", dataLoan.sQMOwnerTitleInsF_QMAmount_rep);

            SetResult("sQMTitleInsFProps_PdByT_dd", dataLoan.sQMTitleInsFProps_Payer);
            SetResult("sQMTitleInsFProps_Apr_chk", dataLoan.sQMTitleInsFProps_Apr);
            SetResult("sQMTitleInsFProps_TrdPty_chk", dataLoan.sQMTitleInsFProps_PaidToThirdParty);
            SetResult("sQMTitleInsFProps_Aff_chk", dataLoan.sQMTitleInsFProps_ThisPartyIsAffiliate);
            SetResult("sQMTitleInsF_QMAmount", dataLoan.sQMTitleInsF_QMAmount_rep);

            SetResult("sQMDocPrepFProps_PdByT_dd", dataLoan.sQMDocPrepFProps_Payer);
            SetResult("sQMDocPrepFProps_Apr_chk", dataLoan.sQMDocPrepFProps_Apr);
            SetResult("sQMDocPrepFProps_TrdPty_chk", dataLoan.sQMDocPrepFProps_PaidToThirdParty);
            SetResult("sQMDocPrepFProps_Aff_chk", dataLoan.sQMDocPrepFProps_ThisPartyIsAffiliate);
            SetResult("sQMDocPrepF_QMAmount", dataLoan.sQMDocPrepF_QMAmount_rep);

            SetResult("sQMNotaryFProps_PdByT_dd", dataLoan.sQMNotaryFProps_Payer);
            SetResult("sQMNotaryFProps_Apr_chk", dataLoan.sQMNotaryFProps_Apr);
            SetResult("sQMNotaryFProps_TrdPty_chk", dataLoan.sQMNotaryFProps_PaidToThirdParty);
            SetResult("sQMNotaryFProps_Aff_chk", dataLoan.sQMNotaryFProps_ThisPartyIsAffiliate);
            SetResult("sQMNotaryF_QMAmount", dataLoan.sQMNotaryF_QMAmount_rep);

            SetResult("sQMAttorneyFProps_PdByT_dd", dataLoan.sQMAttorneyFProps_Payer);
            SetResult("sQMAttorneyFProps_Apr_chk", dataLoan.sQMAttorneyFProps_Apr);
            SetResult("sQMAttorneyFProps_TrdPty_chk", dataLoan.sQMAttorneyFProps_PaidToThirdParty);
            SetResult("sQMAttorneyFProps_Aff_chk", dataLoan.sQMAttorneyFProps_ThisPartyIsAffiliate);
            SetResult("sQMAttorneyF_QMAmount", dataLoan.sQMAttorneyF_QMAmount_rep);

            SetResult("sQMU1TcProps_PdByT_dd", dataLoan.sQMU1TcProps_Payer);
            SetResult("sQMU1TcProps_Apr_chk", dataLoan.sQMU1TcProps_Apr);
            SetResult("sQMU1TcProps_TrdPty_chk", dataLoan.sQMU1TcProps_PaidToThirdParty);
            SetResult("sQMU1TcProps_Aff_chk", dataLoan.sQMU1TcProps_ThisPartyIsAffiliate);
            SetResult("sQMU1Tc_QMAmount", dataLoan.sQMU1Tc_QMAmount_rep);

            SetResult("sQMU2TcProps_PdByT_dd", dataLoan.sQMU2TcProps_Payer);
            SetResult("sQMU2TcProps_Apr_chk", dataLoan.sQMU2TcProps_Apr);
            SetResult("sQMU2TcProps_TrdPty_chk", dataLoan.sQMU2TcProps_PaidToThirdParty);
            SetResult("sQMU2TcProps_Aff_chk", dataLoan.sQMU2TcProps_ThisPartyIsAffiliate);
            SetResult("sQMU2Tc_QMAmount", dataLoan.sQMU2Tc_QMAmount_rep);

            SetResult("sQMU3TcProps_PdByT_dd", dataLoan.sQMU3TcProps_Payer);
            SetResult("sQMU3TcProps_Apr_chk", dataLoan.sQMU3TcProps_Apr);
            SetResult("sQMU3TcProps_TrdPty_chk", dataLoan.sQMU3TcProps_PaidToThirdParty);
            SetResult("sQMU3TcProps_Aff_chk", dataLoan.sQMU3TcProps_ThisPartyIsAffiliate);
            SetResult("sQMU3Tc_QMAmount", dataLoan.sQMU3Tc_QMAmount_rep);

            SetResult("sQMU4TcProps_PdByT_dd", dataLoan.sQMU4TcProps_Payer);
            SetResult("sQMU4TcProps_Apr_chk", dataLoan.sQMU4TcProps_Apr);
            SetResult("sQMU4TcProps_TrdPty_chk", dataLoan.sQMU4TcProps_PaidToThirdParty);
            SetResult("sQMU4TcProps_Aff_chk", dataLoan.sQMU4TcProps_ThisPartyIsAffiliate);
            SetResult("sQMU4Tc_QMAmount", dataLoan.sQMU4Tc_QMAmount_rep);

            SetResult("sQMRecFProps_PdByT_dd", dataLoan.sQMRecFProps_Payer);
            SetResult("sQMRecFProps_Apr_chk", dataLoan.sQMRecFProps_Apr);
            SetResult("sQMRecFProps_TrdPty_chk", dataLoan.sQMRecFProps_PaidToThirdParty);
            SetResult("sQMRecFProps_Aff_chk", dataLoan.sQMRecFProps_ThisPartyIsAffiliate);
            SetResult("sQMRecF_QMAmount", dataLoan.sQMRecF_QMAmount_rep);

            SetResult("sQMCountyRtcProps_PdByT_dd", dataLoan.sQMCountyRtcProps_Payer);
            SetResult("sQMCountyRtcProps_Apr_chk", dataLoan.sQMCountyRtcProps_Apr);
            SetResult("sQMCountyRtcProps_TrdPty_chk", dataLoan.sQMCountyRtcProps_PaidToThirdParty);
            SetResult("sQMCountyRtcProps_Aff_chk", dataLoan.sQMCountyRtcProps_ThisPartyIsAffiliate);
            SetResult("sQMCountyRtc_QMAmount", dataLoan.sQMCountyRtc_QMAmount_rep);

            SetResult("sQMStateRtcProps_PdByT_dd", dataLoan.sQMStateRtcProps_Payer);
            SetResult("sQMStateRtcProps_Apr_chk", dataLoan.sQMStateRtcProps_Apr);
            SetResult("sQMStateRtcProps_TrdPty_chk", dataLoan.sQMStateRtcProps_PaidToThirdParty);
            SetResult("sQMStateRtcProps_Aff_chk", dataLoan.sQMStateRtcProps_ThisPartyIsAffiliate);
            SetResult("sQMStateRtc_QMAmount", dataLoan.sQMStateRtc_QMAmount_rep);

            SetResult("sQMU1GovRtcProps_PdByT_dd", dataLoan.sQMU1GovRtcProps_Payer);
            SetResult("sQMU1GovRtcProps_Apr_chk", dataLoan.sQMU1GovRtcProps_Apr);
            SetResult("sQMU1GovRtcProps_TrdPty_chk", dataLoan.sQMU1GovRtcProps_PaidToThirdParty);
            SetResult("sQMU1GovRtcProps_Aff_chk", dataLoan.sQMU1GovRtcProps_ThisPartyIsAffiliate);
            SetResult("sQMU1GovRtc_QMAmount", dataLoan.sQMU1GovRtc_QMAmount_rep);

            SetResult("sQMU2GovRtcProps_PdByT_dd", dataLoan.sQMU2GovRtcProps_Payer);
            SetResult("sQMU2GovRtcProps_Apr_chk", dataLoan.sQMU2GovRtcProps_Apr);
            SetResult("sQMU2GovRtcProps_TrdPty_chk", dataLoan.sQMU2GovRtcProps_PaidToThirdParty);
            SetResult("sQMU2GovRtcProps_Aff_chk", dataLoan.sQMU2GovRtcProps_ThisPartyIsAffiliate);
            SetResult("sQMU2GovRtc_QMAmount", dataLoan.sQMU2GovRtc_QMAmount_rep);

            SetResult("sQMU3GovRtcProps_PdByT_dd", dataLoan.sQMU3GovRtcProps_Payer);
            SetResult("sQMU3GovRtcProps_Apr_chk", dataLoan.sQMU3GovRtcProps_Apr);
            SetResult("sQMU3GovRtcProps_TrdPty_chk", dataLoan.sQMU3GovRtcProps_PaidToThirdParty);
            SetResult("sQMU3GovRtcProps_Aff_chk", dataLoan.sQMU3GovRtcProps_ThisPartyIsAffiliate);
            SetResult("sQMU3GovRtc_QMAmount", dataLoan.sQMU3GovRtc_QMAmount_rep);

            SetResult("sQMPestInspectFProps_PdByT_dd", dataLoan.sQMPestInspectFProps_Payer);
            SetResult("sQMPestInspectFProps_Apr_chk", dataLoan.sQMPestInspectFProps_Apr);
            SetResult("sQMPestInspectFProps_TrdPty_chk", dataLoan.sQMPestInspectFProps_PaidToThirdParty);
            SetResult("sQMPestInspectFProps_Aff_chk", dataLoan.sQMPestInspectFProps_ThisPartyIsAffiliate);
            SetResult("sQMPestInspectF_QMAmount", dataLoan.sQMPestInspectF_QMAmount_rep);

            SetResult("sQMU1ScProps_PdByT_dd", dataLoan.sQMU1ScProps_Payer);
            SetResult("sQMU1ScProps_Apr_chk", dataLoan.sQMU1ScProps_Apr);
            SetResult("sQMU1ScProps_TrdPty_chk", dataLoan.sQMU1ScProps_PaidToThirdParty);
            SetResult("sQMU1ScProps_Aff_chk", dataLoan.sQMU1ScProps_ThisPartyIsAffiliate);
            SetResult("sQMU1Sc_QMAmount", dataLoan.sQMU1Sc_QMAmount_rep);

            SetResult("sQMU2ScProps_PdByT_dd", dataLoan.sQMU2ScProps_Payer);
            SetResult("sQMU2ScProps_Apr_chk", dataLoan.sQMU2ScProps_Apr);
            SetResult("sQMU2ScProps_TrdPty_chk", dataLoan.sQMU2ScProps_PaidToThirdParty);
            SetResult("sQMU2ScProps_Aff_chk", dataLoan.sQMU2ScProps_ThisPartyIsAffiliate);
            SetResult("sQMU2Sc_QMAmount", dataLoan.sQMU2Sc_QMAmount_rep);

            SetResult("sQMU3ScProps_PdByT_dd", dataLoan.sQMU3ScProps_Payer);
            SetResult("sQMU3ScProps_Apr_chk", dataLoan.sQMU3ScProps_Apr);
            SetResult("sQMU3ScProps_TrdPty_chk", dataLoan.sQMU3ScProps_PaidToThirdParty);
            SetResult("sQMU3ScProps_Aff_chk", dataLoan.sQMU3ScProps_ThisPartyIsAffiliate);
            SetResult("sQMU3Sc_QMAmount", dataLoan.sQMU3Sc_QMAmount_rep);

            SetResult("sQMU4ScProps_PdByT_dd", dataLoan.sQMU4ScProps_Payer);
            SetResult("sQMU4ScProps_Apr_chk", dataLoan.sQMU4ScProps_Apr);
            SetResult("sQMU4ScProps_TrdPty_chk", dataLoan.sQMU4ScProps_PaidToThirdParty);
            SetResult("sQMU4ScProps_Aff_chk", dataLoan.sQMU4ScProps_ThisPartyIsAffiliate);
            SetResult("sQMU4Sc_QMAmount", dataLoan.sQMU4Sc_QMAmount_rep);

            SetResult("sQMU5ScProps_PdByT_dd", dataLoan.sQMU5ScProps_Payer);
            SetResult("sQMU5ScProps_Apr_chk", dataLoan.sQMU5ScProps_Apr);
            SetResult("sQMU5ScProps_TrdPty_chk", dataLoan.sQMU5ScProps_PaidToThirdParty);
            SetResult("sQMU5ScProps_Aff_chk", dataLoan.sQMU5ScProps_ThisPartyIsAffiliate);
            SetResult("sQMU5Sc_QMAmount", dataLoan.sQMU5Sc_QMAmount_rep);
        }
    }

	public sealed class QMCalculationsServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(QMCalculationsServiceItem));
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sRLckdD", dataLoan.sRLckdD);
            SetResult("sInterestRateSetDLckd", dataLoan.sInterestRateSetDLckd);
            SetResult("sInterestRateSetD", dataLoan.sInterestRateSetD_rep);
            SetResult("sDocMagicClosingD", dataLoan.sDocMagicClosingD_rep);
            SetResult("sQMAveragePrimeOfferR", dataLoan.sQMAveragePrimeOfferR_rep);
            SetResult("sQMAveragePrimeOfferRLck", dataLoan.sQMAveragePrimeOfferRLck);
            SetResult("sQMParR", dataLoan.sQMParR_rep);
            SetResult("sQMTotFeeAmount", dataLoan.sQMTotFeeAmount_rep);
            SetResult("sQMTotFinFeeAmount", dataLoan.sQMTotFinFeeAmount_rep);
            SetResult("sQMExclDiscntPnt", dataLoan.sQMExclDiscntPnt_rep);
            SetResult("sQMExclDiscntPntLck", dataLoan.sQMExclDiscntPntLck);
            SetResult("sQMFhaUfmipAtClosingLck", dataLoan.sQMFhaUfmipAtClosingLck);
            SetResult("sQMFhaUfmipAtClosing", dataLoan.sQMFhaUfmipAtClosing_rep);
            SetResult("sQMNonExcludableDiscountPointsPc", dataLoan.sQMNonExcludableDiscountPointsPc_rep);

            SetResult("sQMMaxPrePmntPenalty", dataLoan.sQMMaxPrePmntPenalty_rep);
            SetResult("sQMQualR", dataLoan.sQMQualR_rep);
            SetResult("sQMQualBottom", dataLoan.sQMQualBottom_rep);
            SetResult("sQMAprRSpread", dataLoan.sQMAprRSpread_rep);
            SetResult("sQMParRSpread", dataLoan.sQMParRSpread_rep);

            SetResult("sQMParRSpreadCalcDesc", dataLoan.sQMParRSpreadCalcDesc);
            SetResult("sQMAprRSpreadCalcDesc", dataLoan.sQMAprRSpreadCalcDesc);
            //SetResult("sQMExcessUpfrontMIP", dataLoan.sQMExcessUpfrontMIP_rep);
            SetResult("sQMExcessUpfrontMIP_1", dataLoan.sQMExcessUpfrontMIP_rep);
            SetResult("sQMExcessUpfrontMIPCalcDesc", dataLoan.sQMExcessUpfrontMIPCalcDesc);
            SetResult("sQMExcessDiscntFCalcDesc", dataLoan.sQMExcessDiscntFCalcDesc);

            //SetResult("sQMTotFeeAmount", dataLoan.sQMTotFeeAmount_rep);
            SetResult("sQMTotFeeAmount_1", dataLoan.sQMTotFeeAmount_rep);
            SetResult("sQMExcessDiscntF", dataLoan.sQMExcessDiscntF_rep);
            SetResult("sQMExcessUpfrontMIP", dataLoan.sQMExcessUpfrontMIP_rep);
            //SetResult("sQMMaxPrePmntPenalty", dataLoan.sQMMaxPrePmntPenalty_rep);
            SetResult("sQMMaxPrePmntPenalty_1", dataLoan.sQMMaxPrePmntPenalty_rep);
            SetResult("sQMDiscntBuyDownR", dataLoan.sQMDiscntBuyDownR_rep);
            SetResult("sQMDiscntBuyDownRCalcDesc", dataLoan.sQMDiscntBuyDownRCalcDesc);
            SetResult("sQMLAmt", dataLoan.sQMLAmt_rep);
            SetResult("sQMLAmt_1", dataLoan.sQMLAmt_rep);
            SetResult("sQMLAmtCalcDesc", dataLoan.sQMLAmtCalcDesc);
            SetResult("sQMTotFeeAmt", dataLoan.sQMTotFeeAmt_rep);
            SetResult("sQMTotFeePc", dataLoan.sQMTotFeePc_rep);
            SetResult("sQMExcessDiscntFPc", dataLoan.sQMExcessDiscntFPc_rep);
            SetResult("sQMMaxPointAndFeesAllowedAmt", dataLoan.sQMMaxPointAndFeesAllowedAmt_rep);
            SetResult("sQMMaxPointAndFeesAllowedPc", dataLoan.sQMMaxPointAndFeesAllowedPc_rep);
            SetResult("sQMLoanPassesPointAndFeesTest", dataLoan.sQMLoanPassesPointAndFeesTest);
            SetResult("sQMLoanDoesNotHaveNegativeAmort", dataLoan.sQMLoanDoesNotHaveNegativeAmort);
            SetResult("sQMLoanDoesNotHaveAmortTermOver30Yr", dataLoan.sQMLoanDoesNotHaveAmortTermOver30Yr);
            SetResult("sQMLoanDoesNotHaveBalloonFeature", dataLoan.sQMLoanDoesNotHaveBalloonFeature);
            SetResult("sQMLoanHasDtiLessThan43Pc", dataLoan.sQMLoanHasDtiLessThan43Pc);
            SetResult("sQMIsVerifiedIncomeAndAssets", dataLoan.sQMIsVerifiedIncomeAndAssets);
            SetResult("sQMIsEligibleByLoanPurchaseAgency", dataLoan.sQMIsEligibleByLoanPurchaseAgency);
            SetResult("sQMLoanPassedComplianceChecks", dataLoan.sQMLoanPassedComplianceChecks);
            SetResult("sQMStatusT", dataLoan.sQMStatusT);
            SetResult("sQMStatusTCombo", CPageBase.sQMStatusT_map_rep(dataLoan.sQMStatusT));
            SetResult("sQMLoanHasComplianceChecksOnFile", dataLoan.sQMLoanHasComplianceChecksOnFile);
            SetResult("sQMLoanPurchaseAgency", dataLoan.sQMLoanPurchaseAgency);
            //SetResult("sQMLoanPassesPointAndFeesTest", dataLoan.sQMLoanPassesPointAndFeesTest);
            SetResult("sQMLoanPurchaseAgencyIsDisabled", dataLoan.sQMLoanPurchaseAgencyIsDisabled);

            SetResult("sHighPricedMortgageT", dataLoan.sHighPricedMortgageT);
            SetResult("sHighPricedMortgageTLckd", dataLoan.sHighPricedMortgageTLckd);

            SetResult("sIsExemptFromAtr", dataLoan.sIsExemptFromAtr);
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sInterestRateSetDLckd = GetBool("sInterestRateSetDLckd");
            dataLoan.sInterestRateSetD_rep = GetString("sInterestRateSetD");
            dataLoan.sQMAveragePrimeOfferRLck = GetBool("sQMAveragePrimeOfferRLck");
            dataLoan.sQMAveragePrimeOfferR_rep = GetString("sQMAveragePrimeOfferR");
            dataLoan.sQMParR_rep = GetString("sQMParR");
            dataLoan.sQMExclDiscntPnt_rep = GetString("sQMExclDiscntPnt");
            dataLoan.sQMExclDiscntPntLck = GetBool("sQMExclDiscntPntLck");
            dataLoan.sQMIsVerifiedIncomeAndAssets = GetBool("sQMIsVerifiedIncomeAndAssets");
            dataLoan.sQMLoanPurchaseAgency = GetEnum<E_sQMLoanPurchaseAgency>("sQMLoanPurchaseAgency");
            dataLoan.sQMIsEligibleByLoanPurchaseAgency = GetBool("sQMIsEligibleByLoanPurchaseAgency");
            dataLoan.sQMNonExcludableDiscountPointsPc_rep = GetString("sQMNonExcludableDiscountPointsPc");

            dataLoan.sHighPricedMortgageTLckd = GetBool("sHighPricedMortgageTLckd");
            dataLoan.sHighPricedMortgageT = GetEnum<E_HighPricedMortgageT>("sHighPricedMortgageT");

            dataLoan.sIsExemptFromAtr = GetBool("sIsExemptFromAtr");
            
            dataLoan.sQMFhaUfmipAtClosing_rep = GetString("sQMFhaUfmipAtClosing");
            dataLoan.sQMFhaUfmipAtClosingLck = GetBool("sQMFhaUfmipAtClosingLck");
            dataLoan.sQMMaxPrePmntPenalty_rep = GetString("sQMMaxPrePmntPenalty");
        }
    }

    public partial class MainService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                default:
                    break;
            }
        }

        protected override void Initialize()
        {
			AddBackgroundItem("Calculations", new QMCalculationsServiceItem());
            AddBackgroundItem("PointAndFees", new PointAndFeesServiceItem());
        }
    }
}
