﻿<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import namespace="LendersOffice.AntiXss" %>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Calculations.ascx.cs" Inherits="LendersOfficeApp.newlos.Underwriting.QM.Calculations" %>
<style>
    #calculations {
        display: block;
        max-width: 800px;
        min-width: 650px;
        margin-left: 10px;
    }
    #calculations-column1 {
        width: 45%;
        display: inline-block;
        vertical-align: top;
    }
    #calculations-column2 {
        width: 55%;
        display: inline-block;
        vertical-align: top;
    }
    .fieldGroup {
        display: block;
    }
    .calculations-fieldLabel {
        width: 40%;
        display: inline-block;
    }
    .fieldValue {
        width: auto;
        display: inline-block;
    }
</style>
<div id="calculations">
    <div id="calculations-column1">
        <div class="fieldGroup">
            <div class="calculations-fieldLabel FieldLabel">
                Rate Lock Date
            </div>
            <div class="fieldValue">
                <ml:DateTextBox runat="server" ID="sRLckdD" readonly="true"></ml:DateTextBox>
            </div>
        </div>
        <div class="fieldGroup">
            <div class="calculations-fieldLabel FieldLabel">
                Interest Rate Set Date
            </div>
            <div class="fieldValue">
                <ml:DateTextBox runat="server" ID="sInterestRateSetD" readonly="true"></ml:DateTextBox>
                <asp:CheckBox runat="server" ID="sInterestRateSetDLckd" /><label>Locked</label>
            </div>
        </div>
        <div class="fieldGroup">
            <div class="calculations-fieldLabel FieldLabel">
                Closing Date
            </div>
            <div class="fieldValue">
                <ml:DateTextBox runat="server" ID="sDocMagicClosingD" readonly="true" ></ml:DateTextBox>
            </div>
        </div>
        <div class="fieldGroup">
            <div class="calculations-fieldLabel FieldLabel">
                APOR
            </div>
            <div class="fieldValue">
                <ml:PercentTextBox runat="server" ID="sQMAveragePrimeOfferR" Width="60px" />
                <asp:CheckBox runat="server" ID="sQMAveragePrimeOfferRLck" /><label>Locked</label>
            </div>
        </div>
        <div class="fieldGroup">
            <div class="calculations-fieldLabel FieldLabel">
                Par Rate
            </div>
            <div class="fieldValue">
                <ml:PercentTextBox runat="server" ID="sQMParR" Width="60px" />
            </div>
        </div>
        <div class="fieldGroup">
            <div class="calculations-fieldLabel FieldLabel">
                QM Qual Rate
            </div>
            <div class="fieldValue">
                <ml:PercentTextBox runat="server" ID="sQMQualR" ReadOnly="true" Width="60px" />
            </div>
        </div>
        <div class="fieldGroup">
            <div class="calculations-fieldLabel FieldLabel">
                QM DTI
            </div>
            <div class="fieldValue">
                <ml:PercentTextBox runat="server" ID="sQMQualBottom" ReadOnly="true"  Width="60px" />
            </div>
        </div>
    </div><div id="calculations-column2">
        <div class="fieldGroup">
            <div class="calculations-fieldLabel FieldLabel">
                Total QM Fee Amount
            </div>
            <div class="fieldValue">
                <ml:MoneyTextBox runat="server" ID="sQMTotFeeAmount"  ReadOnly="true" Width="90px"> </ml:MoneyTextBox>
            </div>
        </div>
        <div class="fieldGroup">
            <div class="calculations-fieldLabel FieldLabel">
                Financed QM Fee Amount
            </div>
            <div class="fieldValue">
                <ml:MoneyTextBox runat="server" ID="sQMTotFinFeeAmount" ReadOnly="true" Width="90px"></ml:MoneyTextBox>
            </div>
        </div>
        <div class="fieldGroup">
            <div class="calculations-fieldLabel FieldLabel">
                Excludable discount points
            </div>
            <div class="fieldValue">
                <ml:PercentTextBox runat="server" ID="sQMExclDiscntPnt" Width="60px" />
                <asp:CheckBox runat="server" ID="sQMExclDiscntPntLck" /><label>Locked</label>
            </div>
        </div>
        <div class="fieldGroup AllowNonExcludable">
            <div class="calculations-fieldLabel FieldLabel">
                Non-excludable discount points
            </div>
            <div class="fieldValue">
                <ml:PercentTextBox runat="server" ID="sQMNonExcludableDiscountPointsPc" Width="60px" />
            </div>
        </div>
        <div class="fieldGroup">
            <div class="calculations-fieldLabel FieldLabel">
                FHA UFMIP at closing
            </div>
            <div class="fieldValue">
                <ml:MoneyTextBox runat="server" ID="sQMFhaUfmipAtClosing"  Width="90px"></ml:MoneyTextBox>
                <asp:CheckBox runat="server" ID="sQMFhaUfmipAtClosingLck" /><label>Locked</label>
            </div>
        </div>
        <div class="fieldGroup">
            <div class="calculations-fieldLabel FieldLabel">
                Maximum prepayment penalty
            </div>
            <div class="fieldValue">
                <ml:MoneyTextBox runat="server" ID="sQMMaxPrePmntPenalty"  Width="90px"></ml:MoneyTextBox>
            </div>
        </div>
    </div>
</div>
<table cellspacing="0" cellpadding="2" class="calc" width="800px">
    <colgroup>
        <col class="FieldLabel" />
        <col class="FieldLabel" />
        <col class="FieldLabel" />
    </colgroup>
        <tr class="GridHeader">
            <th colspan="3">
                Intermediate Calculation
            </th>
        </tr>
 
        <tr class="GridItem">
            <td>
                QM APR Rate Spread
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="sQMAprRSpread"></ml:EncodedLabel>
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="sQMAprRSpreadCalcDesc"></ml:EncodedLabel>
            </td>
        </tr>
        <tr class="GridAlternatingItem">
            <td>
                Par rate spread
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="sQMParRSpread"></ml:EncodedLabel>        
            </td>
            <td>
                <ml:EncodedLabel runat="server" id="sQMParRSpreadCalcDesc"></ml:EncodedLabel>
            </td>
        </tr>
        <tr class="GridItem">
            <td>
                Excess Upfront MIP
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="sQMExcessUpfrontMIP" />
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="sQMExcessUpfrontMIPCalcDesc" />
            </td>
        </tr>
        <tr class="GridAlternatingItem">
            <td>
                Excess discount points
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="sQMExcessDiscntFPc"></ml:EncodedLabel>
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="sQMExcessDiscntFCalcDesc"></ml:EncodedLabel>     
            
            </td>
        </tr>
        <tr class="GridItem">
            <td>
                Discount buydown rate
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="sQMDiscntBuyDownR"></ml:EncodedLabel>       
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="sQMDiscntBuyDownRCalcDesc"></ml:EncodedLabel>
            </td>
        </tr>
        <tr class="GridAlternatingItem">
            <td>
                QM total loan amount
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="sQMLAmt"></ml:EncodedLabel>
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="sQMLAmtCalcDesc"></ml:EncodedLabel>
            </td>
        </tr>
            
       </table>
       
       <br />
       <table cellspacing="0" cellpadding="2" class="calc">
       
               <col class="FieldLabel" />
        <col class="FieldLabel" />
        <col class="FieldLabel" />
        
        <tr class="GridHeader">
            <th colspan="3">QM Point and Fees Calculation</th>
            
        </tr>
    
                <tr class="GridItem">
            <td>
                QM Total loan amount
            </td>
            <td>
                    &nbsp;
            </td>
            <td>
                    <ml:EncodedLabel runat="server" ID="sQMLAmt_1"></ml:EncodedLabel>  
            </td>
        </tr>
                <tr class="GridAlternatingItem">
            <td>
                Allowable QM Point and Fees
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="sQMMaxPointAndFeesAllowedAmt"></ml:EncodedLabel>&nbsp;&nbsp;&nbsp;
                <ml:EncodedLabel runat="server" ID="sQMMaxPointAndFeesAllowedPc"></ml:EncodedLabel>
            </td>
        </tr>
                <tr class="GridItem">
            <td colspan="3">
                &nbsp;
            </td>
   
        </tr>
                <tr class="GridAlternatingItem">
            <td>
                Total QM Fee Amount
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="sQMTotFeeAmount_1"></ml:EncodedLabel>
            </td>
        </tr>
        <tr class="GridItem">
            <td>
                Originator Compensation
            </td>
            <td>
                +
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="sGfeOriginatorCompF" />
            </td>
        </tr>
                <tr class="GridAlternatingItem">
            <td>
                Excess Discount Points
            </td>
            <td>
            +
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="sQMExcessDiscntF"></ml:EncodedLabel>
            </td>
        </tr>
                <tr class="GridItem">
            <td>
                Excess Upfront MIP
            </td>
            <td>+
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="sQMExcessUpfrontMIP_1"></ml:EncodedLabel>
            </td>
        </tr>
                <tr class="GridAlternatingItem">
            <td>
               Maximum prepayment penalty
            </td>
            <td>
                +
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="sQMMaxPrePmntPenalty_1"></ml:EncodedLabel>    
            </td>
        </tr>
        <tr class="GridItem">
            <td>
                Total QM Points and Fees
            </td>
            <td> &nbsp;
            </td>
            <td runat="server" id="StatusRow">
                <ml:EncodedLabel runat="server" ID="sQMTotFeeAmt"></ml:EncodedLabel>&nbsp;&nbsp;&nbsp;
                <ml:EncodedLabel runat="server" ID="sQMTotFeePc"></ml:EncodedLabel>    
                <asp:Image runat="server" ImageUrl="~/images/edocs/reject.png" /> 
            </td>
        </tr>
</table>

<div id="divWarning" runat="server">
    <br />
    <img style="padding-left:10px" src="../../../images/warn.png" /> Unable to determine if the fee recipient is a third-party or affiliate, see <a href="Main.aspx?pg=1&loanid=<%=AspxTools.HtmlString(LoanID)%>">QM Point and Fees</a>
</div>

<ul class="nob FieldLabel">
    <li>
        <asp:CheckBox runat="server" ID="sQMLoanPassesPointAndFeesTest" Enabled="false"  /><span class="FieldLabel">Meets points and fees test</span></li>
    <li>
        <asp:CheckBox runat="server" ID="sQMLoanDoesNotHaveNegativeAmort"  Enabled="false" /><span class="FieldLabel">Does not have negative amortization or interest-only features</span></li>
    <li>
        <asp:CheckBox runat="server" ID="sQMLoanDoesNotHaveBalloonFeature" Enabled="false"  /><span class="FieldLabel">Does not have a balloon feature</span></li>
    <li>
        <asp:CheckBox runat="server" ID="sQMLoanDoesNotHaveAmortTermOver30Yr" Enabled="false" /><span class="FieldLabel">Does not have an amortization term in excess of 30 years</span></li>
    <li>
        <asp:CheckBox runat="server" ID="sQMLoanHasDtiLessThan43Pc"  Enabled="false" /><span class="FieldLabel">Has QM DTI less than 43%</span></li>
    <li>
        <asp:CheckBox runat="server" ID="sQMIsVerifiedIncomeAndAssets"  /><span class="FieldLabel">Used verified income and assets</span></li>
    <li>
    <!-- use comboboxes to display it disabled -->
        <asp:CheckBox runat="server" ID="sQMIsEligibleByLoanPurchaseAgency"/><span class="FieldLabel">Eligible for purchase or guarantee by a qualifying agency.</span>
            <asp:DropDownList runat="server" id="sQMLoanPurchaseAgency"> </asp:DropDownList>
            <ml:ComboBox runat="server" ID="sQMLoanPurchaseAgencyCombo"></ml:ComboBox>
         </li>
    <li  runat="server" id="ComplianceChecks">
        <asp:CheckBox runat="server" ID="sQMLoanPassedComplianceChecks" Enabled="false" /><span class="FieldLabel">Passed compliance checks</span></li>
        <li></li>
        <li>
            QM Status <asp:DropDownList runat="server" id="sQMStatusT" style="display: none;"> </asp:DropDownList>
            <ml:ComboBox runat="server" ID="sQMStatusTCombo" ReadOnly></ml:ComboBox>
        </li>
        <li>
            Higher-priced indicator <asp:DropDownList runat="server" id="sHighPricedMortgageT"> </asp:DropDownList>
            <asp:CheckBox runat="server" ID="sHighPricedMortgageTLckd" Text="Locked" />
        </li>
    
    <li>
        <asp:CheckBox ID="sIsExemptFromAtr" runat="server" text="Exempt from ATR regulations"/>
    </li>
</ul>
<script type="text/javascript">
    
    function _postRefreshCalculation(result, clientid) {
        var ids = [
            'sQMAprRSpread',
            'sQMAprRSpreadCalcDesc',
            'sQMParRSpread',
            'sQMParRSpreadCalcDesc',
            'sQMExcessUpfrontMIP',
            'sQMExcessUpfrontMIPCalcDesc',
            'sQMExcessDiscntFPc',
            'sQMExcessDiscntFCalcDesc',
            'sQMDiscntBuyDownR',
            'sQMDiscntBuyDownRCalcDesc',
            'sQMLAmt',
            'sQMLAmtCalcDesc',
            'sQMLAmt_1',
            'sQMMaxPointAndFeesAllowedAmt',
            'sQMMaxPointAndFeesAllowedPc',
            'sQMTotFeeAmount_1',
            'sQMExcessDiscntF',
            'sQMExcessUpfrontMIP_1',
            'sQMMaxPrePmntPenalty_1',
            'sQMTotFeeAmt',
            'sQMTotFeePc'], i = 0, l = ids.length, id, obj;
            
            for( i; i < l; i++){
                id = ids[i]; 
                obj = document.getElementById(clientid + "_" + id);
                res = result[id];
                obj.innerHTML = res;
            }

            $(clientid + '_sQMIsEligibleByLoanPurchaseAgency').prop('disabled', result.sQMLoanPurchaseAgencyIsDisabled);


            var QmPassed = result["sQMLoanPassesPointAndFeesTest"] === "True";
            document.getElementById(clientid + "_" +"sQMLoanPassesPointAndFeesTest").checked = QmPassed;
            
            if(QmPassed)
                document.getElementById(clientid + "_" +"StatusRow").className = "QmSuccess";
            else
                document.getElementById(clientid + "_" +"StatusRow").className = "QmFail";
            
            displayAutomatedCheckBoxes();
    }
    $(function(){
        function updateLockBox() {
            var fieldId = $(this).data('lckid');
            var field = document.getElementById(fieldId);
            if (field == null) return;
            var $field = $(field);
            var isLocked = this.checked;
            
            if (field.tagName == "SELECT") {
                disableDDL(field, !isLocked);
            } else {
                $(field).prop('readonly', !isLocked)
                    .css('background-color', isLocked ? 'white' : 'lightgray');
            }
        }

        $(document).on( 'click','input[data-lckid]', updateLockBox);
        $('input[data-lckid]').each(function(){
            updateLockBox.apply(this);
        });

        $(document).on('change', 'input,select', function(){
            refreshCalculation();
        });
        
        <%if(AllowNonExcludableDiscount) { %>
        $('.NoNonExcludable').hide();
        <%} else { %>
        $('.AllowNonExcludable').hide();
        <%} %>
    });

    displayAutomatedCheckBoxes();
    
</script>
