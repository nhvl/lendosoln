﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QMItemProps.ascx.cs" Inherits="LendersOfficeApp.newlos.Underwriting.QM.QMItemProps" %>
<td>
    <asp:dropdownlist id="PdByT_dd" runat="server" onchange="if(typeof(refreshCalculation) == 'function') refreshCalculation();">
        <asp:ListItem Value="0">borr pd</asp:ListItem>
        <asp:ListItem Value="1">borr fin</asp:ListItem>
        <asp:ListItem Value="2">seller</asp:ListItem>
        <asp:ListItem Value="3">lender</asp:ListItem>
        <asp:ListItem Value="4">broker</asp:ListItem>
    </asp:dropdownlist>
</td>
<td><input id="Apr_chk" runat="server" type="checkbox" onclick="if(typeof(refreshCalculation) == 'function') refreshCalculation();"></td>
<td><input id="TrdPty_chk" runat="server" type="checkbox" ></td>
<td><input id="Aff_chk" runat="server" type="checkbox" ></td>
<td><img id="Warning" runat="server" style="padding-left:10px" src="../../../images/warn.png" /></td>