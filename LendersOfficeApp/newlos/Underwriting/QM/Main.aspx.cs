﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Underwriting.QM
{
    public partial class Main : BaseLoanPage
    {
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            EnableJqueryMigrate = false;

            int page = RequestHelper.GetInt("pg", 0);
            if (page == 1)
            {
                PDFPrintClass = typeof(LendersOffice.Pdf.CQMPointsAndFeesPDF);
            }
            else
            {
                PDFPrintClass = typeof(LendersOffice.Pdf.CQMSummaryFormPDF);
            }

            Tabs.AddToQueryString("loanid", LoanID.ToString());
            Tabs.RegisterControl("QM Calculations", Calculations);
            Tabs.RegisterControl("QM Point and Fees", PointAndFees);
            PageID = "QM";

            base.OnInit(e);
           
        }
    }
}
