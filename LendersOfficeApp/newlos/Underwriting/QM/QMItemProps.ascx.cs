﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using DataAccess;

namespace LendersOfficeApp.newlos.Underwriting.QM
{
    public partial class QMItemProps : BaseLoanUserControl
    {
        protected bool m_Enabled = true;
        public bool Enabled
        {
            get
            {
                return m_Enabled;
            }
            set
            {
                m_Enabled = value;
                SetEnabled(m_Enabled);
            }
        }

        private void SetEnabled(bool enable)
        {
            var childControls = this.GetType().GetFields(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            foreach (var control in childControls)
            {
                if (control.FieldType.BaseType == typeof(HtmlInputControl))
                {
                    ((HtmlControl)control.GetValue(this)).Disabled = !enable;
                }
                else if (control.FieldType.BaseType == typeof(WebControl))
                {
                    ((WebControl)control.GetValue(this)).Enabled = enable;
                }
            }
        }

        public int PdByT
        {
            get { return Convert.ToInt32(PdByT_dd.SelectedValue); }
            set { PdByT_dd.SelectedValue = value.ToString(); }
        }

        public DropDownList PdByT_DDL
        {
            get { return PdByT_dd; }
        }

        public bool Apr
        {
            get { return Apr_chk.Checked; }
            set { Apr_chk.Checked = value; }
        }

        public bool PaidToThirdParty
        {
            get { return TrdPty_chk.Checked; }
            set { TrdPty_chk.Checked = value; }
        }

        public bool ThisPartyIsAffiliate
        {
            get { return Aff_chk.Checked; }
            set { Aff_chk.Checked = value; }
        }

        public bool PaidToThirdPartyVisible
        {
            get { return TrdPty_chk.Visible; }
            set
            {
                TrdPty_chk.Visible = value;
                ThisPartyIsAffiliateVisible = value;
            }
        }

        public bool ThisPartyIsAffiliateVisible
        {
            get { return Aff_chk.Visible; }
            set { Aff_chk.Visible = value; }
        }

        public bool PaidToThirdPartyDisabled
        {
            get { return TrdPty_chk.Disabled; }
            set { TrdPty_chk.Disabled = value; }
        }

        public bool ThisPartyIsAffiliateDisabled
        {
            get { return Aff_chk.Disabled; }
            set { Aff_chk.Disabled = value; }
        }

        public bool ShowQmWarning
        {
            get { return Warning.Visible; }
            set { Warning.Visible = value; }
        }

        public void InitItemProps(int props)
        {
            PdByT = LosConvert.GfeItemProps_Payer(props);
            Apr = LosConvert.GfeItemProps_Apr(props);
            PaidToThirdParty = LosConvert.GfeItemProps_PaidToThirdParty(props);
            ThisPartyIsAffiliate = LosConvert.GfeItemProps_ThisPartyIsAffiliate(props);
            ShowQmWarning = LosConvert.GfeItemProps_ShowQmWarning(props);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string id = this.ClientID;
            TrdPty_chk.Attributes.Add("onclick", "OnTpCheck('" + id + "'); if(typeof(refreshCalculation) == 'function') refreshCalculation();");
            Aff_chk.Attributes.Add("onclick", "OnAffCheck('" + id + "'); if(typeof(refreshCalculation) == 'function') refreshCalculation();");

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "QMItemProps",
                @"<script language='javascript'>
                    function OnTpCheck(ClientID)
                    {
	                    var tp = document.getElementById(ClientID + '_TrdPty_chk');
	                    var aff = document.getElementById(ClientID + '_Aff_chk');
                        if (tp != null && aff != null) {
                            if (!tp.checked)
                                aff.checked = false;
                        }
                    }

                    function OnAffCheck(ClientID)
                    {
	                    var tp = document.getElementById(ClientID + '_TrdPty_chk');
	                    var aff = document.getElementById(ClientID + '_Aff_chk');
                        if (tp != null && aff != null) {
                            if (aff.checked)
		                        tp.checked = true;
                        }
                    }
                </script>");
        }
    }
}