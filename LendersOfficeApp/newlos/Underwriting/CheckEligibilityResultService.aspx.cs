﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.Security;
using LendersOfficeApp.newlos.Lockdesk;
using LendersOffice.RatePrice;

namespace LendersOfficeApp.newlos.Underwriting
{
    public partial class CheckEligibilityResultService : BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "RequestPricing":
                    RequestPricing();
                    break;
                case "IsResultReady":
                    IsResultReady();
                    break;
                case "GetResult":
                    GetResult();
                    break;
            }
        }

        private void RequestPricing()
        {
            Guid sLId = GetGuid("loanid");
            E_sPricingModeT sPricingModeT = (E_sPricingModeT)GetInt("sPricingModeT");
            AbstractUserPrincipal principal = this.User as AbstractUserPrincipal;
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            HybridLoanProgramSetOption historicalOption = options;

            BrokerDB broker = BrokerDB.RetrieveById(principal.BrokerId);

            if (broker.IsEnableHistoricalPricing)
            {
                historicalOption = new HybridLoanProgramSetOption();
                historicalOption.ApplicablePolicies = (E_LoanProgramSetPricingT)GetInt("historicalPolicy", 0);
                historicalOption.RateOptions = (E_LoanProgramSetPricingT)GetInt("historicalRateOption", 0);
            }
            Guid requestId = DistributeUnderwritingEngine.RequestInternalPricingResult(principal, sLId, sPricingModeT,historicalOption);

            SetResult("RequestId", requestId);
        }

        private void IsResultReady()
        {
            Guid requestId = GetGuid("requestid");

            bool isReady = LpeDistributeResults.IsResultAvailable(requestId);

            SetResult("IsReady", isReady);
        }

        private void GetResult()
        {


            Guid requestId = GetGuid("requestid");

            UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultItem(requestId);

            if (null == resultItem)
            {
                throw new CBaseException(ErrorMessages.Generic, "UnderwritingResultItem is null");
            }
            if (resultItem.IsErrorMessage)
            {
                throw new CBaseException(resultItem.ErrorMessage, resultItem.ErrorMessage);
            }

            Guid sLId = GetGuid("LoanID");
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(CheckEligibilityResultService));
            dataLoan.sPricingModeT = (E_sPricingModeT)GetInt("sPricingModeT");
            dataLoan.InitLoad();
            AbstractUserPrincipal principal = this.User as AbstractUserPrincipal;
            bool addOriginatorComp = (dataLoan.BrokerDB.IsUsePriceIncludingCompensationInPricingResult
                && dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees);

            JsonResult result = new JsonResult(principal, dataLoan.sPricingModeT, addOriginatorComp);
            bool isRateMerge = dataLoan.sProdFilterDisplayrateMerge;

            if (isRateMerge)
            {
                BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);
                foreach (var rateMergeGroup in resultItem.GetRateMergeResult(brokerDB))
                {
                    result.Add(rateMergeGroup);
                }
            }
            else
            {
                foreach (var program in resultItem.GetEligibleLoanProgramList())
                {
                    result.Add(program);
                }
            }

            foreach (var ineligibleProgram in resultItem.GetIneligibleLoanProgramList())
            {
                result.AddIneligibleLoanProgram(ineligibleProgram);
            }
            string s = ObsoleteSerializationHelper.JsonSerialize(result);
            long startTicks = GetLong("StartTicks", 0);
            if (startTicks > 0)
            {
                int duration = (int)(DateTime.Now.Ticks - startTicks) / 10000;
                DistributeUnderwritingEngine.RecordPricingTotalTime(requestId, dataLoan.sPricingModeT.ToString(), duration);
            }

            SetResult("json", s);
        }
    }
}
