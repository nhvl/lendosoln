<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="ResumeLock.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Underwriting.ResumeLock" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>Resume Lock with Modifications</title>
  </HEAD>
<body bgColor=gainsboro margin="0" MS_POSITIONING="FlowLayout" scroll="no">
<script language=javascript>
<!--
function _init() {
  resizeForIE6And7(400, 250);
  
  if (<%= AspxTools.JsBool(LendersOffice.Common.RequestHelper.GetBool("backend")) %> )
  {
	document.getElementById('emailCertRow').style.display = 'none';
  }
  
}
function f_close(bOk) {
  var arg = window.dialogArguments || {};
  arg.OK = bOk;
    
  arg.tbReason = <%= AspxTools.JsGetElementById(tbReason) %>.value;
  arg.emailCert = <%= AspxTools.JsGetElementById(emailLockCert) %>.checked;
  onClosePopup(arg);
}


//-->
</script>
<h4 class="page-header">Resume Lock with Modifications</h4>
<form id=ResumeLock method=post runat="server" onreset="return false;">
<TABLE id=Table1 style="MARGIN: 0px" cellSpacing=0 cellPadding=0 width="100%" border=0>
  <tr>
    <td class="FieldLabel" nowrap style="padding-left: 5px;padding-top: 5px">
      <table id="Table2" cellspacing="0" cellpadding="0" border="0">
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td class="FieldLabel" colspan="2">Reason&nbsp;<asp:TextBox ID="tbReason" runat="server" Width="220px"></asp:TextBox></td>
        </tr>
        <tr id="emailCertRow">
          <td colspan="2" class="FieldLabel"><asp:CheckBox ID="emailLockCert" Checked="true" runat="server" Text="View and email the Rate Lock Confirmation after locking the rate" /> </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td nowrap align="center">
      <input type="button" value="Resume Rate Lock" class="ButtonStyle" style="width: 135px" onclick="f_close(true);" nohighlight>&nbsp; 
      <input type="button" value="Cancel" class="ButtonStyle" style="width: 55px" onclick="f_close(false);" nohighlight>
    </td>
  </tr>
</table>
<uc1:cModalDlg id=CModalDlg1 runat="server" />
</FORM>
	
  </body>
</HTML>
