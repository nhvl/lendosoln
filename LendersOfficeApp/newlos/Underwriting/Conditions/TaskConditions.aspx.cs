﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.Task;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Security;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using LendersOffice.Admin;
using LendersOffice.AntiXss;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Underwriting.Conditions
{
    public partial class TaskConditions : BaseLoanPage
    {
        // OPM 211800, 8/19/2015, ML
        // Closers will now be able to view the Conditions page without
        // needing the AllowUnderwritingAccess permission. To support this, we
        // determine the required read/write permissions to have BaseLoanPage
        // check based on the permissions that the user currently has. If the
        // user has AllowCloserRead, AllowCloserWrite, or AllowUnderwritingAccess,
        // they are authorized and BaseLoanPage does not have to check the permissions
        // again. If not, we return a default of AllowUnderwritingAccess and the check
        // will fail.
        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                if (BrokerUser.HasPermission(Permission.AllowCloserRead) ||
                    BrokerUser.HasPermission(Permission.AllowUnderwritingAccess))
                {
                    return ConstApp.SkipConditionBasePagePermissionCheck;
                }

                return ConstApp.DefaultConditionPagePermission;
            }
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                if (BrokerUser.HasPermission(Permission.AllowCloserWrite) ||
                    BrokerUser.HasPermission(Permission.AllowUnderwritingAccess))
                {
                    return ConstApp.SkipConditionBasePagePermissionCheck;
                }

                return ConstApp.DefaultConditionPagePermission;
            }
        }

        public override bool IsReadOnly
        {
            get
            {
                return !(BrokerUser.HasPermission(Permission.AllowCloserWrite) ||
                    BrokerUser.HasPermission(Permission.AllowUnderwritingAccess));
            }
        }


        private ICollection<ConditionCategory> ManageCategories { get; set; }
        private ICollection<ConditionCategory> CloseCategories { get; set; }
        private ICollection<ConditionCategory> WorkOnCategories { get; set; }
        private ICollection<ConditionCategory> AllCategories { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Set printing framework entries.
            PageTitle = "Conditions";
            PageID = "UnderwritingConditionsForTask";

            ConditionCategoriesWithPermissionInfo categories = new ConditionCategoriesWithPermissionInfo(BrokerUser);
            ManageCategories = categories.ManageCategories;
            AllCategories = categories.AllCategories;
            WorkOnCategories = categories.WorkOnCategories;
            CloseCategories = categories.CloseCategories;

            var categoriesForJson = (from category in ManageCategories
                                     select new
                                     {
                                         Id = category.Id,
                                         Category = category.Category
                                     }).ToList();


            string categoryJson = ObsoleteSerializationHelper.JavascriptJsonSerialize(categoriesForJson);


            var conditionChoices = from choice in ConditionChoice.RetrieveForUser(BrokerUser)
                                   select new
                                   {
                                       Id = choice.ConditionChoiceId,
                                       Category = choice.Category.Category,
                                       ConditionType = choice.ConditionType_rep,
                                       ConditionSubject = choice.ConditionSubject,
                                       sLT = choice.sLT_rep
                                   };

            ConditionChoiceTable.DataSource = conditionChoices;
            ConditionChoiceTable.DataBind();

            if (ConditionChoiceTable.Rows.Count > 0)
            {
                ConditionChoiceTable.HeaderRow.TableSection = TableRowSection.TableHeader;
            }


            Tools.Bind_ConditionTypeString(ConditionTypeFilter);

            #region condition choices
            CategoryFilter.DataSource = ManageCategories.Union(CloseCategories).Union(WorkOnCategories)
                                                        .OrderByDescending(p => p.IsDisplayAtTop)
                                                        .ThenBy(p => p.Category);
            CategoryFilter.DataValueField = "Category";
            CategoryFilter.DataTextField = "Category";
            CategoryFilter.DataBind();

            CategoryFilter.Items.Insert(0, new ListItem("All", "All"));
            ConditionTypeFilter.Items.Insert(0, new ListItem("All", "All"));
            #endregion

            CPageData data = CPageData.CreateUsingSmartDependency(LoanID, typeof(TaskConditions));
            data.InitLoad();

            if (data.IsTemplate)
            {
                HeaderPageTitle.InnerText = "Conditions Template";
                TemplateStyle.Visible = true;
                ClientScript.RegisterHiddenField("IsLoanTemplate", Boolean.TrueString);
            }
            else {
                CAppData app = data.GetAppData(0);
                Header.Title = String.Format("{0} - {1}", app.aBLastFirstNm, data.sLNm);
            }

            ConditionChangeWarning.Visible = data.IsConditionLastModifiedDAfterApprovalCertLastSentD;

            ClientScript.RegisterHiddenField("sLT", data.sLT_rep);
            ClientScript.RegisterHiddenField("CanViewHiddenInformation", Task.CanUserViewHiddenInformation(BrokerUser).ToString());

            List<LightCondition> conditions = new List<LightCondition>();
            TaskPermissionProcessor processor = new TaskPermissionProcessor(BrokerUser);
            foreach (Task task in Task.GetActiveConditionsByLoanId(BrokerUser.BrokerId, LoanID, Task.CanUserViewHiddenInformation(BrokerUser),AllCategories))
            {
                if (processor.Resolve(task) != E_UserTaskPermissionLevel.None)
                    conditions.Add(new LightCondition(BrokerUser, task, Task.CanUserViewHiddenInformation(BrokerUser),processor));
            }

            string conditionJson  = ObsoleteSerializationHelper.JavascriptJsonSerialize(conditions);

            var pageSetupData = new
            {
                Categories = categoriesForJson,
                Conditions = conditions,
                EditorName = BrokerUser.DisplayNameForAuditRecentModification,
                Resources = new
                {
                    UpArrowFile = Page.ResolveUrl("~/images/up-blue.gif"),
                    TopArrowFile = Page.ResolveUrl("~/images/up-blue-top.gif"),
                    DownArrowFile = Page.ResolveUrl("~/images/down-blue.gif"),
                    BottomArrowFile = Page.ResolveUrl("~/images/down-blue-bottom.gif"),
                    GrippyFile = Page.ResolveUrl("~/images/grip.png"),
                    HoverUpArrowFile = Page.ResolveUrl("~/images/up-orange.gif"),
                    HoverTopArrowFile = Page.ResolveUrl("~/images/up-orange-top.gif"),
                    HoverDownArrowFile = Page.ResolveUrl("~/images/down-orange.gif"),
                    HoverBottomArrowFile = Page.ResolveUrl("~/images/down-blue-orange.gif")
                }
            };

            ClientScript.RegisterStartupScript(typeof(TaskConditions), "ConditionSetupData", string.Format("var ConditionData = {0};", ObsoleteSerializationHelper.JavascriptJsonSerialize(pageSetupData)),true);

            ConditionRepeater.DataSource = conditions;
            ConditionRepeater.DataBind();
            RegisterService("ConditionService", "/newlos/Underwriting/Conditions/ConditionService.aspx");

            if (Broker.IsUseNewTaskSystemStaticConditionIds)
            {
                ShowNumber.Visible = false;
                ShowCondRowId.Visible = true;
            }
            else
            {
                ShowNumber.Visible = true;
                ShowCondRowId.Visible = false;
            }

            // OPM 108448.  Need to see if this user can manage a permission level.
            var loanIsNotWriteable = (data.CanWrite==false);
            var userCanCreateTask = (Task.Create(LoanID, BrokerID).DefaultPermissionForCondition != null) && false == loanIsNotWriteable;
            ClientScript.RegisterHiddenField("CanCreate", userCanCreateTask.ToString());
            ClientScript.RegisterHiddenField("LoanIsNotWriteable", loanIsNotWriteable.ToString());

            EnableJqueryMigrate = false;
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("jquery.tablesorter.dev.js");
            RegisterJsScript("jquery.tmpl.js");
            RegisterJsScript("Condition/conditionui.js");
            RegisterJsScript("LQBPopup.js");
        }


        public void ConditionRepeatable_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }
            LightCondition condition = (LightCondition)args.Item.DataItem;

            Literal number = (Literal)args.Item.FindControl("Number");
            Literal condRowId = (Literal)args.Item.FindControl("CondRowId");
            Literal status = (Literal)args.Item.FindControl("Status");
            Literal closedDate = (Literal)args.Item.FindControl("ClosedDate");
            Literal closedBy = (Literal)args.Item.FindControl("ClosedBy");
            DropDownList categories = (DropDownList)args.Item.FindControl("Categories");
            Literal subject = (Literal)args.Item.FindControl("Subject");
            Literal internalNotes = (Literal)args.Item.FindControl("InternalNotes");
            Literal assignedTo = (Literal)args.Item.FindControl("AssignedTo");
            Literal dueDate = (Literal)args.Item.FindControl("DueDate");
            PlaceHolder hiddenCell = (PlaceHolder)args.Item.FindControl("HiddenCell");
            HtmlInputCheckBox isHidden = (HtmlInputCheckBox)args.Item.FindControl("IsHidden");
            Literal createdDate = (Literal)args.Item.FindControl("CreatedDate");

            number.Text = (args.Item.ItemIndex + 1).ToString();
            condRowId.Text = condition.CondRowId;
            status.Text = condition.Status;
            closedDate.Text = condition.ClosedDate;
            closedBy.Text = condition.ClosedBy;


            hiddenCell.Visible = Task.CanUserViewHiddenInformation(BrokerUser);

            categories.DataSource = ManageCategories;
            categories.DataValueField = "Id";
            categories.DataTextField = "Category";
            categories.DataBind();

            categories.ClearSelection();
            ListItem selectedItem = categories.Items.FindByValue(condition.CategoryId.ToString());
            if (selectedItem == null)
            {
                selectedItem = new ListItem(condition.Category, condition.CategoryId.ToString());
                categories.Items.Add(selectedItem);
            }
            selectedItem.Selected = true;

            String[] lines = condition.Subject.Split(new String[]{"\n"}, StringSplitOptions.None);
            String subjectText = "";
            for (int i = 0; i < lines.Length; i++)
            {
                subjectText += AspxTools.HtmlString(lines[i].Replace("\n", "").Replace("\r", "")) + "<br>";
            }
            subjectText = subjectText.Substring(0, subjectText.Length - 4);

            lines = condition.InternalNotes.Split(new String[] { "\n" }, StringSplitOptions.None);
            String internalNotesText = "";
            for (int i = 0; i < lines.Length; i++)
            {
                internalNotesText += AspxTools.HtmlString(lines[i].Replace("\n", "").Replace("\r", "")) + "<br>";
            }
            internalNotesText = internalNotesText.Substring(0, internalNotesText.Length - 4);


            subject.Text = subjectText;
            internalNotes.Text = internalNotesText;
            assignedTo.Text = condition.AssignedTo;
            dueDate.Text = condition.DueDateDescription;
            hiddenCell.Visible = Task.CanUserViewHiddenInformation(BrokerUser);
            isHidden.Checked = condition.IsHidden;
            createdDate.Text = condition.CreatedOn;

        }
    }
}
