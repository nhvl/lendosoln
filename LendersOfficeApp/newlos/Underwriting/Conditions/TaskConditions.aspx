﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaskConditions.aspx.cs" EnableEventValidation="false" EnableViewState="false" Inherits="LendersOfficeApp.newlos.Underwriting.Conditions.TaskConditions" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Conditions 2.0</title>
    <style runat="server" id="ShowNumber">
        .number { display: inline; }
        .CondRowId { display: none; }
    </style>
    <style runat="server" id="ShowCondRowId">
        .number { display: none; }
        .CondRowId { display: inline; }
    </style>
    <style type="text/css" >
        body { margin: 0; padding: 0; }
        a, img { outline: none; }
        a.moveArrow:link { text-decoration: none; }
        a img { border: 0; }
        a.moveArrow img { width: 10px; height: 10px; }
        .moveContainer { margin-left: 10px; }
        .grippy, .grippy img { float:left;  padding: 0; margin: 0; }
        .top { margin-top: 15px; }
         h2, h1 { 
            background-color :#003366;
            font-family: Arial, Helvetica, sans-serif;
            color: White;
            font-size: 12px;
        }
        h1 { font-size: 14px; padding: 0; margin: 0;}
        p.strong { font-weight: bold; }
        .centerContents { text-align: center; }
        .force { height: 65px; line-height: 16px; background-color: #003366;  }
        a:hover, a:active, a:focus { outline: 0; border: 0; text-decoration: none;}
		body{ background-color: gainsboro; }
		.closeLink, .reactivateLink { display: block; } 
		#conditions td a.lm {
		    display: block;
		    margin-left: 5px;

		}
		
		.growbox /* Required for autogrow to work */
        {
        	min-height: 60px;
            font-size: 12px;
            font-weight: 400;
            font-family: "Courier New", Courier, monospace;
            overflow: hidden;
        }
        .editbox
        {
        	padding-left: 1px;
        	white-space: pre-wrap;
        	word-wrap:  break-word;
        	word-break: break-word;
        	-ms-word-break : break-all;
            outline: none;
        }
        .growboxcontainer
        {
        	background-color: White;
            border: solid 1px #7F9DB9;
            vertical-align: top;
            max-width: 100%; /* Fixes a bug where a column mysteriously appears */
        }
        
		
		p { margin: 0; } /* for contenteditable */
		.sortableHeader span { color: Blue; text-decoration: underline; cursor: pointer; }
		 
		.hide { display: none; }
		.grippy { cursor: move; }
		
		#ConditionChoices {  background-color: gainsboro; border: black 2px solid; position: absolute; z-index:1000; width: 900px; padding: 2px; padding-bottom: 10px; }
	    #ConditionChoices  table { width: 96%; background-color: White; }
	    #ConditionChoices .Choices { background-color: White; 
	        border: 2px grove; 
            border-right: medium none; 
            cursor: default; 
            height: 500px; 
            margin: 8px; 
            overflow-y: auto;
            padding: 10px; 
        }
        div.fl { display: inline-block; margin-bottom: 10px; }
        div.cctype{ width: 250px; }
        h1 { margin: 0; }
        #InsertPosition { width: 30px; }
        #conditions { margin: 0; table-layout: fixed; }
        td textarea { width:97%; height: 65px; margin-top: 4px; margin-left: 4px; margin-right: 4px; }
        tr.highlight { background-color: gainsboro; }
        #ConditionChoices { cursor: move; }
        #ConditionChoices * { cursor: default; }
        #ConditionChangeWarning { background-color: Yellow; 
            border: black 1px solid;
            text-align: center;
            width: 100%;
            font-weight: bold;
            clear: both;
        }
        .categories { width: 120px; }

        #dialog-message { display: none; 
            background-color: Yellow;
            position:absolute;
            top: 50%;
            left: 48%;
            z-index:1000;
            border: 2px solid black;
            font-size: 18px; 
            padding: 5px;
            font-weight: bold;
        }
        
        .clear { clear: both; }
        #saveBtn { float: right; margin-right: 10px; }
        .MainRightHeader { 
            overflow: auto;
            line-height: 1.5em ;
        }
        
        .MainRightHeader  div { float: Left; }
            .ConditionChoiceTable { clear: both; table-layout:fixed; }
            
            .ConditionChoiceTable td { 
            word-break:break-all;
            }
    </style>

    <style>
        .checkbox-td {
            width: 22px;
        }

        .order-td {
            width: 20px;
        }

        .re-order-td {
            width: 50px;
        }

        .audit-td {
            width: 40px;
        }

        .status-td {
            width: 60px;
        }

        .closed-td {
            width: 70px;
        }

        .category-td {
            width: 120px;
        }

        .subject-td {
            width: 280px;
        }

        .notes-td {
            width: 260px;
        }

        .assigned-td {
            width: 90px;
        }

        .due-date-td {
            width: 110px;
        }

        .hidden-td {
            width: 40px;
        }

        .created-date-td {
            width: 65px;
        }
    </style>
    <ml:PassthroughLiteral runat="server" ID="TemplateStyle" Visible="false">
        <style type="text/css">
            td.closeCell, td.statusCell, th.statusHeader, th.closedHeader { display: none; }
        </style>
    </ml:PassthroughLiteral>
</head>
<body>
    <form id="form1" runat="server">
        <div  class="MainRightHeader" ><div  runat="server" id="HeaderPageTitle">Conditions </div>
        <input type="button" value="Save" id="saveBtn" NoHighlight="NoHighlight" />
        </div>
        <div class="clear"></div>
        <asp:PlaceHolder runat="server" ID="ConditionChangeWarning" Visible="false">
        <div class="warning" id="ConditionChangeWarning">
        The conditions list may have changed since the approval certificate was last emailed.
        </div>
        </asp:PlaceHolder>
        <script type="text/x-jquery-tmpl" id="condition_template" >
        
        <td class="centerContents">
            <input type="checkbox" class="selectable deleteBox" />
        </td>
        <td>
            <span class="number"></span>
            <span class="CondRowId">${CondRowId}</span>
        </td>
        <td>
            <a href="#" class="grippy" title="Drag and drop" >
            <img class="grippy" src="${virtual_root}${Resources.GrippyFile}" alt="G" /> </a>
                        <div class="moveContainer top">
                <a href="#" class="moveArrow Up" title="Move the condition one space up in the list">
                <img src="${virtual_root}${Resources.UpArrowFile}"  /></a>
                <a href="#" class="moveArrow Top"   title="Move the condition to the top of the list">
                <img src="${virtual_root}${Resources.TopArrowFile}"  /></a>
            </div>
            <div class="moveContainer">
                <a href="#" class="moveArrow  Down" title="Move the condition one space down in the list">
                <img src="${virtual_root}${Resources.DownArrowFile}"  /></a>
                <a href="#" class="moveArrow Bottom" title="Move the condition to the bottom of the list">
                <img src="${virtual_root}${Resources.BottomArrowFile}"  /></a>
            </div>
        </td>
        <td>
            <a href="#" class="audit">view</a>
        </td>
        <td class="statusCell">
            <span class="statusText">${Status}</span>
            <a href="#" class="reactivatelink lm">unsign</a> 
            <a href="#" class="closelink lm">sign off</a>
        </td>
        <td class="closeCell">
            
                <span class="closeDate">${ClosedDate}</span><br />
                {{if ClosedBy != ''}} 
                by
                {{/if}}
                <span class="closeBy">${ClosedBy}</span>
          
        </td>
        <td class="categoryCell">
            <select class="categories"> 
            {{if Permission === 'Manage'}} 
            {{each Categories}}
                <option {{if $value.Id === CategoryId}} selected="selected"{{/if}} value=${$value.Id} > 
                    ${$value.Category}
                </option>
            {{/each}}
            {{/if}}
            {{if Permission === 'WorkOn'}}
                <option value=${CategoryId}>${Category}</option>
            {{/if}}
            </select>
           
        </td>
        <td class="subjectCell growboxcontainer">
             <div contenteditable="true" class="subject growbox editbox">${Subject}</div>
        </td>
        <td class="internalNotesCell growboxcontainer" >
            <div contenteditable="true" class="internalNotes growbox editbox">${InternalNotes}</div>
        </td>
        <td class="assigedToCell">
            <span class="assignedToText">${AssignedTo}</span>
            <a href="#" class="assignlink lm">change</a>
        </td>
        <td class="dueCell">
            <span class="dueDateText">${DueDateDescription}</span>
            <a href="#" class="calculatelink lm">calculate</a>
        </td>
        {{if ShowHiddenData }}
        <td>
            <input type="checkbox" class="isHidden" {{if IsHidden }}checked="checked"{{/if}} />
        </td>
        {{/if}}
        <td class="createdDateCell">
            ${CreatedOn}
        </td>
        </script>

    <asp:Repeater runat="server" ID="ConditionRepeater"  OnItemDataBound="ConditionRepeatable_OnItemDataBound">
        <HeaderTemplate>
            <table id="conditions">
            
                <thead>
                    <tr class="GridHeader">
                     
                        <th class="stopClick checkbox-td">
                            <input type="checkbox" class="SelectAll">
                        </th>
                           <th class="stopClick order-td" >
                            &nbsp;
                        </th>
                        <th  class="stopClick re-order-td">
                            Re-Order
                        </th>
                        <th class="stopClick audit-td">
                            Audit
                        </th>
                        <th class="sortableHeader statusHeader status-td" >
                            <span>Status</span>
                        </th>
                        <th class="sortableHeader closedHeader closed-td" >
                            <span>Closed</span>
                        </th>
                        <th class="sortableHeader category-td" >
                            <span>Category</span>
                        </th>
                        <th class="sortableHeader subject-td" >
                            <span>Condition Subject</span>
                        </th>
                        <th class="sortableHeader notes-td" >
                            <span>Internal Notes</span>
                        </th>
                        <th class="sortableHeader assigned-td" >
                            <span>Assigned To</span>
                        </th>
                        <th class="sortableHeader due-date-td" >
                            <span>Due Date</span>
                        </th>
                        <% if (LendersOffice.ObjLib.Task.Task.CanUserViewHiddenInformation(BrokerUser))
                           { %>
                        <th class="stopClick hidden-td" >
                            Hidden
                        </th>
                        <% } %>
                        <th class="sortableHeader created-date-td" >
                            <span>Created Date</span>
                        </th>
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate>
        <FooterTemplate>
        </tbody>
        </table>
        </FooterTemplate>
        <ItemTemplate>
            <tr >
               <td>
                    <input type="checkbox" class="selectable deleteBox" />
                </td>
        
             <td>
                <span class="number"><ml:EncodedLiteral runat="server" ID="Number"> </ml:EncodedLiteral></span>
                <span class="CondRowId"><ml:EncodedLiteral runat="server" ID="CondRowId"> </ml:EncodedLiteral></span>
             </td>
        <td>
            <a href="#" class="grippy" title="Drag and drop" >
                <asp:Image  ImageUrl="~/images/grip.png" alt="G" runat="server" /> 
            </a>
            <div class="moveContainer top">
                <a href="#" class="moveArrow Up" title="Move the condition one space up in the list">
                    <asp:Image ImageUrl="~/images/up-blue.gif" runat="server" /> </a>
                <a href="#" class="moveArrow Top"   title="Move the condition to the top of the list">
                    <asp:Image ImageUrl="~/images/up-blue-top.gif" runat="server" /> </a>
            </div>
            <div class="moveContainer">
                <a href="#" class="moveArrow  Down" title="Move the condition one space down in the list">
                     <asp:Image runat="server" ImageUrl="~/images/down-blue.gif" /> </a>
                <a href="#" class="moveArrow Bottom" title="Move the condition to the bottom of the list">
                     <asp:Image runat="server" ImageUrl="~/images/down-blue-bottom.gif" /></a>
            </div>
        </td>
        <td>
            <a href="#" class="audit">view</a>
        </td>
        <td class="statusCell">
            <span class="statusText"> <ml:EncodedLiteral runat="server" ID="Status"> </ml:EncodedLiteral></span>
            <a href="#" class="reactivatelink lm">unsign</a> 
            <a href="#" class="closelink lm">sign off</a>
        </td>
        <td class="closeCell">
            
                <span class="closeDate"><ml:EncodedLiteral runat="server" ID="ClosedDate"></ml:EncodedLiteral></span><br />
                <span class="closeByLabel hide" >by</span>
                <span class="closeBy"><ml:EncodedLiteral runat="server" ID="ClosedBy"></ml:EncodedLiteral></span>
          
        </td>
        <td class="categoryCell">

            <asp:DropDownList  runat="server" ID="categories" CssClass="categories">
            
            </asp:DropDownList>
            
        </td>
        <td class="subjectCell growboxcontainer">
            <div contenteditable="true" class="subject growbox editbox" runat="server" id="SubjectDiv"><ml:PassthroughLiteral runat="server" ID="Subject"></ml:PassthroughLiteral></div>
        </td>
        <td class="internalNotesCell growboxcontainer">
            <div contenteditable="true" class="internalNotes growbox editbox" runat="server" id="InternalNotesDiv"><ml:PassthroughLiteral runat="server" ID="InternalNotes"></ml:PassthroughLiteral></div>
        </td>
        <td class="assigedToCell">
            <span class="assignedToText"><ml:EncodedLiteral runat="server" ID="AssignedTo"></ml:EncodedLiteral></span>
            <a href="#" class="assignlink lm">change</a>
        </td>
        <td class="dueCell">
            <span class="dueDateText"><ml:EncodedLiteral runat="server" ID="DueDate"></ml:EncodedLiteral></span>
            <a href="#" class="calculatelink lm">calculate</a>
        </td>
       <asp:PlaceHolder runat="server" ID="HiddenCell">
        <td >
            <input type="checkbox" runat="server" id="IsHidden" class="isHidden" />
        </td>
        </asp:PlaceHolder>
        
        <td class="createdDateCell">
            <ml:EncodedLiteral runat="server" ID="CreatedDate"></ml:EncodedLiteral>
        </td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
                <input type="button" value="Add condition" id="AddCondition" />
        <input type="button" value="Insert multiple conditions" id="ConditionChoiceList" />
        <input type="button" value="Import conditions from template..." id="ImportFromTemplate" NoHighlight="NoHighlight"/>
        <input type="button" value="Sign off selected conditions" id="CloseSelectedConditions" class="BatchOperationButton" NoHighlight="NoHighlight"/>
        <input type="button" value="Delete selected conditions" id="DeleteSelectedCondtions" class="BatchOperationButton" NoHighlight="NoHighlight"/>
        <input type="button" value="Restore deleted conditions..." id="RestoreConditions" />
        <div id="ConditionChoices" style="display: none">
  
            
            <div class="Choices dontPropagateMouse" >
            <div class="fl">
                      Category <asp:DropDownList runat="server" ID="CategoryFilter" CssClass="CatFilter"></asp:DropDownList> 
                      </div>
                      <div class="fl cctype">
            Condition Type <asp:DropDownList runat="server" ID="ConditionTypeFilter" CssClass="ConFilter"></asp:DropDownList>
            </div>
            <asp:GridView runat="server" HeaderStyle-CssClass="GridHeader" ID="ConditionChoiceTable" CssClass="ConditionChoiceTable dontPropagateMouse" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderStyle-Width="30px">
                        <ItemTemplate>
                            <input type="hidden" class="ChoiceId"  value='<%# AspxTools.HtmlString( Eval("Id" ).ToString() )%>'/>
                            <input type="checkbox" class="ConditionChoiceSelected" />
                            <input type="hidden" class="Choice_sLT" value='<%# AspxTools.HtmlString( Eval("sLT" ).ToString() )%>'  /> 
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField ItemStyle-CssClass="CatChoice" HeaderText="Category" DataField="Category"  HeaderStyle-Width="130px" />
                    <asp:BoundField ItemStyle-CssClass="Type" HeaderText="Condition Type" DataField="ConditionType" HeaderStyle-Width="130px" />
                    <asp:TemplateField HeaderText="Condition Subject" >
                        <ItemTemplate>
                            <textarea><%# AspxTools.HtmlString( Eval("ConditionSubject").ToString() )%></textarea>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            </div>
            Insert at position <input type="text"  name="InsertPosition" id="InsertPosition" /> / <span id="ConditionCount"></span>
            <input type="button" id="Insert" value="Insert" /> <input type="button" id="Cancel" value="Cancel" />
        </div>
        <script type="text/javascript" src="../../../inc/json.js"></script>
        <div id="dialog-message"  >
	        <p>
		        Saving...
	        </p>
	    
        </div>



    </form>
</body>
</html>
