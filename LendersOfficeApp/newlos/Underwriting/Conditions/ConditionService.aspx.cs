﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.Task;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using System.Data.SqlClient;
using LendersOffice.ObjLib.DocumentConditionAssociation;
using LendersOffice.Admin;

namespace LendersOfficeApp.newlos.Underwriting.Conditions
{
    public partial class ConditionService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        public enum ConditionChangeAction
        {
            Edit = 0,
            Close = 1,
            Reactivate = 2
        }

        public class ConditionChanges
        {
            public string AssignedUserId { get;  set; }
            public string AssignedRoleId { get; set; }
            public string DueDateField { get;  set; }
            public int? DueDateBuisnessDays { get;  set; }
            
            public int CategoryId { get;  set; }
            public string Subject { get;  set; }
            public string InternalNotes { get;  set; }
            public bool? IsHidden { get;  set; }
            public string TaskId { get;  set; }
            public string Permission { get; set; }
            public bool IsDirty { get; set; }
            public string ResolutionBlockTriggerName { get; set; }
            public string ResolutionDateSetterFieldId { get; set; }
        }

        protected override void Process(string methodName)
        {
            PerformanceStopwatch.ResetAll();
            using (PerformanceStopwatch.Start("ConditionService.Process." + methodName))
            {
                switch (methodName)
                {
                    case "LoadCondition":
                        LoadCondition();
                        break;
                    case "SavePage":                        
                        SavePage();
                        break;
                    case "CloseCondition":
                        CloseCondition();
                        break;
                    case "ReactivateCondition":
                        ReactivateCondition();
                        break;
                    case "SaveCondition":
                        SaveCondition();
                        break;
                    case "CreateConditionFromChoice":
                        CreateConditionsFromChoices();
                        break;
                    case "RestoreConditions":
                        RestoreConditions();
                        break;
                    case "ImportFromTemplate":
                        ImportFromTemplate();
                        break;
                    case "GetAssociationsForCondition":
                        GetAssociationsForCondition();
                        break;
                    case "SaveAssociations":
                        SaveAssociations();
                        break;
                    case "BatchCloseConditions":
                        BatchCloseConditions();
                        break;
                }

                Tools.LogInfo("ConditionServicePerformance\n "  + PerformanceStopwatch.ReportOutput);
            }
        }

        private BrokerUserPrincipal CurrentUser
        {
            get
            {
                return BrokerUserPrincipal.CurrentPrincipal;
            }
        }

        private void SaveAssociations()
        {
            Guid sLId = GetGuid("sLId");
            List<string> docIds = null;
            string taskId = GetString("TaskId");

            docIds = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("DocIds"));
            foreach (string docId in docIds) // TODO: make this a transaction
            {
                var assoc = new DocumentConditionAssociation(CurrentUser.BrokerId, sLId, taskId, new Guid(docId));
                assoc.Save();
            }

            SetResult("OK", "OK");
        }

        private void ImportFromTemplate()
        {
            Guid templateId = GetGuid("TemplateId");
            Guid sLId = GetGuid("sLId");
            
            if (!UserCanCreateTasks)
            {
                SetResult("OK", false);
                return;
            }

            // Save the current sort order prior to adding more conditions.
            var sortOrderTaskIds = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("SortOrder"));
            ConditionOrderManager.StoreUserSort(sLId, sortOrderTaskIds);

            IEnumerable<string> newTaskIds = Task.DuplicateTasksToLoan(templateId, sLId, CurrentUser, true, true);

            var newConditions = new List<LightCondition>();

            var permissionProcessor = new TaskPermissionProcessor(CurrentUser);
            var newTasks = Task.GetTasksByEmployeeAccess(sLId, CurrentUser, newTaskIds, permissionProcessor);
            var dueDateInfo = newTasks.Select(t => new DueDateCalcInfo(t));
            LoanDataForTasks loanData = null;

            if (dueDateInfo.Any(d => !string.IsNullOrEmpty(d.FieldId)))
            {
                loanData = Task.GetRequiredLoanData(sLId, dueDateInfo);
            }

            foreach (var task in newTasks)
            {
                if (loanData != null)
                {
                    task.SetLoanDates(loanData.LoanDateCache);
                }

                newConditions.Add(new LightCondition(CurrentUser, task, Task.CanUserViewHiddenInformation(CurrentUser), permissionProcessor));
            }

            SetResult("NewCondition", ObsoleteSerializationHelper.JavascriptJsonSerialize(newConditions));
            SetResult("OK", true);
        }

        private bool? m_userCanCreateTasks;
        private bool UserCanCreateTasks
        {
            get
            {
                if (m_userCanCreateTasks == null)
                {
                    Guid sLId = GetGuid("sLId");
                    bool canCreate = Task.Create(sLId, CurrentUser.BrokerId).DefaultPermissionForCondition != null;
                    if (canCreate)
                    {
                        m_userCanCreateTasks = false == LoanIsNotWriteable;
                    }
                    else
                    {
                        m_userCanCreateTasks = false;
                    }
                }
                return m_userCanCreateTasks.Value;

            }
        }
        private bool? m_loanIsNotWriteable;
        private bool LoanIsNotWriteable
        {
            get
            {
                if (m_loanIsNotWriteable == null)
                {
                    Guid sLId = GetGuid("sLId");
                    var dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(sLId, typeof(ConditionService));
                    dataLoan.InitLoad();
                    m_loanIsNotWriteable = false == dataLoan.CanWrite;
                }
                return m_loanIsNotWriteable.Value;
            }
        }

        private void RestoreConditions()
        {
            List<string> ids = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("ids"));

            if (ids.Count == 0)
            {
                return;
            }

            //security to do - ensure user can read the loan. 
            Guid sLid = GetGuid("sLId");

            
            if (!UserCanCreateTasks)
            {
                SetResult("OK", false);
                return;
            }

            List<Task> taskToRestore = new List<Task>();
            foreach (string taskid in ids)
            {
                Task t = Task.Retrieve(CurrentUser.BrokerId, taskid);
                if (t.LoanId != sLid)
                {
                    throw CBaseException.GenericException("Security exception, task loan id does not match working on loan.");
                }
                taskToRestore.Add(t);
            }


            try
            {
                foreach (Task t in taskToRestore)
                {
                    t.Restore(false);
                }
            }

            finally
            {
                TaskUtilities.EnqueueTasksDueDateUpdate(CurrentUser.BrokerId, sLid);
                TaskUtilities.BumpLoanConditionSummaryVersionAndEnqueue(CurrentUser.BrokerId, sLid);
            }
        }

        private void SavePage()
        {
            Guid sLId = GetGuid("sLId");
            if (LoanIsNotWriteable)
            {
                SetResult("OK", false);
                SetResult("Error", NeedWriteAccess);
                return;
            }

            List<string> deletionIds = null;
            List<ConditionChanges> changes = null;
            List<string> sortOrderTaskIds = null;
            Dictionary<string, Task> conditionTasks = null;
            BatchTaskFailure errorTracker = new BatchTaskFailure(E_TaskSaveFailureOperation.SAVE_CONDITIONS);

            
            
            deletionIds = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("DeletedConditions"));
            
            changes = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<ConditionChanges>>(GetString("ConditionData"));
            sortOrderTaskIds = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("SortOrder"));

            using (PerformanceStopwatch.Start("LoadConditions"))
            {
                conditionTasks = Task.GetActiveConditionsByLoanId(CurrentUser.BrokerId, sLId,
                    Task.CanUserViewHiddenInformation(BrokerUserPrincipal.CurrentPrincipal)).ToDictionary(p => p.TaskId);
            }

            using (PerformanceStopwatch.Start("StoreSort"))
            {
                ConditionOrderManager.StoreUserSort(sLId, sortOrderTaskIds);
            }

            TaskPermissionProcessor taskPermissions = new TaskPermissionProcessor(CurrentUser);

            using (PerformanceStopwatch.Start("SavePage.ForEachIds"))
            {
                foreach (string id in deletionIds)
                {
                    Task condition;
                    if (false == conditionTasks.TryGetValue(id, out condition))
                    {
                        errorTracker.Add(new TaskFailure()
                        {
                            Id = id,
                            Reason = "Cannot find condition",
                            Subject = ""
                        });
                        Tools.LogError("could not find task : " + id + "  LoginNm: " + BrokerUserPrincipal.CurrentPrincipal.LoginNm);
                        continue;
                    }
                    condition.PermissionProcessor = taskPermissions;
                    using (PerformanceStopwatch.Start("SavePage.ForEachIds.GetPermissionLevelFor"))
                    {
                        if (condition.GetPermissionLevelFor(CurrentUser) == E_UserTaskPermissionLevel.None)
                        {
                            continue; //cant save changes to this one. Manage is required for deletion.
                        }
                    }
                    try
                    {
                        using (PerformanceStopwatch.Start("Delete"))
                        {
                            condition.Delete(CurrentUser.DisplayNameForAuditRecentModification, false);
                        }
                    }
                    catch (CBaseException e)
                    {
                        errorTracker.Add(new TaskFailure()
                        {
                            Id = condition.TaskId,
                            Reason = e.UserMessage,
                            Subject = condition.TaskSubject
                        });
                        Tools.LogError("Failed to delete TaskId: " + id, e);
                    }
                    //we may have saved some other conditions so we hanve to catch all here...
                    catch (Exception e)
                    {
                        errorTracker.Add(new TaskFailure()
                        {
                            Id = condition.TaskId,
                            Reason = ErrorMessages.Generic,
                            Subject = condition.TaskSubject
                        });

                        Tools.LogError("Failed to delete taskId: " + id, e);
                    }
                }
            }

            Dictionary<int, ConditionCategory> categories = ConditionCategory.GetCategories(CurrentUser.BrokerId).ToDictionary(category => category.Id);

            using (PerformanceStopwatch.Start("SavePage.ForEachChange"))
            {
                for (int i = 0; i < changes.Count; i++)
                {
                    ConditionChanges change = changes[i];
                    if (change.IsDirty == false)
                    {
                        continue; //no need to do anything changes did not occur
                    }
                    //else
                    //{
                    //    Tools.LogInfo(change.TaskId + " changed.");
                    //}
                    Task condition;
                    if (false == conditionTasks.TryGetValue(change.TaskId, out condition))
                    {
                        errorTracker.Add(new TaskFailure()
                        {
                            Id = change.TaskId,
                            Reason = "Cannot find condition",
                            Subject = change.Subject
                        });
                        Tools.LogError("could not find task : " + change.TaskId + "  LoginNm: " + BrokerUserPrincipal.CurrentPrincipal.LoginNm);

                        continue;
                    }
                    condition.PermissionProcessor = taskPermissions;
                    using (PerformanceStopwatch.Start("SavePage.ForEachChange.GetPermissionLevelFor"))
                    {
                        if (condition.GetPermissionLevelFor(CurrentUser) == E_UserTaskPermissionLevel.None)
                        {
                            continue; ; //cant save changes to this one.
                        }
                    }
                    try
                    {
                        ApplyChanges(condition, change, categories);
                        using( PerformanceStopwatch.Start("Save"))
                        {
                            condition.Save(false);
                        }
                    }
                    catch (CBaseException e)
                    {
                        errorTracker.Add(new TaskFailure()
                        {
                            Id = condition.TaskId,
                            Reason = e.UserMessage,
                            Subject = condition.TaskSubject
                        });
                        Tools.LogError("Failed to update TaskId: " + change.TaskId, e);
                    }
                    catch (Exception e)
                    {
                        errorTracker.Add(new TaskFailure()
                        {
                            Id = condition.TaskId,
                            Reason = ErrorMessages.Generic,
                            Subject = condition.TaskSubject
                        });
                        Tools.LogError("Failed to update TaskId: " + change.TaskId, e);
                    }
                }
            }

            using (PerformanceStopwatch.Start("EnqueueTasksDueDateUpdate"))
            {
                TaskUtilities.EnqueueTasksDueDateUpdate(CurrentUser.BrokerId, sLId);
            }
            
            TaskUtilities.BumpLoanConditionSummaryVersionAndEnqueue(CurrentUser.BrokerId, sLId);

            if (errorTracker.HasErrors())
            {
                string cacheId = errorTracker.SaveToCache();
                SetResult("ErrorCacheId", cacheId);
            }

            SetResult("OK", true);
            //todo update condition lsat mofiei
        }   

        private void ApplyChanges(Task condition, ConditionChanges newData, Dictionary<int, ConditionCategory> categories)
        {
            using (PerformanceStopwatch.Start("ApplyChanges"))
            {
                //save based on the permission given on load. 
                switch (newData.Permission)
                {
                    case "Manage":
                        if (false == string.IsNullOrEmpty(newData.AssignedUserId))
                        {
                            Guid assignedUserId = new Guid(newData.AssignedUserId);
                            condition.TaskAssignedUserId = assignedUserId;
                        }
                        if (false == string.IsNullOrEmpty(newData.AssignedRoleId))
                        {
                            Guid roleId = new Guid(newData.AssignedRoleId);
                            condition.TaskToBeAssignedRoleId = roleId;
                        }
                        if (newData.DueDateBuisnessDays.HasValue && false == string.IsNullOrEmpty(newData.DueDateField))
                        {
                            condition.TaskDueDateLocked = false;
                            condition.TaskDueDateCalcDays = newData.DueDateBuisnessDays.Value;
                            condition.TaskDueDateCalcField = newData.DueDateField;
                            condition.TaskDueDate = null;
                        }

                        if (condition.CondCategoryId != newData.CategoryId)
                        {
                            condition.CondCategoryId = newData.CategoryId;
                            ConditionCategory category = categories[newData.CategoryId];
                            condition.TaskPermissionLevelId = category.DefaultTaskPermissionLevelId;
                        }
                        condition.TaskSubject = newData.Subject;
                        condition.CondInternalNotes = newData.InternalNotes;

                        bool canViewHiddenInfo = Task.CanUserViewHiddenInformation(CurrentUser);

                        if (canViewHiddenInfo && newData.IsHidden.HasValue)
                        {
                            condition.CondIsHidden = newData.IsHidden.Value;
                        }
                        break;
                    case "WorkOn":
                    case "None":
                        //these users cannot do anything besides change the order.
                        break;
                }
            }
        }

        public class SelectedConditionChoice
        {
            public int Id { get; set; }
            public string Subject { get; set; }

        }

        private const string NeedWriteAccess = "Error: Loan write access is required to perform this operation.";

        private void CreateConditionsFromChoices()
        {
            Guid sLId = GetGuid("sLId");
            
            if (!UserCanCreateTasks)
            {
                SetResult("OK", false);
                return;
            }
            CPageData d = CPageData.CreateUsingSmartDependency(sLId, typeof(ConditionService));
            d.InitLoad();

            Dictionary<int, SelectedConditionChoice> conditionChoicesIds = ObsoleteSerializationHelper.JavascriptJsonDeserializer<SelectedConditionChoice[]>(GetString("ConditionChoices")).ToDictionary(p=>p.Id);
            IEnumerable<ConditionChoice> choices = ConditionChoice.Retrieve(CurrentUser.BrokerId, conditionChoicesIds.Keys);
            TaskConditionChoiceCreationCache batchData = null;

            List<LightCondition> newTasks = new List<LightCondition>(conditionChoicesIds.Count);
            HashSet<string> subjects = Task.GetAllTaskSubjectsFor(CurrentUser.BrokerId, sLId);
            bool hasDupes = false;

            List<int> failures = new List<int>();

            foreach (var choice in choices)
            {
                choice.ConditionSubject = conditionChoicesIds[choice.ConditionChoiceId].Subject;

                if (subjects.Contains(choice.ConditionSubject))
                {
                    hasDupes = true;
                    continue;
                }
                try
                {
                    // Only initialize batch data cache once we know we are creating conditions.
                    if (batchData == null)
                    {
                        batchData = new TaskConditionChoiceCreationCache(CurrentUser, sLId, choices);
                    }

                    Task t = Task.Create(sLId, choice, batchData.LoanData, batchData.PermissionProcessor, batchData.PermissionLevels, batchData.LoanAssignments, batchData.ConditionCategoriesById, batchData.DefaultPermissionLevel);
                    t.Save(false);
                    //may not need to do this.
                    t = Task.Retrieve(BrokerUserPrincipal.CurrentPrincipal.BrokerId, t.TaskId, batchData.LoanData.LoanDateCache, batchData.PermissionProcessor, batchData.ConditionCategoriesById);
                    newTasks.Add(new LightCondition(CurrentUser, t, Task.CanUserViewHiddenInformation(CurrentUser), batchData.PermissionProcessor));
                }
                catch (CBaseException e)
                {
                    Tools.LogError("On task save from condition choice ", e);
                    failures.Add(choice.ConditionChoiceId);
                    continue;//ignore the error 
                }
                catch (SqlException e)
                {
                    Tools.LogError("On task save from condition choice ", e);
                    failures.Add(choice.ConditionChoiceId);
                    continue;//ignore the error 
                }
                
            }
            TaskUtilities.EnqueueTasksDueDateUpdate(BrokerUserPrincipal.CurrentPrincipal.BrokerId, sLId);
            TaskUtilities.BumpLoanConditionSummaryVersionAndEnqueue(BrokerUserPrincipal.CurrentPrincipal.BrokerId, sLId);

            SetResult("NewTasks", ObsoleteSerializationHelper.JavascriptJsonSerialize(newTasks));
            SetResult("Failures", ObsoleteSerializationHelper.JavascriptJsonSerialize(failures));
            SetResult("HasDupes", hasDupes.ToString());
        }

        /// <summary>
        /// Closes the given condition but updates it with the latest changes from the user first.
        /// </summary>
        private void CloseCondition()
        {
            if (LoanIsNotWriteable)
            {
                SetResult("OK", false);
                SetResult("Error", NeedWriteAccess);
                return;
            }
            Task task = UpdateCondition(GetString("ConditionChanges"), ConditionChangeAction.Close);
            task.Close();
            LoadCondition(task.TaskId);

            GetAssociationsForCondition();
        }

        private void BatchCloseConditions()
        {
            if (LoanIsNotWriteable)
            {
                SetResult("OK", false);
                SetResult("Error", NeedWriteAccess);
                return;
            }

            ForEachServiceCondition(delegate(Task task)
            {
                if (task.TaskStatus != E_TaskStatus.Closed)
                {
                    task.Close();
                }
            }, ConditionChangeAction.Close);

        }

        /// <summary>
        /// Reactivates the given condition and updates it with the latest changes from the user first.
        /// </summary>
        private void ReactivateCondition()
        {
            if (LoanIsNotWriteable)
            {
                SetResult("OK", false);
                SetResult("Error", NeedWriteAccess);
                return;
            }
            Task task = UpdateCondition(GetString("ConditionChanges"), ConditionChangeAction.Reactivate);
            task.Reactivate();
            LoadCondition(task.TaskId);

            GetAssociationsForCondition();
        }

                 
        /// <summary>
        /// Retrieves the latest condition from the database and puts its results in xml.
        /// </summary>
        private void LoadCondition()
        {
            string taskId = GetString("TaskId");
            LoadCondition(taskId);
        }

        /// <summary>
        /// Retrieve associations for the specified condition and put it in the result xml.
        /// </summary>
        private void GetAssociationsForCondition()
        {
            string taskId = GetString("TaskId");
            Guid sLId = GetGuid("sLId");
            var associations = DocumentConditionAssociation.GetAssociationsByLoanConditionHeavy(BrokerUserPrincipal.CurrentPrincipal.BrokerId, sLId, taskId);

            SetResult("Associations", ObsoleteSerializationHelper.JavascriptJsonSerialize(associations));
        }

        /// <summary>
        /// Retrieve the latest condition from the database and put it in the result XML.
        /// </summary>
        /// <param name="id"></param>
        private void LoadCondition(string id)
        {
            Task t = Task.Retrieve(BrokerUserPrincipal.CurrentPrincipal.BrokerId, id);

            if (t.TaskIsCondition == false)
            {
                throw CBaseException.GenericException("Somehow loaded invalid condition");
            }

            LightCondition c = new LightCondition(BrokerUserPrincipal.CurrentPrincipal, t, Task.CanUserViewHiddenInformation(CurrentUser), null);
            SetResult("Condition", ObsoleteSerializationHelper.JavascriptJsonSerialize(c));             
        }

        private void SaveCondition()
        {
            if (LoanIsNotWriteable)
            {
                SetResult("OK", false);
                SetResult("Error", NeedWriteAccess);
                return;
            }
            Task task = UpdateCondition(GetString("ConditionChanges"), ConditionChangeAction.Edit);
            task.Save(true);
            LoadCondition(task.TaskId);

        }

        public Task UpdateCondition(string data, ConditionChangeAction action)
        {
            if (LoanIsNotWriteable)
            {
                SetResult("OK", false);
                SetResult("Error", NeedWriteAccess);
                return null;
            }

            ConditionChanges change = ObsoleteSerializationHelper.JavascriptJsonDeserializer<ConditionChanges>(data);
            return UpdateCondition(change, action);
        }

        public Task UpdateCondition(ConditionChanges change, ConditionChangeAction action)
        {
            if (LoanIsNotWriteable)
            {
                SetResult("OK", false);
                SetResult("Error", NeedWriteAccess);
                return null;
            }

            Task task = Task.Retrieve(BrokerUserPrincipal.CurrentPrincipal.BrokerId, change.TaskId);
            if (task.TaskIsCondition == false)
            {
                throw CBaseException.GenericException("Invalid task. This update is only for conditions");
            }

            if ((action == ConditionChangeAction.Close || action == ConditionChangeAction.Reactivate) &&
                !change.IsDirty)
            {
                // We don't want to set anything at all if the task isn't dirty.
                return task;
            }

            if (task.CondCategoryId != change.CategoryId)
            {
                ConditionCategory category = ConditionCategory.GetCategory(CurrentUser.BrokerId, change.CategoryId);
                task.CondCategoryId = change.CategoryId;
                task.TaskPermissionLevelId = category.DefaultTaskPermissionLevelId;                
            }
            task.TaskSubject = change.Subject;
            task.CondInternalNotes = change.InternalNotes;

            if (Task.CanUserViewHiddenInformation(BrokerUserPrincipal.CurrentPrincipal)
                && change.IsHidden.HasValue)
            {
                task.CondIsHidden = change.IsHidden.Value;
            }

            if (false == string.IsNullOrEmpty(change.AssignedUserId))
            {
                task.TaskAssignedUserId = new Guid(change.AssignedUserId);
            }
            if (false == string.IsNullOrEmpty(change.AssignedRoleId))
            {
                task.TaskToBeAssignedRoleId = new Guid(change.AssignedRoleId);     
            }
            if (false == string.IsNullOrEmpty(change.DueDateField) && change.DueDateBuisnessDays.HasValue)
            {
                task.TaskDueDateLocked = false;
                task.TaskDueDateCalcField = change.DueDateField;
                task.TaskDueDateCalcDays = change.DueDateBuisnessDays.Value;
            }

            return task;
        }

        /// <summary>
        /// Performs the given operation on the conditions. DOES NOT AUTOMATICALLY SAVE.
        /// Updates the task with the changes from the UI prior to performing the operation.
        /// Keeps track of errors for displaying in the batch error window.
        /// </summary>
        /// <param name="Operation"></param>
        private void ForEachServiceCondition(Action<Task> Operation, ConditionChangeAction action)
        {
            var sLId = GetGuid("sLId");
            var conditionChanges = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<ConditionChanges>>(GetString("ConditionChanges"));
            var resolutionBlockTriggerNames = conditionChanges
                .Select(conditionChange => conditionChange.ResolutionBlockTriggerName)
                .Where(triggerName => !string.IsNullOrEmpty(triggerName));
            var resolutionDateSetterFieldIds = conditionChanges
                .Select(conditionChange => conditionChange.ResolutionDateSetterFieldId)
                .Where(fieldId => !string.IsNullOrEmpty(fieldId));
            var brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;

            var updatedConditions = new List<LightCondition>();

            var errorTracker = new BatchTaskFailure(E_TaskSaveFailureOperation.SAVE_CONDITIONS);

            ResolutionTriggerProcessor resolutionTriggerProcessor = null;
            ResolutionDateSetter resolutionDateSetter = null;

            if (resolutionBlockTriggerNames.Any())
            {
                resolutionTriggerProcessor = new ResolutionTriggerProcessor(
                    sLId,
                    brokerId,
                    PrincipalFactory.CurrentPrincipal,
                    resolutionBlockTriggerNames);
            }

            if (resolutionDateSetterFieldIds.Any())
            {
                resolutionDateSetter = new ResolutionDateSetter(
                    sLId,
                    PrincipalFactory.CurrentPrincipal);
            }

            foreach (var conditionChange in conditionChanges)
            {
                Task task;
                try
                {
                    task = UpdateCondition(conditionChange, action);
                }
                catch (TaskNotFoundException e)
                {
                    errorTracker.Add(new TaskFailure()
                    {
                        Id = conditionChange.TaskId,
                        Reason = e.UserMessage,
                        Subject = ""
                    });
                    continue;
                }

                // Set these properties to cache the workflow trigger results
                // and avoid saving the loan for every resolution date field.
                task.ResolutionTriggerProcessor = resolutionTriggerProcessor;
                task.ResolutionDateSetter = resolutionDateSetter;

                try
                {
                    Operation(task);

                    // The other methods that deal with condition updating load
                    // a fresh copy of the condition from the database before 
                    // sending it back to the client. However, I don't see use 
                    // of the versioning functionality anywhere on this page or
                    // the pages which use this service, so don't do that yet.
                    // opm 171338 gf
                    var lightCondition = new LightCondition(BrokerUserPrincipal.CurrentPrincipal, task, Task.CanUserViewHiddenInformation(CurrentUser), null);
                    lightCondition.Associations = task.AssociatedDocs;
                    updatedConditions.Add(lightCondition);
                }
                catch (TaskException e)
                {
                    errorTracker.Add(new TaskFailure()
                    {
                        Id = task.TaskId,
                        Reason = e.UserMessage,
                        Subject = task.TaskSubject
                    });
                }
            }
            if (conditionChanges.Count > 0)
            {
                TaskUtilities.EnqueueTasksDueDateUpdate(brokerId, sLId);

                if (resolutionDateSetter != null)
                {
                    resolutionDateSetter.SetFieldsToCurrentDateAndTime();
                }
            }
            if (errorTracker.HasErrors())
            {
                string cacheId = errorTracker.SaveToCache();
                SetResult("ErrorCacheId", cacheId);
                SetResult("TotalFailure", errorTracker.Failures.Count() == conditionChanges.Count);
            }
            if (updatedConditions.Any())
            {
                SetResult("Conditions", ObsoleteSerializationHelper.JavascriptJsonSerialize(updatedConditions));
            }
        }

        /// <summary>
        /// Contains the data required for batch task operations based on condition choices.
        /// </summary>
        private class TaskConditionChoiceCreationCache
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TaskConditionChoiceCreationCache"/> class.
            /// </summary>
            /// <param name="principal">The principal of the user performing the batch operation.</param>
            /// <param name="loanId">The id of the loan.</param>
            /// <param name="conditionChoices">The condition choices that will be used to perform the batch operation.</param>
            public TaskConditionChoiceCreationCache(AbstractUserPrincipal principal, Guid loanId, IEnumerable<ConditionChoice> conditionChoices)
            {
                this.LoanData = Task.GetRequiredLoanData(
                    loanId,
                    conditionChoices.Select(c => new DueDateCalcInfo(c)));
                this.PermissionProcessor = new TaskPermissionProcessor(principal);
                this.PermissionLevels = PermissionLevelData.Retrieve(principal.BrokerId);
                this.LoanAssignments = new LoanAssignmentContactTable(principal.BrokerId, loanId);
                this.DefaultPermissionLevel = Task.GetDefaultPermissionLevel(principal.BrokerId);
                this.ConditionCategoriesById = ConditionCategory.GetCategoriesUnordered(
                    principal.BrokerId,
                    conditionChoices.Select(c => c.CategoryId).Distinct())
                    .ToDictionary(c => c.Id);
            }

            /// <summary>
            /// Gets the loan data that is used when performing batch task operations.
            /// </summary>
            /// <value>The loan data that is used when performing batch task operations.</value>
            public LoanDataForTasks LoanData { get; private set; }

            /// <summary>
            /// Gets the task permission processor.
            /// </summary>
            /// <value>The task permission processor.</value>
            public TaskPermissionProcessor PermissionProcessor { get; private set; }

            /// <summary>
            /// Gets the permission level data.
            /// </summary>
            /// <value>The permission level data.</value>
            public PermissionLevelData PermissionLevels { get; private set; }

            /// <summary>
            /// Gets the loan assignments.
            /// </summary>
            /// <value>The loan assignments.</value>
            public LoanAssignmentContactTable LoanAssignments { get; private set; }

            /// <summary
            /// Gets the default permission level id.
            /// </summary>
            /// <value>The default permission level id.</value>
            public int DefaultPermissionLevel { get; private set; }

            /// <summary>
            /// Gets a map from id to condition category.
            /// </summary>
            /// <value>A map from id to condition category.</value>
            public Dictionary<int, ConditionCategory> ConditionCategoriesById { get; private set; }
        }
    }
}
