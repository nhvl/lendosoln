﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeletedConditions.aspx.cs"
    Inherits="LendersOfficeApp.newlos.Underwriting.Conditions.DeletedConditions" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Deleted Conditions</title>
    <style type="text/css">
        .scroll { 
            overflow: auto;
            height: 400px;
        }
        
        .Full { width: 100%; }
    </style>
</head>
<body style="background-color:gainsboro">
    <h4 class="page-header">Deleted Conditions</h4>
    <form id="form1" runat="server">
            <div class="scroll" >
        <asp:GridView runat="server" HeaderStyle-CssClass="GridHeader" CssClass="Full"  RowStyle-CssClass="GridItem" AlternatingRowStyle-CssClass="GridAlternatingItem" UseAccessibleHeader="true" ID="DeletedConditionsGrid" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField >
                    <HeaderTemplate>
                        <input type="checkbox" class="SelectAll" />
                    </HeaderTemplate>
                    <ItemTemplate >
                        <input type="checkbox" class="Selectable" taskid='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Closed">
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ClosedDate").ToString())%><br />
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ClosedBy").ToString())%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Category" HeaderText="Category" />
                <asp:BoundField DataField="Subject" HeaderText="Condition Subject" />
                <asp:BoundField DataField="IsHidden" HeaderText="Hidden" />
                <asp:TemplateField HeaderText="Deleted By">
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DeletedBy").ToString())%><br />
                        on <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DeletedOn").ToString())%>
                        
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div>
    <br />
    <center>
    <input type="button" id="Restore"  disabled="disabled" value="Restore selected conditions" />
    <input type="button" id="Cancel" value="Cancel" />
    </center>
    </div>
	<uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
    <script type="text/javascript">
        function _init() {
            resizeForIE6And7(600,500);
        }
        
        $(function(){
            $('#Cancel').click(function(){
                onClosePopup();
            });
            
            $('input.SelectAll').click(function(){
                $('input.Selectable').prop('checked', this.checked);
                $('input.Selectable').triggerHandler('click');
            });
            
            $('input.Selectable').click(function(){
                var items = $('input.Selectable:checked');
                $('#Restore').prop('disabled', items.length === 0);
            });
            
            $('#Restore').click(function(){
               var ids = [];
               $('input.Selectable:checked').each(function(){
                    ids.push($(this).attr('taskid'));
               });
               
               if( ids.length === 0 ) {
                alert( 'No Conditions selected.');
                return;
               }
               
               var data = {
                ids : JSON.stringify(ids),
                sLId : ML.sLId
                }
                
                var results = gService.ConditionService.call('RestoreConditions', data);
                if( results.error ) {
                    alert( results.UserMessage); 
                }
                else { 
                    var args = {};
                    args.refresh = true;
                    onClosePopup(args);
                }
            });
        });
    </script>    
    </form>
    
    
    
</body>
</html>
