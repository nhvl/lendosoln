﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.Task;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Security;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using LendersOffice.Admin;
using LendersOffice.ObjLib.DocumentConditionAssociation;
using EDocs;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Underwriting.Conditions
{
    public partial class ConditionSignoff : BaseLoanPage
    {
        // OPM 211800, 8/19/2015, ML
        // Closers will now be able to view the Condition Signoff page without 
        // needing the AllowUnderwritingAccess permission. To support this, we
        // determine the required read/write permissions to have BaseLoanPage 
        // check based on the permissions that the user currently has. If the
        // user has AllowCloserRead, AllowCloserWrite, or AllowUnderwritingAccess,
        // they are authorized and BaseLoanPage does not have to check the permissions
        // again. If not, we return a default of AllowUnderwritingAccess and the check 
        // will fail.
        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                if (BrokerUser.HasPermission(Permission.AllowCloserRead) ||
                    BrokerUser.HasPermission(Permission.AllowUnderwritingAccess))
                {
                    return ConstApp.SkipConditionBasePagePermissionCheck;
                }

                return ConstApp.DefaultConditionPagePermission;
            }
        }

        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                if (BrokerUser.HasPermission(Permission.AllowCloserWrite) ||
                    BrokerUser.HasPermission(Permission.AllowUnderwritingAccess))
                {
                    return ConstApp.SkipConditionBasePagePermissionCheck;
                }

                return ConstApp.DefaultConditionPagePermission;
            }
        }

        public override bool IsReadOnly
        {
            get
            {
                return !(BrokerUser.HasPermission(Permission.AllowCloserWrite) ||
                    BrokerUser.HasPermission(Permission.AllowUnderwritingAccess));
            }
        }

        protected override LendersOffice.Common.E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return LendersOffice.Common.E_XUAComaptibleValue.EmulateIE9;
        }

        private ICollection<ConditionCategory> ManageCategories { get; set; }
        private ICollection<ConditionCategory> CloseCategories { get; set; }
        private ICollection<ConditionCategory> WorkOnCategories { get; set; }
        private ICollection<ConditionCategory> AllCategories { get; set; }

        protected void PageLoad(object sender, EventArgs e)
        {
            // Categories
            ConditionCategoriesWithPermissionInfo categories = new ConditionCategoriesWithPermissionInfo(BrokerUser);
            ManageCategories = categories.ManageCategories;
            AllCategories = categories.AllCategories;
            WorkOnCategories = categories.WorkOnCategories;
            CloseCategories = categories.CloseCategories;

            // TODO: why just ManageCategories?
            var categoriesForJson = (from category in ManageCategories
                                     select new
                                     {
                                         Id = category.Id,
                                         Category = category.Category
                                     }).ToList();


            string categoryJson = ObsoleteSerializationHelper.JavascriptJsonSerialize(categoriesForJson);

            CPageData data = CPageData.CreateUsingSmartDependency(LoanID, typeof(ConditionSignoff));
            data.InitLoad();

            // Get all the associations for this loan
            var associationList = DocumentConditionAssociation.GetAssociationsByLoanHeavy(this.BrokerID, LoanID, BrokerUser);

            // Turn it into a dictionary for easier querying
            var associationsGroupedByTaskId = new Dictionary<string, List<DocumentConditionAssociation>>();
            foreach (var associationFromList in associationList)
            {
                List<DocumentConditionAssociation> associationListFromDict;
                if (associationsGroupedByTaskId.TryGetValue(associationFromList.TaskId, out associationListFromDict))
                {
                    associationListFromDict.Add(associationFromList);
                }
                else
                {
                    var newAssociationList = new List<DocumentConditionAssociation>();
                    newAssociationList.Add(associationFromList);
                    associationsGroupedByTaskId.Add(associationFromList.TaskId, newAssociationList);
                }
            }

            List<LightCondition> conditions = new List<LightCondition>();
            TaskPermissionProcessor processor = new TaskPermissionProcessor(BrokerUser);

            // Gets the conditions in row order
            // For 'GetActiveConditionsByLoanId', 'Active' means 'not deleted' as opposed to 'not closed'.
            foreach (Task task in Task.GetActiveConditionsByLoanId(BrokerUser.BrokerId, LoanID, Task.CanUserViewHiddenInformation(BrokerUser), AllCategories))
            {
                if (processor.Resolve(task) != E_UserTaskPermissionLevel.None)
                {
                    var condition = new LightCondition(BrokerUser, task, Task.CanUserViewHiddenInformation(BrokerUser), processor);

                    // We'll also need to include the associations.
                    List<DocumentConditionAssociation> associationListFromDict;
                    if (associationsGroupedByTaskId.TryGetValue(condition.Id, out associationListFromDict))
                    {
                        condition.Associations = associationListFromDict;
                    }

                    conditions.Add(condition);
                }
            }

            var pageSetupData = new
            {
                Permissions = new
                {
                    CanEditEdocs = BrokerUser.HasPermission(Permission.CanEditEDocs),
                    AllowEditingApprovedEDocs = BrokerUser.HasPermission(Permission.AllowEditingApprovedEDocs)
                },
                Categories = categoriesForJson,
                Conditions = conditions,
                EditorName = BrokerUser.DisplayNameForAuditRecentModification,
                Resources = new
                {
                    UpArrowFile = Page.ResolveUrl("~/images/up-blue.gif"),
                    TopArrowFile = Page.ResolveUrl("~/images/up-blue-top.gif"),
                    DownArrowFile = Page.ResolveUrl("~/images/down-blue.gif"),
                    BottomArrowFile = Page.ResolveUrl("~/images/down-blue-bottom.gif"),
                    GrippyFile = Page.ResolveUrl("~/images/grip.png"),
                    HoverUpArrowFile = Page.ResolveUrl("~/images/up-orange.gif"),
                    HoverTopArrowFile = Page.ResolveUrl("~/images/up-orange-top.gif"),
                    HoverDownArrowFile = Page.ResolveUrl("~/images/down-orange.gif"),
                    HoverBottomArrowFile = Page.ResolveUrl("~/images/down-blue-orange.gif")
                }
            };

            ClientScript.RegisterStartupScript(typeof(ConditionSignoff), "ConditionSetupData", string.Format("var ConditionData = {0};", ObsoleteSerializationHelper.JavascriptJsonSerialize(pageSetupData)), true);

            var conditionSignoffConstants = new
            {
                ConditionStatus = new
                {
                    Active = E_TaskStatus.Active.ToString("G"),
                    Resolved = E_TaskStatus.Resolved.ToString("G"),
                    Closed = E_TaskStatus.Closed.ToString("G"),
                },
                AssociationStatus = new
                {
                    Undefined = E_DocumentConditionAssociationStatus.Undefined,
                    Satisfied = E_DocumentConditionAssociationStatus.Satisfied,
                    Unsatisfied = E_DocumentConditionAssociationStatus.Unsatisfied
                },
                DocumentStatus = new
                {
                    Blank = E_EDocStatus.Blank,
                    Obsolete = E_EDocStatus.Obsolete,
                    Approved = E_EDocStatus.Approved,
                    Rejected = E_EDocStatus.Rejected,
                    Screened = E_EDocStatus.Screened
                }
            };
            ClientScript.RegisterStartupScript(typeof(ConditionSignoff), "ConditionSetupConst", string.Format("var Const = {0};", ObsoleteSerializationHelper.JavascriptJsonSerialize(conditionSignoffConstants)), true);

            if (Broker.IsUseNewTaskSystemStaticConditionIds)
            {
                ShowNumber.Visible = false;
                ShowCondRowId.Visible = true;
            }
            else
            {
                ShowNumber.Visible = true;
                ShowCondRowId.Visible = false;
            }

        }

        private void PageInit(object sender, System.EventArgs e)
        {
            // Set printing framework entries.
            this.PageTitle = "Originator Compensation";
            this.PageID = "OriginatorCompensation";

            RegisterService("ConditionService", "/newlos/Underwriting/Conditions/ConditionService.aspx");
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            EnableJqueryMigrate = false;

            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("jquery.tablesorter.dev.js");
            RegisterJsScript("jquery.tmpl.js");
            RegisterJsScript("Condition/jquery.autogrow-conditionsignoff.js");
            RegisterJsScript("Condition/conditionsignoff.js");
            RegisterJsScript("LQBPopup.js");
            this.Init += new System.EventHandler(this.PageInit);
            this.Load += new System.EventHandler(this.PageLoad);
            base.OnInit(e);
        }

    }
}
