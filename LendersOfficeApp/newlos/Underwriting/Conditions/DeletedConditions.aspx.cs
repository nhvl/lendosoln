﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.Task;

namespace LendersOfficeApp.newlos.Underwriting.Conditions
{
    public partial class DeletedConditions : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (false == IsPostBack)
            {
                bool canViewHiddenInfo = Task.CanUserViewHiddenInformation(BrokerUser);
                Dictionary<int, ConditionCategory> categories = ConditionCategory.GetCategories(BrokerUser.BrokerId).ToDictionary(p => p.Id);
                List<Task> conditions = Task.GetDeletedConditionsByLoanId(BrokerUser.BrokerId, LoanID, canViewHiddenInfo);
                var bindableConditions = (from p in conditions
                                         select new
                                         {
                                             ClosedDate = p.TaskClosedDate.HasValue ? p.TaskClosedDate.Value.ToShortDateString() : "",
                                             ClosedBy = p.ClosingUserFullName,
                                             Category = categories[p.CondCategoryId.Value].Category,
                                             Subject = p.TaskSubject,
                                             IsHidden = p.CondIsHidden_rep,
                                             DeletedBy = p.CondDeletedByUserNm,
                                             DeletedOn = p.CondDeletedDate.Value.ToShortDateString(),
                                             TaskId = p.TaskId
                                         }).ToList();

                
                DeletedConditionsGrid.DataSource = bindableConditions;
                DeletedConditionsGrid.DataBind();

                DeletedConditionsGrid.Columns[4].Visible = canViewHiddenInfo;
                RegisterJsScript("jquery-1.6.min.js");
                RegisterJsScript("json.js");
                RegisterService("ConditionService", "/newlos/Underwriting/Conditions/ConditionService.aspx");
            }
            
        }
    }
}
