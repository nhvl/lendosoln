﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConditionSignoff.aspx.cs" EnableEventValidation="false" EnableViewState="false" Inherits="LendersOfficeApp.newlos.Underwriting.Conditions.ConditionSignoff" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Condition Signoff</title>
    <style type="text/css" runat="server" id="ShowNumber">
        .number { display: inline; }
        .CondRowId { display: none; }
    </style>
    <style type="text/css" runat="server" id="ShowCondRowId">
        .number { display: none; }
        .CondRowId { display: inline; }
    </style>
    <style type="text/css">
        td
        {
            vertical-align: top;
            max-width: 100%; /* Fixes a bug where a column mysteriously appears */
        }
        
        td textarea
        {
        	width: 98%;
        }
        
        body{ background-color: gainsboro; }
        #saveBtn { float: right; margin-right: 10px; }
        .HeaderButton { float: right; margin-right: 10px; }
        
        #conditions
        {
        	margin-top: 3.25em;
        	table-layout:fixed;
        	min-width: 940px;
        }
        
        #fixedHeader 
        {
        	width: 100%;
        	position: fixed;
        	top: 0px;
        	background-color: #999999;
        }

        #conditions thead {
        	background-color: #999999;
        }

        .clear { clear: both; }
    
        .MainRightHeader { 
            overflow: auto;
            width: 100%;
            line-height: 1.5em;
        }
        #HeaderPageTitle { float: left; }
        
        span.headerLink { color: Blue; text-decoration: underline; cursor: pointer; }
        
        .growbox /* Required for autogrow to work */
        {
        	min-height: 60px;
            font-size: 12px;
            font-weight: 400;
            font-family: "Courier New", Courier, monospace;
            overflow: hidden;
        }
        .editbox
        {
        	padding-left: 1px;
        	white-space: pre-wrap;
        	word-wrap:  break-word;
        	word-break: break-word;
        	-ms-word-break : break-all;
            outline: none;
        }
        .growboxcontainer
        {
        	background-color: White;
            border: solid 1px #7F9DB9;
        }
        #dummydiv
        {
        	position:   absolute;
            top:        -10000;
            left:       -10000;
        }
        
        p { margin: 0; } /* for contenteditable */
        
        .associatedDocsLinkCell { text-align: center; }
        
        .associatedDocs 
        {
            min-height: 60px;
        }
        
        .checkboxCell
        {
            padding-left: 4px;
        }
        
        /* Used in conditionsignoff.js */
        .associatedDocsBad
        {
        	color: #A00;
        	font-weight: bold;
        }
        .associatedDocsGood
        {
        	color: #0A0;
        	font-weight: bold;
        }
        .associatedDocsCellGood
        {
        	background: #BFB;
        }
        .associatedDocsCellBad
        {
        	background: #FBB;
        }
        .associatedDocsCellNeutral
        {
        	background: #FFF;
        }
        .associatedDocsCellDisabled
        {
        	color: #999;
        	background: #FFF;
        }
        .associatedDocsLinkAssign { }
        .associatedDocsLinkOpen { }
    </style>
    
</head>
<body>
    <form id="form1" runat="server">
    
        <div id="fixedHeader">
            <div class="MainRightHeader">
                <div id="HeaderPageTitle">Conditions</div>
                <input type="button" value="Save" id="saveBtn" NoHighlight />
                <input type="button" value="Sign off" id="CloseConditions" class="BatchOperationButton HeaderButton" />
                <input type="button" value="Add condition" id="AddCondition" class="HeaderButton" />
            </div>
            <div class="clear"></div>
        </div>
        
        <table id="conditions">
            <colgroup>
                <col width= "3.00%" /> <%-- Number --%>
                <col width= "3.25%" /> <%-- Number --%>
                <col width= "4.40%" /> <%-- Status --%>
                <col width="17.50%" /> <%-- Category --%>
                <col width="22.00%" /> <%-- Condition Subject --%>
                <col width="22.00%" /> <%-- Internal Notes --%>
                <col width="22.00%" /> <%-- Associated Documents --%>
                <col width= "6.00%" /> <%-- Associated Documents Link --%>
            </colgroup>
            <thead>
                <tr>
                    <th class="checkboxHeader">
                        <input type="checkbox" class="SelectAll" />
                    </th>
                    <th class="numberHeader">
                        <span class="headerLink">#</span>
                    </th>
                    <th class="statusHeader">
                        <span class="">Status</span>
                    </th>
                    <th class="categoryHeader">
                        <span class="headerLink">Category</span>
                    </th>
                    <th class="subjectHeader">
                        <span class="headerLink">Condition Subject</span>
                    </th>
                    <th class="internalNotesHeader" >
                        <span class="headerLink">Internal Notes</span>
                    </th>
                    <th class="associatedDocsHeader" >
                        <span class="headerLink">Associated Documents</span>
                    </th>
                    <th class="">
                    </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <script type="text/x-jquery-tmpl" id="condition_template" >
        <td class="checkboxCell">
            <input type="checkbox" class="selectBox" />
        </td>
        <td class="numberCell">
            <span class="number">${number}</span>
            <span class="CondRowId">${CondRowId}</span>
        </td>
        <td class="statusCell">
            <span class="statusText">${Status}</span>
            <a href="#" class="reactivatelink lm">unsign</a> 
            <a href="#" class="closelink lm">sign off</a>
        </td>
        <td class="categoryCell">
            <select class="categories">
            {{if Permission === 'Manage'}} 
            {{each Categories}}
                <option {{if $value.Id === CategoryId}} selected="selected"{{/if}} value=${$value.Id} > 
                    ${$value.Category}
                </option>
            {{/each}}
            {{/if}}
            {{if Permission === 'WorkOn' || Permission === 'Close'}}
                <option value=${CategoryId}>${Category}</option>
            {{/if}}
            </select>
           
        </td>
        <td class="subjectCell growboxcontainer">
            <div contenteditable="true" class="subject growbox editbox">{{html SubjectText}}</div>
        </td>
        <td class="internalNotesCell growboxcontainer">
            <div contenteditable="true" class="internalNotes growbox editbox">{{html InternalNotesText}}</div>
        </td>
        <td class="associatedDocsCell ${AssociatedDocsClasses}">
            <div class="associatedDocs growbox">
                {{html AssociatedDocs}}
            </div>
        </td>
        <td class="associatedDocsLinkCell">
            <a class="${AssociatedDocsLinkClass}" href="#">${AssociatedDocsDescription}</a>
        </td>
        </script>
        
        <br />
        <span id="debug"></span>
    </form>
    <div id="dummydiv"></div>
</body>
</html>
