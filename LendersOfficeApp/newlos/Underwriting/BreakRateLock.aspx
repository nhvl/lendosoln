﻿<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BreakRateLock.aspx.cs" Inherits="LendersOfficeApp.newlos.Underwriting.BreakRateLock" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Break Rate Lock</title>
</head>
<body bgColor=gainsboro margin="0" MS_POSITIONING="FlowLayout" scroll="no">
<script language=javascript>
<!--
function _init() {
  resizeForIE6And7(400, 150);
  if (typeof (Page_ClientValidate) == 'function')
    Page_ClientValidate();
  
  <%= AspxTools.JsGetElementById(tbReason) %>.focus();
}
function f_close(bOk) {
  var arg = window.dialogArguments || {};
  arg.OK = bOk;
    
  var valid = false;
  if (typeof(Page_ClientValidate) == 'function')
		valid = Page_ClientValidate();
  if (!valid)
  {
    arg.OK = false;
    if (bOk)
		return;
  }
  arg.tbReason = <%= AspxTools.JsGetElementById(tbReason) %>.value;
  onClosePopup(arg);
  
}
//-->
</script>

    <h4 class="page-header">Break Rate Lock</h4>
    <form id="BreakRateLock" runat="server">
    <table id="Table1" style="margin: 0px" cellspacing="0" cellpadding="0" width="100%" border="0">
      <tr>
        <td class="FieldLabel" style="padding-left:5px;padding-top:5px">Reason&nbsp;<asp:TextBox ID="tbReason" runat="server" Width="220px"></asp:TextBox>
          <asp:RequiredFieldValidator ID="ReasonValidator" runat="server" ControlToValidate="tbReason" Display="Dynamic">
              <img runat="server" src='../../images/error_icon.gif'/>
          </asp:RequiredFieldValidator>
        </td>
      </tr>
      <tr>
        <td nowrap>&nbsp;</td>
      </tr>
      <tr>
        <td nowrap align="center"><input type="button" value="Break Rate Lock" class="ButtonStyle" style="width: 155px" onclick="f_close(true);" nohighlight>&nbsp; <input type="button" value="Cancel" class="ButtonStyle" style="width: 60px" onclick="f_close(false);" nohighlight></td>
      </tr>
    </table>
    </form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
</body>
</html>
