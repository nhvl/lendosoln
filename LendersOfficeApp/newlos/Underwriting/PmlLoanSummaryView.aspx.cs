using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Xml;
using System.Xml.Xsl;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Reminders;
using LendersOffice.Constants;
using LendersOffice.ObjLib.Resource;

namespace LendersOfficeApp.newlos.Underwriting
{

    public partial class PmlLoanSummaryView : LendersOffice.Common.BaseXsltPage
    {
        private string m_pmlLenderSiteId = "";

        protected override string XsltFileLocation 
        {
            get { return ResourceManager.Instance.GetResourcePath(ResourceType.PmlLoanViewXslt, BrokerID); }
        }
        private Guid LoanID 
        {
            get { return RequestHelper.LoanID; }
        }
        private Guid BrokerID 
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
        }
        private bool DisplayMessageToUnderwriter 
        {
            get { return RequestHelper.GetSafeQueryString("note") == "t"; }
        }
        private bool DisplaySubmissionAgreement 
        {
            get { return RequestHelper.GetSafeQueryString("submission") == "t"; }
        }
        private bool DisplayPricing 
        {
            get { return RequestHelper.GetSafeQueryString("pricing") == "t"; }
        }
        private bool DisplayUnderwritingSignature
        {
            get { return RequestHelper.GetSafeQueryString("signature") == "t"; }
        }
        protected override XsltArgumentList XsltParams 
        {
            get 
            {
                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("VirtualRoot", "", Tools.VRoot);
                args.AddParam("LenderPmlSiteId", "", m_pmlLenderSiteId);
                args.AddParam("DisplayMessageToUnderwriter", "", DisplayMessageToUnderwriter.ToString());
                args.AddParam("DisplaySubmissionAgreement", "", DisplaySubmissionAgreement.ToString());
                args.AddParam("DisplayPricing", "", DisplayPricing.ToString());
                args.AddParam("DisplayUnderwritingSignature", "", DisplayUnderwritingSignature.ToString());
                return args;  
            }
        }

        protected override void InitXslt() 
        {
            // 11/10/2004 dd - Theoretically, PmlLenderSiteId should be in loan product.
            // This way we allow independent broker to run qualification against multiple lender.
            // However we are not there yet. Retrieve from current broker.

            Guid brokerPmlSiteId =BrokerUserPrincipal.CurrentPrincipal.BrokerDB.PmlSiteID;
            m_pmlLenderSiteId = Tools.GetFileDBKeyForPmlLogoWithLoanId(brokerPmlSiteId, LoanID);

        }
        protected override void GenerateXmlData(XmlWriter writer) 
        {
            PmlLoanSummaryXmlData.Generate(writer, LoanID);
        }
	}
}
