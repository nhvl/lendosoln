namespace LendersOfficeApp.newlos.Underwriting
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.CustomPmlFields;
    using Forms;

    public partial class PmlReviewInfo : BaseLoanPage
	{
        protected TextBox sUnitsNum;

        protected bool IsEnableBigLoanPage
        {
            get
            {
                return Broker.IsEnableBigLoanPage;
            }
        }

        protected bool EnableRenovationUi => this.Broker.IsEnableRenovationCheckboxInPML || this.Broker.EnableRenovationLoanSupport;

        // mf OPM 4442. We need these guys on the PML Summary


        protected bool m_IsRateLocked = false;
		protected bool m_IsStandAloneSecond = false;
        protected bool m_renderAsSecond = false;
        protected bool m_isAutoPmiOrNewPml = false;
        protected E_sLtv80TestResultT sLtv80TestResultT;

        internal static void BindDataFromControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem item)
        {
            QualRateCalculationPopup.BindData(loan, item);
        }

        internal static void LoadDataForControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem item)
        {
            QualRateCalculationPopup.LoadData(loan, item);
        }

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(PmlReviewInfo));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(0);

            this.RegisterJsGlobalVariables("IsTargeting2019Ulad", dataLoan.sIsTargeting2019Ulad);

            this.QualRatePopup.LoadData(dataLoan);

            m_isAutoPmiOrNewPml = Broker.HasEnabledPMI || dataLoan.IsNewPMLEnabled;
			m_IsStandAloneSecond = dataLoan.sIsStandAlone2ndLien;
            sLtv80TestResultT = dataLoan.sLtv80TestResultT;

            var borrCitizenshipValues = dataApp.GetApplicableCitizenshipValues(E_BorrowerModeT.Borrower);
            Tools.Bind_aProdCitizenT(aProdBCitizenT, borrCitizenshipValues);

            Tools.Bind_sProdDocT(sProdDocT, this.Broker, dataLoan.sProdDocT);

            // 08/06/07 Technically it does not matter if we set these or not.
            // They will be unrendered if we are a second.

            aBNm.Text                              = dataApp.aBNm;
            aBSsn.Text                             = dataApp.aBSsn;
            aCNm.Text                              = dataApp.aCNm;
            aCSsn.Text                             = dataApp.aCSsn;
            sSpState.Value                         = dataLoan.sSpState;
            sOccT.Text                             = GetPropertyPurposeDescription(dataLoan.sOccT);
            sProdCondoStories.Text                 = dataLoan.sProdCondoStories_rep;
            sProdIsTexas50a6Loan.Checked           = dataLoan.sProdIsTexas50a6Loan;
            sPreviousLoanIsTexas50a6Loan.Checked   = dataLoan.sPreviousLoanIsTexas50a6Loan;
            sProdCashoutAmt.Text                   = dataLoan.sProdCashoutAmt_rep;
            if (this.Broker.IsEnableRenovationCheckboxInPML || this.Broker.EnableRenovationLoanSupport)
            {
                sIsRenovationLoan.Checked = dataLoan.sIsRenovationLoan;
                sTotalRenovationCostsLckd.Checked = dataLoan.sTotalRenovationCostsLckd;
                sTotalRenovationCosts.Text = dataLoan.sTotalRenovationCosts_rep;
            }
            sProdImpound.Checked                   = dataLoan.sProdImpound;
            sProdRLckdDays.Text                    = dataLoan.sProdRLckdDays_rep;
            LoadLockPeriodOptionsAndSetTextboxValue(dataLoan);
            sIsIOnly.Text                          = dataLoan.sIsIOnly ? "Yes" : "No";
            sHouseVal.Text                         = dataLoan.sHouseVal_rep;
            sDownPmtPc.Text                        = dataLoan.sDownPmtPc_rep;
            sEquityCalc.Text                       = dataLoan.sEquityCalc_rep;
            sLtvR.Text                             = dataLoan.sLtvR_rep;
            sLAmtCalc.Text                         = dataLoan.sLAmtCalc_rep;
            sCltvR.Text                            = dataLoan.sCltvR_rep;
            sLtvROtherFin.Text                     = dataLoan.sLtvROtherFin_rep;
            sProOFinBal.Text                       = dataLoan.sProOFinBal_rep;
            aBExperianScore.Text                   = dataApp.aBExperianScore_rep;
            aBTransUnionScore.Text                 = dataApp.aBTransUnionScore_rep;
            aBEquifaxScore.Text                    = dataApp.aBEquifaxScore_rep;
            aCTransUnionScore.Text                 = dataApp.aCTransUnionScore_rep;
            aCEquifaxScore.Text                    = dataApp.aCEquifaxScore_rep;
            aCExperianScore.Text                   = dataApp.aCExperianScore_rep;
            sIsSelfEmployed.Checked                = dataLoan.sIsSelfEmployed;
            aIsBorrSpousePrimaryWageEarner.Checked = dataApp.aIsBorrSpousePrimaryWageEarner;
            sPrimAppTotNonspI.Text                 = dataLoan.sPrimAppTotNonspI_rep;
            sOpNegCf.Text                          = dataLoan.sOpNegCf_rep;
            sOccRLckd.Checked = dataLoan.sOccRLckd;
            sOccR.Text                             = dataLoan.sOccR_rep;
            sSpGrossRent.Text                      = dataLoan.sSpGrossRent_rep;
            sPresLTotPersistentHExp.Text           = dataLoan.sPresLTotPersistentHExp_rep;
            sLpTemplateNm.Text                     = dataLoan.sLpTemplateNm;
            this.sLpProductType.Text               = dataLoan.sLpProductType;

            if (!this.Broker.ShowLoanProductIdentifier)
            {
                this.sLoanProductIdentifierRow.Visible = false;
            }
            else
            {
                this.sLoanProductIdentifier.Text = dataLoan.sLoanProductIdentifier;
            }

            //OPM 47246 Update PML with Anti-flip guidelines and pricing av 10/13/2011
            sPriorSalesPrice.Text = dataLoan.sPriorSalesPrice_rep;
            sPriorSalesD.Text = dataLoan.sPriorSalesD_rep;
            Tools.SetDropDownListValue(sPriorSalesPropertySellerT, dataLoan.sPriorSalesPropertySellerT);

			// 6/05/07 mf. OPM 12061. There is now a corporate setting for editing
			// the loan program name.
            if (! BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowEditingLoanProgramName))
				sLpTemplateNm.ReadOnly = true;

            sProRealETx.Text                       = dataLoan.sProRealETx_rep;
            sProOHExp.Text                         = dataLoan.sProOHExp_rep;
            sTransmOMonPmt.Text                    = dataLoan.sTransmOMonPmt_rep;
            sProdIsSpInRuralArea.Checked           = dataLoan.sProdIsSpInRuralArea;
            sProdIsCondotel.Checked                = dataLoan.sProdIsCondotel;
            sProdIsNonwarrantableProj.Checked      = dataLoan.sProdIsNonwarrantableProj;
            sHas1stTimeBuyer.Checked               = dataLoan.sHas1stTimeBuyer;
            sNumFinancedProperties.Text            = dataLoan.sNumFinancedProperties_rep;
            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpZip.Text = dataLoan.sSpZip;
			// 01-14-08 av 18913 
			Tools.Bind_sSpCounty( dataLoan.sSpState, sSpCounty, true) ; 
			Tools.SetDropDownListCaseInsensitive( sSpCounty, dataLoan.sSpCounty );

            Tools.SetDropDownListValue(this.sHomeIsMhAdvantageTri, dataLoan.sHomeIsMhAdvantageTri);

            sQualIR.Text = dataLoan.sQualIR_rep;
            this.sQualIRLckd.Checked = dataLoan.sQualIRLckd;
            sIsQRateIOnly.Checked = dataLoan.sIsQRateIOnly;
			sIsOptionArm.Checked = dataLoan.sIsOptionArm;
			sIsOptionArm.Enabled = ! (dataLoan.sIsRateLocked || IsReadOnly);
            sIOnlyMon.Text = dataLoan.sIOnlyMon_rep;
			sNoteIR.Text = dataLoan.sNoteIR_rep;
			sOptionArmTeaserR.Text = dataLoan.sIsOptionArm ? dataLoan.sOptionArmTeaserR_rep : string.Empty;
            sOptionArmTeaserR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
			sNoteIR.ReadOnly = IsReadOnly || dataLoan.sIsRateLocked;

            Tools.SetDropDownListValue(sProdDocT, dataLoan.sProdDocT);
            Tools.SetDropDownListValue(aProdBCitizenT, dataApp.aProdBCitizenT);
            Tools.SetDropDownListValue(sProdSpT, dataLoan.sProdSpT);
            this.sIsNotPermanentlyAffixed.Checked = dataLoan.sIsNotPermanentlyAffixed;
            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
			sFinMethT.Enabled = ! ( dataLoan.sIsRateLocked || IsReadOnly );

            sIsStudentLoanCashoutRefi.Checked = dataLoan.sIsStudentLoanCashoutRefi;

            Tools.SetDropDownListValue(sProdSpStructureT, dataLoan.sProdSpStructureT);
            Tools.SetDropDownListValue(sProdAvailReserveMonths, dataLoan.sProdAvailReserveMonths_rep);

            Tools.SetDropDownListValue(sProdConvMIOptionT, dataLoan.sProdConvMIOptionT);
            Tools.SetDropDownListValue(sProdMIOptionT, dataLoan.sProdMIOptionT);
            Tools.SetDropDownListValue(sConvSplitMIRT, dataLoan.sConvSplitMIRT);

			sProdHasHousingHistory.Checked = dataLoan.sProdHasHousingHistory;

			sProdEstimatedResidualI.Text = dataLoan.sProdEstimatedResidualI_rep;

            if (Broker.IsAsk3rdPartyUwResultInPml) 
            {
                Tools.SetDropDownListValue(sProd3rdPartyUwResultT, dataLoan.sProd3rdPartyUwResultT);
                sProdIncludeMyCommunityProc.Checked = dataLoan.sProdIncludeMyCommunityProc;
                sProdIncludeHomePossibleProc.Checked = dataLoan.sProdIncludeHomePossibleProc;
                sProdIncludeNormalProc.Checked = dataLoan.sProdIncludeNormalProc;
                sProdIncludeFHATotalProc.Checked = dataLoan.sProdIncludeFHATotalProc;
                sProdIncludeVAProc.Checked = dataLoan.sProdIncludeVAProc;
                sProdIncludeUSDARuralProc.Checked = dataLoan.sProdIncludeUSDARuralProc;
                sProdIsDuRefiPlus.Checked = dataLoan.sProdIsDuRefiPlus;

            }
			m_IsRateLocked = dataLoan.sIsRateLocked;
			
			sLpIsNegAmortOtherLien.Checked = dataLoan.sLpIsNegAmortOtherLien;
			Tools.SetDropDownListValue(sOtherLFinMethT, dataLoan.sOtherLFinMethT);
			sProOFinPmt.Text = dataLoan.sProOFinPmt_rep;


            OtherInfoPanel.Visible = Broker.IsLenderFeeBuyoutEnabledForBranchChannel(dataLoan.sBranchChannelT); 
            Tools.SetDropDownListValue(sLenderFeeBuyoutRequestedT, dataLoan.sLenderFeeBuyoutRequestedT);

            Tools.SetDropDownListValue(this.sQualTermCalculationType, dataLoan.sQualTermCalculationType);
            this.sQualTerm.Value = dataLoan.sQualTerm_rep;
            
            if (dataLoan.sHasPmlSummaryPdf)
            {
                // be careful as this is a pass through literal.
                var pdf = new LendersOffice.Pdf.CPmlSummaryPDF();
                storedPMLSummaryInfo.Text = "Approval issued " + dataLoan.sPmlSummaryPdfLastSavedDFriendlyDisplay;
                string link = string.Format(
                    "{0}/pdf/{1}.aspx?loanid={2}&crack={3}",
                    VirtualRoot,
                    pdf.Name,
                    LoanID,
                    DateTime.Now);

                storedPMLSummaryInfo.Text += $"(<a onclick=\"LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload('{link}');\">view</a>)";
                // (view) links to the saved certificate PDF
            }

            m_renderAsSecond = dataLoan.sLienPosT == E_sLienPosT.Second;

            PopulateCustomPmlFieldTable(dataLoan);

            this.IsDisableAutoCalcOfTexas50a6.Value = Broker.IsDisableAutoCalcOfTexas50a6.ToString();

            this.RegisterJsGlobalVariables("isLoanTypeFHA", dataLoan.sLT == E_sLT.FHA);
            this.RegisterJsGlobalVariables("IsEnableRenovationCheckboxInPML", this.Broker.IsEnableRenovationCheckboxInPML);
            this.RegisterJsGlobalVariables("EnableRenovationLoanSupport", this.Broker.EnableRenovationLoanSupport);
        }
        private void LoadLockPeriodOptionsAndSetTextboxValue(CPageData pageData)
        {
            // get the rate lock option periods
            // adds them to the drop down
            // and "migrates" the old value to one of the drop down values, giving it the smallest larger value.
            if (Broker.IsEnabledLockPeriodDropDown)
            {
                string[] options = Broker.AvailableLockPeriodOptionsCSV.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                var numericOptions = from s in options
                                     select UInt32.Parse(s);
                var sortedOptions = from n in numericOptions
                                    orderby n
                                    select n;

                uint sProdRLckdDaysValue = (uint)pageData.sProdRLckdDays;
                sProdRLckdDaysDDL.Items.Clear();
                sProdRLckdDaysDDL.Items.Add(new ListItem("None", "0"));
                int index = 1;
                sProdRLckdDaysDDL.SelectedIndex = 0;
                sProdRLckdDays.Text = "0";
                bool isSelected = (sProdRLckdDaysValue == 0);
                foreach (uint option in sortedOptions)
                {
                    sProdRLckdDaysDDL.Items.Add(new ListItem(option.ToString(), option.ToString()));
                    if (!isSelected && option >= sProdRLckdDaysValue)
                    {
                        sProdRLckdDaysDDL.SelectedIndex = index;
                        sProdRLckdDays.Text = sProdRLckdDaysDDL.SelectedValue;
                        isSelected = true;
                    }
                    index++;
                }
                sProdRLckdDays.CssClass += " hidden";
            }
        }

        private void PopulateCustomPmlFieldTable(CPageData dataLoan)
        {
            var list = Broker.CustomPmlFieldList;

            foreach (CustomPmlField field in list.FieldsSortedByRank)
            {
                if (!field.IsValid) continue;

                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell labelCell = new HtmlTableCell();
                labelCell.Attributes["class"] = "FieldLabel CustomPmlFieldLabel";
                HtmlTableCell fieldCell = new HtmlTableCell();

                Literal label = new Literal();
                label.Text = field.Description;
                labelCell.Controls.Add(label);

                WebControl control = CustomPmlFieldUtilities.GetControlForFieldType(field.Type);
                control.ID = String.Format("sCustomPMLField{0}", field.KeywordNum);
                CustomPmlFieldUtilities.SetControlAttributesForFieldType(control, field.Type);

                bool isDDL = control is DropDownList; 
                if (isDDL)
                {
                    CustomPmlFieldUtilities.BindCustomFieldDDL(control as DropDownList, field);
                }

                string fieldValue = dataLoan.GetCustomPMLFieldRep(field.KeywordNum);
                if (field.DisplayFieldForLoanPurpose(dataLoan.sLPurposeT))
                {
                    CustomPmlFieldUtilities.SetControlValue(control, fieldValue);
                }
                else
                {
                    if (isDDL)
                    {
                        control.Enabled = false;
                    }
                    else if (control is TextBox)
                    {
                        ((TextBox)control).ReadOnly = true;
                    }
                }

                fieldCell.Controls.Add(control);

                row.Controls.Add(labelCell);
                row.Controls.Add(fieldCell);
                CustomPMLFieldsTable.Rows.Add(row);
            }
        }

        protected override void SaveData() 
        {
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
        }
        #region Mapper methods


        protected string GetPropertyPurposeDescription(E_sOccT sOccT) 
        {
            switch (sOccT) 
            {
                case E_sOccT.Investment: return "Investment"; 
                case E_sOccT.PrimaryResidence: return "Primary Residence"; 
                case E_sOccT.SecondaryResidence: return "Secondary Residence"; 
            }
            return "";
        }

        #endregion
        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "Price My Loan Review Info";
            this.PageID = "PmlReviewInfo";

            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterCSS("jquery-ui-1.11.custom.css");

            Tools.Bind_sProdSpT(sProdSpT);
            Tools.Bind_sProdSpStructureT(sProdSpStructureT);
            Tools.Bind_sFinMethT(sFinMethT);
            Tools.Bind_sLPurposeT(sLPurposeT);
            Tools.Bind_sProdAvailReserveMonths(sProdAvailReserveMonths);
			Tools.Bind_sProdMIOptionT(sProdMIOptionT);
            Tools.Bind_sProdConvMIOptionT(sProdConvMIOptionT, Broker.EnabledBPMISinglePremium_Case115836, Broker.EnableBPMISplitPremium_Case115836);
            Tools.Bind_sConvSplitMIRT(sConvSplitMIRT);
			Tools.Bind_sFinMethT(sOtherLFinMethT);
            Tools.Bind_sPriorSalesPropertySeller(sPriorSalesPropertySellerT);
            Tools.Bind_sLenderFeeBuyoutRequestedT(sLenderFeeBuyoutRequestedT);
            Tools.Bind_QualTermCalculationType(sQualTermCalculationType);
            Tools.Bind_TriState(this.sHomeIsMhAdvantageTri);

            sSpZip.SmartZipcode(sSpCity, sSpState, sSpCounty);
            if (Broker.IsAsk3rdPartyUwResultInPml) 
            {
                Tools.Bind_sProd3rdPartyUwResultT(sProd3rdPartyUwResultT);
            }

			 // 01-30-08 av 18913
			sSpState.Attributes.Add( "onchange", "javascript:UpdateCounties(this,document.getElementById('" + sSpCounty.ClientID + "'), event);" );

            if (!Broker.PmlRequireEstResidualIncome)
            {
                this.EstimatedResidualIncomeRow.Style.Add("display", "none");
            }

            this.PDFPrintClass = typeof(LendersOffice.Pdf.CPmlSummaryPDF);

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
