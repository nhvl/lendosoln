﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CommunityLending.aspx.cs" Inherits="LendersOfficeApp.newlos.Underwriting.CommunityLending" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
        <table cellSpacing="0" cellPadding="0" width="100%">
			<tr>
				<td class="MainRightHeader" noWrap>
				    Community Lending
				</td>
			</tr>
			<tr>
				<td>
				    <table class="FieldLabel">
			            <tr>
                            <td class="FormTableSubHeader" colspan="6">
                                Homeownership Counseling
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                Borrower
                            </td>
                            <td colspan="3">
                                Coborrower
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                Counseling Source <asp:DropDownList ID="aBHomeOwnershipCounselingSourceT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="3">
                                Counseling Source <asp:DropDownList ID="aCHomeOwnershipCounselingSourceT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                Counseling Format <asp:DropDownList ID="aBHomeOwnershipCounselingFormatT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="3">
                                Counseling Format <asp:DropDownList ID="aCHomeOwnershipCounselingFormatT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                        </tr>
			            <tr>
                            <td class="FormTableSubHeader" colspan="6">
                                Down Payment(s)
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Source
                            </td>
                            <td colspan="2">
                                Type
                            </td>
                            <td colspan="2">
                                Amount
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:DropDownList ID="sCommunityLendingDownPaymentSource1T" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="sCommunityLendingDownPayment1T" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="2">
			                    <ml:moneytextbox id="sCommunityLendingDownPaymentSource1Amt" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:DropDownList ID="sCommunityLendingDownPaymentSource2T" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="sCommunityLendingDownPayment2T" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="2">
			                    <ml:moneytextbox id="sCommunityLendingDownPaymentSource2Amt" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:DropDownList ID="sCommunityLendingDownPaymentSource3T" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="sCommunityLendingDownPayment3T" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="2">
			                    <ml:moneytextbox id="sCommunityLendingDownPaymentSource3Amt" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:DropDownList ID="sCommunityLendingDownPaymentSource4T" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="sCommunityLendingDownPayment4T" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="2">
			                    <ml:moneytextbox id="sCommunityLendingDownPaymentSource4Amt" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox>
                            </td>
                        </tr>
			            <tr>
                            <td class="FormTableSubHeader" colspan="6">
                                Closing Costs
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Source
                            </td>
                            <td colspan="2">
                                Type
                            </td>
                            <td colspan="2">
                                Amount
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:DropDownList ID="sCommunityLendingClosingCostSource1T" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="sCommunityLendingClosingCost1T" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="2">
			                    <ml:moneytextbox id="sCommunityLendingClosingCost1Amt" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:DropDownList ID="sCommunityLendingClosingCostSource2T" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="sCommunityLendingClosingCost2T" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="2">
			                    <ml:moneytextbox id="sCommunityLendingClosingCost2Amt" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:DropDownList ID="sCommunityLendingClosingCostSource3T" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="sCommunityLendingClosingCost3T" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="2">
			                    <ml:moneytextbox id="sCommunityLendingClosingCost3Amt" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:DropDownList ID="sCommunityLendingClosingCostSource4T" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="sCommunityLendingClosingCost4T" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                            <td colspan="2">
			                    <ml:moneytextbox id="sCommunityLendingClosingCost4Amt" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox>
                            </td>
                        </tr>
				    </table>
				</td>
			</tr>
		</table>
    </form>
</body>
</html>
