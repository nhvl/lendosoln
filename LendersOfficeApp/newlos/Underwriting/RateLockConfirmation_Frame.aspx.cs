using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Underwriting
{

	public partial class RateLockConfirmation_Frame : LendersOfficeApp.newlos.BaseLoanPage
	{
    
		protected void PageLoad(object sender, System.EventArgs e)
		{
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(RateLockConfirmation_Frame));
            dataLoan.InitLoad();
		}

        protected void PageInit(object sender, System.EventArgs e) 
        {
            // Register the service the old way since we are reusing a single service page
            this.RegisterService("main", "/newlos/Underwriting/PmlLoanSummaryView_FrameService.aspx");
            this.RegisterJsScript("LQBPrintFix.js");
			EnableJqueryMigrate = false;
			this.RegisterJsScript("LQBPopup.js");	
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
