<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="RateLockConfirmation.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Underwriting.RateLockConfirmation" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>Rate Lock Confirmation</title>
  </HEAD>
<body bgColor=gainsboro margin="0" MS_POSITIONING="FlowLayout" scroll="no">
<script language=javascript>
<!--
function _init() {
  resizeForIE6And7(400, 250);

  document.getElementById("sRLckdD_R").style.display = "none";

  var arg = getModalArgs() || {};
  <%= AspxTools.JsGetElementById(sRLckdD) %>.value = f_toDtString(new Date());
  <%= AspxTools.JsGetElementById(sRLckdDays) %>.value = arg.sRLckdDays;
  f_onRateLockChange();

}
function f_close(bOk) {
  var arg = window.dialogArguments || {};
  arg.OK = bOk;

  if (bOk === false)
  {
    onClosePopup(arg);
  }

  var valid = false;
  if (typeof(Page_ClientValidate) == 'function')
  {
      valid = Page_ClientValidate();
  }

  var lockDate = <%= AspxTools.JsGetElementById(sRLckdD) %>.value;

  if (isNaN(Date.parse(lockDate)))
  {
      valid = false;
      document.getElementById("sRLckdD_R").style.display = "inline";
  }
  else
  {
      document.getElementById("sRLckdD_R").style.display = "none";
  }

  if (!valid)
  {
    arg.OK = false;
    return;
  }

  f_onRateLockChange();
  arg.sRLckdDays = <%= AspxTools.JsGetElementById(sRLckdDays) %>.value;
  arg.sRLckdD = lockDate;
  arg.sRLckExpiration = document.getElementById('sRLckdExpiredD').value;
  arg.tbReason = <%= AspxTools.JsGetElementById(tbReason) %>.value;
  arg.emailCert = <%= AspxTools.JsGetElementById(emailLockCert) %>.checked;
  onClosePopup(arg);

}
function f_toDtString(dt) {
  return (dt.getMonth() + 1) + '/' + dt.getDate() + '/' + dt.getFullYear();
}
function doAfterDateFormat(o) {
  f_onRateLockChange();
}

function f_validate(source, args)
{
  var days = <%= AspxTools.JsGetElementById(sRLckdDays) %>;
  if (days.value.match(/^\d+$/) && days.value.length != 0)
	args.IsValid = true;
  else
  {
    args.IsValid = false;
    days.focus();
  }
}

function f_onRateLockChange() {
  if (typeof(Page_ClientValidate) == 'function')
		Page_ClientValidate();

  var sRLckdDays = 0;
  <%-- //fs opm 21179 06/06/08 --%>
  var sRLckdDaysText = <%= AspxTools.JsGetElementById(sRLckdDays) %>.value;

  try {
    sRLckdDays = parseInt(sRLckdDaysText);
    if (isNaN(sRLckdDays))
         sRLckdDays = 0;
  } catch (e) {}

  var args = new Object();
  args["loanid"] = document.getElementById('loanid').value;
  args["sRLckdD"] = <%= AspxTools.JsGetElementById(sRLckdD) %>.value;
  args["sRLckdDays"] = sRLckdDays;
  var result = gService.service.call("CalculateExpiration", args);

  if (!result.error)
    <%= AspxTools.JsGetElementById(sRLckdExpiredD) %>.value = result.value.sRLckdExpiredD;
}
//-->
</script>
<h4 class="page-header">Lock Approved Rate</h4>
<form id=RateLockConfirmation method=post runat="server" onreset="return false;">
<TABLE id=Table1 style="MARGIN: 0px" cellSpacing=0 cellPadding=0 width="100%" border=0>
  <tr>
    <td class="FieldLabel" nowrap style="padding-left: 5px;padding-top: 5px">
      <table id="Table2" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td class="FieldLabel" nowrap>Rate Lock Date</td>
            <td>
                <ml:DateTextBox ID="sRLckdD" runat="server" Width="75" preset="date" onchange="f_onRateLockChange();"></ml:DateTextBox>
                <img alt="Required" src="../../images/error_icon.gif" id="sRLckdD_R" />
            </td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap>Lock Period</td>
          <td nowrap class="FieldLabel">
            <asp:TextBox ID="sRLckdDays" MaxLength="4" runat="server" Width="40px" onchange="f_onRateLockChange();"></asp:TextBox>
            days&nbsp;<asp:CustomValidator ID="DaysFieldValidator" runat="server" ClientValidationFunction="f_validate" Display="Dynamic">
                <span runat="server">
                    <img src='../../images/error_icon.gif'> Only numbers are allowed.
                </span></asp:CustomValidator></td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap>Rate Lock Expiration Date&nbsp;</td>
          <td nowrap>
            <asp:TextBox ID="sRLckdExpiredD" runat="server" Width="75px" ReadOnly="True"></asp:TextBox></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td class="FieldLabel" colspan="2">Reason&nbsp;<asp:TextBox ID="tbReason" runat="server" Width="220px"></asp:TextBox></td>
        </tr>
        <tr>
          <td colspan="2" class="FieldLabel"><asp:CheckBox ID="emailLockCert" Checked="true" runat="server" Text="View and email the Rate Lock Confirmation after locking the rate" /> </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td nowrap align="center">
      <input type="button" value="Lock Rate" class="ButtonStyle" style="width: 105px" onclick="f_close(true);" nohighlight>&nbsp;
      <input type="button" value="Cancel" class="ButtonStyle" onclick="f_close(false);" nohighlight>
    </td>
  </tr>
</table>
<uc1:cModalDlg id=CModalDlg1 runat="server" />
</FORM>

  </body>
</HTML>
