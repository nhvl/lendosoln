﻿<%@ Page language="c#" Codebehind="TaskConditions.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Underwriting.TaskConditions" ValidateRequest="true" EnableEventValidation="false"%>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<HTML>
  <HEAD runat="server">
		<title>Task Conditions</title>
		<style type="text/css" >
			    .hidden { display: none } 
			    .warning { width: 100%; text-align: center; font-size: 100%; font-weight: bold; background-color: yellow;  border: 1px solid black} 
                a.moveArrow { text-decoration: none;  outline:0;}
                a.moveArrow img { border: 0; width: 10px; height: 10px; }
                .Bottom { text-decoration: underline; }
                .vcenter {  vertical-align: middle; }
			    .Count { width: 20px; }
			    .dragMe { cursor: move; }
			    .dragMe  * { cursor: default; }
			    .defaultc { cursor: default;}
			    a:hover, a:active, a:focus { outline: 0; border: 0; text-decoration: none;}
			    .force { height: 65px; line-height: 16px; background-color: #003366;  }
			    .GridSortHandle { background-image: url('../../images/grip.png'); background-repeat:no-repeat; }
			    #Save { float: right; margin-right: 10px; }
			    #saveMsg { }
			    #saveMsg { display: none; margin-right: 1em; }
			    .Title { float: left; }
                #conTable { margin-top: 2px; }
		</style>
		
  </HEAD>
	<body class="RightBackground" MS_POSITIONING="FlowLayout" style="overflow-y: auto" onbeforeunload ="return dirtyCheck();">
			<script type="text/javascript">
			
			        var isDoingPostBack = false;
			        jQuery(document).ready(function() {
			        
			        isDoingPostBack = false;
			        $('.PostBackButton').click(function() {
			            isDoingPostBack = true;
			        });
			        var header = $('.MainRightHeader');
			        var $window = $(window); 
			        function scrollMeToTop() {
			            header.css('top', $window.scrollTop() + "px");
			        }
			        $(window).scroll(scrollMeToTop).resize(scrollMeToTop);
			        var table = $('#<%= AspxTools.ClientId(m_Grid)%>')[0]; 
			        <%-- Datagrids dont render the header in a thead but i need it  there  to keep it in place so things cannot be dragged above it --%>
                    var head = document.createElement("THEAD");
                    head.appendChild(table.rows[0]);
                    table.insertBefore(head, table.childNodes[0]);
                    $(".dragMe").draggable({ containment: 'document', cursor: 'move' });
                    $(".dontPropagateMouse, .dragMe").on('mousedown mouseup', function(e) { e.stopPropagation(); });
                    var item = document.getElementById('<%= AspxTools.ClientId(m_InsertMultipleAt) %>');
                    var insertBtn = document.getElementById('<%= AspxTools.ClientId(m_AddMultiple) %>');
                    insertBtn.disabled = true;
                    addEventHandler(item, 'input', Count_onpropertychange);

                    $('.category, .description, .notes').on('input propertychange', '', dirty_onpropertychange);

				    updateOrder();
				    
				    $('.moveArrow.Top').mouseover(function(e){
				        $(this).children('img').attr('src', '../../images/up-orange-top.gif');
				    }).mouseout(function(e){
				        $(this).children('img').attr('src', '../../images/up-blue-top.gif');
				    });
				    
				    $('.moveArrow.Up').mouseover(function(e){
				        $(this).children('img').attr('src', '../../images/up-orange.gif');
				    }).mouseout(function(e){
				        $(this).children('img').attr('src', '../../images/up-blue.gif');
				    });
				    
				    $('.moveArrow.Bottom').mouseover(function(e){
				        $(this).children('img').attr('src', '../../images/down-orange-bottom.gif');
				    }).mouseout(function(e){
				        $(this).children('img').attr('src', '../../images/down-blue-bottom.gif');
				    });
				   
				   	$('.moveArrow.Down').mouseover(function(e){
				        $(this).children('img').attr('src', '../../images/down-orange.gif');
				    }).mouseout(function(e){
				        $(this).children('img').attr('src', '../../images/down-blue.gif');
				    });
				   

                    
				    $('#<%= AspxTools.ClientId(m_Grid) %> > tbody').sortable({ 
						axis: 'y', 
						handle : '.GridSortHandle', 
						placeholder : 'force',
						update : function(){updateOrder(); setPageDirty();},
						start: function (e, ui) { 
							ui.placeholder.html('<td colspan='+ ui.item.children().length+'>&nbsp;</td>');  // <%-- jquery uses a placeholder of empty tr which is why it doesnt show up --%>
						}
					});
				    $('.GridSortHandle').mouseover(function(){
				        $(this).css('cursor', 'move');
				    });
                });

			    
                function moveUp(input, toTop) {
				    var table = document.getElementById('<%= AspxTools.ClientId(m_Grid) %>');
					var tbody = table.tBodies[0];
				    var row = input.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
					    
				    var rowIndex = row.sectionRowIndex; 
				    
				    if( rowIndex < 1 ) {
				            return;  //alread at the top what do you want us to do?! 
				        }
				        
				        setPageDirty();
				        if(toTop) {
    				        moveRow(input,tbody,rowIndex, 0);		//the header is 0
    				        updateOrder();
    				        return;	            
				        }
				        moveRow(input,tbody,rowIndex, rowIndex -1 );
				        updateOrder();
					}
					
					function moveDown(input, toBottom) {
					    var table = document.getElementById('<%= AspxTools.ClientId(m_Grid) %>');
					    var tbody = table.tBodies[0];
					    var row = input.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
					    
					    var rowIndex = row.sectionRowIndex; 
					    
					    if( rowIndex + 1 == tbody.rows.length ) {
					        return; //already at the bottom cant keep going
					    }
					    setPageDirty();
					    
					    if( toBottom ) {
					        moveRow(input,tbody,rowIndex, tbody.rows.length -1);
    					    updateOrder();
    					    return;
					    }
					    moveRow(input, tbody, rowIndex, rowIndex + 1 );
					    updateOrder();
					}
					
					function moveRow(anchor,tbody, rowIndex, newRowIndex) 
					{
                        var movingRow = tbody.rows[rowIndex]; 
					    
					    var inputs = movingRow.getElementsByTagName('input');
					    
					    for( var i = 0; i < inputs.length; i++) {
					        var input = inputs[i];
					        if(  input.type != 'checkbox' ) {
					            continue;
					        }
					        if( typeof(input.defaultChecked) != 'undefined') {
					            input.defaultChecked = input.checked;
					        }
					    }
                        jQuery(anchor).triggerHandler('mouseout');
                        moveRowPoly(tbody, rowIndex, newRowIndex);
					}
					
					function updateOrder() {
					    var table = document.getElementById('<%= AspxTools.ClientId(m_Grid) %>');
					    var rows = table.tBodies[0].rows;
					    
					    var rowOrder = [];
					    
					    for( var i = 0; i < rows.length; i++ ){
					        var row = rows[i]; 
					        row.cells[1].innerText = i+1; 
					        var spans = row.cells[0].getElementsByTagName('span');
					        rowOrder.push('"' + spans[0].innerText +'"');
					    }
					    
					    document.getElementById('<%=AspxTools.ClientId(m_ConditionOrder) %>').value = "[" + rowOrder.join(",") + "]";
					    
					}
					
					function Count_onpropertychange(event) {
					    var property = event.propertyName;
					    if( event.type == 'input' || property == 'value' ){
					        var maxCountField = document.getElementById('<%= AspxTools.ClientId(m_ConditionLength)%>');
					        var maxCount = parseInt(maxCountField.innerText);
					        var userInsertAtValue = parseInt(this.value);
					        
					        var insertbtn = document.getElementById('<%= AspxTools.ClientId(m_AddMultiple)%>'); 					        
					        if( isNaN(userInsertAtValue) || userInsertAtValue > maxCount  || userInsertAtValue < 1) {
					            insertbtn.disabled = true;
					        }
					        else {
					            insertbtn.disabled = false;
					        }
					    }
					}
					
			    function dirty_onpropertychange(event) {
			        // Since this is bound through jQuery, we have to retrieve the original event
			        if( event.type == 'input' ||  event.originalEvent.propertyName == 'value' ) {
			            var el = retrieveEventTarget(event);
			            if( el.defaultValue != el.value) {
			                setPageDirty();
			            }
			        }
					}
					
					function isNumber(event) {
			            var charCode = event.keyCode;
			       
                        return !( charCode > 31 && (charCode < 48 || charCode > 57));
					}   
            </script>
		<script type="text/javascript">

			<!--
			
            function saveAndRedirect(handler) {
                 g_bPageProcessing = true;
                 if (typeof(handler) == 'function' && handler != null)
                    handler();
            }
            
            function saveMe() {
                if (g_bPageProcessing)
                    return; 
                isDoingPostBack = true;
                var saveMsg = document.getElementById('saveMsg');
                saveMsg.style.display = 'inline';
                g_bPageProcessing = true;
                //ajax_savePage();
                __doPostBack('', '');
            }
                 			
			<% if (IsReadOnly) { %>
      function f_disableAll() {
        var coll = document.getElementsByTagName("input");      
        var length = coll.length;
        if (length > 0) {
          for (i = 0; i < length; i++) {
            var o = coll[i];
            if (o.type=="text") o.readOnly = true;
            else if (o.type =="checkbox") o.disabled = true;
          }
        }   
        
        coll = document.getElementsByTagName("textarea");      
        length = coll.length;
        if (length > 0) {
          for (i = 0; i < length; i++) {
            var o = coll[i];
            o.style.backgroundColor = gReadonlyBackgroundColor;
            o.readOnly = true;
          }
        }
      }
      <% } %>
      
            
			
			function _init()
			{
			  
			  <% if (m_Dirty) { %>
			  setPageDirty();
			  <% } %>
			  
			  <% if (IsReadOnly) { %>
			    <%= AspxTools.JsGetElementById(m_Add) %>.disabled = true;
			    f_disableAll();
			  <% } %>
				if( document.getElementById("m_errorMessage") != null )
				{
					alert( document.getElementById("m_errorMessage").value );
				}
				
				if( document.getElementById('m_Action').value && document.getElementById('m_Action').value == "restoreDeleted" )
				{
				  document.getElementById('m_Action').value = '';
				  f_onDeletedClick();
				}
				

				
				
	
			}
			

		<%--// 07/26/2007 av opm 6489 Opens up a dialog that returns the template guid the use chose to import from. 
			// Running javascript from a submit button is not allowed so I added a regular button that calls this when its clicked. 
			// After the moddal is gone and good values are return then this method clicks on the hidden asp submit button to import 
			// the conditions. --%> 
			function onClickImport() 
			{
				var dlg = new cModalDlg(); 
				var arg = new cGenericArgumentObject();
				dlg.Open(dlg.VRoot + '/newlos/Underwriting/TemplateChooser.aspx', arg, "dialogHeight:375px; dialogWidth:400px;center:yes; resizable:no; scroll:no; status=no; help=no;");
			
				if ( arg.OK ) 
				{
				
					document.getElementById( "m_ImportTemplateId" ).value = arg.TemplateGuid;
					document.getElementById( "m_ImportTemplateName" ).value = arg.TemplateName;
					<%= AspxTools.JsGetElementById(m_ImportFromTemplate) %>.click();
				}			
				
				return false;	
			}
			
			var amIDirty = false;
			
            function ajax_savePage()
            {
                var $form = $('#TaskConditions');
                var $postUrl = $form.attr('action');
                var $formData = $form.serialize();
                
                callWebMethodAsync({
                    async: false,
                    type: "POST",
                    url: $postUrl,
                    data: $formData
                }).then(
					function(data, textStatus, jqXHR) { }, // Do nothing
					function(jqXHR, textStatus, errorThrown) {
                        alert("Could not save! " + errorThrown);
                    }
				);
            }
			
			function dirtyCheck(){
			    if (isDoingPostBack) {
			        return true;
			    }
			    
			    if( amIDirty) {
			        return UnloadMessage;
			    }
			}
			function setPageDirty()
			{
				amIDirty = true;
				document.getElementById('SaveBtn').disabled = false;
			}
			
			function f_filterCategory() {
			  var choice = <%= AspxTools.JsGetElementById(m_ConditionChoicesCategoryFilter) %>.value;
			  var oTable = document.getElementById("ConditionChoicesTable");
			  var nRowCount = oTable.rows.length;
			  
			  for (var i = 0; i < nRowCount; i++) {
			    var bDisplay = oTable.rows[i].cells[1].data == choice;
			    if (choice == "{ALL}")
			      bDisplay = true;
			      
			    oTable.rows[i].style.display = bDisplay ? "" : "none";
			  }
			}
	
			
			<%--
			//08-07-2007 av When the user checks the isDone field this method populates the date box which should not
			//be right next to it. OPM 3317.
			--%>
			function setDateDone(checked, obj) 
			{ 
				var n = obj; 
				do n = n.nextSibling;    <%-- //NextSibling returns whitespace in source also so this skips it--%>
				while ( n && n.nodeType != 1 );	

				if( checked )
				{ 
					if( n.value == '' ) 
					{ 
						var d = new Date(); 
						n.value = ( d.getMonth() + 1 ) + '/' + d.getDate() + '/' + d.getFullYear(); 
						n.readOnly = false;
					} 
					
					n.focus(); 
				} 
				else 
				{ 
					n.value = '';
					n.readOnly = true;
				}

   		      <%-- 
   		      // 05/26/09 OPM 4425. mf. Disable/enable condtion edit when marked as done.
		      // Adding more dirty use of DOM to support this.
		      // Eventually refactor so we can get references to these controls regardless of the page layout.
		      --%>

				n = n.nextSibling;
				while (n)
				{				  
				  if( n.tagName && n.tagName == 'INPUT' ) 
				  {
				    // Category
				    if (checked)
				      n.value =  <%= AspxTools.JsString(BrokerUser.DisplayName) %>;
				    else
				      n.value = '';
				    
				    break;
				  }
				  
				  n = n.nextSibling;
				}

		      category = obj.parentNode.parentNode.parentNode.parentNode.parentNode.nextSibling.children[0];
		      chooseLink = obj.parentNode.parentNode.parentNode.parentNode.parentNode.nextSibling.children[1].children[0].children[0];
		      condition = obj.parentNode.parentNode.parentNode.parentNode.parentNode.nextSibling.nextSibling.children[0];
		      
		      category.readOnly = checked;
		      chooseLink.disabled = checked;
		      chooseLink.onclick = checked ? function(){} : function(event){ onChooseClick( this, false, event ); return false;};
		      condition.readOnly = checked;
				
			}
			
			<%--
			//08-10-07 av ensures that the delete button is disabled if none of the boxes are checked.
			--%>
			function ScanSelected(obj) 
			{
				if ( obj.checked )  
				{
					document.getElementById("m_DeleteProxy").disabled = false;
					return; 
				}
				else 
				{
					var selected = false; 
					var table = <%= AspxTools.JsGetElementById(m_Grid) %>;
					var count = 0;
					var arLen=table.rows.length;
					for ( var i=1, len=arLen; i<len; ++i ){
						var child = table.rows[i].cells[0].children[0]; 
						while ( child && child.nodeType != 1) 
							child = child.nextSibling;
				
						if ( child && child.type == "checkbox" && child.checked ) 
						{
							selected = true; 
						}
						count++;
					}

					
					if ( ! selected ) 
					{
						document.getElementById("m_DeleteProxy").disabled = true; 	
					} 
					
					if ( !selected || count != table.rows.length - 2 ) 
					{
						var SelectAllBox = table.rows[0].cells[0].children[0];		
						while ( SelectAllBox && SelectAllBox.nodeType != 1) 
							SelectAllBox = SelextAllBox.nextSibling;
						if ( SelectAllBox.type == "checkbox" )
							SelectAllBox.checked = false;
					
					}
				
				}
				
			
			}
			function f_onDeletedClick()
			{
			<%-- // 08/25/09 OPM 34275. Let user know page is dirty, allow to save before showing deleted conditions --%>
				PolyShouldShowConfirmSave(amIDirty, function(){
					showModal('/newlos/Underwriting/DeletedConditions.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %>, null, null, null, function(args){ 
						if (args.OK)
						{
							if ( args.Ids && args.Ids.length > 0)
							{
								document.getElementById('m_RestoreConditions').value = args.Ids;
								<%= AspxTools.JsGetElementById(m_RestoreDeleted) %>.click();
							}
						}
					});
				}, function(){
					document.getElementById('m_Action').value = "restoreDeleted";
					saveMe();
				});
			}
					function onClickCheckAll( oCheck , bIsChecked )
					{
						var i , root = oCheck;
					
						while( root != null && root.tagName != "TABLE" )
						{
							root = root.parentElement;
						}
					
						for( i = 1 ; i < root.rows.length ; ++i )
						{
						
							var cell = root.rows[ i ].cells[ 0];
							
							
							if( cell.children[ 0 ].tagName != "INPUT" )
							{
							
								continue;
							}
						
							cell.children[ 0 ].checked = bIsChecked;
						}
						
						if( bIsChecked == true )
						{
							document.getElementById("m_DeleteProxy").disabled = false;
						}
						else
						{
							document.getElementById("m_DeleteProxy").disabled = true;
						}
					}

					function setChooseUI(bIsBatch)
					{
						var oTable = document.getElementById('ConditionChoicesTable');
						var oAddMultipleBtn = <%= AspxTools.JsGetElementById(m_AddMultiple) %>;
						var InputList = oTable.getElementsByTagName('INPUT');
						var LinkList = oTable.getElementsByTagName('A');
						var insertSpan1 = document.getElementById("InsertSpan1");
						var insertSpan2 = document.getElementById("InsertSpan2");
						var m_conditionLength = document.getElementById("<%= AspxTools.ClientId(m_ConditionLength)%>");
						var m_InsertMultipleAt = document.getElementById('<%= AspxTools.ClientId(m_InsertMultipleAt) %>');
						for(var i = 0; i < InputList.length; i++)
							InputList[i].style.display = (bIsBatch) ? 'inline' : 'none';
							
						for(var i = 0; i < LinkList.length; i++)
							LinkList[i].style.display = (bIsBatch) ? 'none' : 'inline';
						
						oAddMultipleBtn.style.display = (bIsBatch) ? 'inline' : 'none';
						insertSpan1.style.display = (bIsBatch) ? 'inline' : 'none';
						InsertSpan2.style.display = (bIsBatch) ? 'inline' : 'none';
						m_conditionLength.style.display = (bIsBatch) ? 'inline' : 'none';
						m_InsertMultipleAt.style.display = (bIsBatch) ? 'inline' : 'none';
						
												
					}

					function onChooseClick( oLink, bBatch, event )
					{
						event = event || window.event;
					    var oPanel = <%= AspxTools.ClientId(m_ConditionChoices) %>;
						setChooseUI(bBatch);
					
						while( oLink != null && oLink.tagName != "TD" )
						{
							oLink = oLink.parentElement;
						}

						if( oLink != null )
						{						
							oPanel.target = oLink.children[ 0 ].id;
						}
												
						var cursorOffsetY = 25; // How far above or below the click to show the div
						var cursorOffsetX = 10; // How far to the right to show the div
						
						var scrollOffsetY = document.body.scrollTop;
						var scrollOffsetX = document.body.scrollLeft;
					
						var frameHeight = document.body.clientHeight;
						var frameWidth = document.body.clientWidth;

						var xpos = event.clientX;
						var ypos = event.clientY;

						oPanel.style.height  = "365px";
						oPanel.style.width   = "500px";
						oPanel.style.display = "block";
						
						if( ( xpos + cursorOffsetX + 500 ) < frameWidth && ( ypos + 365 + cursorOffsetY ) < frameHeight )
						{
							// Below cursor
							oPanel.style.left = ( xpos + cursorOffsetX + scrollOffsetX ) + 'px';
							oPanel.style.top = ( ypos + cursorOffsetY + scrollOffsetY ) + 'px';
							
							oPanel.style.marginLeft = "0%";
							oPanel.style.bottom = "auto";
							oPanel.style.marginBottom = "0%";
						}
						else
						{
							if ( ( xpos + cursorOffsetX + 500 ) <  frameWidth && ( ypos - 365 + cursorOffsetY ) > 0 )
							{
								// Above cursor
								oPanel.style.left = ( xpos + cursorOffsetX + scrollOffsetX ) + 'px';
								oPanel.style.top = ( ypos - 365 - cursorOffsetY + scrollOffsetY ) + 'px';
			
								oPanel.style.marginLeft = "0%";
								oPanel.style.bottom = "auto";
								oPanel.style.marginBottom = "0%";
							}
							
							else
							{
								// Generic safe place
								oPanel.style.left = "0px";
								oPanel.style.top = "auto";

								oPanel.style.marginLeft = "15%";
								oPanel.style.bottom = "0px";
								oPanel.style.marginBottom = "13%";
							}
						}
					}

					function onSelectClick( oLink , oPanel )
					{
						var target;

						target = document.getElementById(oPanel.target);

						while( oLink != null && oLink.tagName != "TR" )
						{
							oLink = oLink.parentElement;
						}

						if( oLink != null && target != null )
						{
							setRow( target , oLink.children[ 1 ].innerText , oLink.children[ 2 ].innerText , "" );

							triggerEvent(target ,"change");
						}
						
						oPanel.style.display = "none";
						
						oPanel.target = "none";

						setPageDirty();
					}
					
					function setRow( oRow , sCategory , sCondition , sNotes )
					{
						while( oRow != null && oRow.tagName != "TR" )
						{
							oRow = oRow.parentElement;
						}

						if( oRow != null )
						{
							oRow.children[ 4 ].children[ 0 ].value = sCategory;
							oRow.children[ 5 ].children[ 0 ].value = sCondition;
							oRow.children[ 6 ].children[ 0 ].value = sNotes;

							oRow.children[ 0 ].children[ 0 ].checked = false;

							oRow.children[ 3 ].children[ 0 ].value = "";
						}
					}

					<%-- // 06/27/06 mf - opm 3950. Build list of categories and
					     // conditions for batch add. --%>
					function CalculateBatchList( )
					{
						oTable = document.getElementById('ConditionChoicesTable');
						
						var CbList = oTable.getElementsByTagName("INPUT");
						var sAddList = ''; 
						for (var i = 0; i < CbList.length; i++)
						{
							if (CbList[i].type && CbList[i].type == "checkbox"
							&& CbList[i].checked == true)
							{
								var oRow = CbList[i];
								while( oRow != null && oRow.tagName != "TR" )
								{
									oRow = oRow.parentElement;
								}
								if (oRow)
								{
									sAddList += (sAddList == '') ? '' : ',';
									sAddList += oRow.children[1].innerText.replace(/,/g,'&comma;')
									+ ','
									+ oRow.children[2].innerText.replace(/,/g,'&comma;');
								}
							}
						}
						if (document.getElementById('BatchAddList'))
							document.getElementById('BatchAddList').value = sAddList;
                        var insertBtn = document.getElementById('<%= AspxTools.ClientId(m_AddMultiple) %>');
                        insertBtn.disabled = sAddList.length == 0;
					}
					
	
			//-->

		</script>


					<div class="MainRightHeader" noWrap>
						<span class="Title">Conditions </span>
						<div id="Save">
						    <span id="saveMsg">Save in progress...</span>
						    <input type="button" value="Save" id="SaveBtn" disabled="disabled" NoHighlight="NoHighlight" onclick="return saveMe()" />
						</div>
					</div>
		<form id="TaskConditions" method="post" runat="server" submitdisabledcontrols="true">
			<input type="hidden" id="m_ImportTemplateId" runat="server" NAME="m_ImportTemplateId"/>
			<input type="hidden" id="m_ImportTemplateName" runat="server" NAME="m_ImportTemplateName"/>
			<input type="hidden" id="m_RestoreConditions" runat="server"/>
			<input type="hidden" id="m_ConditionOrder" runat="server" />
			<input type="hidden" id="m_Action" runat="server" />
			
			<asp:HiddenField runat="server" ID="ConditionSortOrder" Value="ASC" />
			<asp:HiddenField runat="server" ID="CategorySortOrder" Value="ASC"  />
			
			<input id="m_DirtyList" type="hidden">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0" id="conTable">

				<tr>
					<td> <ml:EncodedLabel ID="WarningLabel"   EnableViewState="False" Runat="server"/>
					        <asp:Panel runat="server" ID="ConditionChangeWarning" CssClass="warning">
            The conditions list may have changed since the approval certificate was last emailed.
        </asp:Panel>
					</td>
				</tr>
				<tr>
					<td noWrap>
					</td>
				</tr>
				<tr>
					<td noWrap>
						<asp:DataGrid id="m_Grid" runat="server" AlternatingItemStyle-CssClass="GridAlternatingItem" HeaderStyle-CssClass="GridHeader" ItemStyle-CssClass="GridItem" OnItemCommand="GridItemClick" OnItemDataBound="GridItemBound" AutoGenerateColumns="false" AllowSorting="true"  OnSortCommand="Grid_OnSortCondition">
							<Columns>
						
								<asp:TemplateColumn  HeaderText="<input type='checkbox' onclick='onClickCheckAll( this , checked );'>" ItemStyle-Width="30px" HeaderStyle-Width="30px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
									<ItemTemplate>
										<asp:CheckBox runat="server" id="Selected" onclick="ScanSelected(this)">
										</asp:CheckBox>
										<ml:EncodedLabel id="Key" runat="server" style="DISPLAY: none;">
										</ml:EncodedLabel>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn >
								    <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# AspxTools.HtmlString( (Container.ItemIndex + 1).ToString())%>
                                    </ItemTemplate>
								</asp:TemplateColumn>
									    <asp:TemplateColumn HeaderText="Re-Order" HeaderStyle-Width="60px"  >
									   <ItemStyle VerticalAlign="Middle"   HorizontalAlign="Center" />
                                            <ItemTemplate >
                                                <table runat="server" class="vcenter" cellpadding="0" cellspacing="0" style="width: 50px; height:46px; margin-top: 3px;">
                                                <tbody>
                                                    <tr>
                                                        <td rowspan="2"  class="GridSortHandle">
                                                            &nbsp;
                                                        </td>
                                                        <td rowspan="2">
                                                            &nbsp;
                                                        </td>
                                                        <td align="left" valign="bottom" style="width:13px; height:17px">
                                                            <a href="#" class="moveArrow Up" title="Move the condition one space up in the list" onclick="moveUp(this, false); return false;">
                                                                <img src="../../images/up-blue.gif"  />
                                                            </a>
                                                        </td>
                                                        <td align="right" valign="bottom" style="width:13px; height:17px">
                                                            <a href="#" class="moveArrow Top"   title="Move the condition to the top of the list" onclick="moveUp(this, true); return false;">
                                                                <img src="../../images/up-blue-top.gif"  />
                                                            </a>
                                                        </td>
                                                        <td rowspan="2">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top" style="width:13px; height:17px; padding-top: 3px;">
                                                            <a href="#" class="moveArrow  Down" title="Move the condition one space down in the list" onclick="moveDown(this, false); return false;">
                                                                <img src="../../images/down-blue.gif"  />
                                                            </a>
                                                        </td>
                                                        <td align="right" valign="top" style="width:13px; height:17px;padding-top: 3px;">
                                                           <a href="#" class="moveArrow Bottom" title="Move the condition to the bottom of the list" onclick="moveDown(this, true); return false;">
                                                                <img src="../../images/down-blue-bottom.gif"  />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </ItemTemplate>
							    </asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Date Done"  ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="100px" HeaderStyle-Width="100px">
									<ItemTemplate>
										<Table>
											<tr>
												<td nowrap>
													<asp:CheckBox id="IsDone"  runat="server" onclick="setDateDone(checked,this); setPageDirty()">
													</asp:CheckBox>
													<ml:DateTextBox id="DateDone" runat="server" onchange="setPageDirty()" strictvalidation="true" style="PADDING-LEFT: 4px;" preset="date" HelperAlign="AbsMiddle" Width="78px">
													</ml:DateTextBox>
													<br /><asp:TextBox id="DoneBy" runat="server" onchange="setPageDirty()" style="PADDING-LEFT: 4px;" TextMode="SingleLine" MaxLength="60" Width="120px" />
												</td>
											</tr>
								
										</Table>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Category" HeaderStyle-CssClass="PostBackButton" ItemStyle-Wrap="False" SortExpression="Category">
									<ItemTemplate>
										<asp:TextBox id="Category" CssClass="category" runat="server" onchange="setPageDirty()" style="PADDING-LEFT: 4px;" TextMode="SingleLine" MaxLength="100" Width="120px">
										</asp:TextBox>
										<asp:Panel id="Links" runat="server">
											<span style="PADDING-LEFT: 6px;">
												<a href="#" runat ="server" id="choose" onclick="onChooseClick( this, false, event ); return false;">
													choose</a>
											</span>
										</asp:Panel>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Condition" HeaderStyle-CssClass="PostBackButton" SortExpression="Condition" >
									<ItemTemplate>
										<asp:TextBox id="Description" CssClass="description" runat="server" onchange="setPageDirty()" style="PADDING-LEFT: 4px; FONT: 11px arial;" TextMode="MultiLine" Rows="3" MaxLength="3000" onkeyup="TextAreaMaxLength( this , 3000 );" Width="240px">
										</asp:TextBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderText="Notes">
									<ItemTemplate>
										<asp:TextBox id="Notes" CssClass="notes" runat="server" onchange="setPageDirty()" style="PADDING-LEFT: 4px; FONT: 11px arial;" TextMode="MultiLine" Rows="3" Width="180px" MaxLength="1000" onkeyup="TextAreaMaxLength(this, 1000);">
										</asp:TextBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Hidden">
								  <ItemTemplate>
								    <asp:CheckBox id="IsHidden" runat="server" onclick="setPageDirty()" />
								  </ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Created Date" >
									<ItemTemplate>
										<asp:TextBox id="CreatedD" runat="server" Width="135px" />
									</ItemTemplate>
									
								</asp:TemplateColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
			</table>
	
			<asp:Button id="m_Add" runat="server" class="PostBackButton" Text="Add condition" OnClick="AddClick" style="width:122px">
			</asp:Button>
			<input type=button id="AddMultipleBtn" onclick="onChooseClick( this , true, event ); return false;" style="width:140px" value="Insert multiple conditions">
			<asp:button ID="m_ImportFromTemplate"  class="PostBackButton" CssClass="hidden" runat="server" OnClick="ImportClick" ></asp:button>
			<input type="button" id="importButton" onclick="javascript:onClickImport()" value="Import conditions from template..." style="width:190px;"/>
			<asp:Panel id="m_DeletePanel" runat="server" style="DISPLAY: inline">
				<INPUT id=m_DeleteProxy disabled onclick="if( confirm( 'Delete conditions?' ) == true ) parentElement.children[ 1 ].click();" type=button value="Delete selected conditions" style="width:155px;">
				<asp:Button id=m_Del class="PostBackButton" style="DISPLAY: none" onclick=DeleteClick runat="server"/>
			</asp:Panel>
			<input type="button" onclick="f_onDeletedClick();" value="Restore deleted conditions..." style="width:165px;"/>
			<asp:Button id="m_RestoreDeleted" class="PostBackButton" runat="server" style="DISPLAY: none" onclick="RestoreDeletedClick" />
			

			<asp:Panel id="m_ConditionChoices" runat="server" CssClass="dragMe" style="BORDER-RIGHT: gray 2px outset; BORDER-TOP: gray 2px outset; DISPLAY: none; BACKGROUND: gainsboro; BORDER-LEFT: gray 2px outset; BORDER-BOTTOM: gray 2px outset; POSITION: absolute" target="none" Width="500">
                <div  class="dontPropagateMouse" style="border-right: medium none; padding-right: 2px; border-top: 2px groove;
                    overflow-y: scroll; padding-left: 2px; background: white; padding-bottom: 2px;
                    margin: 8px; border-left: 2px groove; width: 100%; padding-top: 2px; border-bottom: 2px groove;
                    height: 315px; cursor: default; ">
                    Category &nbsp;
                    <asp:DropDownList ID="m_ConditionChoicesCategoryFilter" runat="server" onchange="f_filterCategory();">
                    </asp:DropDownList>
                    <hr>
                    <table cellspacing="2" cellpadding="2" width="100%" border="0" id="ConditionChoicesTable">
                        <asp:Repeater ID="m_ChoiceList" runat="server" EnableViewState="False">
                            <ItemTemplate>
                                <tr onmouseover="this.style.backgroundColor = 'gainsboro';" onmouseout="this.style.backgroundColor = '';">
                                    <td valign="top">
                                        <input runat="server" type="checkbox" id="addChoiceCB" onclick="CalculateBatchList();"
                                            notforedit="true" name="addChoiceCB">
                                    </td>
                                    <td valign="top" data="<%#AspxTools.HtmlString(((string)Eval("Category")).ToUpper().Trim())%>">
                                        <%# AspxTools.HtmlString(((string)Eval("Category" )).Trim()) %>
                                    </td>
                                    <td>
                                        <%# AspxTools.HtmlString((string)Eval("Description")) %>
                                    </td>
                                    <td align="right" valign="top">
                                        <a href="#" onclick="onSelectClick( this , <%= AspxTools.ClientId(m_ConditionChoices) %> ); return false;">
                                            select</a>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <asp:Panel ID="m_EmptyMessage" Style="padding-right: 24px; padding-left: 24px; padding-bottom: 24px;
                        font: 11px arial; color: dimgray; padding-top: 24px; text-align: center" runat="server"
                        EnableViewState="False" Visible="False">
                        No condition choices defined. &nbsp;You can add conditions to your library through
                        the "Condition Choices" editor on the Pipeline, or by adding conditions directly
                        from this page.
                    </asp:Panel>
                </div>
                <div style="padding-right: 8px; padding-left: 8px; padding-bottom: 8px; width: 100%;
                    padding-top: 0px; text-align: center; cursor:move" >
                    <span id="InsertSpan1">
                        Insert at position </span><input  class="Count defaultc" onkeypress="return isNumber(event)"  type="text" runat="server" id="m_InsertMultipleAt"  /> <span id="InsertSpan2">/</span>  <ml:EncodedLabel runat="server" ID="m_ConditionLength" ></ml:EncodedLabel>
                    &nbsp;
                    <asp:Button class="PostBackButton" ID="m_AddMultiple" runat="server" OnClick="AddBatchClick" Text="Insert">
                    </asp:Button> &nbsp; &nbsp;
                                        <input onclick="parentElement.parentElement.style.display = 'none';" class="defaultc" type="button"
                        value="Cancel" />
                </div>

			</asp:Panel>
			<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg></form>
		</form>
	</body>
</HTML>
