using System;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Xml;
using System.Xml.Xsl;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Audit;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;
using ExpertPdf.HtmlToPdf;
using LendersOfficeApp.los.admin;
using LendersOffice.AntiXss;
using LendersOffice.ObjLib.Resource;

namespace LendersOfficeApp.newlos.Underwriting
{
    public partial class PmlLoanSummaryView_FrameService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        private string m_pmlLenderSiteId = "";
        private string GetXsltFileLocation(string formType)
        {
            switch (formType)
            {
                case "pmlsummary":
                    return ResourceManager.Instance.GetResourcePath(ResourceType.PmlLoanViewXslt, this.BrokerID);
                case "approval":
                    return ResourceManager.Instance.GetResourcePath(ResourceType.PmlApprovalLetterXslt, this.BrokerID);
                case "ratelock":
                    return ResourceManager.Instance.GetResourcePath(ResourceType.PmlRateLockConfirmationXslt, this.BrokerID);
                case "suspense":
                    return ResourceManager.Instance.GetResourcePath(ResourceType.SuspenseNoticeXslt, this.BrokerID);
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unknown Form to email");
            }
        }

        private Guid BrokerID
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
        }
        private string DisplayMessageToUnderwriter
        {
            get { return GetString("DisplayMessageToUnderwriter"); }
        }
        private string DisplaySubmissionAgreement
        {
            get { return GetString("DisplaySubmissionAgreement"); }
        }
        private string DisplayPricing
        {
            get { return GetString("DisplayPricing"); }
        }
        private string DisplayUnderwritingSignature
        {
            get { return GetString("DisplayUnderwritingSignature"); }
        }
        private XsltArgumentList GetXsltParams(string formType, string notes)
        {
            string css =  ResourceManager.Instance.GetResourceContents(ResourceType.CertificateCss, this.BrokerID); ; 
            
            XsltArgumentList args = new XsltArgumentList();
            args.AddParam("VirtualRoot", "", Tools.VRoot);
            args.AddParam("LenderPmlSiteId", "", m_pmlLenderSiteId);
            args.AddParam("GenericStyle", "", css);
            args.AddParam("HideJs", "", "True");
            args.AddParam("ConvertPdf", "", "False");
            args.AddParam("Notes", "", notes);
            if (formType == "pmlsummary")
            {
                args.AddParam("DisplayMessageToUnderwriter", "", DisplayMessageToUnderwriter);
                args.AddParam("DisplaySubmissionAgreement", "", DisplaySubmissionAgreement);
                args.AddParam("DisplayPricing", "", DisplayPricing);
                args.AddParam("DisplayUnderwritingSignature", "", DisplayUnderwritingSignature);
            }
            return args;
        }
        private XsltArgumentList GetPdfXsltParams(string formType)
        {
            string css = ResourceManager.Instance.GetResourceContents(ResourceType.CertificateCss, this.BrokerID); ;

            XsltArgumentList args = new XsltArgumentList();
            args.AddParam("VirtualRoot", "", Tools.VRoot);
            args.AddParam("LenderPmlSiteId", "", m_pmlLenderSiteId);
            args.AddParam("GenericStyle", "", css);
            args.AddParam("HideJs", "", "True");
            args.AddParam("ConvertPdf", "", "True");
            if (formType == "pmlsummary")
            {
                args.AddParam("DisplayMessageToUnderwriter", "", DisplayMessageToUnderwriter);
                args.AddParam("DisplaySubmissionAgreement", "", DisplaySubmissionAgreement);
                args.AddParam("DisplayPricing", "", DisplayPricing);
                args.AddParam("DisplayUnderwritingSignature", "", DisplayUnderwritingSignature);
            }
            return args;
        }
        private byte[] GenerateXml(Guid sLId, bool showDeletedConditions, bool showUnderwriterInfo, bool showJuniorUnderwriterInfo, bool includeNonSuspenseConditions)
        {
            byte[] xmlData = null;
            using (MemoryStream stream = new MemoryStream(10000))
            {
                XmlWriterSettings writerSettings = new XmlWriterSettings();
                writerSettings.Encoding = new UTF8Encoding(false); // Skip BOM
                writerSettings.OmitXmlDeclaration = true;

                XmlWriter writer = XmlWriter.Create(stream, writerSettings);
                PmlLoanSummaryXmlData.Generate(writer, sLId, true, showDeletedConditions, showUnderwriterInfo, showJuniorUnderwriterInfo, includeNonSuspenseConditions);
                writer.Flush();
                stream.Seek(0, SeekOrigin.Begin);

                xmlData = stream.ToArray();
            }
            return xmlData;
        }
        private string GetNotes()
        {
            string notes = AspxTools.HtmlString(GetString("Notes", ""));
            if (notes != null && notes.TrimWhitespaceAndBOM() != "")
            {
                notes = string.Format("Note from {0}:<br><br>{1}", BrokerUserPrincipal.CurrentPrincipal.DisplayName, notes.Replace(Environment.NewLine, "<br>"));
            }
            return notes;
        }
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "SendEmail":
                    SendEmail();
                    break;
                case "Save":
                    Save();
                    break;

            }
        }
        
        /// <summary>
        /// Save will convert the form data to pdf and then save it to the database
        /// If it is called with no arguments, then it will retrieve them from the
        /// javascript object
        /// </summary>
        private void Save()
        {
            Guid sLId = GetGuid("LoanID");
            string form = GetString("FormType");
            
            SetPmlLenderSiteId(sLId); // Perhaps this should be in some kind of constructor. Needed for logo

            bool showDeletedConditions = GetBool("IncludeCompleted", true);
            bool showUnderwriterInfo = GetBool("IncludeUnderwriter", true);
            bool showJuniorUnderwriterInfo = GetBool("IncludeJuniorUnderwriter", true);
            bool includeNonSuspenseConditions = GetBool("IncludeNonSuspenseConditions", true);
            byte[] xmlData = GenerateXml(sLId, showDeletedConditions, showUnderwriterInfo, showJuniorUnderwriterInfo, includeNonSuspenseConditions);
            string notes = GetNotes();
            Save(sLId, form, showDeletedConditions, showUnderwriterInfo, showJuniorUnderwriterInfo, includeNonSuspenseConditions, xmlData, notes);
        }

        private void Save(Guid sLId, string form, bool showDeletedConditions, bool showUnderwriterInfo, bool showJuniorUnderwriterInfo, bool includeNonSuspenseConditions, byte[] xmlData, string notes)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(PmlLoanSummaryView_FrameService));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            switch (form)
	        {
                case "pmlsummary":
                    dataLoan.GenerateAndSavePmlSummaryPdf(
                        DisplayMessageToUnderwriter,
                        DisplaySubmissionAgreement,
                        DisplayPricing,
                        DisplayUnderwritingSignature,
                        notes);
                    break;
                case "approval":
                    dataLoan.GenerateAndSaveApprovalCertPdf(showDeletedConditions, showUnderwriterInfo, showJuniorUnderwriterInfo); // check if showdeletedconditions is the same thing
                    break;
                case "ratelock":
                    dataLoan.GenerateAndSaveRateLockConfirmationPdf();
                    break;
                case "suspense":
                    dataLoan.GenerateAndSaveSuspenseNoticePdf(includeNonSuspenseConditions, showUnderwriterInfo, showJuniorUnderwriterInfo);
                    break;
		        default:
                    throw new CBaseException(ErrorMessages.Generic, "Unknown form");
	        }
            dataLoan.Save();
            // INEFFICIENCY: now we interpret the data twice: once for showing in the popup window, and once for saving
            // we show the popup pdf every time someone opens that window
        
        }

        /// <summary>
        /// Send an email containing the html for the form.
        /// </summary>
        private void SendEmail()
        {
            Guid sLId = GetGuid("LoanID");
            string formToEmail = GetString("FormType");
            string recipient = GetString("email");

            SetPmlLenderSiteId(sLId); // Needed for the logo

            string bodyContent = "";
            string auditContent = "";

            bool showDeletedConditions = GetBool("IncludeDeletedConditions", true);
            bool showUnderwriterInfo = GetBool("IncludeUnderwriterContactInfo", true);
            bool showJuniorUnderwriterInfo = GetBool("IncludeJuniorUnderwriterContactInfo", true);
            bool includeNonSuspenseConditions = GetBool("IncludeNonSuspenseConditions", true);
            byte[] xmlData = GenerateXml(sLId, showDeletedConditions, showUnderwriterInfo, showJuniorUnderwriterInfo, includeNonSuspenseConditions);

            string notes = GetNotes();

            #region Logo handling
            bool hasLogo = AttachLogo(sLId);
            string logoId = $"_logo_{DateTime.Now.Ticks}";
            byte[] logo = null;
            if (hasLogo)
            {
                try
                {
                    logo = FileDBTools.ReadData(E_FileDB.Normal, m_pmlLenderSiteId.ToString().ToLower() + ".logo.gif");
                }
                catch (FileNotFoundException)
                {
                    hasLogo = false;
                }
                bodyContent = GenerateCert(xmlData, formToEmail, notes, $"cid:{logoId}");
                auditContent = GenerateCert(xmlData, formToEmail, notes, "LogoPL.aspx?id=" + m_pmlLenderSiteId);
            }
            else
            {
                bodyContent = auditContent = GenerateCert(xmlData, formToEmail, notes, null);
            }
            #endregion

            // 12/12/2005 dd - Retrieve borrower last name to put in email's subject line.
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(PmlLoanSummaryView_FrameService));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CAppData dataApp = dataLoan.GetAppData(0);
            string aBLastNm = dataApp.aBLastNm;
            string aBFirstNm = dataApp.aBFirstNm;
            string loanNm = dataLoan.sLNm;

            EventNotificationRulesView rView = new EventNotificationRulesView(BrokerID); // OPM 8833
            string from = this.GetString("from", "") == "" ? rView.GetFromAddressString(dataLoan.sLId) : this.GetString("from", "");

            string subject = loanNm + " - " + aBFirstNm + " " + aBLastNm + " - " + GetEmailSubject(formToEmail); //gf OPM 94083

            var cbe = new LendersOffice.Email.CBaseEmail(BrokerID)
            {
                DisclaimerType = E_DisclaimerType.NORMAL,
                From = from,
                To = recipient,
                Bcc = "",
                Subject = subject,
                Message = bodyContent,
                IsHtmlEmail = true
            };
            if (hasLogo)
            {
                cbe.AddAttachment(logoId, logo, true);
            }

            AbstractAuditItem audit = null;
            switch (formToEmail)
            {
                case "pmlsummary":
                    audit = new PmlLoanSummaryEmailAuditItem((AbstractUserPrincipal)Page.User, recipient, from, auditContent);
                    break;
                case "ratelock":
                    audit = new RateLockConfirmationEmailAuditItem((AbstractUserPrincipal)Page.User, recipient, from, auditContent);
                    break;
                case "approval":
                    audit = new UWApprovalCertificateEmailAuditItem((AbstractUserPrincipal)Page.User, recipient, from, auditContent);
                    dataLoan.sApprovalCertLastSentD = DateTime.Now; // 10/20/2010 dd - We started to record when approval was last email.
                    break;
                case "suspense":
                    audit = new SuspenseNoticeEmailAuditItem((AbstractUserPrincipal)Page.User, recipient, from, auditContent);
                    break;
            }
            dataLoan.Save();
            cbe.Send();
            if (audit != null)
            {
                AuditManager.RecordAudit(sLId, audit);
            }

        }

        private string GetEmailSubject(string formType)
        {
            switch (formType)
            {
                case "pmlsummary":
                    return "Loan Summary";
                case "approval":
                    return "Underwriting Approval Certificate";
                case "ratelock":
                    return "Rate Lock Confirmation";
                case "suspense":
                    return "Suspense Notice";
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unknown Form to email");
            }
        }
        private void SetPmlLenderSiteId(Guid sLId)
        {
            // 11/10/2004 dd - Theoretically, PmlLenderSiteId should be in loan product.
            // This way we allow independent broker to run qualification against multiple lender.
            // However we are not there yet. Retrieve from current broker.

            Guid brokerPmlSiteId = BrokerUserPrincipal.CurrentPrincipal.BrokerDB.PmlSiteID;
            m_pmlLenderSiteId = Tools.GetFileDBKeyForPmlLogoWithLoanId(brokerPmlSiteId, sLId);

        }


        /// <summary>
        /// Determines if we need to show the logo on the email cert based on   DisplayBranchNameInLoanNotificationEmails
        /// </summary>
        /// <param name="sLId"></param>
        /// <returns></returns>
        private bool AttachLogo(Guid sLId)
        {
            CPageData data = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(PmlLoanSummaryView_FrameService));
            data.InitLoad();

            BranchDB db = new BranchDB(data.sBranchId, data.sBrokerId);
            db.Retrieve();

            return !db.IsDisplayNmModified;
        }

        private string GenerateCert(byte[] xmlData, string formToEmail, string notes, string logoSource)
        {
            using (MemoryStream stream = new MemoryStream(xmlData))
            {
                XsltArgumentList xsltParams = this.GetXsltParams(formToEmail, notes);
                if (false == string.IsNullOrEmpty(logoSource))
                {
                    xsltParams.AddParam("LogoSource", "", logoSource);
                }

                return XslTransformHelper.Transform(GetXsltFileLocation(formToEmail), stream, xsltParams);
            }

        }

    }
}
