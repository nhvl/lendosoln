﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="ApprovalLetter.aspx.cs" Inherits="LendersOfficeApp.newlos.Underwriting.ApprovalLetter" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="RELIED" TagName="HmdaReliedOn" Src="~/newlos/HmdaReliedOn.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Approval Certificate</title>
    <link href="../../css/calendar-win2k-cold-2.css"  rel="Stylesheet" type="text/css" />
    <style type="text/css">
        table.PartialWidth 
        { 
            width: 70%; 
        }
        .width80 {
            width: 80px;
        }
    </style>
</head>
<body bgcolor="gainsboro">
<script type="text/javascript">
<!--

function f_viewApprovalLetter() {
    PolyShouldShowConfirmSave(isDirty(), function(confirmed){
        if (confirmed && !saveMe()){
            return;
        }

        showApprovalLetter();
    }, null, clearDirty);
    
    return false;
}

function showApprovalLetter(){
    if (document.getElementById('IsOpenStatus').value == "1")
    {
        if (!confirm("The loan status is Open. Are you sure you want to continue?"))
            return;
    }
    
    var bIsSuspenseNotice = document.getElementById('hfIsSuspenseNotice').value === 'true';
    var isSuspenseNotice = bIsSuspenseNotice ? 't' : 'f';
    var includeComplete = 'f';
    var includeNonSuspenseConditions = 't';
    if (!bIsSuspenseNotice) {
        includeComplete = document.getElementById('IncludeCompleted').checked ? 't' : 'f';
    }
    else {
        includeNonSuspenseConditions = document.getElementById('IncludeNonSuspenseConditions').checked ? 't' : 'f';
    }
    
    var includeUnderwriter = document.getElementById('sIncludeUwContactInfoOnApprovalCert').checked ? 't' : 'f';
    var includeJuniorUnderwriter = document.getElementById('sIncludeJrUwContactInfoOnApprovalCert').checked ? 't' : 'f';
    var body_url = <%= AspxTools.JsString(VirtualRoot) %> + '/newlos/Underwriting/ApprovalLetter_Frame.aspx?loanid=' + <%= AspxTools.JsString(LoanID.ToString()) %> + '&ic=' + includeComplete + '&uw=' + includeUnderwriter + '&juw=' + includeJuniorUnderwriter + '&isSuspenseNotice=' + isSuspenseNotice + '&includeNonSuspenseConditions=' + includeNonSuspenseConditions;
    var win = window.open(body_url, "", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=780,height=600");

    win.focus();
}

function f_lockField(obj) {
    var maxRate = document.getElementById('<%= AspxTools.ClientId(sMaxR) %>');

    if ( obj.checked ) {
        maxRate.readOnly = false;
    }
    else {
        refreshCalculation();
        maxRate.readOnly = true;
     
    }
}

function _init() {
    var bIsSuspenseNotice = document.getElementById('hfIsSuspenseNotice').value === 'true';
    if (!bIsSuspenseNotice) {
        lockField(<%= AspxTools.JsGetElementById(sAppExpDLckd) %>, 'sAppExpD');
    }
}

function checkIt(evt) {
    var charCode =  evt.keyCode;
   
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }

    return true;
}

    function f_clearReport(){
    if (confirm("Please confirm that you would like to clear the eligibility report on the file."))
    {
        var args = new Object();
        args["loanid"] = <%= AspxTools.JsString(LoanID) %>;

        var result = gService.loanedit.call('ClearAutoEligibilityProcessReport',args); 
                
        if (result.error) {
            alert("Unable to clear report.");
        }

        document.getElementById('sAutoEligibilityProcessResult').value = result.value.sAutoEligibilityProcessResult;
        document.getElementById('sAutoEligibilityProcessResultD').value = result.value.sAutoEligibilityProcessResultD;
    }
}
//-->
</script>

    <form id="form1" runat="server">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
		<td nowrap class="MainRightHeader"><ml:EncodedLiteral runat="Server" ID="TitleHeader" Text="Approval Certificate"></ml:EncodedLiteral></td>
    </tr>
    <tr>
    <td nowrap style="PADDING-LEFT:10px">
        <table cellpadding="0" cellspacing="2" border="0" class="PartialWidth">
            <tr>
                <td colspan="2">
                    <input class="ButtonStyle" runat="server" id="btnViewApprovalLetter" style="width: 295px" onclick="f_viewApprovalLetter();"
                        type="button" runat="server" NoHighlight value="Generate New Approval Certificate...">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ml:PassthroughLabel CssClass="FieldLabel" ID="storedApprovalCertInfo" runat="server" Text="No approval certificate on file"></ml:PassthroughLabel>
                    <br /><br />
                </td>
            </tr>
            <tr><td colspan="2"></td></tr>
            <asp:PlaceHolder runat="server" ID="phIncludeClearedConditions">
            <tr>
                <td class="FieldLabel" colspan="2">
                    <input type="checkbox" id='IncludeCompleted' NotForEdit="true" /><label for="IncludeCompleted">Include Cleared Conditions</label>
                </td>
            </tr>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="phIncludeNonSuspenseConditions">
            <tr>
                <td class="FieldLabel" colspan="2">
                    <input type="checkbox" id='IncludeNonSuspenseConditions' NotForEdit="true" /><label for="IncludeNonSuspense">Include Non-Suspense Conditions</label>
                </td>
            </tr>
            </asp:PlaceHolder>
            <tr>
                <td class="FieldLabel" colspan="2">
                    <asp:CheckBox id='sIncludeUwContactInfoOnApprovalCert' runat="server"/><label for="sIncludeUwContactInfoOnApprovalCert">Include Underwriter Contact Info</label>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel" colspan="2">
                    <asp:CheckBox ID="sIncludeJrUwContactInfoOnApprovalCert" runat="server" /><label for="sIncludeJrUwContactInfoOnApprovalCert">Include Junior Underwriter Contact Info</label>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel" style="width: 20%">
                    Max DTI
                </td>
                <td>
                    <ml:PercentTextBox ID="sMaxDti" runat="server" width="60" preset="percent" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Max Rate
                </td>
                <td>
                    <ml:PercentTextBox ID="sMaxR" runat="server" width="60" preset="percent" />
                    <asp:CheckBox stye="vertical-align:bottom;" runat="server" ID="sMaxRLckd"  Text="lock" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Qualifying Score
                </td>
                <td>
                    <asp:TextBox onpaste="return false;" runat="server" ID="sCreditScoreLpeQual"   width="60"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Appraisal Exp Date
                </td>
                <td>
                    <ml:DateTextBox runat="server" ID="sApprRprtExpD" onchange="onDateAddTime(this);"  preset="date" width="60"></ml:DateTextBox>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Income Doc Exp Date
                </td>
                <td>
                    <ml:DateTextBox runat="server" ID="sIncomeDocExpD" onchange="onDateAddTime(this);"  preset="date" width="60"></ml:DateTextBox>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Credit Report Exp Date
                </td>
                <td>
                    <ml:DateTextBox runat="server" ID="sCrExpD" onchange="onDateAddTime(this);" preset="date" Width="60"></ml:DateTextBox>
                </td> 
            </tr>
            <tr>
                <td class="FieldLabel">
                    Asset Exp Date
                </td>
                <td>
                    <ml:DateTextBox runat="server" ID="sAssetExpD" onchange="onDateAddTime(this);" preset="date" Width="60"></ml:DateTextBox>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Preliminary Title Report Exp Date
                </td>
                <td>
                    <ml:DateTextBox runat="server" ID="sPrelimRprtExpD" onchange="onDateAddTime(this);" preset="date" Width="60"></ml:DateTextBox>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Bond Document Exp Date
                </td>
                <td>
                    <ml:DateTextBox runat="server" ID="sBondDocExpD" onchange="onDateAddTime(this);" preset="date" Width="60"></ml:DateTextBox>
                </td>
            </tr>
            
            <asp:PlaceHolder runat="server" ID="phApprovalExpDate">
            <tr>
                <td class="FieldLabel">
                    Approval Exp Date
                </td>
                <td>
                    <ml:DateTextBox runat="server" ID="sAppExpD" onchange="onDateAddTime(this);" preset="date" Width="60"></ml:DateTextBox>
                    <asp:CheckBox stye="vertical-align:bottom;" runat="server" ID="sAppExpDLckd" Text="lock" onclick="refreshCalculation();" />
                </td>
            </tr>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="phLoanSuspendedDate">
            <tr>
                <td class="FieldLabel">
                    Loan Suspended Date
                </td>
                <td>
                    <ml:DateTextBox runat="server" ID="sSuspendedD" preset="date" Width="60" ReadOnly="true"></ml:DateTextBox>
                </td>
            </tr>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="reliedOnForApprovalCertificate">
            <tr>
                <td class="FieldLabel">
                    <br />
                    Underwriting Dates
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Approved
                </td>
                <td>
                    <ml:DateTextBox runat="server" ID="sApprovD" preset="date" Width="60"></ml:DateTextBox>
                    <asp:TextBox ID="sApprovDTime" runat="server" class="width80" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Final Underwriting
                </td>
                <td>
                    <ml:DateTextBox runat="server" ID="sFinalUnderwritingD" preset="date" Width="60"></ml:DateTextBox>
                    <asp:TextBox ID="sFinalUnderwritingDTime" runat="server" class="width80" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Clear to Close
                </td>
                <td>
                    <ml:DateTextBox runat="server" ID="sClearToCloseD" preset="date" Width="60"></ml:DateTextBox>
                    <asp:TextBox ID="sClearToCloseDTime" runat="server" class="width80" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Credit Decision
                </td>
                <td>
                    <ml:DateTextBox runat="server" ID="sCreditDecisionD" preset="date" Width="60"></ml:DateTextBox>
                    <asp:TextBox ID="sCreditDecisionDTime" runat="server" class="width80" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <table class="Clear Main">
                        <RELIED:HmdaReliedOn runat="server" id="HmdaReliedOn"></RELIED:HmdaReliedOn>
                    </table>
                </td>
            </tr>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="phOvernightProcess" Visible="false">
            <tr>
                <td class="FieldLabel" colspan="2">
                <hr />
                    Overnight Process Report for Eligibility:&nbsp;
                    <asp:TextBox id="sAutoEligibilityProcessResult" ReadOnly="true" runat="server" Width="150px" />&nbsp;
                    <asp:TextBox id="sAutoEligibilityProcessResultD" ReadOnly="true" runat="server" Width="170px" />&nbsp;
                    <input class="ButtonStyle" type="button" id="clearReportBtn" runat="server"
                        value="Clear Report" onclick="f_clearReport()" />
                </td>
            </tr>
            </asp:PlaceHolder>
        </table>
	</td>
	</tr>
    </table>
    <input type="hidden" id="IsOpenStatus" runat="server" />
    <input type="hidden" id="hfIsSuspenseNotice" runat="server" />
    </form>
</body>
</html>
