<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="TemplateChooser.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Underwriting.TemplateChooser" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>Please choose a template...</title>
    <LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet"/>
    <script type="text/javascript">
        function select(id, name)
        {
            var args = {};
            args.OK = true;
            args.TemplateGuid = id;
            args.TemplateName = name;
            onClosePopup(args);
        }
    </script>
</HEAD>
  <body class="bgcolor1" style="overflow: auto;">
	<h4 class="page-header">Available Templates</h4>
    <form id="TemplateChooser" method="post" runat="server">
		<table align="center" class=" class="FormTable" border="0" width="100%">
			<tbody> 
				<tr>
					<td>
						<div style="Height:265px; OVERFLOW-Y:auto; margin-left: 20px; ">
							<ml:EncodedLabel ID="m_NoTemplateLabel" Runat="server" Text="You do not have access to any templates." Visible="False"  EnableViewState="False"/>
							<ul class="Bullet">

                            <asp:Repeater id="m_templateRepeater" runat="server">
                                <ItemTemplate>
                                <li>
                                    <a name="TemplateLinkButton" class="PortletLink" href=<%# AspxTools.HtmlAttribute("javascript:select(" + AspxTools.JsString(GetTemplateId(Container.DataItem)) + "," + AspxTools.JsString(GetTemplateName(Container.DataItem)) + ")") %>>
                                        <%# AspxTools.HtmlString(GetTemplateName(Container.DataItem)) %>
                                    </a>
                                </li>
                                </ItemTemplate>
                            </asp:Repeater>
							</ul>
						</div>
					</td>
				</tr>
				<tr>
					<td align="center">
						<hr/>
						<input type="button" value="Cancel" onclick="onClosePopup();">
					</td>
				</tr>
			</tbody>
		</table>
		<uc1:cModalDlg runat="server" id=CModalDlg1 />
	</form>
	
  </body>
</HTML>
