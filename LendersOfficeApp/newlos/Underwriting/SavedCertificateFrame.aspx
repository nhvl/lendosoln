<%@ Page language="c#" Codebehind="SavedCertificateFrame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Underwriting.SavedCerfiticateFrame" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>SavedCerfiticateFrame</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </HEAD>
  <body MS_POSITIONING="FlowLayout">
	<script language="javascript">
<!--
function f_print() 
{
  frmBody.focus();
  window.print();
}

function f_onChangeDisplay() 
{
  var underwriter = <%= AspxTools.JsGetElementById(DisplayUnderWriter) %>.checked ? "t" : "f";
  var processor = <%= AspxTools.JsGetElementById(DisplayProcessor) %>.checked ? "t" : "f";
  frmBody.location = <%= AspxTools.JsString("SavedCertificate.aspx?loanid=" + RequestHelper.LoanID + "&underwriter=") %> + underwriter + "&processor=" + processor;
}
      
//-->
</script>
	
    <form id="SavedCerfiticateFrame" method="post" runat="server">
			
			<table width="100%" height="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td>
   						<table align="left" valign="top">
								<tr>
									<TD class="FieldLabel"> Include these sections in&nbsp;print-out:</TD>
									<td class="FieldLabel">
										<asp:CheckBox id="DisplayUnderWriter" runat="server" style="PADDING-LEFT: 10px" Text="Underwriter" Checked="True" onclick="f_onChangeDisplay();"></asp:CheckBox>
										<asp:CheckBox id="DisplayProcessor" runat="server" style="PADDING-LEFT: 10px" Text="Processor" onclick="f_onChangeDisplay();"></asp:CheckBox></td>
								</tr>
						</table>
					</td>
					<td align="right" valign="top" style="PADDING-RIGHT:5px;PADDING-TOP:5px">
						<input type="button" onclick="f_print();"  value="Print" class="ButtonStyle"> <input type="button" onclick="onClosePopup();" value="Close" class="ButtonStyle">
					</td>
				</tr>
				
				<tr>
					<td height="100%" colspan="2">
						<iframe src=<%= AspxTools.SafeUrl("SavedCertificate.aspx?loanid=" + RequestHelper.LoanID + "&underwriter=t&processor=f") %> width="100%" height="100%" frameborder=1 id=frmBody>
						</iframe>
					</td>
				</tr>
			</table>
     </form>
	
  </body>
</HTML>
