﻿#region Generated code
namespace LendersOfficeApp.newlos.Underwriting
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.Underwriting;
    using LendersOffice.Security;

    /// <summary>
    /// A backend service page supporting the AUS Ordering Dashboard.
    /// </summary>
    public partial class AusOrderingDashboardServiceItem : AbstractBackgroundServiceItem
    {
         /// <summary>
        /// Constructs a loan data object.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <returns>An initialized loan data object.</returns>
        protected override CPageData ConstructPageDataClass(Guid loanId)
        {
            return CPageData.CreateUsingSmartDependency(loanId, typeof(AusOrderingDashboardServiceItem));
        }

        /// <summary>
        /// Binds loan data from the page to a loan object.
        /// </summary>
        /// <param name="dataLoan">A loan data object.</param>
        /// <param name="dataApp">An application data object.</param>
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var orderJson = GetString("AusOrderListJson");
            var orderList = SerializationHelper.JsonNetDeserialize<List<AusResult>>(orderJson);

            var brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            var loanId = dataLoan.sLId;

            foreach (var order in orderList)
            {
                if (order.BrokerId == Guid.Empty)
                {
                    order.BrokerId = brokerId;
                }

                if (order.LoanId == Guid.Empty)
                {
                    order.LoanId = loanId;
                }
            }

            var oldOrderList = AusResultHandler.RetrieveOrdersForLoan(this.sLId, brokerId);
            var newOrderDictionary = orderList.Where(order => order.AusOrderId != null).ToDictionary(order => order.AusOrderId);

            var deletedOrderList = oldOrderList.Where(order => order.IsManual && !newOrderDictionary.ContainsKey(order.AusOrderId));

            foreach (var order in oldOrderList)
            {
                AusResult newOrder = null;
                newOrderDictionary.TryGetValue(order.AusOrderId, out newOrder);
                if (newOrder == null)
                {
                    continue;
                }

                order.CustomPosition = newOrder.CustomPosition;
                order.Notes = newOrder.Notes;
                if (order.IsManual)
                {
                    if (order.UnderwritingService == UnderwritingService.LP)
                    {
                        order.LpRiskClass = newOrder.LpRiskClass;
                        order.LpPurchaseEligibility = newOrder.LpPurchaseEligibility;
                        order.LpStatus = newOrder.LpStatus;
                        order.LpFeeLevel = newOrder.LpFeeLevel;
                    }
                    else if (order.UnderwritingService == UnderwritingService.DU)
                    {
                        order.DuRecommendation = newOrder.DuRecommendation;
                    }
                    else if (order.UnderwritingService == UnderwritingService.GUS)
                    {
                        order.GusRecommendation = newOrder.GusRecommendation;
                        order.GusRiskEvaluation = newOrder.GusRiskEvaluation;
                    }
                    else if (order.UnderwritingService == UnderwritingService.TOTAL)
                    {
                        order.TotalEligibilityAssessment = newOrder.TotalEligibilityAssessment;
                        order.TotalRiskClass = newOrder.TotalRiskClass;
                    }
                    else if (order.UnderwritingService == UnderwritingService.Other)
                    {
                        order.UnderwritingServiceOtherDescription = newOrder.UnderwritingServiceOtherDescription;
                        order.ResultOtherDescription = newOrder.ResultOtherDescription;
                    }

                    order.CaseId = newOrder.CaseId;
                    order.OrderPlacedBy = newOrder.OrderPlacedBy;
                    order.Timestamp_rep = newOrder.Timestamp_rep;
                }
            }

            var newOrdersToSave = orderList.Where(newOrder => newOrder.AusOrderId == null && newOrder.IsManual).ToList();
            foreach (var order in newOrdersToSave)
            {
                order.BrokerId = brokerId;
                order.LoanId = loanId;
                order.LoanIsFha = dataLoan.sLT == E_sLT.FHA;
            }

            AusResultHandler.SaveAusOrderList(oldOrderList.Concat(newOrdersToSave), brokerId);
            AusResultHandler.DeleteOrderList(deletedOrderList, brokerId);

            dataLoan.sReportFhaGseAusRunsAsTotalRun = GetBool("sReportFhaGseAusRunsAsTotalRun");
        }

        /// <summary>
        /// Loads data from a loan object to the page.
        /// </summary>
        /// <param name="dataLoan">A loan data object.</param>
        /// <param name="dataApp">An application data object.</param>
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            var orderList = AusResultHandler.RetrieveOrdersForLoan(this.sLId, PrincipalFactory.CurrentPrincipal.BrokerId);
            var orderJson = ObsoleteSerializationHelper.JsonSerialize(orderList);
            SetResult("AusOrderListJson", orderJson);
        }
    }

    /// <summary>
    /// Manages service items for the AUS Ordering Dashboard.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed.")]
    public partial class AusOrderingDashboardService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Initializes a background service for the AUS Ordering Dashboard.
        /// </summary>
        protected override void Initialize()
        {
            AddBackgroundItem(string.Empty, new AusOrderingDashboardServiceItem());
        }
    }
}