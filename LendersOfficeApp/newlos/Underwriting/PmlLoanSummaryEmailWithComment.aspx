<%@ Page language="c#" Codebehind="PmlLoanSummaryEmailWithComment.aspx.cs"  AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Underwriting.PmlLoanSummaryEmailWithComment"  EnableEventValidation="false"  EnableViewState="false"%>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<html>
  <head runat="server">
    <title>Email Summary</title>
    <style type="text/css">
        body, textarea { font-family: Arial, Helvetica, sans-serif; }
        body { background-color: #DCDCDC;     font-size: 11px;  margin: 0px;font-weight:bold;    }
        p { margin: 0; padding: 0 }
        ul { list-style: none; padding: 0; margin: 0 0 20px 35px; }
        li { padding-bottom: 5px; }
        #sOtherEmails, #sendToOther  { width: 200px;  }
        fieldset { margin: 0px 5px 5px 5px; padding: 10px 10px 0px 10px; border: none;}
        .ToHeading, .FromHeading { float: left;  }
        .EmailSection { margin-left: 40px } 
        .MainRightHeader
        {
            padding-left: 5px;
            font-weight: bold;
            font-size: 11px;
            padding-bottom: 5px;
            color: white;
            padding-top: 5px;
            font-family: Arial, Helvetica, sans-serif;
            background-color: maroon;
        }
        .TopOther { 
            margin-top: 5px;
        }
       .TopOther a, .TopOther img, .TopOther label, .TopOther textarea{
            vertical-align: top;
     
        }
        .TopOther input { 
            vertical-align: top;
            margin-top: -5px;
        }

        #sendFromOtherWarning {
             color: red;
        }
        .w-full {
            width: 100%;
        }
    </style>       
  </head>
  <body>
<script  type="text/javascript" >
        String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "");
};
        function _init() {
            $("#sendFromOtherDefault").text(ML.DefaultSenderAddress);
            setSendFromOtherWarning();

    	     SetTitle();
    	     SendFromOtherCheck(document.getElementById(<%= AspxTools.JsGetClientIdString(sendToOtherRB)  %>).checked);
    	        resizeForIE6And7( 520, 690);
    	    }
    	    
    	    function resize(w, h) {
              w = w < screen.width ? w : screen.width - 10;
              h = h < screen.height ? h : screen.height - 60;
              var l = (screen.width - w) / 2;
              var t = (screen.height - h - 50) / 2;
              self.dialogWidth = w + 'px'; 
              self.dialogHeight = h + 'px';
              self.dialogLeft = l + 'px';
              self.dialogTop = t + 'px';
              
            }

            function resizeForIE6And7(w, h) 
            {
	            resize( w, h ); 
            		
	            if ( document.documentElement.offsetHeight  != h ) 
	            {
		            var chrome =  h - document.documentElement.offsetHeight - 10; 
		            resize ( w, h + chrome ) ;
	            }
	        }
	        
	        function SetTitle()
	        {
	          var oTitle = document.getElementById("FormType");  
	          
	          var obj = window.dialogArguments || {};
	          switch( obj.FormType )
	          {
	            case "pmlsummary": oTitle.innerText = "PML Summary"; break;
	            case "approval": oTitle.innerText = "Underwriting Approval"; break;
	            case "ratelock": oTitle.innerText = "Rate Lock Confirmation"; break;
	            case "suspense": oTitle.innerText = "Suspense Notice"; break;
	          }
	        }


	        var PageValidation = {
	            FromOther : <%= AspxTools.JsGetClientIdString(sendToOtherRB)  %>,
	            FromOtherEmail :'sendToOther', 
	            ToOtherCB :  'SOther', 
	            ToOtherValue : 'sOtherEmails', 
	            IsFromOtherValid: function() {
	          
	                var rb = document.getElementById(PageValidation.FromOther); 
	                var email = document.getElementById(PageValidation.FromOtherEmail); 
	                var isValid = true;
	                if (rb == null ) {
	                    isValid = false;
	             
	                }
	                else if (rb.checked && email != null ) {
	                    isValid = PageValidation.IsEmailValid(email.value); 
	                }
	                return isValid; 
	            }, 
	            IsEmailValid : function(email) 
	            {
	           
	                var isValid = true; 
	                if ( email == null || email == '' ) 
	                { 
	                    isValid = false; 
	                }
	                else 
	                {
	                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ 
                        isValid = re.test(email.replace(/^\s+|\s+$/g, ''));
	                }
	                return isValid;
	            },
	            
	            AddOtherEmail : function(email){
	                var emails = document.getElementById(PageValidation.ToOtherValue),otherEmails=emails.value.trim(),isEnabled = document.getElementById(PageValidation.ToOtherCB),emailArray,foundAddress = false;
	                email = email.trim();
	                if ( isEnabled == null )  {
	                    return; 
	                }
	                
	                if( PageValidation.IsEmailValid(email) === false ) {
	                    alert("Email " + email + " is invalid.");
	                    return;
	                }
	                
	                if ( isEnabled.checked && emails != null && emails.value.length != 0 ) {   
	                    emailArray = emails.value.split(';');
	                    for(var i = 0; i < emailArray.length; i++ ) 
	                    {
	                        if( emailArray[i].toLowerCase().trim()  === email.toLowerCase() ){
	                            foundAddress = true;
	                            break;
	                        }   
	                    }
	                    if( foundAddress ) {
	                        return;
	                    }
	                    
	       
	                }
	                
	                if( emails.value.trim().length > 0 && emails.value.charAt(emails.value.trim().length-1) != ';' ) {
	                        emails.value+=';\n';
                    }
	                emails.value += email;
	                SendToOtherBlur();
	            },
	            
	            IsToOtherValid : function() { 
	           
	                var isValid = true; 
	                var emails = document.getElementById(PageValidation.ToOtherValue); 
	                
	                var isEnabled = document.getElementById(PageValidation.ToOtherCB);
	            
	                if ( isEnabled == null )  
	                {
	                    return false; 
	                }
	                else if ( isEnabled.checked && emails != null && emails.value.length != 0 ) 
	                {
	                    var emailArray = emails.value.split(';'); 
	                    for(var i = 0; i < emailArray.length; i++ ) 
	                    {
	                        isValid &= PageValidation.IsEmailValid(emailArray[i]);
	                      
	                    }
	                }
	                else if ( isEnabled.checked && emails != null && emails.value.length == 0 )
	                {
	                
	                    isValid = false;
	                }
	                return isValid; 
	            },
	            
	            IsPageValid : function () 
	            {
	                return PageValidation.IsToOtherValid()  && PageValidation.IsFromOtherValid()  ; 
	            }
	            

	        };

            function f_email() {
                if ( !PageValidation.IsPageValid() )   
                {
                    alert('Please fix existing errors.');
                    return;
                }

    	        var obj = getModalArgs() || {};
    	        var args = new Object();
    	        args["LoanID"] = <%=AspxTools.JsString(RequestHelper.LoanID)%>;
    	        
    	        if (obj.FormType == "pmlsummary")
    	        {
    	          args["DisplaySubmissionAgreement"] = obj.DisplaySubmissionAgreement;
    	          args["DisplayMessageToUnderwriter"] = obj.DisplayMessageToUnderwriter;
    	          args["DisplayPricing"] = obj.DisplayPricing;
    	          args["DisplayUnderwritingSignature"] = obj.DisplayUnderwritingSignature;
    	        }
    	        
    	        if(obj.FormType == "approval" || obj.FormType === "suspense")
    	        {
    	          args["IncludeDeletedConditions"] = obj.IncludeCompleted;
    	          args["IncludeUnderwriterContactInfo"] = obj.IncludeUnderwriter;
    	          args["IncludeJuniorUnderwriterContactInfo"] = obj.IncludeJuniorUnderwriter;
    	          args["IncludeNonSuspenseConditions"] = obj.IncludeNonSuspenseConditions
    	        }
    	        
    	        args["FormType"] = obj.FormType;
    	        args["Notes"] = document.getElementById("Comments").value;
    	        args["From" ] = GetFromAddress();   
    	        args["email"] = BuildUpToAddress();
                if ( args["email"] == null || args["email"].length == 0 ) 
                {
                    alert('Please select or enter at least one recipient.');
                    return; 
                }
                
                // Relevant service page: PmlLoanSummaryView_FrameService.aspx
                var result = gService.main.call("SendEmail", args);
                if (!result.error) {
                  switch ( obj.FormType )
                  {
                    case "pmlsummary":
                      alert(<%= AspxTools.JsString(JsMessages.PmlLoanSummaryView_SendComplete) %>);
                      break;
                    case "approval":
                      alert(<%= AspxTools.JsString(JsMessages.PmlLoanSummaryView_ApprovalSendComplete) %>);
                      break;
                    case "ratelock":
                      alert(<%= AspxTools.JsString(JsMessages.PmlLoanSummaryView_RatelockSendComplete) %>);
                      break;
                    case "suspense":
                      alert(<%= AspxTools.JsString(JsMessages.PmlLoanSummaryView_SuspenseNoticeSendComplete) %>);
                      break;
                  }

                  onClosePopup();
    	        
    	        } else {
    	            alert('Unable to send email. \n\n' + result.UserMessage);
    	        }

    	    } 
    	    function BuildUpToAddress() 
    	    {
    	        var ae = document.getElementById('AENameCb');
    	        var lo = document.getElementById('LoNameCb');
    	        var bp = document.getElementById('BpNameCb');
    	        var externalSecondary = document.getElementById('ExternalSecondaryNameCb');
    	        var externalPostCloser = document.getElementById('ExternalPostCloserNameCb');
    	        var pr = document.getElementById('PrNameCb');
    	        var un = document.getElementById('UnNameCb');
    	        var other = document.getElementById('SOther');
    	        var emails = ''; 
    	        if ( ae != null && ae.checked ) 
    	        {
    	            emails += document.getElementById('AEEmail').innerHTML; 
    	        }
    	        if ( lo != null && lo.checked ) 
    	        {
    	            emails += emails.length == 0 ? '' : ';'; 
    	            emails += document.getElementById('LoEmail').innerHTML;  
    	        }
    	        if ( bp != null && bp.checked ) 
    	        {
    	            emails += emails.length == 0 ? '' : ';'; 
    	            emails += document.getElementById('BpEmail').innerHTML;  
    	        }
    	        if ( externalSecondary != null && externalSecondary.checked )
    	        {
    	            emails += emails.length == 0 ? '' : ';'; 
    	            emails += document.getElementById('ExternalSecondaryEmail').innerHTML;
    	        }
    	        if ( externalPostCloser != null && externalPostCloser.checked )
    	        {
    	            emails += emails.length == 0 ? '' : ';'; 
    	            emails += document.getElementById('ExternalPostCloserEmail').innerHTML;
    	        }
    	        if ( pr != null && pr.checked ) 
    	        {
    	            emails += emails.length == 0 ? '' : ';'; 
    	            emails += document.getElementById('PrEmail').innerHTML;  
    	        }
    	        if ( un != null && un.checked ) 
    	        {
    	            emails += emails.length == 0 ? '' : ';'; 
    	            emails += document.getElementById('UnEmail').innerHTML;  
    	        }
    	        if ( other != null && other.checked ) 
    	        {
    	            emails += emails.length == 0 ? '' : ';'; 
    	            emails += document.getElementById('sOtherEmails').value; 
    	        }
    	        return emails.replace(/\s/g, "");
    	    }
    	    function GetFromAddress() 
    	    {
    	        var corporateAdd = <%= AspxTools.JsGetElementById(CorporateEmailAddress)  %> 
    	        var  other = <%= AspxTools.JsGetElementById(sendToOtherRB)  %> 
    	        if ( corporateAdd != null && corporateAdd.checked && <%= AspxTools.JsString( CorporatesEmailAddress ) %> != ''  ) return <%=  AspxTools.JsString( GetCorporateFromString() ) %>;
    	        else if ( other !=null && other.checked && document.getElementById('sendToOther').value )  return document.getElementById('sendToOther').value; 
    	        else 
    	        {
    	            return <%= AspxTools.JsString( GetUserFromString() ) %>; 
    	        }
    	    }
    	    
            function SendToOtherCheck(obj)
            {
                if ( !obj.checked ) 
                {
                    document.getElementById('sOtherEmails').readOnly = true;
                    document.getElementById('SendToOtherRequired').style.display = 'none';   
                    document.getElementById('SendToOtherError').style.display = 'none';
      
                
                }
                else 
                {
                    document.getElementById('sOtherEmails').readOnly = false;
                    document.getElementById('SendToOtherRequired').style.display = '';
                }
                
            }
            function SendFromOtherCheck(enable) 
            {
                document.getElementById('sendToOther').readOnly = !enable;
                document.getElementById('SendFromRequiredImg').style.display = enable ? '' : 'none';   
                if ( !enable ) 
                { 
                    document.getElementById('SendFromErrorImg').style.display = 'none';
                }
            }
            
            function FromOtherBlur() 
            {
             
                document.getElementById('SendFromErrorImg').style.display = PageValidation.IsFromOtherValid() ? 'none' : ''; 

                setSendFromOtherWarning();
            }

            function setSendFromOtherWarning()
            {
                if (!ML.EnableEmailDomainAuthorization)
                {
                    // Hide warning and never show in this case.
                    $("#sendFromOtherWarning").toggle(false);
                    return;
                }

                var showWarning = true;

                var sendToOtherVal = $(<%= AspxTools.JsGetElementById(sendToOther) %>).val();
                var atSignIndex = sendToOtherVal.indexOf("@");

                if (atSignIndex < 0) {
                    $("#sendFromOtherWarning").toggle(false);
                    return;
                }

                var domain = sendToOtherVal.substring(atSignIndex + 1).toLowerCase();
                var authorizedEmailDomains = $.parseJSON(ML.AuthorizedEmailDomains);
                for (var i = 0; i < authorizedEmailDomains.length; i++)
                {
                    if (domain == authorizedEmailDomains[i].toLowerCase())
                    {
                        showWarning = false;
                        break;
                    }
                }

                $("#sendFromOtherWarning").toggle(showWarning);
                $("#sendFromOtherOriginal").text(sendToOtherVal);
            }
            
            function SendToOtherBlur() 
            {
                document.getElementById('SendToOtherError').style.display = PageValidation.IsToOtherValid() ?  'none' : ''; 
            }
            
            function showPickFromContacts() {
                var othercb = document.getElementById('SOther'); 
                if( othercb.checked === false ) {
                    othercb.click();
                }
                
                resizeForIE6And7( 820, 710);
                LQBPopup.Show(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/los/RolodexList.aspx?ass=1&loanid=" + LoanID)) %>,  {
                    onReturn: function (dialogArgs) {
                        if (dialogArgs.OK) {
                            document.getElementById("sOtherEmails").value = "";
                            for (var i = 0; i < dialogArgs.emailList.length; i++)
                            {
                                PageValidation.AddOtherEmail(dialogArgs.emailList[i]);
                            }
                        }
                    },
                    hideCloseButton: true
                }, document.getElementById("sOtherEmails").value);
                              
                return false;
            }

    </script>
    <h4 class="page-header">Email <span id="FormType">PML Summary</span></h4>
    <form id="PmlLoanSummaryEmailWithComment" method="post" runat="server">
        <fieldset>
            <div class="FromHeading" > From: </div>
            <ul>
                    <li>
                        <asp:RadioButton runat="server" Checked="true" GroupName="FromAddress" ID="DefaultEmailAddress" onclick="SendFromOtherCheck(false)"/>
                        <ml:EncodedLiteral runat="server" ID="defaultUserName" > </ml:EncodedLiteral>
                        <span runat="server"  id="DefaultStartSeperator" visible="false" >&lt;</span><a id="defaultemailAddressLink" runat="server"></a><span runat="server" id="DefaultEndSeperator" visible="false" >&gt;</span>
                    </li>
                    <li runat="server" id="corporateSection">
                        <asp:RadioButton runat="server" GroupName="FromAddress" ID="CorporateEmailAddress" onclick="SendFromOtherCheck(false)" /> 
                        <ml:EncodedLiteral runat="server" id="litcorporateLabel"> </ml:EncodedLiteral>
                        <span runat="server" id="CorporateStartSeperator" visible="false" >&lt;</span><a id="corporateAddresLink" runat="server"></a><span runat="server" id="CorporateEndSeperator" visible="false" >&gt;</span>
                    </li> 
                    <li> 
                        <asp:RadioButton runat="server" GroupName="FromAddress" ID="sendToOtherRB"  onclick="SendFromOtherCheck(true)" />  
                        Other: &nbsp; 
                        <input runat="server" type="text" id="sendToOther" onblur="FromOtherBlur()"/>  
                       <img  alt="Required" runat="server" style="display:none" id="SendFromRequiredImg" src="~/images/require_icon.gif" />
                       <img  alt="Error" runat="server" style="display:none" id="SendFromErrorImg" src="~/images/error_icon.gif" /> 
                        <div id="sendFromOtherWarning">
                            Note: Email will be sent from <span id="sendFromOtherDefault"></span>&nbsp;On Behalf Of
                            <br /><span id="sendFromOtherOriginal"></span>. Contact your administrator for more info.
                        </div>
                    </li>
                </ul>
            <div class="ToHeading"> To: </div>
            <ul>
                <li>
                    <input type="checkbox" id="AENameCb" runat="server" /> 
                    <span id="AELabel" runat="server"> Assigned Account Executive: </span>
                    <span id="AEName" runat="server"></span>
                    <div  id="AEemailSection" class="EmailSection"> 
                    <span runat="server" id="AEEmaiLText"> Email :</span>
                    <a id="AEEmail" runat="server"></a>
                    </div>
                </li>
                <li>
                    <input type="checkbox" id="LoNameCb"  runat="server" /> 
                    <span id="LoLabel" runat="server"  >Official Loan Officer: </span>
                    <span id="LoName" runat="server"></span> 
                    <div class="EmailSection"> <span id="LoEmailText" runat="server"> Email:</span>
                        <a id="LoEmail" runat="server"></a> 
                    </div>
                 </li>
                <asp:PlaceHolder id="BpSection" runat="server">
                <li>
                    <input type="checkbox" id="BpNameCb" runat="server" /> 
                    <span id="BpLabel" runat="server">Official Processor (External): </span>
                    <span id="BpName" runat="server"></span> 
                    <div class="EmailSection"> <span id="BpEmailText" runat="server"> Email:</span>
                        <a id="BpEmail" runat="server"></a> 
                    </div>
                </li>
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="ExternalSection">
                <li>
                    <input type="checkbox" id="ExternalSecondaryNameCb" runat="server" /> 
                    <span id="ExternalSecondaryLabel" runat="server">Official Secondary (External):</span>
                    <span id="ExternalSecondaryName" runat="server"></span> 
                    <div class="EmailSection"> <span id="ExternalSecondaryEmailText" runat="server"> Email:</span>
                        <a id="ExternalSecondaryEmail" runat="server"></a> 
                    </div>
                </li>
                <li>
                    <input type="checkbox" id="ExternalPostCloserNameCb" runat="server" /> 
                    <span id="ExternalPostCloserLabel" runat="server">Official Post-Closer (External):</span>
                    <span id="ExternalPostCloserName" runat="server"></span> 
                    <div class="EmailSection"> <span id="ExternalPostCloserEmailText" runat="server"> Email:</span>
                        <a id="ExternalPostCloserEmail" runat="server"></a> 
                    </div>
                 </li>
                </asp:PlaceHolder>
                 <li>
                    <input type="checkbox" id="PrNameCb"  runat="server" /> 
                    <span id="PrLabel" runat="server"  >Official Processor: </span>
                    <span id="PrName" runat="server"></span> 
                    <div class="EmailSection"> <span id="PrEmailText" runat="server"> Email:</span>
                        <a id="PrEmail" runat="server"></a> 
                    </div>
                 </li>
                 <li>
                    <input type="checkbox" id="UnNameCb"  runat="server" /> 
                    <span id="UnLabel" runat="server"  >Official Underwriter: </span>
                    <span id="UnName" runat="server"></span> 
                    <div class="EmailSection"> <span id="UnEmailText" runat="server"> Email:</span>
                        <a id="UnEmail" runat="server"></a> 
                    </div>
                 </li>
                  <li class="TopOther">
                    
                      <div style="float: left">
                          <input type="checkbox" id="SOther" onclick="SendToOtherCheck(this)" />
                          <label for="sOtherEmails">
                              Other:
                          </label>
                          &nbsp;
                          <textarea type="text" readonly="readonly" id="sOtherEmails" style="display:inline" onblur="SendToOtherBlur()"
                              rows="5"></textarea>
                      </div>
                      <img alt="Required" runat="server" style="display: none; float:left;" id="SendToOtherRequired"
                          src="~/images/require_icon.gif" />
                      <img alt="Error" runat="server" style="display: none; float:left; vertical-align: top;" id="SendToOtherError"
                          src="~/images/error_icon.gif" />
                      <div style="float: left; margin-left: 10px; ">
                          <a href="#" onclick="return showPickFromContacts();" runat="server" id="picker">add
                              from contacts</a><br /> Separate multiple email <br />addresses with semicolons</div>
                      </div>
                      <div style="clear:both"></div>
                   </li>
                </ul>
            <p style="clear:both;">
                <label for="Comments"  > Comments :</label>
            </p>
            <p>    
                <textarea name="Comments" id="Comments" cols="55" rows="10" class="w-full"></textarea>   
            </p>
            <p style="padding-top: 20px; margin-left:165px" >
                <input type="button" NoHighlight  value="Send" onclick="f_email()" /> <input type="button"   NoHighlight style="margin-lefT: 10px" onclick="onClosePopup()" value="Cancel" />
            </p>
        </fieldset>
    </form>
  </body>
</html>
