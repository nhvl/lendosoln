namespace LendersOfficeApp.newlos.Underwriting
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;

    public class PmlReviewInfoServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(PmlReviewInfoServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            bool isStandAlone2ndLien = dataLoan.sIsStandAlone2ndLien;

            dataApp = dataLoan.GetAppData(0);

            dataApp.aProdBCitizenT = (E_aProdCitizenT) GetInt("aProdBCitizenT");
            dataApp.aBExperianScore_rep = GetString("aBExperianScore");
            dataApp.aBTransUnionScore_rep = GetString("aBTransUnionScore");
            dataApp.aBEquifaxScore_rep = GetString("aBEquifaxScore");
            dataLoan.sIsSelfEmployed = GetBool("sIsSelfEmployed");
            dataApp.aCExperianScore_rep = GetString("aCExperianScore");
            dataApp.aCTransUnionScore_rep = GetString("aCTransUnionScore");
            dataApp.aCEquifaxScore_rep = GetString("aCEquifaxScore");
            dataApp.aIsBorrSpousePrimaryWageEarner = GetBool("aIsBorrSpousePrimaryWageEarner");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sOccRLckd = GetBool("sOccRLckd");
            dataLoan.sOccR_rep = GetString("sOccR");
            dataLoan.sSpGrossRent_rep = GetString("sSpGrossRent");
            dataLoan.sHomeIsMhAdvantageTri = this.GetEnum<E_TriState>("sHomeIsMhAdvantageTri");

            // 10/13/2011 av - OPM 47246 - Update PML with Anti-flip guidelines and pricing
            dataLoan.sPriorSalesPrice_rep = GetString("sPriorSalesPrice");
            if (string.Empty == GetString("sPriorSalesPropertySellerT"))
            {
                dataLoan.sPriorSalesPropertySellerT = E_sPriorSalesPropertySellerT.Blank;
            }
            else
            {
                dataLoan.sPriorSalesPropertySellerT = (E_sPriorSalesPropertySellerT)GetInt("sPriorSalesPropertySellerT");
            }
           dataLoan.sPriorSalesD_rep = GetString("sPriorSalesD");

            // 3/29/2010 dd - OPM 43144 - We are now sync LO fields when updated the PML specific fields on Property Type.
            E_CalcModeT oldCalcModeT = dataLoan.CalcModeT;
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            dataLoan.sProdSpT = (E_sProdSpT) GetInt("sProdSpT");
            dataLoan.sIsNotPermanentlyAffixed = GetBool("sIsNotPermanentlyAffixed");
            dataLoan.sProdSpStructureT = (E_sProdSpStructureT) GetInt("sProdSpStructureT");
            dataLoan.sProdCondoStories_rep = GetString("sProdCondoStories");
            dataLoan.CalcModeT = oldCalcModeT;


            dataLoan.sProdIsSpInRuralArea = GetBool("sProdIsSpInRuralArea");
            dataLoan.sProdIsCondotel = GetBool("sProdIsCondotel");
            dataLoan.sProdIsNonwarrantableProj = GetBool("sProdIsNonwarrantableProj");
            if (!isStandAlone2ndLien)
            {
                if (BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId).HasEnabledPMI || dataLoan.IsNewPMLEnabled)
                {
                    // OPM 109393.
                    dataLoan.sProdConvMIOptionT = (E_sProdConvMIOptionT)GetInt("sProdConvMIOptionT");
                    dataLoan.sConvSplitMIRT = (E_sConvSplitMIRT) GetInt("sConvSplitMIRT");
                }
                else
                {
                    dataLoan.sProdMIOptionT = (E_sProdMIOptionT)GetInt("sProdMIOptionT");
                }

                //dataLoan.sProOFinPmtPe_rep = GetString("sProOFinPmtPe");
            }
            
            
            if ( !isStandAlone2ndLien )
				dataLoan.sLPurposeT = (E_sLPurposeT) GetInt("sLPurposeT");

            dataLoan.sIsStudentLoanCashoutRefi = GetBool("sIsStudentLoanCashoutRefi");
            dataLoan.sProdIsTexas50a6Loan = GetBool("sProdIsTexas50a6Loan");
            dataLoan.sPreviousLoanIsTexas50a6Loan = GetBool("sPreviousLoanIsTexas50a6Loan");
            dataLoan.sProdCashoutAmt_rep = GetString("sProdCashoutAmt");

            var broker = PrincipalFactory.CurrentPrincipal.BrokerDB;
            if (broker.IsEnableRenovationCheckboxInPML || broker.EnableRenovationLoanSupport)
            {
                dataLoan.sIsRenovationLoan = GetBool("sIsRenovationLoan");
                dataLoan.sTotalRenovationCosts_rep = dataLoan.sIsRenovationLoan ? GetString("sTotalRenovationCosts") : "$0.00";
            }
            dataLoan.sNumFinancedProperties_rep = GetString("sNumFinancedProperties");
			if ( !isStandAlone2ndLien )
			{

				//            dataLoan.sIsIOnly = GetBool("sIsIOnly");
				dataLoan.sProdImpound = GetBool("sProdImpound");
				dataLoan.sProdRLckdDays_rep = GetString("sProdRLckdDays");
			}

            dataLoan.sProdDocT = (E_sProdDocT) GetInt("sProdDocT");
            //dataLoan.sFannieDocT = CommonFunctions.ProductTToFannieDocT(dataLoan.sProdDocT, dataLoan.sFannieDocT); //OPM 58372
            dataLoan.sFinMethT = (E_sFinMethT) GetInt("sFinMethT");
            dataLoan.sProdAvailReserveMonths_rep = GetString("sProdAvailReserveMonths");
            dataLoan.sSpAddr = GetString("sSpAddr");
            dataLoan.sSpCity = GetString("sSpCity");
            dataLoan.sSpZip = GetString("sSpZip");
            dataLoan.sSpCounty = GetString("sSpCounty");

			dataLoan.sQualIR_rep = GetString("sQualIR");
            dataLoan.sQualIRLckd = this.GetBool(nameof(dataLoan.sQualIRLckd));
            dataLoan.sIsQRateIOnly = GetBool("sIsQRateIOnly");
			dataLoan.sIsOptionArm = GetBool("sIsOptionArm");
			dataLoan.sIOnlyMon_rep = GetString("sIOnlyMon");
			dataLoan.sNoteIR_rep = GetString("sNoteIR");
			dataLoan.sOptionArmTeaserR_rep = GetString("sOptionArmTeaserR");
			dataLoan.sLpTemplateNm = GetString("sLpTemplateNm");
            if (dataLoan.BrokerDB.ShowLoanProductIdentifier)
            {
                dataLoan.sLoanProductIdentifier = GetString("sLoanProductIdentifier");
            }

			// OPM 4442
			if (dataLoan.sIsStandAlone2ndLien)
			{
				dataLoan.sLpIsNegAmortOtherLien = GetBool("sLpIsNegAmortOtherLien");
				dataLoan.sOtherLFinMethT = (E_sFinMethT) GetInt("sOtherLFinMethT");
			}
            
            if (broker.IsAsk3rdPartyUwResultInPml) 
            {
                dataLoan.sProd3rdPartyUwResultT = (E_sProd3rdPartyUwResultT) GetInt("sProd3rdPartyUwResultT");
                dataLoan.sProdIncludeMyCommunityProc = GetBool("sProdIncludeMyCommunityProc");
                dataLoan.sProdIncludeHomePossibleProc = GetBool("sProdIncludeHomePossibleProc");
                dataLoan.sProdIncludeNormalProc = GetBool("sProdIncludeNormalProc");
                dataLoan.sProdIncludeFHATotalProc = GetBool("sProdIncludeFHATotalProc");
                dataLoan.sProdIncludeVAProc = GetBool("sProdIncludeVAProc");
                dataLoan.sProdIsDuRefiPlus = GetBool("sProdIsDuRefiPlus");
                dataLoan.sProdIncludeUSDARuralProc = GetBool("sProdIncludeUSDARuralProc");
            }
			if (broker.PmlRequireEstResidualIncome)
				dataLoan.sProdEstimatedResidualI_rep = GetString("sProdEstimatedResidualI");

            string sProdRLckdDays = GetString("sProdRLckdDays", string.Empty);
            if (!string.IsNullOrEmpty(sProdRLckdDays))
            {

                var brokerDB = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
                if (false == brokerDB.IsEnabledLockPeriodDropDown)
                {
                    dataLoan.sProdRLckdDays_rep = GetString("sProdRLckdDays");
                }
                else
                {
                    var lockPeriod = UInt32.Parse(GetString("sProdRLckdDays"));
                    if (brokerDB.AvailableLockPeriodOptions.Contains(lockPeriod) || lockPeriod == 0)
                    {
                        dataLoan.sProdRLckdDays_rep = lockPeriod.ToString();
                        // SK - allowing it to be 0 since other validation should prevent it from being 0 when really don't want it to be.
                    }
                    else
                    {
                        throw new KeyNotFoundException(lockPeriod + " is not one of the available lock period options");
                    }
                }
            }

            dataLoan.sCustomPMLField1_rep = GetString("sCustomPMLField1", "");
            dataLoan.sCustomPMLField2_rep = GetString("sCustomPMLField2", "");
            dataLoan.sCustomPMLField3_rep = GetString("sCustomPMLField3", "");
            dataLoan.sCustomPMLField4_rep = GetString("sCustomPMLField4", "");
            dataLoan.sCustomPMLField5_rep = GetString("sCustomPMLField5", "");
            dataLoan.sCustomPMLField6_rep = GetString("sCustomPMLField6", "");
            dataLoan.sCustomPMLField7_rep = GetString("sCustomPMLField7", "");
            dataLoan.sCustomPMLField8_rep = GetString("sCustomPMLField8", "");
            dataLoan.sCustomPMLField9_rep = GetString("sCustomPMLField9", "");
            dataLoan.sCustomPMLField10_rep = GetString("sCustomPMLField10", "");
            dataLoan.sCustomPMLField11_rep = GetString("sCustomPMLField11", "");
            dataLoan.sCustomPMLField12_rep = GetString("sCustomPMLField12", "");
            dataLoan.sCustomPMLField13_rep = GetString("sCustomPMLField13", "");
            dataLoan.sCustomPMLField14_rep = GetString("sCustomPMLField14", "");
            dataLoan.sCustomPMLField15_rep = GetString("sCustomPMLField15", "");
            dataLoan.sCustomPMLField16_rep = GetString("sCustomPMLField16", "");
            dataLoan.sCustomPMLField17_rep = GetString("sCustomPMLField17", "");
            dataLoan.sCustomPMLField18_rep = GetString("sCustomPMLField18", "");
            dataLoan.sCustomPMLField19_rep = GetString("sCustomPMLField19", "");
            dataLoan.sCustomPMLField20_rep = GetString("sCustomPMLField20", "");

            dataLoan.sQualTermCalculationType = (QualTermCalculationType)GetInt("sQualTermCalculationType");
            dataLoan.sQualTerm_rep = GetString("sQualTerm");

            if (broker.IsLenderFeeBuyoutEnabledForBranchChannel(dataLoan.sBranchChannelT))
            {
                dataLoan.sLenderFeeBuyoutRequestedT = (E_sLenderFeeBuyoutRequestedT)GetInt("sLenderFeeBuyoutRequestedT");
            }

            PmlReviewInfo.BindDataFromControls(dataLoan, dataApp, this);

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("sProdIsTexas50a6Loan", dataLoan.sProdIsTexas50a6Loan);
            SetResult("sPreviousLoanIsTexas50a6Loan", dataLoan.sPreviousLoanIsTexas50a6Loan);
            SetResult("sIsStudentLoanCashoutRefi", dataLoan.sIsStudentLoanCashoutRefi);
            SetResult("sSpGrossRent", dataLoan.sSpGrossRent_rep);
            SetResult("sOccR", dataLoan.sOccR_rep);
            this.SetResult(nameof(dataLoan.sQualIR), dataLoan.sQualIR_rep);
            this.SetResult(nameof(dataLoan.sQualIRLckd), dataLoan.sQualIRLckd);
            this.SetResult(nameof(dataLoan.sIsQRateIOnly), dataLoan.sIsQRateIOnly);
            SetResult("sQualTermCalculationType", dataLoan.sQualTermCalculationType);
            SetResult("sQualTerm", dataLoan.sQualTerm_rep);

            // For PML, sHomeIsMhAdvantageTri will use the value of sProdSpT.
            // Ensure this behavior is replicated in the UI.
            var oldCalcMode = dataLoan.CalcModeT;
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            this.SetResult("sHomeIsMhAdvantageTri", dataLoan.sHomeIsMhAdvantageTri, forDropdown: true);
            dataLoan.CalcModeT = oldCalcMode;

            PmlReviewInfo.LoadDataForControls(dataLoan, dataApp, this);
        }
    }

    public partial class PmlReviewInfoService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new PmlReviewInfoServiceItem());
        }
	}
}
