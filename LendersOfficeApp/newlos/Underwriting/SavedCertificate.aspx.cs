using System;
using System.Web.UI;
using System.Xml;
using System.Xml.Xsl;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.ObjLib.Resource;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOfficeApp.newlos.Underwriting
{

    public partial class SavedCertificate : LendersOffice.Common.BaseXsltPage
	{
        private string m_pmlLenderSiteId = "";

        protected override string XsltFileLocation 
        {
            get
            {
                bool forceNoCustomCert = RequestHelper.GetBool("detailCert") && (Page.User as AbstractUserPrincipal)?.CanViewDualCertificate == true;
                return ResourceManager.Instance.GetResourcePath(ResourceType.PmlCertificateXslt, BrokerID, forceNoCustomCert);
            }
        }

        private Guid LoanID 
        {
            get { return RequestHelper.LoanID; }
        }
        private Guid BrokerID 
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
        }


        protected override XsltArgumentList XsltParams 
        {
            get 
            {
                XsltArgumentList args = new XsltArgumentList();
				args.AddParam("VirtualRoot", "", Tools.VRoot);
				args.AddParam("LenderPmlSiteId", "", m_pmlLenderSiteId);

                AbstractUserPrincipal principal = Page.User as AbstractUserPrincipal;
                // 1/26/2006 dd - OPM 3825 - Allow users with Admin & LockDesk role to see hidden adjustments.
                if (principal.ApplicationType == E_ApplicationT.LendersOffice && (principal.HasPermission(LendersOffice.Security.Permission.CanModifyLoanPrograms) ||
                    principal.HasRole(E_RoleT.Administrator) || principal.HasRole(E_RoleT.LockDesk))) 
                {
                    args.AddParam("ShowHiddenAdj", "", "True");
                }

                if (PrincipalFactory.CurrentPrincipal.BrokerDB.HidePmlCertSensitiveFields)
                {
                    args.AddParam("HideSensitiveFields", "", "True");
                }

                // 10/11/07 mf. OPM 18269 - Users with new permission can see hidden stips (conditions)
                if (principal.HasPermission( Permission.CanViewHiddenInformation ) ||
					principal.HasPermission( Permission.CanModifyLoanPrograms ) )
				{
					args.AddParam("ShowHiddenConditions", "", "True");
				}

                if (ConstSite.DisplayPolicyAndRuleIdOnLpeResult) 
                {
                    args.AddParam("ShowDebugInfo", "", "True");
                }
                return args;
            }
        }
        protected override void InitXslt() 
        {
            // 11/10/2004 dd - Theoretically, PmlLenderSiteId should be in loan product.
            // This way we allow independent broker to run qualification against multiple lender.
            // However we are not there yet. Retrieve from current broker.

            Guid brokerPmlSiteId = BrokerUserPrincipal.CurrentPrincipal.BrokerDB.PmlSiteID;
            m_pmlLenderSiteId = Tools.GetFileDBKeyForPmlLogoWithLoanId(brokerPmlSiteId, LoanID);
        }
        protected override void GenerateXmlData(XmlWriter writer) 
        {

            CPageData dataLoan = new CSavedCertificateData(LoanID);
            dataLoan.InitLoad();

            if (dataLoan.sPmlCertXmlContent.Value != "") 
            {

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(dataLoan.sPmlCertXmlContent.Value);
                if (ConstSite.DisplayPolicyAndRuleIdOnLpeResult == false)
                {

                    doc = UnderwritingResultItem.DedupAdjustmentDescription(doc);
                }
                doc.WriteTo(writer);
            } 
            else 
            {
                writer.WriteRaw("<NoData />");
            }
        }
	}
}
