﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckEligibility.aspx.cs" Inherits="LendersOfficeApp.newlos.Underwriting.CheckEligibility" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %> 
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
   <link type="text/css" href="~/css/quickpricer.css" rel="Stylesheet" runat="server"/>    
</head>
<body>
    <form id="form1" runat="server">
        <script type="text/javascript">
            <!--
          function switchApplicant() {
            // No-op. There are no borrower-specific fields on this page.
          }
            
          function btnGetResults_onclick()
          {
            f_saveMe();
            var sPricingModeT = '';
            if (<%= AspxTools.JsGetElementById(m_pricingOptionBroker) %>.checked)
            {
              sPricingModeT = <%= AspxTools.JsString(E_sPricingModeT.EligibilityBrokerPricing) %>;
            }
            else if (<%= AspxTools.JsGetElementById(m_pricingOptionInvestor) %>.checked)
            {
              sPricingModeT = <%= AspxTools.JsString(E_sPricingModeT.EligibilityInvestorPricing) %>;
            }
                        var extraQuery = '';
            <% if (IsTestHistorical) { %>

             extraQuery += '&historicalPolicy=' + encodeURIComponent(<%= AspxTools.JsGetElementById(historicalPolicy) %>.value);
             extraQuery += '&historicalRateOption=' + encodeURIComponent(<%= AspxTools.JsGetElementById(historicalRateOption) %>.value);
            <% } %>
            location.href = 'CheckEligibilityResult.aspx?loanid=' + ML.sLId + '&sPricingModeT=' + sPricingModeT + extraQuery;
          }
            function investorPricingOptionChanged() {
      
            }
            
            function pricingOptionChanged() {

            }
            
            function _init() {                
                document.getElementById('sFinMethT').disabled = true;

                pricingOptionChanged();
            }
            //-->
        </script>
        <fieldset id="loanInfo" class="QuickPricerFieldSet">
            <legend class="LegendSectionHeader">Loan Information</legend>
            <table>
                <tr>
                    <td>
                        <label id="lbl_sLpTemplateNm" for="sLpTemplateNm">Loan Program</label>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="sLpTemplateNm" Width="405px" runat="server" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label id="lbl_sLpProductType" for="sLpProductType">Registered Product Type</label>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="sLpProductType" Width="405px" runat="server" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label id="lbl_sFinMethT" for="sFinMethT">Amortization Type</label>
                    </td>
                    <td>
                        <asp:DropDownList ID="sFinMethT" runat="server" />
                    </td>
                    <td>
                        <label id="lbl_sTerm" for="sTerm">Term/Due (mnths)</label>                        
                    </td>
                    <td>
                        <asp:TextBox ID="sTerm" Width="50" runat="server" ReadOnly="true" /> /
                        <asp:TextBox ID="sDue" Width="50" runat="server" ReadOnly="true" />                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <label id="lbl_sNoteIR" for="sNoteIR">Note Rate</label>
                    </td>
                    <td>
                        <asp:TextBox ID="sNoteIR" Width="50" runat="server" ReadOnly="true" />
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td>
                        <label id="lbl_sRLckdD" for="sRLckdD">Loan officer rate lock date</label>
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" CssClass="dateField" ID="sRLckdD" ReadOnly="true" />
                    </td>
                    <td>
                        <label id="lbl_sRLckdDays" for="sRLckdDays">Lock period at rate lock</label>                        
                    </td>
                    <td>
                        <asp:TextBox ID="sRLckdDays" Width="50" runat="server" ReadOnly="true" /> days
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <label id="lbl_sRLckdExpiredD" for="sRLckdExpiredD">Loan officer rate lock expiration</label>
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" CssClass="dateField" ID="sRLckdExpiredD" ReadOnly="true" />
                    </td>
                    <td>
                        <label id="lbl_sRLckdExpiredD1" for="sRLckdExpiredInDays">Rate lock expires in</label>                        
                    </td>
                    <td>
                        <asp:TextBox ID="sRLckdExpiredInDays" Width="50" runat="server" ReadOnly="true"/> days
                    </td>  
                </tr>
            </table>
        </fieldset>
        <fieldset id="pricingOptions" class="QuickPricerFieldSet">
            <legend class="LegendSectionHeader">Pricing Options</legend>
                        <table id="tmpHistoricalTable" runat="server" visible="false" style="border:solid 2px red">
            <tr>
                <td colspan="2" style="color:Red; font-weight:bold">HISTORICAL PRICING EXPERIMENT</td>
            </tr>
            <tr>
                <td>Pricing Data Version</td>
                <td><asp:TextBox ID="sHistoricalPricingVersionD" runat=server ReadOnly=true /></td>
            </tr>
            <tr>
            </tr>
            <tr>
            <td>Policy</td>
            <td><asp:DropDownList ID="historicalPolicy" runat="server" /></td>
            </tr>
            <tr>
            <td>Rate Option</td>
            <td><asp:DropDownList ID="historicalRateOption" runat="server" /></td>
            </tr>
            </table>
            <table>
                <tr>
                    <td colspan="4">
                        <asp:RadioButton ID="m_pricingOptionBroker" GroupName="pricingOption" onclick="pricingOptionChanged()" runat="server" Checked="true" Text="Use settings for Front-end results" />
                    </td>
                </tr>
                <tr>
                    <td style="width:30px"></td>
                    <td>
                        <label id="lbl_sProdRLckdDays" for="sProdRLckdDays">Front-end rate lock period</label>
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="sProdRLckdDays" Width="50" runat="server" ReadOnly="true"/> days
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <label id="lbl_tbd">Price group</label>                        
                    </td>
                    <td colspan="2">
                      <asp:TextBox ID="sProdLpePriceGroupNm" runat="server" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:RadioButton ID="m_pricingOptionInvestor" GroupName="pricingOption" onclick="pricingOptionChanged()" runat="server" Text="Use settings for investor results" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <label id="lbl_sProdInvestorRLckdDays" for="sProdInvestorRLckdDays">Investor rate lock period</label>                        
                    </td> 
                    <td>
                        <asp:TextBox ID="sProdInvestorRLckdDays" Width="50" runat="server" ReadOnly="true"/> days
                    </td>
                    <td>
                        <asp:RadioButton ID="m_editRatePeriod" GroupName="sProdInvestorRLckdModeT" Checked="true" onclick="investorPricingOptionChanged()" runat="server" Text="Edit" value="0" Enabled="false"/>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <label id="lbl_sProdInvestorRLckdExpiredD" for="sProdInvestorRLckdExpiredD">Investor rate lock would<br />expire on</label>                        
                    </td> 
                    <td>
                        <ml:DateTextBox runat="server" CssClass="dateField" ID="sProdInvestorRLckdExpiredD" readonly="true" />
                    </td>
                    <td>
                        <asp:RadioButton ID="m_editRateDate" GroupName="sProdInvestorRLckdModeT" onclick="investorPricingOptionChanged()" runat="server" Text="Edit" value="1" Enabled="false"/>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <label id="lbl_tbd3" for="sInvestorLockLpePriceGroupId">Price group</label>                        
                    </td> 
                    <td colspan="2">
                        <asp:DropDownList ID="sInvestorLockLpePriceGroupId" runat="server" Enabled="false"/>
                    </td>
                </tr>
            </table>
        </fieldset>
        <input type="button" id="btnGetResults" value="Get Results >>" onclick="btnGetResults_onclick();" />
    </form>
</body>
</html>
