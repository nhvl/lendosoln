﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.Underwriting
{
    public partial class CommunityLendingServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(CommunityLendingServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sCommunityLendingDownPaymentSource1Amt_rep = GetString("sCommunityLendingDownPaymentSource1Amt");
            dataLoan.sCommunityLendingDownPaymentSource2Amt_rep = GetString("sCommunityLendingDownPaymentSource2Amt");
            dataLoan.sCommunityLendingDownPaymentSource3Amt_rep = GetString("sCommunityLendingDownPaymentSource3Amt");
            dataLoan.sCommunityLendingDownPaymentSource4Amt_rep = GetString("sCommunityLendingDownPaymentSource4Amt");
            dataLoan.sCommunityLendingClosingCost1Amt_rep = GetString("sCommunityLendingClosingCost1Amt");
            dataLoan.sCommunityLendingClosingCost2Amt_rep = GetString("sCommunityLendingClosingCost2Amt");
            dataLoan.sCommunityLendingClosingCost3Amt_rep = GetString("sCommunityLendingClosingCost3Amt");
            dataLoan.sCommunityLendingClosingCost4Amt_rep = GetString("sCommunityLendingClosingCost4Amt");

            dataLoan.sCommunityLendingDownPaymentSource1T = (E_sCommunityLendingDownPaymentSourceT)GetInt("sCommunityLendingDownPaymentSource1T");
            dataLoan.sCommunityLendingDownPaymentSource2T = (E_sCommunityLendingDownPaymentSourceT)GetInt("sCommunityLendingDownPaymentSource2T");
            dataLoan.sCommunityLendingDownPaymentSource3T = (E_sCommunityLendingDownPaymentSourceT)GetInt("sCommunityLendingDownPaymentSource3T");
            dataLoan.sCommunityLendingDownPaymentSource4T = (E_sCommunityLendingDownPaymentSourceT)GetInt("sCommunityLendingDownPaymentSource4T");
            dataLoan.sCommunityLendingDownPayment1T = (E_sCommunityLendingDownPaymentT)GetInt("sCommunityLendingDownPayment1T");
            dataLoan.sCommunityLendingDownPayment2T = (E_sCommunityLendingDownPaymentT)GetInt("sCommunityLendingDownPayment2T");
            dataLoan.sCommunityLendingDownPayment3T = (E_sCommunityLendingDownPaymentT)GetInt("sCommunityLendingDownPayment3T");
            dataLoan.sCommunityLendingDownPayment4T = (E_sCommunityLendingDownPaymentT)GetInt("sCommunityLendingDownPayment4T");
            dataLoan.sCommunityLendingClosingCostSource1T = (E_sCommunityLendingClosingCostSourceT)GetInt("sCommunityLendingClosingCostSource1T");
            dataLoan.sCommunityLendingClosingCostSource2T = (E_sCommunityLendingClosingCostSourceT)GetInt("sCommunityLendingClosingCostSource2T");
            dataLoan.sCommunityLendingClosingCostSource3T = (E_sCommunityLendingClosingCostSourceT)GetInt("sCommunityLendingClosingCostSource3T");
            dataLoan.sCommunityLendingClosingCostSource4T = (E_sCommunityLendingClosingCostSourceT)GetInt("sCommunityLendingClosingCostSource4T");
            dataLoan.sCommunityLendingClosingCost1T = (E_sCommunityLendingClosingCostT)GetInt("sCommunityLendingClosingCost1T");
            dataLoan.sCommunityLendingClosingCost2T = (E_sCommunityLendingClosingCostT)GetInt("sCommunityLendingClosingCost2T");
            dataLoan.sCommunityLendingClosingCost3T = (E_sCommunityLendingClosingCostT)GetInt("sCommunityLendingClosingCost3T");
            dataLoan.sCommunityLendingClosingCost4T = (E_sCommunityLendingClosingCostT)GetInt("sCommunityLendingClosingCost4T");

            dataApp.aBHomeOwnershipCounselingSourceT = (E_aHomeOwnershipCounselingSourceT)GetInt("aBHomeOwnershipCounselingSourceT");
            dataApp.aCHomeOwnershipCounselingSourceT = (E_aHomeOwnershipCounselingSourceT)GetInt("aCHomeOwnershipCounselingSourceT");
            dataApp.aBHomeOwnershipCounselingFormatT = (E_aHomeOwnershipCounselingFormatT)GetInt("aBHomeOwnershipCounselingFormatT");
            dataApp.aCHomeOwnershipCounselingFormatT = (E_aHomeOwnershipCounselingFormatT)GetInt("aCHomeOwnershipCounselingFormatT");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sCommunityLendingDownPaymentSource1Amt", dataLoan.sCommunityLendingDownPaymentSource1Amt_rep);
            SetResult("sCommunityLendingDownPaymentSource2Amt", dataLoan.sCommunityLendingDownPaymentSource2Amt_rep);
            SetResult("sCommunityLendingDownPaymentSource3Amt", dataLoan.sCommunityLendingDownPaymentSource3Amt_rep);
            SetResult("sCommunityLendingDownPaymentSource4Amt", dataLoan.sCommunityLendingDownPaymentSource4Amt_rep);

            SetResult("sCommunityLendingClosingCost1Amt", dataLoan.sCommunityLendingClosingCost1Amt_rep);
            SetResult("sCommunityLendingClosingCost2Amt", dataLoan.sCommunityLendingClosingCost2Amt_rep);
            SetResult("sCommunityLendingClosingCost3Amt", dataLoan.sCommunityLendingClosingCost3Amt_rep);
            SetResult("sCommunityLendingClosingCost4Amt", dataLoan.sCommunityLendingClosingCost4Amt_rep);

            SetResult("sCommunityLendingDownPaymentSource1T", dataLoan.sCommunityLendingDownPaymentSource1T);
            SetResult("sCommunityLendingDownPaymentSource2T", dataLoan.sCommunityLendingDownPaymentSource2T);
            SetResult("sCommunityLendingDownPaymentSource3T", dataLoan.sCommunityLendingDownPaymentSource3T);
            SetResult("sCommunityLendingDownPaymentSource4T", dataLoan.sCommunityLendingDownPaymentSource4T);

            SetResult("sCommunityLendingDownPayment1T", dataLoan.sCommunityLendingDownPayment1T);
            SetResult("sCommunityLendingDownPayment2T", dataLoan.sCommunityLendingDownPayment2T);
            SetResult("sCommunityLendingDownPayment3T", dataLoan.sCommunityLendingDownPayment3T);
            SetResult("sCommunityLendingDownPayment4T", dataLoan.sCommunityLendingDownPayment4T);

            SetResult("sCommunityLendingClosingCostSource1T", dataLoan.sCommunityLendingClosingCostSource1T);
            SetResult("sCommunityLendingClosingCostSource2T", dataLoan.sCommunityLendingClosingCostSource2T);
            SetResult("sCommunityLendingClosingCostSource3T", dataLoan.sCommunityLendingClosingCostSource3T);
            SetResult("sCommunityLendingClosingCostSource4T", dataLoan.sCommunityLendingClosingCostSource4T);

            SetResult("sCommunityLendingClosingCost1T", dataLoan.sCommunityLendingClosingCost1T);
            SetResult("sCommunityLendingClosingCost2T", dataLoan.sCommunityLendingClosingCost2T);
            SetResult("sCommunityLendingClosingCost3T", dataLoan.sCommunityLendingClosingCost3T);
            SetResult("sCommunityLendingClosingCost4T", dataLoan.sCommunityLendingClosingCost4T);

            SetResult("aBHomeOwnershipCounselingSourceT", dataApp.aBHomeOwnershipCounselingSourceT);
            SetResult("aCHomeOwnershipCounselingSourceT", dataApp.aCHomeOwnershipCounselingSourceT);
            SetResult("aBHomeOwnershipCounselingFormatT", dataApp.aBHomeOwnershipCounselingFormatT);
            SetResult("aCHomeOwnershipCounselingFormatT", dataApp.aCHomeOwnershipCounselingFormatT);
        }
    }
    public partial class CommunityLendingService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new CommunityLendingServiceItem());
        }
    }
}
