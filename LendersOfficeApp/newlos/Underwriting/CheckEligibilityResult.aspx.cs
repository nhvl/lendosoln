﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using System.Web.UI.HtmlControls;

namespace LendersOfficeApp.newlos.Underwriting
{
    public partial class CheckEligibilityResult : BaseLoanPage
    {
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new EventHandler(PageInit);
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int sPricingModeT = RequestHelper.GetInt("sPricingModeT");
            RegisterJsGlobalVariables("sPricingModeT", sPricingModeT);
            RegisterJsGlobalVariables("StartTicks", DateTime.Now.Ticks.ToString());


            int historicalPolicy = RequestHelper.GetInt("historicalPolicy", 0);
            int historicalRateOption = RequestHelper.GetInt("historicalRateOption", 0);


            RegisterJsGlobalVariables("historicalPolicy", historicalPolicy);
            RegisterJsGlobalVariables("historicalRateOption", historicalRateOption);
        }

        void PageInit(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("json.js");
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterService("main", "/newlos/Underwriting/CheckEligibilityResultService.aspx");

            this.DisplayCopyRight = false;
            this.IncludeStyleSheet("~/css/quickpricer.css");
        }
    }
}
