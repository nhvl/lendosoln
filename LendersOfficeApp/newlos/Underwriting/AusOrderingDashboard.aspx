﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AusOrderingDashboard.aspx.cs" Inherits="LendersOfficeApp.newlos.Underwriting.AusOrderingDashboard" EnableEventValidation="true"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<!DOCTYPE HTML>
<html>
<head runat="server">
    <title>AUS Ordering Dashboard</title>
    <style type="text/css">
        .hidden {
            display: none;
        }

        .MainPageTable {
            border: 0;
            width: 100%;
            padding: 0;
            border-spacing: 0;
        }

        .AusTable table {
            border-spacing: 0;
        }

        .AusTable .LoanFormHeader {
            margin-bottom: 0;
        }

        .AusTable {
            margin-top: 1.5em;
        }

        .AusTable td, .AusTable th, .PageRow {
            padding: 5px;
        }

        th {
            text-align: left;
        }

        .ReorderArrow
        {
            text-decoration: none;
            color: #000080;
        }

        .AusSectionControl {
            width: 100px;
        }

        .ResultSectionControl {
            width: 240px;
        }

        .CaseIdControl {
            width: 100px;
        }

        .RunByControl {
            width: 125px;
        }

        .NotesControl {
            width: 300px;
        }

        .UnderwritingServiceControl {
            width: 50px;
        }

        .FeeLevelControl {
            width: 25px;
        }

        .TimestampControl {
            width: 160px;
        }

        .FrontSpacerColumn {
            width: 5px;
        }

        .ReorderColumn {
            width: 20px;
        }

        .AusColumn {
            width: 110px;
        }

        .ResultDescriptionColumn {
            width: 130px;
        }

        .ResultControlColumn {
            width: 250px;
        }

        .CaseIdColumn {
            width: 110px;
        }

        .RunByColumn {
            width: 135px;
        }

        .TimestampColumn {
            width: 170px;
        }

        .NotesColumn {
            width: 310px;
        }

        .RemoveColumn {
            width: 35px;
        }

        .BackSpacerColumn { 
            width: 10px;
        }

        div.LoanFormHeader {
            margin-bottom: 5px;
            padding: 5px;
        }
    </style>
</head>
<body class="RightBackground">
    <script language="javascript" type="text/javascript">
        function _init() {
            if ($('#PageIsDirty').is(':checked')) {
                updateDirtyBit();
            }
        }

        var oldRefreshCalculation = window.refreshCalculation;
        window.refreshCalculation = function () {
            populateAusOrderListJson();
            oldRefreshCalculation();
            updateAusOrderTable();
        }

        var oldSaveMe = window.saveMe;
        window.saveMe = function (bRefreshScreen) {
            populateAusOrderListJson();
            return oldSaveMe(bRefreshScreen);
        }

        registerPostSaveMeCallback(function() {
            window.location.href = window.location.href;
        });

        function populateAusOrderListJson() {
            var ausList = [];

            var emptyGuid = "00000000-0000-0000-0000-000000000000";
            $('tr.AusOrderRow').each(function () {
                var row = $(this);

                var isManualField = row.find("input[id$='IsManual']");
                var isManual = isManualField.length ? isManualField.is(':checked') : true;

                var ausOrder = {};
                ausOrder.AusOrderId = parseInt(row.find("input[id$='AusOrderId']").val());
                ausOrder.CustomPosition = parseInt(row.find("input[id$='CustomPosition']").val()) || 0;
                ausOrder.BrokerId = row.find("input[id$='AusBrokerId']").val() || emptyGuid;
                ausOrder.LoanId = row.find("input[id$='AusLoanId']").val() || emptyGuid;
                ausOrder.IsManual = isManual,
                ausOrder.UnderwritingService = parseInt(row.find("input[id$='UnderwritingServiceValue']").val()) || 0;
                ausOrder.LoanIsFha = row.find("input[id$='LoanIsFha']").val() === 'True';

                var underwritingServiceOtherDescription = row.find("input[id$='UnderwritingServiceOtherDescription']").val();
                ausOrder.UnderwritingServiceOtherDescription = underwritingServiceOtherDescription;

                var duRecommendation = row.find("select[id$='DuRecommendationDropdown']").val();
                ausOrder.DuRecommendation = parseInt(duRecommendation) || 0;

                var lpRiskClass = row.find("select[id$='LpRiskClassDropdown']").val();
                ausOrder.LpRiskClass = parseInt(lpRiskClass) || 0;

                var lpPurchaseEligibility = row.find("select[id$='LpPurchaseEligibilityDropdown']").val();
                ausOrder.LpPurchaseEligibility = parseInt(lpPurchaseEligibility) || 0;

                var lpFeeLevel = row.find("input[id$='LpFeeLevelDropdown']").val();
                ausOrder.LpFeeLevel = lpFeeLevel;

                var lpStatus = row.find("select[id$='LpStatusDropdown']").val();
                ausOrder.LpStatus = parseInt(lpStatus) || 0;

                var totalRiskClass = row.find("select[id$='TotalRiskClassDropdown']").val();
                ausOrder.TotalRiskClass = parseInt(totalRiskClass) || 0;

                var totalEligibilityAssessment = row.find("select[id$='TotalEligibilityAssessmentDropdown']").val();
                ausOrder.TotalEligibilityAssessment = parseInt(totalEligibilityAssessment) || 0;

                var gusRecommendation = row.find("select[id$='GusRecommendationDropdown']").val();
                ausOrder.GusRecommendation = parseInt(gusRecommendation) || 0;

                var gusRiskEvaluation = row.find("select[id$='GusRiskEvaluationDropdown']").val();
                ausOrder.GusRiskEvaluation = parseInt(gusRiskEvaluation) || 0;

                var resultOtherDescription = row.find("input[id$='ResultOtherDescription']").val();
                ausOrder.ResultOtherDescription = resultOtherDescription;

                ausOrder.CaseId = row.find("input[id$='CaseId']").val();
                ausOrder.FindingsDocument = row.find("input[id$='FindingsDocument']").val() || emptyGuid;
                ausOrder.OrderPlacedBy = row.find("input[id$='OrderRunBy']").val();

                var timestampId = isManual ? "ManualTimestamp" : "Timestamp";
                var timestampValue = row.find("input[id$='" + timestampId + "']").val();
                if (isManual && isNaN(Date.parse(timestampValue))) {
                    // An invalid date has been entered. We can fall back to the
                    // non-manual timestamp as a cached value.
                    timestampValue = row.find("input[id$='Timestamp']").val();
                }

                ausOrder.Timestamp_rep = timestampValue.replace("PST", "").replace("PDT", "");
                ausOrder.Notes = row.find("textarea[id$='Notes']").val();

                ausList.push(ausOrder);
            });

            $('#AusOrderListJson').val(JSON.stringify(ausList));
        }

        function onAddAusRunClicked() {
            updateAusRunTable("add", null);
        }

        function onRemoveClick(ausRunnerId) {
            var message = "Are you sure you want to delete this AUS run?";
            if (confirm(message)) {
                updateAusRunTable("remove", ausRunnerId);
            }
        }

        function updateModel() {
            updateAusRunTable("update", null);
        }

        function moveOrder(currentIndex, nextIndex) {
            var argument = currentIndex + '|' + nextIndex;
            updateAusRunTable("move", argument);
        }

        function updateAusRunTable(command, argument) {
            populateAusOrderListJson();
            __doPostBack(command, argument);
        }

        function openDuSeamless() {
            LQBPopup.Show(ML.VirtualRoot + '/newlos/SeamlessDU/SeamlessDu.aspx#/audit?loanid=' + ML.sLId, { height: 800, width: '80%', hideCloseButton: true });
        }

        function openLpaSeamless() {
            LQBPopup.Show(ML.VirtualRoot + '/newlos/SeamlessLPA/SeamlessLpa.aspx#/audit?loanid=' + ML.sLId, { height: 800, width: '80%', hideCloseButton: true });
        }
    </script>
    <form id="AusOrderingDashboard" method="post" runat="server">
        <table class="FormTable MainPageTable">
            <tr>
                <td class="MainRightHeader no-wrap">AUS Ordering Dashboard</td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox runat="server" ID="PageIsDirty" class="hidden" />
                    <asp:HiddenField runat="server" ID="AusOrderListJson" />
                    <div class="AusTable">
                    <div class="LoanFormHeader">AUS Tracking</div>
                        <table id="AusOrderTable" class="no-wrap">
                            <tr class="bg-subheader">
                                <th class="FrontSpacerColumn"></th>
                                <th class="ReorderColumn"></th>
                                <th class="AusColumn">AUS</th>
                                <th class="ReportColumn" id="ReportedAsColumnHeader" runat="server">Reported As</th>
                                <th class="ResultDescriptionColumn">Result</th>
                                <th class="ResultControlColumn"></th>
                                <th class="CaseIdColumn">Case ID</th>
                                <th class="RunByColumn">Run By</th>
                                <th class="TimestampColumn">Timestamp</th>
                                <th class="NotesColumn">Notes</th>
                                <th class="RemoveColumn"></th>
                                <th class="BackSpacerColumn"></th>
                            </tr>
                            <asp:Repeater runat="server" ID="AusOrderList" OnItemDataBound="OnAusOrderBound">
                                <ItemTemplate>
                                    <tr class="GridAutoItem AusOrderRow">
                                        <td class="FrontSpacerColumn"></td>
                                        <td class="no-wrap ReorderColumn">
                                            <asp:HyperLink runat="server" class="ReorderArrow" ID="upArrow" onclick='<%# "return moveOrder(" + AspxTools.JsString(Container.ItemIndex) + "," + AspxTools.JsString((Container.ItemIndex - 1)) + ")" %>'>&#x25b2;</asp:HyperLink>
                                            <asp:HyperLink runat="server" class="ReorderArrow" ID="downArrow" onclick='<%# "return moveOrder(" + AspxTools.JsString(Container.ItemIndex) + "," + AspxTools.JsString((Container.ItemIndex + 1)) + ")" %>'>&#x25bc;</asp:HyperLink>
                                        </td>
                                        <td class="AusColumn">
                                            <asp:HiddenField runat="server" ID="AusOrderId" />
                                            <asp:HiddenField runat="server" ID="CustomPosition" />
                                            <asp:HiddenField runat="server" ID="AusBrokerId" />
                                            <asp:HiddenField runat="server" ID="AusLoanId" />
                                            <asp:Checkbox runat="server" ID="IsManual" class="hidden" />
                                            <asp:TextBox runat="server" ID="UnderwritingServiceFriendlyName" class="UnderwritingServiceControl" />
                                            <asp:HiddenField runat="server" ID="UnderwritingServiceValue" />
                                            <asp:HiddenField runat="server" ID="LoanIsFha" />
                                            <span runat="server" id="OtherUnderwritingService" >
                                                <br />
                                                Other Description
                                                <br />
                                                <asp:TextBox runat="server" ID="UnderwritingServiceOtherDescription" MaxLength="50" class="AusSectionControl" />
                                            </span>
                                        </td>
                                        <td class="ReportColumn" id="ReportAsColumn" runat="server">
                                            <ml:EncodedLiteral runat="server" ID="ReportAs" />
                                        </td>
                                        <td class="no-wrap FieldLabel ResultDescriptionColumn">
                                            <span runat="server" id="DuDescriptions" >
                                                Recommendation:
                                            </span>
                                            <span runat="server" id="LpDescriptions" >
                                                Risk Class:
                                                <br />
                                                Purchase Eligibility:
                                                <br />
                                                <span runat="server" id="LpFeeLevelDescription">
                                                    CS/LTV Fee Level:
                                                    <br />
                                                </span>
                                                AUS Status:
                                            </span>
                                            <span runat="server" id="TotalDescriptions" >
                                                Risk Class:
                                                <br />
                                                Eligibility Assessment:
                                            </span>
                                            <span runat="server" id="GusDescriptions" >
                                                Recommendation:
                                                <br />
                                                Risk Evaluation:
                                            </span>
                                            <span runat="server" id="OtherAusDescriptions" >
                                                Description:
                                            </span>
                                        </td>
                                        <td class="ResultControlColumn">
                                            <span runat="server" id="DuControls" >
                                                <asp:DropDownList runat="server" ID="DuRecommendationDropdown" class="ResultSectionControl" />
                                            </span>
                                            <span runat="server" id="LpControls" >
                                                <asp:DropDownList runat="server" ID="LpRiskClassDropdown" class="ResultSectionControl" />
                                                <br />
                                                <asp:DropDownList runat="server" ID="LpPurchaseEligibilityDropdown" class="ResultSectionControl"  onchange="updateModel();"/>
                                                <br />
                                                <span runat="server" id="LpFeeLevelControl">
                                                    <asp:TextBox runat="server" ID="LpFeeLevelDropdown" MaxLength="2" class="FeeLevelControl" />
                                                    <br />
                                                </span>
                                                <asp:DropDownList runat="server" ID="LpStatusDropdown" class="ResultSectionControl" />
                                            </span>
                                            <span runat="server" id="TotalControls" >
                                                <asp:DropDownList runat="server" ID="TotalRiskClassDropdown" class="ResultSectionControl" />
                                                <br />
                                                <asp:DropDownList runat="server" ID="TotalEligibilityAssessmentDropdown" class="ResultSectionControl" />
                                            </span>
                                            <span runat="server" id="GusControls" >
                                                <asp:DropDownList runat="server" ID="GusRecommendationDropdown" class="ResultSectionControl"/>
                                                <br />
                                                <asp:DropDownList runat="server" ID="GusRiskEvaluationDropdown" class="ResultSectionControl"/>
                                            </span>
                                            <span runat="server" id="OtherAusControls" >
                                                <asp:TextBox runat="server" ID="ResultOtherDescription" MaxLength="50" class="ResultSectionControl" />
                                            </span>
                                        </td>
                                        <td class="CaseIdColumn">
                                            <asp:TextBox runat="server" ID="CaseId" class="CaseIdControl" />
                                        </td>
                                        <td class="RunByColumn">
                                            <asp:TextBox runat="server" ID="OrderRunBy" class="RunByControl"/>
                                        </td>
                                        <td class="TimestampColumn">
                                            <asp:TextBox runat="server" ID="Timestamp" class="TimestampControl" />
                                            <ml:DateTextBox runat="server" class="ManualTimestamp" ID="ManualTimestamp" onchange="updateModel();"/>
                                        </td>
                                        <td class="NotesColumn">
                                            <asp:TextBox runat="server" ID="Notes" class="NotesControl" TextMode="MultiLine" Rows="2" />
                                        </td>
                                        <td class="RemoveColumn">
                                            <asp:Button UseSubmitBehavior="false" runat="server" ID="RemoveAusRun" Text="-" />
                                        </td>
                                        <td class="BackSpacerColumn"></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </div>
                    <div class="PageRow">
                        <input nohighlight type="button" id="AddAusRun" value="Add AUS Run" onclick="onAddAusRunClicked();" />
                        <asp:DropDownList runat="server" ID="AusForNewRow" />
                    </div>
                    <div class="PageRow">
                        <label class="FieldLabel"><asp:CheckBox runat="server" nohighlight type="checkbox" id="sReportFhaGseAusRunsAsTotalRun" name="sReportFhaGseAusRunsAsTotalRun" /> For HMDA Reporting, report DU and LPA runs on FHA loans as TOTAL Scorecard runs</label>
                    </div>
                </td>
            </tr>
            <tr id="RunAusSection" runat="server">
                <td>
                    <div class="LoanFormHeader">Run AUS</div>
                    <div>
                        <input id="DuSeamlessButton" type="button" onclick="openDuSeamless();" runat="server" value="Submit to Desktop Underwriter" />
                        <input id="LpaSeamlessButton" type="button" onclick="openLpaSeamless();" runat="server" value="Submit to Loan Product Advisor" />
                    </div>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
