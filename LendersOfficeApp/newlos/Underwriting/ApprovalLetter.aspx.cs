﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using LendersOffice.Pdf;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Underwriting
{
    public partial class ApprovalLetter : BaseLoanPage
    {
        /// <summary>
        /// Saves data to the loan from this page's controls.
        /// </summary>
        /// <param name="dataLoan">The loan to bind to.</param>
        /// <param name="dataApp">The app to bind to.</param>
        /// <param name="serviceItem">The service item calling this method.</param>
        internal static void BindDataFromControls(CPageData dataLoan, CAppData dataApp, AbstractBackgroundServiceItem serviceItem)
        {
            HmdaReliedOn.BindHmdaReliedOnData(dataLoan, dataApp, serviceItem, $"{nameof(HmdaReliedOn)}_");
        }

        /// <summary>
        /// Loads the data from the loan to this page's controls.
        /// </summary>
        /// <param name="dataLoan">The loan to load from.</param>
        /// <param name="dataApp">The app to load from.</param>
        /// <param name="serviceItem">The service item.</param>
        internal static void LoadDataForControls(CPageData dataLoan, CAppData dataApp, AbstractBackgroundServiceItem serviceItem)
        {
            HmdaReliedOn.LoadHmdaReliedOnData(dataLoan, dataApp, serviceItem, $"{nameof(HmdaReliedOn)}_");
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowUnderwritingAccess };
            }
        }

        private bool IsSuspenseNotice
        {
            get { return RequestHelper.GetBool("IsSuspenseNotice"); }
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ApprovalLetter));
            dataLoan.InitLoad();

            // Bind dataloan value to user control.
            sMaxDti.Text = dataLoan.sMaxDti_rep;
            IsOpenStatus.Value = dataLoan.sStatusT == E_sStatusT.Loan_Open ? "1":"0";
            sApprRprtExpD.Text = dataLoan.sApprRprtExpD_rep;
            sIncomeDocExpD.Text = dataLoan.sIncomeDocExpD_rep;
            sCreditScoreLpeQual.Text = dataLoan.sCreditScoreLpeQual_rep;
            sCreditScoreLpeQual.Attributes.Add("onkeypress", "return checkIt(event)");
            sMaxR.Text = dataLoan.sMaxR_rep;
            sCrExpD.Text = dataLoan.sCrExpD_rep;
            sMaxRLckd.Checked = dataLoan.sMaxRLckd;
            sMaxRLckd.Attributes.Add("onclick", "f_lockField(this);");
            sMaxR.ReadOnly = !dataLoan.sMaxRLckd;
            sMaxDti.Attributes.Add("onchange", "refreshCalculation();");
            sAssetExpD.Text = dataLoan.sAssetExpD_rep;
            sPrelimRprtExpD.Text = dataLoan.sPrelimRprtExpD_rep;
            sBondDocExpD.Text = dataLoan.sBondDocExpD_rep;
            sSuspendedD.Text = dataLoan.sSuspendedD_rep;
            sIncludeUwContactInfoOnApprovalCert.Checked = dataLoan.sIncludeUwContactInfoOnApprovalCert;
            sIncludeJrUwContactInfoOnApprovalCert.Checked = dataLoan.sIncludeJrUwContactInfoOnApprovalCert;

            sAppExpD.Text = dataLoan.sAppExpD_rep;
            sAppExpDLckd.Checked = dataLoan.sAppExpDLckd;

            if (dataLoan.BrokerDB.EnableAutoPriceEligibilityProcess)
            {
                phOvernightProcess.Visible = true;
                sAutoEligibilityProcessResult.Text = dataLoan.sAutoEligibilityProcessResultT_rep;
                sAutoEligibilityProcessResultD.Text = dataLoan.sAutoEligibilityProcessResultD_rep;
            }
            else
            {
                phOvernightProcess.Visible = false;
            }


            string issuedD = null;
            string pdfName = null;
            if (!IsSuspenseNotice && dataLoan.sHasApprovalCertPdf)
            {
                issuedD = dataLoan.sApprovalCertPdfLastSavedDFriendlyDisplay;
                var pdf = new LendersOffice.Pdf.CPmlApprovalPDF();
                pdfName = pdf.Name;
            }
            else if (IsSuspenseNotice && dataLoan.sHasSuspenseNoticePdf)
            {
                issuedD = dataLoan.sSuspenseNoticePdfLastSavedDFriendlyDisplay;
                var pdf = new LendersOffice.Pdf.CSuspenseNoticePDF();
                pdfName = pdf.Name;
            }

            if (issuedD != null && pdfName != null)
            {
                // be careful as this is a pass through literal.
                storedApprovalCertInfo.Text = IsSuspenseNotice ? "Suspense notice issued " : "Approval issued "; 
                storedApprovalCertInfo.Text += issuedD + " ";
                string link = string.Format(
                    "{0}/pdf/{1}.aspx?loanid={2}&crack={3}",
                    VirtualRoot,
                    pdfName,
                    LoanID,
                    DateTime.Now);

                storedApprovalCertInfo.Text += $"(<a onclick=\"LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload('{link}');\">view</a>)";
            }

            sApprovD.Text = dataLoan.sApprovD_rep;
            sApprovDTime.Text = dataLoan.sApprovDTime_rep;

            sFinalUnderwritingD.Text = dataLoan.sFinalUnderwritingD_rep;
            sFinalUnderwritingDTime.Text = dataLoan.sFinalUnderwritingDTime_rep;

            sClearToCloseD.Text = dataLoan.sClearToCloseD_rep;
            sClearToCloseDTime.Text = dataLoan.sClearToCloseDTime_rep;

            sCreditDecisionD.Text = dataLoan.sCreditDecisionD_rep;
            sCreditDecisionDTime.Text = dataLoan.sCreditDecisionDTime_rep;
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            // Init your user controls here, i.e: bind drop down list.

            if (!IsSuspenseNotice)
            {
                this.PageTitle = "Underwriting Approval Letter";
                this.PageID = "ApprovalLetter";
                this.PDFPrintClass = typeof(LendersOffice.Pdf.CPmlApprovalPDF); // 3/3/2011 dd - Does not work yet :-(
                TitleHeader.Text = "Approval Certificate";
            }
            else
            {
                this.PageTitle = "Suspense Notice";
                this.PageID = "SuspenseNotice";
                this.PDFPrintClass = typeof(LendersOffice.Pdf.CSuspenseNoticePDF);
                TitleHeader.Text = "Suspense Notice";
                this.reliedOnForApprovalCertificate.Visible = false;
            }

            btnViewApprovalLetter.Value = IsSuspenseNotice ? "Generate New Suspense Notice..." : "Generate New Approval Certificate..."; 
            hfIsSuspenseNotice.Value = IsSuspenseNotice ? "true" : "false";
            storedApprovalCertInfo.Text = IsSuspenseNotice ? "No suspense notice on file" : "No approval certificate on file";

            // Approval Letter Specific Fields
            phIncludeClearedConditions.Visible = !IsSuspenseNotice;
            phApprovalExpDate.Visible = !IsSuspenseNotice;

            // Suspense Notice Specific Fields
            phIncludeNonSuspenseConditions.Visible = IsSuspenseNotice;
            phLoanSuspendedDate.Visible = IsSuspenseNotice;

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
