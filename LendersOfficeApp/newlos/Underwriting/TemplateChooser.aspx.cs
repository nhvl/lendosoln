using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;



namespace LendersOfficeApp.newlos.Underwriting
{
	public partial class TemplateChooser : LendersOffice.Common.BasePage
	{
		protected void PageLoad(object sender, System.EventArgs e)
		{
			DataSet dS = new DataSet();
			bool filterByBranch = !BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowAccessToTemplatesOfAnyBranch);

            SqlParameter[] parameters = {
                                            new SqlParameter( "@BrokerId"       , BrokerUserPrincipal.CurrentPrincipal.BrokerId   )
				, new SqlParameter( "@BranchId"       , BrokerUserPrincipal.CurrentPrincipal.BranchId   )
				, new SqlParameter( "@EmployeeId"     , BrokerUserPrincipal.CurrentPrincipal.EmployeeId )
				, new SqlParameter( "@FilterByBranch" , filterByBranch )
                                        };

			DataSetHelper.Fill( dS, BrokerUserPrincipal.CurrentPrincipal.BrokerId, "RetrieveLoanTemplateByBrokerID", parameters);
			if( dS.Tables.Count > 0 && dS.Tables[0].Rows.Count > 0 )
			{
				dS.Tables[ 0 ].DefaultView.Sort = "sLNm ASC";
				m_templateRepeater.DataSource = dS.Tables[ 0 ];
				m_templateRepeater.DataBind();
			}
			else
			{
				m_NoTemplateLabel.Visible = true;
			}
		}

		/// <summary>
		/// Returns the value in the for the given column
		/// </summary>
		/// <param name="item">A datarowview</param>
		/// <param name="column">the column name</param>
		/// <returns>The value of the given column name</returns>
		private string GetValue(object item, string sColumn)
		{
			DataRowView row = item as DataRowView;
			return row != null ? row[sColumn].ToString() : "";
		}
		/// <summary>
		/// Returns the Template id in the given datarowview.
		/// </summary>
		/// <param name="item">DataRow</param>
		/// <returns>The template id in a string</returns>
		protected string GetTemplateId(object item)
		{
			return GetValue(item,"sLId");
		}

		/// <summary>
		/// Returns the Template name in the given datarowview.
		/// </summary>
		/// <param name="item">DataRow</param>
		/// <returns>The template name in a string</returns>
		protected string GetTemplateName(object item)
		{
			return GetValue(item, "sLNm");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}
