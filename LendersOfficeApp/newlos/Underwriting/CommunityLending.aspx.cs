﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Underwriting
{
    public partial class CommunityLending : BaseLoanPage
    {
        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowUnderwritingAccess };
            }
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(CommunityLending));
            dataLoan.InitLoad();

            sCommunityLendingClosingCost1Amt.Text = dataLoan.sCommunityLendingClosingCost1Amt_rep;
            sCommunityLendingClosingCost2Amt.Text = dataLoan.sCommunityLendingClosingCost2Amt_rep;
            sCommunityLendingClosingCost3Amt.Text = dataLoan.sCommunityLendingClosingCost3Amt_rep;
            sCommunityLendingClosingCost4Amt.Text = dataLoan.sCommunityLendingClosingCost4Amt_rep;
            sCommunityLendingDownPaymentSource1Amt.Text = dataLoan.sCommunityLendingDownPaymentSource1Amt_rep;
            sCommunityLendingDownPaymentSource2Amt.Text = dataLoan.sCommunityLendingDownPaymentSource2Amt_rep;
            sCommunityLendingDownPaymentSource3Amt.Text = dataLoan.sCommunityLendingDownPaymentSource3Amt_rep;
            sCommunityLendingDownPaymentSource4Amt.Text = dataLoan.sCommunityLendingDownPaymentSource4Amt_rep;

            Tools.SetDropDownListValue(sCommunityLendingDownPaymentSource1T, dataLoan.sCommunityLendingDownPaymentSource1T);
            Tools.SetDropDownListValue(sCommunityLendingDownPaymentSource2T, dataLoan.sCommunityLendingDownPaymentSource2T);
            Tools.SetDropDownListValue(sCommunityLendingDownPaymentSource3T, dataLoan.sCommunityLendingDownPaymentSource3T);
            Tools.SetDropDownListValue(sCommunityLendingDownPaymentSource4T, dataLoan.sCommunityLendingDownPaymentSource4T);
            Tools.SetDropDownListValue(sCommunityLendingDownPayment1T, dataLoan.sCommunityLendingDownPayment1T);
            Tools.SetDropDownListValue(sCommunityLendingDownPayment2T, dataLoan.sCommunityLendingDownPayment2T);
            Tools.SetDropDownListValue(sCommunityLendingDownPayment3T, dataLoan.sCommunityLendingDownPayment3T);
            Tools.SetDropDownListValue(sCommunityLendingDownPayment4T, dataLoan.sCommunityLendingDownPayment4T);
            Tools.SetDropDownListValue(sCommunityLendingClosingCostSource1T, dataLoan.sCommunityLendingClosingCostSource1T);
            Tools.SetDropDownListValue(sCommunityLendingClosingCostSource2T, dataLoan.sCommunityLendingClosingCostSource2T);
            Tools.SetDropDownListValue(sCommunityLendingClosingCostSource3T, dataLoan.sCommunityLendingClosingCostSource3T);
            Tools.SetDropDownListValue(sCommunityLendingClosingCostSource4T, dataLoan.sCommunityLendingClosingCostSource4T);
            Tools.SetDropDownListValue(sCommunityLendingClosingCost1T, dataLoan.sCommunityLendingClosingCost1T);
            Tools.SetDropDownListValue(sCommunityLendingClosingCost2T, dataLoan.sCommunityLendingClosingCost2T);
            Tools.SetDropDownListValue(sCommunityLendingClosingCost3T, dataLoan.sCommunityLendingClosingCost3T);
            Tools.SetDropDownListValue(sCommunityLendingClosingCost4T, dataLoan.sCommunityLendingClosingCost4T);

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            Tools.SetDropDownListValue(aBHomeOwnershipCounselingSourceT, dataApp.aBHomeOwnershipCounselingSourceT);
            Tools.SetDropDownListValue(aBHomeOwnershipCounselingFormatT, dataApp.aBHomeOwnershipCounselingFormatT);
            Tools.SetDropDownListValue(aCHomeOwnershipCounselingSourceT, dataApp.aCHomeOwnershipCounselingSourceT);
            Tools.SetDropDownListValue(aCHomeOwnershipCounselingFormatT, dataApp.aCHomeOwnershipCounselingFormatT);
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "CommunityLending";
            this.PageID = "CommunityLending";

            Tools.Bind_aHomeOwnershipCounselingSourceT(aBHomeOwnershipCounselingSourceT);
            Tools.Bind_aHomeOwnershipCounselingSourceT(aCHomeOwnershipCounselingSourceT);

            Tools.Bind_aHomeOwnershipCounselingFormatT(aBHomeOwnershipCounselingFormatT);
            Tools.Bind_aHomeOwnershipCounselingFormatT(aCHomeOwnershipCounselingFormatT);

            Tools.Bind_sCommunityLendingDownPaymentSourceT(sCommunityLendingDownPaymentSource1T);
            Tools.Bind_sCommunityLendingDownPaymentSourceT(sCommunityLendingDownPaymentSource2T);
            Tools.Bind_sCommunityLendingDownPaymentSourceT(sCommunityLendingDownPaymentSource3T);
            Tools.Bind_sCommunityLendingDownPaymentSourceTWithAggregate(sCommunityLendingDownPaymentSource4T);

            Tools.Bind_sCommunityLendingDownPaymentT(sCommunityLendingDownPayment1T);
            Tools.Bind_sCommunityLendingDownPaymentT(sCommunityLendingDownPayment2T);
            Tools.Bind_sCommunityLendingDownPaymentT(sCommunityLendingDownPayment3T);
            Tools.Bind_sCommunityLendingDownPaymentTWithAggregate(sCommunityLendingDownPayment4T);

            Tools.Bind_sCommunityLendingClosingCostSourceT(sCommunityLendingClosingCostSource1T);
            Tools.Bind_sCommunityLendingClosingCostSourceT(sCommunityLendingClosingCostSource2T);
            Tools.Bind_sCommunityLendingClosingCostSourceT(sCommunityLendingClosingCostSource3T);
            Tools.Bind_sCommunityLendingClosingCostSourceTWithAggregate(sCommunityLendingClosingCostSource4T);

            Tools.Bind_sCommunityLendingClosingCostT(sCommunityLendingClosingCost1T);
            Tools.Bind_sCommunityLendingClosingCostT(sCommunityLendingClosingCost2T);
            Tools.Bind_sCommunityLendingClosingCostT(sCommunityLendingClosingCost3T);
            Tools.Bind_sCommunityLendingClosingCostTWithAggregate(sCommunityLendingClosingCost4T);
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
