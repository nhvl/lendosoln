namespace LendersOfficeApp.newlos.Underwriting
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Email;
    using LendersOffice.ObjLib.PriceGroups;
    using LendersOffice.Security;

    public partial class PmlLoanSummaryEmailWithComment : LendersOffice.Common.BaseServicePage, IPmlSummaryDataConsumer
	{
        protected Boolean IsPmlEnabled
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.HasFeatures(E_BrokerFeatureT.PriceMyLoan); }
        }

        protected string FormType
        {
            get
            {
                return RequestHelper.GetSafeQueryString("formType");
            }
        }

        protected Guid LockPolicyId
        {
            get
            {
                return RequestHelper.GetGuid("lockPolicyId", Guid.Empty);
            }
        }

        protected void PageLoad(object sender, System.EventArgs e)
		{
            DataPageProvider.Bind(this);

            if(this.IsSendToOther)
            {
                sendToOtherRB.Checked = true;
                sendToOther.Value = this.OtherEmail;
            }

            picker.Visible = BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowReadingFromRolodex);
            LoName.InnerText = OfficialLoanOfficersName;
            LoEmail.InnerText = OfficialLoanOfficersEmailAddress;
            LoEmail.HRef = AspxTools.HtmlAttribute("mailto:" + OfficialLoanOfficersEmailAddress).Replace("\"", "");
            AEEmail.InnerText = AccountExecutivesEmailAddress;
            AEEmail.HRef = AspxTools.HtmlAttribute("mailto:" + AccountExecutivesEmailAddress).Replace("\"",""); 
            AEName.InnerText = AccountExecutivesName;
            BpEmail.InnerText = OfficialBrokerProcessorsEmailAddress;
            BpEmail.HRef = AspxTools.HtmlAttribute("mailto:" + OfficialBrokerProcessorsEmailAddress).Replace("\"", "");
            BpName.InnerText = OfficialBrokerProcessorsName;
            PrName.InnerText = OfficialProcessorsName;
            PrEmail.InnerText = OfficialProcessorsEmailAddress;
            PrEmail.HRef = AspxTools.HtmlAttribute("mailto:" + OfficialProcessorsEmailAddress).Replace("\"", "");
            UnName.InnerText = OfficialUnderwritersName;
            UnEmail.InnerText = OfficialUnderwritersEmailAddress;
            UnEmail.HRef = AspxTools.HtmlAttribute("mailto:" + OfficialUnderwritersEmailAddress).Replace("\"", "");
            defaultemailAddressLink.InnerText = UsersEmailAddress;
            defaultemailAddressLink.HRef = AspxTools.HtmlAttribute("mailto:" + UsersEmailAddress).Replace("\"", "");
            corporateAddresLink.InnerText = CorporatesEmailAddress;
            corporateAddresLink.HRef = AspxTools.HtmlAttribute("mailto:" + CorporatesEmailAddress).Replace("\"","");
            litcorporateLabel.Text = CorpateLabel;
            defaultUserName.Text = UsersFullName;

            ExternalSecondaryEmail.InnerText = OfficialExternalSecondaryEmailAddress;
            ExternalSecondaryEmail.HRef = AspxTools.HtmlAttribute("mailto:" + OfficialExternalSecondaryEmailAddress).Replace("\"", "");
            ExternalSecondaryName.InnerText = OfficialExternalSecondaryName;

            ExternalPostCloserEmail.InnerText = OfficialExternalPostCloserEmailAddress;
            ExternalPostCloserEmail.HRef = AspxTools.HtmlAttribute("mailto:" + OfficialExternalPostCloserEmailAddress).Replace("\"", "");
            ExternalPostCloserName.InnerText = OfficialExternalPostCloserName;

            if (!string.IsNullOrEmpty(UsersFullName))
            {
                DefaultStartSeperator.Visible = true;
                DefaultEndSeperator.Visible = true; 
            }
            if ( !string.IsNullOrEmpty(CorpateLabel)) 
            {
                CorporateStartSeperator.Visible = true;
                CorporateEndSeperator.Visible = true; 
            }

            if (String.IsNullOrEmpty(CorporatesEmailAddress)) 
            {
                CorporateEmailAddress.Visible= false;
                corporateSection.Visible = false;
                litcorporateLabel.Visible = false; 
            }

            if (String.IsNullOrEmpty(OfficialLoanOfficersEmailAddress))
            {
                LoEmail.Disabled = true;
                LoName.Disabled = true;
                LoLabel.Disabled = true;
                LoNameCb.Disabled = true;
                LoEmailText.Disabled = true;
                LoEmailText.Style.Add(HtmlTextWriterStyle.Color, "gray");

            }

            if (!IsPmlEnabled)
            {
                BpSection.Visible = false;
                ExternalSection.Visible = false;
            }
            else if (String.IsNullOrEmpty(OfficialBrokerProcessorsEmailAddress))
            {
                BpEmail.Disabled = true;
                BpName.Disabled = true;
                BpLabel.Disabled = true;
                BpNameCb.Disabled = true;
                BpEmailText.Disabled = true;
                BpEmailText.Style.Add(HtmlTextWriterStyle.Color, "gray");
            }

            if (string.IsNullOrEmpty(OfficialExternalSecondaryName))
            {
                ExternalSecondaryEmail.Disabled = true;
                ExternalSecondaryName.Disabled = true;
                ExternalSecondaryLabel.Disabled = true;
                ExternalSecondaryNameCb.Disabled = true;
                ExternalSecondaryEmailText.Disabled = true;
                ExternalSecondaryEmailText.Style.Add(HtmlTextWriterStyle.Color, "gray");
            }

            if (string.IsNullOrEmpty(OfficialExternalPostCloserName))
            {
                ExternalPostCloserEmail.Disabled = true;
                ExternalPostCloserName.Disabled = true;
                ExternalPostCloserLabel.Disabled = true;
                ExternalPostCloserNameCb.Disabled = true;
                ExternalPostCloserEmailText.Disabled = true;
                ExternalPostCloserEmailText.Style.Add(HtmlTextWriterStyle.Color, "gray");
            }

            if (string.IsNullOrEmpty(AccountExecutivesEmailAddress))
            {
                AEEmail.Disabled = true;
                AELabel.Disabled = true;
                AEName.Disabled = true;
                AENameCb.Disabled = true;
                AEEmaiLText.Style.Add(HtmlTextWriterStyle.Color, "gray");
                
            }

            if (string.IsNullOrEmpty(OfficialProcessorsEmailAddress))
            {
                PrEmail.Disabled = true;
                PrLabel.Disabled = true;
                PrName.Disabled = true;
                PrNameCb.Disabled = true;
                PrEmailText.Disabled = true;
                PrEmailText.Style.Add(HtmlTextWriterStyle.Color, "gray");
            }

            if (string.IsNullOrEmpty(OfficialUnderwritersEmailAddress))
            {
                UnEmail.Disabled = true;
                UnLabel.Disabled = true;
                UnName.Disabled = true;
                UnNameCb.Disabled = true;
                UnEmailText.Disabled = true;
                UnEmailText.Style.Add(HtmlTextWriterStyle.Color, "gray");
            }

            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("LQBPopup.js");


            HashSet<string> authorizedEmailDomains = new HashSet<string>(AuthorizedEmailDomainUtilities.GetAuthorizedEmailDomains(BrokerID), StringComparer.OrdinalIgnoreCase);
            authorizedEmailDomains.UnionWith(AuthorizedEmailDomainUtilities.AuthorizedEmailDomainWhitelist);

            this.RegisterJsGlobalVariables("AuthorizedEmailDomains", SerializationHelper.JsonNetAnonymousSerialize(authorizedEmailDomains));
            this.RegisterJsGlobalVariables("DefaultSenderAddress", ConstStage.DefaultSenderAddress);
            this.RegisterJsGlobalVariables("EnableEmailDomainAuthorization", ConstStage.EnableEmailDomainAuthorization);
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterService("main", "/newlos/Underwriting/PmlLoanSummaryView_FrameService.aspx");
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

        #region IPmlSummaryDataConsumer Members
        protected string GetCorporateFromString()
        {
            if (CorpateLabel != null && CorpateLabel.TrimWhitespaceAndBOM() != "")
            {
                return string.Format(@"""{0}""<{1}>", CorpateLabel, CorporatesEmailAddress);
            }
            return CorporatesEmailAddress;
        }

        protected string GetUserFromString()
        {
            if (UsersFullName != null && UsersFullName.TrimWhitespaceAndBOM() != "")
            {
                return string.Format(@"""{0}""<{1}>", UsersFullName, UsersEmailAddress);
            }
            return UsersEmailAddress;
        }

        public string UsersEmailAddress { get; set;  }
        public string CorporatesEmailAddress { get; set; }
        public string OfficialLoanOfficersEmailAddress { get; set; }
        public string OfficialLoanOfficersName { get; set; }
        public string OfficialBrokerProcessorsEmailAddress { get; set; }
        public string OfficialBrokerProcessorsName { get; set; }
        public string OfficialExternalSecondaryName { get; set; }
        public string OfficialExternalSecondaryEmailAddress { get; set; }
        public string OfficialExternalPostCloserName { get; set; }
        public string OfficialExternalPostCloserEmailAddress { get; set; }
        public string OfficialProcessorsEmailAddress { get; set; }
        public string OfficialProcessorsName { get; set; }
        public string OfficialUnderwritersEmailAddress { get; set; }
        public string OfficialUnderwritersName { get; set; }
        public string AccountExecutivesEmailAddress { get; set; }
        public string AccountExecutivesName { get; set; }
        public Guid LoanID { get { return RequestHelper.LoanID;  } }
        public Guid BrokerID { get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; } }
        public Guid UsersEmployeeID { get { return BrokerUserPrincipal.CurrentPrincipal.EmployeeId; } }
        public string UsersFullName { get; set; }
        public string CorpateLabel { get; set; }
        public PriceGroup PriceGroup { get; set; }
        public bool IsSendToOther { get; set; }
        public string OtherEmail { get; set; }
        public bool IsRateReLock
        {
            get {
                return string.Equals(this.FormType, "raterelock");
            }

        }

        #endregion
    }
}
