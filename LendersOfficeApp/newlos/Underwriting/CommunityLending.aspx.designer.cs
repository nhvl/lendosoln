﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.5448
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LendersOfficeApp.newlos.Underwriting {
    
    
    public partial class CommunityLending {
        
        /// <summary>
        /// form1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// aBHomeOwnershipCounselingSourceT control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList aBHomeOwnershipCounselingSourceT;
        
        /// <summary>
        /// aCHomeOwnershipCounselingSourceT control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList aCHomeOwnershipCounselingSourceT;
        
        /// <summary>
        /// aBHomeOwnershipCounselingFormatT control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList aBHomeOwnershipCounselingFormatT;
        
        /// <summary>
        /// aCHomeOwnershipCounselingFormatT control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList aCHomeOwnershipCounselingFormatT;
        
        /// <summary>
        /// sCommunityLendingDownPaymentSource1T control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCommunityLendingDownPaymentSource1T;
        
        /// <summary>
        /// sCommunityLendingDownPayment1T control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCommunityLendingDownPayment1T;
        
        /// <summary>
        /// sCommunityLendingDownPaymentSource1Amt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sCommunityLendingDownPaymentSource1Amt;
        
        /// <summary>
        /// sCommunityLendingDownPaymentSource2T control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCommunityLendingDownPaymentSource2T;
        
        /// <summary>
        /// sCommunityLendingDownPayment2T control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCommunityLendingDownPayment2T;
        
        /// <summary>
        /// sCommunityLendingDownPaymentSource2Amt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sCommunityLendingDownPaymentSource2Amt;
        
        /// <summary>
        /// sCommunityLendingDownPaymentSource3T control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCommunityLendingDownPaymentSource3T;
        
        /// <summary>
        /// sCommunityLendingDownPayment3T control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCommunityLendingDownPayment3T;
        
        /// <summary>
        /// sCommunityLendingDownPaymentSource3Amt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sCommunityLendingDownPaymentSource3Amt;
        
        /// <summary>
        /// sCommunityLendingDownPaymentSource4T control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCommunityLendingDownPaymentSource4T;
        
        /// <summary>
        /// sCommunityLendingDownPayment4T control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCommunityLendingDownPayment4T;
        
        /// <summary>
        /// sCommunityLendingDownPaymentSource4Amt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sCommunityLendingDownPaymentSource4Amt;
        
        /// <summary>
        /// sCommunityLendingClosingCostSource1T control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCommunityLendingClosingCostSource1T;
        
        /// <summary>
        /// sCommunityLendingClosingCost1T control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCommunityLendingClosingCost1T;
        
        /// <summary>
        /// sCommunityLendingClosingCost1Amt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sCommunityLendingClosingCost1Amt;
        
        /// <summary>
        /// sCommunityLendingClosingCostSource2T control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCommunityLendingClosingCostSource2T;
        
        /// <summary>
        /// sCommunityLendingClosingCost2T control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCommunityLendingClosingCost2T;
        
        /// <summary>
        /// sCommunityLendingClosingCost2Amt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sCommunityLendingClosingCost2Amt;
        
        /// <summary>
        /// sCommunityLendingClosingCostSource3T control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCommunityLendingClosingCostSource3T;
        
        /// <summary>
        /// sCommunityLendingClosingCost3T control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCommunityLendingClosingCost3T;
        
        /// <summary>
        /// sCommunityLendingClosingCost3Amt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sCommunityLendingClosingCost3Amt;
        
        /// <summary>
        /// sCommunityLendingClosingCostSource4T control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCommunityLendingClosingCostSource4T;
        
        /// <summary>
        /// sCommunityLendingClosingCost4T control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCommunityLendingClosingCost4T;
        
        /// <summary>
        /// sCommunityLendingClosingCost4Amt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sCommunityLendingClosingCost4Amt;
    }
}
