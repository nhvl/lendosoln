﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Constants;
using LendersOffice.Common;
using LendersOffice.Security;
using System.Xml.Xsl;
using DataAccess;
using System.Data.SqlClient;
using System.Xml;
using LendersOffice.ObjLib.Resource;

namespace LendersOfficeApp.newlos.Underwriting
{
    public partial class SuspenseNoticeView : LendersOffice.Common.BaseXsltPage
    {
        private string m_pmlLenderSiteId = "";

        protected override string XsltFileLocation 
        {
            get { return ResourceManager.Instance.GetResourcePath(ResourceType.SuspenseNoticeXslt, BrokerID); }
        }

        private Guid LoanID 
        {
            get { return RequestHelper.LoanID; }
        }

        private Guid BrokerID 
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
        }

        protected override XsltArgumentList XsltParams 
        {
            get 
            {
                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("VirtualRoot", "", Tools.VRoot);
                args.AddParam("LenderPmlSiteId", "", m_pmlLenderSiteId);
                return args;  
            }
        }

        protected override void InitXslt() 
        {
            Guid brokerPmlSiteId = BrokerUserPrincipal.CurrentPrincipal.BrokerDB.PmlSiteID;

            m_pmlLenderSiteId = Tools.GetFileDBKeyForPmlLogoWithLoanId(brokerPmlSiteId, LoanID);

        }

        protected override void GenerateXmlData(XmlWriter writer) 
        {
            string includeNonSuspenseConditions = RequestHelper.GetSafeQueryString("includeNonSuspenseConditions");
            bool includeNonSuspense = true;
            if (!string.IsNullOrEmpty(includeNonSuspenseConditions))
            {
                includeNonSuspense = includeNonSuspenseConditions == "t";
            }

            string includeUnderwriterContactInfo = RequestHelper.GetSafeQueryString("uw");
            bool includeUnderwriter = false;
            if (!string.IsNullOrEmpty(includeUnderwriterContactInfo))
            {
                includeUnderwriter = includeUnderwriterContactInfo == "t";
            }

            string includeJuniorUnderwriterContactInfo = RequestHelper.GetSafeQueryString("juw");
            bool includeJuniorUnderwriter = false;
            if (!string.IsNullOrEmpty(includeJuniorUnderwriterContactInfo))
            {
                includeJuniorUnderwriter = includeJuniorUnderwriterContactInfo == "t";
            }


            PmlLoanSummaryXmlData.Generate(writer,
                LoanID,
                true,  /* mask sensitive info */
                false, /* include completed conditions */
                includeUnderwriter,
                includeJuniorUnderwriter,
                includeNonSuspense);
        }
	}
}
