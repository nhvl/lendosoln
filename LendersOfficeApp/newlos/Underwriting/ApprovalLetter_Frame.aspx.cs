using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Underwriting
{

    public partial class ApprovalLetter_Frame : LendersOfficeApp.newlos.BaseLoanPage
    {

        protected void PageLoad(object sender, System.EventArgs e)
        {

        }
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ApprovalLetter_Frame));
            dataLoan.InitLoad();

            // This page should always look new by the clarification provided in OPM 55442 03/04/11.
        }
        protected void PageInit(object sender, System.EventArgs e)
        {
            // Register the service the old way since we are reusing a single service page
            this.RegisterService("main", "/newlos/Underwriting/PmlLoanSummaryView_FrameService.aspx");
            this.RegisterJsScript("LQBPrintFix.js");
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;

            Title =
            PageTitle =
            FrameHeader.Text = bIsSuspenseNotice ? "Suspense Notice Options" : "PML Underwriting Approval Certificate Options";

        }

        protected string ViewFrameUrl
        {
            get { return bIsSuspenseNotice ? "SuspenseNoticeView.aspx" : "ApprovalLetterView.aspx"; }
        }

        protected string IncludeCompletedString
        {
            get
            {
                string ic = RequestHelper.GetSafeQueryString("ic");
                if (!String.IsNullOrEmpty(ic))
                {
                    return "&ic=" + ic;
                }

                return string.Empty;
            }
        }

        protected string IncludeCompleted
        {
            get
            {
                string ic = RequestHelper.GetSafeQueryString("ic");
                if (!String.IsNullOrEmpty(ic))
                {
                    return ic == "t" ? "true" : "false";
                }

                return "false";
            }
        }

        protected string IncludeUnderwriterString
        {
            get
            {
                string uw = RequestHelper.GetSafeQueryString("uw");
                if (!String.IsNullOrEmpty(uw))
                {
                    return "&uw=" + uw;
                }

                return string.Empty;
            }
        }

        protected string IncludeUnderwriter
        {
            get
            {
                string uw = RequestHelper.GetSafeQueryString("uw");
                if (!String.IsNullOrEmpty(uw))
                {
                    return uw == "t" ? "true" : "false";
                }

                return "false";
            }
        }

        protected string IncludeJuniorUnderwriterString
        {
            get
            {
                string juw = RequestHelper.GetSafeQueryString("juw");
                if (!String.IsNullOrEmpty(juw))
                {
                    return "&juw=" + juw;
                }

                return string.Empty;
            }
        }

        protected string IncludeJuniorUnderwriter
        {
            get
            {
                string juw = RequestHelper.GetSafeQueryString("juw");
                if (!String.IsNullOrEmpty(juw))
                {
                    return juw == "t" ? "true" : "false";
                }

                return "false";
            }
        }

        protected bool bIsSuspenseNotice
        {
            get { return RequestHelper.GetBool("isSuspenseNotice"); }
        }

        protected string IsSuspenseNoticeString
        {
            get
            {
                string isSuspenseNotice = RequestHelper.GetSafeQueryString("isSuspenseNotice");
                if (!String.IsNullOrEmpty(isSuspenseNotice))
                {
                    return "&isSuspenseNotice=" + isSuspenseNotice;
                }

                return string.Empty;
            }
        }

        protected string IsSuspenseNotice
        {
            get
            {
                return bIsSuspenseNotice ? "true" : "false";
            }
        }

        protected string IncludeNonSuspenseConditionsString
        {
            get
            {
                string includeNonSuspenseConditions = RequestHelper.GetSafeQueryString("includeNonSuspenseConditions");
                if (!String.IsNullOrEmpty(includeNonSuspenseConditions))
                {
                    return "&includeNonSuspenseConditions=" + includeNonSuspenseConditions;
                }

                return string.Empty;
            }
        }

        protected string IncludeNonSuspenseConditions
        {
            get
            {
                string includeNonSuspenseConditions = RequestHelper.GetSafeQueryString("includeNonSuspenseConditions");
                if (!String.IsNullOrEmpty(includeNonSuspenseConditions))
                {
                    return includeNonSuspenseConditions == "t" ? "true" : "false";
                }

                return "false";
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
