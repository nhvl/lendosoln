using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Xml;
using System.Xml.Xsl;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Reminders;
using LendersOffice.Constants;
using LendersOffice.ObjLib.Resource;

namespace LendersOfficeApp.newlos.Underwriting
{

    public partial class ApprovalLetterView : LendersOffice.Common.BaseXsltPage
    {
        private string m_pmlLenderSiteId = "";

        protected override string XsltFileLocation 
        {
            get { return ResourceManager.Instance.GetResourcePath(ResourceType.PmlApprovalLetterXslt, BrokerID); }
        }

        private Guid LoanID 
        {
            get { return RequestHelper.LoanID; }
        }
        private Guid BrokerID 
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
        }
        protected override XsltArgumentList XsltParams 
        {
            get 
            {
                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("VirtualRoot", "", Tools.VRoot);
                args.AddParam("LenderPmlSiteId", "", m_pmlLenderSiteId);
                return args;  
            }
        }

        protected override void InitXslt() 
        {
            // 11/10/2004 dd - Theoretically, PmlLenderSiteId should be in loan product.
            // This way we allow independent broker to run qualification against multiple lender.
            // However we are not there yet. Retrieve from current broker.

            Guid brokerPmlSiteId = BrokerUserPrincipal.CurrentPrincipal.BrokerDB.PmlSiteID;
            m_pmlLenderSiteId = Tools.GetFileDBKeyForPmlLogoWithLoanId(brokerPmlSiteId, LoanID);

        }
        protected override void GenerateXmlData(XmlWriter writer) 
        {
            string includeCompletedConditions = RequestHelper.GetSafeQueryString("ic");
            bool includeCompleted = true;
            if (!string.IsNullOrEmpty(includeCompletedConditions))
            {
                includeCompleted = includeCompletedConditions == "t";
            }

            string includeUnderwriterContactInfo = RequestHelper.GetSafeQueryString("uw");
            bool includeUnderwriter = false;
            if (!string.IsNullOrEmpty(includeUnderwriterContactInfo))
            {
                includeUnderwriter = includeUnderwriterContactInfo == "t";
            }

            string includeJuniorUnderwriterContactInfo = RequestHelper.GetSafeQueryString("juw");
            bool includeJuniorUnderwriter = false;
            if (!string.IsNullOrEmpty(includeJuniorUnderwriterContactInfo))
            {
                includeJuniorUnderwriter = includeJuniorUnderwriterContactInfo == "t";
            }

            PmlLoanSummaryXmlData.Generate(writer, LoanID, true, includeCompleted, includeUnderwriter, includeJuniorUnderwriter, true /* include non-suspense conditions */);
        }
	}
}
