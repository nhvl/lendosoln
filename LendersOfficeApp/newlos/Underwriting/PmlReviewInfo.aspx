<%@ Page language="c#" Codebehind="PmlReviewInfo.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Underwriting.PmlReviewInfo" %>
<%@ Register TagPrefix="QRP" TagName="QualRatePopup" Src="~/newlos/Forms/QualRateCalculationPopup.ascx" %>
<%@ Import Namespace="LendersOffice.Common"%>
<%@ Import Namespace="DataAccess" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML>
<HTML>
  <HEAD runat="server">
    <title>PmlReviewInfo</title>
  </HEAD>
  <style>
  .hidden{display:none}
  .nowrap{white-space: nowrap;}
  .left-padding{padding-left: 5px;}
  .w-830 { width: 830px; }
  </style>
<body class=EditBackground MS_POSITIONING="FlowLayout">

<script type="text/javascript">
var loanPageUrl = '../LoanInfo.aspx';
<% if(IsEnableBigLoanPage) { %>
loanPageUrl = '../BigLoanInfo.aspx';
<% } %>
function _init() {
  f_onchange_sLPurposeT();
  f_onchange_sQualIR( true );
  f_onchange_sIsOptionArm();
  f_onchange_sProdDocT();
  f_onchange_sProdCashoutAmt();
  f_onchange_sProdSpT(true);
  f_updateTexasCB();
  lockField(<%=AspxTools.JsGetElementById(this.sQualIRLckd)%>, 'sQualIR');
  
  <%= AspxTools.JsGetElementById(sOccR) %>.readOnly = <%= AspxTools.JsGetElementById(sOccRLckd) %>.checked == false;
  
  <% if (m_isAutoPmiOrNewPml ) { %>
  f_setsProdConvMIOptionT(<%= AspxTools.JsNumeric((int) sLtv80TestResultT) %>, true );  
  <% } else { %>
  f_setsProdMIOptionT(<%= AspxTools.JsNumeric((int) sLtv80TestResultT) %>, true );  
  <% } %>      

    var isRenovationLoan = $("#sIsRenovationLoan").prop('checked');

    $("#navigateToRenovLink").toggle(ML.EnableRenovationLoanSupport);
    $("#plainRenovText").toggle(ML.IsEnableRenovationCheckboxInPML && !ML.EnableRenovationLoanSupport);
    $("#totalRenovationCostsRow").toggle((ML.IsEnableRenovationCheckboxInPML || ML.EnableRenovationLoanSupport) && isRenovationLoan);
    $("#sTotalRenovationCostsLckd").prop('disabled', ML.IsEnableRenovationCheckboxInPML && !ML.EnableRenovationLoanSupport);

    if (ML.IsEnableRenovationCheckboxInPML || ML.EnableRenovationLoanSupport) {
        lockField(<%=AspxTools.JsGetElementById(this.sTotalRenovationCostsLckd)%>, 'sTotalRenovationCosts');
    }

    SetQualTermReadonly();
    $('.see-declarations-item-j-k-container').toggle(!ML.IsTargeting2019Ulad);
}

function SetQualTermReadonly() {
    var isManual = $(".sQualTermCalculationType").val() == '<%=AspxTools.JsStringUnquoted(QualTermCalculationType.Manual)%>';
    $(".sQualTerm").prop("readonly", !isManual);
}


function f_viewPmlSummary() {
  // PROMPT FOR SAVE IF DIRTY BIT HIGH
  PolyShouldShowConfirmSave(isDirty(), function(){
    var body_url = ML.VirtualRoot + '/newlos/Underwriting/PmlLoanSummaryView_Frame.aspx?loanid=' + ML.sLId;
    var win = window.open(body_url, "", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=780,height=600");
    
    win.focus();
  }, saveMe, clearDirty);
  
  return false;
}
function f_onchange_sLPurposeT() {
var  durefinance = document.getElementById('isdurefinance');
  <% if (! m_IsStandAloneSecond) { %>
  var ddl = <%= AspxTools.JsGetElementById(sLPurposeT) %>;
  var purpose = ddl.value;
  
  if (purpose == <%= AspxTools.JsString(E_sLPurposeT.RefinCashout.ToString("D")) %>) {
      <%= AspxTools.JsGetElementById(sProdCashoutAmt) %>.readOnly = false;
      $("#sIsStudentLoanCashoutRefi").prop("disabled", false);
  } else {
    <%= AspxTools.JsGetElementById(sProdCashoutAmt) %>.readOnly = true;
    <%= AspxTools.JsGetElementById(sProdCashoutAmt) %>.value = "$0.00";
    $("#sIsStudentLoanCashoutRefi").prop("disabled", true);
  }
  
  if ( durefinance !== null ) 
  {
    durefinance.style.display = purpose == <%= AspxTools.JsString(E_sLPurposeT.Refin.ToString("D")) %> ? '' : 'none';
   }
  <% } %>
  <% else { %>
  if ( durefinance !== null ) {
    durefinance.style.display = 'none';
  }
  <%} %>
}

function f_onchange_sProdSpT(isInit) {
    var sProdSpT = <%= AspxTools.JsGetElementById(sProdSpT) %>;
    var sProdSpT_Manufactured = <%= AspxTools.JsString(E_sProdSpT.Manufactured.ToString("D")) %>;
    $('.manufactured-datapoint').toggle(sProdSpT.value === sProdSpT_Manufactured);

    if (!isInit) {
        refreshCalculation();
    }
}

function f_onchange_sProdCashoutAmt() {
  var tb = <%= AspxTools.JsGetElementById(sProdCashoutAmt) %>;
  
  if (tb.value == "" || tb.value == "0") {
    tb.value = "$0.00";
  }
  if (tb.value.indexOf("-") >= 0 || tb.value.indexOf("(") >= 0) {
    alert(<%=AspxTools.JsString(JsMessages.LPE_CashoutCannotBeNegative) %>);
    tb.value = "$0.00";
  }
}

function f_onchange_sQualIR( bIsInit )
{
  var sQualIR = <%= AspxTools.JsGetElementById(sQualIR) %>;
    var sIsQRateIOnly = <%= AspxTools.JsGetElementById(sIsQRateIOnly) %>;
    var sUseQualRate = document.getElementById(QualRateFieldIds.ShouldUseQualRate).checked;
    var sQualIRLckd = <%= AspxTools.JsGetElementById(sQualIRLckd) %>.checked;

  if ( bIsInit == false )
    format_percent(sQualIR);
  
  sIsQRateIOnly.disabled = ( sQualIR.value == '0.000%' || sQualIR.value == '' || (!sUseQualRate && !sQualIRLckd) );
}

function f_onchange_sIsOptionArm()
{
  var sOptionArmTeaserR = <%= AspxTools.JsGetElementById(sOptionArmTeaserR) %>;
  var sIsOptionArm = <%= AspxTools.JsGetElementById(sIsOptionArm) %>;
    
  sOptionArmTeaserR.readOnly = <%= AspxTools.JsBool(m_IsRateLocked) %> || ( ! sIsOptionArm.checked );
    
}

function f_onchange_sProdDocT(obj) {
    var obj = obj || <%= AspxTools.JsGetElementById(sProdDocT) %>;
    if (obj.options[obj.selectedIndex].value == 11) {
        document.getElementById("reservesAvailableLabel").style.display = 'none';
        document.getElementById("reservesAvailableDD").style.display = 'none';
    }
    else {
        document.getElementById("reservesAvailableLabel").style.display = '';
        document.getElementById("reservesAvailableDD").style.display = '';  
    }
}

function f_validateNumFinancedProperties(source, args) {
    var tb = <%= AspxTools.JsGetElementById(sNumFinancedProperties) %>;

    var intRegex = /^\d+$/;
    if (tb.value == "" || tb.value < 1 || !intRegex.test(tb.value)) {
        args.IsValid = false;
        alert('Number of financed properties is invalid.');
        return;
    }
}

function f_updateTexasCB(){
    var texasCb = <%= AspxTools.JsGetElementById(sProdIsTexas50a6Loan) %>;
    var $texasRow = $(".TexasRow");
    var loanPurpose = <%= AspxTools.JsGetElementById(sLPurposeT) %>;
    var sSpStatePe = <%= AspxTools.JsGetElementById(sSpState) %>;
    var sOccTPe = <%= AspxTools.JsGetElementById(sOccT) %>;
    var sProdCashoutAmt = <%= AspxTools.JsGetElementById(sProdCashoutAmt) %>;
    var bIsDisableAutoCalcOfTexas50a6 = document.getElementById("IsDisableAutoCalcOfTexas50a6").value === "True";
    
    <% if (!m_renderAsSecond) { %>
      var isVisible = (loanPurpose.value != <%= AspxTools.JsString(E_sLPurposeT.Purchase.ToString("D")) %> && sSpStatePe.value == "TX");
      <% if (!IsReadOnly) { %>
        var isLocked = ((loanPurpose.value == <%= AspxTools.JsString(E_sLPurposeT.RefinCashout.ToString("D")) %> || loanPurpose.value == <%= AspxTools.JsString(E_sLPurposeT.HomeEquity.ToString("D")) %>) 
            && sOccTPe.value == <%= AspxTools.JsString(GetPropertyPurposeDescription(E_sOccT.PrimaryResidence)) %>);
      <% } %>
    <% } else { %>
      var isVisible = sSpStatePe.value == "TX";
      <% if (!IsReadOnly) { %>
        var isLocked = (sOccTPe.value == <%= AspxTools.JsString(GetPropertyPurposeDescription(E_sOccT.PrimaryResidence)) %> && sProdCashoutAmt.value != "$0.00");
      <% } %>
    <% } %>
    
    <% if (!IsReadOnly) { %>
      if (isVisible) {
          $texasRow.show();
        if (isLocked && !bIsDisableAutoCalcOfTexas50a6)
          texasCb.disabled = true;
        else
          texasCb.disabled = false;
      }
      else {
        $texasRow.hide();
        texasCb.disabled = false;
      }
    <% } else { %> 
      TexasRow.style.display = isVisible ? "" : "none"; 
    <% } %>
}

 <%if(Broker.IsEnabledLockPeriodDropDown){ %>
    function f_updateProdRLckdDaysFromDDL(ddl)
    {
        var hiddenTB = <%= AspxTools.JsGetElementById(sProdRLckdDays) %>;
        hiddenTB.value = ddl.value;
        hiddenTB.onchange(); // trigger the calculations that were on the rate lock textbox.
    }
    <%} %>
    
function f_setsProdMIOptionT( ltv80TestResult , bInit )
{
  if (! <%= AspxTools.JsBool(IsReadOnly) %>)
  {
    var ddl = <%= AspxTools.JsGetElementById(sProdMIOptionT) %>;
    if ( ddl != null )
    {
      if ( ltv80TestResult != <%= AspxTools.JsNumeric((int) E_sLtv80TestResultT.Over80) %> )
      {
        ddl.selectedIndex = 0;
        ddl.style.backgroundColor = gReadonlyBackgroundColor;
        ddl.disabled = true;
      }
      else
      {
        ddl.style.backgroundColor = "";
        ddl.disabled = false;
      }
    }
  }
}

function f_setsProdConvMIOptionT( ltv80TestResult , bInit )
{
  if (! <%= AspxTools.JsBool(IsReadOnly) %>)
  {
    var ddl = <%= AspxTools.JsGetElementById(sProdConvMIOptionT) %>;
    if ( ddl != null )
    {
      if ( ltv80TestResult != <%= AspxTools.JsNumeric((int) E_sLtv80TestResultT.Over80) %> )
      {
        ddl.selectedIndex = ddl.length - 1;
        ddl.style.backgroundColor = gReadonlyBackgroundColor;
        ddl.disabled = true;
      }
      else
      {
        ddl.style.backgroundColor = "";
        ddl.disabled = false;
      }
    }

  }
}
    
function goToDeclarations() {
    if (!ML.IsTargeting2019Ulad && parent && parent.treeview && 
            parent.treeview.load && typeof parent.treeview.load === 'function') {
        parent.treeview.load('Declarations.aspx');
    }
}

function f_navigateToRenovLoanPage() {
    if (parent && parent.treeview && parent.treeview.load && typeof parent.treeview.load === 'function') {
        if (ML.isLoanTypeFHA) { // loan type is FHA
            parent.treeview.load('renovation/fha203kmaximummortgage.aspx');
        }    
        else {
            parent.treeview.load('renovation/fanniemaehomestylemaximummortgage.aspx');
        }
    }       
}
</script>


<form id=PmlReviewInfo method=post runat="server">
<QRP:QualRatePopup ID="QualRatePopup" runat="server" />
<asp:HiddenField runat="server" ID="IsDisableAutoCalcOfTexas50a6" />
<table id=Table2 cellSpacing=0 cellPadding=0 border=0>
  <tr>
    <td class=MainRightHeader>PriceMyLoan Summary</td></tr>
  <tr style="PADDING-LEFT: 5px">
    <td class=FieldLabel style="PADDING-LEFT: 5px; PADDING-BOTTOM: 3px; PADDING-TOP: 3px">
        <input class=ButtonStyle style="WIDTH: 210px" onclick=f_viewPmlSummary(); type=button value="Generate PML Status..." AlwaysEnable="true"> 
    </td>
  </tr>
  <tr style="PADDING-LEFT: 5px">
    <td>
       <ml:PassthroughLabel CssClass="FieldLabel" ID="storedPMLSummaryInfo" runat="server" Text="No PML summary on file "></ml:PassthroughLabel>
      <br /><br />
    </td>
  </tr>
  <tr style="PADDING-LEFT: 5px">
    <td>
      <table class="InsetBorder w-830" cellSpacing=0 cellPadding=0>
        <tr style="PADDING-LEFT: 5px">
          <td class=FieldLabel noWrap>
            <a href="javascript:linkMe(loanPageUrl);" title="Go to 'This Loan Info'">Loan Program</a>&nbsp;&nbsp;&nbsp;
          </td>
          <td width="100%">
            <asp:textbox id=sLpTemplateNm runat="server" width="425px" ></asp:textbox>
          </td>
        </tr>
          <tr id="sLoanProductIdentifierRow" runat="server">
              <td class="FieldLabel" noWrap>Loan Product Identifier</td>
              <td>
                  <asp:TextBox runat="server" ID="sLoanProductIdentifier" Width="350px"></asp:TextBox>
              </td>
          </tr>
        <tr class="left-padding">
            <td class="FieldLabel nowrap">
                Loan Product Type
            </td>
            <td>
                <asp:TextBox ID="sLpProductType" runat="server" ReadOnly="true" Width="350px"></asp:TextBox>
            </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td style="PADDING-LEFT: 5px">
      <table class="InsetBorder w-830" cellSpacing=0 cellPadding=0 >
        <tr>
          <td class=LoanFormHeader>Applicant Info</td>
        </tr>
        <tr>
          <td style="PADDING-LEFT: 5px">
            <table cellSpacing=0 cellPadding=0 border=0>
              <tr>
                <td class=FieldLabel><A href="javascript:linkMe('../BorrowerInfo.aspx');" >Applicant Name</A></td>
                <td><asp:textbox id=aBNm runat="server" width="319px" ReadOnly="True"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel><A href="javascript:linkMe('../BorrowerInfo.aspx');" >Applicant SSN</A></td>
                <td><asp:textbox id=aBSsn runat="server" ReadOnly="True"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel><A href="javascript:linkMe('../Forms/Loan1003.aspx?pg=2');" title="Go to 1003 page 3">Applicant Citizenship</A></td>
                <td>
                    <asp:dropdownlist id=aProdBCitizenT runat="server"></asp:dropdownlist>
                    <span class="see-declarations-item-j-k-container">
                        see <a href="#" onclick="goToDeclarations();">declarations</a> items j & k
                    </span>
                </td></tr>
              <tr>
                <td class=FieldLabel><A href="javascript:linkMe('../Worksheets/CreditScores.aspx');" >Applicant 
                  Credit Score</A></td>
                <td class=FieldLabel><span title="Experian">XP:</span> <asp:textbox id=aBExperianScore runat="server" Width="40px"></asp:textbox>&nbsp;&nbsp; 
                  <span title="TransUnion">TU:</span> <asp:textbox id=aBTransUnionScore runat="server" Width="40px"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp; 
                  <span title="Equifax">EF:</span> <asp:textbox id=aBEquifaxScore runat="server" Width="40px"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel>Self Employed</td>
                <td class=FieldLabel><asp:checkbox id=sIsSelfEmployed runat="server" Text="Yes"></asp:checkbox></td></tr>
              <tr style="padding-top:10px">
                <td class=FieldLabel><A href="javascript:linkMe('../BorrowerInfo.aspx');" >Co-Applicant Name</A></td>
                <td><asp:textbox id=aCNm runat="server" width="319px" ReadOnly="True"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel><A href="javascript:linkMe('../BorrowerInfo.aspx');" >Co-Applicant SSN</A></td>
                <td><asp:textbox id=aCSsn runat="server" ReadOnly="True"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel><A href="javascript:linkMe('../Worksheets/CreditScores.aspx');" >Co-Applicant 
                  Credit Score</A></td>
                <td class=FieldLabel><span title="Experian">XP:</span> <asp:textbox id=aCExperianScore runat="server" Width="40px"></asp:textbox>&nbsp;&nbsp; 
                  <span title="TransUnion">TU:</span> <asp:textbox id=aCTransUnionScore runat="server" Width="40px"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp; 
                  <span title="Equifax">EF:</span> <asp:textbox id=aCEquifaxScore runat="server" Width="40px"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel>Co-Applicant is Primary Wage Earner</td>
                <td class=FieldLabel><asp:checkbox id=aIsBorrSpousePrimaryWageEarner runat="server"></asp:checkbox></td></tr>
              <tr>
                <td class=FieldLabel colSpan=2>&nbsp;</td></tr>
              <tr>
                <td class=FieldLabel><A onclick="redirectToUladPage('Liabilities');" title="Go to Liabilities">Total 
                  Non-mortgage Debt Payment</A>&nbsp;&nbsp;</td>
                <td class=FieldLabel><asp:textbox id=sTransmOMonPmt runat="server" ReadOnly="True" Width="90px" cssclass="RightAlign"></asp:textbox>&nbsp;/ 
                  month</td></tr>
              <tr>
                <td class=FieldLabel><A href="javascript:linkMe('../BorrowerInfo.aspx?pg=3');" >Total 
                  Income</A></td>
                <td class=FieldLabel><asp:textbox id=sPrimAppTotNonspI runat="server" ReadOnly="True" Width="90px" cssclass="RightAlign"></asp:textbox>&nbsp;/ 
                  month</td></tr>
               <tr id="EstimatedResidualIncomeRow" runat="server">
                <td class="FieldLabel">Estimated Residual Income</td>
                <td class="FieldLabel"><ml:moneytextbox id="sProdEstimatedResidualI" runat="server" width="90" preset="money" />&nbsp;/ month</td>
              </tr>
              <tr>
                <td class=FieldLabel colSpan=2>Negative 
                  Cash Flow from Other Properties&nbsp;&nbsp;<asp:textbox id=sOpNegCf runat="server" ReadOnly="True" Width="90px" cssclass="RightAlign"></asp:textbox>&nbsp;/ 
                  month</td></tr></table></td></tr></table></td></tr>
  <tr>
    <td style="PADDING-LEFT: 5px">
      <table class="InsetBorder w-830" cellSpacing=0 cellPadding=0 
      >
        <tr>
          <td class=LoanFormHeader>Property Info</td></tr>
        <tr>
          <td style="PADDING-LEFT: 5px">
            <table cellSpacing=0 cellPadding=0>
              <TR>
                <TD class=FieldLabel><A href="javascript:linkMe('../PropertyInfo.aspx');" >Property Address</A></TD>
                <TD colSpan=4><asp:TextBox id=sSpAddr runat="server" Width="236px"></asp:TextBox></TD></TR>
              <TR>
                <TD class=FieldLabel></TD>
                <TD colSpan=4 class=FieldLabel><asp:TextBox id=sSpCity runat="server"></asp:TextBox><ml:statedropdownlist id=sSpState runat="server" onclick="backgroundCalculation();" IncludeTerritories="false"></ml:statedropdownlist><ml:ZipcodeTextBox id=sSpZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox>&nbsp; 
                  County: <asp:DropDownList id=sSpCounty runat="server"></asp:DropDownList></TD></TR>
              <TR>
                <TD class=FieldLabel>&nbsp;</TD>
                <TD colSpan=4></TD></TR>
              <tr>
                <td class=FieldLabel><A href="javascript:linkMe('../PropertyInfo.aspx');" >Property State</A></td>
                <td>See address above</td>
                <TD width=10></TD>
                <td class="FieldLabel">Property Type</td>
                <td><asp:DropDownList ID="sProdSpT" TabIndex="5" runat="server" Width="100px" onchange="f_onchange_sProdSpT(false);"></asp:DropDownList></td>
              </tr>
              <tr>
                <td class=FieldLabel><a href="javascript:linkMe(loanPageUrl);" title="Go to 'This Loan Info'">Property 
                  Use</A></td>
                <td><asp:textbox id=sOccT runat="server" onchange="backgroundCalculation();" ReadOnly="True"></asp:textbox></td>
                <td width=10></td>
                <td class="FieldLabel"><span class="manufactured-datapoint">Not Permanently Affixed?</span></td>
                <td class="FieldLabel"><asp:CheckBox ID="sIsNotPermanentlyAffixed" runat="server" CssClass="manufactured-datapoint" /></td>
              </tr>
              <tr>
                <td class=FieldLabel><A href="javascript:linkMe('../PropertyInfo.aspx?pg=2');" >Occupancy 
                  Rate (%)</A><a href="javascript:linkMe(loanPageUrl);"></A></td>
                <td class="FieldLabel">
                  <asp:CheckBox ID="sOccRLckd" runat="server" Text="Lock" onclick="refreshCalculation();" />
                  <ml:percenttextbox id=sOccR runat="server" width="90px" preset="percent" />
                </td>
                <td width=10></td>
                <td class="FieldLabel"><span class="manufactured-datapoint">Is home MH Advantage?</span></td>
                <td class="FieldLabel"><asp:DropDownList runat="server" ID="sHomeIsMhAdvantageTri" CssClass="manufactured-datapoint"></asp:DropDownList></td>
              </tr>
              <tr>
                <td class=FieldLabel><A href="javascript:linkMe('../PropertyInfo.aspx?pg=2');" >Gross 
                  Rent</A><a href="javascript:linkMe(loanPageUrl);"></A></td>
                <td class="FieldLabel">
                  <ml:moneytextbox id=sSpGrossRent runat="server" width="90" preset="money" />
                </td>
                <td width=10></td>
                <td class="FieldLabel">Structure Type</td>
                <td class="FieldLabel"><asp:DropDownList ID="sProdSpStructureT" TabIndex="5" runat="server" Width="100px"></asp:DropDownList></td>
              </tr>
              <TR>
                <td class=FieldLabel><a href="javascript:linkMe(loanPageUrl);" title="Go to 'This Loan Info'">Proposed Property Tax</A><A href="javascript:linkMe('../PropertyInfo.aspx?pg=2');" ></A></td>
                <td><asp:textbox id=sProRealETx runat="server" ReadOnly="True" Width="90px" CssClass="RightAlign"></asp:textbox></td>
                <td width=10></td>
                <td class="FieldLabel">Condo Stories</td>
                <td><asp:TextBox ID="sProdCondoStories" TabIndex="5" runat="server" Width="40px"></asp:TextBox></td>
              </tr>
              <tr>
                <td class=FieldLabel><a href="javascript:linkMe(loanPageUrl);" title="Go to 'This Loan Info'">Other Proposed Housing 
                  Expenses</A>&nbsp;&nbsp;<A href="javascript:linkMe('../PropertyInfo.aspx?pg=2');" ></A></td>
                <td><asp:textbox id=sProOHExp runat="server" ReadOnly="True" Width="90px" CssClass="RightAlign"></asp:textbox></td>
                <td width=10></td>
                <td class="FieldLabel">Is in Rural Area<a href="javascript:linkMe(loanPageUrl);"></a></td>
                <td class="FieldLabel"><asp:CheckBox ID="sProdIsSpInRuralArea" TabIndex="5" runat="server" Text="Yes"></asp:CheckBox></td>
              </TR>
            <tr>
                <td class="FieldLabel"> <label for="sPriorSalesD">Prior Sales Date</label></td> 
                <td> <ml:DateTextBox runat="server" id="sPriorSalesD"></ml:DateTextBox>
                <td>&nbsp;</td>
                <td class="FieldLabel">Is Condotel</td>
                <td class="FieldLabel"><asp:CheckBox ID="sProdIsCondotel" TabIndex="5" runat="server" Text="Yes"></asp:CheckBox></td>
            </tr>
            </tr>
            <tr>
                <td class="FieldLabel"><label for="sPriorSalesPropertySellerT">Property Seller</label></td>
                <td><asp:DropDownList runat="server" ID="sPriorSalesPropertySellerT"></asp:DropDownList></td>
                <td width=10></td>
                <td class="FieldLabel">Is Non-Warrantable Proj&nbsp;&nbsp;<a href="javascript:linkMe(loanPageUrl);"></a></td>
                <td class="FieldLabel"><asp:CheckBox ID="sProdIsNonwarrantableProj" TabIndex="5" runat="server" Text="Yes"></asp:CheckBox></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
                <td class="FieldLabel"> <label for="sPriorSalesPrice">Prior Sales Price </label> </td>
                <td><ml:MoneyTextBox runat="server" ID="sPriorSalesPrice"></ml:MoneyTextBox></td>
            </tr>
                </table></td></tr></table></td></tr>
    
            
    <% if (Broker.IsAsk3rdPartyUwResultInPml) { %>
  <TR>
    <TD style="PADDING-LEFT: 5px">
      <TABLE class="InsetBorder w-830" id=Table1 cellSpacing=0 cellPadding=0  
      border=0>
        <TR>
          <TD class=LoanFormHeader noWrap colspan="2">AU Options</TD></TR>
        <TR>
          <td nowrap class="FieldLabel">Response</td><TD noWrap><asp:dropdownlist id=sProd3rdPartyUwResultT runat="server"></asp:dropdownlist></TD></TR>
        <TR>
          <td class="FieldLabel">Processing</td><TD noWrap class="FieldLabel">
                <asp:CheckBox ID="sProdIncludeNormalProc" Runat="server" Text="Conventional" TabIndex="12"></asp:CheckBox>&nbsp;&nbsp;
      <asp:CheckBox ID="sProdIncludeMyCommunityProc" Runat="server" Text="My Community / HomeReady" TabIndex="12"></asp:CheckBox>&nbsp;&nbsp;
      <asp:CheckBox ID="sProdIncludeHomePossibleProc" Runat="server" Text="Home Possible"  TabIndex="12"></asp:CheckBox>&nbsp;&nbsp;
      </td></tr>
      <tr>
      <td>&nbsp;</td>
      <td class="FieldLabel">
      <asp:CheckBox ID="sProdIncludeFHATotalProc" Runat="server" Text="FHA Total"  TabIndex="12"></asp:CheckBox>&nbsp;&nbsp;
      <asp:CheckBox ID="sProdIncludeVAProc" Runat="server" Text="VA"  TabIndex="12"></asp:CheckBox>
        <asp:CheckBox ID="sProdIncludeUSDARuralProc" runat="server" Text="USDA Rural" TabIndex="12" />
          </TD></TR>  
          
          <tr id="isdurefinance" ><td class="FieldLabel">Is DU Refi Plus</td>
          <td><asp:CheckBox ID="sProdIsDuRefiPlus" Text="Yes" runat="server"/></td></tr>        
          </TD></TR>  
         
          </TABLE></td></tr> <% } %>  
    <tr>
    <td style="PADDING-LEFT: 5px">
      <table class="InsetBorder w-830" cellSpacing=0 cellPadding=0  
      >
        <tr>
          <td class=LoanFormHeader>Present Housing 
          Expense</td></tr>
        <tr>
          <td style="PADDING-LEFT: 5px">
            <table cellSpacing=0 cellPadding=0>
              <tr>
                <td class=FieldLabel><A href="javascript:linkMe('../BorrowerInfo.aspx?pg=7');" >Present 
                  Housing Expense</A>&nbsp;&nbsp;</td>
                <td><asp:textbox id=sPresLTotPersistentHExp runat="server" ReadOnly="True" CssClass="RightAlign"></asp:textbox></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td style="PADDING-LEFT: 5px">
      <table class="InsetBorder w-830" cellSpacing=0 cellPadding=0  
      >
        <tr>
          <td class=LoanFormHeader>Loan Info</td></tr>
        <tr>
          <td style="PADDING-LEFT: 5px">
            <table cellSpacing=0 cellPadding=0>
              <tr>
                <% if (! m_IsStandAloneSecond) { %>
                <td class=FieldLabel><a href="javascript:linkMe(loanPageUrl);" title="Go to 'This Loan Info'">Loan Purpose</A></td>
                <td><asp:dropdownlist id=sLPurposeT tabIndex=10 runat="server" onchange="backgroundCalculation();"></asp:dropdownlist></td>
                <% } else {%> <td></td><td></td> <% } %>
                <td width=10></td>
                <td class=FieldLabel>Doc Type</td>
                <td><asp:dropdownlist id="sProdDocT" tabIndex="15"  onchange="f_onchange_sProdDocT(this)" runat="server"></asp:dropdownlist></td></tr>
              <tr>
                  <td class="FieldLabel">Student Loan Cashout?</td>
                  <td><asp:CheckBox ID="sIsStudentLoanCashoutRefi" runat="server"/></td>
              </tr>

              <tr class="FieldLabel TexasRow">
                <td>New Loan is Texas 50(a)(6)?</td>
                <td><asp:CheckBox ID="sProdIsTexas50a6Loan" runat="server" tabIndex=10 Text="Yes" /></td>
              </tr>
              <tr class="FieldLabel TexasRow">
                <td>Prior Loan is Texas 50(a)(6)?</td>
                <td><asp:CheckBox ID="sPreviousLoanIsTexas50a6Loan" runat="server" tabIndex=10 Text="Yes" /></td>
              </tr>

              <tr>
                <td class=FieldLabel>Cashout Amount</td>
                <td><ml:moneytextbox id=sProdCashoutAmt tabIndex=10 runat="server" width="90" preset="money" onchange="backgroundCalculation();"></ml:moneytextbox></td>
                <td width=10></td>
                <td class=FieldLabel nowrap>Amortization Type</td>
                <td><asp:dropdownlist id=sFinMethT tabIndex=15 runat="server" Width="90px"></asp:dropdownlist></td></tr>
            <%if (this.EnableRenovationUi)
              { %>
            <tr>
                <td class=FieldLabel>Renovation Loan?</td>
                <td class=FieldLabel><asp:CheckBox ID="sIsRenovationLoan" onclick="refreshCalculation();" TabIndex=10 runat="server" Text="Yes" /></td>
                <td width=10></td>
                <td class=FieldLabel nowrap></td>
                <td></tr>
            <tr id="totalRenovationCostsRow">
                <td class=FieldLabel>
                    <a id="navigateToRenovLink" href="#" onclick="return f_navigateToRenovLoanPage();">Total Renovation Costs</a>
                    <span id="plainRenovText">Total Renovation Costs</span>
                </td>
                <td>
                    <ml:moneytextbox id=sTotalRenovationCosts tabIndex=10 runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox>
                    <asp:CheckBox ID="sTotalRenovationCostsLckd" runat="server" Text="Lock" onclick="refreshCalculation();" />
                </td>
                <td width=10></td>
                <td class=FieldLabel nowrap></td>
                <td></tr>
            <%} %>
             <% if (! m_IsStandAloneSecond) { %>
              <tr>
                <td class=FieldLabel>FTHB</td>
                <td class=FieldLabel><asp:checkbox id="sHas1stTimeBuyer" tabIndex=10 runat="server" Enabled="False" />Yes</td>
                <TD width=10></TD>
                <TD class=FieldLabel>Rate Lock Period</TD>
                <TD class=FieldLabel>
                    <% if (Broker.IsEnabledLockPeriodDropDown){ %>
                        <asp:DropDownList runat="server" ID="sProdRLckdDaysDDL" onchange="f_updateProdRLckdDaysFromDDL(this)"></asp:DropDownList>
                    <% } %>
                    <asp:textbox id=sProdRLckdDays tabIndex=15 runat="server" width="70px"></asp:textbox>&nbsp;days
                </TD>
              </tr>
              <TR>
              <% } %>
              <tr>
                <td class=FieldLabel>Number of Financed Properties</span></td>
                <td class=FieldLabel>
                    <asp:TextBox ID="sNumFinancedProperties" runat="server" Width="90px"></asp:TextBox>
                    <asp:customvalidator id="sNumFinancedPropertiesValidator" runat="server" cssclass="RequireValidatorMessage" ValidateEmptyText="true" ControlToValidate="sNumFinancedProperties" ClientValidationFunction="f_validateNumFinancedProperties" Display="Dynamic"></asp:customvalidator>
                </td>
                <TD width=10></TD>
                <TD class=FieldLabel></TD>
                <TD class=FieldLabel></TD></tr>
              <TR>
              <% if (! m_IsStandAloneSecond) { %>
                <td class=FieldLabel>Has Housing History</td>
                <td class=FieldLabel><asp:checkbox id="sProdHasHousingHistory" Enabled="False" tabIndex=10 runat="server"/>Yes</td>
                <TD width=10></TD>
                <TD class=FieldLabel></TD>
                <TD class=FieldLabel></TD>
              </TR>
              <% } %>
              <TR>
                <TD class=FieldLabel><% if (! m_IsStandAloneSecond) { %>Interest Only <% } else { %>1st Lien is Interest Only <% } %></TD>
                <TD class=FieldLabel><ml:EncodedLiteral id=sIsIOnly runat="server"></ml:EncodedLiteral></TD>
                <TD width=10></TD>
                <TD class=FieldLabel></TD>
                <TD class=FieldLabel></TD></TR>
              <TR>
                <% if (! m_IsStandAloneSecond) { %>              
                <TD class=FieldLabel>Impound</TD>
                <TD class=FieldLabel><asp:checkbox id=sProdImpound tabIndex=10 runat="server" Text="Yes"></asp:checkbox></TD>
                <% } else {%> 
                  <td class="FieldLabel" nowrap>1st Lien has Negative Amort.</td>
                  <td class="FieldLabel" nowrap><asp:checkbox id="sLpIsNegAmortOtherLien" text="Yes (Potential)" runat="server" /></td>
                <% } %>
                <TD width=10></TD>
                <TD class=FieldLabel nowrap><span id="reservesAvailableLabel">Reserves Available</span>&nbsp;</TD>
                <TD class=FieldLabel><span id="reservesAvailableDD"> <asp:dropdownlist id=sProdAvailReserveMonths tabIndex=15 runat="server"></asp:dropdownlist></span></TD>
              </TR>
              <% if ( m_IsStandAloneSecond) { %>
              <tr>
                <td class="FieldLabel">1st Lien Amortization Type</td>
                <td class="FieldLabel"><asp:dropdownlist id="sOtherLFinMethT" runat="server" ></asp:dropdownlist></td>
			 </tr>
			<% } %>
              <tr>
                <td>&nbsp;&nbsp;</td>
                <td></td>
                <td width=10></td>
                <td class=FieldLabel></td>
                <td class=FieldLabel></td></tr>
              <tr>
                <td class=FieldLabel><a href="javascript:linkMe(loanPageUrl);" title="Go to 'This Loan Info'">House Value</A></td>
                <td><asp:textbox id=sHouseVal runat="server" ReadOnly="True" Width="90px" CssClass="RightAlign"></asp:textbox></td>
                <td width=10></td>
                <% if (! m_IsStandAloneSecond) { %>
                    <% if ( m_isAutoPmiOrNewPml  )  { %>
                    <TD class=FieldLabel>Conv Loan PMI Type</TD>
                    <TD class=FieldLabel><asp:dropdownlist id="sProdConvMIOptionT" tabIndex=10 runat="server"></asp:dropdownlist></TD>
                    <% } else { %>
                    <TD class=FieldLabel>Mortgage Insurance</TD>
                    <TD class=FieldLabel><asp:dropdownlist id="sProdMIOptionT" tabIndex=10 runat="server"></asp:dropdownlist></TD>
                    <% } %>
                
                <% } else { %> <td></td><td></td> <% } %>
                </tr>
              <tr>
                <td class=FieldLabel><a href="javascript:linkMe(loanPageUrl);" title="Go to 'This Loan Info'">Down Payment Percent</A></td>
                <td><asp:textbox id=sDownPmtPc runat="server" ReadOnly="True" Width="90px" CssClass="RightAlign"></asp:textbox></td>
                <td width=10></td>
                <td></td><td></td>
                </tr>
              <tr>
                <td class=FieldLabel><a href="javascript:linkMe(loanPageUrl);" title="Go to 'This Loan Info'">Down Payment</A></td>
                <td><asp:textbox id=sEquityCalc runat="server" ReadOnly="True" Width="90px" CssClass="RightAlign"></asp:textbox></td>
                <td width=10></td>

                 <% if (! m_IsStandAloneSecond && m_isAutoPmiOrNewPml ) { %>
                <TD class=FieldLabel nowrap>Split MI Upfront Premium</TD>
                <TD class=FieldLabel><asp:dropdownlist id="sConvSplitMIRT" tabIndex=10 runat="server"></asp:dropdownlist></TD>
                 
                 <% } else { %>
                    <td></td>
                    <td></td>
                <%} %>
                </tr>
              <tr>
                <td class=FieldLabel><a href="javascript:linkMe(loanPageUrl);" title="Go to 'This Loan Info'"><% if (! m_IsStandAloneSecond) { %>Target LTV <% } else { %>2nd Lien LTV <% } %></A></td>
                <td><asp:textbox id=sLtvR runat="server" ReadOnly="True" Width="90px" CssClass="RightAlign"></asp:textbox></td>
                <td width=10></td>
                <td class=FieldLabel><% if (! m_IsStandAloneSecond) { %>Other Financing LTV <% } else { %> 1st Lien LTV <% } %></td>
                <td><asp:textbox id=sLtvROtherFin runat="server" ReadOnly="True" Width="90px" CssClass="RightAlign"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel><a href="javascript:linkMe(loanPageUrl);" title="Go to 'This Loan Info'"><% if (! m_IsStandAloneSecond) { %> Loan Amount <% } else { %>2nd Lien Amount <% } %></A></td>
                <td><asp:textbox id=sLAmtCalc runat="server" ReadOnly="True" Width="90px" CssClass="RightAlign"></asp:textbox></td>
                <td width=10></td>
                <td class=FieldLabel><A href="javascript:linkMe('../LoanInfo.aspx?pg=2');" ><% if (! m_IsStandAloneSecond) { %>Other Financing Balance<% } else { %>1st Lien Balance  <% } %></A></td>
                <td><asp:textbox id=sProOFinBal runat="server" ReadOnly="True" Width="90px" CssClass="RightAlign"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel>Target CLTV</td>
                <td><asp:textbox id=sCltvR runat="server" ReadOnly="True" Width="90px" CssClass="RightAlign"></asp:textbox></td>
                <td width=10></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
              <% if (m_IsStandAloneSecond) { %>
                <td class=FieldLabel>1st Lien Payment</td>
                <td><ml:moneytextbox id="sProOFinPmt" runat="server" ReadOnly="True" Width="90px" CssClass="RightAlign"></ml:moneytextbox></td>
                <td width=10></td>
                <td></td>
                <td></td>
              </tr>
              <% } %>
                
              <tr>
                <td class="FieldLabel"><a id="QualRateCalcLink">Qual Rate</a></td>
                <td class="nowrap">
                    <ml:percenttextbox id="sQualIR" onchange="f_onchange_sQualIR(false);" runat="server" width="90px" preset="percent"></ml:percenttextbox>
                    <asp:CheckBox ID="sQualIRLckd" runat="server" Text="Lock" onclick="refreshCalculation();" />
                </td>
                <td width=10></td>
                <td class="FieldLabel">Is Qual Rate I/O?</td>
                <td class="FieldLabel"><asp:CheckBox ID="sIsQRateIOnly" Runat="server" Text="Yes"></asp:CheckBox></td>
              </tr>

              <tr>
                <td class="FieldLabel">Qual Term</td>
                <td class="nowrap" colspan="4">
                    <asp:DropDownList id="sQualTermCalculationType" runat="server" onchange="refreshCalculation();" CssClass="sQualTermCalculationType" />
                    &nbsp
                    <input type="text" id="sQualTerm" runat="server" preset="numeric" onchange="refreshCalculation();" class="sQualTerm" />
                </td>
              </tr>

              <tr>
                <td class="FieldLabel">Option ARM</td>
                <td class="FieldLabel"><asp:CheckBox ID="sIsOptionArm" onclick="f_onchange_sIsOptionArm();" Runat="server" Text="Yes"></asp:CheckBox></td>
                <td width=10></td>
                <td class="FieldLabel">Teaser Rate</td>
                <td><ml:percenttextbox id="sOptionArmTeaserR" runat="server" width="90px" preset="percent"></ml:percenttextbox></td>
              </tr>
              <tr>
                <td class="FieldLabel">Normal I/O</td>
                <td class="FieldLabel"><asp:TextBox id="sIOnlyMon" Runat=server Width="50px"></asp:TextBox>&nbsp;months</td>
                <td width=10></td>
                <td class="FieldLabel">Note Rate</td>
                <td><ml:percenttextbox id="sNoteIR" runat="server" width="90px" preset="percent" onchange="refreshCalculation();" ></ml:percenttextbox></td></tr>              
  <tr>
    <td class=FieldLabel>&nbsp;</td>
    <td></td>
    <td width=10></td>
    <td></td>
    <td></td></tr></table></td></tr></table>
    </td>
  </tr>
  <tr runat="server" id="OtherInfoPanel">
    <td style="PADDING-LEFT: 5px">
        <table class="InsetBorder w-830" cellSpacing=0 cellPadding=0 >
        <tr>
          <td colspan="2" class=LoanFormHeader>Other Info</td></tr>
        <tr>
            <td class="FieldLabel"> 
                Lender Fee Buyout
            </td>
            <td class="FieldLabel">
                <asp:DropDownList runat="server" ID="sLenderFeeBuyoutRequestedT" />
            </td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
      <td style="PADDING-LEFT: 5px">
        <%-- This table is populated server-side based on the Broker's configuration. --%>
        <table id="CustomPMLFieldsTable" class="InsetBorder w-830" cellspacing="0" cellpadding="0" runat="server">
            <tr><td class="LoanFormHeader" colspan="2">Custom PML Fields</td></tr>
        </table>
      </td>
  </tr>
</table></form>
	
  </body>
</HTML>
