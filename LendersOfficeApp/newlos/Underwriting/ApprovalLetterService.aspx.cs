﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos.Underwriting
{
    public class ApprovalLetterServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(ApprovalLetterServiceItem));
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "ClearAutoEligibilityProcessReport":
                    ClearAutoEligibilityProcessReport();
                    break;
            }
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            // Set value from webform to CPageData

            // dataLoan.sLNm = GetString("sLNm");

            string squal = GetString("sCreditScoreLpeQual");
            if (squal == "") { squal = "0"; }
            int score;
            if (!int.TryParse(squal, out score))
            {
                throw new CBaseException("Qualifying Score should be a positive number.", "User tried entering non numeric value for a numeric value field.");
            }
            dataLoan.sMaxDti_rep = GetString("sMaxDti");
            dataLoan.sApprRprtExpD_rep = GetString("sApprRprtExpD");
            dataLoan.sIncomeDocExpD_rep = GetString("sIncomeDocExpD");
            dataLoan.sCrExpD_rep = GetString("sCrExpD");
            dataLoan.sCreditScoreLpeQual = score;
            dataLoan.sMaxRLckd = GetBool("sMaxRLckd");
            dataLoan.sMaxR_rep = GetString("sMaxR");
            dataLoan.sAssetExpD_rep = GetString("sAssetExpD");
            dataLoan.sPrelimRprtExpD_rep = GetString("sPrelimRprtExpD");
            dataLoan.sBondDocExpD_rep = GetString("sBondDocExpD");
            if (!GetBool("hfIsSuspenseNotice"))
            {
                dataLoan.sAppExpDLckd = GetBool("sAppExpDLckd");
                dataLoan.sAppExpD_rep = GetString("sAppExpD");
                ApprovalLetter.BindDataFromControls(dataLoan, dataApp, this);
                dataLoan.sApprovD_rep = GetString("sApprovD");
                dataLoan.sFinalUnderwritingD_rep = GetString("sFinalUnderwritingD");
                dataLoan.sClearToCloseD_rep = GetString("sClearToCloseD");
                dataLoan.sCreditDecisionD_rep = GetString("sCreditDecisionD");
            }
            dataLoan.sIncludeUwContactInfoOnApprovalCert = GetBool("sIncludeUwContactInfoOnApprovalCert");
            dataLoan.sIncludeJrUwContactInfoOnApprovalCert = GetBool("sIncludeJrUwContactInfoOnApprovalCert");            
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            // Set value from CPageData to webform

            // SetResult("sLNm", dataLoan.sLNm);
            SetResult("sMaxDti", dataLoan.sMaxDti_rep);
            SetResult("IsOpenStatus", dataLoan.sStatusT == E_sStatusT.Loan_Open ? "1":"0");
            SetResult("sApprRprtExpD", dataLoan.sApprRprtExpD_rep);
            SetResult("sIncomeDocExpD", dataLoan.sIncomeDocExpD_rep);
            SetResult("sCrExpD", dataLoan.sCrExpD_rep);
            SetResult("sCreditScoreLpeQual", dataLoan.sCreditScoreLpeQual_rep);
            SetResult("sMaxR", dataLoan.sMaxR_rep);
            SetResult("sMaxRLckd", dataLoan.sMaxRLckd);
            SetResult("sAssetExpD", dataLoan.sAssetExpD_rep);
            SetResult("sPrelimRprtExpD", dataLoan.sPrelimRprtExpD_rep);
            SetResult("sBondDocExpD", dataLoan.sBondDocExpD_rep);
            SetResult("sAppExpD", dataLoan.sAppExpD_rep);
            SetResult("sAppExpDLckd", dataLoan.sAppExpDLckd);
            SetResult("sIncludeUwContactInfoOnApprovalCert", dataLoan.sIncludeUwContactInfoOnApprovalCert);
            SetResult("sIncludeJrUwContactInfoOnApprovalCert", dataLoan.sIncludeJrUwContactInfoOnApprovalCert);
            SetResult("sSuspendedD", dataLoan.sSuspendedD_rep);

            ApprovalLetter.LoadDataForControls(dataLoan, dataApp, this);

            SetResult("sApprovDTime", dataLoan.sApprovDTime_rep);
            SetResult("sFinalUnderwritingDTime", dataLoan.sFinalUnderwritingDTime_rep);
            SetResult("sClearToCloseDTime", dataLoan.sClearToCloseDTime_rep);
            SetResult("sCreditDecisionDTime", dataLoan.sCreditDecisionDTime_rep);
        }

        private void ClearAutoEligibilityProcessReport()
        {
            // OPM 247399.
            var loanId = GetGuid("loanid");
            CPageData dataLoan = ConstructPageDataClass(loanId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sAutoEligibilityProcessResultT = E_AutoEligibilityProcessResultT.ReportCleared;
            dataLoan.sAutoEligibilityProcessResultD = CDateTime.Create(DateTime.Now);
            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            SetResult("sAutoEligibilityProcessResult", dataLoan.sAutoEligibilityProcessResultT_rep);
            SetResult("sAutoEligibilityProcessResultD", dataLoan.sAutoEligibilityProcessResultD_rep);
        }
    }
    public partial class ApprovalLetterService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new ApprovalLetterServiceItem());
        }
    }


}
