<%@ Page language="c#" Codebehind="RateLockConfirmation_Frame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Underwriting.RateLockConfirmation_Frame"  EnableEventValidation="false" enableViewState="False"%>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Rate Lock Confirmation</title>
		<link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet"/>
		<style type="text/css">
		    body { background-color: #DCDCDC; font-weight:bold; overflow: hidden;  } 
		    .ButtonStyle { margin-right: 5px; width: 150px; }
		    #buttonSection { padding: 5px 0 20px 5px; }
		    p{ margin:0; padding:0; }
		    ul { margin: 0; padding: 10px 20px 10px 0; list-style-type: none; } 
		    li { display: inline; }
		    .header { font-size: 1.2em; }
		    .frameBody{
		    	 background-color:White;
		    }
		</style>
		<script type="text/javascript">
		        var certificateSaved = false;

			    function f_save() {
		            
		            var args = new Object();
		            args["LoanID"] = <%=AspxTools.JsString(RequestHelper.LoanID)%>;
		            args["FormType"] = "ratelock";
		            args["Notes"] = "";
		            
		            var result = gService.main.call("Save", args);
			        if (result.error) {
			            alert('Unable to save form. \n\n' + result.UserMessage);
			        }
			        
			        certificateSaved = true;
			        var saveBtn = document.getElementById("saveBtn");
			        saveBtn.disabled = true;
			        saveBtn.blur();
			        
			        document.getElementById("printBtn").value = "Print...";
			        document.getElementById("emailBtn").value = "Email...";
			        
			    }
			    function f_print() {
			        if (!certificateSaved) {
			            f_save();
			        }
			        
			        lqbPrintByFrame(frmBody);
			    }
			    function f_email() {
			    
			        if (!certificateSaved) {
                        f_save();
                    }

			        var args = 
			            {
			                FormType: "ratelock"
			            };

			        showModal(<%= AspxTools.JsString("/newlos/Underwriting/PmlLoanSummaryEmailWithComment.aspx?loanid=" + RequestHelper.LoanID + "&formType=raterelock") %>, args, null, null, null,{hideCloseButton:true});
			    }

                window.onresize = function() { f_resizeIframe('frmBody',10); };
		</script>
	</head>
	<body>  
		<form id="RateLockConfirmation_Frame" method="post" runat="server">
		    <div style="float:left" style="padding-left: 5px" class="FieldLabel header"> PML Rate Lock Confirmation Options </div> 
		    <div style="float:right"> <input type="button" onclick="onClosePopup();" value="Close" style="margin:5px 5px 0 0" class="ButtonStyle" /></div>
		    <div style="clear:both" id="buttonSection"> 
		        <input NoHighlight="true" id="saveBtn" type="button" onclick="f_save();" runat="server" value="Save to loan file" class="ButtonStyle"/>
		        <input NoHighlight="true" id="printBtn" type="button" onclick="f_print();" runat="server" value="Save and Print..." class="ButtonStyle"/> 
		        <input NoHighlight="true" id="emailBtn" type="button" onclick="f_email();" runat="server" value="Save and Email..." class="ButtonStyle" />(SSN's will be shown as ***-**-**** in email)
		    </div>
		    
		    <iframe  onload="f_resizeIframe('frmBody',10);" width="100%" src=<%= AspxTools.SafeUrl("RateLockConfirmationView.aspx?loanid=" + RequestHelper.LoanID) %>  frameborder="1" id="frmBody" class="frameBody"></iframe>

			<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
		</form>

		
	</body>
</html>
