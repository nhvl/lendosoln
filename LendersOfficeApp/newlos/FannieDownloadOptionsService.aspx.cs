using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;

using LendersOffice.CreditReport;
using LendersOffice.CreditReport.FannieMae;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.DU;
using LendersOffice.Constants;
using LendersOffice.Conversions;
using LendersOffice.ObjLib.ServiceCredential;
using System.Linq;

namespace LendersOfficeApp.newlos
{
	public partial class FannieDownloadOptionsService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "LogoutDoDu":
                    LogoutDoDu();
                    break;
                case "ImportToLO":
                    ImportToLO();
                    break;
                case "GetAndSaveCasefileStatus":
                    GetAndSaveCasefileStatus();
                    break;

            }
        }

        private void SetAuthenticationInformation(AbstractFnmaXisRequest request, Guid loanId)
        {
            bool isAutoLogin = GetBool("AutoLogin", false);
            bool isDo = GetBool("IsDo");

            if (isAutoLogin)
            {
                AbstractUserPrincipal principal = this.User as AbstractUserPrincipal;
                if (null == principal)
                {
                    Tools.LogBug("Authentication is not PriceMyLoanPrincipal. Unable to set auto login for DO/DU.");
                    return;
                }

                if (isDo)
                {
                    request.FannieMaeMORNETUserID = principal.DoAutoLoginNm;
                    request.FannieMaeMORNETPassword = principal.DoAutoPassword;
                }
                else
                {
                    if (principal.BrokerDB.UsingLegacyDUCredentials)
                    {
                        request.FannieMaeMORNETUserID = principal.DuAutoLoginNm;
                        request.FannieMaeMORNETPassword = principal.DuAutoPassword;
                    }
                    else
                    {
                        var dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(FannieDownloadOptionsService));
                        dataLoan.InitLoad();

                        var savedCredential = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, ServiceCredentialService.AusSubmission)
                                                .FirstOrDefault(credential => credential.ChosenAusOption.HasValue && credential.ChosenAusOption.Value == AusOption.DesktopUnderwriter && credential.IsEnabledForNonSeamlessDu);
                        if (savedCredential != null)
                        {
                            request.FannieMaeMORNETUserID = savedCredential.UserName;
                            request.FannieMaeMORNETPassword = savedCredential.UserPassword.Value;
                        }
                    }
                }
            }
            else
            {
                request.FannieMaeMORNETUserID = GetString("FannieMaeMORNETUserID");
                request.FannieMaeMORNETPassword = GetString("FannieMaeMORNETPassword");
            }
        }
        private void GetAndSaveCasefileStatus() 
        {
            try 
            {
                Guid sLId = GetGuid("LoanID");
                bool isDo = GetBool("IsDo");

                FnmaDwebCasefileStatusRequest request = new FnmaDwebCasefileStatusRequest(isDo, sLId);
                SetAuthenticationInformation(request, sLId);
                request.MornetPlusCasefileIdentifier = GetString("sDuCaseId");

                FnmaDwebCasefileStatusResponse response = (FnmaDwebCasefileStatusResponse) DUServer.Submit(request);
                if (null == response) 
                {
                    throw new CBaseException(ErrorMessages.Generic, "Unable to send Casefile_Status_Request to FNMA");
                } 
                else 
                {
                    response.SaveToLoanFile(sLId);
                }

                SetResult("LastUnderwritingDate", Tools.GetEasternTimeDescriptionFromExactString(response.LastUnderwritingDate));
                SetResult("UnderwritingRecommendationDescription", response.UnderwritingRecommendationDescription);
                SetResult("UnderwritingStatusDescription", response.UnderwritingStatusDescription);
                SetResult("UnderwritingSubmissionType", response.UnderwritingSubmissionType);
                SetResult("DesktopOriginatorSubmissionStatus", response.DesktopOriginatorSubmissionStatus);
                SetResult("ProductName", response.ProductName);
                SetResult("UpdateDate", Tools.GetEasternTimeDescriptionFromExactString(response.UpdateDate));
                SetResult("AllCreditStatusDescription", response.AllCreditStatusDescription);
                SetResult("LenderInstitutionIdentifier", response.LenderInstitutionIdentifier);
            } 
            catch (Exception exc) 
            {
                Tools.LogErrorWithCriticalTracking(exc);
            }


        }

        private void ImportToLO() 
        {
            LogoutDoDu();

            Guid sLId = GetGuid("LoanID");
            string globallyUniqueIdentifier = GetString("GloballyUniqueIdentifier");
            string status = "ERROR";
            string errorMessage = "";
            string sDuCaseId = GetString("sDuCaseId");
            string errorHeader = "<b>Fannie Mae Casefile Error:</b><br>";
            try 
            {
                AbstractFnmaXisRequest request = CreateCasefileExportRequest();

                AbstractFnmaXisResponse response = DUServer.Submit(request);

                if (response.IsReady) 
                {
                    if (response.HasError) 
                    {
                        status = "ERROR";
                        errorMessage = errorHeader + response.ErrorMessage;
                    }
                    else if (response.HasBusinessError) 
                    {
                        status = "ERROR";
                        errorMessage = errorHeader + response.BusinessErrorMessage + Environment.NewLine + "<pre>" +   response.FnmaStatusLog + "</pre>";

                    }
                    else 
                    {
                        status = "DONE";
                        sDuCaseId = response.MornetPlusCasefileIdentifier;
                        SaveCasefileExportResponse((FnmaXisCasefileExportResponse) response);
                        
                    }
                } 
                else 
                {
                    status = "PROCESSING";
                }
                globallyUniqueIdentifier = response.GloballyUniqueIdentifier;
                
            } 
            catch (CBaseException exc) 
            {
                status= "ERROR";
                errorMessage = "<b>LendingQB Error Response:</b><br>" + exc.UserMessage;
                Tools.LogErrorWithCriticalTracking("Unable to import to LO from DU", exc);
                
            }
            catch (Exception exc) 
            {
                status= "ERROR";
                errorMessage = "<b>LendingQB Error Response:</b><br>" + ErrorMessages.Generic;
                Tools.LogErrorWithCriticalTracking("Unable to import to LO from DU", exc);
            }

            SetResult("Status", status);
            SetResult("ErrorMessage", errorMessage);
            SetResult("GloballyUniqueIdentifier", globallyUniqueIdentifier);
            SetResult("sDuCaseId", sDuCaseId);
        }

        private FnmaXisCasefileExportRequest CreateCasefileExportRequest() 
        {
            Guid sLId = GetGuid("LoanID");
            bool isDo = GetBool("IsDo");

            FnmaXisCasefileExportRequest request = new FnmaXisCasefileExportRequest(sLId);
            SetAuthenticationInformation(request, sLId);
            request.GloballyUniqueIdentifier     = GetString("GloballyUniqueIdentifier");
            request.MornetPlusCasefileIdentifier = GetString("sDuCaseId");
            if (!isDo) 
            {
                // 11/5/2007 dd - DO Account cannot pass in LenderInstitutionIdentifier to retrieve DU Findings. They could only retrieve
                // preliminary findings.
                request.LenderInstitutionIdentifier  = GetString("LenderInstitutionIdentifier", "");
            }

            return request;
        }
        private void LogoutDoDu() 
        {
            Guid sLId = GetGuid("LoanID", Guid.Empty);
            bool isDo = GetBool("IsDo");

            FnmaDwebLogoutRequest request = new FnmaDwebLogoutRequest(isDo, Guid.Empty);
            SetAuthenticationInformation(request, sLId);
            
            DUServer.Submit(request);
        }
        private void SaveCasefileExportResponse(FnmaXisCasefileExportResponse response) 
        {
            Guid sLId = GetGuid("LoanID");
            bool isImport1003 = GetBool("IsImport1003");
            bool isImportDuFindings = GetBool("IsImportDuFindings");
            bool isImportCreditReport = GetBool("IsImportCreditReport");
            
            response.ImportToLoanFile(sLId, BrokerUserPrincipal.CurrentPrincipal, isImport1003, isImportDuFindings, isImportCreditReport);

        }
	}
}
