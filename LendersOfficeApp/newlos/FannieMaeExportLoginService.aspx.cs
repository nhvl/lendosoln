using System;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Audit;
using LendersOffice.Common;
using LendersOffice.DU;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.ObjLib.ServiceCredential;
using System.Linq;

namespace LendersOfficeApp.newlos
{
	public partial class FannieMaeExportLoginService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "ExportToDuDo":
                    ExportToDuDo();
                    break;
            }
        }

        private AbstractFnmaXisRequest CreateRequest() 
        {
            bool isAutoLogin = GetBool("AutoLogin", false);
            Guid sLId = GetGuid("LoanID");
            bool isDo = GetBool("IsDo");

            string fannieMaeMornetUserId = "";
            string fannieMaeMornetPassword = "";
            string sDuCaseId = "";
            string sDuLenderInstitutionId = "";

            if (isAutoLogin)
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(FannieMaeExportLoginService));
                dataLoan.InitLoad();
                sDuCaseId = dataLoan.sDuCaseId;

                AbstractUserPrincipal principal = this.User as AbstractUserPrincipal;
                if (isDo)
                {
                    fannieMaeMornetUserId = principal.DoAutoLoginNm;
                    fannieMaeMornetPassword = principal.DoAutoPassword;
                }
                else
                {
                    if (principal.BrokerDB.UsingLegacyDUCredentials)
                    {
                        fannieMaeMornetUserId = principal.DuAutoLoginNm;
                        fannieMaeMornetPassword = principal.DuAutoPassword;
                    }
                    else
                    {
                        var savedCredential = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, ServiceCredentialService.AusSubmission)
                                                .FirstOrDefault(credential => credential.ChosenAusOption.HasValue && credential.ChosenAusOption.Value == AusOption.DesktopUnderwriter && credential.IsEnabledForNonSeamlessDu);
                        if (savedCredential != null)
                        {
                            fannieMaeMornetUserId = savedCredential.UserName;
                            fannieMaeMornetPassword = savedCredential.UserPassword.Value;
                        }
                    }
                }
            }
            else
            {
                // 11/29/2007 dd - Use information provide by user.
                fannieMaeMornetUserId = GetString("FannieMaeMORNETUserID");
                fannieMaeMornetPassword = GetString("FannieMaeMORNETPassword");
                sDuCaseId = GetString("sDuCaseId", "");
                sDuLenderInstitutionId = GetString("sDuLenderInstitutionId", "");

                CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(FannieMaeExportLoginService));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.sDuCaseId = sDuCaseId;
                dataLoan.sDuLenderInstitutionId = sDuLenderInstitutionId;
                dataLoan.Save();

                bool rememberLastLogin = GetBool("RememberLastLogin", false);
                string rememberLoginNm = rememberLastLogin ? GetString("FannieMaeMORNETUserID") : "";
                string parameterName = isDo ? "@DOLastLoginNm" : "@DULastLoginNm";
                SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", BrokerUserPrincipal.CurrentPrincipal.UserId),
                                            new SqlParameter(parameterName, rememberLoginNm) 
                                        };
                StoredProcedureHelper.ExecuteNonQuery(BrokerUserPrincipal.CurrentPrincipal.BrokerId, "UpdateLastDoDuLpLoginNm", 3, parameters);

            }

            FnmaDwebCasefileImportRequest request = new FnmaDwebCasefileImportRequest(isDo, sLId);
            request.FannieMaeMORNETUserID        = fannieMaeMornetUserId;
            request.FannieMaeMORNETPassword      = fannieMaeMornetPassword;
            request.MornetPlusCasefileIdentifier = sDuCaseId;


            return request;

        }
        private void ExportToDuDo() 
        {
            string status = "ERROR";
            string errorMessage = "";

            AbstractFnmaXisRequest request = CreateRequest();

            try
            {
                AbstractFnmaXisResponse response = DUServer.Submit(request);

                if (response.IsReady)
                {
                    // DWEB Casefile Import only support synchronous connection.
                    if (response.HasError)
                    {
                        status = "ERROR";
                        errorMessage = response.ErrorMessage;
                    }
                    else if (response.HasBusinessError)
                    {
                        status = "ERROR";
                        errorMessage = response.BusinessErrorMessage + Environment.NewLine + response.FnmaStatusLog;
                    }
                    else
                    {

                        AbstractUserPrincipal principal = this.User as AbstractUserPrincipal;
                        bool isDo = GetBool("IsDo");
                        Guid sLId = GetGuid("LoanID");
                        SubmitToFnmaAuditItem audit = new SubmitToFnmaAuditItem(principal, isDo, request.FannieMaeMORNETUserID);
                        AuditManager.RecordAudit(sLId, audit);

                        status = "DONE";
                        SaveResponse((FnmaDwebCasefileImportResponse)response);

                        string cacheAuth = string.Format("{0}\n{1}\n{2}\n{3}", request.Url, request.FannieMaeMORNETUserID, request.FannieMaeMORNETPassword, response.MornetPlusCasefileIdentifier);
                        string id = AutoExpiredTextCache.AddToCache(cacheAuth, new TimeSpan(0, 1, 0)); // Cache for 1 minute.

                        SetResult("sDuCaseId", response.MornetPlusCasefileIdentifier);
                        SetResult("id", id);
                    }
                }
            }
            catch (FnmaInvalidDataException exc)
            {
                status = "ERROR";
                errorMessage = exc.UserMessage;

            }
            catch (CBaseException exc)
            {
                status = "ERROR";
                errorMessage = exc.UserMessage;
                Tools.LogErrorWithCriticalTracking("Unable to export file to DU", exc);
            }
            catch (Exception exc)
            {
                status = "ERROR";
                errorMessage = ErrorMessages.Generic;
                Tools.LogErrorWithCriticalTracking("Unable to export file to DU", exc);
            }


            SetResult("Status", status);
            SetResult("ErrorMessage", errorMessage);
        }

        private void SaveResponse(FnmaDwebCasefileImportResponse response) 
        {
            Guid sLId = GetGuid("LoanID");
            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
            response.SaveToFileDB(sLId);

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(FannieMaeExportLoginService));

            dataLoan.InitSave(sFileVersion);
            dataLoan.sDuCaseId = response.MornetPlusCasefileIdentifier;
            dataLoan.Save();
        }

	}
}
