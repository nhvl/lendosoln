<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="SubjPropInvestment.ascx.cs" Inherits="LendersOfficeApp.newlos.SubjPropInvestment" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" EnableViewState="False" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<script type="text/javascript">
    function f_initControl()
    {
        refreshUI();
    }

    function refreshUI()
    {
        <%= AspxTools.JsGetElementById(sOccR) %>.readOnly = <%= AspxTools.JsGetElementById(sOccRLckd) %>.checked == false;
    }

    function onOccupancyChanged()
    {
        var appOccupancy = <%= AspxTools.JsGetElementById(aOccT) %>;
        var propertyWillBePrimaryResidence = <%= AspxTools.JsGetElementById(sPrimResid) %>;

        if (appOccupancy.options[appOccupancy.selectedIndex].value === "0") // Primary Residence
        {
            propertyWillBePrimaryResidence.checked = true;
        }
        else
        {
            propertyWillBePrimaryResidence.checked = ML.isPrimaryResidence;
        }

        refreshCalculation();
    }
</script>
<table>
    <tr>
        <td class="MainRightHeader" nowrap colspan="4">Subject Property Rental Income
        </td>
    </tr>
    <tr>
        <td>
            <table class="InsetBorder" cellspacing="0" cellpadding="0" width="300">
                <tr>
                    <td class="FieldLabel" nowrap colspan="3">
                        Subject Prop Occupancy for&nbsp;<ml:EncodedLiteral ID="aBNm" runat="server"   />
                        &nbsp;&nbsp;
                        <asp:DropDownList ID="aOccT" runat="server" onchange="onOccupancyChanged();" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">Gross Rent</td>
                    <td class="FieldLabel"></td>
                    <td class="FieldLabel" style="width: 100%">
                        <ml:MoneyTextBox ID="sSpGrossRent" preset="money" Width="70" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap>Occupancy Rate</td>
                    <td class="FieldLabel">
                        <asp:CheckBox ID="sOccRLckd" Text="Lock" runat="server" onclick="refreshCalculation();" />
                    </td>
                    <td class="FieldLabel">
                        <ml:PercentTextBox ID="sOccR" preset="percent" Width="70" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap colspan="3">
                        <asp:CheckBox ID="sSpCountRentalIForPrimaryResidToo" runat="server" Text="Primary Residence Subj Prop Can Have Rental Income" /></td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap colspan="3">
                        <asp:CheckBox ID="sPrimResid" runat="server" Enabled="False" />At least one borrower or coborrower will use subject property as primary residence</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
