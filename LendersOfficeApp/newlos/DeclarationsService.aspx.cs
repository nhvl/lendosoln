﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using System.Collections.Generic;

namespace LendersOfficeApp.newlos
{

    public class DeclarationsServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(DeclarationsServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.aBDecAlimony = GetString("aBDecAlimony").ToUpper();
            dataApp.aBDecBankrupt = GetString("aBDecBankrupt").ToUpper();
            dataApp.aBDecBorrowing = GetString("aBDecBorrowing").ToUpper();
            dataApp.aBDecCitizen = GetString("aBDecCitizen").ToUpper();
            dataApp.aBDecDelinquent = GetString("aBDecDelinquent").ToUpper();
            dataApp.aBDecEndorser = GetString("aBDecEndorser").ToUpper();
            dataApp.aBDecForeclosure = GetString("aBDecForeclosure").ToUpper();
            dataApp.aBDecJudgment = GetString("aBDecJudgment").ToUpper();
            dataApp.aBDecLawsuit = GetString("aBDecLawsuit").ToUpper();
            dataApp.aBDecObligated = GetString("aBDecObligated").ToUpper();
            dataApp.aBDecOcc = GetString("aBDecOcc").ToUpper();
            dataApp.aBDecPastOwnedPropT = (E_aBDecPastOwnedPropT)GetInt("aBDecPastOwnedPropT");
            dataApp.aBDecPastOwnedPropTitleT = (E_aBDecPastOwnedPropTitleT)GetInt("aBDecPastOwnedPropTitleT");
            dataApp.aBDecPastOwnership = GetString("aBDecPastOwnership").ToUpper();
            dataApp.aBDecResidency = GetString("aBDecResidency").ToUpper();

            dataApp.aCDecAlimony = GetString("aCDecAlimony").ToUpper();
            dataApp.aCDecBankrupt = GetString("aCDecBankrupt").ToUpper();
            dataApp.aCDecBorrowing = GetString("aCDecBorrowing").ToUpper();
            dataApp.aCDecCitizen = GetString("aCDecCitizen").ToUpper();
            dataApp.aCDecDelinquent = GetString("aCDecDelinquent").ToUpper();
            dataApp.aCDecEndorser = GetString("aCDecEndorser").ToUpper();
            dataApp.aCDecForeclosure = GetString("aCDecForeclosure").ToUpper();
            dataApp.aCDecJudgment = GetString("aCDecJudgment").ToUpper();
            dataApp.aCDecLawsuit = GetString("aCDecLawsuit").ToUpper();
            dataApp.aCDecObligated = GetString("aCDecObligated").ToUpper();
            dataApp.aCDecOcc = GetString("aCDecOcc").ToUpper();
            dataApp.aCDecPastOwnedPropT = (E_aCDecPastOwnedPropT)GetInt("aCDecPastOwnedPropT");
            dataApp.aCDecPastOwnedPropTitleT = (E_aCDecPastOwnedPropTitleT)GetInt("aCDecPastOwnedPropTitleT");
            dataApp.aCDecPastOwnership = GetString("aCDecPastOwnership").ToUpper();
            dataApp.aCDecResidency = GetString("aCDecResidency").ToUpper();

            dataApp.aBDecForeignNational = GetString("aBDecForeignNational").ToUpper();

            dataApp.aBDecJudgmentExplanation = GetString("aBDecJudgmentExplanation");
            dataApp.aCDecJudgmentExplanation = GetString("aCDecJudgmentExplanation");
            dataApp.aBDecBankruptExplanation = GetString("aBDecBankruptExplanation");
            dataApp.aCDecBankruptExplanation = GetString("aCDecBankruptExplanation");
            dataApp.aBDecForeclosureExplanation = GetString("aBDecForeclosureExplanation");
            dataApp.aCDecForeclosureExplanation = GetString("aCDecForeclosureExplanation");
            dataApp.aBDecLawsuitExplanation = GetString("aBDecLawsuitExplanation");
            dataApp.aCDecLawsuitExplanation = GetString("aCDecLawsuitExplanation");
            dataApp.aBDecObligatedExplanation = GetString("aBDecObligatedExplanation");
            dataApp.aCDecObligatedExplanation = GetString("aCDecObligatedExplanation");
            dataApp.aBDecDelinquentExplanation = GetString("aBDecDelinquentExplanation");
            dataApp.aCDecDelinquentExplanation = GetString("aCDecDelinquentExplanation");
            dataApp.aBDecAlimonyExplanation = GetString("aBDecAlimonyExplanation");
            dataApp.aCDecAlimonyExplanation = GetString("aCDecAlimonyExplanation");
            dataApp.aBDecBorrowingExplanation = GetString("aBDecBorrowingExplanation");
            dataApp.aCDecBorrowingExplanation = GetString("aCDecBorrowingExplanation");
            dataApp.aBDecEndorserExplanation = GetString("aBDecEndorserExplanation");
            dataApp.aCDecEndorserExplanation = GetString("aCDecEndorserExplanation");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("aBDecAlimony", dataApp.aBDecAlimony.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecBankrupt", dataApp.aBDecBankrupt.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecBorrowing", dataApp.aBDecBorrowing.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecCitizen", dataApp.aBDecCitizen.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecDelinquent", dataApp.aBDecDelinquent.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecEndorser", dataApp.aBDecEndorser.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecForeclosure", dataApp.aBDecForeclosure.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecJudgment", dataApp.aBDecJudgment.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecLawsuit", dataApp.aBDecLawsuit.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecObligated", dataApp.aBDecObligated.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecOcc", dataApp.aBDecOcc.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecPastOwnedPropT", dataApp.aBDecPastOwnedPropT);
            SetResult("aBDecPastOwnedPropTitleT", dataApp.aBDecPastOwnedPropTitleT);
            SetResult("aBDecPastOwnership", dataApp.aBDecPastOwnership.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aBDecResidency", dataApp.aBDecResidency.ToUpper().TrimWhitespaceAndBOM());

            SetResult("aCDecAlimony", dataApp.aCDecAlimony.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecBankrupt", dataApp.aCDecBankrupt.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecBorrowing", dataApp.aCDecBorrowing.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecCitizen", dataApp.aCDecCitizen.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecDelinquent", dataApp.aCDecDelinquent.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecEndorser", dataApp.aCDecEndorser.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecForeclosure", dataApp.aCDecForeclosure.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecJudgment", dataApp.aCDecJudgment.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecLawsuit", dataApp.aCDecLawsuit.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecObligated", dataApp.aCDecObligated.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecOcc", dataApp.aCDecOcc.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecPastOwnedPropT", dataApp.aCDecPastOwnedPropT);
            SetResult("aCDecPastOwnedPropTitleT", dataApp.aCDecPastOwnedPropTitleT);
            SetResult("aCDecPastOwnership", dataApp.aCDecPastOwnership.ToUpper().TrimWhitespaceAndBOM());
            SetResult("aCDecResidency", dataApp.aCDecResidency.ToUpper().TrimWhitespaceAndBOM());

            SetResult("aBDecForeignNational", dataApp.aBDecForeignNational.ToUpper().TrimWhitespaceAndBOM());


        }
    }

    public partial class DeclarationsService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {

        protected override void Initialize()
        {
            AddBackgroundItem("", new DeclarationsServiceItem());
        }
    }
}