namespace LendersOfficeApp.newlos
{
    using System.Collections.Specialized;
    using DataAccess;
    using LendersOffice.ObjLib;

    public partial class AssetRecordService : AbstractSingleEditService2 
    {
        protected override void SaveData(CPageData dataLoan, CAppData dataApp, IRecordCollection collection, ICollectionItemBase2 record)
        {
            dataLoan.sLienToIncludeCashDepositInTridDisclosures = GetEnum<ResponsibleLien>("sLienToIncludeCashDepositInTridDisclosures");
            dataApp.aAsstLiaCompletedNotJointly = GetBool("aAsstLiaCompletedNotJointly");

            IAssetCollection recordList = (IAssetCollection) collection;
            var field = (IAssetRegular) record;
            if (null != field) 
            {

                field.AssetT = (E_AssetRegularT) GetInt("AssetT");
                field.VerifExpD_rep = GetString("VerifExpD");
                field.VerifRecvD_rep = GetString("VerifRecvD");
                field.VerifSentD_rep = GetString("VerifSentD");
                field.VerifReorderedD_rep = GetString("VerifReorderedD");
                field.PhoneNumber = GetString("PhoneNumber");
                field.Val_rep = GetString("Val");
                field.Desc = GetString("Desc");
                field.AccNum = GetString("AccNum");
                field.StAddr = GetString("StAddr");
                field.City = GetString("City");
                field.State = GetString("State");
                field.Zip = GetString("Zip");
                field.AccNm = GetString("AccNm");
                field.ComNm = GetString("ComNm");
                field.DepartmentName = GetString("DepartmentName");
                field.OtherTypeDesc = GetString("OtherTypeDesc");
                field.OwnerT = (E_AssetOwnerT) GetInt("OwnerT");
                field.GiftSource = (E_GiftFundSourceT)GetInt("GiftSource");
            }

            // Save special record.
            var business = recordList.GetBusinessWorth(true);
            business.Val_rep = GetString("Business_Val");

            var cashDeposit = recordList.GetCashDeposit1(true);
            cashDeposit.Desc = GetString("CashDeposit1_Desc");
            cashDeposit.Val_rep = GetString("CashDeposit1_Val");

            cashDeposit = recordList.GetCashDeposit2(true);
            cashDeposit.Desc = GetString("CashDeposit2_Desc");
            cashDeposit.Val_rep = GetString("CashDeposit2_Val");

            var lifeInsurance = recordList.GetLifeInsurance(true);
            lifeInsurance.FaceVal_rep = GetString("LifeInsurance_FaceVal");
            lifeInsurance.Val_rep = GetString("LifeInsurance_Val");

            var retirement = recordList.GetRetirement(true);
            retirement.Val_rep = GetString("Retirement_Val");
        
        }

        protected override CPageData DataLoan
        {
            get { return new CAssetRecordData(LoanID); }
        }

        protected override IRecordCollection LoadRecordList(CPageData dataLoan, CAppData dataApp)
        {
            return dataApp.aAssetCollection;
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp, ICollectionItemBase2 record)
        {
            NameValueCollection ret = new NameValueCollection();
            var field = (IAssetRegular) record;

            if (null != field) 
            {
                SetResult("RecordID", field.RecordId);
                SetResult("AssetT", field.AssetT);
                SetResult("AssetT_rep", field.AssetT_rep);
                SetResult("VerifExpD", field.VerifExpD_rep);
                SetResult("VerifRecvD", field.VerifRecvD_rep);
                SetResult("VerifSentD", field.VerifSentD_rep);
                SetResult("VerifReorderedD", field.VerifReorderedD_rep);
                SetResult("PhoneNumber", field.PhoneNumber);
                SetResult("Val", field.Val_rep );
                SetResult("Desc", field.Desc );
                SetResult("AccNum", field.AccNum.Value);
                SetResult("StAddr", field.StAddr );
                SetResult("City", field.City );
                SetResult("State", field.State );
                SetResult("Zip", field.Zip );
                SetResult("AccNm", field.AccNm );
                SetResult("ComNm", field.ComNm );
                SetResult("DepartmentName", field.DepartmentName );
                SetResult("OtherTypeDesc", field.OtherTypeDesc );
                SetResult("OwnerT", field.OwnerT);
                SetResult("OwnerT_rep", field.OwnerT_rep);
                SetResult("GiftSource", field.GiftSource);

            }

            IAssetCollection recordList = dataApp.aAssetCollection;

            var cashDeposit = recordList.GetCashDeposit1(false);
            if (null != cashDeposit) 
            {
                SetResult("CashDeposit1_Desc", cashDeposit.Desc);
                SetResult("CashDeposit1_Val", cashDeposit.Val_rep);
            } 
            else 
            {
                SetResult("CashDeposit1_Desc", "");
                SetResult("CashDeposit1_Val", "");

            }

            cashDeposit = recordList.GetCashDeposit2(false);
            if (null != cashDeposit) 
            {
                SetResult("CashDeposit2_Desc", cashDeposit.Desc);
                SetResult("CashDeposit2_Val", cashDeposit.Val_rep);
            } 
            else 
            {
                SetResult("CashDeposit2_Desc", "");
                SetResult("CashDeposit2_Val", "");

            }

            var lifeInsurance = recordList.GetLifeInsurance(false);
            if (null != lifeInsurance) 
            {
                SetResult("LifeInsurance_FaceVal", lifeInsurance.FaceVal_rep);
                SetResult("LifeInsurance_Val", lifeInsurance.Val_rep);
            } 
            else 
            {
                SetResult("LifeInsurance_FaceVal", "");
                SetResult("LifeInsurance_Val", "");
            }

            var retirement = recordList.GetRetirement(false);
            if (null != retirement) 
            {
                SetResult("Retirement_Val", retirement.Val_rep);
            } 
            else 
            {
                SetResult("Retirement_Val", "");
            }

            var business = recordList.GetBusinessWorth(false);
            if (null != business) 
            {
                SetResult("Business_Val", business.Val_rep);
            } 
            else 
            {
                SetResult("Business_Val", "");
            }
            // Retrieve total & special values.
            SetResult("aAsstLiqTot", dataApp.aAsstLiqTot_rep);
            SetResult("aReTotVal", dataApp.aReTotVal_rep);
            SetResult("aAsstNonReSolidTot", dataApp.aAsstNonReSolidTot_rep);
            SetResult("aAsstValTot", dataApp.aAsstValTot_rep);

        }
    }
}
