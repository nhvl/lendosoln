﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos
{
    public partial class SandboxList : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.PageTitle = "Sandbox Loans";
            this.PageID = "Page_SandboxList";
            m_mainGrid.ItemCommand += new DataGridCommandEventHandler(m_mainGrid_ItemCommand);
        }

        void m_mainGrid_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            Guid sLId = (Guid)m_mainGrid.DataKeys[e.Item.ItemIndex];

            if (e.CommandName == "delete")
            {
                try
                {
                    Tools.DeclareLoanFileInvalid(this.BrokerUser, sLId, false, false);
                }
                catch (LoanDeletedPermissionDenied)
                {
                    var scriptManager = Page.ClientScript;
                    scriptManager.RegisterClientScriptBlock(
                        this.GetType(),
                        "PermissionDenied",
                        "alert('Deleting sandbox files requires the loan file delete permission.');",
                        true);
                }
            }
            LoadData();
        }

        protected override void LoadData()
        {
            var list = Tools.ListSandboxLoans(this.BrokerID, this.LoanID).OrderByDescending(o=>o.sCreatedD);

            m_mainGrid.DataSource = list;
            m_mainGrid.DataBind();
        }
    }
}
