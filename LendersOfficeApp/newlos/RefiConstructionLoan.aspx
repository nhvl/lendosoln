﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RefiConstructionLoan.aspx.cs" Inherits="LendersOfficeApp.newlos.RefiConstructionLoan" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="DataAccess"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Refi/Construction Loan</title>
        <style type="text/css">
            .date-td {
                white-space: nowrap;
            }

            .construction-table {
                width: 700px;
            }
    </style>
</head>
<body  bgcolor="gainsboro">

<script language="javascript">
<!--
  function displayConstruction() {
    var v = document.getElementById('sLPurposeT').value;
    var bConstruction = v == <%= AspxTools.JsString(E_sLPurposeT.Construct) %> || v == <%= AspxTools.JsString(E_sLPurposeT.ConstructPerm) %>;
    var bRefinance = v == <%= AspxTools.JsString(E_sLPurposeT.Refin) %> || v == <%= AspxTools.JsString(E_sLPurposeT.RefinCashout)%> || v == <%= AspxTools.JsString(E_sLPurposeT.FhaStreamlinedRefinance)%> || v == <%= AspxTools.JsString(E_sLPurposeT.VaIrrrl) %> || v == <%= AspxTools.JsString(E_sLPurposeT.HomeEquity) %>;
    <%= AspxTools.JsGetElementById(sLotOrigC) %>.readOnly = !bConstruction;
    <%= AspxTools.JsGetElementById(sLotVal) %>.readOnly = !bConstruction;
    <%= AspxTools.JsGetElementById(sLotAcqYr) %>.readOnly = !bConstruction;
    <%= AspxTools.JsGetElementById(sLotImprovC) %>.readOnly = ML.AreConstructionLoanCalcsMigrated || !bConstruction;
    <%= AspxTools.JsGetElementById(sLotLien) %>.readOnly = ML.AreConstructionLoanCalcsMigrated || !bConstruction;
    <%= AspxTools.JsGetElementById(sLotAcquiredD) %>.readOnly = !bConstruction;
    <%= AspxTools.JsGetElementById(sSpAcqYr) %>.readOnly = !bRefinance;
    <%= AspxTools.JsGetElementById(sSpOrigC) %>.readOnly = !bRefinance;
    <%= AspxTools.JsGetElementById(sSpLien) %>.readOnly = !bRefinance;
    <%= AspxTools.JsGetElementById(sRefPurpose) %>.readOnly = !bRefinance;
    <%= AspxTools.JsGetElementById(sSpImprovDesc) %>.readOnly = !bRefinance;
    disableDDL(<%= AspxTools.JsGetElementById(sSpImprovTimeFrameT) %>, !bRefinance)
  
    <%= AspxTools.JsGetElementById(sSpImprovC) %>.readOnly = !bRefinance;

    $("#LotDateSpan").text(ML.AreConstructionLoanCalcsMigrated ? "Date Acquired" : "Yr Acquired");
    $("#sLotAcquiredDTd").toggle(ML.AreConstructionLoanCalcsMigrated);
    $("#sLotAcqYrTd").toggle(!ML.AreConstructionLoanCalcsMigrated);    

    $(".sLotLien-span").toggle(!ML.AreConstructionLoanCalcsMigrated);
    $(".liabilities-link").toggle(ML.AreConstructionLoanCalcsMigrated);

    $(".sLotValTd").toggle(!ML.AreConstructionLoanCalcsMigrated);
    $(".sPresentValOfLotTd").toggle(ML.AreConstructionLoanCalcsMigrated);
  }
  
    function _init() {
        displayConstruction();
        $(".liabilities-link").click(function(){
            linkMe("BorrowerInfo.aspx", "pg=4");
        });
    }
    //-->
</script>
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
            <td class="MainRightHeader" nowrap>
                Refi/Construction Loan
            </td>
        </tr>
    <tr>
		<td>
			<table class="InsetBorder construction-table" style="border-color:White" cellSpacing="0" cellPadding="0" width="99%" border="0">
				<tr>
					<td class="FormTableSubheader" colSpan="6">Complete this line if construction or 
						construction-permanent loan</td>
				</tr>
				<tr>
					<td class="FieldLabel" title="Year Lot Acquired"><span id="LotDateSpan" /></td>
					<td class="FieldLabel">Original Cost</td>
					<td class="FieldLabel">
                        <span class="sLotLien-span">Existing Liens</span>
                        <a class="liabilities-link">Existing Liens</a>
					</td>
					<td class="FieldLabel">(a) Present Value of Lot</td>
					<td class="FieldLabel" title="Cost of Improvements">(b) Improvements</td>
					<td class="FieldLabel">Total (a + b)</td>
				</tr>
				<tr>

                    <td vAlign="top" id="sLotAcqYrTd" title="Year Lot Acquired">
                        <asp:textbox id="sLotAcqYr" tabIndex="82" runat="server" MaxLength="4" Width="63px"></asp:textbox>
                    </td>
					<td vAlign="top" id="sLotAcquiredDTd" class="date-td" title="Date Lot Acquired">
                        <ml:DateTextBox TabIndex="83" ID="sLotAcquiredD" runat="server"></ml:DateTextBox>
					</td>
					<td vAlign="top"><ml:moneytextbox id="sLotOrigC" tabIndex="84" runat="server" preset="money" width="90"></ml:moneytextbox></td>
					<td vAlign="top"><ml:moneytextbox id="sLotLien" tabIndex="86" runat="server" preset="money" width="90"></ml:moneytextbox></td>
                    <td vAlign="top" class="sLotValTd"><ml:moneytextbox id="sLotVal" tabIndex="88" runat="server" onchange="refreshCalculation();" preset="money" width="90"></ml:moneytextbox></td>
					<td vAlign="top" class="sPresentValOfLotTd"> <ml:MoneyTextBox ID="sPresentValOfLot" Readonly="true" TabIndex="88" runat="server" onchange="refreshCalculation();" preset="money" Width="90"></ml:MoneyTextBox></td>
					<td vAlign="top" title="Cost of Improvements"><ml:moneytextbox id="sLotImprovC" tabIndex="90" runat="server" onchange="refreshCalculation();" preset="money" width="90"></ml:moneytextbox></td>
					<td vAlign="top"><ml:moneytextbox id="sLotWImprovTot" tabIndex="92" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table class="InsetBorder" style="border-color:White" id="Table7" cellSpacing="2" cellPadding="0" width="99%" border="0">
				<tr>
					<td class="FormTableSubheader" colSpan="4">Complete this line if this is a 
						refinance loan</td>
				</tr>
				<tr>
					<td class="FieldLabel">Yr Acquired</td>
					<td class="FieldLabel">Original Cost</td>
					<td class="FieldLabel">Existing Liens</td>
					<td class="FieldLabel">Purpose of Refinance</td>
				</tr>
				<tr>
					<td vAlign="top"><asp:textbox id="sSpAcqYr" tabIndex="94" runat="server" MaxLength="4" Width="63px"></asp:textbox></td>
					<td vAlign="top"><ml:moneytextbox id="sSpOrigC" tabIndex="96" runat="server" preset="money" width="90"></ml:moneytextbox></td>
					<td vAlign="top"><ml:moneytextbox id="sSpLien" tabIndex="98" runat="server" preset="money" width="90"></ml:moneytextbox></td>
					<td class="FieldLabel" vAlign="top" noWrap><ml:combobox id="sRefPurpose" tabIndex="100" runat="server" Width="200px"></ml:combobox></td>
				</tr>
				<tr>
					<td class="FieldLabel" vAlign="top" colSpan="4">Describe Improvements&nbsp;<asp:textbox id="sSpImprovDesc" tabIndex="102" runat="server" MaxLength="60" Width="226px"></asp:textbox><asp:dropdownlist id="sSpImprovTimeFrameT" tabIndex="104" runat="server"></asp:dropdownlist>&nbsp;&nbsp;Cost&nbsp;<ml:moneytextbox id="sSpImprovC" tabIndex="106" runat="server" preset="money" width="90"></ml:moneytextbox></td>
				</tr>
			</table>
		</td>
	</tr>
    </table>
    
    </div>
    </form>
</body>
</html>
