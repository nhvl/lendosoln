<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BorrowerLiabilityFrame.ascx.cs" Inherits="LendersOfficeApp.newlos.BorrowerLiabilityFrame" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import namespace="DataAccess" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
  <script language=javascript>
  <!--
function _init() {
  <% if (IsReadOnly) { %>
    document.getElementById("btnPrev").disabled = true;
    document.getElementById("btnNext").disabled = true;
    document.getElementById("btnInsert").disabled = true;
    document.getElementById("btnAdd").disabled = true;
    document.getElementById("btnSave").disabled = true;
    document.getElementById("btnUp").disabled = true;    
    document.getElementById("btnDown").disabled = true;
    document.getElementById("btnDelete").disabled = true;    
  <% } %>
    f_onresize();

    var lienToPayoffDiv = $("#sLienToPayoffTotDebtDiv");

    lienToPayoffDiv.toggle(ML.IsTrid2);
    lienToPayoffDiv.find("select").prop("disabled", !ML.EnableLienToPayoffTotDebt);
    }

function f_reloadPage() {
  self.location = self.location;
}

function refreshCalculation() {
    callFrameMethod(window, "SingleIFrame", g_currentMode == 0 ? "refreshCalculation" : "f_refreshCalculation");
}

    
    function displayCreditReport()
    {
	    var report_url = gVirtualRoot + "/newlos/services/ViewCreditFrame.aspx?loanid=" + <%=AspxTools.JsString(LoanID)%> + "&applicationid=" + encodeURIComponent(parent.info.f_getCurrentApplicationID()) + "&islead=<%=AspxTools.HtmlStringFromQueryString("islead")%>";
	    var win = openWindowWithArguments(report_url, "report", "toolbar=no,menubar=no,location=no,status=no,resizable=yes") ;
	    if (null != win) win.focus() ;
    	return false;
  
    }
     
    function getAlimony_ComNm(value) { return <%= AspxTools.JsGetElementById(Alimony_ComNm) %>.value;  }
    function setAlimony_ComNm(value) { 
      if (value != null) {
        <%= AspxTools.JsGetElementById(Alimony_ComNm) %>.value = value || ''; 
      }
    }
    
    function getAsstLiaCompletedNotJointly() { return <%= AspxTools.JsGetElementById(aAsstLiaCompletedNotJointly) %>.checked; }
     
    function getAlimony_Pmt() { return <%= AspxTools.JsGetElementById(Alimony_Pmt) %>.value; }
    function setAlimony_Pmt(value) { if (value == null) return; <%= AspxTools.JsGetElementById(Alimony_Pmt) %>.value = value; }
    
    function getAlimony_RemainMons() { return <%= AspxTools.JsGetElementById(Alimony_RemainMons) %>.value; }
    function setAlimony_RemainMons(value) { if (value == null) return; <%= AspxTools.JsGetElementById(Alimony_RemainMons) %>.value = value; }
    
    function getAlimony_NotUsedInRatio() { return <%= AspxTools.JsGetElementById(Alimony_NotUsedInRatio) %>.checked ? "True" : "False"; }
    function setAlimony_NotUsedInRatio(value) { if (value == null) return; <%= AspxTools.JsGetElementById(Alimony_NotUsedInRatio) %>.checked = value; }

    function getChildSupport_ComNm() { return <%= AspxTools.JsGetElementById(ChildSupport_ComNm) %>.value; }
    function setChildSupport_ComNm(value) { if (value == null) return; <%= AspxTools.JsGetElementById(ChildSupport_ComNm) %>.value = value; }

    function getChildSupport_Pmt() { return <%= AspxTools.JsGetElementById(ChildSupport_Pmt) %>.value; }
    function setChildSupport_Pmt(value) { if (value == null) return; <%= AspxTools.JsGetElementById(ChildSupport_Pmt) %>.value = value; }
    
    function getChildSupport_RemainMons() { return <%= AspxTools.JsGetElementById(ChildSupport_RemainMons) %>.value; }
    function setChildSupport_RemainMons(value) { if (value == null) return; <%= AspxTools.JsGetElementById(ChildSupport_RemainMons) %>.value = value; }
    
    function getChildSupport_NotUsedInRatio() { return <%= AspxTools.JsGetElementById(ChildSupport_NotUsedInRatio) %>.checked ? "True" : "False"; }
    function setChildSupport_NotUsedInRatio(value) { if (value == null) return; <%= AspxTools.JsGetElementById(ChildSupport_NotUsedInRatio) %>.checked = value; }
    
    function getJobRelated1_ComNm() { return <%= AspxTools.JsGetElementById(JobRelated1_ComNm) %>.value; }
    function setJobRelated1_ComNm(value) { if (value == null) return; <%= AspxTools.JsGetElementById(JobRelated1_ComNm) %>.value = value; }
    
    function getJobRelated1_Pmt() { return <%= AspxTools.JsGetElementById(JobRelated1_Pmt) %>.value; }
    function setJobRelated1_Pmt(value) { if (value == null) return; <%= AspxTools.JsGetElementById(JobRelated1_Pmt) %>.value = value; }
    
    function getJobRelated1_NotUsedInRatio() { return <%= AspxTools.JsGetElementById(JobRelated1_NotUsedInRatio) %>.checked ? "True" : "False"; }
    function setJobRelated1_NotUsedInRatio(value) { if (value == null) return; <%= AspxTools.JsGetElementById(JobRelated1_NotUsedInRatio) %>.checked = value; }
    
    function getJobRelated2_ComNm() { return <%= AspxTools.JsGetElementById(JobRelated2_ComNm) %>.value; }
    function setJobRelated2_ComNm(value) { if (value == null) return; <%= AspxTools.JsGetElementById(JobRelated2_ComNm) %>.value = value; }
    
    function getJobRelated2_Pmt() { return <%= AspxTools.JsGetElementById(JobRelated2_Pmt) %>.value; }
    function setJobRelated2_Pmt(value) { if (value == null) return; <%= AspxTools.JsGetElementById(JobRelated2_Pmt) %>.value = value; }

    function getJobRelated2_NotUsedInRatio() { return <%= AspxTools.JsGetElementById(JobRelated2_NotUsedInRatio) %>.checked ? "True" : "False"; }
    function setJobRelated2_NotUsedInRatio(value) { if (value == null) return; <%= AspxTools.JsGetElementById(JobRelated2_NotUsedInRatio) %>.checked = value; }
        
    function getLienToPayoffTotDebt() { return <%= AspxTools.JsGetElementById(sLienToPayoffTotDebt) %>.value; }
    function setLienToPayoffTotDebt(value) { if (value == null) return; <%= AspxTools.JsGetElementById(sLienToPayoffTotDebt) %>.value = value; }

    function saveMe() {
        var ret = false;
        var single_saveMe = retrieveFrameProperty(window, "SingleIFrame", "single_saveMe");
        if (single_saveMe) {
            var result = single_saveMe();        
            if(result == null || ret == !result.error)
            {            
                var f_refreshInfoFromDB = retrieveFrameProperty(parent, "info", "f_refreshInfoFromDB");
                if(f_refreshInfoFromDB) {
                    f_refreshInfoFromDB();//update loan summary
                }
                else {
                    callFrameMethod(parent, "info", "f_refreshInfo");//update lead summary
                }
            }
        }
        else
        {
            clearDirty();
        }
        return ret;
    }
    
    function updateDirtyBit_callback() { parent_disableSaveBtn(false); }
  
    function updateTotal(aLiaBalTot, aLiaMonTot, aLiaPdOffTot, sLiaBalLTot, sLiaMonLTot, sRefPdOffAmt) {
      <%= AspxTools.JsGetElementById(aLiaBalTot) %>.innerText = aLiaBalTot;
      <%= AspxTools.JsGetElementById(aLiaMonTot) %>.innerText = aLiaMonTot;
      <%= AspxTools.JsGetElementById(aLiaPdOffTot) %>.innerText = aLiaPdOffTot;
      <%= AspxTools.JsGetElementById(sLiaBalLTot) %>.innerText = sLiaBalLTot;
      <%= AspxTools.JsGetElementById(sLiaMonLTot) %>.innerText = sLiaMonLTot;
      <%= AspxTools.JsGetElementById(sRefPdOffAmt) %>.innerText = sRefPdOffAmt;
    }
    
    var g_currentMode = 0;
    
    function f_switchView(mode) {
      if (mode == g_currentMode)
        return;

      var cancelHandler = function(){
        document.getElementById("rbLiabilityDetails").checked = g_currentMode == 0;
        document.getElementById("rbDebtConsolidate").checked = g_currentMode == 1;
      };
      
      PolyShouldShowConfirmSave(isDirty(), function(){
        g_currentMode = mode;
        var listFrame = document.getElementById("ListIFrame");
        var singleFrame = document.getElementById("SingleIFrame");
        
        parent_resetValues();          
        if (mode == 0) {
          document.getElementById('NavigationalPanel').style.display = '';
          listFrame.src = <%=AspxTools.SafeUrl(Tools.VRoot + "/newlos/BorrowerLiabilityList.aspx?loanid=" + LoanID + "&appid=" + ApplicationID + "&islead=")%> + "<%=AspxTools.HtmlStringFromQueryString("islead")%>";
          singleFrame.src = <%=AspxTools.SafeUrl(Tools.VRoot + "/newlos/LiabilityRecord.aspx?loanid=" + LoanID + "&appid=" + ApplicationID + "&islead=")%> + "<%=AspxTools.HtmlStringFromQueryString("islead")%>";

        } else if (mode == 1) {
          document.getElementById('NavigationalPanel').style.display = 'none';
          listFrame.src = <%=AspxTools.SafeUrl(Tools.VRoot + "/newlos/DebtConsolidateList.aspx?loanid=" + LoanID + "&appid=" + ApplicationID + "&islead=")%> + "<%=AspxTools.HtmlStringFromQueryString("islead")%>";
          singleFrame.src = <%=AspxTools.SafeUrl(Tools.VRoot + "/newlos/DebtConsolidateSummary.aspx?loanid=" + LoanID + "&appid=" + ApplicationID + "&islead=")%> + "<%=AspxTools.HtmlStringFromQueryString("islead")%>";        
        }
        f_onresize();
      }, saveMe, clearDirty, cancelHandler);
    }

    function f_onresize() {

      var screenHeight = document.body.clientHeight;
      var specialAreaHeight = 230;
      var singleIFrameHeight = 0;
      var minimumListIFrameHeight = 0;
      
      if (g_currentMode == 0) {
        singleIFrameHeight = 350;
        minimumListIFrameHeight = 100;
      } else if (g_currentMode == 1) {
        singleIFrameHeight = 180;
        minimumListIFrameHeight = 160;
      }
      
      var listIFrameHeight = screenHeight - singleIFrameHeight - specialAreaHeight;
      if (listIFrameHeight < minimumListIFrameHeight) 
        listIFrameHeight = minimumListIFrameHeight;
      
      document.getElementById("SingleIFrame").height = singleIFrameHeight;
      document.getElementById("ListIFrame").height = listIFrameHeight;
    }
    
    function updateVOButtons(debtType) {
       var btnEditVom = document.getElementById('btnEditVom');
       var btnEditVol = document.getElementById('btnEditVol');
       
       if( debtType === "3" ) {
            btnEditVom.removeAttribute('disabled');
            btnEditVol.disabled = 'disabled';
       }
       else if( debtType === "5" || debtType === "1" ) {
            btnEditVol.removeAttribute('disabled');
            btnEditVom.disabled = 'disabled';
       }
       else {
            btnEditVom.disabled = 'disabled';
            btnEditVol.disabled = 'disabled';
       }
    }
    
    function EditVol() {
        if (isDirty()) {
            var ret = confirm('Save is required before proceeding'); 
            if (ret ) saveMe();
            else { return; }
        }

        var recordid = callFrameMethod(window, "ListIFrame", "list_getRecordID", [parent_iCurrentIndex]);
        linkMe('Verifications/VOLRecord.aspx', 'recordid=' + recordid );
    }
    
    function EditVom() {
        if (isDirty()) {
            var ret = confirm('Save is required before proceeding'); 
            if (ret ) saveMe();
            else { return; }
        }

        var recordid = callFrameMethod(window, "ListIFrame", "list_getRecordID", [parent_iCurrentIndex]);
        linkMe('Verifications/VOMRecord.aspx', 'recordid=' + recordid );
    }
  //-->
  </script>

<table id=Table1 cellSpacing=0 cellPadding=0 width="100%" border=0>
  <tr>
    <td>
      <TABLE id=Table3 cellSpacing=0 cellPadding=0 border=0>
        <TR>
          <TD vAlign=top>
            <table cellspacing=2 cellpadding=0 border=1 style="BORDER-RIGHT: 1px groove; BORDER-TOP: 1px groove; MARGIN-TOP: 5px; BORDER-LEFT: 1px groove; MARGIN-RIGHT: 5px; BORDER-BOTTOM: 1px groove; BORDER-COLLAPSE: collapse" borderColor=black>
              <TR>
                <TD>&nbsp;</TD>
                <TD class=FieldLabel width="90">Balance</TD>
                <TD class=FieldLabel width="90">Payment</TD>
                <TD class=FieldLabel width="90">Paid Off</TD></TR>
              <TR>
                <TD class=FieldLabel>Current App</TD>
                <TD id="aLiaBalTot" runat="server" align="right"></TD>
                <TD id="aLiaMonTot" runat="server" align="right"></TD>
                <TD id="aLiaPdOffTot" runat="server" align="right"></TD></TR>
              <TR>
                <TD class=FieldLabel>Loan Total</TD>
                <TD id="sLiaBalLTot" runat="server" align="right"></TD>
                <TD id="sLiaMonLTot" runat="server" align="right"></TD>
                <TD id="sRefPdOffAmt" runat="server" align="right"></TD>
              </TR>
            </table>
            <INPUT type="button" value="Order Credit..." onclick='linkMe(<%=AspxTools.SafeUrl(Tools.VRoot + "/newlos/Services/OrderCredit.aspx?islead=")%> + "<%=AspxTools.HtmlStringFromQueryString("islead")%>");' AlwaysEnable=true style="WIDTH: 120px; font-weight:bold;margin-top:5px;margin-bottom:5px;background-color:lightyellow;border:2px outset lightyellow;" size="20">&nbsp;&nbsp;<input type="button" value="View Credit" onclick="displayCreditReport();" AlwaysEnable=true style="WIDTH: 93px; font-weight:bold;margin-top:5px;margin-bottom:5px">
            <br />
            <div id="sLienToPayoffTotDebtDiv">
                <span class=FieldLabel>Debts to be paid off with:</span>
                <asp:dropdownlist ID="sLienToPayoffTotDebt" runat="server"></asp:dropdownlist>
            </div>
          </TD>
          <TD vAlign=top>
              <table id=Table2 cellSpacing=0 cellPadding=0 border=0>
                <TR>
                    <td nowrap></td>
                    <TD noWrap colSpan=3 class=FieldLabel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Exclude from ratio</TD>
                    <TD class=FieldLabel noWrap>Monthly Pmt</TD>
                    <TD class=FieldLabel noWrap>Mos left</TD>
                </TR>
                <TR>
                    <td class=FieldLabel nowrap></td>
                    <TD class=FieldLabel noWrap>Alimony</TD>
                    <td class=FieldLabel nowrap><asp:CheckBox id=Alimony_NotUsedInRatio runat="server" text="Excl." onclick="refreshCalculation();" /></td>
                    <TD noWrap><asp:textbox id=Alimony_ComNm  Width="140px" runat="server" /></TD>
                    <TD noWrap><ml:moneytextbox id=Alimony_Pmt  runat="server" CssClass="mask" preset="money" width="80px" onchange="refreshCalculation();" /></TD>
                    <TD noWrap><asp:textbox id=Alimony_RemainMons  Width="37px" runat="server" /></TD>
                </TR>
                <TR>
                    <td class=FieldLabel nowrap></td>
                    <TD class=FieldLabel noWrap>Child Support</TD>
                    <td class=FieldLabel nowrap><asp:CheckBox id="ChildSupport_NotUsedInRatio" runat="server" text="Excl." onclick="refreshCalculation();" /></td>
                    <TD noWrap><asp:textbox id="ChildSupport_ComNm"  Width="140px" runat="server" /></TD>
                    <TD noWrap><ml:moneytextbox id="ChildSupport_Pmt"  runat="server" CssClass="mask" preset="money" width="80px" onchange="refreshCalculation();" /></TD>
                    <TD noWrap><asp:textbox id="ChildSupport_RemainMons"  Width="37px" runat="server" /></TD>
                </TR>
                <TR>
                    <td class=FieldLabel nowrap></td>
                    <TD class=FieldLabel noWrap>Job Expense</TD>
                    <td class=Fieldlabel nowrap><asp:CheckBox id=JobRelated1_NotUsedInRatio runat="server" text="Excl." onclick="refreshCalculation();" /></td>
                    <TD noWrap><asp:textbox id=JobRelated1_ComNm  Width="140px" runat="server" /></TD>
                    <TD noWrap><ml:moneytextbox id=JobRelated1_Pmt  runat="server" CssClass="mask" preset="money" width="80px" onchange="refreshCalculation();" /></TD>
                    <TD noWrap></TD>
                </TR>
                <TR>
                    <td nowrap></td>
                    <TD noWrap></TD>
                    <td class=Fieldlabel nowrap><asp:CheckBox id=JobRelated2_NotUsedInRatio runat="server" text="Excl." onclick="refreshCalculation();" /></td>                
                    <TD noWrap><asp:textbox id=JobRelated2_ComNm  Width="140px" runat="server" /></TD>
                    <TD noWrap><ml:moneytextbox id=JobRelated2_Pmt  runat="server" CssClass="mask" preset="money" width="80px" onchange="refreshCalculation();" /></TD>
                    <TD noWrap></TD>
                </TR>
              </table>
          </TD>          
        </TR>
      </TABLE>
    </td>
  </tr>
  <tr>
  <td>
    <table style="width:100%;">
        <tr>
            <td class="FieldLabel" style="padding-top:3px;padding-bottom:3px">
                View: <input type="radio" name="ViewMode" id="rbLiabilityDetails" checked onclick="f_switchView(0);" NotForEdit="true"><label for="rbLiabilityDetails">Liability Details</label> <input type="radio" name="ViewMode" id="rbDebtConsolidate" onclick="f_switchView(1);"  NotForEdit="true"><label for="rbDebtConsolidate">Debt Consolidation</label>
            </td>
            <td style="text-align:right;">
                <label class="FieldLabel">Assets and Liabilities Completed: </label>
                <asp:RadioButton GroupName="aAsstLiaCompleted" ID="aAsstLiaCompletedJointly" runat="server" /><label class="FieldLabel" for="aAsstLiaCompletedJointly">Jointly</label>
                <asp:RadioButton GroupName="aAsstLiaCompleted" ID="aAsstLiaCompletedNotJointly" runat="server" /><label class="FieldLabel" for="aAsstLiaCompletedNotJointly">Not Jointly</label>
            </td>
        </tr>
    </table>
    </td>
 </tr>
  <tr>
    <td>
      <iframe id=ListIFrame name="ListIFrame" src=<%= AspxTools.SafeUrl(ListUrl) %> width="100%" height=100></iframe>
    </td>
  </tr>
  <tbody id="NavigationalPanel">
  <tr>
    <td><input onclick="runListIFrameMethod('list_go', -1);" type=button value="<< Prev" id=btnPrev NoHighlight>
    <input onclick="runListIFrameMethod('list_go', 1);" type=button value="Next >>" id=btnNext NoHighlight>
    <INPUT onclick="parent_onInsert();" type=button value=Insert id=btnInsert NoHighlight>
    <INPUT onclick="parent_onAdd();" type=button value=Add id=btnAdd NoHighlight>
    <INPUT id=btnSave onclick=saveMe(); type=button value=Save NoHighlight>
    <INPUT onclick="runListIFrameMethod('list_move', 1);" type=button value="Move Up" id=btnUp NoHighlight>
    <INPUT onclick="runListIFrameMethod('list_move', -1);" type=button value="Move Down" id=btnDown NoHighlight>
    <INPUT onclick="runListIFrameMethod('list_deleteRecord');" type=button value=Delete id=btnDelete NoHighlight>&nbsp;&nbsp;&nbsp;
    <INPUT onclick="EditVom();" type="button" value="Edit VOM" id="btnEditVom"  disabled="disabled" NoHighlight />
    <INPUT onclick="EditVol();" type="button" value="Edit VOL" id="btnEditVol" disabled="disabled"  NoHighlight /></td>
  </tr>
  </tbody>
  <tr>
    <td>
      <iframe id=SingleIFrame name="SingleIFrame" src=<%= AspxTools.SafeUrl(EditUrl) %> width="100%" height=350></iframe>
    </td>
  </tr>
</table>
