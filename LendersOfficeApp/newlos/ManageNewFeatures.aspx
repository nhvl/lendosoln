﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageNewFeatures.aspx.cs" Inherits="LendersOfficeApp.newlos.ManageNewFeatures" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Manage New Features</title>
    <style type="text/css">
    
    .notice
    {
        margin: 4px;
        width: 90%;
        background-color: Yellow;
        border: solid 1px black;
        color: Black;
        font-family: Arial, Helvetica, Sans-Serif;
        padding: 4px;
        font-size: 12px;
        font-weight: bold;
    }
	.overnightTip { border: black 3px solid; padding: 5px; display: none; position:absolute; background-color: whitesmoke; z-index: 900 }
    #dialog-confirm
    {
        text-align: center;
    }
    
    </style>
    <script type="text/javascript" src="../inc/utilities.js"></script>
</head>
<body bgcolor="gainsboro">

    <script type="text/javascript">
        var oldSaveMe = window.saveMe;
        window.saveMe = function(bRefreshScreen)
        {
            var saveResult = oldSaveMe(bRefreshScreen);

            if (saveResult)
            {
                // Refresh left tree frame to reload disclosure folders.
                window.parent.treeview.location = window.parent.treeview.location;
            }
            return saveResult;
        }

        function onSettingChange()
        {
            var msg = null;
            var val = document.getElementById("CFPBUISettings").value;

            if (val === "0") {
                msg = "We strongly recommend <strong>NOT</strong> switching back to 'Legacy' mode. Any recent changes to closing costs <strong>WILL</strong> be lost.";
            }

            var warning = document.getElementById("topWarning");
            if (msg) {
                warning.style["display"] = "";
                warning.innerHTML = msg;
            } else {
                warning.style["display"] = "none";
            }
        }
        
        function _init() 
        {
            validatesConfLoanLimitDeterminationD();
            UladMigrationControlsSetup(ML.ShowUladMigrationControls, ML.IsMigratedToLatestUladDataLayer);
        }

        function validatesConfLoanLimitDeterminationD()
        {
            var sConfLoanLimitDeterminationDLckd = <%= AspxTools.JsGetElementById(sConfLoanLimitDeterminationDLckd) %>;
            var sConfLoanLimitDeterminationD = <%= AspxTools.JsGetElementById(sConfLoanLimitDeterminationD) %>;
            var sConfLoanLimitDeterminationDMsg = document.getElementById("sConfLoanLimitDeterminationDMsg");

            sConfLoanLimitDeterminationD.readOnly = !sConfLoanLimitDeterminationDLckd.checked;
            sConfLoanLimitDeterminationD.style.backgroundColor = sConfLoanLimitDeterminationDLckd.checked ? "" : "lightgrey";

            var sConfLoanLimitDeterminationDdate = new Date(sConfLoanLimitDeterminationD.value);
            var isBefore2014 = ( sConfLoanLimitDeterminationDdate && sConfLoanLimitDeterminationDdate < new Date('01/01/2014') );
            
            sConfLoanLimitDeterminationDMsg.innerText = isBefore2014 ? 'We do not support loan limits prior to 2014. The pricing engine will use 2014 limits for this file.' : '';
        }

        function migrateToLatestUladDataLayer() {
            var args = {
                loanid: ML.sLId
            };
            gService.loanedit.callAsyncSimple('MigrateToLatestUladDataLayer', args, function (result) {
                if (!result.error) {
                    UladMigrationControlsSetup(true, true);
                    alert('Succesfully migrated to latest ULAD data layer.');
                    window.parent.treeview.location = window.parent.treeview.location;
                } else {
                    alert(result.UserMessage);
                }
            });
        }

        function UladMigrationControlsSetup(showUladMigrationControls, isMigratedToLatestUladDataLayer) {
            $('#IsMigratedToLatestUladDataLayer').prop('checked', isMigratedToLatestUladDataLayer);
            $('#MigrateToLatestUladDataLayer').toggle(!isMigratedToLatestUladDataLayer);
            $('#MigrateToLatestUladDataLayerDiv').toggle(showUladMigrationControls);
        }
    </script>

    <div style="display: none;" id="dialog-confirm" title="Proceed?">
        <p style="width: 100%">
            Do you want to proceed?
        </p>
    </div>
    <form id="form1" runat="server">
    <div>
        <table width="980px">
            <tr>
                <td class="MainRightHeader">
                    Manage New Features
                </td>
            </tr>
            <tr>
                <td>
                    <div style="display: none;" id="topWarning" class="notice">
                    </div>
                    <% if (isMigratedWithNonLegacyArchive) { %>
                    <div id="noLegacyWarning" class="notice">
                        This loan is only allowed to be in migrated statuses because it has been disclosed in a migrated mode.
                    </div>
                    <% } %>
                    <table cellspacing="2" cellpading="0" width="98%" border="0">
                        <tr>
                            <td class="FieldLabel">
                                New Closing Cost Data Set settings
                            </td>
                            <td>
                                <asp:DropDownList onchange="onSettingChange();" ID="CFPBUISettings" runat="server" />
                            </td>
                        </tr>
                          <tr>
                            <td class="FieldLabel">
                                Enable "This transaction involves a TPO" calculation?
                            </td>
                            <td>
                                <asp:CheckBox id="sGfeIsTPOTransactionIsCalculated" runat="server" /> Yes
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">
                                Exclude Tolerance Cures From Final 1003 Lender Credits?
                            </td>
                            <td>
                                <asp:CheckBox ID="sExcludeToleranceCureFromFinal1003LenderCredit" runat="server" /> Yes
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">
                               Conforming Loan Limit Determination Date
                            </td>
                            <td style="width:600px">
                                <ml:DateTextBox ID="sConfLoanLimitDeterminationD" runat="server" preset="date" Width="75" onchange="validatesConfLoanLimitDeterminationD();" />
                                &nbsp;<asp:CheckBox ID="sConfLoanLimitDeterminationDLckd"  runat="server" onclick="validatesConfLoanLimitDeterminationD(); refreshCalculation();"/> Lock
                                <br />
                                <span id="sConfLoanLimitDeterminationDMsg" style="color:red"></span>
                            </td>
                        </tr>
                        <asp:PlaceHolder id="AutoPricePh" runat="server" Visible="false">
                        <tr>
                            <td colspan="2">
                                <input type="checkbox" runat="server" id="sIsIncludeInAutoPriceEligibilityProcess" disabled="disabled" />
                                <label class="FieldLabel" for="sIsIncludeInAutoPriceEligibilityProcess">Include in Overnight Process for Detecting Price or Eligibility Changes?</label>
                                <a href="#" onclick="Modal.ShowPopup('overnightTipMsg', 'overnightTipClose', event)">explain</a>
                                    <div style="padding-top: 4px">
                                        <div>
                                            <div id="overnightTipMsg" class="overnightTip" style="width:300px"> 
										        This file will be included in the overnight process for detecting price changes when this checkbox is checked. The system will automatically check this checkbox when the loan file is locked and is any loan status between Registered and Clear to Close (up to Clear to Purchase for Correspondent files). 
										        <div style="text-align:center"> [<a href="#" id='overnightTipClose' onclick="Modal.Hide()"> Close </a>]</div>
									        </div>
								        </div>
							        </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="FieldLabel">
                                <asp:RadioButtonList ID="sIncludeInAutoPriceEligibilityProcessT" runat="server" RepeatDirection="Horizontal" onclick="refreshCalculation();" RepeatLayout="Flow" >
                                    <asp:ListItem Text="Calculate" Value="0" />
                                    <asp:ListItem Text="Force Exclude" Value="1" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Overnight Price Change Detection Info
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="FieldLabel">
                                                Locked Rate    
                                            </td>
                                            <td>
                                                <ml:PercentTextBox ID="sAutoPriceProcessResultLockedRate" Width="70" runat="server" preset="percent" ReadOnly="true"></ml:PercentTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel">
                                                Locked Price    
                                            </td>
                                            <td>
                                                <ml:PercentTextBox ID="sAutoPriceProcessResultLockedPrice" Width="70" runat="server" preset="percent" ReadOnly="true"></ml:PercentTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel">
                                                Historical Price    
                                            </td>
                                            <td>
                                                <ml:PercentTextBox ID="sAutoPriceProcessResultHistoricalPrice" Width="70" runat="server" preset="percent" ReadOnly="true"></ml:PercentTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                        <tr>
                            <td class="FieldLabel">
                               Use Initial LE Signed Date as Intent to Proceed Date
                            </td>
                            <td>
                                <asp:CheckBox runat="server" ID="sUseInitialLeSignedDateAsIntentToProceedDate" /> Yes
                            </td>
                        </tr>
                        <tr id="MigrateToLatestUladDataLayerDiv" >
                            <td class="FieldLabel">
                                Latest Version of ULAD Compatible Application Information?
                            </td>
                            <td>
                                <input type="checkbox" id="IsMigratedToLatestUladDataLayer" disabled />
                                <input type="button" id="MigrateToLatestUladDataLayer" value="Migrate to Latest ULAD Compatible Application Information" onclick="return migrateToLatestUladDataLayer()"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
