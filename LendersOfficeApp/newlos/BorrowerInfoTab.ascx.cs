using System;
using System.Collections.Generic;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using System.Linq;
using LendersOffice.Migration;

namespace LendersOfficeApp.newlos
{

	public partial  class BorrowerInfoTab : BaseLoanUserControl, IAutoLoadUserControl
	{
        protected string sSpAddr;
        protected string sSpCity;
        protected string sSpState;
        protected string sSpZip;
        protected bool aBDecOcc;
        protected bool aCDecOcc;
        protected bool isPurchase;
        protected bool use1003LinkForDeclarations;

        protected void PageInit(object sender, System.EventArgs e) 
        {
            ((BasePage)Page).RegisterCSS("BorrowerInfoTab.css");

            BorrowerMembershipId.Visible = false;
            CoborrowerMembershipId.Visible = false;

            RegularExpressionValidator1.ValidationExpression = ConstApp.EmailValidationExpression;
            RegularExpressionValidator2.ValidationExpression = ConstApp.EmailValidationExpression;

            Tools.Bind_aOccT(aOccT);
            Tools.Bind_aAddrT(aBAddrT);
            Tools.Bind_aAddrT(aCAddrT);
            Tools.Bind_aAddrT(aBPrev1AddrT);
            Tools.Bind_aAddrT(aBPrev2AddrT);
            Tools.Bind_aAddrT(aCPrev1AddrT);
            Tools.Bind_aAddrT(aCPrev2AddrT);

            Tools.Bind_aAddrMailSourceT(aBAddrMailSourceT);
            Tools.Bind_aAddrMailSourceT(aCAddrMailSourceT);

            Tools.Bind_aAddrPostSourceT(aBAddrPostSourceT);
            Tools.Bind_aAddrPostSourceT(aCAddrPostSourceT);

            Tools.Bind_aMaritalStatT(aBMaritalStatT);
            Tools.Bind_aMaritalStatT(aCMaritalStatT);
    
            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);

            aBZip.SmartZipcode(aBCity, aBState);
            aBZipMail.SmartZipcode(aBCityMail, aBStateMail);
            aCZip.SmartZipcode(aCCity, aCState);
            aCZipMail.SmartZipcode(aCCityMail, aCStateMail);
            aBPrev1Zip.SmartZipcode(aBPrev1City, aBPrev1State);
            aBPrev2Zip.SmartZipcode(aBPrev2City, aBPrev2State);

            aCPrev1Zip.SmartZipcode(aCPrev1City, aCPrev1State);
            aCPrev2Zip.SmartZipcode(aCPrev2City, aCPrev2State);

            aBZipPost.SmartZipcode(aBCityPost, aBStatePost);
            aCZipPost.SmartZipcode(aCCityPost, aCStatePost);

            Tools.Bind_booleanRadioButtonList(aBIsVeteran);
            Tools.Bind_booleanRadioButtonList(aCIsVeteran);
            Tools.Bind_booleanRadioButtonList(aBIsSurvivingSpouseOfVeteran);
            Tools.Bind_booleanRadioButtonList(aCIsSurvivingSpouseOfVeteran);
            Tools.Bind_booleanRadioButtonList(aBActiveDuty);
            Tools.Bind_booleanRadioButtonList(aCActiveDuty);
            Tools.Bind_booleanRadioButtonList(aBRetiredDischargedOrSeparated);
            Tools.Bind_booleanRadioButtonList(aCRetiredDischargedOrSeparated);
            Tools.Bind_booleanRadioButtonList(aBReserveNationalGuardNeverActivated);
            Tools.Bind_booleanRadioButtonList(aCReserveNationalGuardNeverActivated);
            Tools.Bind_aVServiceT(aBVServiceT);
            Tools.Bind_aVServiceT(aCVServiceT);
            Tools.Bind_aVEnt(aBVEnt);
            Tools.Bind_aVEnt(aCVEnt);
            Tools.Bind_booleanRadioButtonList(aIsBVAFFEx);
            Tools.Bind_booleanRadioButtonList(aIsCVAFFEx);
            
            if (this.BrokerDB.IsCreditUnion)
            {
                BorrowerMembershipId.Visible = true;
                CoborrowerMembershipId.Visible = true;
            }

            this.use1003LinkForDeclarations = this.IsOnLeadPage && !this.BrokerDB.IsEditLeadsInFullLoanEditor;

            Tools.Bind_booleanRadioButtonList(aBDomesticRelationshipTri);
            Tools.Bind_DomesticRelationshipType(aBDomesticRelationshipType);
            Tools.Bind_booleanRadioButtonList(aCDomesticRelationshipTri);
            Tools.Bind_DomesticRelationshipType(aCDomesticRelationshipType);
        }

        public void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(BorrowerInfoTab));
            dataLoan.InitLoad();
            sFileVersion = dataLoan.sFileVersion;
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            var isTargeting2019Ulad = dataLoan.sIsTargeting2019Ulad;
            ((BasePage)this.Page).RegisterJsGlobalVariables("IsTargeting2019Ulad", isTargeting2019Ulad);
            
            Tools.Bind_aTypeT(aBTypeT, isTargeting2019Ulad);
            Tools.Bind_aTypeT(aCTypeT, isTargeting2019Ulad);

            var borrCitizenshipValues = dataApp.GetApplicableCitizenshipValues(E_BorrowerModeT.Borrower);
            Tools.Bind_aProdCitizenT(aProdBCitizenT, borrCitizenshipValues);
            var coborrCitizenshipValues = dataApp.GetApplicableCitizenshipValues(E_BorrowerModeT.Coborrower);
            Tools.Bind_aProdCitizenT(aProdCCitizenT, coborrCitizenshipValues);

            CoborrowerDefinedRow.Visible = dataLoan.sBorrowerApplicationCollectionT != E_sLqbCollectionT.Legacy;

            if (dataApp == null)
            {
                throw new CBaseException(ErrorMessages.LoadLoanFailed, ErrorMessages.LoadLoanFailed);
            }
            else
            {
                // Alias / POA
                var bAliases = new List<string>(dataApp.aBAliases);
                var cAliases = new List<string>(dataApp.aCAliases);
                aBAlias.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(bAliases);
                aCAlias.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(cAliases);

                aHasCoborrowerData.Checked = dataApp.aHasCoborrowerData;
                aHasCoborrowerCalc.Checked = dataApp.aHasCoborrowerCalc;
                
                aBPowerOfAttorneyNm.Value = dataApp.aBPowerOfAttorneyNm;
                aCPowerOfAttorneyNm.Value = dataApp.aCPowerOfAttorneyNm;

                // All the rest
                aBFirstNm.Text = dataApp.aBFirstNm;
                aBPreferredNm.Text = dataApp.aBPreferredNm;
                aBPreferredNmLckd.Checked = dataApp.aBPreferredNmLckd;
                aBMidNm.Text = dataApp.aBMidNm;
                aBLastNm.Text = dataApp.aBLastNm;
                aBSuffix.Text = dataApp.aBSuffix;
                aCFirstNm.Text = dataApp.aCFirstNm;
                aCPreferredNm.Text = dataApp.aCPreferredNm;
                aCPreferredNmLckd.Checked = dataApp.aCPreferredNmLckd;
                aCMidNm.Text = dataApp.aCMidNm;
                aCLastNm.Text = dataApp.aCLastNm;
                aCSuffix.Text = dataApp.aCSuffix;
                aBSsn.Text = dataApp.aBSsn;
                aCSsn.Text = dataApp.aCSsn;
                aBDob.Text = dataApp.aBDob_rep;
                aCDob.Text = dataApp.aCDob_rep;
                aBAge.Text = dataApp.aBAge_rep;
                aBSchoolYrs.Text = dataApp.aBSchoolYrs_rep;
                aCAge.Text = dataApp.aCAge_rep;
                aCSchoolYrs.Text = dataApp.aCSchoolYrs_rep;
                aBDependNum.Text = dataApp.aBDependNum_rep;
                aCDependNum.Text = dataApp.aCDependNum_rep;
                aBDependAges.Text = dataApp.aBDependAges;
                aCDependAges.Text = dataApp.aCDependAges;
                aBHPhone.Text = dataApp.aBHPhone;
                aCHPhone.Text = dataApp.aCHPhone;
                aBCellphone.Text = dataApp.aBCellPhone;
                aCCellphone.Text = dataApp.aCCellPhone;
                aBBusPhone.Text = dataApp.aBBusPhone;
                aCBusPhone.Text = dataApp.aCBusPhone;
                aBEmail.Text = dataApp.aBEmail;
                aCEmail.Text = dataApp.aCEmail;
                aBAddr.Text = dataApp.aBAddr;
                aCAddr.Text = dataApp.aCAddr;
                aBCity.Text = dataApp.aBCity;
                aBState.Value = dataApp.aBState;
                aBZip.Text = dataApp.aBZip;
                aBAddrYrs.Text = dataApp.aBAddrYrs;
                aCAddrYrs.Text = dataApp.aCAddrYrs;
                aCZip.Text = dataApp.aCZip;
                aCState.Value = dataApp.aCState;
                aCCity.Text = dataApp.aCCity;
                aBAddrMail.Text = dataApp.aBAddrMail;
                aCAddrMail.Text = dataApp.aCAddrMail;
                aBCityMail.Text = dataApp.aBCityMail;
                aCCityMail.Text = dataApp.aCCityMail;
                aBStateMail.Value = dataApp.aBStateMail;
                aCStateMail.Value = dataApp.aCStateMail;
                aBZipMail.Text = dataApp.aBZipMail;
                aCZipMail.Text = dataApp.aCZipMail;
                aBPrev1Addr.Text = dataApp.aBPrev1Addr;
                aBPrev1AddrYrs.Text = dataApp.aBPrev1AddrYrs;
                aBPrev1State.Value = dataApp.aBPrev1State;
                aBPrev1Zip.Text = dataApp.aBPrev1Zip;
                aBPrev1City.Text = dataApp.aBPrev1City;
                aCPrev1Addr.Text = dataApp.aCPrev1Addr;
                aCPrev1City.Text = dataApp.aCPrev1City;
                aCPrev1State.Value = dataApp.aCPrev1State;
                aCPrev1Zip.Text = dataApp.aCPrev1Zip;
                aCPrev1AddrYrs.Text = dataApp.aCPrev1AddrYrs;
                aBPrev2Addr.Text = dataApp.aBPrev2Addr;
                aBPrev2AddrYrs.Text = dataApp.aBPrev2AddrYrs;
                aBPrev2State.Value = dataApp.aBPrev2State;
                aBPrev2Zip.Text = dataApp.aBPrev2Zip;
                aBPrev2City.Text = dataApp.aBPrev2City;
                aCPrev2Addr.Text = dataApp.aCPrev2Addr;
                aCPrev2State.Value = dataApp.aCPrev2State;
                aCPrev2Zip.Text = dataApp.aCPrev2Zip;
                aCPrev2City.Text = dataApp.aCPrev2City;
                aCPrev2AddrYrs.Text = dataApp.aCPrev2AddrYrs;
                aBAddrPostSourceTLckd.Checked = dataApp.aBAddrPostSourceTLckd;
                aCAddrPostSourceTLckd.Checked = dataApp.aCAddrPostSourceTLckd;
                aBAddrPost.Text = dataApp.aBAddrPost;
                aCAddrPost.Text = dataApp.aCAddrPost;
                aBCityPost.Text = dataApp.aBCityPost;
                aCCityPost.Text = dataApp.aCCityPost;
                aBStatePost.Value = dataApp.aBStatePost;
                aCStatePost.Value = dataApp.aCStatePost;
                aBZipPost.Text = dataApp.aBZipPost;
                aCZipPost.Text = dataApp.aCZipPost;

                aBFax.Text = dataApp.aBFax;
                aCFax.Text = dataApp.aCFax;
                aBCoreSystemId.Text = dataApp.aBCoreSystemId;
                aCCoreSystemId.Text = dataApp.aCCoreSystemId;
                aBTotalScoreIsFthb.Checked = dataApp.aBTotalScoreIsFthb;
                aBTotalScoreIsFthb.Enabled = !dataApp.aBTotalScoreIsFthbReadOnly;
                aCTotalScoreIsFthb.Checked = dataApp.aCTotalScoreIsFthb;
                aCTotalScoreIsFthb.Enabled = !dataApp.aCTotalScoreIsFthbReadOnly;
                aBHasHousingHist.Checked = dataApp.aBHasHousingHist;
                aCHasHousingHist.Checked = dataApp.aCHasHousingHist;

                if (dataLoan.sLPurposeT != E_sLPurposeT.Purchase)
                {
                    aBFthbAndHousingHistContainer.Visible = false;
                    aCFthbAndHousingHistContainer.Visible = false;
                }

                if (dataApp.aBTotalScoreIsFthb)
                    aBHasHousingHistContainer.Style.Add("display", "");

                if (dataApp.aCTotalScoreIsFthb)
                    aCHasHousingHistContainer.Style.Add("display", "");



                Tools.SetDropDownListValue(aBMaritalStatT, dataApp.aBMaritalStatT);
                Tools.SetDropDownListValue(aCMaritalStatT, dataApp.aCMaritalStatT);
                Tools.SetDropDownListValue(aBTypeT, dataApp.aBTypeT);
                Tools.SetDropDownListValue(aCTypeT, dataApp.aCTypeT);
                Tools.SetDropDownListValue(aBAddrT, dataApp.aBAddrT);
                Tools.SetDropDownListValue(aBPrev1AddrT, dataApp.aBPrev1AddrT);
                Tools.SetDropDownListValue(aBPrev2AddrT, dataApp.aBPrev2AddrT);
                Tools.SetDropDownListValue(aCAddrT, dataApp.aCAddrT);
                Tools.SetDropDownListValue(aCPrev1AddrT, dataApp.aCPrev1AddrT);
                Tools.SetDropDownListValue(aCPrev2AddrT, dataApp.aCPrev2AddrT);
                Tools.SetDropDownListValue(aOccT, dataApp.aOccT);
                Tools.SetDropDownListValue(aProdBCitizenT, dataApp.aProdBCitizenT);
                Tools.SetDropDownListValue(aProdCCitizenT, dataApp.aProdCCitizenT);

                Tools.SetDropDownListValue(aBAddrMailSourceT, dataApp.aBAddrMailSourceT);
                Tools.SetDropDownListValue(aCAddrMailSourceT, dataApp.aCAddrMailSourceT);
                Tools.SetDropDownListValue(aBAddrPostSourceT, dataApp.aBAddrPostSourceT);
                Tools.SetDropDownListValue(aCAddrPostSourceT, dataApp.aCAddrPostSourceT);

                Tools.SetRadioButtonListValue(aBIsVeteran, dataApp.aBIsVeteran.ToString());
                Tools.SetRadioButtonListValue(aCIsVeteran, dataApp.aCIsVeteran.ToString());
                Tools.SetRadioButtonListValue(aBIsSurvivingSpouseOfVeteran, dataApp.aBIsSurvivingSpouseOfVeteran.ToString());
                Tools.SetRadioButtonListValue(aCIsSurvivingSpouseOfVeteran, dataApp.aCIsSurvivingSpouseOfVeteran.ToString());

                var isBeyondV1_CalcVaLoanElig = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V1_CalcVaLoanElig_Opm204251);
                if (isBeyondV1_CalcVaLoanElig)
                {
                    Tools.SetDropDownListValue(aBVServiceT, dataApp.aBVServiceT);
                    Tools.SetDropDownListValue(aCVServiceT, dataApp.aCVServiceT);
                    Tools.SetDropDownListValue(aBVEnt, dataApp.aBVEnt);
                    Tools.SetDropDownListValue(aCVEnt, dataApp.aCVEnt);
                    Tools.SetRadioButtonListValue(aIsBVAFFEx, dataApp.aIsBVAFFEx.ToString());
                    Tools.SetRadioButtonListValue(aIsCVAFFEx, dataApp.aIsCVAFFEx.ToString());
                }

                // Populate information for subject property.
                sSpAddr = dataLoan.sSpAddr;
                sSpCity = dataLoan.sSpCity;
                sSpState = dataLoan.sSpState;
                sSpZip = dataLoan.sSpZip;

                isPurchase = dataLoan.sLPurposeT == E_sLPurposeT.Purchase;
                aBDecOcc = dataApp.aBDecOcc.ToUpper() == "Y" || dataApp.aBDecIsPrimaryResidenceUlad == E_TriState.Yes;
                aCDecOcc = dataApp.aCDecOcc.ToUpper() == "Y" || dataApp.aCDecIsPrimaryResidenceUlad == E_TriState.Yes;

                // 1/13/2014 gf - opm 149142
                sMultiApps.Checked = dataLoan.sMultiApps;
                aSpouseIExcl.Checked = dataApp.aSpouseIExcl;

                IsBeyondV1_CalcVaLoanElig.Value = isBeyondV1_CalcVaLoanElig.ToString();
                aIsBVAElig.Value = dataApp.aIsBVAElig.ToString();
                aIsCVAElig.Value = dataApp.aIsCVAElig.ToString();

                switch (dataApp.aBLanguagePrefType)
                {
                    case LanguagePrefType.English:
                        aBLanguagePrefTypeEng.Checked = true;
                        break;
                    case LanguagePrefType.Chinese:
                        aBLanguagePrefTypeChi.Checked = true;
                        break;
                    case LanguagePrefType.Korean:
                        aBLanguagePrefTypeKor.Checked = true;
                        break;
                    case LanguagePrefType.Spanish:
                        aBLanguagePrefTypeSpa.Checked = true;
                        break;
                    case LanguagePrefType.Tagalog:
                        aBLanguagePrefTypeTag.Checked = true;
                        break;
                    case LanguagePrefType.Vietnamese:
                        aBLanguagePrefTypeVie.Checked = true;
                        break;
                    case LanguagePrefType.Other:
                        aBLanguagePrefTypeOth.Checked = true;
                        break;
                    case LanguagePrefType.Blank:
                        aBLanguagePrefTypeBla.Checked = true;
                        break;
                    default:
                        throw new UnhandledEnumException(dataApp.aBLanguagePrefType);
                }

                switch (dataApp.aCLanguagePrefType)
                {
                    case LanguagePrefType.English:
                        aCLanguagePrefTypeEng.Checked = true;
                        break;
                    case LanguagePrefType.Chinese:
                        aCLanguagePrefTypeChi.Checked = true;
                        break;
                    case LanguagePrefType.Korean:
                        aCLanguagePrefTypeKor.Checked = true;
                        break;
                    case LanguagePrefType.Spanish:
                        aCLanguagePrefTypeSpa.Checked = true;
                        break;
                    case LanguagePrefType.Tagalog:
                        aCLanguagePrefTypeTag.Checked = true;
                        break;
                    case LanguagePrefType.Vietnamese:
                        aCLanguagePrefTypeVie.Checked = true;
                        break;
                    case LanguagePrefType.Other:
                        aCLanguagePrefTypeOth.Checked = true;
                        break;
                    case LanguagePrefType.Blank:
                        aCLanguagePrefTypeBla.Checked = true;
                        break;
                    default:
                        throw new UnhandledEnumException(dataApp.aCLanguagePrefType);
                }

                aBLanguagePrefTypeOtherDesc.Text = dataApp.aBLanguagePrefTypeOtherDesc;
                aCLanguagePrefTypeOtherDesc.Text = dataApp.aCLanguagePrefTypeOtherDesc;
                aBLanguageRefusal.Checked = dataApp.aBLanguageRefusal;
                aCLanguageRefusal.Checked = dataApp.aCLanguageRefusal;

                if (dataApp.aBDomesticRelationshipTri == E_TriState.Yes)
                {
                    Tools.SetRadioButtonListValue(aBDomesticRelationshipTri, true.ToString());
                }
                else if (dataApp.aBDomesticRelationshipTri == E_TriState.No)
                {
                    Tools.SetRadioButtonListValue(aBDomesticRelationshipTri, false.ToString());
                }

                if (dataApp.aCDomesticRelationshipTri == E_TriState.Yes)
                {
                    Tools.SetRadioButtonListValue(aCDomesticRelationshipTri, true.ToString());
                }
                else if (dataApp.aCDomesticRelationshipTri == E_TriState.No)
                {
                    Tools.SetRadioButtonListValue(aCDomesticRelationshipTri, false.ToString());
                }

                Tools.SetDropDownListValue(aBDomesticRelationshipType, dataApp.aBDomesticRelationshipType);
                Tools.SetDropDownListValue(aCDomesticRelationshipType, dataApp.aCDomesticRelationshipType);
                aBDomesticRelationshipTypeOtherDescription.Text = dataApp.aBDomesticRelationshipTypeOtherDescription;
                aCDomesticRelationshipTypeOtherDescription.Text = dataApp.aCDomesticRelationshipTypeOtherDescription;
                aBDomesticRelationshipStateCode.Value = dataApp.aBDomesticRelationshipStateCode;
                aCDomesticRelationshipStateCode.Value = dataApp.aCDomesticRelationshipStateCode;

                Tools.SetRadioButtonListValue(aBActiveDuty, dataApp.aBActiveDuty.ToString());
                Tools.SetRadioButtonListValue(aCActiveDuty, dataApp.aCActiveDuty.ToString());
                aBActiveDutyExpirationDate.Text = dataApp.aBActiveDutyExpirationDate_rep;
                aCActiveDutyExpirationDate.Text = dataApp.aCActiveDutyExpirationDate_rep;
                Tools.SetRadioButtonListValue(aBRetiredDischargedOrSeparated, dataApp.aBRetiredDischargedOrSeparated.ToString());
                Tools.SetRadioButtonListValue(aCRetiredDischargedOrSeparated, dataApp.aCRetiredDischargedOrSeparated.ToString());
                Tools.SetRadioButtonListValue(aBReserveNationalGuardNeverActivated, dataApp.aBReserveNationalGuardNeverActivated.ToString());
                Tools.SetRadioButtonListValue(aCReserveNationalGuardNeverActivated, dataApp.aCReserveNationalGuardNeverActivated.ToString());
            }
        }

        public void SaveData() 
        {
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
        }
    }
}
