﻿namespace LendersOfficeApp.newlos
{
    using System;
    using DataAccess;
    using LendersOffice.Migration;

    public partial class RefiConstructionLoan : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(RefiConstructionLoan));
            dataLoan.InitLoad();

            ClientScript.RegisterHiddenField("sLPurposeT", ""+(int)dataLoan.sLPurposeT);

            sLotAcqYr.Text = dataLoan.sLotAcqYr;
            sLotAcquiredD.Text = dataLoan.sLotAcquiredD_rep;
            sLotOrigC.Text = dataLoan.sLotOrigC_rep;
            sLotLien.Text = dataLoan.sLotLien_rep;
            sLotVal.Text = dataLoan.sLotVal_rep;
            sPresentValOfLot.Text = dataLoan.sPresentValOfLot_rep;
            sLotImprovC.Text = dataLoan.sLotImprovC_rep;
            sLotWImprovTot.Text = dataLoan.sLotWImprovTot_rep;

            sSpAcqYr.Text = dataLoan.sSpAcqYr;
            sSpOrigC.Text = dataLoan.sSpOrigC_rep;
            sSpLien.Text = dataLoan.sSpLien_rep;
            sRefPurpose.Text = dataLoan.sRefPurpose;
            sSpImprovDesc.Text = dataLoan.sSpImprovDesc;
            Tools.SetDropDownListValue(sSpImprovTimeFrameT, dataLoan.sSpImprovTimeFrameT);
            sSpImprovC.Text = dataLoan.sSpImprovC_rep;

            sRefPurpose.Items.Add("No Cash-Out Rate/Term");
            sRefPurpose.Items.Add("Limited Cash-Out Rate/Term");
            sRefPurpose.Items.Add("Cash-Out/Home Improvement");
            sRefPurpose.Items.Add("Cash-Out/Debt Consolidation");
            sRefPurpose.Items.Add("Cash-Out/Other");

            this.RegisterJsGlobalVariables("AreConstructionLoanCalcsMigrated",
                LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges));
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Refi/Construction Loan";
            this.PageID = "RefiConstructionLoan";

            Tools.Bind_sSpImprovTimeFrameT(sSpImprovTimeFrameT);
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
