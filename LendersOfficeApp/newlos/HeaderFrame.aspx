<%@ Page language="c#" Codebehind="HeaderFrame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.HeaderFrame" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html>
  <head runat="server">
    <title>HeaderFrame</title>
  </head>
  <body class="HeaderBackground">
  <script type="text/javascript">
  function goBackPipeline() {
    var ownerWindow = parent.opener;
    if (null == ownerWindow || ownerWindow.closed) {
      // If parent window already closed then open a new one. Unfortunately that means the list of current loan
      // windows open will be lost.
      // Before launching the new pipeline window, need to ask user if they want to save current loan.
      //     Otherwise, they will lose their changes. 
        PolyShouldShowConfirmSave(parent.body.isDirty(), function(){
            var preferedWidth = 1024;
            var preferedHeight = 768;
        
            var w = preferedWidth < screen.width ? preferedWidth : screen.width - 10;
            var h = preferedHeight < screen.height ? preferedHeight : screen.height - 60;
            var left = (screen.width - w) / 2;
            var top = (screen.height - h - 50) / 2;
        
            var options = 'height=' + h + ',width=' + w + ',left=' + left + ',top=' + top + ',location=no,menubar=no,resizable=yes,status=no,toolbar=no';

            var win = window.open(<%= AspxTools.JsString(VirtualRoot) %> + '/los/main.aspx?showAlert=t', <%= AspxTools.JsString(m_serverID) %> + 'pipeline', options);      
            if (null == win) {
                alert('Unable to open pipeline window. Please disable your popup blocker and try again.');
                return;
            }
            win.focus();
            return;
        }, parent.body.f_saveMe);
    }
    else if (null != ownerWindow && ownerWindow.parent.frmCode != null) {
        /*
        If this loanapp is open new frame from /los/main.aspx then when click on pipeline,
        it should bring the parent browser to focus instead of redirect to los/main.aspx.
        */
      ownerWindow.parent.focus(); // Focus parent.

      return;
    }
}
function closeMe(bCancel) {
    if (typeof(parent.body.f_closeMe) == "function") 
    {
      return parent.body.f_closeMe(bCancel);
    }
    else 
    {
      
      var w = parent.opener;
      if (null != w && !w.closed) {
        w.focus();
      }
    
      window.parent.close();
    }
  return true;
}
function f_chat() 
{
	var sUserName = <%= AspxTools.JsString(m_userName) %>;
    var sBrokerName = <%= AspxTools.JsString(m_brokerName) %>;
    var sUserMail = <%= AspxTools.JsString(m_userMail) %>;

    f_chatSupport(sUserName, sBrokerName, sUserMail);
}
// OPM 4134 Ethan - time() & browser() called by f_chat(), needed by RecordsLookupService.aspx.cs
function browser()
{
	var version = "";

	if (navigator.appName != "Microsoft Internet Explorer")
	{
		version = navigator.userAgent();
		return version;
	}
	else
	{
		if (navigator.appVersion.indexOf("MSIE") != -1)
		{
			temp = navigator.appVersion.split("MSIE");
			version = parseFloat(temp[1])
			return version;
		}
	}
}
function f_disableCloseLink(bDisabled) {
    setDisabledAttr($j('#CloseLink'), bDisabled);
    setDisabledAttr($j('#MainLink'), bDisabled);
}
</script>
    <form id="HeaderFrame" method="post" runat="server">
        <table  cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr style="background-color: rgb(0,0,0)">
                <td valign="bottom"> 
                    <span class="white" style="font-weight:bold;"> 
                            <font size="4"><img height="20" src=<%=AspxTools.SafeUrl(VirtualRoot + "/images/spacer.gif")%> width="5" align="absMiddle"/>
                                LendingQB
                            </font>
                    </span> 
                </td>
                <td class="white">
                    <ml:EncodedLiteral id=m_welcomeLabel runat="server"></ml:EncodedLiteral>
                </td>
                <td align="right"> 
                    <asp:PlaceHolder runat="server" id="m_HomeLink" >
                        <img height="12" src=<%=AspxTools.SafeUrl(VirtualRoot + "/images/homebox.gif")%> width="12" align="absMiddle"/>&nbsp;
                        <a href="javascript:void(0)" onclick="if (!hasDisabledAttr(this)) return goBackPipeline();" class="PortletLink" Title="Back to main window" id="MainLink">Main Window</a>&nbsp;&nbsp;
                    </asp:PlaceHolder>
                    <img src=<%=AspxTools.SafeUrl(VirtualRoot + "/images/chat.gif")%> align="absmiddle"/>&nbsp;
                    <a runat="server" id="m_NewSupportLink" onclick="f_chat();" class="PortletLink" title="Instant Support">Instant Support</a>&nbsp;&nbsp;
                    <img height="12" src=<%=AspxTools.SafeUrl(VirtualRoot + "/images/xbox.gif")%> width="12" align="absMiddle"/>&nbsp;
                    <a href="javascript:void(0)" onclick="if (!hasDisabledAttr(this)) return closeMe();" class="PortletLink" title="Close" id="CloseLink">Close</a>
        &nbsp;&nbsp;</td>
            </tr>
         </table>

    <asp:PlaceHolder runat="server" ID="KayakoSupportMethod">
    <script type="text/javascript">
        function f_chatSupport(sUserName, sBrokerName, sUserMail) {
            var result =  gService.lookupRecord.call("SupportStatus");	
            if (result.value.error) {
                alert(result.value.UserMessage);
            }
            else if (result.value.redirect) {
                window.open(result.value.redirect,'_blank');
            }
            
            return false;
        }
    </script>
    </asp:PlaceHolder>
    </form>
  </body>
</html>
