using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;
using LendersOfficeApp.common;

namespace LendersOfficeApp.newlos
{
	public partial  class BorrowerHousingExpense : BaseLoanUserControl, IAutoLoadUserControl
	{

		protected void PageInit(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            Tools.Bind_aOccT(aOccT);
		}
        public void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(BorrowerHousingExpense));
            dataLoan.InitLoad();
            sFileVersion = dataLoan.sFileVersion;
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            aPres1stM.Text				= dataApp.aPres1stM_rep;
            aPresRent.Text				= dataApp.aPresRent_rep;
            aPresOFin.Text				= dataApp.aPresOFin_rep;
            aPresHazIns.Text			= dataApp.aPresHazIns_rep;
            aPresRealETx.Text			= dataApp.aPresRealETx_rep;
            aPresMIns.Text				= dataApp.aPresMIns_rep;
            aPresHoAssocDues.Text		= dataApp.aPresHoAssocDues_rep;
            aPresOHExp.Text				= dataApp.aPresOHExp_rep;
            aPresTotHExp.Text			= dataApp.aPresTotHExp_rep;
            aPresOHExpDesc.Text			= dataApp.aPresOHExpDesc;
            Tools.SetDropDownListValue(aOccT, dataApp.aOccT);

            sPresLTotPersistentHExp.Text = dataLoan.sPresLTotPersistentHExp_rep;


        }

        public void SaveData() 
        {

        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
