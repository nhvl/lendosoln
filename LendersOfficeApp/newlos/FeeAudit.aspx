﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FeeAudit.aspx.cs" Inherits="LendersOfficeApp.newlos.FeeAudit" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common.SerializationTypes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Fee Audit</title>
    <style type="text/css">
      body
      {
          background-color: #DCDCDC;
      }
      #FeeComparisonTable td
      {
          padding:10px;
          vertical-align:top;
      }
      #FeeComparisonTable td > table td
      {
          padding:0px;
      }
      #FeeComparisonTable select
      {
          width: 200px;
      }
      #FeeComparisonTable thead tr
      {
          background-color:Gray;
      }
      input.short
      {
          width:50px;
      }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <p><a class="FieldLabel" style="padding-left:10px;" href="#" onclick="linkMe('ToleranceCure.aspx');">See Tolerance Cure Calculation</a></p>
    <table id="FeeComparisonTable">
        <thead>
            <tr>
                <th>FeeView</th>
                <th class="InitialDisclosureColumn">Initial Disclosed Values</th>
                <th class="LatestDisclosureColumn">Last Disclosed Values</th>
                <th class="LiveValuesColumn">Current Values</th>
            </tr>
        </thead>
        <tbody>
            <tr id="ArchiveTypeAndDatePickerRow" class="GridAlternatingItem"> 
                <td></td>
                <td class="InitialDisclosureColumn">
                    <table >
                        <tr>
                            <td>
                                <span class="FieldLabel">Disclosure Type</span>
                            </td>
                            <td>
                                <select class="ArchiveTypePicker" data-associated-drop-down-id="initialDisclosureArchiveDates" doesntDirty>
                                    <option value=<%=AspxTools.HtmlAttribute(ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate.ToString("g")) %> selected>Loan Estimate</option>
                                    <option value=<%=AspxTools.HtmlAttribute(ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure.ToString("g")) %>>Closing Disclosure</option>
                                </select> 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="FieldLabel">Archive</span>
                            </td>
                            <td>
                                <select class="ArchiveDateDropdown" id="initialDisclosureArchiveDates" doesntDirty>
                                </select>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="LatestDisclosureColumn">
                    <table >
                        <tr>
                            <td>
                                <span class="FieldLabel">Disclosure Type</span>
                            </td>
                            <td>
                                <select class="ArchiveTypePicker" data-associated-drop-down-id="lastDisclosureArchiveDates" doesntDirty>
                                    <option value=<%=AspxTools.HtmlAttribute(ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate.ToString("g")) %> selected>Loan Estimate</option>
                                    <option value=<%=AspxTools.HtmlAttribute(ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure.ToString("g")) %>>Closing Disclosure</option>
                                </select> 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="FieldLabel">Archive</span>
                            </td>
                            <td>
                                <select class="ArchiveDateDropdown" id="lastDisclosureArchiveDates" doesntDirty>
                                </select>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="LiveValuesColumn">
                    <table>
                        <tr>
                            <td>
                                <span class="FieldLabel">Disclosure Type</span>
                            </td>
                            <td>
                                <select id="LiveValuesTypePicker" doesntDirty>
                                    <option value=<%=AspxTools.HtmlAttribute(ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate.ToString("g")) %> >Loan Estimate</option>
                                    <option value=<%=AspxTools.HtmlAttribute(ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure.ToString("g")) %> selected>Closing Disclosure</option>
                                </select> 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="LoadingDiv">
     Loading<IMG src="../images/status.gif" >
    </div>
    </div>
    </form>
    <script type="text/javascript">
        $(function() {
            var $feeComparisonTableBody = $('#FeeComparisonTable > tbody');
            var $loadingDiv = $('#LoadingDiv');

            $('select.ArchiveTypePicker').on('change',
                function() {
                    var selectedArchiveType = $(this).val();

                    if (false == (selectedArchiveType in model.ArchiveIDsByType)) {
                        return;
                    }

                    var $associatedArchiveDatesDropDown = $('#' + $(this).data('associatedDropDownId'));
                    if ($associatedArchiveDatesDropDown.length === 0) {
                        return;
                    }

                    $associatedArchiveDatesDropDown.children().remove();

                    var archiveIDsByType = model.ArchiveIDsByType[selectedArchiveType];
                    for (var i = 0; i < archiveIDsByType.length; i++) {
                        var archiveID = archiveIDsByType[i];
                        $associatedArchiveDatesDropDown.append('<option value="' + archiveID + '">' + model.ArchiveDateByID[archiveID] + '</option>');
                    }

                    var defaultArchiveID = null;
                    var $containingCell = $(this).parents().closest('td').parents().closest('td');
                    if ($containingCell.hasClass('InitialDisclosureColumn')) {
                        defaultArchiveID = model.InitialDisclosedArchiveID;
                    }
                    else if ($containingCell.hasClass('LatestDisclosureColumn')) {
                        defaultArchiveID = model.LastDisclosedArchiveID;
                    }

                    selectDropDownOptionByValue($associatedArchiveDatesDropDown, defaultArchiveID);
                    $associatedArchiveDatesDropDown.change();
                });

            // initialize the drop down values.
            $('select.ArchiveTypePicker').each(function() { $(this).change(); });

            function selectDropDownOptionByValue($dropdown, value) {

                var $options = $dropdown.children();
                $options.prop('selected', false);

                var $optionsToSelect = $options.filter(function() { return this.value === value });
                var $optionToSelect = $optionsToSelect.length == 0 ? $options.first() : $optionsToSelect.first();
                $optionToSelect.prop('selected', true);
            }

            $('#LiveValuesTypePicker').on('change', updateTableBasedOnSelections);
            $('select.ArchiveDateDropdown').on('change', updateTableBasedOnSelections);

            // initialize the table.
            $('#LiveValuesTypePicker').change();

            function getKeyForFeeView(feeView) {
                // this is ok since typeid is fixed length, but if need to add more variable length variables need to go a different route.
                return feeView.Fee.typeid + '_' + feeView.Fee.desc;
            }

            function updateTableBasedOnSelections() {
                window.setTimeout(clearFeeRows, 0);

                window.setTimeout(function() { addFeeRows(createFeeViewComparisonsFromSelectedArchives()) }, 10);
            }

            function clearFeeRows() {
                // delete all but the first row.
                $feeComparisonTableBody.children().slice(1).remove();
                showLoading();
            }

            function showLoading() {
                $loadingDiv.toggle(true);
            }
            function hideLoading() {
                $loadingDiv.toggle(false);
            }

            function addFeeRows(feeViewComparisons) {
                var ractive = new Ractive(
                {
                    template: '#FeeComparisonTableRow',
                    data: feeViewComparisons,
                    twoway: false
                });

                $('#FeeComparisonTable').append(ractive.toHTML());
                hideLoading();
            }

            function getClosingCostFeeViewsByArchiveID(archiveID) {
                if (archiveID in model.ClosingCostSetFeeViewsByArchiveID) {
                    return model.ClosingCostSetFeeViewsByArchiveID[archiveID];
                }
                else {
                    var args =
                    {
                        loanID: ML.sLId,
                        archiveID: archiveID
                    };
                    var result = gService.loanedit.call('GetFeeViewsForArchiveByID', args);

                    if (false === result.error) {
                        var feeViews = JSON.parse(result.value.fees).Fees;
                        model.ClosingCostSetFeeViewsByArchiveID[archiveID] = feeViews;
                        return feeViews;
                    }
                    else {
                        alert("Failed to acquire feeViews.  Please reselect.");
                        return [];
                    }
                }
            }


            function createFeeViewComparisonsFromSelectedArchives() {
                var initialDisclosedArchiveID = $('#initialDisclosureArchiveDates').val();
                var initialDisclosedFeeViews = initialDisclosedArchiveID !== null ? getClosingCostFeeViewsByArchiveID(initialDisclosedArchiveID) : [];
                var lastDisclosedArchiveID = $('#lastDisclosureArchiveDates').val();
                var lastDisclosedFeeViews = lastDisclosedArchiveID !== null ? getClosingCostFeeViewsByArchiveID(lastDisclosedArchiveID) : [];
                var currentFeeViews = $('#LiveValuesTypePicker').val() === 'LoanEstimate' ? model.LiveLoanEstimateDisclosureFeeViews : model.LiveClosingDisclosureFeeViews;

                var feeViewComparisonsByFeeViewKey = {};
                var feeViewComparisons = [];

                var feeViewSets = [initialDisclosedFeeViews, lastDisclosedFeeViews, currentFeeViews];

                for (var i = feeViewSets.length - 1; i > -1; i--) {
                    addFeeViewsToComparisons(feeViewComparisonsByFeeViewKey, feeViewComparisons, feeViewSets, i);
                }

                return {
                    feeViewcomparisons: feeViewComparisons,
                    roleDescriptionForFeeView: function(feeView) {
                        var fee = feeView.Fee;
                        if (fee.bene_desc !== '') {
                            return fee.bene_desc;
                        }
                        
                        return RoleDescriptionByType[fee.bene];
                    },
                    sectionNameForFeeView: function(feeView) {
                        return model.SectionNameByIntegerValue[feeView.Section];
                    },
                    responsiblePartyNameForFeeView: function(feeView) {
                        var responsible = feeView.Fee.responsible + ''
                        switch (responsible) {
                            case ML.EnumsByName.E_GfeResponsiblePartyT.LeaveBlank:
                                return "Borrower";
                            default:
                                return ML.EnumsByValue.E_GfeResponsiblePartyT[responsible];
                        }
                    },
                    paidByPartyNameForPayment: function(payment) {
                        var paidby = payment.paid_by + '';
                        switch (paidby) {
                            case ML.EnumsByName.E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                                return "";
                            case ML.EnumsByName.E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                                return "Borrower Financed";
                            default:
                                return ML.EnumsByValue.E_ClosingCostFeePaymentPaidByT[paidby];
                        }
                    }
                };
            }

            function addFeeViewsToComparisons(feeViewComparisonsByFeeViewKey, feeViewComparisons, feeViewSets, indOfFeeViewSetToUse) {
                var feeViewSetToUse = feeViewSets[indOfFeeViewSetToUse];

                for (var i = 0; i < feeViewSetToUse.length; i++) {
                    var feeView = feeViewSetToUse[i];
                    var feeViewKey = getKeyForFeeView(feeView);

                    if (feeViewKey in feeViewComparisonsByFeeViewKey) {
                        continue;
                    }

                    var feeViews = [];

                    for (var j = 0; j < feeViewSets.length; j++) {
                        feeViews.push(findMatchingFeeView(feeView, feeViewSets, j, indOfFeeViewSetToUse));
                    }

                    var comparison =
                    {
                        desc: feeView.Fee.desc,
                        type: feeView.Fee.org_desc,
                        feeViews: feeViews,
                        ToleranceBucket: model.ToleranceBucket
                    };

                    feeViewComparisonsByFeeViewKey[feeViewKey] = comparison;
                    feeViewComparisons.push(comparison);
                }
            }

            // may return null if feeViews is null or undefined OR if feeView isn't in feeViews by typeid.
            function findMatchingFeeView(feeView, feeViewSets, indOfFeeViewSetToFindItIn, indOfFeeViewSetItWasIn) {
                if (!feeViewSets) {
                    return null;
                }

                if (indOfFeeViewSetItWasIn === indOfFeeViewSetToFindItIn) {
                    return feeView;
                }

                var feeViews = feeViewSets[indOfFeeViewSetToFindItIn];
                var feeViewKey = getKeyForFeeView(feeView);

                for (var i = 0; i < feeViews.length; i++) {
                    if (feeViewKey == getKeyForFeeView(feeViews[i])) {
                        return feeViews[i];
                    }
                }
                return null;
            }
        });
    </script>
    <script id="FeeComparisonTableRow" type="text/ractive">
        {{#each feeViewcomparisons:i}}
            <tr class="{{ i % 2 == 0 ? "GridItem" : "GridAlternatingItem"}}">
                {{>FeeViewDescriptionCell}}
                {{>FeeViewCells}}
            </tr>
        {{/each}}
    </script>
    <script id="FeeViewDescriptionCell" type="text/ractive">
        <td class="FeeViewDescriptionCell">
            Fee Description: {{desc}} <br/>
            Fee Type: {{type}}
        </td>
    </script>
    <script id="FeeViewCells" type="text/ractive">
        {{#each feeViews:j}}
            <td class="FeeViewCell" align="{{(typeof(aff) !== 'undefined') ? 'left' : 'center'}}">
                {{#if typeof(Section) !== 'undefined'}}
                    <table style="width:100%">
                        <tbody>
                            {{>PaymentRows}}
                            <tr>
                                <td>Paid to: </td>
                                <td>{{roleDescriptionForFeeView(.)}}</td>
                                <td>Section: </td>
                                <td>{{sectionNameForFeeView(.)}}</td>
                            </tr>
                            <tr>
                                <td colspan="3">Responsible Party: {{responsiblePartyNameForFeeView(.)}}</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <label><input type="checkbox" disabled="disabled" checked="{{Fee.tp}}" />TP</label>
                    <label><input type="checkbox" disabled="disabled" checked="{{Fee.aff}}" />AFF</label>
                    <label><input type="checkbox" disabled="disabled" checked="{{Fee.can_shop}}" />Can Shop</label>
                    <label><input type="checkbox" disabled="disabled" checked="{{Fee.did_shop}}" />Did Shop</label>
                <br />
                    <label><input type="checkbox" disabled="disabled" checked="{{Fee.is_optional}}" />Optional</label>
                    <label><input type="checkbox" disabled="disabled" checked="{{Fee.apr}}" />APR</label>
                    <label><input type="checkbox" disabled="disabled" checked="{{Fee.fha}}" />FHA</label>
                    <label><input type="checkbox" disabled="disabled" checked="{{Fee.dflp}}" />DFLP</label>
                    {{#if j == 2}}
                    <div>
                            <span style="display: inline-block; float:left; font-weight: bold;">{{ToleranceBucket[Fee.typeid][0]}}</span>
                            <span style="display: inline-block; float:right; font-weight: bold; 
                                {{#if ToleranceBucket[Fee.typeid][2] == 'false'}} color: {{ToleranceBucket[Fee.typeid][3] == 'true' ? 'red' : 'green'}};{{/if}}">
                                    {{#if ToleranceBucket[Fee.typeid][2] == 'false'}}                   
                                        {{ToleranceBucket[Fee.typeid][3] == 'true' ? '+' : '-'}}
                                    {{/if}}
                                    {{ToleranceBucket[Fee.typeid][1]}}
                            </span>
                    </div>
                    {{/if}}
                {{else}}
                    <span>Fee does not appear in this archive.</span>
                {{/if}}
            </td>
        {{/each}}
    </script>
    <script id="PaymentRows" type="text/ractive">
        {{#each Fee.pmts}}
            <tr>
                <td>Amount: </td>
                <td><input type="text" value="{{amt}}" readonly="readonly" class="short"/></td>
                <td style="text-align:right">Paid by: </td>
                <td>{{paidByPartyNameForPayment(.)}}</td>
            </tr>
        {{/each}}
    </script>
</body>
</html>
