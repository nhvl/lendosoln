﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Common.SerializationTypes;
using System.Runtime.Serialization;

namespace LendersOfficeApp.newlos
{
    public partial class FeeAuditServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override void Process(string method)
        {
            switch (method)
            {
                case "GetFeeViewsForArchiveByID":
                    GetFeeViewsForArchiveByID();
                    break;
                default:
                    throw new ArgumentException("unknown method " + method);
            }
        }

        protected void GetFeeViewsForArchiveByID()
        {
            var archiveID = GetGuid("archiveID");
            var loanID = GetGuid("loanID");

            var fees = new FeesView();
            fees.Fees = Tools.GetFeeViewsForClosingCostArchive(loanID, archiveID).ToArray();

            SetResult("fees", SerializationHelper.JsonNetSerialize(fees));
            
        }

        public class FeesView
        {
            public ClosingCostFeeWithSection[] Fees { get; set; }
        }

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            throw new NotImplementedException();
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            throw new NotImplementedException();
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            throw new NotImplementedException();
        }
    }

    public partial class FeeAuditService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new FeeAuditServiceItem());
        }
    }
}
