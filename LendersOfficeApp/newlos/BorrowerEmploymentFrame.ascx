<%@ Register TagPrefix="UC" TagName="CFM" Src="../newlos/Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BorrowerEmploymentFrame.ascx.cs" Inherits="LendersOfficeApp.newlos.BorrowerEmploymentFrame" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<style>
    .SelfEmployedDataContainer {
        white-space: nowrap;
    }

        .SelfEmployedDataContainer div:not(:last-child) {
            padding-bottom: 3px;
        }

        .SelfEmployedDataContainer div + div {
            padding-top: 3px;
        }

    .self-ownership-share {
        margin-left: 10px;
    }
</style>
<script language=javascript>
<!--
var oRolodex = null;  

function _init() {
  if ( <%= AspxTools.JsBool(IsReadOnly) %>) {
    document.getElementById("btnPrev").disabled = true;
    document.getElementById("btnNext").disabled = true;
    document.getElementById("btnInsert").disabled = true;
    document.getElementById("btnAdd").disabled = true;
    document.getElementById("btnSave").disabled = true;
    document.getElementById("btnUp").disabled = true;    
    document.getElementById("btnDown").disabled = true;
    document.getElementById("btnDelete").disabled = true;    
    document.getElementById("btnSwap").disabled = true;
  }
  parent_onEmplrBusPhoneLock(document.getElementById("<%= AspxTools.ClientId(aEmplrBusPhoneLckd) %>"));
  
  if (null == oRolodex)
        oRolodex = new cRolodex(); 

    var $selfEmployedCheckbox = $('.SelfEmployedCheckboxContainer input');
    $selfEmployedCheckbox.change(determineShowSelfEmploymentSection);
    determineShowSelfEmploymentSection();
}

    function determineShowSelfEmploymentSection(event) {
        var $selfOwnershipShare = $('.self-ownership-share');
        var isSelfEmployed = $('.SelfEmployedCheckboxContainer input').is(':checked');

        $selfOwnershipShare.prop('disabled', !isSelfEmployed);
        if (!isSelfEmployed) {
            $selfOwnershipShare.val('0');
            $('.is-special-borrower-employer-relationship').prop('checked', false);
        }
    }

function IsDecimalNumberOnly(v) {
	var data = v.value.replace(/^\s+|\s+$/g,''); 
	v.value = data; 
	if (( !/^\d*(\.\d*)?$/.test(data) || data == '.' ) && data != ''  ) {
		v.value = ''; 
		document.getElementById( v.id + '_Error' ).style.display = 'inline';
	}
	else {
		document.getElementById(v.id + '_Error' ).style.display = 'none';
	}
}

function numberEntered(e) {
	if ( e.value.length > 0 ) 
	{		var  x = parseInt(e.value.charAt(e.value.length -1)) ;
			if ( e.value.charAt(e.value.length -1 ) != '.' &&  isNaN(x)  )
			{
				e.value = e.value.substring( 0, e.value.length-1 ); 
				document.getElementById( e.id + '_Error' ).style.display = 'inline';
			}
			else 
			{
				document.getElementById( e.id + '_Error' ).style.display = 'none';
			}
			
	} 
}
function saveMe() {
    var ret = false;
    var single_saveMe = retrieveFrameProperty(window, "SingleIFrame", "single_saveMe");
    if (single_saveMe) {
        var result = single_saveMe();        
        if(ret = !result.error)
        {            
            callFrameMethod(parent, "info", "f_refreshInfo");
        }
        else
        {
            clearDirty();
        }
      }
      return ret;
}
function parent_getSpecialValues() {
  return getAllFormValues("<%= AspxTools.ClientId(this) %>");
}
function parent_updatePrimaryRecord(obj) {
  document.getElementById("<%= AspxTools.JsStringUnquoted(ClientID) %>_IsSwap").value = ""; // clear IsSwap
  var prefix = "<%= AspxTools.ClientId(this) %>:";

  var coll = document.forms[0];
  for (property in obj) {
    var field = coll[prefix + property];
    if (field != null) {
      if (field.tagName == null) { // A radio button list.
        for (index = 0; index < field.length; index++)
          field[index].checked = field[index].value == obj[property];
      } else if (field.type == 'checkbox') {
        field.checked = obj[property] == 'True';
      } else {
        field.value = obj[property];
      }
    }
  }
}
function updateDirtyBit_callback() { parent_disableSaveBtn(false); }
function parent_editVOE(recordid) {
  PolyShouldShowConfirmSave(isDirty(), function(){
    var appid = parent.info.f_getCurrentApplicationID();
  linkMe('Verifications/VOERecord.aspx?recordid=' + encodeURIComponent(recordid));
  }, saveMe);
}

function parent_onEmplrBusPhoneLock(cb) {
  var args = new Object();
  args["LoanId"] = <%= AspxTools.JsString(LoanID) %>;
  args["applicationid"] = <%= AspxTools.JsString(ApplicationID) %>;
  args["sFileVersion"] = <%= AspxTools.JsString(sFileVersion.ToString()) %>;
  args["aEmplrBusPhoneLckd"] = document.getElementById("<%= AspxTools.ClientId(aEmplrBusPhoneLckd) %>").checked ? "True" : "False";
    args["IsBorrower"] = <%= AspxTools.JsBool(IsBorrower)%> ? "True" : "False";

  var result = callFrameMethod(window, "SingleIFrame", "f_getPrimaryBusinessPhone", [args]);
 
  if (result && !result.error) {
    document.getElementById("<%= AspxTools.ClientId(Primary_EmplrBusPhone) %>").value = result.value["aEmplrBusPhone"];
  }
  
  var item = document.getElementById("<%= AspxTools.ClientId(Primary_EmplrBusPhone) %>");
  item.readOnly = !cb.checked;
}
    function parent_onEditVOE() {
        var recordid = callFrameMethod(window, "ListIFrame", "list_getRecordID", [parent_iCurrentIndex]);
  
        if (null != recordid) {
            parent_editVOE(recordid);
        }
    }
<%-- Swap primary employment with selected previous employment record --%>
    function parent_swap() {
    var recordid = callFrameMethod(window, "ListIFrame", "list_getRecordID", [parent_iCurrentIndex]);
  
  if (null == recordid) {
    if (confirm('Do you want to move primary record to new previous employment record?')) {
      document.getElementById("<%= AspxTools.ClientId(this) %>_IsSwap").value = "T";
      clearDirty();
      parent_onAdd();
      updateDirtyBit();
    }
  } else {
      if (confirm('Do you want to swap primary record with highlight previous employment record?')) {
          var SingleIFrame = retrieveFrame(window, "SingleIFrame");
          var iframeDoc = SingleIFrame.contentDocument ?
              SingleIFrame.contentDocument :
              (SingleIFrame.contentWindow ? SingleIFrame.contentWindow.document : SingleIFrame.document);
          if(SingleIFrame) {
              swapMe(document.getElementById("<%= AspxTools.ClientId(Primary_EmplrNm) %>"), iframeDoc.getElementById("EmplrNm"));
              swapMe(document.getElementById("<%= AspxTools.ClientId(Primary_IsSelfEmplmt) %>"), iframeDoc.getElementById("IsSelfEmplmt"));
              swapMe(document.getElementById("<%= AspxTools.ClientId(Primary_EmplrAddr) %>"), iframeDoc.getElementById("EmplrAddr"));
              swapMe(document.getElementById("<%= AspxTools.ClientId(Primary_EmplrCity) %>"), iframeDoc.getElementById("EmplrCity"));
              swapMe(document.getElementById("<%= AspxTools.ClientId(Primary_EmplrState) %>"), iframeDoc.getElementById("EmplrState"));
              swapMe(document.getElementById("<%= AspxTools.ClientId(Primary_EmplrZip) %>"), iframeDoc.getElementById("EmplrZip"));
              swapMe(document.getElementById("<%= AspxTools.ClientId(Primary_EmplrBusPhone) %>"), iframeDoc.getElementById("EmplrBusPhone"));
              swapMe(document.getElementById("<%= AspxTools.ClientId(Primary_JobTitle) %>"), iframeDoc.getElementById("JobTitle"));
              swapMe(document.getElementById("<%= AspxTools.ClientId(Primary_EmplrFax) %>"), iframeDoc.getElementById("EmplrFax"));
              swapMe(document.getElementById("<%= AspxTools.ClientId(Primary_VerifSentD) %>"), iframeDoc.getElementById("VerifSentD"));
              swapMe(document.getElementById("<%= AspxTools.ClientId(Primary_VerifReorderedD) %>"), iframeDoc.getElementById("VerifReorderedD"));
              swapMe(document.getElementById("<%= AspxTools.ClientId(Primary_VerifRecvD) %>"), iframeDoc.getElementById("VerifRecvD"));
              swapMe(document.getElementById("<%= AspxTools.ClientId(Primary_VerifExpD) %>"), iframeDoc.getElementById("VerifExpD"));
              swapMe(document.getElementById("<%= AspxTools.ClientId(Primary_EmployeeIdVoe) %>"), iframeDoc.getElementById("EmployeeIdVoe"));
              swapMe(document.getElementById("<%= AspxTools.ClientId(Primary_EmployerCodeVoe) %>"), iframeDoc.getElementById("EmployerCodeVoe"));
              document.getElementById("<%= AspxTools.ClientId(Primary_EmplmtLen) %>").value = '';
              document.getElementById("<%= AspxTools.ClientId(Primary_ProfLen) %>").value = '';
              document.getElementById("<%=AspxTools.HtmlString(Primary_EmpltStartD.ClientID) %>").value = '';
              document.getElementById("<%=AspxTools.HtmlString(Primary_ProfStartD.ClientID) %>").value = '';
              iframeDoc.getElementById("EmplmtStartD").value = '';
              iframeDoc.getElementById("EmplmtEndD").value = '';
              iframeDoc.getElementById("MonI").value = '';
              iframeDoc.getElementById("IsCurrent").checked = false;
              iframeDoc.getElementById("EmplmtEndD").readOnly = iframeDoc.getElementById("IsCurrent").checked;
              updateDirtyBit();
          }
      }
  }
}

function swapMe(obj0, obj1) {
  if (null != obj0 && null != obj1) {
    if (obj0.type == 'checkbox') {
      <%-- Assume two controls are checkbox. --%>
      var tmp = obj1.checked;
      obj1.checked = obj0.checked;
      obj0.checked = tmp;
    } else {
      var tmp = obj1.value;
      obj1.value = obj0.value;
      obj0.value = tmp;
    }
  }
}
function _customRefreshButtonsUI() {
  document.getElementById("btnEditVOE").disabled = parent_iCurrentIndex < 1;
  document.getElementById("btnSwap").disabled = parent_iCurrentIndex < 1;  
}


  function pickAndPopulate() {
    var agentType =  <%= AspxTools.JsString(DataAccess.E_AgentRoleT.Other.ToString("D")) %>;
    oRolodex.chooseFromRolodex(agentType, <%= AspxTools.JsString(LoanID) %>, null, false, false, function (arg){
      if (args.OK) {
      
        document.getElementById('<%= AspxTools.ClientId(Primary_EmplrNm) %>').value = args.AgentCompanyName;
        document.getElementById('<%= AspxTools.ClientId(Primary_EmplrAddr) %>').value = args.AgentStreetAddr;
        document.getElementById('<%= AspxTools.ClientId(Primary_EmplrCity) %>').value = args.AgentCity;
        document.getElementById('<%= AspxTools.ClientId(Primary_EmplrState) %>').value = args.AgentState;
        document.getElementById('<%= AspxTools.ClientId(Primary_EmplrZip) %>').value = args.AgentZip;
        document.getElementById('<%= AspxTools.ClientId(Primary_EmplrFax) %>').value = args.FaxOfCompany;

        // Do not want to appear to clobber phone from Borrower Info
        var BusPhone = document.getElementById('<%= AspxTools.ClientId(Primary_EmplrBusPhone) %>');
        BusPhone.value = (BusPhone.readOnly) ? BusPhone.value : args.PhoneOfCompany;

        updateDirtyBit();
      }
    });
    
  }
  
  function f_CalcEmpltStartD() {
    var startD = document.getElementById('<%= AspxTools.HtmlString(Primary_EmpltStartD.ClientID) %>');
    var len = document.getElementById('<%= AspxTools.HtmlString(Primary_EmplmtLen.ClientID) %>');
    if(len.value)
    {
        len.value = Number(len.value).toFixed(2);
        startD.value = f_CalcStartDateFromToday(len.value);
    }
    else
    {
        startD.value = '';
    }
  }
  
  function f_CalcEmpltLen() {
    var startD = document.getElementById('<%= AspxTools.HtmlString(Primary_EmpltStartD.ClientID) %>');
    var len = document.getElementById('<%= AspxTools.HtmlString(Primary_EmplmtLen.ClientID) %>');
    if(startD.value)
    {
        setTimeout(function(){
        len.value = f_CalcLengthToToday(startD.value);
        }, 100);
    }
    else
    {
        len.value = '';
    }
  }
  
  function f_CalcProfStartD() {
    var startD = document.getElementById('<%= AspxTools.HtmlString(Primary_ProfStartD.ClientID) %>');
    var len = document.getElementById('<%= AspxTools.HtmlString(Primary_ProfLen.ClientID) %>');
    if(len.value)
    {
        len.value = Number(len.value).toFixed(2);
        startD.value = f_CalcStartDateFromToday(len.value);
    }
    else
    {
        startD.value = '';
    }
  }
  
  function f_CalcProfLen() {
    var startD = document.getElementById('<%= AspxTools.HtmlString(Primary_ProfStartD.ClientID) %>');
    var len = document.getElementById('<%= AspxTools.HtmlString(Primary_ProfLen.ClientID) %>');
    if(startD.value){
        setTimeout(function(){
            len.value = f_CalcLengthToToday(startD.value);
            }, 100);
    }
    else
    {
        len.value = '';
    }
  }
  
  function f_CalcStartDateFromToday(yearsBack) {
    try {
        var dt = new Date();
        var partialMonth = (yearsBack*12)%1;
        var monthsBack = yearsBack*12 - partialMonth;
        var MONTH_LENGTH = 30.4;
        dt.setFullYear(dt.getFullYear(), dt.getMonth() - monthsBack, dt.getDate() - (partialMonth*MONTH_LENGTH).toFixed());
        return (dt.getMonth()+1) +"/"+ dt.getDate() +"/"+ dt.getFullYear();
    }
    catch (e) {
        return '';
    }
  }
  
  function f_CalcLengthToToday(startD) {
    try {
        var start = new Date(Date.parse(startD));
        var now = new Date();
        var years = now.getFullYear() - start.getFullYear();
        var months = now.getMonth() - start.getMonth();
        var days = now.getDate() - start.getDate();
        var MONTH_LENGTH = 30.4;
        months += days/MONTH_LENGTH;
        years += months/12;
        return years.toFixed(2);
    }
    catch (e) {
        return '';
    }
  }

  function runListIFrameMethod(methodName, arg) {
    callFrameMethod(window, "ListIFrame", methodName, [arg]);
  }

//-->
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td colspan=4 class="FieldLabel">Primary Employment of 
					<ml:EncodedLiteral id="DisplayName" runat="server" ></ml:EncodedLiteral>
					<asp:checkbox CssClass="SelfEmployedCheckboxContainer" id=Primary_IsSelfEmplmt runat="server" Text="Self-employed"></asp:checkbox>&nbsp;
					<input type=button value="Edit VOE" onclick="parent_editVOE(<%= AspxTools.JsString(Primary_RecordID) %>);" >
                </td>
              </tr>
              <tr>
				<td></td>
				<td>
					<uc:CFM id="CFM" runat="server"></uc:CFM>
				</td>
			  </tr>
              <tr>
                <td class=FieldLabel> Employer</td>
                <td><asp:textbox id=Primary_EmplrNm runat="server" Width="220px" tabindex=1></asp:textbox></td>
                <td class=FieldLabel>Job Start Date</td>
                <td><ml:DateTextBox ID=Primary_EmpltStartD runat="server" tabindex=2></ml:DateTextBox></td>
                <td class=FieldLabel>Yrs on Job</td>
                <td><asp:textbox id=Primary_EmplmtLen Width="40px" runat="server" tabindex=3> </asp:textbox>
					<span id="<%= AspxTools.ClientId(Primary_EmplmtLen) %>_Error" style="color:red; display:none;"> Only numbers are allowed. </span>
				</td>
              </tr>	
              <tr>
                <td class=FieldLabel>Address</td>
                <td><asp:textbox id=Primary_EmplrAddr runat="server" Width="256px" tabindex=1></asp:textbox></td>
                <td class=FieldLabel>Profession Start Date</td>
                <td><ml:DateTextBox ID=Primary_ProfStartD runat="server" TabIndex=2></ml:DateTextBox></td>
                <TD class=FieldLabel>Yrs in Profession</TD>
                <TD><asp:textbox id=Primary_ProfLen runat="server" Width="40px" tabindex=3></asp:textbox>
					<span id="<%= AspxTools.ClientId(Primary_ProfLen) %>_Error" style="color:red; display:none;"> Only numbers are allowed. </span>
				</TD>
              
              </tr>
              <tr>
                <td class=FieldLabel>City</td>
                <td><asp:textbox id=Primary_EmplrCity runat="server" Width="173px" tabindex=1></asp:textbox><ml:statedropdownlist id=Primary_EmplrState runat="server" tabindex=1></ml:statedropdownlist><ml:zipcodetextbox id=Primary_EmplrZip CssClass="mask" preset="zipcode" width="50" runat="server" tabindex=1></ml:zipcodetextbox></td>
                <td class=FieldLabel>Employer Phone</td>
                <td class=FieldLabel><ml:phonetextbox id=Primary_EmplrBusPhone CssClass="mask" preset="phone" width="120" runat="server" tabindex=3></ml:phonetextbox></td>
                <td class=FieldLabel><asp:CheckBox id=aEmplrBusPhoneLckd runat="server" Text="Locked" onclick="parent_onEmplrBusPhoneLock(this);" tabindex=3></asp:CheckBox></td>
              </tr>
              <tr>
                <td class=FieldLabel>Position/Title</td>
                <td><asp:textbox id=Primary_JobTitle runat="server" Width="223px" tabindex=2></asp:textbox></td>
                <td class=FieldLabel>Employer Fax</td>
                <td><ml:phonetextbox id=Primary_EmplrFax CssClass="mask" preset="phone" width="120" runat="server" tabindex=3></ml:phonetextbox></td>
              </tr>
              <tr>
                  <td title="Identifies an employee on The Work Number database" class="FieldLabel">Employee ID (VOE)</td>
                  <td title="Identifies an employee on The Work Number database"><asp:TextBox ID="Primary_EmployeeIdVoe" runat="server" preset="custom" mask="###########"></asp:TextBox></td>
                  <td title="Identifies an employer on The Work Number database" class="FieldLabel">Employer Code (VOE)</td>
                  <td title="Identifies an employer on The Work Number database"><asp:TextBox ID="Primary_EmployerCodeVoe" runat="server" preset="custom" mask="##########"></asp:TextBox></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td>
            <table class=InsetBorder id=Table1 cellspacing=0 cellpadding=0 border=0>
              <tr>
                <td class=FieldLabel>Verif Sent</td>
                <td nowrap><ml:datetextbox id=Primary_VerifSentD CssClass="mask" preset="date" width="75" runat="server" tabindex=5></ml:datetextbox></td>
                <td class=FieldLabel>Re-order</td>
                <td nowrap><ml:datetextbox id=Primary_VerifReorderedD CssClass="mask" preset="date" width="75" runat="server" tabindex=5></ml:datetextbox></td>
                <td class=FieldLabel>Received</td>
                <td nowrap><ml:datetextbox id=Primary_VerifRecvD CssClass="mask" preset="date" width="75" runat="server" tabindex=5></ml:datetextbox></td>
                <td class=FieldLabel>Expected</td>
                <td nowrap><ml:datetextbox id=Primary_VerifExpD CssClass="mask" preset="date" width="75" runat="server" tabindex=5></ml:datetextbox></td></tr></table></td>
        </tr>
        <tr class="SelfEmployedDataContainer">
            <td>
                <div>
                    <div>
                        <label for="Primary_IsSpecialBorrowerEmployerRelationship" class="FieldLabel">Employed by a family member, property seller, real estate agent, or other party to the transaction?</label>
                        <input runat="server" type="checkbox" class="is-special-borrower-employer-relationship" id="Primary_IsSpecialBorrowerEmployerRelationship" />
                    </div>
                    <div>
                        <span class="FieldLabel">Ownership share of</span>
                        <asp:DropDownList runat="server" CssClass="self-ownership-share" ID="Primary_SelfOwnershipShareT"></asp:DropDownList>
                    </div>
                </div>
            </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td><iframe id=ListIFrame src="BorrowerEmploymentList.aspx?loanid=<%= AspxTools.JsStringUnquoted(LoanID.ToString()) %>&amp;appid=<%= AspxTools.JsStringUnquoted(ApplicationID.ToString()) %>&amp;isborrower=<%= AspxTools.JsBool(IsBorrower) %>" width="100%" height=80></iframe></td>
  </tr>
  <tr>
    <td><input onclick="runListIFrameMethod('list_go', -1);" type=button value="<< Prev" id=btnPrev NoHighlight>
        <input onclick="runListIFrameMethod('list_go', 1);" type=button value="Next >>" id=btnNext NoHighlight>
        <input onclick="parent_onInsert();" type=button value=Insert id=btnInsert NoHighlight>
        <input onclick="parent_onAdd();" type=button value=Add id=btnAdd NoHighlight>
        <input id=btnSave onclick=saveMe(); type=button value=Save NoHighlight>
        <input onclick="runListIFrameMethod('list_move', 1);" type=button value="Move Up" id=btnUp NoHighlight>
        <input onclick="runListIFrameMethod('list_move', -1);" type=button value="Move Down" id=btnDown NoHighlight>
        <input onclick="runListIFrameMethod('list_deleteRecord');" type=button value=Delete id=btnDelete NoHighlight>
        <input type=button value="Edit VOE" id=btnEditVOE onclick="parent_onEditVOE();" NoHighlight>
        <input type=button value="Swap" onclick="parent_swap();" NoHighlight id=btnSwap>
    </td>
  </tr>
  <tr>
    <td>
      <iframe id=SingleIFrame src=<%= AspxTools.SafeUrl("EmploymentRecord.aspx?loanid=" +LoanID + "&appid=" +ApplicationID + "&isborrower=" + AspxTools.JsBool(IsBorrower)) %> width="100%" height=420></iframe>
    </td>  
  </tr>
</table>

