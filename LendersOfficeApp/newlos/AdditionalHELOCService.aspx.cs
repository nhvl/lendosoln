﻿namespace LendersOfficeApp.newlos
{
    using System;
    using DataAccess;
    using LendersOffice.Common;

    public class AdditionalHELOCServiceItem : AbstractBackgroundServiceItem
    {
        protected override DataAccess.CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(AdditionalHELOCServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sCreditLineAmt_rep = GetString("sCreditLineAmt");
            dataLoan.sLAmtCalc_rep = GetString("sLAmtCalc");
            dataLoan.sNoteIR_rep = GetString("sNoteIR");
            dataLoan.sHcltvR_rep = GetString("sHcltvR");
            dataLoan.sTerm_rep = GetString("sTerm");
            dataLoan.sDue_rep = GetString("sDue");
            dataLoan.sRAdjIndexR_rep = GetString("sRAdjIndexR");
            dataLoan.sRAdjMarginR_rep = GetString("sRAdjMarginR");
            dataLoan.sQualIR_rep = GetString("sQualIR");
            dataLoan.sQualIRLckd = this.GetBool(nameof(dataLoan.sQualIRLckd));
            dataLoan.sLAmtLckd = GetBool("sLAmtLckd");

            dataLoan.sHelocDraw_rep = GetString("sHelocDraw");
            dataLoan.sHelocQualPmtBaseT = (E_sHelocPmtBaseT)GetInt("sHelocQualPmtBaseT");
            dataLoan.sHelocQualPmtFormulaT = (E_sHelocPmtFormulaT)GetInt("sHelocQualPmtFormulaT");
            dataLoan.sHelocQualPmtFormulaRateT = (E_sHelocPmtFormulaRateT)GetInt("sHelocQualPmtFormulaRateT");
            dataLoan.sHelocQualPmtAmortTermT = (E_sHelocPmtAmortTermT)GetInt("sHelocQualPmtAmortTermT");
            dataLoan.sHelocQualPmtPcBase_rep = GetString("sHelocQualPmtPcBase");
            dataLoan.sHelocQualPmtMb_rep = GetString("sHelocQualPmtMb");

            dataLoan.sHelocPmtBaseT = (E_sHelocPmtBaseT) GetInt("sHelocPmtBaseT");
            dataLoan.sHelocPmtFormulaT = (E_sHelocPmtFormulaT)GetInt("sHelocPmtFormulaT");
            dataLoan.sHelocPmtFormulaRateT = (E_sHelocPmtFormulaRateT)GetInt("sHelocPmtFormulaRateT");
            dataLoan.sHelocPmtAmortTermT = (E_sHelocPmtAmortTermT)GetInt("sHelocPmtAmortTermT");
            dataLoan.sHelocPmtPcBase_rep = GetString("sHelocPmtPcBase");
            dataLoan.sHelocPmtMb_rep = GetString("sHelocPmtMb");

            dataLoan.sHelocAnnualFee_rep = GetString("sHelocAnnualFee");
            dataLoan.sHelocDrawFee_rep = GetString("sHelocDrawFee");
            dataLoan.sHelocMinimumAdvanceAmt_rep = GetString("sHelocMinimumAdvanceAmt");
            dataLoan.sHelocReturnedCheckFee_rep = GetString("sHelocReturnedCheckFee");
            dataLoan.sHelocStopPaymentFee_rep = GetString("sHelocStopPaymentFee");
            dataLoan.sHelocCalculatePrepaidInterest = GetBool("sHelocCalculatePrepaidInterest");

            dataLoan.sHelocTerminationFeePeriod_rep = GetString("sHelocTerminationFeePeriod");
            dataLoan.sHelocTerminationFee_rep = GetString("sHelocTerminationFee");
            dataLoan.sHelocMinimumPayment_rep = GetString("sHelocMinimumPayment");
            dataLoan.sHelocMinimumPaymentPc_rep = GetString("sHelocMinimumPaymentPc");

            dataLoan.sDaysInYr_rep = GetString("sDaysInYr");

            dataLoan.sRAdj1stCapMon_rep = GetString("sRAdj1stCapMon");
            dataLoan.sRLifeCapR_rep = GetString("sRLifeCapR");


            dataLoan.sInitialRateT = (E_sInitialRateT)GetInt("sInitialRateT");
            dataLoan.sFirstLienTrusteeNm = GetString("sFirstLienTrusteeNm");
            dataLoan.sFirstLienRecordingD_rep = GetString("sFirstLienRecordingD");
            dataLoan.sFirstLienHolderNm = GetString("sFirstLienHolderNm") ;
            dataLoan.sFirstLienRecordingBookNum = GetString("sFirstLienRecordingBookNum");
            dataLoan.sFirstLienRecordingPageNum = GetString("sFirstLienRecordingPageNum");
            dataLoan.sFirstLienRecordingInstrumentNum = GetString("sFirstLienRecordingInstrumentNum");

            AdditionalHELOC.BindDataFromControls(dataLoan, dataLoan.GetAppData(0), this);
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sCreditLineAmt", dataLoan.sCreditLineAmt_rep);
            SetResult("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sQualTopR",  dataLoan.sQualTopR_rep);
            SetResult("sQualBottomR", dataLoan.sQualBottomR_rep);
            SetResult("sLtvR", dataLoan.sLtvR_rep);
            SetResult("sCltvR", dataLoan.sCltvR_rep);
            SetResult("sHcltvR", dataLoan.sHcltvR_rep);
            SetResult("sTerm", dataLoan.sTerm_rep);
            SetResult("sDue", dataLoan.sDue_rep);
            SetResult("sRAdjIndexR", dataLoan.sRAdjIndexR_rep);
            SetResult("sRAdjMarginR", dataLoan.sRAdjMarginR_rep);
            SetResult("sQualIR", dataLoan.sQualIR_rep);
            this.SetResult(nameof(dataLoan.sQualIRLckd), dataLoan.sQualIRLckd);
            SetResult("sLAmtLckd", dataLoan.sLAmtLckd);

            SetResult("sHelocDraw", dataLoan.sHelocDraw_rep);
            SetResult("sHelocRepay", dataLoan.sHelocRepay_rep);

            SetResult("sHelocQualPmtBaseT", dataLoan.sHelocQualPmtBaseT);
            SetResult("sHelocQualPmtBaseAmt", dataLoan.sHelocQualPmtBaseAmt_rep);
            SetResult("sHelocQualPmtFormulaT", dataLoan.sHelocQualPmtFormulaT);
            SetResult("sHelocQualPmtFormulaRateT", dataLoan.sHelocQualPmtFormulaRateT);
            SetResult("sHelocQualPmtAmortTermT", dataLoan.sHelocQualPmtAmortTermT);
            SetResult("sHelocQualPmtAmortTerm", dataLoan.sHelocQualPmtAmortTerm_rep);
            SetResult("sHelocQualPmtPcBase", dataLoan.sHelocQualPmtPcBase_rep);
            SetResult("sHelocQualPmtMb", dataLoan.sHelocQualPmtMb_rep);

            SetResult("sHelocPmtBaseT", dataLoan.sHelocPmtBaseT);
            SetResult("sHelocPmtBaseAmt", dataLoan.sHelocPmtBaseAmt_rep);
            SetResult("sHelocPmtFormulaT", dataLoan.sHelocPmtFormulaT);
            SetResult("sHelocPmtFormulaRateT", dataLoan.sHelocPmtFormulaRateT);
            SetResult("sHelocPmtAmortTermT", dataLoan.sHelocPmtAmortTermT);
            SetResult("sHelocPmtAmortTerm", dataLoan.sHelocPmtAmortTerm_rep);
            SetResult("sHelocPmtPcBase", dataLoan.sHelocPmtPcBase_rep);
            SetResult("sHelocPmtMb", dataLoan.sHelocPmtMb_rep);

            SetResult("sProThisMQual", dataLoan.sProThisMQual_rep);
            SetResult("sProThisMPmt", dataLoan.sProThisMPmt_rep);

            SetResult("sHelocAnnualFee", dataLoan.sHelocAnnualFee_rep);
            SetResult("sHelocDrawFee", dataLoan.sHelocDrawFee_rep);
            SetResult("sHelocMinimumAdvanceAmt", dataLoan.sHelocMinimumAdvanceAmt_rep);
            SetResult("sHelocReturnedCheckFee", dataLoan.sHelocReturnedCheckFee_rep);
            SetResult("sHelocStopPaymentFee", dataLoan.sHelocStopPaymentFee_rep);
            SetResult("sHelocCalculatePrepaidInterest", dataLoan.sHelocCalculatePrepaidInterest);

            SetResult("sHelocTerminationFeePeriod", dataLoan.sHelocTerminationFeePeriod_rep);
            SetResult("sHelocTerminationFee", dataLoan.sHelocTerminationFee_rep);
            SetResult("sHelocMinimumPayment", dataLoan.sHelocMinimumPayment_rep);
            SetResult("sHelocMinimumPaymentPc", dataLoan.sHelocMinimumPaymentPc_rep);

            SetResult("sFullyIndexedR", dataLoan.sFullyIndexedR_rep);
            SetResult("sDaysInYr", dataLoan.sDaysInYr_rep);
            SetResult("sDailyPeriodicR", dataLoan.sDailyPeriodicR_rep);
            SetResult("sDailyPeriodicFullyIndexR", dataLoan.sDailyPeriodicFullyIndexR_rep);

            SetResult("sRAdj1stCapMon",  dataLoan.sRAdj1stCapMon_rep);
            SetResult("sRLifeCapR", dataLoan.sRLifeCapR_rep);

            SetResult("sInitialRateT", dataLoan.sInitialRateT);
            SetResult("sFirstLienTrusteeNm", dataLoan.sFirstLienTrusteeNm);
            SetResult("sFirstLienRecordingD", dataLoan.sFirstLienRecordingD_rep);
            SetResult("sFirstLienHolderNm", dataLoan.sFirstLienHolderNm);
            SetResult("sFirstLienRecordingBookNum", dataLoan.sFirstLienRecordingBookNum);
            SetResult("sFirstLienRecordingPageNum", dataLoan.sFirstLienRecordingPageNum);
            SetResult("sFirstLienRecordingInstrumentNum", dataLoan.sFirstLienRecordingInstrumentNum);

            AdditionalHELOC.LoadDataForControls(dataLoan, dataLoan.GetAppData(0), this);
        }
    }

    public partial class AdditionalHELOCService : BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new AdditionalHELOCServiceItem());
        }

    }
}
