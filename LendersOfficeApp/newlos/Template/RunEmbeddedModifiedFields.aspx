﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RunEmbeddedModifiedFields.aspx.cs" Inherits="LendersOfficeApp.newlos.Template.RunEmbeddedModifiedFields" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html>
<head runat="server">
    <title>PML Modified Fields</title>
</head>
<body MS_POSITIONING="FlowLayout" class="EditBackground" scroll="no">
<script language=javascript>
  <!--
    resizeTo(650, 500);
  //-->
  </script>
    <form id="RunEmbeddedModFields" method="post" runat="server">
    <iframe src=<%= AspxTools.SafeUrl(m_url) %> width="100%" height="100%"></iframe>
    <uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
    </form>
</body>
</html>
