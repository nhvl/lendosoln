<%@ Import namespace="DataAccess" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page language="c#" Codebehind="QualifiedLoanProgramSearchResultNew.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Template.QualifiedLoanProgramSearchResultNew" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
    <title>QualifiedLoanProgramSearchResult</title>
      <style type="text/css">
          .RefreshAlert
          {
              border: black 1px solid;
              padding: 5px;
              margin: 5px;
              font-weight: bold;
              padding-bottom: 5px;
              color: black;
              padding-top: 5px;
              background-color: yellow;
              width: 600px;
          }
      </style>
</head>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro">
    <script type="text/javascript">
    <!--
    
    <%-- This js method is for non lpe loan program apply --%>     
    function f_applyProgram(id) {
      var args = new Object();
      args["lLpTemplateId"] = id;
      args["sLId"] = ML.sLId;
      args["sFileVersion"] = document.getElementById("sFileVersion").value;
      var result = gService.qualify.call("ApplyProgram", args);
      
      if (!result.error) {
        returnToInfoScreen();
      } else {
        var errMsg = result.UserMessage || 'Unable to apply loan program. Please try again.';
        alert(errMsg);
        self.location = self.location; // Refresh the page.
      }
      
      
      
      return false;
    }
    function f_viewProgram(id) {
      //var body_url = ML.VirtualRoot + "/los/Template/LoanProgramView.aspx?fileid=" + id;
      var body_url = <%=AspxTools.JsNumeric(E_UrlOption.LoanProgramView) %>;
      var params = encodeURIComponent("fileid=" + id);

      window.open(ML.VirtualRoot + "/common/PrintView_Frame.aspx?body_url=" + body_url + "&menu_param=print&params=" + params, "", "toolbar=no,menubar=no,location=no,status=no,resizable=yes");
      return false;
    }
    function returnToInfoScreen() {
      <% if (m_isLead) { %>
        self.location = ML.VirtualRoot + "/los/lead/LeadEdit.aspx?loanid=" + ML.sLId + "&islead=t&history=t";
      <% } else { %>    
        var appID = parent.info.f_getCurrentApplicationID();
        <% if(IsBigLoanPage) { %>
            self.location =  ML.VirtualRoot + "/newlos/BigLoanInfo.aspx?loanid=" + ML.sLId + "&appid=" + encodeURIComponent(appID) + "&history=t";    
        <% }else { %>
            self.location =  ML.VirtualRoot + "/newlos/loaninfo.aspx?loanid=" + ML.sLId + "&appid=" + encodeURIComponent(appID) + "&history=t";    
        <% } %>
      <% } %>
    }      
    //-->
    </script>	
    <form id="QualifiedLoanProgramSearchResultNew" method="post" runat="server">
        <asp:Panel runat="server" ID="ProgramSetMessage" CssClass="RefreshAlert">
            A loan program template cannot be applied because a loan program has already been registered/locked via PML. If you want to populate TIL data from a template, please use the "Populate from loan program template" button on the TIL.
        </asp:Panel>
      <input type="hidden" name="ids" >
      <asp:Panel id=m_main runat="server"></asp:Panel>
      <asp:TreeView id="m_tree" runat="server" 
        ExpandDepth="1" 
        EnableViewState="false"
        HoverNodeStyle-BackColor="gainsboro" HoverNodeStyle-ForeColor="black"
        ShowLines="true">
        <%--
        CollapseImageUrl="/webctrl_client/1_0/images/folderopen.gif" ExpandImageUrl="/webctrl_client/1_0/images/folder.gif"
        NoExpandImageUrl="/webctrl_client/1_0/treeimages/i.gif"  --%>
      
      </asp:TreeView>

     </form>
	
  </body>
</html>
