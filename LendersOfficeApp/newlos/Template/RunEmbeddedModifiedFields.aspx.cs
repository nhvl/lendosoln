﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Template
{
    public partial class RunEmbeddedModifiedFields : LendersOffice.Common.BasePage
    {
        protected string m_url = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal; //Page.User as BrokerUserPrincipal;
            if (null == principal)
                RequestHelper.Signout(); // 10/12/2005 dd - If unable to cast to BrokerUserPrincipal redirect to log out.

            Guid loanId = new Guid(RequestHelper.GetSafeQueryString("loanid"));
            m_url = VirtualRoot + "/embeddedpml/main/ListModifiedFields.aspx?userid=" + principal.UserId + "&LoanID=" + loanId;
        }

    }
}
