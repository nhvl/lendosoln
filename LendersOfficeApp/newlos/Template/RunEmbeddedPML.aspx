<%@ Page language="c#" Codebehind="RunEmbeddedPML.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Template.RunEmbeddedPML" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
<head runat="server">
<title>RunEmbeddedPML</title>
</head>
<body MS_POSITIONING="FlowLayout" class="EditBackground" scroll=no>
<script type="text/javascript">
<!--
function f_updateSummary(sQualTopR, sQualBottomR, sCltvR, sLtvR, sHcltvR) {
    parent.info.f_update(sQualTopR, sQualBottomR, sCltvR, sLtvR, sHcltvR);
}

function f_displayLoanSummary(bVisible, reload) {
    if (typeof(parent.info.f_displayLoanSummary) == 'function')
        parent.info.f_displayLoanSummary(bVisible, reload);
}

function f_afterLPESubmitted() {
    <% if (m_isLead) { %>
        parent.treeview.load('LeadEdit.aspx');
    <% } else { %>
        parent.treeview.load('LockDesk/BrokerRateLock.aspx');
    <% } %>
}

function _init() {
    var $iframe = $j('#embeddedPMLFrame');
    var m_url = <%= AspxTools.JsString(m_url) %>;
    
    <% if (!m_isLoanTypeSupported) { %>
        alert("The current loan purpose is not supported.");
        window.history.back();
        return;
    <% } %>
    
   
    <% if (DoBreakout) { %>
        if (top.location != document.location) {
            // Workaround implemented to preserve window management
            // when switching to and from the new PML UI
            
            // We need to bypass the removal from the current loan list on unload
            // that is usually done in LoanAppCode.aspx
            // because we're not actually "unloading"
            var winOpener = parent.window.opener;
      
            if (null != winOpener && !winOpener.closed) {
                winOpener.gSkipWindowManagement = true;
            }
            
            top.location.href = m_url;
        }
    <% } else { %>
        $iframe.attr('src', m_url);
        $iframe.show();
    <% } %>
}
//-->
</script>

    <form id="RunEmbeddedPML" method="post" runat="server">
        <iframe id="embeddedPMLFrame" width="100%" height="100%"></iframe>
    </form>


  </body>
</html>
