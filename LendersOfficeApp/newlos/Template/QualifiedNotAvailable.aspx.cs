namespace LendersOfficeApp.newlos.Template
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.UI.WebControls;
    using ConfigSystem;
    using DataAccess;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Security;

    public partial class QualifiedNotAvailable : LendersOffice.Common.BasePage
	{
    
		protected void PageLoad(object sender, System.EventArgs e)
		{
            CPageData dataLoan = new CCheckLpeRunModeData(RequestHelper.LoanID);
            dataLoan.InitLoad();

            LoanValueEvaluator evaluator = new LoanValueEvaluator(BrokerUserPrincipal.CurrentPrincipal.ConnectionInfo, BrokerUserPrincipal.CurrentPrincipal.BrokerId, RequestHelper.LoanID, WorkflowOperations.UseOldSecurityModelForPml, 
                WorkflowOperations.RunPmlToStep3, 
                WorkflowOperations.RunPmlForRegisteredLoans, 
                WorkflowOperations.RunPmlForAllLoans);
            evaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(BrokerUserPrincipal.CurrentPrincipal));

            bool useOldEngine = LendingQBExecutingEngine.CanPerform(WorkflowOperations.UseOldSecurityModelForPml, evaluator);

            List<string> reasons = new List<string>();
            if( useOldEngine ) 
            {
                Info.Text = "Cannot run PriceMyLoan on this file. Reasons";
                foreach ( E_sReasonNotAllowingResubmission s in dataLoan.sReasonForNotAllowingResubmission )
                {
                    reasons.Add( Tools.Get_sReasonNotAllowingResubmission_Description(s));
                }
            }
            else 
            {
                List<ExecutionEngineDebugMessage> debugMessages = new List<ExecutionEngineDebugMessage>();
                debugMessages.Add(LendingQBExecutingEngine.GetExecutionEngineDebugMessage(WorkflowOperations.RunPmlForAllLoans, evaluator));
                debugMessages.Add(LendingQBExecutingEngine.GetExecutionEngineDebugMessage(WorkflowOperations.RunPmlForRegisteredLoans, evaluator));
                debugMessages.Add(LendingQBExecutingEngine.GetExecutionEngineDebugMessage(WorkflowOperations.RunPmlToStep3, evaluator));

                bool failedOnRestraint = true;
                IEnumerable<string> messages = debugMessages.Where(m => m.FailedConditionType == ConditionType.Restraint).SelectMany(m => m.UserFriendlyMessages);

                if (!messages.Any())
                {
                    failedOnRestraint = false;
                    messages = debugMessages.Where(m => m.FailedConditionType == ConditionType.Privilege).SelectMany(m => m.UserFriendlyMessages);
                }

                reasons = messages.Distinct().Take(10).ToList();

                if (reasons.Count > 0)
                {
                    if (failedOnRestraint)
                    {
                        Info.Text = "Cannot run PriceMyLoan on this file, because of the following reasons:";
                    }
                    else
                    {
                        Info.Text = "Cannot run PriceMyLoan on this file. It may be run under any of the following circumstances:";
                    }
                }
            }

            StringBuilder sb = new StringBuilder();

            sb.Append("<ul>");
            foreach (string s in reasons) 
            {
                sb.AppendFormat("<li>{0}", AspxTools.HtmlString(s));
            }
            sb.Append("</ul>");
            m_mainHtml.Text = sb.ToString();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
