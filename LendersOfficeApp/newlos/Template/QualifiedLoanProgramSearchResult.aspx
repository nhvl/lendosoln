<%@ Page language="c#" Codebehind="QualifiedLoanProgramSearchResult.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Template.QualifiedLoanProgramSearchResult" %>
<%@ Import namespace="DataAccess" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<html>
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>QualifiedLoanProgramSearchResult</title>
</head>
<style type="text/css">
    .RefreshAlert {
        border: black 1px solid;
        padding: 5px;
        margin: 5px;
        font-weight: bold;
        padding-bottom: 5px;
        color: black;
        padding-top: 5px;
        background-color: yellow;
        width: 600px;
    }
    .tree-hover {
        background-color:gainsboro;
        color:black;
    }

    span.dynatree-node a.disabled, span.dynatree-node a.apply, span.dynatree-node a.view {
        border-color: transparent;
        padding: 0;
        margin: 0;
    }
    span.dynatree-node a.disabled:hover,
    span.dynatree-node a.apply:hover,
    span.dynatree-node a.view:hover,
    span.dynatree-active a
    {
        background-color: transparent;
        border-color: transparent;
    }
    span.dynatree-node a.apply, span.dynatree-node a.view {
        color:blue;
        text-decoration: underline;
        cursor: pointer;
    }
    ul.dynatree-container.dynatree-container {
        background-color: rgb(220, 220, 220);
    }
</style>
<body MS_POSITIONING="FlowLayout" bgcolor="gainsboro">
<form method="post" runat="server">
    <asp:Panel runat="server" ID="ProgramSetMessage" CssClass="RefreshAlert">
        A loan program template cannot be applied because a loan program has already been registered/locked via PML.
        If you want to populate TIL data from a template, please use the "Populate from loan program template" button on the TIL.
    </asp:Panel>
    <input type="hidden" id="ids" name="ids">
    <asp:Panel id="m_main" runat="server"></asp:Panel>
    <div id="m_tree"></div>
</form>
<script type="text/javascript">
    var body_url = <%=AspxTools.JsNumeric(E_UrlOption.LoanProgramView) %>;

    function _init() {
        if (parent.frmSearchCriteria != null) {
            parent.enabledGetAllReasonsCB(true);
            parent.selectFirstOption();

            <% if (!m_isLpeMode) { %>
                parent.f_disableAll();
            <% } %>
        }
    }

    function returnToInfoScreen() {
        <% if (m_isLead) { %>
            location.href = ML.VirtualRoot + "/los/lead/LeadEdit.aspx?loanid=" + ML.sLId + "&islead=t&history=t";
        <% } else { %>
            var appID = parent.info.f_getCurrentApplicationID();
            location.href = ML.VirtualRoot + "/newlos/loaninfo.aspx?loanid=" + ML.sLId + "&appid=" + encodeURIComponent(appID) + "&history=t";
        <% } %>
    }
</script>
<script type="text/javascript">
    var g_ids = null;
    var g_checkCount = 0;

    (function ($) {
        var guidEmpty = "00000000-0000-0000-0000-000000000000";

        var mTree = $("#m_tree");

        renderTree(state);
        mTree.on("click", "a.apply", function(event){
            var node = $.ui.dynatree.getNode(event.currentTarget);
            var lLpTemplateId = node.data.key;
            var result = gService.qualify.call("ApplyProgram", {
                lLpTemplateId: lLpTemplateId,
                sLId         : ML.sLId,
                sFileVersion : document.getElementById("sFileVersion").value
            });

            if (!result.error) {
                returnToInfoScreen();
            } else {
                alert(result.UserMessage || 'Unable to apply loan program. Please try again.');
                location.reload();
            }
            return false;
        });
        mTree.on("click", "a.view", function(event){
            var node = $.ui.dynatree.getNode(event.currentTarget);
            var lLpTemplateId = node.data.key;
            var params = encodeURIComponent("fileid=" + lLpTemplateId);

            window.open(ML.VirtualRoot + "/common/PrintView_Frame.aspx?body_url=" + body_url + "&menu_param=print&params=" + params, "", "toolbar=no,menubar=no,location=no,status=no,resizable=yes");
            return false;
        });

		function renderTree(state){
            try { mTree.dynatree("destroy"); } catch(e) { }

            var root = list2DynaTree(state);

			mTree.dynatree({
                children: [root],
                checkbox: state.isLpeMode,
                noLink: true,
                onPostInit: function () {
				    this.getRoot().visit(function(node){
						if (node.getLevel() < 2) node.expand(true);
					});
                },
                onSelect: function(selected, node) {
                    if (!node.data.isFolder) {
                        g_checkCount += (selected) ? 1 : -1;
                    } else {
                        node.visit(function(desNode){
                            desNode.select(selected);
                            g_checkCount += (selected) ? 1 : -1;
                        });
                    }
                },
            });
		}

		function list2DynaTree(state) {
			var fp2cMap = {}; // Dictionary map folder.FolderID -> to it's subfolder DynatreeNode[]
			var lp2cMap = {}; // Dictionary map folder.FolderID -> to it's Loan Program children DynatreeNode[]
            var id2Node = {}; // Dictionary map folder.FolderID -> to coresponding DynatreeNode

            var rootChildren = [].concat(
                state.folders.map(function(folder) {
                    var node = ({key:folder.FolderID, title:folder.FolderName, isFolder:true });

                    id2Node[folder.FolderID] = node;

                    if (folder.ParentFolderID == null || folder.ParentFolderID == guidEmpty) return node;

                    var children = fp2cMap[folder.ParentFolderID] = fp2cMap[folder.ParentFolderID] || [];
                    children.push(node);
                    return null;
                }).filter(filterNull).sort(folderCompare),
                state.loanPrograms.map(function(loan) {
                    var node = ({key:loan.lLpTemplateId, lLpTemplateNm:loan.lLpTemplateNm, title:renderLoanProgram(loan), isFolder:false, icon:false });

                    id2Node[loan.lLpTemplateId] = node;

                    if (loan.FolderID == null|| loan.FolderID == guidEmpty) return node;

                    var children = lp2cMap[loan.FolderID] = lp2cMap[loan.FolderID] || [];
                    children.push(node);
                    return null;
                }).filter(filterNull).sort(loanCompare)
            );

		    Object.keys(lp2cMap).forEach(function(id){
		        var p = id2Node[id];
		        if (p == null) {debugger; return;}
		        p.children = [].concat(
		            (fp2cMap[id] || []).sort(folderCompare),
		            (lp2cMap[id] || []).sort(loanCompare)
		        );
		    });

			Object.keys(fp2cMap).forEach(function(id){
				var p = id2Node[id];
				if (p == null) {debugger; return;}
				p.children = [].concat(
                    (fp2cMap[id] || []).sort(folderCompare),
                    (lp2cMap[id] || []).sort(loanCompare)
                );
            });

            return ({ key:"root", title:"Root Folder", children:rootChildren, id2Node: id2Node, unselectable:true, icon:false });
        }

        function renderLoanProgram(loan){
            return "<span>" + loan.lLpTemplateNm + (state.isLpeMode ? "" : (
                (state.IsLoanProgramRegistered ? ' (<a class="disabled">apply</a>)' : ' (<a class="apply" title="Apply">apply</a>)') +
                    ' (<a class="view" title="View">view</a>)'
            )) + "</span>";
        }

        function filterNull(x) { return x != null }

        function folderCompare(a, b) { return stringCompare(a.FolderName, b.FolderName) }
        function loanCompare(a, b) { return stringCompare(a.lLpTemplateNm, b.lLpTemplateNm) }
        function stringCompare(a, b){
            var la = a.toLowerCase();
            var lb = b.toLowerCase();
            return (
                (la < lb) ? -1 : (
                (la > lb) ?  1 : (
                ( a <  b) ? -1 : (
                ( a >  b) ?  1 : (
                0)))));
        }
    })(jQuery);

    function checkAllLoanPrograms(bChecked) {
        var tree = $("#m_tree").dynatree("getTree");
        g_checkCount = 0;
        tree.visit(function(node){
            node.select(bChecked);
            if (bChecked) g_checkCount += 1;
        });
    }
    function _executeLoanSearch() {
        g_ids = getCheckedLoanProgram();

        if (g_ids.length == 0) {
            alert("Need to select at least one loan program to search");
            return;
        } else {
            document.getElementById("ids").value = g_ids.join(";");
            document.forms[0].submit();
        }
    }
    function getCheckedLoanProgram() {
        var tree = $("#m_tree").dynatree("getTree");
        var nodes = tree.getSelectedNodes();
        return nodes.filter(function(node){ return !node.data.isFolder }).map(function(node){ return node.lLpTemplateId });
    }
</script>
</body>
</html>
