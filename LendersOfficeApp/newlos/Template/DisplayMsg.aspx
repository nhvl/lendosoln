<%@ Page language="c#" Codebehind="DisplayMsg.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Template.DisplayMsg" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
  <head>
    <title>DisplayMsg</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">            
  </head>
  <body MS_POSITIONING="FlowLayout" class="RightBackground" scroll="yes">
	<script language=javascript>
  <!--
    function _init() {
      document.getElementById("Title").innerHTML = dialogArguments.Title;
      document.getElementById("Message").innerHTML = dialogArguments.Message;
    }
  //-->
  </script>

    <form id="DisplayMsg" method="post" runat="server">
      <TABLE id=Table1 cellSpacing=0 cellPadding=0 width="100%" border=0 class=FormTable>
        <TR>
          <TD id="Title" class=FormTableHeader></TD>
        </TR>
        <TR>
          <TD id="Message"></TD>
        </TR>
        <TR>
          <TD align=middle><INPUT type=button value=Close onclick="onClosePopup();"></TD>
        </TR>
      </TABLE>

     </form>
	
  </body>
</html>
