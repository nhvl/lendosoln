using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;
using LendersOffice.AntiXss;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Template
{

    public partial class BrokerNotes : BaseLoanPage
	{
    
		protected void PageLoad(object sender, System.EventArgs e)
		{
            if (!Page.IsPostBack) 
            {
                CPageData dataLoan = new CLpeBrokerNotesData(RequestHelper.LoanID);
                dataLoan.InitLoad();
                sFileVersion = dataLoan.sFileVersion;
                sLpeNotesFromBrokerToUnderwriterNew.Text = dataLoan.sLpeNotesFromBrokerToUnderwriterNew;

            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

        protected void m_btnOK_Click(object sender, System.EventArgs e)
        {
            try
            {
                CPageData dataLoan = new CLpeBrokerNotesData(RequestHelper.LoanID);
                dataLoan.InitSave(sFileVersion);

                dataLoan.sLpeNotesFromBrokerToUnderwriterNew = sLpeNotesFromBrokerToUnderwriterNew.Text;
                dataLoan.Save();

                LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this.Page, "OK", "true");
            }
            catch (LoanFieldWritePermissionDenied exc)
            {
                AddInitScriptFunctionWithArgs("f_accessDenied", AspxTools.JsString(exc.UserMessage)); return;
            }
        }
	}
}
