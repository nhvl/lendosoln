using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using DataAccess;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Template
{
    public partial class QualifiedLoanProgramSearchResult : BaseLoanPage
    {
        protected bool m_isLpeMode = false;

        protected bool m_isLead
        {
            get { return LendersOffice.Common.RequestHelper.GetBool("islead"); }
        }

        private void RegisterResource()
        {
            EnableJquery = false;
            EnableJqueryMigrate = false;
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("dynatree/jquery.cookie.js");
            RegisterJsScript("dynatree/jquery.dynatree.js");
            RegisterCssFromInc("dynatree/skin-vista/ui.dynatree.css");
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.DisplayCopyRight = false;
            this.RegisterService("qualify", "/newlos/Template/QualifiedLoanProgramSearchResultService.aspx");
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
            RegisterResource();

            if (!Page.IsPostBack)
            {
                LoadLoanPrograms();
            }
        }

        private void LoadLoanPrograms()
        {
            var dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(QualifiedLoanProgramSearchResult));
            dataLoan.InitLoad();

            if (dataLoan.lpeRunModeT != E_LpeRunModeT.Full && m_isLpeMode)
            {
                // 12/17/2004 dd - sIsAllowedToRunLpe only applicable to broker with pricing engine subscription
                Response.Redirect("QualifiedNotAvailable.aspx");
                return;
            }

            var isLoanProgramRegistered = dataLoan.sLpTemplateId != Guid.Empty;
            var folders = LoadLoanProductFolders(BrokerID, m_isLpeMode);
            var loanPrograms = LoanLoanPrograms(BrokerID);

            this.RegisterJsObjectWithJsonNetAnonymousSerializer("state", new
            {
                IsLoanProgramRegistered = isLoanProgramRegistered,
                isLpeMode = m_isLpeMode,

                folders,
                loanPrograms,
            });

            ProgramSetMessage.Visible = isLoanProgramRegistered && m_isLpeMode == false;
        }

        private static List<object> LoanLoanPrograms(Guid brokerId)
        {
            // 2/1/2005 dd - The main tree will always list all loan product. Disregard of the sProdFinMethFilterT.
            // 2/11/2005 dd - The main tree will always use the maximum rate lock requested and maximum penalty request.

            var loanPrograms = new List<object>();
            using (var reader = CLoanProgramsFromDb.ListByBrokerIdForNonlpe(brokerId))
            {
                while (reader.Read())
                {
                    var lLpTemplateId = reader["lLpTemplateId"];
                    var lLpTemplateNm = WebUtility.HtmlEncode(reader["lLpTemplateNm"].ToString()).TrimWhitespaceAndBOM();
                    var FolderID = reader["FolderID"] == DBNull.Value ? Guid.Empty : (Guid) reader["FolderID"];

                    loanPrograms.Add(new
                    {
                        lLpTemplateId,
                        lLpTemplateNm,
                        FolderID,
                    });
                }
            }

            return loanPrograms;
        }

        private static List<object> LoadLoanProductFolders(Guid brokerId, bool isLpeMode)
        {
            var folders = new List<object>();

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerID", brokerId),
                new SqlParameter("@IsLpe", isLpeMode)
            };
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListLoanProductFolderByBrokerID", parameters))
            {
                while (reader.Read())
                {
                    var FolderID = (Guid) reader["FolderID"];
                    var ParentFolderID = reader["ParentFolderID"] == DBNull.Value ? null : (Guid?) reader["ParentFolderID"];
                    var FolderName = WebUtility.HtmlEncode(reader["FolderName"].ToString()).TrimWhitespaceAndBOM();

                    folders.Add(new
                    {
                        FolderID,
                        ParentFolderID,
                        FolderName,
                    });
                }
            }

            return folders;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
