using System;
using System.Collections;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Constants;
using LendersOfficeApp.los.RatePrice;
using System.Collections.Generic;
using LendersOffice.Common.Controls;
using LendersOffice.Common;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.newlos.Template
{
	public partial class QualifiedLoanProgramSearchResultNew : BaseLoanPage
	{
        private Hashtable m_hashTable = null;
        protected bool m_isLpeMode = false;

        private bool IsLoanProgramRegistered { get; set; }

        protected bool m_isLead 
        {
            get { return LendersOffice.Common.RequestHelper.GetBool("islead"); }
        }

        protected bool IsBigLoanPage
        {
            get
            {
                return Broker.IsEnableBigLoanPage;
            }
        }

        private TreeNodeWithInfo GetNode(Guid key) 
        {
            TreeNodeWithInfo node = (TreeNodeWithInfo) m_hashTable[key];
            if (null == node) 
            {
                node = new TreeNodeWithInfo();
                node.SelectAction = TreeNodeSelectAction.None;
                m_hashTable[key] = node;
            }
            return node;
        }
        protected void PageInit(object sender, System.EventArgs e) 
        {
            if (ConstStage.UseOldQualifiedLoanProgramSearchResult)
            {
                Server.Transfer("QualifiedLoanProgramSearchResult.aspx", true);
            }
            this.DisplayCopyRight = false;
            this.RegisterService("qualify", "/newlos/Template/QualifiedLoanProgramSearchResultService.aspx");
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
            if (!Page.IsPostBack) 
            {
                LoadLoanPrograms();
            } 
		}

        private void LoadLoanPrograms() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(QualifiedLoanProgramSearchResultNew));
            
            dataLoan.InitLoad();
            if (dataLoan.lpeRunModeT != E_LpeRunModeT.Full && m_isLpeMode)
            {
                // 12/17/2004 dd - sIsAllowedToRunLpe only applicable to broker with pricing engine subscription
                Response.Redirect("QualifiedNotAvailable.aspx");
                return;
            }

            IsLoanProgramRegistered = dataLoan.sLpTemplateId != Guid.Empty;
          
            m_tree.Nodes.Clear();
            m_hashTable = new Hashtable();

            TreeNodeWithInfo root = new TreeNodeWithInfo();
            root.Text = "Root Folder";
            root.SelectAction = TreeNodeSelectAction.None;

            m_tree.Nodes.Add(root);


            SqlParameter[] parameters = { 
                                            new SqlParameter("@BrokerID", BrokerID),
                                            new SqlParameter("@IsLpe", m_isLpeMode)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader( DataSrc.LpeSrc, "ListLoanProductFolderByBrokerID", parameters)) 
            {
                while (reader.Read()) 
                {
                    TreeNodeWithInfo parent = root;
                    TreeNodeWithInfo child = GetNode((Guid) reader["FolderID"]);
                    child.Text = Server.HtmlEncode(reader["FolderName"].ToString()).TrimWhitespaceAndBOM();

                    child.Info["Type"] = "FolderType";
                    child.ImageUrl = "/webctrl_client/1_0/images/folder.gif";

                    if (reader["ParentFolderID"] != DBNull.Value) 
                    {
                        parent = GetNode((Guid) reader["ParentFolderID"]);
                    }

					SortedNodeAdd(parent, child);
                }

                ProgramSetMessage.Visible = IsLoanProgramRegistered && m_isLpeMode == false; 
            }

            // 2/1/2005 dd - The main tree will always list all loan product. Disregard of the sProdFinMethFilterT.
            // 2/11/2005 dd - The main tree will always use the maximum rate lock requested and maximum penalty request.
            using (DbDataReader reader = ListLoanPrograms(dataLoan.sLienPosT) ) 
            {
                while (reader.Read()) 
                {
                    TreeNodeWithInfo parent = root;
                    TreeNodeWithInfo child = new TreeNodeWithInfo();

                    child.SelectAction = TreeNodeSelectAction.None;

                    if (m_isLpeMode) 
                    {
                        child.Text = Server.HtmlEncode(reader["lLpTemplateNm"].ToString()).TrimWhitespaceAndBOM();
                    }
                    else if (IsLoanProgramRegistered)
                    {
                        child.Text = string.Format(@"{0} (<a href=""#"" disabled=""disabled"" onclick=""return false;"">apply</a>)&nbsp;&nbsp;(<a href=# style=""color:blue;"" onclick=""return f_viewProgram('{1}');"" title=""View"">view</a>)", Server.HtmlEncode(reader["lLpTemplateNm"].ToString()).TrimWhitespaceAndBOM(), reader["lLpTemplateId"]); 
                    }
                    else
                    {
                        string str = string.Format(@"{0} (<a href=# style=""color:blue;"" onclick=""return f_applyProgram('{1}');"" title=""Apply"">apply</a>)&nbsp;&nbsp;(<a href=# style=""color:blue;"" onclick=""return f_viewProgram('{1}');"" title=""View"">view</a>)",
                            Server.HtmlEncode(reader["lLpTemplateNm"].ToString()).TrimWhitespaceAndBOM(), reader["lLpTemplateId"]);
                        child.Text = str;
                    }

                    child.Info["Type"] = "LoanProgramType";
                    child.Value = reader["lLpTemplateId"].ToString();

                    if (reader["FolderID"] != DBNull.Value) 
                    {
                        parent = GetNode((Guid) reader["FolderID"]);
                    }

					SortedNodeAdd(parent, child);
                }
            }

        }

		// 05/18/06 mf - OPM 4695. We want the nodes to be in alphabetical order.

		// This method inserts the node into the proper alphabetic location
		// of the parent.  The folders should be above the other nodes to
		// resemble windows explorer.
		private void SortedNodeAdd(TreeNodeWithInfo parent, TreeNodeWithInfo child)
		{
			string childText = child.Text;
            string childType = child.Info["Type"];

            for (int i = 0; i < parent.ChildNodes.Count; i++)
            {
                var ithChild = (TreeNodeWithInfo)parent.ChildNodes[i];

                if (childType == "FolderType")
                {
                    if (ithChild.Info["Type"] != "FolderType" || childText.CompareTo(ithChild.Text) < 0)
                    {
                        parent.ChildNodes.AddAt(i, child);
                        return;
                    }
                }
                else
                {
                    if(ithChild.Info["Type"] != "FolderType" && childText.CompareTo(ithChild.Text) < 0)
                    {
                        parent.ChildNodes.AddAt(i, child);
                        return;
                    }
                }
            }
			parent.ChildNodes.Add(child);
		}

        private DbDataReader ListLoanPrograms(E_sLienPosT sLienPosT) 
        {
            return CLoanProgramsFromDb.ListByBrokerIdForNonlpe(BrokerID);
        }

        private void DisplayProgressingIndicator() 
        {
            m_main.Controls.Add(new PassthroughLiteral() { Text = string.Format("<img src='{0}/images/status.gif'>", VirtualRoot) });
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
