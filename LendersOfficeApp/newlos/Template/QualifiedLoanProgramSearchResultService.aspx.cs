using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos.Template
{
	public partial class QualifiedLoanProgramSearchResultService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "ApplyProgram":
                    ApplyProgram();
                    break;
            }
        }
        private void ApplyProgram() 
        {
            Guid lLpTemplateId = GetGuid("lLpTemplateId");
            Guid sLId = GetGuid("sLId");
            int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
            CLoanProductData loanProduct = new CLoanProductData(lLpTemplateId);
            loanProduct.InitLoad();

            CPageData dataLoan = new CLOSubmitFirstLoanData(sLId);
            dataLoan.InitSave(sFileVersion);
            dataLoan.ApplyLoanProductTemplate(loanProduct, true);
            dataLoan.Save();
            SetResult("Result", "OK");
        }
	}
}
