using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.ConfigSystem;
using System.Text;
using LendersOffice.Security;
using LendersOfficeApp.los.admin;

namespace LendersOfficeApp.newlos.Template
{
	public partial class RunEmbeddedPML : LendersOfficeApp.newlos.BaseLoanPage
	{

        protected string m_url = "QualifiedNotAvailable.aspx";
        private bool m_doBreakout = false; // break out of the frame
        protected bool m_isLoanTypeSupported = true;
        protected bool m_is1stLienCredit = false;

        protected bool DoBreakout // break out of the frame
        {
            get { return m_doBreakout; }
            set { m_doBreakout = value; }
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            m_url = Tools.GetEmbeddedPmlUrl(BrokerUser, LoanID, m_isLead,
                ref m_isLoanTypeSupported, ref m_doBreakout);

            var isBetaParam = RequestHelper.GetSafeQueryString("isBeta");
            if(!string.IsNullOrEmpty(isBetaParam) && Broker.IsEnablePmlResults3Ui(BrokerUser))
            {
                m_url += "&isBeta=" + isBetaParam;
            }

            // moved to LosUtils.cs
            //CPageData dataLoan = new CCheckLpeRunModeData(LoanID);
            //dataLoan.InitLoad();

           
            //E_LpeRunModeT lpeRunModeT = dataLoan.lpeRunModeT;

            //string oldEngineDebugInfo, newEgineDebugInfo;

            //LoanValueEvaluator evaluator = new LoanValueEvaluator(BrokerUser.BrokerId, LoanID, ConstApp.OPERATION_USE_OLD_SECURITY_MODEL_PML, ConstApp.OPERATION_RUN_PML_ALL_LOANS );
            //evaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(BrokerUser));
            //bool useOldEngine = LendingQBExecutingEngine.CanPerform(ConstApp.OPERATION_USE_OLD_SECURITY_MODEL_PML, evaluator);

            
            //if (useOldEngine)
            //{
            //    string oldEngineUrl = GetOldEngineRedirectUrl(lpeRunModeT, out oldEngineDebugInfo);
            //    m_url = oldEngineUrl;
            //}
            //else
            //{
            //    // Construction, Construction Perm, and Other are not supported by the new UI
            //    switch (dataLoan.sLPurposeT)
            //    {
            //        case E_sLPurposeT.Construct:
            //        case E_sLPurposeT.ConstructPerm:
            //        case E_sLPurposeT.Other:
            //            m_isLoanTypeSupported = false;
            //            break;
            //        default:
            //            string newEngineUrl = GetNewEngineRedirectUrl(out newEgineDebugInfo);
            //            m_url = newEngineUrl;
            //            break;
            //    }
            //}

            //if (dataLoan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed)
            //{
            //    DoBreakout = false;  // because then it was linked to directly without an outer frame.
            //}
        }

        // moved to LosUtils.cs
        //private string GetOldEngineRedirectUrl(E_LpeRunModeT lpeRunModeT, out string debugInfo)
        //{
        //    debugInfo = "lpeRunModeT: " + lpeRunModeT;
        //    switch (lpeRunModeT)
        //    {
        //        case E_LpeRunModeT.NotAllowed:
        //            return  "QualifiedNotAvailable.aspx?loanid=" + LoanID;
        //        case E_LpeRunModeT.Full:
        //        case E_LpeRunModeT.OriginalProductOnly_Direct:
        //            string url = VirtualRoot + "/embeddedpml/main/agents.aspx?loanid=" + LoanID;
        //            DoBreakout = false;

        //            BrokerDB broker = BrokerDB.RetrieveById(BrokerID);
        //            bool isNewPmlUIEnabled = false;
        //            if (broker.IsNewPmlUIEnabled)
        //            {
        //                isNewPmlUIEnabled = true;
        //            }
        //            else
        //            {
        //                EmployeeDB employee = EmployeeDB.RetrieveById(BrokerID, EmployeeID);
        //                if (employee.IsNewPmlUIEnabled)
        //                {
        //                    isNewPmlUIEnabled = true;
        //                }
        //            }

        //            if (isNewPmlUIEnabled)
        //            {
        //                url = VirtualRoot + string.Format("/embeddedpml/webapp/pml.aspx?loanid={0}&source={1}", LoanID, "LO");
        //                if (m_isLead)
        //                {
        //                    url += "&isLead=t";
        //                }
        //                DoBreakout = true;
        //            }
        //            return url;
        //        default:
        //            throw new UnhandledEnumException(lpeRunModeT);

        //    }
        //}

        //private string GetNewEngineRedirectUrl(out string debugInfo)
        //{
        //    LoanValueEvaluator evaluator = new LoanValueEvaluator(BrokerUser.BrokerId, LoanID, ConstApp.OPERATION_RUN_PML_ALL_LOANS, ConstApp.OPERATION_RUN_PML_REGISTERED_LOANS, ConstApp.OPERATION_RUN_PML_TO_STEP_3);
        //    evaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(BrokerUser));

        //    bool canRunToStep3 = LendingQBExecutingEngine.CanPerform(ConstApp.OPERATION_RUN_PML_TO_STEP_3, evaluator);
        //    bool canRunAll = LendingQBExecutingEngine.CanPerform(ConstApp.OPERATION_RUN_PML_ALL_LOANS, evaluator);
        //    bool canRunOrig = LendingQBExecutingEngine.CanPerform(ConstApp.OPERATION_RUN_PML_REGISTERED_LOANS, evaluator);

        //    StringBuilder sb = new StringBuilder();

        //    sb.AppendLine("canRunToStep3: "+ canRunToStep3);
        //    sb.AppendLine("canRunAll: "+ canRunAll);
        //    sb.AppendLine("canRunOrig: " + canRunOrig);

        //    debugInfo = sb.ToString();

        //    string url = "QualifiedNotAvailable.aspx?loanid=" + LoanID;

        //    if (canRunToStep3 || canRunAll || canRunOrig)
        //    {
        //        url = VirtualRoot + "/embeddedpml/main/agents.aspx?loanid=" + LoanID;
        //        DoBreakout = false;

        //        BrokerDB broker = BrokerDB.RetrieveById(BrokerID);
        //        bool isNewPmlUIEnabled = false;
        //        if (broker.IsNewPmlUIEnabled)
        //        {
        //            isNewPmlUIEnabled = true;
        //        }
        //        else
        //        {
        //            EmployeeDB employee = EmployeeDB.RetrieveById(BrokerID, EmployeeID);
        //            if (employee.IsNewPmlUIEnabled)
        //            {
        //                isNewPmlUIEnabled = true;
        //            }
        //        }

        //        if (isNewPmlUIEnabled)
        //        {
        //            url = VirtualRoot + string.Format("/embeddedpml/webapp/pml.aspx?loanid={0}&source={1}", LoanID, "LO");
        //            if (m_isLead)
        //            {
        //                url += "&isLead=t";
        //            }
        //            DoBreakout = true;
        //        }
        //    }

        //    return url;
        //}

        protected bool m_isLead 
        {
            get { return RequestHelper.GetBool("islead"); }
        }
        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageID = "QualifiedLoanProgramSearch";
            this.DisplayCopyRight = false;
            this.IsRefreshLoanSummaryInfo = false;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

            this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion
	}
}
