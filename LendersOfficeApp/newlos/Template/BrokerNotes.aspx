<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="BrokerNotes.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Template.BrokerNotes" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>BrokerNotes</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
		<link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </HEAD>
  <body MS_POSITIONING="FlowLayout" bgcolor=gainsboro leftmargin=5 scroll=no>
			<script language="javascript">
<!--
function _init() {
	resize(500, 320);
	<%= AspxTools.JsGetElementById(sLpeNotesFromBrokerToUnderwriterNew) %>.focus();
}
//-->
		</script>
    <form id="BrokerNotes" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding-left:5px">
				<TR>
					<TD noWrap class=MainRightHeader>Notes</TD>
				</TR>
				<TR>
					<TD noWrap>
						<asp:TextBox id="sLpeNotesFromBrokerToUnderwriterNew" runat="server" TextMode="MultiLine" Width="100%" Height="187px" onkeyup="TextAreaMaxLength(this, 700);"></asp:TextBox></TD>
				</TR>
				<tr><td>&nbsp;</td></tr>
				<TR>
					<TD noWrap align="center">
						<asp:Button id="m_btnOK" runat="server" Text="OK" CssClass="ButtonStyle" Width="50px" onclick="m_btnOK_Click"></asp:Button>&nbsp;&nbsp;
						<INPUT type="button" value="Cancel" onclick="onClosePopup();" class="ButtonStyle" style="WIDTH: 50px"></TD>
				</TR>
			</TABLE>
			<uc1:cModalDlg id="Cmodaldlg2" runat="server"></uc1:cModalDlg>
     </form>
	
  </body>
</HTML>
