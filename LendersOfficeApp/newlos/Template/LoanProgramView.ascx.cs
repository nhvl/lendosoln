using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Template
{

	/// <summary>
	///		Summary description for LoanProgramView.
	/// </summary>
	public partial  class LoanProgramView : System.Web.UI.UserControl
	{
        private CLoanProductData m_dataSource;


        public CLoanProductData DataSource 
        {
            set { m_dataSource = value; }
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

        protected void PageDataBinding(object sender, System.EventArgs e) 
        {
            if (null == m_dataSource)
                return;

            lLpTemplateNmLabel.Text = m_dataSource.FullName ;
            lLenderNmLabel.Text = m_dataSource.lLendNm ;
            switch (m_dataSource.lLT) 
            {
                case E_sLT.Conventional:
                    lLTLabel.Text = "Conventional"; break;
                case E_sLT.FHA:
                    lLTLabel.Text = "FHA"; break;
                case E_sLT.UsdaRural:
                    lLTLabel.Text = "USDA/Rural Housing"; break;
                case E_sLT.Other:
                    lLTLabel.Text = "Other"; break;
                case E_sLT.VA:
                    lLTLabel.Text = "VA"; break;
            }

            lTermLabel.Text = m_dataSource.lTerm_rep + " / " + m_dataSource.lDue_rep;
            lLockedDaysLabel.Text = m_dataSource.lLockedDays_rep ;
            lRAdj1stCapRLabel.Text = m_dataSource.lRadj1stCapR_rep ;
            lRAdj1stCapMonLabel.Text = m_dataSource.lRadj1stCapMon_rep ;
            lRAdjCapMonLabel.Text = m_dataSource.lRAdjCapR_rep ;
            lRAdjCapMonLabel.Text = m_dataSource.lRAdjCapMon_rep ;
            lRAdjLifeCapRLabel.Text = m_dataSource.lRAdjLifeCapR_rep ;
            lRAdjMarginRLabel.Text = m_dataSource.lRAdjMarginR_rep ;
            lRAdjIndexRLabel.Text = m_dataSource.lRAdjIndexR_rep ;
            lRAdjFloorRLabel.Text = m_dataSource.lRAdjFloorR_rep ;
            switch (m_dataSource.lRAdjRoundT) 
            {
                case E_sRAdjRoundT.Down:
                    lRAdjRound.Text = "Down"; break;
                case E_sRAdjRoundT.Normal:
                    lRAdjRound.Text = "Normal"; break;
                case E_sRAdjRoundT.Up:
                    lRAdjRound.Text = "Up"; break;
            }
            lPmtAdjCapRLabel.Text = m_dataSource.lPmtAdjCapR_rep ;
            lPmtAdjCapMonLabel.Text = m_dataSource.lPmtAdjCapMon_rep ;
            lPmtAdjRecastPeriodMonLabel.Text = m_dataSource.lPmtAdjRecastPeriodMon_rep ;
            lPmtAdjRecastStopLabel.Text = m_dataSource.lPmtAdjRecastStop_rep ;
            lPmtAdjMaxBalLabel.Text = m_dataSource.lPmtAdjMaxBalPc_rep ;
            lBuydwnMon1Label.Text = m_dataSource.lBuydwnMon1_rep ;
            lBuydwnR1Label.Text = m_dataSource.lBuydwnR1_rep ;
            lBuydwnMon2Label.Text = m_dataSource.lBuydwnMon2_rep ;
            lBuydwnR2Label.Text = m_dataSource.lBuydwnR2_rep ;
            lBuydwnMon3Label.Text = m_dataSource.lBuydwnMon3_rep ;
            lBuydwnR3Label.Text = m_dataSource.lBuydwnR3_rep ;
            lBuydwnMon4Label.Text = m_dataSource.lBuydwnMon4_rep ;
            lBuydwnR4Label.Text = m_dataSource.lBuydwnR4_rep ;
            lBuydwnMon5Label.Text = m_dataSource.lBuydwnMon5_rep ;
            lBuydwnR5Label.Text = m_dataSource.lBuydwnR5_rep ;
            lGradPmtYrsLabel.Text = m_dataSource.lGradPmtYrs_rep ;
            lGradPmtRLabel.Text = m_dataSource.lGradPmtR_rep ;
            lIOnlyMonLabel.Text = m_dataSource.lIOnlyMon_rep ;
            lHasVarRFeature_chk.Checked = m_dataSource.lHasVarRFeature ;
            lVarRNotesLabel.Text = m_dataSource.lVarRNotes ;
            lAprIncludesReqDeposit_chk.Checked = m_dataSource.lAprIncludesReqDeposit ;
            lHasDemandFeature_chk.Checked = m_dataSource.lHasDemandFeature ;
            lFilingFLabel.Text = m_dataSource.lFilingF ;
            lLateDaysLabel.Text = m_dataSource.lLateDays ;
            lLateChargePcLabel.Text = m_dataSource.lLateChargePc ;
            lLateChargeBaseLabel.Text = m_dataSource.lLateChargeBaseDesc ;

            switch (m_dataSource.lPrepmtPenaltyT) 
            {
                case E_sPrepmtPenaltyT.May:
                    lPrepmtPenaltyTLabel.Text = "May"; break;
                case E_sPrepmtPenaltyT.WillNot:
                    lPrepmtPenaltyTLabel.Text = "Will not"; break;
            }
            switch (m_dataSource.lPrepmtRefundT) 
            {
                case E_sPrepmtRefundT.May:
                    lPrepmtRefundTLabel.Text = "May"; break;
                case E_sPrepmtRefundT.WillNot:
                    lPrepmtRefundTLabel.Text = "Will not"; break;
            }
            switch (m_dataSource.lAssumeLT) 
            {
                case E_sAssumeLT.May:
                    lAssumeLTLabel.Text = "May"; break;
                case E_sAssumeLT.MayNot:
                    lAssumeLTLabel.Text = "Will not"; break;
            }
            switch (m_dataSource.lFinMethT) 
            {
                case E_sFinMethT.Fixed:
                    lFinMethTLabel.Text = "Fixed"; break;
                case E_sFinMethT.ARM:
                    lFinMethTLabel.Text = "ARM"; break;
                case E_sFinMethT.Graduated:
                    lFinMethTLabel.Text = "Graduated"; break;
            }


            
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);
            this.DataBinding += new System.EventHandler(this.PageDataBinding);

        }
		#endregion
	}
}
