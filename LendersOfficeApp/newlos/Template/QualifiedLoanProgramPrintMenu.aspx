<%@ Page language="c#" Codebehind="QualifiedLoanProgramPrintMenu.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Template.QualifiedLoanProgramPrintMenu" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>QualifiedLoanProgramPrintMenu</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <script language=javascript>
    <!--
    function printMe() {
      parent.frmBody.printMe();
      
    }
    //-->
    </script>
</head>
  <body>
    <form>
      <input type=button value="Print ..." onclick="printMe();">&nbsp;&nbsp;
      <input type=button value="Close" onclick="parent.close();">
    </form>
  </body>
</html>
