<%@ Control Language="c#" AutoEventWireup="false" Codebehind="LoanProgramView.ascx.cs" Inherits="LendersOfficeApp.newlos.Template.LoanProgramView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewstate=false%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
      <table id="Table10" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr><td align=center><h4><ml:EncodedLabel id="lLenderNmLabel" runat="server"></ml:EncodedLabel></h4></td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td align=center><h4><ml:EncodedLabel id="lLpTemplateNmLabel" runat="server"></ml:EncodedLabel></h4></td></tr>
        <tr><td>&nbsp;</td></tr>
        
        <tr><td align=center><h5>PRODUCT DESCRIPTION</h5></td></tr>
        <tr><td>&nbsp;</td></tr>
        
        <tr>
          <td>
            <table id="Table12" cellspacing="0" cellpadding="0" border="0" style="FONT-SIZE: 10pt">
        <tr>
          <td style="WIDTH: 166px" class=FieldLabel>Generated</td>
          <td class=FieldLabel><%=AspxTools.HtmlString(DateTime.Now)%> by LendingQB</td></tr>
              <tr>
                <td style="WIDTH: 166px" class=FieldLabel>Loan Type</td>
                <td class=FieldLabel>
                  <ml:EncodedLabel id="lLTLabel" runat="server"></ml:EncodedLabel></td>
              </tr>
              <tr>
                <td  style="WIDTH: 166px" class=FieldLabel>Term/Due</td>
                <td class=FieldLabel>
                  <ml:EncodedLabel id="lTermLabel" runat="server"></ml:EncodedLabel></td>
              </tr>
              <tr>
                <td style="WIDTH: 166px" class=FieldLabel>Finance Method</td>
                <td class=FieldLabel>
                  <ml:EncodedLabel id="lFinMethTLabel" runat="server"></ml:EncodedLabel></td>
              </tr>
              <tr>
                <td  style="WIDTH: 166px" class=FieldLabel>Lock Period</td>
                <td class=FieldLabel>
                  <ml:EncodedLabel id="lLockedDaysLabel" runat="server"></ml:EncodedLabel> days</td>
              </tr>
              <tr>
                <td style="WIDTH: 166px"></td>
                <td></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table id="Table1" style="BORDER-RIGHT: gray 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: gray 1px solid; MARGIN-TOP: 5px; PADDING-LEFT: 5px; FONT-SIZE: 10pt; MARGIN-BOTTOM: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: gray 1px solid; MARGIN-RIGHT: 5px; PADDING-TOP: 5px; BORDER-BOTTOM: gray 1px solid" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td style="FONT-WEIGHT: bold" colspan="9">Adjustable Rate Mortgage</td>
              </tr>
              <tr>
                <td>1st Adjust Cap</td>
                <td>1st Change</td>
                <td>Adjust Cap</td>
                <td>Adjust Period</td>
                <td>Life Cap</td>
                <td>Margin</td>
                <td>Index</td>
                <td>Floor</td>
                <td nowrap>Round
                  <ml:EncodedLabel id="lRAdjRound" runat="server"></ml:EncodedLabel>&nbsp;to</td>
              </tr>
              <tr>
                <td>
                  <ml:EncodedLabel id="lRAdj1stCapRLabel" runat="server"></ml:EncodedLabel></td>
                <td>
                  <ml:EncodedLabel id="lRAdj1stCapMonLabel" runat="server"></ml:EncodedLabel></td>
                <td>
                  <ml:EncodedLabel id="lRAdjCapRLabel" runat="server"></ml:EncodedLabel></td>
                <td>
                  <ml:EncodedLabel id="lRAdjCapMonLabel" runat="server"></ml:EncodedLabel></td>
                <td>
                  <ml:EncodedLabel id="lRAdjLifeCapRLabel" runat="server"></ml:EncodedLabel></td>
                <td>
                  <ml:EncodedLabel id="lRAdjMarginRLabel" runat="server"></ml:EncodedLabel></td>
                <td>
                  <ml:EncodedLabel id="lRAdjIndexRLabel" runat="server"></ml:EncodedLabel></td>
                <td>
                  <ml:EncodedLabel id="lRAdjFloorRLabel" runat="server"></ml:EncodedLabel></td>
                <td>
                  <ml:EncodedLabel id="lRAdjRoundToRLabel" runat="server"></ml:EncodedLabel></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table id="Table4" style="BORDER-RIGHT: gray 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: gray 1px solid; MARGIN-TOP: 5px; PADDING-LEFT: 5px; FONT-SIZE: 10pt; MARGIN-BOTTOM: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: gray 1px solid; MARGIN-RIGHT: 5px; PADDING-TOP: 5px; BORDER-BOTTOM: gray 1px solid" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td style="FONT-WEIGHT: bold" colspan="5">Adjustable Payment Mortgage</td>
              </tr>
              <tr>
                <td>Adjust Cap</td>
                <td>Adjust Period</td>
                <td>Recast Period</td>
                <td>Recast Stop
                </td>
                <td>Max Balance</td>
              </tr>
              <tr>
                <td>
                  <ml:EncodedLabel id="lPmtAdjCapRLabel" runat="server"></ml:EncodedLabel></td>
                <td>
                  <ml:EncodedLabel id="lPmtAdjCapMonLabel" runat="server"></ml:EncodedLabel></td>
                <td>
                  <ml:EncodedLabel id="lPmtAdjRecastPeriodMonLabel" runat="server"></ml:EncodedLabel></td>
                <td>
                  <ml:EncodedLabel id="lPmtAdjRecastStopLabel" runat="server"></ml:EncodedLabel></td>
                <td>
                  <ml:EncodedLabel id="lPmtAdjMaxBalLabel" runat="server"></ml:EncodedLabel></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table id="Table11" cellspacing="0" cellpadding="0" width="300" border="0" style="FONT-SIZE:10pt">
              <tr>
                <td valign="top">
                  <table id="Table6" style="BORDER-RIGHT: gray 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: gray 1px solid; MARGIN-TOP: 5px; PADDING-LEFT: 5px; FONT-SIZE: 10pt; MARGIN-BOTTOM: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: gray 1px solid; WIDTH: 148px; PADDING-TOP: 5px; BORDER-BOTTOM: gray 1px solid; HEIGHT: 192px" cellspacing="0" cellpadding="0" width="148" border="0">
                    <tr>
                      <td style="FONT-WEIGHT: bold" colspan="3">Buydown Mortgage</td>
                    </tr>
                    <tr>
                      <td style="WIDTH: 16px"></td>
                      <td style="WIDTH: 45px">Rate:</td>
                      <td style="WIDTH: 137px">Term (mths):</td>
                    </tr>
                    <tr>
                      <td style="WIDTH: 16px">1</td>
                      <td style="WIDTH: 45px">
                        <ml:EncodedLabel id="lBuydwnR1Label" runat="server"></ml:EncodedLabel></td>
                      <td style="WIDTH: 137px">
                        <ml:EncodedLabel id="lBuydwnMon1Label" runat="server"></ml:EncodedLabel></td>
                    </tr>
                    <tr>
                      <td style="WIDTH: 16px">2</td>
                      <td style="WIDTH: 45px">
                        <ml:EncodedLabel id="lBuydwnR2Label" runat="server"></ml:EncodedLabel></td>
                      <td style="WIDTH: 137px">
                        <ml:EncodedLabel id="lBuydwnMon2Label" runat="server"></ml:EncodedLabel></td>
                    </tr>
                    <tr>
                      <td style="WIDTH: 16px">3</td>
                      <td style="WIDTH: 45px">
                        <ml:EncodedLabel id="lBuydwnR3Label" runat="server"></ml:EncodedLabel></td>
                      <td style="WIDTH: 137px">
                        <ml:EncodedLabel id="lBuydwnMon3Label" runat="server"></ml:EncodedLabel></td>
                    </tr>
                    <tr>
                      <td style="WIDTH: 16px">4</td>
                      <td style="WIDTH: 45px">
                        <ml:EncodedLabel id="lBuydwnR4Label" runat="server"></ml:EncodedLabel></td>
                      <td style="WIDTH: 137px">
                        <ml:EncodedLabel id="lBuydwnMon4Label" runat="server"></ml:EncodedLabel></td>
                    </tr>
                    <tr>
                      <td style="WIDTH: 16px">5</td>
                      <td style="WIDTH: 45px">
                        <ml:EncodedLabel id="lBuydwnR5Label" runat="server"></ml:EncodedLabel></td>
                      <td style="WIDTH: 137px">
                        <ml:EncodedLabel id="lBuydwnMon5Label" runat="server"></ml:EncodedLabel></td>
                    </tr>
                  </table>
                </td>
                <td style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px" valign="top">
                  <table id="Table7" style="BORDER-RIGHT: gray 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: gray 1px solid; PADDING-LEFT: 5px; FONT-SIZE: 10pt; PADDING-BOTTOM: 5px; MARGIN: 5px; BORDER-LEFT: gray 1px solid; PADDING-TOP: 5px; BORDER-BOTTOM: gray 1px solid; HEIGHT: 69px" cellspacing="0" cellpadding="0" width="190" border="0">
                    <tr>
                      <td style="FONT-WEIGHT: bold" colspan="2">Other Arrangements</td>
                    </tr>
                    <tr>
                      <td style="WIDTH: 16px">Years:</td>
                      <td style="WIDTH: 45px">
                        <ml:EncodedLabel id="lGradPmtYrsLabel" runat="server"></ml:EncodedLabel></td>
                    </tr>
                    <tr>
                      <td style="WIDTH: 16px">Rate:</td>
                      <td style="WIDTH: 45px">
                        <ml:EncodedLabel id="lGradPmtRLabel" runat="server"></ml:EncodedLabel></td>
                    </tr>
                  </table>
                  <table id="Table9" style="BORDER-RIGHT: gray 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: gray 1px solid; PADDING-LEFT: 5px; FONT-SIZE: 10pt; PADDING-BOTTOM: 5px; MARGIN: 5px; BORDER-LEFT: gray 1px solid; PADDING-TOP: 5px; BORDER-BOTTOM: gray 1px solid; HEIGHT: 70px" cellspacing="0" cellpadding="0" width="190" border="0">
                    <tr>
                      <td style="FONT-WEIGHT: bold" colspan="2">Graduated Payment Mortgage</td>
                    </tr>
                    <tr>
                      <td style="WIDTH: 101px" nowrap>Interest only(mths):</td>
                      <td style="WIDTH: 45px">
                        <ml:EncodedLabel id="lIOnlyMonLabel" runat="server"></ml:EncodedLabel></td>
                    </tr>
                  </table>
                </td>
                <td valign="top"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td onclick="return false;">
            <asp:checkbox id="lAprIncludesReqDeposit_chk" runat="server" width="603px" text="Required deposit: The annual percentage rate does not take into account your required deposit."></asp:checkbox></td>
        </tr>
        <tr>
          <td onclick="return false;">
            <asp:checkbox id="lHasDemandFeature_chk" runat="server" width="603px" text="Demand feature: This obligation has a demand feature."></asp:checkbox></td>
        </tr>
        <tr>
          <td onclick="return false;">
            <asp:checkbox id="lHasVarRFeature_chk" runat="server" width="717px" text="Variable rate feature: This loan contains a variable rate feature. A variable rate disclosure has been provided earlier."></asp:checkbox></td>
        </tr>
        <tr>
          <td>
            <ml:EncodedLabel id="lVarRNotesLabel" runat="server"></ml:EncodedLabel></td>
        </tr>
        <tr>
          <td><strong>Filing Fees:</strong>
            <ml:EncodedLabel id="lFilingFLabel" runat="server"></ml:EncodedLabel></td>
        </tr>
        <tr>
          <td></td>
        </tr>
        <tr>
          <td><strong>Late charge:</strong> If a payment is more 
            than
            <ml:EncodedLabel id="lLateDaysLabel" runat="server"></ml:EncodedLabel>days late, you will 
            be charged
            <ml:EncodedLabel id="lLateChargePcLabel" runat="server"></ml:EncodedLabel>&nbsp;of
            <ml:EncodedLabel id="lLateChargeBaseLabel" runat="server"></ml:EncodedLabel></td>
        </tr>
        <tr>
          <td><strong>Prepayment:</strong> If you pay off early 
            you,</td>
        </tr>
        <tr>
          <td>
            <table id="Table13" style="WIDTH: 602px" cellspacing="0" cellpadding="0" width="602" border="0">
              <tr>
                <td style="WIDTH: 48px">
                  <ml:EncodedLabel id="lPrepmtPenaltyTLabel" runat="server"></ml:EncodedLabel></td>
                <td>have to pay a penalty.</td>
              </tr>
              <tr>
                <td style="WIDTH: 48px">
                  <ml:EncodedLabel id="lPrepmtRefundTLabel" runat="server"></ml:EncodedLabel></td>
                <td>be entitled to a refund of part of the finance charge.</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td><strong>Assumption:</strong> Someone buying your 
            property&nbsp;&nbsp;
            <ml:EncodedLabel id="lAssumeLTLabel" runat="server"></ml:EncodedLabel>&nbsp;assume the 
            remainder of your loan on the original terms.</td>
        </tr>
        <tr>
          <td></td>
        </tr>
      </table>
