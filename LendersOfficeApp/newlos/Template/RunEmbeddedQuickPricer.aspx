﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RunEmbeddedQuickPricer.aspx.cs" Inherits="LendersOfficeApp.newlos.Template.RunEmbeddedQuickPricer" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html>
<head runat="server">
    <title>Quick Pricer</title>
</head>
<body MS_POSITIONING="FlowLayout" class="EditBackground" scroll="no">
<script language=javascript>
  <!--
    resizeTo(1150, 925);
  //-->
  </script>
    <form id="RunEmbeddedQP" method="post" runat="server">
    <iframe src=<%= AspxTools.SafeUrl(m_url) %> width="100%" height="100%"></iframe>
    <uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
    </form>
</body>
</html>
