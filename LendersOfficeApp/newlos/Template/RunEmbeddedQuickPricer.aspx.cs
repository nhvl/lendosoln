﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.ObjLib.QuickPricer;

namespace LendersOfficeApp.newlos.Template
{
    public partial class RunEmbeddedQuickPricer : LendersOffice.Common.BasePage
    {
        protected string m_url = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal; //Page.User as BrokerUserPrincipal;
            if (null == principal)
                RequestHelper.Signout(); // 10/12/2005 dd - If unable to cast to BrokerUserPrincipal redirect to log out.

            m_url = VirtualRoot + "/embeddedpml/QuickPricerNonAnonymous.aspx?userid=" + principal.UserId;
            
            if (principal.IsActuallyUsePml2AsQuickPricer)
            {
                Guid slid;

                try
                {
                    slid = QuickPricer2LoanPoolManager.GetSandboxedUnusedLoanAndMarkAsInUse();
                }
                catch (AccessDenied ad)
                {
                    var msg = string.Format(
                            "User {0} has IsActuallyUsePml2AsQuickPricer true, but was unable to create a QuickPricer 2.0 Loan." + 
                            "Redirecting them to {1}",
                            principal.UserId, // {0}
                            m_url); // {1}

                    Tools.LogErrorWithCriticalTracking(
                        msg,
                        ad);

                    return;
                }

                bool isLoanTypeSupported = true;
                bool doBreakout = false;
                var url = Tools.GetEmbeddedPmlUrl(principal, slid, true,
                    ref isLoanTypeSupported, ref doBreakout);

                if (false == isLoanTypeSupported || false == url.Contains("/pml.aspx", StringComparison.OrdinalIgnoreCase))
                {
                    // go to regular QuickPricer if would otherwise redirect to an unsupported url.
                    var msg = string.Format(
                            "User {0} has IsActuallyUsePml2AsQuickPricer true, and created loan {1}, but due to workflow couldn't go to PML, " +
                            "and instead would have been redirected to {2}",
                            principal.UserId, // {0}
                            slid, // {1}
                            url); // {2}

                    Tools.LogErrorWithCriticalTracking(
                        msg,
                        new Exception(msg));

                    return; 
                }

                Response.Redirect(url, false); 
            }
        }
    }
}
