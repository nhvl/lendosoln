using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Services;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Integration.GenericFramework;
using System.Linq;

namespace LendersOfficeApp.newlos
{
    public partial class LeftTreeFrameService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        private BrokerUserPrincipal BrokerUser 
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; /*User as BrokerUserPrincipal;*/ }
        }

        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "CreateSecondLoan":
                    CreateSecondLoan();
                    break;
                case "LoadGenericFrameworkVendor":
                    LoadGenericFrameworkVendor();
                    break;
                case "updateFavorites":
                    UpdateFavorites();
                    break;
            }
        }

        private void CreateSecondLoan()
        {
            Guid sLId = GetGuid("LoanID");

            CPageData dataLoan = new CCreateSubfinancingLoanData(sLId);
            dataLoan.InitLoad();

            if (dataLoan.IsTemplate) 
            {
                SetResult("Error", ErrorMessages.Create2nd_LoanIsTemplate);
            }
            else if (dataLoan.sLinkedLoanInfo.IsLoanLinked) 
            {
                SetResult("Error", ErrorMessages.Create2nd_HasLoanLinked);
            }
            else if (!dataLoan.HasWriteLoanPermission)
            {
                SetResult("Error", ErrorMessages.Create2nd_LacksWriteLoan);
            }
            else
            {
                if (dataLoan.sLienPosT == E_sLienPosT.First)
                {
                    CPageData secondLoan = dataLoan.SpinOffLoanForSubsequent2ndLienLoanManually(BrokerUser);
                    secondLoan.Save();

                    SetResult("SecondLoanID", secondLoan.sLId);
                    SetResult("SecondLoanName", secondLoan.sLNm);
                }
                else
                {
                    SetResult("Error", ErrorMessages.Create2nd_NotFirst);
                }
            }
        }

        private void LoadGenericFrameworkVendor()
        {
            Guid brokerID = PrincipalFactory.CurrentPrincipal.BrokerId;
            string providerID = GetString("ProviderID");
            Guid loanID = GetGuid("sLId");

            var response = GenericFrameworkVendorServer.SubmitRequest(brokerID, providerID, loanID, LinkLocation.LoanEditor);

            bool hasError = true;
            string errorMessage = null;
            if (response != null && response.Window != null && response.Window.Url != null)
            {
                SetResult("PopupURL", response.Window.Url);
                SetResult("PopupHeight", response.Window.Height);
                SetResult("PopupWidth", response.Window.Width);
                SetResult("PopupIsModal", response.Window.IsModal);
                hasError = false;
            }
            else if (response != null && !string.IsNullOrEmpty(response.ErrorMessage))
            {
                errorMessage = response.ErrorMessage;
            }
            else
            {
                errorMessage = ErrorMessages.Generic;
            }

            SetResult("ErrorMessage", errorMessage);
            SetResult("HasError", hasError);
        }

        private void UpdateFavorites()
        {
            string pageId = GetString("PageId", "");
            string lastPageId = GetString("LastPageId", "");
            string favoriteType = GetString("FavoriteType", "");
            string operation = GetString("Operation", "");
            EmployeeDB empDb = new EmployeeDB(PrincipalFactory.CurrentPrincipal.EmployeeId, PrincipalFactory.CurrentPrincipal.BrokerId);
            empDb.Retrieve();
            Dictionary<string, string> favoritesDict = ObsoleteSerializationHelper.JsonDeserialize<Dictionary<string, string>>(empDb.FavoritePageIdsJSON);
            if (favoritesDict == null)
            {
                favoritesDict = new Dictionary<string, string>();
            }

            string favoritesJSON = "";
            favoritesDict.TryGetValue(favoriteType, out favoritesJSON);

            List<string> favoritesList = ObsoleteSerializationHelper.JsonDeserialize<List<string>>(favoritesJSON);
            if (favoritesList == null)
            {
                favoritesList = new List<string>();
            }
            else
            {
                favoritesList = favoritesList.Distinct().ToList();
            }

            switch(operation)
            {
                case "add":
                    if (!favoritesList.Contains(pageId))
                    {
                        favoritesList.Add(pageId);
                    }
                    break;
                case "remove":
                    favoritesList.Remove(pageId);
                    break;
                case "move":

                    favoritesList.Remove(pageId);

                    if (string.IsNullOrEmpty(lastPageId))
                    {
                        favoritesList.Add(pageId);
                    }
                    else
                    {
                        int indexOfLast = favoritesList.IndexOf(lastPageId);
                        favoritesList.Insert(indexOfLast, pageId);
                    }
                    break;
                 default: 
                    throw new Exception("An unhandled operation was given when updating Favorites.");
            }


            favoritesJSON = ObsoleteSerializationHelper.JsonSerialize(favoritesList);

            if (favoritesDict.ContainsKey(favoriteType))
            {
                favoritesDict[favoriteType] = favoritesJSON;
            }
            else
            {
                favoritesDict.Add(favoriteType, favoritesJSON);
            }

            empDb.FavoritePageIdsJSON = ObsoleteSerializationHelper.JsonSerialize(favoritesDict);
            empDb.Save(PrincipalFactory.CurrentPrincipal);
        }
    }
}
