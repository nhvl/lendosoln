﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TitleAndEstate.aspx.cs" Inherits="LendersOfficeApp.newlos.TitleAndEstate" %>
<%@ Import namespace="DataAccess"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Title And Estate</title>
    <style type="text/css">
        #SellerInfo
        {
            padding-bottom: 3px;
        }
        .MostlyFull
        {
            width: 28em;
        }
        .FullWidth
        {
            width: 34em;
        }
        .TitleWidth
        {
            vertical-align: top;
            width: 15em;
            padding: 3px;
            font-weight: bold;
            display: inline-block;
        }
        #TitleOnylBorrowerTable { list-style-type: none; margin: 0; padding: 0;}
        #TitleOnylBorrowerTable li {
            margin: 0;
            padding-left: 0;
        }
        .hidden { display: none; }
        #SellerList
        {
            list-style-type: none;
            margin: 0px;
            padding: 3px 0px;
            border: 0px;
        }
        #SellerList li
        {
            margin-bottom: 20px;
        }
        div.FieldItem
        {
            margin: 5px 0px;
        }
        .City
        {
            width: 25.4em;
        }
        .Zipcode
        {
            width: 4em;
        }
        .SellerType
        {
            width: 7.7em;
        }
        .SellerEntityType
        {
            width: 26em;
        }
        .email-error
        {
            color: red;
            display: none;
        }
    </style>
</head>
<body bgcolor="gainsboro">
    <%--
    Chrome does NOT like being told to disable autofill. This attribute value was suggested by MDN
    and must be applied at both the form and the input level.
    https://developer.mozilla.org/en-US/docs/Web/Security/Securing_your_site/Turning_off_form_autocompletion#Disabling_autocompletion
    --%>
    <form id="form1" runat="server" autocomplete="nope">
    <div>
    <table  cellspacing="0" cellpadding="0" width="700">
        <tr>
            <td class="MainRightHeader" nowrap>
                Title and Estate
            </td>
        </tr>
        <tr>
            <td>
                <table class="InsetBorder" id="Table3" cellSpacing="0" cellPadding="0" width="99%">
                    <tr id="BorrowerTitleWillBeHeldInWhatNamesRow">
                        <td class="FieldLabel">Title will be held in what Name(s)</td>
                        <td class="FieldLabel">
                            <asp:textbox id="aTitleNm1" tabIndex="108" runat="server" MaxLength="56" Width="211px"></asp:textbox>
                            <asp:CheckBox id="aTitleNm1Lckd" Text="Lock" runat="server" onclick="refreshCalculation();" tabindex="109"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr id="CoborrowerTitleWillBeHeldInWhatNamesRow" class="show-for-coborrower">
                        <td>
                            <label id="CoborrowerTitleWillBeHeldInWhatNamesLabel" class="FieldLabel">
                                Title will be held in what Name(s)
                            </label>
                        </td>
                        <td class="FieldLabel">
                            <asp:textbox id="aTitleNm2" tabIndex="112" runat="server" MaxLength="56" Width="211px"></asp:textbox>
                            <asp:CheckBox id="aTitleNm2Lckd" Text="Lock" runat="server" onclick="refreshCalculation();" tabindex="113"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr class="ulad-data">
                        <td class="FieldLabel">
                            <ml:EncodedLiteral runat="server" ID="aBNm"></ml:EncodedLiteral> is currently on title
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="aBIsCurrentlyOnTitle"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="show-for-coborrower ulad-data">
                        <td class="FieldLabel">
                            <ml:EncodedLiteral runat="server" ID="aCNm"></ml:EncodedLiteral> is currently on title
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="aCIsCurrentlyOnTitle"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="BorrowerCurrentTitleNameRow" class="ulad-data">
                        <td class="FieldLabel">Title currently held in what Name(s)</td>
                        <td class="FieldLabel">
                            <asp:textbox id="aBCurrentTitleName" tabIndex="108" runat="server" MaxLength="56" Width="211px"></asp:textbox>
                            <asp:CheckBox id="aBCurrentTitleNameLckd" Text="Lock" runat="server" onclick="refreshCalculation();" tabindex="109"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr id="CoborrowerCurrentTitleNameRow" class="show-for-coborrower ulad-data">
                        <td class="FieldLabel">
                            <span id="CoorrowerCurrentTitleNameLabel">Title currently held in what Name(s)</span>
                        </td>
                        <td class="FieldLabel">
                            <asp:textbox id="aCCurrentTitleName" tabIndex="112" runat="server" MaxLength="56" Width="211px"></asp:textbox>
                            <asp:CheckBox id="aCCurrentTitleNameLckd" Text="Lock" runat="server" onclick="refreshCalculation();" tabindex="113"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Manner in which Title will be held</td>
                        <td class="FieldLabel" noWrap><ml:combobox id="aManner" tabIndex="116" runat="server" Width="210px"></ml:combobox></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Estate will be held in
                        </td>
                        <td class="FieldLabel"><asp:dropdownlist id="sEstateHeldT" tabIndex="118" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>&nbsp;&nbsp;Expiration 
                            date
                            <ml:datetextbox id="sLeaseHoldExpireD" tabIndex="120" disableYearLimit='true' runat="server" preset="date" width="75" dttype="DateTime"></ml:datetextbox></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" title="Source of Down Payment, Settlement Charges and/or Subordinate Financing">Source of Down Payment</td>
                        <td class="FieldLabel"><ml:combobox id="sDwnPmtSrc" tabIndex="122" runat="server" Width="350px"></ml:combobox></td>
                    </tr>
                    <tr>
                        <td align=center class="FieldLabel">(Explain on this line)</td>
                        <td class="FieldLabel"><asp:textbox id="sDwnPmtSrcExplain" tabIndex="124" runat="server" MaxLength="36" Width="243px"></asp:textbox></td>
                    </tr>
                    <tr class="ulad-data">
                        <td class="FieldLabel">Indian Country Land Tenure Status</td>
                        <td class="FieldLabel"><asp:DropDownList runat="server" ID="sNativeAmericanLandsT"></asp:DropDownList></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        <td >
        <input type="hidden" runat="server" id="TitleOnlyBorrowerJSON" />
            <div class="InsetBorder" id="TitleOnlyList">
        
             <div class="FormTableSubHeader" class="FullWidth">Non-Obligate Borrowers</div>
             
            <asp:Repeater runat="server" ID="TitleOnlyBorrowers" OnItemDataBound="TitleOnlyBorrowers_OnItemDataBound" >
                <ItemTemplate>
                    <li data-title-only-borrower-id=<%# AspxTools.HtmlAttribute( ((TitleBorrower)Container.DataItem).Id.ToString()) %>>
                        <input type="hidden" class="TitleId" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).Id) %>" />
                        
                        <div>  
                            <span class="TitleWidth">First Name</span>
                            <input type="text" class="TitleFirstNm FullWidth" onchange="refreshCalculation();" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).FirstNm) %>" />
                        </div>
                               
                        <div>  
                            <span class="TitleWidth">Middle Name</span>
                            <input type="text" class="TitleMidNm" onchange="refreshCalculation();" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).MidNm) %>" />
                        </div>
                        <div>
                            <span class="TitleWidth">Last Name</span>
                            <input type="text" class="TitleLastNm FullWidth" onchange="refreshCalculation();" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).LastNm) %>" />
                            <a href="#" class="EditAliasPOAClick">Edit Aliases/POA</a>
                            <input type="hidden" class="TitlePOA" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).POA) %>" />
                            <asp:HiddenField ID="TitleAliases" runat="server" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).Aliases) %>" />
                        </div>
                        <div>
                            <span class="TitleWidth">SSN</span>
                            <input type="text" class="TitleSSN mask"  preset="ssn" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).SSN) %>" />
                        </div>
                        <div>
                            <span class="TitleWidth">Date of Birth</span>
                            <ml:DateTextBox runat="server" ID="TitleDob" class="TitleDOB mask" preset="date" Width="75"></ml:DateTextBox>
                        </div>
                        <div>
                            <span class="TitleWidth">Home Phone</span>
                            <input type="text" class="TitleHPhone mask" preset="phone" width="120" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).HPhone) %>" />
                        </div>
                        <div>
                            <span class="TitleWidth">Email</span>
                            <input type="text" class="TitleEmail FullWidth" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).Email) %>" />
                            <span class="email-error">Invalid Email</span>
                       </div>
                       <div class="AddressParts">
                            <div>
                                <span class="TitleWidth">Address</span>
                                <input type="text" class="TitleAddress FullWidth StreetAddress" autocomplete="nope" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).Address) %>" />
                            </div>
                            <div>
                                <span class="TitleWidth">&nbsp;</span>
                                <input type="text" class="TitleCity City" autocomplete="nope" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).City) %>" />
                                <ml:StateDropDownList ID="StateDropDownList1" autocomplete="nope" Value='<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).State) %>' CssClass="TitleState" runat="server"></ml:StateDropDownList>
                                <input type="text" class="TitleZip  mask ZipCode" autocomplete="nope" preset="zipcode" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).Zip) %>" />
                            </div>
                       </div>
                       <div>
                            <span class="TitleWidth">Title Type</span>
                            <asp:DropDownList runat="server" ID="TitleRelationshipTitleT" CssClass="TitleRelationshipTitleT FullWidth"></asp:DropDownList>
                        
                       </div>
                       <div class="hidden tilteotherdesc">
                        
                            <span class="TitleWidth">Title Other Desc</span>
                            <input type="text" class="TitleRelationshipTitleTOtherDesc" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).RelationshipTitleTOtherDesc) %>" />
                       </div>
                       <div>
                           <span class="TitleWidth">Associated Application</span>
                           <asp:DropDownList runat="server" ID="TitleAssociatedApplicationId" CssClass="TitleAssociatedApplicationId FullWidth"></asp:DropDownList>
                       </div>
                       <div class="ulad-data">
                           <span class="TitleWidth">Currently Holds Title</span>
                           <input type="checkbox" class="TitleCurrentlyHoldsTitle" <%# AspxTools.HtmlString(((TitleBorrower)Container.DataItem).CurrentlyHoldsTitle ? "checked" : string.Empty) %> />
                       </div>
                       <div class="ulad-data">
                           <span class="TitleWidth">Name on Current Title</span>
                           <input type="text" class="TitleNameOnCurrentTitle FullWidth" maxlength="128" value=<%# AspxTools.HtmlAttribute(((TitleBorrower)Container.DataItem).NameOnCurrentTitle) %> />
                           <input type="checkbox" class="TitleNameOnCurrentTitleLckd title-borrower-lock" <%# AspxTools.HtmlString(((TitleBorrower)Container.DataItem).NameOnCurrentTitleLckd ? "checked" : string.Empty) %> />
                       </div>
                       <div class="ulad-data">
                           <span class="TitleWidth">Will Hold Title</span>
                           <input type="checkbox" class="TitleWillHoldTitle" <%# AspxTools.HtmlString(((TitleBorrower)Container.DataItem).WillHoldTitle ? "checked" : string.Empty) %> />
                       </div>
                       <div class="ulad-data">
                           <span class="TitleWidth">Title will be Held in What Name</span>
                           <input type="text" class="TitleWillBeHeldInWhatName FullWidth" maxlength="128" value=<%# AspxTools.HtmlAttribute(((TitleBorrower)Container.DataItem).TitleWillBeHeldInWhatName) %> />
                           <input type="checkbox" class="TitleWillBeHeldInWhatNameLckd title-borrower-lock" <%# AspxTools.HtmlString(((TitleBorrower)Container.DataItem).TitleWillBeHeldInWhatNameLckd ? "checked" : string.Empty) %> />
                       </div>
                       <a href="#" class="RemoveTitleRow" >remove</a>       
                    </li>                   
                </ItemTemplate>
                <HeaderTemplate>
                    <ul id="TitleOnylBorrowerTable" >
                            <li  class="hidden " id="TitleRowTempl">                       
                         <input type="hidden" class="TitleId"  />
                        
                        <div>  
                            <span class="TitleWidth">First Name</span>
                            <input type="text" class="TitleFirstNm FullWidth" onchange="refreshCalculation();"  />
                        </div>
                               
                        <div>  
                            <span class="TitleWidth">Middle Name</span>
                            <input type="text" class="TitleMidNm" onchange="refreshCalculation();"  />
                        </div>
                        <div>
                            <span class="TitleWidth">Last Name</span>
                            <input type="text" class="TitleLastNm FullWidth" onchange="refreshCalculation();" /><a href="#" class="EditAliasPOAClick">Edit Aliases/POA</a>
                            <input type="hidden" class="TitlePOA" />
                            <asp:HiddenField ID="TitleAliases" runat="server" />
                        </div>
                        <div>
                            <span class="TitleWidth">SSN</span>
                            <input type="text" class="TitleSSN mask"  preset="ssn" />
                        </div>
                        <div>
                            <span class="TitleWidth">Date of Birth</span>
                            <ml:DateTextBox runat="server" ID="TitleDob" class="TitleDOB mask" preset="date" Width="75"></ml:DateTextBox>
                        </div>
                        <div>
                            <span class="TitleWidth">Home Phone</span>
                            <ml:PhoneTextBox runat="server" class="TitleHPhone mask" preset="phone" Width="120"></ml:PhoneTextBox>
                        </div>
                        <div>
                            <span class="TitleWidth">Email</span>
                            <input type="text" class="TitleEmail FullWidth" />
                            <span class="email-error">Invalid Email</span>
                       </div>
                       <div class="AddressParts">
                            <div>
                                <span class="TitleWidth">Address</span>
                                <input type="text" class="TitleAddress FullWidth StreetAddress" autocomplete="nope"  />
                            </div>
                            <div>
                                <span class="TitleWidth">&nbsp;</span>
                                <input type="text" class="TitleCity City" autocomplete="nope" />
                                <ml:StateDropDownList ID="StateDropDownList1" autocomplete="nope"  CssClass="TitleState State" runat="server"></ml:StateDropDownList>
                                <input type="text" class="TitleZip  mask ZipCode" preset="zipcode" autocomplete="nope"  />
                            </div>
                       </div>
                       <div>
                            <span class="TitleWidth">Title Type</span>
                            <asp:DropDownList runat="server" ID="TitleRelationshipTitleT" CssClass="TitleRelationshipTitleT FullWidth"></asp:DropDownList>
                        
                       </div>
                       <div class="hidden tilteotherdesc">
                        
                            <span class="TitleWidth">Title Other Desc</span>
                            <input type="text" class="TitleRelationshipTitleTOtherDesc" />
                            </div>
                       <div>
                           <span class="TitleWidth">Associated Application</span>
                           <asp:DropDownList runat="server" ID="TitleAssociatedApplicationId" CssClass="TitleAssociatedApplicationId FullWidth"></asp:DropDownList>
                       </div>
                       <div class="ulad-data">
                           <span class="TitleWidth">Currently Holds Title</span>
                           <input type="checkbox" class="TitleCurrentlyHoldsTitle" />
                       </div>
                       <div class="ulad-data">
                           <span class="TitleWidth">Name on Current Title</span>
                           <input type="text" class="TitleNameOnCurrentTitle FullWidth" maxlength="128" />
                           <input type="checkbox" class="TitleNameOnCurrentTitleLckd title-borrower-lock" />
                       </div>
                       <div class="ulad-data">
                           <span class="TitleWidth">Will Hold Title</span>
                           <input type="checkbox" class="TitleWillHoldTitle" />
                       </div>
                       <div class="ulad-data">
                           <span class="TitleWidth">Title will be Held in What Name</span>
                           <input type="text" class="TitleWillBeHeldInWhatName FullWidth" maxlength="128" />
                           <input type="checkbox" class="TitleWillBeHeldInWhatNameLckd title-borrower-lock" />
                       </div>
                            <a href="#" class="RemoveTitleRow" >remove</a>       
</li>                 
                </HeaderTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
             <a href="#" class="AddTitleOnlyBorrower">add Non-Obligate Borrower</a>
            </div>
    </td>
    </tr>
        <tr>
            <td>
                <div id="SellerInfo" class="InsetBorder">
                    <div class="FormTableSubHeader">Seller Info</div>
                    <input type="hidden" id="SellerInfoHolder" name="SellerInfoHolder" value="" />
                    <div class="FieldItem">
                        <span class="FieldLabel TitleWidth">Number of Sellers</span>
                        <asp:DropDownList ID="NumberOfSellers" runat="server"></asp:DropDownList>
                        <input type="button" value="Populate from Agents" id="PopulateSellersFromAgentsBtn" />
                    </div>
                    <ul id="SellerList">
                        <asp:Repeater ID="Sellers" runat="server" OnItemDataBound="SellerList_OnItemDataBound">
                        <ItemTemplate>
                            <li class="SellerListItem">
                                <div>
                                    <span class="TitleWidth FieldLabel">Seller Name</span>
                                    <asp:TextBox ID="Name" CssClass="Name FullWidth" runat="server" />
                                    <input type="hidden" ID="AgentId" runat="server" class="AgentId" />
                                </div>
                                <div class="AddressParts">
                                    <div>
                                        <span class="TitleWidth FieldLabel">Seller Address</span>
                                        <asp:TextBox ID="StreetAddress" CssClass="StreetAddress FullWidth" runat="server" />
                                    </div>
                                    <div>
                                        <span class="TitleWidth FieldLabel">&nbsp;</span>
                                        <asp:TextBox ID="City" CssClass="City" runat="server" />
                                        <ml:StateDropDownList runat="server" class="State" ID="State" />
                                        <asp:TextBox ID="Zipcode" CssClass="Zipcode" runat="server" />
                                    </div>
                                </div>
                                <div>
                                    <span class="TitleWidth FieldLabel">&nbsp;</span>
                                    <asp:DropDownList ID="SellerType" CssClass="SellerType" runat="server">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="SellerEntityType" CssClass="SellerEntityType" runat=server>
                                    </asp:DropDownList>
                                </div>
                            </li>
                        </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="FieldItem">
                        <span class="TitleWidth FieldLabel">Seller Vesting </span>
                        <asp:TextBox ID="sSellerVesting" runat="server" CssClass="MostlyFull" TextMode="MultiLine"
                            Rows="4"></asp:TextBox>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <%--Create new seller form elements using this template.--%>
                <ul id="SellerTemplate" style="display: none;">
                    <li class="SellerListItem">
                        <div>
                            <span class="TitleWidth FieldLabel">Seller Name</span>
                            <input type="text" class="FullWidth Name" />
                            <input type="hidden" class="AgentId" value="<%=AspxTools.HtmlString(Guid.Empty) %>" />
                        </div>
                        <div class="AddressParts">
                            <div>
                                <span class="TitleWidth FieldLabel">Seller Address</span>
                                <input type="text" class="FullWidth StreetAddress" />
                            </div>
                            <div>
                                <span class="TitleWidth FieldLabel">&nbsp;</span>
                                <input type="text" class="City" />
                                <ml:StateDropDownList runat="server" class="State" ID="SellerTemplateState" />
                                <input type="text" class="Zipcode" />
                            </div>
                        </div>
                        <div>
                            <span class="TitleWidth FieldLabel">&nbsp;</span>
                            <asp:DropDownList ID="SellerTemplateSellerType" CssClass="SellerType" runat="server"></asp:DropDownList>
                            <asp:DropDownList ID="SellerTemplateSellerEntityType" CssClass="SellerEntityType" runat=server></asp:DropDownList>
                        </div>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
    <script language="javascript" type="text/javascript">
    
        var numTitleOnlyBorrowerClicked = 0;
function combineName(first, last, middle, suffix) {
  var name = first;
  if (middle.replace(/^[\s]+/g,"").length > 0) name += " " + middle;
  if (last.replace(/^[\s]+/g,"").length > 0) name += " " + last;
  if (suffix.replace(/^[\s]+/g,"").length > 0) name += ", " + suffix;
  
  return name;
  
}

  function _init() {
    <%= AspxTools.JsGetElementById(sLeaseHoldExpireD) %>.readOnly = <%= AspxTools.JsGetElementById(sEstateHeldT) %>.selectedIndex == 0;  
    lockField(<%= AspxTools.JsGetElementById(aTitleNm1Lckd) %>, 'aTitleNm1');
    lockField(<%= AspxTools.JsGetElementById(aTitleNm2Lckd) %>, 'aTitleNm2');
    lockField(<%= AspxTools.JsGetElementById(aBCurrentTitleNameLckd) %>, 'aBCurrentTitleName');
    lockField(<%= AspxTools.JsGetElementById(aCCurrentTitleNameLckd) %>, 'aCCurrentTitleName');

    if ( <%= AspxTools.JsBool(useNewNonPurchaseBorrower) %> )
    {
        $('.AddTitleOnlyBorrower').hide();
        if (<%= AspxTools.JsBool(TitleOnlyBorrowers.Items.Count == 0) %>)
        {
            $('#TitleOnlyList').hide();
        }
    }
  }

  function PopulateSellersFromAgentsBtn_click() {
    var i, agentCount;
    if (AgentsOfRoleSeller == null) {
        $('#NumberOfSellers').val('0'); return;
    }
    $('#SellerList').children().remove();
    agentCount = AgentsOfRoleSeller.length;
    $('#NumberOfSellers').val(agentCount);
    for (i = 0; i < agentCount; ++i)
        $('#SellerList').append(GeneratePopulatedSeller(AgentsOfRoleSeller[i]));
    if(typeof(updateDirtyBit) != 'undefined')
        updateDirtyBit();
  }
  
  function GeneratePopulatedSeller(SellerObject) {
    var newSeller = FreshSeller();
    $(newSeller).find('.Name').first().val(SellerObject.Name);
    $(newSeller).find('.AgentId').first().val(SellerObject.AgentId);
    $(newSeller).find('.StreetAddress').first().val(SellerObject.Address.StreetAddress);
    $(newSeller).find('.City').first().val(SellerObject.Address.City);
    $(newSeller).find('.State').first().val(SellerObject.Address.State);
    $(newSeller).find('.Zipcode').first().val(SellerObject.Address.PostalCode);
    return newSeller;
  }
  
  function FreshSeller() {
    return $('#SellerTemplate').children('li').first().clone();
  }
  
  function disableSellerEntityType($sellerType) {
    var IsNotEntity = ($sellerType.val() != 'Entity');
    var sellerEntityType = $sellerType.next();
    if (IsNotEntity) sellerEntityType.val('Undefined');
    sellerEntityType.prop('disabled', IsNotEntity);
  }

    function setTitleCurrentlyHeldInNameSectionDisplay() {
        var yesValue = <%= AspxTools.JsString(E_TriState.Yes) %>;
        var borrowerCurrentlyOnTitle = $('#aBIsCurrentlyOnTitle').val() === yesValue;
        var coborrowerCurrentlyOnTitle = ML.HasCoborrower && $('#aCIsCurrentlyOnTitle').val() === yesValue;

        $('#BorrowerCurrentTitleNameRow').toggle(borrowerCurrentlyOnTitle);
        $('#CoorrowerCurrentTitleNameLabel').toggle(!borrowerCurrentlyOnTitle);
        $('#CoborrowerCurrentTitleNameRow').toggle(coborrowerCurrentlyOnTitle);
    }
  
  window.onload = function() {
      disableSellerEntityType($('.SellerType'));
      $('#PopulateSellersFromAgentsBtn').click(PopulateSellersFromAgentsBtn_click);

      $('#SellerList').on('change','.SellerType',function(){disableSellerEntityType($(this));});

      $('#SellerList').on('keydown','input',function(event){if(typeof(updateDirtyBit) != 'undefined') updateDirtyBit(event);});
      $('#SellerList').on('change','select',function(event){if(typeof(updateDirtyBit) != 'undefined') updateDirtyBit(event);});

      $('.Name').change(function () {
          $listItem = $(this).closest('.SellerListItem');
          $listItem.find('.AgentId').val(<%=AspxTools.JsString(Guid.Empty) %>);
      });

      $('#NumberOfSellers').change(function() {
          var selectedNum = $(this).val();
          var currentNum = $('#SellerList').children('li').length;
          while (selectedNum < currentNum) {
              $('#SellerList').children('li').last().remove();
              --currentNum;
          }
          while (selectedNum > currentNum) {
              $('#SellerList').append(FreshSeller());
              ++currentNum;
          }
      });
  };
  
  jQuery(function($){
      if (ML.IsTargeting2019Ulad) {
          $('.show-for-coborrower').toggle(ML.HasCoborrower);

          $('#BorrowerTitleWillBeHeldInWhatNamesRow').toggle(!ML.BorrowerIsCurrentTitleOnly);
          $('#CoborrowerTitleWillBeHeldInWhatNamesRow').toggle(ML.HasCoborrower && !ML.CoborrowerIsCurrentTitleOnly);
          $('#CoborrowerTitleWillBeHeldInWhatNamesLabel').toggle(ML.BorrowerIsCurrentTitleOnly && ML.HasCoborrower && !ML.CoborrowerIsCurrentTitleOnly);

          $('#aBIsCurrentlyOnTitle').prop('disabled', ML.BorrowerIsCurrentTitleOnly);
          $('#aCIsCurrentlyOnTitle').prop('disabled', ML.CoborrowerIsCurrentTitleOnly);

          setTitleCurrentlyHeldInNameSectionDisplay();
      }
      else {
          $('.ulad-data').hide();
      }
      
    $('#TitleOnylBorrowerTable .title-borrower-lock').each(function (index, checkbox) {
	    updateTitleLockBoxDisplay(checkbox);
    });

    $('#aBIsCurrentlyOnTitle, #aCIsCurrentlyOnTitle').change(function (event) {
        setTitleCurrentlyHeldInNameSectionDisplay();
	    refreshCalculation();
    });

    $('#TitleOnylBorrowerTable').on('click', 'a.EditAliasPOAClick', function() {
            var parent = $(this).parents('li');
            var aliasPOAData = {
                alias : parent.find('input[id$=TitleAliases]').val(),
                powerOfAttorney : parent.find('input.TitlePOA').val()
            }; 
            var args = {
                data : JSON.stringify(aliasPOAData)
            };
           
            var path = '/newlos/Borrower/EditAliasesPOA.aspx';
            var url = path
                + '?loanid=' + <%= AspxTools.JsString(LoanID) %>
                + '&borrmode=B';   
            showModal(url, args, null, null, function(modalResult){
                if (modalResult.OK) {
                    var data = JSON.parse(modalResult.data);
                    parent.find('input[id$=TitleAliases]').val(data.alias);
                    parent.find('input.TitlePOA').val(data.powerOfAttorney);
                    // Mark page as dirty
                    updateDirtyBit();
                } 
            },{hideCloseButton:true});    
            return false;
      });

    function updateTitleLockBoxDisplay(checkbox) {
        $(checkbox).prev().prop('disabled', !checkbox.checked);
    }

    function validateEmailAddress(input) {
        var isValid = true;
        if (input.value !== '') {
            var emailRegex = new RegExp(ML.EmailValidationExpression);
            isValid = emailRegex.test(input.value);
        }
        
        return isValid;
    }

    $('#TitleOnylBorrowerTable').on('change', '.title-borrower-lock', function (event) {
        var target = retrieveEventTarget(event);
        updateTitleLockBoxDisplay(target);
        refreshCalculation();
    });

    $('#TitleOnylBorrowerTable').on('blur', '.TitleEmail', function (event) {
        var target = retrieveEventTarget(event);
        var isValid = validateEmailAddress(target);
        $(target).parent().find('span.email-error').toggle(!isValid);
    });
    
      $('a.AddTitleOnlyBorrower').click(function(){
        numTitleOnlyBorrowerClicked++;

        var parent = $('#TitleOnylBorrowerTable');
        var cloneRow =  $('#TitleRowTempl').clone().removeClass('hidden');
        cloneRow.removeAttr('id');

        // To clone the calendar
        var createdCalendar = cloneRow.find('.TitleDOB');
        var newId = $(createdCalendar).prop('id')+numTitleOnlyBorrowerClicked;
        $(createdCalendar).prop('id', newId);
        $(createdCalendar).siblings('a').attr('onclick', "return displayCalendar('"+newId+"')");
        _initMask(createdCalendar.get(0));
        _initDynamicInput(createdCalendar.get(0));

        parent.append(cloneRow);
        _initMask(cloneRow.find('input.TitleSSN').get(0));
        _initMask(cloneRow.find('input.TitleHPhone').get(0));
        _initMask(cloneRow.find('input.TitleZip').get(0));

        var id = getId();
        cloneRow.attr('data-title-only-borrower-id', id).find('input.TitleId').val(id);
        cloneRow.find('input[id$=TitleAliases]').val(JSON.stringify([]));
        return false;
    });
            $('#TitleOnylBorrowerTable').on('click', 'a.RemoveTitleRow', function(){
                updateDirtyBit();
                $(this).parents('li').remove();
                return false;
            });

            $('#TitleOnylBorrowerTable').on('keydown, change', 'input', function(event){
                if(typeof(updateDirtyBit) != 'undefined') updateDirtyBit(event);
            });
            
            $(document).on('change', 'select.TitleRelationshipTitleT', function(){
                if ($(this).val() == 53) {
                    $(this).parents('div').siblings('div.tilteotherdesc').removeClass('hidden');
                }
                else {
                    $(this).parents('div').siblings('div.tilteotherdesc').addClass('hidden');
                    $(this).parents('div').siblings('div.tilteotherdesc').val('');
                }   
            });

            $(document).on('blur', '.Zipcode', function(event){
                var parent = $(this).parents('li');
                smartZipcode(
                    this,
                    parent.find('.City').get(0),
                    parent.find('.State').get(0), null, event);
            });
            
            $(document).on('focus', '.Zipcode, .TitleDOB', function(){
                this.oldValue = this.value;   
            });
    
    function getId() {
        var result = gService.utils.call('UniqueId', {});
        return result.value.Id;;
    }

        //Stuff the contents of our seller list into an input before save
        var oldSaveMe = saveMe;
        saveMe = function(bRefresh)
        {
            var isValid = true;
            $('#TitleOnylBorrowerTable .TitleEmail').each(function (index, input) {
                if (!validateEmailAddress(input)) {
                    isValid = false;
                    return false;
                }
            });

            if (!isValid) {
                return false;
            }

            var sellerList = [];
            $('#SellerList li').each(function(){
                var current, address = 
                { 
                    'StreetAddress': $(this).find('.StreetAddress').val(),
                    'City': $(this).find('.City').val(),
                    'State': $(this).find('.State').val(),
                    'PostalCode': $(this).find('.Zipcode').val()
                };
                current = {
                    'Name': $(this).find('.Name').val(),
                    'AgentId': $(this).find('.AgentId').val(),
                    'Address': address,
                    'Type': $(this).find('.SellerType').val(),
                    'EntityType': $(this).find('.SellerEntityType').val()
                };
                sellerList.push(current);
            });
            var result = {
                    'ListOfSellers': sellerList
                };
            var saveVal = JSON.stringify(result);
            $('#SellerInfoHolder').val(saveVal);
            $('#TrustInfoHolder').val(JSON.stringify(result));
                
            return oldSaveMe(bRefresh);
        }
  });
    </script>
</html>
