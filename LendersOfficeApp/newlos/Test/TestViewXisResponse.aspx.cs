namespace LendersOfficeApp.newlos.Test
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Xml;
    using System.Xml.Serialization;
    using DataAccess;
    using DocMagic.DsiDocRequest;
    using global::Integration.Encompass;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Conversions;
    using LendersOffice.Conversions.Closing231;
    using LendersOffice.Conversions.ComplianceEase;
    using LendersOffice.Conversions.DocuTech;
    using LendersOffice.Conversions.Drive;
    using LendersOffice.Conversions.LoanProspector;
    using LendersOffice.DU;
    using LendersOffice.Integration.LoanProductAdvisor;
    using LendersOffice.Integration.MortgageInsurance;
    using LendersOffice.Integration.TotalScorecard;
    using LendersOffice.Integration.TotalScorecard.SOAPMISMO;
    using LendersOffice.ObjLib.Conversions.LoansPQ.Exporter;
    using LendersOffice.ObjLib.FHAConnection;
    using LendersOffice.TOTALScorecard.MISMO;
    using LendersOfficeApp.Integration.DataTrac;
    using LendersOfficeApp.newlos.Underwriting;
    using TotalScoreCard;
    using LendersOffice.Security;
    using LendersOffice.Conversions.Mismo3.Version4;

    public partial class TestViewXisResponse : BaseLoanPage
	{
        protected void PageInit(object sender, System.EventArgs e)
        {
            if (Tools.IsNonAuthorProduction)
            {
                LoanOverwriteToolLink.InnerText = "Use LOAuth to export/import loan.";
                LoanOverwriteToolLink.Disabled = true;
            }

            MIUtil.Bind_MIProvider(MIVendor, MortgageInsuranceVendorBrokerSettings.ListActiveVendorByBrokerId(this.BrokerID), MortgageInsuranceVendorConfig.ListActiveVendors());
        }        

		protected void PageLoad(object sender, System.EventArgs e)
		{
            string reasonUserCantViewTestPage;
            if (!BrokerUser.CanUserViewLoanEditorTestPage(out reasonUserCantViewTestPage))
            {
                throw new DataAccess.CBaseException(
                    LendersOffice.Common.ErrorMessages.GenericAccessDenied,
                    reasonUserCantViewTestPage);
            }

            string type = RequestHelper.GetSafeQueryString("type");
            string name = RequestHelper.GetSafeQueryString("name");
            switch (type) 
            {
                case "DU_UNDERWRITE":
                    WriteDuUnderwrite(name);
                    break;
                case "CASEFILE_EXPORT":
                    WriteCasefileExport(name);
                    break;
                case "CASEFILE_IMPORT":
                    WriteCasefileImport(name);
                    break;
                case "DWEB_CASEFILEIMPORT":
                    WriteDwebCasefileImport(name);
                    break;
                case "DWEB_CASEFILESTATUS":
                    WriteDwebCasefileStatus(name);
                    break;
                case "FREDDIE":
                    if (name == "XML")
                    {
                        WriteFreddie();
                    }
                    else if (name == "HTML")
                    {
                        WriteFreddieHtml();
                    }
                    break;
                case "DOCMAGIC" :
                    WriteDocMagic();
                    break;
                case "DOCUTECH":
                    WriteDocuTech();
                    break;
                case "COMPLIANCEEASE":
                    WriteComplianceEase();
                    break;
                case "ENCOMPASS360BUY":
                    WriteEncompass360(E_Encompass360RequestT.BuysideLock);
                    break;
                case "ENCOMPASS360SELL":
                    WriteEncompass360(E_Encompass360RequestT.SellsideLock);
                    break;
                case "ENCOMPASS360REGISTER":
                    WriteEncompass360(E_Encompass360RequestT.UnderwriteOnly);
                    break;
                case "DATATRACEXPORT":
                    DataTracExport();
                    break;
                case "DRIVE":
                    WriteXml(Encoding.ASCII.GetString(DriveServer.SerializeDriveRequestToBytes(DriveServer.CreateRequest("testUserName", "testPassword", this.LoanID))));
                    break;
                case "LPA_MISMO":
                    WriteLpa();
                    break;
                case "MI_MISMO231":
                    MortgageInsuranceMismo231Export(name == "Quote", new Guid(RequestHelper.GetSafeQueryString("VendorID")));
                    break;
                case "MISMO231":
                    Mismo231Export();
                    break;
                case "MISMO26":
                    MismoClosing26Export();
                    break;
                case "MISMO33":
                    this.Mismo3Export(includeExtensions: false, use34Export: false);
                    break;
                case "MISMO33EXT":
                    this.Mismo3Export(includeExtensions: true, use34Export: false);
                    break;
                case "MISMO34":
                    this.Mismo3Export(includeExtensions: false, use34Export: true);
                    break;
                case "MISMO34EXT":
                    this.Mismo3Export(includeExtensions: true, use34Export: true);
                    break;
                case "LOXML":
                    LOXmlFormat();
                    break;
                case "lpq":
                    CLFExportForLPQ();
                    break;
                case "FHA_CONNECTION":
                    WriteFhaConnection(name);
                    break;
                case "LoanFileExport":
                    //LoanFileXmlExport();
                    break;
                case "CertificateXmlData":
                    GenerateCertificateXmlData();
                    break;
                case "PmlLoanSummaryXmlData":
                    GeneratePmlLoanSummaryXmlData();
                    break;
                case "TotalScorecard":
                    GenerateTotalScorecard();
                    break;
                case "ARCHIVE_COMPARE":
                    CompareArchiveToCurrentData();
                    break;
                case "sClosingCostSetJsonContent":
                    WritesClosingCostSetJsonContent();
                    break;

            }
            Array values = Enum.GetValues(typeof(FormatTarget));
            string[] names = Enum.GetNames(typeof(FormatTarget));
            for (int i = 0; i < values.Length; ++i)
            {
                m_formatTargetForLOXml.Items.Add(new ListItem(names[i], values.GetValue(i).ToString()));
            }
            m_formatTargetForLOXml.SelectedValue = FormatTarget.MismoClosing.ToString();

            Array vendorVals = Enum.GetValues(typeof(E_DocumentVendor));
            string[] vendorNames = Enum.GetNames(typeof(E_DocumentVendor));
            for (int i = 0; i < vendorVals.Length; ++i)
            {
                mismo3Vendor.Items.Add(new ListItem(vendorNames[i], vendorVals.GetValue(i).ToString()));
            }
            
            mismo3Vendor.SelectedValue = E_DocumentVendor.UnknownOtherNone.ToString();
		}

        /// <summary>
        /// Updates the cache table for the loan file.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        protected void UpdateCacheButton_Click(object sender, EventArgs e)
        {
            var updatedCache = Tools.UpdateCacheTable(this.LoanID, dirtyFields: null, sendEmailOnFailure: false);

            this.ClientScript.RegisterStartupScript(
                typeof(TestViewXisResponse), 
                "CacheUpdateAlert", 
                "alert('Update " + (updatedCache ? "successful" : "failed") + ".');", 
                addScriptTags: true);
        }

        /// <summary>
        /// Generates XML for a Mortgage Insurance request. 
        /// Most of this is pieced together from a combination of <see cref="LendersOfficeApp.newlos.Services.OrderMIPolicy.Page_Load(object, EventArgs)"/> for loading, 
        /// <see cref="LendersOfficeApp.newlos.Services.OrderMIPolicyService.CreateQuoteOrder()"/> for quote order creation, and 
        /// <see cref="LendersOfficeApp.newlos.Services.OrderMIPolicyService.CreatePolicyOrder(bool)"/> for policy order creation. 
        /// It will likely break or become inaccurate if those methods are changed without also including a similar change in this method.
        /// The logic of these methods should be merged ASAP (OPM Case 449477).
        /// </summary>
        /// <param name="quote">Whether or not the MI request is for a quote (true) or a policy (false).</param>
        private void MortgageInsuranceMismo231Export(bool quote, Guid vendorId)
        {
            CPageData data = new CPageData(LoanID, new string[] { "sBranchId", "sProdConvMIOptionT", "sUfmipIsRefundableOnProRataBasis", "sIsMipPrepaid", "sFfUfMipIsBeingFinanced", "sMiLenderPaidCoverage" });
            data.InitLoad();

            Guid BranchId = data.sBranchId;
            Mismo231.MI.MI_MIPremiumRefundableTypeEnumerated premiumRefundability;
            if (data.sProdConvMIOptionT != E_sProdConvMIOptionT.LendPaidSinglePrem)
            {
                premiumRefundability = data.sUfmipIsRefundableOnProRataBasis ? Mismo231.MI.MI_MIPremiumRefundableTypeEnumerated.Refundable : Mismo231.MI.MI_MIPremiumRefundableTypeEnumerated.NotRefundable;
            }
            else
            {
                premiumRefundability = Mismo231.MI.MI_MIPremiumRefundableTypeEnumerated.NotRefundable;
            }

            string policyNumber = MortgageInsuranceVendorBranchSettings.ListCredentialsByBranchId(BrokerID, BranchId).FirstOrDefault(vendor => vendor.VendorId == vendorId)?.PolicyId;

            Mismo231.MI.MI_MIRenewalCalculationTypeEnumerated renewalOption = data.sProdConvMIOptionT == E_sProdConvMIOptionT.BorrPaidMonPrem || data.sProdConvMIOptionT == E_sProdConvMIOptionT.BorrPaidSplitPrem 
                ? Mismo231.MI.MI_MIRenewalCalculationTypeEnumerated.Constant 
                : Mismo231.MI.MI_MIRenewalCalculationTypeEnumerated.NoRenewals;

            Mismo231.MI.MI_MIInitialPremiumAtClosingTypeEnumerated premiumAtClosing;
            if (data.sProdConvMIOptionT == E_sProdConvMIOptionT.BorrPaidMonPrem)
            {
                premiumAtClosing = data.sIsMipPrepaid ? Mismo231.MI.MI_MIInitialPremiumAtClosingTypeEnumerated.Prepaid : Mismo231.MI.MI_MIInitialPremiumAtClosingTypeEnumerated.Deferred;
            }
            else
            {
                premiumAtClosing = Mismo231.MI.MI_MIInitialPremiumAtClosingTypeEnumerated.Prepaid;
            }

            MIOrderInfo order = new MIOrderInfo(
                LoanID,
                ApplicationID,
                vendorId,
                data.sProdConvMIOptionT,
                MISplitPremiumOption.DefaultMismoValue,
                premiumRefundability,
                data.sFfUfMipIsBeingFinanced,
                BrokerID,
                UserID,
                policyNumber,
                data.sMiLenderPaidCoverage,
                renewalOption,
                premiumAtClosing,
                true,
                quote,
                "00-Dummy-Quote-Number-00",
                string.Empty,
                DelegationType.Blank,
                E_MiRequestType.User
                );

            MIRequestProvider provider = new MIRequestProvider(LoanID, PrincipalFactory.CurrentPrincipal);
            order = provider.CreateMortgageInsuranceRequest(order, new MortgageInsuranceVendorBrokerSettings(vendorId, BrokerID));

            WriteXml(order.SerializeRequest());
        }

        private void CompareArchiveToCurrentData()
        {
            CPageData data = new CPageData(LoanID, new string[] { "sfExtractStaticGFEArchive", "sLastDisclosedGFEArchiveD", "sLastDisclosedGFEArchiveD" });
            data.InitLoad();
            IGFEArchive current = data.ExtractStaticGFEArchive();
            IGFEArchive archive = data.LastDisclosedGFEArchive;

            if (archive != null)
            {
                Type type = typeof(IGFEArchive);



                HtmlTable table = new HtmlTable();
                HtmlTableRow row = new HtmlTableRow();
                row.Cells.Add(new HtmlTableCell("th") { InnerText = "Field" });
                row.Cells.Add(new HtmlTableCell("th") { InnerText = "Current" });
                row.Cells.Add(new HtmlTableCell("th") { InnerText = "Archive" });
                row.Cells.Add(new HtmlTableCell("th") { InnerText = "Matches" });
                table.Rows.Add(row);

                foreach (System.Reflection.PropertyInfo pi in type.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
                {
                    row = new HtmlTableRow();
                    table.Rows.Add(row);

                    object selfValue = type.GetProperty(pi.Name).GetValue(current, null);
                    object toValue = type.GetProperty(pi.Name).GetValue(archive, null);

                    string selfData = selfValue == null ? "-null-" : selfValue.ToString();
                    string toData = toValue == null ? "-null-" : toValue.ToString();

                    row.Cells.Add(new HtmlTableCell() { InnerText = pi.Name });
                    row.Cells.Add(new HtmlTableCell() { InnerText = selfData });
                    row.Cells.Add(new HtmlTableCell() { InnerText = toData });
                    row.Cells.Add(new HtmlTableCell() { InnerText = selfData.Equals(toData).ToString() });
                }

                HtmlTextWriter writer = new HtmlTextWriter(Response.Output);
                Label l = new Label();
                l.Text = String.Concat("Using Archive : ", data.sLastDisclosedGFEArchiveD_rep);
                Response.Clear();
                Response.ContentType = "text/html";
                l.RenderControl(writer);
                table.RenderControl(writer);
                Response.End();
            }

            else
            {
                Response.Clear();
                Response.ContentType = "text/html";
                Response.Write("NO ARCHIVE");
                Response.End();
            }

        }


        private void GeneratePmlLoanSummaryXmlData()
        {
            using (MemoryStream stream = new MemoryStream(10000))
            {
                XmlWriterSettings writerSettings = new XmlWriterSettings();
                writerSettings.Encoding = new UTF8Encoding(false); // Skip BOM
                writerSettings.OmitXmlDeclaration = true;

                XmlWriter writer = XmlWriter.Create(stream, writerSettings);
                PmlLoanSummaryXmlData.Generate(writer, LoanID);

                writer.Flush();
                string xml = System.Text.Encoding.UTF8.GetString(stream.ToArray());
                WriteXml(xml);
            }
        }
        private void GenerateTotalScorecard()
        {
            CFHATotalAuditData dataLoan = new CFHATotalAuditData(LoanID);
            dataLoan.InitLoad();
            TotalScorecardRequest totalScorecardRequest = TotalScorecardRequestBase.GenerateTotalScorecardRequest(dataLoan);
            string xml;
            using (MemoryStream stream = new MemoryStream())
            {
                if (LendersOffice.Constants.ConstStage.TotalScorecardSOAPMISMOEnabled)
                {
                    XmlSerializer soapRequestSerializer = new XmlSerializer(typeof(process));
                    process soapRequest = TotalScorecardRequestGenerator.ConvertFromRestRequest(totalScorecardRequest, LoanID);
                    soapRequestSerializer.Serialize(stream, soapRequest);
                }
                else
                {
                    XmlWriterSettings writerSettings = new XmlWriterSettings();
                    writerSettings.Encoding = new UTF8Encoding(false); // Skip BOM
                    writerSettings.OmitXmlDeclaration = true;

                    using (XmlWriter writer = XmlWriter.Create(stream, writerSettings))
                    {
                        totalScorecardRequest.WriteXml(writer);
                    }
                }

                xml = Encoding.UTF8.GetString(stream.ToArray());
            }
            WriteXml(xml);
        }

        private void WritesClosingCostSetJsonContent()
        {
            CPageData dataLoan = new CPageData(LoanID, new string[] {"sClosingCostSet","sBrokerLockAdjustXmlContent"});
            dataLoan.InitLoad();

            WriteJson(dataLoan.sClosingCostSet.ToJson());
        }
        private void GenerateCertificateXmlData()
        {
            CPageData dataLoan = new CPageData(LoanID, new[] { "sPmlCertXmlContent" });
            dataLoan.InitLoad();

            if (dataLoan.sPmlCertXmlContent.Value == "")
            {
                WriteNoContent();
            }
            else
            {
                WriteXml(dataLoan.sPmlCertXmlContent.Value);
            }
        }


        private void CLFExportForLPQ()
        {
            CLFExporter x = new CLFExporter();
            WriteXml(x.Export(LoanID));
        }

        private void MismoClosing26Export()
        {
            string xml = null;
            string vendorName = RequestHelper.GetSafeQueryString("vendorName");
            bool isClosingPackage = RequestHelper.GetBool("closingPackage");
            bool useTemporaryArchive = RequestHelper.GetBool("temporaryArchive");
            List<string> feeDescrepancies;

            try
            {
                var tridDocPackage = new LendersOffice.Integration.DocumentVendor.VendorConfig.DocumentPackage(
                "Test TRID Document Package",
                isClosingPackage ? LendersOffice.Integration.DocumentVendor.VendorConfig.PackageType.Closing : LendersOffice.Integration.DocumentVendor.VendorConfig.PackageType.Initial,
                hasGFE: false,
                hasTIL: false,
                hasLoanEstimate: !isClosingPackage,
                hasClosingDisclosure: isClosingPackage,
                triggersPtmBilling: false);

                xml = LendersOffice.Conversions.GenericMismoClosing26.GenericMismoClosing26Exporter.ExportCustom(LoanID, vendorName, tridDocPackage, out feeDescrepancies, PrincipalFactory.CurrentPrincipal, useTemporaryArchive);
            }
            catch (FeeDiscrepancyException e)
            {
                Response.Clear();
                WriteHtml(e.UserMessage);
            }
            WriteXml(xml);
        }
        private void LOXmlFormat()
        {
            FormatTarget formatTarget = (FormatTarget)Enum.Parse(typeof(FormatTarget), RequestHelper.GetSafeQueryString("formatTarget"));
            bool bypassFieldSecurity = true;
            string xml = LOFormatExporter.Export(LoanID, LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal,
                LendersOffice.Integration.DocumentVendor.VendorConfig.LOXmlExportFields,
                formatTarget, bypassFieldSecurity);
            WriteXml(xml);
        }
        private void Mismo231Export()
        {
            Closing231Exporter exporter = new Closing231Exporter(LoanID, "");
            WriteXml(exporter.Export());

        }

        /// <summary>
        /// Write a MISMO 3.3.x message to the HTTP response output stream.
        /// </summary>
        /// <param name="includeExtensions">True to include LendingQB-designed extensions. False to omit.</param>
        /// <param name="use34Export">Whether the 34 export should be used.</param>
        private void Mismo3Export(bool includeExtensions, bool use34Export)
        {
            E_DocumentVendor vendor = (E_DocumentVendor)Enum.Parse(typeof(E_DocumentVendor), RequestHelper.GetSafeQueryString("mismo3Vendor"));
            bool isClosingPackage = RequestHelper.GetBool("closingpackage");
            bool useTemporaryArchive = RequestHelper.GetBool("temporaryArchive");
            var tridDocPackage = new LendersOffice.Integration.DocumentVendor.VendorConfig.DocumentPackage(
                "Test TRID Document Package",
                isClosingPackage ? LendersOffice.Integration.DocumentVendor.VendorConfig.PackageType.Closing : LendersOffice.Integration.DocumentVendor.VendorConfig.PackageType.Initial,
                hasGFE: false,
                hasTIL: false,
                hasLoanEstimate: !isClosingPackage,
                hasClosingDisclosure: isClosingPackage,
                triggersPtmBilling: false);

            string xml = string.Empty;
            if (!use34Export)
            {
                xml = LendersOffice.Conversions.Mismo3.Version3.Mismo33RequestProvider.SerializeMessage(
                LoanID,
                vendorAccountID: null,
                transactionID: Guid.NewGuid().ToString(),
                docPackage: tridDocPackage,
                includeIntegratedDisclosureExtension: includeExtensions,
                principal: PrincipalFactory.CurrentPrincipal,
                vendor: vendor,
                useTemporaryArchive: useTemporaryArchive,
                useMismo3DefaultNamespace: vendor == E_DocumentVendor.DocuTech);
            }
            else
            {
                Mismo34ExporterOptions options = new Mismo34ExporterOptions()
                {
                    LoanId = LoanID,
                    TransactionId = Guid.NewGuid().ToString(),
                    DocumentPackage = tridDocPackage,
                    IncludeIntegratedDisclosureExtension = includeExtensions,
                    Principal = PrincipalFactory.CurrentPrincipal,
                    DocumentVendor = vendor,
                    UseTemporaryArchive = useTemporaryArchive,
                    UseMismo3DefaultNamespace = vendor == E_DocumentVendor.DocuTech
                };

                var exporter = new Mismo34Exporter(options);
                xml = exporter.SerializePayload();
            }

            this.WriteXml(xml.TrimWhitespaceAndBOM());
        }

        private void DataTracExport()
        {
            DataTracExporter dtExporter = new DataTracExporter(LoanID, Broker.BrokerID, "Test", "asd", "dt", false/*updateExisting*/, DataTracExchange.E_DataTracOriginatorType.R);
            WriteXml(dtExporter.ExportXmlData());
        }
        private void WriteEncompass360(E_Encompass360RequestT eRequestType)
        {
            EncompassResponse encompassResponse = new EncompassResponse();
            string xml = encompassResponse.Export(LoanID, eRequestType);
            xml = xml.Replace("encoding=\"utf-16\"", "");

            WriteXml(xml);

        }
        private void WriteComplianceEase()
        {
            ComplianceEaseExporter exporter = new ComplianceEaseExporter(LoanID, ComplianceEase.E_TransmittalDataComplianceAuditType.PreClose);
            WriteXml(exporter.Export());

        }
        private void WriteDocuTech()
        {
            var packageType = DocuTech.E_DocumentsPackageType.InitialDisclosure;
            DocuTechExporter exporter = new DocuTechExporter(LoanID, packageType);
            try
            {
                WriteXml(exporter.Export("PRMLadmin", "docutech", packageType, PrincipalFactory.CurrentPrincipal));
            }
            catch (FeeDiscrepancyException e)
            {
                Response.Clear();
                WriteHtml(e.UserMessage);
            }
        }
        private void WriteDocMagic()
        {
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.BufferOutput = true;
            try
            {
                DsiDocumentServerRequest request = DocMagicMismoRequest.CreateSaveRequest("A", "B", "C", LoanID, null);
                using (StringWriter ms = new StringWriter())
                {
                    using (XmlWriter xw = XmlWriter.Create(ms, new XmlWriterSettings() { Indent = true, Encoding = new UTF8Encoding() }))
                    {
                        xw.WriteProcessingInstruction("xml", "version='1.0' encoding='UTF-8'");
                        request.WriteXml(xw);
                    }
                    Response.Write(ms.ToString());
                }
                Response.Flush();
                Response.Flush();
                Response.End();
            }
            catch (FeeDiscrepancyException e)
            {
                Response.Clear();
                WriteHtml(e.UserMessage);
            }
        }

        private void WriteLpa()
        {
            var exporter = LoanProspectorExporter.CreateExporterForSubmitRequest(isNonSeamless: true, loanId: LoanID, craInfo: LoanSubmitRequest.GetTemporaryCraInformation(LoanID), user: this.BrokerUser);
            string xmlForViewing = Regex.Replace(exporter.Export(), "(<!DOCTYPE [^>]+>)", "<!--$1-->");
            this.WriteXml(xmlForViewing);
        }
          
        private void WriteFreddie() 
        {
            CPageData dataLoan = new CPageData(LoanID, new[] { "sFreddieFeedbackResponseXml" });
            dataLoan.InitLoad();

            if (dataLoan.sFreddieFeedbackResponseXml.Value == "") 
            {
                WriteNoContent();
            } 
            else 
            {
                WriteXml(dataLoan.sFreddieFeedbackResponseXml.Value);
            }
        }

        private void WriteFreddieHtml()
        {
            CPageData dataLoan = new CPageData(LoanID, new[] { "sFreddieFeedbackHtml" });
            dataLoan.InitLoad();

            WriteHtml(dataLoan.sFreddieFeedbackHtml.Value);
        }
        private void WriteDuUnderwrite(string name) 
        {
            FnmaXisDuUnderwriteResponse xisResponse = new FnmaXisDuUnderwriteResponse(LoanID);
            if (!xisResponse.IsValid) 
            {
                WriteNoContent();
            } 
            else 
            {
                switch(name) 
                {
                    case "RAW":
                        WriteRawResponse(xisResponse);
                        break;
                    case "HTML_FINDINGS":
                        WriteHtml(xisResponse.FnmaFindingHtml);
                        break;
                    case "XML_FINDINGS":
                        //<!DOCTYPE CodifiedFindings SYSTEM \"CFFVersion1.1.dtd\">
                        WriteXml(xisResponse.FnmaFindingXml.Replace("<!DOCTYPE CodifiedFindings SYSTEM \"CFFVersion1.1.dtd\">", ""));
                        break;
                    default:
                        WriteNoContent();
                        break;
                }
            }
        }
        private void WriteCasefileExport(string name) 
        {
            FnmaXisCasefileExportResponse xisResponse = new FnmaXisCasefileExportResponse(LoanID);
            if (!xisResponse.IsValid) 
            {
                WriteNoContent();
            } 
            else 
            {
                switch(name) 
                {
                    case "RAW":
                        WriteRawResponse(xisResponse);
                        break;
                    case "XML_FINDINGS":
                        WriteXml(xisResponse.FnmaFindingsXml.Replace("<!DOCTYPE CodifiedFindings SYSTEM \"CFFVersion1.1.dtd\">", ""));
                        break;
                        
                    default:
                        WriteNoContent();
                        break;
                }

            }
        }
        private void WriteDwebCasefileStatus(string name) 
        {
            FnmaDwebCasefileStatusResponse xisResponse = new FnmaDwebCasefileStatusResponse(LoanID);
            if (!xisResponse.IsValid) 
            {
                WriteNoContent();
            } 
            else 
            {
                switch(name) 
                {
                    case "RAW":
                        WriteRawResponse(xisResponse);
                        break;
                    default:
                        WriteNoContent();
                        break;
                }

            }
        }
        private void WriteDwebCasefileImport(string name) 
        {
            FnmaDwebCasefileImportResponse xisResponse = new FnmaDwebCasefileImportResponse(LoanID);
            if (!xisResponse.IsValid) 
            {
                WriteNoContent();
            } 
            else 
            {
                switch(name) 
                {
                    case "RAW":
                        WriteRawResponse(xisResponse);
                        break;
                    default:
                        WriteNoContent();
                        break;
                }

            }
        }
        private void WriteCasefileImport(string name) 
        {
            FnmaXisCasefileImportResponse xisResponse = new FnmaXisCasefileImportResponse(LoanID);
            if (!xisResponse.IsValid) 
            {
                WriteNoContent();
            } 
            else 
            {
                switch(name) 
                {
                    case "RAW":
                        WriteRawResponse(xisResponse);
                        break;
                    default:
                        WriteNoContent();
                        break;
                }

            }
        }

        private void WriteFhaConnection(string name)
        {
            AbstractFHAConnectionRequest request;
            switch (name)
            {
                case "CASE_ASSIGNMENT_ASSIGN":
                    request = new CaseNumberAssignmentRequest(LoanID, false);
                    break;
                case "CASE_ASSIGNMENT_UPDATE":
                    request = new CaseNumberAssignmentRequest(LoanID, true);
                    break;
                case "HOLDS_TRACKING":
                    request = new HoldsTrackingRequest(LoanID);
                    break;
                case "CASE_QUERY":
                    request = new CaseQueryRequest(LoanID);
                    break;
                case "CAIVRS_AUTHORIZATION":
                    request = new CAIVRSAuthorizationRequest(LoanID);
                    break;
                default:
                    WriteNoContent();
                    return;
            }
            
            WriteXml(request.ToString());
        }


        private void WriteNoContent() 
        {
            Response.ContentType = "text/html";
            Response.Write("COULD NOT FIND RESPONSE");
            Response.Flush();
            Response.End();

        }

        private void WriteJson(string json)
        {
            Response.ContentType = "text/html";
            Response.Write("<html><body><pre>");
            Response.Write(SerializationHelper.JsonBeautify(json));
            Response.Write("</pre></body></html>");
            Response.Flush();
            Response.End();

        }
        private void WriteRawResponse(AbstractFnmaXisResponse xisResponse) 
        {
            Response.ContentType = "text/plain";
            Response.AddHeader("Content-Disposition", "attachment; filename=\"raw.txt\"");
            Response.Write(xisResponse.RawData);
            Response.Flush();
            Response.End();
        }
        private void WriteHtml(string html) 
        {
            Response.ContentType = "text/html";
            Response.Write(html);
            Response.Flush();
            Response.End();

        }
        private void WriteXml(string xml) 
        {
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.BufferOutput = true;

            Response.Write(xml);

            Response.Flush();
            Response.End();
        }
        
        private void ExportXml(string filename)
        {
            Response.ContentType = "application/octet-stream";
            Response.AppendHeader("Content-Disposition", $"attachment; filename=\"{filename}\"");
            Response.TransmitFile(filename);
            Response.End();
        }

        private void WriteTxt(string filename)
        {
            Response.ContentType = "application/octet-stream";
            Response.AppendHeader("Content-Disposition", $"attachment; filename=\"{filename}\"");
            Response.TransmitFile(filename);
            Response.End();
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
		}
		#endregion
	}
}
