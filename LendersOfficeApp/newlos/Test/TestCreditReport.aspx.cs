using System;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.CreditReport;
using LendersOffice.Integration.TotalScorecard;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Security;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos.Test
{

    public partial class TestCreditReport : LendersOfficeApp.newlos.BaseLoanPage
	{
		private Hashtable m_hash = new Hashtable();
		protected System.Web.UI.WebControls.Label CreditRatingCodeLabel;
		private bool m_invokeMethod = true;

		private CPageData m_dataLoan = null;
		private CAppData m_dataApp = null;
		private CreditReportProxy m_credProxy = null; 
	

		private CreditReportProxy  CreditProxy 
		{
			get 
			{
                if (m_credProxy == null)
                {
                    m_credProxy = m_dataApp.CreditReportData.Value as CreditReportProxy;

                    if (m_credProxy != null)
                    {
                        m_credProxy.IsDebug = true;
                        m_dataApp.SetLoanDataOn(m_credProxy); // 11/17/2009 dd - Set this so IsSubjectPropertyMortgage is calculate correctly
                    }
				}

				return m_credProxy;
			}
		}


		private static Hashtable m_hashDebugTagListXML = new Hashtable();	// nw - used for OPM 4911

		protected void PageInit( object sender, System.EventArgs e )
		{
            if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.CanModifyLoanPrograms))
            {
                // OPM 464934. Only internal users are permitted access.
                //
                // Ideally, we would want both internal "PMLXXXX" SAE users 
                // and internal "I" users who have used the become+ feature 
                // from loadmin. This is because a some keywords will have a 
                // change in their evaluation based on the user (mostly due 
                // to user-level feature subscription affecting a core 
                // calculation). However, because the become+ feature is not
                // currently usable, testers will have to modify their user 
                // directly if they encounter such a scenario.
                throw new AccessDenied("User does not have permission to view page.");
            }

            bindConditionKeywords();
            m_ddlBorrowerCreditMode.Items.Add(new ListItem("Both", "Both"));
            m_ddlBorrowerCreditMode.Items.Add(new ListItem("Borrower", "Borrower"));
            m_ddlBorrowerCreditMode.Items.Add(new ListItem("CoBorrower", "CoBorrower"));
		}

        protected void PageLoad(object sender, System.EventArgs e)
        {
            try 
            {
                m_dataLoan = new CPriceEngineData(LoanID);
                m_dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                m_dataLoan.InitLoad();
                m_dataApp = m_dataLoan.GetAppData( 0 );

                Button2.Enabled = false;

	            #if (DEBUG)
		            Button2.Enabled = true;
		        #endif

				if (RequestHelper.GetSafeQueryString("cmd") == "xml")
					ViewCreditXML();
				else if (RequestHelper.GetSafeQueryString("cmd") == "liabilities") 
				{
					ViewLiabilities();
				}
				else if (RequestHelper.GetSafeQueryString("cmd") == "ind_lia") 
				{
					ViewIndividualLiability();
				}
				else if (RequestHelper.GetSafeQueryString("cmd") == "ind_public") 
				{
					ViewIndividualPublicRecord();
				}
				else if (RequestHelper.GetSafeQueryString("cmd") == "publicrecords") 
				{
					ViewPublicRecords();
				}
				else if (RequestHelper.GetSafeQueryString("cmd") == "debug_tag_list")
				{
					ViewDebugTagList();
				}
				else 
				{
					// when we open TestCreditReport.aspx for a new loan file or when we upload a new credit report
					// in localhost, we need to use a new hash table for debug tag list
					m_hashDebugTagListXML.Clear();

					// Put user code to initialize the page here
					if (!Page.IsPostBack) 
					{
						m_btnCalculate_Click(null, null);
					}
				}
            } 
            catch (Exception exc) 
            {
                ErrorMsg.Text = exc.ToString();
            }

        }


        private void ViewIndividualLiability() 
        {
            if (null == CreditProxy) 
            {
                return;
            }

            Response.ContentType = "text/xml";
            XmlDocument doc = new XmlDocument();
            string xml = CreditProxy.GetLiabilityXml(RequestHelper.GetSafeQueryString("liaid"));

            if (null == xml || "" == xml)
                return;


            doc.LoadXml(xml);

            doc.Save(Response.OutputStream);
            Response.Flush();
            Response.End();
        }
        private void ViewIndividualPublicRecord() 
        {
            if (null == CreditProxy) 
            {
                return;
            }

            Response.ContentType = "text/xml";
            XmlDocument doc = new XmlDocument();
            string xml = CreditProxy.GetPublicRecordXml(RequestHelper.GetSafeQueryString("publicid"));

            if (null == xml || "" == xml)
                return;


            doc.LoadXml(xml);

            doc.Save(Response.OutputStream);
            Response.Flush();
            Response.End();
        }
        private void ViewPublicRecords() 
        {
            if (null == CreditProxy) 
            {
                return;
            }

            StringBuilder sb = new StringBuilder();

            sb.Append("<h2>Public Records Count = " + CreditProxy.AllPublicRecords.Count + "</h2>");

            foreach (AbstractCreditPublicRecord o in CreditProxy.AllPublicRecords) 
            {
                string[][] args = 
                              {
                                  new string[] {"ID", o.ID},
                                  new string[] {"Type", o.Type.ToString()},
                                  new string[] {"LastEffectiveDate", o.LastEffectiveDate.ToShortDateString()},

                                  new string[] {"ReportedDate", o.ReportedDate.ToShortDateString()},
                                  new string[] {"BkFileDate", o.BkFileDate.ToShortDateString()},
                                  new string[] {"DispositionDate", o.DispositionDate.ToShortDateString()},

                                  new string[] {"DispositionType", o.DispositionType.ToString()},
                                  new string[] {"BankruptcyLiabilitiesAmount", o.BankruptcyLiabilitiesAmount.ToString()},
                                  new string[] {"IsDischarged", o.IsDischarged.ToString()},
                                  new string[] {"IsBankruptcyFiled", o.IsBankruptcyFiled.ToString()},
                                  new string[] {"IsCollection", o.IsCollection.ToString()},
                                  new string[] {"IsCollectionPaid", o.IsCollectionPaid.ToString()},
                                  new string[] {"IsCollectionUnpaid", o.IsCollectionUnpaid.ToString()},
                                  new string[] {"IsJudgment", o.IsJudgment.ToString()},
                                  new string[] {"IsJudgmentPaid", o.IsJudgmentPaid.ToString()},
                                  new string[] {"IsJudgmentUnpaid", o.IsJudgmentUnpaid.ToString()},
                                  new string[] {"IsLien", o.IsLien.ToString()},
                                  new string[] {"IsLienUnpaid", o.IsLienUnpaid.ToString()},
                                  new string[] {"IsNontaxLien", o.IsNontaxLien.ToString()},
                                  new string[] {"IsNontaxLienPaid", o.IsNontaxLienPaid.ToString()},
                                  new string[] {"IsNontaxLienUnpaid", o.IsNontaxLienUnpaid.ToString()},
                                  new string[] {"IsTaxLien", o.IsTaxLien.ToString()},
                                  new string[] {"IsTaxLienUnpaid", o.IsTaxLienUnpaid.ToString()}

                              };
                sb.Append("<hr>");

                sb.Append(@"<table cellspacing=2 border=1>");
                int count = args.Length;
                int columnsPerRow = 3;
                int j = 0;
                for (int i = 0; i < count; i++) 
                {
                    if (j == 0) 
                    {
                        sb.Append("<tr>");
                    }

                    string[] items = args[i];
                    sb.AppendFormat("<td>{0}</td><td class=FieldValue>{1}</td>", items[0], AspxTools.HtmlString(items[1]));

                    if (j == (columnsPerRow - 1)) 
                    {
                        sb.Append("</tr>");
                        j = -1;
                    }
                    j++;
                }

                sb.Append("<tr><td><input type=button onclick=f_viewPublicRecordXml(" + AspxTools.JsString(o.ID) + "); value='View Xml'></td></tr>");
                sb.Append("</table>");

            }
            MainHtml.Text = sb.ToString();

        }
        private void ViewLiabilities() 
        {
            if (null == CreditProxy) 
            {
                return;
            }

            StringBuilder sb = new StringBuilder();

            sb.Append("<h2>Liabilities Count = " + CreditProxy.AllLiabilities.Count + "</h2>");

            foreach (AbstractCreditLiability o in CreditProxy.AllLiabilities) 
            {
                string[][] args = 
                              {
                                  new string[] {"ID", o.ID},
                                  new string[] {"IsActive", o.IsActive.ToString()},
                                  new string[] {"IsBankruptcy", o.IsBankruptcy.ToString()},

                                  new string[] {"AccountIdentifier", o.AccountIdentifier},
                                  new string[] {"IsLate", o.IsLate.ToString()},
                                  new string[] {"IsChargeOff", o.IsChargeOff.ToString()},

                                  new string[] {"Creditor Name", o.CreditorName},
                                  new string[] {"IsCredit", o.IsCredit.ToString()},                                

                                  new string[] {"Account Opened Date", o.AccountOpenedDate.ToShortDateString()},
                                  new string[] {"IsPaid", o.IsPaid.ToString()},
                                  new string[] {"IsCollection", o.IsCollection.ToString()},

                                  new string[] {"Account Reported Date", o.AccountReportedDate.ToShortDateString()},
                                  new string[] {"Late30", o.Late30},
                                  new string[] {"IsForeclosure", o.IsForeclosure.ToString()},

                                  new string[] {"LastActivityDate", o.LastActivityDate.ToShortDateString()},
                                  new string[] {"Late60", o.Late60},

                                  new string[] {"IsSubjectPropertyMortgage", o.IsSubjectPropertyMortgage.ToString()},
                                  new string[] {"IsSubjectProperty1stMortgage", o.IsSubjectProperty1stMortgage.ToString()},

                                  new string[] {"Late90", o.Late90},
                                  new string[] {"IsRepos", o.IsRepos.ToString()},

                                  new string[] {"MonthsReviewedCount", o.MonthsReviewedCount.ToString()},
                                  new string[] {"Late120", o.Late120},
                                  new string[] {"IsDerogCurrently", o.IsDerogCurrently.ToString()},

                                  new string[] {"HasBeenDerog", o.HasBeenDerog.ToString()},
                                  new string[] {"Account End Derog Date", o.EndDerogDate.ToShortDateString()},
                                  new string[] {"IsCurrent(bIncludeBadHistory=true)", o.IsCurrent.ToString()},

                                  new string[] {"LiabilityType", o.LiabilityType.ToString()},
                                  new string[] {"MonthlyPaymentAmount", o.MonthlyPaymentAmount.ToString()},
                                  new string[] {"IsCurrent(regardless of past derog)", o.IsCurrent.ToString()},

                                  new string[] {"UnpaidBalanceAmount", o.UnpaidBalanceAmount.ToString()},
                                  new string[] {"PaymentPatternStartDate", o.PaymentPatternStartDate.ToShortDateString()},
                                  new string[] {"", ""},

                                  new string[] {"HighCreditAmount", o.HighCreditAmount.ToString()},
                                  new string[] {"IsAuthorizedUser", o.IsAuthorizedUser.ToString()},
                                  new string[] {"IsStudentLoansNotInRepayment", o.IsStudentLoansNotInRepayment.ToString()}

                              };
                sb.Append("<hr>");

                sb.Append(@"<table cellspacing=2 border=1>");
                int count = args.Length;
                int columnsPerRow = 3;
                int j = 0;
                for (int i = 0; i < count; i++) 
                {
                    if (j == 0) 
                    {
                        sb.Append("<tr>");
                    }

                    string[] items = args[i];
                    sb.AppendFormat("<td>{0}</td><td class=FieldValue>{1}</td>", items[0], AspxTools.HtmlString(items[1]));

                    if (j == (columnsPerRow - 1)) 
                    {
                        sb.Append("</tr>");
                        j = -1;
                    }
                    j++;
                }

                sb.AppendFormat("<tr><td colspan={0}>Late Dates</td></tr><tr><td colspan={0}><ul>", columnsPerRow * 2);
                foreach (CreditAdverseRating rating in o.LateOnlyCreditAdverseRatingList) 
                {
                    sb.Append("<li>" + AspxTools.HtmlString(rating));
                }
                sb.AppendFormat("</ul></td></tr><tr><td colspan={0}>All Adverse Rating List</td></tr><tr><td colspan={0}><ul>", columnsPerRow * 2);
                foreach (CreditAdverseRating rating in o.CreditAdverseRatingList) 
                {
                    sb.Append("<li>" + AspxTools.HtmlString(rating));
                }
                sb.Append("</ul></td></tr>");
                sb.Append("<tr><td><input type=button onclick=f_viewLiabilityXml(" + AspxTools.JsString(o.ID) + "); value='View Liablity Xml'></td></tr></table>");

            }
            MainHtml.Text = sb.ToString();

        }

		/// <summary>
		/// Removes Sensitive data from the MCL Response. 
		/// </summary>
		/// <param name="doc"></param>
		private void ScrubSensitiveDataFromMCLResponse(XmlDocument doc) 
		{
			XmlNode response = doc.SelectSingleNode("//OUTPUT/RESPONSE" );  
			if ( response == null ) { return;} 
			
			XmlNodeList formats = doc.SelectNodes("//OUTPUT/RESPONSE/OUTPUT_FORMAT"); 
			foreach ( XmlNode f in formats )  //deletes stuff like pdf and html we are only cleaning xml and mismo so the rest should be removed. 
			{
				if ( f.Attributes["format_type"].Value != "XML" && f.Attributes["format_type"].Value != "MISMO2_3_1"  ) 
				{
					f.ParentNode.RemoveChild( f ); 
				}
			}
			
			XmlNode xmlFormat = doc.SelectSingleNode("//OUTPUT/RESPONSE/OUTPUT_FORMAT[@format_type='XML']" );
			if ( xmlFormat != null ) 
			{
				XmlDocument xmlCreditReport = new XmlDocument(); 
				xmlCreditReport.LoadXml( xmlFormat.InnerText ); 
				
				XmlNode header = xmlCreditReport.SelectSingleNode("//CREDITDATA/HEADER"); 
				header.Attributes["instant_view_pw"].Value = ""; 
				
				XmlNode consumer = xmlCreditReport.SelectSingleNode("//CREDITDATA/CONSUMER"); 
				consumer.Attributes["firstname"].Value = "JOHN"; 
				consumer.Attributes["lastname"].Value = "DOE";
				consumer.Attributes["ssn"].Value = "100000000"; 
				consumer.Attributes["middlename"].Value = ""; 
				
				XmlNode address = xmlCreditReport.SelectSingleNode("//CREDITDATA/ADDRESS"); 
				address.Attributes["streetnumber"].Value = "123"; 
				address.Attributes["streetname" ].Value = "SEF"; 
				address.Attributes["aptnumber"  ].Value = "999"; 
				string xml = Regex.Replace( xmlCreditReport.OuterXml,"alias=\"[^\"]+?\"", "alias=\"Cleared\"") ; 
				xml = Regex.Replace( xml, "ssn=\"[^\"]+?\"", "ssn=\"100000000\"");

				XmlCDataSection cData  = doc.CreateCDataSection( xml  ) ; 
				xmlFormat.InnerText = ""; 
				xmlFormat.AppendChild( cData ); 
			}
			XmlNode mismoFormat = doc.SelectSingleNode("//OUTPUT/RESPONSE/OUTPUT_FORMAT[@format_type='MISMO2_3_1']"); 
			if ( mismoFormat != null ) 
			{ 
				XmlDocument mismoDoc = new XmlDocument(); 
				mismoDoc.LoadXml( mismoFormat.InnerText ); 
				XmlDocument cleanedMismo = ScrubSensitiveDataFromMismoReport( mismoDoc ); 
				XmlCDataSection cData = doc.CreateCDataSection( cleanedMismo.OuterXml ); 
				mismoFormat.InnerText = "" ;
				mismoFormat.AppendChild( cData ); 	
			} 
			
		}

		/// <summary>
		/// Deals with the Mismo response. Clearing sensitive data
		/// </summary>
		/// <param name="doc"></param>
		private void  ScrubSensitiveDataFromMismoResponse( XmlDocument doc ) 
		{
			XmlNode node = doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE") ;
			if ( node == null ) { return; }
			doc.LoadXml(ScrubSensitiveDataFromMismoReport(doc).OuterXml ); 
		}

		/// <summary>
		/// Removes name, ssn alias address from the mismo report. 
		/// </summary>
		/// <param name="doc"></param>
		/// <returns></returns>
		private XmlDocument ScrubSensitiveDataFromMismoReport( XmlDocument doc ) 
		{

			XmlNode residence = doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/BORROWER/_RESIDENCE");
            if (residence != null && residence.Attributes != null && residence.Attributes.GetNamedItem("_StreetAddress") != null)
            {
                residence.Attributes["_StreetAddress"].Value = "123 Main St";

            }
			XmlNodeList embeddedFiles = doc.SelectNodes("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/CREDIT_RESPONSE/EMBEDDED_FILE"); 
			foreach( XmlNode embeddedFile in embeddedFiles ) 
			{
				embeddedFile.ParentNode.RemoveChild(embeddedFile); 
			}

			string xml = doc.OuterXml; 

			xml = Regex.Replace(xml, "_FirstName=\"[^\"]+\"+?", "_FirstName=\"JOHN\"" );
			xml = Regex.Replace(xml, "_LastName=\"[^\"]+\"+?", "_LastName=\"DOE\"" );
			xml = Regex.Replace(xml, "_SSN=\"\\d{9}\"","_SSN=\"100000000\"" );
			xml = Regex.Replace(xml, "_UnparsedName=\"[^\"]+\"+?", "_UnparsedName=\"JOHN\"" );
			xml = Regex.Replace(xml, "alias=\"[^\"]+\"+?", "alias=\"Cleared\"" );
			XmlDocument cleanedMismo = new XmlDocument();
			cleanedMismo.LoadXml(xml);
			return cleanedMismo; 

		}
        private void ViewCreditXML() 
        {
            CPageData dataLoan = new CCreditReportViewData(LoanID);
            dataLoan.InitLoad();
            
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            ICreditReportView creditReportView = dataApp.CreditReportView.Value;

            if (null == creditReportView) 
            {
                return;
            }
            Response.ContentType = "text/xml";

            XmlDocument doc = new XmlDocument();
			doc.LoadXml(creditReportView.RawXml);
			
			ScrubSensitiveDataFromMCLResponse(doc); 
			ScrubSensitiveDataFromMismoResponse(doc);
			
            doc.Save(Response.OutputStream);
            Response.Flush();
            Response.End();

        }
		// 5/16/2006 nw - OPM 4911 - 2) Display XML for all items in a particular Debug TagItem List
		private void ViewDebugTagList()
		{
			Response.ContentType = "text/xml";
			XmlDocument doc = new XmlDocument();
			string xml = (m_hashDebugTagListXML[RequestHelper.GetSafeQueryString("methodname")]).ToString();

			if (null == xml || "" == xml)
				return;

			doc.LoadXml(xml);
			doc.Save(Response.OutputStream);
            Response.Flush();
            Response.End();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion

		private void  bindConditionKeywords() 
		{
			CCondition condition = CCondition.CreateTestCondition( "" );
			keyword_dd.DataSource = condition.ValidSymbols;
			keyword_dd.DataBind();
		}
        protected void m_btnGenerateTemporaryTotalScore_Click(object sender, EventArgs e)
        {
            
            CreditRiskEvaluation.GenerateTemporaryTotalScoreCertificate(LoanID);
        }
        protected void m_btnCalculate_Click(object sender, System.EventArgs e)
        {
				try
				{
                    m_dataApp.SetLoanDataOn(CreditProxy);
                    //CreditProxy.SetLoanData( m_dataApp  );
                    switch (m_ddlBorrowerCreditMode.SelectedValue.ToLower())
                    {
                        case "borrower":
                            CreditProxy.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
                            m_dataApp.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
                            break;
                        case "coborrower":
                            CreditProxy.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Coborrower;
                            m_dataApp.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Coborrower;
                            break;
                        default:
                            CreditProxy.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Both;
                            m_dataApp.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Both;
                            break;
                    }

					// 12/7/2006 nw - OPM 8459 - Accept Q-keywords values "QCLTV=80;QLTV=80;QLAMT=241000.00;QSCORE=700;"
					string sUpdateString = cond_ed.Text.ToUpper();
					if (q_keywords_tb.Text.Length > 0)
					{
						int index, indexStart, indexEnd;
						string keywordValue;

						index = q_keywords_tb.Text.IndexOf("QCLTV");
						if (index >= 0)
						{
							indexStart = index + "QCLTV=".Length;
							indexEnd = q_keywords_tb.Text.IndexOf(";", index);
							if (indexEnd == -1)
								indexEnd = q_keywords_tb.Text.Length;
							keywordValue = q_keywords_tb.Text.Substring(indexStart, indexEnd - indexStart);
							sUpdateString = sUpdateString.Replace("QCLTV", keywordValue);
						}

						index = q_keywords_tb.Text.IndexOf("QLTV");
						if (index >= 0)
						{
							indexStart = index + "QLTV=".Length;
							indexEnd = q_keywords_tb.Text.IndexOf(";", index);
							if (indexEnd == -1)
								indexEnd = q_keywords_tb.Text.Length;
							keywordValue = q_keywords_tb.Text.Substring(indexStart, indexEnd - indexStart);
							sUpdateString = sUpdateString.Replace("QLTV", keywordValue);
						}

						index = q_keywords_tb.Text.IndexOf("QLAMT");
						if (index >= 0)
						{
							indexStart = index + "QLAMT=".Length;
							indexEnd = q_keywords_tb.Text.IndexOf(";", index);
							if (indexEnd == -1)
								indexEnd = q_keywords_tb.Text.Length;
							keywordValue = q_keywords_tb.Text.Substring(indexStart, indexEnd - indexStart);
							sUpdateString = sUpdateString.Replace("QLAMT", keywordValue);
						}

						index = q_keywords_tb.Text.IndexOf("QSCORE");
						if (index >= 0)
						{
							indexStart = index + "QSCORE=".Length;
							indexEnd = q_keywords_tb.Text.IndexOf(";", index);
							if (indexEnd == -1)
								indexEnd = q_keywords_tb.Text.Length;
							keywordValue = q_keywords_tb.Text.Substring(indexStart, indexEnd - indexStart);
							sUpdateString = sUpdateString.Replace("QSCORE", keywordValue);
						}
					}

					XmlDocument xmlDocEval = new XmlDocument();
					XmlElement root = xmlDocEval.CreateElement( "PricePolicy" );

					CPricePolicy policy = CPricePolicy.CreateNewPricePolicy( BrokerID, Guid.Empty );
					CRule rule = policy.AddNewRule();

					// rule.Condition.UserExpression = cond_ed.Text;	// nw - OPM 8459
					rule.Condition.UserExpression = sUpdateString;

					
					if( rule.Condition.Evaluate( xmlDocEval, m_dataLoan.sSymbolTableForPriceRule ) )
						condRes_ed.Text = "Evaluated to TRUE";
					else
						condRes_ed.Text = "Evaluated to FALSE";
					condRes_ed.ForeColor = Color.Green;
				}
				catch( Exception ex )
				{
					condRes_ed.Text = "Condition syntax is not valid. ";
					condRes_ed.Text += ex.Message + "\r\nCallStack:" + ex.StackTrace;
					condRes_ed.ForeColor = Color.Red;
				}

             

                if (null == CreditProxy) 
                {
                    MainHtml.Text = "NO CREDIT REPORT.";
                    return;
                }

            try 
            {

                if (null != CreditProxy) 
                {
                    GenerateDebugList(CreditProxy);

                    GenerateHtml(CreditProxy);
                } 
            } 
            catch (Exception exc) 
            {
                ErrorMsg.Text = exc.ToString();
                m_invokeMethod = false;
                if (null != CreditProxy) 
                {
                    GenerateDebugList(CreditProxy as ICreditReport );

                    GenerateHtml(CreditProxy as ICreditReport);
                } 

            }
        }
        private void GenerateHtml(ICreditReport creditReport) 
        {
            MainHtml.Text = "";
            if (null != creditReport) 
            {
                bool isOptimisticDefault = ((CreditReportProxy) creditReport).IsOptimisticDefault;
                if (!isOptimisticDefault) 
                {
                    try 
                    {
                        if (null != creditReport.BorrowerInfo) 
                        {
                            MainHtml.Text = string.Format("<table><tr><td>Borrower Name</td><td>{0} {1}</td></tr>", AspxTools.HtmlString(creditReport.BorrowerInfo.FirstName), AspxTools.HtmlString(creditReport.BorrowerInfo.LastName));
                            MainHtml.Text += string.Format("<tr><td colspan=2>Borrower Credit Score: Equifax <b>{0}</b>, Experian <b>{1}</b>, TransUnion <b>{2}</b></td></tr>", AspxTools.HtmlString(creditReport.BorrowerScore.Equifax), AspxTools.HtmlString(creditReport.BorrowerScore.Experian), AspxTools.HtmlString(creditReport.BorrowerScore.TransUnion));
                        }
                    } 
                    catch {  } //  Ignore because CCreditReportWithOptimisticDefault will throw exception.  
                    try 
                    {
                        if (null != creditReport.CoborrowerInfo) 
                        {
                            MainHtml.Text += string.Format("<tr><td>Coborrower Name</td><td>{0} {1}</td></tr>", AspxTools.HtmlString(creditReport.CoborrowerInfo.FirstName), AspxTools.HtmlString(creditReport.CoborrowerInfo.LastName));
                            MainHtml.Text += string.Format("<tr><td colspan=2>CoBorrower Credit Score: Equifax <b>{0}</b>, Experian <b>{1}</b>, TransUnion <b>{2}</b></td></tr>", AspxTools.HtmlString(creditReport.CoborrowerScore.Equifax), AspxTools.HtmlString(creditReport.CoborrowerScore.Experian), AspxTools.HtmlString(creditReport.CoborrowerScore.TransUnion));
                        }
                    } 
                    catch {} //  Ignore because CCreditReportWithOptimisticDefault will throw exception.  

					MainHtml.Text += string.Format("<tr><td>Credit Rating Code Used: {0}</td></tr>", AspxTools.HtmlString(creditReport.CreditRatingCodeType));
                    MainHtml.Text += "</table><hr>";    
                }
                else 
                {
                    MainHtml.Text = "<h3>Manual Credit Report</h3>";
                }

            } 

            MainHtml.Text += "<table border=1 cellspacing=3>";
            MainHtml.Text += "<tr><td>Name</td><td>Arguments</td><td>Result</td><td>Execute Time</td></tr>";

            ArrayList keyList = new ArrayList(m_hash.Keys);
            keyList.Sort();
            foreach (string key in keyList) 
            {
                MainHtml.Text += "<tr><td colspan=4 align=center style='font-weight:bold;background-color:maroon;color:white'>" + AspxTools.HtmlString(key) + "</td></tr>";
                ArrayList list = (ArrayList) m_hash[key];
                list.Sort();
                foreach (string s in list) 
                    MainHtml.Text += s;
            }
            MainHtml.Text += "</table>";
        }
        private void GenerateDebugList(ICreditReport creditReport) 
        {
            bool isCreditReportProxy = creditReport is CreditReportProxy;
            
            MethodInfo[] methods = null;

            if (isCreditReportProxy)
                methods = typeof(LendersOffice.CreditReport.CreditReportProxy).GetMethods();
            else
                methods = typeof(LendersOffice.CreditReport.CCreditReportWithOptimisticDefault).GetMethods();



            foreach (MethodInfo m in methods) 
            {
                object[] attrs = m.GetCustomAttributes(typeof(LendersOffice.CreditReport.CreditDebugAttribute), true);
                if (attrs.Length == 0) continue;
                string category = ((CreditDebugAttribute) attrs[0]).Category;
                
                ParameterInfo[] parameters = m.GetParameters();
                object[] args = new object[parameters.Length];

                string line = "<tr><td>" + AspxTools.HtmlString(m.Name) + "&nbsp;&nbsp;[<a href=\"#\" onclick=\"return f_toggleDependend('_" + AspxTools.JsStringUnquoted(m.Name) + "');\">Click to see debug info</a>]</td><td>";
                int index = 0;
				string sCacheKey = m.Name + ":";	// nw - cache key should be methodName:parameter(s)
                foreach (ParameterInfo p in parameters) 
                {
                    string sValue = "";
                    string _name = m.Name + "_" + p.Name;

                    if (p.ParameterType == typeof(int)) 
                    {
                        int iValue = GetValue(_name, 24);
                        args[index] = iValue;
                        sValue = iValue.ToString();
                        line += string.Format("{0}:<input type=text name={1} value={2} style='width:50px'>&nbsp;&nbsp;", AspxTools.HtmlString(p.Name), AspxTools.HtmlAttribute(_name), AspxTools.HtmlAttribute(sValue));

                    } 
                    else if (p.ParameterType == typeof(string)) 
                    {
                        sValue = GetString(_name);
                        args[index] = sValue;
                        // For string parameters, use textarea.
                        line += string.Format("{0}:<textarea name={1} style='width:350px;height:140px'>{2}</textarea>&nbsp;&nbsp;", AspxTools.HtmlString(p.Name), AspxTools.HtmlAttribute(_name), AspxTools.HtmlString(sValue));

                    }
                    else 
                    {
                        throw new CBaseException( p.ParameterType.Name + " type is not supported yet.", p.ParameterType.Name + " type is not supported yet." );
                    }
                    index++;
					sCacheKey = sCacheKey + sValue.ToLower() + "&";
                }
				sCacheKey = sCacheKey.Substring(0, sCacheKey.Length - 1);

                long _t0 = DateTime.Now.Ticks;
                string result = "N/A";
                if (m_invokeMethod) 
                {
                    try 
                    {
                        result = m.Invoke(creditReport, args).ToString();
                    } 
                    catch (Exception exc) 
                    {
                        Tools.LogError("m_method=" + m.Name);
                        throw exc;
                    }
                }
                long _t1 = DateTime.Now.Ticks;
                line += "</td><td class='FieldLabel'>" + AspxTools.HtmlString(result) + "</td><td>"+ ((_t1 - _t0) / 10000L )+" ms.</td></tr>";

                if (isCreditReportProxy) 
                {
                    line += GenerateTagList((CreditReportProxy) creditReport, sCacheKey);
                }
                GetCategory(category).Add(line);
            }

            System.Reflection.PropertyInfo[] properties = null;
            if (isCreditReportProxy) 
                properties = typeof(LendersOffice.CreditReport.CreditReportProxy).GetProperties();
            else
                properties = typeof(LendersOffice.CreditReport.CCreditReportWithOptimisticDefault).GetProperties();

            foreach (System.Reflection.PropertyInfo p in properties) 
            {
                
                object[] attrs = p.GetCustomAttributes(typeof(LendersOffice.CreditReport.CreditDebugAttribute), true);
                if (attrs.Length == 0) continue;

                string category = ((CreditDebugAttribute) attrs[0]).Category;

                long _t0 = DateTime.Now.Ticks;
                string result = p.GetValue(creditReport, new object[0]).ToString();
                long _t1 = DateTime.Now.Ticks;

                string line = string.Format("<tr><td>{0}&nbsp;&nbsp;[<a href=\"#\" onclick=\"return f_toggleDependend('_{0}');\">Click to see debug info</a>]</td><td>&nbsp;</td><td class='FieldLabel'>{1}</td><td>{2} ms.</td></tr>", AspxTools.JsStringUnquoted(p.Name), AspxTools.HtmlString(result), ((_t1 - _t0) / 10000L ));

                if (isCreditReportProxy) 
                {
                    line += GenerateTagList((CreditReportProxy) creditReport, p.Name);
                }

                GetCategory(category).Add(line);
            }
        }

        private string GenerateTagList(CreditReportProxy creditReport, string sCacheKey) 
        {
            ArrayList list = creditReport.GetTagItems(sCacheKey);
            if (null == list || list.Count == 0)
                return "";

			string methodName;
			int methodNameIndex = sCacheKey.IndexOf(':');
			if (methodNameIndex > 0)
				methodName = sCacheKey.Substring(0, methodNameIndex);
			else
				methodName = sCacheKey;
			
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<tr><td style='padding-left:30px' colspan=4><span style='display:none' id={0}><ul>", AspxTools.HtmlAttribute("_" + methodName));
			sb.AppendFormat("<li><a href=\"#\" onclick=\"f_viewDebugTagListXml('{0}'); return false;\">View All</a></li>", AspxTools.JsStringUnquoted(methodName));
			StringBuilder sbViewAllXML = new StringBuilder();
			sbViewAllXML.AppendFormat("<{0}_DebugTagListXML>", methodName);
            foreach (string s in list) 
            {
                // sb.AppendFormat("<li>{0}</li>", s);

				// 5/15/2006 nw - OPM 4911 - 1) Make each debug tag item link to its XML data

				string sType;

				// parse s to figure out if it is liability or public record, and its type ID
				if (s.StartsWith("[Debug - Liability"))
					sType = "Liability";
				else if (s.StartsWith("[Debug - PublicRecord"))
					sType = "PublicRecord";
				else
				{
					sb.AppendFormat("<li>{0}</li>", s);
					continue;
				}

				int nStartIndex = s.IndexOf(":ID=") + 4;
				int nEndIndex = s.IndexOf(",", nStartIndex);
				if (0 > nEndIndex)
					nEndIndex = s.Length - 1;		// public record debug tag items don't have a ",", end with "]"
				string sTypeID = s.Substring(nStartIndex, nEndIndex - nStartIndex);

				// Manually entered tradeline will be given an ID such as "2a765176-8572-4248-bf07-4bc3bc0469ae".  
				// The credit report doesn't have data for these entries, so don't create link for them.
				if (36 == sTypeID.Length && '-' == sTypeID[8] && '-' == sTypeID[13] && '-' == sTypeID[18] && '-' == sTypeID[23])
				{
					sb.AppendFormat("<li>{0}</li>", s);
					continue;
				}

				// instead of making the entire debug tag item into a link, just make the sTypeID portion into link
				// sb.AppendFormat("<li><a href=\"#\" onclick=\"f_view{0}Xml('{1}'); return false;\">{2}</a></li>", sType, sTypeID, s);
				StringBuilder sbDebugTagItemWithLink = new StringBuilder();
				sbDebugTagItemWithLink.Append(s.Substring(0, nStartIndex));
				sbDebugTagItemWithLink.AppendFormat("<a href=\"#\" onclick=\"f_view{0}Xml({1}); return false;\">{2}</a>", AspxTools.JsStringUnquoted(sType), AspxTools.JsString(sTypeID), AspxTools.HtmlString(sTypeID));
				sbDebugTagItemWithLink.Append(s.Substring(nEndIndex));
				sb.AppendFormat("<li>{0}</li>", sbDebugTagItemWithLink.ToString());

				// 5/16/2006 nw - OPM 4911 - 2) Keep track of every item's XML data to be used by "View All" link.
				if ("Liability" == sType)
                    sbViewAllXML.Append(creditReport.GetLiabilityXml(sTypeID));
				else if ("PublicRecord" == sType)
					sbViewAllXML.Append(creditReport.GetPublicRecordXml(sTypeID));
				else
					sbViewAllXML.Append("");	// shouldn't happen
            }
			sbViewAllXML.AppendFormat("</{0}_DebugTagListXML>", methodName);
			if (!m_hashDebugTagListXML.Contains(methodName))
				m_hashDebugTagListXML.Add(methodName, sbViewAllXML.ToString());
			sb.Append("</ul></span></td></tr>");
            return sb.ToString();
        }
        private ArrayList GetCategory(string name) 
        {
            ArrayList ret = (ArrayList) m_hash[name];

            if (null == ret) 
            {
                ret = new ArrayList();
                m_hash[name] = ret;
            }
            return ret;
        }
        private int GetValue(string name, int defaultValue) 
        {
            try 
            {
                return int.Parse(Request.Form[name]);
            } 
            catch 
            {
                return defaultValue;
            }

        }
        private string GetString(string name) 
        {
            string s = Request.Form[name];
            return null == s ? "" : s;
        }

        protected void Button1_Click(object sender, System.EventArgs e)
        {
            #if (DEBUG)
            try 
            {
                if (null != m_file.PostedFile) 
                {
                    ImportXmlCreditReport(m_file.PostedFile.InputStream);
					m_btnCalculate_Click(null, null);
                }
            } 
            catch (Exception exc) {
                Tools.LogError(exc);
            }
            #endif
        }

        protected void Button2_Click(object sender, System.EventArgs e)
        {
            #if (DEBUG)
            CreditReportServer.DeleteCreditReport(this.BrokerID, ApplicationID);
            #endif
        }
        #if (DEBUG)
        /// <summary>
        /// This method is capable of allowing user to upload any credit file and save to the loan.
        /// DO NOT move this code to production. It's only purpose if for debugging.
        /// </summary>
        /// <param name="stream"></param>
        private void ImportXmlCreditReport(Stream stream) 
        {
            XmlDocument doc = new XmlDocument();
            doc.XmlResolver = null;
            
            doc.Load(stream);

            string xml = doc.InnerXml;

            if (doc.ChildNodes[0].Name == "CREDITDATA") 
            {
                // 12/2/2005 dd - This is MCL data, append decorative tag around it.
                xml = "<OUTPUT ><RESPONSE><ORDER_DETAIL status_code=\"READY\" report_id=\"613820\" /><OUTPUT_FORMAT format_type=\"XML\" ><![CDATA[" + xml
                    + "]]></OUTPUT_FORMAT></RESPONSE></OUTPUT>";
            }


            Guid dbFileKey = Guid.NewGuid();
            byte[] data = System.Text.Encoding.UTF8.GetBytes(xml);

            FileDBTools.WriteData(E_FileDB.Normal, dbFileKey.ToString(), data);

            SqlParameter[] parameters = new SqlParameter[] {
                                            new SqlParameter("@Owner", ApplicationID),
                                            new SqlParameter("@ExternalFileId", "TEST"),
                                            new SqlParameter("@DbFileKey", dbFileKey),
                                            new SqlParameter("@ComId", new Guid("D0CDB7A2-9040-4CCF-8D72-F67CAB3252E8")), // Only work on local DB only.
                                            new SqlParameter("@UserID", UserID),
                                            new SqlParameter("@CrAccProxyId", DBNull.Value),
                                                               new SqlParameter("@HowDidItGetHere", "Test")

                                        };
            StoredProcedureHelper.ExecuteNonQuery(this.BrokerID, "InsertCreditReportFile", 0, parameters);

        }

        #endif
	}
}
