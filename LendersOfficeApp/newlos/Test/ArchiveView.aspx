﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ArchiveView.aspx.cs" Inherits="LendersOfficeApp.newlos.Test.ArchiveView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <script type="text/javascript" src="../../inc/utilities.js"></script>
</head>
  <body MS_POSITIONING="FlowLayout" class="RightBackground" >
      	<script type="text/javascript">
<!--
      	function showData(anchor, event) {
      	    if (anchor && anchor.attributes && anchor.attributes.data) {
      	        document.getElementById('fieldContent').innerText = anchor.attributes.data.value;
      	        Modal.ShowPopup('FieldDetails', null, event);
      	    }
}

//-->
</script>
    <div class="MainRightHeader">Archive View</div>
    <form id="form1" runat="server">
    <div>
      <span class="FieldLabel">Archive:</span> <asp:DropDownList AutoPostBack="true" ID="ArchiveList" runat="server" Width="300"></asp:DropDownList>&nbsp;
        <span class="FieldLabel">Compare Archive:</span> <asp:DropDownList AutoPostBack="true" ID="CompareArchiveList" runat="server" Width="300"></asp:DropDownList>
        <br />
        <asp:PlaceHolder ID="ArchiveDump" runat="server" ></asp:PlaceHolder>
    </div>
        <div id="FieldDetails" style="border: black 3px solid; padding: 5px; display: none; position:absolute; background-color: whitesmoke;">
            <div id="fieldContent" style="overflow-y: scroll; height:400px; width:800px"></div>
            <div style="text-align:center"> [<a href="#" onclick="Modal.Hide()"> Close </a>]
            </div>
    </form>
</body>
</html>
