﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FieldDebugger.aspx.cs" Inherits="LendersOfficeApp.newlos.Test.FieldDebugger" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Field Debugger</title>
    <style type="text/css">
        .FieldID { width: 200px; }
        .FieldPropertyImage { cursor: pointer; }
        #Dialog { display: none; }
        #AppliedArchive { float: right; }
    </style>
</head>
<body bgcolor="gainsboro">
    <div class="MainRightHeader">Field Debugger</div>
    <form id="form1" runat="server">
        <div id="AppliedArchive">
            Use data from archive: 
            <asp:DropDownList ID="ArchiveToUse" runat="server" />
        </div>
    <div>
        <input type="hidden" id="FieldDependencyDebugger" />
            <ul id="FieldDependencySource" style="display: none;">
                <li>
                    <input class="ExpandField" type="button" value="+" />
                    <input class="FieldID" type="text" placeholder="Input field ID" />
                    <span class="FieldValue"></span>
                    <ul class="DependentFields" style="display:none;"></ul>
                </li>
            </ul>
    </div>
    <div id="Dialog">
        <p id="DialogFieldName"></p>
        <pre id="DialogContent"></pre>
    </div>
    </form>
</body>
 <script type="text/javascript">
     function InitializeFieldDependencyList() {
         var rootClassName = 'FieldRootDependency';
         var ul = $('<ul class="' + rootClassName + '"></ul>');
         ul.append($NewFieldListItem());
         $('#FieldDependencyDebugger').after(ul);
         $('.' + rootClassName).on('click', '.ExpandField', onExpandDependencyFieldClick);
         $('.' + rootClassName).on('keyup', '.FieldID', function (e) { if (e.keyCode === 13) { $(this).siblings('.ExpandField').click(); } }); // Fire click event on 'Enter'
     }
     function onExpandDependencyFieldClick() {
        var $this = $(this);
        var $fieldID = $this.siblings('.FieldID');
        var $fieldValue = $this.siblings('.FieldValue');
        $fieldValue.text('');
        var $dependentFields = $this.siblings('.DependentFields');
        $dependentFields.empty();
        var data = RetrieveDependentFieldsAndValues($fieldID.val());
        if (data && data.ErrorMessage) {
            alert(data.ErrorMessage);
        } else if (data && data.Field) {
            addFieldDebugItem($fieldID, $fieldValue, data.Field);

            if (data.DependentFields) {
                $dependentFields.show();
                if (!data.DependentFields.length) {
                    $dependentFields.append('<li>No dependent fields.</li>');
                } else {
                    for (var i = 0; i < data.DependentFields.length; ++i) {
                        var $newListItem = $NewFieldListItem();

                        addFieldDebugItem($newListItem.children('.FieldID'), $newListItem.children('.FieldValue'), data.DependentFields[i]);

                        $dependentFields.append($newListItem);
                    }
                }
            }
        }
     }
     function $NewFieldListItem() {
         return $('#FieldDependencySource').children().clone();
     }
     function addFieldDebugItem(idContainer, textContainer, item) {
         idContainer.val(item.FieldName);

         if (item.Message !== null) {
             textContainer.text(item.Message);

             return;
         }

         textContainer.text('= ' + item.FieldValue + '  ')
             .append($('<img src="' + ML.VirtualRoot + '/images/edocs/maximize.png" alt="View Field Properties" class="FieldPropertyImage" />')
             .data('fieldName', item.FieldName)
             .data('json', item.FieldJson)
             .click(function () { showObjectModal($(this).data('fieldName'), $(this).data('json')); }));
     }
     function showObjectModal(fieldName, json) {
         $('#Dialog').dialog({
             modal: true,
             closeOnEscape: true,
             open: function (event, ui) {
                 $("#DialogFieldName").text(fieldName);
                 $('#DialogContent').text(json);
                 $(".ui-dialog-titlebar-close").show();
             },
             dialogClass: "LQBDialogBox",
             title: 'View Field Properties',
             width: 400,
             height: 400
         });
     }
     function RetrieveDependentFieldsAndValues(fieldID) {
         var ret = false;
         var DTO = { sLId: ML.sLId, aAppId: ML.aAppId, fieldID: fieldID, archiveId: $('#ArchiveToUse').val() };
         callWebMethodAsync({
             type: "POST",
             contentType: "application/json; charset=utf-8",
             url: 'FieldDebugger.aspx/GetFieldAndDependencies',
             data: JSON.stringify(DTO),
             dataType: 'json',
             async: false,
             success: function(msg) {
                 ret = msg.d;
             },
             error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert(JSON.parse(XMLHttpRequest.responseText).Message);
             }
         });
         return ret;
     }
     InitializeFieldDependencyList();
 </script>
</html>
