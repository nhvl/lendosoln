﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Irs4506TVendorXsltTester.aspx.cs" Inherits="LendersOfficeApp.newlos.Test.Irs4506TVendorXsltTester" ValidateRequest="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
        LoXml: <asp:TextBox ID="LoXmlElement" runat="server" TextMode="MultiLine" Columns="80" Rows="3"></asp:TextBox> <br />
        <asp:Button runat="server" ID="LoXmlExportBtn" Text="Export Above From Loan as Xml" OnClick="ClickExport" /> <br />
        Xml: <asp:TextBox ID="NormalXmlElement" runat="server" TextMode="MultiLine" Columns="80" Rows="3"></asp:TextBox> <br /> 
        Xsl: <asp:TextBox ID="XslElement" runat="server" TextMode="MultiLine" Columns="80" Rows="3"></asp:TextBox><br />
        Vendor: <asp:DropDownList runat="server" ID="ActiveLqbModelVendorDropdown"></asp:DropDownList><br />
        TransactionId: <asp:TextBox ID="TransactionIdElement" runat="server"></asp:TextBox> <br />
        <asp:CheckBox runat="server" ID="CoborrowerModeCheckbox" Text="Coborrower Mode?" /> <br />
        Edocs DocId: <asp:TextBox ID="DocumentIdElement" runat="server"></asp:TextBox> (optional. NOTE: if provided then the EncodedDoc element won't match.) <br />
        ApplicationId: <asp:TextBox ID="ApplicationIdElement" runat="server"></asp:TextBox> (optional) <br />
        <asp:Button runat="server" ID="ExportAndConvertBtn" Text="Export and Transform Resulting Xml" OnClick="ClickExportAndConvert"/>
        <asp:Button runat="server" ID="TransformButton" Text="Transform Just Xml" OnClick="ClickTransform"/> <br />
        Transform Result:<asp:TextBox runat="server" ID="ResultantXmlElement" TextMode="MultiLine" Columns="80" Rows="3"></asp:TextBox> <br />
        <asp:Button runat="server" ID="Get4560TRequestXml" Text="Get 4506T request xml" OnClick="ClickGet4506TRequestXml" Columns="80" Rows="3"/> <br />
        4506TXml: <asp:TextBox runat="server" ID="Four506TRequestXmlElement" TextMode="MultiLine" Columns="80" Rows="3"></asp:TextBox> <br />
        <asp:Button runat="server" ID="CompareXsltAnd4506tXmlBtn" Text="Compare Xslt transform result and 4506T class result" OnClick="ClickCompareXsltAnd4506tXml"/> (or use fav diff tool)<br /> 
        <ml:EncodedLiteral runat="server" ID="XsltXmlVs4506tXmlEqualityStatus" Visible="false"/><br />
        Error: <asp:TextBox runat="server" ID="ErrorStatus" TextMode="MultiLine" Columns="80" Rows="3"></asp:TextBox>
    </form>
</body>
</html>
