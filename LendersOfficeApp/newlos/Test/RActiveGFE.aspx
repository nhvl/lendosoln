﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="RActiveGFE.aspx.cs" Inherits="LendersOfficeApp.newlos.Test.RActiveGFE" EnableViewState="false" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<!doctype html>
<html lang='en-GB'>
<head runat="server">
  <meta charset='utf-8'>
  <title>Ractive test</title>
  <style type="text/css">
      body
      {
          background-color: #DCDCDC;
      }
      .modal-background
      {
          position: fixed;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          background-color: rgba(0,0,0,0.5);
          padding: 0.5em;
          text-align: center;
          -moz-box-sizing: border-box;
          box-sizing: border-box;
      }
      .modal-outer
      {
          position: relative;
          width: 100%;
          height: 100%;
      }
      .modal
      {
          position: relative;
          background-color: white;
          padding: 2em;
          box-shadow: 1px 1px 3px rgba(0,0,0,0.1);
          margin: 0 auto;
          display: inline-block;
          max-width: 100%;
          max-height: 100%;
          overflow-y: auto;
          -moz-box-sizing: border-box;
          box-sizing: border-box;
      }
      .modal-button
      {
          text-align: center;
          background-color: rgb(70,70,180);
          color: white;
          padding: 0.5em 1em;
          display: inline-block;
          cursor: pointer;
      }
      .modal ul li
      {
          text-align: left;
      }
      .modal input[type='text']:not([preset])
      {
          width:40px;
      }
      .DDLImg, .ComboboxImg
      {
          width: 18px;
          height: 18px;
          top: 6px;
      }
      dev.MainRightHeader
      {
          padding: 10px;
      }
      table.GfeTable
      {
          margin-top: 10px;
      }
      .OptionsList
      {
          list-style-type: none;
          display: none;
          background-color: white;
          border-style: solid;
          border-width: 1px;
          z-index: 1;
          max-height: 300px;
          overflow-y: auto;
          padding: 0px;
          margin: 0px;
          margin: 0 auto;
          text-align: left;
          position: relative;
      }
    .RactiveDDLContainer
            {
                overflow:visible;
                height:13px;
                display:inline-block;
            }
      .SectionDiv
      {
          width: 900px;
          
          border: solid 1px;
          margin: 5px;
          padding: 6px;
          white-space:nowrap;
      }
      .SectionDiv div.FormTableSubheader
      {
          padding: 3px;
      }
      input[preset='money']
      {
          width: 90px;
      }
      input[preset='percent']
      {
          width: 70px;
      }
      select[disabled=""]
      {
          background-color: white;
      }
      .DateUnitInput
      {
          width: 38px;
          text-align:left !important;
      }
      .TimeInput
      {
          width: 25px;
      }
      .PaidBySplitDDL
      {
          width:35px;
      }
      
      .PayableDDL
      {
          width:85px;
      }
      .SectionDiv td
      {
          padding: 3px;
      }
      .invalid, .dupe {
        border: solid 3px red;
      }
      #wrapper {
	            _width: 100%;
	            _height: 100%;
	            _overflow: auto;
	            _position: relative;
            }
            
		    #footerWrapper 
		    {
		        position: fixed;
		        left: 0px;
		        bottom: 0px;
		        width: 100%;
		        
	            _position: absolute; /* Overrides position attribute in quirks mode */
            }
            
            #footer {
	            _margin-right: 17px; /* for right scroll bar in quirks mode */
	            
	            /**
	            /* Following properties needed to keep mouse clicks from falling through to underlying divs.
	            /* Placed at #footer level instead of #footerWrapper so that user can still click on #wrapper's scrollbar
	            **/
	            background: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7); /* background image set to 1x1 transparent GIF. */
	            width: 100%; /* needs to be set to 100%, or #footer width will match width of active tab */
            }
            
                .tabContent {
        background-color: gainsboro;
        padding: 10px;
    }
    
    #Tabs {
        background-color: Transparent;
    }
    
    #closeIcon{
        padding: 10px;
        float:right;
    }
    
    #closeIcon a,
    #closeIcon a:hover
    {
        font-size:15px;
        color: Gray;
        text-decoration: none;
    }
    
    #dialog-confirm
    {
        text-align: center;
    }
    
    .ContactImg
    {
        vertical-align:middle;
        margin-left:3px;
    }
    .ContactImg:hover
    {
        cursor:pointer;
    }
    .Hidden
    {
        display:none;
    }
    .Disabled
    {
        background-color:lightgrey !important;
        color: black;
    }
    
    .GfeTable tbody td, .RactiveDDL, .DDLImg, .ContactImg
    {
        vertical-align:top !important;
    }
    
    .BelowHeader{margin-top:3px !important;}
    #CheckboxDiv .FormTableHeader{ padding: 3px; margin: -6px -6px 0px -6px;}
    .money
    {
        text-align:right;
    }
    .FeeTablesContainer
    {
        display:inline-block;
    }
    .FeeTablesContainer table
    {
        width:100%;
        min-width:1080px;
    }
    .AlignLabelForLockCB 
    { 
        width: 125px; 
        display: inline-block;
    }

    #Dot
    {
        padding-bottom: 25px;
    }

    .SubFeeConfirmOverwriteContent
    {
        text-align: left;
        width: 400px;
    }

    .SubFeeConfirmOverwriteContentBtns
    {
        text-align: center;
    }
  </style>
</head>

<body>

<form runat="server" >
 
    <div style="display:none;" id="dialog-confirm" title="Delete fee?">
        <p style="width: 100%">
        </p>
    </div>
  <div id='container'></div>
  
  <div id="ConfirmAgent" style="display:none;">Selecting this contact will add it to the official contact list.</div>
  <div id="ClearAgent" style="display:none;">This action will remove the contact from the official contact list.</div>

  <script id="ClosingCostTemplate" type="text/ractive">
      <div class='MainRightHeader'><ml:EncodedLabel id="newGFETitle" runat="server"></ml:EncodedLabel></div>
      
      <div class="SectionDiv" runat="server" id="DisclosureRow">
        <input type="button" onclick="openPopup('newlos/Disclosure/SafeHarborDisclosureAdjustable.aspx','SHDA' );" value="Edit adjustable rate anti-steering disclosure form"/>
		<input type="button" onclick="openPopup('newlos/Disclosure/SafeHarborDisclosureFixed.aspx','SHDF');" value="Edit fixed rate anti-steering disclosure form" id="bEditFixed" />
      </div>
      
      <div class="SectionDiv FieldLabel" runat="server" id="phGFEArchiveRow">
            <input type="button" runat="server" ID="bRecordGFEToArchive" value="Record GFE data to archive" on-click="archiveGFE" />
            
			Last Disclosed GFE archive: <asp:DropDownList runat="server" ID="sLastDisclosedGFEArchiveId"></asp:DropDownList>
      </div>
      
      <div class="SectionDiv" runat="server" ID="phSelectGFERow">

		        <table><tr>
		            <td class="FieldLabel">
		               Select Archived GFE:  
		               <asp:DropDownList runat="server" ID="ddlGFEArchives" AutoPostBack="true" AlwaysEnable></asp:DropDownList> 
		               
		               <input type="button" id="bPrintGFE" value="Print Archived GFE" onclick="openGFEPDF()" AlwaysEnable="true"/>
		            </td>
		        </tr></table>
		        
	  </div>
      
    <div class="SectionDiv FieldLabel">
        Initial GFE Disclosure Date <datePicker value="{{sGfeInitialDisclosureD}}" readonly="{{IsProtectDisclosureDates}}" id="sGfeInitialDisclosureD" />
&nbsp;
        GFE Redisclosure Disclosure Date <datePicker value="{{sGfeRedisclosureD}}" readonly="{{IsProtectDisclosureDates}}" id="sGfeRedisclosureD" />
    </div>
    
    <div class="SectionDiv">
    <table>
    <tr>
		<td class="FieldLabel" nowrap colspan="6">Perform calculations:
		<input type="radio" name="{{ByPassBgCalcForGfeAsDefault}}" value="{{boolFalse}}"/> automatically
		<input type="radio" name="{{ByPassBgCalcForGfeAsDefault}}" value="{{boolTrue}}"/> manually
			<input disabled="{{!ByPassBgCalcForGfeAsDefault}}" on-click="triggerManualRecalc" tabindex="-1" type="button" value="Recalculate  (Alt + C)" name="btnCalculate" id="btnCalculate"/>
		</td>
	</tr>
        <tr>
            <td colSpan="7">
                <uc:CFM ID="CFM" runat="server" />
            </td>
        </tr>
        <tr>
	        <td class="FieldLabel" nowrap>Prepared By</td>
		    <td class="FieldLabel" nowrap colspan="5"><input type="text" value="{{GfeTilCompanyName}}" id="GfeTilCompanyName" runat="server" style="width:258px;" /></td>
	    </tr>
	    <tr>
		    <td class="FieldLabel" nowrap>Address</td>
		    <td class="FieldLabel" nowrap colspan="5"><input type="text" value="{{GfeTilStreetAddr}}" id="GfeTilStreetAddr" runat="server" style="width:258px;" /></td>
	    </tr>
	    <tr>
		    <td class="FieldLabel" nowrap></td>
		    <td class="FieldLabel" nowrap colspan="5"><input type="text" value="{{GfeTilCity}}" id="GfeTilCity" runat="server" />
		    <select NotEditable="true" name="GfeTilState" id="GfeTilState" value="{{GfeTilState}}" tabindex="0" >
		        <option value=""></option><option value="AA">AA</option><option value="AE">AE</option><option value="AK">AK</option><option value="AL">AL</option><option value="AP">AP</option><option value="AR">AR</option><option value="AS">AS</option><option value="AZ">AZ</option><option value="CA">CA</option><option value="CO">CO</option><option value="CT">CT</option><option value="DC">DC</option><option value="DE">DE</option><option value="FL">FL</option><option value="GA">GA</option><option value="GU">GU</option><option value="HI">HI</option><option value="IA">IA</option><option value="ID">ID</option><option value="IL">IL</option><option value="IN">IN</option><option value="KS">KS</option><option value="KY">KY</option><option value="LA">LA</option><option value="MA">MA</option><option value="MD">MD</option><option value="ME">ME</option><option value="MI">MI</option><option value="MN">MN</option><option value="MO">MO</option><option value="MP">MP</option><option value="MS">MS</option><option value="MT">MT</option><option value="NC">NC</option><option value="ND">ND</option><option value="NE">NE</option><option value="NH">NH</option><option value="NJ">NJ</option><option value="NM">NM</option><option value="NV">NV</option><option value="NY">NY</option><option value="OH">OH</option><option value="OK">OK</option><option value="OR">OR</option><option value="PA">PA</option><option value="PR">PR</option><option value="RI">RI</option><option value="SC">SC</option><option value="SD">SD</option><option value="TN">TN</option><option value="TX">TX</option><option value="UT">UT</option><option value="VA">VA</option><option value="VI">VI</option><option value="VT">VT</option><option value="WA">WA</option><option value="WI">WI</option><option value="WV">WV</option><option value="WY">WY</option>
		    </select>
		    <input type="text" value="{{GfeTilZip}}" id="GfeTilZip" preset="zipcode" on-blur="smartZipcodeChange : 'GfeTilCity', 'GfeTilState'"/>
	    </tr>
	    <tr>
		    <td class="FieldLabel" nowrap>Phone</td>
		    <td class="FieldLabel" nowrap colspan="5"><ml:phonetextbox id="GfeTilPhoneOfCompany" value="{{GfeTilPhoneOfCompany}}" runat="server"  preset="phone" />&nbsp;
		    Email <input type="text" value="{{GfeTilEmailAddr}}" ID="GfeTilEmailAddr" runat="server"  EnableViewState="false"/></td>
	    </tr>
	    <tr runat="server" value="{{trPreparedDate}}" id="trPreparedDate" visible="true">
		    <td class="FieldLabel" nowrap>Prepared Date</td>
		    <td class="FieldLabel" nowrap colspan="5"><datePicker value="{{GfeTilPrepareDate}}" id="GfeTilPrepareDate" /></td>
	    </tr>
	    <tr>
	        <td class="FieldLabel">Loan Program</td>
	        <td> <input type="text" style="width:430px;" value="{{sLpTemplateNm}}"/></td>
	    </tr>
	    <tr>
	        <td class="FieldLabel">CC Template</td>
	        <td><input type="text" style="width:430px;" value="{{sCcTemplateNm}}"/><input type="button" value="Find Closing Cost Template"  onclick="findCCTemplate();" /></td>
	    </tr>
	</table>
    </div>
    
    <div class="SectionDiv">
        <TABLE>
		<TR>
	        <TD>
                <table>
                <tr>
                    <td class="FieldLabel">Total Loan Amt</td>
                    <td><input type="text" on-blur="domblur" ID="sFinalLAmt"  value="{{sFinalLAmt}}"   preset="money" intro="initMask" ReadOnly="True" /></td>
                </tr>
                <tr>
                    <td class="FieldLabel">Interest Rate</td>
                    <td>
                        <input type="text" on-blur="domblur" ID="sNoteIR" value="{{sNoteIR}}"   preset="percent" intro="initMask"  />
                        </td>
                </tr>
                <tr>
                    <td class="FieldLabel">Term/Due</td>
                    <td>
                        <input type="text" class="DateUnitInput" intro="initMask" preset="numeric" ID="sTerm" value="{{sTerm}}"    MaxLength="3" />
                        /
                        <input type="text" class="DateUnitInput" intro="initMask" preset="numeric" ID="sDue" value="{{sDue}}"    MaxLength="3" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">Amort Type</td>
                    <td>
                        <asp:DropDownList runat="server" ID="sFinMethT" value="{{sFinMethT}}"   />
                    </td>
                </tr>
                </table>
	        </TD>
	        <TD style="vertical-align:top">
	           <TABLE>
    	       <TR>
    	           <TD class=FieldLabel>
    	                <label class="AlignLabelForLockCB">1st Payment Date</label>
 	                    <input type="checkbox" ID="sSchedDueD1Lckd" checked="{{sSchedDueD1Lckd}}" on-change="recalc" />
	                </TD>
                    <TD><datePicker value="{{sSchedDueD1}}" readonly="{{!sSchedDueD1Lckd}}" id="sSchedDueD1" />
    	       </TR>
    	       <TR>
    	           <TD class=FieldLabel>
                       <label class="AlignLabelForLockCB">Estimated Closing Date</label>
 	                   <input type="checkbox" ID="sEstCloseDLckd" checked="{{sEstCloseDLckd}}" on-change="recalc" />
                   </TD>
                   <TD><datePicker type="text" id="sEstCloseD" on-blur="domblur" value="{{sEstCloseD}}" readonly="{{!sEstCloseDLckd}}" /></TD>
    	       </TR>
    	       <TR>
    	           <TD class="FieldLabel">
                        <label class="AlignLabelForLockCB">Consummation Date</label>
	                    <input type="checkbox" on-change="recalc" ID="sConsummationDLckd" checked="{{sConsummationDLckd}}" Text="Lock"/>
	               </TD>
    	           <TD><datePicker id="sConsummationD" readonly="{{!sConsummationDLckd}}" value="{{sConsummationD}}" /></td>
    	       </TR>
    	       <TR>
    	            <TD class="FieldLabel">Days in Year</TD>
    	            <TD><input type="text" id="sDaysInYr" class="DateUnitInput" intro="initMask" preset="numeric" value="{{sDaysInYr}}" MaxLength="3" /></TD>
    	       </TR>
	           </TABLE>
	        </TD>
        </TR>
        <TR>
            <TD>
                <table class="InsetBorder">
                 <tr>
                     <td class="FormTableSubheader" colSpan="4">Adjustable Rate Mortgage - Rate Adjustment</td>
                 </tr>
                 <tr>
                     <td class=FieldLabel>1st Adj Cap</td>
                     <td style="width:90px"><input type="text" on-blur="domblur" id="sRAdj1stCapR" value="{{sRAdj1stCapR}}"   preset="percent" intro="initMask" /></td>
                     <td class=FieldLabel>Adj Cap</td>
                     <td><input type="text" on-blur="domblur" id="sRAdjCapR" value="{{sRAdjCapR}}"   preset="percent" intro="initMask" /></td>
                 </tr>
                 <tr>
                     <td class=FieldLabel>1st Change</td>
                     <td><input type="text" id="sRAdj1stCapMon" value="{{sRAdj1stCapMon}}" intro="initMask" preset="numeric" class="DateUnitInput"  MaxLength="3"/>&nbsp;mths</td>
                     <td class=FieldLabel>Adj Period</td>
                     <td><input type="text" class="DateUnitInput" intro="initMask" preset="numeric" id="sRAdjCapMon" value="{{sRAdjCapMon}}" class="DateUnitInput"  MaxLength="3"/> mths</td>
                 </tr>
                 <tr>
                     <td class=FieldLabel>Life Adj Cap</td>
                     <td><input type="text" on-blur="domblur" id="sRAdjLifeCapR" value="{{sRAdjLifeCapR}}" preset="percent" intro="initMask" /></td>
                 </tr>
                </table>
            </TD>
            <TD>
                <table class=InsetBorder>
                 <tr>
                     <td class="FormTableSubheader" colSpan="4">ARM Potential Negative Amort. - Payment Adjustment</td>
                 </tr>
                 <tr>
                     <td class=FieldLabel>Adj Cap</td>
                     <td style="width:90px"><input type="text" on-blur="domblur" id="sPmtAdjCapR" value="{{sPmtAdjCapR}}"   preset="percent" intro="initMask" /></td>
                     <td class=FieldLabel>Recast Pd</td>
                     <td><input type="text" class="DateUnitInput" intro="initMask" preset="numeric" id="sPmtAdjRecastPeriodMon" value="{{sPmtAdjRecastPeriodMon}}"    MaxLength="3"/>&nbsp;mths</td>
                 </tr>
                 <tr>
                     <td class=FieldLabel>Adj Period</td>
                     <td><input type="text" class="DateUnitInput" intro="initMask" preset="numeric" id="sPmtAdjCapMon" value="{{sPmtAdjCapMon}}"    MaxLength="3"/>&nbsp;mths</td>
                     <td class=FieldLabel>Recast Stop</td>
                     <td><input type="text" class="DateUnitInput" intro="initMask" preset="numeric" id="sPmtAdjRecastStop" value="{{sPmtAdjRecastStop}}"    MaxLength="3"/>&nbsp;mths</td>
                 </tr>
                 <tr>
                     <td class=FieldLabel>Max Balance Pc</td>
                     <td><input type="text" on-blur="domblur" id="sPmtAdjMaxBalPc" value="{{sPmtAdjMaxBalPc}}"   preset="percent" intro="initMask" /></td>
                 </tr>
                </table>
            </TD>
        </TR>
        <TR>
            <TD><strong>Interest Only Months</strong> <input type="text" class="DateUnitInput" intro="initMask" preset="numeric" id="sIOnlyMon" value="{{sIOnlyMon}}"  Width="36"  MaxLength="3"/> mths</TD>
        </TR>
	    </TABLE>
    </div>
    
    <div class="SectionDiv">
        <TABLE style="width:100%;">
	    <TR class=FieldLabel>
	        <TD colspan="3">
	            1. The interest rate for this GFE is available through <input type="checkbox" on-change="recalc" checked="{{sGfeNoteIRAvailTillDLckd}}" /><datePicker readonly="{{!sGfeNoteIRAvailTillDLckd}}" value="{{sGfeNoteIRAvailTillD}}" id="sGfeNoteIRAvailTillD" /> 
	            <input type="checkbox" on-change="toggleTimeControl:'GfeNoteIRAvailTillD'" checked="{{sIsPrintTimeForGfeNoteIRAvailTillD}}" /><timeControl value_hour="{{sGfeNoteIRAvailTillD_Time_hour}}" value_min="{{sGfeNoteIRAvailTillD_Time_minute}}" value_am="{{sGfeNoteIRAvailTillD_Time_am}}" id="sGfeNoteIRAvailTillD_Time"  readonly="{{!sIsPrintTimeForGfeNoteIRAvailTillD}}"/>

	            <asp:Repeater runat="server" ID="TimeZoneRepeater1">
                <HeaderTemplate>
                    <select NotEditable="true" value="{{sGfeNoteIRAvailTillDTimeZoneT}}" disabled="{{!sIsPrintTimeForGfeNoteIRAvailTillD}}" >
                </HeaderTemplate>
                <ItemTemplate>
                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                </ItemTemplate>
                <FooterTemplate>
                </select>
                </FooterTemplate>
              </asp:Repeater>
              
	        </TD>
	    </TR>
	    <TR class=FieldLabel>
	        <TD colspan="3">
	            2. This estimate for all other settlement charges is available <input type="checkbox" on-change="recalc" checked="{{sGfeEstScAvailTillDLckd}}" /><datePicker readonly="{{!sGfeEstScAvailTillDLckd}}" value="{{sGfeEstScAvailTillD}}" id="sGfeEstScAvailTillD"/> 
	            <input type="checkbox" on-change="toggleTimeControl:'GfeEstScAvailTillD'" checked="{{sIsPrintTimeForsGfeEstScAvailTillD}}" /><timeControl value_hour="{{sGfeEstScAvailTillD_Time_hour}}" value_min="{{sGfeEstScAvailTillD_Time_minute}}" value_am="{{sGfeEstScAvailTillD_Time_am}}" id="sGfeEstScAvailTillD_Time"  readonly="{{!sIsPrintTimeForsGfeEstScAvailTillD}}"/>

	            <asp:Repeater runat="server" ID="TimeZoneRepeater2">
                <HeaderTemplate>
                    <select NotEditable="true" value="{{sGfeEstScAvailTillDTimeZoneT}}"  ID="" disabled="{{!sIsPrintTimeForsGfeEstScAvailTillD}}" >
                </HeaderTemplate>
                <ItemTemplate>
                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                </ItemTemplate>
                <FooterTemplate>
                </select>
                </FooterTemplate>
              </asp:Repeater>
              
	        </TD>
	    </TR>
	    <TR class=FieldLabel>
	        <TD colspan="3">3. The rate lock period is  <input type="checkbox" on-change="recalc" checked="{{sGfeRateLockPeriodLckd}}" /><input type="text" readonly="{{!sGfeRateLockPeriodLckd}}" class="DateUnitInput" id="sGfeRateLockPeriod" value="{{sGfeRateLockPeriod}}"  Width="36"  MaxLength="3"/> days.</TD>
	    </TR>
	    <TR class=FieldLabel>
	        <TD colspan="3">4. The rate must be locked <input type="text" class="DateUnitInput" id="sGfeLockPeriodBeforeSettlement" value="{{sGfeLockPeriodBeforeSettlement}}"  Width="36"  MaxLength="3"/> days before settlement.</TD>
	    </TR>
	    <TR>
	        <TD class=FieldLabel>Can the interest rate rise?</TD>
	        <TD>
	            <input type="radio" disabled="true" name="{{sGfeCanRateIncrease}}" value="{{boolFalse}}"/>No<input type="radio" disabled="true" checked="{{sGfeCanRateIncrease}}" name="{{sGfeCanRateIncrease}}" value="{{boolTrue}}"/>Yes
	        </TD>
	        <TD>it can rise to a maximum of <input type="text" readonly="true" on-blur="domblur" value="{{sRLifeCapR}}" id="sRLifeCapR" preset="percent" intro="initMask" />. 
	            The first change will be in <input type="text" readonly="true" value="{{sGfeFirstInterestChangeIn}}" id="sGfeFirstInterestChangeIn"  MaxLength="3"/>.</TD>
	    </TR>
	    <TR>
	        <TD class=FieldLabel>Can the loan balance rise?</TD>
	        <TD>
	            <input type="radio" disabled="true" name="{{sGfeCanLoanBalanceIncrease}}" value="{{boolFalse}}"/>No<input type="radio" disabled="true" name="{{sGfeCanLoanBalanceIncrease}}" value="{{boolTrue}}"/>Yes
	        <TD>it can rise to a maximum of <input type="text" on-blur="domblur" id="sGfeMaxLoanBalance" readonly="true" value="{{sGfeMaxLoanBalance}}" preset="money" intro="initMask" />.</TD>
	    </TR>
	    <TR>
	        <TD class=FieldLabel style="vertical-align:top">Can the monthly amount rise?</TD>
	        <TD>
	            <input type="radio" disabled="true"  name="{{sGfeCanPaymentIncrease}}" value="{{boolFalse}}"/>No<input type="radio" disabled="true" name="{{sGfeCanPaymentIncrease}}" value="{{boolTrue}}"/>Yes
	        </TD>
	        <TD>the first increase can be in <input type="text" readonly="true" id="sGfeFirstPaymentChangeIn" value="{{sGfeFirstPaymentChangeIn}}"  MaxLength="3"/> 
	            and the monthly amount owed can rise to <input readonly="true" type="text" on-blur="domblur" value="{{sGfeFirstAdjProThisMPmtAndMIns}}" id="sGfeFirstAdjProThisMPmtAndMIns"  preset="money" intro="initMask" />.<br />
	            The maximum it can ever rise <input type="text" readonly="true" on-blur="domblur" value="{{sGfeMaxProThisMPmtAndMIns}}" id="sGfeMaxProThisMPmtAndMIns" preset="money" intro="initMask" />.</TD>
	    </TR>
	    <TR>
	        <TD class=FieldLabel>Is there a prepay penalty?</TD>
	        <TD>
	            <input type="radio"  name="{{sGfeHavePpmtPenalty}}" value="{{boolFalse}}"/>No<input type="radio" name="{{sGfeHavePpmtPenalty}}" value="{{boolTrue}}"/>Yes
	        </TD>
	        <TD>the maximum prepayment <input  readonly="{{!sGfeHavePpmtPenalty}}" type="text" on-blur="domblur" value="{{sGfeMaxPpmtPenaltyAmt}}" id="sGfeMaxPpmtPenaltyAmt"  preset="money" intro="initMask"  />.</TD>
	    </TR>
	    <TR>
	        <TD class=FieldLabel>Is there a balloon?</TD>
	        <TD>
	            <input type="radio" disabled="true" name="{{sGfeIsBalloon}}" value="{{boolFalse}}"/>No<input type="radio" disabled="true" name="{{sGfeIsBalloon}}" value="{{boolTrue}}"/>Yes
	        </TD>
	        <TD>the balloon payment of <input  readonly="true" type="text" on-blur="domblur" value="{{sGfeBalloonPmt}}" id="sGfeBalloonPmt" preset="money" intro="initMask" /> 
	            due in <input type="text"  readonly="true" value="{{sGfeBalloonDueInYrs}}" intro="initMask" preset="numeric" class="DateUnitInput" id="sGfeBalloonDueInYrs" Width="36" MaxLength="3"/> yrs.</TD>
	    </TR>
	    <TR>
	        <TD class=FieldLabel>Is an escrow account required?</TD>
	        <TD>
	            <input type="radio" name="{{sMldsHasImpound}}" value="{{boolFalse}}"/>No<input type="radio" name="{{sMldsHasImpound}}" value="{{boolTrue}}"/>Yes
	        </TD>
	    </TR>
	</TABLE>
    </div>

    <div class="SectionDiv" id="ccTableDiv" runat="server">
        <table id="ccTable" width="100%">
	        <tr class="FormTableHeader">
	            <td>
	                Closing Cost Automation Options
	            </td>
	        </tr>
	        <tr>
	            <td>
                    <input type="radio" name="{{sClosingCostAutomationUpdateT}}" value="{{'2'}}" on-change="recalc"/>Use automation to update all closing costs.<br/>
	                <input type="radio" name="{{sClosingCostAutomationUpdateT}}" value="{{'0'}}" on-change="recalc"/>Use automation to update the fees whose conditions no longer apply.<br/>
	                <input type="radio" name="{{sClosingCostAutomationUpdateT}}" value="{{'1'}}" on-change="recalc"/>Use closing costs currently on the loan file for similar scenarios.<br/>
	            </td>
	        </tr>
	    </table>
    </div>
    
    <div class="SectionDiv FieldLabel" id="CheckboxDiv">
        {{#if <%= AspxTools.JsBool(ShowRequireFeesFromDropDownCheckbox) %>}}
            <input type="checkbox" checked="{{sIsRequireFeesFromDropDown }}" />  <span>Protect compliance settings for fees</span>
        {{/if}}
        
        {{#if <%= AspxTools.JsBool(ShowIsManuallySetThirdPartyAffiliatePropsCheckbox) %>}}
            <input type="checkbox" checked="{{sIsManuallySetThirdPartyAffiliateProps }}" />  <span>Manually enter third-party and affiliate properties</span>
        {{/if}}
        
        {{#if <%= AspxTools.JsBool(ShowRequireFeesFromDropDownCheckbox) %> || <%= AspxTools.JsBool(ShowIsManuallySetThirdPartyAffiliatePropsCheckbox) %>}}
            <br/>
        {{/if}}
        
        <input type="checkbox" checked="{{sGfeIsTPOTransaction}}" disabled="{{sGfeIsTPOTransactionIsCalculated}}"  on-change="recalc"/>  <span>This transaction involves a TPO</span>
        
        
        <div class="FormTableHeader BelowHeader">Loan originator compensation source</div>
        
        <span class>Loan originator is paid by</span> 
        <input type="radio" name="{{sOriginatorCompensationPaymentSourceT}}" value="{{'1'}}" on-change="recalc"/> Borrower
        <input type="radio" name="{{sOriginatorCompensationPaymentSourceT}}" value="{{'2'}}" on-change="recalc"/> Lender
        
    </div>
    
    <div class="FeeTablesContainer">
    {{#SectionList:i}}  
     <table border="0" style="white-space: nowrap; padding-left: 5px;" cellpadding="2" cellspacing="0" class="GfeTable" >
        <thead>
            <tr class="GridHeader">
                <td colspan="17">
                  {{SectionName}}
                </td>
            </tr>
            <tr class="FormTableHeader " >
                <td>
                    HUD<br/>Line
                </td>
                <td>
                    Description/Memo 
                </td>
                <td>
                    GFE<br/>Box
                </td>
                <td>
                    APR
                </td>
                <td>
                    FHA
                </td>
                <td>
                    Paid to
                </td>
                {{#if CanReadDFLP}}
                    <td>
                        DFLP
                    </td>
                {{/if}}
                <td>
                    TP
                </td>
                <td>
                    AFF
                </td>
                <td>
                    Can<br/>Shop
                </td>
                <td>
                    Did<br/>Shop
                </td>
                <td></td>
                <td>
                    Amount
                </td>
                <td>
                    Paid by
                </td>
                <td>
                    Payable
                </td>
                <td>
                    Date paid
                </td>
                <td>
                
                </td>
            </tr>
        </thead>
        <tbody>
            {{#ClosingCostFeeList:j}}
                {{>ClosingCostFeeTemplate}}
            {{/#ClosingCostFeeList}}
        </tbody>
        {{#if SectionName.indexOf("1000") == -1}}
        <tfooter>
            <tr>
                <td colspan="16">
                    <input type="button" value="Add" on-click="addFee:{{i}}"/>
                </td>
            </tr>
        </tfooter>
        {{/if}}
           </table>
    {{/SectionList}}
    </div>
    
    <div class="SectionDiv">
    
        <div class="FormTableSubheader">
			GFE Summary
		</div>
    
        <table>
			<tr>
				<td>
					A1
				</td>
				<td class="RightPadding">
					<input type="text" value="{{sGfeOriginationF}}" id="sGfeOriginationF"  ReadOnly="true" preset="money"/>
				</td>
				<td>
					B3
				</td>
				<td class="RightPadding">
					<input type="text" value="{{sGfeRequiredServicesTotalFee}}"  id="sGfeRequiredServicesTotalFee"  ReadOnly="true" preset="money"/>
				</td>
				<td>
					B6
				</td>
				<td class="RightPadding">
					<input type="text" value="{{sGfeServicesYouShopTotalFee}}"  id="sGfeServicesYouShopTotalFee"  ReadOnly="true" preset="money"/>
				</td>
				<td>
					B9
				</td>
				<td class="RightPadding">
					<input type="text" value="{{sGfeInitialImpoundDeposit}}"  id="sGfeInitialImpoundDeposit"  ReadOnly="true" preset="money"/>
				</td>
				<td>
					B
				</td>
				<td>
					<input type="text" value="{{sGfeTotalOtherSettlementServiceFee}}"  id="sGfeTotalOtherSettlementServiceFee"  ReadOnly="true" preset="money"/>
				</td>
			</tr>
			<tr>
				<td>
					A2
				</td>
				<td>
					<input type="text" value="{{sLDiscnt}}"  id="sLDiscnt2"  ReadOnly="true" preset="money"/>
				</td>
				<td>
					B4
				</td>
				<td>
					<input type="text" value="{{sGfeLenderTitleTotalFee}}"  id="sGfeLenderTitleTotalFee"  ReadOnly="true" preset="money"/>
				</td>
				<td>
					B7
				</td>
				<td>
					<input type="text" value="{{sGfeGovtRecTotalFee}}"  id="sGfeGovtRecTotalFee"  ReadOnly="true" preset="money"/>
				</td>
				<td>
					B10
				</td>
				<td>
					<input type="text" value="{{sGfeDailyInterestTotalFee}}"  id="sGfeDailyInterestTotalFee"  ReadOnly="true" preset="money"/>
				</td>
				<td>
					A+B
				</td>
				<td>
					<input type="text" value="{{sGfeTotalEstimateSettlementCharge}}"  id="sGfeTotalEstimateSettlementCharge4"  ReadOnly="true" preset="money"/>
				</td>
			</tr>
			<tr>
				<td>
					A
				</td>
				<td>
					<input type="text" value="{{sGfeAdjOriginationCharge}}"  id="sGfeAdjOriginationCharge"  ReadOnly="true" preset="money"/>
				</td>
				<td>
					B5
				</td>
				<td>
					<input type="text" value="{{sGfeOwnerTitleTotalFee}}"  id="sGfeOwnerTitleTotalFee"  ReadOnly="true" preset="money"/>
				</td>
				<td>
					B8
				</td>
				<td>
					<input type="text" value="{{sGfeTransferTaxTotalFee}}"  id="sGfeTransferTaxTotalFee"  ReadOnly="true" preset="money"/>
				</td>
				<td>
					B11
				</td>
				<td>
					<input type="text" value="{{sGfeHomeOwnerInsuranceTotalFee}}"  id="sGfeHomeOwnerInsuranceTotalFee"  ReadOnly="true" preset="money"/>
				</td>
				<td>
					N/A
				</td>
				<td>
					<input type="text" value="{{sGfeNotApplicableF}}"  id="sGfeNotApplicableF"  ReadOnly="true" preset="money"/>
				</td>
			</tr>
		</table>
    </div>
    
    <div class="SectionDiv">
    
        <div class="FormTableSubheader">
            Tradeoffs Table
        </div>
        
        <TABLE ID="table4">
        <tr>
            <td></td>
            <td class="FieldLabel" style="width:120px">This GFE</td>
            <td class="FieldLabel" style="width:150px">Lower closing costs</td>
            <td class="FieldLabel">Lower rate</td>
        </tr>
        <tr>
            <td class="FieldLabel">Initial Loan Amt</td>
            <td><input type="text" value="{{sFinalLAmt}}" ID="sFinalLAmt2" preset="money" ReadOnly="true" /></td>
            <td><input type="text" value="{{sGfeTradeOffLowerCCLoanAmt}}" ID="sGfeTradeOffLowerCCLoanAmt" tabIndex="202" preset="money" on-change="recalc" /></td>
            <td><input type="text" value="{{sGfeTradeOffLowerRateLoanAmt}}" ID="sGfeTradeOffLowerRateLoanAmt" tabIndex="205" preset="money" on-change="recalc" /></td>
        </tr>
        <tr>
            <td class="FieldLabel">Initial Rate</td>
            <td><input type="text" value="{{sNoteIR}}" ID="sNoteIR2" preset="percent" ReadOnly="true" /></td>
            <td><input type="text" value="{{sGfeTradeOffLowerCCNoteIR}}" ID="sGfeTradeOffLowerCCNoteIR" tabIndex="203" preset="percent" on-change="recalc" /></td>
            <td><input type="text" value="{{sGfeTradeOffLowerRateNoteIR}}" ID="sGfeTradeOffLowerRateNoteIR" tabIndex="206" preset="percent" on-change="recalc" /></td>
        </tr>
        <tr>
            <td class="FieldLabel">Monthly Payment</td>
            <td><input type="text" value="{{sGfeProThisMPmtAndMIns}}" ID="sGfeProThisMPmtAndMIns" preset="money" ReadOnly="true" /></td>
            <td><input type="text" value="{{sGfeTradeOffLowerCCMPmtAndMIns}}" ID="sGfeTradeOffLowerCCMPmtAndMIns" preset="money" ReadOnly="true" /></td>
            <td><input type="text" value="{{sGfeTradeOffLowerRateMPmtAndMIns}}" ID="sGfeTradeOffLowerRateMPmtAndMIns" preset="money" ReadOnly="true" /></td>
        </tr>
        <tr>
            <td class="FieldLabel">Monthly Payment Change</td>
            <td style="font-style:italic">No Change</td>
            <td><input type="text" value="{{sGfeTradeOffLowerCCMPmtAndMInsDiff}}" ID="sGfeTradeOffLowerCCMPmtAndMInsDiff" preset="money" ReadOnly="true" /> more</td>
            <td><input type="text" value="{{sGfeTradeOffLowerRateMPmtAndMInsDiff}}" ID="sGfeTradeOffLowerRateMPmtAndMInsDiff" preset="money" ReadOnly="true" /> less</td>
        </tr>
        <tr>
            <td class="FieldLabel">Closing Cost Change</td>
            <td style="font-style:italic">No Change</td>
            <td><input type="text" value="{{sGfeTradeOffLowerCCClosingCostDiff}}" ID="sGfeTradeOffLowerCCClosingCostDiff" preset="money" ReadOnly="true" /> less</td>
            <td><input type="text" value="{{sGfeTradeOffLowerRateClosingCostDiff}}" ID="sGfeTradeOffLowerRateClosingCostDiff" preset="money" ReadOnly="true" /> more</td>
        </tr>
        <tr>
            <td class="FieldLabel">Total Closing Costs</td>
            <td><input type="text" value="{{sGfeTotalEstimateSettlementCharge}}" ID="sGfeTotalEstimateSettlementCharge" preset="money" ReadOnly="true" /></td>
            <td><input type="text" value="{{sGfeTradeOffLowerCCClosingCost}}" ID="sGfeTradeOffLowerCCClosingCost" tabIndex="204" preset="money" on-change="recalc" /></td>
            <td><input type="text" value="{{sGfeTradeOffLowerRateClosingCost}}" ID="sGfeTradeOffLowerRateClosingCost" tabIndex="207" preset="money" on-change="recalc" /></td>
        </tr>
		</TABLE>
    </div>
    
    <div class="SectionDiv">
        <div class="FormTableSubheader">
            Shopping Cart
        </div>
        <TABLE ID="table7">
            <TR>
                <TD></TD>
                <TD class="FieldLabel">This loan</TD>
                <TD class="FieldLabel">Loan 2</TD>
                <TD class="FieldLabel">Loan 3</TD>
                <TD class="FieldLabel">Loan 4</TD>
            </TR>
            <TR>
                <TD class="FieldLabel">Loan originator name</TD>
                <TD><input type="text" value="{{GfeTilCompanyName}}" ID="GfeTilCompanyName2" ReadOnly="true" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan1OriginatorName}}" ID="sGfeShoppingCartLoan1OriginatorName" tabIndex="208" on-change="recalc" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan2OriginatorName}}" ID="sGfeShoppingCartLoan2OriginatorName" tabIndex="220" on-change="recalc" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan3OriginatorName}}" ID="sGfeShoppingCartLoan3OriginatorName" tabIndex="232" on-change="recalc" /></TD>
            </TR>
            <TR>
                <TD class="FieldLabel">Initial loan amount</TD>
                <TD><input type="text" value="{{sFinalLAmt}}" ID="sFinalLAmt3" ReadOnly="true" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan1LoanAmt}}" ID="sGfeShoppingCartLoan1LoanAmt" tabIndex="209" on-change="recalc" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan2LoanAmt}}" ID="sGfeShoppingCartLoan2LoanAmt" tabIndex="221" on-change="recalc" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan3LoanAmt}}" ID="sGfeShoppingCartLoan3LoanAmt" tabIndex="233" on-change="recalc" /></TD>
            </TR>
            <TR>
                <TD class="FieldLabel">Loan term</TD>
                <TD><input type="text" value="{{sTerm}}" ID="sTerm2" ReadOnly="true" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan1LoanTerm}}" ID="sGfeShoppingCartLoan1LoanTerm" tabIndex="210" on-change="recalc" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan2LoanTerm}}" ID="sGfeShoppingCartLoan2LoanTerm" tabIndex="222" on-change="recalc" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan3LoanTerm}}" ID="sGfeShoppingCartLoan3LoanTerm" tabIndex="234" on-change="recalc" /></TD>
            </TR>
            <TR>
                <TD class="FieldLabel">Initial interest rate</TD>
                <TD><input type="text" value="{{sNoteIR}}" ID="sNoteIR3" ReadOnly="true" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan1NoteIR}}" ID="sGfeShoppingCartLoan1NoteIR" tabIndex="211" on-change="recalc" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan2NoteIR}}" ID="sGfeShoppingCartLoan2NoteIR" tabIndex="223" on-change="recalc" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan3NoteIR}}" ID="sGfeShoppingCartLoan3NoteIR" tabIndex="235" on-change="recalc" /></TD>
            </TR>
            <TR>
                <TD class="FieldLabel">Initial monthly payment</TD>
                <TD><input type="text" value="{{sGfeProThisMPmtAndMIns}}" ID="sGfeProThisMPmtAndMIns2" ReadOnly="true" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan1InitialPmt}}" ID="sGfeShoppingCartLoan1InitialPmt" tabIndex="212" on-change="recalc" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan2InitialPmt}}" ID="sGfeShoppingCartLoan2InitialPmt" tabIndex="224" on-change="recalc" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan3InitialPmt}}" ID="sGfeShoppingCartLoan3InitialPmt" tabIndex="236" on-change="recalc" /></TD>
            </TR>
            <TR>
                <TD class="FieldLabel">Rate lock period</TD>
                <TD><input type="text" value="{{sGfeRateLockPeriod}}" ID="sGfeRateLockPeriod2" ReadOnly="true" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan1RateLockPeriod}}" ID="sGfeShoppingCartLoan1RateLockPeriod" tabIndex="213" on-change="recalc" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan2RateLockPeriod}}" ID="sGfeShoppingCartLoan2RateLockPeriod" tabIndex="225" on-change="recalc" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan3RateLockPeriod}}" ID="sGfeShoppingCartLoan3RateLockPeriod" tabIndex="237" on-change="recalc" /></TD>
            </TR>
            <TR>
                <TD class="FieldLabel">Can interest rate rise?</TD>
                <TD><input type="checkbox" checked="{{sGfeCanRateIncrease}}" ID="sGfeCanRateIncrease3" disabled  /></TD>
                <TD><input type="checkbox" checked="{{sGfeShoppingCartLoan1CanRateIncreaseTri}}" ID="sGfeShoppingCartLoan1CanRateIncreaseTri" tabIndex="214" on-change="recalc"  /></TD>
                <TD><input type="checkbox" checked="{{sGfeShoppingCartLoan2CanRateIncreaseTri}}" ID="sGfeShoppingCartLoan2CanRateIncreaseTri" tabIndex="226" on-change="recalc"  /></TD>
                <TD><input type="checkbox" checked="{{sGfeShoppingCartLoan3CanRateIncreaseTri}}" ID="sGfeShoppingCartLoan3CanRateIncreaseTri" tabIndex="238" on-change="recalc"  /></TD>
            </TR>
            <TR>
                <TD class="FieldLabel">Can loan balance rise?</TD>
                <TD><input type="checkbox" checked="{{sGfeCanLoanBalanceIncrease}}" ID="sGfeCanLoanBalanceIncrease2" disabled  /></TD>
                <TD><input type="checkbox" checked="{{sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri}}" ID="sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri" tabIndex="215" on-change="recalc"  /></TD>
                <TD><input type="checkbox" checked="{{sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri}}" ID="sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri" tabIndex="227" on-change="recalc"  /></TD>
                <TD><input type="checkbox" checked="{{sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri}}" ID="sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri" tabIndex="239" on-change="recalc"  /></TD>
            </TR>
            <TR>
                <TD class="FieldLabel">Can monthly payment rise?</TD>
                <TD><input type="checkbox" checked="{{sGfeCanPaymentIncrease}}" ID="sGfeCanPaymentIncrease2" disabled  /></TD>
                <TD><input type="checkbox" checked="{{sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri}}" ID="sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri" tabIndex="216" on-change="recalc"  /></TD>
                <TD><input type="checkbox" checked="{{sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri}}" ID="sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri" tabIndex="228" on-change="recalc"  /></TD>
                <TD><input type="checkbox" checked="{{sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri}}" ID="sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri" tabIndex="240" on-change="recalc"  /></TD>
            </TR>
            <TR>
                <TD class="FieldLabel">Prepayment penalty?</TD>
                <TD><input type="checkbox" checked="{{sGfeHavePpmtPenalty}}" ID="sGfeHavePpmtPenalty2" disabled  /></TD>
                <TD><input type="checkbox" checked="{{sGfeShoppingCartLoan1HavePpmtPenaltyTri}}" ID="sGfeShoppingCartLoan1HavePpmtPenaltyTri" tabIndex="217" on-change="recalc"  /></TD>
                <TD><input type="checkbox" checked="{{sGfeShoppingCartLoan2HavePpmtPenaltyTri}}" ID="sGfeShoppingCartLoan2HavePpmtPenaltyTri" tabIndex="229" on-change="recalc"  /></TD>
                <TD><input type="checkbox" checked="{{sGfeShoppingCartLoan3HavePpmtPenaltyTri}}" ID="sGfeShoppingCartLoan3HavePpmtPenaltyTri" tabIndex="241" on-change="recalc"  /></TD>
            </TR>
            <TR>
                <TD class="FieldLabel">Balloon payment?</TD>
                <TD><input type="checkbox" checked="{{sGfeIsBalloon}}" ID="sGfeIsBalloon2" disabled  /></TD>
                <TD><input type="checkbox" checked="{{sGfeShoppingCartLoan1IsBalloonTri}}" ID="sGfeShoppingCartLoan1IsBalloonTri" tabIndex="218" on-change="recalc"  /></TD>
                <TD><input type="checkbox" checked="{{sGfeShoppingCartLoan2IsBalloonTri}}" ID="sGfeShoppingCartLoan2IsBalloonTri" tabIndex="230" on-change="recalc"  /></TD>
                <TD><input type="checkbox" checked="{{sGfeShoppingCartLoan3IsBalloonTri}}" ID="sGfeShoppingCartLoan3IsBalloonTri" tabIndex="242" on-change="recalc"  /></TD>
            </TR>
            <TR>
                <TD class="FieldLabel">Total Settlement Charge?</TD>
                <TD><input type="text" value="{{sGfeTotalEstimateSettlementCharge}}" ID="sGfeTotalEstimateSettlementCharge2" ReadOnly="true" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan1TotalClosingCost}}" ID="sGfeShoppingCartLoan1TotalClosingCost" tabIndex="219" on-change="recalc" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan2TotalClosingCost}}" ID="sGfeShoppingCartLoan2TotalClosingCost" tabIndex="231" on-change="recalc" /></TD>
                <TD><input type="text" value="{{sGfeShoppingCartLoan3TotalClosingCost}}" ID="sGfeShoppingCartLoan3TotalClosingCost" tabIndex="243" on-change="recalc" /></TD>
            </TR>
        </TABLE>
    </div>
        <div ID="OptionsContainer" >
            <asp:Repeater runat="server" ID="BeneficiaryRepeater">
                        <HeaderTemplate>
                            <ul class="BeneficiaryOptions OptionsList" tabindex="1" on-focus="reselectInput" on-blur="hideDesc" on-mouseenter="onHoverDescriptions" on-mouseleave="onMouseOutDescriptions">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li on-mouseenter="highlightLI" on-mouseleave="unhighlightLI" on-click="selectDDL" val="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"> <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></li>
                        </ItemTemplate>
                        <FooterTemplate>
                        </ul>
                        </FooterTemplate>
                      </asp:Repeater>
                      
                      <asp:Repeater runat="server" ID="CreditDescRepeater">
                        <HeaderTemplate>
                            <ul class="CreditDescOptions OptionsList" tabindex="1" on-focus="reselectInput" on-blur="hideDesc" on-mouseenter="onHoverDescriptions" on-mouseleave="onMouseOutDescriptions">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li on-mouseenter="highlightLI" on-mouseleave="unhighlightLI" on-click="selectCombobox" val="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"> <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></li>
                        </ItemTemplate>
                        <FooterTemplate>
                        </ul>
                        </FooterTemplate>
                      </asp:Repeater>
    </div>
    {{#if !IsArchive}}
        {{> Footer}}
    {{/if}}
  </script>
  
  <script id="TimeControl" type='text/ractive'>
      Time: <input class="TimeInput" type="text" id="{{id + ''}}" value="{{value_hour}}" readonly="{{readonly}}" on-blur="hourBlur" on-keyup="hourKeyUp" maxlength="2"/>
      : <input class="TimeInput" type="text" id="{{id + '_minute'}}" value="{{value_min}}" readonly="{{readonly}}" on-blur="minuteBlur" maxlength="2"/>
      <select NotEditable="true" id="{{id + '_am'}}"  value="{{value_am}}" disabled="{{readonly}}">
        <option value="AM">AM</option>
        <option value="PM">PM</option>
      </select>
  </script>
  
  <script id="DatePicker" type='text/ractive'>
    <input type="text" class="mask date" readonly="{{readonly}}"  preset="date" id="{{id}}" intro="initMask" on-blur="updatePicker"   value="{{value}}" />
    <a on-click="displayCalendar" href="#" on-click="showCalendar:{{id}}" >
        <img title="Open calendar" src="[[VRoot]]/images/pdate.gif" border="0">
    </a>
  </script> 
     
  <script id='RactiveCombobox' type='text/ractive'>
            <div class="RactiveDDLContainer">
                <input on-keydown='keyScroll' field="{{Field}}" value="{{value}}" readonly='{{disabled}}' {{IsArchive || IsReadonly?"": "NotEditable"}} class="RactiveCombobox {{'RactiveCombobox'+i+j}} {{Prefix}}Combobox" type="text" on-blur="hideDesc: '{{Prefix}}Options'"  /><img class="DDLImg" src= "../../images/IEArrow.png" on-click="showOptions: {{i}},{{j}},{{k}}, '{{Prefix}}Options'" >
            </div>
  </script>
  <script id='RactiveDDL' type='text/ractive'>
            <div class="RactiveDDLContainer">
                <input on-keydown='keyScroll' disabled='{{disabled}}' {{IsArchive || IsReadonly?"": "NotEditable"}} class="{{disabled? "Disabled": ""}} RactiveDDL {{'RactiveDDL'+i+j}} {{Prefix}}DDL" readonly type="text" on-click="showOptions: {{i}},{{j}},{{k}},'{{Prefix}}Options'"  on-blur="hideDesc: '{{Prefix}}Options'"  /><img class="DDLImg" src= "../../images/IEArrow.png" on-click="showOptions: {{i}},{{j}},{{k}}, '{{Prefix}}Options'" >
                <input type="hidden" value="{{value}}" />
            </div>
  </script>
  
  <script id='ClosingCostFeeTemplate' type='text/ractive'>
    <tr class="{{ j % 2 == 0 ? "GridItem" : "GridAlternatingItem"}}">
        <td class="FieldLabel"> 
            <input type="text" value="{{hudline}}"  data-hud-start="{{HudLineStart}}" data-hud-end="{{HudLineEnd}}" data-is-sub-fee="{{(!!sourceid)}}" class="hudline" maxlength="4" style="width:25px;" readonly="{{(is_system && !editableSystem) || sIsRequireFeesFromDropDown}}" />
        </td>
        <td>
            <input  type="text" value="{{desc}}" readonly="{{sIsRequireFeesFromDropDown || f.t === 20 || f.t === 18}}" style="width: 160px;" />
            {{#if desc != org_desc}}
                <div>Type: {{org_desc}}</div>
            {{/if}}
        </td>
        <td>
            <asp:Repeater runat="server" ID="SectionRepeater">
                <HeaderTemplate>
                    <select NotEditable="true" disabled="{{sIsRequireFeesFromDropDown}}" intro="sectionIntro:{hudline: {{hudline}}, isSystem: {{is_system}}, gfeGroup: {{gfeGrps}} }" value="{{section}}">
                </HeaderTemplate>
                <ItemTemplate>
                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                </ItemTemplate>
                <FooterTemplate>
                </select>
                </FooterTemplate>
              </asp:Repeater>
        </td>
        <td>
            <input type="checkbox" checked="{{apr}}" disabled="{{sIsRequireFeesFromDropDown}}"/> 
        </td>
        <td>
           <input type="checkbox" checked="{{fha}}" disabled="{{sIsRequireFeesFromDropDown}}"/>
        </td>
        <td>
			<ractiveDDL Prefix='Beneficiary' value = '{{bene}}' /><img class="ContactImg" on-mouseover="onContactHover" on-mouseout="onContactMouseOut" on-click="showAgentPicker : {{i}}, {{j}}" src="../../images/contacts.png">
			{{#if bene_desc || bene_id != <%=AspxTools.JsString(Guid.Empty) %> }}
			    <div> Company: {{bene_desc}} </div>
			{{/if}}
        </td>
        {{#if CanReadDFLP}}
            <td>
               <input type="checkbox" disabled="{{!CanSetDFLP}}" checked="{{dflp}}" />
            </td>
        {{/if}}
        <td>
            <input type="checkbox" checked="{{tp}}" disabled = "{{!sIsManuallySetThirdPartyAffiliateProps && DisableTPAffIfHasContact && (bene_id != '<%=AspxTools.HtmlString(Guid.Empty) %>')}}" on-click="tpClick:{{i}},{{j}}" /> 
        </tD>
        <td>
           <input type="checkbox" checked="{{aff}}" disabled="{{!tp || (!sIsManuallySetThirdPartyAffiliateProps && DisableTPAffIfHasContact && (bene_id != '<%=AspxTools.HtmlString(Guid.Empty) %>'))}}" /> 
        </td>
        <td>
            <input type="checkbox" checked="{{can_shop}}" on-change="CanShopChange: {{i}}, {{j}}"  disabled="{{sIsRequireFeesFromDropDown || disc_sect == 2 || disc_sect == 3}}" /> 
        </td>
        <td>
            <input type="checkbox" checked="{{did_shop}}" disabled="{{!can_shop}}" />
        </td>
        <td style="text-align: right;">
            {{#if SectionName.indexOf("1000") == -1 && f.t !== 0 && f.t !== 4 && f.t !== 5 && f.t !== 14 && f.t !== 17 && f.t !== 18 && f.t !== 19 && f.t !== 20 && f.t !== 21}}
                <input type="button" value="C" on-click="updateCalc:{{i}},{{j}}"/>
            {{/if}}
        </td>
        <td>
            {{#if f.t === 3}} 
                <input type="text" value="{{f.base}}" class="mask money" preset="money" intro="initMask" on-blur="domblur" on-change="recalc"  />
            {{else}}
                <input type="text" value="{{total}}" class="mask money"  readonly="{{f.t != 3}}" preset="money" intro="initMask" on-blur="domblur" />
            {{/if}}
        </td>
        <td>
              {{#if (pmts && pmts.length == 1)}} 
                <select NotEditable="true" value="{{pmts[0].paid_by}}" on-change="updatePaidBy:{{i}},{{j}},{{total}}"
                    disabled="{{typeid == <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId) %>}}">
                   {{#if sClosingCostFeeVersionT == 2 && (f.t != 5 && f.t != 21)}}
                    <option value="-1">--split--</option>
                   {{/if}}
                    <option value="1">borr pd</option>
                    <option value="2">seller</option>
                    <option value="3">borr fin</option>
                   {{#if typeid != <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId) %> }}
                    <option value="4">lender</option>
                   {{/if }}
                    <option value="5">broker</option>
                </select>
            {{else}}
            &nbsp;
            {{/if}}
        </td>
        <td>
            {{#if (pmts && pmts.length == 1)}} 
                <select disabled="{{pmts[0].paid_by == 3}}" NotEditable="true" value="{{pmts[0].pmt_at}}" on-change="recalc">
                    <option value="1">at closing</option>
                    <option value="2">outside of closing</option>                
                </selecT>
            {{else}}
            &nbsp;
            {{/if}}
            
        </td>
        <td>
            {{#if pmts && pmts.length == 1}} 
      
              <datePicker readonly="{{pmts[0].pmt_at == 1}}" value="{{pmts[0].pmt_dt}}" id="d{{i}}{{j}}" />
            {{else}}
                &nbsp;
            {{/if}}
        </td>
        <td> 
            {{#if f.t !== 17 && f.t !== 4 && f.t !== 5 && f.t !== 21 && f.t !== 20 && SectionName.indexOf("1000") == -1 && typeid !== 'f17c6573-e1bb-447d-ba89-31183be07be8' && (typeid !== <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId) %> || total == '$0.00')}}
                {{#if f.t === 3}} 
                    <input type="button" value=" - " on-click="delete:{{i}},{{j}},{{desc}},{{f.base}},{{org_desc}}"/>
                {{else}}
                    <input type="button" value=" - " on-click="delete:{{i}},{{j}},{{desc}},{{total}},{{org_desc}}"/>
                {{/if}}
            {{/if}}
        </td>
    </tr>
    {{#if pmts.length > 1}} 
    {{#pmts:k}}
        {{>ClosingCostFeePaymentTemplate}}
    {{/pmts}}
    {{/if}}
  </script>
  <script id='ClosingCostFeePaymentTemplate' type='text/ractive'>
    <tr>
        <td colspan="10">
            &nbsp;
        </td>
        <td>
            {{#if (is_system && !mipFinancedPmt && !cashPdPmt)}}
                <input type="button" value="+" on-click="AddSplitPayment:{{i}},{{j}}" />
            {{/if}}
        </td>
        <td>
            <input on-change="recalc" readonly="{{is_system || mipFinancedPmt || cashPdPmt}}" style="text-align:right" type="text" value="{{amt}}" class="mask money" preset="money"  on-blur="domblur" intro="initMask"/>
        </td>
        <td>&nbsp;</td>
        <td>
            <select NotEditable="true" value="{{paid_by}}" on-change="updatePaidBySplit:{{i}},{{j}},{{k}}" disabled="{{is_system || mipFinancedPmt}}" >
                    <option value="1">borr pd</option>
                    <option value="2">seller</option>
                    {{#if !cashPdPmt}}
                    <option value="3">borr fin</option>
                    {{/if}}
                    {{#if typeid != <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId) %> }}
                        <option value="4">lender</option>
                    {{/if }}
                    <option value="5">broker</option>
           </select>
        </td>
        <td>
            <select NotEditable="true" value="{{pmt_at}}" disabled="{{is_system || paid_by == 3 || mipFinancedPmt}}" on-change="recalc">
                    <option value="1">at closing</option>
                    <option value="2">outside of closing</option>     
            </select>
        </td>
        <td>   
            <input type="text" class="mask date" preset="date" id="d{{i}}{{j}}{{k}}" intro="initMask" value="{{pmt_dt}}" on-blur="domblur" readonly="{{is_system || pmt_at == 1}}" />
            <a on-click="displayCalendar" href="#" on-click="showCalendar:d{{i}}{{j}}{{k}}" >
            <img title="Open calendar" src="[[VRoot]]/images/pdate.gif" border="0" />
            </a>
        </td>
        <td>
			{{#if !is_system && !cashPdPmt && !mipFinancedPmt}}
                <input type="button" value=" - " on-click="removePayment:{{i}},{{j}},{{k}},{{amt}},{{paid_by}},{{desc}},{{org_desc}}"/>            
			{{/if}}
        </td>
    </tr>
  </script>
  <script id="CalculationModalContent" type="text/ractive">
  <p>
    {{#if fee.t == 0 || fee.t == 4 || fee.t == 5 || fee.t == 17 || fee.t == 18}} 
        Not Supported
    
    {{/if}}
    {{#if fee.t == 1 || fee.t == 2 || fee.t == 3}}
        <p>
            <select value="{{fee.t}}">
                <option value="3">Flat Amount</option>
                <option value="1">Full</option>
                <option value="2">Percent Only</option>
            </select>
        </p>
    {{/if}}  
    {{#if fee.t == 1 || fee.t == 8}}
    <label>Percent <input type="text" value="{{fee.p}}" preset="percent" intro="initMask" class="money mask"/> </label>  of  
    <asp:Repeater runat="server" ID="PercentRepeater">
                <HeaderTemplate>
                    <select NotEditable="true" value="{{fee.pt}}" >
                </HeaderTemplate>
                <ItemTemplate>
                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                </ItemTemplate>
                <FooterTemplate>
                </select>
                </FooterTemplate>
              </asp:Repeater>
                + <input type="text" value="{{fee.base}}" preset="money" intro="initMask" class="money mask" /> 
    {{/if}}
    {{#if fee.t == 2}} 
        <label>Percent <input type="text" class="mask"  value="{{fee.p}}" preset="percent" intro="initMask" /> </label>  of  
        <asp:Repeater runat="server" ID="PercentRepeater2">
                <HeaderTemplate>
                    <select NotEditable="true" value="{{fee.pt}}" >
                </HeaderTemplate>
                <ItemTemplate>
                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                </ItemTemplate>
                <FooterTemplate>
                </select>
                </FooterTemplate>
              </asp:Repeater> 
    {{/if}}
    {{#if fee.t == 3}} 
        <input type="text" value="{{fee.base}}" preset="money" class="mask money"  intro="initMask" /> 
    {{/if}}
    {{#if fee.t == 6}}
        <input type="text" value="{{fee.period}}" intro="initMask" preset="numeric" style="width: 40px;" /> period
    {{/if}}
    {{#if fee.t == 7}}
        <input on-change="recalc" type="checkbox" checked="{{loan.sIPiaDyLckd}}"> Lock
        <input type="text" value="{{loan.sIPiaDy}}" readonly="{{!loan.sIPiaDyLckd}}" class="mask DateUnitInput" preset="numeric" intro="initMask" /> days @
        <input on-change="recalc" type="checkbox" checked="{{loan.sIPerDayLckd}}" /> Lock
        <input type="text" value="{{loan.sIPerDay}}" readonly="{{!loan.sIPerDayLckd}}" decimalDigits="6" class="mask money" preset="money" intro="initMask" /> per day
    {{/if}}
    {{#if fee.t == 9 || fee.t == 10}}
        <input type="text" value="{{fee.period}}" intro="initMask" preset="numeric" /> months @ <input type="text" value="{{fee.base}}" class="mask money" preset="money" intro="initMask" readonly="true" /> / month
    {{/if}}
    {{#if fee.t == 11}}
        <input type="text" value="{{fee.period}}" intro="initMask" preset="numeric" readonly="true" /> periods
    {{/if}}
    {{#if fee.t == 12 || fee.t == 13 || fee.t == 15 || fee.t == 16}}
        <input type="text" preset="numeric" value="{{fee.period}}" intro="initMask" /> months @ <input type="text" readonly="true" value="{{fee.base}}" class="mask money" preset="money" intro="initMask" /> / month
    {{/if}}
  </p>
  
  <p>
    <input type="button" value=" OK " on-click="close: true"/> <input type="button" value=" Cancel " on-click="close: false"/> 
  </p>
  </script>
  
  <script id="FeePickerModalContent" type="text/ractive">
    <h3>Available Fees for {{SectionName}}</h3>
    <div>
        <ul>
        {{#FeeListAsBorrowerFees:i}} 
            <li><a href="#" on-click="selectFee:{{i}}">{{desc}}</a></li>
        {{/FeeListAsBorrowerFees}}
        {{^FeeListAsBorrowerFees}}
            <li>No Available Fees</li>
        {{/FeeListAsBorrowerFees}}
        </ul>
        <input type="button" value=" Cancel " on-click="close"/> 
    </div>
  </script>

    <script id="SubFeeConfirmOverwriteContent" type="text/ractive">
        <div class="SubFeeConfirmOverwriteContent">
            The following {{#if SubFees.length > 1}}fees{{else}}fee{{/if}}:

            <ul>
                {{#SubFees:i}} 
                    <li>{{desc}}</li>
                {{/SubFees}}
            </ul>

            {{#if SubFees.length > 1}}are{{else}}is{{/if}} considered to be the same fee type as the "{{ParentDesc}}"{{#if ParentDesc.substr(ParentDesc.length-3) !== "fee" }}&nbsp;fee{{/if}}.
            Adding the "{{ParentDesc}}"{{#if ParentDesc.substr(ParentDesc.length-3) !== "fee" }}&nbsp;fee{{/if}} will cause the current {{#if SubFees.length > 1}}fees{{else}}fee{{/if}}
            to be removed. Would you like to proceed?
         
            <br>
            <br>

            <div class="SubFeeConfirmOverwriteContentBtns">
                <input type="button" value=" OK " on-click="subFeeDlgOk:{{i}}"/> 
                <input type="button" value=" Cancel " on-click="close"/> 
            </div>
        </div>
    </script>
  
  <script id="modal" type="text/ractive">
  <div class='modal-background'><div class='modal-outer'><div class='modal'>{{>modalContent}}</div></div></div>
  </script>
  
  <script id="Footer" type="text/ractive">
        <div id="footerWrapper">
        <div id="footer">
            <ul id="Tabs" class="tabnav">
                <li id="LenderCreditsLnk">
                    <a href="#LenderCreditsLnk" onclick="switchTab('LenderCredits');">Lender Credits</a>
                </li>
                <li id="PaidTotalsLnk">
                    <a href="#PaidTotalsLnk" onclick="switchTab('PaidTotals');">Lender and Seller paid totals</a>
                </li>
                <li id="DotLnk">
                    <a href="#DotLnk" onclick="switchTab('Dot');">Details of Transaction</a>
                </li>
                <li id="MinLnk">
                    <a href="#MinLnk" onclick="switchTab('Min');">Minimize</a>
                </li>
            </ul>

            <div id="closeIcon">
                <a href="#MinLnk" onclick="switchTab('Min');">&times;</a>
            </div>

            <div id="LenderCredits" class="tabContent">
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="FieldLabel">
                                        Credit/charge calculation method
                                    </td>
                                    <td style="text-align: right;">
                                        <asp:Repeater runat="server" ID="sLenderCreditCalculationMethodTRep">
                                            <HeaderTemplate>
                                                <select on-change="recalc" on-blur="domblur" id="sLenderCreditCalculationMethodT" value="{{sLenderCreditCalculationMethodT}}">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                    <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </select>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>    
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{#if sLenderCreditCalculationMethodT == 0}}
                                <table>
                                    <tr>
                                        <td class="FieldLabel">
                                            Final price
                                        </td>
                                        <td style="text-align: right;">
                                            <input readonly="true" type="text" intro="initMask" value="{{sBrokerLockOriginatorPriceBrokComp1PcPrice}}" id="sBrokerLockOriginatorPriceBrokComp1PcPrice" preset="percent" />
                                        </td>
                                        <td style="text-align: right;">
                                            <input readonly="true" type="text" intro="initMask" value="{{sBrokerLockOriginatorPriceBrokComp1PcAmt}}" id="sBrokerLockOriginatorPriceBrokComp1PcAmt" preset="money" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">
                                            Initial credit (-)/charge (+)
                                        </td>
                                        <td>
                                            <input type="text" intro="initMask" value="{{sLDiscntPc}}" id="sLDiscntPc" preset="percent" on-change="recalc" />
                                            of
                                            <asp:Repeater runat="server" ID="sLDiscntBaseTRep">
                                                <HeaderTemplate>
                                                    <select on-change="recalc" on-blur="domblur" id="sLDiscntBaseT" value="{{sLDiscntBaseT}}">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                        <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </select>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            +
                                            <input type="text" intro="initMask" value="{{sLDiscntFMb}}" id="sLDiscntFMb" preset="money" on-change="recalc" />
                                        </td>
                                        <td>
                                            <input readonly="true" type="text" intro="initMask" value="{{sLDiscnt}}" id="sLDiscnt" preset="money" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">
                                            Credit for lender paid fees
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:Repeater runat="server" ID="sGfeCreditLenderPaidItemTRep">
                                                <HeaderTemplate>
                                                    <select on-change="recalc" on-blur="domblur" id="sGfeCreditLenderPaidItemT" value="{{sGfeCreditLenderPaidItemT}}">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                        <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </select>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                        <td>
                                            <input readonly="true" type="text" intro="initMask" value="{{sGfeCreditLenderPaidItemF}}" id="sGfeCreditLenderPaidItemF" preset="money" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">
                                            General lender credit
                                        </td>
                                        <td></td>
                                        <td>
                                            <input readonly="true" type="text" intro="initMask" value="{{sGfeLenderCreditF}}" id="sGfeLenderCreditF" preset="money" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">
                                            Additional credits at closing
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">
                                            Lender paid items not included in initial credit
                                        </td>
                                        <td></td>
                                        <td>
                                            <input readonly="true" type="text" intro="initMask" value="{{sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt}}" id="sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt" preset="money" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="text" intro="initMask" value="{{sLenderCustomCredit1Description}}" id="sLenderCustomCredit1Description" />
                                        </td>
                                        <td style="text-align: right;">
                                            <input type="text" intro="initMask" value="{{sLenderCustomCredit1Amount}}" id="sLenderCustomCredit1Amount" preset="money" on-change="recalc" />
                                        </td>
                                        <td>
                                            <input readonly="true" type="text" intro="initMask" value="{{sLenderCustomCredit1AmountAsCharge}}" id="sLenderCustomCredit1AmountAsCharge" preset="money" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="text" intro="initMask" value="{{sLenderCustomCredit2Description}}" id="sLenderCustomCredit2Description" />
                                        </td>
                                        <td style="text-align: right;">
                                            <input type="text" intro="initMask" value="{{sLenderCustomCredit2Amount}}" id="sLenderCustomCredit2Amount" preset="money" on-change="recalc" />
                                        </td>
                                        <td>
                                            <input readonly="true" type="text" intro="initMask" value="{{sLenderCustomCredit2AmountAsCharge}}" id="sLenderCustomCredit2AmountAsCharge" preset="money" />
                                        </td>
                                    </tr>
                                </table>
                            {{else}}
                                <table>
                                    <tr>
                                        <td></td>
                                        <td class="FieldLabel">
                                            Final price
                                        </td>
                                        <td>
                                            <asp:Repeater runat="server" ID="sLDiscntBaseTRep_calc">
                                                <HeaderTemplate>
                                                    <select on-change="recalc" on-blur="domblur" id="sLDiscntBaseT_calc" value="{{sLDiscntBaseT}}">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                        <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </select>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            &nbsp;
                                            <input readonly="true" type="text" intro="initMask" value="{{sBrokerLockOriginatorPriceBrokComp1PcPrice}}" id="sBrokerLockOriginatorPriceBrokComp1PcPrice_calc" preset="percent" />
                                        </td>                                      
                                        <td>
                                            <input readonly="true" type="text" intro="initMask" value="{{sBrokerLockOriginatorPriceBrokComp1PcAmt}}" id="sBrokerLockOriginatorPriceBrokComp1PcAmt_calc" preset="money" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td class="FieldLabel">Lender credit target</td>
                                        <td></td>
                                        <td style="text-align: right;">
                                            <input readonly="true" type="text" intro="initMask" value="{{sLenderCreditAvailableAmt_Neg}}" id="sLenderCreditAvailableAmt_Neg" preset="money" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">
                                            Disclose credit on
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Repeater runat="server" ID="sLenderPaidFeeDiscloseLocationTRep">
                                                <HeaderTemplate>
                                                    <select on-change="recalc" on-blur="domblur" id="sLenderPaidFeeDiscloseLocationT" value="{{sLenderPaidFeeDiscloseLocationT}}">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                        <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </select>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                        <td class="FieldLabel">
                                            Credit for lender paid fees
                                        </td>
                                        <td></td>
                                        <td style="text-align: right;">
                                            <input readonly="true" type="text" intro="initMask" value="{{sLenderPaidFeesAmt_Neg}}" id="sLenderPaidFeesAmt_Neg" preset="money" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Repeater runat="server" ID="sLenderGeneralCreditDiscloseLocationTRep">
                                                <HeaderTemplate>
                                                    <select on-change="recalc" on-blur="domblur" id="sLenderGeneralCreditDiscloseLocationT" value="{{sLenderGeneralCreditDiscloseLocationT}}">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                        <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </select>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                        <td class="FieldLabel">
                                            General lender credit
                                        </td>
                                        <td></td>
                                        <td style="text-align: right;">
                                            <input readonly="true" type="text" intro="initMask" value="{{sLenderGeneralCreditAmt_Neg}}" id="sLenderGeneralCreditAmt_Neg" preset="money" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Repeater runat="server" ID="sLenderCustomCredit1DiscloseLocationTRep">
                                                <HeaderTemplate>
                                                    <select on-change="recalc" on-blur="domblur" id="sLenderCustomCredit1DiscloseLocationT" value="{{sLenderCustomCredit1DiscloseLocationT}}">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                        <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </select>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                        <td>
                                            <input type="text" intro="initMask" value="{{sLenderCustomCredit1Description}}" id="sLenderCustomCredit1Description_calc" />
                                        </td>
                                        <td style="text-align: right;">
                                            <input type="text" intro="initMask" value="{{sLenderCustomCredit1Amount}}" id="sLenderCustomCredit1Amount_calc" preset="money" on-change="recalc" />
                                        </td>
                                        <td style="text-align: right;">
                                            <input readonly="true" type="text" intro="initMask" value="{{sLenderCustomCredit1AmountAsCharge}}" id="sLenderCustomCredit1AmountAsCharge" preset="money" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Repeater runat="server" ID="sLenderCustomCredit2DiscloseLocationTRep">
                                                <HeaderTemplate>
                                                    <select on-change="recalc" on-blur="domblur" id="sLenderCustomCredit2DiscloseLocationT" value="{{sLenderCustomCredit2DiscloseLocationT}}">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                        <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </select>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                        <td>
                                            <input type="text" intro="initMask" value="{{sLenderCustomCredit2Description}}" id="sLenderCustomCredit2Description_calc" />
                                        </td>
                                        <td style="text-align: right;">
                                            <input type="text" intro="initMask" value="{{sLenderCustomCredit2Amount}}" id="sLenderCustomCredit2Amount_calc" preset="money" on-change="recalc" />
                                        </td>
                                        <td style="text-align: right;">
                                            <input readonly="true" type="text" intro="initMask" value="{{sLenderCustomCredit2AmountAsCharge}}" id="sLenderCustomCredit2AmountAsCharge" preset="money" />
                                        </td>
                                    </tr>
                                </table>
                            {{/if}}
                        </td>
                        <td style="vertical-align: top;">
                            <table>
                                <tr>
                                    <td class="FieldLabel">
                                        Initial credit (-)/charge (+)
                                    </td>
                                    <td>
                                        <input readonly="true" type="text" intro="initMask" value="{{sLDiscnt}}" id="sLDiscnt_init" preset="money" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel">
                                        Total credit (-)/charge (+)
                                    </td>
                                    <td>
                                        <input readonly="true" type="text" intro="initMask" value="{{sLenderActualTotalCreditAmt_Neg}}" id="sLenderActualTotalCreditAmt_Neg" preset="money" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>

            <div id="PaidTotals" class="tabContent">
                <table>
                    <tr>
                        <td>Total fees paid by lender: 
                        </td>
                        <td>
                            <input type="text" intro="initMask" id="sGfeTotalFundByLender" value="{{sGfeTotalFundByLender}}" preset="money" width="77" readonly="True" />
                        </td>
                        <td>Total fees paid by seller: 
                        </td>
                        <td>
                            <input type="text" intro="initMask" id="sGfeTotalFundBySeller" value="{{sGfeTotalFundBySeller}}" preset="money" width="77" readonly="True" />
                        </td>
                    </tr>
                </table>
            </div>

            <div id="Dot" class="tabContent">
                <div>
                    <table>
                        <% if (!hideHeaderAndBoarder)
                           {%>
                        <tr>
                            <td class="MainRightHeader" nowrap>Details of Transaction
                            </td>
                        </tr>
                        <% } %>
                        <tr>
                            <td>
                                <table <%if (!hideHeaderAndBoarder)
                                         {%>
                                    class="InsetBorder" <% } %> style="border-color: White" id="Table1" cellspacing="0" cellpadding="0" width="700" border="0">
                                    <tr>
                                        <td nowrap>
                                            <table id="Table4" cellspacing="0" cellpadding="0" border="0">
                                                <tr>
                                                    <td></td>
                                                    <td colspan="2">Locked</td>
                                                    <td width="5"></td>
                                                    <td></td>
                                                    <td colspan="2">Locked</td>
                                                </tr>
                                                <tr>
                                                    <td>a. Purchase price</td>
                                                    <td style="width: 11px"></td>
                                                    <td style="width: 79px">
                                                      {{ #if !(IsConstruction && AreConstructionLoanDataPointsMigrated) }}
                                                        <input type="text" intro="initMask" value="{{sPurchPrice}}" id="sPurchPrice" tabindex="3" width="86" preset="money" on-change="recalc" />
                                                      {{else}}
                                                        <input type="text" readonly intro="initMask" value="{{sPurchasePrice1003}}" id="sPurchasePrice1003" tabindex="3" width="86" preset="money" on-change="recalc" />
                                                      {{/if}}
                                                     </td>
                                                    <td></td>
                                                    <td>j. <a tabindex="-1" href="javascript:linkMe(gVirtualRoot + '/newlos/LoanInfo.aspx?pg=2');">Subordinate financing</a></td>
                                                    <td style="width: 6px"></td>
                                                    <td>
                                                        <input type="text" intro="initMask" value="{{sONewFinBal}}" id="sONewFinBal" tabindex="4" width="86px" preset="money" on-change="recalc" readonly="True" /></td>
                                                </tr>
                                                <tr>
                                                    <td>b. Alterations, improvements, repairs</td>
                                                    <td style="width: 11px">
                                                        <input type="checkbox" disabled="{{IsConstruction && AreConstructionLoanDataPointsMigrated}}" checked="{{sAltCostLckd}}" disabled="{{!sIsRenovationLoan}}" id="sAltCostLckd" tabindex="3" on-change="recalc" />
                                                    </td>
                                                    <td style="width: 79px">
                                                        <input type="text" intro="initMask" readonly="{{!sAltCostLckd || (IsConstruction && AreConstructionLoanDataPointsMigrated)}}" value="{{sAltCost}}" id="sAltCost" tabindex="3" width="86px" preset="money" on-change="recalc" /></td>
                                                    <td></td>
                                                    <td>k. Borrower's closing costs paid by Seller</td>
                                                    <td style="width: 6px">
                                                        <input type="checkbox" checked="{{sTotCcPbsLocked}}" id="sTotCcPbsLocked" tabindex="4" on-change="recalc" /></td>
                                                    <td>
                                                        <input type="text" intro="initMask" readonly="{{!sTotCcPbsLocked}}" value="{{sTotCcPbs}}" id="sTotCcPbs" tabindex="4" width="86px" preset="money" on-change="recalc" /></td>
                                                </tr>
                                                <tr>
                                                    <td>c. Land (if acquired separately)</td>
                                                    <td style="width: 11px"></td>
                                                    <td style="width: 79px">
                                                      {{ #if !(IsConstruction && AreConstructionLoanDataPointsMigrated) }}
                                                        <input type="text" intro="initMask" value="{{sLandCost}}" id="sLandCost" tabindex="3" width="86px" preset="money" on-change="recalc" />
                                                      {{else}}
                                                        <input type="text" readonly intro="initMask" value="{{sLandIfAcquiredSeparately1003 }}" id="sLandIfAcquiredSeparately1003 " tabindex="3" width="86px" preset="money" on-change="recalc" />
                                                      {{/if}}
                                                    </td>
                                                    <td style="height: 23px"></td>
                                                    <td>l.
                                      <ractiveCombobox value="{{sOCredit1Desc}}" disabled="{{!sOCredit1Lckd}}" Prefix="CreditDesc" Field="sOCredit1Desc" />
                                                        <td>
                                                            <input type="checkbox" checked="{{sOCredit1Lckd}}" id="sOCredit1Lckd" disabled="{{sLoads1003LineLFromAdjustments}}" on-change="recalc" tabindex="4" /></td>
                                                    <td style="height: 23px">
                                                        <input readonly="{{!sOCredit1Lckd}}" type="text" intro="initMask" value="{{sOCredit1Amt}}" id="sOCredit1Amt" tabindex="4" width="86px" preset="money" on-change="recalc" /></td>
                                                </tr>
                                                <tr>
                                                    <td>d. Refi (incl. debts to be paid off)&nbsp;</td>
                                                    <td style="width: 11px">
                                                        <input type="checkbox" disabled="{{IsConstruction && AreConstructionLoanDataPointsMigrated}}" checked="{{sRefPdOffAmt1003Lckd}}" id="sRefPdOffAmt1003Lckd" tabindex="3" on-change="recalc" /></td>
                                                    <td style="width: 79px">
                                                        <input readonly="{{!sRefPdOffAmt1003Lckd || (IsConstruction && AreConstructionLoanDataPointsMigrated)}}" type="text" intro="initMask" value="{{sRefPdOffAmt1003}}" id="sRefPdOffAmt1003" tabindex="3" width="86px" preset="money" on-change="recalc" /></td>
                                                    <td></td>
                                                    <td>&nbsp;&nbsp;&nbsp;<ractiveCombobox value="{{sOCredit2Desc}}" Prefix="CreditDesc" Field="sOCredit2Desc" />
                                                    </td>
                                                    <td></td>
                                                    <td>
                                                        <input type="text" intro="initMask" value="{{sOCredit2Amt}}" id="sOCredit2Amt" tabindex="4" width="86px" preset="money" on-change="recalc" /></td>
                                                </tr>
                                                <tr>
                                                    <td>e. Estimated prepaid items&nbsp;</td>
                                                    <td style="width: 11px">
                                                        <input type="checkbox" checked="{{sTotEstPp1003Lckd}}" id="sTotEstPp1003Lckd" tabindex="3" on-change="recalc" /></td>
                                                    <td style="width: 79px">
                                                        <input type="text" readonly="{{!sTotEstPp1003Lckd}}" intro="initMask" value="{{sTotEstPp1003}}" id="sTotEstPp1003" tabindex="3" width="86px" preset="money" on-change="recalc" /></td>
                                                    <td></td>
                                                    <td colspan="2">&nbsp;&nbsp;&nbsp;<ractiveCombobox value="{{sOCredit3Desc}}" Prefix="CreditDesc" Field="sOCredit3Desc" />
                                                    </td>
                                                    <td>
                                                        <input type="text" intro="initMask" value="{{sOCredit3Amt}}" id="sOCredit3Amt" tabindex="4" width="86px" preset="money" on-change="recalc" /></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td style="width: 11px"></td>
                                                    <td style="width: 79px"></td>
                                                    <td></td>
                                                    <td colspan="2">&nbsp;&nbsp;&nbsp;<ractiveCombobox value="{{sOCredit4Desc}}" Prefix="CreditDesc" Field="sOCredit4Desc" />
                                                    </td>
                                                    <td>
                                                        <input type="text" intro="initMask" value="{{sOCredit4Amt}}" id="sOCredit4Amt" tabindex="4" width="86px" preset="money" on-change="recalc" /></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td style="width: 11px"></td>
                                                    <td style="width: 79px"></td>
                                                    <td></td>
                                                    <td colspan="2">&nbsp;&nbsp;&nbsp;Lender credit
                                                    </td>
                                                    <td>
                                                        <input type="text" intro="initMask" value="{{sOCredit5Amt}}" id="sOCredit5Amt" tabindex="4" width="86px" preset="money" readonly="true" /></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td style="width: 11px"></td>
                                                    <td style="width: 79px"></td>
                                                    <td></td>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Other financing closing costs</td>
                                                    <td align="right">-&nbsp;</td>
                                                    <td>
                                                        <input type="text" intro="initMask" value="{{sONewFinCc}}" id="sONewFinCc" tabindex="4" width="86px" preset="money" on-change="recalc" /></td>
                                                </tr>
                                                <tr>
                                                    <td>f. Estimated closing costs</td>
                                                    <td style="width: 11px">
                                                        <input type="checkbox" checked="{{sTotEstCc1003Lckd}}" id="sTotEstCc1003Lckd" tabindex="3" on-change="recalc" /></td>
                                                    <td style="width: 79px">
                                                        <input readonly="{{!sTotEstCc1003Lckd}}" type="text" intro="initMask" value="{{sTotEstCcNoDiscnt1003}}" id="sTotEstCcNoDiscnt1003" tabindex="3" width="86px" preset="money" on-change="recalc" /></td>
                                                    <td colspan="2">m. Loan amount (exclude PMI, MIP, FF financed)</td>
                                                    <td style="width: 6px">
                                                        <input type="checkbox" disabled="<%= AspxTools.JsBool(!m_isPurchase || this.isRenovationLoan) %>" checked="{{sLAmtLckd}}" id="sLAmtLckd" on-change="recalc" tabindex="202" /></td>
                                                    <td>
                                                        <input type="text" intro="initMask" value="{{sLAmtCalc}}" id="sLAmtCalc" tabindex="4" readonly="{{!sLAmtLckd}}" on-change="recalc" width="86px" preset="money" /></td>
                                                </tr>
                                                <tr>
                                                    <td>g. PMI,&nbsp;MIP, Funding Fee</td>
                                                    <td style="width: 11px">
                                                        <input type="checkbox" checked="{{sFfUfmip1003Lckd}}" id="sFfUfmip1003Lckd" tabindex="3" on-change="recalc" /></td>
                                                    <td style="width: 79px">
                                                        <input readonly="{{!sFfUfmip1003Lckd}}" type="text" intro="initMask" value="{{sFfUfmip1003}}" id="sFfUfmip1003" tabindex="3" width="86px" preset="money" on-change="recalc" /></td>
                                                    <td></td>
                                                    <td>n. PMI, MIP, Funding Fee financed</td>
                                                    <td style="width: 6px"></td>
                                                    <td>
                                                        <input type="text" intro="initMask" value="{{sFfUfmipFinanced}}" id="sFfUfmipFinanced" tabindex="4" readonly="True" width="86px" preset="money" /></td>
                                                </tr>
                                                <tr>
                                                    <td>h. Discount (if Borrower will pay)</td>
                                                    <td style="width: 11px">
                                                        <input type="checkbox" checked="{{sLDiscnt1003Lckd}}" id="sLDiscnt1003Lckd" tabindex="3" on-change="recalc" /></td>
                                                    <td style="width: 79px">
                                                        <input type="text" readonly="{{!sLDiscnt1003Lckd}}" intro="initMask" value="{{sLDiscnt1003}}" id="sLDiscnt1003" tabindex="3" width="86px" preset="money" on-change="recalc" /></td>
                                                    <td></td>
                                                    <td>o. Loan amount (add m &amp; n)</td>
                                                    <td style="width: 6px"></td>
                                                    <td>
                                                        <input type="text" intro="initMask" value="{{sFinalLAmt}}" id="sFinalLAmt" tabindex="4" readonly="True" width="86px" preset="money" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel">i. Total costs (add items a to h)</td>
                                                    <td style="width: 11px"></td>
                                                    <td style="width: 79px">
                                                        <input type="text" intro="initMask" value="{{sTotTransC}}" id="sTotTransC" tabindex="3" readonly="True" width="86px" preset="money" /></td>
                                                    <td></td>
                                                    <td>p. Cash from / to Borr (subtract j, k, l &amp; o from i)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                    <td style="width: 6px">
                                                        <input type="checkbox" checked="{{sTransNetCashLckd}}" id="sTransNetCashLckd" tabindex="4" on-change="recalc" /></td>
                                                    <td>
                                                        <input type="text" readonly="{{!sTransNetCashLckd}}" intro="initMask" value="{{sTransNetCash}}" id="sTransNetCash" tabindex="4" width="86px" preset="money" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
  </script>

  </form>

  <script>
    
    var globalRActive;
    
     function checkHudLines(tbody) 
     {
        var h = {};
        
        tbody.find('input.hudline[readonly]').each(function(i,o) {
         var prop = 'n' + o.value;
            h[prop] = true;
        });
        
        tbody.find('input.hudline:not([readonly])').each(function(i,o) {
            var $o = $j(this),
                i = parseInt(o.value),
                r = i >= parseInt($o.attr('data-hud-start')) && i <= parseInt($o.attr('data-hud-end')),
                td = $o.closest('td'),
                ti =$j();
            
            td.find('img').remove();
            $o.toggleClass('invalid', !r);     
            
            if (!r){
                if (ti.length == 0) {
                    ti = $j('<img/>', {
                        src : VRoot + '/images/fail.png',
                        align : 'bottom',
                        style : 'margin-left: 3px;'
                    }).appendTo(td);
                }
                
                ti.attr('title', 'You can only enter a HUD line number between ' + $o.attr('data-hud-start') + ' and ' + $o.attr('data-hud-end') +'.');
            }
               
            if (o.value == $j(o).attr('data-hud-start')){
                return;
            }
            
            if (!r){
                return; 
            }
                
            var prop = 'n' + o.value;
            if (h.hasOwnProperty(prop)){
                $o.addClass('dupe');
                if (ti.length == 0) {
                    ti = $j('<img/>', {
                        src : VRoot + '/images/fail.png',
                        align : 'bottom',
                        style : 'margin-left: 3px;'
                    }).appendTo(td);
                }
                var t = ti.attr('title');
                if (!t){
                    t = '';
                }
                ti.attr('title', t + ' You cannot enter duplicate HUD line numbers.');
            }
            else {
                $o.removeClass('dupe');
                
                if (!$o.attr('data-is-sub-fee')) { // Don't mark subfees with same HUD line number as duplicates. 
                    h[prop] = true;
                }
            }
        });
    }
        
        
    $j(function(){
        
        $j(document).delegate('input.hudline:not([readonly])', 'change', function(){
            checkHudLines($j(this).closest('tbody'));
        });
        
        
        function lqblog(msg){
            if (window.console && window.console.log){
                window.console.log(msg);
            }
        }
        
        var initMask = function(o) {
            _initMask(o.node, true);
            o.complete();
        };
        
        var sectionTransition = function(t) {
            if (t.isIntro) {
                var node = $j(t.node);
                var section = node.text();

                var selector = determineShownSections(t.params.hudline, t.params.isSystem, t.params.gfeGroup);
                node.find("option").not(selector).remove();

                var numVisible = node.find("option").length;
                if (numVisible < 2) {
                    //Pseudo disable it so it looks better
                    node.mousedown(function(event) { event.preventDefault(); });
                    node.css("background-color", "white");
                    node.css("color", "grey");
                }
                if (numVisible < 1) {
                    node.css('display', 'none');
                }
            }
        }

        function determineShownSections(hudline, isSystem, gfeGroup) {
            var selector = "";

            // Create the selectors for the custom fees first.
            if (!isSystem) {
                if (hudline >= 800 && hudline <= 899) {
                    selector = addContainsSelector(selector, "A1");
                    selector = addContainsSelector(selector, "B3");
                    selector = addContainsSelector(selector, "N/A");
                } else if (hudline >= 900 && hudline <= 999) {
                    selector = addContainsSelector(selector, "B3");
                    selector = addContainsSelector(selector, "B11");
                    selector = addContainsSelector(selector, "N/A");
                } else if (hudline >= 1000 && hudline <= 1099) {
                    selector = addContainsSelector(selector, "B9");
                } else if (hudline >= 1100 && hudline <= 1199) {
                    selector = addContainsSelector(selector, "B4");
                    selector = addContainsSelector(selector, "B6");
                    selector = addContainsSelector(selector, "N/A");
                } else if (hudline >= 1200 && hudline <= 1299) {
                    selector = addContainsSelector(selector, "B7");
                    selector = addContainsSelector(selector, "B8");
                } else if (hudline >= 1300 && hudline <= 1399) {
                    selector = addContainsSelector(selector, "B4");
                    selector = addContainsSelector(selector, "B6");
                    selector = addContainsSelector(selector, "N/A");
                }

                return selector;
            }

            if (gfeGroup === 0) {
                selector = addContainsSelector(selector, "N/A");
            }
            else if (gfeGroup === 1) {
                selector = addContainsSelector(selector, "A1");
            }
            else if (gfeGroup === 2) {
                selector = addContainsSelector(selector, "A2");
            }
            else if (gfeGroup === 3) {
                selector = addContainsSelector(selector, "B3");
            }
            else if (gfeGroup === 4) {
                selector = addContainsSelector(selector, "B4");
            }
            else if (gfeGroup === 5) {
                selector = addContainsSelector(selector, "B5");
            }
            else if (gfeGroup === 6) {
                selector = addContainsSelector(selector, "B6");
            }
            else if (gfeGroup === 7) {
                selector = addContainsSelector(selector, "B7");
            }
            else if (gfeGroup === 8) {
                selector = addContainsSelector(selector, "B8");
            }
            else if (gfeGroup === 9) {
                selector = addContainsSelector(selector, "B9");
            }
            else if (gfeGroup === 10) {
                selector = addContainsSelector(selector, "B10");
            }
            else if (gfeGroup === 11) {
                selector = addContainsSelector(selector, "B11");
            }
            else if (gfeGroup === 12) {
                selector = addContainsSelector(selector, "B4");
                selector = addContainsSelector(selector, "B6");
            }
            else {
                selector = addContainsSelector(selector, "N/A");
            }

            return selector;
        }

        function addContainsSelector(selector, data) {
            if (selector != "") {
                selector += ",";
            }

            selector += ":contains('" + data + "')";
            return selector;
        }
        
        Ractive.transitions.initMask = initMask;
        Ractive.transitions.sectionIntro = sectionTransition;

        $j(document).on('change', 'input,select', updateDirtyBit);
        
        function highlightLI(el)
        {
            el.css("background-color", "yellow");
            el.attr("class", "selected");
        }
        
        function unhighlightLI(el)
        {
            el.css("background-color", "");
            el.removeAttr("class");
        }
        
        function callWebMethod(webMethodName, data, error, success, pageName) {
            if (!pageName){
                pageName = 'RActiveGFE.aspx/';
            }
            
            var settings = {
                async: false, 
                type: 'POST',
                url : pageName + webMethodName,
                data : JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                error : error,
                success : function(d){ checkWebErrorWithName(d, webMethodName, success); }
            };

            callWebMethodAsync(settings);
        }
        
        var calculationModalContent = Ractive.parse(document.getElementById('CalculationModalContent').innerHTML),
            feePickerModalContent = Ractive.parse(document.getElementById('FeePickerModalContent').innerHTML);
        var subFeeConfirmOverwriteContent = Ractive.parse(document.getElementById('SubFeeConfirmOverwriteContent').innerHTML);
    
        Modal = Ractive.extend({
            el: document.body,
            append: true,
            template:  '#modal',

            init: function () {
                var self = this, resizeHandler;
                this.outer = this.find( '.modal-outer' );
                this.modal = this.find( '.modal' );

                this.on( 'close', function ( event ) {
                    this.teardown();
                });

                $j('window').resize(self.center); 
                
                this.on( 'teardown', function () {
                  $j('window').unbind('resize', self.center);
                }, false );

                this.center();
        },
        
        center: function () {
            var outerHeight, modalHeight, verticalSpace;
            outerHeight = this.outer.clientHeight;
            modalHeight = this.modal.clientHeight;
            verticalSpace = ( outerHeight - modalHeight ) / 2;
            this.modal.style.top = verticalSpace + 'px';
        }
    });
    
    var FeePickerModal = Modal.extend({
        partials : {
            modalContent : feePickerModalContent
        }
    });

    var SubFeeConfirmOverwriteModal = Modal.extend({
        partials: {
            modalContent: subFeeConfirmOverwriteContent
        }
    });
    
    var CalculationModal = Modal.extend({
      partials : { 
        modalContent : calculationModalContent
      }, 
      
      init: function ( options ) {
        // wherever we overwrite methods, such as `init`, we can call the
        // overwritten method as `this._super`
        var that = this;
        
        var originalData = $j.extend(true, {}, that.get());
        
        this.on('close', function(e, update){
            //this is needed for mask.js
            
            if (!update) {
                $j.extend(that.get("fee"), originalData.fee);
                $j.extend(that.get("loan"), originalData.loan);
            }
            else
            {
               that.updateModel();
            }
                 
        });
        
        this.on('recalc', function(){
            ractive.fire('recalc');
            window.setTimeout(function(){
                ractive.update();
                that.get("loan") = ractive.get();
                that.update();
            }, 0);
        });
        
        this._super( options );
    
      },
    });
    var startTime = Date.now();
    
          var TimeControl = Ractive.extend({
            template: '#TimeControl',
            data:{value: '',
                id: '',
                readonly: false
                },
            init: function(){
                this.on('minuteBlur', function(e)
                {
                    time_onminuteblur(e.node);
                });
                
                this.on('hourKeyUp', function(e)
                {
                    time_onhourkeyup(e.node, e.node.id + '_minute', e.original );
                });
                
                this.on('hourBlur', function(e)
                {
                    time_onhourblur(e.node);
                });
            }
          
          });

          var DatePicker = Ractive.extend({
            template: '#DatePicker',
            data: {
                value: '',
                id: '',
                readonly: false
                
            },
            init: function(){
                var self = this;
            
                this.on('showCalendar', function(e,id){
                    return displayCalendar(id, function(cal) { 
                        if (cal && cal.sel) {
                            queueRactiveUpdate(ractive, self, cal.sel.value);
                            cal.hide();
                        }
                    });
                });
                this.on('domblur', function(event){
                    var kp = event.keypath;
                    //sadly this blur event fires before the mask.js blur event 
                    //by using setTimeout we queue up the function until after all the other
                    //js code.
                    window.setTimeout(function(){
                        self.updateModel("value", true);
                    });
                 });
                 
                this.on('updatePicker', function(event)
                {
                    window.setTimeout(function(){
                        queueRactiveUpdate(ractive, self, event.node.value);
                    }, 0);;
                });
              }   
          });
          
          var RactiveCombobox = Ractive.extend({
              template: '#RactiveCombobox',
              data: {
                  value: '',
                  Prefix: '',
                  disabled: '',
                  Field: ''
              },
              init: function() {

                  
                if(<%=AspxTools.JsBool(!IsReadOnly) %>)
                {
                      this.on('showOptions', function(event, i, j, k, OptionsId) {
                          if(!this.get("disabled"))
                          {
                              var options = $j("." + OptionsId);
                              options.toggle();

                              var node = $j(event.node);
                              if (node.prop('tagName').toLowerCase() == 'input') {
                              
                                  //called from the input
                                  var img = node.parent().find('img');
                                  img.after(options);
                              }
                              else {
                              
                                  //called from the img
                                  node.after(options);
                                  node = node.parent().find("input");
                                  node.focus();
                             }
                         }
                      });

                      this.on('hideDesc', function(event, OptionsId) {
                          var options = $j("." + OptionsId);

                          if (canHideDescriptions) {
                              options.hide();
                              $j("#OptionsContainer").append(options);
                          }

                      });

                      this.on('keyScroll', function(event){
                      
                        var code = event.original.which; //get the keycode
                        var ul = $j(event.node).parent().find('ul'); //get the ul
                        
                        var selectedLI = ul.find(".selected");
                        
                        var hasSelectedLI = selectedLI.length > 0;

                        switch(code)
                        {
                            case 13:
                                if(hasSelectedLI)
                                {
                                    selectedLI.click();
                                }
                            break;
                            case 38: //up
                                if(hasSelectedLI){
                                   var nextLI = selectedLI.prev();
                                   
                                   if(nextLI.length > 0){
                                        unhighlightLI(selectedLI);
                                        highlightLI(nextLI);
                                        ul.scrollTop(ul.scrollTop() + nextLI.position().top - ul.height()/2);
                                   }
                                }
                                else{
                                        //select the first LI if there is none
                                        highlightLI(ul.find('li').first());
                                }
                            break;
                            case 40: //down
                            
                                if(!ul.is(":visible")){
                                   $j(event.node).click();
                                }
                                
                                if(hasSelectedLI){
                                
                                   var nextLI = selectedLI.next();
                                   if(nextLI.length > 0){
                                        unhighlightLI(selectedLI);
                                        highlightLI(nextLI);
                                        ul.scrollTop(ul.scrollTop() + nextLI.position().top - ul.height()/2);
                                   }
                                }
                                else{
                                        //select the first LI if there is none
                                        highlightLI(ul.find('li').first());
                                }
                            break;
                        }
                      });

                      this.on('recalc', function(event, success) {

                          ractive.fire('recalc');

                      });
                  }
              }
          });
          
          var RactiveDDL = Ractive.extend({
              template: '#RactiveDDL',
              data: {
                  value: '',
                  Prefix: '',
                  disabled: ''
              },
              init: function() {

                  var input = $j(this.el).find(".RactiveDDL");
                  var selectedListItem = $j("."+ this.get("Prefix") + "Options li[val='" + this.get("value") + "']");
                  input.val(selectedListItem.text());
                  
                if(<%=AspxTools.JsBool(!IsReadOnly) %>)
                {
                      this.on('showOptions', function(event, i, j, k, OptionsId) {
                          if(!this.get("disabled"))
                          {
                              var options = $j("." + OptionsId);
                              options.toggle();

                              currentFeeRow = j;
                              currentSection = i;
                              if(OptionsId == 'PaidBySingleOptions' || (OptionsId == 'PayableOptions' && k == undefined))
                              {
                                k = 0;
                              }
                              currentPayment = k;

                              var node = $j(event.node);
                              if (node.prop('tagName').toLowerCase() == 'input') {
                              
                                  //called from the input
                                  var img = node.parent().find('img');
                                  img.after(options);
                              }
                              else {
                              
                                  //called from the img
                                  node.after(options);
                                  node = node.parent().find("input");
                                  node.focus();
                             }
                         }
                      });

                      this.on('hideDesc', function(event, OptionsId) {
                          var options = $j("." + OptionsId);

                          if (canHideDescriptions) {
                              options.hide();
                              $j("#OptionsContainer").append(options);
                          }

                      });

                      this.on('keyScroll', function(event){
                      
                        var code = event.original.which; //get the keycode
                        var ul = $j(event.node).parent().find('ul'); //get the ul
                        
                        var selectedLI = ul.find(".selected");
                        
                        var hasSelectedLI = selectedLI.length > 0;

                        switch(code)
                        {
                            case 13:
                                if(hasSelectedLI)
                                {
                                    selectedLI.click();
                                }
                            break;
                            case 38: //up
                                if(hasSelectedLI){
                                   var nextLI = selectedLI.prev();
                                   
                                   if(nextLI.length > 0){
                                        unhighlightLI(selectedLI);
                                        highlightLI(nextLI);
                                        ul.scrollTop(ul.scrollTop() + nextLI.position().top - ul.height()/2);
                                   }
                                }
                                else{
                                        //select the first LI if there is none
                                        highlightLI(ul.find('li').first());
                                }
                            break;
                            case 40: //down
                            
                                if(!ul.is(":visible")){
                                   $j(event.node).click();
                                }
                                
                                if(hasSelectedLI){
                                
                                   var nextLI = selectedLI.next();
                                   if(nextLI.length > 0){
                                        unhighlightLI(selectedLI);
                                        highlightLI(nextLI);
                                        ul.scrollTop(ul.scrollTop() + nextLI.position().top - ul.height()/2);
                                   }
                                }
                                else{
                                        //select the first LI if there is none
                                        highlightLI(ul.find('li').first());
                                }
                            break;
                        }
                      });

                      this.on('recalc', function(event, success) {

                          ractive.fire('recalc');

                      });
                  }
              }
          });

    var ractive = new Ractive({
      // The `el` option can be a node, an ID, or a CSS selector.
      el: 'container',
      append: false,

      // We could pass in a string, but for the sake of convenience
      // we're passing the ID of the <script> tag above.
      template: '#ClosingCostTemplate',
      partials : '#ClosingCostFeeTemplate,#ClosingCostFeePaymentTemplate,#Footer,#DatePicker',

              components: { ractiveDDL: RactiveDDL, ractiveCombobox: RactiveCombobox, datePicker: DatePicker, timeControl: TimeControl },
      // Here, we're passing in some initial data
      data: ClosingCostData,
      
      init : function() {
        
      }
    });
    
    var observer = ractive.observe('SectionList.*.ClosingCostFeeList.*.bene', function(newValue, oldValue, keypath, i, j, k){
        if(oldValue != null && newValue != null && oldValue != newValue)
        {
            var splitArray = keypath.split(".")
            var field = splitArray[splitArray.length - 1];
            
            var prefix = "Beneficiary";
            
            var DDLClass = prefix + "DDL.RactiveDDL" + i + j;
            
            var OptionsClass = prefix + "Options";
            
            $j("." + DDLClass).val( $j("." + OptionsClass).find("li[val='"+newValue+"']").text());
        }
    });
    
    $j(".RactiveDDL").css("width", $j(".BeneficiaryOptions").width());
	$j(".OptionsList").css("width", $j(".OptionsList").width() + $j(".DDLImg").width() + 2);
	
	$j(".RactiveCombobox").css("width", $j(".CreditDescOptions").width());
	$j(".CreditDescOptions").css("width", $j(".CreditDescOptions").width() + $j(".DDLImg").width() + 2);
	
	ractive.on('onContactHover', function(event){
	    var el = $j(event.node);
	    el.attr("src", '../../images/contacts_clicked.png');
	});
	
	ractive.on('onContactMouseOut', function(event){
	    var el = $j(event.node);
	    el.attr("src", '../../images/contacts.png');
	});
	
	ractive.on('showAgentPicker', function(event, i, j){
	    var el = $j(event.node);
        var rolodex = new cRolodex();
    
        var type = el.parent().find("input[type='hidden']").val();

        rolodex.chooseFromRolodex(type, ML.sLId, true, false, function(args){
            if (args.OK == true) {
                if(args.Clear == true) {
                    $j("#ClearAgent").dialog({
                        modal: true,
                        height: 150,
                        width: 350,
                        title: "",
                        dialogClass: "LQBDialogBox",
                        resizable: false,
                        buttons: [
                            {text: "OK",
                                click: function(){
                                    // Remove Agent from official contacts
                                    var model = ractive.get();
                                    var data = {
                                        loanId: ML.sLId,
                                        viewModelJson: JSON.stringify(model),
                                        recordId: ractive.get("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id"),
                                        IsArchive: model.IsArchive
                                    }
                                
                                    callWebMethod("ClearAgent", 
                                        data,
                                        function(){
                                            alert("Error:  Could not remove agent.");
                                        }, 
                                        //On Success
                                        function(m){
                                            model = JSON.parse(m.d);
                                            ractive.set(model);
                                        });
                                
                                    // TODO: TURN ALL THESE WEB METHOD CALLS INTO SERVICE CALLS
    //                                // Remove Agent from official contacts
    //                                var model = ractive.get();
    //                                var args = new Object();
    //                                args["loanid"] = ML.sLId;
    //                                args["IsArchivePage"] = model.IsArchive;
    //                                args["sFileVersion"] = document.getElementById("sFileVersion").value;
    //                                args["RecordId"] = ractive.get("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id");
    //                                  
    //                                var result = gService.loanedit.call("ClearAgent", args);
    //                                
    //                                if (result.error) {
    //                                    alert("Error:  Could not remove agent.");
    //                                    $j(this).dialog("close");
    //                                    return;               
    //                                }
    //                                
    //                                model = JSON.parse(result.value.viewModel);
    //                                ractive.set(model);
                                    
    ////                                updateDirtyBit();
    ////                                ractive.fire('recalc');
                                    $j(this).dialog("close");
                                }
                            },
                            {text: "Cancel",
                                click: function(){
                                    $j(this).dialog("close");
                                }
                            }
                        ]
                    });
                } else {
                    var id = "";
                    var populateFromRolodex = false;
                    var agentType = args.AgentType;
                        
                    if(args.RecordId != "" && args.RecordId != <%=AspxTools.JsString(Guid.Empty) %> )
                    {
                        //If they're choosing from an agent record, simply set it's record id as the fee's bene_id.
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id", args.RecordId);
                        //Update paidTo DDL
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene", agentType);
                        //Re-enable beneficiary automation.
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".disable_bene_auto", false);
                        
                        updateDirtyBit();
                        ractive.fire('recalc');
                    }
                    else
                    {
                        if(args.BrokerLevelAgentID != <%=AspxTools.JsString(Guid.Empty) %>)
                        {
                            //If the selcted contact has no record id, then add it to the agents list.
                            //If there is a BrokerLevelAgentID, populate using the rolodex and that id
                            id=args.BrokerLevelAgentID;
                            populateFromRolodex = true;  
                        }
                        else
                        {
                            //If the selcted contact has no record id, then add it to the agents list.
                            //If there isn't a BrokerLevelAgentID, populate using the employee info
                            
                            id=args.EmployeeId;
                            populateFromRolodex = false;  
                        }
                        
                        $j("#ConfirmAgent").dialog({
                            modal: true,
                            height: 150,
                            width: 350,
                            title: "",
                            dialogClass: "LQBDialogBox",
                            resizable: false,
                            buttons: [
                                {text: "OK",
                                click: function(){
                                    callWebMethod("CreateAgent", 
                                        {loanId: ML.sLId, id: id, populateFromRolodex: populateFromRolodex, agentType: agentType},
                                        function(){alert("Error:  Could not create a new agent.");}, 
                                        //On Success
                                        function(d){
                                            var m = JSON.parse(d.d);
                                            // Get the recordID and set it for the bene_id
                                            recordId = m.RecordId;
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id", recordId);
                                            //Update the fee's paidTo DDL
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene", agentType);
                                            //Re-enable beneficiary automation.
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".disable_bene_auto", false);
                                            
                                            updateDirtyBit();
                                            ractive.fire('recalc');
                                            
                                        });
                                    $j(this).dialog("close");
                                    }
                                },
                                {text: "Cancel",
                                    click: function(){
                                        $j(this).dialog("close");
                                    }
                                }
                            ]
                        });
                    }
                }
            }
        });
	});
    
    ractive.on('domblur', function(event){
        var kp = event.keypath;
        //sadly this blur event fires before the mask.js blur event 
        //by using setTimeout we queue up the function until after all the other
        //js code.
        window.setTimeout(function(){
            ractive.updateModel(kp, true);
        });
    });
    
    ractive.on('delete', function(event, i, j, desc, amt, orgDesc){
        var tbody = $j(event.node).closest('tbody');
        var d = desc;
        if (!d)
        {
            d = orgDesc;
        }
        $j('#dialog-confirm').find('p').html('Are you sure you would like to remove the following fee? <br />').append(document.createTextNode(d + ' : ' + amt));
        $j('#dialog-confirm').dialog({
            modal: true,
            buttons: {
                "Yes": function() {
                    $j(this).dialog("close");
                    ClosingCostData.SectionList[i].ClosingCostFeeList.splice(j,1);
                    updateDirtyBit();
                    checkHudLines(tbody);
                },
                "No": function()
                {
                    $j(this).dialog("close");
                }
            },
            closeOnEscape: false,
            width: "400",
            draggable: false,
            dialogClass: "LQBDialogBox",
            resizable: false
        });
    });

    ractive.on('updatePaidBy', function(e, i, j, total){
        var obj = e.context, 
            node = e.node,
            value = node.value ;
            
        if (value == -1){
               var model = ractive.get(), 
                   fee = model.SectionList[i].ClosingCostFeeList[j];
               fee.pmts[0].paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Borrower.ToString("D")) %>;
               
               // Add new system payment
               var newPayment = new Object();
               newPayment.amt = "";
               newPayment.ent = 0;
               newPayment.is_fin = false;
               newPayment.is_system = true;
               newPayment.made = false;
               newPayment.paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Borrower.ToString("D")) %>;
               newPayment.pmt_at = <%= AspxTools.HtmlString(E_GfeClosingCostFeePaymentTimingT.AtClosing.ToString("D")) %>;
               newPayment.pmt_dt = "";
               fee.pmts.push(newPayment);
               
               updateDirtyBit();
        }
        else if(value == 3)
        {
            obj.pmts[0].paid_by = value; 
            obj.pmts[0].pmt_at = 1; // Set to 'at closing'
            ractive.update(e.keypath);
        }
        else {
            obj.pmts[0].paid_by = value; 
            ractive.update(e.keypath);
        }
        
        ractive.fire('recalc');
    });
    
    ractive.on('updatePaidBySplit', function(e){
        var obj = e.context, 
            node = e.node,
            value = node.value ;
            
        if(value == 3)
        {
            obj.paid_by = value; 
            obj.pmt_at = 1; // Set to 'at closing'
            ractive.update(e.keypath);
        }
        else {
            obj.paid_by = value; 
            ractive.update(e.keypath);
        }
        
        ractive.fire('recalc');
    });
    
    ractive.on('AddSplitPayment', function(event, i, j){
        updateDirtyBit();

        var model = ractive.get(), 
        fee = model.SectionList[i].ClosingCostFeeList[j];

        var newPayment = new Object();
        newPayment.amt = "";
        newPayment.ent = 0;
        newPayment.is_fin = false;
        newPayment.is_system = false;
        newPayment.made = false;
        newPayment.paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Borrower.ToString("D")) %>;
        newPayment.pmt_at = <%= AspxTools.HtmlString(E_GfeClosingCostFeePaymentTimingT.AtClosing.ToString("D")) %>;
        newPayment.pmt_dt = "";
        fee.pmts.push(newPayment);
        
        ractive.fire('recalc');
    });
    
    ractive.on('updateCalc', function(event, i, j){
       
        var o = this.get('SectionList');
        var m = new CalculationModal({ data : {fee: o[i].ClosingCostFeeList[j].f, loan: this.get()}});
        m.on('teardown', function(){
            ractive.fire('recalc');
        });
    });
    
    ractive.on('toggleTimeControl', function(event, prefix)
    {
       //sIsPrintTimeForGfeNoteIRAvailTillD 
       //{{sGfeNoteIRAvailTillD_Time_hour}}" value_min="{{sGfeNoteIRAvailTillD_Time_minute}}" value_am="{{sGfeNoteIRAvailTillD_Time_am}}" id="sGfeNoteIRAvailTillD_Time"  readonly="{{!sIsPrintTimeForGfeNoteIRAvailTillD}}"/>
       
       var editable = event.node.checked;
       if(!editable)
       {
            ractive.set("s" + prefix + "_Time_hour", "12");
            ractive.set("s" + prefix + "_Time_minute", "00");
            ractive.set("s" + prefix + "_Time_am", "AM");
            ractive.set("s" + prefix + "TimeZoneT", 0);
       }
    });
    
    ractive.on('recalc', function(event, success){
        if(!ractive.get("ByPassBgCalcForGfeAsDefault") || manualUpdate)
        {
            window.setTimeout(function(){
            ractive.set('GfeTilAgentRoleT', $j("#CFM_m_officialContactList").val());
            ractive.set('GfeTilIsLocked', $j("#CFM_m_rbManualOverride").prop("checked"));
            
            var startRecalc = Date.now(),
                model = ractive.get(), 
                data = { loanId: ML.sLId, viewModelJson: JSON.stringify(model), IsArchive: model.IsArchive },  
                fin = function(m) {
                     var load = Date.now();
                     lqblog('Time To Load Data ' + (load - startRecalc) + 'ms.');
                     model = JSON.parse(m.d);
                     var d = Date.now();
                     
                     model.ByPassBgCalcForGfeAsDefault = ractive.get("ByPassBgCalcForGfeAsDefault");

                     ractive.set(model);
                  
                     var c = Date.now();
                     lqblog('Time to set data ' + ( c-d) + 'ms.');
                     if (success){
                        success();
                     }
                },
                error = function(e){
                    alert('Error');
                };
                
            callWebMethod('CalculateData', data, error, fin);
            });
        }
    });
    
    function GetPaidByDesc(paidby)
    {
        var paidByDesc = "";
        if(paidby == 1)
        {
            paidByDesc = "borr pd";
        }
        else if(paidby == 2)
        {
            paidByDesc = "seller";
        }
        else if(paidby == 3)
        {
            paidByDesc = "borr fin";
        }
        else if(paidby == 4)
        {
            paidByDesc = "lender";
        }
        else if(paidby == 5)
        {
            paidByDesc = "broker";
        }
        else if(paidby == 6)
        {
            paidByDesc = "other";
        }
        
        return paidByDesc;
    }
           
    ractive.on('removePayment', function(e,i, j, k, amt, paidby, desc, orgDesc){
        var d = desc;
        if (!d)
        {
            d = orgDesc;
        }
        var paidByDesc = GetPaidByDesc(paidby);
        
        $j('#dialog-confirm').find('p').text('Are you sure you would like to remove the ' + amt + ' ' + paidByDesc + ' split payment for the ' + d + '?');
        
        $j('#dialog-confirm').dialog({
            modal: true,
            buttons: {
                "Yes": function()
                {
                    $j(this).dialog("close");
                    updateDirtyBit();
                    var model = ractive.get(); 
                    model.SectionList[i].ClosingCostFeeList[j].pmts.splice(k,1);
                    ractive.fire('recalc');
                },
                "No": function()
                {
                    $j(this).dialog("close");
                }
            },
            closeOnEscape: false,
            width: "400",
            draggable: false,
            dialogClass: "LQBDialogBox",
            resizable: false
        });
    });
    
    ractive.on('addFee', function(event,i){
         var model = ractive.get(),
             section =  model.SectionList[i],
             sectionName = section.SectionName,
             data = { loanId: ML.sLId, viewModelJson: JSON.stringify(model),  sectionName : sectionName  }; 
         
         callWebMethod('GetAvailableClosingCostFeeTypes', data, 
         
         function(){
            alert('Error'); 
         },
         
         function(m) {
            var result = JSON.parse(m.d);
            var y = new FeePickerModal({data : result.Section });
            y.on('selectFee', function(e, si){
                // OPM 468071 - Check if selected fee has existing sub fees.
                var selectedFee = result.Section.FeeListAsBorrowerFees[si];

                var subFees;
                for (var idx=0; idx< result.SubFeesBySourceFeeTypeId.length; idx++)
                {
                    if (result.SubFeesBySourceFeeTypeId[idx].SourceFeeTypeId == selectedFee.typeid)
                    {
                        subFees = result.SubFeesBySourceFeeTypeId[idx].SubFees;
                        break;
                    }
                }

                if (typeof(subFees) !== "undefined" && subFees.length > 0) {
                    // Confirm sub fee overwrite.
                    var confirmDlg = new SubFeeConfirmOverwriteModal({ data: {SubFees: subFees, ParentDesc: selectedFee.desc } });
                    confirmDlg.on('subFeeDlgOk', function(e, si) {
                        addSelectedFee(selectedFee, section, i);
                        confirmDlg.teardown();
                        y.teardown();
                        return false;
                    });
                }
                else {
                    // No sub fees, just add seleced fee.
                    addSelectedFee(selectedFee, section, i);
                    y.teardown();
                    return false;
                }

                return false;
            });
         });
    });

    function addSelectedFee (selectedFee, feeSection, sectionIndex) {
        feeSection.ClosingCostFeeList.push(selectedFee);
        updateDirtyBit();
        $j(".RactiveDDL" + sectionIndex + (feeSection.ClosingCostFeeList.length - 1)).css("width", $j(".OptionsList").width() - $j(".DDLImg").width() - 2);
        checkHudLines($j(event.node).closest('table').find('tbody'));
        ractive.fire('recalc');
    }

    ractive.on('showCalendar', function(e,id){
        return displayCalendar(id);
    });
    
    ractive.on('CanShopChange', function(e, i, j){
        var keypath = 'SectionList.' + i + '.ClosingCostFeeList.' + j + '.did_shop';
        var can_shop = $j(e.node).prop('checked');
        if(!can_shop)
        {
            ractive.set(keypath, false);
        }
    });
    
    ractive.on('tpClick', function(e, i, j){
        var model = ractive.get(); 
        if ( !model.SectionList[i].ClosingCostFeeList[j].tp )
        {
            model.SectionList[i].ClosingCostFeeList[j].aff = false;
            ractive.set(model);
        }
    });
    
    var canHideDescriptions = true;
    var currentFeeRow = 0;
    var currentSection = 0;
    var currentPayment = 0;

        ractive.on('onHoverDescriptions', function(event) {
            canHideDescriptions = false;
        });

        ractive.on('onMouseOutDescriptions', function(event) {
            canHideDescriptions = true;
        });

        ractive.on('highlightLI', function(event) {
            var node = $j(event.node);
            var allLI = node.parent().find('li');
            unhighlightLI(allLI);
            highlightLI(node);
            
        });

        ractive.on('unhighlightLI', function(event) {
            var node = $j(event.node);
            unhighlightLI(node);
        });
        
        ractive.on('selectCombobox', function(event){
            var node = $j(event.node);
            var text = node.text();
            
            //get the keypath to be updated
            var keypath = node.parent().parent().find("input").attr("field");
            
            ractive.set(keypath, text);
            updateDirtyBit(event.original);
            canHideDescriptions = true;
            
            node.parent().hide();
             $j("#OptionsContainer").append(node.parent());
        });


        ractive.on('selectDDL', function(event) {
            var node = $j(event.node);
            var text = node.text();
              
            var options = node.parent();

            var td = node.parent().parent();
            
              td.find(".RactiveDDL").val(node.text());
              td.find("input[type='hidden']").val(node.attr('val'));
              
              
            var value = node.attr('val');
              
            node.parent().hide();
            $j("#OptionsContainer").append(node.parent());
            canHideDescriptions = true;
             
            // Get Agent Id
            var args = new Object();
            args["loanid"] = ML.sLId;
            args["IsArchivePage"] = ractive.get("IsArchive");
            args["AgentType"] = value;
              
            var result = gService.cfpb_utils.call("GetAgentIdByAgentType", args);
            
            if (!result.error) {
                //Update the fee's Beneficiary ID
                ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene_id", result.value.RecordId);
                // Update the fee's Beneficiary Description
                ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene_desc", result.value.CompanyName);
                //Update the fee's paidTo DDL
                ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene", value);
                //Re-enable beneficiary automation.
                ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".disable_bene_auto", false);
            }
            
            updateDirtyBit();
            ractive.fire('recalc'); // Sets TP & AFF bits.
         });
         
         ractive.on('reselectInput', function(event) {
             //this is called by the UL itself
             $j(event.node).parent().find('input').focus();    
         });
         
         ractive.on('smartZipcodeChange', function(event, cityId, stateId)
         {
            city = $j("#" + cityId)[0];
            state = $j("#" + stateId)[0];

            smartZipcode(event.node, city, state, null, event.original);
         });
        
         function smartZipcode_callback() {
             ractive.updateModel(stateId);
             ractive.updateModel(cityId);
         }
         
         ractive.on('archiveGFE', function(event){
            if(isDirty()){
               alert('Please save the page before attempting to archive');
             }
             else{
               callWebMethod('RecordGFEToArchive', {LoanID: ML.sLId}, function(){ alert('Error'); }, 
               function(m){
                    data = JSON.parse(m.d);
                    if(data.Error)
                    {
                        alert(data.Error);
                    }
                    else
                    {
                        var option = $j("<option />");
                        option.text(data.sLastDisclosedGFEArchiveId);
                        option.val(data.sLastDisclosedGFEArchiveId);
                        $j("#sLastDisclosedGFEArchiveId").prepend(option);
                        $j("#sLastDisclosedGFEArchiveId").val(data.sLastDisclosedGFEArchiveId);
                        
                        var body_url = <%=AspxTools.JsNumeric(E_UrlOption.Page_2015GFE) %>;
                        window.top.location = ML.VirtualRoot + '/newlos/LoanApp.aspx' + '?loanid='+ML.sLId+
                        "&appid="+ML.aAppId+'&body_url='+ body_url;
                    }
               });
             }
         });
         
    var c = 0;
    for (var i = 0; i < ClosingCostData.SectionList.length; i++) {
        c += ClosingCostData.SectionList[i].ClosingCostFeeList.length; 
    }
    lqblog('Number of fees : '+  c);
    lqblog('Initial Render : ' + (Date.now() - startTime) + 'ms');
    
  if(<%=AspxTools.JsBool(IsReadOnly) %>)
  {
    $j("*").not( "#bPrintGFE, #ddlGFEArchives").attr("readonly", "");
    $j("input[type!='text'], select").not( "#bPrintGFE, #ddlGFEArchives, input[type!='submit']").attr("disabled", "true");
  }

    window.f_saveMe = function() {
    
    if ($j('input.invalid,input.dupe').length > 0){
        alert('Cannot save until the HUD line issues are fixed.');
        return false;
    }
            
    ractive.set('GfeTilAgentRoleT', $j("#CFM_m_officialContactList").val());
    ractive.set('GfeTilIsLocked', $j("#CFM_m_rbManualOverride").prop("checked"));
    var model = ractive.get();
    model.sLastDisclosedGFEArchiveId = $j("#sLastDisclosedGFEArchiveId").val();

        var data = { loanId: ML.sLId, viewModelJson: JSON.stringify(model), IsArchive: model.IsArchive },
            success = false;
        
        callWebMethod('Save', data, function(){ 
            alert('Failed');
        }, 
        function(m) {
            success = true;
            clearDirty();
            if(parent.info && typeof(parent.info.f_refreshInfo) === 'function')
            {
                parent.info.f_refreshInfo();
            }

            model = JSON.parse(m.d);
            ractive.set(model);
        });
        return success;
    } 
    
    var bAltDown;
    var manualUpdate;
    $j("body").on("keydown", document_keyDown);
    $j("body").on("keyup", document_keyUp);
    function document_keyDown(event)
    {
       if(event.keyCode == 18) //alt key
       {
            bAltDown = true;
       }
       else if(!event.altKey) //don't get stuck with bAltDown indefinitely
       {
            bAltDown = false;
       }
       else if(event.keyCode == 67 && bAltDown) //"c" key
       {
            ractive.fire('triggerManualRecalc');
       }

    }
    
    ractive.on('triggerManualRecalc', function(event)
    {
        if(ractive.get("ByPassBgCalcForGfeAsDefault"))
            {
                manualUpdate = true;
                ractive.fire('recalc');
                ractive.set("ByPassBgCalcForGfeAsDefault", true);
                manualUpdate = false;
            }
    });
    
    function document_keyUp(event)
    {
        if(event.keyCode == 18) //alt key
        {
            bAltDown = false;
        }
	}
    globalRActive = ractive;
    
    window.saveMe = window.f_saveMe;
    
    $wrapper = $j("#wrapper");
    $closeIcon = $j("#closeIcon");
    $tabLinks = $j("#Tabs li");
    $contentDivs = $j(".tabContent");
    switchTab("Min");
});

/////////////////////////////////////////////////////////////////////

  function openGFEPDF() {
      var args = 'GFEArchiveId=' + <%=AspxTools.JsGetElementById(ddlGFEArchives) %>.value;
      var url = gVirtualRoot + '/pdf/GoodFaithEstimate2010Archive.aspx?loanid=' + ML.sLId + "&"+args;
      LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
	return false;
  }
  
  function openPopup(linkFirstPart, uniqueName)
  {
    var virtualRoot = <%=AspxTools.SafeUrl(VirtualRoot)%>;
    var popup = window.open(virtualRoot+'/'+linkFirstPart+'?loanid='+ML.sLId+
        "&appid="+ML.aAppId+
        "&ispopup=1", uniqueName, "toolbar=no,menubar=no,location=no,status=no,scrollbars=yes,resizable=yes");
    popup.focus();
  }
  
  function findCCTemplate() {

    showModal(<%= AspxTools.JsString(ClosingCostPageUrl) %>, null, null, null, function(args){
        if (args.OK) {
            self.location = self.location; // Refresh;
        }
    },{hideCloseButton:true});
  }
  
  function selectedDate(cal, date)
    {
        cal.sel.value = date;
        cal.callCloseHandler();
        if (typeof (updateDirtyBit) == 'function') updateDirtyBit();
        cal.sel.blur();
    } 
    
    /*<------------Footer Script------------------>*/
    var $wrapper;
    var $closeIcon;
    var $tabLinks;
    var $contentDivs;

    function switchTab(tabId) {
        $closeIcon.hide();
        $contentDivs.hide();
        $tabLinks.removeClass();

        $tabLinks.filter("#" + tabId + "Lnk").addClass("selected");

        $div = $contentDivs.filter("#" + tabId);
        if ($div.length != 0) {   // need check because minimize has no content
            $div.show();
            $closeIcon.show();
        }

        $j(window).trigger('footer/switchTab')
    }

    /*<-------------Details of Transaction Script---------------->*/

    function lockFieldByElement(cb, tb) {
        tb.readOnly = !cb.checked;

        if (!cb.checked) 
            tb.style.backgroundColor = "lightgrey";
        else
            tb.style.backgroundColor = "";
    }
  </script>

</body>
</html>
