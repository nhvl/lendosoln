﻿//-----------------------------------------------------------------------
// <copyright file="LoanOverwriteImportExport.aspx.cs" company="Meridianlink">
//     Copyright (c) Sprocket Enterprises. All rights reserved.
// </copyright>
// <author>Zijing Jia</author>
// <summary>
//  This is the export/import tool for loan overwrite. It only update the loan file.
// </summary>
//-----------------------------------------------------------------------

namespace LendersOfficeApp.newlos.Test
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Web;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.ObjLib.Conversions.LoanFileImportExport;
    using LendersOffice.AntiXss;
    using System.IO;
    using DataAccess.TempFile;

    /// <summary>
    /// Import class handle loan overwrite. 
    /// </summary>
    public partial class LoanOverwriteImportExport : BaseLoanPage
    {
        /// <summary>
        /// Mapping address of white list, a short cut. Use Server.MapPath to transform it into a real address.
        /// Giving this the same address as the senderWhiteList since they both represent the same thing, so there is no need to maintain two files.
        /// </summary>
        public static readonly string LoanOverwriteWhitelistAddr = @"~/senderWhiteList.csv.config";

        /// <summary>
        /// Gets or sets white list dictionary. "WhitelistData" includes 3 sets and 1 dictionary.
        /// </summary>
        /// <value>Dictionary: key->string, value->WhitelistData.</value>
        public static Dictionary<string, WhitelistData> WhitelistDictionary { get; set; }

        /// <summary>
        /// Generates list of loan-table strings (entry list) and broker connection info strings (combo list) for loan export.
        /// </summary>
        /// <param name="connBrokerIdCustomerCodeListList">A list of combo lists of (broker id, customer code) pairs separated by the '|' vertical line symbol, the outer list of length 1.</param>
        /// <param name="listOfLoanTableRecordStrings">A list of entry lists, where each entry is a list like tablename|slid|slidvalue, and the outer list is length 1.</param>
        /// <param name="loanId">The loan id of current loan.</param>
        private static void GetTableEntriesAndBrokerConnectionsForLoan(out List<List<string>> connBrokerIdCustomerCodeListList, out List<List<string>> listOfLoanTableRecordStrings, string loanId)
        {
            connBrokerIdCustomerCodeListList = new List<List<string>>();
            listOfLoanTableRecordStrings = new List<List<string>>();
            Guid connInfoBrokerId = Guid.Empty;
            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(new Guid(loanId), out connInfoBrokerId);
            string customerCode = ShareImportExport.GetCustomerCodeByBrokerId(connInfoBrokerId);
            int exportTableCnt = ShareImportExport.ExportTableNames.Count();

            List<string> appIdList = ShareImportExport.GetAppIdListByLoanId(connInfoBrokerId, loanId);
            List<string> curComboList = new List<string>();
            List<string> curEntryList = new List<string>();

            string combo = connInfoBrokerId + "|" + customerCode;
            for (int i = 0; i < exportTableCnt; i++)
            {
                string loanTableRecordString = string.Empty;
                if (ShareImportExport.ExportTableNames[i].StartsWith("APPLICATION_"))
                {
                    foreach (string appId in appIdList)
                    {
                        loanTableRecordString = ShareImportExport.ExportTableNames[i] + "|aAppId|" + appId;
                        curComboList.Add(combo);
                        curEntryList.Add(loanTableRecordString);
                    }
                }
                else
                {
                    loanTableRecordString = ShareImportExport.ExportTableNames[i] + "|sLId|" + loanId;
                    curComboList.Add(combo);
                    curEntryList.Add(loanTableRecordString);
                }
            }

            connBrokerIdCustomerCodeListList.Add(curComboList);
            listOfLoanTableRecordStrings.Add(curEntryList);
        }

        /// <summary>
        /// Default function of load page.
        /// </summary>
        /// <param name="sender">Object sender.</param> 
        /// <param name="e">Event EventArgs e.</param> 
        protected void Page_Load(object sender, EventArgs e)
        {
            string reasonCantView;
            if (!BrokerUser.CanUserViewLoanEditorTestPage(out reasonCantView))
            { 
                throw new CBaseException("No access to this page.", reasonCantView);
            }

            var isNonAuthorProduction = Tools.IsNonAuthorProduction;
            var isProductionOfAnySort = ConstAppDavid.CurrentServerLocation == ServerLocation.Production;

            UnorderedListOfImportExportOperations.Visible = false == isNonAuthorProduction;
            divForProductionMessage.Visible = isNonAuthorProduction;

            listItemForLoanImport.Visible = false == isProductionOfAnySort;
            listItemForLoanImportOnProd.Visible = isProductionOfAnySort;
        }

        /// <summary>
        /// A tool for exporting file in xml format.
        /// </summary>
        /// <param name="filename">The physic file address.</param>
        protected void ExportFile(string filename)
        {
            Response.ContentType = "application/octet-stream";
            Response.AppendHeader("Content-Disposition", $"attachment; filename=\"{filename}\"");
            Response.TransmitFile(filename);
            Response.End();
        }

        /// <summary>
        /// Export the current white list.
        /// </summary>
        /// <param name="sender">Object sender.</param> 
        /// <param name="e">Event EventArgs e.</param> 
        protected void ExportMainWhiteList(object sender, EventArgs e)
        {
            string fileName = Tools.GetServerMapPath(LoanOverwriteImportExport.LoanOverwriteWhitelistAddr);
            if (!FileOperationHelper.Exists(fileName))
            {
                var warningMsg = "The whitelist does not exist. You can import the example white list instead.";
                string warningAlert = GetAlertScriptForMsg(warningMsg);
                Response.Write(warningAlert);
            }
            else
            {
                this.ExportFile(fileName);
            }
        }

        /// <summary>
        /// Export default white list. All columns are set to be "Export".
        /// </summary>
        /// <param name="sender">Object sender.</param> 
        /// <param name="e">Event EventArgs e.</param> 
        protected void ExportMainDefaultWhiteList(object sender, EventArgs e)
        {
            string fileName = ShareImportExport.GetDefaultCsvWhitelistByList(ShareImportExport.ExportTableNames.ToList());
            this.ExportFile(fileName);
        }

        /// <summary>
        /// Upload white list and read the white list into a dictionary structure.
        /// </summary>
        /// <param name="sender">Object sender.</param> 
        /// <param name="e">Event EventArgs e.</param> 
        protected void Click_UploadMainWhitelist(object sender, EventArgs e)
        {
            string savePath = Tools.GetServerMapPath(LoanOverwriteImportExport.LoanOverwriteWhitelistAddr);
            var tmpPath = savePath + ".tmp";

            if (this.FileUpload_MainWhiteList.HasFile)
            {
                int fileSize = this.FileUpload_MainWhiteList.PostedFile.ContentLength;
                this.FileUpload_MainWhiteList.SaveAs(tmpPath);

                List<string> errorMsgList = new List<string>();
                Dictionary<string, string> reconstructMap = null;
                Dictionary<string, WhitelistData> whitelistDictionary = null;
                ShareImportExport.ReadCsvWhiteList(ref errorMsgList, out reconstructMap, out whitelistDictionary, savePath, true);
            
                if (errorMsgList.Any())
                {
                    foreach (string msg in errorMsgList)
                    {
                        string error = GetAlertScriptForMsg(msg);
                        Response.Write(error);
                    }

                    this.Status.ForeColor = Color.Red;
                    this.Status.Text = "Whitelist was not successfully imported into the server.  Please correct the error(s) and try again.";
                }
                else
                {
                    FileOperationHelper.Copy(tmpPath, savePath, allowOverwrite:true);

                    var msg = "Your whitelist was uploaded successfully.";
                    Response.Write(GetAlertScriptForMsg(msg));
                    this.Status.ForeColor = Color.Green;
                    this.Status.Text = msg;
                }
            }
            else
            {
                this.Status.Text = "You did not specify a file to upload.";
            }
        }

        private string GetAlertScriptForMsg(string msg)
        {
            return @"<script language='javascript'>alert('" + msg + "');</script>";
        }


        protected void Click_ExportLoan(object sender, EventArgs e)
        {
            if(ConstStage.UseLibForLoanReplication)
            {
                Click_ExportLoan_New(sender, e);
            }
            else
            {
                Click_ExportLoan_Old(sender, e);
            }
        }
        protected void Click_ExportLoan_New(object sender, EventArgs e)
        {
            var exportOptions = new LoanExportOptions()
            {
                LoanId = this.LoanID,
                IncludeFileDbEntries = this.LoanExportIncludeFiledbCheckBoxId.Checked
            };

            var result = LoanReplicator.ExportLoan(this.BrokerUser, exportOptions);

            if (result.Errors.Any())
            {
                this.Status.ForeColor = Color.Red;
                this.Status.Text = "errors occurred.<br/>" + string.Join("<br/>", result.Errors.Select(errorMessage => AspxTools.HtmlString(errorMessage)));
            }
            else
            {
                this.ExportFile(TempFileUtils.Name2Path(result.TempFileName));
            }
        }

        /// <summary>
        /// Export loan xml.
        /// </summary>
        /// <param name="sender">Object sender.</param> 
        /// <param name="e">Event EventArgs e.</param> 
        protected void Click_ExportLoan_Old(object sender, EventArgs e)
        {
            bool needUpdateWhiteList;
            bool needReview;
            Dictionary<string, WhitelistData> whitelistDictionary;
            Dictionary<string, string> reconstructMap;
            List<string> errorList;
            IEnumerable<FileDBEntryInfoForExport> fileDBKeysForDirectTransfer;

            string fileName = PrepareLoanExportAndMissingLoanFields(
                out needUpdateWhiteList,
                out needReview,
                out whitelistDictionary,
                out reconstructMap,
                out errorList,
                out fileDBKeysForDirectTransfer);

            this.ExportFile(fileName);
        }

        private string PrepareLoanExportAndMissingLoanFields(
            out bool needUpdateWhiteList,
            out bool needReview,
            out Dictionary<string, WhitelistData> whitelistDictionary,
            out Dictionary<string, string> reconstructMap,
            out List<string> errorList,
            out IEnumerable<FileDBEntryInfoForExport> fileDBKeysForDirectTransfer)
        {
            string mainDirectory = Tools.GetServerMapPath(@"~/");
            string whitelistPath = Tools.GetServerMapPath(LoanOverwriteImportExport.LoanOverwriteWhitelistAddr);
            HttpContext.Current.Server.ScriptTimeout = ConstStage.JustinBrokerExportTimeOutInSeconds;
            errorList = new List<string>();
            List<string> warningList = new List<string>();

            List<List<string>> connBrokerIdCustomerCodeListList = null;
            List<List<string>> listOfLoanTableRecordStrings = null;
            LoanOverwriteImportExport.GetTableEntriesAndBrokerConnectionsForLoan(out connBrokerIdCustomerCodeListList, out listOfLoanTableRecordStrings, LoanID.ToString());
            
            string filename = TableReplicateTool.ExportTablesBySpAndEntryList(
                ref errorList,
                ref warningList,
                out whitelistDictionary,
                out reconstructMap,
                out needReview,
                out needUpdateWhiteList,
                out fileDBKeysForDirectTransfer,
                whitelistPath,
                mainDirectory,
                "MAIN_DB",
                this.LoanExportIncludeFiledbCheckBoxId.Checked,
                new HashSet<string>(ShareImportExport.ExportTableNames),
                isLoanOverwrite: true,
                listOfListsOfBrokerConnectionInfoStrings: connBrokerIdCustomerCodeListList,
                listOfListsOfTableRecordStrings: listOfLoanTableRecordStrings,
                needDebugInfo: true);

            if (errorList.Any())
            {
                Tools.LogInfo("loanoverwriteimportexport_export_errors: " + string.Join(Environment.NewLine, errorList));
            }

            if (warningList.Any())
            {
                Tools.LogInfo("loanoverwriteimportexport_export_warnings: " + string.Join(Environment.NewLine, warningList));
            }

            return filename;
        }

        /// <summary>
        /// Export the updated whitelist.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">EventArgs e</param>
        protected void Click_ExportMissingColumnsForLoan(object sender, EventArgs e)
        {
            bool needUpdateWhiteList;
            bool needReview;
            Dictionary<string, WhitelistData> whitelistDictionary;
            Dictionary<string, string> reconstructMap;
            List<string> errorList;
            IEnumerable<FileDBEntryInfoForExport> fileDBKeysForDirectTransfer;

            PrepareLoanExportAndMissingLoanFields(
                out needUpdateWhiteList,
                out needReview,
                out whitelistDictionary,
                out reconstructMap,
                out errorList,
                out fileDBKeysForDirectTransfer);

            var tempFilePath = TempFileUtils.NewTempFilePath() + ShareImportExport.WhiteListFileExtension;
            ShareImportExport.WriteCsvWhistList(ref errorList, ref reconstructMap, whitelistDictionary, tempFilePath, true);

            this.ExportFile(tempFilePath);
        }

        /// <summary>
        /// Import the loan xml.
        /// </summary>
        /// <param name="sender">Object sender.</param> 
        /// <param name="e">Event EventArgs e.</param> 
        protected void Click_UploadLoan(object sender, EventArgs e)
        {
            if(ConstStage.UseLibForLoanReplication)
            {
                Click_UploadLoan_New(sender, e);
            }
            else
            {
                Click_UploadLoan_Old(sender, e);
            }
        }

        private void Click_UploadLoan_New(object sender, EventArgs e)
        {
            if (!this.FileUpload_OptionalXml.HasFile)
            {
                this.Status.Text = "You did not specify a file to upload.";
                this.Status.ForeColor = Color.Red;
                return;
            }
            
            string savePath = TempFileUtils.NewTempFilePath() + "_upload.xml";
            string tempFileName = Path.GetFileName(savePath);
            this.FileUpload_OptionalXml.SaveAs(savePath);
            TempFileAccessManager.AssertThatCurrentUserCreatedFile(BrokerUser, tempFileName);
            ShareImportExport.RemoveLeadingOrEndingSpaceOfFile(savePath);

            var importOptions = new LoanImportOptions(tempFileName)
            {
                LoanIdToOverwrite = this.LoanID,
                DefaultOtherBrokerFields = this.cbDefaultExternalBrokerReferenceFields.Checked
            };            

            var result = LoanReplicator.ImportLoan(this.BrokerUser, importOptions);            

            if (result.Errors.Any())
            {
                foreach (string m in result.Errors)
                {
                    string errorMsg = GetAlertScriptForMsg(m);
                    Response.Write(errorMsg);
                }

                this.Status.ForeColor = Color.Red;
                this.Status.Text = "File is not updated. Please correct the error and try again.";
            }
            else
            {
                var msg = "The file is uploaded successfully";
                Response.Write(GetAlertScriptForMsg(msg));
                this.Status.ForeColor = Color.Green;
                this.Status.Text = "Your file was uploaded successfully.";
            }
        }

        private void Click_UploadLoan_Old(object sender, EventArgs e)
        { 
            var stageSiteSource = ConstAppDavid.CurrentServerLocation;

            if (ServerLocation.Production == stageSiteSource)
            {
                var msg = "Warning: You are not allowed to import brokers into product server!";
                Response.Write(GetAlertScriptForMsg(msg));
                return;
            }

            if (!this.FileUpload_OptionalXml.HasFile)
            {
                // Notify the user that a file was not uploaded.
                this.Status.Text = "You did not specify a file to upload.";
                this.Status.ForeColor = Color.Red;
                return;
            }

            // e.g. sInvestorRolodexId
            bool defaultExternalBrokerReferenceFields = cbDefaultExternalBrokerReferenceFields.Checked;

            int fileSize = this.FileUpload_OptionalXml.PostedFile.ContentLength;
            string savePath = TempFileUtils.NewTempFilePath() + "_upload.xml";
            this.FileUpload_OptionalXml.SaveAs(savePath);
            ShareImportExport.RemoveLeadingOrEndingSpaceOfFile(savePath);

            List<string> errorList = new List<string>();
            int justinOneSqlQueryTimeOutInSeconds = ConstStage.JustinOneSqlQueryTimeOutInSeconds;
            LoanOverwriteTool.ImportOneXmlFile_LoanUpdate(
                ref errorList,
                ShareImportExport.ExportTableNames,
                savePath,
                LoanID.ToString(),
                UserID.ToString(),
                justinOneSqlQueryTimeOutInSeconds,
                defaultExternalBrokerReferenceFields);

            if (errorList.Count > 0)
            {
                foreach (string m in errorList)
                {
                    string errorMsg = GetAlertScriptForMsg(m);
                    Response.Write(errorMsg);
                }

                this.Status.ForeColor = Color.Red;
                this.Status.Text = "File is not updated. Please correct the error and try again.";
            }
            else
            {
                var msg = "The file is uploaded successfully";
                Response.Write(GetAlertScriptForMsg(msg));
                this.Status.ForeColor = Color.Green;
                this.Status.Text = "Your file was uploaded successfully.";
            }
            
        }
    }
}
