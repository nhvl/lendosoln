﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Test
{
    public partial class RActiveGFEService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override bool PerformResultOptimization
        {
            get
            {
                return true;
            }
        }

        protected BrokerDB Broker
        {
            get { return BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId); }
        }

        protected bool IsArchivePage
        {
            get { return Broker.IsGFEandCoCVersioningEnabled && (GetBool("IsArchivePage") == true); }
        }

        private CPageData CreatePageData()
        {
            Guid loanID = GetGuid("loanid");
            if (Broker.IsGFEandCoCVersioningEnabled && !IsArchivePage)
            {
                return new CPageData(loanID, CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(RActiveGFEService)).Union(
                    CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release));
            }
            else
            {
                return CPageData.CreateUsingSmartDependency(loanID, typeof(RActiveGFEService));
            }
        }

        protected override void Process(string methodName)
        {
            if (IsArchivePage)
            {
                Tools.LogError("Programming Error: Should not call services on archive page.");
                return;
            }
            switch (methodName)
            {
                case "GetAgentIdByAgentType":
                    GetAgentIdByAgentType();
                    break;
            }
        }

        private void GetAgentIdByAgentType()
        {
            E_AgentRoleT agentType = (E_AgentRoleT)GetInt("AgentType");

            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();

            Guid recordId = Guid.Empty;
            string companyName = "";

            CAgentFields agent = dataLoan.GetAgentOfRole(agentType, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            if (agent != CAgentFields.Empty)
            {
                recordId = agent.RecordId;
                companyName = agent.CompanyName;
            }

            SetResult("RecordId", recordId.ToString());
            SetResult("CompanyName", companyName);
        }
    }
}
