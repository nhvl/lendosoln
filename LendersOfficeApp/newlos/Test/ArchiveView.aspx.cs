﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.AntiXss;
using LendersOffice.Common.SerializationTypes;

// This was created in a few hours for same-day hotfix.
// There is room for improvement.
namespace LendersOfficeApp.newlos.Test
{
    public partial class ArchiveView : BaseLoanPage
    {
        CPageData x_dataLoan = null;
        protected CPageData DataLoan
        {
            get
            {
                if (x_dataLoan == null)
                {
                    x_dataLoan = new CPageData(LoanID, CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(ArchiveView)).Union(new string[] { "sfApplyClosingCostArchive" }));
                    x_dataLoan.InitLoad();
                }
                return x_dataLoan;
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            if (BrokerUser.HasPermission(LendersOffice.Security.Permission.CanModifyLoanPrograms) == false)
            {
                throw new DataAccess.CBaseException(LendersOffice.Common.ErrorMessages.GenericAccessDenied,
                    "Must have CanModifyLoanPrograms permission to edit.");
            }

            if (!Page.IsPostBack)
            {
                DataLoan.sClosingCostArchive.ForEach(archive => ArchiveList.Items.Add(GetItem(archive)));
                ArchiveList.Items.Insert(0, new ListItem("This Loan Data (No Archive Applied)", Guid.Empty.ToString()));

                DataLoan.sClosingCostArchive.ForEach(archive => CompareArchiveList.Items.Add(GetItem(archive)));
                CompareArchiveList.Items.Insert(0, new ListItem("None", Guid.Empty.ToString()));

                if (DataLoan.sLastDisclosedClosingCostArchive != null)
                {
                    // Init to latest if we can.
                    ArchiveList.SelectedValue = DataLoan.sLastDisclosedClosingCostArchive.Id.ToString();
                }
                else
                {
                    ArchiveList.SelectedIndex = 0;
                }
                CompareArchiveList.SelectedIndex = 0;
            }
        }

        private void BindArchiveDump()
        {
            Dictionary<string, string> firstValues;
            if (ArchiveList.SelectedIndex != 0)
            {
                var id = new Guid(ArchiveList.SelectedValue);
                firstValues = GetValueList(DataLoan.sClosingCostArchive.First(archive => archive.Id == id));
            }
            else
            {
                firstValues = GetValueListCurrentLoan();
            }

            Dictionary<string, string> secondValues;
            if (CompareArchiveList.SelectedIndex != 0)
            {
                var id = new Guid(CompareArchiveList.SelectedValue);
                secondValues = GetValueList(DataLoan.sClosingCostArchive.First(archive => archive.Id == id));
            }
            else
            {
                secondValues = new Dictionary<string, string>();
            }

            bool isForCompare = secondValues.Count() != 0;
            
            HtmlTable fieldTable = new HtmlTable();
            fieldTable.Width = "100%";
            HtmlTableRow headerRow = new HtmlTableRow();
            headerRow.Attributes.Add("class", "GridHeader");
            headerRow.Cells.Add(new HtmlTableCell() { InnerText = "Field" });
            headerRow.Cells.Add(new HtmlTableCell() { InnerText = "Value" });
            headerRow.Cells.Add(new HtmlTableCell() { InnerText = isForCompare ? "Compare Value" : "" });
            fieldTable.Rows.Add(headerRow);

            int rowCount = 0;
            foreach (var field in firstValues.Keys)
            {
                HtmlTableRow itemRow = new HtmlTableRow();
                itemRow.Cells.Add(new HtmlTableCell() { InnerText = field });
                itemRow.Cells.Add(new HtmlTableCell() { InnerHtml = GetDataString(firstValues[field]) });
                itemRow.Cells.Add(new HtmlTableCell() { InnerHtml = isForCompare ? GetDataString(secondValues[field]) : "" });

                if (isForCompare && firstValues[field] != secondValues[field])
                {
                    itemRow.Attributes.Add("class","GridSelectedItem");
                }
                else
                {
                    itemRow.Attributes.Add("class", rowCount % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                }
                rowCount++;
                fieldTable.Rows.Add(itemRow);
            }
            ArchiveDump.Controls.Add(fieldTable);
        }

        private string GetDataString(string data)
        {
            if (data == null)
            {
                return "[NO DATA]";
            }

            if (data.Length > 250)
            {
                return "<a href='#' onclick='showData(this, event);' data=" + AspxTools.HtmlAttribute(data) + " >view</a>" ;
            }

            return AspxTools.HtmlString(data);
        }

        private Dictionary<string, string> GetValueList(ClosingCostArchive archive)
        {
            Dictionary<string, string> result = new Dictionary<string,string>();
            foreach (var field in GetFieldList())
            {
                if (archive.HasValue(field))
                {
                    result.Add(field, archive.GetValue(field));
                }
                else
                {
                    result.Add(field, null);
                }
            }
            return result;
        }

        private Dictionary<string, string> GetValueListCurrentLoan()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            var dataApp = DataLoan.GetAppData(0);
            foreach (var field in GetFieldList())
            {
                string value;
                try
                {
                    value = PageDataUtilities.GetValue(DataLoan, dataApp, field);
                }
                catch (System.Reflection.TargetInvocationException exc)
                {
                    Tools.LogError("Could not get from loan: " + field, exc);
                    value = "[TargetInvocationException]";
                }
                catch (CBaseException exc )
                {
                    Tools.LogError("Internal Archive View: Could not get from loan: " + field, exc);
                    value = "[CBaseException]";
                }

                result.Add(field, value);
            }
            return result;
        }

        private IEnumerable<string> GetFieldList()
        {
            return ClosingCostArchive.GetLoanPropertyList(ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015)
                .Union(ClosingCostArchive.GetLoanPropertyList(ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate))
                .Union(ClosingCostArchive.GetLoanPropertyList(ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure));
        }

        private ListItem GetItem(ClosingCostArchive archive)
        {
            string title;
            switch (archive.ClosingCostArchiveType)
            {
                case ClosingCostArchive.E_ClosingCostArchiveType.Gfe2010:
                case ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015: title = "GFE"; break;
                case ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate: title = "LE"; break;
                case ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure: title = "CD"; break;
                default:
                    throw new UnhandledEnumException(archive.ClosingCostArchiveType);
            }
            title += " " + archive.DateArchived;
            return new ListItem(title, archive.Id.ToString());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindArchiveDump();
        }
    }
}