﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoanOverwriteImportExport.aspx.cs" Inherits="LendersOfficeApp.newlos.Test.LoanOverwriteImportExport" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
    <script type="text/javascript">
        function ShowLoading()
        {
            document.getElementById('LoadingText').innerText = 'Loading... (this may take a few minutes depending on the size of the loan.)';
        }
    </script>
    <div>
        <div id="Header" class="MainRightHeader" >Loan Overwrite Export Tool </div>
        <ul runat="server" id="UnorderedListOfImportExportOperations">
            <li id="listItemForLoanExport">
                <asp:CheckBox ID="LoanExportIncludeFiledbCheckBoxId" Text = "Include filedb content" Checked="false" runat="server" />
                <asp:Button ID="ExportWithOptionID" Text="Export Loan" OnClick="Click_ExportLoan" OnClientClick="ShowLoading()" runat="server">
                </asp:Button>
                <span id="LoadingText"></span><br />
                <span><ml:PassThroughLabel id="Status" runat="server">            </ml:PassThroughLabel></span>
            </li>
            <hr />
            <li runat="server" id="listItemForLoanImportOnProd">
                You may not import into loans on production.
            </li>
            <li runat="server" id="listItemForLoanImport">
                <div>Loan file import.  Select one XML file to upload (can include Loan, Broker, Branch):</div>
                <asp:FileUpload id="FileUpload_OptionalXml" runat="server">
                </asp:FileUpload>
                <asp:CheckBox runat="server" ID="cbDefaultExternalBrokerReferenceFields" Text="Default fields referring to other broker?" />
                
                <br/>
                
                <span onclick = "f_LoanUpdateWarning();">
                    <asp:Button id="Button2" Text="Import Loan" OnClick="Click_UploadLoan" runat="server">
                    </asp:Button>
                </span>
            </li>
            <hr />
            <li id="listItemForExportOfWhiteList">
                <asp:Button runat="server" ID="ExportMissingColumnsForLoan"  Text="Export Missing Columns For Loan" OnClick="Click_ExportMissingColumnsForLoan" />

                <asp:Button ID="ExportMainWhiteListID" Text="Export Main White List" onclick="ExportMainWhiteList" runat="server">
                </asp:Button>
                
                <asp:Button ID="ExportMainDefaultWhiteListID" Text="Export Full (nonsecure) White List" onclick="ExportMainDefaultWhiteList" runat="server">
                </asp:Button>
                </li>
            <hr />
            <li id="listItemForImportWhiteList"> 
                <div>Whitelist import.  Select one rule file to upload (Field Name, Is Protected, Default Value):</div>
                <asp:FileUpload id="FileUpload_MainWhiteList" runat="server">
                </asp:FileUpload>
                
                <br/><br/> 
                <asp:Button id="UploadMainWhitelistID" Text="Import White List" OnClick="Click_UploadMainWhitelist" runat="server">
                </asp:Button>
            </li>
        </ul>
        <div runat="server" id="divForProductionMessage">
            <span style="color:red;font-weight:bold;">You may not import/export loans from production.  Use LOAuth instead.</span>
        </div>
    </div>
    </form>
    
    <hr/>
    <div id="Div1" class="MainRightHeader" >FAQ </div>
    <div id = "loan_tool_faq">
<p>1. The difference between loan-overwrite tool and loan-replicate tool.<br />&nbsp;&nbsp;&nbsp; The loan-overwrite tool will only overwrite the current loan data when user try to import the xml into current loan that s/he opened. It will not touch foreign key or primary key.<br />&nbsp;&nbsp;&nbsp; The loan-replicate tool is kind of a database table transportation tool. For example, user can transport a loan in production server, together with all its environment into test server. This includes the user account, broker and all related table information that support this loan to run. Even if the broker or user account doesn't exist in test server, the program will create one that is exactly the same as the one in production server. The target of this tool is to mimic the loan environment in production server as much as it could for testing purpose.<br />&nbsp;&nbsp; &nbsp;<br />2. New columns need to be reviewed. Please export white list and check them.<br />&nbsp;&nbsp;&nbsp; To solve this, you can export the whitelist first. It is a csv file and can be open by excel. Some fields at the top rows has blank column "export-replicate" and "export-overwrite". If you don't know, you can just set both to be "Yes" and remember to add "test-SetAsYes" at note column of the same row. This will help us to know that this column still need to be reviewed.<br /><br />3. Explain the value of "export-replicate" and "export-overwrite".<br />&nbsp;&nbsp;&nbsp; "Yes" means this value can be exported and seen by others and can be imported.<br />&nbsp;&nbsp;&nbsp; "No" means this value need to be protected. Use also need to fill the "substitute-..." column at right side, next to the "export-..."&nbsp; column with the substitute value. This value will be seen during export. In loan-replicate tool, this value will be inserted if there is no value in the original database with the substitute value. In loan-overwrite tool, the loan already exists and the program has no need to enforce the value so the program will ignore the protected fields and import other fields. This is suitable for fields like password.<br />&nbsp;&nbsp;&nbsp;&nbsp; "NoExportNoImport" means this field should be exported and surely will not be imported in both loan-overwrite tool and loan-replicate tool. Fields like 'ssn' is suitable under this protection. It requires the field to be nullable before setting it to this value since the program will have to ignore it during insertion or update.<br /><br />4. Why two export and substitute columns?<br />&nbsp;&nbsp;&nbsp; The loan-overwrite tool and loan-replicate tool share the same white list.<br />&nbsp;&nbsp;&nbsp; As you can see, there are two sets of export-substitute columns: ( export-replicate, substitution-replicate ) and (export-overwrite, substitution-overwrite). The former one is for loan-replicate tool which export the loan from current server and import it into remote one, together with all the related data to the loan table, such as user account, broker. The latter one is for loan-overwrite which will overwrite the current loan you import the xml into.<br /><br />5. Explain the nuke fields.<br />&nbsp;&nbsp;&nbsp; When user use loan-overwrite tool and try to import xml into a loan at production server, this restriction will be activated.<br />&nbsp;&nbsp;&nbsp; It requires user to set certain fields at production server database to be certain value to enable the import process for safety purpose. When user import loan at other server like test/dev server, this restriction is waived.<br />&nbsp;&nbsp;&nbsp; Set primary borrower's first and last name to be "nuke": that is "aBFirstNm" and "aBLastNm" at APPLICATION_A.<br />&nbsp;&nbsp;&nbsp; Set loan name to be start with "@_@ NUKE THIS LOAN": that is "sLNm" in LOAN_FILE_E.&nbsp; Since loan name is required to be unique, you cannot just overwrite the whole value with "@_@ NUKE THIS LOAN".<br />Instead, add "@_@ NUKE THIS LOAN" in front of the original loan name. Consider that there is lenght limit over loan name as 36 characters, you can delete some original loan name characters if it is too long. But just make sure it start with "@_@ NUKE THIS LOAN" and make sure the whole thing is unique.<br /><br />6. Error of "The process cannot access the file 'c:\LendOSoln\StandAlonePricingEngine\pml_shared\findNextLog.txt' because it is being used by another process. "<br />&nbsp;&nbsp;&nbsp; When seeing this error, it is probably because somebody else is also using the tool and it causes conflict. Try to refresh the page and do it again. <br /><br />7. Why some data does not  refresh after import?<br />&nbsp;&nbsp;&nbsp; This is because the import doesn't update the value at loan_file_cache. One solution is that try to change any value and change it back and then click save. <br />&nbsp; <br /></p>
    </div>

    <script type="text/javascript">
    <!--
    function f_LoanUpdateWarning() {
        var code = Math.floor(Math.random() * 1000) + '';
        var ans = prompt("WARNING: Data replaced by import will be PERMANENTLY and irrevocably LOST forever without ability to restore it.  " +
	      "Please type '" + code + "' below to confirm it:", "");
        var doIt = (ans != null && ans == code);

        if (doIt == false && ans != null)
            alert("Code does not match.  Operations will not be performed.");
        else if (ans == null)
            alert("Please enter the code");

        return doIt;
    }
    //-->
    </script>
</body>
</html>
