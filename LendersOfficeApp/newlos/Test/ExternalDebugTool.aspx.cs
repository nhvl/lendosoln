
using System;
using System.IO;
using System.Text;
using System.Xml;
using DataAccess;
using DocMagic.DsiDocRequest;
using Integration.Encompass;
using LendersOffice.Common;
using LendersOffice.Conversions;
using LendersOffice.Conversions.Closing231;
using LendersOffice.Conversions.ComplianceEase;
using LendersOffice.Conversions.DocuTech;
using LendersOffice.DU;
using LendersOffice.ObjLib.Conversions.LoansPQ.Exporter;
using LendersOffice.ObjLib.FHAConnection;
using LendersOfficeApp.Integration.DataTrac;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Test
{
    public partial class ExternalDebugTool : BaseLoanPage
    {
        protected void PageLoad(object sender, System.EventArgs e)
        {
            bool permit = BrokerUser.HasPermission(LendersOffice.Security.Permission.CanModifyLoanPrograms);
            string type = RequestHelper.GetSafeQueryString("type");
            string name = RequestHelper.GetSafeQueryString("name");
            switch (type)
            {
                case "DU_UNDERWRITE":
                    WriteDuUnderwrite(name);
                    break;
                case "CASEFILE_EXPORT":
                    WriteCasefileExport(name);
                    break;
                case "CASEFILE_IMPORT":
                    WriteCasefileImport(name);
                    break;
                case "DWEB_CASEFILEIMPORT":
                    WriteDwebCasefileImport(name);
                    break;
                case "DWEB_CASEFILESTATUS":
                    WriteDwebCasefileStatus(name);
                    break;
                case "FREDDIE":
                    if (name == "XML")
                    {
                        WriteFreddie();
                    }
                    else if (name == "HTML")
                    {
                        WriteFreddieHtml();
                    }
                    break;
                case "DOCMAGIC":
                    WriteDocMagic();
                    break;
                case "DOCUTECH":
                    WriteDocuTech();
                    break;
                case "COMPLIANCEEASE":
                    WriteComplianceEase();
                    break;
                case "ENCOMPASS360BUY":
                    WriteEncompass360(E_Encompass360RequestT.BuysideLock);
                    break;
                case "ENCOMPASS360SELL":
                    WriteEncompass360(E_Encompass360RequestT.SellsideLock);
                    break;
                case "ENCOMPASS360REGISTER":
                    WriteEncompass360(E_Encompass360RequestT.UnderwriteOnly);
                    break;
                case "DATATRACEXPORT":
                    DataTracExport();
                    break;
                case "MISMO231":
                    Mismo231Export();
                    break;
                case "MISMO26":
                    MismoClosing26Export();
                    break;
                case "lpq":
                    CLFExportForLPQ();
                    break;
                case "FHA_CONNECTION":
                    WriteFhaConnection(name);
                    break;
            }
        }


        private void CLFExportForLPQ()
        {
            CLFExporter x = new CLFExporter();
            WriteXml(x.Export(LoanID));
        }

        private void MismoClosing26Export()
        {
            string xml = null;
            try
            {
                xml = LendersOffice.Conversions.GenericMismoClosing26.GenericMismoClosing26Exporter.Export(LoanID, PrincipalFactory.CurrentPrincipal);
            }
            catch (FeeDiscrepancyException e)
            {
                Response.Clear();
                WriteHtml(e.UserMessage);
            }
            WriteXml(xml);
        }
        private void Mismo231Export()
        {
            Closing231Exporter exporter = new Closing231Exporter(LoanID, "");
            WriteXml(exporter.Export());

        }
        private void DataTracExport()
        {
            DataTracExporter dtExporter = new DataTracExporter(LoanID, Broker.BrokerID, "Test", "asd", "dt", false/*updateExisting*/, DataTracExchange.E_DataTracOriginatorType.R);
            WriteXml(dtExporter.ExportXmlData());
        }
        private void WriteEncompass360(E_Encompass360RequestT eRequestType)
        {
            EncompassResponse encompassResponse = new EncompassResponse();
            string xml = encompassResponse.Export(LoanID, eRequestType);
            xml = xml.Replace("encoding=\"utf-16\"", "");

            WriteXml(xml);

        }
        private void WriteComplianceEase()
        {
            ComplianceEaseExporter exporter = new ComplianceEaseExporter(LoanID, ComplianceEase.E_TransmittalDataComplianceAuditType.PreClose);
            WriteXml(exporter.Export());

        }
        private void WriteDocuTech()
        {
            var packageType = DocuTech.E_DocumentsPackageType.InitialDisclosure;
            DocuTechExporter exporter = new DocuTechExporter(LoanID, packageType);
            try
            {
                WriteXml(exporter.Export("PRMLadmin", "docutech", packageType, PrincipalFactory.CurrentPrincipal));
            }
            catch (FeeDiscrepancyException e)
            {
                Response.Clear();
                WriteHtml(e.UserMessage);
            }
        }
        private void WriteDocMagic()
        {
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.BufferOutput = true;
            try
            {
                DsiDocumentServerRequest request = DocMagicMismoRequest.CreateSaveRequest("A", "B", "C", LoanID, null);
                using (StringWriter ms = new StringWriter())
                {
                    using (XmlWriter xw = XmlWriter.Create(ms, new XmlWriterSettings() { Indent = true, Encoding = new UTF8Encoding() }))
                    {
                        xw.WriteProcessingInstruction("xml", "version='1.0' encoding='UTF-8'");
                        request.WriteXml(xw);
                    }
                    Response.Write(ms.ToString());
                }
                Response.Flush();
                Response.Flush();
                Response.End();
            }
            catch (FeeDiscrepancyException e)
            {
                Response.Clear();
                WriteHtml(e.UserMessage);
            }
        }

        private void WriteFreddie()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ExternalDebugTool));
            dataLoan.InitLoad();

            if (dataLoan.sFreddieFeedbackResponseXml.Value == "")
            {
                WriteNoContent();
            }
            else
            {
                WriteXml(dataLoan.sFreddieFeedbackResponseXml.Value);
            }
        }

        private void WriteFreddieHtml()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ExternalDebugTool));
            dataLoan.InitLoad();

            WriteHtml(dataLoan.sFreddieFeedbackHtml.Value);
        }
        private void WriteDuUnderwrite(string name)
        {
            FnmaXisDuUnderwriteResponse xisResponse = new FnmaXisDuUnderwriteResponse(LoanID);
            if (!xisResponse.IsValid)
            {
                WriteNoContent();
            }
            else
            {
                switch (name)
                {
                    case "RAW":
                        WriteRawResponse(xisResponse);
                        break;
                    case "HTML_FINDINGS":
                        WriteHtml(xisResponse.FnmaFindingHtml);
                        break;
                    case "XML_FINDINGS":
                        //<!DOCTYPE CodifiedFindings SYSTEM \"CFFVersion1.1.dtd\">
                        WriteXml(xisResponse.FnmaFindingXml.Replace("<!DOCTYPE CodifiedFindings SYSTEM \"CFFVersion1.1.dtd\">", ""));
                        break;
                    default:
                        WriteNoContent();
                        break;
                }
            }
        }
        private void WriteCasefileExport(string name)
        {
            FnmaXisCasefileExportResponse xisResponse = new FnmaXisCasefileExportResponse(LoanID);
            if (!xisResponse.IsValid)
            {
                WriteNoContent();
            }
            else
            {
                switch (name)
                {
                    case "RAW":
                        WriteRawResponse(xisResponse);
                        break;
                    case "XML_FINDINGS":
                        WriteXml(xisResponse.FnmaFindingsXml.Replace("<!DOCTYPE CodifiedFindings SYSTEM \"CFFVersion1.1.dtd\">", ""));
                        break;

                    default:
                        WriteNoContent();
                        break;
                }

            }
        }
        private void WriteDwebCasefileStatus(string name)
        {
            FnmaDwebCasefileStatusResponse xisResponse = new FnmaDwebCasefileStatusResponse(LoanID);
            if (!xisResponse.IsValid)
            {
                WriteNoContent();
            }
            else
            {
                switch (name)
                {
                    case "RAW":
                        WriteRawResponse(xisResponse);
                        break;
                    default:
                        WriteNoContent();
                        break;
                }

            }
        }
        private void WriteDwebCasefileImport(string name)
        {
            FnmaDwebCasefileImportResponse xisResponse = new FnmaDwebCasefileImportResponse(LoanID);
            if (!xisResponse.IsValid)
            {
                WriteNoContent();
            }
            else
            {
                switch (name)
                {
                    case "RAW":
                        WriteRawResponse(xisResponse);
                        break;
                    default:
                        WriteNoContent();
                        break;
                }

            }
        }
        private void WriteCasefileImport(string name)
        {
            FnmaXisCasefileImportResponse xisResponse = new FnmaXisCasefileImportResponse(LoanID);
            if (!xisResponse.IsValid)
            {
                WriteNoContent();
            }
            else
            {
                switch (name)
                {
                    case "RAW":
                        WriteRawResponse(xisResponse);
                        break;
                    default:
                        WriteNoContent();
                        break;
                }

            }
        }

        private void WriteFhaConnection(string name)
        {
            AbstractFHAConnectionRequest request;
            switch (name)
            {
                case "CASE_ASSIGNMENT_ASSIGN":
                    request = new CaseNumberAssignmentRequest(LoanID, false);
                    break;
                case "CASE_ASSIGNMENT_UPDATE":
                    request = new CaseNumberAssignmentRequest(LoanID, true);
                    break;
                case "HOLDS_TRACKING":
                    request = new HoldsTrackingRequest(LoanID);
                    break;
                case "CASE_QUERY":
                    request = new CaseQueryRequest(LoanID);
                    break;
                case "CAIVRS_AUTHORIZATION":
                    request = new CAIVRSAuthorizationRequest(LoanID);
                    break;
                default:
                    WriteNoContent();
                    return;
            }

            WriteXml(request.ToString());
        }


        private void WriteNoContent()
        {
            Response.ContentType = "text/html";
            Response.Write("COULD NOT FIND RESPONSE");
            Response.Flush();
            Response.End();

        }

        private void WriteRawResponse(AbstractFnmaXisResponse xisResponse)
        {
            Response.ContentType = "text/plain";
            Response.AddHeader("Content-Disposition", "attachment; filename=\"raw.txt\"");
            Response.Write(xisResponse.RawData);
            Response.Flush();
            Response.End();
        }
        private void WriteHtml(string html)
        {
            Response.ContentType = "text/html";
            Response.Write(html);
            Response.Flush();
            Response.End();

        }
        private void WriteXml(string xml)
        {
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.BufferOutput = true;

            Response.Write(xml);

            Response.Flush();
            Response.End();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
