﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos.Test
{
    public partial class LogLastDisclosedDCalculationInfo : BaseLoanPage
    {
        protected override void LoadData()
        {
            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(LoanID, typeof(LogLastDisclosedDCalculationInfo));
            dataLoan.InitLoad();
            dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
            if (dataLoan.GFEArchives.Count() == 0)
            {
                Feedback.Text = "No Archive on file";
                return;
            }

            var dataAppPrimary = dataLoan.GetAppData(0);

            var dataLoanWithGFEArchiveApplied = new CPageData(LoanID, CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(LogLastDisclosedDCalculationInfo)).Union(
                CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release));
            dataLoanWithGFEArchiveApplied.InitLoad();
            var lastDisclosedGFEArchive = dataLoan.LastDisclosedGFEArchive;
            dataLoanWithGFEArchiveApplied.ApplyGFEArchiveExcludingFields(dataLoan.LastDisclosedGFEArchive, ConstApp.FieldsToExcludeWhenApplyingGFEArchiveForAprCalc);

            var dataAppPrimaryFromGFE = dataLoanWithGFEArchiveApplied.GetAppData(0);
            
            var exludedFieldNamesAndValues = new string[ConstApp.FieldsToExcludeWhenApplyingGFEArchiveForAprCalc.Count];
            int i = 0;
            foreach (var field in ConstApp.FieldsToExcludeWhenApplyingGFEArchiveForAprCalc)
            {
                string hybridVal = "not found.";
                try
                {
                    hybridVal = PageDataUtilities.GetValue(dataLoanWithGFEArchiveApplied, dataAppPrimaryFromGFE, field);
                }
                catch(CBaseException e)
                {
                    if(e.DeveloperMessage != field + " not found.")
                    {
                        throw;
                    }
                }
                string liveVal = "not found.";
                try
                {
                    liveVal = PageDataUtilities.GetValue(dataLoan, dataAppPrimary, field);
                }
                catch (CBaseException cbe)
                {
                    if (cbe.DeveloperMessage != field + " not found.")
                    {
                        throw;
                    }
                }
                
                object gfeVal = "skipped.";
                if(lastDisclosedGFEArchive.Fields.ContainsKey(field))
                {
                    gfeVal = lastDisclosedGFEArchive.Fields[field];
                }
                exludedFieldNamesAndValues[i] = string.Join("\t", new string[]{ field, liveVal, hybridVal, gfeVal.ToString()});
                i++;
            }

            var logTime = DateTime.Now;
            var msg = string.Format("LogLastDisclosedDCalculationInfo {2} sApr after apply GFEArchive excluding fields was {0}{1}" + 
                "Values of fields after applying the archive but were loaded from live data were:{1}"+
                "FieldName  Live   Hybrid  GFE{1}",
                dataLoanWithGFEArchiveApplied.sApr,
                Environment.NewLine,
                logTime);
            msg += string.Join(Environment.NewLine, exludedFieldNamesAndValues);
            Tools.LogInfo(msg);

            Feedback.Text = "Log generated around " + logTime + " with text LogLastDisclosedDCalculationInfo";
        }
    }
}
