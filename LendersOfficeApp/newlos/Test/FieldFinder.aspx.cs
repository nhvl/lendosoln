﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOfficeApp.LOAdmin.TaskBackendUtilities;
using System.Web.Services;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Test
{
    public partial class FieldFinder : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnInit(EventArgs e)
        {
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            DisplayCopyRight = false;
            base.OnInit(e);
        }

        //It's actually a List<anon type>, but this is the only way to pass it.
        [WebMethod]
        public static List<object> GetNamesLike(string prefix)
        {
            var result = FieldFinderExtension.FieldsStartWith(prefix).Take(20);
            return result.ToList();
        }

        [WebMethod]
        public static bool ServiceAvailable()
        {
            FieldFinderExtension.Available();
            return true;
        }
    }


    static class FieldFinderExtension
    {
        private static Dictionary<string, FieldEnumerator.LoanFieldInfo> FieldsByName = FieldEnumerator.ListLoanFieldsByName();
        private static IOrderedEnumerable<string> OrderedFields = FieldsByName.Keys.OrderBy(a => a);

        /// <summary>
        /// The static constructor for this class may take a long time to load (due to FieldEnumerator.ListLoanFieldsByName() 
        /// taking a while). This method will return after the class is initialized.
        /// </summary>
        public static void Available() { }

        public static IEnumerable<object> FieldsStartWith(string prefix)
        {
            return OrderedFields.StartsWith(prefix);
        }
        public static IEnumerable<object> StartsWith(this IOrderedEnumerable<string> me, string prefix)
        {

            bool seen = false;
            foreach (var element in me)
            {
                if (element.StartsWith(prefix, StringComparison.InvariantCultureIgnoreCase))
                {
                    seen = true;
                    //label, pages, id, name and url are magic strings the client interprets
                    yield return new
                    {
                        label = element.Replace(prefix, "<strong>" + prefix + "</strong>"),
                        value = element,
                        pages = from a in FieldsByName[element].Pages.OrderByDescending((a) => a.Fields.Count)
                                select new { name = a.Name, url = a.Url.Replace("~", DataAccess.Tools.VRoot) }
                    };
                }
                //If we've already seen at least one entry but now we're not matching any more,
                //we can stop.
                else if (seen)
                {
                    break;
                }
            }
        }
    }
}

