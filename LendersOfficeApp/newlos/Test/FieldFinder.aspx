﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FieldFinder.aspx.cs" Inherits="LendersOfficeApp.newlos.Test.FieldFinder" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">

        jQuery(function($) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: 'FieldFinder.aspx/ServiceAvailable',
                dataType: 'json',
                async: true,
                success: function(msg) {
                    $('#ReadyState').toggleClass('Ready NotReady').text("Service ready!");
                    window.focus();
                },
                failure: function(response) { alert("error - " + response); },
                error: function(XMLHttpRequest, textStatus, errorThrown) { alert(textStatus + " " + errorThrown); }
            });


            var FieldId = $("#FieldId")
                .autocomplete({
                    minLength: 2,
                    source: function(req, res) {
                        var term = req.term;
                        var DTO = { 'prefix': term };
                        var lastXhr = $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: 'FieldFinder.aspx/GetNamesLike',
                            data: JSON.stringify(DTO),
                            async: true,
                            success: function(data, status, xhr) {
                                //Debounce the xmlhttprequest -
                                //there may be many queued up at a given time, 
                                //but we only want to accept the most recent
                                if (lastXhr !== xhr) {
                                    return;
                                }
                                res(data.d);
                            }
                        });
                    },

                    select: function(event, ui) {
                        //UI.item.pages is [ { "page name", "page url"} .. ]"
                        var pages = ui.item.pages;
                        var fieldId = ui.item.value;
                        var urlList = [];

                        if (pages[0].name == "Everything Else") {
                            $('#Pages').empty().append("<li class='Error'>No page found</li>");
                            return;
                        }

                        $.each(pages, function(idx) {

                            urlList.push('<li class="Action VisitPage">');
                            urlList.push(this.name);
                            urlList.push('<input type="Hidden" class="PageUrl" value="');
                            urlList.push(this.url);
                            urlList.push('"/>');
                            urlList.push('<input type="Hidden" class="FieldId" value="');
                            urlList.push(fieldId);
                            urlList.push('"/></li>');

                        });

                        $('#Pages').empty().append(urlList.join(''));
                    }

                });

            //Override the rendering method so the bolding in the response works
            FieldId.data("ui-autocomplete")._renderItem = function(ul, item) {
                return $("<li></li>")
						.data("ui-autocomplete-item", item)
						.append("<a>" + item.label + "</a>")
						.appendTo(ul);
            };

            $('#Pages').on('click', 'li', function() {
                var url = $(this).children('.PageUrl').val();
                var fieldId = $(this).children('.FieldId').val();

                var firstDelim = "?";
                if (url.indexOf('?') != -1) {
                    firstDelim = "&";
                }

                var appAndLoanId = '&loanid=' + ML.sLId + '&appid=' + ML.aAppId;

                window.opener.location = url + firstDelim + "highlightId=" + encodeURIComponent(fieldId) + appAndLoanId;
                window.opener.focus();
            });

            $('#FieldId').keypress(function(e) {
                if (e.which == 13) {
                    e.preventDefault();
                }
            });

        });
    
    </script>

    <style type="text/css">
        #Input
        {
            float: left;
        }
        #Output
        {
            float: right;
        }
        #Pages
        {
            list-style-type: none;
        }
        .Action
        {
            cursor: pointer;
            color: Blue;
            text-decoration: underline;
        }
        #Header
        {
            margin-bottom: 20px;
        }
        #Main
        {
            padding-left: 10px;
            padding-right: 10px;
        }
        .NotReady, .Error
        {
            color: Red;
        }
        .Ready
        {
            color: Green;
        }
    </style>
</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
    <div>
        <div id="Header" class="MainRightHeader">
            Field Finder</div>
        <div id="Main">
            <div id="Output">
                <ul id="Pages">
                </ul>
            </div>
            <div id="Input">
                <label>
                    Field id:<input type="text" id="FieldId" /></label>
                <div id="ReadyState" class="NotReady">
                    Service not ready!</div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
