﻿namespace LendersOfficeApp.newlos.Test
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Web.UI.WebControls;
    using System.Web.Services;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Security;

    public partial class FieldDebugger : BaseLoanPage
    {

        protected void Page_Init(object sender, EventArgs e)
        {
            this.DisplayCopyRight = false;
            
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterCSS("jquery-ui-1.11.custom.css");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
            {
                throw new AccessDenied();
            }

            this.ArchiveToUse.Items.Add(new ListItem("Live data", Guid.Empty.ToString()) { Selected = true });

            if (this.Broker.IsGFEandCoCVersioningEnabled)
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(FieldDebugger));

                dataLoan.InitLoad();

                var tridTargetVersion = dataLoan.sTridTargetRegulationVersionT;
                foreach (var archive in dataLoan.sClosingCostArchive)
                {
                    var archiveType = archive.ClosingCostArchiveType.ToString();

                    if (archive.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate &&
                        tridTargetVersion == LendersOffice.ObjLib.TRID2.TridTargetRegulationVersionT.TRID2017)
                    {
                        if (ClosingCostArchive.IsDisclosedDataArchive(tridTargetVersion, archive))
                        {
                            archiveType += " (Disclosed)";
                        }
                        else
                        {
                            archiveType += " (Tolerance)";
                        }
                    }

                    this.ArchiveToUse.Items.Add(new ListItem(archiveType + ": " + archive.DateArchived, archive.Id.ToString()));
                }
            }
        }

        [WebMethod]
        public static object GetFieldAndDependencies(Guid sLId, Guid aAppId, string fieldID, Guid? archiveId = null)
        {
            if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.CanModifyLoanPrograms))
            {
                throw new AccessDenied();
            }

            try
            {
                bool isHeloc = Tools.IsLoanAHELOC(PrincipalFactory.CurrentPrincipal, sLId);
                fieldID = DependencyUtils.NormalizeMemberName(fieldID?.Trim() ?? string.Empty);

                // OPM 466743 - Add support for the sStatusProgressT field, which does not actually exist,
                // but is used in loan reporting and workflow because it provides an ordered list of statuses.
                if (fieldID.Equals("sStatusProgressT", StringComparison.OrdinalIgnoreCase))
                {
                    fieldID = "sStatusT";
                }

                var dependentFieldSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

                if (fieldID.StartsWith("GetAgentOfRole", StringComparison.OrdinalIgnoreCase))
                {
                    dependentFieldSet.Add("sfGetAgentOfRole");
                }
                else if (fieldID.StartsWith("GetPreparerOfForm", StringComparison.OrdinalIgnoreCase))
                {
                    dependentFieldSet.Add("sfGetPreparerOfForm");
                }
                else
                {
                    PropertyInfo property = null;

                    if (fieldID.StartsWith("a", StringComparison.OrdinalIgnoreCase))
                    {
                        property = Tools.GetProperty(fieldID, typeof(CAppBase));
                    }
                    else if (fieldID.StartsWith("s", StringComparison.OrdinalIgnoreCase))
                    {
                        property = Tools.GetProperty(fieldID, typeof(CPageBase), typeof(CPageHelocBase));
                    }

                    property = property ?? Tools.GetProperty(fieldID, typeof(CPageBase), typeof(CAppBase), typeof(CPageHelocBase));
                    if (property == null)
                    {
                        return new { ErrorMessage = "Field '" + fieldID + "' not recognized." };
                    }

                    fieldID = property.Name;

                    ////Case 477201 - If loan is HELOC and type is CPageBase, there could be a CPageHelocBase analog that should be used instead.
                    if (isHeloc && property.DeclaringType == typeof(CPageBase))
                    {
                        property = Tools.GetProperty(fieldID, typeof(CPageHelocBase)) ?? property;
                    }

                    foreach (DependsOnAttribute attribute in property.GetCustomAttributes(typeof(DependsOnAttribute), false))
                    {
                        foreach (var o in attribute.GetList())
                        {
                            dependentFieldSet.Add(o.Name);
                        }
                    }
                }

                var necessaryFields = new List<string>()
                {
                    fieldID
                };

                if (archiveId.HasValue && archiveId != Guid.Empty)
                {
                    necessaryFields.Add("sfApplyClosingCostArchive");
                }

                dependentFieldSet.Remove(fieldID);

                var selectStatementProvider = CSelectStatementProvider.GetProviderForTargets(necessaryFields, expandTriggersIfNeeded: true);
                var dataLoan = isHeloc ? new CPageHelocBase(sLId, "FieldDebugger", selectStatementProvider) : new CPageBaseWrapped(sLId, "FieldDebugger", selectStatementProvider);

                dataLoan.InitLoad();

                if (archiveId.HasValue && archiveId != Guid.Empty)
                {
                    dataLoan.ApplyClosingCostArchive(archiveId.Value);
                }

                var dataApp = dataLoan.GetAppData(aAppId);

                var dependentFieldItems = new List<FieldDebuggerItem>(dependentFieldSet.Count);

                foreach (var field in dependentFieldSet)
                {
                    dependentFieldItems.Add(GetFieldValue(field, dataLoan, dataApp));
                }

                return new
                {
                    Field = GetFieldValue(fieldID, dataLoan, dataApp),
                    DependentFields = dependentFieldItems.ToArray()
                };
            }
            catch (Exception exc)
            {
                // Let the CPE look up the cause.
                Tools.LogWarning($"[FieldDependencyDebugger] sLId: {sLId} aAppId: {aAppId} field: [{fieldID}] failed", exc);
                throw;
            }
        }

        private static FieldDebuggerItem GetFieldValue(string field, CPageBase loan, CAppBase app)
        {
            PropertyInfo property = Tools.GetProperty(field, typeof(CPageBase), typeof(CPageHelocBase));
            if (property != null)
            {
                return GetPropertyValue(loan, property);
            }

            property = Tools.GetProperty(field, typeof(CAppBase));
            if (property != null)
            {
                return GetPropertyValue(app, property);
            }

            return new FieldDebuggerItem
            {
                FieldName = field,
                Message = "UNABLE TO FIND MATCHING PROPERTY IN (" + string.Join(", ", typeof(CPageBase), typeof(CPageHelocBase), typeof(CAppBase)) + ")"
            };
        }

        private static FieldDebuggerItem GetPropertyValue(object dataSource, PropertyInfo property)
        {
            try
            {
                object value = property.GetGetMethod(nonPublic: true)?.Invoke(dataSource, null);

                string stringValue = value?.ToString();
                string jsonValue = SerializationHelper.JsonNetSafeSerializeBeautifully(value);
                string message = null;
                if (stringValue == null)
                {
                    message = "FIELD VALUE IS NULL";
                }
                else if (stringValue == string.Empty || jsonValue == string.Empty || jsonValue == "\"\"")
                {
                    message = "FIELD VALUE IS EMPTY STRING OR NOT DEFINED";
                }

                return new FieldDebuggerItem
                {
                    FieldName = property.Name,
                    Message = message,
                    FieldValue = message == null ? stringValue : null,
                    FieldJson = message == null ? jsonValue : null
                };
            }
            catch (TargetInvocationException exc)
            {
                Tools.LogWarning($"[FieldDependencyDebugger] Could not determine value for property {property.Name}", exc);
                return new FieldDebuggerItem
                {
                    FieldName = property.Name,
                    Message = "COULD NOT DETERMINE FIELD VALUE"
                };
            }
        }
    }
}
