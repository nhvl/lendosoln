﻿//-----------------------------------------------------------------------
// <copyright file="LoXmlWithXsltTester.aspx.cs" company="Meridianlink">
//     Copyright (c) Sprocket Enterprises. All rights reserved.
// </copyright>
// <author>Scott Kibler</author>
// <summary>
//  This is for testing combining loxml with xslt.
// </summary>
//-----------------------------------------------------------------------

namespace LendersOfficeApp.newlos.Test
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml.Xsl;
    using DataAccess;
    using LendersOffice.Common;
    

    /// <summary>
    /// Import class handle loan overwrite. 
    /// </summary>
    public partial class LoXmlWithXsltTester : BaseLoanPage
    {
        protected void PageLoad(object sender, System.EventArgs e)
        {
            this.ErrorStatus.Text = string.Empty;
        }

        /// <summary>
        /// Transforms the xml with the specified xsl data.  May also call LOFormatExporter.Export.
        /// <param name="sender">The sender of the method.</param>
        /// <param name="e">The arguments for the method.</param>
        protected void ClickTransform(object sender, EventArgs e)
        {
            try
            {
                var transformDefinitionFile = TempFileUtils.NewTempFile();
                File.WriteAllText(transformDefinitionFile.Value, this.XslElement.Text.Trim());

                var xsltArguments = new XsltArgumentList();
                var argValueByArgName = SerializationHelper.JsonNetDeserialize<Dictionary<string, string>>(this.XsltArgumentsElement.Text.Trim());
                foreach (var kvp in argValueByArgName)
                {
                    xsltArguments.AddParam(kvp.Key, string.Empty, kvp.Value);
                }

                var resultantXml = XslTransformHelper.Transform(transformDefinitionFile.Value, this.NormalXmlElement.Text.Trim(), xsltArguments);
                this.ResultantXmlElement.Text = resultantXml;
            }
            catch (Exception ex)
            {
                this.ErrorStatus.Text += ex.ToString();
            }
        }

        protected void ClickExport(object sender, EventArgs e)
        {
            try
            {
                var loanExportXml = this.LoXmlElement.Text.Trim();
                this.NormalXmlElement.Text = LendersOffice.Conversions.LOFormatExporter.Export(this.LoanID, this.BrokerUser, loanExportXml);
            }
            catch (Exception ex)
            {
                this.ErrorStatus.Text += Environment.NewLine + ex.ToString();
            }
        }

        protected void ClickExportAndConvert(object sender, EventArgs e)
        {
            this.ClickExport(sender, e);
            this.ClickTransform(sender, e);
        }

        override protected void OnInit(EventArgs e)
        {
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            
            this.InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
    }
}
