<%@ Page language="c#" Codebehind="TestViewXisResponse.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Test.TestViewXisResponse" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head runat="server">
    <title>INTERNAL DEBUGGING TOOLS</title>
  </head>
  <body MS_POSITIONING="FlowLayout" class="RightBackground" >
    <script type="text/javascript" src="../../inc/ModelessDlg.js"> </script>
  
	<script type="text/javascript">
<!--
function f_viewPart(sResponseType, sPartName) {
  var additionalParams = '';
  if (sResponseType === 'LOXML') additionalParams = '&formatTarget=' + document.getElementById('m_formatTargetForLOXml').value;
  else if (sResponseType === 'MISMO26') { 
      var isClosingPackage = <%=AspxTools.JsGetElementById(closingPackage26)%>;
      var temporaryArchive = <%=AspxTools.JsGetElementById(useTemporaryArchive26)%>;
      additionalParams = '&vendorName=' + document.getElementById('m_GenericMismoClosing26VendorName').value + '&closingpackage=' + isClosingPackage.checked + '&temporaryArchive=' + temporaryArchive.checked;
  }
  else if (sResponseType === 'MISMO33' || sResponseType === 'MISMO33EXT' || sResponseType === 'MISMO34' || sResponseType === 'MISMO34EXT' ) {
      var isClosingPackage = <%=AspxTools.JsGetElementById(closingPackage33)%>;
      var vendorT = <%=AspxTools.JsGetElementById(mismo3Vendor)%>;
      var temporaryArchive = <%=AspxTools.JsGetElementById(useTemporaryArchive33)%>;
      additionalParams = '&mismo3Vendor=' + vendorT[vendorT.selectedIndex].value + '&closingpackage=' + isClosingPackage.checked + '&temporaryArchive=' + temporaryArchive.checked;
  }
  else if (sResponseType === 'MI_MISMO231')
  {
      additionalParams = '&VendorID=' + document.getElementById('MIVendor').value;
  }
  
  window.open('TestViewXisResponse.aspx?loanid=' + ML.sLId +'&type=' + sResponseType + '&name=' + sPartName + additionalParams);
}

function FieldFinder() {
    /*window.parent.showModelessDialog(ML.VirtualRoot + '/newlos/test/fieldfinder.aspx?loanid=' + ML.sLId + '&appid=' + ML.aAppId, window.parent,
"dialogWidth: 400px; dialogHeight: 550px; center: yes; resizable: yes; scroll: yes; status: yes; help: no;");//*/
    window.open('fieldfinder.aspx?loanid=' + ML.sLId + '&appid=' + ML.aAppId, "test",
    "width=440, height=550, center=yes, resizable=yes, scroll=yes, status=yes, help=no");//*/

}

function OpenPage_OptionalImportExportTool() {
    OpenPage('LoanOverwriteImportExport.aspx');
}

function OpenPage_LoXmlXsltTester() {
    OpenPage('LoXmlWithXsltTester.aspx');
}

function OpenPage_Irs4506TXsltTester() {
    OpenPage('Irs4506TVendorXsltTester.aspx', 
        'irs4506T' + ML.sLId, 
        'width=600, height=650, center=yes, resizable=yes, scroll=yes, status=yes, help=no');
}

function OpenPage(pageName, windowName, windowFeatures) {
    window.open(pageName + '?loanid=' + ML.sLId + '&appid=' + ML.aAppId, windowName, windowFeatures);
}

function f_LoanUpdateWarning() {
    var code = Math.floor(Math.random() * 1000) + '';
    var ans = prompt("WARNING: Data replaced by import will be PERMANENTLY and irrevocably LOST forever without ability to restore it.  " +
	  "Please type '" + code + "' below to confirm it:", "");
    var doIt = (ans != null && ans == code);

    if (doIt == false && ans != null)
        alert("Code does not match.  Operations will not be performed.");

    return doIt;
}

window.onload = function() {
    document.getElementById('_ReadOnly').value = "True"; // Don't allow saving on this page.
};
//-->
</script>
    <div class="MainRightHeader">INTERNAL DEBUGGING TOOLS</div>
    <form id="TestViewXisResponse" method="post" runat="server">

    <ul>
        <li><a href="#" onclick="OpenPage('FieldDebugger.aspx', null, 'height=500,width=750,resizable,scrollbars');return false;">Field Debugger</a></li>
      <li>
      DU Underwrite Response
      <ul>
        <li><a href='#' onclick="f_viewPart('DU_UNDERWRITE', 'RAW');">Raw Response</a></li>
        <li><a href='#' onclick="f_viewPart('DU_UNDERWRITE', 'HTML_FINDINGS');">HTML Findings</a></li>
        <li><a href='#' onclick="f_viewPart('DU_UNDERWRITE', 'XML_FINDINGS');">XML Findings</a></li>
      </ul>
      </li>
      <li>Casefile Import Response
      <ul>
        <li><a href='#' onclick="f_viewPart('CASEFILE_IMPORT', 'RAW');">Raw Response</a></li>
        <li>Routing Output</li>
        <li>Control Output</li>
      </ul>
      
      </li>
      
      <li>Casefile Export Response
      <ul>
        <li><a href='#' onclick="f_viewPart('CASEFILE_EXPORT', 'RAW');">Raw Response</a></li>
        <li><a href='#' onclick="f_viewPart('CASEFILE_EXPORT', 'XML_FINDINGS');">DU Findings XML</a></li>
        <li>Routing Output</li>
        <li>Control Output</li>
      </ul>
      
      </li>
      <li>DWEB Casefile Import Response
      <ul>
        <li><a href='#' onclick="f_viewPart('DWEB_CASEFILEIMPORT', 'RAW');">Raw Response</a></li>
        <li>Routing Output</li>
        <li>Control Output</li>
      </ul>
      
      </li>     
      <li>DWEB Casefile Status Response
      <ul>
        <li><a href='#' onclick="f_viewPart('DWEB_CASEFILESTATUS', 'RAW');">Raw Response</a></li>
        <li>Routing Output</li>
        <li>Control Output</li>
      </ul>
      
      </li>          
      <li>Loan Prospector / Loan Product Advisor MISMO
        <ul>
          <li><a href='#' onclick="f_viewPart('LPA_MISMO', 'XML');">Loan Prospector / Loan Product Advisor MISMO</a></li>
        </ul>
      </li> 
      <li>Loan Prospector Feedback
        <ul>
          <li><a href='#' onclick="f_viewPart('FREDDIE', 'XML');">Loan Prospector Feedback Xml</a></li>
          <li><a href="#" onclick="f_viewPart('FREDDIE', 'HTML')">Loan Prospector Feedback HTML (sFreddieFeedbackHtml) </a></li>
        </ul>
      </li> 
      <li>Doc Magic <a href='#' onclick="f_viewPart('DOCMAGIC', 'XML');">Closing Mismo Loan XML</a></li>
      <li>DocuTech <a href="#" onclick="f_viewPart('DOCUTECH', 'XML');">DOCUMENT_REQUEST_IMPORT</a></li>
      <li>ComplianceEase <a href="#" onclick="f_viewPart('COMPLIANCEEASE', 'XML');">Export</a></li>
      <li>Encompass 360
        <ul>
          <li><a href="#" onclick="f_viewPart('ENCOMPASS360BUY', 'XML');">Buy-side lock</a></li>
          <li><a href="#" onclick="f_viewPart('ENCOMPASS360SELL', 'XML');">Sell-side lock</a></li>
          <li><a href="#" onclick="f_viewPart('ENCOMPASS360REGISTER', 'XML');">Loan Registration</a></li>
        </ul>
      </li>
      <li>Datatrac</li>
      <ul>
        <li><a href="#" onclick="return f_viewPart('DATATRACEXPORT', 'XML');">Export</a></li>
      </ul>
      <li>DataVerify DRIVE <a href="#" onclick="return f_viewPart('DRIVE', 'XML');">Export</a></li>
      <li>Closing 2.3.1</li>
      <ul>
        <li><a href="#" onclick="return f_viewPart('MISMO231', 'XML');">Closing 2.3.1 XML</a></li>
      </ul>      
        <li>
            Mortgage Insurance - MISMO 2.3.1
        </li>
        <ul>
            <li>
                <a href="#" onclick="return f_viewPart('MI_MISMO231', document.getElementById('MIOrderType').value);">Mortgage Insurance XML</a>
                <br />
                Vendor: 
                <asp:DropDownList ID="MIVendor" runat="server" />
                Order Type: 
                <select ID="MIOrderType" >
                    <option selected="true">Quote</option>
                    <option>Policy</option>
                </select>
            </li>
        </ul>
      <li>Generic Mismo Closing 2.6</li>
      <ul>
        <li>Closing disclosure package <asp:CheckBox ID="closingPackage26" runat="server" Checked="false" /></li>
        <li>Use Temporary Archive from Audit?<asp:CheckBox ID="useTemporaryArchive26" runat="server" Checked="false" />(Will throw Exception if archive does not exist)</li>
        <li><a href="#" onclick="return f_viewPart('MISMO26', 'XML');">Generic Mismo Closing 2.6 XML</a>  VendorName: <input type="text" id="m_GenericMismoClosing26VendorName" value="DocuTech" title="Hint: 'DocuTech' is case-sensitive" /></li>
        <li><asp:DropDownList ID="m_formatTargetForLOXml" runat="server" readOnly/><a href="#" onclick="return f_viewPart('LOXML', 'XML');">LOXmlFormat Xml</a></li>
      </ul>
      <li>MISMO 3</li>
      <ul>
        <li>Vendor: <asp:DropDownList ID="mismo3Vendor" runat="server" readOnly/></li>
        <li>Closing disclosure package <asp:CheckBox ID="closingPackage33" runat="server" Checked="false" /></li>
        <li>Use Temporary Archive from Audit?<asp:CheckBox ID="useTemporaryArchive33" runat="server" Checked="false" />(Will throw Exception if archive does not exist)</li>
        <li><a href="#" onclick="return f_viewPart('MISMO33', 'XML');">MISMO 3.3 XML</a></li>
        <li><a href="#" onclick="return f_viewPart('MISMO33EXT', 'XML');">MISMO 3.3 XML with extensions</a></li>
        <li><a href="#" onclick="return f_viewPart('MISMO34', 'XML');">MISMO 3.4 XML</a></li>
        <li><a href="#" onclick="return f_viewPart('MISMO34EXT', 'XML');">MISMO 3.4 XML with extensions</a></li>
      </ul>
      <li>Audit prototype</li>
      <ul>
        <li><a href="#" onclick="audit()">Open doc magic audit prototype</a></li>
      </ul>
        
      <li>LPQ</li>
      <ul>
        <li><a href="#" onclick="f_viewPart('lpq', 'XML')">Export to LPQ</a></li>
      </ul>
      <li>CertificateXmlData</li>
        <ul><li><a href="#" onclick="f_viewPart('CertificateXmlData', 'XML')">CertificateXmlData</a></li></ul>
      <li>PmlLoanSummaryXmlData</li>
        <ul><li><a href="#" onclick="f_viewPart('PmlLoanSummaryXmlData', 'XML')">PmlLoanSummaryXmlData (use in pml_loanview.xslt.config, PmlRateLockConfirmation.xslt.config, pml_certificate.xslt.config)</a></li></ul>
        <li>
            <a href="#" onclick="f_viewPart('ARCHIVE_COMPARE', 'HTML')">Current Data vs Latest Archive</a>
        </li>
                <li>
            <a href="#" onclick="OpenPage('ArchiveView.aspx', null, 'height=1000,width=1200,resizable,scrollbars');return false;" >Closing Cost Archive Viewer</a>
        </li>
        <li>sClosingCostSetJsonContent</li>
        <ul>
            <li><a href="#" onclick="f_viewPart('sClosingCostSetJsonContent', 'HTML')">sClosingCostSetJsonContent</a></li>
        </ul>
		<li>
            <a href="#" onclick="OpenPage('LogLastDisclosedDCalculationInfo.aspx')">Last Disclosed Date Log Generator</a>
        </li>
      <li>FHA Connection B2G</li>
      <ul>
        <li><a href="#" onclick="f_viewPart('FHA_CONNECTION', 'CASE_ASSIGNMENT_ASSIGN')">Case Number Assignment</a></li>
        <li><a href="#" onclick="f_viewPart('FHA_CONNECTION', 'CASE_ASSIGNMENT_UPDATE')">Case Number Update</a></li>
        <li><a href="#" onclick="f_viewPart('FHA_CONNECTION', 'HOLDS_TRACKING')">Holds Tracking</a></li>
        <li><a href="#" onclick="f_viewPart('FHA_CONNECTION', 'CASE_QUERY')">Case Query</a></li>
        <li><a href="#" onclick="f_viewPart('FHA_CONNECTION', 'CAIVRS_AUTHORIZATION')">CAIVRS Authorization</a></li>
      </ul>
      <li><a href="#" onclick="f_viewPart('TotalScorecard', 'XML')">Total Scorecard</a></li>
      <li>Field finder</li>      <ul>        <li><a href="#" onclick="FieldFinder()">Field Finder</a></li>      </ul>
      <li><asp:Button runat="server" ID="UpdateCacheButton" OnClick="UpdateCacheButton_Click" Text="Update Loan File Cache" /></li>
      <li>Test LoXml + Xsl transform</li> <ul><li><a href="#" onclick="OpenPage_LoXmlXsltTester()">LoXml Export and Transform Tester</a></li></ul>
      <li>Test Irs4506T LoXml + Xsl transform</li> <ul><li><a href="#" onclick="OpenPage_Irs4506TXsltTester()">Irs4506T LoXml Export and Transform Tester</a></li></ul>
    
      <li>Export/Import Loan File
      <ul>
        <li><a href="#" runat="server" ID="LoanOverwriteToolLink" onclick="if (!hasDisabledAttr(this)) { OpenPage_OptionalImportExportTool(); }">Loan Overwrite Export/Import Tool (Overwrite current loan with xml loan file)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;New!</a></li><br/><br/>
      </ul>  
      </li>
    </ul>
     </form>
  </body>
</html>
