﻿namespace LendersOfficeApp.newlos.Test
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Web.Services;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Data.Common;

    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Pdf;
    using LendersOffice.Rolodex;
    using LendersOffice.Security;
    using LendersOfficeApp.newlos.Forms;
    using LendersOffice.Migration;

    [DataContract]
    public class SampleClosingCostFeeViewModel
    {
        public SampleClosingCostFeeViewModel()
        {
            VRoot = Tools.VRoot;
        }

        [DataMember]
        public E_sClosingCostFeeVersionT sClosingCostFeeVersionT { get; set; }


        [DataMember]
        public string sLAmtCalc { get; set; }

        [DataMember]
        public bool sLAmtLckd { get; set; }

        [DataMember]
        public decimal TotalAmount
        {
            get
            {
                decimal total = 0;

                if (SectionList != null)
                {
                    foreach (BorrowerClosingCostFeeSection section in SectionList)
                    {
                        if (section.FilteredClosingCostFeeList != null)
                        {
                            foreach (BorrowerClosingCostFee fee in section.FilteredClosingCostFeeList)
                            {
                                total += fee.TotalAmount;
                            }
                        }
                    }

                }
                return total;
            }
            private set { }
        }

        [DataMember]
        public IEnumerable<BorrowerClosingCostFeeSection> SectionList { get; set; }

        [DataMember]
        public bool sIsRequireFeesFromDropDown { get; set; }

        [DataMember]
        public bool sIsManuallySetThirdPartyAffiliateProps { get; set; }

        [DataMember]
        public string VRoot
        {
            get;
            set;
        }

        [DataMember]
        public string sGfeInitialDisclosureD { get; set; }
        [DataMember]
        public string sGfeRedisclosureD { get; set; }
        [DataMember]
        public bool IsProtectDisclosureDates { get; set; }

        [DataMember]
        public bool ByPassBgCalcForGfeAsDefault { get; set; }

        [DataMember]
        public string GfeTilCompanyName { get; set; }
        [DataMember]
        public string GfeTilStreetAddr { get; set; }
        [DataMember]
        public string GfeTilCity { get; set; }
        [DataMember]
        public string GfeTilState { get; set; }
        [DataMember]
        public string GfeTilZip { get; set; }
        [DataMember]
        public string GfeTilPhoneOfCompany { get; set; }
        [DataMember]
        public string GfeTilEmailAddr { get; set; }
        [DataMember]
        public string GfeTilPrepareDate { get; set; }
        [DataMember]
        public string GfeTilPrepareDateToolTip { get; set; }
        [DataMember]
        public bool trPreparedDateVisible { get; set; }
        [DataMember]
        public bool GfeTilIsLocked { get; set; }
        [DataMember]
        public DataAccess.E_AgentRoleT GfeTilAgentRoleT { get; set; }

        [DataMember]
        public bool IsRemovePreparedDates { get; set; }
        [DataMember]
        public string sLpTemplateNm { get; set; }
        [DataMember]
        public string sCcTemplateNm { get; set; }

        [DataMember]
        public string sFinalLAmt { get; set; }
        [DataMember]
        public string sNoteIR { get; set; }
        [DataMember]
        public string sTerm { get; set; }
        [DataMember]
        public string sDue { get; set; }
        [DataMember]
        public int sFinMethT { get; set; }
        [DataMember]
        public bool sSchedDueD1Lckd { get; set; }
        [DataMember]
        public string sSchedDueD1 { get; set; }
        [DataMember]
        public bool sEstCloseDLckd { get; set; }
        [DataMember]
        public string sEstCloseD { get; set; }
        [DataMember]
        public bool sConsummationDLckd { get; set; }
        [DataMember]
        public string sConsummationD { get; set; }
        [DataMember]
        public string sDaysInYr { get; set; }
        [DataMember]
        public string sRAdj1stCapR { get; set; }
        [DataMember]
        public string sRAdjCapR { get; set; }
        [DataMember]
        public string sRAdj1stCapMon { get; set; }
        [DataMember]
        public string sRAdjCapMon { get; set; }
        [DataMember]
        public string sRAdjLifeCapR { get; set; }
        [DataMember]
        public string sPmtAdjCapR { get; set; }
        [DataMember]
        public string sPmtAdjRecastPeriodMon { get; set; }
        [DataMember]
        public string sPmtAdjCapMon { get; set; }
        [DataMember]
        public string sPmtAdjRecastStop { get; set; }
        [DataMember]
        public string sPmtAdjMaxBalPc { get; set; }
        [DataMember]
        public string sIOnlyMon { get; set; }

        [DataMember]
        public bool sGfeNoteIRAvailTillDLckd { get; set; }
        [DataMember]
        public string sGfeNoteIRAvailTillD { get; set; }
        [DataMember]
        public bool sIsPrintTimeForGfeNoteIRAvailTillD { get; set; }
        [DataMember]
        public string sGfeNoteIRAvailTillD_Time_hour { get; set; }
        [DataMember]
        public string sGfeNoteIRAvailTillD_Time_minute { get; set; }
        [DataMember]
        public string sGfeNoteIRAvailTillD_Time_am { get; set; }
        [DataMember]
        public int sGfeNoteIRAvailTillDTimeZoneT { get; set; }
        [DataMember]
        public bool sGfeEstScAvailTillDLckd { get; set; }
        [DataMember]
        public string sGfeEstScAvailTillD { get; set; }
        [DataMember]
        public bool sIsPrintTimeForsGfeEstScAvailTillD { get; set; }
        [DataMember]
        public string sGfeEstScAvailTillD_Time_hour { get; set; }
        [DataMember]
        public string sGfeEstScAvailTillD_Time_minute { get; set; }
        [DataMember]
        public string sGfeEstScAvailTillD_Time_am { get; set; }
        [DataMember]
        public int sGfeEstScAvailTillDTimeZoneT { get; set; }
        [DataMember]
        public bool sGfeRateLockPeriodLckd { get; set; }
        [DataMember]
        public string sGfeRateLockPeriod { get; set; }
        [DataMember]
        public string sGfeLockPeriodBeforeSettlement { get; set; }
        [DataMember]
        public bool sGfeCanRateIncrease { get; set; }
        [DataMember]
        public string sRLifeCapR { get; set; }
        [DataMember]
        public string sGfeFirstInterestChangeIn { get; set; }
        [DataMember]
        public bool sGfeCanLoanBalanceIncrease { get; set; }
        [DataMember]
        public string sGfeMaxLoanBalance { get; set; }
        [DataMember]
        public string sGfeFirstPaymentChangeIn { get; set; }
        [DataMember]
        public string sGfeFirstAdjProThisMPmtAndMIns { get; set; }
        [DataMember]
        public bool sGfeCanPaymentIncrease { get; set; }
        [DataMember]
        public string sGfeMaxProThisMPmtAndMIns { get; set; }
        [DataMember]
        public bool sGfeHavePpmtPenalty { get; set; }
        [DataMember]
        public string sGfeMaxPpmtPenaltyAmt { get; set; }
        [DataMember]
        public bool sGfeIsBalloon { get; set; }
        [DataMember]
        public string sGfeBalloonPmt { get; set; }
        [DataMember]
        public string sGfeBalloonDueInYrs { get; set; }
        [DataMember]
        public bool sMldsHasImpound { get; set; }

        [DataMember]
        public bool boolTrue = true;
        [DataMember]
        public bool boolFalse = false;

        [DataMember]
        public string sGfeOriginationF { get; set; }
        [DataMember]
        public string sLDiscnt { get; set; }
        [DataMember]
        public string sGfeRequiredServicesTotalFee { get; set; }
        [DataMember]
        public string sGfeServicesYouShopTotalFee { get; set; }
        [DataMember]
        public string sGfeInitialImpoundDeposit { get; set; }
        [DataMember]
        public string sGfeTotalOtherSettlementServiceFee { get; set; }
        [DataMember]
        public string sGfeLenderTitleTotalFee { get; set; }
        [DataMember]
        public string sGfeGovtRecTotalFee { get; set; }
        [DataMember]
        public string sGfeDailyInterestTotalFee { get; set; }
        [DataMember]
        public string sGfeAdjOriginationCharge { get; set; }
        [DataMember]
        public string sGfeOwnerTitleTotalFee { get; set; }
        [DataMember]
        public string sGfeTransferTaxTotalFee { get; set; }
        [DataMember]
        public string sGfeHomeOwnerInsuranceTotalFee { get; set; }
        [DataMember]
        public string sGfeNotApplicableF { get; set; }

        [DataMember]
        public string sGfeTradeOffLowerCCLoanAmt { get; set; }
        [DataMember]
        public string sGfeTradeOffLowerRateLoanAmt { get; set; }
        [DataMember]
        public string sGfeTradeOffLowerCCNoteIR { get; set; }
        [DataMember]
        public string sGfeTradeOffLowerRateNoteIR { get; set; }
        [DataMember]
        public string sGfeProThisMPmtAndMIns { get; set; }
        [DataMember]
        public string sGfeTradeOffLowerCCMPmtAndMIns { get; set; }
        [DataMember]
        public string sGfeTradeOffLowerRateMPmtAndMIns { get; set; }
        [DataMember]
        public string sGfeTradeOffLowerCCMPmtAndMInsDiff { get; set; }
        [DataMember]
        public string sGfeTradeOffLowerRateMPmtAndMInsDiff { get; set; }
        [DataMember]
        public string sGfeTradeOffLowerCCClosingCostDiff { get; set; }
        [DataMember]
        public string sGfeTradeOffLowerRateClosingCostDiff { get; set; }
        [DataMember]
        public string sGfeTotalEstimateSettlementCharge { get; set; }
        [DataMember]
        public string sGfeTradeOffLowerCCClosingCost { get; set; }
        [DataMember]
        public string sGfeTradeOffLowerRateClosingCost { get; set; }

        [DataMember]
        public string sGfeShoppingCartLoan1OriginatorName { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan2OriginatorName { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan3OriginatorName { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan1LoanAmt { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan2LoanAmt { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan3LoanAmt { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan1LoanTerm { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan2LoanTerm { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan3LoanTerm { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan1NoteIR { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan2NoteIR { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan3NoteIR { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan1InitialPmt { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan2InitialPmt { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan3InitialPmt { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan1RateLockPeriod { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan2RateLockPeriod { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan3RateLockPeriod { get; set; }
        [DataMember]
        public bool sGfeShoppingCartLoan1CanRateIncreaseTri { get; set; }
        [DataMember]
        public bool sGfeShoppingCartLoan2CanRateIncreaseTri { get; set; }
        [DataMember]
        public bool sGfeShoppingCartLoan3CanRateIncreaseTri { get; set; }
        [DataMember]
        public bool sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri { get; set; }
        [DataMember]
        public bool sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri { get; set; }
        [DataMember]
        public bool sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri { get; set; }
        [DataMember]
        public bool sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri { get; set; }
        [DataMember]
        public bool sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri { get; set; }
        [DataMember]
        public bool sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri { get; set; }
        [DataMember]
        public bool sGfeShoppingCartLoan1HavePpmtPenaltyTri { get; set; }
        [DataMember]
        public bool sGfeShoppingCartLoan2HavePpmtPenaltyTri { get; set; }
        [DataMember]
        public bool sGfeShoppingCartLoan3HavePpmtPenaltyTri { get; set; }
        [DataMember]
        public bool sGfeShoppingCartLoan1IsBalloonTri { get; set; }
        [DataMember]
        public bool sGfeShoppingCartLoan2IsBalloonTri { get; set; }
        [DataMember]
        public bool sGfeShoppingCartLoan3IsBalloonTri { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan1TotalClosingCost { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan2TotalClosingCost { get; set; }
        [DataMember]
        public string sGfeShoppingCartLoan3TotalClosingCost { get; set; }

        [DataMember]
        public E_sClosingCostAutomationUpdateT sClosingCostAutomationUpdateT { get; set; }
        [DataMember]
        public string sLastDisclosedGFEArchiveD { get; set; }
        [DataMember]
        public string sLastDisclosedGFEArchiveId { get; set; }

        [DataMember]
        public bool IsArchive { get; set; }

        [DataMember]
        public bool IsReadonly { get; set; }

        [DataMember]
        public string sGfeTotalFundByLender { get; set; }
        [DataMember]
        public string sGfeTotalFundBySeller { get; set; }
        [DataMember]
        public string sPurchPrice { get; set; }
        [DataMember]
        public bool sIsRenovationLoan { get; set; }
        [DataMember]
        public bool sAltCostLckd { get; set; }
        [DataMember]
        public string sAltCost { get; set; }
        [DataMember]
        public string sLandCost { get; set; }
        [DataMember]
        public bool sRefPdOffAmt1003Lckd { get; set; }
        [DataMember]
        public string sRefPdOffAmt1003 { get; set; }
        [DataMember]
        public bool sTotEstPp1003Lckd { get; set; }
        [DataMember]
        public string sTotEstPp1003 { get; set; }
        [DataMember]
        public bool sTotEstCc1003Lckd { get; set; }
        [DataMember]
        public string sTotEstCcNoDiscnt1003 { get; set; }
        [DataMember]
        public bool sFfUfmip1003Lckd { get; set; }
        [DataMember]
        public string sFfUfmip1003 { get; set; }
        [DataMember]
        public bool sLDiscnt1003Lckd { get; set; }
        [DataMember]
        public string sLDiscnt1003 { get; set; }
        [DataMember]
        public string sTotTransC { get; set; }
        [DataMember]
        public string sOCredit1Desc { get; set; }
        [DataMember]
        public string sOCredit2Desc { get; set; }
        [DataMember]
        public string sOCredit3Desc { get; set; }
        [DataMember]
        public string sOCredit4Desc { get; set; }
        [DataMember]
        public string sONewFinBal { get; set; }
        [DataMember]
        public bool sTotCcPbsLocked { get; set; }
        [DataMember]
        public string sTotCcPbs { get; set; }
        [DataMember]
        public bool sOCredit1Lckd { get; set; }
        [DataMember]
        public string sOCredit1Amt { get; set; }
        [DataMember]
        public string sOCredit2Amt { get; set; }
        [DataMember]
        public string sOCredit3Amt { get; set; }
        [DataMember]
        public string sOCredit4Amt { get; set; }
        [DataMember]
        public string sOCredit5Amt { get; set; }
        [DataMember]
        public string sONewFinCc { get; set; }
        [DataMember]
        public string sFfUfmipFinanced { get; set; }
        [DataMember]
        public bool sTransNetCashLckd { get; set; }
        [DataMember]
        public string sTransNetCash { get; set; }

        [DataMember]
        public bool sIPiaDyLckd { get; set; }
        [DataMember]
        public string sIPiaDy { get; set; }
        [DataMember]
        public bool sIPerDayLckd { get; set; }
        [DataMember]
        public string sIPerDay { get; set; }

        [DataMember]
        public E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT { get; set; }
        [DataMember]
        public E_sOriginatorCompensationPaymentSourceT oldOriginatorCompensationPaymentSourceT { get; set; }
        [DataMember]
        public bool DisableTPAffIfHasContact { get; set; }
        [DataMember]
        public bool sGfeIsTPOTransaction { get; set; }
        [DataMember]
        public bool sGfeIsTPOTransactionIsCalculated { get; set; }
        [DataMember]
        public bool CanReadDFLP { get; set; }
        [DataMember]
        public bool CanSetDFLP { get; set; }

        [DataMember]
        public E_sLenderCreditCalculationMethodT sLenderCreditCalculationMethodT { get; set; }
        [DataMember]
        public string sBrokerLockOriginatorPriceBrokComp1PcPrice { get; set; }
        [DataMember]
        public string sBrokerLockOriginatorPriceBrokComp1PcAmt { get; set; }
        [DataMember]
        public E_PercentBaseT sLDiscntBaseT { get; set; }
        [DataMember]
        public string sLDiscntPc { get; set; }
        [DataMember]
        public string sLDiscntFMb { get; set; }
        [DataMember]
        public E_CreditLenderPaidItemT sGfeCreditLenderPaidItemT { get; set; }
        [DataMember]
        public string sGfeCreditLenderPaidItemF { get; set; }
        [DataMember]
        public string sGfeLenderCreditF { get; set; }
        [DataMember]
        public string sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt { get; set; }
        [DataMember]
        public string sLenderCustomCredit1Description { get; set; }
        [DataMember]
        public string sLenderCustomCredit1Amount { get; set; }
        [DataMember]
        public string sLenderCustomCredit1AmountAsCharge { get; set; }
        [DataMember]
        public string sLenderCustomCredit2Description { get; set; }
        [DataMember]
        public string sLenderCustomCredit2Amount { get; set; }
        [DataMember]
        public string sLenderCustomCredit2AmountAsCharge { get; set; }
        [DataMember]
        public string sLenderActualTotalCreditAmt_Neg { get; set; }
        [DataMember]
        public string sLenderCreditAvailableAmt_Neg { get; set; }
        [DataMember]
        public E_LenderCreditDiscloseLocationT sLenderPaidFeeDiscloseLocationT { get; set; }
        [DataMember]
        public string sLenderPaidFeesAmt_Neg { get; set; }
        [DataMember]
        public E_LenderCreditDiscloseLocationT sLenderGeneralCreditDiscloseLocationT { get; set; }
        [DataMember]
        public string sLenderGeneralCreditAmt_Neg { get; set; }
        [DataMember]
        public E_LenderCreditDiscloseLocationT sLenderCustomCredit1DiscloseLocationT { get; set; }
        [DataMember]
        public E_LenderCreditDiscloseLocationT sLenderCustomCredit2DiscloseLocationT { get; set; }
        [DataMember]
        public BorrowerClosingCostFee LastSavedBorrowerOrigCompFee { get; set; }

        [DataMember]
        public bool IsConstruction { get; set; }

        [DataMember]
        public bool AreConstructionLoanDataPointsMigrated { get; set; }

        [DataMember]
        public string sPurchasePrice1003 { get; set; }

        [DataMember]
        public string sLandIfAcquiredSeparately1003 { get; set; }
    }

    public partial class RActiveGFE : BaseLoanPage
    {
        protected override bool DisableViewState
        {
            get { return true; }
        }

        protected bool IsArchivePage
        {
            get
            {
                return Broker.IsGFEandCoCVersioningEnabled
                    && (RequestHelper.GetSafeQueryString("IsArchivePage") == "true" || RequestHelper.GetSafeQueryString("IsClosingCostMigrationArchivePage") == "true")
                    && !IsLeadPage;
            }
        }
     
        protected bool ShowGFEArchiveRecorder
        {
            get
            {
                return Broker.IsGFEandCoCVersioningEnabled && !IsArchivePage && !IsLeadPage;
            }
        }

        protected bool ShowRequireFeesFromDropDownCheckbox
        {
            get
            {
                return BrokerUser.HasPermission(Permission.AllowEnablingCustomFeeDescriptions);
            }
        }

        private bool? x_ShowIsManuallySetThirdPartyAffiliatePropsCheckbox = null;
        protected bool ShowIsManuallySetThirdPartyAffiliatePropsCheckbox
        {
            get
            {
                if (!x_ShowIsManuallySetThirdPartyAffiliatePropsCheckbox.HasValue)
                {
                    AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
                    IEnumerable<string> fieldDependencyList = LendersOffice.ConfigSystem.LendingQBExecutingEngine.GetDependencyFieldsByOperation(principal.BrokerId, WorkflowOperations.ManuallyOverrideQMSettings);

                    LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(principal.ConnectionInfo, fieldDependencyList, LoanID);
                    valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));

                    x_ShowIsManuallySetThirdPartyAffiliatePropsCheckbox = LendersOffice.ConfigSystem.LendingQBExecutingEngine.CanPerform(WorkflowOperations.ManuallyOverrideQMSettings, valueEvaluator);
                }
                return x_ShowIsManuallySetThirdPartyAffiliatePropsCheckbox.Value;
            }
        }

        protected bool IsClosingCostMigrationArchivePage
        {
            get
            {
                return Broker.IsGFEandCoCVersioningEnabled
                    && RequestHelper.GetSafeQueryString("IsClosingCostMigrationArchivePage") == "true"
                    && !IsLeadPage;
            }
        }

        public bool hideHeaderAndBoarder = true;
        protected bool m_isPurchase = false;
        protected bool isRenovationLoan = false;

        public override bool IsReadOnly
        {
            get
            {
                return IsArchivePage || base.IsReadOnly;
            }
        }

        protected string ClosingCostPageUrl
        {
            get
            {
                BrokerDB db = BrokerDB.RetrieveById(BrokerUser.BrokerId);
                if (db.AreAutomatedClosingCostPagesVisible)
                {
                    return string.Format("/los/Template/ClosingCost/ClosingCostTemplatePicker.aspx?sLId={0}", LoanID.ToString("N"));
                }
                else
                {
                    return string.Format("/los/view/closingcostlist.aspx?loanid={0}", LoanID.ToString("N"));
                }
            }
        }

        private IEnumerable<KeyValuePair<string, string>> GetGFESectionValues()
        {
            List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();
           // items.Add(new KeyValuePair<string, string>(E_GfeSectionT.LeaveBlank.ToString("d"), ""));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B1.ToString("d"), "A1"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B2.ToString("d"), "A2"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B3.ToString("d"), "A3"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B4.ToString("d"), "B4"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B5.ToString("d"), "B5"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B6.ToString("d"), "B6"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B7.ToString("d"), "B7"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B8.ToString("d"), "B8"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B9.ToString("d"), "B9"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B10.ToString("d"), "B10"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.B11.ToString("d"), "B11"));
            items.Add(new KeyValuePair<string, string>(E_GfeSectionT.NotApplicable.ToString("d"), "N/A"));
            return items;
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        private void BindGfeSectionDropDown(Repeater repeater)
        {
            List<ListItem> items = new List<ListItem>();
            items.Add(Tools.CreateEnumListItem("A1", E_GfeSectionT.B1));
            items.Add(Tools.CreateEnumListItem("A2", E_GfeSectionT.B2));
            items.Add(Tools.CreateEnumListItem("B3", E_GfeSectionT.B3));
            items.Add(Tools.CreateEnumListItem("B4", E_GfeSectionT.B4));
            items.Add(Tools.CreateEnumListItem("B5", E_GfeSectionT.B5));
            items.Add(Tools.CreateEnumListItem("B6", E_GfeSectionT.B6));
            items.Add(Tools.CreateEnumListItem("B7", E_GfeSectionT.B7));
            items.Add(Tools.CreateEnumListItem("B8", E_GfeSectionT.B8));
            items.Add(Tools.CreateEnumListItem("B9", E_GfeSectionT.B9));
            items.Add(Tools.CreateEnumListItem("B10", E_GfeSectionT.B10));
            items.Add(Tools.CreateEnumListItem("B11", E_GfeSectionT.B11));
            items.Add(Tools.CreateEnumListItem("N/A", E_GfeSectionT.NotApplicable));
            SectionRepeater.DataSource = items;
            SectionRepeater.DataBind();
        }

        private void BindCreditDesc()
        {
            List<ListItem> items = new List<ListItem>();
            items.Add(new ListItem("Cash Deposit on sales contract", "Cash Deposit on sales contract"));
            items.Add(new ListItem("Seller Credit", "Seller Credit"));
            items.Add(new ListItem("Lender Credit", "Lender Credit"));
            items.Add(new ListItem("Relocation Funds", "Relocation Funds"));
            items.Add(new ListItem("Employer Assisted Housing", "Employer Assisted Housing"));
            items.Add(new ListItem("Lease Purchase Fund", "Lease Purchase Fund"));
            items.Add(new ListItem("Borrower Paid Fees", "Borrower Paid Fees"));
            items.Add(new ListItem("Paid Outside of Closing", "Paid Outside of Closing"));
            items.Add(new ListItem("Broker Credit", "Broker Credit"));

            CreditDescRepeater.DataSource = items;
            CreditDescRepeater.DataBind();
        }

        private void BindLoanEstimateSectionDropDown(HtmlSelect ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("A", E_IntegratedDisclosureSectionT.SectionA));
            ddl.Items.Add(Tools.CreateEnumListItem("B", E_IntegratedDisclosureSectionT.SectionB));
            ddl.Items.Add(Tools.CreateEnumListItem("C", E_IntegratedDisclosureSectionT.SectionC));
            ddl.Items.Add(Tools.CreateEnumListItem("E", E_IntegratedDisclosureSectionT.SectionE));
            ddl.Items.Add(Tools.CreateEnumListItem("F", E_IntegratedDisclosureSectionT.SectionF));
            ddl.Items.Add(Tools.CreateEnumListItem("G", E_IntegratedDisclosureSectionT.SectionG));
            ddl.Items.Add(Tools.CreateEnumListItem("H", E_IntegratedDisclosureSectionT.SectionH));
        }

        private void BindDDLGFEArchives(CPageData dataLoan)
        {
            List<ListItem> items = new List<ListItem>();

            foreach (var archive in dataLoan.sClosingCostArchive.Where(p => p.ClosingCostArchiveType == LendersOffice.Common.SerializationTypes.ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015) 
                .OrderBy( p=> DateTime.Parse(p.DateArchived) ))
            {
                ddlGFEArchives.Items.Add(new ListItem(archive.DateArchived, archive.Id.ToString()));
            }
        }

        private void BindBeneficiaryDropDown(Repeater repeater)
        {
            DropDownList dl = new DropDownList();
            RolodexDB.PopulateAgentTypeDropDownList(dl);
            repeater.DataSource = dl.Items;
            repeater.DataBind();
        }

        private bool ByPassBgCalcForGfeAsDefault()
        {
            SqlParameter[] parameters = { new SqlParameter("@UserId", BrokerUser.UserId) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "RetrieveByPassBgCalcForGfeAsDefaultByUserId", parameters))
            {
                if (reader.Read())
                {
                   return (bool)reader["ByPassBgCalcForGfeAsDefault"];
                }
            }
            return false;
        }

        public void Bind_sTimeZoneT()
        {
            List<ListItem> Items = new List<ListItem>();

            Items.Add(Tools.CreateEnumListItem("", E_sTimeZoneT.LeaveBlank));
            Items.Add(Tools.CreateEnumListItem("Pacific Time", E_sTimeZoneT.PacificTime));
            Items.Add(Tools.CreateEnumListItem("Mountain Time", E_sTimeZoneT.MountainTime));
            Items.Add(Tools.CreateEnumListItem("Central Time", E_sTimeZoneT.CentralTime));
            Items.Add(Tools.CreateEnumListItem("Eastern Time", E_sTimeZoneT.EasternTime));

            TimeZoneRepeater1.DataSource = Items;
            TimeZoneRepeater1.DataBind();

            TimeZoneRepeater2.DataSource = Items;
            TimeZoneRepeater2.DataBind();
        }

        public void BindPercentRepeaters()
        {
            List<ListItem> Items = new List<ListItem>();

            Items.Add(Tools.CreateEnumListItem("Loan Amount", E_PercentBaseT.LoanAmount));
            Items.Add(Tools.CreateEnumListItem("Purchase Price", E_PercentBaseT.SalesPrice));
            Items.Add(Tools.CreateEnumListItem("Appraisal Value", E_PercentBaseT.AppraisalValue));
            Items.Add(Tools.CreateEnumListItem("Total Loan Amount", E_PercentBaseT.TotalLoanAmount));

            PercentRepeater.DataSource = Items;
            PercentRepeater.DataBind();

            PercentRepeater2.DataSource = Items;
            PercentRepeater2.DataBind();
        }

        protected override void OnInit(EventArgs e)
        {
            RegisterService("cfpb_utils", "/newlos/Test/RActiveGFEService.aspx");
            base.OnInit(e);

            if (IsArchivePage)
            {
                this.PageID = "RActiveNewGfeArchive";
            }
            else
            {
                this.PageID = "RActiveNewGfe";
                if (IsLeadPage)
                {
                    PDFPrintClass = typeof(CIFW2015);
                }
                else
                {
                    PDFPrintClass = typeof(LendersOffice.Pdf.CGoodFaithEstimate2010PDF);
                }
            }

            if (IsArchivePage)
            {
                var dataLoan = new CPageData(LoanID, new string[] { "sClosingCostArchive", "sLastDisclosedGFEArchiveD", "sLastDisclosedGFEArchiveId" });
                dataLoan.InitLoad();
                BindDDLGFEArchives(dataLoan);

                if (!IsPostBack)
                {
                    Tools.SetDropDownListValue(ddlGFEArchives, dataLoan.sLastDisclosedGFEArchiveId_rep);
                }
            }

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            newGFETitle.Text = IsLeadPage ? "Initial Fees Worksheet" : "Good Faith Estimate";
            bRecordGFEToArchive.Visible = ShowGFEArchiveRecorder && BrokerUser.HasPermission(Permission.AllowManuallyArchivingGFE);
            DisclosureRow.Visible = !IsArchivePage;
            CFM.Type = "19";
            CFM.CompanyNameField = GfeTilCompanyName.ClientID;
            CFM.StreetAddressField = GfeTilStreetAddr.ClientID;
            CFM.CityField = GfeTilCity.ClientID;
            CFM.StateField = "GfeTilState";
            CFM.ZipField = "GfeTilZip";
            CFM.CompanyPhoneField = GfeTilPhoneOfCompany.ClientID;
            CFM.EmailField = GfeTilEmailAddr.ClientID;
            CFM.IsAllowLockableFeature = true;

            //Need to do it this way so I dont have the ID element...
            BindGfeSectionDropDown(SectionRepeater);
            BindBeneficiaryDropDown(BeneficiaryRepeater);
            BindCreditDesc();
            Bind_sTimeZoneT();

            RegisterJsScript("ractive-0.7.1.min.js");
            RegisterJsScript("mask.js");

            RegisterCSS("stylesheetnew.css");

            BindPercentRepeaters();

            Response.AddHeader("X-UA-Compatible", "IE=edge");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("Ractive-Common.js");

            CPageData dataLoan;

            if (IsArchivePage)
            {
                dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(RActiveGFE));
                dataLoan.InitLoad();

                if (dataLoan.sClosingCostArchive.Count != 0)
                {
                    dataLoan.ApplyClosingCostArchive(new Guid(ddlGFEArchives.SelectedValue));
                }
                else
                {
                    throw new CBaseException("Archive not found", "Archive not found");
                }
            }
            else
            {
                dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(RActiveGFE));
                dataLoan.InitLoad();
            }

            DropDownList ddl = new DropDownList();
            Tools.Bind_sGfeCreditLenderPaidItemT(ddl);
            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                ddl.Items.Remove(ddl.Items.FindByValue(E_CreditLenderPaidItemT.OriginatorCompensationOnly.ToString("D")));
            }
            sGfeCreditLenderPaidItemTRep.DataSource = ddl.Items;
            sGfeCreditLenderPaidItemTRep.DataBind();

            ddl = new DropDownList();
            Tools.Bind_PercentBaseLoanAmountsT(ddl);
            sLDiscntBaseTRep.DataSource = ddl.Items;
            sLDiscntBaseTRep.DataBind();
            sLDiscntBaseTRep_calc.DataSource = ddl.Items;
            sLDiscntBaseTRep_calc.DataBind();

            ddl = new DropDownList();
            Tools.Bind_sLenderCreditCalculationMethodT(ddl);
            sLenderCreditCalculationMethodTRep.DataSource = ddl.Items;
            sLenderCreditCalculationMethodTRep.DataBind();

            ddl = new DropDownList();
            Tools.Bind_LenderCreditDiscloseLocationT(ddl);
            sLenderPaidFeeDiscloseLocationTRep.DataSource = ddl.Items;
            sLenderPaidFeeDiscloseLocationTRep.DataBind();
            sLenderGeneralCreditDiscloseLocationTRep.DataSource = ddl.Items;
            sLenderGeneralCreditDiscloseLocationTRep.DataBind();
            sLenderCustomCredit1DiscloseLocationTRep.DataSource = ddl.Items;
            sLenderCustomCredit1DiscloseLocationTRep.DataBind();
            sLenderCustomCredit2DiscloseLocationTRep.DataSource = ddl.Items;
            sLenderCustomCredit2DiscloseLocationTRep.DataBind();

            m_isPurchase = dataLoan.sLPurposeT != E_sLPurposeT.Purchase && dataLoan.sLPurposeT != E_sLPurposeT.Construct && dataLoan.sLPurposeT != E_sLPurposeT.ConstructPerm;
            this.isRenovationLoan = dataLoan.sIsRenovationLoan;

            SampleClosingCostFeeViewModel viewModel = new SampleClosingCostFeeViewModel();

            viewModel.sGfeIsTPOTransaction = dataLoan.sGfeIsTPOTransaction;
            viewModel.sGfeIsTPOTransactionIsCalculated = dataLoan.sGfeIsTPOTransactionIsCalculated && dataLoan.sBranchChannelT != E_BranchChannelT.Correspondent && dataLoan.sBranchChannelT != E_BranchChannelT.Blank;

            viewModel.DisableTPAffIfHasContact = dataLoan.sBranchChannelT == E_BranchChannelT.Retail || dataLoan.sBranchChannelT == E_BranchChannelT.Wholesale || dataLoan.sBranchChannelT == E_BranchChannelT.Broker;

            viewModel.sClosingCostFeeVersionT = dataLoan.sClosingCostFeeVersionT;
            viewModel.sIPiaDyLckd = dataLoan.sIPiaDyLckd;
            viewModel.sIPiaDy = dataLoan.sIPiaDy_rep;
            viewModel.sIPerDayLckd = dataLoan.sIPerDayLckd;
            viewModel.sIPerDay = dataLoan.sIPerDay_rep;

            viewModel.IsArchive = IsArchivePage;
            viewModel.IsReadonly = IsReadOnly;

            viewModel.sLastDisclosedGFEArchiveD = dataLoan.sLastDisclosedGFEArchiveD_rep;
            viewModel.sLastDisclosedGFEArchiveId = dataLoan.sLastDisclosedGFEArchiveId_rep;

            viewModel.SectionList = dataLoan.sClosingCostSet.GetViewForSerialization(E_ClosingCostViewT.LoanHud1);
            viewModel.sLAmtCalc = dataLoan.sLAmtCalc_rep;
            viewModel.sLAmtLckd = dataLoan.sLAmtLckd;
            viewModel.sIsRequireFeesFromDropDown = dataLoan.sIsRequireFeesFromDropDown;
            viewModel.sIsManuallySetThirdPartyAffiliateProps = dataLoan.sIsManuallySetThirdPartyAffiliateProps;
            viewModel.sOriginatorCompensationPaymentSourceT = dataLoan.sOriginatorCompensationPaymentSourceT;
            viewModel.oldOriginatorCompensationPaymentSourceT = dataLoan.sOriginatorCompensationPaymentSourceT;

            if (dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid)
            {
                // If we're starting with a borrower-paid orig comp fee, we want to keep track of it so we can restore it later on if the user switches back to borrower paid.
                viewModel.LastSavedBorrowerOrigCompFee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId);
            }

            viewModel.ByPassBgCalcForGfeAsDefault = ByPassBgCalcForGfeAsDefault();

            viewModel.sGfeInitialDisclosureD = dataLoan.sGfeInitialDisclosureD_rep;
            viewModel.sGfeRedisclosureD = dataLoan.sGfeRedisclosureD_rep;
            viewModel.IsProtectDisclosureDates = Broker.IsProtectDisclosureDates;

            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew);

            viewModel.GfeTilCompanyName = gfeTil.CompanyName;
            viewModel.GfeTilStreetAddr = gfeTil.StreetAddr;
            viewModel.GfeTilCity = gfeTil.City;
            viewModel.GfeTilState = gfeTil.State;
            viewModel.GfeTilZip = gfeTil.Zip;
            viewModel.GfeTilPhoneOfCompany = gfeTil.PhoneOfCompany;
            viewModel.GfeTilEmailAddr = gfeTil.EmailAddr;

            viewModel.sLpTemplateNm = dataLoan.sLpTemplateNm;
            viewModel.sCcTemplateNm = dataLoan.sCcTemplateNm;

            Tools.Bind_sFinMethT(sFinMethT);

            viewModel.sFinalLAmt = dataLoan.sFinalLAmt_rep;
            viewModel.sNoteIR = dataLoan.sNoteIR_rep;
            viewModel.sTerm = dataLoan.sTerm_rep;
            viewModel.sDue = dataLoan.sDue_rep;
            viewModel.sFinMethT = (int)dataLoan.sFinMethT;
            viewModel.sSchedDueD1Lckd = dataLoan.sSchedDueD1Lckd;
            viewModel.sSchedDueD1 = dataLoan.sSchedDueD1_rep;
            viewModel.sEstCloseDLckd = dataLoan.sEstCloseDLckd;
            viewModel.sEstCloseD = dataLoan.sEstCloseD_rep;
            viewModel.sConsummationDLckd = dataLoan.sConsummationDLckd;
            viewModel.sConsummationD = dataLoan.sConsummationD_rep;
            viewModel.sDaysInYr = dataLoan.sDaysInYr_rep;
            viewModel.sRAdj1stCapR = dataLoan.sRAdj1stCapR_rep;
            viewModel.sRAdjCapR = dataLoan.sRAdjCapR_rep;
            viewModel.sRAdj1stCapMon = dataLoan.sRAdj1stCapMon_rep;
            viewModel.sRAdjCapMon = dataLoan.sRAdjCapMon_rep;
            viewModel.sRAdjLifeCapR = dataLoan.sRAdjLifeCapR_rep;
            viewModel.sPmtAdjCapR = dataLoan.sPmtAdjCapR_rep;
            viewModel.sPmtAdjRecastPeriodMon = dataLoan.sPmtAdjRecastPeriodMon_rep;
            viewModel.sPmtAdjCapMon = dataLoan.sPmtAdjCapMon_rep;
            viewModel.sPmtAdjRecastStop = dataLoan.sPmtAdjRecastStop_rep;
            viewModel.sPmtAdjMaxBalPc = dataLoan.sPmtAdjMaxBalPc_rep;
            viewModel.sIOnlyMon = dataLoan.sIOnlyMon_rep;


            viewModel.sGfeNoteIRAvailTillDLckd = dataLoan.sGfeNoteIRAvailTillDLckd;
            viewModel.sGfeNoteIRAvailTillD = dataLoan.sGfeNoteIRAvailTillD_Date;
            viewModel.sIsPrintTimeForGfeNoteIRAvailTillD = dataLoan.sIsPrintTimeForGfeNoteIRAvailTillD;

            viewModel.sGfeEstScAvailTillDLckd = dataLoan.sGfeEstScAvailTillDLckd;
            viewModel.sGfeEstScAvailTillD = dataLoan.sGfeEstScAvailTillD_Date;

            viewModel.sGfeNoteIRAvailTillDTimeZoneT = (int)dataLoan.sGfeNoteIRAvailTillDTimeZoneT;
            viewModel.sGfeEstScAvailTillDTimeZoneT = (int)dataLoan.sGfeEstScAvailTillDTimeZoneT;

            string[] timeVal;
            if (dataLoan.sGfeNoteIRAvailTillD_Time.Length != 0)
            {
                timeVal = dataLoan.sGfeNoteIRAvailTillD_Time.Split(':');
                viewModel.sGfeNoteIRAvailTillD_Time_am = timeVal[1].Substring(3);
                viewModel.sGfeNoteIRAvailTillD_Time_hour = timeVal[0];
                viewModel.sGfeNoteIRAvailTillD_Time_minute = timeVal[1].Substring(0, 2);
            }

            if (dataLoan.sGfeEstScAvailTillD_Time.Length != 0)
            {
                timeVal = dataLoan.sGfeEstScAvailTillD_Time.Split(':');
                viewModel.sGfeEstScAvailTillD_Time_am = timeVal[1].Substring(3);
                viewModel.sGfeEstScAvailTillD_Time_hour = timeVal[0];
                viewModel.sGfeEstScAvailTillD_Time_minute = timeVal[1].Substring(0, 2);
            }

            viewModel.sIsPrintTimeForsGfeEstScAvailTillD = dataLoan.sIsPrintTimeForsGfeEstScAvailTillD;

            viewModel.sGfeRateLockPeriodLckd = dataLoan.sGfeRateLockPeriodLckd;
            viewModel.sGfeRateLockPeriod = dataLoan.sGfeRateLockPeriod_rep;
            viewModel.sGfeLockPeriodBeforeSettlement = dataLoan.sGfeLockPeriodBeforeSettlement_rep;
            viewModel.sGfeCanRateIncrease = dataLoan.sGfeCanRateIncrease;
            viewModel.sRLifeCapR = dataLoan.sRLifeCapR_rep;
            viewModel.sGfeFirstInterestChangeIn = dataLoan.sGfeFirstInterestChangeIn;
            viewModel.sGfeCanLoanBalanceIncrease = dataLoan.sGfeCanLoanBalanceIncrease;
            viewModel.sGfeMaxLoanBalance = dataLoan.sGfeMaxLoanBalance_rep;
            viewModel.sGfeFirstPaymentChangeIn = dataLoan.sGfeFirstPaymentChangeIn;
            viewModel.sGfeFirstAdjProThisMPmtAndMIns = dataLoan.sGfeFirstAdjProThisMPmtAndMIns_rep;
            viewModel.sGfeCanPaymentIncrease = dataLoan.sGfeCanPaymentIncrease;
            viewModel.sGfeMaxProThisMPmtAndMIns = dataLoan.sGfeMaxProThisMPmtAndMIns_rep;
            viewModel.sGfeHavePpmtPenalty = dataLoan.sGfeHavePpmtPenalty;
            viewModel.sGfeMaxPpmtPenaltyAmt = dataLoan.sGfeMaxPpmtPenaltyAmt_rep;
            viewModel.sGfeIsBalloon = dataLoan.sGfeIsBalloon;
            viewModel.sGfeBalloonPmt = dataLoan.sGfeBalloonPmt_rep;
            viewModel.sGfeBalloonDueInYrs = dataLoan.sGfeBalloonDueInYrs;
            viewModel.sMldsHasImpound = dataLoan.sMldsHasImpound;

            viewModel.sGfeOriginationF = dataLoan.sGfeOriginationF_rep;
            viewModel.sLDiscnt = dataLoan.sLDiscnt_rep;
            viewModel.sGfeRequiredServicesTotalFee = dataLoan.sGfeRequiredServicesTotalFee_rep;
            viewModel.sGfeServicesYouShopTotalFee = dataLoan.sGfeServicesYouShopTotalFee_rep;
            viewModel.sGfeInitialImpoundDeposit = dataLoan.sGfeInitialImpoundDeposit_rep;
            viewModel.sGfeTotalOtherSettlementServiceFee = dataLoan.sGfeTotalOtherSettlementServiceFee_rep;
            viewModel.sGfeLenderTitleTotalFee = dataLoan.sGfeLenderTitleTotalFee_rep;
            viewModel.sGfeGovtRecTotalFee = dataLoan.sGfeGovtRecTotalFee_rep;
            viewModel.sGfeDailyInterestTotalFee = dataLoan.sGfeDailyInterestTotalFee_rep;
            viewModel.sGfeAdjOriginationCharge = dataLoan.sGfeAdjOriginationCharge_rep;
            viewModel.sGfeOwnerTitleTotalFee = dataLoan.sGfeOwnerTitleTotalFee_rep;
            viewModel.sGfeTransferTaxTotalFee = dataLoan.sGfeTransferTaxTotalFee_rep;
            viewModel.sGfeHomeOwnerInsuranceTotalFee = dataLoan.sGfeHomeOwnerInsuranceTotalFee_rep;
            viewModel.sGfeNotApplicableF = dataLoan.sGfeNotApplicableF_rep;

            viewModel.sGfeTradeOffLowerCCLoanAmt = dataLoan.sGfeTradeOffLowerCCLoanAmt_rep;
            viewModel.sGfeTradeOffLowerRateLoanAmt = dataLoan.sGfeTradeOffLowerRateLoanAmt_rep;
            viewModel.sGfeTradeOffLowerCCNoteIR = dataLoan.sGfeTradeOffLowerCCNoteIR_rep;
            viewModel.sGfeTradeOffLowerRateNoteIR = dataLoan.sGfeTradeOffLowerRateNoteIR_rep;
            viewModel.sGfeProThisMPmtAndMIns = dataLoan.sGfeProThisMPmtAndMIns_rep;
            viewModel.sGfeTradeOffLowerCCMPmtAndMIns = dataLoan.sGfeTradeOffLowerCCMPmtAndMIns_rep;
            viewModel.sGfeTradeOffLowerRateMPmtAndMIns = dataLoan.sGfeTradeOffLowerRateMPmtAndMIns_rep;
            viewModel.sGfeTradeOffLowerCCMPmtAndMInsDiff = dataLoan.sGfeTradeOffLowerCCMPmtAndMInsDiff_rep;
            viewModel.sGfeTradeOffLowerRateMPmtAndMInsDiff = dataLoan.sGfeTradeOffLowerRateMPmtAndMInsDiff_rep;
            viewModel.sGfeTradeOffLowerCCClosingCostDiff = dataLoan.sGfeTradeOffLowerCCClosingCostDiff_rep;
            viewModel.sGfeTradeOffLowerRateClosingCostDiff = dataLoan.sGfeTradeOffLowerRateClosingCostDiff_rep;
            viewModel.sGfeTotalEstimateSettlementCharge = dataLoan.sGfeTotalEstimateSettlementCharge_rep;
            viewModel.sGfeTradeOffLowerCCClosingCost = dataLoan.sGfeTradeOffLowerCCClosingCost_rep;
            viewModel.sGfeTradeOffLowerRateClosingCost = dataLoan.sGfeTradeOffLowerRateClosingCost_rep;

            viewModel.sGfeShoppingCartLoan1OriginatorName = dataLoan.sGfeShoppingCartLoan1OriginatorName;
            viewModel.sGfeShoppingCartLoan2OriginatorName = dataLoan.sGfeShoppingCartLoan2OriginatorName;
            viewModel.sGfeShoppingCartLoan3OriginatorName = dataLoan.sGfeShoppingCartLoan3OriginatorName;
            viewModel.sGfeShoppingCartLoan1LoanAmt = dataLoan.sGfeShoppingCartLoan1LoanAmt_rep;
            viewModel.sGfeShoppingCartLoan2LoanAmt = dataLoan.sGfeShoppingCartLoan2LoanAmt_rep;
            viewModel.sGfeShoppingCartLoan3LoanAmt = dataLoan.sGfeShoppingCartLoan3LoanAmt_rep;
            viewModel.sGfeShoppingCartLoan1LoanTerm = dataLoan.sGfeShoppingCartLoan1LoanTerm_rep;
            viewModel.sGfeShoppingCartLoan2LoanTerm = dataLoan.sGfeShoppingCartLoan2LoanTerm_rep;
            viewModel.sGfeShoppingCartLoan3LoanTerm = dataLoan.sGfeShoppingCartLoan3LoanTerm_rep;
            viewModel.sGfeShoppingCartLoan1NoteIR = dataLoan.sGfeShoppingCartLoan1NoteIR_rep;
            viewModel.sGfeShoppingCartLoan2NoteIR = dataLoan.sGfeShoppingCartLoan2NoteIR_rep;
            viewModel.sGfeShoppingCartLoan3NoteIR = dataLoan.sGfeShoppingCartLoan3NoteIR_rep;
            viewModel.sGfeShoppingCartLoan1InitialPmt = dataLoan.sGfeShoppingCartLoan1InitialPmt_rep;
            viewModel.sGfeShoppingCartLoan2InitialPmt = dataLoan.sGfeShoppingCartLoan2InitialPmt_rep;
            viewModel.sGfeShoppingCartLoan3InitialPmt = dataLoan.sGfeShoppingCartLoan3InitialPmt_rep;
            viewModel.sGfeShoppingCartLoan1RateLockPeriod = dataLoan.sGfeShoppingCartLoan1RateLockPeriod_rep;
            viewModel.sGfeShoppingCartLoan2RateLockPeriod = dataLoan.sGfeShoppingCartLoan2RateLockPeriod_rep;
            viewModel.sGfeShoppingCartLoan3RateLockPeriod = dataLoan.sGfeShoppingCartLoan3RateLockPeriod_rep;
            viewModel.sGfeShoppingCartLoan1CanRateIncreaseTri = dataLoan.sGfeShoppingCartLoan1CanRateIncreaseTri == E_TriState.Yes;
            viewModel.sGfeShoppingCartLoan2CanRateIncreaseTri = dataLoan.sGfeShoppingCartLoan2CanRateIncreaseTri == E_TriState.Yes;
            viewModel.sGfeShoppingCartLoan3CanRateIncreaseTri = dataLoan.sGfeShoppingCartLoan3CanRateIncreaseTri == E_TriState.Yes;
            viewModel.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri = dataLoan.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri == E_TriState.Yes;
            viewModel.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri = dataLoan.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri == E_TriState.Yes;
            viewModel.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri = dataLoan.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri == E_TriState.Yes;
            viewModel.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri = dataLoan.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri == E_TriState.Yes;
            viewModel.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri = dataLoan.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri == E_TriState.Yes;
            viewModel.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri = dataLoan.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri == E_TriState.Yes;
            viewModel.sGfeShoppingCartLoan1HavePpmtPenaltyTri = dataLoan.sGfeShoppingCartLoan1HavePpmtPenaltyTri == E_TriState.Yes;
            viewModel.sGfeShoppingCartLoan2HavePpmtPenaltyTri = dataLoan.sGfeShoppingCartLoan2HavePpmtPenaltyTri == E_TriState.Yes;
            viewModel.sGfeShoppingCartLoan3HavePpmtPenaltyTri = dataLoan.sGfeShoppingCartLoan3HavePpmtPenaltyTri == E_TriState.Yes;
            viewModel.sGfeShoppingCartLoan1IsBalloonTri = dataLoan.sGfeShoppingCartLoan1IsBalloonTri == E_TriState.Yes;
            viewModel.sGfeShoppingCartLoan2IsBalloonTri = dataLoan.sGfeShoppingCartLoan2IsBalloonTri == E_TriState.Yes;
            viewModel.sGfeShoppingCartLoan3IsBalloonTri = dataLoan.sGfeShoppingCartLoan3IsBalloonTri == E_TriState.Yes;
            viewModel.sGfeShoppingCartLoan1TotalClosingCost = dataLoan.sGfeShoppingCartLoan1TotalClosingCost_rep;
            viewModel.sGfeShoppingCartLoan2TotalClosingCost = dataLoan.sGfeShoppingCartLoan2TotalClosingCost_rep;
            viewModel.sGfeShoppingCartLoan3TotalClosingCost = dataLoan.sGfeShoppingCartLoan3TotalClosingCost_rep;

            viewModel.IsConstruction = dataLoan.sLPurposeT == E_sLPurposeT.Construct || dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm;
            viewModel.AreConstructionLoanDataPointsMigrated = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges);

            if (!IsArchivePage)
            {
                viewModel.sGfeTotalFundByLender = dataLoan.sGfeTotalFundByLender_rep;
                viewModel.sGfeTotalFundBySeller = dataLoan.sGfeTotalFundBySeller_rep;
                viewModel.sPurchPrice = dataLoan.sPurchPrice_rep;
                viewModel.sPurchasePrice1003 = dataLoan.sPurchasePrice1003_rep;
                viewModel.sIsRenovationLoan = dataLoan.sIsRenovationLoan;
                viewModel.sAltCostLckd = dataLoan.sAltCostLckd;
                viewModel.sAltCost = dataLoan.sAltCost_rep;
                viewModel.sLandCost = dataLoan.sLandCost_rep;
                viewModel.sLandIfAcquiredSeparately1003 = dataLoan.sLandIfAcquiredSeparately1003_rep;
                viewModel.sRefPdOffAmt1003Lckd = dataLoan.sRefPdOffAmt1003Lckd;
                viewModel.sRefPdOffAmt1003 = dataLoan.sRefPdOffAmt1003_rep;
                viewModel.sTotEstPp1003Lckd = dataLoan.sTotEstPp1003Lckd;
                viewModel.sTotEstPp1003 = dataLoan.sTotEstPp1003_rep;
                viewModel.sTotEstCc1003Lckd = dataLoan.sTotEstCc1003Lckd;
                viewModel.sTotEstCcNoDiscnt1003 = dataLoan.sTotEstCcNoDiscnt1003_rep;
                viewModel.sFfUfmip1003Lckd = dataLoan.sFfUfmip1003Lckd;
                viewModel.sFfUfmip1003 = dataLoan.sFfUfmip1003_rep;
                viewModel.sLDiscnt1003Lckd = dataLoan.sLDiscnt1003Lckd;
                viewModel.sLDiscnt1003 = dataLoan.sLDiscnt1003_rep;
                viewModel.sTotTransC = dataLoan.sTotTransC_rep;
                viewModel.sOCredit1Desc = dataLoan.sOCredit1Desc;
                viewModel.sOCredit2Desc = dataLoan.sOCredit2Desc;
                viewModel.sOCredit3Desc = dataLoan.sOCredit3Desc;
                viewModel.sOCredit4Desc = dataLoan.sOCredit4Desc;
                viewModel.sONewFinBal = dataLoan.sONewFinBal_rep;
                viewModel.sTotCcPbsLocked = dataLoan.sTotCcPbsLocked;
                viewModel.sTotCcPbs = dataLoan.sTotCcPbs_rep;
                viewModel.sOCredit1Lckd = dataLoan.sOCredit1Lckd;
                viewModel.sOCredit1Amt = dataLoan.sOCredit1Amt_rep;
                viewModel.sOCredit2Amt = dataLoan.sOCredit2Amt_rep;
                viewModel.sOCredit3Amt = dataLoan.sOCredit3Amt_rep;
                viewModel.sOCredit4Amt = dataLoan.sOCredit4Amt_rep;
                viewModel.sOCredit5Amt = dataLoan.sOCredit5Amt_rep;
                viewModel.sONewFinCc = dataLoan.sONewFinCc_rep;
                viewModel.sFfUfmipFinanced = dataLoan.sFfUfmipFinanced_rep;
                viewModel.sTransNetCashLckd = dataLoan.sTransNetCashLckd;
                viewModel.sTransNetCash = dataLoan.sTransNetCash_rep;
            }

            viewModel.sClosingCostAutomationUpdateT = dataLoan.sClosingCostAutomationUpdateT;
            var feeServiceHistory = new DataAccess.FeeService.FeeServiceApplication.ClosingCostApplicationHistory(dataLoan.sFeeServiceApplicationHistoryXmlContent);
            ccTableDiv.Visible = !IsArchivePage && Broker.CalculateclosingCostInPML && dataLoan.sIsLoanProgramRegistered && !feeServiceHistory.IsBlank;

            viewModel.IsRemovePreparedDates = Broker.IsRemovePreparedDates;
            if (false == Broker.IsRemovePreparedDates)
            {
                viewModel.GfeTilPrepareDate = gfeTil.PrepareDate_rep;
                viewModel.GfeTilPrepareDateToolTip = "Hint:  Enter 't' for today's date.  Enter 'o' for opened date.";
            }
            else
            {
                trPreparedDate.Visible = false;
            }
            CFM.IsLocked = gfeTil.IsLocked;
            CFM.AgentRoleT = gfeTil.AgentRoleT;

            phGFEArchiveRow.Visible = ShowGFEArchiveRecorder;
            phSelectGFERow.Visible = IsArchivePage;
            if (ShowGFEArchiveRecorder)
            {
                var dataLoanDDLData = new CPageData(LoanID, new string[] { });
                if (dataLoan.sClosingCostArchive.Count == 0)
                {
                    sLastDisclosedGFEArchiveId.Items.Add(new ListItem("<-- NONE -->", Guid.Empty.ToString()) { Selected = true });
                }
                else
                {
                    foreach (var revision in dataLoan.sClosingCostArchive.Where(p => p.ClosingCostArchiveType == LendersOffice.Common.SerializationTypes.ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015))
                    {
                        sLastDisclosedGFEArchiveId.Items.Add(new ListItem(revision.DateArchived, revision.Id.ToString()));
                    }
                }

            }
            if (IsArchivePage && !IsClosingCostMigrationArchivePage)
            {
                var dataLoanDDLData = new CPageData(LoanID, new string[] { "sLastDisclosedGFEArchiveD, sLastDisclosedGFEArchiveId" });
                if (!IsPostBack)
                {
                    dataLoanDDLData.InitLoad();
                }
            }
          

            //Last Disclosed date
            if (Broker.IsGFEandCoCVersioningEnabled && !IsArchivePage)
            {
                if (dataLoan.sLastDisclosedGFEArchiveId == Guid.Empty)
                {
                    Tools.SetDropDownListValue(sLastDisclosedGFEArchiveId, Guid.Empty.ToString());
                }
                else
                {
                    Tools.SetDropDownListValue(sLastDisclosedGFEArchiveId, dataLoan.sLastDisclosedGFEArchiveId_rep);
                }
            }

            viewModel.CanReadDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserRead);
            viewModel.CanSetDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserWrite);

            // Lender Credits Tab
            viewModel.sLenderCreditCalculationMethodT = dataLoan.sLenderCreditCalculationMethodT;
            viewModel.sBrokerLockOriginatorPriceBrokComp1PcPrice = dataLoan.sBrokerLockOriginatorPriceBrokComp1PcPrice;
            viewModel.sBrokerLockOriginatorPriceBrokComp1PcAmt = dataLoan.sBrokerLockOriginatorPriceBrokComp1PcAmt_rep;
            viewModel.sLDiscntPc = dataLoan.sLDiscntPc_rep;
            viewModel.sLDiscntBaseT = dataLoan.sLDiscntBaseT;
            viewModel.sLDiscntFMb = dataLoan.sLDiscntFMb_rep;
            viewModel.sGfeCreditLenderPaidItemT = dataLoan.sGfeCreditLenderPaidItemT;
            viewModel.sGfeCreditLenderPaidItemF = dataLoan.sGfeCreditLenderPaidItemF_rep;
            viewModel.sGfeLenderCreditF = dataLoan.sGfeLenderCreditF_rep;
            viewModel.sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt = dataLoan.sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt_rep;
            viewModel.sLenderCustomCredit1Description = dataLoan.sLenderCustomCredit1Description;
            viewModel.sLenderCustomCredit1Amount = dataLoan.sLenderCustomCredit1Amount_rep;
            viewModel.sLenderCustomCredit1AmountAsCharge = dataLoan.sLenderCustomCredit1AmountAsCharge_rep;
            viewModel.sLenderCustomCredit2Description = dataLoan.sLenderCustomCredit2Description;
            viewModel.sLenderCustomCredit2Amount = dataLoan.sLenderCustomCredit2Amount_rep;
            viewModel.sLenderCustomCredit2AmountAsCharge = dataLoan.sLenderCustomCredit2AmountAsCharge_rep;
            viewModel.sLenderActualTotalCreditAmt_Neg = dataLoan.sLenderActualTotalCreditAmt_Neg_rep;
            viewModel.sLenderCreditAvailableAmt_Neg = dataLoan.sLenderCreditAvailableAmt_Neg_rep;
            viewModel.sLenderPaidFeeDiscloseLocationT = dataLoan.sLenderPaidFeeDiscloseLocationT;
            viewModel.sLenderPaidFeesAmt_Neg = dataLoan.sLenderPaidFeesAmt_Neg_rep;
            viewModel.sLenderGeneralCreditDiscloseLocationT = dataLoan.sLenderGeneralCreditDiscloseLocationT;
            viewModel.sLenderGeneralCreditAmt_Neg = dataLoan.sLenderGeneralCreditAmt_Neg_rep;
            viewModel.sLenderCustomCredit1DiscloseLocationT = dataLoan.sLenderCustomCredit1DiscloseLocationT;
            viewModel.sLenderCustomCredit2DiscloseLocationT = dataLoan.sLenderCustomCredit2DiscloseLocationT;

            RegisterJsObject("ClosingCostData", viewModel);

        }

        private static CPageData GetArchiveCPageData(Guid LoanID)
        {
            return new CPageData(LoanID, CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(GoodFaithEstimate2010)).Union(
                CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release));
        }

        [WebMethod]
        public static string RecordGFEToArchive(Guid LoanID)
        {
            try
            {
                AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
                if (principal.HasPermission(Permission.AllowManuallyArchivingGFE))
                {

                    CPageData dataLoan = new CPageData(LoanID, CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(RActiveGFE))
                        .Union(LendersOffice.Common.SerializationTypes.ClosingCostArchive.GetLoanPropertyList(
                        LendersOffice.Common.SerializationTypes.ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015)));


                    dataLoan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);

                    dataLoan.ArchiveClosingCosts(LendersOffice.Common.SerializationTypes.ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015, E_GFEArchivedReasonT.ManuallyArchived);

                    if (dataLoan.sClosingCostArchive.Count() == 1) // update sGfeInitialDisclosureD iff this the first time archiving the GFE.
                    {
                        dataLoan.Set_sGfeInitialDisclosureD(CDateTime.Create(DateTime.Now)
                            , E_GFEArchivedReasonT.InitialDislosureDateSetViaGFE);
                    }

                    try
                    {
                        dataLoan.Save();
                    }
                    catch (PageDataSaveDenied exc)
                    {
                        return "{error: '" + exc.UserMessage + "'}";
                    }

                    return "{\"sLastDisclosedGFEArchiveD\": \"" + dataLoan.sLastDisclosedGFEArchiveD_rep + "\", \"sLastDisclosedGFEArchiveId\": \"" + dataLoan.sLastDisclosedGFEArchiveId_rep + "\"}";
                }
                else
                {
                    var usrMsg = "You do not have permission to manually archive the Good Faith Estimate.";
                    throw new AccessDenied(usrMsg, usrMsg);
                }
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        protected override E_JqueryVersion GetJQueryVersion()
        {
            return E_JqueryVersion._1_7_1;
        }

        private static SampleClosingCostFeeViewModel BindToDataLoan(CPageData dataLoan, string viewModelJson, Guid? idToAddPayment, bool? saveByPassBgCalcForGfeAsDefault)
        {
            SampleClosingCostFeeViewModel viewModel = ObsoleteSerializationHelper.JsonDeserializeAndSanitize<SampleClosingCostFeeViewModel>(viewModelJson);

            if (saveByPassBgCalcForGfeAsDefault == true)
            {
                SqlParameter[] parameters = {
                                            new SqlParameter("@ByPassBgCalcForGfeAsDefault", viewModel.ByPassBgCalcForGfeAsDefault)
                                            , new SqlParameter("@UserId", PrincipalFactory.CurrentPrincipal.UserId)
                                        };
                StoredProcedureHelper.ExecuteNonQuery(PrincipalFactory.CurrentPrincipal.BrokerId, "SetPassBgCalcForGfeAsDefaultByUserId", 3, parameters); 
            }

            dataLoan.sOriginatorCompensationPaymentSourceT = viewModel.sOriginatorCompensationPaymentSourceT;
            dataLoan.sGfeIsTPOTransaction = viewModel.sGfeIsTPOTransaction;

            dataLoan.sClosingCostSet.UpdateWith((IEnumerable<BaseClosingCostFeeSection>)viewModel.SectionList);

            if (!dataLoan.BrokerDB.IsProtectDisclosureDates)
            {
                dataLoan.Set_sGfeInitialDisclosureD_rep(viewModel.sGfeInitialDisclosureD, E_GFEArchivedReasonT.InitialDislosureDateSetViaGFE);
                dataLoan.sGfeRedisclosureD_rep = viewModel.sGfeRedisclosureD;
            }

            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew);

            gfeTil.IsLocked = viewModel.GfeTilIsLocked;
            gfeTil.AgentRoleT = viewModel.GfeTilAgentRoleT;
            gfeTil.CompanyName = viewModel.GfeTilCompanyName;
            gfeTil.StreetAddr = viewModel.GfeTilStreetAddr;
            gfeTil.City = viewModel.GfeTilCity;
            gfeTil.State = viewModel.GfeTilState;
            gfeTil.Zip = viewModel.GfeTilZip;
            gfeTil.PhoneOfCompany = viewModel.GfeTilPhoneOfCompany;
            gfeTil.EmailAddr = viewModel.GfeTilEmailAddr;

            if (!dataLoan.BrokerDB.IsRemovePreparedDates)
            {
                gfeTil.PrepareDate_rep = viewModel.GfeTilPrepareDate;
            }
            gfeTil.Update();

            dataLoan.sLpTemplateNm = viewModel.sLpTemplateNm;
            dataLoan.sCcTemplateNm = viewModel.sCcTemplateNm;

            if (dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled )
            {
                dataLoan.sLastDisclosedGFEArchiveId_rep = viewModel.sLastDisclosedGFEArchiveId;
            }

            dataLoan.sEstCloseDLckd = viewModel.sEstCloseDLckd;
            dataLoan.sSchedDueD1Lckd = viewModel.sSchedDueD1Lckd;
            dataLoan.sConsummationDLckd = viewModel.sConsummationDLckd;
            dataLoan.sGfeNoteIRAvailTillDLckd = viewModel.sGfeNoteIRAvailTillDLckd;
            dataLoan.sGfeEstScAvailTillDLckd = viewModel.sGfeEstScAvailTillDLckd;
            dataLoan.sGfeRateLockPeriodLckd = viewModel.sGfeRateLockPeriodLckd;

            dataLoan.sNoteIR_rep = viewModel.sNoteIR;
            dataLoan.sTerm_rep = viewModel.sTerm;
            dataLoan.sDue_rep = viewModel.sDue;
            dataLoan.sFinMethT = (E_sFinMethT)viewModel.sFinMethT;
            dataLoan.sConsummationD_rep = viewModel.sConsummationD;
            dataLoan.sDaysInYr_rep = viewModel.sDaysInYr;
            dataLoan.sRAdj1stCapR_rep = viewModel.sRAdj1stCapR;
            dataLoan.sRAdjCapR_rep = viewModel.sRAdjCapR;
            dataLoan.sRAdj1stCapMon_rep = viewModel.sRAdj1stCapMon;
            dataLoan.sRAdjCapMon_rep = viewModel.sRAdjCapMon;
            dataLoan.sRAdjLifeCapR_rep = viewModel.sRAdjLifeCapR;
            dataLoan.sPmtAdjCapR_rep = viewModel.sPmtAdjCapR;
            dataLoan.sPmtAdjRecastPeriodMon_rep = viewModel.sPmtAdjRecastPeriodMon;
            dataLoan.sPmtAdjCapMon_rep = viewModel.sPmtAdjCapMon;
            dataLoan.sPmtAdjRecastStop_rep = viewModel.sPmtAdjRecastStop;
            dataLoan.sPmtAdjMaxBalPc_rep = viewModel.sPmtAdjMaxBalPc;
            dataLoan.sIOnlyMon_rep = viewModel.sIOnlyMon;

            DateTime dt = DateTime.MinValue;

            // OPM 224880, 9/9/2015, ML
            // Because setting these fields to a blank or invalid value will
            // cause the inputs on the page to incorrectly update and retain
            // the invalid values when the input is unlocked, we should only
            // set these loan fields if the date entered in the input on the 
            // page is valid.
            if (DateTime.TryParse(viewModel.sSchedDueD1, out dt))
            {
                dataLoan.sSchedDueD1_rep = viewModel.sSchedDueD1;
            }

            if (DateTime.TryParse(viewModel.sEstCloseD, out dt))
            {
                dataLoan.sEstCloseD_rep = viewModel.sEstCloseD;
            }

            dataLoan.sIsPrintTimeForGfeNoteIRAvailTillD = viewModel.sIsPrintTimeForGfeNoteIRAvailTillD;
            dataLoan.sIsPrintTimeForsGfeEstScAvailTillD = viewModel.sIsPrintTimeForsGfeEstScAvailTillD;

            dt = DateTime.MinValue;
            DateTime.TryParse(viewModel.sGfeEstScAvailTillD + " " +  viewModel.sGfeEstScAvailTillD_Time_hour + ":" + viewModel.sGfeEstScAvailTillD_Time_minute + " " + viewModel.sGfeEstScAvailTillD_Time_am, out dt);
            dataLoan.sGfeEstScAvailTillD = dt;

            dt = DateTime.MinValue;
            DateTime.TryParse(viewModel.sGfeNoteIRAvailTillD + " " + viewModel.sGfeNoteIRAvailTillD_Time_hour + ":" + viewModel.sGfeNoteIRAvailTillD_Time_minute + " " + viewModel.sGfeNoteIRAvailTillD_Time_am, out dt);
            dataLoan.sGfeNoteIRAvailTillD = dt;

            dataLoan.sGfeNoteIRAvailTillDTimeZoneT = (E_sTimeZoneT)viewModel.sGfeNoteIRAvailTillDTimeZoneT;
            dataLoan.sGfeEstScAvailTillDTimeZoneT = (E_sTimeZoneT)viewModel.sGfeEstScAvailTillDTimeZoneT;

            dataLoan.sIsPrintTimeForsGfeEstScAvailTillD = viewModel.sIsPrintTimeForsGfeEstScAvailTillD;

            dataLoan.sGfeRateLockPeriod_rep = viewModel.sGfeRateLockPeriod;

            if (!dataLoan.sIsRateLocked)
            {
                dataLoan.sGfeLockPeriodBeforeSettlement_rep = viewModel.sGfeLockPeriodBeforeSettlement;
            }

            dataLoan.sRLifeCapR_rep = viewModel.sRLifeCapR;
            dataLoan.sGfeHavePpmtPenalty = viewModel.sGfeHavePpmtPenalty;
            dataLoan.sGfeMaxPpmtPenaltyAmt_rep = viewModel.sGfeMaxPpmtPenaltyAmt;
            dataLoan.sMldsHasImpound = viewModel.sMldsHasImpound;

            dataLoan.sGfeTradeOffLowerCCLoanAmt_rep = viewModel.sGfeTradeOffLowerCCLoanAmt;
            dataLoan.sGfeTradeOffLowerRateLoanAmt_rep = viewModel.sGfeTradeOffLowerRateLoanAmt;
            dataLoan.sGfeTradeOffLowerCCNoteIR_rep = viewModel.sGfeTradeOffLowerCCNoteIR;
            dataLoan.sGfeTradeOffLowerRateNoteIR_rep = viewModel.sGfeTradeOffLowerRateNoteIR;
            dataLoan.sGfeTradeOffLowerCCClosingCost_rep = viewModel.sGfeTradeOffLowerCCClosingCost;
            dataLoan.sGfeTradeOffLowerRateClosingCost_rep = viewModel.sGfeTradeOffLowerRateClosingCost;

            dataLoan.sGfeShoppingCartLoan1OriginatorName = viewModel.sGfeShoppingCartLoan1OriginatorName;
            dataLoan.sGfeShoppingCartLoan2OriginatorName = viewModel.sGfeShoppingCartLoan2OriginatorName;
            dataLoan.sGfeShoppingCartLoan3OriginatorName = viewModel.sGfeShoppingCartLoan3OriginatorName;

            if (!String.IsNullOrEmpty(dataLoan.sGfeShoppingCartLoan1OriginatorName))
            {
                dataLoan.sGfeShoppingCartLoan1CanRateIncreaseTri = viewModel.sGfeShoppingCartLoan1CanRateIncreaseTri ? E_TriState.Yes : E_TriState.No;
                dataLoan.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri = viewModel.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri ? E_TriState.Yes : E_TriState.No;
                dataLoan.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri = viewModel.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri ? E_TriState.Yes : E_TriState.No;
                dataLoan.sGfeShoppingCartLoan1HavePpmtPenaltyTri = viewModel.sGfeShoppingCartLoan1HavePpmtPenaltyTri ? E_TriState.Yes : E_TriState.No;
                dataLoan.sGfeShoppingCartLoan1IsBalloonTri = viewModel.sGfeShoppingCartLoan1IsBalloonTri ? E_TriState.Yes : E_TriState.No;
            }
            else
            {
                dataLoan.sGfeShoppingCartLoan1CanRateIncreaseTri = E_TriState.Blank;
                dataLoan.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri = E_TriState.Blank;
                dataLoan.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri = E_TriState.Blank;
                dataLoan.sGfeShoppingCartLoan1HavePpmtPenaltyTri = E_TriState.Blank;
                dataLoan.sGfeShoppingCartLoan1IsBalloonTri = E_TriState.Blank;
            }

            dataLoan.sGfeShoppingCartLoan1LoanAmt_rep = viewModel.sGfeShoppingCartLoan1LoanAmt;
            dataLoan.sGfeShoppingCartLoan1LoanTerm_rep = viewModel.sGfeShoppingCartLoan1LoanTerm;
            dataLoan.sGfeShoppingCartLoan1NoteIR_rep = viewModel.sGfeShoppingCartLoan1NoteIR;
            dataLoan.sGfeShoppingCartLoan1InitialPmt_rep = viewModel.sGfeShoppingCartLoan1InitialPmt;
            dataLoan.sGfeShoppingCartLoan1RateLockPeriod_rep = viewModel.sGfeShoppingCartLoan1RateLockPeriod;
            dataLoan.sGfeShoppingCartLoan1TotalClosingCost_rep = viewModel.sGfeShoppingCartLoan1TotalClosingCost;

            if (!String.IsNullOrEmpty(dataLoan.sGfeShoppingCartLoan2OriginatorName))
            {
                dataLoan.sGfeShoppingCartLoan2CanRateIncreaseTri = viewModel.sGfeShoppingCartLoan2CanRateIncreaseTri ? E_TriState.Yes : E_TriState.No;
                dataLoan.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri = viewModel.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri ? E_TriState.Yes : E_TriState.No;
                dataLoan.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri = viewModel.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri ? E_TriState.Yes : E_TriState.No;
                dataLoan.sGfeShoppingCartLoan2HavePpmtPenaltyTri = viewModel.sGfeShoppingCartLoan2HavePpmtPenaltyTri ? E_TriState.Yes : E_TriState.No;
                dataLoan.sGfeShoppingCartLoan2IsBalloonTri = viewModel.sGfeShoppingCartLoan2IsBalloonTri ? E_TriState.Yes : E_TriState.No;
            }
            else
            {
                dataLoan.sGfeShoppingCartLoan2CanRateIncreaseTri = E_TriState.Blank;
                dataLoan.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri = E_TriState.Blank;
                dataLoan.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri = E_TriState.Blank;
                dataLoan.sGfeShoppingCartLoan2HavePpmtPenaltyTri = E_TriState.Blank;
                dataLoan.sGfeShoppingCartLoan2IsBalloonTri = E_TriState.Blank;
            }

            dataLoan.sGfeShoppingCartLoan2LoanAmt_rep = viewModel.sGfeShoppingCartLoan2LoanAmt;
            dataLoan.sGfeShoppingCartLoan2LoanTerm_rep = viewModel.sGfeShoppingCartLoan2LoanTerm;
            dataLoan.sGfeShoppingCartLoan2NoteIR_rep = viewModel.sGfeShoppingCartLoan2NoteIR;
            dataLoan.sGfeShoppingCartLoan2InitialPmt_rep = viewModel.sGfeShoppingCartLoan2InitialPmt;
            dataLoan.sGfeShoppingCartLoan2RateLockPeriod_rep = viewModel.sGfeShoppingCartLoan2RateLockPeriod;
            dataLoan.sGfeShoppingCartLoan2TotalClosingCost_rep = viewModel.sGfeShoppingCartLoan2TotalClosingCost;

            if (!String.IsNullOrEmpty(dataLoan.sGfeShoppingCartLoan3OriginatorName))
            {
                dataLoan.sGfeShoppingCartLoan3CanRateIncreaseTri = viewModel.sGfeShoppingCartLoan3CanRateIncreaseTri ? E_TriState.Yes : E_TriState.No;
                dataLoan.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri = viewModel.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri ? E_TriState.Yes : E_TriState.No;
                dataLoan.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri = viewModel.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri ? E_TriState.Yes : E_TriState.No;
                dataLoan.sGfeShoppingCartLoan3HavePpmtPenaltyTri = viewModel.sGfeShoppingCartLoan3HavePpmtPenaltyTri ? E_TriState.Yes : E_TriState.No;
                dataLoan.sGfeShoppingCartLoan3IsBalloonTri = viewModel.sGfeShoppingCartLoan3IsBalloonTri ? E_TriState.Yes : E_TriState.No;
            }
            else
            {
                dataLoan.sGfeShoppingCartLoan3CanRateIncreaseTri = E_TriState.Blank;
                dataLoan.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri = E_TriState.Blank;
                dataLoan.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri = E_TriState.Blank;
                dataLoan.sGfeShoppingCartLoan3HavePpmtPenaltyTri = E_TriState.Blank;
                dataLoan.sGfeShoppingCartLoan3IsBalloonTri = E_TriState.Blank;
            }
 
            dataLoan.sGfeShoppingCartLoan3LoanAmt_rep = viewModel.sGfeShoppingCartLoan3LoanAmt;
            dataLoan.sGfeShoppingCartLoan3LoanTerm_rep = viewModel.sGfeShoppingCartLoan3LoanTerm;
            dataLoan.sGfeShoppingCartLoan3NoteIR_rep = viewModel.sGfeShoppingCartLoan3NoteIR;
            dataLoan.sGfeShoppingCartLoan3InitialPmt_rep = viewModel.sGfeShoppingCartLoan3InitialPmt;
            dataLoan.sGfeShoppingCartLoan3RateLockPeriod_rep = viewModel.sGfeShoppingCartLoan3RateLockPeriod;
            dataLoan.sGfeShoppingCartLoan3TotalClosingCost_rep = viewModel.sGfeShoppingCartLoan3TotalClosingCost;

            if (!viewModel.IsArchive)
            {
                dataLoan.sRefPdOffAmt1003Lckd = viewModel.sRefPdOffAmt1003Lckd;
                dataLoan.sTotEstPp1003Lckd = viewModel.sTotEstPp1003Lckd;
                dataLoan.sTotEstCc1003Lckd = viewModel.sTotEstCc1003Lckd;
                dataLoan.sFfUfmip1003Lckd = viewModel.sFfUfmip1003Lckd;
                dataLoan.sLDiscnt1003Lckd = viewModel.sLDiscnt1003Lckd;
                dataLoan.sOCredit1Lckd = viewModel.sOCredit1Lckd;
                dataLoan.sTransNetCashLckd = viewModel.sTransNetCashLckd;
                dataLoan.sAltCostLckd = viewModel.sAltCostLckd;

                dataLoan.sPurchPrice_rep = viewModel.sPurchPrice;
                dataLoan.sAltCost_rep = viewModel.sAltCost;
                dataLoan.sLandCost_rep = viewModel.sLandCost;
                dataLoan.sRefPdOffAmt1003_rep = viewModel.sRefPdOffAmt1003;
                dataLoan.sTotEstPp1003_rep = viewModel.sTotEstPp1003;
                dataLoan.sTotEstCcNoDiscnt1003_rep = viewModel.sTotEstCcNoDiscnt1003;
                dataLoan.sFfUfmip1003_rep = viewModel.sFfUfmip1003;
                dataLoan.sLDiscnt1003_rep = viewModel.sLDiscnt1003;
                dataLoan.sOCredit1Desc = viewModel.sOCredit1Desc;

                if (false == dataLoan.sLoads1003LineLFromAdjustments)
                {
                    dataLoan.sOCredit2Desc = viewModel.sOCredit2Desc;
                    dataLoan.sOCredit3Desc = viewModel.sOCredit3Desc;
                    dataLoan.sOCredit4Desc = viewModel.sOCredit4Desc;
                }

                dataLoan.sTotCcPbsLocked = viewModel.sTotCcPbsLocked;
                dataLoan.sTotCcPbs_rep = viewModel.sTotCcPbs;
                dataLoan.sOCredit1Amt_rep = viewModel.sOCredit1Amt;

                if (false == dataLoan.sLoads1003LineLFromAdjustments)
                {
                    dataLoan.sOCredit2Amt_rep = viewModel.sOCredit2Amt;
                    dataLoan.sOCredit3Amt_rep = viewModel.sOCredit3Amt;
                    dataLoan.sOCredit4Amt_rep = viewModel.sOCredit4Amt;
                }

                dataLoan.sONewFinCc_rep = viewModel.sONewFinCc;
                dataLoan.sTransNetCash_rep = viewModel.sTransNetCash;
            }

            dataLoan.sClosingCostAutomationUpdateT = viewModel.sClosingCostAutomationUpdateT;

            dataLoan.sIPiaDyLckd = viewModel.sIPiaDyLckd;
            dataLoan.sIPiaDy_rep = viewModel.sIPiaDy;
            dataLoan.sIPerDayLckd = viewModel.sIPerDayLckd;
            dataLoan.sIPerDay_rep = viewModel.sIPerDay;

            if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowEnablingCustomFeeDescriptions))
            {
                dataLoan.sIsRequireFeesFromDropDown = viewModel.sIsRequireFeesFromDropDown;
            }
            dataLoan.sIsManuallySetThirdPartyAffiliateProps = viewModel.sIsManuallySetThirdPartyAffiliateProps;

            // Lender Credits tab
            dataLoan.sLenderCreditCalculationMethodT = viewModel.sLenderCreditCalculationMethodT;
            dataLoan.sLDiscntPc_rep = viewModel.sLDiscntPc;
            dataLoan.sLDiscntBaseT = viewModel.sLDiscntBaseT;
            dataLoan.sLDiscntFMb_rep = viewModel.sLDiscntFMb;
            dataLoan.sGfeCreditLenderPaidItemT = viewModel.sGfeCreditLenderPaidItemT;
            dataLoan.sLenderCustomCredit1Description = viewModel.sLenderCustomCredit1Description;
            dataLoan.sLenderCustomCredit1Amount_rep = viewModel.sLenderCustomCredit1Amount;
            dataLoan.sLenderCustomCredit2Description = viewModel.sLenderCustomCredit2Description;
            dataLoan.sLenderCustomCredit2Amount_rep = viewModel.sLenderCustomCredit2Amount;
            dataLoan.sLenderPaidFeeDiscloseLocationT = viewModel.sLenderPaidFeeDiscloseLocationT;
            dataLoan.sLenderGeneralCreditDiscloseLocationT = viewModel.sLenderGeneralCreditDiscloseLocationT;
            dataLoan.sLenderCustomCredit1DiscloseLocationT = viewModel.sLenderCustomCredit1DiscloseLocationT;
            dataLoan.sLenderCustomCredit2DiscloseLocationT = viewModel.sLenderCustomCredit2DiscloseLocationT;

            return viewModel;
        }

        private static string CreateViewModel(CPageData dataLoan, bool IsArchivePage, bool reinitialize, SampleClosingCostFeeViewModel oldViewModel)
        {
            SampleClosingCostFeeViewModel viewModel = new SampleClosingCostFeeViewModel();

            viewModel.DisableTPAffIfHasContact = dataLoan.sBranchChannelT == E_BranchChannelT.Retail || dataLoan.sBranchChannelT == E_BranchChannelT.Wholesale || dataLoan.sBranchChannelT == E_BranchChannelT.Broker;
            viewModel.LastSavedBorrowerOrigCompFee = oldViewModel != null ? oldViewModel.LastSavedBorrowerOrigCompFee : null;

            if (oldViewModel != null && oldViewModel.oldOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid && 
                dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid)
            {
                // We're updating the borrower paid originator compensation fee, so we should update the last saved originator compensation fee amounts.
                // No need to update when going from borrower paid to lender paid since the last saved should still be the most recent borrower paid amounts.
                viewModel.LastSavedBorrowerOrigCompFee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId);
            }

            if (reinitialize)
            {
                dataLoan.sClosingCostSet.Reinitialize();
            }

            viewModel.SectionList = dataLoan.sClosingCostSet.GetViewForSerialization(E_ClosingCostViewT.LoanHud1);
            viewModel.sLAmtCalc = dataLoan.sLAmtCalc_rep;
            viewModel.sLAmtLckd = dataLoan.sLAmtLckd;

            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew);

            viewModel.sGfeInitialDisclosureD = dataLoan.sGfeInitialDisclosureD_rep;
            viewModel.sGfeRedisclosureD = dataLoan.sGfeRedisclosureD_rep;
            viewModel.IsProtectDisclosureDates = dataLoan.BrokerDB.IsProtectDisclosureDates;

            if (IsArchivePage)
            {
                if (dataLoan.sClosingCostArchive.Count != 0)
                {
                    // We should be getting the selected value from the dropdown rather than the latest.
                    var latestArchive = dataLoan.sClosingCostArchive.FirstOrDefault( p=> p.ClosingCostArchiveType == LendersOffice.Common.SerializationTypes.ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015);
                    dataLoan.ApplyClosingCostArchive(latestArchive);
                }
                else
                {
                    throw new CBaseException("Archive not found", "Archive not found");
                }
            }

            if (oldViewModel != null && oldViewModel.oldOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid &&
                oldViewModel.LastSavedBorrowerOrigCompFee != null)
            {
                // The page before this recalc had a lender paid orig comp but it was switched to borrower paid.
                // We should try to restore the old borrower paid money amounts.
                BorrowerClosingCostFee currentOrigCompFee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId);
                if (currentOrigCompFee != null)
                {
                    currentOrigCompFee.FormulaT = oldViewModel.LastSavedBorrowerOrigCompFee.FormulaT;
                    currentOrigCompFee.BaseAmount = oldViewModel.LastSavedBorrowerOrigCompFee.BaseAmount;
                    currentOrigCompFee.Percent = oldViewModel.LastSavedBorrowerOrigCompFee.Percent;
                    currentOrigCompFee.PercentBaseT = oldViewModel.LastSavedBorrowerOrigCompFee.PercentBaseT;
                }

                viewModel.LastSavedBorrowerOrigCompFee = oldViewModel.LastSavedBorrowerOrigCompFee;
            }

            viewModel.sIsRequireFeesFromDropDown = dataLoan.sIsRequireFeesFromDropDown;
            viewModel.sIsManuallySetThirdPartyAffiliateProps = dataLoan.sIsManuallySetThirdPartyAffiliateProps;
            viewModel.sOriginatorCompensationPaymentSourceT = dataLoan.sOriginatorCompensationPaymentSourceT;
            viewModel.oldOriginatorCompensationPaymentSourceT = dataLoan.sOriginatorCompensationPaymentSourceT;

            viewModel.IsArchive = IsArchivePage;
            viewModel.sLastDisclosedGFEArchiveD = dataLoan.sLastDisclosedGFEArchiveD_rep;
            viewModel.sLastDisclosedGFEArchiveId = dataLoan.sLastDisclosedGFEArchiveId_rep;
            viewModel.sClosingCostFeeVersionT = dataLoan.sClosingCostFeeVersionT;
            viewModel.GfeTilCompanyName = gfeTil.CompanyName;
            viewModel.GfeTilStreetAddr = gfeTil.StreetAddr;
            viewModel.GfeTilCity = gfeTil.City;
            viewModel.GfeTilState = gfeTil.State;
            viewModel.GfeTilZip = gfeTil.Zip;
            viewModel.GfeTilPhoneOfCompany = gfeTil.PhoneOfCompany;
            viewModel.GfeTilEmailAddr = gfeTil.EmailAddr;
            viewModel.GfeTilPrepareDate = gfeTil.PrepareDate_rep;

            viewModel.sLpTemplateNm = dataLoan.sLpTemplateNm;
            viewModel.sCcTemplateNm = dataLoan.sCcTemplateNm;

            viewModel.sFinalLAmt = dataLoan.sFinalLAmt_rep;
            viewModel.sNoteIR = dataLoan.sNoteIR_rep;
            viewModel.sTerm = dataLoan.sTerm_rep;
            viewModel.sDue = dataLoan.sDue_rep;
            viewModel.sFinMethT = (int)dataLoan.sFinMethT;
            viewModel.sSchedDueD1Lckd = dataLoan.sSchedDueD1Lckd;
            viewModel.sSchedDueD1 = dataLoan.sSchedDueD1_rep;
            viewModel.sEstCloseDLckd = dataLoan.sEstCloseDLckd;
            viewModel.sEstCloseD = dataLoan.sEstCloseD_rep;
            viewModel.sConsummationDLckd = dataLoan.sConsummationDLckd;
            viewModel.sConsummationD = dataLoan.sConsummationD_rep;
            viewModel.sDaysInYr = dataLoan.sDaysInYr_rep;
            viewModel.sRAdj1stCapR = dataLoan.sRAdj1stCapR_rep;
            viewModel.sRAdjCapR = dataLoan.sRAdjCapR_rep;
            viewModel.sRAdj1stCapMon = dataLoan.sRAdj1stCapMon_rep;
            viewModel.sRAdjCapMon = dataLoan.sRAdjCapMon_rep;
            viewModel.sRAdjLifeCapR = dataLoan.sRAdjLifeCapR_rep;
            viewModel.sPmtAdjCapR = dataLoan.sPmtAdjCapR_rep;
            viewModel.sPmtAdjRecastPeriodMon = dataLoan.sPmtAdjRecastPeriodMon_rep;
            viewModel.sPmtAdjCapMon = dataLoan.sPmtAdjCapMon_rep;
            viewModel.sPmtAdjRecastStop = dataLoan.sPmtAdjRecastStop_rep;
            viewModel.sPmtAdjMaxBalPc = dataLoan.sPmtAdjMaxBalPc_rep;
            viewModel.sIOnlyMon = dataLoan.sIOnlyMon_rep;

            string[] timeVal = dataLoan.sGfeNoteIRAvailTillD_Time.Split(':');
            viewModel.sGfeNoteIRAvailTillD_Time_am = timeVal[1].Substring(3);
            viewModel.sGfeNoteIRAvailTillD_Time_hour = timeVal[0];
            viewModel.sGfeNoteIRAvailTillD_Time_minute = timeVal[1].Substring(0, 2);

            timeVal = dataLoan.sGfeEstScAvailTillD_Time.Split(':');
            viewModel.sGfeEstScAvailTillD_Time_am = timeVal[1].Substring(3);
            viewModel.sGfeEstScAvailTillD_Time_hour = timeVal[0];
            viewModel.sGfeEstScAvailTillD_Time_minute = timeVal[1].Substring(0, 2);

            viewModel.sGfeNoteIRAvailTillDLckd = dataLoan.sGfeNoteIRAvailTillDLckd;
            viewModel.sGfeNoteIRAvailTillD = dataLoan.sGfeNoteIRAvailTillD_Date;
            viewModel.sIsPrintTimeForGfeNoteIRAvailTillD = dataLoan.sIsPrintTimeForGfeNoteIRAvailTillD;

            viewModel.sGfeEstScAvailTillDLckd = dataLoan.sGfeEstScAvailTillDLckd;
            viewModel.sGfeEstScAvailTillD = dataLoan.sGfeEstScAvailTillD_Date;

            viewModel.sGfeNoteIRAvailTillDTimeZoneT = (int)dataLoan.sGfeNoteIRAvailTillDTimeZoneT;
            viewModel.sGfeEstScAvailTillDTimeZoneT = (int)dataLoan.sGfeEstScAvailTillDTimeZoneT;

            viewModel.sIsPrintTimeForsGfeEstScAvailTillD = dataLoan.sIsPrintTimeForsGfeEstScAvailTillD;

            viewModel.sGfeRateLockPeriodLckd = dataLoan.sGfeRateLockPeriodLckd;
            viewModel.sGfeRateLockPeriod = dataLoan.sGfeRateLockPeriod_rep;
            viewModel.sGfeLockPeriodBeforeSettlement = dataLoan.sGfeLockPeriodBeforeSettlement_rep;
            viewModel.sGfeCanRateIncrease = dataLoan.sGfeCanRateIncrease;
            viewModel.sRLifeCapR = dataLoan.sRLifeCapR_rep;
            viewModel.sGfeFirstInterestChangeIn = dataLoan.sGfeFirstInterestChangeIn;
            viewModel.sGfeCanLoanBalanceIncrease = dataLoan.sGfeCanLoanBalanceIncrease;
            viewModel.sGfeMaxLoanBalance = dataLoan.sGfeMaxLoanBalance_rep;
            viewModel.sGfeFirstPaymentChangeIn = dataLoan.sGfeFirstPaymentChangeIn;
            viewModel.sGfeFirstAdjProThisMPmtAndMIns = dataLoan.sGfeFirstAdjProThisMPmtAndMIns_rep;
            viewModel.sGfeCanPaymentIncrease = dataLoan.sGfeCanPaymentIncrease;
            viewModel.sGfeMaxProThisMPmtAndMIns = dataLoan.sGfeMaxProThisMPmtAndMIns_rep;
            viewModel.sGfeHavePpmtPenalty = dataLoan.sGfeHavePpmtPenalty;
            viewModel.sGfeMaxPpmtPenaltyAmt = dataLoan.sGfeMaxPpmtPenaltyAmt_rep;
            viewModel.sGfeIsBalloon = dataLoan.sGfeIsBalloon;
            viewModel.sGfeBalloonPmt = dataLoan.sGfeBalloonPmt_rep;
            viewModel.sGfeBalloonDueInYrs = dataLoan.sGfeBalloonDueInYrs;
            viewModel.sMldsHasImpound = dataLoan.sMldsHasImpound;

            viewModel.sGfeOriginationF = dataLoan.sGfeOriginationF_rep;
            viewModel.sLDiscnt = dataLoan.sLDiscnt_rep;
            viewModel.sGfeRequiredServicesTotalFee = dataLoan.sGfeRequiredServicesTotalFee_rep;
            viewModel.sGfeServicesYouShopTotalFee = dataLoan.sGfeServicesYouShopTotalFee_rep;
            viewModel.sGfeInitialImpoundDeposit = dataLoan.sGfeInitialImpoundDeposit_rep;
            viewModel.sGfeTotalOtherSettlementServiceFee = dataLoan.sGfeTotalOtherSettlementServiceFee_rep;
            viewModel.sGfeLenderTitleTotalFee = dataLoan.sGfeLenderTitleTotalFee_rep;
            viewModel.sGfeGovtRecTotalFee = dataLoan.sGfeGovtRecTotalFee_rep;
            viewModel.sGfeDailyInterestTotalFee = dataLoan.sGfeDailyInterestTotalFee_rep;
            viewModel.sGfeAdjOriginationCharge = dataLoan.sGfeAdjOriginationCharge_rep;
            viewModel.sGfeOwnerTitleTotalFee = dataLoan.sGfeOwnerTitleTotalFee_rep;
            viewModel.sGfeTransferTaxTotalFee = dataLoan.sGfeTransferTaxTotalFee_rep;
            viewModel.sGfeHomeOwnerInsuranceTotalFee = dataLoan.sGfeHomeOwnerInsuranceTotalFee_rep;
            viewModel.sGfeNotApplicableF = dataLoan.sGfeNotApplicableF_rep;

            viewModel.sGfeTradeOffLowerCCLoanAmt = dataLoan.sGfeTradeOffLowerCCLoanAmt_rep;
            viewModel.sGfeTradeOffLowerRateLoanAmt = dataLoan.sGfeTradeOffLowerRateLoanAmt_rep;
            viewModel.sGfeTradeOffLowerCCNoteIR = dataLoan.sGfeTradeOffLowerCCNoteIR_rep;
            viewModel.sGfeTradeOffLowerRateNoteIR = dataLoan.sGfeTradeOffLowerRateNoteIR_rep;
            viewModel.sGfeProThisMPmtAndMIns = dataLoan.sGfeProThisMPmtAndMIns_rep;
            viewModel.sGfeTradeOffLowerCCMPmtAndMIns = dataLoan.sGfeTradeOffLowerCCMPmtAndMIns_rep;
            viewModel.sGfeTradeOffLowerRateMPmtAndMIns = dataLoan.sGfeTradeOffLowerRateMPmtAndMIns_rep;
            viewModel.sGfeTradeOffLowerCCMPmtAndMInsDiff = dataLoan.sGfeTradeOffLowerCCMPmtAndMInsDiff_rep;
            viewModel.sGfeTradeOffLowerRateMPmtAndMInsDiff = dataLoan.sGfeTradeOffLowerRateMPmtAndMInsDiff_rep;
            viewModel.sGfeTradeOffLowerCCClosingCostDiff = dataLoan.sGfeTradeOffLowerCCClosingCostDiff_rep;
            viewModel.sGfeTradeOffLowerRateClosingCostDiff = dataLoan.sGfeTradeOffLowerRateClosingCostDiff_rep;
            viewModel.sGfeTotalEstimateSettlementCharge = dataLoan.sGfeTotalEstimateSettlementCharge_rep;
            viewModel.sGfeTradeOffLowerCCClosingCost = dataLoan.sGfeTradeOffLowerCCClosingCost_rep;
            viewModel.sGfeTradeOffLowerRateClosingCost = dataLoan.sGfeTradeOffLowerRateClosingCost_rep;

           viewModel.sGfeShoppingCartLoan1OriginatorName = dataLoan.sGfeShoppingCartLoan1OriginatorName;
           viewModel.sGfeShoppingCartLoan2OriginatorName = dataLoan.sGfeShoppingCartLoan2OriginatorName;
           viewModel.sGfeShoppingCartLoan3OriginatorName = dataLoan.sGfeShoppingCartLoan3OriginatorName;
           viewModel.sGfeShoppingCartLoan1LoanAmt = dataLoan.sGfeShoppingCartLoan1LoanAmt_rep;
           viewModel.sGfeShoppingCartLoan2LoanAmt = dataLoan.sGfeShoppingCartLoan2LoanAmt_rep;
           viewModel.sGfeShoppingCartLoan3LoanAmt = dataLoan.sGfeShoppingCartLoan3LoanAmt_rep;
           viewModel.sGfeShoppingCartLoan1LoanTerm = dataLoan.sGfeShoppingCartLoan1LoanTerm_rep;
           viewModel.sGfeShoppingCartLoan2LoanTerm = dataLoan.sGfeShoppingCartLoan2LoanTerm_rep;
           viewModel.sGfeShoppingCartLoan3LoanTerm = dataLoan.sGfeShoppingCartLoan3LoanTerm_rep;
           viewModel.sGfeShoppingCartLoan1NoteIR = dataLoan.sGfeShoppingCartLoan1NoteIR_rep;
           viewModel.sGfeShoppingCartLoan2NoteIR = dataLoan.sGfeShoppingCartLoan2NoteIR_rep;
           viewModel.sGfeShoppingCartLoan3NoteIR = dataLoan.sGfeShoppingCartLoan3NoteIR_rep;
           viewModel.sGfeShoppingCartLoan1InitialPmt = dataLoan.sGfeShoppingCartLoan1InitialPmt_rep;
           viewModel.sGfeShoppingCartLoan2InitialPmt = dataLoan.sGfeShoppingCartLoan2InitialPmt_rep;
           viewModel.sGfeShoppingCartLoan3InitialPmt = dataLoan.sGfeShoppingCartLoan3InitialPmt_rep;
           viewModel.sGfeShoppingCartLoan1RateLockPeriod = dataLoan.sGfeShoppingCartLoan1RateLockPeriod_rep;
           viewModel.sGfeShoppingCartLoan2RateLockPeriod = dataLoan.sGfeShoppingCartLoan2RateLockPeriod_rep;
           viewModel.sGfeShoppingCartLoan3RateLockPeriod = dataLoan.sGfeShoppingCartLoan3RateLockPeriod_rep;
           viewModel.sGfeShoppingCartLoan1CanRateIncreaseTri = dataLoan.sGfeShoppingCartLoan1CanRateIncreaseTri == E_TriState.Yes;
           viewModel.sGfeShoppingCartLoan2CanRateIncreaseTri = dataLoan.sGfeShoppingCartLoan2CanRateIncreaseTri == E_TriState.Yes;
           viewModel.sGfeShoppingCartLoan3CanRateIncreaseTri = dataLoan.sGfeShoppingCartLoan3CanRateIncreaseTri == E_TriState.Yes;
           viewModel.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri = dataLoan.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri == E_TriState.Yes;
           viewModel.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri = dataLoan.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri == E_TriState.Yes;
           viewModel.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri = dataLoan.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri == E_TriState.Yes;
           viewModel.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri = dataLoan.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri == E_TriState.Yes;
           viewModel.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri = dataLoan.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri == E_TriState.Yes;
           viewModel.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri = dataLoan.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri == E_TriState.Yes;
           viewModel.sGfeShoppingCartLoan1HavePpmtPenaltyTri = dataLoan.sGfeShoppingCartLoan1HavePpmtPenaltyTri == E_TriState.Yes;
           viewModel.sGfeShoppingCartLoan2HavePpmtPenaltyTri = dataLoan.sGfeShoppingCartLoan2HavePpmtPenaltyTri == E_TriState.Yes;
           viewModel.sGfeShoppingCartLoan3HavePpmtPenaltyTri = dataLoan.sGfeShoppingCartLoan3HavePpmtPenaltyTri == E_TriState.Yes;
           viewModel.sGfeShoppingCartLoan1IsBalloonTri = dataLoan.sGfeShoppingCartLoan1IsBalloonTri == E_TriState.Yes;
           viewModel.sGfeShoppingCartLoan2IsBalloonTri = dataLoan.sGfeShoppingCartLoan2IsBalloonTri == E_TriState.Yes;
           viewModel.sGfeShoppingCartLoan3IsBalloonTri = dataLoan.sGfeShoppingCartLoan3IsBalloonTri == E_TriState.Yes;
           viewModel.sGfeShoppingCartLoan1TotalClosingCost = dataLoan.sGfeShoppingCartLoan1TotalClosingCost_rep;
           viewModel.sGfeShoppingCartLoan2TotalClosingCost = dataLoan.sGfeShoppingCartLoan2TotalClosingCost_rep;
           viewModel.sGfeShoppingCartLoan3TotalClosingCost = dataLoan.sGfeShoppingCartLoan3TotalClosingCost_rep;

            viewModel.IsConstruction = dataLoan.sLPurposeT == E_sLPurposeT.Construct || dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm;
            viewModel.AreConstructionLoanDataPointsMigrated = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges);

           viewModel.sGfeTotalFundByLender = dataLoan.sGfeTotalFundByLender_rep;
           viewModel.sGfeTotalFundBySeller = dataLoan.sGfeTotalFundBySeller_rep;
           viewModel.sPurchPrice = dataLoan.sPurchPrice_rep;
           viewModel.sPurchasePrice1003 = dataLoan.sPurchasePrice1003_rep;
           viewModel.sIsRenovationLoan = dataLoan.sIsRenovationLoan;
           viewModel.sAltCostLckd = dataLoan.sAltCostLckd;
           viewModel.sAltCost = dataLoan.sAltCost_rep;
           viewModel.sLandCost = dataLoan.sLandCost_rep;
           viewModel.sLandIfAcquiredSeparately1003 = dataLoan.sLandIfAcquiredSeparately1003_rep;
           viewModel.sRefPdOffAmt1003Lckd = dataLoan.sRefPdOffAmt1003Lckd;
           viewModel.sRefPdOffAmt1003 = dataLoan.sRefPdOffAmt1003_rep;
           viewModel.sTotEstPp1003Lckd = dataLoan.sTotEstPp1003Lckd;
           viewModel.sTotEstPp1003 = dataLoan.sTotEstPp1003_rep;
           viewModel.sTotEstCc1003Lckd = dataLoan.sTotEstCc1003Lckd;
           viewModel.sTotEstCcNoDiscnt1003 = dataLoan.sTotEstCcNoDiscnt1003_rep;
           viewModel.sFfUfmip1003Lckd = dataLoan.sFfUfmip1003Lckd;
           viewModel.sFfUfmip1003 = dataLoan.sFfUfmip1003_rep;
           viewModel.sLDiscnt1003Lckd = dataLoan.sLDiscnt1003Lckd;
           viewModel.sLDiscnt1003 = dataLoan.sLDiscnt1003_rep;
           viewModel.sTotTransC = dataLoan.sTotTransC_rep;
           viewModel.sOCredit1Desc = dataLoan.sOCredit1Desc;
           viewModel.sOCredit2Desc = dataLoan.sOCredit2Desc;
           viewModel.sOCredit3Desc = dataLoan.sOCredit3Desc;
           viewModel.sOCredit4Desc = dataLoan.sOCredit4Desc;
           viewModel.sONewFinBal = dataLoan.sONewFinBal_rep;
           viewModel.sTotCcPbsLocked = dataLoan.sTotCcPbsLocked;
           viewModel.sTotCcPbs = dataLoan.sTotCcPbs_rep;
           viewModel.sOCredit1Lckd = dataLoan.sOCredit1Lckd;
           viewModel.sOCredit1Amt = dataLoan.sOCredit1Amt_rep;
           viewModel.sOCredit2Amt = dataLoan.sOCredit2Amt_rep;
           viewModel.sOCredit3Amt = dataLoan.sOCredit3Amt_rep;
           viewModel.sOCredit4Amt = dataLoan.sOCredit4Amt_rep;
           viewModel.sOCredit5Amt = dataLoan.sOCredit5Amt_rep;
           viewModel.sONewFinCc = dataLoan.sONewFinCc_rep;
           viewModel.sFfUfmipFinanced = dataLoan.sFfUfmipFinanced_rep;
           viewModel.sTransNetCashLckd = dataLoan.sTransNetCashLckd;
           viewModel.sTransNetCash = dataLoan.sTransNetCash_rep;

           viewModel.sClosingCostAutomationUpdateT = dataLoan.sClosingCostAutomationUpdateT;

           viewModel.sIPiaDyLckd = dataLoan.sIPiaDyLckd;
           viewModel.sIPiaDy = dataLoan.sIPiaDy_rep;
           viewModel.sIPerDayLckd = dataLoan.sIPerDayLckd;
           viewModel.sIPerDay = dataLoan.sIPerDay_rep;

           viewModel.sGfeIsTPOTransaction = dataLoan.sGfeIsTPOTransaction;
           viewModel.sGfeIsTPOTransactionIsCalculated = dataLoan.sGfeIsTPOTransactionIsCalculated;
           viewModel.CanReadDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserRead);
           viewModel.CanSetDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserWrite);

           // Lender Credits Tab
           viewModel.sLenderCreditCalculationMethodT = dataLoan.sLenderCreditCalculationMethodT;
           viewModel.sBrokerLockOriginatorPriceBrokComp1PcPrice = dataLoan.sBrokerLockOriginatorPriceBrokComp1PcPrice;
           viewModel.sBrokerLockOriginatorPriceBrokComp1PcAmt = dataLoan.sBrokerLockOriginatorPriceBrokComp1PcAmt_rep;
           viewModel.sLDiscntPc = dataLoan.sLDiscntPc_rep;
           viewModel.sLDiscntBaseT = dataLoan.sLDiscntBaseT;
           viewModel.sLDiscntFMb = dataLoan.sLDiscntFMb_rep;
           viewModel.sGfeCreditLenderPaidItemT = dataLoan.sGfeCreditLenderPaidItemT;
           viewModel.sGfeCreditLenderPaidItemF = dataLoan.sGfeCreditLenderPaidItemF_rep;
           viewModel.sGfeLenderCreditF = dataLoan.sGfeLenderCreditF_rep;
           viewModel.sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt = dataLoan.sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt_rep;
           viewModel.sLenderCustomCredit1Description = dataLoan.sLenderCustomCredit1Description;
           viewModel.sLenderCustomCredit1Amount = dataLoan.sLenderCustomCredit1Amount_rep;
           viewModel.sLenderCustomCredit1AmountAsCharge = dataLoan.sLenderCustomCredit1AmountAsCharge_rep;
           viewModel.sLenderCustomCredit2Description = dataLoan.sLenderCustomCredit2Description;
           viewModel.sLenderCustomCredit2Amount = dataLoan.sLenderCustomCredit2Amount_rep;
           viewModel.sLenderCustomCredit2AmountAsCharge = dataLoan.sLenderCustomCredit2AmountAsCharge_rep;
           viewModel.sLenderActualTotalCreditAmt_Neg = dataLoan.sLenderActualTotalCreditAmt_Neg_rep;
           viewModel.sLenderCreditAvailableAmt_Neg = dataLoan.sLenderCreditAvailableAmt_Neg_rep;
           viewModel.sLenderPaidFeeDiscloseLocationT = dataLoan.sLenderPaidFeeDiscloseLocationT;
           viewModel.sLenderPaidFeesAmt_Neg = dataLoan.sLenderPaidFeesAmt_Neg_rep;
           viewModel.sLenderGeneralCreditDiscloseLocationT = dataLoan.sLenderGeneralCreditDiscloseLocationT;
           viewModel.sLenderGeneralCreditAmt_Neg = dataLoan.sLenderGeneralCreditAmt_Neg_rep;
           viewModel.sLenderCustomCredit1DiscloseLocationT = dataLoan.sLenderCustomCredit1DiscloseLocationT;
           viewModel.sLenderCustomCredit2DiscloseLocationT = dataLoan.sLenderCustomCredit2DiscloseLocationT;

            return ObsoleteSerializationHelper.JsonSerialize(viewModel);
        }

        [WebMethod]
        public static string GetAvailableClosingCostFeeTypes(Guid loanId, string viewModelJson, string sectionName)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(RActiveGFE));
                dataLoan.InitLoad();

                // 4/1/2015 dd - The ClosingCostFeeSection get modify in memory when calling section.ClosingCostFeeList.
                // To discard the BrokerDB from cache we will force to reload from database each time.
                BrokerDB db = BrokerDB.RetrieveByIdForceRefresh(PrincipalFactory.CurrentPrincipal.BrokerId);

                SampleClosingCostFeeViewModel viewModel = BindToDataLoan(dataLoan, viewModelJson, null, null);

                FeeSetupClosingCostSet closingCostSet = db.GetUnlinkedClosingCostSet();
                FeeSetupClosingCostFeeSection section = (FeeSetupClosingCostFeeSection)closingCostSet.GetSection(E_ClosingCostViewT.LenderTypeHud1, sectionName);

                List<BaseClosingCostFee> fees = new List<BaseClosingCostFee>();

                foreach (BorrowerClosingCostFeeSection closingSection in viewModel.SectionList)
                {
                    foreach (BorrowerClosingCostFee fee in closingSection.FilteredClosingCostFeeList)
                    {
                        fees.Add(fee.ConvertToFeeSetupFee());
                    }
                }

                section.SetExcludeFeeList(fees, dataLoan.sClosingCostFeeVersionT, o => o.CanManuallyAddToEditor == false);

                List<object> subFeesBySourceFeeTypeId = section.FilteredClosingCostFeeList
                    .Select(f => new {
                        SourceFeeTypeId = f.ClosingCostFeeTypeId,
                        SubFees = dataLoan.sClosingCostSet.GetFees(fee => fee.SourceFeeTypeId == f.ClosingCostFeeTypeId).ToList()
                    }).ToList<object>();

                return SerializationHelper.JsonNetAnonymousSerialize(new { Section = section, SubFeesBySourceFeeTypeId = subFeesBySourceFeeTypeId });
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string CalculateData(Guid loanId, string viewModelJson, bool IsArchive)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(RActiveGFE));
                dataLoan.InitLoad();
                
                SampleClosingCostFeeViewModel oldViewModel = BindToDataLoan(dataLoan, viewModelJson, null, null);

                return CreateViewModel(dataLoan, IsArchive, true, oldViewModel);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string Save(Guid loanId, string viewModelJson, bool IsArchive)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(RActiveGFE));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                SampleClosingCostFeeViewModel oldViewModel = null;
                if (!IsArchive)
                {
                    oldViewModel = BindToDataLoan(dataLoan, viewModelJson, null, true);

                    if (dataLoan.sClosingCostAutomationUpdateT == E_sClosingCostAutomationUpdateT.PreserveFeesOnLoan)
                    {
                        // OPM 141471.
                        dataLoan.RecalculateClosingFeeConditions(true); //Force the calc regardless if revision changed.
                        dataLoan.sClosingCostAutomationUpdateT = E_sClosingCostAutomationUpdateT.UpdateOnConditionChange;
                    }
                }

                dataLoan.Save();

                return CreateViewModel(dataLoan, IsArchive, false, oldViewModel);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                { 
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string CreateAgent(Guid loanId, Guid id, bool populateFromRolodex, E_AgentRoleT agentType)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(RActiveGFE));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                CAgentFields agent = dataLoan.GetAgentFields(Guid.Empty);
                if (populateFromRolodex)
                {
                    agent.PopulateFromRolodex(id, BrokerUserPrincipal.CurrentPrincipal);
                }
                else
                {
                    CommonFunctions.CopyEmployeeInfoToAgent(PrincipalFactory.CurrentPrincipal.BrokerId, agent, id);
                }

                agent.AgentRoleT = agentType;
                agent.Update();

                dataLoan.Save();

                return "{\"RecordId\": \"" + agent.RecordId.ToString() + "\"}";
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string ClearAgent(Guid loanId, string viewModelJson, Guid recordId, bool IsArchive)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(RActiveGFE));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                // Bind unsaved data to loan
                SampleClosingCostFeeViewModel oldViewModel = BindToDataLoan(dataLoan, viewModelJson, null, null);

                DataSet ds = dataLoan.sAgentDataSet;
                DataTable table = ds.Tables[0];

                // Find agent to be deleted.
                DataRow toBeDeleted = null;
                foreach (DataRow row in table.Rows)
                {
                    if ((string)row["recordId"] == recordId.ToString())
                    {
                        // I could not invoke the Delete() on row object here.
                        // Doing so will modify the collection of table.Rows which cause
                        // the exception to be throw. dd 4/21/2003
                        toBeDeleted = row;
                        break;
                    }
                }

                // If agent found then delete and save. Otherwise do nothing.
                if (toBeDeleted != null)
                {
                    dataLoan.RecordAuditOnSave(AgentRecordChangeAuditHelper.CreateAuditEvent(toBeDeleted, dataLoan.sSpState, AgentRecordChangeType.DeleteRecord));

                    // OPM 209868 - Clear beneficiary info in any closing cost fee where this agent is the beneficiary.
                    // If we have access to this page/method we can assume that the loan has been migrated.
                    dataLoan.sClosingCostSet.ClearBeneficiary(new Guid((string)toBeDeleted["recordId"]));
                    toBeDeleted.Delete();
                }

                dataLoan.sAgentDataSet = ds;

                dataLoan.Save();

                // Return deserialized data set.
                return CreateViewModel(dataLoan, IsArchive, false, oldViewModel);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }
    }
}
