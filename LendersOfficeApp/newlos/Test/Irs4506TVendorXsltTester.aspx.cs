﻿//-----------------------------------------------------------------------
// <copyright file="Irs4506TVendorXsltTester.aspx.cs" company="Meridianlink">
//     Copyright (c) Sprocket Enterprises. All rights reserved.
// </copyright>
// <author>Scott Kibler</author>
// <summary>
//  This is for testing combining loxml with xslt and comparing it with the irs4506T request xml.
// </summary>
//-----------------------------------------------------------------------

namespace LendersOfficeApp.newlos.Test
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Xsl;
    using DataAccess;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Integration.Irs4506T;
    using LendersOffice.Integration;

    /// <summary>
    /// Import class handle loan overwrite. 
    /// </summary>
    public partial class Irs4506TVendorXsltTester : BaseLoanPage
    {
        private CommonInputsForRequest ci;

        protected void PageLoad(object sender, EventArgs e)
        {
            this.ErrorStatus.Text = string.Empty;
            this.XsltXmlVs4506tXmlEqualityStatus.Visible = false;
        }

        protected void PageInit(object sender, EventArgs e)
        {
            this.TransactionIdElement.Text = Guid.NewGuid().ToString();

            var activeLqbModelVendors = Irs4506TVendorConfiguration.ListActiveVendorByBrokerId(BrokerID).Where(v => v.CommunicationModel == Irs4506TCommunicationModel.LendingQB).ToList();
            ActiveLqbModelVendorDropdown.DataSource = activeLqbModelVendors;
            ActiveLqbModelVendorDropdown.DataTextField = "VendorName";
            ActiveLqbModelVendorDropdown.DataValueField = "VendorId";
            ActiveLqbModelVendorDropdown.DataBind();
        }

        /// <summary>
        /// Transforms the xml with the specified xsl data.  May also call LOFormatExporter.Export.
        /// <param name="sender">The sender of the method.</param>
        /// <param name="e">The arguments for the method.</param>
        protected void ClickTransform(object sender, EventArgs e)
        {
            try
            {
                if (ConstAppDavid.CurrentServerLocation == ServerLocation.Production)
                {
                    if (!Tools.RequestIsOnLoauth().Value)
                    {
                        // The reason to block is to prevent many different xslt's from being loaded into the cache on any server other than loauth.
                        throw new NotSupportedException("You cannot run this here, except through our internal site.");
                    }
                }
                
                var userFileName = BrokerUser.UserId.ToString();
                var xslFilePath = TempFileUtils.Name2Path(SharedDbBackedFilesManager.GetFileKey(SharedDbBackedFileType.Xsl, userFileName));
                File.WriteAllText(xslFilePath, this.XslElement.Text.Trim());

                var loXmlFilePath = TempFileUtils.Name2Path(SharedDbBackedFilesManager.GetFileKey(SharedDbBackedFileType.LoXml, userFileName));
                File.WriteAllText(loXmlFilePath, this.LoXmlElement.Text.Trim());

                var ci = this.GetCommonInputs();
                var requestFormatDetails = new VendorSpecificIntegrationRequestFormat()
                {
                    IntegrationRequestType = IntegrationRequestType.Irs4506T_Order,
                    VendorId = ci.VendorId,
                    LoXmlFileName = userFileName,
                    XslFileName = userFileName,
                };

                var resultAndEncoding = Irs4506TServer.CreateRequestWithXslTransform(requestFormatDetails, 
                    this.BrokerUser, ci.TransactionId, ci.Credentials, this.LoanID, ci.ApplicationId, ci.BorrowerMode, ci.Edoc);

                var encoding = resultAndEncoding.Encoding;
                var bytes = resultAndEncoding.Bytes;
                this.ResultantXmlElement.Text = encoding.GetString(bytes);
            }
            catch (Exception ex)
            {
                this.ErrorStatus.Text += ex.ToString();
            }
        }

        protected void ClickExport(object sender, EventArgs e)
        {
            try
            {
                var loanExportXml = this.LoXmlElement.Text.Trim();
                var borrowerMode = this.CoborrowerModeCheckbox.Checked ? E_BorrowerModeT.Coborrower : E_BorrowerModeT.Borrower;
                this.NormalXmlElement.Text = LendersOffice.Conversions.LOFormatExporter.Export(this.LoanID, this.BrokerUser, loanExportXml, default(FormatTarget), bypassFieldSecurity:false, isClosingDisclosure:false, useTempArchiveForTridFile:false, excludeZeroAdjustmentForDocGenInLoXml:false, borrowerMode:borrowerMode);
            }
            catch (Exception ex)
            {
                this.ErrorStatus.Text += Environment.NewLine + ex.ToString();
            }
        }

        protected void ClickExportAndConvert(object sender, EventArgs e)
        {
            this.ClickExport(sender, e);
            this.ClickTransform(sender, e);
        }

        protected void ClickGet4506TRequestXml(object sender, EventArgs e)
        {
            try
            {
                var ci = this.GetCommonInputs();
                
                var request = Irs4506TServer.CreateRequestObject(this.BrokerUser, ci.TransactionId, ci.Credentials, this.LoanID, this.ApplicationID, ci.BorrowerMode, ci.Edoc);
                using (var ms = new MemoryStream())
                {
                    var settings = new XmlWriterSettings();
                    settings.Encoding = System.Text.Encoding.ASCII;
                    using (XmlWriter writer = XmlTextWriter.Create(ms, settings))
                    {
                        request.WriteXml(writer);
                    }

                    this.Four506TRequestXmlElement.Text = settings.Encoding.GetString(ms.ToArray());
                }
            }
            catch (Exception ex)
            {
                this.ErrorStatus.Text += Environment.NewLine + ex.ToString();
            }
        }

        protected void ClickCompareXsltAnd4506tXml(object sender, EventArgs e)
        {
            try
            {
                this.ClickGet4506TRequestXml(sender, e);
                this.ClickExportAndConvert(sender, e);
                
                this.XsltXmlVs4506tXmlEqualityStatus.Visible = true;
                bool areEqual = this.Four506TRequestXmlElement.Text == this.ResultantXmlElement.Text;
                this.XsltXmlVs4506tXmlEqualityStatus.Text = "The xml from xsl transform vs the class are " + (areEqual ? string.Empty : "NOT ") + "Equal";
            }
            catch (Exception ex)
            {
                this.ErrorStatus.Text += Environment.NewLine + ex.ToString();
            }
        }

        override protected void OnInit(EventArgs e)
        {
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            this.InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.PageLoad);
            this.Init += new EventHandler(this.PageInit);
        }

        /// <summary>
        /// Gets the inputs from the UI to be used in building the request.
        /// </summary>
        /// <returns>The inputs to be used for the request.</returns>
        private CommonInputsForRequest GetCommonInputs()
        {
            if(ci != null)
            {
                return ci;
            }

            ci = new CommonInputsForRequest();
            var applicationIdString = this.ApplicationIdElement.Text.Trim();
            if (!string.IsNullOrEmpty(applicationIdString))
            {
                ci.ApplicationId = Guid.Parse(applicationIdString);
            }
            else
            {
                ci.ApplicationId = this.ApplicationID;
            }

            ci.TransactionId = Guid.Parse(this.TransactionIdElement.Text.Trim());

            ci.VendorId = Guid.Parse(this.ActiveLqbModelVendorDropdown.SelectedValue);
            ci.Credentials = Irs4506TVendorCredential.Retrieve(this.BrokerUser.BrokerId, this.BrokerUser.UserId, ci.VendorId);

            ci.BorrowerMode = this.CoborrowerModeCheckbox.Checked ? E_BorrowerModeT.Coborrower : E_BorrowerModeT.Borrower;

            var documentIdString = this.DocumentIdElement.Text.Trim();
            if (!string.IsNullOrEmpty(documentIdString))
            {
                var documentId = Guid.Parse(documentIdString);
                var edocRepo = EDocumentRepository.GetUserRepository();
                ci.Edoc = edocRepo.GetDocumentById(documentId);
            }

            return ci;
        }

        /// <summary>
        /// A POCO class that merely encapsulates the UI inputs and other inputs to the request builder.
        /// </summary>
        private class CommonInputsForRequest
        {
            /// <summary>
            /// Gets or sets the id of the application to be used for the request.
            /// </summary>
            /// <value>The id of the application of the loan to be used for the request.</value>
            public Guid ApplicationId { get; set; }

            /// <summary>
            /// Gets or sets the id of the vendor to be used for the request.
            /// </summary>
            /// <value>The id of the vendor to be used for the request.</value>
            public Guid VendorId { get; set; }

            /// <summary>
            /// Gets or sets the id of the transaction to be used for the request;
            /// </summary>
            /// <value>The id to be used for the transaction.</value>
            public Guid TransactionId { get; set; }

            /// <summary>
            /// Gets or sets the credentials to be used for the request;
            /// </summary>
            /// <value>The credentials to be used for the request.  Shoudl not be null.</value>
            public Irs4506TVendorCredential Credentials { get; set; }

            /// <summary>
            /// Gets or sets the borrower mode to be used for the request;
            /// </summary>
            /// <value>The borrower mode to be used for the request.</value>
            public E_BorrowerModeT BorrowerMode { get; set; }

            /// <summary>
            /// The edocument to be used for the request, or null.
            /// </summary>
            /// <value>The edocument instance ot be used for the request, or null.</value>
            public EDocument Edoc { get; set; }
        }
    }
}
