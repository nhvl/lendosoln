<%@ Page language="c#" Codebehind="TestCreditReport.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Test.TestCreditReport" EnableSessionState="False" enableViewState="False" validateRequest="False"%>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>TestCreditReport</title>
		<STYLE type="text/css">
.FieldValue { FONT-WEIGHT: bold; COLOR: green }
		</STYLE>
	</HEAD>
	<body MS_POSITIONING="FlowLayout" class="RightBackground" onresize="f_onresize();">
		<script type="text/javascript">
var MismoTools =
{
	mismoWindow : null,  
	OpenWindow : function() 
	{
	
		if (  !this.mismoWindow || this.mismoWindow.closed ) 
		{
			this.mismoWindow = window.open('TestCreditReport.aspx?format=mismo&loanid=' + <%= AspxTools.JsString(LoanID) %> + '&appid=' + <%= AspxTools.JsString(ApplicationID)%>);
		}
		
		
		if ( this.mismoWindow.focus ) 
		{
			this.mismoWindow.focus();
		}
	}
};
 
function _init() {
  f_onresize();
}
  function f_onresize() {
    var h = document.body.clientHeight;
    document.getElementById("MainDiv").style.height=(h - 150) + "px";    
  }
  
function f_constructUrl() {
  var sLId = <%= AspxTools.JsString(LoanID) %>;
  var aAppId = <%= AspxTools.JsString(ApplicationID) %>;
  var url =  'TestCreditReport.aspx?appid=' + aAppId + '&loanid=' + sLId;

  return url;
  
}  
function f_viewRawXML() {
  var url = f_constructUrl() + '&cmd=xml';
  window.open(url);
}
function f_viewLiabilities() {
  var url = f_constructUrl() + '&cmd=liabilities';
  window.open(url);
}
function f_viewLiabilityXml(id) {
  var url = f_constructUrl() + '&cmd=ind_lia&liaid=' + id;
  window.open(url);
}
function f_viewPublicRecords() {
  var url = f_constructUrl() + '&cmd=publicrecords';
  window.open(url);
}
function f_viewPublicRecordXml(id) {
  var url = f_constructUrl() + '&cmd=ind_public&publicid=' + id;
  window.open(url);

}
function f_viewDebugTagListXml(name) {
  var url = f_constructUrl() + '&cmd=debug_tag_list&methodname=' + name;
  window.open(url);
}
function f_toggleDependend(id) {
  var o = document.getElementById(id);
  if (null == o) {
    alert('No debug info to display.');
    return false;
  }

  o.style.display = o.style.display == 'none' ? '' : 'none';
  return false;
}
function f_viewTemporaryTotalScore() {
  var url = <%= AspxTools.JsString(DataAccess.Tools.VRoot) %> + '/newlos/services/TemporaryFHATotalFindings.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %>;
  window.open(url);
}
      function insertKeyword( ed, ddl ) {
          ed.focus();
          ed.value += " " + ddl.options[ddl.selectedIndex].value + " ";
      }
    
		</script>
		<form id="TestCreditReport" method="post" runat="server" enctype="multipart/form-data">
		
			<% if (RequestHelper.GetSafeQueryString("cmd") == "" || RequestHelper.GetSafeQueryString("cmd") == null) { %>
			<input type="file" style="WIDTH: 512px; HEIGHT: 22px" size="66" runat="server" id="m_file">
			<asp:Button id="Button1" runat="server" Text="Upload XML Credit (DEBUG ONLY)" width="234px" onclick="Button1_Click"></asp:Button><asp:Button id="Button2" runat="server" Text="Delete Credit Report (DEBUG ONLY)" onclick="Button2_Click"></asp:Button>&nbsp;
			<asp:Button id="m_btnCalculate" runat="server" Text="Calculate" onclick="m_btnCalculate_Click"></asp:Button>
			<asp:Button ID="m_btnGenerateTemporaryTotalScore" runat="server" Text="Generate Temporary TotalScore Certificate" OnClick="m_btnGenerateTemporaryTotalScore_Click" />
			<input type="button" onclick="f_viewTemporaryTotalScore();" value="View Temporary TotalScore Certificate" />
			<hr>
		
			<%}%> 
			
			
			<div><strong>Q-Keywords:</strong>
				<asp:TextBox ID="q_keywords_tb" runat="server" TextMode="SingleLine" Width="80%"></asp:TextBox>
			</div>
			<div><STRONG>Keyword:</STRONG>
				<asp:DropDownList id="keyword_dd" runat="server" onchange="insertKeyword( document.getElementById('cond_ed'), this);"></asp:DropDownList>&nbsp;&lt;&lt;&lt;=== 
				s<EM>elect to insert.  NOTE THAT ALL Q-Keywords HAVE VALUE OF 0 HERE UNLESS SPECIFIED ABOVE</EM>&nbsp;
			</div>
			<div>
			Borrower Credit Mode <asp:DropDownList ID="m_ddlBorrowerCreditMode" runat="server"></asp:DropDownList>
			</div>
			<DIV>
				<asp:TextBox id="cond_ed" runat="server" TextMode="MultiLine" Width="100%" Height="149px"></asp:TextBox></DIV>
			<asp:TextBox id="condRes_ed" runat="server" TextMode="MultiLine" Width="100%" Height="45px"></asp:TextBox>
			<HR>
			<div style="COLOR: red">
				<P>&nbsp;
					<ml:EncodedLiteral id="ErrorMsg" Runat="server"></ml:EncodedLiteral></P>
				<P></P>
			</div>

			<hr>
			<input type="button" value="View Raw XML" onclick="f_viewRawXML();">
			<input type="button" value="View Liabilities" onclick="f_viewLiabilities();">
			<input type="button" value="View Public Records" onclick="f_viewPublicRecords();">
			<hr>
			<div id="MainDiv" style="BORDER-RIGHT:1px inset; BORDER-TOP:1px inset; OVERFLOW:visible; BORDER-LEFT:1px inset; BORDER-BOTTOM:1px inset">
				<ml:PassthroughLiteral id="MainHtml" runat="server"></ml:PassthroughLiteral>
			</div>
			<hr>
			<input type="button" value="View Raw XML" onclick="f_viewRawXML();">
			<input type="button" value="View Liabilities" onclick="f_viewLiabilities();">
			<input type="button" value="View Public Records" onclick="f_viewPublicRecords();">
		</form>
	</body>
</HTML>
