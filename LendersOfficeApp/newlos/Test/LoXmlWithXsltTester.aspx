﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoXmlWithXsltTester.aspx.cs" Inherits="LendersOfficeApp.newlos.Test.LoXmlWithXsltTester" ValidateRequest="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
        Xml for LoXml: <asp:TextBox ID="LoXmlElement" runat="server" TextMode="MultiLine"></asp:TextBox> <br />
        <asp:Button runat="server" ID="LoXmlExportBtn" Text="Export Above From Loan as Xml" OnClick="ClickExport" /> <br />
        Xml: <asp:TextBox ID="NormalXmlElement" runat="server" TextMode="MultiLine"></asp:TextBox> <br /> 
        Xsl: <asp:TextBox ID="XslElement" runat="server" TextMode="MultiLine"></asp:TextBox><br />
        [Args]:<asp:TextBox runat="server" ID="XsltArgumentsElement" TextMode="MultiLine"></asp:TextBox> (optional, json key value pairs of extra xslt arguments.)<br />
        <asp:Button runat="server" ID="TransformButton" Text="Transform!" OnClick="ClickTransform"/> <br />
        Result:<asp:TextBox runat="server" ID="ResultantXmlElement" TextMode="MultiLine"></asp:TextBox> <br />
        <asp:Button runat="server" ID="ExportAndConvertBtn" Text="Do both!" OnClick="ClickExportAndConvert" /> <br />
        Error: <asp:TextBox runat="server" ID="ErrorStatus" TextMode="MultiLine"></asp:TextBox>
    </form>
</body>
</html>
