<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>

<%@ Page Language="c#" CodeBehind="FannieAddendum.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FannieAddendum" %>

<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="lqb" TagName="FannieAddendumSections" Src="~/newlos/FannieAddendum/FannieAddendumSections.ascx" %>
<%@ Import Namespace="LendersOffice.Security" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>FannieAddendum</title>
    <style type="text/css">
        .CreditReferenceNumber {
            font-weight: bold;
        }

        .CopyButton {
            width: 50px;
        }

        .WarningText {
            color: red;
        }

        .Hidden {
            display: none;
        }

        .WarningIcon {
            height: 25px;
            vertical-align: bottom;
        }

        td {
            padding: 3px;
            vertical-align: top;
            white-space: nowrap;
        }
        
        .section {
            min-width: 540px;
            width: 49%;
            border-collapse: collapse;
            display: inline-block;
            padding: 0;
            margin: 3px 5px;
            border: thin black groove;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        .originator-id-label {
            display: inline-block;
            width: 160px;
        }

        body {
            background-color: gainsboro;
            overflow-y: scroll;
        }

        .readonly {
            background-color: lightgrey;
        }

        .tooltip {
            position: relative;
            display: inline-block;
            border-bottom: 1px dotted black;
            text-decoration: none;
            cursor: pointer;
        }

        .tooltip .tooltiptext {
            width: 500px;
            background-color: white;
            text-align: center;
            border: 1px solid black;
            color: black;
            padding: 2px;

            position: absolute;
            z-index: 1;
            top: -3px;
            left: 105%;
        }

        .invisible {
            visibility: hidden;
        }

        fieldset {
            padding: 0;
            margin: 0;
            border: 0;
        }

        fieldset legend
        {
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            width: 100%;
            padding: 3px 3px 3px 5px;
        }

        .field-container {
            padding: 3px 3px 3px 5px;
        }

        .field-container > .left-column
        {
            display: inline-block;
            width: 30%;
            min-width: 120px;
            vertical-align: top;
        }

        #AdditionalVaInformation .field-container > .left-column
        {
            width: 50%;
            min-width: 200px;
        }
        
        .field-container > input, .field-container > select{
            vertical-align: top;
        }

        .checkbox-label {
            vertical-align: bottom;
        }

        .content-container {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-flow: row wrap;
            -webkit-flex-flow: row wrap;
            flex-flow: row wrap;
            margin-top: 22px;
        }

        .section-interior {
            display: flex; 
            -ms-flex-direction: column;
            -webkit-flex-direction:column;
             flex-direction:column;
        }

        .indented-borrower-name {
            margin-left: 22px;
            width: 150px;
            display: inline-block;
        }

        .divider {
            height: 10px;
        }

        #sDUDownPmtSrcs td:nth-child(1), #sDuThirdPartyProviders td:nth-child(1) {
            width: 20%;
        }

        #sDUDownPmtSrcs td:nth-child(2) {
            width: 50%;
        }

        #sDUDownPmtSrcs td:nth-child(3) {
            width: 25%;
        }

        #sDUDownPmtSrcs, #sDuThirdPartyProviders {
            width: 100%;
        }

        #sDUDownPmtSrcs td > input[type="text"], #sDuThirdPartyProviders td > input[type="text"], #sDuThirdPartyProviders td > select  {
            width: 100%;
            display: block;
        }

        #sDUDownPmtSrcs td > div > input {
            width: 95%;
        }

        .MainRightHeader {
            position: fixed;
            width: 100%;
            top: 0;
        }

        input.ng-invalid, select.ng-invalid {
            background-color: pink;
        }

        input[type=text]:not([readonly]), select:not(.readonly):not([disabled]) {
            background-color: white !important; 
        }

        #sCommunitySecondsRepaymentStructureT {
            max-width: 360px;
        }

        .ui-tooltip {
            background-color: white;
        }

        #contentIframe td {
            white-space: normal;
        }

        #btnGetStatusDo, #btnGetStatusDu {
            width: 350px;
            margin-right: 30px;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        var seamlessFolder = gVirtualRoot + '/newlos/SeamlessDU';

    function copyToClipboard(str) {
        copyStringToClipboard(str);
    }
    function f_displayMask(bDisplay) {

        f_displayBlockMask(bDisplay);
        parent.treeview.f_displayBlockMask(bDisplay);
        parent.info.f_displayBlockMask(bDisplay);

        parent.header.f_disableCloseLink(bDisplay);
        var contentIframe = document.getElementById('contentIframe');
        contentIframe.style.display = bDisplay ? '' : 'none';

        if (bDisplay) {
            document.getElementById('WaitingMessage').innerText = "This loan editor has been disabled to prevent any accidental changes to the file during submission to " + (g_bIsDo ? "DO" : "DU") + ". The loan editor will be re-enabled after you close the " + (g_bIsDo ? "DO" : "DU") + " window.";
      
            var width = 400;
            var height = 250;
            contentIframe.style.width = width + 'px';
            contentIframe.style.height = height + 'px';
        
            var left = f_getScrollLeft() + (f_getViewportWidth() - width) / 2;
            var top = f_getScrollTop() + (f_getViewportHeight() - height) / 2;
        
            contentIframe.style.position = 'absolute';
            contentIframe.style.left = left + 'px';
            contentIframe.style.top = top + 'px';
        }
          
    }
  
    function _init() {  
        registerPostSaveMeCallback(_postSaveCallback);
    
        <% if (!m_enabledDUExport && !BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanExportLoan))
        { %>
            <%=AspxTools.JsGetElementById(btnExport) %>.disabled = true;
            jQuery('#errMsg').show();
        <% } %>
    
        <% if (IsLeadPage && !Broker.IsExpandedLeadEditorDoEnabled && m_enabledDUExport)
            { %>
          document.getElementById('btnSubmitDoDu').disabled = true;
          document.getElementById('btnGetStatusDo').disabled = true;
        <% }
            else if (IsLeadPage && !Broker.IsExpandedLeadEditorDuEnabled && m_enabledDUExport)
            { %>
        document.getElementById('btnSubmitDu').disabled = true;
        document.getElementById('btnSubmitDuSeamless').disabled = true;
          document.getElementById('btnGetStatusDu').disabled = true;
        <% } %>

        <% if (RequestHelper.GetSafeQueryString("mode") == "exportdu")
        { %>

        var btnSubmitDoDu = document.getElementById('btnSubmitDoDu');
        if (!ML.CanRunDo && btnSubmitDoDu) {
            btnSubmitDoDu.disabled = true;
        }

        var btnSubmitDu = document.getElementById('btnSubmitDu')
        if (!ML.CanRunDu && btnSubmitDu) {
            btnSubmitDu.disabled = true;
        }

        var btnSubmitDuSeamless = document.getElementById('btnSubmitDuSeamless')
        if (!ML.CanRunDu && btnSubmitDuSeamless) {
            btnSubmitDuSeamless.disabled = true;
        }

        if (ML.CanRunDo) {
            $("#WarningIconDo").css("visibility", "hidden");
        } else {
            $("#WarningIconDo").attr('title', ML.RunDoDenialReason);
        }

        if (ML.CanRunDu) {
            $("#WarningIconDu, #WarningIconDuSeamless").css("visibility", "hidden");
        } else {
            $("#WarningIconDu, #WarningIconDuSeamless").attr('title', ML.RunDuDenialReason);
        }
        <% } %>
  }

  function exportFannie() {
      <% if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanExportLoan))
        { %>   
      if (isDirty()) {
          if (saveMe() == false) {
              return;
          }
      }
        
      self.location="FannieAddendumVerification.aspx?loanid=<%=AspxTools.JsStringUnquoted(LoanID.ToString())%>&appid=<%=AspxTools.JsStringUnquoted(ApplicationID.ToString())%>&version=<%=AspxTools.JsStringUnquoted(DateTime.Now.Ticks.ToString())%>";          
    <% } %>
  }

<% if (m_enabledDUExport)
        { %> 
        var g_fnmaWindow = null;
        var g_isFnmaLogin = true;
        var g_pollingInterval = 1000;
        var g_bIsDo;
        var g_sUserName;
        var g_sDuCaseId;
        var g_sPassword;
        var g_globallyUniqueIdentifier;
        var g_sIsImport1003 = false;
        var g_sIsImportDuFindings = false;
        var g_sIsImportCreditReport = false;
        var g_bHasAutoLogin = false;

        function f_getStatusDUDO(sIsDo) {
            if (isDirty())
                saveMe();
      
            var args = new Object();
      
            showModal('/newlos/FannieMaeCasefileStatus.aspx?loanid=<%=AspxTools.JsStringUnquoted(LoanID.ToString())%>&isdo=' + sIsDo, args, null, null, null,{ hideCloseButton: true });

}
function f_exportDUDO(sIsDo) {
    if (isDirty()) {
        if (saveMe() == false) {
            return;
        }
    }
    
    var args = new Object();
    showModal('/newlos/FannieMaeExportLogin.aspx?loanid=<%=AspxTools.JsStringUnquoted(LoanID.ToString())%>&isdo=' + sIsDo, args, null, null, function (args) {
        if (args.OK) {
            g_bIsDo = args.bIsDo;
            g_sUserName = args.sUserName;
            g_sPassword = args.sPassword;
            g_sDuCaseId = args.sDuCaseId;
            g_bHasAutoLogin = args.bHasAutoLogin;
            g_fnmaWindow = f_launchDODU(args.CacheId);
            g_fnmaWindow.focus();
            f_displayMask(true);
            f_detectFnmaWindowStatus();
        }
    }, { hideCloseButton: true });
}
function f_setFnmaLogin(bValue) {
    g_isFnmaLogin = bValue;
}


function f_detectFnmaWindowStatus() {
    if (null != g_fnmaWindow && g_fnmaWindow.closed) {
        f_displayMask(false);
        f_fnmaDownloadOptions(g_bIsDo, g_sUserName, g_sPassword, g_sDuCaseId, g_bHasAutoLogin);

        g_fnmaWindow = null;
        return;
    }
    window.setTimeout(f_detectFnmaWindowStatus, g_pollingInterval);
}

function f_launchDODU(sCacheId) {
    var bMaximize = true;
    var bCenter = false;

    var w = 0;
    var h = 0;
    var left = 0;
    var top = 0;

    if (!bMaximize) {
        var preferedWidth = 1024;
        var preferedHeight = 768;
        w = preferedWidth < screen.width ? preferedWidth : screen.width - 10;
        h = preferedHeight < screen.height ? preferedHeight : screen.height - 60;
    } else {
        w = screen.availWidth - 10;
        h = screen.availHeight - 50;
    }

    var w = window.open('<%=AspxTools.JsStringUnquoted(VirtualRoot)%>/newlos/Services/LaunchDODU.aspx?id=' + sCacheId, 'ExportLogin_<%=AspxTools.JsStringUnquoted(LoanID.ToString("N"))%>', 'width=' + w + ',height=' + h + ',left=' + left + ',top=' + top + ',menu=no,status=yes,location=no,resizable=yes,scrollbars=yes');
    if (w != null)
    {
        w.focus();
    }
    return w;
}
function f_fnmaDownloadOptions(bIsDo, sUserName, sPassword, sDuCaseId, bHasAutoLogin) {
    var args = new Object();
    args.bIsDo = bIsDo;
    args.sUserName = sUserName;
    args.sPassword = sPassword;
    args.sDuCaseId = sDuCaseId;
    args.bHasAutoLogin = bHasAutoLogin;
    args.ApplicationID = <%=AspxTools.JsString(ApplicationID)%>;
    <%=AspxTools.JsGetElementById(this.FannieMaeAddendumSections.sDuCaseId)%>.value = sDuCaseId;
    showModal('/newlos/FannieDownloadOptions.aspx?loanid=<%=AspxTools.JsStringUnquoted(LoanID.ToString())%>', args, null, null, null,{ hideCloseButton: true });
}

function f_viewDuFindings() {
    var report_url = "<%=AspxTools.JsStringUnquoted(VirtualRoot)%>/newlos/services/ViewDUFindingFrame.aspx?loanid=<%=AspxTools.JsStringUnquoted(RequestHelper.LoanID.ToString())%>";
    var win = window.open(report_url, "report", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=780,height=600") ;
    win.focus() ;
    
    return false;
}

<% } %>
    
        $(function($) {
            $('#sLT, #sFannieFips, #sFannieFipsLckd, #sIsCommunityLending, #sFannieCommunityLendingT').change(checkValidation);
        });

   function checkValidation()
   {
       var pageValid = FannieAddendumSectionsValidate();

       $('#btnSubmitDoDu').prop('disabled', !(ML.CanRunDo && pageValid));
       $('#btnSubmitDu, #btnSubmitDuSeamless').prop('disabled', !(ML.CanRunDu && pageValid));
       $('#btnExport').prop('disabled', !pageValid);
   }

   function _postSaveMe(result) {
       if (result.RefreshNavigation && parent && parent.treeview) {
           parent.treeview.location.reload();
           window.setTimeout(function() {
               if (parent.treeview && typeof parent.treeview.selectPageID === 'function') {
                   parent.treeview.selectPageID(<%= AspxTools.JsString(this.PageID) %>);
                    }
            }, 500);
        }
   }

        function openDuSeamless() {
            if (isDirty())
            {
                if (ConfirmSaveNoCancel())
                {
                    f_saveMe();
                }
            }
            LQBPopup.Show(ML.VirtualRoot + '/newlos/SeamlessDU/SeamlessDu.aspx#/audit?loanid='+ ML.sLId, { height: 800, width: '80%', hideCloseButton: true });
        }
    </script>

    <form id="FannieAddendum" method="post" runat="server">
        <div class="MainRightHeader">Fannie Mae Addendum</div>
        <lqb:FannieAddendumSections id="FannieMaeAddendumSections" runat="server"></lqb:FannieAddendumSections>
        <div style="margin: 3px 5px">
            <% if (m_enabledDUExport)
                { %>
                <div>
                    <input type="button" value="Submit to Desktop Originator ..." onclick="f_exportDUDO('t');" id="btnSubmitDoDu" class="ButtonStyle" notforedit nohighlight style="width: 250px" />
                    <img src="../../images/warning25x25.png" id="WarningIconDo" class="WarningIcon" />
                    <input type="button" value="Submit to DU (Seamless) ..." onclick="openDuSeamless();" id="btnSubmitDuSeamless" class="ButtonStyle" notforedit nohighlight style="width: 250px" runat="server" />
                    <img src="../../images/warning25x25.png" id="WarningIconDuSeamless" class="WarningIcon" />
                    <input type="button" value="Submit to Desktop Underwriter ..." onclick="f_exportDUDO('f');" id="btnSubmitDu" class="ButtonStyle" notforedit nohighlight style="width: 250px" runat="server" />
                    <img src="../../images/warning25x25.png" id="WarningIconDu" class="WarningIcon" />
                    <input type="button" value="View Findings ..." onclick="f_viewDuFindings();" id="btnViewFindings" class="ButtonStyle" alwaysenable="true" notforedit nohighlight style="width: 150px"/>
                </div>
            <br />
                <div>
                    <input type="button" value="Get Desktop Originator Casefile Status..." onclick="f_getStatusDUDO('t');" id="btnGetStatusDo" class="ButtonStyle" alwaysenable="true" notforedit nohighlight />
                    <input type="button" value="Get Desktop Underwriter Casefile Status..." onclick="f_getStatusDUDO('f');" id="btnGetStatusDu" class="ButtonStyle" alwaysenable="true" notforedit nohighlight />
            <% } else { %>
            <div>
            <% } %>
                <input type="button" value="Next" onclick="exportFannie();" id="btnExport" class="ButtonStyle" alwaysenable="true" runat="server">
            </div>

            <div id="errMsg" style="display: none"><font color="red">You do not have permission to export loan.</font></div>
        </div>
        <div id="LoanHeaderPopup">
            <div id="LoanHeaderPopupText">&nbsp;</div>
        </div>
        <div style="border-right: black 2px solid; padding-right: 10px; border-top: black 2px solid; display: none; padding-left: 10px; z-index: 300; padding-bottom: 10px; border-left: black 2px solid; padding-top: 10px; border-bottom: black 2px solid; background-color: gainsboro" id="contentIframe">
            <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top">
                        <div style="font-weight: bold; font-size: 14px">This loan editor has been disabled.</div>
                        <br>
                        <div id="WaitingMessage" style="font-size: 12px"></div>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" height="100%" style="font-size: 12px"><ml:PassthroughLiteral ID="CreditReferenceText" runat="server"></ml:PassthroughLiteral></td>
                </tr>
            </table>
        </div>
        <input type="hidden" id="ClipboardText">
        </div>
    </form>
</body>
</html>
