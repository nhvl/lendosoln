<%@ Import namespace="LendersOffice.Common" %>
<%@ Import namespace="LendersOffice.Security" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page language="c#" Codebehind="FannieAddendumVerification.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FannieAddendumVerification" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>FannieAddendumVerification</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="<%=AspxTools.HtmlString(StyleSheet)%>" type=text/css rel=stylesheet>

  </HEAD>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro">
	<script type="text/javascript">
function f_displayMask(bDisplay) {

    f_displayBlockMask(bDisplay);
    parent.treeview.f_displayBlockMask(bDisplay);
    parent.info.f_displayBlockMask(bDisplay);

    parent.header.f_disableCloseLink(bDisplay);
		var contentIframe = document.getElementById('contentIFrame');
		contentIframe.style.display = bDisplay ? '' : 'none';

    if (bDisplay) {
      document.getElementById('WaitingMessage').innerText = "LendingQB is waiting for the " + (g_bIsDo ? "DO" : "DU") + " window to be closed to proceed.";
      
  		var width = 300;
	  	var height = 150;
  		contentIframe.style.width = width + 'px';
		  contentIframe.style.height = height + 'px';
		
		  var left = f_getScrollLeft() + (f_getViewportWidth() - width) / 2;
		  var top = f_getScrollTop() + (f_getViewportHeight() - height) / 2;
		
		  contentIframe.style.position = 'absolute';
		  contentIframe.style.left = left + 'px';
		  contentIframe.style.top = top + 'px';
		}
	      
}

function f_exportFannie() {
  <% if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanExportLoan)) { %>  
    window.open("<%=AspxTools.JsStringUnquoted(VirtualRoot)%>/los/underwriting/FannieExport.aspx?loanid=<%=AspxTools.JsStringUnquoted(LoanID.ToString())%>&version=<%=AspxTools.JsStringUnquoted(RequestHelper.GetSafeQueryString("version"))%>","_parent");   
  <% } %>
}
function f_goBack() {
    self.location = "FannieAddendum.aspx?loanid=<%=AspxTools.JsStringUnquoted(LoanID.ToString())%>&appid=" + encodeURIComponent(parent.info.f_getCurrentApplicationID());
}
</script>

    <form id="FannieAddendumVerification" method="post" runat="server">
<table id=Table1 cellspacing=0 cellpadding=0 border=0 width="100%">
  <tr>
    <td class=MainRightHeader style="PADDING-LEFT:5px">Fannie Mae Verification</td></tr>
  <tr>
    <td style="PADDING-LEFT:5px;PADDING-TOP:5px">
      <input id=btnExport type=button value="Download FNM 3.2 file ..." onclick="f_exportFannie();" notforedit class="ButtonStyle" AlwaysEnable="true" style="width:190px">&nbsp;&nbsp;
      <input type=button value=Back onclick="f_goBack();" notforedit class="ButtonStyle" AlwaysEnable="true">&nbsp;&nbsp;
    </td></tr>
  <tr><td><hr></td></tr>
  <tr>
    <td style="PADDING-LEFT:5px"><ml:PassthroughLiteral id="m_msg" runat="server" enableviewstate="False"></ml:PassthroughLiteral></td></tr></table>
        <div style="z-index:300; display:none;border:2px black solid;background-color:gainsboro" id="contentIframe">
      <table width="100%" height="100%" cellpadding=0 cellspacing=0>
      <tr><td valign="center" align="center"" style="font-weight:bold;color:black;font-size:14px;">
      <div id="WaitingMessage"></div>
        </td></tr>
      </table>
        </div>
     </form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
  </body>
</HTML>
