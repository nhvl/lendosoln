namespace LendersOfficeApp.newlos
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using DataAccess.FannieMae;
    using LendersOffice.Common;
    using LendersOffice.CreditReport;
    using LendersOffice.ConfigSystem.Operations;
    using System.Linq;
    using MeridianLink.CommonControls;
    using LendersOffice.AntiXss;
    using LendersOffice.Security;

    public partial class FannieAddendum : BaseLoanPage
    {
        protected bool m_enabledDUExport
        {
            get
            {
                return RequestHelper.GetSafeQueryString("mode") == "exportdu";
            }
        }

        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return new WorkflowOperation[]
            {
                WorkflowOperations.RunDo,
                WorkflowOperations.RunDu
            };
        }

        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[]
            {
                WorkflowOperations.RunDo,
                WorkflowOperations.RunDu
            };
        }

        protected bool CanRunDo
        {
            get
            {
                return this.UserHasWorkflowPrivilege(WorkflowOperations.RunDo);
            }
        }

        protected bool CanRunDu
        {
            get
            {
                return this.UserHasWorkflowPrivilege(WorkflowOperations.RunDu);
            }
        }

        protected override void LoadData()
        {
            this.FannieMaeAddendumSections.EnabledDUExport = this.m_enabledDUExport;

            if (m_enabledDUExport)
            {
                this.CreditReferenceText.Text = CreditReportUtilities.RetrieveCreditReferenceDataPerApplication(LoanID);
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(FannieAddendum));
            dataLoan.InitLoad();
            this.FannieMaeAddendumSections.LoadData(dataLoan);
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.PageTitle = "Fannie Mae Addendum";
            if (RequestHelper.GetSafeQueryString("mode") == "exportdu")
            {
                this.PageID = "ExportDu";
            }
            else
            {
                this.PageID = "FannieAddendum";
            }

            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterJsScript("widgets/jquery.downpayment.js");
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterCSS("jquery-ui-1.11.custom.css");

            this.RegisterJsScript("geocode.js");
            this.RegisterService("geocode", "/newlos/GeoCodeService.aspx");

            this.RegisterJsScript("angular-1.5.5.min.js");

            RegisterJsObjectWithJsonNetAnonymousSerializer("downpaymentSources", Tools.Options_sDwnPmtSrc);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
            // OPM 245347 - Workflow restrictions for running DO/DU.
            RegisterJsGlobalVariables("CanRunDo", this.CanRunDo);
            RegisterJsGlobalVariables("CanRunDu", this.CanRunDu);

            RegisterJsGlobalVariables("RunDoDenialReason", this.GetReasonUserCannotPerformPrivilege(WorkflowOperations.RunDo));
            RegisterJsGlobalVariables("RunDuDenialReason", this.GetReasonUserCannotPerformPrivilege(WorkflowOperations.RunDu));

            var duSeamlessSettings = Broker.GetSeamlessDuSettings();
            btnSubmitDu.Visible = !duSeamlessSettings.SeamlessDuEnabled || duSeamlessSettings.IsDuFallbackEnabled(PrincipalFactory.CurrentPrincipal);
            btnSubmitDuSeamless.Visible = duSeamlessSettings.SeamlessDuEnabled;
        }
    }
}
