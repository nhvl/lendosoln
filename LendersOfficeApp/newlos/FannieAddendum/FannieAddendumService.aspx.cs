namespace LendersOfficeApp.newlos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using DataAccess;
    using DataAccess.FannieMae;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Migration;

    /// <summary>
    /// Provides background service calls for saving and updating the <see cref="FannieAddendumSections"/> fields.
    /// </summary>
    /// <remarks>
    /// This is a PML copy of \LendOSoln\LendersOfficeApp\newlos\FannieAddendum\FannieAddendumService.aspx.cs and both should be kept synchronized.
    /// </remarks>
    public class FannieAddendumServiceItem : AbstractBackgroundServiceItem 
    {
        private E_sDisclosureRegulationT disclosureRegulationTBeforeBind;
        private E_sDisclosureRegulationT disclosureRegulationTOnLoad;

        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FannieAddendumServiceItem));
        }

        /// <summary>
        /// Defines a data class for parsing JSON from the UI related to first-time homebuyer counseling.
        /// </summary>
        [DataContract]
        private class AppCounseling
        {
            [DataMember]
            public E_aTotalScoreFhtbCounselingT aBTotalScoreFhtbCounselingT = E_aTotalScoreFhtbCounselingT.LeaveBlank;
            [DataMember]
            public E_aTotalScoreFhtbCounselingT aCTotalScoreFhtbCounselingT = E_aTotalScoreFhtbCounselingT.LeaveBlank;

            public AppCounseling()
            { }
        }

        /// <summary>
        /// Defines a data class for parsing JSON from the UI related to app-level present housing info.
        /// </summary>
        [DataContract]
        private class PresentHousing
        {
            [DataMember]
            public string aPresTotHExpDesc = string.Empty;
            [DataMember]
            public string aPresTotHExpCalc = string.Empty;
            [DataMember]
            public bool aPresTotHExpCalcLckd = false;

            public PresentHousing()
            { }
        }

        /// <summary>
        /// Defines a data class for parsing JSON from the UI related to VA taxes.
        /// </summary>
        [DataContract]
        private class VaTaxes
        {
            [DataMember]
            public string aVaBFedITax = string.Empty;
            [DataMember]
            public string aVaBStateITax = string.Empty;
            [DataMember]
            public string aVaBSsnTax = string.Empty;
            [DataMember]
            public string aVaBOITax = string.Empty;
            [DataMember]
            public string aVaCFedITax = string.Empty;
            [DataMember]
            public string aVaCStateITax = string.Empty;
            [DataMember]
            public string aVaCSsnTax = string.Empty;
            [DataMember]
            public string aVaCOITax = string.Empty;

            public VaTaxes()
            { }
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            this.disclosureRegulationTBeforeBind = dataLoan.sDisclosureRegulationT;
            
            dataLoan.sLT = (E_sLT)GetInt("sLT");
           
            dataApp.a1003SignD_rep = GetString("a1003SignD");

            dataLoan.sFannieARMPlanNum_rep = GetString("sFannieARMPlanNum");
            dataLoan.sFannieARMPlanNumLckd = GetBool("sFannieARMPlanNumLckd");

            dataLoan.sHmdaMsaNum = GetString("sHmdaMsaNum");
            dataLoan.sHmdaCountyCode = GetString("sHmdaCountyCode");
            dataLoan.sHmdaStateCode = GetString("sHmdaStateCode");
            dataLoan.sHmdaCensusTract = GetString("sHmdaCensusTract");
            dataLoan.sFannieFips = GetString("sFannieFips");
            dataLoan.sFannieFipsLckd = GetBool("sFannieFipsLckd");
            if (dataLoan.sLT == E_sLT.FHA)
            {
                dataLoan.sFHAHousingActSection = GetString("sFHAHousingActSection");
                dataLoan.sFhaLenderIdT = GetEnum<E_sFhaLenderIdT>("sFhaLenderIdT");
                dataLoan.sFHALenderIdCode = GetString("sFHALenderIdCode");
                dataLoan.sFHASponsorAgentIdCode = GetString("sFHASponsorAgentIdCode");
                dataLoan.sFHASellerContribution_rep = GetString("sFHASellerContribution");
                var appsCounselingTypes = SerializationHelper.JsonNetDeserialize<List<AppCounseling>>(GetString("appCounseling"));
                for (int i = 0; i < dataLoan.nApps; i++)
                {
                    CAppData currentApp = dataLoan.GetAppData(i);
                    currentApp.aBTotalScoreFhtbCounselingT = appsCounselingTypes[i].aBTotalScoreFhtbCounselingT;
                    currentApp.aCTotalScoreFhtbCounselingT = appsCounselingTypes[i].aCTotalScoreFhtbCounselingT;
                }
            }
            else if (dataLoan.sLT == E_sLT.VA)
            {
                dataLoan.GetAppData(0).aVaEntitleAmt_rep = GetString("aVaEntitleAmt");
                dataLoan.sVaProMaintenancePmt_rep = GetString("sVaProMaintenancePmt");
                dataLoan.sVaProUtilityPmt_rep = GetString("sVaProUtilityPmt");
                dataLoan.sFHASellerContribution_rep = GetString("sFHASellerContributionVa");
                var vaTaxes = SerializationHelper.JsonNetDeserialize<List<VaTaxes>>(GetString("vaTaxes"));
                for (int i = 0; i < dataLoan.nApps; i++)
                {
                    CAppData currentApp = dataLoan.GetAppData(i);
                    currentApp.aVaBFedITax_rep = vaTaxes[i].aVaBFedITax;
                    currentApp.aVaBStateITax_rep = vaTaxes[i].aVaBStateITax;
                    currentApp.aVaBSsnTax_rep = vaTaxes[i].aVaBSsnTax;
                    currentApp.aVaBOITax_rep = vaTaxes[i].aVaBOITax;
                    currentApp.aVaCFedITax_rep = vaTaxes[i].aVaCFedITax;
                    currentApp.aVaCStateITax_rep = vaTaxes[i].aVaCStateITax;
                    currentApp.aVaCSsnTax_rep = vaTaxes[i].aVaCSsnTax;
                    currentApp.aVaCOITax_rep = vaTaxes[i].aVaCOITax;
                }
            }

            dataLoan.sIsSellerProvidedBelowMktFin = GetBool("sIsSellerProvidedBelowMktFin") ? "Y" : "N" ;
            dataLoan.sArmIndexT = (E_sArmIndexT)GetInt("sArmIndexT");
            dataLoan.sArmIndexTLckd = GetBool("sArmIndexTLckd");
            dataLoan.sFannieSpT = (E_sFannieSpT)GetInt("sFannieSpT");
            dataLoan.sFannieDocT = (E_sFannieDocT)GetInt("sFannieDocT");
            dataLoan.sHomeIsMhAdvantageTri = this.GetEnum<E_TriState>("sHomeIsMhAdvantageTri");
            dataLoan.sDuCaseId = GetString("sDuCaseId");
            dataLoan.sDuLenderInstitutionId = GetString("sDuLenderInstitutionId", dataLoan.sDuLenderInstitutionId);
            dataLoan.sIsCommunityLending = GetBool("sIsCommunityLending");
            dataLoan.sFannieMsa = GetString("sFannieMsa");
            dataLoan.sFannieCommunityLendingT = (E_sFannieCommunityLendingT) GetInt("sFannieCommunityLendingT");
            dataLoan.sFannieCommunitySecondsRepaymentStructureT = (E_sFannieCommunitySecondsRepaymentStructureT)GetInt("sCommunitySecondsRepaymentStructureT");
            dataLoan.sFannieHomebuyerEducationT = (E_FannieHomebuyerEducation)GetInt("sFannieHomebuyerEducationT");
            dataLoan.sIsCommunitySecond = GetBool("sIsCommunitySecond");
            dataLoan.sFannieIncomeLimitAdjPc_rep = GetString("sFannieIncomeLimitAdjPc");
            dataLoan.sFannieProdDesc = GetString("sFannieProdDesc");
            dataLoan.sFannieProductCode = GetString("sFannieProductCode");
            dataLoan.sSpProjectClassFannieT = GetEnum<E_sSpProjectClassFannieT>("sSpProjectClassFannieT");


            dataLoan.sExportAdditionalLiabitiesFannieMae = GetBool("sExportAdditionalLiabitiesFannieMae", dataLoan.sExportAdditionalLiabitiesFannieMae);
            dataLoan.sExportAdditionalLiabitiesDODU = GetBool("sExportAdditionalLiabitiesDODU", dataLoan.sExportAdditionalLiabitiesDODU);

            dataLoan.sIsUseDUDwnPmtSrc = GetBool("sIsUseDUDwnPmtSrc");
            var downpaymentSource = dataLoan.sDUDwnPmtSrc;
            downpaymentSource.Import(GetString("sDUDwnPmtSrc"));
            dataLoan.sDUDwnPmtSrc = downpaymentSource;

            dataLoan.sDuThirdPartyProviders = FannieMaeThirdPartyProviderList.ParseFromJson(GetString("duThirdPartyProviders"));

            dataApp.a1003InterviewD_rep = GetString("a1003InterviewD") ;
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V17_UpdateApplicationSubmittedCalculation))
            {
                dataApp.a1003InterviewDLckd = GetBool("a1003InterviewDLckd");
            }

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V18_ConsolidateTaxIdFields))
            {
                dataLoan.sSponsoredOriginatorEIN = GetString("sSponsoredOriginatorEIN");
                dataLoan.sFhaSponsoredOriginatorEinLckd = GetBool("sFhaSponsoredOriginatorEinLckd");
            }

            var presentHousingItems = ObsoleteSerializationHelper.JsonDeserialize<List<PresentHousing>>(GetString("presentHousing"));
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData currentApp = dataLoan.GetAppData(i);
                currentApp.aPresTotHExpDesc = presentHousingItems[i].aPresTotHExpDesc;
                currentApp.aPresTotHExpCalc_rep = presentHousingItems[i].aPresTotHExpCalc;
                currentApp.aPresTotHExpLckd = presentHousingItems[i].aPresTotHExpCalcLckd;
            }

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            this.disclosureRegulationTOnLoad = dataLoan.sDisclosureRegulationT;

            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData currentApp = dataLoan.GetAppData(i);

                SetResult("aPresTotHExpDesc" + i, currentApp.aPresTotHExpDesc);
                SetResult("aPresTotHExpCalc" + i, currentApp.aPresTotHExpCalc_rep);
                SetResult("aPresTotHExpCalcLckd" + i, currentApp.aPresTotHExpLckd);
            }

            SetResult("sIsUseDUDwnPmtSrc", dataLoan.sIsUseDUDwnPmtSrc);
            SetResult("sDUDwnPmtSrc", dataLoan.sDUDwnPmtSrc.Serialize());

            if (dataLoan.sLT == E_sLT.FHA)
            {
                SetResult("sFHAHousingActSection", dataLoan.sFHAHousingActSection);
            }

            bool refreshNavigation = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                this.disclosureRegulationTBeforeBind != this.disclosureRegulationTOnLoad;
            if (refreshNavigation)
            {
                SetResult("RefreshNavigation", refreshNavigation);
            }
            SetResult("sArmIndexT", dataLoan.sArmIndexT);
            SetResult("sFannieARMPlanNum", dataLoan.sFannieARMPlanNum_rep);

            SetResult("a1003InterviewD", dataApp.a1003InterviewD_rep);
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V17_UpdateApplicationSubmittedCalculation))
            {
                SetResult("a1003InterviewDLckd", dataApp.a1003InterviewDLckd);
            }

            SetResult("sPurchPrice", dataLoan.sPurchPrice.ToString());
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V18_ConsolidateTaxIdFields))
            {
                SetResult("sSponsoredOriginatorEIN", dataLoan.sSponsoredOriginatorEIN);
                SetResult("sFhaSponsoredOriginatorEinLckd", dataLoan.sFhaSponsoredOriginatorEinLckd);
            }

            this.SetResult("sHomeIsMhAdvantageTri", dataLoan.sHomeIsMhAdvantageTri, forDropdown: true);
        }
    }

    public partial class FannieAddendumService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize() 
        {
            AddBackgroundItem("FannieMaeAddendumSections", new FannieAddendumServiceItem());
        }
    }
}
