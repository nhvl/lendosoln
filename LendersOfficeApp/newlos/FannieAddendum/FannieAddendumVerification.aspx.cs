using System;
using System.Collections;
using System.Linq;
using LendersOffice.Conversions;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos
{
    public partial class FannieAddendumVerification : BaseLoanPage
    {
        protected void PageLoad(object sender, System.EventArgs e)
        {
            ArrayList list = null;

            FannieMae32Exporter exporter = new FannieMae32Exporter(LoanID);
            exporter.Export();

            list = exporter.WarningMessages;

            int errorCount = exporter.ErrorMessages.Count();
            int warnCount = exporter.WarningMessages.Count;

            if (errorCount > 0 || warnCount > 0)
            {
                var errorListHtml = new System.Text.StringBuilder();
                errorListHtml.Append("<ul>");
                foreach (string msg in list)
                {
                    errorListHtml.Append("<li> Warning: " + AspxTools.HtmlString(msg));
                }

                foreach (string msg in exporter.ErrorMessages)
                {
                    errorListHtml.Append("<li> ERROR: " + AspxTools.HtmlString(msg));
                }

                errorListHtml.Append("</ul>");
                m_msg.Text = errorListHtml.ToString();
            }
            else
            {
                m_msg.Text = "0 warning, 0 error";
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
