﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FannieAddendumSections.ascx.cs"
    Inherits="LendersOfficeApp.newlos.FannieAddendumSections"
    ClientIDMode="Static" %>

<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.Security" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<%-- Developer Note: This page is replicated by the page at \LendOSoln\PML\webapp\Partials\LoanInformation\FannieAddendumSections.ascx in the TPO portal. Make sure to update both when changing content.--%>

<script type="text/javascript">
    jQuery(function($) {
        $('#sDUDownPmtSrcs').downpayments({
            'downpaymentSources': downpaymentSources, 
            'data' : downPaymentData,
            readonly : $('#_ReadOnly').val() == 'True' || $('#DUDownPaymentSource1003').is(':checked')
        });
    
        $('#DUDownPaymentSource1003').change(function(){
            $('#sDUDownPmtSrcs').downpayments('setReadOnly', true);
        });

        $('#DUDownPaymentSourceJSON').change(function(){
            $('#sDUDownPmtSrcs').downpayments('setReadOnly', false);
        });

        $('#sLT').change(function() {
            var sLT = $(this).val();
            $('#FHAAddendumRow').toggle(sLT === <%=AspxTools.JsString(E_sLT.FHA.ToString("D")) %>);
            $('#AdditionalVaInformation').toggle(sLT === <%=AspxTools.JsString(E_sLT.VA.ToString("D")) %>);
            FannieAddendumSectionsValidate();
        }).change();

        $('#sFannieCommunityLendingT').change(FannieAddendumSectionsValidate).change;
        $('#sFannieFips').change(validateFips).change();
        $('#sFannieFipsLckd').change(onFipsLocked).change();

        $('#sFannieSpT').change(function () {
            onPropertyTypeChange();
            refreshCalculation();
        });

        onPropertyTypeChange();

        <% if (!IsReadOnly)
        { %>
        $('#sIsCommunityLending').change(onClick_sIsCommunityLending).change();
        <% } %>
        <%=AspxTools.JQuery(sFannieARMPlanNumLckd)%>.change(function() {lockField(<%=AspxTools.JQuery(sFannieARMPlanNumLckd)%>, <%=AspxTools.JQuery(sFannieARMPlanNum)%>);}).change();
        <%= AspxTools.JQuery(sArmIndexTLckd) %>.change(function() {lockField(<%= AspxTools.JQuery(sArmIndexTLckd) %>, <%= AspxTools.JQuery(sArmIndexT) %>)}).change();
        
        $("[id$='_aPresTotHExpCalcLckd']")
            .change(function() {
                lockField($(this), $(this).parent().find("[id$='_aPresTotHExpCalc']"))
            }).change();

        <%= AspxTools.JQuery(a1003InterviewDLckd)%>.change(function() {lockField(<%= AspxTools.JQuery(a1003InterviewDLckd)%>, <%= AspxTools.JQuery(a1003InterviewD)%>); }).change();

        var $sFhaSponsoredOriginatorEinLckd = <%= AspxTools.JQuery(sFhaSponsoredOriginatorEinLckd)%>;
        $sFhaSponsoredOriginatorEinLckd.change(function() {
            lockField($sFhaSponsoredOriginatorEinLckd, <%= AspxTools.JQuery(sSponsoredOriginatorEIN)%>);
        }).change();
    });

    var fannieCommunityLendingT_originalIndex = <%=AspxTools.JsString(sFannieCommunityLendingT.SelectedValue)%>;

    function lockField(lockedCheckbox, lockedFieldInput)
    {
        lockedFieldInput.prop('readOnly', !lockedCheckbox.prop('checked')).toggleClass('readonly', !lockedCheckbox.prop('checked'));
        if (lockedFieldInput.is('select'))
        {
            lockedFieldInput.prop('disabled', !lockedCheckbox.prop('checked'));
        }

        if (typeof gService !=='undefined' && typeof gService.loanedit !== 'undefined') 
        {
            refreshCalculation();
        }
    }

    var duValidationApp = angular.module('duValidation', []);
    duValidationApp.factory('thirdPartyProvider', function() {
        thirdPartyProvider = function(Name, ReferenceNumber)
        {
            if (typeof Name === 'undefined') this.Name = '';
            else this.Name = Name;
            if (typeof ReferenceNumber === 'undefined') this.ReferenceNumber = '';
            else this.ReferenceNumber = ReferenceNumber;
        }

        return thirdPartyProvider;
    });

    duValidationApp.controller('duValidationController', function($scope, thirdPartyProvider)
    {
        if (typeof thirdPartyProviders === 'undefined' || !thirdPartyProviders || thirdPartyProviders.length === 0)
        {
            $scope.thirdPartyProviders = [new thirdPartyProvider(), new thirdPartyProvider(), new thirdPartyProvider()];
        }
        else
        {
            $scope.thirdPartyProviders = thirdPartyProviders;
            while ($scope.thirdPartyProviders.length < 3)
            {
                $scope.thirdPartyProviders.push(new thirdPartyProvider());
            }
        }

        if (typeof duApprovedProviders !== 'undefined' && !!duApprovedProviders && duApprovedProviders.length !== 0)
        {
            duApprovedProviders.sort(function(a, b){<%-- By DisplayName, except Placeholder is last --%>
                if (a.ProviderName.toLowerCase() === 'placeholder') {
                    return 1;
                } else if (b.ProviderName.toLowerCase() === 'placeholder') {
                    return -1;
                } else {
                    return a.DisplayName.localeCompare(b.DisplayName);
                }
            });
            $scope.thirdPartyProviderNames = duApprovedProviders;
        }

        $scope.addItem = function() {
            var newProvider = new thirdPartyProvider();
            $scope.thirdPartyProviders.push(newProvider);
        };

        $scope.removeItem = function(index) {
            $scope.thirdPartyProviders.splice(index, 1);
            $scope.onInputChange();
        }

        $scope.onInputChange = function() {
            checkValidation();
            updateDirtyBit();
        }
    });

    function _postGetAllFormValues(args)
    {
        args.sDUDwnPmtSrc = JSON.stringify($j('#sDUDownPmtSrcs').downpayments('serialize'));
        args.sIsUseDUDwnPmtSrc = $j('#DUDownPaymentSourceJSON').is(':checked');
        if (<%=AspxTools.JQuery(sFhaLenderIdT_RegularFhaLender)%>.prop('checked'))
                args.sFhaLenderIdT = <%=AspxTools.JsNumeric(E_sFhaLenderIdT.RegularFhaLender)%>;
            else if (<%=AspxTools.JQuery(sFhaLenderIdT_SponsoredOriginatorEIN)%>.prop('checked'))
                args.sFhaLenderIdT = <%=AspxTools.JsNumeric(E_sFhaLenderIdT.SponsoredOriginatorEIN)%>;
            else args.sFhaLenderIdT = <%=AspxTools.JsNumeric(E_sFhaLenderIdT.NoOriginatorId)%>;
        args.presentHousing = JSON.stringify($j('.presentHousingItem').map(function() {
            return {
                "aPresTotHExpDesc": $j(this).find("[id*='aPresTotHExpDesc']").val(),
                "aPresTotHExpCalc": $j(this).find("[id*='aPresTotHExpCalc']").val(),
                "aPresTotHExpCalcLckd": $j(this).find("[id*='aPresTotHExpCalcLckd']").prop('checked')
            };
        }).get());
        args.appCounseling = JSON.stringify(jQuery('.appCounseling').map(function() 
        { return {
            "aBTotalScoreFhtbCounselingT": $j(this).find("[id*='aBTotalScoreFhtbCounselingT']").val(), 
            "aCTotalScoreFhtbCounselingT": $j(this).find("[id*='aCTotalScoreFhtbCounselingT']").val() }; 
        }).get());
        args.vaTaxes = JSON.stringify(jQuery('.AppTaxes').map(function() 
        { return {
            "aVaBFedITax": $j(this).find("[id*='aVaBFedITax']").val(), 
            "aVaBStateITax": $j(this).find("[id*='aVaBStateITax']").val(), 
            "aVaBSsnTax": $j(this).find("[id*='aVaBSsnTax']").val(), 
            "aVaBOITax": $j(this).find("[id*='aVaBOITax']").val(),
            "aVaCFedITax": $j(this).find("[id*='aVaCFedITax']").val(), 
            "aVaCStateITax": $j(this).find("[id*='aVaCStateITax']").val(), 
            "aVaCSsnTax": $j(this).find("[id*='aVaCSsnTax']").val(), 
            "aVaCOITax": $j(this).find("[id*='aVaCOITax']").val() }; 
        }).get());
        args.duThirdPartyProviders = JSON.stringify(angular.element($j('#duValidation')).scope().thirdPartyProviders.filter(function(provider) { return provider.Name && provider.ReferenceNumber }));
    }

    function _postSaveCallback(values) {
        $j('#DUDownPaymentSource1003')[0].checked = values.sIsUseDUDwnPmtSrc === 'False';
        $j('#DUDownPaymentSourceJSON')[0].checked = values.sIsUseDUDwnPmtSrc === 'True';
        $j('#sDUDownPmtSrcs').downpayments('initialize', JSON.parse(values.sDUDwnPmtSrc) );
        $j("[id$='_aPresTotHExpCalcLckd']").each(function(index) {
            if (!$j(this).prop('checked'))
                $j(this).parent().find("[id$='_aPresTotHExpCalc']").val(values['aPresTotHExpCalc' + index]);
        });
    }

    registerPostRefreshCalculationCallback(_postSaveCallback);
    
    function FannieAddendumSectionsValidate()
    {
        var invalidFields = $j('.ng-invalid');
        var communityLendingValid = ValidateCommunityLending()
        var fipsValid = validateFips();

        return communityLendingValid && invalidFields.length === 0 && validateFips();
    }

    function ValidateCommunityLending()
    {
        var v = <%=AspxTools.JsGetElementById(sLT)%>.value;
        var communityLendingEnabled = <%=AspxTools.JsGetElementById(sIsCommunityLending)%>.checked == true;
        if (!communityLendingEnabled)
        {
            $j('#sDiscontinuedProductImage, #sDiscontinuedProductMessage, #sDisableCL, #enableCommunityLending').hide();
            return true;
        }

        var communityLendingForbiddenForLoanType = (v === <%=AspxTools.JsString(E_sLT.FHA.ToString("D"))%> || v === <%=AspxTools.JsString(E_sLT.VA.ToString("D"))%>);
        var communityLendingProduct = $j('#<%=AspxTools.JsStringUnquoted(sFannieCommunityLendingT.ClientID)%> option:selected').text();
        var communityLendingProductDiscontinued = communityLendingProduct.match('discontinued') != null && <%=AspxTools.JQuery(sFannieCommunityLendingT)%>.val() !== fannieCommunityLendingT_originalIndex;

        $j('#sDiscontinuedProductImage, #sDiscontinuedProductMessage').toggle(communityLendingProductDiscontinued);
        $j('#sDisableCL, #enableCommunityLending').toggle(communityLendingForbiddenForLoanType);

        return !communityLendingForbiddenForLoanType && !communityLendingProductDiscontinued;
    }

    function onClick_sIsCommunityLending()
    {
        <% if (!IsReadOnly)
        { %>
        bEnable = <%=AspxTools.JQuery(sIsCommunityLending)%>.prop('checked');
        <%=AspxTools.JQuery(sFannieMsa, sFannieIncomeLimitAdjPc)%>.prop('readOnly', !bEnable);
        <%=AspxTools.JQuery(sFannieCommunityLendingT, sCommunitySecondsRepaymentStructureT, sFannieHomebuyerEducationT)%>.prop('disabled', !bEnable).toggleClass('readonly', !bEnable);
        <%=AspxTools.JQuery(sIsCommunitySecond)%>.prop('disabled', !bEnable);
        ValidateCommunityLending();
        var v = <%=AspxTools.JQuery(sLT)%>.val();
        if ((v == <%=AspxTools.JsString(E_sLT.FHA.ToString("D"))%> || v == <%=AspxTools.JsString(E_sLT.VA.ToString("D"))%>) && !bEnable)
        {
            resetCommunityLending();
        }
        FannieAddendumSectionsValidate();
        <% } %>
    }

    function resetCommunityLending()
    {
        <%=AspxTools.JQuery(sFannieMsa)%>.val("");
        <%=AspxTools.JsGetElementById(sFannieCommunityLendingT)%>.value = "0";
        <%=AspxTools.JsGetElementById(sIsCommunitySecond)%>.checked = false;
        <%=AspxTools.JsGetElementById(sFannieIncomeLimitAdjPc)%>.value = "0.000%";
    }
    
    function validateFips()
    {
        lockField($j("#sFannieFipsLckd"), $j("#sFannieFips"));
        if ($j("#sFannieFipsLckd").prop('checked'))
        {
            var fipsCode = $j("#sFannieFips");
            if (fipsCode.val().length === 0 || fipsCode.val().length === 11)
            {
                $j("#FipsCodeWarning").hide();
                $j("#FipsCodeInvalid").hide();
                return true;
            }
            else
            {
                $j("#FipsCodeWarning").show();
                $j("#FipsCodeInvalid").show();
                return false;
            }
        }

        return true;
    }

    function onFipsLocked()
    {
        if (<%=AspxTools.JQuery(sFannieFipsLckd)%>.prop('checked'))
        {
            <%=AspxTools.JQuery(sFannieFips)%>.prop('disabled', false);
        }
        else
        {
            $j("#sFannieFips").prop('disabled', true);
            $j("#FipsCodeWarning").hide();
            $j("#FipsCodeInvalid").hide();
            
            // Recalculate the FIPS code from the existing geocode information.
            var stateCode = $j('#sHmdaStateCode').val();
            var countyCode = $j("#sHmdaCountyCode").val();
            var censusTract = $j("#sHmdaCensusTract").val();
            var fipsCode = getFipsCode(stateCode, countyCode, censusTract);
            
            if (fipsCode.length != 11)
            {
                fipsCode = '';
            }
                
            $j("#sFannieFips").val(fipsCode);
        }

        validateFips();
    }

    function fannieAddendum_importGeoCodes()
    {
        return importGeoCodes({
            "sSpAddr": document.getElementById("sSpAddr").value,
            "sSpCity": document.getElementById("sSpCity").value,
            "sSpCounty": document.getElementById("sSpCounty").value,
            "sSpState": document.getElementById("sSpState").value,
            "sSpZip": document.getElementById("sSpZip").value,
            "sHmdaActionD": document.getElementById("sHmdaActionD").value
        });
    }

    function onPropertyTypeChange() {
        var manufacturedTypes = [
            <%=AspxTools.JsString(E_sFannieSpT.Manufactured)%>,
            <%=AspxTools.JsString(E_sFannieSpT.ManufacturedCondoPudCoop)%>
        ];

        var propertyType = <%=AspxTools.JQuery(this.sFannieSpT)%>.val();
        $('#HomeIsMhAdvantageRow').toggle(manufacturedTypes.indexOf(propertyType) !== -1);
    }
</script>

<input type="Hidden" id="sSpAddr" runat="server" value="" />
<input type="Hidden" id="sSpCity" runat="server" value="" />
<input type="Hidden" id="sSpCounty" runat="server" value="" />
<input type="Hidden" id="sSpState" runat="server" value="" />
<input type="Hidden" id="sSpZip" runat="server" value="" />
<input type="Hidden" id="sHmdaActionD" runat="server" value="" />
<input type="Hidden" id="sHmdaMsaNum" runat="server" value="" />
<input type="Hidden" id="sHmdaCountyCode" runat="server" value="" />
<input type="Hidden" id="sHmdaStateCode" runat="server" value="" />
<input type="Hidden" id="sHmdaCensusTract" runat="server" value="" />
<input type="Hidden" id="_ClientID" runat="server" value="FannieAddendumDataSections" />

<div class="content-container">
    <div class="section">
        <fieldset>
            <legend class="LoanFormHeader">Loan File</legend>
            <div class="section-interior">
                <div class="field-container">
                    <div class="FieldLabel left-column">DO / DU Case ID</div>
                    <asp:TextBox ID="sDuCaseId" runat="server" MaxLength="30" ClientIDMode="Static"></asp:TextBox>
                </div>
                <div class="field-container">
                    <div class="FieldLabel left-column">Lender Institution ID</div>
                    <asp:TextBox ID="sDuLenderInstitutionId" runat="server" />
                </div>
                <div class="field-container">
                    <div class="FieldLabel left-column">Doc Type</div>
                    <asp:DropDownList ID="sFannieDocT" runat="server"></asp:DropDownList>
                </div>
                <div class="field-container">
                    <div class="FieldLabel left-column">Loan Type</div>
                    <asp:DropDownList ID="sLT" runat="server" />
                </div>
            </div>
        </fieldset>
    </div>
    <div class="section">
        <fieldset>
            <legend class="LoanFormHeader">Loan Application</legend>
            <div class="section-interior">
                <div class="field-container">
                    <div class="FieldLabel left-column">Signature Date</div>
                    <ml:DateTextBox ID="a1003SignD" runat="server" preset="date"></ml:DateTextBox>
                </div>
                <div class="field-container">
                    <div class="FieldLabel left-column">Interview Date</div>
                    <ml:DateTextBox ID="a1003InterviewD" runat="server" Width="75" preset="date"></ml:DateTextBox>
                    <asp:CheckBox ID="a1003InterviewDLckd" runat="server" onclick="refreshCalculation();" Text="Lckd" />
                </div>
                <div class="field-container">
                    <div class="FieldLabel left-column">Product Description</div>
                    <asp:TextBox ID="sFannieProdDesc" runat="server" />
                </div>
                <div class="field-container">
                    <div class="FieldLabel left-column">Fannie Product Code</div>
                    <asp:TextBox ID="sFannieProductCode" MaxLength="15" runat="server" />
                </div>
                <div class="field-container">
                    <div class="FieldLabel left-column">Fannie ARM Plan #</div>
                    <ml:ComboBox ID="sFannieARMPlanNum" runat="server" Width="300px"></ml:ComboBox>&nbsp;<asp:CheckBox ID="sFannieARMPlanNumLckd" runat="server" onclick="refreshCalculation();" />
                </div>
                <div class="field-container">
                    <div class="FieldLabel left-column">ARM Index Type</div>
                    <asp:DropDownList ID="sArmIndexT" onchange="refreshCalculation();" runat="server"></asp:DropDownList>&nbsp;<asp:CheckBox ID="sArmIndexTLckd" runat="server" onclick="refreshCalculation();" />
                </div>
                <div class="field-container">
                    <div class="FieldLabel left-column">FIPS Code</div>
                    <asp:TextBox ID="sFannieFips" runat="server" Enabled="false"></asp:TextBox>
                    <asp:CheckBox ID="sFannieFipsLckd" runat="server" />
                    <img id="FipsCodeInvalid" class="Hidden" src="<%=AspxTools.HtmlString(ResolveUrl("~/images/error_icon.gif"))%>" alt="Required" />
                    <input type="button" id="LookupGeoCode" value="Import Geocodes" onclick="fannieAddendum_importGeoCodes();" />
                </div>
                <div class="field-container Hidden WarningText" id="FipsCodeWarning">The FIPS Code must be 11 digits. State Code + County Code + Census Tract</div>
            </div>
        </fieldset>
    </div>
    <div class="section">
        <fieldset>
            <legend class="LoanFormHeader">Transmittal Summary</legend>
            <div class="section-interior">
                <div class="field-container">
                    <!--<div class="FieldLabel left-column">Seller Provided Below Market Financing</div> -->
                    <asp:CheckBox ID="sIsSellerProvidedBelowMktFin" runat="server" Text="Seller Provided Below Market Financing" CssClass="FieldLabel"></asp:CheckBox>
                </div>
                <div class="field-container">
                    <div class="FieldLabel left-column">Fannie Mae Property Type</div>
                    <asp:DropDownList ID="sFannieSpT" runat="server"></asp:DropDownList>
                </div>
                <div class="field-container" id="HomeIsMhAdvantageRow">
                    <div class="FieldLabel left-column">Is home MH Advantage?</div>
                    <asp:DropDownList ID="sHomeIsMhAdvantageTri" runat="server"></asp:DropDownList>
                </div>
                <div class="field-container">
                    <div class="FieldLabel left-column">Fannie Mae Project Classification</div>
                    <asp:DropDownList ID="sSpProjectClassFannieT" runat="server"></asp:DropDownList>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="section">
        <fieldset>
            <legend class="LoanFormHeader">Community Lending</legend>
            <div class="field-container">
                <asp:CheckBox ID="sIsCommunityLending" onclick="onClick_sIsCommunityLending();" runat="server" Text="Community Lending" CssClass="FieldLabel"></asp:CheckBox>
                <img id="sDisableCL" class="Hidden" src="<%=AspxTools.HtmlString(ResolveUrl("~/images/error_icon.gif")) %>" alt="Error" />
                <span class="Hidden WarningText" id="enableCommunityLending">Community Lending is not allowed for FHA and VA loans. </span>
            </div>
            <div class="field-container">
                <div class="FieldLabel left-column">Metropolitan Statistical Area or County</div>
                <asp:TextBox ID="sFannieMsa" runat="server" MaxLength="100" Width="200px"></asp:TextBox>
            </div>
            <div class="field-container">
                <div class="FieldLabel left-column">Community Lending Product</div>
                <asp:DropDownList ID="sFannieCommunityLendingT" runat="server"></asp:DropDownList><img id="sDiscontinuedProductImage" src="<%=AspxTools.HtmlString(ResolveUrl("~/images/error_icon.gif"))%>" class="Hidden" alt="Required" />
                <span id="sDiscontinuedProductMessage" class="Hidden WarningText">Product no longer supported by Fannie Mae. Please select a different product to submit</span>
            </div>
            <div class="field-container">
                <div class="FieldLabel left-column">Community Seconds Repayment Structure</div>
                <asp:DropDownList runat="server" ID="sCommunitySecondsRepaymentStructureT"></asp:DropDownList>
            </div>
            <div class="field-container">
                <div class="FieldLabel left-column">Homebuyer Education Completion</div>
                <asp:DropDownList runat="server" ID="sFannieHomebuyerEducationT"></asp:DropDownList>
            </div>
            <div class="field-container">
                <asp:CheckBox ID="sIsCommunitySecond" runat="server" Text="Community Seconds" CssClass="FieldLabel"></asp:CheckBox>
            </div>
            <div class="field-container">
                <div class="FieldLabel left-column">Income Limit Adj. Factor</div>
                <ml:PercentTextBox ID="sFannieIncomeLimitAdjPc" Width="60px" runat="server" preset="percent"></ml:PercentTextBox>
            </div>
        </fieldset>
    </div>
    <div class="section">
        <fieldset>
            <legend class="LoanFormHeader">Loan Application</legend>
            <div class="field-container">
                <div class="FieldLabel left-column">Present housing expense included in ratios (excluding rent)</div>
                <ml:MoneyTextBox ID="sPresLTotPersistentHExpMinusRent" runat="server" ReadOnly="true" />
            </div>
            <div class="field-container">
                <div class="FieldLabel left-column">Mortgage liability payments linked to primary residences (all applications)</div>
                <ml:MoneyTextBox ID="sReTotMPmtPrimaryResidence" runat="server" ReadOnly="true" />
            </div>
            <div class="field-container">
                <asp:CheckBox ID="sExportAdditionalLiabitiesFannieMae" runat="server" />
                <asp:CheckBox ID="sExportAdditionalLiabitiesDODU" runat="server" />
                <span class="FieldLabel checkbox-label">Add liabilities for additional housing expenses</span>
            </div>
            <asp:Repeater ID="PresentHousingRepeater" runat="server" OnItemDataBound="PresentHousingRepeater_OnItemDataBound" ClientIDMode="AutoID">
                <ItemTemplate>
                    <div class="field-container presentHousingItem">
                        <div class="FieldLabel indented-borrower-name">
                            <div>
                                <ml:EncodedLiteral ID="aBNm" runat="server" ></ml:EncodedLiteral>
                            </div>
                            <div>
                                <ml:EncodedLiteral ID="aCNm" runat="server" ></ml:EncodedLiteral>
                            </div>
                        </div>
                        <asp:TextBox ID="aPresTotHExpDesc" runat="server" Width="200px"></asp:TextBox>
                        <ml:MoneyTextBox ID="aPresTotHExpCalc" runat="server" />
                        <asp:CheckBox ID="aPresTotHExpCalcLckd" runat="server" onclick="refreshCalculation();" />
                    </div>
                </ItemTemplate>
                <SeparatorTemplate>
                    <hr />
                </SeparatorTemplate>
            </asp:Repeater>
        </fieldset>
    </div>
    <div class="section">
        <fieldset>
            <legend class="LoanFormHeader">Down Payment</legend>
            <div class="field-container">
                <asp:RadioButton runat="server" ID="DUDownPaymentSource1003" CssClass="FieldLabel" Text="Use 1003 downpayment source" GroupName="DwnSrc" />
                <asp:RadioButton runat="server" ID="DUDownPaymentSourceJSON" CssClass="FieldLabel" Text="Enter new downpayment sources" GroupName="DwnSrc" />
                <table id="sDUDownPmtSrcs"></table>
            </div>
        </fieldset>
    </div>
    <div class="section" id="duValidation" ng-app="duValidation" ng-controller="duValidationController">
        <fieldset>
            <legend class="LoanFormHeader">DU Validation</legend>
            <div class="field-container">
                <table id="sDuThirdPartyProviders">
                    <thead>
                        <tr>
                            <td class="FieldLabel">3rd Party Data Provider Name</td>
                            <td class="FieldLabel">Reference Number</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="provider in thirdPartyProviders" ng-form="thirdPartyProvider">
                            <td>
                                <select name="providerNameSelect" ng-model="provider.Name" ng-options="providerName.ProviderName as providerName.DisplayName for providerName in thirdPartyProviderNames" ng-required="!provider.Name && !!provider.ReferenceNumber" ng-blur="onInputChange()"></select>
                                <span ng-show="thirdPartyProvider.providerNameSelect.$error.required" class="WarningText">Please choose a provider name.</span>
                            </td>
                            <td>
                                <input name="providerReferenceNumber" type="text" maxlength="50" ng-model="provider.ReferenceNumber" ng-required="!provider.ReferenceNumber && !!provider.Name" ng-blur="onInputChange()" />
                                <span ng-show="thirdPartyProvider.providerReferenceNumber.$error.required" class="WarningText">Please enter a reference number.</span>
                            </td>
                            <td>
                                <input type="button" value=" - " ng-click="removeItem($index)" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <input class="FieldLabel left-column" type="button" value="Add DU 3rd Party Provider" ng-click="addItem()" />
            </div>
        </fieldset>
    </div>
    <div id="FHAAddendumRow" class="section Hidden">
        <fieldset>
            <legend class="LoanFormHeader">Additional FHA Loan Information</legend>
            <div class="field-container">
                <div class="FieldLabel left-column">Section of the Act</div>
                <ml:ComboBox runat="server" ID="sFHAHousingActSection"></ml:ComboBox>
            </div>
            <div class="field-container">
                <div class="FieldLabel left-column">Originator Identifier</div>
                <div style="display: inline-block">
                    <div>
                        <div class="originator-id-label">
                            <asp:RadioButton runat="server" Text="Lender ID" GroupName="sFhaLenderIdT" ID="sFhaLenderIdT_RegularFhaLender" CssClass="FieldLabel" />
                        </div>
                        <asp:TextBox runat="server" ID="sFHALenderIdCode"></asp:TextBox>
                    </div>
                    <div>
                        <div class="originator-id-label">
                            <asp:RadioButton runat="server" Text="Sponsored Originator EIN" GroupName="sFhaLenderIdT" ID="sFhaLenderIdT_SponsoredOriginatorEIN" CssClass="FieldLabel" />
                        </div>
                        <asp:TextBox runat="server" ID="sSponsoredOriginatorEIN" ReadOnly="true"></asp:TextBox>
                        <asp:CheckBox runat="server" ID="sFhaSponsoredOriginatorEinLckd" />
                    </div>
                    <div>
                        <div class="FieldLabel originator-id-label">Sponsor/Agent ID</div>
                        <asp:TextBox runat="server" ID="sFHASponsorAgentIdCode"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="field-container">
                <div class="FieldLabel left-column">Seller Contribution</div>
                <ml:MoneyTextBox ID="sFHASellerContribution" runat="server"></ml:MoneyTextBox>
            </div>
            <asp:Repeater runat="server" ID="AppCounselTypes" OnItemDataBound="AppCounselTypes_OnItemDataBound">
                <ItemTemplate>
                    <div class="appCounseling">
                        <div class="field-container">
                            <div class="FieldLabel left-column">
                                <ml:EncodedLiteral runat="server" ID="aBNm" />
                                Counsel Type
                            </div>
                            <asp:DropDownList runat="server" ID="aBTotalScoreFhtbCounselingT"></asp:DropDownList>
                        </div>
                        <asp:Panel runat="server" CssClass="field-container" ID="coborrower">
                            <div class="FieldLabel left-column">
                                <ml:EncodedLiteral runat="server" ID="aCNm"  />
                                Counsel Type
                            </div>
                            <asp:DropDownList runat="server" ID="aCTotalScoreFhtbCounselingT"></asp:DropDownList>
                        </asp:Panel>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </fieldset>
    </div>
    <div id="AdditionalVaInformation" class="section Hidden">
        <fieldset>
            <legend class="LoanFormHeader">Additional VA Information</legend>
            <div class="field-container">
                <div class="FieldLabel left-column">Entitlement Amount</div>
                <ml:MoneyTextBox runat="server" preset="money" ID="aVaEntitleAmt"></ml:MoneyTextBox>
            </div>
            <div class="field-container">
                <div class="FieldLabel left-column">Maintenance</div>
                <ml:MoneyTextBox runat="server" preset="money" ID="sVaProMaintenancePmt"></ml:MoneyTextBox>
            </div>
            <div class="field-container">
                <div class="FieldLabel left-column">Utilities (including heat)</div>
                <ml:MoneyTextBox runat="server" preset="money" ID="sVaProUtilityPmt"></ml:MoneyTextBox>
            </div>
            <div class="field-container">
                <div class="FieldLabel left-column">Seller Contribution</div>
                <ml:MoneyTextBox runat="server" preset="money" ID="sFHASellerContributionVa"></ml:MoneyTextBox>
            </div>
            <hr />
            <asp:Repeater runat="server" ID="VaAppTaxes" OnItemDataBound="VaAppTaxes_OnItemDataBound">
                <ItemTemplate>
                    <div class="AppTaxes">
                        <div class="field-container">
                            <div class="FieldLabel left-column">
                                <ml:EncodedLiteral runat="server" ID="aBNm0" />
                                Federal Tax (Monthly)
                            </div>
                            <ml:MoneyTextBox runat="server" preset="money" ID="aVaBFedITax"></ml:MoneyTextBox>
                        </div>
                        <div class="field-container">
                            <div class="FieldLabel left-column">
                                <ml:EncodedLiteral runat="server" ID="aBNm1"  />
                                State Tax (Monthly)
                            </div>
                            <ml:MoneyTextBox runat="server" preset="money" ID="aVaBStateITax"></ml:MoneyTextBox>
                        </div>
                        <div class="field-container">
                            <div class="FieldLabel left-column">
                                <ml:EncodedLiteral runat="server" ID="aBNm2"  />
                                Social Security Tax (Monthly)
                            </div>
                            <ml:MoneyTextBox runat="server" preset="money" ID="aVaBSsnTax"></ml:MoneyTextBox>
                        </div>
                        <div class="field-container">
                            <div class="FieldLabel left-column">
                                <ml:EncodedLiteral runat="server" ID="aBNm3" />
                                Other Tax (Monthly)
                            </div>
                            <ml:MoneyTextBox runat="server" preset="money" ID="aVaBOITax"></ml:MoneyTextBox>
                        </div>
                        <asp:PlaceHolder runat="server" ID="coborrower">
                            <div class="field-container">
                                <div class="FieldLabel left-column">
                                    <ml:EncodedLiteral runat="server" ID="aCNm0"  />
                                    Federal Tax (Monthly)
                                </div>
                                <ml:MoneyTextBox runat="server" preset="money" ID="aVaCFedITax"></ml:MoneyTextBox>
                            </div>
                            <div class="field-container">
                                <div class="FieldLabel left-column">
                                    <ml:EncodedLiteral runat="server" ID="aCNm1"  />
                                    State Tax (Monthly)
                                </div>
                                <ml:MoneyTextBox runat="server" preset="money" ID="aVaCStateITax"></ml:MoneyTextBox>
                            </div>
                            <div class="field-container">
                                <div class="FieldLabel left-column">
                                    <ml:EncodedLiteral runat="server" ID="aCNm2"  />
                                    Social Security Tax (Monthly)
                                </div>
                                <ml:MoneyTextBox runat="server" preset="money" ID="aVaCSsnTax"></ml:MoneyTextBox>
                            </div>
                            <div class="field-container">
                                <div class="FieldLabel left-column">
                                    <ml:EncodedLiteral runat="server" ID="aCNm3" />
                                    Other Tax (Monthly)
                                </div>
                                <ml:MoneyTextBox runat="server" preset="money" ID="aVaCOITax"></ml:MoneyTextBox>
                            </div>
                        </asp:PlaceHolder>
                    </div>
                </ItemTemplate>
                <SeparatorTemplate>
                    <hr />
                </SeparatorTemplate>
            </asp:Repeater>
        </fieldset>
    </div>
</div>
