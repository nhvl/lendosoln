using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;
using LendersOfficeApp.newlos.Status;
using LendersOffice.Constants;


namespace LendersOfficeApp.newlos
{

	public partial  class BorrowerEmploymentFrame : BaseLoanUserControl, IAutoLoadUserControl
	{
		#region member variables
		protected ContactFieldMapper CFM;
		#endregion

        private bool m_isBorrower = true;

        protected Guid Primary_RecordID = Guid.Empty;

        public bool IsBorrower 
        {
            get { return m_isBorrower; }
            set { m_isBorrower = value; }
        }
        public void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(BorrowerEmploymentFrame));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            DisplayName.Text = m_isBorrower ? dataApp.aBNm : dataApp.aCNm;

            aEmplrBusPhoneLckd.Checked = m_isBorrower ? dataApp.aBEmplrBusPhoneLckd : dataApp.aCEmplrBusPhoneLckd;


            IEmpCollection recordList = m_isBorrower ? dataApp.aBEmpCollection : dataApp.aCEmpCollection;

            IPrimaryEmploymentRecord field = recordList.GetPrimaryEmp(false);

            if (field == null)
            {
                dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(LoanID, typeof(BorrowerEmploymentFrame));
                // 12/8/2010 dd - For the very first time always create a primary employment record.
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataApp = dataLoan.GetAppData(ApplicationID);
                recordList = m_isBorrower ? dataApp.aBEmpCollection : dataApp.aCEmpCollection;

                field = recordList.GetPrimaryEmp(true);

                dataLoan.Save();
            }

            Primary_RecordID = field.RecordId;
            Primary_IsSelfEmplmt.Checked = field.IsSelfEmplmt;
            Primary_EmplrNm.Text = field.EmplrNm;
            Primary_EmplrAddr.Text = field.EmplrAddr;
            Primary_EmplrCity.Text = field.EmplrCity;
            Primary_EmplrState.Value = field.EmplrState;
            Primary_EmplrZip.Text = field.EmplrZip;
            Primary_JobTitle.Text = field.JobTitle;
            Primary_EmpltStartD.Text = field.EmpltStartD_rep;
            Primary_EmplmtLen.Text = field.EmplmtLen_rep;
            Primary_ProfLen.Text = field.ProfLen_rep;
            Primary_ProfStartD.Text = field.ProfStartD_rep;
            Primary_EmplrBusPhone.Text = field.EmplrBusPhone;
            Primary_EmplrFax.Text = field.EmplrFax;
            Primary_VerifSentD.Text = field.VerifSentD_rep;
            Primary_VerifReorderedD.Text = field.VerifReorderedD_rep;
            Primary_VerifRecvD.Text = field.VerifRecvD_rep;
            Primary_VerifExpD.Text = field.VerifExpD_rep;
            Primary_EmployerCodeVoe.Text = field.EmployerCodeVoe;
            Primary_EmployeeIdVoe.Text = field.EmployeeIdVoe;
            Tools.SetDropDownListValue(Primary_SelfOwnershipShareT, field.SelfOwnershipShareT);
            Primary_IsSpecialBorrowerEmployerRelationship.Checked = field.IsSpecialBorrowerEmployerRelationship;
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            Page.ClientScript.RegisterHiddenField(this.ClientID + "_IsSwap", "");
            Primary_EmplrZip.SmartZipcode(Primary_EmplrCity, Primary_EmplrState);
            Primary_EmplmtLen.Attributes.Add("onblur", "IsDecimalNumberOnly(this); f_CalcEmpltStartD();");
			Primary_EmplmtLen.Attributes.Add("onkeyup", "return numberEntered(this)" );
            Primary_EmplmtLen.Attributes.Add("onpaste", "return false;");
            Primary_EmpltStartD.Attributes.Add("onblur", "f_CalcEmpltLen();");
            Primary_EmpltStartD.Attributes.Add("onchange", "f_CalcEmpltLen();");
            Primary_ProfLen.Attributes.Add("onblur", "IsDecimalNumberOnly(this); f_CalcProfStartD();");
			Primary_ProfLen.Attributes.Add("onkeyup", "return numberEntered(this)" );
            Primary_ProfLen.Attributes.Add("onpaste", "return false;");
            Primary_ProfStartD.Attributes.Add("onblur", "f_CalcProfLen()");
            Primary_ProfStartD.Attributes.Add("onchange", "f_CalcProfLen()");
            Tools.Bind_SelfOwnershipShare(Primary_SelfOwnershipShareT);
        }

        public void SaveData() 
        {
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            // Only add this javascript when this usercontrol is visible, else borrower info, monthly income tab will not work.
            if (this.Visible) 
            {
                Page.ClientScript.RegisterHiddenField("ListLocation", string.Format("BorrowerInfo.aspx?loanid={0}&pg={1}", LoanID, m_isBorrower ? "1" : "2"));
                ((BasePage)this.Page).RegisterJsScript("singleedit2.js");
                ((BasePage)this.Page).RegisterJsScript("LQBPrintFix.js");
            }


			// OPM 17652 - Ethan
			CFM.Type				= "0"; 
			CFM.CompanyNameField	= Primary_EmplrNm.ClientID;
			CFM.StreetAddressField	= Primary_EmplrAddr.ClientID;
			CFM.CityField			= Primary_EmplrCity.ClientID;
			CFM.StateField			= Primary_EmplrState.ClientID;
			CFM.ZipField			= Primary_EmplrZip.ClientID;
			CFM.CompanyPhoneField	= Primary_EmplrBusPhone.ClientID;
			CFM.AgentTitleField		= Primary_JobTitle.ClientID;
			CFM.CompanyFaxField		= Primary_EmplrFax.ClientID;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion
	}
}
