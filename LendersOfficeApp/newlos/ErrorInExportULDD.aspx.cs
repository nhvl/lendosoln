﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Conversions;

namespace LendersOfficeApp.newlos
{    
    public partial class ErrorInExportingULDD : LendersOffice.Common.BaseServicePage
    {
        protected UlddExportErrorItem[] errors;       
        protected void Page_Load(object sender, EventArgs e)
        {
            string key = RequestHelper.GetSafeQueryString("key");

            errors = ObsoleteSerializationHelper.JavascriptJsonDeserializer<UlddExportErrorItem[]>(AutoExpiredTextCache.GetFromCache(key));
        }

        protected String showErrorMessage(UlddExportErrorItem str)
        {
            return str.ErrorMessage.TrimWhitespaceAndBOM();            
        }
        protected String showPage(UlddExportErrorItem err)
        {
            return err.PageLocation.TrimWhitespaceAndBOM();            
        }

        
    }
}
