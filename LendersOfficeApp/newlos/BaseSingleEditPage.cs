using System;
using System.Data;
using DataAccess;
using LendersOffice.Common;


namespace LendersOfficeApp.newlos
{
	/// <summary>
	/// This is a base class for single edit record page. It will generate require client script
	/// for navigating record list.
	/// </summary>
	public class BaseSingleEditPage<T> : BaseLoanPage where T : class
	{
        protected Guid RecordID 
        {
            get { return RequestHelper.GetGuid("recordid", Guid.Empty); }
        }

        protected bool IsUlad
        {
            get
            {
                return RequestHelper.GetBool("isUlad") || this.ApplicationID == Guid.Empty;
            }
        }

        protected Guid NewRecordID;
        protected int CurrentIndex;

        /// <summary>
        /// It is your responsibility to have {Page_Name}Service.aspx in the same
        /// directory as {Page_Name}.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PageInit(object sender, System.EventArgs e) 
        {
            int l = VirtualRoot.Length;
            string url = Request.Path.Substring(l, Request.Path.Length - l - 5) + "service.aspx";
            
            RegisterService("singleedit", url);
            RegisterJsScript("singleedit.js");

            if(this.IsUlad)
            {
                RegisterJsScript("VerificationRecord-Ulad.js");
            }

            AddInitScriptFunction("_initEdit");
        }

        protected override void LoadData() 
        {
            if (!this.IsUlad)
            {
                T field = null;

                if (RecordID != Guid.Empty)
                {
                    field = RetrieveRecord();
                    BindSingleRecord(field);
                }
            }

            GenerateRecordList(GetDataView());

            //ClientScript.RegisterHiddenField("LoanID", LoanID.ToString());
            //ClientScript.RegisterHiddenField("ApplicationID", ApplicationID.ToString());
            ClientScript.RegisterHiddenField("RecordID", RecordID.ToString());
            ClientScript.RegisterHiddenField("currentindex", CurrentIndex.ToString());
            ClientScript.RegisterHiddenField("ListLocation", ListLocation);
            ClientScript.RegisterHiddenField("IsUlad", this.IsUlad.ToString());
        }

        #region Methods need to be override

        /// <summary>
        /// Url of the main list for this record.
        /// </summary>
        protected virtual string ListLocation 
        {
            get 
            { 
                throw new NotImplementedException("Need to define ListLocation");
            }
        }

        protected virtual T RetrieveRecord() 
        {
            throw new NotImplementedException("Need to define CreateNewRecord()");
        }

        protected virtual void BindSingleRecord(T f) 
        {
            throw new NotImplementedException("Need to define CreateNewRecord()");
        }
        protected virtual DataView GetDataView() 
        {
            throw new NotImplementedException("Need to define CreateNewRecord()");
        }

        #endregion
        protected override void OnInit(System.EventArgs e) 
        {
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }

        private void GenerateRecordList(DataView view) 
        {
            if (null == view) 
            {
                ClientScript.RegisterArrayDeclaration("list", ""); // Register empty arrary list.
                return;
            }
            view.Sort = RequestHelper.GetSafeQueryString("orderby");
            string[] list = new string[view.Count];
            CurrentIndex = view.Count;
            for (int i = 0; i < view.Count; i++) 
            {
                DataRowView row = view[i];
                string rid = (string) row["recordid"];
                if (rid == RecordID.ToString()) 
                {
                    CurrentIndex = i;
                }
                list[i] = "'" + rid + "'";
            }
            ClientScript.RegisterArrayDeclaration("list", string.Join(",", list));

        }

	}

    public class BaseSingleEditPage2 : BaseLoanPage 
    {
        protected Guid RecordID 
        {
            get { return RequestHelper.GetGuid("recordid", Guid.Empty); }
        }

        /// <summary>
        /// It is your responsibility to have {Page_Name}Service.aspx in the same
        /// directory as {Page_Name}.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PageInit(object sender, System.EventArgs e) 
        {
            // 2/10/2004 dd - This page always embed inside a frame. Therefore copyright message is not necessary.
            this.DisplayCopyRight = false;
            this.IsRefreshLoanSummaryInfo = false;
            int l = VirtualRoot.Length;
            string url = Request.Path.Substring(l, Request.Path.Length - l - 5) + "service.aspx";
            RegisterService("singleedit", url);
            RegisterJsScript("singleedit2.js");
            this.RegisterJsScript("LQBPrintFix.js");
            AddInitScriptFunction("_initEdit");

            //ClientScript.RegisterHiddenField("LoanID", LoanID.ToString());
            //ClientScript.RegisterHiddenField("ApplicationID", ApplicationID.ToString());
            ClientScript.RegisterHiddenField("RecordID", RecordID.ToString());
            ClientScript.RegisterHiddenField("RowIndex", "");
            ClientScript.RegisterHiddenField("Direction", "");
            ClientScript.RegisterHiddenField("SortExpression", "");
            ClientScript.RegisterHiddenField("PreviousSortExpression", "");

        }
        protected override void OnInit(System.EventArgs e) 
        {
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
