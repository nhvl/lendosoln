namespace LendersOfficeApp.newlos
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Common.TextImport;
    using LendersOffice.Constants;
    using LendersOffice.Pdf;
    using LendersOffice.PdfForm;
    using LendersOffice.Security;

    public partial class PrintList : BaseLoanPage
    {
        protected System.Web.UI.WebControls.CheckBox IsPdfEncryption;
        protected Guid CustomWordGroupId = new Guid("11111111-1111-1111-1111-111111111111");
        protected Guid CustomPdfGroupId = new Guid("22222222-2222-2222-2222-222222222222");

        protected string m_defaultPrintID;

        protected bool AllowPrintTestPdf
        {
            get { return ConstAppDavid.CurrentServerLocation == ServerLocation.LocalHost || ConstAppDavid.CurrentServerLocation == ServerLocation.Development; }
        }
        private void BindGroupName(DropDownList ddl) 
        {
            var groups = EmployeeDB.RetrieveById(BrokerID, EmployeeID).AvailablePrintGroups;

            if (!IsLeadPage)
            {
                groups = from g in groups
                         where g.Id !=  this.CustomPdfGroupId ||
                               g.Id != this.CustomWordGroupId
                         select g;
            }

            ddl.DataSource = groups;
            ddl.DataTextField = "Name";
            ddl.DataValueField = "Id";
            ddl.DataBind();
        }

        private List<PdfPrintListItem> x_cache = null;

        List<string> ignore1098List = new List<string>()
        {
            "LendersOffice.Pdf.CTaxForm_1098PDF",
            "LendersOffice.Pdf.CTaxForm_1098_2010PDF",
            "LendersOffice.Pdf.CTaxForm_1098_2011PDF",
            "LendersOffice.Pdf.CTaxForm_1098_2012PDF",
            "LendersOffice.Pdf.CTaxForm_1098_2013PDF",
            "LendersOffice.Pdf.CTaxForm_1098_2014PDF",
            "LendersOffice.Pdf.CTaxForm_1098_2016PDF",
            "LendersOffice.Pdf.CTaxForm_1098_2017PDF"
        };

        private List<PdfPrintListItem> LoadMainPrintList(CPageData dataLoan) 
        {
            if (x_cache != null)
            {
                return x_cache;
            }
            List<PdfPrintListItem> items; 
            if (false == IsLeadPage)
            {
                items = PdfPrintList.RetrieveAll();
            }
            else
            {
                items = PdfPrintList.RetrieveAllForLead();
            }

            var principal = PrincipalFactory.CurrentPrincipal;

            List<string> ignoreList = new List<string>();
            if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
            {
                ignoreList.Add("LendersOffice.Pdf.CIFW2015");
                ignoreList.Add("LendersOffice.Pdf.CFundingWorksheet2015"); 
            }
            else if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated)
            {
                ignoreList.Add("LendersOffice.Pdf.CInitialFeesWorksheetPDF");

                if(dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                {
                    ignoreList.Add("LendersOffice.Pdf.CStandardFundingWorksheetPDF");
                }
                else
                {
                    ignoreList.Add("LendersOffice.Pdf.CFundingWorksheet2015");
                }
            }
            else
            {
                ignoreList.Add("LendersOffice.Pdf.CInitialFeesWorksheetPDF");
                ignoreList.Add("LendersOffice.Pdf.CStandardFundingWorksheetPDF");
            }

            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                ignoreList.Add("LendersOffice.Pdf.CChangeOfCircumstancesPDF");
            }
            else
            {
                ignoreList.Add("LendersOffice.Pdf.CChangeOfCircumstancesNewPDF");
            }

            if(!principal.HasConversationLogFeatureEnabled)
            {
                ignoreList.Add(typeof(CConversationLogPDF).FullName);
            }

            if (!Broker.EnableRenovationLoanSupport || !dataLoan.sIsRenovationLoan)
            {
                ignoreList.Add(typeof(HomeStyleMaxMortgage1035).FullName);
                ignoreList.Add(typeof(Limited203kPurchaseTransaction).FullName);
                ignoreList.Add(typeof(Limited203kRefinanceTransaction).FullName);
                ignoreList.Add(typeof(Standard203kPurchaseTransaction).FullName);
                ignoreList.Add(typeof(Standard203kRefinanceTransaction).FullName);
            }

            x_cache = items.Where(p => !ignoreList.Contains(p.Type)).ToList();
            return x_cache;
        }

        private List<PdfPrintListItem> LoadCustomWordFormList() 
        {
            List<PdfPrintListItem> list = new List<PdfPrintListItem>();
            list.Add(new PdfPrintListItem() { Type = "LendersOffice.Pdf.CustomFormList" });
            return list;
        }

        private List<PdfPrintListItem> LoanCustomPdfFormList()
        {
            List<PdfPrintListItem> list = new List<PdfPrintListItem>();
            list.Add(new PdfPrintListItem() { Type = "LendersOffice.Pdf.CustomPDFFormList" });
            return list;
        }

        private List<PdfPrintListItem> GetPrintItemsInGroup(CPageData dataLoan, string value)
        {
            Guid groupID = Guid.Empty;
            if (value != null && value != "")
            {
                try
                {
                    groupID = new Guid(value);
                }
                catch {
                    var emp = EmployeeDB.RetrieveById(BrokerID, EmployeeID);
                    if (emp.RestrictPrintGroups)
                    {
                        if (emp.AvailablePrintGroups.Any())
                        {
                            groupID = emp.AvailablePrintGroups.First().Id;
                        }
                        else
                        {
                            return new List<PdfPrintListItem>();
                        }
                    }
                }
            }

            return GetPrintItemsInGroup(dataLoan, groupID);
        }

        private List<PdfPrintListItem> GetPrintItemsInGroup(CPageData dataLoan, PrintListGroup group)
        {
            return GetPrintItemsInGroup(dataLoan, group.Id);
        }

        private List<PdfPrintListItem> GetPrintItemsInGroup(CPageData dataLoan, Guid groupID) 
        {
            HashSet<string> ignoreSet = new HashSet<string>();
            if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
            {
                    ignoreSet.Add("LendersOffice.Pdf.CIFW2015");
                    ignoreSet.Add("LendersOffice.Pdf.CFundingWorksheet2015");
            }
            else if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated)
            {
                    ignoreSet.Add("LendersOffice.Pdf.CInitialFeesWorksheetPDF");
                    ignoreSet.Add("LendersOffice.Pdf.CFundingWorksheet2015");
            }
            else
            {
                    ignoreSet.Add("LendersOffice.Pdf.CInitialFeesWorksheetPDF");
                    ignoreSet.Add("LendersOffice.Pdf.CStandardFundingWorksheetPDF");
            }

            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                ignoreSet.Add("LendersOffice.Pdf.CChangeOfCircumstancesPDF");
            }
            else
            {
                ignoreSet.Add("LendersOffice.Pdf.CChangeOfCircumstancesNewPDF");
            }

            if (groupID == Guid.Empty) 
            {
                return LoadMainPrintList(dataLoan);
            } 
            else if (groupID == new Guid("11111111-1111-1111-1111-111111111111")) 
            {
                return LoadCustomWordFormList();
            }
            else if (groupID == new Guid("22222222-2222-2222-2222-222222222222"))
            {
                return LoanCustomPdfFormList();
            }
            // 5/27/2004 dd - Load group definition from DB.
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", BrokerID),
                                            new SqlParameter("@GroupID", groupID)
                                        };
            string xml = "";
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerID, "RetrievePrintGroup", parameters)) 
            {
                if (reader.Read() )
                {
                    xml = (string) reader["GroupDefinition"];
                }
            }

            if (xml == "")
                return LoadMainPrintList(dataLoan);
            else 
            {
                XmlDocument doc = Tools.CreateXmlDoc(xml);
                XmlNodeList nodeList =doc.SelectNodes("//group/print");
                List<PdfPrintListItem> list = new List<PdfPrintListItem>();
                foreach (XmlElement el in nodeList)
                {
                    var printListItem = new PdfPrintListItem() { Type = el.GetAttribute("type") };
                    if (ignoreSet.Contains(printListItem.Type))
                    {
                        continue;
                    }

                    list.Add(printListItem);
                }
                return list;
            }

        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.RegisterService("print", "/newlos/PrintListService.aspx");
            this.EnableJqueryMigrate = false;

            this.RegisterJsScript("~/common/PrintingControls/", "ePrintCtrl.js");
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("ModelessDlg.js");

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);

            var repository = EDocs.EDocumentRepository.GetUserRepository();
            if (!repository.CanCreateDocument)
            {
                this.btnPrintToEDocs.Style.Add("display", "none");
            }
            
            this.RegisterJsGlobalVariables("appId", this.ApplicationID);

            BindGroupName(m_printGroupDDL);

            btnEprint.Visible = ConstApp.DetermineIsIE(Request);

            CPageData dataLoan = new CPrintData(LoanID);
            dataLoan.InitLoad();

            int? reportingYear = dataLoan.s1098ReportingYear;
            
            if (!reportingYear.HasValue || reportingYear.Value == 0)
            {
                reportingYear = dataLoan.s1098FilingYear;
            }            

            switch (reportingYear)
            {
                case 2018:
                    this.ignore1098List.Remove("LendersOffice.Pdf.CTaxForm_1098_2018PDF");
                    break;
                case 2017:
                    this.ignore1098List.Remove("LendersOffice.Pdf.CTaxForm_1098_2017PDF");
                    break;
                case 2016:
                    this.ignore1098List.Remove("LendersOffice.Pdf.CTaxForm_1098_2016PDF");
                    break;
                case 2014:
                    this.ignore1098List.Remove("LendersOffice.Pdf.CTaxForm_1098_2014PDF");
                    break;
                case 2013:
                    this.ignore1098List.Remove("LendersOffice.Pdf.CTaxForm_1098_2013PDF");
                    break;
                case 2012:
                    this.ignore1098List.Remove("LendersOffice.Pdf.CTaxForm_1098_2012PDF");
                    break;
                case 2011:
                    this.ignore1098List.Remove("LendersOffice.Pdf.CTaxForm_1098_2011PDF");
                    break;
                case 2010:
                    this.ignore1098List.Remove("LendersOffice.Pdf.CTaxForm_1098_2010PDF");
                    break;
                default:
                    this.ignore1098List.Remove("LendersOffice.Pdf.CTaxForm_1098PDF");
                    break;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("<table class=FormTable onclick='clearDirty();' cellspacing=0 cellpadding=0 border=0>");
            List<PdfPrintListItem> list;

            var defaultSelectedPageId = RequestHelper.GetSafeQueryString("printid");

            if (defaultSelectedPageId.Equals("CHUD_92900_APDF") && !dataLoan.sIsRequire2016FhaAddendum)
            {
                defaultSelectedPageId = "CHUD_92900_A_2010_PDF";
            }

            if (Page.IsPostBack || string.IsNullOrEmpty(defaultSelectedPageId))
            {
                list = GetPrintItemsInGroup(dataLoan, Request.Form[m_printGroupDDL.ClientID] ?? m_printGroupDDL.SelectedValue);
            }
            else
            {
                Guid? groupToSelect = GetFirstPrintGroupIdContaining(dataLoan, defaultSelectedPageId);
                if (!groupToSelect.HasValue)
                {
                    list = new List<PdfPrintListItem>();
                }
                else
                {
                    list = GetPrintItemsInGroup(dataLoan, groupToSelect.Value);
                    m_printGroupDDL.SelectedValue = groupToSelect.ToString();
                }
            }

            int index = 0;
            NameValueCollection arguments = new NameValueCollection();
            
            arguments["brokerid"] = BrokerID.ToString();

			int length = 0;
            bool isObsoleteFolder = false;

            foreach (PdfPrintListItem printItem in list) 
            {
                if (ignore1098List.Contains(printItem.Type))
                {
                    continue;
                }

                if (printItem.Type == "Header") 
                {
                    isObsoleteFolder = printItem.Description.Equals("Obsolete Forms");
                    
					// OPM 791 - If the length is the same now as it was at the beginning of the last header, there were
					// no applicable print elements in between, so we display the word 'none' to avoid confusion
					if(sb.Length == length)
					{
						sb.Append("<tr><td colspan=2 style='padding-left:20px'>None</td></tr>");
					}
					sb.AppendFormat(@"<tr><td colspan=2 class=FieldLabel>{0}</td></tr>", printItem.Description);
					length = sb.Length;
                } 
                else 
                {
					string type = printItem.Type;
                    if (type.StartsWith("CustomForm:")) 
                    {
                        IndividualCustomForm o = new IndividualCustomForm();
                        o.CustomFormID = new Guid(type.Substring(11));
                        o.ID = "_c" + index;
                        o.DataLoan = dataLoan;
                        o.Arguments = arguments;
                        o.RenderPrintLink(sb);
                        index++;
                    } 
                    else if (type.StartsWith(ConstAppDavid.CustomPdfProtocolPrefix)) 
                    {
                        try
                        {
                            Guid formId = new Guid(type.Substring(ConstAppDavid.CustomPdfProtocolPrefix.Length));

                            PdfForm customPdf = PdfForm.LoadById(formId);
                                CCustomPDF o = new CCustomPDF();

                                o.RenderPrintLink(sb, customPdf.FormId.ToString("N"), customPdf.Description, 0);
                                index++;
                        }
                        catch (NotFoundException)
                        {
                            // Skip
                        }
                    }
                    else
                    {
                        if (!dataLoan.sIsRequire2016FhaAddendum && !isObsoleteFolder && printItem.Type.Equals("LendersOffice.Pdf.CHUD_92900_APDF"))
                        {
                            type = "LendersOffice.Pdf.CHUD_92900_A_2010_PDF";
                        }
                        else if (!dataLoan.sIsRequire2016FhaAddendum && isObsoleteFolder && printItem.Type.Equals("LendersOffice.Pdf.CHUD_92900_A_2010_PDF"))
                        {
                            // Do not render the 2010 version of the PDF in the Obsolete folder if the loan still requires it.
                            continue;
                        }

                        ConstructorInfo c = PDFClassHashTable.GetConstructor(type);
                        IPDFPrintItem o = (IPDFPrintItem)c.Invoke(new object[0]);
                        o.ID = "_c" + index;
                        o.DataLoan = dataLoan;
                        o.Arguments = arguments;
                        o.RenderPrintLink(sb);
                        index++;
                    }
                }
            }

			// OPM 791 - If the length is the same now as it was at the beginning of the last header, there were
			// no applicable print elements in between, so we display the word 'none' to avoid confusion
			if(sb.Length == length)
			{
				sb.Append("<tr><td colspan=2 style='padding-left:20px'>None</td></tr>");
			}
            sb.Append("</table>");

            m_main.Text = sb.ToString();
            
            if (!Page.IsPostBack) 
            {
                m_defaultPrintID = defaultSelectedPageId;
            }
            
            this.PageTitle = "Print";
            this.PageID = "Print";
        }

        private Guid? GetFirstPrintGroupIdContaining(CPageData dataLoan, string defaultSelectedPageId)
        {
            var employee = EmployeeDB.RetrieveById(BrokerID, EmployeeID);

            //Employees with no restrictions should always get the all forms print group
            if (!employee.RestrictPrintGroups) return Guid.Empty;
            if (!employee.AvailablePrintGroups.Any()) return null;

            foreach (var group in employee.AvailablePrintGroups)
            {
                var printItems = GetPrintItemsInGroup(dataLoan, group);
                foreach (var item in printItems)
                {
                    if (item.Type.EndsWith(defaultSelectedPageId, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return group.Id;
                    }
                }
            }

            return employee.AvailablePrintGroups.First().Id;
        }


		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion


    }
}
