﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdditionalHELOC.aspx.cs" Inherits="LendersOfficeApp.newlos.AdditionalHELOC" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<%@ Register TagPrefix="QRP" TagName="QualRatePopup" Src="~/newlos/Forms/QualRateCalculationPopup.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
    table:not(.qual-rate-table) { padding : 3px; margin: 3px; }
    
    .SubHeader {
        font-weight: bold;
	    font-size: 11px;
	    color: navy ;
	    font-family: Arial, Helvetica, sans-serif;
        background-color: silver;  
        padding-top:5px;
        padding-left:5px;
        padding-bottom:5px;  
    }
    </style>
</head>
<body class="EditBackground">
<script type="text/javascript">
    function _init() {

    lockField(<%=AspxTools.JsGetElementById(this.sQualIRLckd)%>, 'sQualIR');

    var sHelocQualPmtFormulaT_Value = <%= AspxTools.JsGetElementById(sHelocQualPmtFormulaT)%>.value;
    var sHelocPmtFormulaT_Value = <%= AspxTools.JsGetElementById(sHelocPmtFormulaT)%>.value;
    
    if (sHelocQualPmtFormulaT_Value == <%= AspxTools.JsString(E_sHelocPmtFormulaT.Amortizing) %>)
    {
        document.getElementById('TermHeaderQual').innerText = "Term";
        document.getElementById('RateHeaderQual').style.display = "block";
        document.getElementById('RateCellQual').style.display = "block";
        document.getElementById('PercentHeaderQual').innerText = "";
        document.getElementById('PercentHeaderSpacerQual').style.display = "none";
        document.getElementById('PercentSpacerQual').style.display = "none";
        <%= AspxTools.JsGetElementById(sHelocQualPmtFormulaRateT) %>.style.display = "inline";
        <%= AspxTools.JsGetElementById(sHelocQualPmtAmortTermT) %>.style.display = "inline";
        <%= AspxTools.JsGetElementById(sHelocQualPmtAmortTerm) %>.style.display = "inline";
        document.getElementById('InterestOnlyDivisorQual').style.display = "none";
        <%= AspxTools.JsGetElementById(sHelocQualPmtPcBase) %>.readOnly = true;
    } 
    else if (sHelocQualPmtFormulaT_Value == <%= AspxTools.JsString(E_sHelocPmtFormulaT.InterestOnly) %>)
    {
        document.getElementById('TermHeaderQual').innerText = "";
        document.getElementById('RateHeaderQual').style.display = "block";
        document.getElementById('RateCellQual').style.display = "block";
        document.getElementById('PercentHeaderQual').innerText = "";
        document.getElementById('PercentHeaderSpacerQual').style.display = "none";
        document.getElementById('PercentSpacerQual').style.display = "none";
        <%= AspxTools.JsGetElementById(sHelocQualPmtFormulaRateT) %>.style.display = "inline";
        <%= AspxTools.JsGetElementById(sHelocQualPmtAmortTermT) %>.style.display = "none";
        <%= AspxTools.JsGetElementById(sHelocQualPmtAmortTerm) %>.style.display = "none";
        document.getElementById('InterestOnlyDivisorQual').style.display = "inline";
        <%= AspxTools.JsGetElementById(sHelocQualPmtPcBase) %>.readOnly = true;
    }
    else
    {
        document.getElementById('TermHeaderQual').innerText = "";
        document.getElementById('RateHeaderQual').style.display = "none";
        document.getElementById('RateCellQual').style.display = "none";
        document.getElementById('PercentHeaderQual').innerText = "Percentage";
        document.getElementById('PercentHeaderSpacerQual').style.display = "block";
        document.getElementById('PercentSpacerQual').style.display = "block";
        <%= AspxTools.JsGetElementById(sHelocQualPmtFormulaRateT) %>.style.display = "none";
        <%= AspxTools.JsGetElementById(sHelocQualPmtAmortTermT) %>.style.display = "none";
        <%= AspxTools.JsGetElementById(sHelocQualPmtAmortTerm) %>.style.display = "none";
        document.getElementById('InterestOnlyDivisorQual').style.display = "none";
        <%= AspxTools.JsGetElementById(sHelocQualPmtPcBase) %>.readOnly = false;

    }

    if (sHelocPmtFormulaT_Value == <%= AspxTools.JsString(E_sHelocPmtFormulaT.Amortizing) %>)
    {
        document.getElementById('TermHeader').innerText = "Term";
        document.getElementById('RateHeader').style.display = "block";
        document.getElementById('RateCell').style.display = "block";
        document.getElementById('PercentHeader').innerText = "";
        document.getElementById('PercentHeaderSpacer').style.display = "none";
        document.getElementById('PercentSpacer').style.display = "none";
        <%= AspxTools.JsGetElementById(sHelocPmtFormulaRateT) %>.style.display = "inline";
        <%= AspxTools.JsGetElementById(sHelocPmtAmortTermT) %>.style.display = "inline";
        <%= AspxTools.JsGetElementById(sHelocPmtAmortTerm) %>.style.display = "inline";
            document.getElementById('InterestOnlyDivisor').style.display = "none";
        <%= AspxTools.JsGetElementById(sHelocPmtPcBase) %>.readOnly = true;
    } 
    else if (sHelocPmtFormulaT_Value == <%= AspxTools.JsString(E_sHelocPmtFormulaT.InterestOnly) %>)
    {
        document.getElementById('TermHeader').innerText = "";
        document.getElementById('RateHeader').style.display = "block";
        document.getElementById('RateCell').style.display = "block";
        document.getElementById('PercentHeader').innerText = "";
        document.getElementById('PercentHeaderSpacer').style.display = "none";
        document.getElementById('PercentSpacer').style.display = "none";
        <%= AspxTools.JsGetElementById(sHelocPmtFormulaRateT) %>.style.display = "inline";
        <%= AspxTools.JsGetElementById(sHelocPmtAmortTermT) %>.style.display = "none";
        <%= AspxTools.JsGetElementById(sHelocPmtAmortTerm) %>.style.display = "none";
        document.getElementById('InterestOnlyDivisor').style.display = "inline";
        <%= AspxTools.JsGetElementById(sHelocPmtPcBase) %>.readOnly = true;
    }
    else
    {
        document.getElementById('TermHeader').innerText = "";
        document.getElementById('RateHeader').style.display = "none";
        document.getElementById('RateCell').style.display = "none";
        document.getElementById('PercentHeader').innerText = "Percentage";
        document.getElementById('PercentHeaderSpacer').style.display = "block";
        document.getElementById('PercentSpacer').style.display = "block";
        <%= AspxTools.JsGetElementById(sHelocPmtFormulaRateT) %>.style.display = "none";
        <%= AspxTools.JsGetElementById(sHelocPmtAmortTermT) %>.style.display = "none";
        <%= AspxTools.JsGetElementById(sHelocPmtAmortTerm) %>.style.display = "none";
        document.getElementById('InterestOnlyDivisor').style.display = "none";
        <%= AspxTools.JsGetElementById(sHelocPmtPcBase) %>.readOnly = false;
    }

    var loanAmountLocked = <%= AspxTools.JsGetElementById(sLAmtLckd) %>;
    loanAmountLocked.disabled = ML.IsRenovationLoan;
    <%= AspxTools.JsGetElementById(sLAmtCalc) %>.disabled = !loanAmountLocked.checked;

}
</script>
    <form id="form1" runat="server">
    <QRP:QualRatePopup ID="QualRatePopup" runat="server" />
    <div>
        <div class="MainRightHeader">Additional HELOC Data</div>
        <table>
            <tr>
                <td class="FieldLabel">Line Amount</td>
                <td><ml:moneytextbox id=sCreditLineAmt width="80" runat="server" onchange="refreshCalculation();" preset="money" /></td>
                <td class="FieldLabel">DPR Days/year</td>
                <td><asp:textbox id=sDaysInYr width="37px" runat="server" onchange="refreshCalculation();" maxlength="3" /></td>
                <td class="FieldLabel">Top</td>
                <td><ml:percenttextbox id=sQualTopR width="55" runat="server" preset="percent"  readonly=true/></td>
                <td class="FieldLabel">LTV</td>
                <td><ml:percenttextbox id=sLtvR width="55" runat="server" preset="percent" readonly=true/></td>
            </tr>
            <tr>
                <td class="FieldLabel">Initial Draw <asp:CheckBox ID="sLAmtLckd" runat="server" onclick="refreshCalculation();" Text="Lock"/></td>
                <td><ml:moneytextbox id=sLAmtCalc width="80" runat="server" onchange="refreshCalculation();" preset="money" /></td>
                <td class="FieldLabel">Index</td>
                <td><ml:percenttextbox id=sRAdjIndexR width="55" runat="server" onchange="refreshCalculation();" preset="percent" /></td>
                <td class="FieldLabel">Bottom</td>
                <td><ml:percenttextbox id=sQualBottomR width="55" runat="server" preset="percent"  readonly=true/></td>
                <td class="FieldLabel">CLTV</td>
                <td><ml:percenttextbox id=sCltvR width="55" runat="server" preset="percent"  readonly=true/></td>
            </tr>
            <tr>
                <td class="FieldLabel">Draw / Repay (mths)</td>
                <td><asp:textbox id=sHelocDraw width="37px" runat="server" onchange="refreshCalculation();" maxlength="3" /> / <asp:textbox id=sHelocRepay width="37px" runat="server" maxlength="3" readonly=true/></td>
                <td class="FieldLabel">Margin</td>
                <td><ml:percenttextbox id=sRAdjMarginR width="55" runat="server" onchange="refreshCalculation();" preset="percent" /></td>
                <td class="FieldLabel"></td>
                <td></td>
                <td class="FieldLabel">HCLTV</td>
                <td><ml:percenttextbox id=sHcltvR width="55" runat="server" preset="percent"  readonly=true/></td>
            </tr>
            <tr>
                <td class="FieldLabel">Term / Due (mths)</td>
                <td><asp:textbox id=sTerm width="37px" runat="server" onchange="refreshCalculation();" maxlength="3" /> / <asp:textbox id=sDue width="37px" runat="server" onchange="refreshCalculation();" maxlength="3" /></td>
                <td class="FieldLabel">Maximum Rate</td>
                <td><ml:percenttextbox id=sRLifeCapR width="55" runat="server" onchange="refreshCalculation();" preset="percent" /></td>
                <td class="FieldLabel"></td>
                <td></td>
                <td class="FieldLabel"></td>
                <td></td>
            </tr>
            <tr>
                <td class="FieldLabel">Initial Rate</td>
                <td><ml:percenttextbox id=sNoteIR width="55" runat="server" onchange="refreshCalculation();" preset="percent" /></td>
                <td class="FieldLabel">Fully Indexed Rate</td>
                <td><ml:percenttextbox id=sFullyIndexedR width="55" runat="server" preset="percent"  readonly=true/></td>

                <td class="FieldLabel"></td>
                <td></td>
            </tr>
            <tr>
                            <td class="FieldLabel">Daily Periodic Rate</td>
                <td><ml:percenttextbox id=sDailyPeriodicR width="75" runat="server" preset="percent"  readonly=true/></td>
                <td class="FieldLabel">Fully Indexed DPR</td>
                <td><ml:percenttextbox id=sDailyPeriodicFullyIndexR width="75" runat="server" preset="percent"  readonly=true/></td>

                <td class="FieldLabel"></td>
                <td></td>
                
            </tr>
            <tr>
                <td class="FieldLabel">Initial Rate Term</td>
                <td><asp:textbox id=sRAdj1stCapMon width="37px" runat="server" onchange="refreshCalculation();" maxlength="3" /> mths</td>
                
                <td class="FieldLabel"><a id="QualRateCalcLink">Qual Rate</a></td>
                <td>
                    <ml:percenttextbox id="sQualIR" width="55" runat="server" onchange="refreshCalculation();" preset="percent" />
                    <asp:CheckBox ID="sQualIRLckd" runat="server" Text="Lock" onclick="refreshCalculation();" />
                </td>
                <td class="FieldLabel"></td>
                <td></td>
                
            </tr>            
            <tr>
                <td class="FieldLabel">Initial Rate Type</td>
                <td><asp:DropDownList ID="sInitialRateT" runat="server" /></td>
                <td class="FieldLabel"></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>

        <table style="white-space: nowrap;">
            <tr class="FieldLabel">
                <td colspan="2"></td>
                <td>Balance</td>
                <td></td>
                <td id="RateHeaderQual">Rate</td>
                <td><span id="PercentHeaderQual">Percentage</span></td>
                <td id="PercentHeaderSpacerQual"></td>
                <td><span id="TermHeaderQual">Term</span></td>
                <td colspan="2"></td>
                <td>Fixed Amount</td>
                <td></td>
                <td>Payment</td>
            </tr>
            <tr>
                <td class="FieldLabel">Qualifying payment</td>
                <td><asp:DropDownList runat="server" ID="sHelocQualPmtFormulaT" onchange="refreshCalculation();"/></td>
                <td><asp:DropDownList runat="server" id="sHelocQualPmtBaseT" onchange="refreshCalculation();"/></td>
                <td><ml:moneytextbox id="sHelocQualPmtBaseAmt" width="80" runat="server" preset="money" readonly="true"/></td>
                <td id="RateCellQual"><asp:DropDownList runat="server" ID="sHelocQualPmtFormulaRateT" onchange="refreshCalculation();"/></td>
                <td><ml:percenttextbox id="sHelocQualPmtPcBase" width="55" runat="server" onchange="refreshCalculation();" preset="percent" /></td>
                <td id="PercentSpacerQual"></td>
                <td><span id="InterestOnlyDivisorQual">/ 12</span><asp:DropDownList runat="server" ID="sHelocQualPmtAmortTermT" onchange="refreshCalculation();"/></td>
                <td><asp:textbox id="sHelocQualPmtAmortTerm" width="37px" runat="server" onchange="refreshCalculation();" maxlength="3" ReadOnly="true" /></td>
                <td>+</td>
                <td><ml:moneytextbox id="sHelocQualPmtMb" width="80" runat="server" onchange="refreshCalculation();" preset="money" /></td>
                <td>=</td>
                <td><ml:moneytextbox id="sProThisMQual" width="80" runat="server" preset="money"  readonly="true"/></td>
            </tr>
            <tr class="FieldLabel">
                <td colspan="2"></td>
                <td>Balance</td>
                <td></td>
                <td id="RateHeader">Rate</td>
                <td><span id="PercentHeader">Percentage</span></td>
                <td id="PercentHeaderSpacer"></td>
                <td><span id="TermHeader">Term</span></td>
                <td colspan="2"></td>
                <td>Fixed Amount</td>
                <td></td>
                <td>Payment</td>
            </tr>
            <tr>
                <td class="FieldLabel">Display payment</td>
                <td><asp:DropDownList runat="server" ID="sHelocPmtFormulaT" onchange="refreshCalculation();"/></td>
                <td><asp:DropDownList runat="server" id="sHelocPmtBaseT" onchange="refreshCalculation();"/></td>
                <td><ml:moneytextbox id="sHelocPmtBaseAmt" width="80" runat="server" preset="money" readonly="true"/></td>
                <td id="RateCell"><asp:DropDownList runat="server" ID="sHelocPmtFormulaRateT" onchange="refreshCalculation();"/></td>
                <td><ml:percenttextbox id="sHelocPmtPcBase" width="55" runat="server" onchange="refreshCalculation();" preset="percent" /></td>
                <td id="PercentSpacer"></td>
                <td><span id="InterestOnlyDivisor">/ 12</span><asp:DropDownList runat="server" ID="sHelocPmtAmortTermT" onchange="refreshCalculation();"/></td>
                <td><asp:textbox id="sHelocPmtAmortTerm" width="37px" runat="server" onchange="refreshCalculation();" maxlength="3" ReadOnly="true" /></td>
                <td>+</td>
                <td><ml:moneytextbox id="sHelocPmtMb" width="80" runat="server" onchange="refreshCalculation();" preset="money" /></td>
                <td>=</td>
                <td><ml:moneytextbox id="sProThisMPmt" width="80" runat="server" preset="money"  readonly="true"/></td>
            </tr>
        </table>
        <div class="SubHeader">HELOC Terms, Conditions, and Fees</div>
        <table>
            <tr>
                <td class="FieldLabel">HELOC Annual Fee</td>
                <td><ml:moneytextbox id=sHelocAnnualFee width="80" runat="server" onchange="refreshCalculation();" preset="money" /></td>
                <td>&nbsp;</td>
                <td class="FieldLabel">HELOC Termination Fee Period</td>
                <td><asp:textbox id=sHelocTerminationFeePeriod width="37px" runat="server" onchange="refreshCalculation();" maxlength="3" /> mths</td>
            </tr>
            <tr>
                <td class="FieldLabel">HELOC Draw Fee</td>
                <td><ml:moneytextbox id=sHelocDrawFee width="80" runat="server" onchange="refreshCalculation();" preset="money" /></td>
                <td>&nbsp;</td>
                <td class="FieldLabel">HELOC Termination Fee</td>
                <td><ml:moneytextbox id=sHelocTerminationFee width="80" runat="server" onchange="refreshCalculation();" preset="money" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">HELOC Minimum Advance Amt</td>
                <td><ml:moneytextbox id=sHelocMinimumAdvanceAmt width="80" runat="server" onchange="refreshCalculation();" preset="money" /></td>
                <td>&nbsp;</td>
                <td class="FieldLabel">HELOC Minimum Payment</td>
                <td><ml:moneytextbox id=sHelocMinimumPayment width="80" runat="server" onchange="refreshCalculation();" preset="money" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">HELOC Returned Check Fee</td>
                <td><ml:moneytextbox id=sHelocReturnedCheckFee width="80" runat="server" onchange="refreshCalculation();" preset="money" /></td>
                <td>&nbsp;</td>
                <td class="FieldLabel">HELOC Minimum Payment %</td>
                <td><ml:percenttextbox id=sHelocMinimumPaymentPc width="55" runat="server" onchange="refreshCalculation();" preset="percent" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">HELOC Stop Payment Fee</td>
                <td><ml:moneytextbox id=sHelocStopPaymentFee width="80" runat="server" onchange="refreshCalculation();" preset="money" /></td>
                <td>&nbsp;</td>
                <td class="FieldLabel">Calculate and collect prepaid interest</td>
                <td><input type="checkbox" id="sHelocCalculatePrepaidInterest" runat="server" /></td>
            </tr>                                                
        </table>
        <div class="SubHeader">First Lien Recording Information</div>
        <table>
            <tr>
            <td class="FieldLabel">First Lien Trustee</td>
            <td><asp:TextBox ID="sFirstLienTrusteeNm" runat="server"/></td>
            </tr>
                        <tr>
            <td class="FieldLabel">First Lien Holder</td>
            <td><asp:TextBox ID="sFirstLienHolderNm" runat="server" /></td>
            </tr>
                        <tr>
            <td class="FieldLabel">First Lien Recording Date</td>
            <td><ml:datetextbox ID="sFirstLienRecordingD" runat="server" preset="date" /></td>
            </tr>
                        <tr>
            <td class="FieldLabel">First Lien Recording Book #</td>
            <td class="FieldLabel"><asp:TextBox ID="sFirstLienRecordingBookNum" runat="server" />&nbsp;&nbsp;
            Page # <asp:TextBox ID="sFirstLienRecordingPageNum" runat="server" />&nbsp;&nbsp;
            Instrument # <asp:TextBox ID="sFirstLienRecordingInstrumentNum" runat="server" />
            </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
