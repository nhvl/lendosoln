﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.ObjLib.DocMagicLib;
using DataAccess;
using LendersOffice.Constants;
using DataAccess.LoanValidation;
using LendersOffice.Integration.DocumentVendor;

namespace LendersOfficeApp.newlos
{
    public partial class SelectDocMagicPlan : BasePage
    {
        protected Guid LoanID
        {
            get { return RequestHelper.LoanID; }
        }

   		private String CommandToDo
   		{
   			set
   			{
   				ClientScript.RegisterHiddenField( "m_commandToDo" , value );
   			}
   		}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!DocumentVendorFactory.CurrentVendors().Any(a => a.Skin.AutomaticPlanCodes))
            {
                CommandToDo = "Close";
                return;
            }

            if (Page.IsPostBack == false)
            {
                // OPM 83183. Must make sure this loan's data is still in good shape.
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SelectDocMagicPlan));
                dataLoan.InitLoad();
                LoanValidationResultCollection validationResultCollection = dataLoan.ValidateForRunningDocMagicPlanCode();
                if (validationResultCollection.ErrorCount != 0)
                {
                    // Data is not in state where we can determine plan code.
                    m_selectionView.ActiveViewIndex = 1;
                    m_warningList.DataSource = validationResultCollection.ErrorList;
                    m_warningList.DataBind();
                }
                else
                {
                    m_selectionView.ActiveViewIndex = 0;
                    
                    // Make sure we are up to date with latest code result
                    List<DocMagicPlanCode> codes = DocMagicPlanCodeEvaluator.CalculateApplicablePlanCodes(LoanID);

                    if (codes.Count == 0)
                    {
                        // Nothing is applicable. This should almost never happen.
                        // Allow user to choose from list of generic codes.
                        codes = DocMagicPlanCode.ListDocMagicPlanCodes(dataLoan.BrokerDB, null, true);
                    }

                    if (codes.Count == 0)
                    {
                        Tools.LogError("There are absolutely no plan codes for " + LoanID + " should we show an error page?");
                    }

                    m_planCodes.DataSource = codes;
                    m_planCodes.DataTextField = "PlanCodeDesc";
                    m_planCodes.DataValueField = "PlanCodeId";
                    m_planCodes.DataBind();
                }
            }
        }

        protected void SaveClick(object sender, System.EventArgs a)
        {
            Guid selectedPlanCodeID;
            try
            {
                selectedPlanCodeID = new Guid(m_planCodes.SelectedValue);

                DocMagicPlanCode.SetPlanCode(null, LoanID, selectedPlanCodeID);

                CommandToDo = "Close";
            }
            catch (FormatException fe)
            {
                Tools.LogError("selected plancode value was: " + m_planCodes.SelectedValue, fe);
                throw;
            }
            
        }

    }
}
