﻿namespace LendersOfficeApp.newlos
{
    using System;

    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;

    public partial class PropertyDetailsUserControl : LendersOfficeApp.newlos.BaseLoanUserControl, IAutoLoadUserControl
    {
        public void LoadData()
        {
            Tools.Bind_sGseSpT(sGseSpT);
            Tools.Bind_TriState(this.sHomeIsMhAdvantageTri);

            Tools.Bind_sLoanBeingRefinancedAmortizationT(this.sLoanBeingRefinancedAmortizationT);
            Tools.Bind_sLoanBeingRefinancedLienPosT(this.sLoanBeingRefinancedLienPosT);
            Tools.Bind_sSpProjectClassFreddieT(sSpProjectClassFreddieT);
            Tools.Bind_sSpProjectClassFannieT(sSpProjectClassFannieT);
            Tools.Bind_sSpProjectStatusT(sSpProjectStatusT);
            Tools.Bind_sSpProjectAttachmentT(sSpProjectAttachmentT);
            Tools.Bind_sSpProjectDesignT(sSpProjectDesignT);
            Tools.Bind_sSpValuationMethodT(sSpValuationMethodT);
            Tools.Bind_sSpAppraisalFormT(sSpAppraisalFormT);
            Tools.Bind_CuRiskT(this.sSpOvervaluationRiskT);
            Tools.Bind_CuRiskT(this.sSpPropertyEligibilityRiskT);
            Tools.Bind_CuRiskT(this.sSpAppraisalQualityRiskT);
            Tools.Bind_sSpAvmModelT(sSpAvmModelT);
            Tools.Bind_sSpGseRefiProgramT(sSpGseRefiProgramT);
            Tools.Bind_sSpInvestorCurrentLoanT(sSpInvestorCurrentLoanT);
            Tools.Bind_sSpGseCollateralProgramT(sSpGseCollateralProgramT);

            Tools.Bind_sManufacturedHomeConditionT(sManufacturedHomeConditionT);
            Tools.Bind_sManufacturedHomeCertificateofTitleT(sManufacturedHomeCertificateofTitleT);
            Tools.Bind_sCooperativeFormOfOwnershipT(sCooperativeFormOfOwnershipT);

            Tools.Bind_TriState(sManufacturedHomeAttachedToFoundation);

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(PropertyDetailsUserControl));
            dataLoan.InitLoad();

            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Text = dataLoan.sSpZip.TrimWhitespaceAndBOM();

            Tools.SetDropDownListValue(sGseSpT, dataLoan.sGseSpT);
            Tools.SetDropDownListValue(this.sHomeIsMhAdvantageTri, dataLoan.sHomeIsMhAdvantageTri);

            sIsNewConstruction.Checked = dataLoan.sIsNewConstruction;

            sUnitsNum.Text = dataLoan.sUnitsNum_rep;
            sProjNm.Text = dataLoan.sProjNm;
            sCpmProjectId.Text = dataLoan.sCpmProjectId;
            sSpUnit1BedroomsCount.Text = dataLoan.sSpUnit1BedroomsCount_rep;
            sSpUnit2BedroomsCount.Text = dataLoan.sSpUnit2BedroomsCount_rep;
            sSpUnit3BedroomsCount.Text = dataLoan.sSpUnit3BedroomsCount_rep;
            sSpUnit4BedroomsCount.Text = dataLoan.sSpUnit4BedroomsCount_rep;
            sSpUnit1EligibleRent.Text = dataLoan.sSpUnit1EligibleRent_rep;
            sSpUnit2EligibleRent.Text = dataLoan.sSpUnit2EligibleRent_rep;
            sSpUnit3EligibleRent.Text = dataLoan.sSpUnit3EligibleRent_rep;
            sSpUnit4EligibleRent.Text = dataLoan.sSpUnit4EligibleRent_rep;
            sSpProjectDwellingUnitCount.Text = dataLoan.sSpProjectDwellingUnitCount_rep;
            sSpProjectDwellingUnitSoldCount.Text = dataLoan.sSpProjectDwellingUnitSoldCount_rep;
            sSpValuationEffectiveD.Text = dataLoan.sSpValuationEffectiveD_rep;
            sSpAppraisalId.Text = dataLoan.sSpAppraisalId;
            sSpSubmittedToUcdpD.Text = dataLoan.sSpSubmittedToUcdpD_rep;
            sSpGseLoanNumCurrentLoan.Text = dataLoan.sSpGseLoanNumCurrentLoan;
            sFhaEndorsementD.Text = dataLoan.sFhaEndorsementD_rep;
            sFhaUfmipAmt.Text = dataLoan.sFhaUfmipAmt_rep;

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V11_ConsolidateFHACaseNumberFields_Opm186897))
            {
                sFHAPreviousCaseNum.Text = dataLoan.sFHAPreviousCaseNum;
                sSpFhaCaseNumCurrentLoan.Visible = false;
            }
            else
            {
                sSpFhaCaseNumCurrentLoan.Text = dataLoan.sSpFhaCaseNumCurrentLoan;
                sFHAPreviousCaseNum.Visible = false;
            }

            Tools.SetDropDownListValue(sSpProjectClassFreddieT, dataLoan.sSpProjectClassFreddieT);
            Tools.SetDropDownListValue(sSpProjectClassFannieT, dataLoan.sSpProjectClassFannieT);
            Tools.SetDropDownListValue(sSpProjectStatusT, dataLoan.sSpProjectStatusT);
            Tools.SetDropDownListValue(sSpProjectAttachmentT, dataLoan.sSpProjectAttachmentT);
            Tools.SetDropDownListValue(sSpProjectDesignT, dataLoan.sSpProjectDesignT);
            Tools.SetDropDownListValue(sSpValuationMethodT, dataLoan.sSpValuationMethodT);
            Tools.SetDropDownListValue(sSpAppraisalFormT, dataLoan.sSpAppraisalFormT);

            this.sSpCuScore.Text = dataLoan.sSpCuScore_rep;
            Tools.SetDropDownListValue(this.sSpOvervaluationRiskT, dataLoan.sSpOvervaluationRiskT);
            Tools.SetDropDownListValue(this.sSpPropertyEligibilityRiskT, dataLoan.sSpPropertyEligibilityRiskT);
            Tools.SetDropDownListValue(this.sSpAppraisalQualityRiskT, dataLoan.sSpAppraisalQualityRiskT);

            Tools.SetDropDownListValue(sSpAvmModelT, dataLoan.sSpAvmModelT);
            Tools.SetDropDownListValue(sSpGseRefiProgramT, dataLoan.sSpGseRefiProgramT);
            Tools.SetDropDownListValue(sSpInvestorCurrentLoanT, dataLoan.sSpInvestorCurrentLoanT);  // page sets it according to refi program if it's different.
            Tools.SetDropDownListValue(sSpGseCollateralProgramT, dataLoan.sSpGseCollateralProgramT);            

            sManufacturedHomeMake.Text = dataLoan.sManufacturedHomeMake;
            sManufacturedHomeModel.Text = dataLoan.sManufacturedHomeModel;
            sManufacturedHomeYear.Text = dataLoan.sManufacturedHomeYear;
            Tools.SetDropDownListValue(sManufacturedHomeConditionT, dataLoan.sManufacturedHomeConditionT);
            sManufacturedHomeLengthFeet.Text = dataLoan.sManufacturedHomeLengthFeet_rep;
            sManufacturedHomeWidthFeet.Text = dataLoan.sManufacturedHomeWidthFeet_rep;
            sManufacturedHomeManufacturer.Text = dataLoan.sManufacturedHomeManufacturer;
            sManufacturedHomeSerialNumber.Text = dataLoan.sManufacturedHomeSerialNumber;
            sManufacturedHomeHUDLabelNumber.Text = dataLoan.sManufacturedHomeHUDLabelNumber;
            Tools.SetDropDownListValue(sManufacturedHomeAttachedToFoundation, dataLoan.sManufacturedHomeAttachedToFoundation);
            sManufacturedHomeCertificateofTitle.Text = dataLoan.sManufacturedHomeCertificateofTitle;
            Tools.SetDropDownListValue(sManufacturedHomeCertificateofTitleT, dataLoan.sManufacturedHomeCertificateofTitleT);
            sCooperativeApartmentNumber.Text = dataLoan.sCooperativeApartmentNumber;
            sCooperativeNumberOfShares.Text = dataLoan.sCooperativeNumberOfShares_rep;
            sCooperativeLeaseD.Text = dataLoan.sCooperativeLeaseD_rep;
            sCooperativeLeaseAssignD.Text = dataLoan.sCooperativeLeaseAssignD_rep;
            sCooperativeLienSearchD.Text = dataLoan.sCooperativeLienSearchD_rep;
            sCooperativeLienSearchNumber.Text = dataLoan.sCooperativeLienSearchNumber;
            Tools.SetDropDownListValue(sCooperativeFormOfOwnershipT, dataLoan.sCooperativeFormOfOwnershipT);
            sCooperativeStockCertNumber.Text = dataLoan.sCooperativeStockCertNumber;
            sLeaseholdLeaseD.Text = dataLoan.sLeaseholdLeaseD_rep;
            sLeaseHoldExpireD.Text = dataLoan.sLeaseHoldExpireD_rep;
            sLeaseholdRecordD.Text = dataLoan.sLeaseholdRecordD_rep;
            sLeaseHoldInstrumentNumber.Text = dataLoan.sLeaseHoldInstrumentNumber;
            sLeaseholdHolderName.Text = dataLoan.sLeaseholdHolderName;
            sDeedGeneratedD.Text = dataLoan.sDeedGeneratedD_rep;
            sPurchaseContractDate.Text = dataLoan.sPurchaseContractDate_rep;
            sFinancingContingencyExpD.Text = dataLoan.sFinancingContingencyExpD_rep;
            sFinancingContingencyExtensionExpD.Text = dataLoan.sFinancingContingencyExtensionExpD_rep;

            Tools.SetDropDownListValue(this.sLoanBeingRefinancedAmortizationT, dataLoan.sLoanBeingRefinancedAmortizationT);
            Tools.SetDropDownListValue(this.sLoanBeingRefinancedLienPosT, dataLoan.sLoanBeingRefinancedLienPosT);
            this.sLoanBeingRefinancedInterestRate.Text = dataLoan.sLoanBeingRefinancedInterestRate_rep;
            this.sLoanBeingRefinancedRemainingTerm.Text = dataLoan.sLoanBeingRefinancedRemainingTerm_rep;
            this.sLoanBeingRefinancedTotalOfPayments.Value = dataLoan.sLoanBeingRefinancedTotalOfPayments_rep;
            this.sLoanBeingRefinancedLtvR.Text = dataLoan.sLoanBeingRefinancedLtvR_rep;
            this.sLoanBeingRefinancedUsedToConstructAlterRepair.Checked = dataLoan.sLoanBeingRefinancedUsedToConstructAlterRepair;

            if(dataLoan.sLT == E_sLT.FHA)
            {
                AppraisalSection.Attributes.Add("class", AppraisalSection.Attributes["class"] + " fha");
                sApprVal.Text = dataLoan.sApprVal_rep;
                sSpFhaDocId.Text = dataLoan.sSpFhaDocId;
                sSpSubmittedToFhaD.Text = dataLoan.sSpSubmittedToFhaD_rep;

            }

            switch (dataLoan.sGseSpT)
            {
                case E_sGseSpT.LeaveBlank:
                    break;
                case E_sGseSpT.Attached:
                    break;
                case E_sGseSpT.Condominium:
                    break;
                case E_sGseSpT.Cooperative:
                    CooperativePanel.Style["display"] = "inherit";
                    break;
                case E_sGseSpT.Detached:
                    break;
                case E_sGseSpT.DetachedCondominium:
                    break;
                case E_sGseSpT.HighRiseCondominium:
                    break;
                case E_sGseSpT.ManufacturedHomeCondominium:
                case E_sGseSpT.ManufacturedHomeMultiwide:
                case E_sGseSpT.ManufacturedHousingSingleWide:
                case E_sGseSpT.ManufacturedHousing:
                    ManufacturedHousingPanel.Style["display"] = "inherit";
                    break;
                case E_sGseSpT.Modular:
                    break;
                case E_sGseSpT.PUD:
                    break;
                default:
                    throw new UnhandledEnumException(dataLoan.sGseSpT);
            }

        }

        public void SaveData()
        {

        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            sSpZip.SmartZipcode(sSpCity, sSpState);
        }

        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);

        }
    }
}