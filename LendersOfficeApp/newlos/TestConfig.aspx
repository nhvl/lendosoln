﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestConfig.aspx.cs" Inherits="LendersOfficeApp.newlos.TestConfig" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Test Configuration</title>
</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
        <div>
            <table width="980px">
                <tr>
                    <td class="MainRightHeader">
                        Test Configuration
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="2" cellpading="0" width="98%" border="0">
                            <tr>
                                <td class="FieldLabel">
                                    Test Environment
                                </td>
                                <td>
                                    <asp:DropDownList ID="sTestLoanFileEnvironmentId" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
