﻿namespace LendersOfficeApp.newlos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.ConversationLog;
    using LendersOffice.ObjLib.Security.Authorization.ConversationLog;
    using LendersOffice.ObjLib.Views;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.ConversationLog;    

    /// <summary>
    /// Page that displays the ability to post new associated with the loan.
    /// </summary>
    public partial class ConversationLogNewPost : BaseLoanPage
    {
        /// <summary>
        /// PageInit function.
        /// </summary>
        /// <param name="sender">Control that calls PageInit.</param>
        /// <param name="e">System event arguments.</param>
        protected void PageInit(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            this.DisplayCopyRight = false;
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("mask.js");

            this.PageID = "ConversationLogNewPost";
            
            this.UseNewFramework = true;
            this.IsAppSpecific = false;
            // no need to refresh the summary info.
            this.IsRefreshLoanSummaryInfo = false;

            this.RegisterService("newPostService", "/newlos/ConversationLogService.aspx");

            var securityToken = SecurityService.CreateToken();

            var loan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(ConversationLog));
            loan.InitLoad();
            var identifier = ResourceIdentifier.CreateForLoanFile(securityToken.BrokerId, loan.sLRefNm);
            if (identifier == null)
            {
                throw new LqbGrammar.Exceptions.ServerException(ErrorMessage.SystemError);
            }

            var resourceId = new ResourceId()
            {
                Type = ResourceType.LoanFile,
                Identifier = identifier.Value
            };

            this.RegisterJsGlobalVariables("resourceString", identifier.Value.ToString());
            
            this.RegisterJsGlobalVariables("enableConversationLogPermissions", ConstStage.EnableConversationLogPermissions);
            if (ConstStage.EnableConversationLogPermissions)
            {
                var permissionLevels = ConversationLogPermissionLevel.GetOrderedPermissionLevelsForBrokerIncludingDefault(securityToken.BrokerId);
                var viewOfPermissionLevels = permissionLevels.Select(p => ViewOfPermissionLevelForLoUser.FromPermissionLevel(securityToken, p)).ToList();
                this.RegisterJsObjectWithJsonNetSerializer("permissionLevels", viewOfPermissionLevels);
            }
            else
            {
                this.RegisterJsObjectWithJsonNetSerializer("permissionLevels", new List<ViewOfPermissionLevelForLoUser>());
            }
        }

        /// <summary>
        /// Loads the relevant models and registers them in the UI initialization code.
        /// </summary>
        protected override void LoadData()
        {
            var securityToken = SecurityService.CreateToken();
            var allCategories = ConversationLogHelper.GetAllCategories(securityToken, ConstStage.ConversationLogIP, ConstStage.ConversationLogPort);
            
            var viewOfCategories = allCategories
                .Where(c => c.IsActive)
                .Select(c => new ViewOfCategoryForLoUser(c))
                .ToList();

            this.RegisterJsObjectWithJsonNetSerializer("categories", viewOfCategories);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            IsAppSpecific = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion
    }
}