using System;
using System.Collections.Specialized;

using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;
using LqbGrammar.DataTypes;

namespace LendersOfficeApp.newlos
{
	public abstract class AbstractSingleEditService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected int sFileVersion
        {
            get { return GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck); }
        }
		/// <summary>
		/// Get args["loanid"]. Will throw exception if loanid does not existed.
		/// </summary>
		protected Guid LoanID 
		{
			get { return GetGuid("loanid"); }
		}

		/// <summary>
		/// Get args["applicationid"]. Will throw exception if applicationid does not existed.
		/// </summary>
		protected Guid ApplicationID 
		{
			get { return GetGuid("applicationid"); }
		}

		/// <summary>
		/// Get args["recordid"]. Return Guid.Empty if recordid does not existed.
		/// </summary>
		protected Guid RecordID 
		{
            get { return GetGuid("recordid", Guid.Empty); }
		}

		/// <summary>
		/// Get args["nextrecordid"]. Return Guid.Empty if recordid does not existed.
		/// </summary>
		protected Guid NextRecordID 
		{
            get { return GetGuid("nextrecordid", Guid.Empty); }
		}

        /// <summary>
        /// Gets whether the record is a Ulad record or not.
        /// </summary>
        protected bool IsUlad
        {
            get { return this.ApplicationID == Guid.Empty; }
        }

		protected override void Process(string methodName) 
		{
			switch (methodName) 
			{
				case "LoadData":
					LoadData();
                    break;
				case "SaveData":
					SaveData();
                    break;
				case "SaveDataAndLoadNext":
					SaveDataAndLoadNext();
                    break;
				case "CalculateData":
					CalculateData();
                    break;
                case "DeleteRecord":
                    DeleteRecord();
                    break;
                case "MoveRecord":
                    MoveRecord();
                    break;
			}
			CustomProcess(methodName);
		}

        /// <summary>
        /// Override this method if you need to handle more method name beside "LoadData", "SaveData" etc..
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        protected virtual void CustomProcess(string methodName) 
        {
        }

		protected abstract void LoadData();
		protected abstract void SaveData();
		protected abstract void SaveDataAndLoadNext();
		protected virtual  void CalculateData()
		{
			throw new  CBaseException(ErrorMessages.Generic, "This page service component must implement CalculateData() method." );
		}
        protected virtual void DeleteRecord() 
        {
        }
        protected virtual void MoveRecord() 
        {
        }

        protected UnzonedDate? CreateUnzonedDateFromArgs(string fieldId)
        {
            var dateString = GetString(fieldId);
            if(string.IsNullOrEmpty(dateString))
            {
                return null;
            }

            var date = DateTime.Parse(dateString);
            return UnzonedDate.Create(date);
        }
	}


    /// <summary>
    /// Only inherit this class if you are using it for edit borrower liability/assets/REO/employment record.
    /// </summary>
    public abstract class AbstractSingleEditService2 : LendersOffice.Common.BaseSimpleServiceXmlPage 
    {
        protected int sFileVersion
        {
            get { return GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck); }
        }
        protected override bool PerformResultOptimization
        {
            get
            {
                return false;
            }
        }

        protected abstract CPageData DataLoan { get; }

        #region "PROPERTIES"
        /// <summary>
        /// Get args["loanid"]. Will throw exception if loanid does not existed.
        /// </summary>
        protected Guid LoanID 
        {
            get { return GetGuid("loanid"); }
        }

        /// <summary>
        /// Get args["applicationid"]. Will throw exception if applicationid does not existed.
        /// </summary>
        protected Guid ApplicationID 
        {
            get { return GetGuid("applicationid"); }
        }

        /// <summary>
        /// Get args["recordid"]. Return Guid.Empty if recordid does not existed. Use this ID for all saving activity.
        /// </summary>
        protected Guid RecordID 
        {
            get { return GetGuid("recordid", Guid.Empty); }
        }

        /// <summary>
        /// Get args["nextrecordid"]. Return Guid.Empty if recordid does not existed. Use this ID for all retrieving activity.
        /// </summary>
        protected Guid NextRecordID 
        {
            get { return GetGuid("nextrecordid", Guid.Empty); }
        }
        #endregion 

        protected override void Process(string methodName) 
        {
            CPageData dataLoan = DataLoan;
            switch (methodName) 
            {
                case "SaveData":
                    dataLoan.InitSave(sFileVersion);
                    SaveData(dataLoan);
                    dataLoan.Save();
                    SetResult("sFileVersion", dataLoan.sFileVersion);
                    break;
                case "LoadData":
                    dataLoan.InitLoad();
                    LoadData(dataLoan);
                    break;
                case "SaveDataAndLoadData":
                    dataLoan.InitSave(sFileVersion);
                    SaveData(dataLoan);
                    dataLoan.Save();
                    SetResult("sFileVersion", dataLoan.sFileVersion);
                    LoadData(dataLoan);
                    break;
                case "CalculateData":
                    dataLoan.InitLoad();
                    SaveData(dataLoan);
                    
                    LoadData(dataLoan);
                    break;
                case "DeleteRecord":
                    dataLoan.InitSave(sFileVersion);
                    DeleteRecord(dataLoan); // Delete Record always invoke dataLoan.Save();
                    SetResult("sFileVersion", dataLoan.sFileVersion);
                    break;
                case "SaveDataAndDeleteRecord":
                    dataLoan.InitSave(sFileVersion);
                    SaveData(dataLoan);
                    DeleteRecord(dataLoan); // Delete Record always invoke dataLoan.Save();
                    SetResult("sFileVersion", dataLoan.sFileVersion);
                    break;
                case "InsertRecord":
                    dataLoan.InitSave(sFileVersion);
                    InsertRecord(dataLoan);  // InsertRecord always invoke dataLoan.Save();
                    SetResult("sFileVersion", dataLoan.sFileVersion);
                    break;
                case "SaveDataAndInsertRecord":
                    dataLoan.InitSave(sFileVersion);
                    SaveData(dataLoan);
                    InsertRecord(dataLoan); // InsertRecord always invoke dataLoan.Save();
                    SetResult("sFileVersion", dataLoan.sFileVersion);
                    break;
                case "MoveRecord":
                    dataLoan.InitSave(sFileVersion);
                    MoveRecord(dataLoan); // MoveRecord always invoke dataLoan.Save();
                    SetResult("sFileVersion", dataLoan.sFileVersion);
                    break;
                case "SaveDataAndMoveRecord":
                    dataLoan.InitSave(sFileVersion);
                    SaveData(dataLoan);
                    MoveRecord(dataLoan); // MoveRecord always invoke dataLoan.Save();
                    SetResult("sFileVersion", dataLoan.sFileVersion);
                    break;
                case "Sort":
                    dataLoan.InitSave(sFileVersion);
                    Sort(dataLoan);
                    SetResult("sFileVersion", dataLoan.sFileVersion);
                    break;
                case "SaveDataAndSort":
                    dataLoan.InitSave(sFileVersion);
                    SaveData(dataLoan);

                    Sort(dataLoan);
                    SetResult("sFileVersion", dataLoan.sFileVersion);
                    break;


            }
            CustomProcess(methodName);
        }
        /// <summary>
        /// Override this method if you need to handle more method name beside "LoadData", "SaveData" etc..
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        protected virtual void CustomProcess(string methodName) 
        {
        }

        private void SaveData(CPageData dataLoan) 
        {
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            var recordList = LoadRecordList(dataLoan, dataApp);
            // Do not load regular record when recordID is Guid.Empty
			var record = RecordID == Guid.Empty ? null : recordList.GetRegRecordOf(RecordID);

            SaveData(dataLoan, dataApp, recordList, record);

            recordList.Flush();
        }
        private void LoadData(CPageData dataLoan) 
        {
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            var recordList = LoadRecordList(dataLoan, dataApp);
            // Do not load regular record when recordID is Guid.Empty

            var record = NextRecordID == Guid.Empty ? null : recordList.GetRegRecordOf(NextRecordID);

            LoadData(dataLoan, dataApp, record);
        }
        private void InsertRecord(CPageData dataLoan) 
        {
            int rowIndex = GetInt("RowIndex", -1);

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            var recordList = LoadRecordList(dataLoan, dataApp);
            
            ICollectionItemBase2 record = null;
            if (rowIndex != -1)
                record = recordList.AddRegularRecordAt(rowIndex);
            else
                record = recordList.AddRegularRecord();

            dataLoan.Save(); // Need to save data before getting calculate value.

            LoadData(dataLoan, dataApp, record);

        }
        private void MoveRecord(CPageData dataLoan) 
        {
            int direction = GetInt("Direction"); // -1 - Down, 1 - Up.

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            var recordList = LoadRecordList(dataLoan, dataApp);
            var record = recordList.GetRegRecordOf(RecordID);

            if (direction == -1)
                record.MoveDown();
            else if (direction == 1)
                record.MoveUp();

            dataLoan.Save();

            LoadData(dataLoan, dataApp, record);
        }
        private void DeleteRecord(CPageData dataLoan) 
        {
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            var recordList = LoadRecordList(dataLoan, dataApp);
            var record = recordList.GetRegRecordOf(RecordID);

            record.IsOnDeathRow = true;
            
            dataLoan.Save();

            record = NextRecordID == Guid.Empty ? null : recordList.GetRegRecordOf(NextRecordID);
            LoadData(dataLoan, dataApp, record);
        }

        private void Sort(CPageData dataLoan) 
        {
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            string sortExpression = GetString("SortExpression", null); // SortExpression is in format {fieldname} {ASC | DESC}

            if (sortExpression != null) 
            {
                string[] parts = sortExpression.Split(' ');
                string fieldName = parts[0];
                bool isAsc = (parts.Length > 1 && parts[1].ToUpper() == "DESC") ? false : true; // Default isAsc=true
                var recordList = LoadRecordList(dataLoan, dataApp);
            
                recordList.SortRegularRecordsByKey(fieldName, isAsc);
                dataLoan.Save();
            }

        }


        /// <summary>
        /// Return all special records value, calculated values and regular record.
        /// </summary>
        /// <param name="dataLoan"></param>
        /// <param name="dataApp"></param>
        /// <param name="record">if null then only load special records and calculated value.</param>
        /// <returns></returns>
        protected abstract void LoadData(CPageData dataLoan, CAppData dataApp, ICollectionItemBase2 record) ;
        protected abstract IRecordCollection LoadRecordList(CPageData dataLoan, CAppData dataApp);
        /// <summary>
        /// Save special records value and regular record.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="dataLoan"></param>
        /// <param name="dataApp"></param>
        /// <param name="recordList"></param>
        /// <param name="record">Regular record can be NULL</param>
        protected abstract void SaveData(CPageData dataLoan, CAppData dataApp, IRecordCollection recordList, ICollectionItemBase2 record);


    }
}
