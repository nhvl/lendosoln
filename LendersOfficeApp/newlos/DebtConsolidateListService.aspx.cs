namespace LendersOfficeApp.newlos
{
    using System;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;

    public class DebtConsolidateListServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {

        private bool IsPmlClient 
        {
            get 
            {
                return LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
            }
        }
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(DebtConsolidateListServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            int count = GetInt("count");

            dataLoan.sLAmtLckd = GetBool("sLAmtLckd");
            dataLoan.sLAmtCalc_rep = GetString("sLAmtCalc");
            dataLoan.sNoteIR_rep = GetString("sNoteIR");
            dataLoan.sTerm_rep = GetString("sTerm");
            dataLoan.sDue_rep = GetString("sDue");
            dataLoan.sIOnlyMon_rep = GetString("sIOnlyMon");
            if (IsPmlClient) 
            {
                dataLoan.sProdCashoutAmt_rep = GetString("sProdCashoutAmt");
            }
            dataLoan.sTransNetCash_rep = GetString("sTransNetCash");
            dataLoan.sTransNetCashLckd = GetBool("sTransNetCashLckd");
            dataLoan.sLPurposeT = (E_sLPurposeT) GetInt("sLPurposeT");

            ILiaCollection liaColl = dataApp.aLiaCollection;
            for (int i = 0; i < count; i++) 
            {
                Guid recordId = GetGuid("RecordId_" + i);
                try 
                {
                    ILiabilityRegular liaReg = liaColl.GetRegRecordOf(recordId);

                    string willBePdOff = GetString("WillBePdOff_" + i);
                    string usedInRatio = GetString("UsedInRatio_" + i);

                    if (willBePdOff != "") 
                    {
                        liaReg.WillBePdOff = willBePdOff == "True";
                    }
                    if (usedInRatio != "") 
                    {
                        liaReg.NotUsedInRatio = usedInRatio == "False";
                    }

                } 
                catch (CBaseException) 
                {
                    // 12/18/2006 dd - Record could not be found in Xml. Reason, other user already remove it.
                    // TODO: Need to modify GetRegRecordOf to throw more specific exception when record not found.
                    CBaseException exc = new CBaseException(ErrorMessages.LiablityListModifiedError, ErrorMessages.LiablityListModifiedError);
                    exc.IsEmailDeveloper = false;

                    throw exc;
                }
            }

            #region Save special records
            var alimony = liaColl.GetAlimony(true);
            alimony.OwedTo = GetString("Alimony_ComNm");
            alimony.Pmt_rep = GetString("Alimony_Pmt");
            alimony.RemainMons_rep = GetString("Alimony_RemainMons");
            alimony.NotUsedInRatio = GetBool("Alimony_NotUsedInRatio");

            var childSupport = liaColl.GetChildSupport(true);
            childSupport.OwedTo = GetString("ChildSupport_ComNm");
            childSupport.Pmt_rep = GetString("ChildSupport_Pmt");
            childSupport.RemainMons_rep = GetString("ChildSupport_RemainMons");
            childSupport.NotUsedInRatio = GetBool("ChildSupport_NotUsedInRatio");

            var jobRelated = liaColl.GetJobRelated1(true);
            jobRelated.ExpenseDesc = GetString("JobRelated1_ComNm");
            jobRelated.Pmt_rep = GetString("JobRelated1_Pmt");
            jobRelated.NotUsedInRatio = GetBool("JobRelated1_NotUsedInRatio");

            jobRelated = liaColl.GetJobRelated2(true);
            jobRelated.ExpenseDesc = GetString("JobRelated2_ComNm");
            jobRelated.Pmt_rep = GetString("JobRelated2_Pmt");
            jobRelated.NotUsedInRatio = GetBool("JobRelated2_NotUsedInRatio");

            #endregion

            liaColl.Flush();

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("sLiaBalLTot", dataLoan.sLiaBalLTot_rep);
            SetResult("sLiaMonLTot", dataLoan.sLiaMonLTot_rep);
            SetResult("sRefPdOffAmt", dataLoan.sRefPdOffAmt_rep);
            SetResult("aLiaBalTot", dataApp.aLiaBalTot_rep);
            SetResult("aLiaMonTot", dataApp.aLiaMonTot_rep);
            SetResult("aLiaPdOffTot", dataApp.aLiaPdOffTot_rep);
            SetResult("sQualBottomR", dataLoan.sQualBottomR_rep);
            SetResult("sTransNetCash", dataLoan.sTransNetCash_rep);
            SetResult("sMonthlyPmt", dataLoan.sMonthlyPmt_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sLAmtLckd", dataLoan.sLAmtLckd);
            SetResult("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            SetResult("sTerm", dataLoan.sTerm_rep);
            SetResult("sDue", dataLoan.sDue_rep);
            SetResult("sIOnlyMon", dataLoan.sIOnlyMon_rep);
            SetResult("sTransNetCashLckd", dataLoan.sTransNetCashLckd);
            SetResult("sTransmTotMonPmt", dataLoan.sTransmTotMonPmt_rep);
            if (IsPmlClient) 
            {
                SetResult("sProdCashoutAmt", dataLoan.sProdCashoutAmt_rep);
            }
            SetResult("sLPurposeT", dataLoan.sLPurposeT);
			SetResult("sCurrentTotMonPmt", dataLoan.sCurrentTotMonPmt_rep);
			SetResult("sMonSavingPmt", dataLoan.sMonSavingPmt_rep);

            ILiaCollection recordList = dataApp.aLiaCollection;
            // Load special record value.
            var alimony = recordList.GetAlimony(false);
            if (null != alimony) 
            {
                SetResult("Alimony_ComNm", alimony.OwedTo);
                SetResult("Alimony_Pmt", alimony.Pmt_rep);
                SetResult("Alimony_RemainMons", alimony.RemainMons_rep);
                SetResult("Alimony_NotUsedInRatio", alimony.NotUsedInRatio);
            } 
            else 
            {
                SetResult("Alimony_ComNm", "");
                SetResult("Alimony_Pmt", "");
                SetResult("Alimony_RemainMons", "");
                SetResult("Alimony_NotUsedInRatio", false);
            }

            var childSupport = recordList.GetChildSupport(false);
            SetResult("ChildSupport_ComNm", childSupport?.OwedTo ?? string.Empty);
            SetResult("ChildSupport_Pmt", childSupport?.Pmt_rep ?? string.Empty);
            SetResult("ChildSupport_RemainMons", childSupport?.RemainMons_rep ?? string.Empty);
            SetResult("ChildSupport_NotUsedInRatio", childSupport?.NotUsedInRatio ?? false);

            var jobRelated = recordList.GetJobRelated1(false);
            if (null != jobRelated) 
            {
                SetResult("JobRelated1_Pmt", jobRelated.Pmt_rep);
                SetResult("JobRelated1_ComNm", jobRelated.ExpenseDesc);
                SetResult("JobRelated1_NotUsedInRatio", jobRelated.NotUsedInRatio);
            } 
            else 
            {
                SetResult("JobRelated1_Pmt", "");
                SetResult("JobRelated1_ComNm", "");
                SetResult("JobRelated1_NotUsedInRatio", false);

            }

            jobRelated = recordList.GetJobRelated2(false);
            if (null != jobRelated) 
            {
                SetResult("JobRelated2_ComNm", jobRelated.ExpenseDesc);
                SetResult("JobRelated2_Pmt", jobRelated.Pmt_rep);
                SetResult("JobRelated2_NotUsedInRatio", jobRelated.NotUsedInRatio);

            } 
            else 
            {
                SetResult("JobRelated2_ComNm", "");
                SetResult("JobRelated2_Pmt", "");
                SetResult("JobRelated2_NotUsedInRatio", false);

            }


        }
    }
	public partial class DebtConsolidateListService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new DebtConsolidateListServiceItem());
        }
	}
}
