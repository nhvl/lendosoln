﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConversationLogNewPost.aspx.cs" Inherits="LendersOfficeApp.newlos.ConversationLogNewPost" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Conversation Log</title>
    <style type="text/css">
        body
        {            
            background-color: gainsboro;
        }
        .hidden
        {
            display:none
        }        
        .categorySelectorRequired
        {
            color:red;
        }
        .categoryNameSection
        {
            background-color:#003366;
            color:white;
            font-weight:bold;
            text-decoration:underline;
            padding:5px;
            padding-left:15px;
        }
        #permissionLevelsDisplaySection
        {
            position:fixed;
            padding:6px;
            background-color:white;
            border-style:solid;
            border-width:1px;
            border-color:black;
            z-index:1;
            left:200px;
        }
        #newCommentSection
        {
            padding-top:10px;
            background-color:gainsboro;
            height:70px;
        }
        .categorySelectorLabelSection
        {
            display: inline-block;
            width: 70px;
        }
        .categorySelectorSection
        {
            display: inline-block;
        }
        .newCommentBtnAndLinkSection
        {
            margin: 2px;
        }
        .newCommentCategorySelectorSection
        {
            display: inline-block;
            vertical-align: top;
            width: 230px;
        }
        .newCommentTextAndAddSection
        {
            display: inline-block;
        }
        .newCommentTextSection
        {
            margin: 2px;
            margin-bottom: 5px;
        }
        select#categorySelector
        {
            width: 150px;
            padding:1px;
        }
        textarea#newCommentTextArea
        {
            width:450px;
        }
        select::-ms-expand
        {
            <%-- this only works in ie10+ --%>
            background-color:white;
            border-style:none;
        }
    </style>
</head>
<body>
    <script id="categoryOptionTemplate" type="text/x-jquery-templ">
        <option value="${id}">${displayName}</option>
    </script>

    <script id="permissionLevelsBodySectionTemplate" type="text/x-jquery-tmpl">
        {{each postablePermissionLevels}}
            <tr>
                <td>
                    <a class="selectsPermissionLevel" data-id="${id}">${name}</a>
                </td>
                <td>${description}</td>
            </tr>
        {{/each}}
    </script>

    <script type="text/javascript">
        var categoryById = {};
        var selectedCategoryId = -1;
        var selectedPermissionLevelId = -1;

        jQuery(function ($) {
            $(window).on('beforeunload', function (e) {
                registerCommentInfoWithTreeFrame();
            });

            function registerCommentInfoWithTreeFrame() {
                var obj = top.frames['treeview'].QRI.getObject();
                obj.text = $('#newCommentTextArea').val();
                obj.selectedCategoryId = selectedCategoryId;
                obj.selectedPermissionLevelId = selectedPermissionLevelId;
            }

            function loadValuesFromTreeFrame()
            {
                var obj = top.frames['treeview'].QRI.getObject();
                if (typeof obj.text !== 'undefined') {
                    $('#newCommentTextArea').val(obj.text).trigger('keyup');
                }

                if (typeof obj.selectedCategoryId !== 'undefined') {
                    selectedCategoryId = obj.selectedCategoryId;
                }

                if (typeof obj.selectedPermissionLevelId !== 'undefined') {
                    selectedPermissionLevelId = obj.selectedPermissionLevelId;
                }
            }

            initializeModel();
            loadValuesFromTreeFrame();
            initializePageView();
            initializeHandlers();
            

            function initializeModel()
            {
                var numCategories = categories.length;
                var category;
                for (var i = 0; i < numCategories; i++) 
                {
                    category = categories[i];
                    categoryById[category.id] = category;
                }

                initializeSelectedCategoryId();

                if (ML.enableConversationLogPermissions) {
                    permissionLevelById = arrayOfObjectsToDictionary(permissionLevels, function (p) { return p.id; }, identity);
                }

                initializeSelectedPermissionLevelId();
            }

            function initializeSelectedCategoryId() {
                if (selectedCategoryId == -1) {
                    return;
                }

                var numCategories = categories.length;
                for (var i = 0; i < numCategories; i++) {
                    var category = categories[i];
                    if (category.isActive === false) {
                        continue;
                    }
                    if (category.id == selectedCategoryId) {
                        // nothing to do, the selected category is active.
                        return;
                    }
                }

                // if couldn't find the selected category, choose the first active one as selected.
                for (var i = 0; i < numCategories; i++) {
                    var category = categories[i];
                    if (category.isActive === false) {
                        continue;
                    }
                    selectedCategoryId = category.id;
                    break;
                }
            }

            function initializePageView()
            {
                initializeCategorySelector();
                if (ML.enableConversationLogPermissions) {
                    initializePermissionLevelsSection();
                }
                enableOrDisablePostButton();
                updateViews();
            }

            function enableOrDisablePostButton() {
                var $savesNewComment = $('.savesNewComment');
                var $newComment = $('#newCommentTextArea');
                var disable = $newComment.val() === '';
                disable |= (selectedCategoryId == -1)
                $savesNewComment.prop('disabled', disable);
            }

            function initializePermissionLevelsSection() {
                var $permissionLevelsBodySection = $('#permissionLevelsBodySection');
                $permissionLevelsBodySection.empty();
                var $permissionLevelsBodySectionTemplate = $('#permissionLevelsBodySectionTemplate').template();
                var postablePermissionLevels = arrayFilter(permissionLevels, function (p) { return p.canPost && p.isActive; });
                var $permissionLevelsBodySectionInner = $.tmpl($permissionLevelsBodySectionTemplate, { postablePermissionLevels: postablePermissionLevels });
                $permissionLevelsBodySection.append($permissionLevelsBodySectionInner)
            }

            function arrayFilter(array, condition) {
                var resultArray = [];
                var arrayLength = array.length;
                for (var i = 0; i < arrayLength; i++) {
                    var item = array[i];
                    if (condition(item)) {
                        resultArray.push(item);
                    }
                }

                return resultArray;
            }

            function initializeHandlers() {
                var $savesNewComment = $('.savesNewComment');
                $('#newCommentTextArea').on('keyup blur', function () {
                    enableOrDisablePostButton();
                });
                
                $('.savesNewComment').on('click', saveNewComment);

                $('#categorySelector').on('change', function () {
                    selectedCategoryId = this.value;
                    enableOrDisablePostButton();
                    $('.categorySelectorRequired').toggle(selectedCategoryId == -1);
                    if (false === ML.enableConversationLogPermissions) {
                        return;
                    }

                    if (selectedCategoryId == -1)
                    {
                        setSelectedPermissionLevelId(-1);
                    }
                    else
                    {
                        setSelectedPermissionLevelId(categoryById[selectedCategoryId].defaultPermissionLevelId);
                    }
                });

                var $permissionLevelInfoSection = $('#selectedPermissionLevelInfoSection');
                $permissionLevelInfoSection.on('click', '.displaysPermissionLevelInfo', displaySelectedPermissionLevelInfo);
                $permissionLevelInfoSection.on('click', '.promptsForPermissionLevelChange', openPermissionLevelsSelector);

                var $permissionLevelsBodySection = $('#permissionLevelsBodySection');
                $('.cancelsPermissionLevelSelection').on('click', hidePermissionLevelsSelector);
                $permissionLevelsBodySection.on('click', '.selectsPermissionLevel', selectPermissionLevel);
            }

            function initializeSelectedPermissionLevelId() {
                if (selectedCategoryId == -1 || ML.enableConversationLogPermissions === false) {
                    setSelectedPermissionLevelId(-1);
                }
                else {
                    if (selectedPermissionLevelId == -1) {
                        setSelectedPermissionLevelId(categoryById[selectedCategoryId].defaultPermissionLevelId);
                    }
                }
            }


            function displaySelectedPermissionLevelInfo() {
                alert(permissionLevelById[selectedPermissionLevelId].description);
            }

            function selectPermissionLevel(eventObj) {
                var $permissionLevelLink = $(eventObj.target);
                var id = $permissionLevelLink.data('id');

                if (id == "") {
                    id = null;
                }

                setSelectedPermissionLevelId(id);
                hidePermissionLevelsSelector();
            }

            function setSelectedPermissionLevelId(id) {
                // choose the first permission level that the user can post to.
                if (id != -1 && !permissionLevelById[id].canPost) {
                    for (var i = 0; i < permissionLevels.length; i++) {
                        if (permissionLevels[i].canPost) {
                            id = permissionLevels[i].id;
                            break;
                        }
                    }
                }

                selectedPermissionLevelId = id;
                updatePermissionLevelView();
            }

            function updatePermissionLevelView() {
                if (selectedPermissionLevelId == -1 || ML.enableConversationLogPermissions === false) {
                    $('#selectedPermissionLevelInfoSection').hide()
                }
                else {
                    $('.permissionLevelNameSection').text(permissionLevelById[selectedPermissionLevelId].name);
                    $('#selectedPermissionLevelInfoSection').show();
                }
            }

            function openPermissionLevelsSelector() {
                if (ML.enableConversationLogPermissions) {
                    $('#permissionLevelsDisplaySection').show();
                    parent.QRC.expandDialog(true);
                }
            }

            function hidePermissionLevelsSelector() {
                $('#permissionLevelsDisplaySection').hide();
                parent.QRC.expandDialog(false);
            }

            function arrayOfObjectsToDictionary(array, keyGetter, valueGetter) {
                var result = {};
                var arrayLength = array.length;

                for (var i = 0; i < arrayLength; i++) {
                    var item = array[i];
                    result[keyGetter(item)] = valueGetter(item);
                }

                return result;
            }

            function identity(p) {
                return p;
            }

            function saveNewComment()
            {
                var $newCommentTextArea = $('#newCommentTextArea');
                var args = {
                    resourceString: ML.resourceString,
                    commentText: $newCommentTextArea.val(),
                    categoryId: selectedCategoryId,
                    permissionLevelId: selectedPermissionLevelId,
                    excludeConversations:true
                };      

                var results = gService.newPostService.call("PostComment", args);
                if (!results.error) {
                    reInitializePageFromResult(results, noop);
                    if (typeof (results.value['alertMessage']) !== 'undefined') {
                        return;
                    }

                    $newCommentTextArea.val('').trigger('keyup');
                    $newCommentTextArea.closest('.newCommentTextAndAddSection').find('.savesNewComment').css('background-color', '');
                    parent.QRC.closeConversationLog();
                }
                else
                {
                    alert(results.UserMessage);
                }
            }

            function noop() { }

            function reInitializePageFromResult(results, extraModelInitializer) {
                var wasSuccessful = typeof (results.value['alertMessage']) === 'undefined';

                if (wasSuccessful === false) {
                    alert(results.value['alertMessage']);
                }

                ML.enableConversationLogPermissions = (results.value['enableConversationLogPermissions'] === 'True');

                if (ML.enableConversationLogPermissions === true) {
                    var newPermissionLevelsJson = results.value['newPermissionLevels'];
                    permissionLevels = JSON.parse(newPermissionLevelsJson);
                }
                else {
                    permissionLevels = [];
                }

                var newCategoriesJson = results.value['updatedCategoriesJson'];
                categories = JSON.parse(newCategoriesJson);
                initializeModel();
                if (wasSuccessful === true) {
                    extraModelInitializer();
                }
                initializePageView();
            }

            function initializeCategorySelector() {
                var $categoryOptionTemplate = $('#categoryOptionTemplate').template();
                var $categoriesSelector = $('#categorySelector');
                $categoriesSelector.empty();

                if (selectedCategoryId == -1) {
                    var $blankCategoryOptionElement = $.tmpl($categoryOptionTemplate, { id: -1, displayName: '' });
                    $blankCategoryOptionElement.prop('selected', 'selected');
                    $categoriesSelector.append($blankCategoryOptionElement);
                    $('.categorySelectorRequired').show();
                }

                var numCategories = categories.length;
                // append the categories, and mark the one that is selected as selected.
                for (var i = 0; i < numCategories; i++) {
                    var category = categories[i];
                    if (category.isActive === false) {
                        continue;
                    }
                    var $categoryOptionElement = $.tmpl($categoryOptionTemplate, category);
                    if (category.id == selectedCategoryId) {
                        $categoryOptionElement.prop('selected', 'selected');
                        $('.categorySelectorRequired').hide();
                    }
                    $categoriesSelector.append($categoryOptionElement);
                }                
            }


            function updateNewCommentSectionVisibility() {
                var hasActiveCategory = false;
                for (var i = 0; i < categories.length; i++) {
                    if (categories[i].isActive) {
                        hasActiveCategory = true;
                        break;
                    }
                }

                var hasPostablePermissionLevel = false;
                for (var i = 0; i < permissionLevels.length; i++) {
                    if (permissionLevels[i].canPost) {
                        hasPostablePermissionLevel = true;
                        break;
                    }
                }

                $('#newCommentSection').toggle(hasActiveCategory && (false === ML.enableConversationLogPermissions || hasPostablePermissionLevel));
            }

            function updateViews() {
                updateNewCommentSectionVisibility();
                updatePermissionLevelView();
            }

        });
    </script>

    <form id="ConvLogNewPostForm" runat="server">    
    <div id="container">
        <div id="permissionLevelsDisplaySection" class="hidden">
            <table>
                <thead>
                    <th>Permission</th>
                    <th>Description</th>
                </thead>
                <tbody id="permissionLevelsBodySection">
                    <!-- load from model -->
                </tbody>
            </table>
            <div>
                <input type="button" value="Cancel"  class="cancelsPermissionLevelSelection"/>
            </div>
        </div>
         <div id="newCommentSection" style="display:none">
            <div class="newCommentTextAndAddSection">
                <div class="newCommentTextSection"><textarea id="newCommentTextArea" rows="5" cols="120" NotForEdit></textarea></div>
                <div class="newCommentBtnAndLinkSection"><input type="button" disabled="disabled" value="New Comment" class="savesNewComment"/></div>
            </div>             
            <table class="newCommentCategorySelectorSection">
                <tr>
                    <td>
                        <div class="categorySelectorLabelSection">Category:<span class="categorySelectorRequired">*</span></div>
                    </td>
                    <td>
                        <div class="categorySelectorSection">
                            <select id="categorySelector" NotForEdit>
                                <!-- Load from model. -->
                            </select>
                        </div>
                    </td>
                </tr>
                <tr id="selectedPermissionLevelInfoSection" class="hidden">
                    <td>
                        <div class="permissionLevelSelectorLabelSection">Permission:</div>
                    </td>
                    <td>
                        <span class="permissionLevelNameSection">
                            <!-- load from model -->
                        </span>
                        <a class="displaysPermissionLevelInfo">?</a>
                        <a class="promptsForPermissionLevelChange">change</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
