﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataLayerMigration.aspx.cs" Inherits="LendersOfficeApp.newlos.DataLayerMigration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Data Layer Migrations</title>
    <style type="text/css">
        body
        {
            background-color: gainsboro;
        }
        .EmptySpace
        {
            width: 40px;
        }
        .MigrationPageheader
        {
            padding: 0.2em;
            width: 100%;
            border: 0px;
        }
        .PadLeft
        {
            padding-left: 20px;
        }
        .TopBorder > td
        {
            border-top: 1px solid black;
        }
        .LeftBorder > td
        {
            border-left: 1px solid black;
        }
        .MigrationResultsTable td
        {
            padding: 4px;
        }
        .MigrationResultsTable > tr:last-child td
        {
            border-bottom: 1px solid black;
        }
        .MigrationResultsTable tr > td:last-child
        {
            border-right: 1px solid black;
        }
        .MigrationAuditTable
        {
            padding: 4px 4px 4px 20px;
        }
        .MigrationAuditTable > tr:last-child td
        {
            border-bottom: 1px solid black;
        }
        .MigrationAuditTable tr > td:last-child
        {
            border-right: 1px solid black;
        }
        .MigrationAuditTable td
        {
            padding: 4px;
        }
        .Error
        {
            font-weight: bold;
            color: red;
        }
        .Conditions
        {
            list-style-type: none;
            padding: 0px;
        }
        .Conditions li
        {
            padding: 3px;
        }
        .NoWrap
        {
            white-space: nowrap;
        }
        .ApplyBtn
        {
            margin: 10px;
        }
        .Pages
        {
            list-style-type: none;
            padding: 0px;
        }
        #targetRollbackVersion
        {
            width: 30px;
        }
        .NewHighlight
        {
            background-color: lightyellow;
        }
        .Okay
        {
            text-align: center;
            color: rgb(51, 105, 30);
        }
        .Conflict
        {
            text-align: center;
            color: rgb(213, 0, 0);
        }
        #DetailsContent
        {
            padding: 3px; 
            border: 1px solid black;
        }
        #DetailsCloseButton
        {
            padding: 5px;
        }
        .modal-background 
        {
            position: fixed;
            top: 0; left: 0; width: 100%; height: 100%;
            background-color: rgba(0,0,0,0.5);
            padding: 0.5em;
            text-align: center;
            -moz-box-sizing: border-box; box-sizing: border-box;
        }
        .modal-outer 
        {
            position: relative;
            width: 100%;
            height: 100%;
        }
        .modal 
        {
            position: relative;
            background-color: gainsboro;
            padding: 2em 2em 1em 2em;
            margin: 0 auto;
            display: inline-block;
            max-width: 100%;
            max-height: 100%;
            overflow-y: auto;
            text-align: left;
            -moz-box-sizing: border-box; box-sizing: border-box;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        var globalRactive;
        var detailsModal;

        jQuery(function ($) {

            function ExpandClick(ractive, event, fullPageSet)
            {
                var isCollapsed = ractive.get(event.keypath + ".IsCollapsed");
                if (isCollapsed) {
                    ractive.set(event.keypath + ".DisplaySet", fullPageSet);
                    ractive.set(event.keypath + ".IsCollapsed", false);
                    event.node.innerHTML = "less...";
                }
                else {
                    ractive.set(event.keypath + ".DisplaySet", fullPageSet.slice(0, 3));
                    ractive.set(event.keypath + ".IsCollapsed", true);
                    event.node.innerHTML = "more...";
                }
            }

            function PageLinkClick(name)
            {
                redirectToUladIfUladPage(gVirtualRoot + name);
                return;
            }

            globalRactive = new Ractive({
                debug: true,
                append: true,
                template: '#PageTemplate',
                partials: '#FieldValueTemplate,#CollectionTableTemplate,#PageLinksTemplate',
                el: '#container',
                data: MigrationPageModel,
                init:
                    function () {
                        var self = this;

                        this.on("expandClick", function (event, fullPageSet) {
                            ExpandClick(self, event, fullPageSet);
                        });

                        this.on("linkMe", function (event, name) {
                            PageLinkClick(name);
                        });

                        this.on("rollback", function (event, index) {
                            var rollbackTarget = $('#targetRollbackVersion').val();

                            var args = {};
                            args.LoanId = ML.sLId;
                            args.targetRollbackVersion = rollbackTarget;

                            var results = gService.loanedit.call("RollbackVersion", args);
                            if (!results.error) {
                                var resultStatus = results.value["IsSuccessful"];
                                if(resultStatus == 'True')
                                {
                                    var newModel = results.value["MigrationPageModel"];
                                    self.set(JSON.parse(newModel));

                                    alert("Rollback successful.");
                                    parent.info.f_refreshInfoFromDB();
                                }
                                else
                                {
                                    alert(results.value["ErrorMessage"]);
                                }
                            }
                            else
                            {
                                alert(results.UserMessage);
                            }
                        });

                        this.on("displayDetails", function (event, index) {
                            var auditHistory = self.get('MigrationAuditHistory');
                            var detailsPopup = new detailsModal({
                                data: auditHistory[index]
                            })
                        });

                        this.on("targetChanged", function (event) {
                            var args = {};
                            args.LoanId = ML.sLId;
                            args.TargetVersion = $(event.node).val();

                            var results = gService.loanedit.call("TargetVersionChanged", args);
                            if (!results.error)
                            {
                                var newModel = results.value["MigrationPageModel"];
                                self.reset(JSON.parse(newModel));
                            }
                            else
                            {
                                alert(results.UserMessage);
                            }
                        });

                        this.on("applyMigrations", function (event) {
                            var args = {};
                            args.LoanId = ML.sLId;
                            args.TargetVersion = $('#targetMigrationVersion').val();

                            var results = gService.loanedit.call("ApplyMigrations", args);
                            if (!results.error) {
                                var newModel = results.value["MigrationPageModel"];
                                var resultStatus = results.value["IsSuccessful"];
                                self.set(JSON.parse(newModel));
                                if (resultStatus == 'True')
                                {
                                    alert("Migrations completed successfully.");
                                    parent.info.f_refreshInfoFromDB();
                                }
                                else
                                {
                                    alert("Migrations unsuccessful. Please review the migration status preview for any changes to this file that may have prevented the migration from completing.")
                                }
                            }
                            else
                            {
                                alert(results.UserMessage);
                            }
                        });
                    }
            });

            detailsModal = Ractive.extend({
                el: document.body,
                append: true,
                template: '#DetailsModalTemplate',
                partials: '#FieldValueTemplate,#CollectionTableTemplate,#PageLinksTemplate',
                init:
                    function () {
                        var self = this;
                        this.outer = this.find('.modal-outer');
                        this.modal = this.find('.modal');

                        this.on('closePopup', function () {
                            self.teardown();
                        });

                        this.on("expandClick", function (event, fullPageSet) {
                            ExpandClick(self, event, fullPageSet);
                            this.center();
                        });

                        this.on("linkMe", function (event, name) {
                            PageLinkClick(name);
                        });

                        this.center();
                    },
                center:
                    function () {
                        var outerHeight, modalHeight, verticalSpace;

                        outerHeight = this.outer.clientHeight;
                        modalHeight = this.modal.clientHeight;

                        verticalSpace = (outerHeight - modalHeight) / 2;

                        this.modal.style.top = verticalSpace + 'px';
                    }
            });
        });
    </script>

    <script id="DetailsModalTemplate" type="text/ractive">
        <div class='modal-background'>
          <div class='modal-outer'>
            <div class='modal'>
                <div class="FormTableSubheader" id="DetailsContent">
                    {{Version}} - {{Name}}
                </div>
                <table>
                    {{#if LoanFileFieldValues.length != 0}}
                    <tr>
                        <td>
                            {{>FieldValueTemplate { AppName: "", Result: Result, LoanFileFieldValues: LoanFileFieldValues} }}
                        </td>
                    </tr>
                    {{/if}}    
                    {{#if AppFieldValues.length != 0}}
                    {{#AppFieldValues:i}}
                    <tr>
                        <td>
                            {{>FieldValueTemplate { AppName: "Application " + (i+1) + " - " + AppFieldValues[i].Key, Result: Result, LoanFileFieldValues: AppFieldValues[i].Value} }}
                        </td>
                    </tr>
                    {{/#AppFieldValues}}
                    {{/if}}
                    {{#if CollectionValues.length != 0}}
                    <table class="MigrationResultsTable" border="0" cellpadding="0" cellspacing="0">
                        <tr class="FieldLabel GridHeader TopBorder LeftBorder">
                            <td>Field name</td>
                            <td>Location</td>
                            <td>Old value</td>
                            {{#if AlwaysShowNewValues || (Result == 3 && OnlyShowOldValues != true)}}
                            <td class="NoWrap">New value</td>
                            {{/if}}
                        </tr>
                        {{#CollectionValues:i}}
                            {{>CollectionTableTemplate { CollectionName: FriendlyName, CollectionLocation: Pages, OverallResult: Result, Index: i, Layer: 0 } }}
                        {{/#CollectionValues}}
                    </table>
                    {{/if}}
                </table>
                <div id="DetailsCloseButton">
                    <input type="button" value=" Close " on-click="closePopup"/>
                </div>
            </div>
          </div>
        </div>
    </script>

    <script id="FieldValueTemplate" type="text/ractive">
        <table class="MigrationResultsTable" border="0" cellpadding="0" cellspacing="0">
            {{#if AppName != ""}}
            <tr class="FieldLabel GridHeader TopBorder LeftBorder">
                <td class="NoWrap" colspan="{{(AlwaysShowNewValues || (Result == 3 && OnlyShowOldValues != true)) ? "4" : "3"}}">{{AppName}}</td>
            </tr>
            {{/if}}
            <tr class="FieldLabel GridHeader TopBorder LeftBorder">
                <td class="NoWrap">Field name</td>
                <td>Location</td>
                <td class="NoWrap">Old value</td>
                {{#if AlwaysShowNewValues || (Result == 3 && OnlyShowOldValues != true)}}
                <td class="NoWrap">New value</td>
                {{/if}}
            </tr>
            {{#LoanFileFieldValues:i}}
            <tr class="LeftBorder {{ i % 2 == 0 ? "GridItem " : "GridAlternatingItem "}}">
                <td class="FieldLabel NoWrap">
                    <span>
                        {{FieldName}} <br />
                        ({{FieldId}})
                    </span>
                </td>
                <td>
                    {{>PageLinksTemplate { DisplaySet: Pages.slice(0,3), IsCollapsed: true, FullPageSet: Pages } }}
                </td>
                <td>
                    {{OldValue}}
                </td>
                {{#if AlwaysShowNewValues || (Result == 3 && OnlyShowOldValues != true)}}
                <td class="{{OldValue != NewValue ? "NewHighlight" : ""}}">
                    {{NewValue}}
                </td>
                {{/if}}
            </tr>
            {{/#LoanFileFieldValues}}
        </table>
    </script>

    <script id="PageLinksTemplate" type="text/ractive">
        <ul class="Pages">
            {{#DisplaySet:name}}
            <li>
                <a on-click="linkMe:{{Value}}" href="javascript:void(0);">{{Key}}</a>
            </li>
            {{/#DisplaySet}}
            {{#if FullPageSet.length > 3}}
            <li>
                <a on-click="expandClick:{{FullPageSet}}" href="javascript:void(0);">more...</a>
            </li>
            {{/if}}
        </ul>
    </script>

    <script id="CollectionTableTemplate" type="text/ractive">

        <tr class="LeftBorder" style="background-color: rgba({{38+(Layer*10)}},{{138+(Layer*10)}},{{171+(Layer*10)}},{{1-(Layer*.15)}})">
            <td class="FieldLabel" style="padding-left: {{10*Layer}}px;">
                {{CollectionName}}
            </td>
            <td>
                {{#if CollectionLocation != null && CollectionLocation.length != 0}}
                {{>PageLinksTemplate { DisplaySet:CollectionLocation.slice(0,3), IsCollapsed: true, FullPageSet: CollectionLocation } }}
                {{/if}}
            </td>
            <td></td>
            {{#if AlwaysShowNewValues || (OverallResult == 3 && OnlyShowOldValues != true)}}
            <td></td>
            {{/if}}
        </tr>
        {{#if FinalValues != null && FinalValues.length != 0}}
        {{#FinalValues:i}}
        <tr class="LeftBorder {{ i % 2 == 0 ? "GridItem " : "GridAlternatingItem "}}">
            <td class="FieldLabel" style="padding-left: {{10*(Layer+1)}}px;">
                {{FieldName}}
            </td>
            <td></td>
            <td>
                {{OldValue}}
            </td>
            {{#if AlwaysShowNewValues || (OverallResult == 3 && OnlyShowOldValues != true)}}
            <td class="{{OldValue != NewValue ? "NewHighlight" : ""}}"> 
                {{NewValue}}   
            </td>
            {{/if}}
        </tr>
        {{/#FinalValues}}
        {{/if}}
        
        {{#if CollectionValues != null && CollectionValues.length != 0}}
        {{#CollectionValues:j}}
            {{#if UsesItemType == false}}
                {{>CollectionTableTemplate { CollectionName: FriendlyName + " - " + (j+1), CollectionLocation: null, OverallResult: OverallResult, Index: i, Layer: Layer+1} }}
            {{else}}
                {{>CollectionTableTemplate { CollectionName: ItemType + " Type - " + FriendlyName, CollectionLocation: null, OverallResult: OverallResult, Index: i, Layer: Layer+1} }}
            {{/if}}
        {{/#CollectionValues}}
        {{/if}}

    </script>

    <script id="PageTemplate" type="text/ractive">
        <div id="VersionSection">
            <table border="0">
                <tr>
                    <td>
                        <span class="FieldLabel">
                            Loan file current migration version:
                        </span>
                        <span>
                            {{CurrentVersion}}
                        </span>
                    </td>
                    <td class="EmptySpace">&nbsp;</td>
                    <td>
                        <span class="FieldLabel">
                            Target migration version:
                        </span>
                        <select doesntDirty on-change="targetChanged" id="targetMigrationVersion" value="{{TargetVersion}}"
                           disabled="{{AvailableVersions.length == 1 && AvailableVersions[0] == CurrentVersion}}">
                            {{#AvailableVersions}}
                                <option value="{{this}}">{{this}}</option>    
                            {{/#AvailableVersions}}
                        </select>
                    </td>
                    <td class="EmptySpace">&nbsp;</td>
                    <td>
                        <span class="FieldLabel">
                            Newest version:
                        </span>
                        <span>{{NewestVersion}}</span>
                    </td>
                    <td class="EmptySpace">&nbsp;</td>
                    {{#if DisplayRollback == true && CurrentVersion != 0}}
                    <td>
                        Rollback to version:
                        <input doesntDirty type="text" id="targetRollbackVersion" /> 
                        <input type="button" value="Rollback" on-click="rollback" />
                    </td>
                    {{/if}}
                </tr>
            </table>
        </div>

        <div id="MigrationAuditHistory">
            <div class="FormTableHeader MigrationPageheader">
                Applied migrations
            </div>
            <div>
                {{#if MigrationAuditHistory.length == 0}}
                    <div class="PadLeft">None</div>
                {{else}}
                    <table class="MigrationAuditTable" border="0" cellpadding="0" cellspacing="0">
                        <tr class="FieldLabel GridHeader TopBorder LeftBorder">
                            <td class="NoWrap">#</td>
                            <td class="NoWrap">Description</td>
                            <td class="NoWrap">User</td>
                            <td class="NoWrap">Date</td>
                            <td></td>
                        </tr>
                        {{#MigrationAuditHistory:i}}
                        <tr class="LeftBorder {{ i % 2 == 0 ? "GridItem " : "GridAlternatingItem "}}">
                            <td>{{Version}}</td>
                            <td class="NoWrap">{{Name}}</td>
                            <td>{{UserName}}</td>
                            <td class="NoWrap">{{DateRan}}</td>
                            <td class="NoWrap">
                                <a href="javascript:void(0)" on-click="displayDetails:{{i}}">details</a>
                            </td>
                        </tr>
                        {{/#MigrationAuditHistory}}
                    </table>
                {{/if}}
            </div>
        </div>
        
        <div id="MigrationStatusSection">
            <div class="FormTableHeader MigrationPageheader">
                Migration statuses
            </div>
            <div>
                {{#if MigrationResults.length == 0}}
                    <div class="PadLeft">
                        All migrations have been applied.
                    </div>
                {{else}}
                    <table width="100%" cellpadding="0" border="0">
                        {{#MigrationResults}}
                        <tr class="FormTableSubheader">
                            <td colspan="5">
                                {{Version}} - {{Name}}
                            </td>
                        </tr>
                        {{#if HasPermissionErrors}}
                        <tr >                    
                            <td colspan="5">
                                Due to lack of permissions, you may not see the fields required to run this migration.
                            </td>
                        </tr>
                        {{/if}}
                        <tr>
                            <td width="2%">&nbsp;</td>
                            <td>
                                <table>
                                    {{#if LoanFileFieldValues.length != 0}}
                                    <tr>
                                        <td>
                                            {{>FieldValueTemplate { AppName: "", Result: Result, LoanFileFieldValues: LoanFileFieldValues} }}
                                        </td>
                                    </tr>
                                    {{/if}}    
                                    {{#if AppFieldValues.length != 0}}
                                    {{#AppFieldValues:i}}
                                    <tr>
                                        <td>
                                            {{>FieldValueTemplate { AppName: "Application " + (i+1) + " - " + AppFieldValues[i].Key, Result: Result, LoanFileFieldValues: AppFieldValues[i].Value} }}
                                        </td>
                                    </tr>
                                    {{/#AppFieldValues}}
                                    {{/if}}
                                    {{#if CollectionValues != null && CollectionValues.length != 0}}
                                    <table class="MigrationResultsTable" border="0" cellpadding="0" cellspacing="0">
                                        <tr class="FieldLabel GridHeader TopBorder LeftBorder">
                                            <td>Field name</td>
                                            <td>Location</td>
                                            <td>Old value</td>
                                            {{#if AlwaysShowNewValues || (Result == 3 && OnlyShowOldValues != true)}}
                                            <td class="NoWrap">New value</td>
                                            {{/if}}
                                        </tr>
                                        {{#CollectionValues:i}}
                                            {{>CollectionTableTemplate { CollectionName: FriendlyName, CollectionLocation: Pages, OverallResult: Result, Index: i, Layer: 0 } }}
                                        {{/#CollectionValues}}
                                    </table>
                                    {{/if}}
                                </table>
                            </td>
                            {{#if Result == 1}}
                            <td class="Error">
                                <ul class="Conditions">
                                    {{#ConditionErrorMessages:i}}
                                        <li>
                                            {{this}}
                                        </li>
                                    {{/#ConditionErrorMessages}}
                                </ul>
                            </td>
                            {{else}}
                                {{#if Result == 3}}
                                <td>&nbsp;</td>
                                {{else}}
                                <td class="Error">
                                    {{GeneralError}}
                                </td>
                                {{/if}}
                            {{/if}}
                            <td>
                                {{#if Result == 3}}
                                <div class="Okay">
                                    <img src="{{VRoot}}/images/success.png" border="0">
                                    <div>Okay</div>
                                </div>
                                {{else}}
                                <div class="Conflict">
                                    <img src="{{VRoot}}/images/error.png" border="0">
                                    <div>Error</div>
                                </div>                        
                                {{/if}}
                            </td>
                        </tr>
                        {{/#MigrationResults}}
                    </table>
                {{/if}}
            </div>
            <br />
            <div class="ApplyBtn">
                <input on-click="applyMigrations" disabled="{{AreAllMigrationsSuccessful == false}}" type="button" value="Apply"/>
            </div>
        </div>         
    </script>

    <form id="DataLayerMigrations" runat="server">
    <div class="MainRightHeader" nowrap>Loan Data Migrations</div>
    <div id="container">
    </div>
    </form>
</body>
</html>
