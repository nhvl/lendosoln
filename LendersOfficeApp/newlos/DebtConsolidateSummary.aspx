<%@ Import namespace="LendersOffice.Constants" %>
<%@ Import namespace="DataAccess" %>
<%@ Import Namespace="LendersOffice.Admin" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="DebtConsolidateSummary.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.DebtConsolidateSummary" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>DebtConsolidateSummary</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </HEAD>
  <body MS_POSITIONING="FlowLayout" class="RightBackground">
	<script language=javascript>
<!--
function _init() {
  f_refreshUI();
}
function f_update(oResult) {
  populateForm(oResult.value, "");
}
function f_getFormValues() {
  return getAllFormValues("");
}
function f_refreshCalculation() {
    var f_refreshCalculation = retrieveFrameProperty(parent, "ListIFrame", "f_refreshCalculation");
    if(f_refreshCalculation) {
        f_refreshCalculation()
        f_refreshUI();
    }
}

function f_refreshUI() {

  
  if (<%= AspxTools.JsGetElementById(sLPurposeT) %>.value != <%=AspxTools.JsString(E_sLPurposeT.Purchase.ToString("D"))%> || ML.IsRenovationLoan) {
    <%= AspxTools.JsGetElementById(sLAmtLckd) %>.disabled = true;
    <%= AspxTools.JsGetElementById(sLAmtLckd) %>.checked = true;    
  } else {
    <%= AspxTools.JsGetElementById(sLAmtLckd) %>.disabled = false;
  }
  lockField(<%= AspxTools.JsGetElementById(sLAmtLckd) %>, "sLAmtCalc");
  lockField(<%= AspxTools.JsGetElementById(sTransNetCashLckd) %>, "sTransNetCash");  
  <% if (BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan)) { %>
    <%= AspxTools.JsGetElementById(sProdCashoutAmt) %>.readOnly = !ML.IsStandAloneSecond && <%= AspxTools.JsGetElementById(sLPurposeT) %>.value != <%=AspxTools.JsString(E_sLPurposeT.RefinCashout.ToString("D"))%>;
  <% } %>
}

  function single_saveMe() {
    callFrameMethod(parent, "ListIFrame", "saveMe");
  }

function updateDirtyBit_callback() { parent.updateDirtyBit(); }

// Return 0 - Automatically, 1 - Manually
function f_getCalcMode() {
  if (document.getElementById("rbAutomatic").checked) return 0;
  else if (document.getElementById("rbManual").checked) return 1;
  else return -1;
}
function f_forceCalculation() {
    callFrameMethod(parent, "ListIFrame", "f_backgroundCalculation");
}
//-->
    </script>

    <form id="DebtConsolidateSummary" method="post" runat="server">
<TABLE id=Table1 cellSpacing=0 cellPadding=0 border=0>
  <TR>
    <TD noWrap colSpan=7 class=FieldLabel>Perform 
      calculations: <input type=radio id="rbAutomatic" name="calcMode" NotForEdit="True"><label for="rbAutomatic">Automatically</label>&nbsp;&nbsp;
      <input type=radio id="rbManual" name="calcMode" checked NotForEdit="True"><label for="rbManual">Manually</label>&nbsp;&nbsp;&nbsp;<INPUT type=button value="Recalculate" onclick="f_forceCalculation();"></TD></TR>
  <TR>
    <TD noWrap>&nbsp;</TD>
    <TD noWrap></TD>
    <TD noWrap></TD>
    <TD noWrap width=20></TD>
    <TD noWrap></TD>
    <TD noWrap></TD>
    <TD noWrap></TD></TR>
  <TR>
    <TD noWrap class=FieldLabel>Loan Purpose</TD>
    <TD noWrap>&nbsp;</TD>
    <TD noWrap><asp:DropDownList id=sLPurposeT runat="server" onchange="f_refreshCalculation();"></asp:DropDownList></TD>
    <TD noWrap width=20></TD>
    <TD class=FieldLabel noWrap>PITI</TD>
    <TD noWrap></TD>
    <TD noWrap><ml:MoneyTextBox id=sMonthlyPmt runat="server" preset="money" width="90" ReadOnly="True"></ml:MoneyTextBox></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap>Loan Amount</TD>
    <TD class=FieldLabel noWrap><asp:CheckBox id=sLAmtLckd runat="server" onclick="f_refreshCalculation();"></asp:CheckBox></TD>
    <TD noWrap><ml:MoneyTextBox id=sLAmtCalc runat="server" width="90" preset="money" onchange="f_refreshCalculation();"></ml:MoneyTextBox></TD>
    <TD noWrap></TD>
    <TD class=FieldLabel noWrap>Bottom Ratio</TD>
    <TD noWrap></TD>
    <TD noWrap><ml:PercentTextBox id=sQualBottomR runat="server" preset="percent" width="70" ReadOnly="True"></ml:PercentTextBox></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap>Note Rate</TD>
    <TD class=FieldLabel noWrap></TD>
    <TD noWrap><ml:PercentTextBox id=sNoteIR runat="server" width="70" preset="percent" onchange="f_refreshCalculation();"></ml:PercentTextBox></TD>
    <TD noWrap></TD>
    <TD class=FieldLabel noWrap>Cash from/to Borrower</TD>
    <TD noWrap><asp:CheckBox id=sTransNetCashLckd runat="server" onclick="f_refreshCalculation();"></asp:CheckBox></TD>
    <TD noWrap><ml:MoneyTextBox id=sTransNetCash runat="server" preset="money" width="90" onchange="f_refreshCalculation();"></ml:MoneyTextBox></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap>Term / Due (mths)</TD>
    <TD class=FieldLabel noWrap></TD>
    <TD noWrap><asp:TextBox id=sTerm runat="server" Width="34px" MaxLength="3" onchange="f_refreshCalculation();"></asp:TextBox>&nbsp;/&nbsp;<asp:TextBox id=sDue runat="server" Width="34px" MaxLength="3" onchange="f_refreshCalculation();"></asp:TextBox></TD>
    <TD noWrap></TD>
    <TD noWrap class="FieldLabel">Total All Monthly Payments</TD>
    <TD noWrap></TD>
    <TD noWrap><ml:MoneyTextBox id=sTransmTotMonPmt runat="server" preset="money" width="90" ReadOnly="True"></ml:MoneyTextBox></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap>Interest Only</TD>
    <TD class=FieldLabel noWrap></TD>
    <TD noWrap><asp:TextBox id=sIOnlyMon runat="server" Width="33px" onchange="f_refreshCalculation();"></asp:TextBox>&nbsp;mths</TD>
    <TD noWrap></TD>
    <TD class="FieldLabel" noWrap>Current Total Payment</TD>
    <TD noWrap></TD>
    <TD noWrap><ml:MoneyTextBox id="sCurrentTotMonPmt" runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap>
    <% if (BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
       { %>
          Cashout Amount
    <% } %>
    </TD>
    <TD class=FieldLabel noWrap></TD>
    <TD noWrap>
    <% if (BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
       { %>
    <ml:MoneyTextBox id=sProdCashoutAmt runat="server" width="90" preset="money" onchange="f_refreshCalculation();"></ml:MoneyTextBox>
    <% } %>
    </TD>
    <TD noWrap></TD>    
    <TD class="FieldLabel" noWrap>Monthly Savings</TD>
    <TD noWrap></TD>
    <TD noWrap><ml:MoneyTextBox id="sMonSavingPmt" runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></TD></TR>
</TABLE>

     </form>
	
  </body>
</HTML>
