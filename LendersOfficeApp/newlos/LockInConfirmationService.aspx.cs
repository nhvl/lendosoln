using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos
{
    public class LockInConfirmationServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return new CLockInConfirmationData(sLId);
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.LockInConfirmation, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("LockInConfirmationCompanyName", preparer.CompanyName);
            SetResult("LockInConfirmationStreetAddr", preparer.StreetAddr);
            SetResult("LockInConfirmationCity", preparer.City);
            SetResult("LockInConfirmationState", preparer.State);
            SetResult("LockInConfirmationZip", preparer.Zip);
            SetResult("LockInConfirmationPhoneOfCompany", preparer.PhoneOfCompany);
            SetResult("LockInConfirmationPreparedDate", preparer.PrepareDate_rep);
            
            SetResult("sLT", dataLoan.sLT);
            SetResult("sLAmt", dataLoan.sLAmtCalc_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sTerm", dataLoan.sTerm_rep);
            SetResult("sDue", dataLoan.sDue_rep);
            SetResult("sLpTemplateNm", dataLoan.sLpTemplateNm);
            SetResult("sRLckdExpiredD", dataLoan.sRLckdExpiredD_rep);

            SetResult("sLOrigFPc", dataLoan.sLOrigFPc_rep);
            SetResult("sLDiscntPc", dataLoan.sLDiscntPc_rep);
            SetResult("sLOrigFMb", dataLoan.sLOrigFMb_rep);
            SetResult("sLDiscntFMb", dataLoan.sLDiscntFMb_rep);
            SetResult("sLOrigF", dataLoan.sLOrigF_rep);
            SetResult("sLDiscnt", dataLoan.sLDiscnt_rep);

        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.LockInConfirmation, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("LockInConfirmationCompanyName");
            preparer.StreetAddr = GetString("LockInConfirmationStreetAddr");
            preparer.City = GetString("LockInConfirmationCity");
            preparer.State = GetString("LockInConfirmationState");
            preparer.Zip = GetString("LockInConfirmationZip");
            preparer.PhoneOfCompany = GetString("LockInConfirmationPhoneOfCompany");
            preparer.PrepareDate_rep = GetString("LockInConfirmationPreparedDate");
            preparer.Update();
            
            dataLoan.sLT = (E_sLT) GetInt("sLT");
            dataLoan.sLAmtCalc_rep = GetString("sLAmt");
            dataLoan.sNoteIR_rep = GetString("sNoteIR");
            dataLoan.sTerm_rep = GetString("sTerm");
            dataLoan.sDue_rep = GetString("sDue");
            dataLoan.sLpTemplateNm = GetString("sLpTemplateNm");
            dataLoan.Set_sRLckdExpiredD_Manually(GetString("sRLckdExpiredD"));
            dataLoan.sLOrigFPc_rep = GetString("sLOrigFPc");
            dataLoan.sLDiscntPc_rep = GetString("sLDiscntPc");
            dataLoan.sLOrigFMb_rep = GetString("sLOrigFMb");
            dataLoan.sLDiscntFMb_rep = GetString("sLDiscntFMb");

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }
    }

	public partial class LockInConfirmationService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new LockInConfirmationServiceItem());
        }

	}
}
