﻿namespace LendersOfficeApp.newlos.ElectronicDocs
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Edocs.AutoSaveDocType;

    /// <summary>
    /// Allows users to upload documents to a loan file.
    /// </summary>
    public partial class UploadDocs : BaseLoanUserControl, IAutoLoadUserControl
    {
        /// <summary>
        /// Gets or sets the number of uploads to allow the control to display.
        /// </summary>
        public int SupportedUploadCount { get; set; } = 12;

        /// <summary>
        /// Gets or sets a value indicating whether the control supports barcode scanning to set the <seealso cref="EDocument.DocType"/>.
        /// </summary>
        public bool SupportsBarcodeScan { get; set; } = true;

        /// <summary>
        /// Gets or sets a value indicating whether the control restricts uploads to PDFs 
        /// when not uploading UCD files.
        /// </summary>
        public bool RestrictToPdfForNonUcd { get; set; } = false;

        /// <summary>
        /// Gets or sets the id of the application that will contain the uploaded document, or null if the application needs to be selected by the user.
        /// </summary>
        /// <remarks>
        /// In my testing, I couldn't set this value from the page with <c>&lt;% ... %&gt;</c> tags, so it will need to be set in the code behind.
        /// </remarks>
        public Guid? SupportedApplicationID { get; set; }

        /// <summary>
        /// Gets a value indicating whether the upload should be restricted to UCD files.
        /// </summary>
        /// <value>A value indicating whether the upload should be restricted to UCD files.</value>
        private bool RestrictToUcd
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["restricttoucd"]))
                {
                    bool restrictToUcd;
                    if (!bool.TryParse(RequestHelper.GetSafeQueryString("restricttoucd"), out restrictToUcd))
                    {
                        return false;
                    }

                    return restrictToUcd;
                }

                return false;
            }
        }

        /// <summary>
        /// Loads data for the control.
        /// </summary>
        public void LoadData()
        {
            CPageData dataloan = CPageData.CreateUsingSmartDependency(LoanID, typeof(UploadDocs));
            dataloan.InitLoad();

            IEnumerable<AppNameAndId> apps;
            if (this.SupportedApplicationID.HasValue)
            {
                var app = dataloan.GetAppData(this.SupportedApplicationID.Value);
                apps = new[]
                {
                    new AppNameAndId(app.aBNm_aCNm, app.aAppId)
                };
            }
            else
            {
                apps = dataloan.GetAppNameAndIds();
            }

            ((BasePage)this.Page).RegisterJsObjectWithJsonNetAnonymousSerializer("Apps", apps);
        }

        /// <summary>
        /// Saves data for the control.
        /// </summary>
        public void SaveData()
        {
        }

        /// <summary>
        /// Loads the control.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.DragDropZone.Attributes.Add("limit", this.SupportedUploadCount.ToString());

            var basePage = (BasePage)this.Page;

            var allowedExtensions = this.GetAllowedExtensions(basePage);
            this.DragDropZone.Attributes.Add("extensions", allowedExtensions);

            basePage.RegisterJsScript("utilities.js");
            basePage.RegisterJsScript("angular-1.5.5.min.js");
            basePage.RegisterJsScript("DragDropUpload.js");
            basePage.RegisterJsScript("DocumentUploadHelper.js");
            basePage.RegisterJsScript("UploadDocs.js");

            basePage.RegisterCSS("UploadDocs.css");
            basePage.RegisterCSS("DragDropUpload.css");

            basePage.RegisterJsGlobalVariables("RestrictToUcd", this.RestrictToUcd);
            basePage.RegisterJsGlobalVariables("SupportedApplicationID", this.SupportedApplicationID.ToString());
            basePage.RegisterJsGlobalVariables("SupportsBarcodeScan", this.SupportsBarcodeScan);
            basePage.RegisterJsGlobalVariables("ScanDocMagicBarcodesDocTypeId", ConstApp.ScanDocMagicBarcodesDocTypeId);

            Tuple<bool, string> results = Tools.IsWorkflowOperationAuthorized(this.BrokerUser, this.LoanID, LendersOffice.ConfigSystem.Operations.WorkflowOperations.UploadEDocs);
            basePage.RegisterJsGlobalVariables("CanUploadEdocs", results.Item1);
            basePage.RegisterJsGlobalVariables("UploadEdocsFailureMessage", results.Item2);

            if (this.RestrictToUcd)
            {
                var docTypeInfo = AutoSaveDocTypeFactory.GetDocTypeForPageId(E_AutoSavePage.UniformClosingDataset, this.BrokerDB.BrokerID, E_EnforceFolderPermissions.False);
                basePage.RegisterJsGlobalVariables("UcdDocType", docTypeInfo?.DocType?.Id.ToString());
            }
        }

        /// <summary>
        /// Gets the allowed extensions for the control.
        /// </summary>
        /// <param name="basePage">
        /// The base page for the control.
        /// </param>
        /// <returns>
        /// The allowed extensions.
        /// </returns>
        private string GetAllowedExtensions(BasePage basePage)
        {
            if (this.RestrictToUcd)
            {
                var docTypeInfo = AutoSaveDocTypeFactory.GetDocTypeForPageId(
                    E_AutoSavePage.UniformClosingDataset,
                    this.BrokerDB.BrokerID,
                    E_EnforceFolderPermissions.False);

                if (docTypeInfo?.DocType != null)
                {
                    basePage.RegisterJsGlobalVariables("UcdDefaultDocTypeId", docTypeInfo.DocType.Id.ToString());
                    basePage.RegisterJsGlobalVariables("UcdDefaultDocTypeName", docTypeInfo.DocType.FolderAndDocTypeName);
                }

                return ".xml";
            }

            if (this.RestrictToPdfForNonUcd)
            {
                return ".pdf";
            }

            var allowedExtensions = new List<string>(4)
            {
                ".xml",
                ".pdf"
            };

            if (this.BrokerDB.AllowExcelFilesInEDocs)
            {
                allowedExtensions.Add(".xls");
                allowedExtensions.Add(".xlsx");
            }

            return string.Join(", ", allowedExtensions);
        }
    }
}
