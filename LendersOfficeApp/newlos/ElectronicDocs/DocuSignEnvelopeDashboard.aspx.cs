﻿#region Auto-generated code
namespace LendersOfficeApp.newlos.ElectronicDocs
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.DocumentSigning;
    using LendersOffice.Security;

    /// <summary>
    /// DocuSign envelope dashboard.
    /// </summary>
    public partial class DocuSignEnvelopeDashboard : BaseLoanPage
    {
        /// <summary>
        /// Determines whether DocuSign esigning is enabled.
        /// </summary>
        private Lazy<bool> isDocuSignESignEnabled = new Lazy<bool>(() =>
        {
            var settings = LendersOffice.Admin.DocuSign.LenderDocuSignSettings.Retrieve(PrincipalFactory.CurrentPrincipal.BrokerId);

            return PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCreatingDocumentSigningEnvelopes) && settings != null && settings.IsSetupComplete() && settings.DocuSignEnabled;
        });

        /// <summary>
        /// Forces the window to IE 10 mode.
        /// </summary>
        /// <returns>The compatability mode.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.IE10;
        }

        /// <summary>
        /// Loads the data for the page.
        /// </summary>
        protected override void LoadData()
        {
            if (!isDocuSignESignEnabled.Value)
            {
                throw new AccessDenied();
            }

            Guid loanId = RequestHelper.GetGuid("loanid");
            var principal = PrincipalFactory.CurrentPrincipal;
            
            var envelopes = DocuSignEnvelope.Load(principal.BrokerId, loanId);
            EDocs.EDocumentRepository repo = EDocs.EDocumentRepository.GetUserRepository(PrincipalFactory.CurrentPrincipal);
            var loanDocs = repo.GetDocumentsByLoanId(loanId).ToDictionary(doc => doc.DocumentId);
            List<DocuSignEnvelopeViewModel> viewModels = envelopes?.Select(envelope => new DocuSignEnvelopeViewModel(envelope, loanDocs))?.ToList();

            this.RegisterJsGlobalVariables("IsInternal", principal.HasPermission(Permission.CanModifyLoanPrograms));
            this.RegisterJsObjectWithJsonNetSerializer("Envelopes", viewModels ?? new List<DocuSignEnvelopeViewModel>());
        }

        /// <summary>
        /// Initialize function.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;

            this.EnableJqueryMigrate = false;

            this.PageTitle = "DocuSign Envelopes";
            this.PageID = "DocuSignEnvelopeDashboard";

            this.RegisterJsScript("DocuSignEnvelopeDashboard.js");
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("SimplePopups.js");
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("angular-1.5.5.min.js");
            this.RegisterCSS("font-awesome.css");

            base.OnInit(e);
        }
    }
}