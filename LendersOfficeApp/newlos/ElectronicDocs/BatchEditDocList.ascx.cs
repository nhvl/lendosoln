﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using EDocs;
using DataAccess;
using System.Runtime.Serialization;
using LendersOffice.AntiXss;
using LendersOffice.Security;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class BatchEditDocList : System.Web.UI.UserControl
    {
        public ILookup<int, EDocument> DataSource;

        public override void DataBind()
        {
            NavigationList.DataSource = DataSource;
            NavigationList.DataBind();
            base.DataBind();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var Page = this.Page as BaseLoanPage;

            if (Page == null)
            {
                throw new Exception(this.GetType().Name + " must be used on a page that inherits from BaseLoanPage");
            }

            //Don't bother doing all this stuff if we're not visible.
            if (this.Visible)
            {
                Page.RegisterJsScript("json.js");
                Page.EnableJqueryMigrate = false;
                Page.RegisterJsScript("jquery-ui-1.11.4.min.js");
                Page.RegisterCSS("jquery-ui-1.11.custom.css");
                Page.RegisterJsScript("PdfEditor.js");
                Page.RegisterCSS("PdfEditor.css");
                Page.RegisterJsScript("BatchEditDocList.js");
                Page.RegisterCSS("BatchEditDocList.css");
            }
        }

        protected void NavigationList_OnDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            IGrouping<int, EDocument> item = args.Item.DataItem as IGrouping<int, EDocument>;
            Literal folderName = args.Item.FindControl("FolderName") as Literal;
            Repeater docTypeList = args.Item.FindControl("Documents") as Repeater;
            folderName.Text = item.First().Folder.FolderNm;
            docTypeList.DataSource = item;
            docTypeList.DataBind();
        }

        protected void Document_OnDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            EDocument doc = args.Item.DataItem as EDocument;
            var docTypeName = (EncodedLiteral)args.Item.FindControl("DocTypeName");
            var notes = (EncodedLiteral)args.Item.FindControl("Notes");
            var status = (PassthroughLiteral)args.Item.FindControl("Status");
            var signedImage = args.Item.FindControl("ESignImage");

            docTypeName.Text = doc.DocTypeName;
            notes.Text = doc.PublicDescription;
            signedImage.Visible = doc.IsESigned;
            switch (doc.DocStatus)
            {
                case E_EDocStatus.Blank:
                    status.Text = "<span class='docType' data-docStatusVal='"+ (int)doc.DocStatus+"' style='font-weight:bold;'></span>";
                    break;
                case E_EDocStatus.Approved:
                    status.Text = "<span class='docType' data-docStatusVal='" + (int)doc.DocStatus + "' style='font-weight:bold; color:darkgreen;'>Protected</span>";
                    break;
                case E_EDocStatus.Screened:
                    status.Text = "<span class='docType' data-docStatusVal='" + (int)doc.DocStatus + "' style='font-weight:bold; color:darkblue;'>Screened</span>";
                    break;
                default:
                    status.Text = "<span class='docType' data-docStatusVal='" + (int)doc.DocStatus + "' style='font-weight:bold; color:darkblue;'>" + AspxTools.HtmlString(doc.DocStatus.ToString()) + "</span>";
                    break;

            }
        }
    }
}
