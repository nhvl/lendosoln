﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class AcceptConsumerRequest : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            if (Page.IsPostBack)
            {
                return; 
            }

            Rotation.Value = "f";
            HandlePageRequest();
        }


        protected void HandlePageRequest()
        {
            string requestIdText = RequestHelper.GetSafeQueryString("RID");
            long requestId;
            if (!long.TryParse(requestIdText, out requestId))
            {
                throw new CBaseException(ErrorMessages.Generic, "Invalid RID");
            }

            ConsumerActionItem request = new ConsumerActionItem(requestId);

            if (request.ResponseStatus != E_ConsumerResponseStatusT.PendingResponse)
            {
                throw new CBaseException(ErrorMessages.Generic, "Request is not ready for approval");
            }

            FileDBPdfFile pdfFile = new FileDBPdfFile(request.PdfFileDbKey);


            if (RequestHelper.GetInt("Pic", -1) == 1)
            {
                RenderPng(pdfFile.GetPNG(RequestHelper.GetInt("pg")));
                return;
            }

            if (Rotation.Value == "t")
            {
                pdfFile.RotatePages(180);
            }


            //we do not want to reuse the images if something loaded this before us. 
            pdfFile.DeleteCachedImages();
            pdfFile.CachePNGFiles();

            List<EDocs.PdfPageItem> pages = pdfFile.GetPdfPageInfoList();

            RID.Value = requestIdText;
            RegisterJsObject("PdfPages", pages);

            if (!BrokerUserPrincipal.CurrentPrincipal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                return;
            }

            var principal = BrokerUserPrincipal.CurrentPrincipal;

            EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(principal.BrokerId, principal.EmployeeId, principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);

            Image2.ImageUrl = "UserSignature.aspx?eid=" + principal.EmployeeId;
            SignaturePanel.Visible = signInfo.HasUploadedSignature;

            if (signInfo.HasUploadedSignature)
            {
                var details = signInfo.SignatureDetails;

                if (details.Width > details.Height)
                {
                    Image2.Height = 50;
                    Image2.Width = (int)(50 * details.AspectRatio);
                }
                else
                {
                    Image2.Width = 168;
                    Image2.Height = (int)(168 / details.AspectRatio);
                }

                Image2.Attributes.Add("ar", details.AspectRatio.ToString("0.00"));
            }
        }

        protected void RotatePage(object sender, EventArgs args)
        {
            Rotation.Value = Rotation.Value == "f" ? "t" : "f";
            HandlePageRequest();

        }

        private void RenderPng(byte[] buffer)
        {
            Response.Clear();
            Response.ContentType = "image/png";
            Response.Cache.SetExpires(DateTime.Now.AddDays(1));
            Response.Cache.SetLastModified(DateTime.Now);
            Response.OutputStream.Write(buffer, 0, buffer.Length);
            Response.Flush();
            Response.End();

        }


    }
}
