﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using EDocs;
using PdfRasterizerLib;
using LendersOffice.Constants;
using System.IO;
using iTextSharp.text.pdf;
using System.Web.Services;
using LendersOffice.PdfLayout;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class PreviewAndSignPdf : BasePage
    {
        public string Key
        {
            get { return RequestHelper.GetSafeQueryString("key"); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (RequestHelper.GetSafeQueryString("Pic") == "1")
            {
                string file = AutoExpiredTextCache.GetFileFromCache(GetPngFileName(RequestHelper.GetInt("pg")));

                if (string.IsNullOrEmpty(file))
                {
                    Response.TrySkipIisCustomErrors = true;
                    Response.StatusCode = 404;
                    return;
                }

                Response.Clear();
                Response.ContentType = "image/png";
                Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
                Response.Cache.SetLastModified(DateTime.Now);
                Response.WriteFile(file);
                Response.Flush();
                Response.End();
                return;
            }

            EnableJqueryMigrate = false;
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("pdfeditor.js");
            ClientScript.RegisterHiddenField("Key", Key);
            string fullkey = Key;
            
            if (RequestHelper.GetSafeQueryString("ie") == "1")
            {
                ClientScript.RegisterHiddenField("docid", RequestHelper.GetGuid("docid").ToString());
                //keep in sync with select form 
                fullkey = SelectForm.GetEdocCacheKey(BrokerUserPrincipal.CurrentPrincipal.UserId, RequestHelper.GetGuid("docid"), new Guid(Key));
            }
            else
            {
                ClientScript.RegisterHiddenField("docid", Guid.Empty.ToString());
                fullkey = Key + "_" + BrokerUserPrincipal.CurrentPrincipal.UserId.ToString();
            }

            byte[] m_pdfData = AutoExpiredTextCache.GetBytesFromCache(fullkey);
            List<PdfPageItem> pages = GetPageInfoList(m_pdfData);
            RegisterJsObject("PdfPages", pages);

            IPdfRasterizer rasterizer = PdfRasterizerFactory.Create(ConstSite.PdfRasterizerHost);
            List<byte[]> pngContentList = rasterizer.ConvertToPng(m_pdfData, ConstAppDavid.PdfPngScaling);

            for (int i = 1; i <= pngContentList.Count; i++)
            {
                string outputFileName = GetPngFileName(i);
                AutoExpiredTextCache.RemoveFromCacheImmediately(outputFileName);
                AutoExpiredTextCache.AddToCache(pngContentList[i - 1], TimeSpan.FromHours(1), outputFileName);
            }

            if (BrokerUserPrincipal.CurrentPrincipal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                var principal = BrokerUserPrincipal.CurrentPrincipal;

                EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(principal.BrokerId, principal.EmployeeId, principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);

                Image2.ImageUrl = "UserSignature.aspx?eid=" + principal.EmployeeId;
                SignaturePanel.Visible = signInfo.HasUploadedSignature;

                if (signInfo.HasUploadedSignature)
                {
                    var details = signInfo.SignatureDetails;

                    if (details.Width > details.Height)
                    {
                        Image2.Height = 50;
                        Image2.Width = (int)(50 * details.AspectRatio);
                    }
                    else
                    {
                        Image2.Width = 168;
                        Image2.Height = (int)(168 / details.AspectRatio);
                    }

                    Image2.Attributes.Add("ar", details.AspectRatio.ToString("0.00"));
                }
            }
        }


        private string GetPngFileName(int page)
        {
            return string.Format("CreatRequest_{0}_{1}.png", Key, page);
        }

        private List<PdfPageItem> GetPageInfoList(byte[] pdf)
        {
            PdfReader pdfReader = EDocumentViewer.CreatePdfReader(pdf);
            int pageCount = pdfReader.NumberOfPages;
            List<PdfPageItem> list = new List<PdfPageItem>(pageCount);
            for (int i = 1; i <= pageCount; i++)
            {
                var size = pdfReader.GetPageSizeWithRotation(i);
                list.Add(new PdfPageItem() { DocId = Guid.Empty, Page = i, Version = 0, Rotation = 0, PageHeight = (int)size.Height, PageWidth = (int)size.Width });

            }

            return list;

        }

        [WebMethod]
        public static string AddSignatures(string key, string pages, Guid docId)
        {
            if (docId != Guid.Empty)
            {
                Guid gKey = new Guid(key);
                key = SelectForm.GetEdocCacheKey(PrincipalFactory.CurrentPrincipal.UserId, docId, gKey);
            }
            else
            {
                key = key + "_" + BrokerUserPrincipal.CurrentPrincipal.UserId.ToString();
            }

            var fields = ObsoleteSerializationHelper.JsonDeserialize<List<PdfField>>(pages);
            EDocumentViewer.SignPdf(BrokerUserPrincipal.CurrentPrincipal, key, fields);
            return "OK";
        }



    }
}
