﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeleteEDocPage.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.DeleteEDocPage" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Delete EDoc Page</title>
    <style>
    .padding {
      padding-left:10px;
    }
    .page_textbox 
    {
      width:30px;
      text-align:center;
    }    
    </style>
</head>
<body bgcolor="gainsboro" onunload="getModalArgs().callback()">
  <script type="text/javascript">
    var g_iCurrentPage = 0;
    var g_iTotalPages = 0;
    function _init() {
      
      if(window.opener && window.opener.SetupModelessEnvironment){
          window.opener.SetupModelessEnvironment(window);
      }
      
      resize(200, 150);
      
      var arg = getModalArgs() || {};
      g_iTotalPages = arg.TotalPages;
      g_iCurrentPage = arg.CurrentPage;
      document.getElementById("NumOfPagesLabel").innerText = g_iTotalPages;

      var selectionFrom = 1, selectionTo = 1; 
      if ( arg.SelectedRanges != null && arg.SelectedRanges[0]  != null){
          var selectionRange = arg.SelectedRanges[0]; 
          selectionFrom = selectionRange.Start ? selectionRange.Start + 1 : 1;
          selectionTo = selectionFrom + (selectionRange.Count ? selectionRange.Count - 1 : 0);
      }
      
      document.getElementById("tbFrom").value = selectionFrom;
      document.getElementById("tbTo").value = selectionTo;
    }
    function onOK() {
      var arg = getModalArgs() || {};

      var bIsSelected = document.getElementById("rbCurrentPage").checked;
      var fromPage = 0;
      var numOfPagesDelete = 0;
      
      if (bIsSelected) {
        arg.Selected = true;
      } else {
        fromPage = parseInt(document.getElementById("tbFrom").value);
        var to = parseInt(document.getElementById("tbTo").value);
        if (isNaN(fromPage) || isNaN(to) || to < fromPage || fromPage <= 0 || to > g_iTotalPages || fromPage.toString() !=document.getElementById("tbFrom").value || to.toString() != document.getElementById("tbTo").value) {
          alert('Invalid Page Ranges.');
          document.getElementById("tbFrom").focus();
          return;
        }

        numOfPagesDelete = to - fromPage + 1;     
        arg.FromPage = fromPage-1;
        arg.NumOfPagesDelete = numOfPagesDelete; 
      }
      //We need a zero-based from page, the user enters it in as one-based

      arg.OK = true;
      onClosePopup(arg);
    }
    function onCancel() {
      var arg = getModalArgs() || {};

      arg.OK = false;
      onClosePopup(arg);
    }
    function renderUI() {
      var bIsSelectedPages = <%= AspxTools.JsGetElementById(rbCurrentPage) %>.checked;

      if (bIsSelectedPages) {
        document.getElementById("tbFrom").value = "";
        document.getElementById("tbTo").value = "";
      }
    }
    function enablePageRange() {
      <%= AspxTools.JsGetElementById(rbPageRange) %>.checked = true;
    }
  </script>
    <form id="form1" runat="server">
    <div>
    <table width="100%" cellpadding="5" cellspacing="0" id="MainTable">
      <tr><td class="MainRightHeader">Delete Pages</td></tr>
      <tr>
        <td class="padding">
          <asp:RadioButton ID="rbCurrentPage" Checked="true" runat="server" GroupName="sel" Text="Selected pages" onclick="renderUI();" />
        </td>
      </tr>
      <tr>
        <td class="padding">
          <asp:RadioButton ID="rbPageRange" runat="server" GroupName="sel" Text="From:" />
          <input type="text" class="page_textbox" id="tbFrom" onfocus="enablePageRange();"/> To: 
          <input type="text" class="page_textbox" id="tbTo" onfocus="enablePageRange();"/> of 
          <span id="NumOfPagesLabel" />
          </label>
        </td>
      </tr>
      <tr>
        <td align="center">
          <input type="button" value="OK" onclick="onOK();"/>&nbsp;&nbsp;<input type="button" value="Cancel" onclick="onCancel();"/>
        </td>
      </tr>
    </table>
    </div>
    <uc1:cmodaldlg id="Cmodaldlg1" runat="server" />
    </form>
</body>
</html>
