﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreviewAndSignPdf.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.PreviewAndSignPdf" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Preview &amp; Sign</title>
        <style type="text/css">
        body { 
            background-color: gainsboro;
            overflow-x: hidden; 
            font-weight: bold;
        }

        #loadingmask { 
            display: block; 
            position:absolute;  
            background-color: Gray; 
            filter:alpha(opacity=60); 
            opacity: 0.6; 
            top : 0;
            bottom: 0;
            left: 0;
            right: 0;
            z-index: 9999;
            
        }
    </style>
    <link type="text/css" href="../../css/pdfeditor.css" rel="Stylesheet" />
    <link type="text/css" href="../../css/jquery-ui-1.11.custom.css" rel="Stylesheet" />
</head>
<body>
    <h4 class="page-header">Preview</h4>
    <div id="loadingmask" ></div>
    
    <form id="form1" runat="server">
    <div id="PdfEditor">
    
    </div>
    
        <asp:Panel runat="server" ID="SignaturePanel" Visible="false">
        &nbsp;&nbsp;Drag and drop signature into place on document to sign
        <table border="0" cellpadding="5">
            <tr>
                <td>
                    <div class="pdf-field-template pdf-field-signature-img" id="sigDiv">
                        <asp:Image ID="Image2" CssClass="pdf-field-content" runat="server" ImageUrl="~/images/edocs/signature.png"
                            Width="100%" Height="100%" />
                    </div>
                </td>
                <td>
                    <div class="pdf-field-template pdf-field-date">
                        <asp:Image ID="DateIMg" CssClass="pdf-field-content" runat="server" ImageUrl="~/ViewEDocPdf.aspx?cmd=date"
                            Width="100%" Height="100%" />
                    </div>
                </td>
            </tr>
        </table>
                &nbsp;&nbsp;&nbsp;&nbsp; <input type="button" class="ButtonStyle"  disabled="disabled" id="Save" value="Save" NoHighlight="NoHighlight"  /> &nbsp;&nbsp;&nbsp;&nbsp;
        <input type="button" class="ButtonStyle" id="Cancel" value="Cancel" />
    </asp:Panel>
    
    </form>
    <script type="text/javascript">
        $(function(){
            var div = $('#sigDiv');
            if( div.length > 0 ) {
            var width = div.children('img').width();
            var height = div.children('img').height();
            div.height(height);
            div.width(width);
            div.children('img').width('100%');
            div.children('img').height('100%');
            }
           var save = $('#Save'); 
           resizeForIE6And7(1024, 768);
           var uniqueKey = Math.round(Math.random()*10000000);
           var key = $('#Key').val();
           var  pdfEditor = new LendersOffice.PdfEditor('PdfEditor', PdfPages, {
                GenerateFullPageSrc : function(o) {
                    var data = ['PreviewAndSignPdf.aspx?Pic=1&Key=',key,'&',o.Page,'&pg=',o.Page,'&crack=',uniqueKey]; 
                    return data.join(''); 
                },
                AllowEdit: function () { return true; },
                CanEditAnnotations: true,
                EmptyImgSrc: gVirtualRoot + '/images/pixel.gif',
                OnPdfFieldDrop : function() {
                    save.prop("disabled", false);
                }                  
            });
            
            pdfEditor.Initialize(function() { $('#loadingmask').hide();  } );
                
            function _GetSignatureSaveData(){
                var signatures = [];
                $.each(pdfEditor.GetPdfFields(), function(i,o){
                    if( o.cssClass == 'pdf-field-signature-img' || o.cssClass == 'pdf-field-date' ) {
                        signatures.push(o);
                    }
                });
                return JSON.stringify(signatures);
            }
        
            
            $('.pdf-field-template').draggable({
                revert:'invalid', 
                helper:'clone', 
                zIndex: 2700, 
                start : function() {
                    $('#editor').css('z-index', '-1' ); //needed for 6 and 7 so the draggable doesnt end in the back of the editor
                },
                stop : function() {
                    $('#editor').css('z-index', '' ); 
                }
            });
            $('#Cancel').click(function(){onClosePopup()});
            $(document).keyup(function(e){
            
                var keyPressed = e.which;
                if( keyPressed !== 46 && keyPressed !== 8 ) {
                    return;
                }
                $.each(pdfEditor.GetSelectedPdfFields(),function(idx,o){
                    var $this  = $(o);
                    pdfEditor.RemovePdfField(o);
                });
                
                
                if( pdfEditor.GetPdfFields().length === 0 ) {
                    save.prop('disabled', true);
                }
                
            });   
            save.click(function(){
                save.prop('disabled', true);
                var sig = _GetSignatureSaveData();
                if( sig === '[]') {
                    onClosePopup();
                }
                else {
                    if( false == confirm('Signatures will be permanently applied to this document.')  ) {
                        save.prop("disabled", false);
                        return; //nothing to do 
                    }
                    var data = {
                        'key' : key,
                        'pages' : sig,
                        'docId' : $('#docid').val()
                    };
                    
                    data = JSON.stringify(data);
                    callWebMethodAsync({
                        type: "POST",
                        url: "PreviewAndSignPdf.aspx/AddSignatures",
                        data: data,
                        contentType: 'application/json; charset=utf-8',
                        dataType: "json"
                    }).then(
                        function(msg){
                          if( msg.d == 'OK' ) {
                            onClosePopup();
                          }    
                          else {
                            alert('Could not sign pdf.');
                            save.prop("disabled", false);
                            
                          }
                        },
                        function(){
                            alert('Could not sign pdf');
                            save.prop("disabled", false);
                        }
                    );
                }
            });
        });
    </script>
</body>
</html>
