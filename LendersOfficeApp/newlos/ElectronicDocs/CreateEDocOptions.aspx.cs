﻿#region generated code
namespace LendersOfficeApp.newlos.ElectronicDocs
#endregion
{
    using System;
    using System.Web.UI.WebControls;
    using LendersOffice.Common;

    /// <summary>
    /// Allows a user to specify options for a <see cref="EDocs.EDocument"/> that will be created.
    /// </summary>
    public partial class CreateEDocOptions : BaseLoanPage
    {
        /// <summary>
        /// Handles the Page object's <c>Init</c> event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            this.DisplayCopyRight = false;
            this.RegisterJsScript("Utilities.js");
        }

        /// <summary>
        /// Handles the Page object's <c>Load</c> event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var loan = DataAccess.CPageData.CreateUsingSmartDependency(this.LoanID, typeof(CreateEDocOptions));
            loan.InitLoad();
            foreach (var app in loan.Apps)
            {
                string appDescription = app.aBNm + (string.IsNullOrEmpty(app.aCNm) ? null : (" & " + app.aCNm));
                this.ApplicationPicker.Items.Add(new ListItem(appDescription, app.aAppId.ToString()));
            }
        }
    }
}