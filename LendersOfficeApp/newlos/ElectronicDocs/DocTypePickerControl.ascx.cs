﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using EDocs;
using LendersOffice.Security;
using LendersOffice.Constants;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class DocTypePickerControl : System.Web.UI.UserControl
    {
        // These fields double as html tag attributes
        // So, you can do something like
        //     <uc:DocTypePicker id="..." runat="..." Width="250px" OnChange="alert('hello');">
        public string Width = "";
        public string OnChange = "";
        public string Value
        {
            get { return m_selectedDocType.Text; }
            set { m_selectedDocType.Text = value; }
        }
        public string DefaultId
        {
            get
            {
                return m_defaultId.Value;
            }
        }

        public DocType SelectedDocType
        {
            get
            {
                var id = int.Parse(Value);
                if (id < 0) return null;

                return EDocumentDocType.GetDocTypeById(this.BrokerID, id);
            }
        }

        public string DefaultText
        {
            get
            {
                return m_defaultText.Value;
            }
            set
            {
                m_defaultText.Value = value;
                m_selectedDocTypeDescription.Text = value;
            }
        }

        public string LinkText
        {
            get
            {
                return m_dialogLink.InnerText;
            }
            set
            {
                m_dialogLink.InnerText = value;
            }
        }

        public bool Enabled
        {
            get
            {
                return m_selectedDocType.Enabled && m_selectedDocTypeDescription.Enabled;
            }
            set
            {
                m_selectedDocType.Enabled = value;
                m_dialogLink.Style["display"] = value ? "" : "none";
            }
        }
        public TextBox ValueCtrl { get { return m_selectedDocType; } }
        public Label DescriptionCtrl { get { return m_selectedDocTypeDescription; } }

        protected Guid BrokerID 
        {
            get 
            {
                if (BrokerUserPrincipal.CurrentPrincipal != null)
                {
                    return BrokerUserPrincipal.CurrentPrincipal.BrokerId;
                }
                else
                {
                    return LendersOffice.Common.RequestHelper.GetGuid("brokerid");
                }                
            } 
        }

        public void Clear()
        {
            m_selectedDocType.Text = "-1";
            m_selectedDocTypeDescription.Text = DefaultText;
        }

        public void Select(int docTypeId)
        {
            this.Value = docTypeId.ToString();
            CalcAndDescriptionText(this.Value);
        }

        public void EnableClearLink()
        {
            m_closeSpan.Visible = true;
            m_clearLink.Attributes.Add("onclick", string.Concat("return ", AspxTools.HtmlString(this.ClientID), ".clear();"));
        }

        public void SetDefault(string defaultText, string defaultId)
        {
            DefaultText = defaultText;
            m_defaultId.Value = defaultId;
        }

        private void CalcAndDescriptionText(string docTypeId)
        {
            if (docTypeId == ConstApp.ScanDocMagicBarcodesDocTypeId.ToString()) // OR we could add a doctype with id=-100... nah
            {
                m_selectedDocTypeDescription.Text = "--Assign Using Doc Magic Barcodes--";
            }
            else if (docTypeId != "-1")
            {
                var docTypeList = EDocumentDocType.GetDocTypesByBroker(BrokerID, E_EnforceFolderPermissions.True).ToList();
                var selectedDocType = docTypeList.Find((DocType d) => d.DocTypeId == this.Value);
                if (selectedDocType == null)
                {
                    return;
                }
                m_selectedDocTypeDescription.Text = selectedDocType.FolderAndDocTypeName;
            }
        }

        public void DisablePicking()
        {
            m_dialogSpan.Visible = false;
            m_clearLink.Visible = false;
            m_dialogLink.Visible = false;
            m_closeSpan.Visible = false;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            m_selectedDocType.Style.Add("display", "none");
            DefaultText = "No Doc Type Selected";
            LinkText = "select Doc Type";

            // This should be put in the ascx file, but we can't put it there since the control is runat="server"
            m_dialogLink.Attributes["onclick"] = AspxTools.HtmlString(this.ClientID) + ".showDialog(this); return false;";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            m_DocTypePickerDiv.Style.Add("width", Width);
            m_selectedDocType.Attributes.Add("onchange", OnChange);
            CalcAndDescriptionText(this.Value);
        }
    }
}