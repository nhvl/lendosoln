﻿#region Generated Code
namespace LendersOfficeApp.newlos.ElectronicDocs
#endregion
{
    using System;
    using LendersOffice.Common;
    using LendersOffice.PdfForm;

    /// <summary>
    /// Provides a means for selecting custom PDF forms.
    /// </summary>
    public partial class SelectCustomPdfForm : BasePage
    {
        /// <summary>
        /// Gets a value indicating whether the page should display the clear button.
        /// </summary>
        private bool DisplayClearButton => RequestHelper.GetBool("displayClearButton");

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.RegisterJsGlobalVariables("DisplayClearButton", this.DisplayClearButton);

            this.FormList.DataSource = PdfForm.RetrieveCustomFormsOfCurrentBroker();
            this.FormList.DataBind();
        }
    }
}
