﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using EDocs;
using DataAccess;
using LendersOffice.Common;
using System.Web.Services;
using LendersOffice.ObjLib.DatabaseMessageQueue;
using LendersOfficeApp.los.admin;
using System.Text;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class CopyDocsToLoanFilesService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "SubmitDocsToCopyQueue":
                    SubmitDocsToCopyQueue();
                    break;
            }
        }

        /// <summary>
        /// Submits the documents to the queue for processing.
        /// </summary>
        private void SubmitDocsToCopyQueue()
        {
            List<string> docIds = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("DocIds"));
            List<string> destLoanIds = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("DestLoanIds"));
            Guid srcLoanId = GetGuid("SrcLoanId");

            DBMessageQueue copyEDocQueue = new DBMessageQueue(ConstMsg.CopyEDocQueue);
            string brokerId = PrincipalFactory.CurrentPrincipal.BrokerId.ToString();
            string userId = PrincipalFactory.CurrentPrincipal.UserId.ToString();
            string employeeId = PrincipalFactory.CurrentPrincipal.EmployeeId.ToString();

            // Each message will correspond to an entire copy transaction.
            StringBuilder messageData = new StringBuilder("DestLoanIds=");
            foreach (string destLoanId in destLoanIds)
            {
                messageData.AppendFormat("{0},", destLoanId);
            }
            messageData.Append(";DocIds=");
            foreach (string docId in docIds)
            {
                messageData.AppendFormat("{0},", docId);
            }

            // Submit a message to the queue to be processed by the EDocsCopyQueueProcessor runnable.
            copyEDocQueue.Send(String.Format("UserId={0},BrokerId={1},EmployeeId={2},SrcLoanId={3}", userId, brokerId, employeeId, srcLoanId), messageData.ToString());
        }
    }
}
