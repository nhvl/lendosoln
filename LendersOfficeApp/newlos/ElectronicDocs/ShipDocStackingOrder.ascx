﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShipDocStackingOrder.ascx.cs"
    Inherits="LendersOfficeApp.newlos.ElectronicDocs.ShipDocStackingOrder" %>
    
    <%@ Import Namespace="LendersOffice.AntiXss" %>
    <%@ Import Namespace="DataAccess" %>
    <asp:HiddenField runat="server" ID="HasPDFWithOwnerPassword" />
    <asp:HiddenField runat="server" ID="HasPDFWithUnsupportedFeatures" />
    <script type="text/javascript" src="../../inc/utilities.js"></script>
    <script type="text/javascript" src="../../inc/jquery-ui-1.11.4.min.js" ></script> 
<style>
    #DownloadModal {
        padding: 5px;
        border: 3px solid black;
        position: absolute;
        z-index: 900;
        background-color: whitesmoke;
        height: 140px;
        width: 420px;
    }
    #DownloadModal table {
        width: 100%;
        text-align: left;
    }
    #DownloadModal th {
        text-align: left;
    }
    .ShippingPackageProgress div {
        padding: 10px;
    }
</style>


    <script type="text/javascript" >
        var currentSort = '';

        $(document).ready(function() {
            $("#sort").tablesorter({
                headers: {
                    0: { sorter: false }
                   , 4: { sorter: false }
                   , 5: { sorter: false }
                   , 6: { sorter: false }
                   , 8: { sorter: false }
                }
            });

            $("#folderSortLink").click(function() {
                var sortedOrder = getSortedOrder();
                currentSort = [[[1, sortedOrder], [2, sortedOrder], [3, sortedOrder]]];
                $("table").trigger("sorton", currentSort);
                setSortedOrder();
                return false;
            });

            $("#modDateSortLink").click(function() {
                var sortedOrder = getSortedOrder();
                currentSort = [[[7, sortedOrder]]];
                $("table").trigger("sorton", currentSort);
                setSortedOrder();
                return false;
            });

            $("#docTypeSortLink").click(function() {
                // By putting the current sort in the sorton trigger, it will effectively not sort the individual 
                // column
                // I need to do this instead of setting the header sorter to false because I want the column
                // to be sortable, but only when a different column is clicked, not when this column is clicked.
                // OPM 53189
                $("table").trigger("sorton", currentSort);
                return false;
            });

            $("#applicationSortLink").click(function() {
                // By putting the current sort in the sorton trigger, it will effectively not sort the individual 
                // column
                // I need to do this instead of setting the header sorter to false because I want the column
                // to be sortable, but only when a different column is clicked, not when this column is clicked.
                // OPM 53189
                $("table").trigger("sorton", currentSort);
                return false;
            });

            if ($('#<%= AspxTools.ClientId(HasPDFWithOwnerPassword)%>').val() === 'True' ||
                    $('#<%= AspxTools.ClientId(HasPDFWithUnsupportedFeatures)%>').val() === 'True') {
                $('#singlepdf input').prop('disabled', true);
                $('#DownloadZip').prop('checked', true);
                $('#SinglePdfError, #Star').show();
            }
            else {
                $('#singlepdf input').prop("disabled", false);
                $('#SinglePdfError, #Star').hide();
            }

            $('#sort tbody').sortable({
                helper: function(e, ui) {
                    ui.children().each(function() {
                        $(this).width($(this).width());
                    });
                    return ui;
                },
                forcePlaceholderSize: true,
                forceHelperSize: true,
                start: function(event, ui) {
                    $(this).sortable('refreshPositions')
                }
            });

            document.getElementById('Download').style.display = '';
            document.getElementById('PrintToEDocs').style.display = '';
        });
    </script>
    <table cellpadding="2" style="width: 100%;" id="sort">
        <thead>
        <tr class="GridHeader">
            <th style="width: 50px">
                Re-Order
            </th>
            <th id="folderSortLink" >
                <a href="#">Folder</a>
            </th>
            <th id="docTypeSortLink">
                Doc Type
            </th>
            <th id="applicationSortLink">
                Application
            </th>
            <th >
                Description
            </th>
            <th >
                Internal Comments
            </th>
            <th>
                Pages
            </th>
            <th style="width:100px" id="modDateSortLink" >
                <a href="#">Last Modified</a>
            </th>
            <th style="width:50px" >
                &nbsp;
            </th>
        </tr>
        </thead>
        <tbody id="DocumentStackingOrder" >
            <asp:Repeater runat="server" ID="Documents" OnItemDataBound="DocumentStackingOrder_OnItemDataBound">
                <ItemTemplate>
                    <tr class="GridItem">
                        <td align="center">
                            <a href='javascript:void(0);' style='text-decoration: none' onclick='moveUpInList(this.parentNode.parentNode);'>
                                &#x25b2;</a> <a href='javascript:void(0);' style='text-decoration: none' onclick='moveDownInList(this.parentNode.parentNode);'>
                                    &#x25bc;</a>
                            <asp:HiddenField runat="server" ID="id" />
                            <asp:HiddenField runat="server" ID="IsESigned" />
                        </td>
                        <td>
                            <ml:EncodedLiteral runat="server" ID="Folder"></ml:EncodedLiteral>
                        </td>
                        <td>
                            <ml:EncodedLiteral runat="server" ID="DocType"></ml:EncodedLiteral>
                        </td>
                        <td>
                            <ml:EncodedLiteral runat="server" ID="Application"></ml:EncodedLiteral>
                        </td>
                        <td>
                            <ml:EncodedLiteral runat="server" ID="Description"></ml:EncodedLiteral>
                        </td>
                        <td>
                            <ml:EncodedLiteral runat="server" ID="InternalDescription"></ml:EncodedLiteral>
                        </td>
                        <td  align="right">
                            <ml:EncodedLiteral runat="server" ID="PageCount"></ml:EncodedLiteral>
                        </td>
                        <td>
                            <ml:EncodedLiteral runat="server" ID="LastModified"></ml:EncodedLiteral>
                        </td>
                        <td align="center">
                            <a runat="server" id="viewLink">view</a>
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="GridAlternatingItem">
                        <td align="center">
                            <a href='javascript:void(0);' style='text-decoration: none' onclick='moveUpInList(this.parentNode.parentNode);'>
                                &#x25b2;</a> <a href='javascript:void(0);' style='text-decoration: none' onclick='moveDownInList(this.parentNode.parentNode);'>
                                    &#x25bc;</a>
                            <asp:HiddenField runat="server" ID="id" />
                            <asp:HiddenField runat="server" ID="IsESigned" />
                        </td>
                        <td>
                            <ml:EncodedLiteral runat="server" ID="Folder"></ml:EncodedLiteral>
                        </td>
                        <td>
                            <ml:EncodedLiteral runat="server" ID="DocType"></ml:EncodedLiteral>
                        </td>
                        <td>
                            <ml:EncodedLiteral runat="server" ID="Application"></ml:EncodedLiteral>
                        </td>
                        <td>
                            <ml:EncodedLiteral runat="server" ID="Description"></ml:EncodedLiteral>
                        </td>
                        <td>
                            <ml:EncodedLiteral runat="server" ID="InternalDescription"></ml:EncodedLiteral>
                        </td>
                        <td align="right">
                            <ml:EncodedLiteral runat="server" ID="PageCount"></ml:EncodedLiteral>
                        </td>
                        <td>
                            <ml:EncodedLiteral runat="server" ID="LastModified"></ml:EncodedLiteral>
                        </td>
                        <td align="center">
                            <a runat="server" id="viewLink">view</a>
                        </td>
                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:Repeater>
        </tbody>
    </table>

    <div id="DownloadModal" class="Hidden">
        <table>
            <thead>
                <tr>
                    <th>
                        Download
                    </th>
                </tr>
            </thead>
            <tbody id="DownloadOptions">
                    <tr>
                        <td>
                            
                            <label id="singlepdf">
                            <input type="radio" name="DownloadType"  checked="checked" id="DownloadSingle" value="1" />
                            as a single PDF document  
                            </label><span id="Star" style="color: Red" >*</span>
                            <br />
                            <input type="radio" name="DownloadType" id="DownloadZip" value="0" />
                            as a zip file containing multiple PDF documents
                            <span class="ESignMergeOption">
                                <br />
                                <input type="radio" name="DownloadType" id="DownloadZipMergingDocsWithoutSignature" value="2" />
                                as a zip file with all unsigned PDF documents merged and all signed PDF documents in the zip file
                            </span>
                        </td>
                    </tr>
                    <tr id="SinglePdfError">
                        <td style="color:Red;">
                            *Cannot download a single pdf because one or more of the selected eDocs has an owner password or an unsupported feature.
                        </td>
                    </tr>
                    <tr>
                        <td class="RightAlign">
                            <input type="button" value="Download" onclick="downloadStart_onClick();"/>
                            <input type="button" value="Cancel" onclick="Modal.Hide();" />
                        </td>
                    </tr>
            </tbody>
            <tbody id="DownloadProgress" style="display:none">
                <tr>
                    <td>
                        Please wait while we build your shipping package 
                    </td>
                </tr>
                <tr>
                    <td><img src="../../images/status.gif" alt="Loading" /></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="Hidden" id="LQBPopupProgress">
        <div>Please wait while we build your shipping package</div>
        <div><img src="../../images/status.gif" alt="Loading" /></div>
    </div>
    
<script type="text/javascript">
    function removeFromList(row) {
        var tBody = document.getElementById('DocumentStackingOrder');
        tBody.deleteRow(row.rowIndex - 1);
    }

    function moveDownInList(row) {
        var tBody = document.getElementById('DocumentStackingOrder');
        var clickedRowIndex = row.rowIndex;
        if (clickedRowIndex != tBody.rows.length) {
            row1 = tBody.getElementsByTagName("tr")[clickedRowIndex - 1];
            row2 = tBody.getElementsByTagName("tr")[clickedRowIndex];
        }
        else {
            row1 = tBody.getElementsByTagName("tr")[0];
            row2 = tBody.getElementsByTagName("tr")[clickedRowIndex - 1];
        }
        tBody.insertBefore(row2, row1);
        
    }

    function moveUpInList(row) {
        var tBody = document.getElementById('DocumentStackingOrder');
        var clickedRowIndex = row.rowIndex;
        if (clickedRowIndex != 1) {
            row1 = tBody.getElementsByTagName("tr")[clickedRowIndex - 2];
            row2 = tBody.getElementsByTagName("tr")[clickedRowIndex - 1];
            tBody.insertBefore(row2, row1);
        }
        else {
            row = tBody.getElementsByTagName("tr")[clickedRowIndex - 1];
            tBody.appendChild(row);          
        }
    }

    function getOrder() {
        var tBody = document.getElementById('DocumentStackingOrder');
        var order = '';
        for (var i = 0; i < tBody.rows.length; i++)
            order += tBody.getElementsByTagName("tr")[i].children[0].innerHTML + ',';
        
        return order;
    }
    
    
    function MainDownload_onClick(event) {
        Modal.ShowPopup('DownloadModal', null, event);
        
        $('.ESignMergeOption').toggle(containsESignedDocuments());
    }

    function containsESignedDocuments() {
        return $('input[type="hidden"][id$="IsESigned"][value="True"]').length > 0;
    }
    
    function downloadStart_onClick() {
        var downloadType = $('input[type="radio"][name="DownloadType"]:checked').val();
        if (downloadType === '1' && containsESignedDocuments()) {
            var msg = 'This shipping package contains an ESigned document. ' +
                'Downloading a single PDF document will invalidate the signatures. ' +
                'Do you want to continue?';
            if (!confirm(msg)) {
                return;
            }
        }
        
        lar = window;
        document.getElementById('DownloadOptions').style.display = 'none';
        document.getElementById('DownloadProgress').style.display = 'block';

        var obj = {
            IDS : getDocumentIds(),
            DownloadType : downloadType
        };
        
        var results = gService.edoc.call('GenerateShippingPackage', obj);
        
        if ( results.error ) {
            alert(results.error.UserMessage);
        }
        else {
            var url = <%= AspxTools.SafeUrl(GetUrl("~/newlos/ElectronicDocs/GetShippingPackage.aspx")) %> + '?key=' + results.value.key + '&sLId=' + ML.sLId;
            fetchFileAsync(url, downloadType == '1' ? 'application/pdf' : 'application/octet', fnStartInit);
        }
    }
    function getDocumentIds() {
        var tBody = document.getElementById('DocumentStackingOrder');
        var rows = tBody.getElementsByTagName("tr");
        var ids = new Array();
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            var inputs = row.getElementsByTagName('input');
            for (var x = 0; x < inputs.length; x++) {
                var input = inputs[x];

                if (input.type === 'hidden' && input.id.match(/id$/)) {
                    ids.push(input.value);
                }
            }
        }

        return ids;
    }

    function MainPrintToEDocs_onClick() {
        if (containsESignedDocuments()) {
            var msg = 'This shipping package contains an ESigned document. ' +
                'Saving a single PDF document will invalidate the signatures. ' +
                'Do you want to continue?';
            if (!confirm(msg)) {
                return;
            }
        }

        var printArgs = {
            IDS: getDocumentIds().join(','),
            sLId: ML.sLId
        };

        var edocMetadata = null;

        var args = {
            'setEDocMetadata': function (d) { edocMetadata = d; }
        };

        var settings = {
            'hideCloseButton': true,
            'height': 415,
            'width': 400,
            'onReturn': function () { returnFromPrintToEDocsEDocSettings(printArgs, edocMetadata); }
        };
        LQBPopup.Show(gVirtualRoot + '/newlos/ElectronicDocs/CreateEDocOptions.aspx?loanid=' + ML.sLId, settings, args);
    }
    function returnFromPrintToEDocsEDocSettings(printArgs, edocMetadata) {
        printArgs['EDoc_aAppId'] = edocMetadata.appId;
        printArgs['EDoc_DocTypeId'] = edocMetadata.docType;
        printArgs['EDoc_Description'] = edocMetadata.description;
        printArgs['EDoc_InternalComments'] = edocMetadata.internalComments;

        var onSuccess = returnFromPrintToEDocsAction;
        var onError = returnFromPrintToEDocsAction;

        window.setTimeout(function () { LQBPopup.ShowElement($('#LQBPopupProgress'), { 'hideCloseButton': true, 'popupClasses': 'ShippingPackageProgress', 'height': 100, 'width': 300 }); }, 0);
        gService.edoc.callAsyncSimple('PrintShippingPackageToEDocs', printArgs, onSuccess, onError);
    }
    function returnFromPrintToEDocsAction(result) {
        window.setTimeout(LQBPopup.Hide, 0);
        if (result.value && result.value.Success === 'True') {
            alert('Document(s) successfully printed to EDocs.');
        } else {
            alert((result.value && result.value.ErrorMessage) || 'An error occurred while printing to EDocs.');
        }
    }

function fnStartInit(event)
{
        document.getElementById('DownloadOptions').style.display = '';
        document.getElementById('DownloadProgress').style.display = 'none';
        Modal.Hide();
}
function fnStartInitLegacy(ifrm) {
    if (ifrm.readyState === 'interactive') {
        document.getElementById('DownloadOptions').style.display = '';
        document.getElementById('DownloadProgress').style.display = 'none';
        Modal.Hide();
    }

}
</script>
	<!--[if lte IE 6.5]><iframe id="selectmask" style="display:none; position:absolute;top:0px;left:0px;filter:mask()"  src="javascript:void(0)" ></iframe><![endif]-->


