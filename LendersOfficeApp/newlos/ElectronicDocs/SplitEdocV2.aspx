﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SplitEdocV2.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.SplitEdocV2" %>
<%@ Register TagPrefix="uc" TagName="DocTypePicker" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body bgcolor="gainsboro" onunload="getModalArgs().callback()">
<script type="text/javascript">
    jQuery(function($) {
        resize(520, 445);
        $('#Associate').click(function(){
            var LoanId = ML.sLId;
            var url = <%= AspxTools.SafeUrl(Page.ResolveUrl("~/newlos/ElectronicDocs/EditConditionAssociation.aspx")) %> + "?LoanId=" + LoanId;
            var w = window.open(url, "EditConditionAssociations", "width=500,height=670,resizable=yes,scrollbars=yes,status=yes");
            w.focus();
        }); 
        
        OnConditionAssociationSave = function(tasks){
            $('#CurrentConditions').empty();
            
            var newOptions = [];
            newOptions.push('<option value="');
            $.each(tasks, function(id, subject){
                newOptions.push(id);
                newOptions.push('">');
                newOptions.push(subject);
                newOptions.push('</option><option value="');
            });
            
            newOptions.pop();
            newOptions.push('</option>');
            
            $('#CurrentConditions').append(newOptions.join(''));
        };
        
        function GetConditionAssociations(){
            var ret = [];
            $('#CurrentConditions option').each(function(){
                ret.push($(this).val());
            });
            
            return ret;
        }
        
        $('.DocTypePickerLink a').click(function() {
                var docTypeId = $(this).find('.SelectedDocTypeId').val();
                if (docTypeId != -1) $('#SelectDocType').hide();
            });
        
        $('#OK').click(function(){
            
            var docTypeId = $('.SelectedDocTypeId').val();
            if(docTypeId == -1)
            {
                $('.SelectedDocTypeName').effect('highlight', {}, 3000);
                $('#SelectDocType').show();
                return;
            }
            
            var arg = getModalArgs() || {};
            
            var selectionRange = arg.SelectedRanges?  arg.SelectedRanges[0] : {};
            var selectionFrom =  (selectionRange.Start + 1) || 1;
            var selectionTo = selectionFrom + ((selectionRange.Count - 1) || 0);
            
            var ret = {
                StartPage: selectionFrom,
                EndPage: selectionTo,
                DocType: docTypeId,
                Description: $('#Description').text(),
                Comments: $('#Comments').text(),
                Status: 0,
                AppId: $('#ApplicationSelect').val(),
                Conditions: GetConditionAssociations(),
                DoctypeDescription: $('.SelectedDocTypeName').text()
            };
            
            arg.result = JSON.stringify([ret]);
            arg.OK = true;
            onClosePopup(arg);
            
        });
        
        $('#Cancel').click(function(){ onClosePopup(); });
    });
    
    function OnConditionAssociationSave(DTO)
    {
        //Placeholder, gets assigned in jQuery.
    }
</script>
<style type="text/css">
    label
    {
        display: inline-block;
        font-weight: bold;
        width: 75px;
    }
    label.Header
    {
        width: auto;
        display: block;
    }

    ul
    {
        list-style-type: none;
        margin: 0px;
        padding: 0px;
        padding-left: 5px;
    }
    li
    {
        padding-bottom: 15px;
    }
    #Associate
    {
        float: right;
        position: relative;
        right: 5%;
        bottom: 10px;
    }
    .Controls
    {
        position: relative;
        left: 50%;
        margin-left: -50px;
        width: 125px;
        top: 20px;
    }
    textarea, select[multiple="multiple"]
    {
        width: 95%;
        height: 60px;
    }
    .SelectedDocTypeName
    {
        background: #DCDCDC
    }
    
    #SelectDocType
    {
        display: block;
        color: Red;
        display: none;
    }
    
</style>
    <form id="form1" runat="server">
    <div>
    <h1 class="MainRightHeader">
        Split Doc
    </h1>
    <ul>
    <li><label>Application </label> <asp:DropDownList runat="server" ID="ApplicationSelect"></asp:DropDownList></li>
    <li><label>Doc Type </label><uc:DocTypePicker runat="server" ID="DocType" /></li>
    <li><label class="Header">Description </label>
        <textarea id="Description"></textarea></li>
    <li><label class="Header">Internal Comments </label>
        <textarea id="Comments"></textarea></li>
    <li><label class="Header">Associated Conditions </label>
        <select id="CurrentConditions" multiple="multiple"></select></li>
    </ul>
    <a href="#" id="Associate">Associate with conditions </a>
    
    <div class="Controls">
        <span id="SelectDocType">Please select a doc type</span>
        <input type="button" id="OK" value="OK" /> <input type="button" id="Cancel" value="Cancel" />
    </div>
    
    </div>
    </form>
</body>
</html>
