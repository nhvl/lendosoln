﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.LoanSearch;
using LendersOffice.Security;
using System.Data;
using DataAccess;
using LendersOffice.Common;
using EDocs;
using LendersOffice.PdfLayout;
using System.Web.Services;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class CopyDocsToLoanFiles : BaseLoanPage
    {
        public override bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            RegisterService("copy", "/newlos/ElectronicDocs/CopyDocsToLoanFilesService.aspx");
            RegisterJsScript("jquery.tablesorter.min.js");

            this.EnableJqueryMigrate = false;

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DocIds.Value = AutoExpiredTextCache.GetFromCache(RequestHelper.GetSafeQueryString("key"));
        }

        protected void Search(object sender, EventArgs e)
        {
            LendingQBSearchFilter searchFilter = new LendingQBSearchFilter();
            searchFilter.sLNm = loanNumberFilter.Value;
            searchFilter.aBLastNm = lastNameFilter.Value;
            searchFilter.aBFirstNm = firstNameFilter.Value;
            searchFilter.sStatusT = -1;
            searchFilter.sLT = -1;

            if (searchFilter.sLNm != "")
            {
                if (searchFilter.sLNm.EndsWith("*"))
                {
                    loanNumPartialMatch.Checked = true;
                }
                else if (loanNumPartialMatch.Checked)
                {
                    searchFilter.sLNm += "*";
                }
            }
            if (searchFilter.aBLastNm != "")
            {
                if (searchFilter.aBLastNm.EndsWith("*"))
                {
                    lastNamePartialMatch.Checked = true;
                }
                else if (lastNamePartialMatch.Checked)
                {
                    searchFilter.aBLastNm += "*";
                }
            }
            if (searchFilter.aBFirstNm != "")
            {
                if (searchFilter.aBFirstNm.EndsWith("*"))
                {
                    firstNamePartialMatch.Checked = true;
                }
                else if (firstNamePartialMatch.Checked)
                {
                    searchFilter.aBFirstNm += "*";
                }
            }

            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            // Just default sort order to this for now.
            string sortExpr = "sStatusD DESC";
            int numLoans;
            int firstRow = 0;
            int lastRow = 250;

            DataSet dataSet = LendingQBSearch.Search(principal, searchFilter, sortExpr, firstRow, lastRow, out numLoans);

            Loans.DataSource = dataSet.Tables[0].DefaultView;
            Loans.DataBind();
        }

        protected void Loans_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            HtmlInputHidden loanId = (HtmlInputHidden)args.Item.FindControl("LoanId");
            EncodedLiteral loanNumber = (EncodedLiteral)args.Item.FindControl("LoanNumber");
            EncodedLiteral lastName = (EncodedLiteral)args.Item.FindControl("LastName");
            EncodedLiteral firstName = (EncodedLiteral)args.Item.FindControl("FirstName");
            EncodedLiteral propertyAddress = (EncodedLiteral)args.Item.FindControl("PropertyAddress");
            EncodedLiteral loanType = (EncodedLiteral)args.Item.FindControl("LoanType");
            EncodedLiteral loanStatus = (EncodedLiteral)args.Item.FindControl("LoanStatus");
            EncodedLiteral statusDate = (EncodedLiteral)args.Item.FindControl("StatusDate");
            HtmlTableRow row = (HtmlTableRow)args.Item.FindControl("Row");

            object dataItem = args.Item.DataItem;
            if (dataItem == null) return;

            row.Attributes.Add("class", args.Item.ItemType == ListItemType.Item ? "GridItem" : "GridAlternatingItem");
            loanId.Value = DataBinder.Eval(dataItem, "sLId").ToString();
            loanNumber.Text = DataBinder.Eval(dataItem, "sLNm").ToString();
            lastName.Text = DataBinder.Eval(dataItem, "aBLastNm").ToString();
            firstName.Text = DataBinder.Eval(dataItem, "aBFirstNm").ToString();
            propertyAddress.Text = DataBinder.Eval(dataItem, "PropertyAddress").ToString();
            loanType.Text = DataBinder.Eval(dataItem, "LoanType").ToString();
            loanStatus.Text = DataBinder.Eval(dataItem, "LoanStatus").ToString();
            string statusD = DataBinder.Eval(dataItem, "sStatusD").ToString();
            statusDate.Text = statusD.Split(' ')[0];
        }
    }
}
