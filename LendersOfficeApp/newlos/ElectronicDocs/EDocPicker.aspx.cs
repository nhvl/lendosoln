﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDocs;
using LendersOffice.Common;
using LendersOffice.ObjLib.Task;
using LendersOffice.ObjLib.DocumentConditionAssociation;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class EDocPicker : BaseLoanPage
    {

        private HashSet<Guid> Associations = new HashSet<Guid>();

        protected int? RequiredDocTypeId { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            //need to load associations also and filter those out 
            //need to allow associations from opener and filter those out .
            string taskid = RequestHelper.GetSafeQueryString("taskid");
            if (!string.IsNullOrEmpty(taskid))
            {
                Task t = Task.Retrieve(BrokerID, taskid);
                ConditionId.Text = t.TaskId;
                CondCategory.Text = t.CondCategoryId_rep;
                CondRequiredDoctype.Text = t.CondRequiredDocType_rep;
                TaskDueDate.Text = t.TaskDueDate_rep;
                TaskSubject.Text = t.TaskSubject;
                RequiredDocTypeId = t.CondRequiredDocTypeId;
                if ("persist".Equals(RequestHelper.GetSafeQueryString("mode")))
                {
                    ClientScript.RegisterHiddenField("TaskId", taskid);
                    Associations = new HashSet<Guid>(t.AssociatedDocs.Select(y => y.DocumentId));
                    OkBtn.Attributes.Add("class", "persist");
                }
            }
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();

            Func<EDocument, bool> docMatchesFilter = 
                edoc => edoc.DocStatus != E_EDocStatus.Obsolete && edoc.DocStatus != E_EDocStatus.Rejected;

            IEnumerable<EDocument> docs = repo.GetDocumentsByLoanId(LoanID).Where(docMatchesFilter);
            List<DocType> p = EDocumentDocType.GetStackOrderedDocTypesByBroker(Broker).ToList();
            Dictionary<string, int> docTypeIndeces = new Dictionary<string,int>();

            for(int i =0; i<p.Count; i++)
            {
                docTypeIndeces.Add(p[i].DocTypeId, i);
            }
            DocListing.DataSource = docs.OrderBy(y=>GetIndex(y.DocumentTypeId.ToString(), docTypeIndeces));
            DocListing.DataBind();
            EnableJqueryMigrate = false;
            DisplayCopyRight = false;
            RegisterJsScript("jquery.tablesorter.min.js");

        }

        [WebMethod]
        public static object SaveAssociations(string taskId, Guid[] documentIds)
        {
            Task t = Task.Retrieve(PrincipalFactory.CurrentPrincipal.BrokerId, taskId);
            t.UpdateDocumentAssociations(documentIds);
            t.Save(false);
            return new { Status = "OK" };
        }

        protected string GetEDocClass(EDocument edoc)
        {
            if (RequiredDocTypeId.HasValue && edoc.DocumentTypeId == RequiredDocTypeId.Value)
            {
                return "highlight";
            }
            return "";
        }

        protected void DocListing_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }
            EDocument doc = args.Item.DataItem as EDocument;
            HtmlInputCheckBox ch = args.Item.FindControl("picker") as HtmlInputCheckBox;
            ch.Attributes.Add("data-docid", doc.DocumentId.ToString());

            if (Associations.Contains(doc.DocumentId))
            {
                ch.Checked = true;
            }
        }

        public int GetIndex(string id, Dictionary<string,int> indeces)
        {
           int index;
           if (indeces.TryGetValue(id, out index))
           {
               return index;
           }

           return int.MaxValue;
        }
    }
}
