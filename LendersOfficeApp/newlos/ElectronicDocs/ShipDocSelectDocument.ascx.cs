﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDocs;
using DataAccess;
using LendersOffice.Common;
using System.Web.UI.HtmlControls;
using EDocs.UI;
using System.Drawing;
using System.Text;
using MeridianLink.CommonControls;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class ShipDocSelectDocument : LendersOfficeApp.newlos.BaseLoanUserControl, IAutoLoadUserControl
    {
        private const string CheckboxStateCookieKey = "SHIPDOCSELECT_CHECKBOX_STATE";
        public string SelectedShippingTemplateName
        {
            get
            {
                return ShippingTemplates.SelectedItem.Text;
            }
        }

        public string SelectedShippingTemplateId
        {
            get { return ShippingTemplates.SelectedValue; }
        }

        //Store/Load Checkbox values as '0' = unchecked, '1' = checked
        protected void RetrieveSavedCheckboxState()
        {
            string bitstring = "01000"; //Default to only "Protected"
            if (null != HttpContext.Current)
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies.Get(CheckboxStateCookieKey);
                if (null != cookie && cookie.Value.Length == 5)
                {
                    bitstring = cookie.Value;
                }
            }
            showStatusBlank.Checked = bitstring[0] != '0';
            showStatusProtected.Checked = bitstring[1] != '0';
            showStatusScreened.Checked = bitstring[2] != '0';
            showStatusRejected.Checked = bitstring[3] != '0';
            showStatusObsolete.Checked = bitstring[4] != '0';

        }
        protected void SaveCheckboxState()
        {
            StringBuilder bitstring = new StringBuilder();
            bitstring.Append(showStatusBlank.Checked ? "1" : "0");
            bitstring.Append(showStatusProtected.Checked ? "1" : "0");
            bitstring.Append(showStatusScreened.Checked ? "1" : "0");
            bitstring.Append(showStatusRejected.Checked ? "1" : "0");
            bitstring.Append(showStatusObsolete.Checked ? "1" : "0");
            RequestHelper.StoreToCookie(CheckboxStateCookieKey, bitstring.ToString(), true);
        }


        private static ListItemType[] x_skipList = new ListItemType[] {
                ListItemType.Footer,
                ListItemType.Header,
                ListItemType.Pager,
                ListItemType.Separator
            };

        /// <summary>
        /// Binds a document list to the UI, with an optional parameter specifying whether to preserve or clear
        /// the existing doc selections. The default behavior is to preserve selections on rebinding.
        /// </summary>
        /// <param name="newData">The documents to bind.</param>
        /// <param name="senderId">The control initiating this request.  Only used for debugging.</param>
        /// <param name="clearSelectedDocs">A value indicating whether to clear all document selections from the list.</param>
        private void UpdateDocumentsDataSource(List<EDocument> newData, string senderId, bool clearSelectedDocs = false)
        {
            // If there are ANY documents in EDocs with a status other than the blank status,
            // then only documents with an approved status should be displayed on this page.
            // Otherwise display all documents.
            //OPM 77725: Allow any status based on UI Filters
            //    if (newData.FirstOrDefault(e => e.DocStatus != E_EDocStatus.Blank) != null)
            //        Documents.DataSource = newData.Where(e => e.DocStatus == E_EDocStatus.Approved);
            //    else

            var checkedIds = clearSelectedDocs ? new List<string>() : FindSelectedDocs(newData, senderId);

            //Push hidden status types to the end of the list
            List<EDocument> newOrder = new List<EDocument>(newData.Count);
            newOrder.AddRange(newData.Where(d => IsVisibleStatusType(d.DocStatus)));
            newOrder.AddRange(newData.Where(d => !IsVisibleStatusType(d.DocStatus)));

            Documents.DataSource = newOrder;
            Documents.DataBind();

            //Re-check selection boxes
            foreach (RepeaterItem item in Documents.Items)
            {
                HiddenField docId = (HiddenField)item.FindControl("id");
                HtmlInputCheckBox chb = (HtmlInputCheckBox)item.FindControl("SelectItem");
                if (checkedIds.Contains(docId.Value))
                    chb.Checked = true;
            }

            SaveCheckboxState();
        }

        private List<string> FindSelectedDocs(List<EDocument> newData, string senderId)
        {
            Dictionary<Guid, bool> docsThatAreVisible = newData.ToDictionary(p => p.DocumentId, p => IsVisibleStatusType(p.DocStatus));

            //Remember which IDs are checked, re-check them after rebinding
            List<string> checkedIds = new List<string>();
            foreach (RepeaterItem item in Documents.Items)
            {
                Guid docIdGuid;
                HiddenField docId = (HiddenField)item.FindControl("id");
                if (!Guid.TryParse(docId.Value, out docIdGuid))
                {
                    try
                    {
                        HtmlInputCheckBox selectItem = (HtmlInputCheckBox)item.FindControl("SelectItem");
                        docIdGuid = new Guid(selectItem.Attributes["docid"]);
                        Tools.LogInfo("[UpdateDocumentsDataSource][UsingCheckBoxID] docid from checkbox:" + docIdGuid);
                    }
                    catch (FormatException)
                    {
                        EncodedLabel internalDescription = (EncodedLabel)item.FindControl("InternalDescription");
                        EncodedLabel description = (EncodedLabel)item.FindControl("Description");
                        EncodedLabel folder = (EncodedLabel)item.FindControl("Folder");
                        EncodedLabel docType = (EncodedLabel)item.FindControl("DocType");
                        EncodedLabel docStatus = (EncodedLabel)item.FindControl("Status");
                        EncodedLabel lastModified = (EncodedLabel)item.FindControl("LastModified");
                        EncodedLabel application = (EncodedLabel)item.FindControl("Application");
                        EncodedLabel pageCount = (EncodedLabel)item.FindControl("PageCount");
                        HtmlInputCheckBox selectItem = (HtmlInputCheckBox)item.FindControl("SelectItem");

                        Tools.LogError(String.Format("[UpdateDocumentsDataSource] " +
                            "LoanId: {0}, DocId: {1}, ItemIndex: {2}, ShippingTemplateId: {3}, " +
                            "SenderId: {4}, Folder: {5}, DocType: {6}, DocStatus: {7}, " +
                            "LastModified: {8}, Application: {9}, PageCount: {10}, DocId from checkbox: {11}.",
                            LoanID, docId.Value, item.ItemIndex, SelectedShippingTemplateId,
                            senderId, folder.Text, docType.Text, docStatus.Text,
                            lastModified.Text, application.Text, pageCount.Text, selectItem.Attributes["docid"]));
                        throw;
                    }
                }

                //only reselect visible ones
                if (!docsThatAreVisible.ContainsKey(docIdGuid) || !docsThatAreVisible[docIdGuid])
                {
                    continue;
                }

                HtmlInputCheckBox chb = (HtmlInputCheckBox)item.FindControl("SelectItem");
                if (chb.Checked)
                {
                    checkedIds.Add(docId.Value);
                }
            }

            return checkedIds;
        }

        protected bool IsVisibleStatusType(string s)
        {
            switch (s)
            {
                case ("Protected"):
                    return showStatusProtected.Checked;
                case (""):
                    return showStatusBlank.Checked;
                case ("Screened"):
                    return showStatusScreened.Checked;
                case ("Rejected"):
                    return showStatusRejected.Checked;
                case ("Obsolete"):
                    return showStatusObsolete.Checked;
                default:
                    return false;
            }
        }

        protected bool IsVisibleStatusType(E_EDocStatus s)
        {
            return IsVisibleStatusType(EDocument.StatusText(s));
        }

        protected void ShippingTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            EDocumentRepository documentRepository = EDocumentRepository.GetUserRepository();
            List<EDocument> documents = new List<EDocument>(documentRepository.GetDocumentsByLoanId(LoanID));

            // Temporarily get the sender id for debugging purposes.
            string senderId = string.Empty;
            if (sender is Control)
            {
                senderId = ((Control)sender).ID;
            }

            documents = documents.OrderBy(p => p.Folder.FolderNm).ThenBy(p => p.DocTypeName).ThenByDescending(p => p.LastModifiedDate).ToList();
            if (int.Parse(SelectedShippingTemplateId) < 0)
            {
                UpdateDocumentsDataSource(documents, senderId, clearSelectedDocs: true);
                return;
            }

            List<EDocument> sortedList = new List<EDocument>();

            IEnumerable<DocType> docTypesInTemplate = this.GetCurrentShippingTemplateDocs();

            foreach (DocType doc in docTypesInTemplate)
            {
                sortedList.AddRange(documents.Where(d => d.DocTypeName.ToLower() == doc.DocTypeName.ToLower() && d.Folder.FolderNm.ToLower() == doc.Folder.FolderNm.ToLower()));
                documents.RemoveAll(d => d.DocTypeName.ToLower() == doc.DocTypeName.ToLower() && d.Folder.FolderNm.ToLower() == doc.Folder.FolderNm.ToLower());
            }

            // Add the documents that were not in the doctype at the end of the list
            sortedList.AddRange(documents);
            UpdateDocumentsDataSource(sortedList, senderId);
        }
        private Tuple<int, IEnumerable<DocType>> cachedDocTypesByTemplateId = null;
        private IEnumerable<DocType> GetCurrentShippingTemplateDocs()
        {
            int selectedTemplate = int.Parse(SelectedShippingTemplateId);
            if (selectedTemplate < 0)
            {
                return new List<DocType>();
            }

            if (this.cachedDocTypesByTemplateId == null || this.cachedDocTypesByTemplateId.Item1 != selectedTemplate)
            {
                EDocumentShippingTemplate st = new EDocumentShippingTemplate();
                IEnumerable<DocType> docTypesInTemplate = st.GetShippingTemplateData(selectedTemplate);
                this.cachedDocTypesByTemplateId = Tuple.Create(selectedTemplate, docTypesInTemplate);
            }

            return this.cachedDocTypesByTemplateId.Item2;
        }

        // Used to determine whether or not a row needs to be checked
        // when sorting for a shipping template
        private bool DocIsInTemplate(EDocument userDoc)
        {
            int selectedTemplate = int.Parse(SelectedShippingTemplateId);
            if (selectedTemplate < 0)
            {
                return false;
            }

            IEnumerable<DocType> docTypesInTemplate = this.GetCurrentShippingTemplateDocs();

            foreach (DocType doc in docTypesInTemplate)
            {
                if (userDoc.DocTypeName.ToLower() == doc.DocTypeName.ToLower() && userDoc.Folder.FolderNm.ToLower() == doc.Folder.FolderNm.ToLower() && IsVisibleStatusType(userDoc.DocStatus))
                {
                    return true;
                }
            }

            return false;
        }

        public IList<ShippingReviewView> GetSelectedDocumentIds()
        {
            IList<ShippingReviewView> entries = new List<ShippingReviewView>(Documents.Items.Count);
            foreach (RepeaterItem item in Documents.Items)
            {
                if (x_skipList.Contains(item.ItemType))
                {
                    continue;
                }

                HiddenField docId = (HiddenField)item.FindControl("id");
                EncodedLabel internalDescription = (EncodedLabel)item.FindControl("InternalDescription");
                EncodedLabel description = (EncodedLabel)item.FindControl("Description");
                EncodedLabel folder = (EncodedLabel)item.FindControl("Folder");
                EncodedLabel docType = (EncodedLabel)item.FindControl("DocType");
                EncodedLabel docStatus = (EncodedLabel)item.FindControl("Status");
                EncodedLabel lastModified = (EncodedLabel)item.FindControl("LastModified");
                EncodedLabel application = (EncodedLabel)item.FindControl("Application");
                EncodedLabel pageCount = (EncodedLabel)item.FindControl("PageCount");
                HiddenField ownerPw = (HiddenField)item.FindControl("HasOwnerPassword");
                HtmlInputCheckBox chb = (HtmlInputCheckBox)item.FindControl("SelectItem");
                HiddenField pdfHasUnsupportedFeatures = (HiddenField)item.FindControl("PdfHasUnsupportedFeatures");
                HiddenField isESigned = (HiddenField)item.FindControl("IsESigned");

                if (!chb.Checked)
                {
                    continue;
                }
                else if (!this.IsVisibleStatusType(EDocument.StatusFromText(docStatus.Text)))
                {
                    continue;
                }

                entries.Add(new ShippingReviewView()
                {
                    InternalDescription = internalDescription.Text,
                    Description = description.Text,
                    Folder = folder.Text,
                    DocType = docType.Text,
                    PageCount = int.Parse(pageCount.Text),
                    Id = new Guid(docId.Value),
                    Application = application.Text,
                    Status = E_ShippingReviewEntryStatus.OK,
                    HasOwnerPassword = bool.Parse(ownerPw.Value),
                    LastModified = lastModified.Text,
                    PdfHasUnsupportedFeatures = bool.Parse(pdfHasUnsupportedFeatures.Value),
                    DocStatus = EDocument.StatusFromText(docStatus.Text),
                    IsESigned = bool.Parse(isESigned.Value)
                });
            }

            return entries;
        }

        protected void Documents_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (x_skipList.Contains(args.Item.ItemType))
            {
                return;
            }

            EDocument currentDoc = args.Item.DataItem as EDocument;

            if (!currentDoc.LoanId.HasValue)
            {
                throw new CBaseException(ErrorMessages.Generic, "DocsReceive-Encountered EDocument without loan id. Should not be possible.");
            }

            HtmlAnchor viewLink = (HtmlAnchor)args.Item.FindControl("viewLink");
            Label indicator = (Label)args.Item.FindControl("PasswordIndicator");

            EncodedLabel internalDescription = (EncodedLabel)args.Item.FindControl("InternalDescription");
            EncodedLabel description = (EncodedLabel)args.Item.FindControl("Description");
            EncodedLabel folder = (EncodedLabel)args.Item.FindControl("Folder");
            EncodedLabel docType = (EncodedLabel)args.Item.FindControl("DocType");
            EncodedLabel application = (EncodedLabel)args.Item.FindControl("Application");
            EncodedLabel pageCount = (EncodedLabel)args.Item.FindControl("PageCount");
            EncodedLabel lastModified = (EncodedLabel)args.Item.FindControl("LastModified");
            EncodedLabel status = args.Item.FindControl("Status") as EncodedLabel;

            HtmlInputCheckBox select = (HtmlInputCheckBox)args.Item.FindControl("SelectItem");
            select.Checked = DocIsInTemplate(currentDoc);

            HiddenField docId = (HiddenField)args.Item.FindControl("id");
            HiddenField ownerPw = (HiddenField)args.Item.FindControl("HasOwnerPassword");
            HiddenField pdfHasUnsupportedFeatures = (HiddenField)args.Item.FindControl("PdfHasUnsupportedFeatures");
            HiddenField isESigned = (HiddenField)args.Item.FindControl("IsESigned");


            select.Attributes.Add("docid", currentDoc.DocumentId.ToString());
            viewLink.HRef = "javascript:view('" + AspxTools.JsStringUnquoted(currentDoc.DocumentId.ToString()) + "')";
            docId.Value = currentDoc.DocumentId.ToString();
            folder.Text = currentDoc.Folder.FolderNm;
            docType.Text = currentDoc.DocTypeName;

            ownerPw.Value = bool.FalseString;
            pdfHasUnsupportedFeatures.Value = currentDoc.PdfHasUnsupportedFeatures.ToString();
            isESigned.Value = currentDoc.IsESigned.ToString();
            lastModified.Text = currentDoc.LastModifiedDate.ToShortDateString();
            pageCount.Text = currentDoc.PageCount.ToString();
            internalDescription.Text = Server.HtmlEncode(currentDoc.InternalDescription);  //we only need this in the first control thats shown to the user because all other controls take the encoded value  from this control.
            description.Text = Server.HtmlEncode(currentDoc.PublicDescription);
            application.Text = Server.HtmlEncode(currentDoc.AppName);

            Color sColor = Color.FromArgb(0, 128, 0);

            if (currentDoc.DocStatus == E_EDocStatus.Obsolete || currentDoc.DocStatus == E_EDocStatus.Rejected)
            {
                sColor = Color.FromArgb(255, 0, 0);
            }
            else if (currentDoc.DocStatus == E_EDocStatus.Screened)
            {
                sColor = Color.FromArgb(0, 64, 128);
            }

            status.ForeColor = sColor;
            status.Font.Bold = true;
            status.Text = EDocument.StatusText(currentDoc.DocStatus);
        }


        #region IAutoLoadUserControl Members

        public void LoadData()
        {
            if (Page.IsPostBack)
            {
                return;
            }

            EDocumentShippingTemplate st = new EDocumentShippingTemplate();
            List<ShippingTemplate> templates = st.CombinedActiveShippingTemplateList;


            ShippingTemplates.DataSource = templates;

            ShippingTemplates.DataValueField = "ShippingTemplateId";
            ShippingTemplates.DataTextField = "ShippingTemplateName";

            ShippingTemplates.DataBind();

            ShippingTemplates.Items.Insert(0, new ListItem() { Selected = true, Text = "-- NONE --", Value = "-1" });


            EDocumentRepository documentRepository = EDocumentRepository.GetUserRepository();
            List<EDocument> docs = documentRepository.GetDocumentsByLoanId(LoanID).OrderBy(p => p.Folder.FolderNm).ThenBy(p => p.DocTypeName).ThenByDescending(p => p.LastModifiedDate).ToList();

            //o	If there are ANY documents in EDocs with a status other than the blank status, then only documents with an approved status should be displayed on this page.  Otherwise display all documents.*/
            //OPM 77725: Allow any status based on UI Filters
            //  if (docs.FirstOrDefault(e => e.DocStatus != E_EDocStatus.Blank) != null)
            //      Documents.DataSource = docs.Where(e => e.DocStatus == E_EDocStatus.Approved);
            //  else
            RetrieveSavedCheckboxState();
            UpdateDocumentsDataSource(docs, string.Empty);
        }

        public void SaveData()
        {
        }

        #endregion
    }
}