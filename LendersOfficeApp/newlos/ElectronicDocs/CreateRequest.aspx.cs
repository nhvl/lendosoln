﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDocs;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;
using CommonProjectLib.Common.Lib;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class CreateRequest : BaseLoanPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;

            if (!BrokerUser.HasPermission(LendersOffice.Security.Permission.CanViewEDocs))
            {
                throw new AccessDenied();
            } 

            BrokerDB db = this.Broker;
            if (!db.IsEnableNewConsumerPortal)
            {
                m_requestTypeShare.Style.Add("visibility", "hidden");
            }

            if (db.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                m_previewLink.InnerText = "preview/sign";
            }

            this.RegisterService("service", "/newlos/ElectronicDocs/CreateRequestService.aspx");
            this.DisplayCopyRight = false;
            m_noValidAppMsg.Text = ErrorMessages.MissingNecessaryInformationToCreateRequest;


            List<CreateRequestApplicationItem> apps = new List<CreateRequestApplicationItem>();
            CPageData dataloan = CPageData.CreateUsingSmartDependency(LoanID, typeof(CreateRequest));
            dataloan.InitLoad();

            BranchDB branchDb = new BranchDB(dataloan.sBranchId, dataloan.sBrokerId);
            branchDb.Retrieve();

            if (Broker.IsEnableNewConsumerPortal)
            {
                if (!branchDb.ConsumerPortalId.HasValue)
                {
                    m_noValidAppMsg.Text = "The loan is not associated with a consumer portal.";
                    m_appsInvalid.Visible = true;
                    m_appsValid.Visible = !m_appsInvalid.Visible;
                    return;
                }
            }

            emailsUnique.Value = dataloan.AreAllBorrowerEmailsUnique.ToString();

            for (int i = 0; i < dataloan.nApps; i++)
            {
                CAppData appData = dataloan.GetAppData(i);
                bool hasBorrower = false;
                bool hasCoborrower = false;
                bool hasBorrowerEmail = false;
                bool hasCoborrowerEmail = false;

                string name = "";
                if (!string.IsNullOrEmpty(appData.aBNm))
                {
                    name = appData.aBNm;
                    hasBorrower = true;
                    if (!string.IsNullOrEmpty(appData.aCNm))
                    {
                        name += " & " + appData.aCNm;
                        hasCoborrower = true;
                    }
                }
                else if (!string.IsNullOrEmpty(appData.aCNm))
                {
                    name = appData.aCNm;
                    hasCoborrower = true;
                }

                string email = "";
                if (UserDataValidation.IsValidEmailAddress(appData.aBEmail))
                {
                    email = appData.aBEmail;
                    hasBorrowerEmail = true;
                    if (UserDataValidation.IsValidEmailAddress(appData.aCEmail))
                    {
                        email += " & " + appData.aCEmail;
                        hasCoborrowerEmail = true;
                    }
                }
                else if (UserDataValidation.IsValidEmailAddress(appData.aCEmail))
                {
                    email = appData.aCEmail;
                    hasCoborrowerEmail = true;
                }

                if (appData.aBSsn == appData.aCSsn && UserDataValidation.IsValidSSN(appData.aCSsn))
                    m_noValidAppMsg.Text = ErrorMessages.DuplicateSsnInLoanFile;
                else if ((!hasBorrowerEmail || UserDataValidation.IsValidSSN(appData.aBSsn))
                    && (!hasCoborrowerEmail || UserDataValidation.IsValidSSN(appData.aCSsn))
                    && !String.IsNullOrEmpty(email))
                    apps.Add(new CreateRequestApplicationItem(name
                        , email
                        , appData.aAppId
                        , hasBorrower
                        , hasCoborrower
                        , hasBorrowerEmail
                        , hasCoborrowerEmail));
            }

            if (db.IsEnableNewConsumerPortal)
            {

                sTitleBorrower.Items.Add("");
                foreach (TitleBorrower borrower in dataloan.sTitleBorrowers)
                {
                    if (UserDataValidation.IsValidSSN(borrower.SSN) && UserDataValidation.IsValidEmailAddress(borrower.Email))
                    {
                        ListItem li = new ListItem(borrower.FullName, borrower.Id.ToString("N"));
                        li.Attributes.Add("data-email", borrower.Email);
                        sTitleBorrower.Items.Add(li);
                    }
                }

                titleCbRow.Visible = sTitleBorrower.Items.Count > 1;
            }

            

            m_appList.DataSource = apps;
            m_appList.DataBind();

            m_appsInvalid.Visible = apps.Count == 0;
            m_appsValid.Visible = !m_appsInvalid.Visible;
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }
    }

    public class CreateRequestApplicationItem
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public Guid AppId { get; set; }
        public bool hasBorrower { get; set; }
        public bool hasCoborrower { get; set; }
        public bool hasBorrowerEmail { get; set; }
        public bool hasCoborrowerEmail { get; set; }

        public CreateRequestApplicationItem(string name
            , string email
            , Guid appId
            , bool br
            , bool cbr
            , bool hbe
            , bool hce)
        {
            Name = name;
            Email = email;
            AppId = appId;
            hasBorrower = br;
            hasCoborrower = cbr;
            hasBorrowerEmail = hbe;
            hasCoborrowerEmail = hce;
        }
    }
}
