﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.ObjLib.DocumentConditionAssociation;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class DocumentRejection : BaseLoanPage
    {
        private bool UseStaticNumbering { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            RejectBtn.Visible = BrokerUser.HasPermission(Permission.AllowApprovingRejectingEDocs);
            DisplayCopyRight = false;

            this.EnableJqueryMigrate = false;

            UseStaticNumbering = Broker.IsUseNewTaskSystemStaticConditionIds;

            var associations = DocumentConditionAssociation.GetAssociationsByLoanDocumentHeavy(this.BrokerID, LoanID, RequestHelper.GetGuid("docid"), BrokerUser).Where(p => p.TaskId != RequestHelper.GetSafeQueryString("TaskId")).ToList();


            if (RejectBtn.Visible)
            {
                AssociationSection.Visible = associations.Count > 0;

                Associations.DataSource = associations;
                Associations.DataBind();
            }
        }

        protected override bool EnableJQueryInitCode
        {
            get
            {
                return true;
            }
        }

        protected string GetTaskId(DocumentConditionAssociation assoc)
        {
            if (UseStaticNumbering)
            {
                return assoc.CondRowId.ToString();
            }
            else
            {
                return assoc.TaskId;
            }
        }
    }
}
