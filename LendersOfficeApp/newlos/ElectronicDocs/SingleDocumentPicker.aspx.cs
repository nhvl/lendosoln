﻿namespace LendersOfficeApp.newlos.ElectronicDocs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using EDocs;
    using EDocs.Contents;
    using LendersOffice.Common;

    public partial class SingleDocumentPicker : BaseLoanPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            EnableJqueryMigrate = false;
            DisplayCopyRight = false;
            RegisterJsScript("jquery.tablesorter.min.js");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Gets a value indicating whether obsolete and rejected docs should be excluded.
        /// </summary>
        /// <value>A boolean indicating whether obsolete and rejected docs should be excluded.</value>
        protected bool NoObsoleteAndRejectedDocs
        {
            get
            {
                return RequestHelper.GetBool("nor");
            }
        }

        /// <summary>
        /// Gets a value indicating whether to only display ESign-prepped docs.
        /// </summary>
        /// <value>A boolean indicating whether to only display ESign-prepped docs.</value>
        protected bool RestrictToESign
        {
            get
            {
                return RequestHelper.GetBool("esignonly");
            }
        }

        protected IEnumerable<Guid?> ExcludedDocIds { get; } = RequestHelper.GetSafeQueryString("exclude")?.Split(',').Select(id => id.ToNullable<Guid>(Guid.TryParse));

        /// <summary>
        /// Generates the HTML "select" anchor for selecting the document.
        /// </summary>
        /// <param name="doc">The document that will be selected by the anchor.</param>
        /// <returns>An HtmlAnchor element for being displayed in the aspx page.</returns>
        protected HtmlAnchor SelectAnchor(EDocument doc)
        {
            HtmlAnchor anchor = new HtmlAnchor()
            {
                InnerText = "select"
            };
            if (this.RestrictToESign && doc.ImageStatus != E_ImageStatus.HasCachedImages)
            {
                anchor.Disabled = true;
                anchor.Title = "Please wait until this document has finished preparing.";
            }
            else
            {
                anchor.Attributes["class"] = "select";
            }

            return anchor;
        }

        /// <summary>
        /// Gets a value indicating whether the viewer should be restricted to UCD files.
        /// </summary>
        /// <value>A value indicating whether the viewer should be restricted to UCD files.</value>
        protected bool RestrictToUcd
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["restricttoucd"]))
                {
                    bool restrictToUcd;
                    if (!bool.TryParse(RequestHelper.GetSafeQueryString("restricttoucd"), out restrictToUcd))
                    {
                        return false;
                    }

                    return restrictToUcd;
                }

                return false;
            }
        }

        protected IEnumerable<Guid> AssociatedUcdDocs
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["associateducddocs"]))
                {
                    var associatedDocIds = ObsoleteSerializationHelper.JavascriptJsonDeserializer<string[]>(Request.QueryString["associateducddocs"]);
                    return associatedDocIds.Select(id => Guid.Parse(id));
                }

                return Enumerable.Empty<Guid>();
            }
        }

        protected override void LoadData()
        {
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            List<DocType> p = EDocumentDocType.GetStackOrderedDocTypesByBroker(Broker).ToList();
            Dictionary<string, int> docTypeIndices = new Dictionary<string, int>();

            for (int i = 0; i < p.Count; i++)
            {
                docTypeIndices.Add(p[i].DocTypeId, i);
            }

            IEnumerable<EDocument> docs = repo.GetDocumentsByLoanId(LoanID);

            if (this.ExcludedDocIds != null && this.ExcludedDocIds.Count() > 0)
            {
                var excludedDocsSet = new HashSet<Guid?>(ExcludedDocIds);
                docs = docs.Where(doc => !excludedDocsSet.Contains(doc.DocumentId));
            }

            if (this.RestrictToUcd)
            {
                docs = docs.Where(doc => !this.AssociatedUcdDocs.Contains(doc.DocumentId)
                    && UcdGenericEDocument.IsUcdEdocValid(doc.DocumentId, this.BrokerID));
            }

            if (this.RestrictToESign)
            {
                docs = docs.Where(doc => doc.HasESignTags);
            }

            if (this.NoObsoleteAndRejectedDocs)
            {
                docs = docs.Where(doc => doc.DocStatus != E_EDocStatus.Rejected && doc.DocStatus != E_EDocStatus.Obsolete).ToList();
            }

            var source = docs.OrderBy(y => GetIndex(y.DocumentTypeId.ToString(), docTypeIndices));
            m_repeater.DataSource = source;
            m_repeater.DataBind();

            var docTypeFolders = new List<string>();
            docTypeFolders.Add("All");
            docTypeFolders.AddRange(p.Select(d => d.Folder.FolderNm).Distinct());
            DocTypeFilter.DataSource = docTypeFolders;
            DocTypeFilter.DataBind();
        }

        public int GetIndex(string id, Dictionary<string, int> indices)
        {
            int index;
            if (indices.TryGetValue(id, out index))
            {
                return index;
            }

            return int.MaxValue;
        }

        /// <summary>
        /// Converts an <see cref="EDocument"/> object into a JSON representation to pass around in the JS.
        /// </summary>
        /// <param name="document">The document to convert.</param>
        /// <returns>The JSON representation of the document.</returns>
        protected string GetDocumentJson(EDocument document)
        {
            return SerializationHelper.JsonNetAnonymousSerialize(new
            {
                DocumentId = document.DocumentId,
                FolderName = document.Folder.FolderNm,
                DocTypeName = document.DocTypeName,
                Description = document.PublicDescription,
                Comments = document.InternalDescription,
                LastModifiedDate = document.LastModifiedDate,
            });
        }
    }
}
