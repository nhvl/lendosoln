﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDocs;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class ForceChangeDocType : LendersOffice.Common.BaseServicePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterService("edoc", "/newlos/ElectronicDocs/EDocsUtilitiesService.aspx");
        }

        protected void SaveBtnOnClick(object sender, EventArgs args)
        {
            if (DocType.Value == "-1")
            {
                ErrorMsg.Text = "Please select a doc type.";
                return;
            }

            var repo =  EDocumentRepository.GetUserRepository();
            var edoc = repo.GetDocumentById(RequestHelper.GetGuid("docid"));
            if (edoc.CanEdit)
            {
                edoc.DocumentTypeId = int.Parse(DocType.Value);
                repo.Save(edoc);
            }
            Response.Redirect("EditEdocV2.aspx?loanid=" + RequestHelper.GetGuid("loanid") + "&docid=" + RequestHelper.GetGuid("docid"),true);
        }
    }
}
