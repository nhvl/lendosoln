﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using EDocs;
using System.Web.UI.HtmlControls;
using LendersOffice.PdfLayout;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class SignatureTemplate : BaseServicePage
    {
        protected string m_pdfFormId = null;
        
        protected void Page_Load(object sender, EventArgs e)
        {

            m_pdfFormId = RequestHelper.GetSafeQueryString("docid");
            string fullKey = m_pdfFormId + "_" + BrokerUserPrincipal.CurrentPrincipal.UserId.ToString();
            int numberOfPages = RequestHelper.GetInt("numpages", -1);

            List<KeyValuePair<Guid, string>> templateList = new List<KeyValuePair<Guid, string>>();

            templateList.Add(new KeyValuePair<Guid, string>(Guid.Empty, "--- Select Signature Template ---"));
            foreach (var o in PdfSignatureTemplate.ListTemplatesByCurrentUser())
            {
                templateList.Add(new KeyValuePair<Guid, string>(o.TemplateId, o.Name));
            }

            this.RegisterJsObject("TemplateList", templateList);

            byte[] pdfContent = AutoExpiredTextCache.GetBytesFromCache(fullKey);

            List<PdfPageItem> pages = GetPageInfo(pdfContent);
            //for (int i = 1; i <= numberOfPages; i++)
            //{
            //    pages.Add(new PdfPageItem() { DocId = Guid.Empty, Page = i });
            //}

            this.RegisterJsObject("PdfPages", pages);
        }
        private List<PdfPageItem> GetPageInfo(byte[] pdfContent)
        {
            iTextSharp.text.pdf.PdfReader pdfReader = new iTextSharp.text.pdf.PdfReader(pdfContent);

            int pageCount = pdfReader.NumberOfPages;
            List<PdfPageItem> list = new List<PdfPageItem>(pageCount);
            for (int i = 1; i <= pageCount; i++)
            {
                var size = pdfReader.GetPageSizeWithRotation(i);
                list.Add(new PdfPageItem() { DocId = Guid.Empty, Page = i, Version = 0, Rotation = 0, PageHeight = (int)size.Height, PageWidth = (int)size.Width });

            }
            return list;

        }
        protected void Page_Init(object sender, EventArgs e)
        {
            if (this.Header != null)
            {
                RegisterCSS("jquery-ui-1.11.custom.css");
                IncludeStyleSheet("~/css/PdfEditor.css");
            }
            
            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("json.js");
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterJsScript("PdfEditor.js");
            this.RegisterService("editor", "/newlos/ElectronicDocs/SignatureTemplateService.aspx");
        }
    }
}
