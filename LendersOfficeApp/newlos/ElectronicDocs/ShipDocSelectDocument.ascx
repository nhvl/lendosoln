﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShipDocSelectDocument.ascx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.ShipDocSelectDocument" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="EDocs" %>
<script type="text/javascript">
    var currentSort = '';

    $(document).ready(function() {
        var sort = '';
        if ($('#DocSelectionList tbody')[0].innerHTML.trim() != '') {
            $("#DocSelectionList").tablesorter({
                headers: {
                    0: { sorter: false }
                   , 5: { sorter: false }
                   , 6: { sorter: false }
                   , 7: { sorter: false }
                   , 9: { sorter: false }
                }
            });

            $("#folderSortLink").click(function() {
                var sortedOrder = getSortedOrder();
                currentSort = [[[10, 0], [2, sortedOrder], [3, sortedOrder], [4, sortedOrder]]];
                $("table").trigger("sorton", currentSort);
                setSortedOrder();
                return false;
            });

            $("#modDateSortLink").click(function() {
                var sortedOrder = getSortedOrder();
                currentSort = [[[10, 0], [8, sortedOrder]]];
                $("table").trigger("sorton", currentSort);
                setSortedOrder();
                return false;
            });

            $("#statusSortLink").click(function() {
                var sortedOrder = getSortedOrder();
                currentSort = [[[10, 0], [1, sortedOrder]]];
                $("table").trigger("sorton", currentSort);
                setSortedOrder();
                return false;
            });

            $("#docTypeSortLink").click(function() {
                // By putting the current sort in the sorton trigger, it will effectively not sort the individual 
                // column
                // I need to do this instead of setting the header sorter to false because I want the column
                // to be sortable, but only when a different column is clicked, not when this column is clicked.
                // OPM 53189
                $("table").trigger("sorton", currentSort);
                return false;
            });

            $("#applicationSortLink").click(function() {
                // By putting the current sort in the sorton trigger, it will effectively not sort the individual 
                // column
                // I need to do this instead of setting the header sorter to false because I want the column
                // to be sortable, but only when a different column is clicked, not when this column is clicked.
                // OPM 53189
                $("table").trigger("sorton", currentSort);
                return false;
            });

            $("#DocSelectionList").on("sortStart", function() {
                selectCBList = $('#DocSelectionList tbody input:checked');
            });
            $("#DocSelectionList").on("sortEnd", function() {
                setClass($('#DocSelectionList tbody')[0]);
                if (initSort) {
                    initSort = false;
                }
                else {
                    $('#DocSelectionList tbody input').each(function(i, e) {
                        var id =  $(e).attr('id');
                        
                        var checked = false;
                        for (var i = 0; i < selectCBList.length; i++) {
                            if (selectCBList[i].id == id) {
                                checked = true;
                                break;
                            }
                        }
                        
                        $(e).prop('checked', checked);
                    });
                }
            });

            var hiddenRows = $("#DocSelectionList tbody tr").filter(
            function() {
                return $(":nth-child(11)", this).text() === '1';
            });
            hiddenRows.css("display", "none");
        }

        $('.SelectAllItems').click(function(event) {
            var selectAll = this.checked;
            var $checkboxes = $('#DocSelectionList tbody')
                .find('input[type=checkbox]');
            var checkboxIsVisible = false;

            $checkboxes.each(function(index, element) {
                var $this = $(this);
                var thisIsVisible = $this.is(':visible');
                $this.prop('checked', thisIsVisible && selectAll);
                checkboxIsVisible = checkboxIsVisible || thisIsVisible;
            });

            disableNextButton(!selectAll || !checkboxIsVisible);
        });
    });
</script>
<span>Shipping Template&nbsp;&nbsp;<asp:DropDownList runat="server" AutoPostBack="true" ID="ShippingTemplates" onselectedindexchanged="ShippingTemplates_SelectedIndexChanged" ></asp:DropDownList>
</span>
&nbsp;&nbsp;&nbsp;
<span>
    <b>Filter by Status</b>
    <asp:CheckBox ID="showStatusBlank" runat="server" AutoPostBack="true" Text="No Status" OnCheckedChanged="ShippingTemplates_SelectedIndexChanged" />
    &nbsp;&nbsp;
    <asp:CheckBox ID="showStatusProtected" runat="server" AutoPostBack="true" Text="Protected" OnCheckedChanged="ShippingTemplates_SelectedIndexChanged" />
    &nbsp;&nbsp;
    <asp:CheckBox ID="showStatusScreened" runat="server" AutoPostBack="true" Text="Screened" OnCheckedChanged="ShippingTemplates_SelectedIndexChanged" />
    &nbsp;&nbsp;
    <asp:CheckBox ID="showStatusRejected" runat="server" AutoPostBack="true" Text="Rejected" OnCheckedChanged="ShippingTemplates_SelectedIndexChanged" />
    &nbsp;&nbsp;
    <asp:CheckBox ID="showStatusObsolete" runat="server" AutoPostBack="true" Text="Obsolete" OnCheckedChanged="ShippingTemplates_SelectedIndexChanged" />
</span>
<br />
<asp:Repeater runat="server" ID="Documents" OnItemDataBound="Documents_OnItemDataBound">
    <HeaderTemplate>
        <table cellpadding="1" width="100%" id="DocSelectionList">
            <thead>
                <tr class="GridHeader">
                    <th><input type="checkbox" runat="server" id="SelectAllItems" class="SelectAllItems" /></th>
                    <th id="statusSortLink"><a href="#">Status</a></th>
                    <th id="folderSortLink"><a href="#">Folder</a></th>
                    <th id="docTypeSortLink">Doc Type</th>
                    <th id="applicationSortLink">Application</th>
                    <th >Description</th>
                    <th >Internal Comments</th>
                    <th>Pages</th>
                    <th id="modDateSortLink" style="width:100px"><a href="#">Last Modified</a></th>
                    <th>&nbsp;</th>
                    <th style="display:none"></th> <!-- Extra column to differentiate hidden/visible rows - for sorting -->
                </tr>
            </thead>
            <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr class="GridItem">
            <td align="center"> <ml:EncodedLabel runat="server" ID="PasswordIndicator" style="color:Red;font-size:medium" Text=""></ml:EncodedLabel> <input type="checkbox"  onclick="RefreshNextButton()"  runat="server" id="SelectItem" /> <asp:HiddenField runat="server" ID="HasOwnerPassword" />  <asp:HiddenField runat="server" ID="PdfHasUnsupportedFeatures" /><asp:HiddenField runat="server" ID="id" /><asp:HiddenField runat="server" ID="IsESigned" /></td>
            <td><ml:EncodedLabel runat="server" ID="Status"></ml:EncodedLabel></td>
            <td><ml:EncodedLabel runat="server" ID="Folder"></ml:EncodedLabel></td>
            <td><ml:EncodedLabel runat="server" ID="DocType"></ml:EncodedLabel></td>
            <td><ml:EncodedLabel runat="server" ID="Application"></ml:EncodedLabel></td>
            <td><ml:EncodedLabel runat="server" ID="Description"></ml:EncodedLabel></td>
            <td><ml:EncodedLabel runat="server" ID="InternalDescription"></ml:EncodedLabel></td>
            <td><ml:EncodedLabel runat="server" ID="PageCount"></ml:EncodedLabel></td>
            <td><ml:EncodedLabel runat="server" ID="LastModified"></ml:EncodedLabel></td>
            <td><a runat="server" id="viewLink">view</a></td>
            <td style="display:none" runat="server"><%# AspxTools.HtmlString(IsVisibleStatusType((E_EDocStatus)DataBinder.Eval(Container.DataItem, "docStatus"))?"0":"1") %></td>
        </tr>
    </ItemTemplate>
        <AlternatingItemTemplate>
        <tr class="GridAlternatingItem">
            <td align="center"> <ml:EncodedLabel runat="server" ID="PasswordIndicator" style="color:Red;font-size:medium" Text=""></ml:EncodedLabel> <input type="checkbox"  onclick="RefreshNextButton()" runat="server" id="SelectItem" /> <asp:HiddenField runat="server" ID="HasOwnerPassword" />  <asp:HiddenField runat="server" ID="PdfHasUnsupportedFeatures" /><asp:HiddenField runat="server" ID="id" /><asp:HiddenField runat="server" ID="IsESigned" /></td>
            <td><ml:EncodedLabel runat="server" ID="Status"></ml:EncodedLabel></td>
            <td><ml:EncodedLabel runat="server" ID="Folder"></ml:EncodedLabel></td>
            <td><ml:EncodedLabel runat="server" ID="DocType"></ml:EncodedLabel></td>
            <td><ml:EncodedLabel runat="server" ID="Application"></ml:EncodedLabel></td> 
            <td><ml:EncodedLabel runat="server" ID="Description"></ml:EncodedLabel></td>           
            <td><ml:EncodedLabel runat="server" ID="InternalDescription"></ml:EncodedLabel></td>
            <td><ml:EncodedLabel runat="server" ID="PageCount"></ml:EncodedLabel></td>
            <td><ml:EncodedLabel runat="server" ID="LastModified"></ml:EncodedLabel></td>
            <td><a runat="server" id="viewLink">view</a></td>
            <td style="display:none" runat="server"><%# AspxTools.HtmlString(IsVisibleStatusType((E_EDocStatus)DataBinder.Eval(Container.DataItem, "docStatus"))?"0":"1") %></td>
        </tr>
    </AlternatingItemTemplate>
    
    <FooterTemplate>
            </tbody>
        </table>
    </FooterTemplate>
</asp:Repeater>
<ml:EncodedLabel ID="m_passwordMessage" runat="server" style="color:Red; font-size:small; font-weight:bolder" Visible="false" Text="* This eDoc has an owner password and cannot be shipped as a single PDF" />

<script type="text/javascript">


    
     function _init() {
        RefreshNextButton();
     }

    function RefreshNextButton() {
        disableNextButton(true);
        var table = document.getElementById('DocSelectionList');

        var inputs = table.getElementsByTagName('input');
        var masterCheckBox = inputs[0];
        
        for( var i = 1; i < inputs.length; i++) {
            var input = inputs[i];
            if ( typeof(input.type) !== 'undefined' && input.type === 'checkbox' && input.checked ) {
                disableNextButton(false);
                break;
            }
        }
        
        masterCheckBox.checked = true;
        for (var i = 1; i < inputs.length; i++) {
            var input = inputs[i];
            if (typeof (input.type) !== 'undefined' && input.type === 'checkbox' && !input.checked) {
                masterCheckBox.checked = false;
                break;
            }
        }        
    }

    
    Array.prototype.contains = function(obj) {
        var i = this.length;
        while (i--) {
            if (this[i] === obj) {
            return true;
        }
        }
        return false;
    }

</script>
