﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public class EditEDocV2ServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(EditEDocV2ServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            // Set value from webform to CPageData

            // dataLoan.sLNm = GetString("sLNm");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            // Set value from CPageData to webform

            // SetResult("sLNm", dataLoan.sLNm);
        }
    }
    public partial class EditEDocV2Service : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new EditEDocV2ServiceItem());
        }
    }


}
