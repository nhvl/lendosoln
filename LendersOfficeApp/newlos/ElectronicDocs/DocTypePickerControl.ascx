﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocTypePickerControl.ascx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.DocTypePickerControl" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Security" %>

<%--
    Case 65393.
    EDocument Type Picker. This replaces the asp:DropDownList previously used to pick out EDocument types.
    In the codebehind, the Value attribute will contain the user's selected value. Use that to save.
    
    Custom Attributes:
        Width       - The width of the control
        OnChange    - The javascript to call once a doctype has been selected
        Value       - The number of the doctype selected (readonly)
        ValueCtrl   - The control holding the Value. This is useful for getting the value in javascript,
                        since Value will bind at the page's compile time.
                        Example: <%= AspxTools.JsGetElementById(myDocPicker.ValueCtrl)%>.value
    TODO: add readonly attribute
--%>

<script type="text/javascript">
<!--
var docTypePickerLink = "/newlos/ElectronicDocs/DocTypePicker.aspx" ;

<% if ( BrokerUserPrincipal.CurrentPrincipal == null) { %>
    docTypePickerLink += "?brokerId=" + <%= AspxTools.JsString(BrokerID) %>
<% } %>

function fireChange(ctrl) {

    triggerEvent(ctrl, "change");
}

var <%= AspxTools.HtmlString(this.ClientID) %> = {
    showDialog : function(elem) {
        var queryString = "";
        showModal(docTypePickerLink + queryString, null, null, null, function(result){
                if (result && result.docTypeId && result.docTypeName) { // New doc type selected
                    <%= AspxTools.JsGetElementById(m_selectedDocType)%>.value = result.docTypeId;
                    <%= AspxTools.JsGetElementById(m_selectedDocTypeDescription)%>.textContent = result.docTypeName;
                
                    if (typeof OnChange_gDocTypePicker === 'function') {
                        OnChange_gDocTypePicker(result.docTypeId, result.docTypeName, result.folder, result.doconly);
                    }
                
                    // Provide (imperfect) onchange functionality for the user control
                    // Imperfections: 'this' will refer to the m_selectedDocType textbox
                    var ctrl = <%= AspxTools.JsGetElementById(m_selectedDocType)%>;
                    fireChange(ctrl);
                }
            },
            { hideCloseButton: true });
        return;
    },
    change : function() {
        var ctrl = <%= AspxTools.JsGetElementById(m_selectedDocType)%>;
        fireChange(ctrl);
    },
    clear : function() {
        <%= AspxTools.JsGetElementById(m_selectedDocTypeDescription) %>.innerHTML = <%= AspxTools.JsString(DefaultText) %>;
        <%= AspxTools.JsGetElementById(m_selectedDocType) %>.value = "-1";
        fireChange(<%= AspxTools.JsGetElementById(m_selectedDocType)%>);
        return false;
    }
}

-->
</script>

<span class="DocTypePicker" id="m_DocTypePickerDiv" runat="server">
    <ml:EncodedLabel runat="server" ID="m_selectedDocTypeDescription" class="SelectedDocTypeName"></ml:EncodedLabel>
       <span runat="server" id="m_closeSpan"  visible="false" >
        [ <a runat="server" href="javascript:void(0);" id="m_clearLink" >clear</a> ]
    </span>
    <span runat="server" id="m_dialogSpan" class="DocTypePickerLink">
        [ <a runat="server" id="m_dialogLink" href="javascript:void(0);" class="dialog-link">select Doc Type</a> ]
    </span>

    <asp:TextBox runat="server" ID="m_selectedDocType" Text="-1" class="SelectedDocTypeId"/>
    <input type="hidden" value="" runat="server" class="curid" id="m_defaultId" />
    <input type="hidden" value="" runat="server" id="m_defaultText" />
</span>
