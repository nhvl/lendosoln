﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BatchRotateEDoc.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.BatchRotateEDoc" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="LendersOffice.AntiXss" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Batch Rotate</title>
    <style>
    .padding {
      padding-left:10px;
    }
    .page_textbox 
    {
      width:30px;
      text-align:center;
    }    
    </style>    
</head>
<body bgcolor="gainsboro" onunload="getModalArgs().callback()">
  <script type="text/javascript">
    var g_iTotalPages = 0;
    
    function _init() {
      if(window.opener && window.opener.SetupModelessEnvironment){
          window.opener.SetupModelessEnvironment(window);
      }

      resize(200, 250);

      var arg = getModalArgs() || {};
      g_iTotalPages = arg.TotalPages;
      document.getElementById("NumOfPagesLabel").innerText = g_iTotalPages;
      
      var selectionFrom = 1, selectionTo = 1; 
      if ( arg.SelectedRanges != null && arg.SelectedRanges[0]  != null){
          var selectionRange = arg.SelectedRanges[0]; 
          selectionFrom = selectionRange.Start ? selectionRange.Start + 1 : 1;
          selectionTo = selectionFrom + (selectionRange.Count ? selectionRange.Count - 1 : 0);
      }
      
      document.getElementById("tbFrom").value = selectionFrom;
      document.getElementById("tbTo").value = selectionTo;

    }
    function getRotation() {
      if (<%= AspxTools.JsGetElementById(rbRotateRight) %>.checked) {
        return 90;
      } else if (<%= AspxTools.JsGetElementById(rbRotateLeft) %>.checked) {
        return -90;
      } else if (<%= AspxTools.JsGetElementById(rbRotate180) %>.checked) {
        return 180;
      } else {
        return 0;
      }
    }
    function onOK() {
      var arg = getModalArgs() || {};

      arg.rotation = getRotation();
      
      var bIsAllPages = <%= AspxTools.JsGetElementById(rbApplyAll) %>.checked;
      
      if (bIsAllPages) {
        arg.From = 1;
        arg.To = g_iTotalPages;
      } else {
        var from = parseInt(document.getElementById("tbFrom").value);
        var to = parseInt(document.getElementById("tbTo").value);
        if (isNaN(from) || isNaN(to) || to < from || from <= 0 || to > g_iTotalPages || from.toString() != document.getElementById("tbFrom").value || to.toString() != document.getElementById("tbTo").value) {
          alert('Invalid Page Ranges.');
          document.getElementById("tbFrom").focus();
          return;
        }

        arg.From = from;
        arg.To = to;      
      }
      arg.OK = true;
      onClosePopup(arg);
    }
    function onCancel() {
      var arg = getModalArgs();
      arg.OK = false;
      onClosePopup(arg);
    }
    function renderUI() {
      var bIsApplyAll = <%= AspxTools.JsGetElementById(rbApplyAll) %>.checked;

      if (bIsApplyAll) {
        document.getElementById("tbFrom").value = "";
        document.getElementById("tbTo").value = "";
      }
    }
    function enablePageRange() {
      <%= AspxTools.JsGetElementById(rbApplyRange) %>.checked = true;
    }
    
  </script>
    <form id="form1" runat="server">
    <div>
    <table width="100%">
      <tr><td class="MainRightHeader">Batch Rotate</td></tr>
      <tr>
        <td class="padding">
          <asp:RadioButton ID="rbRotateRight" runat="server" GroupName="op" Text="Rotate right (90 degrees)" value="90"/>
        </td>
      </tr>
      <tr>
        <td class="padding">
          <asp:RadioButton ID="rbRotateLeft" runat="server" GroupName="op" Text="Rotate left (90 degrees)" value="-90"/>
        </td>
      </tr>
      <tr>
        <td class="padding">
          <asp:RadioButton ID="rbRotate180" runat="server" GroupName="op" Text="Flip upside-down (180 degrees)" value="180" Checked="true"/>
        </td>
      </tr>
      <tr><td>&nbsp;</td></tr>
      <tr><td class="padding">
        <asp:RadioButton ID="rbApplyAll" runat="server" GroupName="apply" Text="Apply to all pages" Checked="true" onclick="renderUI();"/>
      </td></tr>
      <tr>
        <td class="padding">
          <asp:RadioButton ID="rbApplyRange" runat="server" GroupName="apply" Text="Apply to pages:" />
          <input type="text" class="page_textbox" id="tbFrom" onfocus="enablePageRange();"/> to <input type="text" class="page_textbox" id="tbTo" onfocus="enablePageRange();"/> of <span id="NumOfPagesLabel" />
      </td></tr>
      <tr>
        <td align="center">
          <input type="button" value="OK" onclick="onOK();" />&nbsp;&nbsp;
          <input type="button" value="Cancel" onclick="onCancel();" />
        </td>
      </tr>

    </table>
    </div>
    <uc1:cmodaldlg id="Cmodaldlg1" runat="server" />
    </form>
</body>
</html>
