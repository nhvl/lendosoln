﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentRejection.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.DocumentRejection" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.ObjLib.DocumentConditionAssociation" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Reject Document</title>
    <style type="text/css" >
        body {
            padding: 10px;
        }
        .seperator { margin-top: 20px; }
        .scrollable { height: 80px; overflow-y: auto;}
        td.right { border-right: 1px solid black; }
    </style>
</head>
<body class="EditBackground">
    <h4 class="page-header">Reject Document</h4>
    <form id="form1" runat="server" onsubmit="return false;">
    <div>
        <label>Reason for rejection: <input type="text" id="Reason" size=50/></label>
        <asp:PlaceHolder runat="server" id="AssociationSection" Visible="false">
        <br />
        <br />
            Other associated conditions:
            <div class="scrollable">
            <asp:Repeater runat="server" ID="Associations">
                <HeaderTemplate>
                    <table style="border-collapse: collapse;"  cellpadding=2>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="GridItem">
                        <td class="right">
                            <%# AspxTools.HtmlString(GetTaskId((DocumentConditionAssociation)Container.DataItem)) %>
                        </td>
                        <td>
                            <%# AspxTools.HtmlString(((DocumentConditionAssociation)Container.DataItem).TaskSubject) %>
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="GridAlternatingItem">
                        <td class="right">
                            <%# AspxTools.HtmlString(GetTaskId((DocumentConditionAssociation)Container.DataItem)) %>
                        </td>
                        <td>
                            <%# AspxTools.HtmlString(((DocumentConditionAssociation)Container.DataItem).TaskSubject) %>
                        </td>
                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            </div>
        </asp:PlaceHolder>

        <div class="seperator">
            <input type="button" value="Reject for THIS Condition" id="RejectThis" />
            <input type="button" id="RejectBtn" runat="server" value="Reject for ALL Conditions" />
            <input type="button" value="Cancel" id="Cancel" />
        </div>
    </div>
    </form>
    <script type="text/javascript">
        $(function(){
            resizeForIE6And7(500,200);
            
            $('#RejectBtn').click(function(){
                var args = {};
                args.Status = 'All';
                args.Reason = $('#Reason').val();
                onClosePopup(args);
            });
            
            $('#RejectThis').click(function(){
                var args = {};
                args.Status = 'This';
                args.Reason = $('#Reason').val();
                onClosePopup(args);  
            });
            
            $('#Cancel').click(function(){ onClosePopup(); });
        });
    </script>
</body>
</html>
