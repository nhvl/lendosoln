﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.ObjLib.DocumentConditionAssociation;
using LendersOffice.Security;
using System.Web.Services;
using EDocs;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class EditConditionAssociation : BaseLoanPage
    {
        private Guid DocId
        {
            get
            {
                return RequestHelper.GetGuid("DocId", Guid.Empty);
            }
        }

        private bool HasDocId
        {
            get
            {
                bool ret;

                try
                {
                    var raw = Request["DocId"];
                    if (string.IsNullOrEmpty(raw)) return false;

                    var temp = new Guid(raw);
                    ret = true;
                }
                catch (FormatException)
                {
                    ret = false;
                }


                return ret;
            }
        }

        protected class ConditionAssociationDTO
        {
            public string TaskId;
            public bool IsProtected;
            public bool IsAssociated;
            public string ConditionText;
            public string Category;
            public int RowId;
            public int Order;

            public ConditionAssociationDTO(AssociatedTask t, bool docIsProtected, int idx, bool useStaticConditionIds)
            {
                IsAssociated = t.IsAssociated;
                TaskId = t.Condition.TaskId;
                RowId = useStaticConditionIds ? t.Condition.CondRowId : idx + 1;
                Order = idx;

                IsProtected = false;
                if (docIsProtected && !BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowEditingApprovedEDocs))
                {
                    if (t.Association != null)
                    {
                        IsProtected = t.Association.Status == E_DocumentConditionAssociationStatus.Satisfied;
                    }
                }

                ConditionText = RowId + ". " + (IsProtected ? "(PROTECTED) " : "") + t.Condition.TaskSubject;
                Category = t.Condition.CondCategoryId_rep;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DisplayCopyRight = false;
            EnableJqueryMigrate = false;
            
            var conditions = DocumentConditionAssociation.GetConditionsByLoanDocument(PrincipalFactory.CurrentPrincipal, LoanID, DocId, excludeThoseUserDoesntHaveAccessTo: true)
                             .Where(a => a.Condition.TaskStatus != LendersOffice.ObjLib.Task.E_TaskStatus.Closed).ToList();
            var source = new List<ConditionAssociationDTO>(conditions.Count());

            RegisterJsScript("json.js");
            RegisterJsScript("jquery.tablesorter.min.js");
            
            var docIsProtected = false;
            if(HasDocId) docIsProtected = EDocumentRepository.GetUserRepository().GetDocumentById(DocId).DocStatus == E_EDocStatus.Approved;

            for (var i = 0; i < conditions.Count; i++)
            {
                var c = conditions[i];
                source.Add(new ConditionAssociationDTO(c, docIsProtected, i, Broker.IsUseNewTaskSystemStaticConditionIds));
            }

            RegisterJsGlobalVariables("DocId", DocId);
            RegisterJsGlobalVariables("HasDocId", HasDocId);

            ConditionsRepeater.DataSource = source.OrderBy(a => a.RowId);
            ConditionsRepeater.DataBind();

        }
    }
}
