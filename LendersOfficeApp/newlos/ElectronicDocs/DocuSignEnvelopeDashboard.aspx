﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocuSignEnvelopeDashboard.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.DocuSignEnvelopeDashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" ng-app="DocuSignEnvelopeDashboard">
<head runat="server">
    <title>DocuSign Envelope Dashboard</title>
    <style type="text/css">
        div.BlockMask {
            position: fixed;
        }
    </style>
</head>
<body class="BackgroundGainsboro">
    <form id="form1" runat="server">
        <div class="MainRightHeader">DocuSign Envelope Statuses</div>
        <div id="EnvelopeTableControllerDiv" ng-controller="EnvelopeTableController" class="ng-cloak">
            <div status-Details-Popup class="Hidden" id="StatusDetails" status-Details="StatusDetails"></div>
            <div class="Hidden" id="VoidConfirmationPopup">
                <div class="FullWidthHeight BackgroundGainsboro VoidConfirmationDiv">
                    <div class="align-left MainRightHeader">Void Confirmation</div>
                    <div class="PaddingTop horizontal-margin-auto Width75Pc">
                        <div class="align-left">
                            <div class="FieldLabel">Void reason for {{VoidDetails.EnvelopeName}}:</div>
                            <div>The reason(s) stated will be sent to all recipients.</div>
                        </div>
                        <br />
                        <div>
                            <textarea class="VoidReason Width100Pc" id="VoidReason" cols="60" rows="15" maxlength="200"></textarea>
                            <div class="align-left SubtextItalics">
                                DocuSign character limit: <span class="VoidReasonCharCount">0</span>/200
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="align-bottom">
                        <input type="button" value="Void" class="ConfirmVoidBtn"/>
                        <input type="button" onclick="LQBPopup.Return(null);" value="Cancel" />
                    </div>
                </div>
            </div>

            <table class="EnvelopeTable Table">
                <thead>
                    <tr class="LoanFormHeader">
                        <th class="NameColumn">
                            <a ng-click="SetSort('Name')">Name</a> 
                            <span class="fa fa-sort-alpha-asc" ng-show="EnvelopeOrder === 'Name' && !EnvelopeOrderReverse"></span><span class="fa fa-sort-alpha-desc" ng-show="EnvelopeOrder === 'Name' && EnvelopeOrderReverse"></span>
                        </th>
                        <th class="EnvelopeStatusColumn">
                            <a ng-click="SetSort('Status_rep')">Envelope Status</a>
                            <span class="fa fa-sort-alpha-asc" ng-show="EnvelopeOrder === 'Status_rep' && !EnvelopeOrderReverse"></span><span class="fa fa-sort-alpha-desc" ng-show="EnvelopeOrder === 'Status_rep' && EnvelopeOrderReverse"></span>
                        </th>
                        <th class="CreationDateColumn">
                            <a ng-click="SetSort('CreationDate')">Creation Date</a>
                            <span class="fa fa-sort-desc" ng-show="EnvelopeOrder === 'CreationDate' && !EnvelopeOrderReverse"></span><span class="fa fa-sort-asc" ng-show="EnvelopeOrder === 'CreationDate' && EnvelopeOrderReverse"></span>
                        </th>
                        <th class="SenderNameColumn">
                            <a ng-click="SetSort('SenderName')">Sender Name</a>
                            <span class="fa fa-sort-alpha-asc" ng-show="EnvelopeOrder === 'SenderName' && !EnvelopeOrderReverse"></span><span class="fa fa-sort-alpha-desc" ng-show="EnvelopeOrder === 'SenderName' && EnvelopeOrderReverse"></span>
                        </th>
                        <th class="RecipientsColumn">
                            <a ng-click="SetSort('Recipients[0].Name')">Recipients</a>
                            <span class="fa fa-sort-alpha-asc" ng-show="EnvelopeOrder === 'Recipients[0].Name' && !EnvelopeOrderReverse"></span><span class="fa fa-sort-alpha-desc" ng-show="EnvelopeOrder === 'Recipients[0].Name' && EnvelopeOrderReverse"></span>
                        </th>
                        <th class="SentDocumentsColumn">
                            <a ng-click="SetSort('SentDocuments[0].Name')">Sent Documents</a>
                            <span class="fa fa-sort-alpha-asc" ng-show="EnvelopeOrder === 'SentDocuments[0].Name' && !EnvelopeOrderReverse"></span><span class="fa fa-sort-alpha-desc" ng-show="EnvelopeOrder === 'SentDocuments[0].Name' && EnvelopeOrderReverse"></span>
                        </th>
                        <th class="ReturnedDocumentsColumn">
                            <a ng-click="SetSort('ReturnedDocuments[0].Name')">Returned Documents</a>
                            <span class="fa fa-sort-alpha-asc" ng-show="EnvelopeOrder === 'ReturnedDocuments[0].Name' && !EnvelopeOrderReverse"></span><span class="fa fa-sort-alpha-desc" ng-show="EnvelopeOrder === 'ReturnedDocuments[0].Name' && EnvelopeOrderReverse"></span>
                        </th>
                        <th class="OperationsColumn"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="ReverseGridAutoItem" ng-repeat="Envelope in Envelopes | orderBy : EnvelopeOrder : EnvelopeOrderReverse track by Envelope.Id">
                        <td>
                            <span class="Name">{{Envelope.Name}}</span>
                        </td>
                        <td>
                            <span class="EnvelopeStatus">{{Envelope.Status_rep}}</span>
                            <br />
                            <a ng-click="ShowEnvelopeStatuses(Envelope)">view details</a>
                        </td>
                        <td>
                            <span class="CreationDate">{{Envelope.CreationDate | date : 'M/d/yyyy h:mm a'}}</span>
                        </td>
                        <td>
                            <span class="SenderName">{{Envelope.SenderName}}</span>
                        </td>
                        <td>
                            <div ng-if="Envelope.Recipients.length > 0">
                                <span>{{Envelope.Recipients[0].Name}}</span>
                                <br />
                                <a ng-click="ShowRecipientDetails(Envelope.Id)">view details</a>
                            </div>
                        </td>
                        <td>
                            <div ng-if="Envelope.SentDocuments.length == 1">
                                <span ng-if="Envelope.SentDocuments[0].EdocId == ''">{{Envelope.SentDocuments[0].Name}}</span>
                                <a ng-if="Envelope.SentDocuments[0].EdocId != ''" ng-click="DownloadDocument(Envelope.SentDocuments[0])">{{Envelope.SentDocuments[0].Name}}</a>
                            </div>
                            <div ng-if="Envelope.SentDocuments.length > 1">
                                <ul class="UndecoratedList">
                                    <li ng-repeat="Document in Envelope.SentDocuments">
                                        <span ng-if="Document.EdocId == ''" ng-class="{Hidden: !$first && !Envelope.ShowSentDocs}">{{Document.Name}}</span>
                                        <a ng-if="Document.EdocId != ''" ng-class="{Hidden: !$first && !Envelope.ShowSentDocs}" ng-click="DownloadDocument(Document)">{{Document.Name}}</a>
                                    </li> 
                                </ul>
                                <a ng-if="!Envelope.ShowSentDocs" ng-click="Envelope.ShowSentDocs = !Envelope.ShowSentDocs">view more...</a>
                                <a ng-if="Envelope.ShowSentDocs" ng-click="Envelope.ShowSentDocs = !Envelope.ShowSentDocs">view less...</a>
                            </div>
                        </td>
                        <td>
                            <div ng-if="Envelope.ReturnedDocuments.length == 1">
                                <span ng-if="Envelope.ReturnedDocuments[0].EdocId == ''">{{Envelope.ReturnedDocuments[0].Name}}</span>
                                <a ng-if="Envelope.ReturnedDocuments[0].EdocId != ''" ng-click="DownloadDocument(Envelope.ReturnedDocuments[0])">{{Envelope.ReturnedDocuments[0].Name}}</a>
                            </div>
                            <div ng-if="Envelope.ReturnedDocuments.length > 1">
                                <ul class="UndecoratedList">
                                    <li ng-repeat="Document in Envelope.ReturnedDocuments">
                                        <span ng-if="Document.EdocId == ''" ng-class="{Hidden: !$first && !Envelope.ShowReturnedDocs}">{{Document.Name}}</span>
                                        <a ng-if="Document.EdocId != ''" ng-class="{Hidden: !$first && !Envelope.ShowReturnedDocs}" ng-click="DownloadDocument(Document)">{{Document.Name}}</a>
                                    </li> 
                                </ul>
                                <a ng-if="!Envelope.ShowReturnedDocs" ng-click="Envelope.ShowReturnedDocs = !Envelope.ShowReturnedDocs">view more...</a>
                                <a ng-if="Envelope.ShowReturnedDocs" ng-click="Envelope.ShowReturnedDocs = !Envelope.ShowReturnedDocs">view less...</a>
                            </div>
                        </td>
                        <td class="no-wrap">
                            <a ng-if="IsInternal" ng-click="RequestUpdate(Envelope)">request</a>
                            <span ng-if="IsInternal && (Envelope.CanVoid || Envelope.IncompleteInPerson)">&nbsp;|&nbsp;</span>
                            <a ng-if="Envelope.CanVoid" ng-click="RequestVoid(Envelope)">void</a>
                            <span ng-if="Envelope.CanVoid && Envelope.IncompleteInPerson">&nbsp;|&nbsp;</span>
                            <a ng-if="Envelope.IncompleteInPerson" ng-click="ResumeSigning(Envelope)">recover</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
