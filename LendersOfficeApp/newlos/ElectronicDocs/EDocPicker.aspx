﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EDocPicker.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.EDocPicker" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="EDocs" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <base target="_self" />
    <title>EDocument Selector</title>
    <style type="text/css">
        div.scrollable {

            overflow-y: auto;
            height: 300px;
        }

        div.buttonPanel  {
            text-align: center;
            margin-top: 10px;
        }
        table {
            table-layout: fixed;
        }
        #DocTable th { color: Blue; text-decoration: underline; cursor: pointer; }
        #DocTable th.none { text-decoration: none; cursor: auto; }
        .padding {
            padding-top: 10px;
            padding-left: 10px;
            padding-right: 10px;
        }
        .highlight {
            background-color: Yellow !important;
        }
    </style>
</head>
<body class="EditBackground">
    <h4 class="page-header">Select additional documents to add to this condition</h4>
    <form id="form1" runat="server">
        <div class="padding">
     <div class="FieldLabel">Please select which documents to associate with this condition. Documents of the required type are highlighted.    <br />
</div>
        <table >
            <tr class="GridHeader">
                <th>Condition</th>
                <th>Category</th>
                <th>Subject</th>
                <th>Required Doc Type</th>
                <th>Due Date</th>
            </tr>
            <tr class="GridItem">
                <td><ml:EncodedLiteral runat="server" ID="ConditionId"></ml:EncodedLiteral></td>
                <td><ml:EncodedLiteral runat="server" ID="CondCategory"></ml:EncodedLiteral></td>
                <td><ml:EncodedLiteral runat="server" ID="TaskSubject"></ml:EncodedLiteral></td>
                <td><ml:EncodedLiteral runat="server" ID="CondRequiredDoctype"></ml:EncodedLiteral></td>
                <td><ml:EncodedLiteral runat="server" ID="TaskDueDate"></ml:EncodedLiteral></td>
            </tr>
        </table>
        <br />
        <br />
    <div>
     <label class="FieldLabel" >Search By Description: <input type="text" name="Query" id="Query" /> </label>
       </div>
    <div class="scrollable">
        <asp:Repeater runat="server" ID="DocListing"  OnItemDataBound="DocListing_OnItemDataBound">
            <HeaderTemplate>
                <table id="DocTable">
                    <colgroup>
                        <col width="20" />
                        <col width="125" />
                        <col width="125" />
                        <col />
                        <col width="150"/>
                        <col width="90"/>
                        <col width="30"/>
                    </colgroup>
                    <thead>
                        <tr class="GridHeader">
                            <th class="none"><input type="checkbox" name="selectAll" id="selectAll" /></th>
                            <th >Folder</th>
                            <th>DocType</th>
                            <th>Description</th>
                            <th>Comments</th>
                            <th>Last Modified</th>
                            <th class="none">&nbsp;</td>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class='<%# AspxTools.HtmlString(GetEDocClass(  ((EDocument)Container.DataItem ))) %>'>
                    <td>
                        <input type="checkbox"  runat="server" id="picker" class="picked" />
                    </td>
                    <td>
                        <%# AspxTools.HtmlString( ((EDocument)Container.DataItem).Folder.FolderNm ) %>
                    </td>
                    <td>
                        <%# AspxTools.HtmlString( ((EDocument)Container.DataItem).DocTypeName ) %>
                    </td>
                    <td>
                        <%# AspxTools.HtmlString( ((EDocument)Container.DataItem).PublicDescription ) %>
                    </td>
                    <td>
                        <%# AspxTools.HtmlString( ((EDocument)Container.DataItem).InternalDescription ) %>
                    </td>
                    <td>
                        <%# AspxTools.HtmlString( ((EDocument)Container.DataItem).LastModifiedDate.ToString("M/d/yy h:mm tt")) %>
                    </td>
                    <td>
                        <a href="#" data-docid='<%# AspxTools.HtmlString( ((EDocument)Container.DataItem).DocumentId.ToString() )%>' class="view"  >view</a>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</div>
        <div class="buttonPanel">
            <input type="button" value=" OK "  id="OkBtn" runat="server"/>
            <input type="button" value="Cancel" id="CancelBtn" />
        </div>

        </form>
    <script type="text/javascript">
        $(function(){
            var rows = $('#DocTable tbody tr'), data = [], $query = $('#Query'), timeoutID = null,
                str = null, $o, $tds, isAlt = false, cbx = $('input.picked'), docs  = [];
            var dlgArgs = getModalArgs();
            if (dlgArgs && dlgArgs.docs) {
                docs = JSON.parse(dlgArgs.docs);
            }

            resizeForIE6And7(700,500);
            var filter = [];
            if (dlgArgs && dlgArgs.FilterDocsIds){
                filter = dlgArgs.FilterDocsIds;
            }
            rows.each( function(i,o){
                $o = $(o);
                $tds = $o.children('td');
                if ($.inArray($tds.eq(0).find('input.picked').data('docid'), filter)> -1) {
                    $o.remove();
                    return;
                }
                if ($.inArray($tds.eq(0).find('input.picked').data('docid'), docs) > -1)  {
                    $tds.eq(0).find('input').prop('checked', true);
                }
                str = $tds.eq(2).text().toLowerCase() + '~' + $tds.eq(3).text().toLowerCase() + '~' + $tds.eq(4).text().toLowerCase();
                data.push(str);
                $o.addClass(isAlt ? 'GridAlternatingItem' : 'GridItem');
                isAlt = !isAlt;
            });

            rows = $('#DocTable tbody tr');

            $('#DocTable').on('click', 'a.view', function(){
                var docid= $(this).data('docid');
                window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + docid , '_parent');
                return false;
            }).tablesorter({
                headers : { 0: { sorter: false}, 6: {sorter: false} }
            }).on("sortEnd", function(){
                var alt = false;
                $('#DocTable tbody tr').each(function(i,o){
                    if ( alt ) { $(o).removeClass('GridItem').addClass('GridAlternatingItem');}
                    else { $(o).removeClass('GridAlternatingItem').addClass('GridItem'); }
                    alt = !alt;
                });
            });

            function onClose(hasChanged, args) {
                if (isLqbPopup(window)) {
                    var returnObject = { OK: false, HasChanged: hasChanged };
                    returnObject.DocIds = args ? args.DocIds : null;
                    parent.LQBPopup.Return(returnObject);
                }
                else {
                    onClosePopup();
                }
            }

            function saveAssociations() {
                var docIds = [];
                cbx.filter(':checked').each(function(){
                    docIds.push($(this).data('docid'));
                });
                var data = {
                    taskId : $('#TaskId').val(),
                    documentIds : docIds
                };

                callWebMethodAsync({
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    url: 'EDocPicker.aspx/SaveAssociations',
                    data: JSON.stringify(data),
                    dataType: 'json',
                    async: false
                }).then(
                    function(args) {
                        onClose(true, args);
                    },
                    function(args) {
                        alert("There was an unexpected error sading the associations. Please close the page and try again.");
                    }
                );
            }

            function searchTable() {
                var value = $query.val(), i = 0, rowCount = data.length, row;
                if (value.length == 0) {
                    rows.show();
                    return;
                }

                value = value.toLowerCase();

                for (i; i < rowCount; i++) {
                    row = data[i];
                    if (row.indexOf(value) == -1) {
                        rows.eq(i).hide();
                    }
                    else {
                        rows.eq(i).show();
                    }
                }
            }

            $query.keyup (function(){
                if (timeoutID) {
                    clearTimeout(timeoutID);
                }

                timeoutID = setTimeout(searchTable, 500);
            });

            $('#selectAll').change(function(){
                var $o = $(this);

                if ($o.is(':checked')) {
                    cbx.filter(':visible').prop('checked', true);
                }
                else {
                    cbx.prop('checked', false);
                }
            });

            $('#CancelBtn').click(function(){
                onClose();
            });

            $('#OkBtn').click(function(){
                if ($(this).hasClass('persist')) {
                    saveAssociations();
                    return;
                }
                var docIds = [];
                cbx.filter(':checked').each(function(){
                    docIds.push($(this).data('docid'));
                });

                var args = window.dialogArguments || {};
                args.DocIds = docIds;

                onClose(true, args);
            });

            $('form').submit(function(){
                return false;
            });

        });
    </script>
</body>
</html>
