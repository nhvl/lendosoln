﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocsReceived.ascx.cs" EnableViewState="false"
    Inherits="LendersOfficeApp.newlos.ElectronicDocs.DocsReceived" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<div id='CalculationOverlay' class="Hidden"></div>
<div id="UploadGenericDoc" style="display: none; font-weight: bold; background-color: gainsboro; position:absolute; padding: 10px; border: 4px solid black;">
    <legend>Update File</legend>

    <input type="hidden" id="DocToReplaceId" name="DocToReplaceId" />
    
    <div class="InsetBorder full-width">
        <div id="DragDropZone" class="drag-drop-container" extensions=".xml, .pdf, .xls, .xlsx" limit="1"></div>
    </div>

    <br />
    <br />
    <input type="button" value="Replace File" id="ReplaceFileBtn" /> &nbsp;&nbsp;&nbsp; <input type="reset" value="Cancel" NoHighlight="NoHighlight" onclick="Modal.Hide()" />
</div>

<style type="text/css">
    .Closed
    {
        display:none;
    }
    .Opened
    {
        display:;
    }
    td.Opened
    {
        font-weight:bold;
    }
    td.Opened a
    {
        font-weight:normal;
    }
    td.Opened .actionLink
    {
        font-weight:bold;
    }
    .Hidden { display: none; }
    
    .info {
        width: 100px;
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
    }

    .IconLegend
    {
        padding-top: 5px;
    }

    .IconLegend .secondColumn
    {
        padding-left: 30px;
    }

    .CreateEditableCopy 
    {
        white-space: nowrap;
    }

    .FolderName
    {
        width: 100px;
        vertical-align: top;
    }

    .DocType
    {
        width: 150px;
        vertical-align: top;
    }
    .LastModified
    {
        vertical-align: top;
    }

    .Highlight
    {
        background-color: yellow;
    }

    #CalculationOverlay {
        position: absolute;
        left: 0;
        top: 0; 
        height: 100%;
        width: 100%;
        background-color: black;
        opacity: 0;
        z-index: 9999998;
    }

    #CalculationOverlay.darkened {
        opacity: .3;
    }
</style>

<script type="text/javascript">    
    var progressingDownload = false;
    function focusself() {
        window.focus();
    }
    
    function view(docid, event) {
        if(progressingDownload)
        {
            return;
        }
        progressingDownload = true;

        openWithProgress(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + docid, event);
    }
    
    function openXmlDoc(docid) {
        window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?cmd=xml&docid=' + docid, 'xmldoc');
    }
    function openWithProgress(link, event)
    {   
        fetchFileAsync(link, 'application/pdf; charset=UTF-8', fnStartInit);
    }
        
    var windowMap = {};
    
    function reassign(docid, loanid){
        var id = 'a' +docid ;
        if( !!!windowMap[id]  || windowMap[id].closed )  {
            windowMap[id] = window.open('ForceChangeDocType.aspx?loanid=' + loanid + '&docid=' + docid,id, "height=100,width=600,menubar=no,scrollbars=no,resizable=yes");
        }
        windowMap[id].focus();   
    }
    
    function edit(link, docid, loanid){
        if(hasDisabledAttr(link)) {
            return false;
        }
        
        
        var id = 'a' +docid ;
        if( !!!windowMap[id]  || windowMap[id].closed )  {
            windowMap[id] = window.open('EditEdocV2.aspx?loanid=' + loanid + '&docid=' + docid,id, "height=864,width=1024,menubar=no,scrollbars=no,resizable=yes");
        }
        windowMap[id].focus();   
    }
    
    function restore(loanid){
        window.open('DeletedDocs.aspx?loanid=' + loanid, 'deleted_docs' + loanid, "height=400,width=1024,menubar=no,scrollbars=yes,resizable=yes");
    }

    function showAudit(docid) {
        showModal('/newlos/ElectronicDocs/EDocumentAudit.aspx?loanid='+<%= AspxTools.JsString(LoanID) %> + '&docid='+ docid, null, 'dialogHeight:400px;dialogWidth:500px;', null, null);
    }
    
    var colNumsToSearch = [];
    window.onload = function() {
        var colsToSearch = [
            'Status',
            'Folder',
            'Doc Type',
            'Borrower',
            'Description',
            'Internal Comments',
            'Associated Conditions',
            'Last Modified',
            'Uploaded Date',
            'Uploaded By'],
            table = document.getElementById('ReceivedDocsTable'),
            cols = table.tHead.rows[0].getElementsByTagName('th');
        
        for (var i = 0; i < cols.length; i++) {
            for (var iColNames = 0; iColNames < colsToSearch.length; iColNames++) {
                if ( cols[i].textContent.indexOf( colsToSearch[iColNames] ) >= 0 ) {
                    colNumsToSearch.push(i);
                }
            }
        }
    }
    
    function updateSearchResults(searchTerm) {
        var table = document.getElementById('ReceivedDocsTable'),
            rows = table.tBodies[0].rows,
            isAlt = false;
        
        searchTerm = searchTerm.toLowerCase();
        for (var ir = 0; ir < rows.length; ir++) {
            var row = rows[ir],
                searchResultInRow = false,
                lowestCurrCell = 0;
            if (row.getAttribute('userdeleted') == 'true') {
                continue;
            }
            
            for (var ic = 0; ic < row.cells.length; ic++) {
                if ( ic === colNumsToSearch[lowestCurrCell] ) { //if current column is a column we are searching
                    var cell = row.cells[ic];
                    cell.innerHTML = cell.textContent; //remove HTML in cells to remove highlighting from partial matches
                    var posFound = cell.textContent.toLowerCase().indexOf(searchTerm);
                    if ( posFound >= 0 ) {
                        var cellText = cell.textContent;

                        cell.innerHTML = '';

                        do {
                            var previousText = cellText.substr(0,posFound);
                            var previousTextNode = document.createElement("span");
                            previousTextNode.textContent = previousText;

                            var highlightedText = cellText.substr(posFound, searchTerm.length);
                            var highlightedTextNode = document.createElement("span");
                            highlightedTextNode.className = "Highlight";
                            highlightedTextNode.textContent = highlightedText;

                            cell.appendChild(previousTextNode);
                            cell.appendChild(highlightedTextNode);

                            cellText = cellText.substr(posFound + searchTerm.length);
                        } while ( (posFound = cellText.toLowerCase().indexOf(searchTerm)) > 0 )

                        var remainingTextNode = document.createTextNode(cellText);
                        cell.appendChild(remainingTextNode);

                        searchResultInRow = true;
                    }

                    lowestCurrCell++;
                }
            }
            if ( searchResultInRow ) {
                row.style.display = '';  
                row.className = isAlt ? 'GridAlternatingItem' : 'GridItem';     
                isAlt = !isAlt;
                continue;
            }
            row.style.display = 'none';
        }
    }
    
    function updateDisplay() {
        var ddlValue = document.getElementById('<%= AspxTools.ClientId( Folders ) %>' ).value;
        var table = document.getElementById('ReceivedDocsTable');
        var rows = table.tBodies[0].rows; 
        var isAlt = false;

        for( var i = 0; i < rows.length; i++)
        {
            var row = rows[i];
            if (row.getAttribute('userdeleted') == 'true') {
                continue;
            }
        
            if ( ddlValue == '-1' ||  row.getAttribute('folderid') == ddlValue  ) {
                row.style.display = '';  
                row.className = isAlt ? 'GridAlternatingItem' : 'GridItem';     
                isAlt = !isAlt;
                continue;
            }
            row.style.display = 'none';
            $('.BatchEdit', row).prop('checked', false);
        }
        
        UpdateBatchEditGuids();
        //set the window's hash value (the stuff after the #) to indicate the current selection.
        window.location.hash = ddlValue;
        
    }
    
    function alternateClasses(rows) {
        var isAlt = false;
        for( var i = 0; i < rows.length; i++ ) {
            var row = rows[i];
            if ( row.style.display === 'none' ) { continue; }
            row.className = isAlt ? 'GridAlternatingItem' : 'GridItem'; 
            isAlt = !isAlt; 
        }
        
    }

    $(function () {
        var dragDropSettings = {
            blockUpload: !ML.CanUploadEdocs,
            blockUploadMsg: ML.UploadEdocsFailureMessage
        };

        registerDragDropUpload(document.getElementById('DragDropZone'), dragDropSettings);

        var ColumnHeaderIdsToIndex = (function() {
            var table = document.getElementById('ReceivedDocsTable'),
                cols = table.tHead.rows[0].getElementsByTagName('th');
            var columnData = {};
            for (var i = 0; i < cols.length; i++) {
                columnData[cols[i].id] = i;
            }
            return columnData;
        })();

        var DocColumnHeaderIndex = {<%-- if this looks a little indirect, that's because it is. Declaring this as an object literal makes js intelli-sense work --%>
            Annotation: ColumnHeaderIdsToIndex['AnnotationColumn'],
            Icon: ColumnHeaderIdsToIndex['IconColumnHeader'],
            ESignReady: ColumnHeaderIdsToIndex['ESignReadyColumnHeader'],
            Status: ColumnHeaderIdsToIndex['StatusColumnHeader'],
            Action: ColumnHeaderIdsToIndex['ActionColumnHeader'],
            Folder: ColumnHeaderIdsToIndex['FolderColumnHeader'],
            DocType: ColumnHeaderIdsToIndex['DocTypeColumnHeader'],
            Borrower: ColumnHeaderIdsToIndex['BorrowerColumnHeader'],
            Description: ColumnHeaderIdsToIndex['DescriptionColumnHeader'],
            InternalComments: ColumnHeaderIdsToIndex['InternalCommentsColumnHeader'],
            AssociatedConditions: ColumnHeaderIdsToIndex['AssociatedConditionsColumnHeader'],
            Pages: ColumnHeaderIdsToIndex['PagesColumnHeader'],
            LastModified: ColumnHeaderIdsToIndex['LastModifiedColumnHeader'],
            DocTypeOrder: ColumnHeaderIdsToIndex['DocTypeOrderNumberColumnHeader'],
            UploadedDate: ColumnHeaderIdsToIndex['UploadedDateColumnHeader'],
            UploadedBy: ColumnHeaderIdsToIndex['UploadedByColumnHeader']
        };

        $('#searchBox').on('keyup', function() { updateSearchResults(this.value); });
        $('a.clearassoc').click(function(){
            var docId = $(this).data('docid');
            gService.edoc.call('ClearAssociations', { DocumentId : docId, LoanId : ML.sLId });
            reload();
        });
        $('#ReplaceFileBtn').click(function(){
            if (getFilesCount('DragDropZone') === 0) {
                LQBPopup.ShowString('Please select a file to upload.');
                return;
            }

            triggerOverlayImmediately();
            applyCallbackFileObjects("DragDropZone", function (file) {
                var args = {
                    loanid: ML.sLId,
                    DocumentId: $('#DocToReplaceId').val(),
                };

                DocumentUploadHelper.Upload('UpdateGenericDocContent', file, args, function (result) {
                    removeOverlay();

                    if (result.error) {
                        alert(result.UserMessage);
                    }
                    else {
                        window.location.href = window.location.href;
                    }
                });
            });
        });
        $('a.replaceLink').click(function(event){
            $('#DocToReplaceId').val($(this).data('docid'));
            Modal.ShowPopup('UploadGenericDoc', null, event);
            var $row = $(this).parents('tr');
            var row = $row[0];
            replaceTr = row;
            oldReplaceClass = row.className;
            row.className ='GridSelectedItem'; 
                    return false;
        });
            
        var isAsc = 0;
        var disabledSorts = [
            DocColumnHeaderIndex.Annotation,
            DocColumnHeaderIndex.Icon,
            DocColumnHeaderIndex.ESignReady,
            DocColumnHeaderIndex.Action,
            DocColumnHeaderIndex.InternalComments,
            DocColumnHeaderIndex.Pages
        ];
        
        var batchWindow; 
        $('.OpenBatchEditor').click(function(){
        var data = { 
            ID : $('.BatchEditGuids').val()
            };
            
            var result = gService.edoc.call('BatchStore',  data); 
            
            if( result.error ){
                alert( result.UserMessage ) ;
                return;
            }
            
            var body_url = $('.BatchUrl').val() + "?loanid=" + ML.sLId + "&key=" + result.value.key; 
            if( !!!batchWindow || batchWindow.closed) {
                batchWindow = window.open( body_url, $('.WindowName').val(),  "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes,width=950,height=600");
            }
            batchWindow.focus();
        });

        var docuSignEnvelopeWindow;
        $('.ViewDocuSignEnvelopesBtn').click(function() {
            var url = ML.VirtualRoot + "/newlos/ElectronicDocs/DocuSignEnvelopeDashboard.aspx?loanid=" + ML.sLId;
            if(!!!docuSignEnvelopeWindow || docuSignEnvelopeWindow.closed) {
                docuSignEnvelopeWindow = window.open(url, "DocuSignEnvelopeDashboard", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes,width=1000,height=500");
            }

            docuSignEnvelopeWindow.focus();
        });
        
        $(document).on('click', 'a.deleteGenericDoc', function(){
            var $row = $(this).parents('tr');
            var row = $row[0];
            var oldClassName = row.className; 
            row.className ='GridSelectedItem';
            
            if (confirm('Delete document?')) {
                var data = {DocId : $(this).data('docid')};
                var result = gService.edoc.call('DeleteGeneric',data); 
                row.className = oldClassName;
                          
                if( result.error ){
                    alert( result.UserMessage ) ;
                    return;
                }
                reload();
            }
            else {
                row.className = oldClassName;
            }
            
            
        }); 
        
        var copyDocsWindow;
        $('.CopyDocsToLoan').click(function() {
            var data = { 
                ID : $('.BatchEditGuids').val()
            };
            
            var result = gService.edoc.call('BatchStore',  data); 
            
            if( result.error ){
                alert( result.UserMessage ) ;
                return;
            }
            
            var body_url = <%= AspxTools.SafeUrl(Page.ResolveUrl("~/newlos/ElectronicDocs/CopyDocsToLoanFiles.aspx")) %>;
                        
            if ( !copyDocsWindow || copyDocsWindow.closed) {
                var name = "copy_docs_" + $('.WindowName').val();
                copyDocsWindow = window.open( body_url + "?loanid=" + ML.sLId + "&key=" + result.value.key, name, "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes,width=780,height=450");
            }
            copyDocsWindow.focus();
        });

        $('.CreateEditableCopies').click(function() {
            var data = {
                DocumentIds : $('.BatchEditGuids').val()
            };

            var result = gService.edoc.call('CreateEditableCopies', data); 

            if (result.error) {
                alert(result.UserMessage);

                if (JSON.parse(data.DocumentIds).length <= 1) {
                    return;
                }
            }

            reload();
        });

        $('.CreateEditableCopy').click(function() {
            var docId = $(this).closest('tr').attr('docid');
            var data = {
                DocumentIds : JSON.stringify([docId])
            };

            var result = gService.edoc.call('CreateEditableCopies', data); 

            if (result.error) {
                alert(result.UserMessage);
                return;
            }

            reload();
        });

        
        $('.actionLink').click(function(){
            //Although the docs don't give an explicit example, for each class that's provided
            //toggleClass will remove it if present, and add it if not present. In this case, that
            //means that since the elements start with .Closed, they'll switch between .Closed and .Opened
            //every time they're clicked.
            $(this).parent().next().toggleClass('Closed Opened');
            $(this).closest('tr').find('td').toggleClass('Opened');

            return false;   
        }); 
        
        $('a').mouseenter(function(){
            $(this).css('color', 'orange');
        });
        
        $('a').mouseleave(function(){
            $(this).css('color', 'blue');
        });        

        $(document).on('click', '#statusSortLink', function (e) {
            e.stopPropagation();
            e.preventDefault();
            
            $('#rblDocTypeOrder_ab').prop('checked', true); //! note this should be prop, but only in later versions of jquery.
            
            if( $("#ReceivedDocsTable tr").length < 3 ) {
                return false; 
            }
            $("#ReceivedDocsTable thead th").each(function(i, o){ 
                if( $.inArray(i, disabledSorts) != -1 ) {
                    return;
                }
                $(o).removeAttr('sortDisabled');
            });
            
            var currentSort = [[
                [DocColumnHeaderIndex.Status, isAsc],
                [DocColumnHeaderIndex.Folder, isAsc], 
                [DocColumnHeaderIndex.DocType, isAsc], 
                [DocColumnHeaderIndex.Borrower, isAsc], 
                [DocColumnHeaderIndex.Description, isAsc],
                [DocColumnHeaderIndex.LastModified, isAsc]
            ]];
            $("table").trigger("sorton",currentSort);
            isAsc = isAsc == 1 ? 0 : 1;
            return false;
        });

        $(document).on('click', '#folderSortLink', function (e) {
            e.stopPropagation();
            e.preventDefault();
            
            $('#rblDocTypeOrder_ab').prop('checked', true);
            
            if( $("#ReceivedDocsTable tr").length < 3 ) {
                return false; 
            }
            $("#ReceivedDocsTable thead th").each(function(i, o){ 
                if( $.inArray(i, disabledSorts) != -1 ) {
                    return;
                }
                $(o).removeAttr('sortDisabled');
            });
            
            var currentSort = [[
                [DocColumnHeaderIndex.Folder,isAsc], 
                [DocColumnHeaderIndex.DocType,isAsc], 
                [DocColumnHeaderIndex.Borrower,isAsc], 
                [DocColumnHeaderIndex.Description,isAsc]
            ]];
            $("table").trigger("sorton",currentSort);
            isAsc = isAsc == 1 ? 0 : 1;
            return false;
        });

        $(document).on('click', '#docTypeSortLink', function(e) {
            e.stopPropagation();
            e.preventDefault();
            
            $('#rblDocTypeOrder_ab').prop('checked', true); 
            
            if( $("#ReceivedDocsTable tr").length < 3 ) {
                return false; 
            }
            $("#ReceivedDocsTable thead th").each(function(i, o){ 
                if( $.inArray(i, disabledSorts) != -1 ) {
                    return;
                }
                $(o).removeAttr('sortDisabled');
            });
            
            var currentSort = [[
                [DocColumnHeaderIndex.DocType,isAsc], 
                [DocColumnHeaderIndex.Folder,isAsc], 
                [DocColumnHeaderIndex.Borrower,isAsc], 
                [DocColumnHeaderIndex.Description,isAsc]
            ]];
            $("table").trigger("sorton",currentSort);
            isAsc = isAsc == 1 ? 0 : 1;
            return false;
        }); 

        $(document).on('click', '#modDateSortLink', function(e) {
            e.stopPropagation();
            e.preventDefault();
            $(this).parent().removeAttr('sortDisabled');
            $('#ReceivedDocsTable').trigger("sorton", [[[DocColumnHeaderIndex.LastModified, isAsc]]]);
            isAsc = isAsc == 1 ? 0 : 1;
        }); 

        $(document).on('click', '#uploadedDateSortLink', function (e) {
            e.stopPropagation();
            e.preventDefault();
            $(this).parent().removeAttr('sortDisabled');
            $('#ReceivedDocsTable').trigger("sorton", [[[DocColumnHeaderIndex.UploadedDate, isAsc]]]); //uploaded date
            isAsc = isAsc == 1 ? 0 : 1;
        });

        $(document).on('click', '#uploadedBySortLink', function (e) {
            e.stopPropagation();
            e.preventDefault();
            $(this).parent().removeAttr('sortDisabled');
            $('#ReceivedDocsTable').trigger("sorton", [[[DocColumnHeaderIndex.UploadedBy, isAsc]]]); //uploaded by
            isAsc = isAsc == 1 ? 0 : 1;
        });

        $('#rblDocTypeOrder_so').click(function(e)
        {
            $('#ReceivedDocsTable').trigger('sorton',[[[DocColumnHeaderIndex.DocTypeOrder,false], [DocColumnHeaderIndex.LastModified, true]]]); // doctype order number, always descending
            setCookie("EdocsOrderDefaultView", "StackOrder");
        });
        $('#rblDocTypeOrder_ab').click(function(e)
        {
            $('#ReceivedDocsTable').trigger('sorton',[[[DocColumnHeaderIndex.Folder,false], [DocColumnHeaderIndex.DocType,true]]]); // doctype order number, always descending
            setCookie("EdocsOrderDefaultView", "");
        });

        $("#ReceivedDocsTable  thead th").click(function(e){
            this.sortDisabled = true; 
        });
        var tb=  $("#ReceivedDocsTable").on("sortEnd", function() {
            alternateClasses(this.tBodies[0].rows);
        });

        $('[id$="SendDocuSignEnvelope"]').click(function(event) {
            var checkedBoxes = $('input[id$="BatchEditCB"]:checked');
            var edocIds = $.map(checkedBoxes.siblings('[id$="EdocId"]').get(), function(element) { return element.value; });
            window.open(ML.VirtualRoot + '/newlos/Services/DocuSign/SubmitDocuSignEnvelope.aspx?docIds=' + edocIds.join() + '&loanid=' + ML.sLId, "_blank", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes");
        });

        $('.deleteLink').click(function(e){
            e.stopPropagation();
            e.preventDefault();
            
            var $row = $(this).parents('tr');
            var row = $row[0];
            var oldClassName = row.className; 
            row.className ='GridSelectedItem'; 
            var answer = confirm('Delete document?');
            if ( answer ) {
                var ob = { 
                    DocId : $row.attr('docid')
                };
            
                var results = gService.edoc.call('DeleteEDocument', ob); 
            
                if ( results.value.Success) {
                    $row.hide(); 
                    $row.attr("userdeleted", 'true');
                    $('.BatchEdit', row).prop("checked", false);
			        UpdateBatchEditGuids();
                    alternateClasses(tb[0].tBodies[0].rows);
                    //OPM 69366: Enable the restore button if we've just deleted something (saves a page refresh)
                    var RestoreBtn = document.getElementById('<%= AspxTools.ClientId( DeletedDocs ) %>' );
                    RestoreBtn.disabled = false;
                    return;
                }
                if ( results.error ) {
                    alert( results.UserMessage );               
                } else if ( typeof ( results.ErrorMessage ) !== 'undefined' )  {
                    alert( results.ErrorMessage );
                } else { 
                    alert( 'System error.' );
                }
            }
            row.className  = oldClassName; 
        });
        
        if( tb[0].tBodies[0].rows.length != 0 ){
            var arr = [
                DocColumnHeaderIndex.Annotation,
                DocColumnHeaderIndex.Icon,
                DocColumnHeaderIndex.ESignReady,
                DocColumnHeaderIndex.Status,
                DocColumnHeaderIndex.Action,
                DocColumnHeaderIndex.InternalComments,
                DocColumnHeaderIndex.Pages
            ];
            var headersValue = {};
            for (var i = 0; i < arr.length; ++i) {
                headersValue[arr[i]] = { sorter: false };
            }
            tb.tablesorter({
                headers: headersValue
            });

            if(compareCookie('EdocsOrderDefaultView', 'StackOrder')) {
                $('#ReceivedDocsTable').trigger('sorton',[[[DocColumnHeaderIndex.DocTypeOrder,false], [DocColumnHeaderIndex.LastModified, true]]]); // doctype order number, always descending
                $('#rblDocTypeOrder_so').prop('checked', true);
            } else {
                $('#ReceivedDocsTable').trigger('sorton',[[[DocColumnHeaderIndex.Folder,false], [DocColumnHeaderIndex.DocType,true]]]); // doctype order number, always descending
                $('#rblDocTypeOrder_ab').prop('checked', true);
            }
        }
        
        tb.find('a.retrylink').click(function(a){
            var tr = $(this).parents('tr');
            var id = tr.attr('docid');

            var data =  {
                DocumentId : id
            };  
            
            var eta = tr.find(".eta").show().text('Requesting conversion..');
            var error = tr.find('.ErrorStatusContainer').hide(); 
            var results = gService.edoc.call('ResetImageStatus', data);
            
            if( results.error ) {
                eta.hide();
                error.show();
                alert('Error requesting conversion');
                return false;
            }
            
            eta.text('Prepping editor - est. time remaining: ' + results.value.eta);
            isQueued = true;
            return false;
        
        });
        
        //On page load, if the window's hash value is a number, select it in the ddl and update the display based on that.
        if(window.location.hash != null)
        {
            //The hash comes out as #<number>, we don't want the # out in front.
            var selected = window.location.hash.substring(1,window.location.hash.length);
            var ddl = document.getElementById('<%= AspxTools.ClientId( Folders ) %>' );
            for(var i = 0; i < ddl.length; i++)
            {
                //If it's something that exists in the ddl, then select that and update
                if(ddl[i].value == selected)
                {
                    ddl.selectedIndex = i;
                    updateDisplay();
                    break;
                }
                //Otherwise ignore it.
            }
        }
        var rowsThatNeedUpdates = tb.find('div.processingStatus');
       
        function updateDocumentStatuses(doc){
            var data = $(doc).find('data');
            if( data.length > 0 ) {
                var processingDocs = JSON.parse(data.attr('processingDocs'));
                var processingDescriptions = JSON.parse(data.attr('processingDescriptions'));
                var errorDocs = JSON.parse(data.attr('errorDocs'));
                
                rowsThatNeedUpdates.each(function(){
                    var $this = $(this), docId = $this.parents('tr').attr('docid'); 
                    var error = $('.ErrorStatusContainer', this), eta = $('.eta', this); 
                    var anchor = $('.myeditlink', $this.parents('tr')[0]);
                    
                    var index = $.inArray( docId, processingDocs) ;
                    if( index > -1 ) {
                        error.hide();
                        eta.show();
                        eta.text('Prepping editor - est. time remaining: ' + processingDescriptions[index]);
                        return; 
                    }
                    index = $.inArray( docId, errorDocs) ;
                    if( index > -1 ) {
                        error.show(); 
                        eta.hide();
                        
                        return;
                    }
                    setDisabledAttr(anchor, false);
                    $this.hide();
                });
                
                isQueued = processingDocs.length > 0;
            }
        }
        
        var xml = buildXmlRequest({'slid':ML.sLId, 'EdocsIdsList': ML.EdocsIdsList}); 
        var isQueued = rowsThatNeedUpdates.length > 0; 
        function  getLoanEditorStatus()
        {
            callWebMethodAsync({
                type: 'POST', 
                url: gService.edoc.url + '?method=PollStatus',
                data : xml,
                contentType : 'text/xml',
                cache: false
            }).then(updateDocumentStatuses);
        }
        
        function doPoll()
        {
            if( isQueued ) {
                getLoanEditorStatus();
            }   
        }
        
        
        setInterval(doPoll, 30000);
        
        $('.BatchEditMaster').click(function()
        {
            var checked = $(this).prop("checked");
            $('#ReceivedDocsTable input[type="checkbox"].BatchEdit').filter(':visible').prop("checked", checked);
            UpdateBatchEditGuids();
        });
        
        $('.BatchEdit').click(function(){
            UpdateBatchEditGuids();
        });
        
        $('#ReceivedDocsTable').on("sortEnd", function(){
			UpdateBatchEditGuids();
		});
        
        UpdateBatchEditGuids();
        
    });
    
    
    function reload() {
        window.location.reload(); 
    }
    
    function UpdateBatchEditGuids()
    {
        var guids = [];
        var selectionContainsESignedDoc = false;
        var nonESignReadyDocs = [];
        $('#ReceivedDocsTable input[type="checkbox"].BatchEdit').filter(':visible').filter(':checked').next().each(function() {
            guids.push($(this).val());

            var $parentRow = $(this).closest('tr');

            if ($parentRow.find('a.CreateEditableCopy').length > 0) {
                selectionContainsESignedDoc = true;
            }

            if (!$parentRow.data('esignPrepped')) {
                var folderName = $.trim($parentRow.find('.FolderName').text());
                var docType = $.trim($parentRow.find('.DocType').text());
                var lastModified = $.trim($parentRow.find('.LastModified').text());
                nonESignReadyDocs.push( folderName + ' : ' + docType + ' ' + lastModified);
            }
        });
        
        $('.BatchEditGuids').val(JSON.stringify(guids));
        
        $('.OpenBatchEditor,.CopyDocsToLoan').prop("disabled", !guids.length);

        $sendDocuSignEnvelope = <%= AspxTools.JQuery(this.SendDocuSignEnvelope) %>;
        $sendDocuSignEnvelope.prop('disabled', guids.length <= 0 || nonESignReadyDocs.length > 0);
        if (nonESignReadyDocs.length > 0) {
            $sendDocuSignEnvelope.attr('title', nonESignReadyDocs.length + ' documents are not E-Sign prepped: \r\n' + nonESignReadyDocs.join('\r\n'));
        }
        else if (guids.length <= 0) {
            $sendDocuSignEnvelope.attr('title', 'No documents are selected.');
        }
        else
        {
            $sendDocuSignEnvelope.attr('title', '');
        }

        $('.CreateEditableCopies').prop("disabled", !selectionContainsESignedDoc);
    }
    
    function fnStartInit(event)
    {
            Modal.Hide();
            progressingDownload = false;
    }
   
</script>

    <script type="text/javascript">
    
    
        $(function($) {
            
            $(document).on('click', 'a.deleteStatus', function(e){
            
                e.preventDefault();
                
                var data = {
                    id : $(this).attr('mid')
                };
                
                var result = gService.edoc.call('DeleteDocMagicError', data);
                
                if( result.error ) {
                    alert(result.UserMessage);
                }
                else {
                    $(this).closest('tr').remove();
                    Modal.Hide();
                    window.clearTimeout(currentTimeout);
                    runforever();
                }
                
           
                
            });
            $('a.DocMagicDetail').click(function(e){
                e.preventDefault();
                Modal.ShowPopup($(this).attr('dataid'), null, e);
                return false;
            });
            $('a.CloseLink').click(function(e){
                Modal.Hide();
            });

            $('a.migratedocs').click(function(e){
                if (hasDisabledAttr(this))
                {
                    return;
                }

                var data = {
                    sLId : ML.sLId
                };
                
                var result = gService.edoc.call('MigrateDocs', data);
                $('input.migratedocs').prop('disabled', true);
                $('span.migrateclickmsg').hide();
            });
            
 
            var pdfIds = [];

            /*
             * Sends a request to retrieve barcode scanning status info and 
             * updates the associated UI. If some documents have completed,
             * refresh the page.
             *
             * Returns the number of seconds to wait until the next refresh.
             * A return value of 0 indicates the polling should cease.
             */
            function UpdateDocMagicBar() {  
                  var refreshInterval = 0;
                  var data = { 
                    sLId : ML.sLId
                  };

                  var result = gService.edoc.call('DocMagicStatus',  data);
                  
                  if( result.error ) {
                    alert( result.UserMessage ) ;
                    return 0;
                  }
                  
                  refreshInterval = result.value.DocListBarcodeStatusRefreshIntervalSeconds;
                  var step1Data = JSON.parse(result.value.InQStatus); 
                  var step2Data = JSON.parse(result.value.ConvertStatus); 
                  var step3Data = JSON.parse(result.value.ScanStatus); 
                  var step4Data = JSON.parse(result.value.SplitStatus); 
                  var step5Data = JSON.parse(result.value.ErrorStatus); 
                  
                  var steps = [
                    { data : step1Data, id : "#step1details", tableid : "#table1", countid : "#Count1", liid : "#step1li" } ,
                    { data : step2Data, id : "#step2details", tableid : "#table2", countid : "#Count2", liid : "#step2li" } ,
                    { data : step3Data, id : "#step3details", tableid : "#table3", countid : "#Count3", liid : "#step3li" } ,
                    { data : step4Data, id : "#step4details", tableid : "#table4", countid : "#Count4", liid : "#step4li" } ,
                    { data : step5Data, id : "#step5details", tableid : "#table5", countid : "#Count5", liid : "#step5li" } 
                  ];
                  
                  
                  var showBar = false;
                  for( var i = 0; i < steps.length; i++) {
                    var item = steps[i];
                    var showLink =  item.data.length != 0; 
                    $(item.id).toggle(showLink);
                    if( showLink ) {
                        showBar = true;
                    }
                    var msg = item.data.length + " Document";
                    if( item.data.length != 1 ) { 
                        msg += "s"; 
                    }
                    msg += " ";
                    $(item.countid).text(msg);
                    $(item.liid).toggle(showLink);
                    
                    var h = hypescriptDom;
                    $(item.tableid).children('tbody').empty().append($.map(item.data, function(row) {
                        return (
                            h("tr", {className: cssClass}, [
                                h("td", {}, row.Application),
                                h("td", {}, row.Description),
                                h("td", {}, row.InternalComments),
                                h("td", {}, row.Pages),
                                h("td", {}, row.UploadedOn)
                            ].concat(i !== 4 ? [] : [
                                h("td", {}, row.StatusNotes),
                                h("td", {}, h("a", {href:"#", className:"deleteStatus", attrs:{mid:row.Id}}, "clear"))
                            ]))
                        );
                    }));
                  }
                  
                  var currentIds = $.map(item.data, function(row){return row.Id});
                  var someCompleted = false;
                  for( var i = 0; i < pdfIds.length; i++ ) {
                    if( $.inArray(pdfIds[i], currentIds) === -1 ) {
                        someCompleted = true;
                        break;
                    }
                  }
                  
                  pdfIds = currentIds;
                  $('div.DocMagicBar').toggle(showBar);
                if( someCompleted ) {
                    reload();
                }

                return refreshInterval;
            }
            var currentTimeout = null;
            function runforever() {
                if (!ML.isBarcodeScannerEnabled) {
                    return;
                }

                var refreshIntervalInSeconds = UpdateDocMagicBar();
                if (refreshIntervalInSeconds > 0) {
                    currentTimeout = window.setTimeout(runforever, refreshIntervalInSeconds * 1000);
                }
            }         
            
            // Use the default value for the refresh interval here. It may be
            // updated during polling.
            setTimeout(runforever, ML.docListBarcodeStatusRefreshIntervalSeconds * 1000);
        });
    </script>
<asp:HiddenField ID="sortedColumn" runat="server" Value="folder" />
<asp:HiddenField ID="sortOrder" runat="server" Value="0" />



<div id="DownloadProgress" style="display: none; background-color: Gray; position:absolute"> 
    <table>
        <tbody>
            <tr>
                <td>
                    Please wait while we prepare your document
                </td>
            </tr>
            <tr>
                <td>
                    <img src="../../images/status.gif" alt="Loading" />
                </td>
            </tr>
        </tbody>
    </table>
</div>

<label runat="server">
Search: <input type="text" id="searchBox" />
</label>
<label runat="server" id="FolderDDLLabel" style="margin-top:25px;">
Show documents from&nbsp;&nbsp;<asp:DropDownList NotForEdit="NotForEdit" onchange="updateDisplay()" runat="server" ID="Folders"></asp:DropDownList>
</label>
View:
<input id="rblDocTypeOrder_so" type="radio" name="rblDocTypeOrder" checked="checked" />
stack order

<input id="rblDocTypeOrder_ab" type="radio" name="rblDocTypeOrder" />
alphabetical

<asp:Panel runat="server" style="background-color: yellow; border: 1px solid black; font-weight:bold; padding:5px; margin: 5px 0px;" ID="MigrationPanel">
This loan file contains documents that are not accessible in the eDoc editor.  <span class="migrateclickmsg">Click <a href="#" class="migratedocs" >here</a> to start preparing the documents.</span>  This message will disappear when all documents are ready.
</asp:Panel>

<asp:Panel runat="server" ID="DocMagicStatusBar" >
    <div class="DocMagicBar">
        <ul>
            <li id="step1li"><span id="Count1"></span>in Scanning Queue <a href="#"  dataid="section1" id="step1details" class="DocMagicDetail">details </a></li>
            <li id="step2li">Preparing <span id="Count2"></span>for Scanning <a href="#"  dataid="section2" id="step2details" class="DocMagicDetail">details </a></li>
            <li id="step3li">Scanning Barcodes for <span id="Count3"></span><a href="#"  dataid="section3" id="step3details" class="DocMagicDetail">details </a></li>
            <li id="step4li">Splitting Documents <a href="#"  dataid="section4" id="step4details" class="DocMagicDetail">details </a></li>
            <li id="step5li">Error Processing <span id="Count5" ></span><a id="step5details" href="#"  dataid="section5" class="DocMagicDetail">
                details </a></li>
        </ul>
    </div>
    
    
    <div id="section1" class="DocMagicTable">
        <table id="table1" class="FormTable">
            <thead>
                <tr class="GridHeader">
                    <td>Borrower</td>
                    <td>Description</td>
                    <td>Internal Comments</td>
                    <td>Pages</td>
                    <td>Uploaded</td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <a href="#" class="CloseLink" >close</a>
    </div>
    <div id="section2" class="DocMagicTable">
            <table id="table2" class="FormTable">
            <thead>
                <tr class="GridHeader">
                    <td>Borrower</td>
                    <td>Description</td>
                    <td>Internal Comments</td>
                    <td>Pages</td>
                    <td>Uploaded</td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <a href="#" class="CloseLink" >close</a>
        
    </div>
    <div id="section3" class="DocMagicTable">
            <table id="table3" class="FormTable">
            <thead class="GridHeader">
                <tr>
                    <td>Borrower</td>
                    <td>Description</td>
                    <td>Internal Comments</td>
                    <td>Pages</td>
                    <td>Uploaded</td>
                </tr>
            </thead>
            <tbody>
            </tbody>
            
        </table>
        <a href="#" class="CloseLink" >close</a>
        
    </div>
    <div id="section4"  class="DocMagicTable">
            <span id="Count4"></span> in ScanningQueue
            <table id="table4" class="FormTable" > 
            <thead>
                <tr class="GridHeader">
                    <td>Borrower</td>
                    <td>Description</td>
                    <td>Internal Comments</td>
                    <td>Pages</td>
                    <td>Uploaded</td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <a href="#" class="CloseLink" >close</a>
    </div>
    <div id="section5" class="DocMagicTable">
            <table id="table5" class="FormTable">
            <thead>
                <tr class="GridHeader">
                    <td>Borrower</td>
                    <td>Description</td>
                    <td>Internal Comments</td>
                    <td>Pages</td>
                    <td>Uploaded</td>
                    <td>Error Details</td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <a href="#" class="CloseLink" >close</a>
    </div>
    
</asp:Panel>

<asp:Repeater runat="server" ID="Documents" 
    OnItemDataBound="Documents_OnItemDataBound" >
    <HeaderTemplate>
        <table id="ReceivedDocsTable" class="FormTable" cellpadding="2" cellspacing="1"  >
        <thead>
           <tr class="GridHeader">
                <th style="width:30px" runat="server" align="left" id="AnnotationColumn"><input type="checkbox" id="BatchEditMaster" class="BatchEditMaster" runat="server"/></th>
                <th runat="server" id="IconColumnHeader"></th>
                <th runat="server" id="ESignReadyColumnHeader" nowrap>E-Sign<br />Ready</th>
                <th style="width:90px" id="StatusColumnHeader"><a id="statusSortLink">Status</a></th>
                <th style="width:90px" id="ActionColumnHeader">&nbsp;</th>
                <th style="width:90px" id="FolderColumnHeader"><a id="folderSortLink">Folder</a></th>
                <th style="width:10px" id="DocTypeColumnHeader"><a id="docTypeSortLink">Doc Type</a></th>
                <th style="width:140px" id="BorrowerColumnHeader">Borrower</th>
                <th style="width:140px" id="DescriptionColumnHeader">Description</th>
                <th style="width:140px" id="InternalCommentsColumnHeader">Internal Comments</th>
                <th style="width:100px" id="AssociatedConditionsColumnHeader">Associated Conditions</th>
                <th style="width:40px" id="PagesColumnHeader">Pages</th>
                <th style="width:120px" id="LastModifiedColumnHeader"><a id="modDateSortLink">Last Modified</a></th>
                <th style="display:none" id="DocTypeOrderNumberColumnHeader">DocTypeOrderNumber</th>
                <th style="width:120px" id="UploadedDateColumnHeader"><a id="uploadedDateSortLink">Uploaded Date</a></th>
                <th style="width:140px" id="UploadedByColumnHeader"><a id="uploadedBySortLink">Uploaded By</a></th>
            </tr>
        </thead>
        <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr class="GridItem" runat="server" id="Row">
            <td align="left" runat="server" id="AnnotationColumn">
                <input type="checkbox" class="BatchEdit" id="BatchEditCB" runat="server"/>
                <input type='hidden' id="EdocId" runat="server"/>

                <ml:EncodedLiteral runat="server" ID="ContentType"></ml:EncodedLiteral>
            </td>
            <td runat="server" id="IconColumnCell"></td>
            <td runat="server" id="ESignReadyColumnCell" class="align-center"></td>
            <td align="center" runat="server" id="StatusColumn">
                <div class="processingStatus" runat="server"  id="imgStatus" >
                    <span class="eta" id="eta" runat="server">Retrieving ETA...</span>
                    <div class="ErrorStatusContainer" runat="server" id="imgError">
                        Prepping editor failed. Click <a href="#" class="retrylink">here</a> to retry.
                    </div>
                </div>
                <asp:PlaceHolder runat="server" ID="Status"></asp:PlaceHolder>
            </td>
            <td align="left" valign="top">
                <div style="DISPLAY: block; COLOR: blue;" >
				    <a href="#" class="actionLink">action...</a>
				</div>
                <div class="Div0 Closed" style="padding-left: 10px;">
                        <a runat="server" id="View" style="display:block;"
                            title="View PDF">view pdf</a>

                        <a runat="server" id="Edit"  class="myeditlink"
                            title="Edit Document" style="display:block;cursor:hand; text-decoration: underline" >open editor</a>

                        <a runat="server" id="Audit" style="display:block;"
                            title="View Audit History">audit history</a>

                        <a runat="server" id="Delete" href="#" style="display:block;"
                            title="Delete Document" class="deleteLink">delete</a>

                        <a runat="server" id="viewXml" visible="false"
                            title="View xml">view xml </a>

                        <a runat="server" id="CreateEditableCopy" class="CreateEditableCopy" visible="false"
                            title="Create Editable Copy">create editable copy</a>

                </div>
            </td>
            <td class="FolderName">
                <ml:EncodedLiteral runat="server" ID="FolderNm"></ml:EncodedLiteral>
            </td>
            <td class="DocType">
                <ml:EncodedLiteral runat="server" ID="DocType"></ml:EncodedLiteral>
            </td>
            <td valign="top">
                <ml:EncodedLiteral runat="server" id="App"></ml:EncodedLiteral>
            </td>
            <td valign="top">
                <ml:EncodedLiteral runat="server" ID="PublicDescription"></ml:EncodedLiteral>
            </td>

            <td style="width:200px; overflow: hidden" valign="top">
                <ml:EncodedLiteral runat="server" ID="InternalDescription"></ml:EncodedLiteral>
            </td>
            <td>
                    <asp:Repeater runat="server" id="ConditionAssociations" OnItemDataBound="ConditionAssociations_OnItemDataBound">
                        <ItemTemplate>
                            <div runat="server" id="Info" class="info"></div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <a href="#" runat="server" id="ClearConditions"  visible="false" class="clearassoc">clear associations</a>
            </td>
            <td align="right" valign="top">
                <ml:EncodedLiteral runat="server" ID="PageCount"></ml:EncodedLiteral>
            </td>
            <td class="LastModified">
                <ml:EncodedLiteral runat="server" ID="LastModified"></ml:EncodedLiteral>
            </td>
            <td style="display:none">
                <ml:EncodedLiteral runat="server" ID="DocTypeOrderNumber"></ml:EncodedLiteral>
            </td>
            <td valign="top">
                <ml:EncodedLiteral runat="server" ID="UploadedDate"></ml:EncodedLiteral>
            </td>
            <td valign="top">
                <ml:EncodedLiteral runat="server" ID="UploadedBy"></ml:EncodedLiteral>
            </td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </tbody> 
        </table>
    </FooterTemplate>
</asp:Repeater>
<br />
<asp:Button runat="server" ID="DeletedDocs" Text="Restore deleted docs..." /> 
<input type="button" runat="server" ID="OpenBatchEditor" class="OpenBatchEditor" value="Batch edit selected docs"   />
<input type="button" runat="server" id="CopyDocsToLoan" class="CopyDocsToLoan" value="Copy selected docs to loan file" />
<input type="button" runat="server" id="CreateEditableCopies" class="CreateEditableCopies" value="Create editable copies" />
<input type="button" runat="server" id="ViewDocuSignEnvelopesBtn" class="ViewDocuSignEnvelopesBtn" value="View DocuSign Statuses" />
<input type="button" runat="server" id="SendDocuSignEnvelope" value="Send DocuSign Envelope"/>
<input type="hidden" runat="server" id="BatchEditorUrl" class="BatchUrl"  />
<input type="hidden" runat="server" id="WindowName" class="WindowName" />
<input type="hidden" runat="server" id="BatchEditGuids" class="BatchEditGuids" />

<asp:Panel runat="server" ID="IconLegend" CssClass="IconLegend">
    <table>
        <tr>
            <td><img src="~/images/edocs/SigIcon.png" runat="server" /></td>
            <td>- Document has been ESigned</td>
            <td class="secondColumn"><img src="~/images/edocs/UCDIcon.png" runat="server" /></td>
            <td>- UCD Document</td>
        </tr>
        <tr>
            <td><img src="~/images/edocs/XMLIcon.png" runat="server" /></td>
            <td>- XML Document</td>
            <td class="secondColumn"><img src="~/images/edocs/NoteIcon2.png" runat="server" /></td>
            <td>- Document contains notes and notations</td>
        </tr>
    </table>
</asp:Panel>
