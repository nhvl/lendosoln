﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SplitEDoc.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.SplitEDoc" %>

<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=8">
    <title></title>
    <style>
        .padding
        {
            padding-left: 10px;
        }
        .page_textbox
        {
            width: 30px;
            text-align: center;
        }
        .ErrorItem
        {
            font-size: 11px;
            font-family: Arial, Helvetica, sans-serif;
            color: Black;
            background-color: #AA0000;
        }
        .DocTypePicker
        {
            text-align: center;
        }
        .Template
        {
            display: none;
        }
        td.conditions
        {
            text-align: right;
        }
        
        td.conditions a
        {
            height: 10%;
        }
        
        textarea, select.conditions
        {
            width: 100%;
            height: 60px;
        }
        
        select.conditions
        {
            width: 200px;
            position: relative;
            top: 5px;
        }
        
        a.associate
        {
            position:relative;
            top: 2px;
        }
    </style>
</head>
<body bgcolor="gainsboro" onunload="getModalArgs().callback()">

    <script type="text/javascript">

        var g_iTotalPages = 0;
        var index = 0;
        var validPage = true;
        var onRangeChange;
        var onAddFinish;
        
        var parentEvents = {
            "AddNewRange": function(selectedOnParent){
                mergeSelectedWithCurrent(getSelectedRanges(true), selectedOnParent[0]);
                onAddFinish();
            }
        };
        
        function rangesOverlap(A, B)
        {
            return inRange(A.Start, B) || inRange(A.End, B) ||
                   inRange(B.Start, A) || inRange(B.End, A);
        }
        
        function inRange(test, range)
        {
            return test >= range.Start && test <= range.End;
        }
        
        RangeCompareResult = {
            //All but the largest B in this category needs to be deleted
            BWithinA: 1,
            
            //B needs to be split into three rows - before A, A, after A
            //Some of these might be empty, and the A parts might overlap.
            AWithinB: 2,
            AOverlapsB: 3,
            
            //Ignore it, since it doesn't touch the new range
            Disjoint: 4, 
            
            //Don't do anything here, since we already have it
            Equal: 5
        }
        
        function rangeCompare(A, B){
            if(!rangesOverlap(A, B)) return RangeCompareResult.Disjoint;
            if(A.Start == B.Start && A.End == B.End) return RangeCompareResult.Equal;
            
            if(B.Start >= A.Start && B.End <= A.End) return RangeCompareResult.BWithinA;
            if(A.Start >= B.Start && A.End <= B.End) return RangeCompareResult.AWithinB;
            
            return RangeCompareResult.AOverlapsB;
        }
        
        function categorizeRanges(ranges, test)
        {
            var categories = {};
            $.each(RangeCompareResult, function(name, value){
                categories[value] = [];
            });
            
            var len = ranges.length;
            for(var i = 0; i < len; i++)
            {
                var current = ranges[i];
                var result = rangeCompare(test, current.Range);
                
                categories[result].push(current);
            }
            
            $.each(categories, function(idx, toSort){
                categories[idx] = toSort.sort(function(a, b) { 
                    return b.Range.Count - a.Range.Count;
                });
            });
            
            return categories;
        }
        
        function mergeSelectedWithCurrent(currentIndexes, newRange){
            if(!newRange) return;
            
            currentIndexes = currentIndexes.sort(function(a, b){
                return a.Range.Start - b.Range.Start;
            });
            
            newRange = addEnd(newRange);
            var categorizedRanges = categorizeRanges(currentIndexes, newRange);
            debugger;
            //Handle the case where up to two ranges border us
            var borders = categorizedRanges[RangeCompareResult.AOverlapsB];
            enforceBorders(borders, newRange);
            
            //Handle the case where we're enclosed in another range (there can only be one of these)
            var enclosing = categorizedRanges[RangeCompareResult.AWithinB];
            breakEnclosing(enclosing[0], newRange);
            
            //Handle the case where there's other ranges within us
            var within = categorizedRanges[RangeCompareResult.BWithinA];
            absorbRanges(within, newRange);
            
            //Handle the case where we're equal to an existing range (don't do anything else)
            if(categorizedRanges[RangeCompareResult.Equal].length == 1) return;
            
            var row = addRow();
            setStartEndTbs(row, newRange);
        }
        
        function breakEnclosing(enclosing, newRange)
        {
            if(!enclosing) return;
            
            var enclosingLow = enclosing;
            //Make a copy for the high side
            var enclosingHi = $.extend(true, {}, enclosing);
            //And then make a new row for it since it won't have one.
            var rowId = $(addRow()).attr('id');
            enclosingHi.Index = rowId;
            
            enclosingLow.Range.End = newRange.Start - 1;
            enclosingHi.Range.Start = newRange.End + 1;
            
            //Then it's just a matter of enforcing borders
            enforceBorders([enclosingLow, enclosingHi], newRange);
        }
        
        
        function enforceBorders(borders, newRange)
        {
            var len = borders.length;
            if(!len) return;
            
            for(var i = 0; i < len; i++)
            {   
                var neighbor = borders[i].Range;
                if(inRange(neighbor.Start, newRange))
                {
                    neighbor.Start = newRange.End + 1;
                }
                if(inRange(neighbor.End, newRange))
                {
                    neighbor.End = newRange.Start - 1;
                }
                
                neighbor.Count = recalcCount(neighbor);
                
                setStartEndTbs($('#' + borders[i].Index), neighbor);
            }
        }
        
        function recalcCount(range)
        {
            return range.End - range.Start + 1;
        }
        
        function absorbRanges(within, newRange)
        {
            var len = within.length;
            for(var i = 0; i < len; i++){
                var current = within[i];
                removeFromList($('#'+ current.Index)[0]);
            }
        }
        
        function setStartEndTbs(row, range)
        {
            range = convertToDisplay(range);
            $(row).find('.start').val(range.Start);
            $(row).find('.end').val(range.End);
        }
        
        function convertToDisplay(range)
        {   
            range = addEnd(range);
            range.Start++;
            range.End++;
            return range;
        }
        
        
        function getAssociatedRanges(source, newRange)
        {
            var associations = [];
            var len = source.length;
            for(var i = 0; i < len; i++){
                var current = source[i];
                if(!rangesOverlap(current.Range, newRange)) continue;
                
                associations.push(current);
            }
            
            return associations
        }
        
        function addEnd(range)
        {
            range.End = range.Start + range.Count - 1;
            return range;
        }
        
        function trigger(eventName, eventParamJSON)
        {
            var param = JSON.parse(eventParamJSON);
            parentEvents[eventName](param);
        }

        function __init() {
            // __init (two underscores) is not to be confused with _init (one underscore), since we inject javascript to run _init automatically
            // We don't want this to run more than we need it to
            
            resize(1000, 365);
            
            
            var arg = getModalArgs();
            if (!arg){
                 return;
            }

            var extraParams = window.opener.GetChildWindowArgsRaw().ExtraParameters;

            g_iTotalPages = arg.TotalPages;
            onRangeChange = extraParams.OnRangeChange || function(){};
            onAddFinish = extraParams.OnAddFinish || function(){};
            
            var selectedRanges = arg.SelectedRanges || {};
            var len = selectedRanges.length;
            
            if(len == 0){
                addRow();
                return;
            }

            for (var i = 0; i < len; i++) {
                var row = addRow();
                var start = selectedRanges[i].Start + 1;
                var end = start + selectedRanges[i].Count - 1;
                document.getElementById('start' + i).value = start;
                document.getElementById('end' + i).value = end;
            }
        }
        function split() {
            if (validatePage()) {
                var arg = window.dialogArguments || {};
                arg.result = serializePage();
                arg.OK = true;
                onClosePopup(arg);
            }
        }

        function serializePage() {
            var items = [];
            for (var i = 0; i < index; i++) {
                if (document.getElementById('start' + i) == null) {
                    continue;
                }

                items.push({
                    StartPage: AdjustmentTable.__IE8NullFix(document.getElementById('start' + i).value),
                    EndPage: AdjustmentTable.__IE8NullFix(document.getElementById('end' + i).value),
                    DocType: AdjustmentTable.__IE8NullFix(document.getElementById('selectedDocType' + i).value),
                    Description: AdjustmentTable.__IE8NullFix(document.getElementById('description' + i).value),
                    Comments: AdjustmentTable.__IE8NullFix(document.getElementById('comments' + i).value),
                    Status : document.getElementById('status' + i).value,
                    AppId: $('#application' + i).val(),
                    Conditions: getConditionAssociations(i),
                    DoctypeDescription: document.getElementById("docTypeDescription" + i).innerText
                });
            }
            return JSON.stringify(items);
        }
        
        function getSelectedRanges(includeRowIdx)
        {
            var ranges = [];
            for(var i = 0; i < index; i++)
            {
                if (document.getElementById('start' + i) == null) {
                    continue;
                }
                
                var start = +AdjustmentTable.__IE8NullFix(document.getElementById('start' + i).value);
                var end = +AdjustmentTable.__IE8NullFix(document.getElementById('end' + i).value);
                if(!start || !end) continue;
                start -= 1;
                if(start < 0 || start > g_iTotalPages || end < 1 || end > g_iTotalPages || end < start) continue;
                
                
                var count = end - start;
                
                var result = {
                    Start: +start,
                    Count: +count
                };
                
                if(!includeRowIdx) 
                {
                    ranges.push(result);
                }
                else 
                {
                    ranges.push({
                        Index: i,
                        Range: addEnd(result)
                    });
                }
            }
            
            return ranges;
        }
        
        function getConditionAssociations(idx){
            var ret = [];
            $('#conditions' + idx + ' option').each(function(){
                ret.push($(this).val());
            });
            
            return ret;
        }

        function validatePage() {
            var noInvalidRange = true;
            var noUnselectedDocTypes = true;
            var noOverlap = true;
            for (var i = 0; i < index; i++) {
                if (document.getElementById('start' + i) == null) {
                    continue;
                }
                var errorOnThisPage = false;
                var start = parseInt(document.getElementById('start' + i).value);
                var end = parseInt(document.getElementById('end' + i).value);
                var docPickerValue = parseInt(document.getElementById('selectedDocType' + i).value);
                var thisEntryNoInvalidRange = true;
                var thisEntryNoUnselectedDocTypes = true;
                var thisEntryNoOverlap = true;


                if (!isValidPage(start) || !isValidPage(end)) {
                    thisEntryNoInvalidRange = false;
                }
                if (start > end) {
                    thisEntryNoInvalidRange = false;
                }
                if (docPickerValue == -1) {
                    thisEntryNoUnselectedDocTypes = false;
                }

                for (var j = 0; j < index; j++) {
                    if (document.getElementById('start' + j) == null) {
                        continue;
                    }
                    if (j == i) {
                        continue;
                    }

                    var currentStart = parseInt(document.getElementById('start' + j).value);
                    var currentEnd = parseInt(document.getElementById('end' + j).value);

                    if ((start >= currentStart) && (start <= currentEnd) || (end >= currentStart) && (end <= currentEnd)
            || (currentStart >= start) && (currentStart <= end) || (currentEnd >= start) && (currentEnd <= end)) {
                        thisEntryNoOverlap = false;
                    }
                }

                //remove the error class if there were no errors, or add it if there were.
                var elem = document.getElementById(i);
                if (thisEntryNoOverlap && thisEntryNoUnselectedDocTypes && thisEntryNoInvalidRange) {
                    elem.className = elem.className.replace(/- ErrorItem/, '');
                }
                else {
                    //This is a neat trick - it disables whatever class was here before without removing it
                    //e.g, class="GridItem" turns in to class="GridItem- ErrorItem", 
                    //and GridItem- doesn't exist.
                    elem.className += '- ErrorItem';
                }

                noOverlap = noOverlap && thisEntryNoOverlap;
                noInvalidRange = noInvalidRange && thisEntryNoInvalidRange;
                noUnselectedDocTypes = noUnselectedDocTypes && thisEntryNoUnselectedDocTypes;
            }

            if (validPage) {
                var errorMsg = '';
                if (!noInvalidRange)
                    errorMsg += 'Invalid Page Ranges.\n';
                if (!noUnselectedDocTypes)
                    errorMsg += 'No Doc Type Selected.\n';
                if (!noOverlap)
                    errorMsg += 'Overlapping Page Ranges.\n';
                if (errorMsg != '')
                    alert(errorMsg);
            }

            validPage = noInvalidRange && noUnselectedDocTypes && noOverlap;
            document.getElementById('m_okay').disabled = !validPage;
            setDisabledAttr(document.getElementById('m_addLink'), !validPage);
            return validPage;
        }
        function onCancel() {
            var arg = window.dialogArguments || {};
            arg.OK = false;
            onClosePopup(arg);
        }
        function isValidPage(pg) {
            var o = parseInt(pg);
            if (isNaN(o) || o.toString() != pg) {
                return false;
            }
            if (o <= 0 || o > g_iTotalPages) {
                return false;
            }
            return true;
        }

        function addRow() {
            if (!validPage) {
                return;
            }

            var tBody = document.getElementById('rangeTable');
            var row = document.createElement('tr');
            row.setAttribute("id", index);
            var cell = document.createElement('td');
            $(cell).addClass('range');
            cell.setAttribute("noWrap", true);
            cell.innerHTML = '<input class="start" id="start' + index + '" style="width:25px" onchange="setClass();" /> to <input class="end" id="end' + index + '" style="width:25px" onchange="setClass();" /> of ' + g_iTotalPages;
            row.appendChild(cell);

            cell = document.createElement('td');
            $(cell).addClass('DocTypePicker');
            cell.appendChild(createDocTypePicker(index));
            row.appendChild(cell);

            cell = document.createElement('td');
            $('#ApplicationDDLTemplate').clone().attr('id', 'application' + index).removeClass('Template').appendTo(cell);
            row.appendChild(cell);
            
            cell = document.createElement('td');
            addStatusDdl(index,cell);
            row.appendChild(cell);
            

            cell = document.createElement('td');
            $(cell).addClass('description');
            cell.innerHTML = '<textarea id="description' + index + '" onkeyup="textLimit(this, 200);"></textarea>';
            row.appendChild(cell);

            cell = document.createElement('td');
            $(cell).addClass('comments');
            cell.innerHTML = '<textarea id="comments' + index + '" onkeyup="textLimit(this, 200);"></textarea>';
            row.appendChild(cell);

            cell = document.createElement('td');
            $(cell).addClass('conditions');
            cell.innerHTML = '<div><select class="conditions" multiple="multiple" id="conditions' + index + '"></select></div><a href="#" class="associate" id="associate' + index + '">Associate with conditions </a>';
            row.appendChild(cell);
            
            cell = document.createElement('td');
            cell.innerHTML = '<a href="javascript:void(0);" onclick="removeFromList(this.parentNode.parentNode);">delete</a>';
            row.appendChild(cell);

            tBody.appendChild(row);
            index++;
            setClass();
            document.getElementById('m_dataDiv').scrollTop = row.offsetTop;
            
            return row;
        }
        
        function addStatusDdl(num, cell){
            var statuses = [ 
                {t : ' ', i : 0 }, 
                { t: 'Screened', i : 4 },
                { t: 'Protected', i : 2},
                { t: 'Rejected', i : 3 },
                { t: 'Obsolete', i : 1}
            ]; 
            
            if ($('#FilterApprovalRejection').val() == 'true'){
                statuses.splice(2,2);
            }
            
            var select = document.createElement('select'); 
            select.className = 'statusDdl';
            cell.appendChild(select);
            select.setAttribute('id', 'status' + num);
            for (var i = 0; i < statuses.length; i++){
                var m = statuses[i];
                select.options[i] = new Option(m.t, m.i); 
            }
        }        
        
        function removeFromList(row) {
            tBody = document.getElementById('rangeTable');
            tBody.deleteRow(row.rowIndex - 1);
            setClass();
        }

        function textLimit(field, maxlen) {
            if (field.value.length > maxlen) {
                field.value = field.value.substring(0, maxlen);
            }
        }

        // This is an incredibly gutted version of the doc type picker control (DocTypePickerControl.ascx).
        // I would have much preferred to use the control, but I'm not sure how to integrate it with the row
        // manipulation endemic to this page. --MP, case 65393
        function createDocTypePicker(index) {
            // By the way, setAttribute doesn't work in IE7.
            var $span = $('<span>');

            var $label = $('<div>');
            $label.attr('id', 'docTypeDescription' + index);
            $label.text("No Doc Type Selected");

            $span.append($label);

            var $text0 = $('<span>');
            $text0.text(' [ ');
            $span.append($text0);

            var $link = $('<a>');
            $link.attr('href', '#');
            $link.click(function() { DocType_showDialog(index); });
            $link.text('select Doc Type');
            $span.append($link);

            var $text1 = $('<span>');
            $text1.text(' ]');
            $span.append($text1);

            var $selectedDocType = $('<input type="text">');
            $selectedDocType.attr('id', "selectedDocType" + index);
            $selectedDocType.css('display', 'none');
            $selectedDocType.val('-1');
            $span.append($selectedDocType);

            return $span[0];
        }

        var docTypePickerLink = "/newlos/ElectronicDocs/DocTypePicker.aspx";
        function DocType_showDialog(index) {
            var queryString = "";
            showModal(docTypePickerLink + queryString, null, null, null, function(result){
                if (result && result.docTypeId && result.docTypeName) {
                    var selectedDocType = document.getElementById('selectedDocType' + index);
                    var docTypeDescription = document.getElementById('docTypeDescription' + index);
                    selectedDocType.value = result.docTypeId;
                    docTypeDescription.textContent = result.docTypeName;
                    if (!validPage) {
                        validatePage();
                    }
                }
            },{ hideCloseButton: true });
            return;
        }

        function setClass() {
            var tBody = document.getElementById('rangeTable');
            for (var i = 0; i < tBody.rows.length; i++) {
                var row = tBody.getElementsByTagName("tr")[i];
                if (i % 2 == 1)
                    row.className = 'GridAlternatingItem';
                else
                    row.className = 'GridItem';
            }
            if (!validPage)
                validatePage();
        }    
        
        function OnConditionAssociationSave(){
            //Placeholder, gets filled in by jquery
        }
        
        function GetSerializedConditionSettings(){
            //Placeholder, gets filled in by jquery
        }
        
        jQuery(function($){
            var $currentAssociationBox;
            $("#SplitTable").on('click', '.associate', function(){
                $currentAssociationBox = $(this).closest('td').find('select.conditions');
                var LoanId = ML.sLId;
                var url = <%= AspxTools.SafeUrl(Page.ResolveUrl("~/newlos/ElectronicDocs/EditConditionAssociation.aspx")) %> + "?LoanId=" + LoanId;
                var w = window.open(url, "EditConditionAssociations", "width=500,height=715,resizable=yes,scrollbars=yes,status=yes");
                w.focus();
            }).on('change', '.range .start, .range .end', function(){
                var temp = getSelectedRanges();
                if(temp.length) onRangeChange(JSON.stringify(temp));
            });
            
            OnConditionAssociationSave = function(tasks){
                $currentAssociationBox.empty();
                
                var newOptions = [];
                newOptions.push('<option value="');
                $.each(tasks, function(id, subject){
                    newOptions.push(id);
                    newOptions.push('">');
                    newOptions.push(subject);
                    newOptions.push('</option><option value="');
                });
                
                newOptions.pop();
                newOptions.push('</option>');
                
                $currentAssociationBox.append(newOptions.join(''));
            };
            
            function getConditionAssociations($current){
                var ret = [];
                $('option', $current).each(function(){
                    ret.push($(this).val());
                });
                
                return ret;
            }
            
            GetSerializedConditionSettings = function(){
                var assoc = getConditionAssociations($currentAssociationBox);
                return JSON.stringify(assoc);
            }
           
            if(window.opener && window.opener.SetupModelessEnvironment) {
                window.opener.SetupModelessEnvironment(window);
            }

            __init();
        });
    </script>
    <h4 class="page-header">Split Doc</h4>
    <form id="form1" runat="server">
    <asp:DropDownList ID="ApplicationDDLTemplate" runat="server" CssClass="Template"></asp:DropDownList>
    <table width="100%" cellpadding="4" cellspacing="0">
        <tr>
            <td colspan="2">
                <div id="m_dataDiv">
                    <table id="SplitTable" width="97%" cellpadding="4" cellspacing="0">
                        <thead>
                            <tr class="GridHeader">
                                <td>
                                    Page Range
                                </td>
                                <td>
                                    Doc Type
                                </td>
                                <td>
                                    Application
                                </td>
                                <td>
                                    Status
                                </td>
                                <td>
                                    Description
                                </td>
                                <td>
                                    Internal Comments
                                </td>
                                <td>
                                    Associated Conditions
                                </td>
                                <td>
                                </td>
                            </tr>
                        </thead>
                        <tbody id="rangeTable">
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <a href="#" id="m_addLink" onclick="addRow();">add another page range</a>
                <div style="float: right; padding-right: 30px">
                    <input type="button" id="m_okay" value="Split into new documents" onclick="split();" AlwaysEnable
                        nohighlight />&nbsp;&nbsp;<input type="button" value="Cancel" onclick="onCancel();" AlwaysEnable />
                </div>
            </td>
        </tr>
    </table>
    <uc1:cModalDlg ID="Cmodaldlg1" runat="server" />
    </form>
</body>
</html>
