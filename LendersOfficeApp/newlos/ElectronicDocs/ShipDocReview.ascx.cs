﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using EDocs;
using EDocs.UI;
using System.Drawing;
using MeridianLink.CommonControls;
using LendersOffice.Common;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class ShipDocReview : UserControl
    {

        private static ListItemType[] x_skipList = new ListItemType[] { 
                ListItemType.Footer,
                ListItemType.Header,
                ListItemType.Pager,
                ListItemType.Separator
            };



        public ICollection<ShippingReviewView> DataSource
        {
            // Assumes that the value is already properly ordered and ready for display.
            set
            {

                Documents.DataSource = value;
                if (value.Where(p => p.Status == E_ShippingReviewEntryStatus.MISSING).Count() > 0)
                {
                    Warning.Visible = true;
                }
                else
                {
                    Warning.Visible = false;
                }
                Documents.DataBind();
            }


        }



        public List<ShippingReviewView> GetSelectedDocumentIds()
        {
            List<ShippingReviewView> entries = new List<ShippingReviewView>(Documents.Items.Count);
            foreach (RepeaterItem item in Documents.Items)
            {
                if (x_skipList.Contains(item.ItemType))
                {
                    continue;
                }
                HiddenField docId = item.FindControl("id") as HiddenField;
                HiddenField ownerPw = item.FindControl("HasOwnerPassword") as HiddenField;
                HiddenField pdfHasUnsupportedFeatures = item.FindControl("PdfHasUnsupportedFeatures") as HiddenField;
                HiddenField isESigned = item.FindControl("IsESigned") as HiddenField;

                HtmlInputCheckBox chb = item.FindControl("SelectItem") as HtmlInputCheckBox;
                if (chb == null || !chb.Checked || String.IsNullOrEmpty(docId.Value))
                {
                    continue;
                }

                EncodedLabel internalDescription = item.FindControl("InternalDescription") as EncodedLabel;
                EncodedLabel description = item.FindControl("Description") as EncodedLabel;
                EncodedLabel folder = item.FindControl("Folder") as EncodedLabel;
                EncodedLabel docType = item.FindControl("DocType") as EncodedLabel;
                EncodedLabel docStatus = item.FindControl("DocStatus") as EncodedLabel;
                EncodedLabel application = item.FindControl("Application") as EncodedLabel;
                EncodedLabel pageCount = item.FindControl("PageCount") as EncodedLabel;
                EncodedLabel lastModified = item.FindControl("LastModified") as EncodedLabel;

                entries.Add(new ShippingReviewView()
                {
                    InternalDescription = internalDescription.Text,
                    Description = description.Text,
                    Folder = folder.Text,
                    DocType = docType.Text,
                    Application = application.Text,
                    PageCount = int.Parse(pageCount.Text),
                    Id = new Guid(docId.Value),
                    Status = E_ShippingReviewEntryStatus.OK,
                    HasOwnerPassword = bool.Parse(ownerPw.Value),
                    LastModified = lastModified.Text,
                    PdfHasUnsupportedFeatures = bool.Parse(pdfHasUnsupportedFeatures.Value),
                    DocStatus = EDocument.StatusFromText(docStatus.Text),
                    IsESigned = bool.Parse(isESigned.Value)
                });
            }

            return entries;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }



        protected void Documents_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (x_skipList.Contains(args.Item.ItemType))
            {
                return;
            }

            var data = args.Item.DataItem as ShippingReviewView;
            HtmlInputCheckBox selectBox = args.Item.FindControl("SelectItem") as HtmlInputCheckBox;
            EncodedLabel folder = args.Item.FindControl("Folder") as EncodedLabel;
            EncodedLabel docType = args.Item.FindControl("DocType") as EncodedLabel;
            EncodedLabel internalDescription = args.Item.FindControl("InternalDescription") as EncodedLabel;
            EncodedLabel description = args.Item.FindControl("Description") as EncodedLabel;
            EncodedLabel docStatus = args.Item.FindControl("DocStatus") as EncodedLabel;
            EncodedLabel application = args.Item.FindControl("Application") as EncodedLabel;
            EncodedLabel pageCount = args.Item.FindControl("PageCount") as EncodedLabel;
            EncodedLabel lastModified = args.Item.FindControl("LastModified") as EncodedLabel;
            var status = args.Item.FindControl("Status") as MeridianLink.CommonControls.EncodedLiteral;
            HtmlAnchor link = args.Item.FindControl("viewLink") as HtmlAnchor;
            HiddenField id = args.Item.FindControl("id") as HiddenField;
            HiddenField hasOwnerPassword = args.Item.FindControl("HasOwnerPassword") as HiddenField;
            HiddenField pdfHasUnsupportedFeatures = args.Item.FindControl("PdfHasUnsupportedFeatures") as HiddenField;
            HiddenField isESigned = args.Item.FindControl("IsESigned") as HiddenField;
            HtmlTableRow row = args.Item.FindControl("row") as HtmlTableRow;


            status.Text = data.Status.ToString();
            folder.Text = data.Folder;
            docType.Text = data.DocType;
            lastModified.Text = data.LastModified;

            if (data.Status == E_ShippingReviewEntryStatus.MISSING)
            {
                link.Visible = false;
                selectBox.Visible = false;

                row.Style.Add("background-color", "#ffe4e1");
                return;
            }

            if (data.Status == E_ShippingReviewEntryStatus.DUPLICATE)
            {
                row.Style.Add("background-color", "#ffffcc");
            }

            hasOwnerPassword.Value = data.HasOwnerPassword.ToString();
            pdfHasUnsupportedFeatures.Value = data.PdfHasUnsupportedFeatures.ToString();
            isESigned.Value = data.IsESigned.ToString();
            selectBox.Checked = true;
            internalDescription.Text = data.InternalDescription;
            description.Text = data.Description;
            application.Text = data.Application;
            pageCount.Text = data.PageCount.ToString();
            status.Text = data.Status.ToString();
            link.HRef = "javascript:view('" + AspxTools.JsStringUnquoted(data.Id.ToString()) + "');";
            id.Value = data.Id.ToString();

            Color sColor = Color.FromArgb(0, 128, 0);

            if (data.DocStatus == E_EDocStatus.Obsolete || data.DocStatus == E_EDocStatus.Rejected)
            {
                sColor = Color.FromArgb(255, 0, 0);
            }
            else if (data.DocStatus == E_EDocStatus.Screened)
            {
                sColor = Color.FromArgb(0, 64, 128);
            }

            docStatus.ForeColor = sColor;
            docStatus.Font.Bold = true;
            docStatus.Text = EDocument.StatusText(data.DocStatus);
        }
    }
}