﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using DataAccess;
using EDocs;
using CommonProjectLib.Common.Lib;
using LendersOffice.Security;
using LendersOffice.ObjLib.DocumentConditionAssociation;
using LendersOffice.AntiXss;
using EDocs.Contents;
using LendersOffice.Constants;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class DocsReceived : LendersOfficeApp.newlos.BaseLoanUserControl, IAutoLoadUserControl
    {
        /// <summary>
        /// Controls whether the column containing icons is displayed in the document table.
        /// This is intended to be set before the repeater is bound.
        /// </summary>
        private bool displayIconColumn = false;
        private EDocumentRepository m_repo = EDocumentRepository.GetUserRepository();
        private Dictionary<Guid, GenericEDocument> m_xmlDocs;
        private IDictionary<int, int> m_docTypeOrderNumberByID;
        private ILookup<Guid, DocumentConditionAssociation> m_associations; 
        public bool ShowOnlyActive { get; set; }
        public bool ShowOnlyInActive { get; set; }
        private bool HasStaticCondNumbering { get; set; }

        private bool UserCanReadNotes
        {
            get
            {
                return BrokerUser.HasPermission(LendersOffice.Security.Permission.CanViewEDocsInternalNotes);
            }
        }
        private bool BatchEditorEnabled
        {
            get
            {
                return this.BrokerDB.IsEnableBatchEdocEditor && BrokerUser.HasPermission(Permission.CanEditEDocs);
            }
        }

        private static ListItemType[] x_skipList = new ListItemType[] { 
                ListItemType.Footer,
                ListItemType.Header,
                ListItemType.Pager,
                ListItemType.Separator
            };


        private Lazy<bool> IsDocuSignEsignEnabled = new Lazy<bool>(() =>
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            var configuration = LendersOffice.Admin.DocuSign.LenderDocuSignSettings.Retrieve(principal.BrokerId);
            return PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCreatingDocumentSigningEnvelopes) &&
                   configuration != null && configuration.IsSetupComplete() && configuration.DocuSignEnabled;
        });

        #region IAutoLoadUserControl Members

        public void LoadData()
        {
            if (!BrokerUser.HasPermission(LendersOffice.Security.Permission.CanViewEDocs))
            {
                throw new LendersOffice.Security.AccessDenied();
            }

            MigrationPanel.Visible = EDocsMigration.DoesLoanHaveLegacyEDocs(this.BrokerDB.BrokerID, LoanID);

            BrokerDB db = this.BrokerDB;
            HasStaticCondNumbering = db.IsUseNewTaskSystemStaticConditionIds;

            var basePage = this.Page as BasePage;
            if (basePage == null)
            {
                throw new CBaseException(
                    ErrorMessages.Generic,
                    "This user control is meant to be used on a BasePage.");
            }

            bool isBarcodeScannerEnabled = db.IsDocMagicBarcodeScannerEnabled ||
                db.IsDocuTechBarcodeScannerEnabled ||
                db.IsIDSBarcodeScannerEnabled;
            basePage.RegisterJsGlobalVariables(
                "isBarcodeScannerEnabled",
                isBarcodeScannerEnabled);
            basePage.RegisterJsGlobalVariables(
                "docListBarcodeStatusRefreshIntervalSeconds",
                ConstStage.DocListBarcodeStatusRefreshIntervalSeconds);

            var docs = m_repo.GetDocumentsByLoanId(LoanID);

            m_associations = DocumentConditionAssociation.GetAssociationsByLoanHeavy(BrokerUser.BrokerId, LoanID, BrokerUser).ToLookup(py => py.DocumentId);

            m_docTypeOrderNumberByID = db.GetStackOrderByDocTypeID();

            Dictionary<int, string> folders = new Dictionary<int, string>();
            List<object> bindingDocList = new List<object>();
            List<string> docIdList = new List<string>();
            var genericDocs = m_repo.GetGenericDocumentsByLoanId(LoanID);
            m_xmlDocs = genericDocs.ToDictionary(x => x.DocumentId);

            foreach (EDocument doc in docs)
            {
                doc.RequeueCheck();
                if (false == folders.ContainsKey(doc.FolderId))
                {
                    folders.Add(doc.FolderId, doc.Folder.FolderNm);
                }
                if( (ShowOnlyActive && !(doc.DocStatus == E_EDocStatus.Blank || doc.DocStatus == E_EDocStatus.Approved || doc.DocStatus == E_EDocStatus.Screened)) ||
                    (ShowOnlyInActive && !(doc.DocStatus == E_EDocStatus.Obsolete || doc.DocStatus == E_EDocStatus.Rejected)))
                    continue;

                bindingDocList.Add(doc);

                docIdList.Add(doc.DocumentId.ToString());

                this.displayIconColumn |= doc.IsESigned || doc.HasInternalAnnotations || m_xmlDocs.ContainsKey(doc.DocumentId);
            }

            foreach (var doc in genericDocs)
            {

                if (false == folders.ContainsKey(doc.FolderId))
                {
                    folders.Add(doc.FolderId, doc.Folder.FolderNm);
                }


                if (!doc.IsLinkedToEDoc && ShowOnlyActive) 
                {
                    bindingDocList.Add(doc);
                    this.displayIconColumn = true;
                }
            }

            Documents.DataSource = bindingDocList;
            Documents.DataBind();

            this.IconLegend.Visible = this.displayIconColumn;

            var deletedDocs = m_repo.GetDeletedDocumentsByLoanId(LoanID, BrokerUser.BrokerId);
            //OPM 69366: the restore deleted docs button should be visible but disabled
            //           if there are no docs to restore.
            DeletedDocs.Enabled = deletedDocs.Any();

            //The function bound to the on client click event should return false, 
            //because otherwise the button causes a nasty and unwanted postback event.
            //We have to use "Attributes.Add" because if you use "OnClientClick" ASP doesn't bind anything to the button,
            //since the button starts out disabled (disabled buttons don't need events I guess).
            //This is problematic because we occasionally enable it with Javascript later,
            //and its event needs to be bound.
            DeletedDocs.Attributes.Add("onclick", "javascript:" + string.Format("restore('{0}'); return false;", AspxTools.JsStringUnquoted(LoanID.ToString("N"))));


            Folders.DataSource = folders.OrderBy(f => f.Value);
            Folders.DataValueField = "Key";
            Folders.DataTextField = "Value";
            Folders.DataBind();

            var p = new System.Web.UI.WebControls.ListItem("All Folders", "-1", true);
            p.Selected = true;
            Folders.Items.Insert(0,p);

            if (Folders.Items.Count < 3)
            {
                FolderDDLLabel.Disabled = true;
                Folders.Enabled = false;
                Folders.ToolTip = "Only One Folder Available";
            }

            OpenBatchEditor.Visible = BatchEditorEnabled;
            SendDocuSignEnvelope.Visible = PrincipalFactory.CurrentPrincipal.CanCreateDocuSignEnvelopes();
            BatchEditorUrl.Value =  Tools.GetEDocsLink(ResolveUrl("EditEDocBatch.aspx"));
            WindowName.Value = LoanID.ToString("N");
            ((LendersOfficeApp.newlos.BaseLoanPage)this.Page).RegisterJsGlobalVariables("EdocsIdsList", string.Join(",", docIdList.ToArray()));


            this.ViewDocuSignEnvelopesBtn.Visible = IsDocuSignEsignEnabled.Value;
        }

        public void SaveData()
        {
        }

        private string m_orderingFormat = null;

        /// <summary>
        /// Because js tablesorter isn't aware of how to sort numbers, we have to pad them with zeros.
        /// </summary>
        private string GetOrderingFormat()
        {
            if (this.m_orderingFormat == null)
            {
                if (m_docTypeOrderNumberByID == null)
                {
                    m_docTypeOrderNumberByID = this.BrokerDB.GetStackOrderByDocTypeID();
                }

                var orderingValues = m_docTypeOrderNumberByID.Values;
                var maxOrder = orderingValues.Any() ? orderingValues.Max() : 10;
                var numDigitsInMaxOrder = (int)Math.Ceiling(Math.Log10(maxOrder));
                this.m_orderingFormat = "D" + numDigitsInMaxOrder;
            }

            return this.m_orderingFormat;
        }

        protected void Documents_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (x_skipList.Contains(args.Item.ItemType))
            {
                if (args.Item.ItemType == ListItemType.Header)
                {
                    HtmlInputCheckBox MasterBatchCB = (HtmlInputCheckBox)args.Item.FindControl("BatchEditMaster");
                    MasterBatchCB.Visible = BatchEditorEnabled;
                    HtmlTableCell iconColumnHeader = (HtmlTableCell)args.Item.FindControl("IconColumnHeader");
                    if (!this.displayIconColumn)
                    {
                        iconColumnHeader.Attributes["class"] = "Hidden";
                    }

                    HtmlTableCell esignreadyHeader = (HtmlTableCell)args.Item.FindControl("ESignReadyColumnHeader");
                    esignreadyHeader.Visible = this.IsDocuSignEsignEnabled.Value;
                }

                return;
            }

            HtmlAnchor viewLink = args.Item.FindControl("View") as HtmlAnchor;
            HtmlAnchor viewXmlLink = args.Item.FindControl("ViewXml") as HtmlAnchor;
            HtmlAnchor editLink = args.Item.FindControl("Edit") as HtmlAnchor;
            HtmlAnchor deleteLink = args.Item.FindControl("Delete") as HtmlAnchor;
            HtmlAnchor auditLink = args.Item.FindControl("Audit") as HtmlAnchor;
            HtmlAnchor createEditableCopyLink = args.Item.FindControl("CreateEditableCopy") as HtmlAnchor;
            HtmlTableRow row = args.Item.FindControl("Row") as HtmlTableRow; 

            EncodedLiteral internalDescription = (EncodedLiteral)args.Item.FindControl("InternalDescription");
            EncodedLiteral publicDescription = (EncodedLiteral)args.Item.FindControl("PublicDescription");
            EncodedLiteral folderName = (EncodedLiteral)args.Item.FindControl("FolderNm");
            EncodedLiteral docType = (EncodedLiteral)args.Item.FindControl("DocType");
            EncodedLiteral lastModified = (EncodedLiteral)args.Item.FindControl("LastModified");
            EncodedLiteral pageCount = (EncodedLiteral)args.Item.FindControl("PageCount");
            EncodedLiteral app = (EncodedLiteral)args.Item.FindControl("App");
            EncodedLiteral contentType = (EncodedLiteral)args.Item.FindControl("ContentType");
            PlaceHolder status = (PlaceHolder)args.Item.FindControl("Status");
            EncodedLiteral docTypeOrderNumber = (EncodedLiteral)args.Item.FindControl("DocTypeOrderNumber");
            EncodedLiteral uploadedDate = (EncodedLiteral)args.Item.FindControl("UploadedDate");
            EncodedLiteral uploadedBy = (EncodedLiteral)args.Item.FindControl("UploadedBy");
            Repeater associations = args.Item.FindControl("ConditionAssociations") as Repeater;

            HtmlContainerControl imageStatusDiv = args.Item.FindControl("imgStatus") as HtmlContainerControl;
            HtmlGenericControl eta = args.Item.FindControl("eta") as HtmlGenericControl;
            HtmlContainerControl imageError = args.Item.FindControl("imgError") as HtmlContainerControl;

            HtmlInputHidden idHolder = (HtmlInputHidden)args.Item.FindControl("EdocId");
            HtmlInputCheckBox batchEdit = (HtmlInputCheckBox)args.Item.FindControl("BatchEditCB");
            HtmlAnchor conditionAssociations = (HtmlAnchor)args.Item.FindControl("ClearConditions");

            HtmlTableCell iconColumnCell = (HtmlTableCell)args.Item.FindControl("IconColumnCell");
            HtmlTableCell eSignReadyColumnCell = (HtmlTableCell)args.Item.FindControl("ESignReadyColumnCell");

            eSignReadyColumnCell.Visible = this.IsDocuSignEsignEnabled.Value;
            row.Attributes.Add("class", args.Item.ItemType == ListItemType.Item ? "GridItem" : "GridAlternatingItem" );
            if (!this.displayIconColumn)
            {
                iconColumnCell.Attributes["class"] = "Hidden";
            }

            GenericEDocument xmlDoc = args.Item.DataItem as GenericEDocument;
            if (xmlDoc != null)
            {
                viewLink.Visible = true;
                editLink.Visible = false;
                batchEdit.Visible = false;
                deleteLink.Visible = false;
                auditLink.Visible = false;
                foreach (HtmlImage icon in Tools.GetDocumentIcons(xmlDoc))
                {
                    iconColumnCell.Controls.Add(icon);
                }

                if (xmlDoc.SupportsContentUpdate && PrincipalFactory.CurrentPrincipal.HasPermission(Permission.CanEditEDocs))
                {
                    viewXmlLink.Visible = true;
                    viewXmlLink.HRef = "#";
                    viewXmlLink.InnerText = "upload new version";
                    viewXmlLink.Attributes.Add("data-docid", xmlDoc.DocumentId.ToString());
                    viewXmlLink.Attributes.Add("class", "replaceLink");
                }

                deleteLink.Visible = true;
                deleteLink.Attributes.Add("data-docid", xmlDoc.DocumentId.ToString());
                deleteLink.Attributes.Add("class", "deleteGenericDoc");
                deleteLink.HRef = "#";
                deleteLink.InnerText = "delete document";

                internalDescription.Text = xmlDoc.InternalComments;
                publicDescription.Text = xmlDoc.Description;
                folderName.Text = xmlDoc.FolderNm;
                app.Text = xmlDoc.AppName;
                docType.Text = xmlDoc.DocTypeName;
                lastModified.Text = xmlDoc.CreatedD.ToString();
                viewLink.HRef = string.Format("javascript:openXmlDoc('{0}');", AspxTools.JsStringUnquoted(xmlDoc.DocumentId.ToString()));
                pageCount.Text = "N/A";
                viewLink.InnerText = "download " + xmlDoc.FriendlyFileType.ToLower();
                contentType.Text = xmlDoc.FriendlyFileType;
                imageStatusDiv.Visible = false;
                row.Attributes.Add("folderid", xmlDoc.FolderId.ToString());
                if (m_docTypeOrderNumberByID.ContainsKey(xmlDoc.DocTypeId))
                {
                    docTypeOrderNumber.Text = m_docTypeOrderNumberByID[xmlDoc.DocTypeId].ToString(GetOrderingFormat());
                }
                else
                {
                    docTypeOrderNumber.Text = int.MaxValue.ToString();
                }

                uploadedDate.Text = xmlDoc.CreatedD.ToString();
                uploadedBy.Text = EmployeeDB.GetEmployeeNameByUserId(xmlDoc.BrokerId, xmlDoc.CreatedByUserId);
                return;
            }

            EDocument currentDoc = args.Item.DataItem as EDocument;
            if(m_docTypeOrderNumberByID.ContainsKey(currentDoc.DocumentTypeId))
            {
                docTypeOrderNumber.Text = m_docTypeOrderNumberByID[currentDoc.DocumentTypeId].ToString(GetOrderingFormat());
            }
            else
            {
                docTypeOrderNumber.Text = int.MaxValue.ToString();
            }

            if (m_xmlDocs.ContainsKey(currentDoc.DocumentId))
            {
                viewXmlLink.Visible = true;
                viewXmlLink.HRef = string.Format("javascript:openXmlDoc('{0}');", AspxTools.JsStringUnquoted(currentDoc.DocumentId.ToString()));
            }

            foreach (HtmlImage icon in Tools.GetDocumentIcons(currentDoc, m_xmlDocs))
            {
                iconColumnCell.Controls.Add(icon);
            }

            if (!currentDoc.LoanId.HasValue)
            {
                throw new CBaseException(ErrorMessages.Generic, "DocsReceive-Encountered EDocument without loan id. Should not be possible.");
            }

            createEditableCopyLink.Visible = currentDoc.IsESigned;

            bool etaIsVisible = false;
            bool errorIsVisible = false;

            editLink.Disabled = true;
            batchEdit.Visible = false;
            row.Attributes.Add("docid", currentDoc.DocumentId.ToString());
            row.Attributes.Add("folderid", currentDoc.FolderId.ToString());
            switch (currentDoc.ImageStatus)
            {
                case E_ImageStatus.NoCachedImagesNotInQueue:
                    Tools.LogError("E_ImageStatus.NoCachedImagesNotInQueue found in display -- Should never happen.");
                    break;
                case E_ImageStatus.NoCachedImagesButInQueue:
                    etaIsVisible = true;
                    eta.InnerText = "Prepping editor - est. time remaining: "  + currentDoc.GetImageReadyETA();
                    break;
                case E_ImageStatus.HasCachedImages:
                    editLink.Disabled = false;
                    batchEdit.Visible = true;
                    imageStatusDiv .Visible= false;
                    break;
                case E_ImageStatus.Error:
                    errorIsVisible = true;
                    break;
                default:
                    throw new UnhandledEnumException(currentDoc.ImageStatus);
            }

            if (etaIsVisible == false)
            {
                eta.Attributes.Add("style", "display:none");
            }
            if (errorIsVisible == false)
            {
                imageError.Attributes.Add("style", "display:none");
            }

            deleteLink.Visible = BrokerUser.HasPermission(LendersOffice.Security.Permission.CanEditEDocs);

            auditLink.HRef = "javascript:showAudit('" + AspxTools.JsStringUnquoted(currentDoc.DocumentId.ToString()) + "');";

            folderName.Text = currentDoc.Folder.FolderNm ;
            docType.Text = currentDoc.DocTypeName;
            if (currentDoc.HasESignTags)
            {
                eSignReadyColumnCell.Controls.Add(new HtmlImage() { Src = "~/images/pass.png" });
                row.Attributes.Add("data-esign-prepped", "true");
            }

            viewLink.HRef = "javascript:";
            idHolder.Value = currentDoc.DocumentId.ToString();

            app.Text = currentDoc.AppName;
            viewLink.Attributes.Add("onclick", "javascript:view('" + AspxTools.JsStringUnquoted(currentDoc.DocumentId.ToString()) + "', event);");

            editLink.Attributes.Add("onclick", string.Format("javascript:edit(this,'{0}','{1}');", AspxTools.JsStringUnquoted(currentDoc.DocumentId.ToString("N")), AspxTools.JsStringUnquoted(currentDoc.LoanId.Value.ToString("N"))));
            editLink.Target = currentDoc.DocumentId.ToString("N");
            if (currentDoc.PdfHasUnsupportedFeatures)
            {
                editLink.Attributes["onclick"] = "javascript:alert('Cannot edit as pdf has unsupported feature(s).');";
                batchEdit.Visible = false;
            }
            lastModified.Text = currentDoc.LastModifiedDate.ToString();
            pageCount.Text = currentDoc.PageCount.ToString();
            internalDescription.Text = currentDoc.InternalDescription;
            publicDescription.Text = currentDoc.PublicDescription;

            if (currentDoc.DocStatus == E_EDocStatus.Approved && !BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowEditingApprovedEDocs))
            {
                deleteLink.Visible = false;
                batchEdit.Visible = false;
            }

            status.Controls.Add(Tools.GetDocumentStatusControl(currentDoc.DocStatus, currentDoc.StatusDescription));

            if (currentDoc.RequiresDocTypeChange)
            {
                batchEdit.Visible = false;
            }

            if (!BatchEditorEnabled)
            {
                batchEdit.Visible = false;
            }

            if (m_associations.Contains(currentDoc.DocumentId))
            {
                if (currentDoc.DocStatus == E_EDocStatus.Rejected)
                {
                    conditionAssociations.Attributes.Add("data-docid", currentDoc.DocumentId.ToString("N"));
                    conditionAssociations.Visible = true;
                }
                associations.DataSource = m_associations[currentDoc.DocumentId];
                associations.DataBind();
            }

            uploadedDate.Text = currentDoc.CreatedDate.ToString();

            if (currentDoc.CreatedByUserId.HasValue)
            {
                uploadedBy.Text = EmployeeDB.GetEmployeeNameByUserId(currentDoc.BrokerId, currentDoc.CreatedByUserId.Value);
            }
        }

        #endregion
        protected void ConditionAssociations_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            
            HtmlContainerControl container = (HtmlContainerControl)args.Item.FindControl("info");
            DocumentConditionAssociation association = args.Item.DataItem as DocumentConditionAssociation;

            if (association == null)
            {
                return;
            }

            string id = association.TaskId;

            if (HasStaticCondNumbering)
            {
                id = association.CondRowId.ToString();
            }

            container.Attributes.Add("title", AspxTools.HtmlString(association.TaskSubject));
            container.InnerText = string.Format("{0}. {1}", id,  association.TaskSubject);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var basePage = (BasePage)this.Page;

            basePage.RegisterJsScript("DragDropUpload.js");
            basePage.RegisterJsScript("DocumentUploadHelper.js");

            basePage.RegisterCSS("DragDropUpload.css");
        }
    }
}