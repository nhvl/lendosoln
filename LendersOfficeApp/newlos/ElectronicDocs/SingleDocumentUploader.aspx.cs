﻿#region Generated Code
namespace LendersOfficeApp.newlos.ElectronicDocs
#endregion
{
    using System;
    using LendersOffice.Common;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "*", Justification = "Temporary while getting this to work.")]
    public partial class SingleDocumentUploader : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("LQBPopup.js");

            this.DisplayCopyRight = false;
            this.UploadDocs.SupportedApplicationID = this.ApplicationID;
            ((IAutoLoadUserControl)this.UploadDocs).LoadData();
        }
    }
}
