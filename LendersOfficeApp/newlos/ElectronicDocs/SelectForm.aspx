﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectForm.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.SelectForm" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Select Form</title>
	<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
    
</head>
<body width="100%" >

 <script type="text/javascript">
         function _init() {
            resize(600, 450);
            $('ul.tabnav a').click(function(){
                 $(this).parents('ul').find('li').removeClass('selected');
                $('div.tabentry').hide();
                $('#' + $(this).data('showid')).show();
                $(this).parent().addClass('selected');
                return false;
            });
        }

        function selectForm(id, fileName, isStandardForm, recordId, description) {
            var data = window.dialogArguments || {};            
            data.id = id;
            data.fileName = fileName;
            data.isStandardForm = isStandardForm;
            data.recordId = recordId;
            data.formDescription = description;
            data.formType = 'form';
            onClosePopup(data);
        }
        
        function selectEDoc(docid, version, key){
            var data = window.dialogArguments || {};            
           data.docid = docid;
           data.version = version;
           data.formType = 'edoc';
           data.key = key;
           onClosePopup(data);
        }
        
        function previewDoc(docid) {
            var link = <%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + docid;
            window.open(link);
        }
     function viewForm(formId, fileName, isStandardForm, recordId) {
         var pageSize = 0; //LetterSize
         var options = 0; //Form with data
                           
         if (isStandardForm == "True") {
             if (fileName == null || fileName.length == 0) {
                 return;
             }

            var path = <%= AspxTools.JsString(VirtualRoot)%> + "/pdf/" + fileName + "?loanid=" + <%=AspxTools.JsString(LoanID)%> + "&applicationid=" + <%=AspxTools.JsString(aAppId) %> +"&recordid=" + recordId + "&printoptions=" + options;    
            LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(path);
            return false;         
         }
         else {
             if (formId == null || formId.length == 0) {
                 return;
             }

             var path = <%= AspxTools.JsString(VirtualRoot)%> + "/pdf/Custom.aspx?loanid=" + <%=AspxTools.JsString(LoanID)%> + "&applicationid=" + <%=AspxTools.JsString(aAppId) %> + "&recordid=" + recordId + "&custompdfid=" + formId + "&pagesize=" + pageSize + "&printoptions=" + options + "&crack=" + new Date();
             LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(path);
             return false;
         }        
        }
</script> 
    <h4 class="page-header">Select Form</h4>
    <form id="form1" runat="server">
        <div class="Tabs">
                <ul class="tabnav">
                    <li class="selected" ><a href="#" data-showid='CustomForms' >Custom Forms</a></li>
                    <li runat="server" id="EdocTab" visible="false"><a href="#" data-showid='Edocs' >Edocs</a></li>
                </ul>
        </div>
        
        <div id="CustomForms" class="tabentry">
        <table width="99%" border="0" cellspacing="2" cellpadding="3">
			<tr>
			    <td>
			        Only forms that contain e-signature form fields can be e-signed.
			    </td>
			</tr>
			<tr>
			    <td>
        			<ml:CommonDataGrid id="m_formList" runat="server">
						<alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
						<itemstyle cssclass="GridItem"></itemstyle>
						<headerstyle cssclass="GridHeader"></headerstyle>
						<Columns>
							<asp:TemplateColumn>
								<itemtemplate>
									<a href="#" onclick="selectForm(<%# AspxTools.JsString(Eval("FormId").ToString())%>, <%# AspxTools.JsString(Eval("PreviewLink") == null ? string.Empty : Eval("PreviewLink").ToString())%>, <%# AspxTools.JsString(Eval("IsStandardForm").ToString())%>, <%# AspxTools.JsString(Eval("RecordId").ToString()) %>,<%# AspxTools.JsString(Eval("Description").ToString()) %>);">select</a>
								</itemtemplate>
							</asp:TemplateColumn>
                            <asp:TemplateColumn>
								<itemtemplate>
									<a href="#" onclick="viewForm(<%# AspxTools.JsString(Eval("FormId").ToString())%>, <%# AspxTools.JsString(Eval("PreviewLink") == null ? string.Empty : Eval("PreviewLink").ToString())%>, <%# AspxTools.JsString(Eval("IsStandardForm").ToString())%>, <%# AspxTools.JsString(Eval("RecordId").ToString()) %>);return false;">view</a>
								</itemtemplate>
							</asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Form">
								<itemtemplate>
									<%# AspxTools.HtmlString(Eval("IsStandardForm").ToString()) == "True" ? Eval("Description").ToString() : AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Description").ToString())%>
								</itemtemplate>
							</asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="eSignable by borrower?">
								<itemtemplate>
									<%# AspxTools.HtmlString(bool.Parse(DataBinder.Eval(Container.DataItem, "IsContainBorrowerSignature").ToString()) ? "yes" : " ")%>
								</itemtemplate>
							</asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="eSignable by coborrower?">
								<itemtemplate>
									<%# AspxTools.HtmlString(bool.Parse(DataBinder.Eval(Container.DataItem, "IsContainCoborrowerSignature").ToString()) ? "yes" : " ")%>
								</itemtemplate>
							</asp:TemplateColumn>
						</Columns>
					</ml:CommonDataGrid>
			    </td>
			</tr>
			<tr>
			    <td>
			        <input type="button" value="Cancel" onclick="onClosePopup();" />
			    </td>
			</tr>
		</table>
		</div>

        <div id="Edocs" style="display:none;" class="tabentry">
            <asp:Repeater runat="server" ID="EDocSelector" OnItemDataBound="EDocSelector_OnItemDataBound">
                <HeaderTemplate>
                    <table class="FormTable" >
                        <thead>
                            <tr class="GridHeader">
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>Doc Type</th>
                                <th>Description</th>
                                <th>Shared Previously?</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                        <tr class='<%# AspxTools.HtmlString( Container.ItemIndex % 2 == 0 ? "GridItem" : "GridAlternatingItem") %>'>
                            <td>
                                <asp:LinkButton runat="server" ID="SelectDocument" OnClick="SelectDocument_OnClick" >select</asp:LinkButton>
                            </td>
                            <td>
                                <a runat="server" id="preview" href="#">view</a>
                            </td>
                            <td>
                                <ml:EncodedLiteral runat="server" ID="DocType"></ml:EncodedLiteral>
                            </td>
                            <td>
                                <ml:EncodedLiteral runat="server" ID="Description"></ml:EncodedLiteral>
                            </td>
                            <td>
                                <ml:EncodedLiteral runat="server" ID="SharedPreviously"></ml:EncodedLiteral>
                            </td>
                        </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
    </form>
</body>
</html>
