﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using EDocs.UI;
using MeridianLink.CommonControls;
using LendersOffice.AntiXss;
using LendersOffice.Security;
using DataAccess;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class ShipDocStackingOrder : System.Web.UI.UserControl
    {
        private static ListItemType[] x_skipList = new ListItemType[] { 
                ListItemType.Footer,
                ListItemType.Header,
                ListItemType.Pager,
                ListItemType.Separator
        };


        protected void Page_Load(object sender, EventArgs e)
        {
            ((LendersOffice.Common.BasePage)this.Page).RegisterJsScript("LQBPopup.js");
        }


        public void SetData(ICollection<ShippingReviewView> view)
        {
            Documents.DataSource = view;
            Documents.DataBind();

            HasPDFWithOwnerPassword.Value =   view.Any(p => p.HasOwnerPassword).ToString();
            HasPDFWithUnsupportedFeatures.Value = view.Any(p => p.PdfHasUnsupportedFeatures).ToString();
        }

        protected string GetUrl(string page)
        {
            string url = Page.ResolveUrl(page); 
            if (PrincipalFactory.CurrentPrincipal.BrokerDB.IsTestingAmazonEdocs)
            {
                return url;
            }

            return Tools.GetEDocsLink(url);
        }

        protected void DocumentStackingOrder_OnItemDataBound(object senver, RepeaterItemEventArgs args)
        {
            if (x_skipList.Contains(args.Item.ItemType))
            {
                return;
            }

            ShippingReviewView view = args.Item.DataItem as ShippingReviewView;

            EncodedLiteral folder = args.Item.FindControl("Folder") as EncodedLiteral;
            EncodedLiteral docType = args.Item.FindControl("DocType") as EncodedLiteral;
            EncodedLiteral lastModified = args.Item.FindControl("LastModified") as EncodedLiteral;
            EncodedLiteral description = args.Item.FindControl("Description") as EncodedLiteral;
            EncodedLiteral internalDescription = args.Item.FindControl("InternalDescription") as EncodedLiteral;
            EncodedLiteral application = args.Item.FindControl("Application") as EncodedLiteral;
            EncodedLiteral pageCount = args.Item.FindControl("PageCount") as EncodedLiteral;
            HtmlAnchor viewLink = args.Item.FindControl("viewLink") as HtmlAnchor;
            HiddenField id = args.Item.FindControl("id") as HiddenField;
            HiddenField isESigned = args.Item.FindControl("IsESigned") as HiddenField;

            folder.Text = view.Folder;
            docType.Text = view.DocType;
            lastModified.Text = view.LastModified;
            description.Text = view.Description;
            internalDescription.Text = view.InternalDescription;
            pageCount.Text = view.PageCount.ToString();
            viewLink.HRef = string.Format("javascript:view('{0}');", AspxTools.JsStringUnquoted(view.Id.ToString()));
            id.Value = view.Id.ToString();
            application.Text = view.Application;
            isESigned.Value = view.IsESigned.ToString();
        }
    }
}