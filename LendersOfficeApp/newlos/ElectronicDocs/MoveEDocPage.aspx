﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MoveEDocPage.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.MoveEDocPage" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="LendersOffice.AntiXss" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Move Page</title>
    <style>
    .padding {
      padding-left:10px;
    }
    .page_textbox 
    {
      width:30px;
      text-align:center;      
    }    
    </style>    
</head>
<body bgcolor="gainsboro" onunload="getModalArgs().callback()">
  <script type="text/javascript">
    var g_iCurrentPage = 0;
    var g_iTotalPages = 0;
  
    function _init() {
      if(window.opener && window.opener.SetupModelessEnvironment){
          window.opener.SetupModelessEnvironment(window);
      }
      resize(200, 250);

      var arg = getModalArgs() || {};
      g_iTotalPages = arg.TotalPages;
      g_iCurrentPage = arg.CurrentPage;
      
      document.getElementById("NumOfPagesLabel").innerText = g_iTotalPages;

      var selectionFrom = 1, selectionTo = 1; 
      if ( arg.SelectedRanges != null && arg.SelectedRanges[0]  != null){
          var selectionRange = arg.SelectedRanges[0]; 
          selectionFrom = selectionRange.Start ? selectionRange.Start + 1 : 1;
          selectionTo = selectionFrom + (selectionRange.Count ? selectionRange.Count - 1 : 0);
      }
      
      document.getElementById("tbPageFrom").value = selectionFrom;
      document.getElementById("tbPageTo").value = selectionTo;
    }
    function onOK() {
      var arg = getModalArgs() || {};

      var pageFrom = parseInt(document.getElementById("tbPageFrom").value);
      if (isNaN(pageFrom) || pageFrom <= 0 || pageFrom > g_iTotalPages || pageFrom.toString() != document.getElementById("tbPageFrom").value) {
        alert('Invalid page number.');
        document.getElementById("tbPageFrom").focus();
        return;
      }
      
      var pageTo = parseInt(document.getElementById("tbPageTo").value);
      if (isNaN(pageTo) || pageTo <= 0 || pageTo > g_iTotalPages || pageTo.toString() != document.getElementById("tbPageTo").value || pageFrom > pageTo) {
        alert('Invalid page number.');
        document.getElementById("tbPageTo").focus();
        return;
      }

      var afterPage = -2;
      
      if (<%= AspxTools.JsGetElementById(rbBeforeFirstPage) %>.checked) 
      {
        afterPage = 0;
      } else if (<%= AspxTools.JsGetElementById(rbAfterLastPage) %>.checked) {
        afterPage = g_iTotalPages;
      } else if (<%= AspxTools.JsGetElementById(rbAfterPage) %>.checked) {
        var pg = parseInt(document.getElementById("tbNewLocation").value);
        if (isNaN(pg) || pg <= 0 || pg > g_iTotalPages || pg != document.getElementById("tbNewLocation").value) {
          alert('Invalid Page Number.');
          document.getElementById("tbNewLocation").focus();
          return;
        }
        afterPage = pg;
      } else {
        alert('Invalid choice.');
        return;
      }
      
      if(afterPage <= pageTo && afterPage >= pageFrom)
      {
        alert("Invalid choice - can't move pages to the same place.");
        return;
      }
      
      arg.From = pageFrom;
      arg.AfterPage = afterPage;
      //Page moving is inclusive, so moving pages 2-3 means moving 2 pages total
      arg.PageCount = pageTo - pageFrom + 1;
      arg.OK = true;
      onClosePopup(arg);
    }
    function onCancel() {
      var arg = window.dialogArguments || {};

      arg.OK = false;    
      onClosePopup(arg);
    }
    function renderUI() {
      var bIsAfterSpecificPage = <%= AspxTools.JsGetElementById(rbAfterPage) %>.checked;

      if (!bIsAfterSpecificPage) {
        document.getElementById("tbNewLocation").value = "";
      }
    }
    function enablePageRange() {
      <%= AspxTools.JsGetElementById(rbAfterPage) %>.checked = true;
    }    
  </script>
    <form id="form1" runat="server">
    <div>
    <table width="100%">
      <tr><td class="MainRightHeader">Move Page</td></tr>
      <tr><td class="padding">Move Page(s): <input type="text" class="page_textbox" id="tbPageFrom" /> to <input type="text" class="page_textbox" id="tbPageTo" /></td></tr>
      <tr><td class="padding"><asp:RadioButton ID="rbBeforeFirstPage" runat="server" Checked="true" GroupName="op" Text="Before first page" onclick="renderUI();"/></td></tr>
      <tr><td class="padding"><asp:RadioButton ID="rbAfterLastPage" runat="server" GroupName="op" Text="After last page" onclick="renderUI();"/></td></tr>
      <tr><td class="padding"><asp:RadioButton ID="rbAfterPage" runat="server" GroupName="op" Text="After page:" />
      <input type="text" class="page_textbox" id="tbNewLocation" onfocus="enablePageRange();"/> of <span id="NumOfPagesLabel" /></td></tr>
      <tr>
        <td align="center">
          <input type="button" value="OK" onclick="onOK();" />&nbsp;&nbsp;
          <input type="button" value="Cancel" onclick="onCancel();" />
        </td>
      </tr>

    </table>
    </div>
    <uc1:cmodaldlg id="Cmodaldlg1" runat="server" />    
    </form>
</body>
</html>
