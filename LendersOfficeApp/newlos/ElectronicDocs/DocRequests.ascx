﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocRequests.ascx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.DocRequests" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<script type="text/javascript">

    function createRequest() {
        resetUserMsg();
        var settings = {
            onReturn: function () { __doPostBack('', ''); }
        };
        LQBPopup.Show(gVirtualRoot + "/newlos/ElectronicDocs/CreateRequest.aspx?LoanID=" + <%= AspxTools.JsString(LoanID) %>, settings);
    }
    
    function toggleBatchButtons() {
        var selectedCheckboxes = $('.PendingDocumentRequestCB:checked');
        
        $('#DeleteRequests').prop("disabled", selectedCheckboxes.length === 0);
        $('#SendRequests').prop("disabled", selectedCheckboxes.length === 0);
    }
        
    function getSelectedRequestIds() {
        var selectedCheckboxes = $('.PendingDocumentRequestCB:checked');
        var selectedRequestIds = [];
        
        selectedCheckboxes.each(function(index, element) {
            var requestId = $(this).siblings('input[type="hidden"]').val();
            selectedRequestIds.push(requestId);
        });
        
        return selectedRequestIds;
    }
    
    function deleteSelectedRequests() {
        var requestIds = getSelectedRequestIds();
        <%= AspxTools.JsGetElementById(m_requestId) %>.value = requestIds.join(',');
        <%= AspxTools.JsGetElementById(m_requestAction) %>.value = 'DeletePending';
        __doPostBack('', '');
    }
    
    function sendSelectedRequests() {
        var requestIds = getSelectedRequestIds();
        <%= AspxTools.JsGetElementById(m_requestId) %>.value = requestIds.join(',');
        <%= AspxTools.JsGetElementById(m_requestAction) %>.value = 'SendPending';
        __doPostBack('', '');
    }
    
    function cancelRequest(id) {
		if (!confirm("Cancel this request?"))
            return;
        
        <%= AspxTools.JsGetElementById(m_requestId) %>.value = id;
        <%= AspxTools.JsGetElementById(m_requestAction) %>.value = 'Cancel';
        __doPostBack('', '');
    }
    $(document).ready(function(){
        $('a.ResetPass').click(function(){
            var id = $(this).data('id');
            
            if (confirm('Change the consumers password and send the new one via email?')){
                <%= AspxTools.JsGetElementById(m_requestId) %>.value = id
                <%= AspxTools.JsGetElementById(m_requestAction) %>.value = 'Reset';
                __doPostBack('', '');
            }
        });
        
        $('a.AccountCreate').click(function(){
            <%= AspxTools.JsGetElementById(m_requestId) %>.value = $(this).data('email');
            <%= AspxTools.JsGetElementById(m_requestAction) %>.value = 'Create';
            __doPostBack('', '');
        });
        
        $('#CreateRequest').click(createRequest);
        $('#DeleteRequests').click(deleteSelectedRequests);
        $('#SendRequests').click(sendSelectedRequests);
        $('.PendingDocumentRequestCB').click(toggleBatchButtons);
        $('a.Explain').click(function(event) {
            var explanation = $(event.target).siblings('input[type="hidden"]').val();
            alert(explanation);
        });
        
        toggleBatchButtons();
    });
    function rejectRequest(id) {
        <%= AspxTools.JsGetElementById(m_requestId) %>.value = id;
        <%= AspxTools.JsGetElementById(m_requestAction) %>.value = 'Reject';
        __doPostBack('', '');
        
    }
    
    function cancelShareDocument(id) {
        <%= AspxTools.JsGetElementById(m_requestId) %>.value = id;
        <%= AspxTools.JsGetElementById(m_requestAction) %>.value = 'cancelShare';
        __doPostBack('', '');
    }
    
    function previewShareDocument(id){  
        <%= AspxTools.JsGetElementById(m_requestId) %>.value = id;
        <%= AspxTools.JsGetElementById(m_requestAction) %>.value = 'previewShare';
        __doPostBack('', '');
    }
    
    function reviewRequest(id) {
        resetUserMsg();
        showModal('/newlos/ElectronicDocs/AcceptConsumerRequest.aspx?RID='+ id , null, null, null, function(modalArgs){ 
            if ( typeof(modalArgs.Status) == 'undefined' ) {
                return false;
            }
            else if ( modalArgs.Status == 'Cancel') {
                return false;
            }
            else if(modalArgs.Status == 'Accept') {
                acceptRequest(id, modalArgs.SignatureData);  
            }
            else if ( modalArgs.Status = 'Reject' ) {
                rejectRequest(id);
            }
        },{ hideCloseButton: true });
        return false;
    }
    
    function acceptRequest(id, signatureData ) {
        <%= AspxTools.JsGetElementById(m_requestId) %>.value = id;
        <%= AspxTools.JsGetElementById(m_requestAction) %>.value = 'Accept';
        <%= AspxTools.JsGetElementById(m_signatureData) %>.value = signatureData;
        __doPostBack('', '');
    }
    
    function activatePortal() {
        if (!hasDisabledAttr(<%=AspxTools.JsGetElementById(ActivateLink) %>))
        {
            <%= AspxTools.JsGetElementById(m_requestAction) %>.value = 'ActivateCPortal';
            __doPostBack('', '');
        }
    }
    
    function resendRequest(id) {
        resetUserMsg();
		<%= AspxTools.JsGetElementById(m_requestId) %>.value = id;
        <%= AspxTools.JsGetElementById(m_requestAction) %>.value = 'Resend';
        __doPostBack('', '');
    }
    
    function resetUserMsg() {
        <%= AspxTools.JsGetElementById(m_userMsg) %>.innerText = '';
    }
    
    function view(id){
        resetUserMsg();
        window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEDoc.aspx"))) %> + '?reqid=' + id, '_parent');
    }
</script>

<style type="text/css">
    a.ActivateLink
    {
        margin-left: 15px;
        <% if(ActivateLink.Disabled) {%>        
        cursor:default;
        <%}%>       
    }
    td.red {
        color: Red;
    }
    
    .QueueLabel 
    {
        color: Red;
    }
    
    a.Explain
    {
        margin-left: 5px;
    }
</style>

<asp:HiddenField id="m_signatureData" runat="server" />
<asp:HiddenField ID="m_requestAction" runat="server" />
<asp:HiddenField ID="m_requestId" runat="server" />
<ml:EncodedLabel ID="m_userMsg" runat="server" style="color:Green"></ml:EncodedLabel>

<div id="divTracking" runat="server">
    <table>
        <tr>
            <td class="FieldLabel" nowrap>
                Consumer Portal Application Creation Date:
            </td>
            <td nowrap>
                <asp:TextBox ID="sConsumerPortalCreationD" runat="server" Width="75" ReadOnly />
            </td>
            <td width="25px">
            </td>
            <td class="FieldLabel" nowrap>
                Consumer Portal Application Creation Type:
            </td>
            <td nowrap>
                <asp:TextBox ID="sConsumerPortalCreationT" runat="server" Width="90" ReadOnly />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" nowrap>
                Consumer Portal Application Submitted Date:
            </td>
            <td nowrap>
                <asp:TextBox ID="sConsumerPortalSubmittedD"  runat="server" Width="75" ReadOnly />
            </td>
        <asp:PlaceHolder runat="server" ID="phsConsumerPortalVerbalSubmissionD" Visible="false">
            <td width="25px">
            </td>
            <td class="FieldLabel" nowrap>
                Consumer Portal Verbal Submission Date:
            </td>
            <td nowrap>
                <ml:DateTextBox ID="sConsumerPortalVerbalSubmissionD" runat="server" preset="date" Width="75"></ml:DateTextBox>
            </td>
        </asp:PlaceHolder>
        </tr>
    </table>
</div>

<a href="#" onclick="activatePortal();" runat="server" id="ActivateLink" class="ActivateLink" onmouseover="if(hasDisabledAttr(this)) {this.style.cursor='default';} else{this.style.cursor='hand';}">activate borrower portal</a>
<asp:Repeater runat="server" ID="ConsumerPermissionRepeater" Visible="false" OnItemDataBound="ConsumerPermissionRepeater_OnItemDataBound">
    <HeaderTemplate>
        <br />The following consumers have access to this loan:
        <table id="NewConsumersTable" class="FormTable"  cellpadding="2" cellspacing="1"  >
        <thead>
            <tr class="GridHeader">
                <th>Email</td>
                <th>Last Login Date</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
    </HeaderTemplate>
    <FooterTemplate>
        </tbody>
        </table>
    </FooterTemplate>
    <ItemTemplate>
        <tr class="GridItem">
            <td>
                <ml:EncodedLiteral runat="server" ID="Email"></ml:EncodedLiteral>
            </td>
            <td>
                <ml:EncodedLiteral runat="server" ID="LastLogin"></ml:EncodedLiteral>
            </td>
            <td>
            <a href="#" runat="server" class="ResetPass" id="TempPasswordLink" >send temp password</a>
            </td>
        </tr>
    </ItemTemplate>
</asp:Repeater>
<asp:Repeater id="NoAccountsConsumersRepeaters" runat="server" Visible="false" OnItemDataBound="NoAccountsConsumersRepeaters_OnItemDataBound">
    <HeaderTemplate>
           <br />The following consumers do not have access to this loan
        <table id="NewConsumersTable" class="FormTable"  cellpadding="2" cellspacing="1"  >
        <thead>
            <tr class="GridHeader">
                <th>Borrower Name</td>
                <th>Email</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
    </HeaderTemplate>
    <FooterTemplate>
    </tbody>
    </table>
    <br />
    </FooterTemplate>
    <ItemTemplate>
          <tr class="GridItem">
            <td>
                <ml:EncodedLiteral runat="server" ID="BorrowerName"></ml:EncodedLiteral>
            </td>
            <td>
                <ml:EncodedLiteral runat="server" ID="Email"></ml:EncodedLiteral>
            </td>
            <td>
            <a href="#" runat="server" class="AccountCreate" id="CreateLink" >give access to loan</a>
            </td>
        </tr>
    </ItemTemplate>
</asp:Repeater>
<div>
<br />
<div>Document Request Queue:</div>
<div class="QueueLabel"><ml:EncodedLiteral runat="server" ID="QueueSendTimeLabel"></ml:EncodedLiteral></div>
</div>
<asp:Repeater ID="PendingDocumentRequestsTable" runat="server" Visible="true" OnItemDataBound="PendingDocumentRequestsTable_OnItemDataBound">
    <HeaderTemplate>
        <table id="PendingDocumentRequestTable" class="FormTable" cellpadding="2" cellspacing="1">
            <thead>
                <tr class="GridHeader">
                    <th></th>
                    <th>Request Type</th>
                    <th>Section</th>
                    <th>Doc Type</th>
                    <th>Application</th>
                    <th>Description</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr class="<%# AspxTools.HtmlString( Container.ItemIndex % 2 == 0 ? "GridItem" : "GridAlternatingItem")  %>">
            <td>
                <input type="checkbox" class="PendingDocumentRequestCB" NotForEdit />
                <asp:HiddenField runat="server" ID="RequestId" />
            </td>
            <td>
                <ml:EncodedLiteral runat="server" ID="Type"></ml:EncodedLiteral>
            </td>
            <td>
                <ml:EncodedLiteral runat="server" ID="Section"></ml:EncodedLiteral>
            </td>
            <td>
                <ml:EncodedLiteral runat="server" ID="DocType"></ml:EncodedLiteral>
            </td>
            <td>
                <ml:EncodedLiteral runat="server" ID="Application"></ml:EncodedLiteral>
            </td>
            <td>
                <ml:EncodedLiteral runat="server" ID="Description"></ml:EncodedLiteral>
            </td>
            <td>
                <ml:EncodedLiteral runat="server" ID="Status"></ml:EncodedLiteral><a href="#" id="ExplainLink" runat="server" class="Explain">?</a>
                <asp:HiddenField runat="server" ID="StatusExplanation" />
            </td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
            </tbody>
        </table>
    </FooterTemplate>
</asp:Repeater>
<input type="button" id="CreateRequest" value="Create Request" />
<input type="button" id="DeleteRequests" value="Delete Selected Requests" />
<input type="button" id="SendRequests" value="Send Selected Requests" />
<table id="ReceivedDocsTable" class="FormTable" style="table-layout: fixed" cellpadding="2" cellspacing="1"  >
    <tr class="GridHeader">
        <th>View</th>
        <th>Action</th>
        <th>Section</th>
        <th>Doc Type</th>
        <th>Application</th>
        <th>Description</th>
        <th>Received On</th>
    </tr>
    <asp:Repeater runat="server" ID="Documents" OnItemDataBound="Documents_OnItemDataBound">
        <ItemTemplate>
            <tr class="GridItem" id="row" runat="server">
                <td>
				    <a runat="server" id="ViewLink">view</a>
                    <ml:EncodedLiteral ID="ViewLiteral" Text="Not received yet" runat="server"></ml:EncodedLiteral>
                </td>
                <td>
				    <a runat="server" id="ReviewLink">accept/reject</a>
				    <a runat="server" id="CancelLink">cancel</a>
                    <ml:EncodedLiteral ID="DividerLiteral" Text=" / " runat="server"></ml:EncodedLiteral>
				    <a runat="server" id="ResendLink">resend</a>
                </td>
                <td>
                    <ml:EncodedLiteral ID="FolderDescription" runat="server"></ml:EncodedLiteral>
                </td>
                <td>
                    <ml:EncodedLiteral ID="DocumentTypeDescription" runat="server"></ml:EncodedLiteral>
                </td>
                <td>
                 <ml:EncodedLabel ID="Application" runat="server"></ml:EncodedLabel>
                </td>
                <td>
                    <ml:EncodedLiteral ID="RequestDescription" runat="server"></ml:EncodedLiteral>
                </td>
                <td>
                    <ml:EncodedLiteral ID="ConsumerCompletedDate" runat="server"></ml:EncodedLiteral>
                </td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="GridAlternatingItem" id="row" runat="server">
                <td>
				    <a runat="server" id="ViewLink">view</a>
                    <ml:EncodedLiteral ID="ViewLiteral" Text="Not received yet" runat="server"></ml:EncodedLiteral>
                </td>
                <td>
				    <a runat="server" id="ReviewLink">accept/reject</a>
				    <a runat="server" id="CancelLink">cancel</a>
                    <ml:EncodedLiteral ID="DividerLiteral" Text=" / " runat="server"></ml:EncodedLiteral>
				    <a runat="server" id="ResendLink">resend</a>
                </td>
                <td>
                    <ml:EncodedLiteral ID="FolderDescription" runat="server"></ml:EncodedLiteral>
                </td>
                <td>
                    <ml:EncodedLiteral ID="DocumentTypeDescription" runat="server"></ml:EncodedLiteral>
                </td>
                <td>
                   <ml:EncodedLabel ID="Application" runat="server"></ml:EncodedLabel>
                </td>
                <td>
                    <ml:EncodedLiteral ID="RequestDescription" runat="server"></ml:EncodedLiteral>
                </td>
                <td>
                    <ml:EncodedLiteral ID="ConsumerCompletedDate" runat="server"></ml:EncodedLiteral>
                </td>
            </tr>
        </AlternatingItemTemplate>
    </asp:Repeater>
    
    <br />

</table>

<asp:PlaceHolder runat="server" ID="SharedDocumentsNewPortal" Visible=false>
<br />
    Shared Documents:
    <asp:Repeater runat="server" ID="SharedDocumentsRepeater" OnItemDataBound="SharedDocumentsRepeater_OnItemDataBound">
        <HeaderTemplate>
            <table class="FormTable" style="table-layout: fixed" cellpadding="2" cellspacing="1">
                <tbody>
                  <tr class="GridHeader">
                    <td width=200>View</td>
                    <td>Action</td>
                    <td>Application</td>
                    <td>Description</td>
                    <td>Shared On</td>
                </tr>
        </HeaderTemplate>
        <FooterTemplate>
                </tbody>
                </table>
        </FooterTemplate>
        <ItemTemplate>
                <tr class='<%# AspxTools.HtmlString( Container.ItemIndex % 2 == 0 ? "GridItem" : "GridAlternatingItem")  %>'>
                    <td><ml:EncodedLiteral runat="server" ID="View"></ml:EncodedLiteral></td>
                    <td>
                        <a href="#" runat="server" id="cancel">cancel</a> /
                        <a href="#" runat="server" target="_blank" id="preview">preview</a>
                    </td>
                    <td runat="server" id="appCell" ><ml:EncodedLiteral runat="server" ID="Application"></ml:EncodedLiteral></td>
                    <td><ml:EncodedLiteral runat="server" ID="Description"></ml:EncodedLiteral></td>
                    <td><ml:EncodedLiteral runat="server" ID="SharedOn"></ml:EncodedLiteral></td>
                </tr>
        </ItemTemplate>
    </asp:Repeater>
</asp:PlaceHolder>
<asp:Panel runat="server" ID="NameMismatch" Visible="false" >
    <ml:EncodedLabel runat="server" ID="NameMismatchMessage" ForeColor="Red"></ml:EncodedLabel>
</asp:Panel>
