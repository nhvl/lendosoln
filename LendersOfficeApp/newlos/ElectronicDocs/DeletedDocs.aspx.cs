﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDocs;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using LendersOffice.Security;
using LendersOffice.AntiXss;
using DataAccess;
using MeridianLink.CommonControls;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class DeletedDocs : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            RegisterJsScript("jquery.tablesorter.min.js");
            
            var docs = EDocumentRepository.GetUserRepository().GetDeletedDocumentsByLoanId(LoanID, BrokerUser.BrokerId).OrderByDescending(p=>p.LastDeletedOn).ToList();
            var folders = (from c in docs
                           orderby c.Folder.FolderNm
                           select new
                           {
                               FolderName = c.Folder.FolderNm,
                               FolderID = c.Folder.FolderId
                           }).Distinct();


            FolderDD.DataSource = folders;
            FolderDD.DataValueField = "FolderID";
            FolderDD.DataTextField = "FolderName";
            FolderDD.DataBind();

            FolderDD.Items.Insert(0,new ListItem("All Folders", "-1")); 

            DeletedDocuments.DataSource = docs;
            DeletedDocuments.DataBind();
        }

        protected void DeletedDocuments_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }

            EDocument doc = (EDocument)args.Item.DataItem;

            HtmlInputCheckBox restore = (HtmlInputCheckBox)args.Item.FindControl("restore");
            HtmlAnchor viewLink = (HtmlAnchor)args.Item.FindControl("view");

            EncodedLiteral folder = (EncodedLiteral)args.Item.FindControl("Folder");
            EncodedLiteral docType = (EncodedLiteral)args.Item.FindControl("DocType");
            EncodedLiteral app = (EncodedLiteral)args.Item.FindControl("Application");
            EncodedLiteral desc = (EncodedLiteral)args.Item.FindControl("Description");
            EncodedLiteral pages = (EncodedLiteral)args.Item.FindControl("Pages");
            EncodedLiteral comments = (EncodedLiteral)args.Item.FindControl("InternalComments");
            PassthroughLiteral deletedBy = (PassthroughLiteral)args.Item.FindControl("DeletedBy");

            viewLink.HRef = Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx" + string.Format("?docid={0}&cmd=del", doc.DocumentId)));
            restore.Attributes.Add("docid", doc.DocumentId.ToString());
            restore.Attributes.Add("folderid", doc.Folder.FolderId.ToString());
            folder.Text = doc.Folder.FolderNm;
            docType.Text = doc.DocTypeName;
            app.Text = doc.AppName;
            desc.Text = doc.PublicDescription;
            pages.Text = doc.PageCount.ToString();
            comments.Text = doc.InternalDescription;
            deletedBy.Text = AspxTools.HtmlString(doc.LastDeletedBy) + "<br/>" + AspxTools.HtmlString(doc.LastDeletedOn);
        }

        [WebMethod]
        public static void RestoreDocs(string[] ids, string loanid)
        {
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            var docs = repo.GetDeletedDocumentsByLoanId(new Guid(loanid), BrokerUserPrincipal.CurrentPrincipal.BrokerId).ToDictionary(p=>p.DocumentId);

            foreach (string id in ids)
            {
                Guid docId = new Guid(id);
                EDocument doc = docs[docId];
                if (doc == null)
                {
                    continue;
                }

                repo.RestoreDocument(doc, "Restored");
            }
        }
    }
}
