﻿#region Auto-generated code
namespace LendersOfficeApp.newlos.ElectronicDocs
#endregion
{
    using System;
    using System.Linq;
    using LendersOffice.Common;
    using LendersOffice.Integration.DocumentSigning;
    using LendersOffice.Security;

    /// <summary>
    /// DocuSign recipient dashboard.
    /// </summary>
    public partial class DocuSignRecipientDashboard : BaseLoanPage
    {
        /// <summary>
        /// Determines whether DocuSign esigning is enabled.
        /// </summary>
        private Lazy<bool> isDocuSignESignEnabled = new Lazy<bool>(() =>
        {
            var settings = LendersOffice.Admin.DocuSign.LenderDocuSignSettings.Retrieve(PrincipalFactory.CurrentPrincipal.BrokerId);

            return PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCreatingDocumentSigningEnvelopes) && settings != null && settings.IsSetupComplete() && settings.DocuSignEnabled;
        });

        /// <summary>
        /// Forces the window to IE 10 mode.
        /// </summary>
        /// <returns>The compatability mode.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.IE10;
        }
        
        /// <summary>
        /// Loads the data for the page.
        /// </summary>
        protected override void LoadData()
        {
            if (!isDocuSignESignEnabled.Value)
            {
                throw new AccessDenied();
            }

            Guid loanId = RequestHelper.GetGuid("loanid");
            int envelopeId = RequestHelper.GetInt("envelopeid");

            var principal = PrincipalFactory.CurrentPrincipal;

            var envelope = DocuSignEnvelope.Load(principal.BrokerId, loanId, envelopeId);
            DocuSignEnvelopeViewModel viewModel = new DocuSignEnvelopeViewModel(envelope, EDocs.EDocumentRepository.GetUserRepository(PrincipalFactory.CurrentPrincipal).GetDocumentsByLoanId(loanId).ToDictionary(doc => doc.DocumentId));

            this.RegisterJsObjectWithJsonNetSerializer("Recipients", viewModel.Recipients);
            this.RegisterJsGlobalVariables("EnvelopeName", viewModel.Name);
        }

        /// <summary>
        /// Initialize function.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;

            this.PageTitle = "DocuSign Recipients";
            this.PageID = "DocuSignRecipientDashboard";

            EnableJqueryMigrate = false;
            this.RegisterJsScript("DocuSignRecipientDashboard.js");
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("SimplePopups.js");
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("angular-1.5.5.min.js");

            base.OnInit(e);
        }
    }
}
