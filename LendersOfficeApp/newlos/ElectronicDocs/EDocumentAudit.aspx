﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EDocumentAudit.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.EDocumentAudit" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Audit Events</title>
    <style type="text/css" >
        body { background-color: gainsboro; }
   
    </style>
   
</head>
<body>
<script type="text/javascript">
    function viewOriginal(docid) {
        window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?o=1&docid=' + docid, '_parent');
        return false;
    }
    
    function viewPopup(elem, event){
        var $elem = $(elem);
        $('#BeforeEdit').html($elem.attr('BeforeEdit'));
        $('#AfterEdit').html($elem.attr('AfterEdit'));
        Modal.ShowPopup("ExtraDetails", null, event);
        return false;
    }
</script>
    <h4 class="page-header"><ml:EncodedLiteral runat="server" ID="AuditHeader"></ml:EncodedLiteral></h4>
    <form id="form1" runat="server">
  <div style="padding: 5px">
    <a href="#" runat="server" id="ViewOriginalLink" >View Original Document</a>
  
        <asp:DataGrid Width="100%" AlternatingItemStyle-CssClass="GridAlternatingItem" OnItemDataBound="AuditEvents_OnItemDataBound" ItemStyle-CssClass="GridItem"  HeaderStyle-CssClass="GridHeader" runat="server" ID="AuditEvents" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundColumn   DataField="ModifiedDate" HeaderText="Date" ></asp:BoundColumn>
                <asp:TemplateColumn  HeaderText="Description" >
                    <ItemTemplate>
                        <ml:EncodedLabel runat="server" ID="Description"></ml:EncodedLabel>
                        <a href="#" runat="server" id="Details" onclick="return viewPopup(this, event);">details</a>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="Name" HeaderText="Name"></asp:BoundColumn>
            </Columns>
        </asp:DataGrid>
    </div>
    <div id="ExtraDetails" class="popup" style="z-index: 900; border-right: black 3px solid; padding-right: 10px;
        border-top: black 3px solid; display: none; padding-left: 10px; padding-bottom: 10px;
        border-left: black 3px solid; width: 325px; padding-top: 5px; border-bottom: black 3px solid;
        position: absolute; background-color: whitesmoke">
        <b>Before edit:</b>
        <br />
        <span id="BeforeEdit"></span>
        <br />
        <br />
        <b>After edit:</b >
        <br />
        <span id="AfterEdit"></span>    
        <center>
            <a href="#" onclick="Modal.Hide(); return false;" >Close</a>
        </center>
    </div>
			

    </form>
</body>
</html>
