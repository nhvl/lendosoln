﻿#region Auto-generated code
namespace LendersOfficeApp.newlos.ElectronicDocs
#endregion
{
    using System;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin.DocuSign;
    using LendersOffice.Common;
    using LendersOffice.Integration.DocumentSigning;
    using LendersOffice.Security;

    /// <summary>
    /// Service page for the DocuSignEnvelopeDashboard page.
    /// </summary>
    public partial class DocuSignEnvelopeDashboardService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Gets the DocuSign settings.
        /// </summary>
        private Lazy<LenderDocuSignSettings> docuSignSettings = new Lazy<LenderDocuSignSettings>(() =>
        {
            return LenderDocuSignSettings.Retrieve(PrincipalFactory.CurrentPrincipal.BrokerId);
        });

        /// <summary>
        /// Triggers a process by name.
        /// </summary>
        /// <param name="methodName">The name of the method to invoke.</param>
        protected override void Process(string methodName)
        {
            if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCreatingDocumentSigningEnvelopes) ||
                this.docuSignSettings.Value == null || !this.docuSignSettings.Value.IsSetupComplete() || !this.docuSignSettings.Value.DocuSignEnabled)
            {
                throw new AccessDenied();
            }

            switch (methodName)
            {
                case "RequestUpdate":
                    this.RequestUpdate();
                    break;
                case "RequestVoid":
                    this.RequestVoid();
                    break;
                case "ResumeSigning":
                    this.ResumeSigning();
                    break;
                default:
                    throw new ArgumentException($"Unknown method {methodName}");
            }
        }

        /// <summary>
        /// Updates the envelope manually.
        /// </summary>
        protected void RequestUpdate()
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            if (!principal.HasPermission(Permission.CanModifyLoanPrograms))
            {
                this.SetResult("Error", "Insufficient permissions.");
                return;
            }

            int id = this.GetInt("Id", -1);
            Guid loanId = this.GetGuid("LoanId");

            if (id == -1)
            {
                this.SetResult("Error", "No envelope chosen.");
                return;
            }

            LenderDocuSignSettings docuSignSettings = this.docuSignSettings.Value;

            DocuSignAuthenticationData authData = new DocuSignAuthenticationData(true, docuSignSettings.TargetEnvironment == DocuSignEnvironment.Test, docuSignSettings.IntegratorKey, docuSignSettings.DocuSignUserId, null, docuSignSettings.RsaPrivateKey);
            var envelope = DocuSignEnvelope.Load(principal.BrokerId, loanId, id);
            if (envelope == null)
            {
                this.SetResult("Error", "No matching envelope found.");
                return;
            }

            DocuSignDriver driver = new DocuSignDriver();
            var updatedEnvelopeResult = driver.GetAndUpdateEnvelope(authData, envelope);
            if (updatedEnvelopeResult.HasError)
            {
                Tools.LogError(updatedEnvelopeResult.Error);
                this.SetResult("Error", updatedEnvelopeResult.Error is CBaseException ? ((CBaseException)updatedEnvelopeResult.Error).UserMessage : ErrorMessages.Generic);
                return;
            }

            this.SetResult("Envelope", SerializationHelper.JsonNetAnonymousSerialize(new DocuSignEnvelopeViewModel(updatedEnvelopeResult.Value, EDocs.EDocumentRepository.GetUserRepository(PrincipalFactory.CurrentPrincipal).GetDocumentsByLoanId(loanId).ToDictionary(doc => doc.DocumentId))));
        }

        /// <summary>
        /// Voids the envelope.
        /// </summary>
        protected void RequestVoid()
        {
            int id = this.GetInt("Id", -1);
            Guid loanId = this.GetGuid("LoanId");
            string voidReason = this.GetString("VoidReason");

            if (id == -1)
            {
                this.SetResult("Error", "No envelope chosen.");
                return;
            }

            if (string.IsNullOrEmpty(voidReason))
            {
                this.SetResult("Error", "Please specify a void reason.");
                return;
            }

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            LenderDocuSignSettings docuSignSettings = this.docuSignSettings.Value;

            DocuSignAuthenticationData authData = new DocuSignAuthenticationData(true, docuSignSettings.TargetEnvironment == DocuSignEnvironment.Test, docuSignSettings.IntegratorKey, docuSignSettings.DocuSignUserId, null, docuSignSettings.RsaPrivateKey);
            var envelope = DocuSignEnvelope.Load(principal.BrokerId, loanId, id);
            if (envelope == null)
            {
                this.SetResult("Error", "No matching envelope found.");
                return;
            }

            DocuSignDriver driver = new DocuSignDriver();
            var result = driver.VoidEnvelope(authData, envelope.EnvelopeId, voidReason);
            if (result.HasError)
            {
                Tools.LogError(result.Error);
                this.SetResult("Error", ErrorMessages.Generic);
                return;
            }

            // We have successfully voided so we need to update the envelope record.
            envelope.VoidAndSave(voidReason, principal);

            this.SetResult("Envelope", SerializationHelper.JsonNetAnonymousSerialize(new DocuSignEnvelopeViewModel(envelope, EDocs.EDocumentRepository.GetUserRepository(PrincipalFactory.CurrentPrincipal).GetDocumentsByLoanId(loanId).ToDictionary(doc => doc.DocumentId))));
        }

        /// <summary>
        /// Resumes signing a DocuSign envelope with in-person signers.
        /// </summary>
        private void ResumeSigning()
        {
            int envelopeId = this.GetInt("Id", -1);
            Guid loanId = this.GetGuid("LoanId");

            if (envelopeId == -1)
            {
                this.SetResult("Error", "No envelope chosen.");
                return;
            }

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            var envelope = DocuSignEnvelope.Load(principal.BrokerId, loanId, envelopeId);

            int? nextSigner = envelope?.Recipients.FirstOrDefault(recipient => recipient.Type == RecipientType.InPersonSigner && recipient.CurrentStatus != DocuSignRecipientStatus.Signed && recipient.CurrentStatus != DocuSignRecipientStatus.Completed)?.SigningOrder;

            if (nextSigner == null)
            {
                this.SetResult("Error", "There is no signer to resume with.");
                return;
            }

            string state = DocuSignRecipientView.GenerateStateToken(principal, loanId);
            var callbackUri = DocuSignRecipientView.GetRecipientViewCallbackUri(
                urlBase: System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + Tools.VRoot,
                loanId: loanId,
                envelopeId: envelopeId,
                nextSigningOrder: nextSigner.Value,
                state: state);
            this.SetResult("RecipientViewUrl", callbackUri);
        }
    }
}