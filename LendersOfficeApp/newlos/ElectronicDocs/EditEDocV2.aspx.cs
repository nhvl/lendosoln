namespace LendersOfficeApp.newlos.ElectronicDocs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    public partial class EditEDocV2 : BaseLoanPage
    {
        private Guid? m_appId = Guid.Empty;

        protected bool IsUsingAmazonEdocs
        {
            get { return Broker.IsTestingAmazonEdocs; }
        }
        protected bool EnableDuplicateEDocProtectionChecking
        {
            get
            {
                return Broker.EnableDuplicateEDocProtectionChecking;
            }
        }
        protected Guid DocId
        {
            get
            {
                Guid id; 
                if (IsBatch)
                {
                    var orderedDocs = GetOrderedDocuments();
                    if (!orderedDocs.Any()) return Guid.Empty;

                    id = orderedDocs.First().First().DocumentId;
                }
                else
                {
                    id = RequestHelper.GetGuid("docid");
                }

                return id;
            }
        }

        protected string AppId
        {
            get { return m_appId.ToString(); }
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Determines whether the specified e-doc is editable.
        /// </summary>
        /// <param name="eDoc">
        /// The document to check.
        /// </param>
        /// <param name="forListing">
        /// True if this call is used to determine the list of documents
        /// to display in batch, false otherwise.
        /// </param>
        /// <returns>
        /// True if the document is editable, false otherwise.
        /// </returns>
        private bool IsEditable(EDocument eDoc, bool forListing)
        {
            if (!this.HasPermissionsToEditEdoc(eDoc))
            {
                return false;
            }

            if (this.IsBatch && ConstStage.AllowESignedDocsInBatchEditor && forListing && eDoc.IsESigned)
            {
                return eDoc.CanRead();
            }

            return eDoc.CanEdit;
        }

        private bool HasPermissionsToEditEdoc(EDocument edoc)
        {
            if (!this.BrokerUser.HasPermission(Permission.CanEditEDocs))
            {
                return false;
            }

            if (edoc.DocStatus == E_EDocStatus.Approved &&
                !this.BrokerUser.HasPermission(Permission.AllowEditingApprovedEDocs))
            {
                return false;
            }

            return true;
        }

        protected bool AllowHideEDocsFromPML
        {
            get { return BrokerUser.HasPermission(Permission.AllowHideEDocsFromPML); }
        }

        private const int MaxSignatureWidth = 100;
        private const int MaxSignatureHeight = 50;

        protected override void LoadData()
        {
            if (!this.BrokerUser.HasPermission(Permission.CanViewEDocs))
            {
                HandleException(new AccessDenied());
                return;
            }


            // Load Edocs Page Information.
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            EDocument eDoc;
            try
            {
                if (IsBatch)
                {
                    EDocsMigration.RequestMigrationForLegacyEdocs(this.BrokerID, this.LoanID);

                    var requestedDocs = GetOrderedDocuments();

                    BatchEditList.Visible = true;
                    BatchEditList.DataSource = requestedDocs;
                    BatchEditList.DataBind();

                    // This will select the values, i.e. documents, in the grouping
                    var documentsToDisplay = requestedDocs.SelectMany(grouping => grouping);
                    if (documentsToDisplay.Any())
                    {
                        // The representative EDoc for the batch editor should be the
                        // first document displayed in the list using the default filters.
                        var firstDocMatchingDefaultFilters = documentsToDisplay.FirstOrDefault(doc => doc.DocStatus.EqualsOneOf(E_EDocStatus.Blank, E_EDocStatus.Approved, E_EDocStatus.Screened));
                        eDoc = firstDocMatchingDefaultFilters ?? documentsToDisplay.First();
                    }
                    else
                    {
                        //If we don't have any docs, just show an access denied exception.
                        //This should only happen during the smoke test.
                        HandleException(new AccessDenied("Unable to open Batch Editor because there are no EDocs to edit.", "Unable to open Batch Editor because there are no EDocs to edit."));
                        return;
                    }

                    if (ConstStage.AllowESignedDocsInBatchEditor)
                    {
                        var nonEditableDocs = documentsToDisplay.Where(doc => !this.IsEditable(doc, forListing: false)).Select(doc => doc.DocumentId);
                        var signedDocs = documentsToDisplay.Where(doc => doc.IsESigned).Select(doc => doc.DocumentId);

                        this.RegisterJsObjectWithJsonNetAnonymousSerializer("NonEditableDocIds", nonEditableDocs);
                        this.RegisterJsObjectWithJsonNetAnonymousSerializer("SignedDocIds", signedDocs);
                    }
                }
                else
                {
                    eDoc = repo.GetDocumentById(DocId);
                    btnFilter.Visible = false;
                }
            }
            catch (CBaseException)
            {
                //The AccessDenied we get out of the function just says "Access Denied", so give the user a better message.
                HandleException(new CBaseException("Another user has moved or deleted this eDoc. Please select a different one.", "eDoc was moved or deleted"));
                return;
            }

            if (!IsBatch && eDoc.RequiresDocTypeChange && repo.CanUpdateDocument(eDoc))
            {
                Response.Redirect("ForceChangeDocType.aspx?loanid=" + RequestHelper.GetGuid("loanid") + "&docid=" + eDoc.DocumentId, true);
                return;
            }
            if (eDoc.PdfHasUnsupportedFeatures)
            {
                string exc = "You have reached the page in error, as it has a signature and is not editable";
                HandleException(new CBaseException(exc,exc));
            }

            CPageData loanData = CPageData.CreateUsingSmartDependency(LoanID, typeof(EditEDocV2));
            loanData.InitLoad();
            CAppData app = loanData.GetAppData(0);
            this.Header.Title = string.Format("{0} - {1} - EDoc Editor - {2}: {3} - {4}", app.aBLastFirstNm, loanData.sLNm, eDoc.Folder.FolderNm, eDoc.DocTypeName, (eDoc.PublicDescription + " " + eDoc.InternalDescription).TrimWhitespaceAndBOM());

            if (eDoc.LoanId != this.LoanID)
            {
                // 1/7/2010 dd - This to make sure that a document is belong to this loan file.
                HandleException(new AccessDenied());
                return;
            }
            ConditionAssociations.Visible = Broker.IsUseNewTaskSystem;

            if (this.Broker.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(BrokerUser.BrokerId, BrokerUser.EmployeeId, BrokerUser.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);

                Signature.Visible = signInfo.HasUploadedSignature && eDoc.CanEdit && BrokerUser.HasPermission(Permission.CanEditEDocs);
                SignatureImg.ImageUrl = "UserSignature.aspx?eid=" + BrokerUser.EmployeeId;

                if (signInfo.HasUploadedSignature)
                {
                    var details = signInfo.SignatureDetails;
                    double signatureHeight = Math.Min(MaxSignatureHeight, details.Height);
                    double signatureWidth = Math.Min(MaxSignatureWidth, details.Width);

                    //aspect ratio = width/height
                    var heightFromWidth = signatureWidth / details.AspectRatio;
                    var widthFromHeight = signatureHeight * details.AspectRatio;

                    if (heightFromWidth > signatureHeight) signatureHeight = heightFromWidth;
                    else if (widthFromHeight > signatureWidth) signatureWidth = widthFromHeight;

                    SignatureImg.Width = new Unit(signatureWidth, UnitType.Pixel);
                    SignatureImg.Height = new Unit(signatureHeight, UnitType.Pixel);
                    SignatureImg.Attributes.Add("ar", details.AspectRatio.ToString("0.00"));
                }
            }

            Fields.Visible = BrokerUser.HasPermission(Permission.CanEditEDocsInternalNotes);
            DragAndDropTable.Visible = Signature.Visible || Fields.Visible;

            this.RegisterJsGlobalVariables("UseFallbackEditBehavior", !this.IsBatch || !ConstStage.AllowESignedDocsInBatchEditor);
            this.RegisterJsGlobalVariables("CanEdit", this.IsEditable(eDoc, forListing: false));

            PasteAnnotations.Visible = eDoc.CanEdit && BrokerUser.HasPermission(Permission.CanEditEDocs) && BrokerUser.HasPermission(Permission.CanEditEDocsInternalNotes); 
            
            // We allow editing these even if the PDF is password protected or Accepted already, so only the CanRead permission needs to be checked for proper role, etc
            m_docTypeList.Enabled = eDoc.CanRead() && BrokerUser.HasPermission(Permission.CanEditEDocs);
            m_docDescription.Enabled = eDoc.CanRead() && BrokerUser.HasPermission(Permission.CanEditEDocs); 
            m_publicDescription.Enabled = eDoc.CanRead() && BrokerUser.HasPermission(Permission.CanEditEDocs);  
            m_appId = eDoc.AppId;

            m_docDescription.Text = eDoc.InternalDescription;
            m_publicDescription.Text = eDoc.PublicDescription;

            // If this is an old document type, the user won't be able to select it through the picker.
            var docs = EDocumentDocType.GetDocTypesByBroker(BrokerUser.BrokerId, E_EnforceFolderPermissions.True).ToList();
            var previouslySelectedDoc = docs.Find((DocType d) =>
                d.DocTypeId == eDoc.DocumentTypeId.ToString()
            );

            if (previouslySelectedDoc != null)
            {
                m_docTypeList.ValueCtrl.Text = previouslySelectedDoc.DocTypeId;
                m_docTypeList.DescriptionCtrl.Text = previouslySelectedDoc.FolderAndDocTypeName;
            }
            else
            {
                m_docTypeList.ValueCtrl.Text = eDoc.DocumentTypeId.ToString();
                m_docTypeList.DescriptionCtrl.Text = eDoc.DocTypeName;
            }
            
            int pageCount = eDoc.PageCount;

            Version.Value = eDoc.Version.ToString();
            List<PdfPageItem> pages = eDoc.GetPdfPageInfoList();

            m_docStatus.Items.Add(new ListItem(EDocument.StatusText(E_EDocStatus.Blank), E_EDocStatus.Blank.ToString("d")));

            if(BrokerUser.HasPermission(Permission.AllowScreeningEDocs))
            {
                m_docStatus.Items.Add(new ListItem(EDocument.StatusText(E_EDocStatus.Obsolete), E_EDocStatus.Obsolete.ToString("d")));
            }

            if (BrokerUser.HasPermission(Permission.AllowApprovingRejectingEDocs))
            {
                m_docStatus.Items.Add(new ListItem(EDocument.StatusText(E_EDocStatus.Approved), E_EDocStatus.Approved.ToString("d")));
                m_docStatus.Items.Add(new ListItem(EDocument.StatusText(E_EDocStatus.Rejected), E_EDocStatus.Rejected.ToString("d")));
            }
            if(BrokerUser.HasPermission(Permission.AllowScreeningEDocs))
            {
                m_docStatus.Items.Add(new ListItem(EDocument.StatusText(E_EDocStatus.Screened), E_EDocStatus.Screened.ToString("d")));
            }
            m_docStatus.Enabled = eDoc.CanRead() && (BrokerUser.HasPermission(Permission.AllowScreeningEDocs) || BrokerUser.HasPermission(Permission.AllowApprovingRejectingEDocs));
                
            m_docStatus.SelectedValue = ((E_EDocStatus)eDoc.DocStatus).ToString("d");
            m_docStatusDescription.Text = eDoc.StatusDescription;

            m_HideFromPML.Checked = eDoc.HideFromPMLUsers;

            // OPM 64817 - NOTE: ASP.Net control.visible flag removes control from DOM when hidden.
            // Set visibility through css style instead so control can still be accessed via javascript.
            ulHideFromPML.Style.Add(HtmlTextWriterStyle.Visibility, "hidden");

            // Only unhide if user has permission to hide docs from PML
            if (AllowHideEDocsFromPML)
            {
                foreach (EDocRoles edr in eDoc.Folder.AssociatedRoleIds)
                {
                    if ((edr.RoleId == Role.Get(E_RoleT.Administrator).Id && edr.IsPmlUser)
                        || (edr.RoleId == Role.Get(E_RoleT.LoanOfficer).Id && edr.IsPmlUser))
                    {
                        ulHideFromPML.Style.Add(HtmlTextWriterStyle.Visibility, "");
                        break;
                    }
                }
            }
            
            m_docStatus.Attributes["onChange"] = "onStatusChange();";

            //When the “approved” status is selected, all users without the “Allow editing approved EDocs” permission will be unable to edit this document
            if (m_docStatus.SelectedValue == E_EDocStatus.Approved.ToString("d") && !BrokerUser.HasPermission(Permission.AllowEditingApprovedEDocs))
            {
                PasteAnnotations.Visible = false;
            }

            this.ClientScript.RegisterHiddenField("EditConditionsOnly", (eDoc.DocStatus == E_EDocStatus.Approved &&
                                                            BrokerUser.HasPermission(Permission.AllowEditingApprovedEDocs)).ToString());

            this.RegisterJsObject("PagesWithAnnotations", eDoc.GetPagesWithAnnotationsAndTags());
            this.RegisterJsObject("Annotations", eDoc.InternalAnnotations.GenerateEditorList(BrokerUserPrincipal.CurrentPrincipal));
            this.RegisterJsObject("ESignTags", eDoc.ESignTags.GenerateEditorList(BrokerUserPrincipal.CurrentPrincipal));
            this.RegisterJsObject("EDocPages", pages);
            this.RegisterJsObject("DocDetails", new EDocBatchEditDetails(eDoc, 0));
            //Apparently RegisterJsObject doesn't like guids, so we'll use a hidden field.
            this.ClientScript.RegisterHiddenField("DocId", eDoc.DocumentId.ToString());
            this.ClientScript.RegisterHiddenField("CurrentDate", DateTime.Now.ToShortDateString());

            if (repo.GetDocumentsByLoanId(LoanID).Count <= 1)
            {
                ClientScript.RegisterHiddenField("DisableInsert", "There are no other documents in this loan");
            }

            var docuSignConfig = LendersOffice.Admin.DocuSign.LenderDocuSignSettings.Retrieve(this.BrokerUser.BrokerId);
            var canAssignTagsToEdoc = docuSignConfig != null && docuSignConfig.IsSetupComplete() && docuSignConfig.DocuSignEnabled &&
                               this.BrokerUser.HasPermission(Permission.AllowCreatingDocumentSigningEnvelopes) && (this.IsBatch || this.HasPermissionsToEditEdoc(eDoc));

            // For batch mode, the docs that cannot be edited by the user are filtered out. Thus the ESign checks are sufficient for showing the tag tab.
            // For single mode, any doc can be opened up in the editor, thus we need to check if the user has permission to edit docs in order to show the tag tab.
            ESignTagsTab.Visible = canAssignTagsToEdoc;
            ViewESignTags.Visible = canAssignTagsToEdoc;
            this.RegisterJsGlobalVariables("CanEditESignTags", canAssignTagsToEdoc);
        }
        private void HandleException(CBaseException exc)
        {
            //Register some empty pages and annotations so things don't chuck exceptions
            this.RegisterJsObject("PagesWithAnnotations", new List<int>());
            this.RegisterJsObject("Annotations", new List<EDocumentAnnotationItem>());
            this.RegisterJsObject("EDocPages", new List<PdfPageItem>());
            this.AddInitScriptFunction("displayError", "function displayError() { ForceQuit(" + AspxTools.JsString(exc.UserMessage) + ");}");

        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.IE9;
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            BatchEditList.Visible = false;
        
            DisplayCopyRight = false;
            
            if (this.Header != null)
            {
                this.RegisterCSS("jquery-ui-1.11.custom.css");
                this.RegisterCSS("PdfEditor.css");
            }

            this.RegisterJsScript("json.js");
            EnableJqueryMigrate = false;
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("PdfEditor.js");
            this.RegisterCSS("EditEdoc.css");
            this.RegisterService("loanedit", "/newlos/ElectronicDocs/EditEDocService.aspx");

            //this.PageTitle = "PAGE TITLE";
            //this.PageID = "PageId"; // Must match with PageId in LoanNavigation.xml.config
            //this.PDFPrintClass = typeof(PDFPrintClass); // Only if this page has a pdf printing class.
            UniqueKey.Value = Guid.NewGuid().ToString();
            CanEditAnnotations.Value = BrokerUser.HasPermission(Permission.CanEditEDocsInternalNotes).ToString();
            InternalNoteTab.Visible = BrokerUser.HasPermission(Permission.CanViewEDocsInternalNotes);
            ViewInternalNotes.Visible = InternalNoteTab.Visible;
            CanEditEdocs.Value = BrokerUser.HasPermission(Permission.CanEditEDocs).ToString();
            LendersOffice.Rolodex.RolodexDB.PopulateAgentTypeDropDownList(AgentRole, useShortened: true);
        }

        protected override void OnInit(EventArgs e)
        {
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }

        protected bool IsBatch
        {
            get
            {
                return !string.IsNullOrEmpty(RequestHelper.GetSafeQueryString("key"));
            }
        }

        private ILookup<int, EDocument> GetOrderedDocuments()
        {
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            string key = RequestHelper.GetSafeQueryString("key");
            IList<EDocument> documents;

            if (string.IsNullOrEmpty(key))
            {
                documents = new List<EDocument>();
            }
            else
            {
                if (key.Equals("all", StringComparison.InvariantCultureIgnoreCase))
                {
                    documents = repo.GetDocumentsByLoanId(RequestHelper.GetGuid("loanid"));
                }
                else
                {
                    List<Guid> doocumentIds = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<Guid>>(AutoExpiredTextCache.GetFromCache(RequestHelper.GetSafeQueryString("key")));
                    List<EDocument> workingSet = new List<EDocument>();
                    foreach (Guid id in doocumentIds)
                    {
                        EDocument doc = repo.GetDocumentById(id);
                        workingSet.Add(doc);
                    }
                    documents = workingSet;

                }
            }

            return documents
                .Where(p => p.ImageStatus == E_ImageStatus.HasCachedImages && IsEditable(p, forListing: true))
                .OrderBy(p => p.FolderAndDocTypeName)
                .ToLookup(p => p.FolderId);
        }
    }
}
