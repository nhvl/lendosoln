﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AcceptConsumerRequest.aspx.cs" EnableViewState="false" Inherits="LendersOfficeApp.newlos.ElectronicDocs.AcceptConsumerRequest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <script type="text/javascript" src="../../inc/json.js"></script>
    <link type="text/css" rel="Stylesheet"  href="../../css/jquery-ui-1.11.custom.css" />
    <link type="text/css" rel="Stylesheet" href="../../css/pdfeditor.css" />
    <title>Accept Consumer Request</title>
    <script type="text/javascript" src="../../inc/jquery-ui-1.11.4.min.js"></script>    
    <script type="text/javascript" src="../../inc/PdfEditor.js"></script>
    <style type="text/css">
        body { font-weight: bold; }
    </style>
    <style type="text/css">
        body,
        html {
            background-color: gainsboro;
            overflow-x: hidden; 
        }

        #loadingmask { 
            display: block; 
            position:absolute;  
            background-color: Gray; 
            filter:alpha(opacity=60); 
            opacity: 0.6; 
            top : 0;
            bottom: 0;
            left: 0;
            right: 0;
            
        }
    </style>
    
</head>
<body>
    <script type="text/javascript">
           
        
        $(function(){
            var div = $('#sigDiv');
            if( div.length > 0 ) {
            var width = div.children('img').width();
            var height = div.children('img').height();
            div.height(height);
            div.width(width);
            div.children('img').width('100%');
            div.children('img').height('100%');
            }
            resizeForIE6And7(950,800);
            var uniqueKey = Math.round(Math.random()*10000000);
            var rid = $('#RID').val();
            var rot = $('#rot').val(); 
            var  pdfEditor = new LendersOffice.PdfEditor('PdfEditor', PdfPages, {
                    GenerateFullPageSrc : function(o) {
                        var data = [<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/AcceptConsumerRequest.aspx"))) %> + '?Pic=1&RID=',rid,'&',o.Page,'&pg=',o.Page,'&key=',uniqueKey, '&rot=',rot]; 
                        return data.join(''); 
                    },
                    
                    AllowEdit: function () { return true; },
                    EmptyImgSrc: gVirtualRoot + '/images/pixel.gif'                    
                }
            );
            pdfEditor.Initialize(function() { $('#loadingmask').hide();  } );
            var acceptBtn = $('#AcceptDocument'); 
            $('#AcceptCB').click(function(){
                acceptBtn.prop("disabled", !$(this).is(':checked'));
            });
            
            $('#AcceptDocument, #RejectDocument, #Cancel').click(function(){
                 var arg = window.dialogArguments || {};
                switch(this.id){
                    case 'RejectDocument': 
                        arg.Status = 'Reject';
                        break;
                    case 'AcceptDocument':
                        arg.SignatureData =  _GetSignatureSaveData();
                        if( arg.SignatureData !== '[]') {
                            if( false == confirm('Signatures will be permanently applied to this document.') ) {
                                return;
                            }
                        }
                        arg.Status = 'Accept';
                        break;
                    case 'Cancel':
                        arg.Status = 'Cancel';
                        break;
                    default: 
                        alert("Unhandled id");
                }
                
                onClosePopup(arg);
            });  
            
            function _GetSignatureSaveData(){
                var signatures = [];
                $.each(pdfEditor.GetPdfFields(), function(i,o){
                    if( o.cssClass == 'pdf-field-signature-img' || o.cssClass == 'pdf-field-date' ) {
                        signatures.push(o);
                    }
                });
                return JSON.stringify(signatures);
            }
        
            $(document).keyup(function(e){
            
                var keyPressed = e.which;
                if( keyPressed !== 46 && keyPressed !== 8 ) {
                    return;
                }
                $.each(pdfEditor.GetSelectedPdfFields(),function(idx,o){
                    var $this  = $(o);
                    pdfEditor.RemovePdfField(o);
                });

            });   
            $('.pdf-field-template').draggable({
                revert:'invalid', 
                helper:'clone', 
                zIndex: 2700, 
                start : function() {
                    $('#editor').css('z-index', '-1' ); //needed for 6 and 7 so the draggable doesnt end in the back of the editor
                },
                stop : function() {
                    $('#editor').css('z-index', '' ); 
                }
            });
         });
         

    </script>
    <h4 class="page-header">Review</h4>
    <div id="loadingmask" ></div>
    <form id="form1" runat="server">
    <asp:HiddenField runat="server" ID="RID" />
    <asp:HiddenField runat="server" ID="PageCount" />
    <asp:HiddenField runat="server" ID="Rotation" />
    <div id="PdfEditor">
    
    </div>
    <asp:Panel runat="server" ID="SignaturePanel" Visible="false">
        Drag and drop signature into place on document to sign
        <table>
            <tr>
                <td>
                    <div class="pdf-field-template pdf-field-signature-img" id="sigDiv">
                        <asp:Image ID="Image2" CssClass="pdf-field-content" runat="server" ImageUrl="~/images/edocs/signature.png"
                            Width="100%" Height="100%" />
                    </div>
                </td>
                <td>
                    <div class="pdf-field-template pdf-field-date">
                        <asp:Image ID="DateIMg" CssClass="pdf-field-content" runat="server" ImageUrl="~/ViewEDocPdf.aspx?cmd=date"
                            Width="100%" Height="100%" />
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    
    <div>
        <label >
        <input type="checkbox" id="AcceptCB" />  I have reviewed and verified the correctness of this form. 
        </label>
        
        <input type="button" class="ButtonStyle" id="AcceptDocument" value="Accept Document" disabled="disabled" />
        <input type="button" class="ButtonStyle" id="RejectDocument" value="Reject Document" />
        <asp:Button runat="server" ID="Rotate" CssClass="ButtonStyle"  Visible="false"  Text="Rotate pages 180 degrees" UseSubmitBehavior="false" OnClientClick="this.disabled = true;" OnClick="RotatePage" />
        <input type="button" class="ButtonStyle" id="Cancel" value="Cancel"  />

    </div>
    <uc1:cModalDlg runat="server" ID="modal" />
    
    </form>


    
</body>
</html>
