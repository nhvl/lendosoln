﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InsertEDocPage.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.InsertEDocPage" EnableViewState="false"%>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style>
    .padding {
      padding-left:10px;
    }
    .page_textbox 
    {
      width:30px;
    }    
    
    #Options
    {
        margin: 5px 0px 12px 15px;
        padding: 0px;
        list-style-type: none;
    }
    
    li.DeleteFromSource
    {
        margin-top: 15px;
    }
    
    #Controls
    {
        width: 100px;
        position: relative;
        left: 50%;
        margin-left: -50px;
    }
    .Action
    {
        color: Blue;
        cursor: pointer;
        text-decoration: underline;
    }
    </style>      
</head>
<body bgcolor="gainsboro" onunload="getModalArgs().callback()">
  <script type="text/javascript">
    var g_iCurrentPage = 0;
    var g_iTotalPages = 0;
    var g_docId = null;
    var g_docVersion = null;
    var g_docPageCount = null;
    var docToInsert = null;
    
    function _init() {
      
      if(window.opener && window.opener.SetupModelessEnvironment){
          window.opener.SetupModelessEnvironment(window);
      }

      resize(500, 450);
      
      var arg = getModalArgs() || {};
      g_iTotalPages = arg.TotalPages;
      document.getElementById("NumOfPagesLabel").innerText = g_iTotalPages;         

      g_iCurrentPage = arg.CurrentPage;

      if ( arg.SelectedRanges == null || arg.SelectedRanges[0]  == null){
        document.getElementById('tbNewLocation').value = 1;
        return;
      }
      
      var selectionRange = arg.SelectedRanges[0];
      
      var selectionFrom = selectionRange.Start ? selectionRange.Start + 1 : 1;
      var selectionTo = selectionFrom + (selectionRange.Count ? selectionRange.Count - 1 : 0);


      document.getElementById("replaceFrom").value = selectionFrom;
      document.getElementById("replaceTo").value = selectionTo;
      
      document.getElementById('tbNewLocation').value = selectionTo;
    }
    function onOK() {
      
      if(!checkRange(document.getElementById("docToInsertBegin"), document.getElementById("docToInsertEnd"), docToInsert.length))
      {
        return;
      }
      
      var sliceBegin = document.getElementById("docToInsertBegin").value - 1;
      var sliceEnd = document.getElementById("docToInsertEnd").value;
      
      var arg = getModalArgs() || {};
      arg.DocId = g_docId;
      arg.DocVersion = g_docVersion;
      arg.DocPageCount = g_docPageCount;
      arg.ReplaceCount = 0;
      var afterPage = 0;
      if (<%= AspxTools.JsGetElementById(rbBeforeFirstPage) %>.checked) 
      {
        afterPage = 0;
      } else if (<%= AspxTools.JsGetElementById(rbAfterLastPage) %>.checked) {
        afterPage = g_iTotalPages;
      } else if (<%= AspxTools.JsGetElementById(rbAfterPage) %>.checked) {
        var pg = parseInt(document.getElementById("tbNewLocation").value);
        if (isNaN(pg) || pg <= 0 || pg > g_iTotalPages || pg.toString() != document.getElementById("tbNewLocation").value) {
          alert('Invalid Page Number.');
          document.getElementById("tbNewLocation").focus();
          return;
        }
        afterPage = pg;
        
      } else if (<%= AspxTools.JsGetElementById(rbReplacePages) %>.checked)
      {
        if(!checkRange(document.getElementById("replaceFrom"), document.getElementById("replaceTo"), null))
        {
            return;
        }
        //Replacement is inclusive, (e.g, replace from page 2 to 3 means replace both pages 2 and 3) so we need to add one
        arg.ReplaceCount = document.getElementById("replaceTo").value - document.getElementById("replaceFrom").value + 1;
        afterPage = document.getElementById("replaceFrom").value - 1;
      }
      
      else {
        alert('Invalid choice.');
        return;
      }
      arg.AfterPage = afterPage;
      arg.OK = true;
      arg.DeleteFromSource = document.getElementById("DeleteFromSourceDocument").checked;
      if(arg.DeleteFromSource)
      {
        var temp = [];
        var pagesToKeep = docToInsert.slice(0, sliceBegin).concat(docToInsert.slice(sliceEnd));
        arg.NewSource = JSON.stringify(pagesToKeep);
      }
      //No sense in making the caller grab this data again - even though we have to 
      //perform a cross-window translation, it's still faster than hitting the server.
      arg.docToInsert = JSON.stringify(docToInsert.slice(sliceBegin, sliceEnd));
      onClosePopup(arg);
    }
    function checkRange(fromField, toField, max)
    {
        var rangeFrom = parseInt(fromField.value);
        var rangeTo = parseInt(toField.value);
        
        //Make sure the user is entering sane values
        
        if(rangeFrom.toString() != fromField.value)
        {
            alert("Invalid entry: can only begin from a number (are there any stray space characters?)");
            fromField.focus();
            return false;
        }
        
        if(rangeFrom < 1)
        {
            alert("Invalid number to begin from; it must be at least 1");
            fromField.focus();
            return false;
        }
        
        if(rangeTo.toString() != toField.value)
        {
            alert("Invalid entry: can only end with a number (are there any stray space characters?)");
            toField.focus();
            return false;
        }
        
        if(max && rangeTo > max)
        {
            alert("Invalid number to end with; it may be at most " + max);
            toField.focus();
            return false;
        }
        
        if(rangeFrom > rangeTo)
        {
            alert("Invalid range - cannot go from a higher number to a lower one");
            fromField.focus();
            return false;
        }
        //Sanity prevails
        return true;
    }
    
    function onCancel() {
      onClosePopup();
    }
    
      function selectEDoc(o) {
          var pageLink = $(o);
      g_docId = pageLink.siblings('input.DocId').val();
      g_docVersion = pageLink.siblings('input.Version').val();
      g_docPageCount = parseInt(pageLink.attr('pagecount'));
      
      var args = { docid : g_docId};
      var result = gService.loanedit.call("GetEdocsPageInfo", args);
      if( result.error) {
          var errMsg = 'Unable to insert page. Please try again.';
          if (null != result.UserMessage) {
              errMsg = result.UserMessage;
          }
          alert(errMsg);
          onClosePopup();
      } 
      else
      {
        docToInsert = JSON.parse(result.value.EDocPages);
      }
      

      document.getElementById("SelectFilesPanel").style.display = "none";
      document.getElementById("InsertPagePanel").style.display = "";
      document.getElementById("DocType").innerText = pageLink.attr('doctype');
      document.getElementById("docToInsertEnd").value = docToInsert.length;
      document.getElementById("docToInsertMaxPages").innerHTML = docToInsert.length;

      var isSigned = result.value.IsSigned === "True";
      $('.DeleteFromSource input').prop('disabled', isSigned);
      $('#DeleteFromSourceExplanation').toggle(isSigned);

      if (isSigned) {
          $('#DeleteFromSourceDocument').prop('checked', false);
      }
    }
    
      function viewEDoc(o) {
          var pageLink = $(o);
      window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + pageLink.attr('docid'), '_self');
      return false;
    }
    
    function renderUI() {
      var bIsAfterSpecificPage = <%= AspxTools.JsGetElementById(rbAfterPage) %>.checked;
      var bReplacePages = <%= AspxTools.JsGetElementById(rbReplacePages) %>.checked;

      if (!bIsAfterSpecificPage) {
        document.getElementById("tbNewLocation").value = "";
      }
      
      if (!bReplacePages) {
        document.getElementById("replaceFrom").value = "";
        document.getElementById("replaceTo").value = "";
      }
    }
    
    function enablePageRange() {
      <%= AspxTools.JsGetElementById(rbAfterPage) %>.checked = true;
    }
    
    function enableReplaceRange() {
      <%= AspxTools.JsGetElementById(rbReplacePages) %>.checked = true;
    }
    
    jQuery(function($){
        $('#m_eDocsList th:eq(3), #m_eDocsList th:eq(6)').addClass('Action');
        if ($('#m_eDocsList tbody').children().length > 0) {
                
                $.tablesorter.defaults.widgetZebra = {
                    css: ["GridItem", "GridAlternatingItem"]
                };
                var sorting = [[0, 0]];
                $('#m_eDocsList').tablesorter(
                {
                    sortList: sorting,
                    widgets: ['zebra']
                });
            }
    });
    
  </script>
    <h4 class="page-header">Select document to insert</h4>
    <form id="form1" runat="server">
    <div>
    <div id="SelectFilesPanel">
    <asp:GridView ID="m_eDocsList" runat="server" AutoGenerateColumns="false"  HeaderStyle-CssClass="GridHeader" RowStyle-CssClass="GridItem" AlternatingRowStyle-CssClass="GridAlternatingItem" Width="100%">
      <Columns>
        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
          <ItemTemplate>
            <ml:EncodedLabel ID="warningImage" Text="*" ForeColor="Red" runat="server" Visible='<%# AspxTools.HtmlString(((int)Eval("ImageStatus") != (int)DataAccess.E_ImageStatus.HasCachedImages).ToString()) == "True"%>'></ml:EncodedLabel>
            <input type="hidden" value=<%# AspxTools.HtmlString(Eval("Version").ToString()) %> class="Version"/>
            <input type="hidden" value=<%# AspxTools.HtmlString(Eval("DocumentId").ToString()) %> class="DocId"/>
            <a href="#" DocType=<%# AspxTools.HtmlString(Eval("DocTypeName").ToString()) %> PageCount=<%# AspxTools.HtmlString(Eval("PageCount").ToString()) %> onclick="return selectEDoc(this);" runat="server" Visible='<%# AspxTools.HtmlString(((int)Eval("ImageStatus") == (int)DataAccess.E_ImageStatus.HasCachedImages).ToString()) == "True"%>'>select</a>
          </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
          <ItemTemplate>
          <a href="#" DocId=<%# AspxTools.HtmlString(Eval("DocumentId").ToString()) %> onclick="return viewEDoc(this);">view</a>
          </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemTemplate>
                <%#AspxTools.HtmlControl(this.GetImageForDocList((bool)Eval("IsESigned"))) %>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField HeaderText="Doc Type" DataField="DocTypeName" />
        <asp:BoundField HeaderText="Internal Comments" DataField="InternalDescription" />        
        <asp:BoundField HeaderText="Description" DataField="PublicDescription" />
        <asp:BoundField HeaderText="Last Modified" DataField="LastModifiedDate" />
        <asp:BoundField HeaderText="Pages" DataField="PageCount" />
      </Columns>
    </asp:GridView>
    <ml:EncodedLabel ID="m_warningMsg" ForeColor="Red" Text="* - This document is still being processed" runat="server"></ml:EncodedLabel><br />
    <input type="button" value="Cancel" onclick="onCancel();" />
    </div>
    <div id="InsertPagePanel" style="display:none">
      <h1 class="MainRightHeader">Insert Pages</h1>
      <span id="InsertSettings" class="padding">
      Insert Document pages <input type="text" id="docToInsertBegin" value="1" class="page_textbox" /> to <input type="text" id="docToInsertEnd" class="page_textbox" /> of <span id="docToInsertMaxPages">ERRAR</span> from <span id="DocType" />
      </span>
      <ul id="Options">
          <li> <asp:RadioButton ID="rbBeforeFirstPage" runat="server" GroupName="op" Text="Before first page" onclick="renderUI();"/></li>
          <li> <asp:RadioButton ID="rbAfterPage" runat="server" GroupName="op" Checked="true" Text="After page:"/>
               <input type="text" class="page_textbox" id="tbNewLocation" onfocus="enablePageRange();"/> of <span id="NumOfPagesLabel"></span></li>
          <li><asp:RadioButton ID="rbAfterLastPage" runat="server" GroupName="op" Text="After last page"  onclick="renderUI();"/></li>
          <li><asp:RadioButton ID="rbReplacePages" runat="server" GroupName="op" Text="Replace pages " onclick="renderUI()"/> 
              <input type="text" id="replaceFrom" class="page_textbox" onfocus="enableReplaceRange();"/> to <input type="text" id="replaceTo" class="page_textbox" onfocus="enableReplaceRange()"/></li>
          <li class="DeleteFromSource">
              <label><input type="checkbox" id="DeleteFromSourceDocument" /> Delete pages from source document after insertion</label>
              <br />
              <span id="DeleteFromSourceExplanation">Unable to delete pages from signed documents.</span>
          </li>
      </ul>
      <br />
      <div id="Controls"><input type="button" value="OK" onclick="onOK();" /><input type="button" value="Cancel" onclick="onCancel();" /></div>
    </div>
    </div>
        <uc1:cmodaldlg id="Cmodaldlg1" runat="server" />
    </form>
</body>
</html>
