﻿#region Generated code
namespace LendersOfficeApp.newlos.ElectronicDocs
#endregion
{
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocumentCapture;

    /// <summary>
    /// Provides an interface for uploading documents to a capture vendor.
    /// </summary>
    public partial class DocumentCaptureUpload : BaseLoanUserControl, IAutoLoadUserControl
    {
        /// <summary>
        /// Gets the containing page as a loan page.
        /// </summary>
        protected new BaseLoanPage Page
        {
            get
            {
                return (BaseLoanPage)base.Page;
            }
        }

        /// <summary>
        /// Loads data for the control.
        /// </summary>
        public void LoadData()
        {
            if (!this.BrokerUser.BrokerDB.EnableKtaIntegration)
            {
                return;
            }

            if (string.IsNullOrEmpty(ConstStage.KtaApiUrl))
            {
                ErrorUtilities.DisplayErrorPage(
                    "KTA API url is null or empty.",
                    isSendEmail: false,
                    brokerID: this.BrokerUser.BrokerId,
                    employeeID: this.BrokerUser.EmployeeId);

                return;
            }

            var serviceCredential = KtaUtilities.GetServiceCredential(this.BrokerUser);
            var hasServiceCredential = serviceCredential != null;

            this.CredentialTable.Visible = !hasServiceCredential;

            this.Page.RegisterJsGlobalVariables("HasKtaServiceCredential", hasServiceCredential);
            this.Page.RegisterJsGlobalVariables("PollInterval", ConstStage.KtaUploadPollInterval);

            this.Page.RegisterJsGlobalVariables("CanSubmitToKta", this.Page.UserHasWorkflowPrivilege(WorkflowOperations.UseDocumentCapture));
            this.Page.RegisterJsGlobalVariables("KtaSubmissionDenialReason", this.Page.GetReasonUserCannotPerformPrivilege(WorkflowOperations.UseDocumentCapture));
        }

        /// <summary>
        /// Saves data for the control.
        /// </summary>
        public void SaveData()
        {
            // No op
        }
    }
}