﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditConditionAssociation.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.EditConditionAssociation" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body bgcolor="gainsboro">
    <script type="text/javascript">

        window.resizeTo(700, 735);
        jQuery(function($) {

            $.tablesorter.addParser({
                id: 'conditionCell',
                is: function(s) {
                    return false;
                },
                format: function(value, table, cell) {
                    return $('.Order', cell).val();
                },
                type: 'numeric'
            });

            if ($('#ConditionsList tbody').children().length != 0) {
                $.tablesorter.defaults.widgetZebra = {
                    css: ["GridItem", "GridAlternatingItem"]
                };
                var sorting = [[1, 0]];
                $('#ConditionsList').tablesorter(
                {
                    sortList: sorting,
                    widgets: ['zebra'],
                    headers: {
                        0: {
                            sorter: false
                        },
                        2: {
                            sorter: 'conditionCell'
                        }
                    }
                });
            }

            var searchTimer;
            $('#Search').keyup(function() {
                var value = $(this).val();
                window.clearTimeout(searchTimer);
                window.setTimeout(function() {
                    doSearch(value);
                    //redo sorting in order to re-apply our stripes
                    $('#ConditionsList').trigger("sorton", [$('#ConditionsList')[0].config.sortList]);
                }, 200);
            });

            var $rows = $('#ConditionsList tbody tr');
            var searchDataCache = [];
            $rows.each(function() {
                var temp = [];
                $('td', this).each(function() {
                    temp.push($(this).text().toLowerCase());
                });
                searchDataCache.push(temp.join(' '));
            });


            function doSearch(val) {
                val = val.toLowerCase();
                $('.Hidden').removeClass('Hidden');

                var len = searchDataCache.length;
                for (var i = 0; i < len; i++) {
                    if (searchDataCache[i].indexOf(val) == -1) {
                        $rows[i].className += ' Hidden';
                    }
                }
            }

            $('#Cancel').click(function() {
                onClosePopup();
            });

            $('.InitChecked').prop('checked', true);
            $('.Protected input').prop('disabled', true);

            $('#OK').click(function() {
                var button = this;
                var oldVal = $(button).val();
                $(button).val("Please wait...").prop('disabled', true);

                try {
                    var SetOnly = GetSetSaveInfo();
                    var FullInfo = GetFullSaveInfo();
                    var sSetOnly = JSON.stringify(SetOnly);
                    var sFullInfo = JSON.stringify(FullInfo);
                    window.opener.OnConditionAssociationSave(SetOnly, FullInfo, sSetOnly, sFullInfo);
                } catch (e) { }

                onClosePopup();

            });


            function GetFullSaveInfo() {
                var ret = {};
                $('tr').each(function() {
                    var checked = $(this).find('input.Associated').is(":checked");
                    var taskId = $(this).find('input.TaskId').val();
                    if (!taskId) return;

                    ret[taskId] = checked;
                });
                return ret;
            }

            function GetSetSaveInfo() {
                var ret = {};
                $('tr').each(function() {
                    var checked = $(this).find('input.Associated').is(":checked");
                    if (!checked) return;

                    var taskId = $(this).find('input.TaskId').val();
                    if (!taskId) return;

                    ret[taskId] = $('td.Text.Condition', this).text();
                });
                return ret;
            }

            function Init(sDTO) {
                if (!sDTO) return;
                var DTO = JSON.parse(sDTO);
                if ($.isArray(DTO)) {
                    $.each(DTO, function(idx, id) {
                        $('#id_' + id + ' input.Associated').prop('checked', true);
                    });
                }
                else {
                    $.each(DTO, function(id, isSet) {
                        $('#id_' + id + ' input.Associated').prop('checked', isSet);
                    });
                }
            }

            try {
                Init(window.opener.GetSerializedConditionSettings());
            }
            catch (e) {
                //It's ok if we can't get initial settings
            }
            window.setTimeout(function() {
                $('#OK')[0].disabled = false;
                $('#Cancel')[0].disabled = false;
            }, 50);

        });
        
    </script>
    <style type="text/css">
    #Controls
    {
        width: 100px;
        position: relative;
        left: 50%;
        margin-left: -50px;
    }
    #Conditions
    {
        height: 600px;
        overflow: auto;
    }
    .MainRightHeader
    {
        margin: 0px 0px 0px 0px;
    }
    .Checkbox
    {
        width: 20px;
    }
    .Text
    {
        padding: 2px 0px 2px 3px;
    }
    .Category
    {
        font-weight: bold;
        padding-right: 5px;
    }
    #ConditionsList
    {
        width: 97%;
    }
    .Protected
    {
        color: #777;
    }
    th
    {
        border-top: solid 1px black;
        background-color: #999999;
        padding: 5px;
    }
    th, td
    {
        border-bottom: solid 1px black;
        border-right: solid 1px black;
    }
    th.Sortable
    {
        color: Blue;
        cursor: pointer;
        text-decoration: underline;
    }
    
    th.Sortable:hover
    {
        color: Orange;
    }
    #SearchControl
    {
        padding: 2px;
    }
    
    .Hidden
    {
        display: none;
    }
    
    </style>
    <h4 class="page-header">Select Conditions to associate with this document</h4>
    <form id="form1" runat="server">
    <div>
    <div id="Conditions">
    <div id="SearchControl">
        <label>Search: <input type="text" id="Search" /></label>
    </div>
    <table id="ConditionsList" cellspacing="0">
    <thead>
        <tr>
            <th><!-- Checkbox --> &nbsp; </th>
            <th class="Sortable">Category</th>
            <th class="Sortable">Condition</th>
        </tr>
    </thead>
    <tbody>
    <asp:Repeater ID="ConditionsRepeater" runat="server">
        <ItemTemplate>
        <tr id="id_<%#AspxTools.HtmlString(((ConditionAssociationDTO)Container.DataItem).TaskId)%>" class="<%#AspxTools.HtmlString(((ConditionAssociationDTO)Container.DataItem).IsProtected?"Protected":"") %>">
            <td class="Checkbox">
                <input type="checkbox" class="Associated <%#AspxTools.HtmlString(((ConditionAssociationDTO)Container.DataItem).IsAssociated?"InitChecked":"") %>"/>
                <input type="hidden" class="TaskId" value="<%#AspxTools.HtmlString(((ConditionAssociationDTO)Container.DataItem).TaskId)%>" />
            </td>
            <td class="Text Category"> <%# AspxTools.HtmlString(((ConditionAssociationDTO)Container.DataItem).Category) %> </td>
            <td class="Text Condition"> 
                <input type="hidden" class="Order" value="<%#AspxTools.HtmlString(((ConditionAssociationDTO)Container.DataItem).Order + "")%>" />
                <%# AspxTools.HtmlString(((ConditionAssociationDTO)Container.DataItem).ConditionText) %> 
            </td>
        </tr>
        </ItemTemplate>
    </asp:Repeater>
    </tbody>
    </table>
    </div>
    <div id="Controls">
        <input type="button" value="OK" id="OK"/> <input type="button" value="Cancel" id="Cancel" />
    </div>
    </div>
    
    </form>
</body>
</html>
