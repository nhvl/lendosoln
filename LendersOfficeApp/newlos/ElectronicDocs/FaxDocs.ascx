﻿<%@ Control
    Language="C#"
    AutoEventWireup="true"
    CodeBehind="FaxDocs.ascx.cs"
    Inherits="LendersOfficeApp.newlos.ElectronicDocs.FaxDocs"
%>
<%@ Register TagPrefix="uc" TagName="DocTypePicker" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx"%>
<%@Import Namespace="LendersOffice.AntiXss"%>

<script type="text/javascript" language="javascript">
<!--

    $(function(){
        
        $('.select input').click(updateBatchLinks);
        $('.ddl').change(updateBatchLinks);
        $('#batchSelectAllCb').click(function(){
            var amIChecked = this.checked;
            $('.select input:visible').each(function(){
                this.checked = amIChecked;
            });
            
            updateBatchLinks();
        });
    });
    
    function updateBatchLinks() {
            var enableCover = false;
            var areAllFaxOkay = true;
            var items = $('.select input:checked'); 
            items.each(function(i,o){
                enableCover = true;
                updateFax = true;
                var p = $(o).closest('tr').find('select'); 
                if( p.val() == '00000000-0000-0000-0000-000000000000')
                {
                    areAllFaxOkay = false;
                }
            });
            
            if( items.length === 0 ) {
                areAllFaxOkay = false;
            }
            
            $('a.batchCoverLink').toggle(enableCover);
            $('a.batchFaxLink').toggle(areAllFaxOkay);
    }
    
  function _init()
  {
    onDocTypeChange();
    
  }
  

  
  function onClearSelectClick()
  {
  
    <%= AspxTools.HtmlString(DocPicker0.ClientID) %>.clear();
    <%= AspxTools.HtmlString(DocPicker1.ClientID) %>.clear();
    <%= AspxTools.HtmlString(DocPicker2.ClientID) %>.clear();
    <%= AspxTools.HtmlString(DocPicker3.ClientID) %>.clear();
    <%= AspxTools.HtmlString(DocPicker4.ClientID) %>.clear();
    <%= AspxTools.HtmlString(DocPicker5.ClientID) %>.clear();
    
    <%= AspxTools.JsGetElementById(AppDdl_0)%>.selectedIndex = 0;
    <%= AspxTools.JsGetElementById(AppDdl_1)%>.selectedIndex = 0;
    <%= AspxTools.JsGetElementById(AppDdl_2)%>.selectedIndex = 0;
    <%= AspxTools.JsGetElementById(AppDdl_3)%>.selectedIndex = 0;
    <%= AspxTools.JsGetElementById(AppDdl_4)%>.selectedIndex = 0;
    <%= AspxTools.JsGetElementById(AppDdl_5)%>.selectedIndex = 0;
    
    <%= AspxTools.JsGetElementById(DescTb_0)%>.value = '';
    <%= AspxTools.JsGetElementById(DescTb_1)%>.value = '';
    <%= AspxTools.JsGetElementById(DescTb_2)%>.value = '';
    <%= AspxTools.JsGetElementById(DescTb_3)%>.value = '';
    <%= AspxTools.JsGetElementById(DescTb_4)%>.value = '';
    <%= AspxTools.JsGetElementById(DescTb_5)%>.value = '';
    
    
    <%= AspxTools.JsGetElementById(batchSelectCb_0)%>.checked = false;
    <%= AspxTools.JsGetElementById(batchSelectCb_0)%>.checked = false;
    <%= AspxTools.JsGetElementById(batchSelectCb_0)%>.checked = false;
    <%= AspxTools.JsGetElementById(batchSelectCb_0)%>.checked = false;
    <%= AspxTools.JsGetElementById(batchSelectCb_0)%>.checked = false;
    
    onDocTypeChange();
    updateBatchLinks();
  }
  

  function onDocTypeChange() 
  {
    var bEnable = <%= AspxTools.JsGetElementById(DocPicker0.ValueCtrl)%>.value != -1;
    var faxBEnable = <%= AspxTools.JsGetElementById(AppDdl_0) %>.value != '00000000-0000-0000-0000-000000000000' && bEnable;
    document.getElementById('downloadFaxLink_0').style.display = faxBEnable ? 'inline' : 'none';
    document.getElementById('downloadBarcodeLink_0').style.display = bEnable ? 'inline' : 'none';
    <%= AspxTools.JsGetElementById(batchSelectCb_0)%>.style.display = bEnable ? 'inline' : 'none';
    if( !bEnable ) {
        <%= AspxTools.JsGetElementById(batchSelectCb_0)%>.checked = false;        
    }
    <%= AspxTools.JsGetElementById(DescTb_0)%>.readOnly = !bEnable;
    
    bEnable = <%= AspxTools.JsGetElementById(DocPicker1.ValueCtrl)%>.value != -1;
    faxBEnable = <%= AspxTools.JsGetElementById(AppDdl_1) %>.value != '00000000-0000-0000-0000-000000000000' && bEnable;
    document.getElementById('downloadFaxLink_1').style.display = faxBEnable ? 'inline' : 'none';
    document.getElementById('downloadBarcodeLink_1').style.display = bEnable ? 'inline' : 'none';
    <%= AspxTools.JsGetElementById(batchSelectCb_1)%>.style.display = bEnable ? 'inline' : 'none';
    <%= AspxTools.JsGetElementById(DescTb_1)%>.readOnly = !bEnable;
        if( !bEnable ) {
        <%= AspxTools.JsGetElementById(batchSelectCb_1)%>.checked = false;        
    }
    bEnable = <%= AspxTools.JsGetElementById(DocPicker2.ValueCtrl)%>.value != -1;
    faxBEnable = <%= AspxTools.JsGetElementById(AppDdl_2) %>.value != '00000000-0000-0000-0000-000000000000' && bEnable;
    document.getElementById('downloadFaxLink_2').style.display = faxBEnable ? 'inline' : 'none';
    document.getElementById('downloadBarcodeLink_2').style.display = bEnable ? 'inline' : 'none';
    <%= AspxTools.JsGetElementById(batchSelectCb_2)%>.style.display = bEnable ? 'inline' : 'none';
    <%= AspxTools.JsGetElementById(DescTb_2)%>.readOnly = !bEnable;
    if( !bEnable ) {
        <%= AspxTools.JsGetElementById(batchSelectCb_2)%>.checked = false;        
    }
    bEnable = <%= AspxTools.JsGetElementById(DocPicker3.ValueCtrl)%>.value != -1;
    faxBEnable = <%= AspxTools.JsGetElementById(AppDdl_3) %>.value != '00000000-0000-0000-0000-000000000000' && bEnable;
    document.getElementById('downloadFaxLink_3').style.display = faxBEnable ? 'inline' : 'none';
    document.getElementById('downloadBarcodeLink_3').style.display = bEnable ? 'inline' : 'none';
    <%= AspxTools.JsGetElementById(batchSelectCb_3)%>.style.display = bEnable ? 'inline' : 'none';
    <%= AspxTools.JsGetElementById(DescTb_3)%>.readOnly = !bEnable;
        if( !bEnable ) {
        <%= AspxTools.JsGetElementById(batchSelectCb_3)%>.checked = false;        
    }
    bEnable = <%= AspxTools.JsGetElementById(DocPicker4.ValueCtrl)%>.value != -1;
    faxBEnable = <%= AspxTools.JsGetElementById(AppDdl_4) %>.value != '00000000-0000-0000-0000-000000000000' && bEnable;
    document.getElementById('downloadFaxLink_4').style.display = faxBEnable ? 'inline' : 'none';
    document.getElementById('downloadBarcodeLink_4').style.display = bEnable ? 'inline' : 'none';
    <%= AspxTools.JsGetElementById(batchSelectCb_4)%>.style.display = bEnable ? 'inline' : 'none';
    <%= AspxTools.JsGetElementById(DescTb_4)%>.readOnly = !bEnable;
    if( !bEnable ) {
        <%= AspxTools.JsGetElementById(batchSelectCb_4)%>.checked = false;        
    }
    bEnable = <%= AspxTools.JsGetElementById(DocPicker5.ValueCtrl)%>.value != -1;
    faxBEnable = <%= AspxTools.JsGetElementById(AppDdl_5) %>.value != '00000000-0000-0000-0000-000000000000' && bEnable;
    document.getElementById('downloadFaxLink_5').style.display = faxBEnable ? 'inline' : 'none';
    document.getElementById('downloadBarcodeLink_5').style.display = bEnable ? 'inline' : 'none';
    <%= AspxTools.JsGetElementById(batchSelectCb_5)%>.style.display = bEnable ? 'inline' : 'none';
    <%= AspxTools.JsGetElementById(DescTb_5)%>.readOnly = !bEnable;
        if( !bEnable ) {
        <%= AspxTools.JsGetElementById(batchSelectCb_5)%>.checked = false;        
    }
  }
  
  function updateDirtyBit_callback() {
    clearDirty();
  }

//-->
</script>

<table class="FormTable" cellspacing="0" border="1" id="m_Grid" style="border-collapse:collapse;">
<tr class="GridHeader">
<td>Doc Type</td>
<td>Application</td>
<td>Description</td>
<td>Fax Cover</td>
<td>Barcode Covers</td>
<td><input type="checkbox" id="batchSelectAllCb" />&nbsp;<label for="batchSelect">All</label></td>
</tr>

<tr class="GridItem">
<td><uc:DocTypePicker runat="server" ID="DocPicker0" Width="250px" OnChange="onDocTypeChange()"></uc:DocTypePicker> </td>
<td><asp:DropDownList ID="AppDdl_0" runat="server" Width="250px" onchange="onDocTypeChange()" class="ddl" /></td>
<td><asp:TextBox id="DescTb_0" runat="server" Width="375px" MaxLength="200" /></td>
<td><span id="downloadFaxLink_0" ><asp:LinkButton runat="server" Text="download" CommandName="downloadFax" CommandArgument="0" OnCommand="OnDownloadCommand"/></span></td>
<td><span id="downloadBarcodeLink_0" ><asp:LinkButton ID="LinkButton1" runat="server" Text="download" CommandName="downloadBar" CommandArgument="0" OnCommand="OnDownloadCommand"/></span></td>
<td><asp:CheckBox id="batchSelectCb_0" runat="server" CssClass="select"  /></td>
</tr>

<tr class="GridItemAlternatingItem">
<td><uc:DocTypePicker runat="server" ID="DocPicker1" Width="250px" OnChange="onDocTypeChange()"></uc:DocTypePicker> </td>
<td><asp:DropDownList ID="AppDdl_1" runat="server" onchange="onDocTypeChange()" Width="250px" class="ddl" /></td>
<td><asp:TextBox id="DescTb_1" runat="server" Width="375px" MaxLength="200" /></td>
<td><span id="downloadFaxLink_1" ><asp:LinkButton id="downloadFax_1" runat="server" Text="download" CommandName="downloadFax" CommandArgument="1" OnCommand="OnDownloadCommand" /></span></td>
<td><span id="downloadBarcodeLink_1" ><asp:LinkButton ID="downloadBarcode_1" runat="server" Text="download" CommandName="downloadBar" CommandArgument="1" OnCommand="OnDownloadCommand"/></span></td>
<td><asp:CheckBox id="batchSelectCb_1" runat="server" CssClass="select"  /></td>
</tr>

<tr class="GridItem">
<td><uc:DocTypePicker runat="server" ID="DocPicker2" Width="250px" OnChange="onDocTypeChange()"></uc:DocTypePicker> </td>
<td><asp:DropDownList ID="AppDdl_2" runat="server"  onchange="onDocTypeChange()" Width="250px" class="ddl" /></td>
<td><asp:TextBox id="DescTb_2" runat="server" Width="375px" MaxLength="200" /></td>
<td><span id="downloadFaxLink_2" ><asp:LinkButton id="downloadFax_2" runat="server" Text="download" CommandName="downloadFax" CommandArgument="2" OnCommand="OnDownloadCommand" /></span></td>
<td><span id="downloadBarcodeLink_2" ><asp:LinkButton ID="downloadBarcode_2" runat="server" Text="download" CommandName="downloadBar" CommandArgument="2" OnCommand="OnDownloadCommand"/></span></td>
<td><asp:CheckBox id="batchSelectCb_2" runat="server"   CssClass="select" /></td>
</tr>

<tr class="GridItemAlternatingItem">
<td><uc:DocTypePicker runat="server" ID="DocPicker3" Width="250px" OnChange="onDocTypeChange()"></uc:DocTypePicker> </td>
<td><asp:DropDownList ID="AppDdl_3" runat="server"  onchange="onDocTypeChange()" Width="250px" class="ddl" /></td>
<td><asp:TextBox id="DescTb_3" runat="server" Width="375px" MaxLength="200" /></td>
<td><span id="downloadFaxLink_3" ><asp:LinkButton id="downloadFax_3" runat="server" Text="download" CommandName="downloadFax" CommandArgument="3" OnCommand="OnDownloadCommand" /></span></td>
<td><span id="downloadBarcodeLink_3" ><asp:LinkButton ID="downloadBarcode_3" runat="server" Text="download" CommandName="downloadBar" CommandArgument="3" OnCommand="OnDownloadCommand"/></span></td>
<td><asp:CheckBox id="batchSelectCb_3" runat="server"   /></td>
</tr>


<tr class="GridItem">
<td><uc:DocTypePicker runat="server" ID="DocPicker4" Width="250px" OnChange="onDocTypeChange()"></uc:DocTypePicker> </td>
<td><asp:DropDownList ID="AppDdl_4" runat="server" onchange="onDocTypeChange()" Width="250px" class="ddl" /></td>
<td><asp:TextBox id="DescTb_4" runat="server" Width="375px" MaxLength="200" /></td>
<td><span id="downloadFaxLink_4" ><asp:LinkButton id="downloadFax_4" runat="server" Text="download" CommandName="downloadFax" CommandArgument="4" OnCommand="OnDownloadCommand" /></span></td>
<td><span id="downloadBarcodeLink_4" ><asp:LinkButton ID="downloadBarcode_4" runat="server" Text="download" CommandName="downloadBar" CommandArgument="4" OnCommand="OnDownloadCommand"/></span></td>
<td><asp:CheckBox id="batchSelectCb_4" runat="server" CssClass="select"  /></td>
</tr>

<tr class="GridItemAlternatingItem">
<td><uc:DocTypePicker runat="server" ID="DocPicker5" Width="250px" OnChange="onDocTypeChange()"></uc:DocTypePicker> </td>
<td><asp:DropDownList ID="AppDdl_5" runat="server" onchange="onDocTypeChange()" Width="250px" class="ddl" /></td>
<td><asp:TextBox id="DescTb_5" runat="server" Width="375px" MaxLength="200" /></td>
<td><span id="downloadFaxLink_5" ><asp:LinkButton id="downloadFax_5" runat="server" Text="download" CommandName="downloadFax" CommandArgument="5" OnCommand="OnDownloadCommand" /></span></td>
<td><span id="downloadBarcodeLink_5" ><asp:LinkButton ID="downloadBarcode_5" runat="server" Text="download" CommandName="downloadBar" CommandArgument="5" OnCommand="OnDownloadCommand"/></span></td>
<td><asp:CheckBox id="batchSelectCb_5" runat="server" CssClass="select"   /></td>
</tr>
    <tr>
        <td align="left" colspan="3">
            <input type="button" value="Clear list" class="ButtonStyle" onclick="onClearSelectClick()" />
            <asp:Button runat="server" ID="m_generateGenericBarcodePackage" class="ButtonStyle"  style="width:260px" Text="Download generic barcode package" OnClick="GenerateBarcodePackage" />
        </td>
        <td colspan="3" align="right">
            <asp:LinkButton ID="batchDownloadBtn" runat="server" Text="download selected fax covers" CommandName="downloadFax" style="display:none" class="batchFaxLink"
                CommandArgument="selected" OnCommand="OnDownloadCommand" /> <br />
                   <asp:LinkButton ID="LinkButton2" runat="server" Text="download selected barcode covers" CommandName="downloadBar" class="batchCoverLink"
                CommandArgument="selected" OnCommand="OnDownloadCommand"  style="display:none" /> 
        </td>
    </tr>
</table>
