﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentCaptureAuditDetails.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.DocumentCaptureAuditDetails" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Capture Audit Details</title>
    <style type="text/css">
        body {
            background-color: gainsboro;
        }

        #HeaderTable {
            border-collapse: collapse;
            border-spacing: 0;
            border: 0;
            width: 100%;
        }

        #ContentTable {
            left: 5%;
            border-spacing: 20px 2px; 
        }

        #FooterDiv {
            position: fixed;
            bottom: 10px;
            width: 100%;
            align-content: center;
        }
    </style>
    <script type="text/javascript">
        jQuery(function ($) {
            $('#CloseButton').click(function () {
                parent.LQBPopup.Return(null);
            });
        });
    </script>
</head>
<body>
    <div class="MainRightHeader">Capture Audit Details</div>
    <form id="form1" runat="server">
        <table id="ContentTable">
            <tr>
                <td class="FieldLabel">Loan Number:</td>
                <td>
                    <ml:EncodedLiteral ID="LoanNumber" runat="server"></ml:EncodedLiteral>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">OCR Request ID:</td>
                <td>
                    <ml:EncodedLiteral ID="RequestId" runat="server"></ml:EncodedLiteral>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">Latest Status:</td>
                <td>
                    <ml:EncodedLiteral ID="Status" runat="server"></ml:EncodedLiteral>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">Latest Status Date and Time:</td>
                <td>
                    <ml:EncodedLiteral ID="StatusTimestamp" runat="server"></ml:EncodedLiteral>
                </td>
            </tr>
            <tr>
                <!-- Spacer row -->
            </tr>
            <tr>
                <td class="FieldLabel">
                    Supplementary Details:
                </td>
                <td>
                    <ml:EncodedLiteral ID="SupplementaryData" runat="server"></ml:EncodedLiteral>
                </td>
            </tr>
        </table>
        <div id="FooterDiv" class="align-center">
            <input type="button" id="CloseButton" value="Close" />
        </div>
    </form>
</body>
</html>
