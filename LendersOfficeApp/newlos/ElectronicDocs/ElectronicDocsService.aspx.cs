﻿using System;
using DataAccess;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public class DocRequestsServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(DocRequestsServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            // Set value from webform to CPageData

            if (dataLoan.sConsumerPortalCreationD.IsValid)
            {
                bool wassConsumerPortalVerbalSubmissionDValid = dataLoan.sConsumerPortalVerbalSubmissionD.IsValid;

                dataLoan.sConsumerPortalVerbalSubmissionD_rep = GetString("sConsumerPortalVerbalSubmissionD");

                if (wassConsumerPortalVerbalSubmissionDValid && false == dataLoan.sConsumerPortalVerbalSubmissionD.IsValid)
                {
                    var msg = "Cannot invalidate the Consumer Portal Verbal Submission Date.";
                    throw new CBaseException(msg, msg);
                }
            }
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            // Set value from CPageData to webform

            SetResult("sConsumerPortalVerbalSubmissionD", dataLoan.sConsumerPortalVerbalSubmissionD_rep);
        }
    }
    public partial class ElectronicDocsService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("DocRequests", new DocRequestsServiceItem());
        }
    }
}
