﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using LendersOffice.PdfForm;

using DataAccess;
using CommonLib;
using LendersOffice.Admin;
using LendersOffice.Common;
using EDocs;
using LendersOffice.AntiXss;
using LendersOffice.PdfLayout;
using LendersOffice.Security;
using LendersOffice.ObjLib.ConsumerPortal;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class CreateRequestService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "SetForm":
                    SetForm();
                    break;
                case "SetEdoc":
                    SetEDoc();
                    break;
                case "SubmitRequestToQueue":
                    SubmitRequestToQueue();
                    break;
                case "CacheForm":
                    GenerateExistingStandardFormAndStoreInCache();
                    break;
                default:
                    break;
            }
        }

        private void SetEDoc()
        {
            EDocumentRepository repository = EDocumentRepository.GetUserRepository();
            var doc = repository.GetDocumentById(GetGuid("EDocId"));
            if (doc != null)
            {
                SetResult("Desc", doc.FolderAndDocTypeName + " " +  doc.PublicDescription);
            }
        }

        private void SetForm()
        {
            PdfForm selectedForm = PdfForm.LoadById(GetGuid("id"));
            SetResult("m_FormName", selectedForm.IsStandardForm ? selectedForm.Description : AspxTools.HtmlString(selectedForm.Description));
            SetResult("borrowerSignable", selectedForm.IsContainBorrowerSignature);
            SetResult("coborrowerSignable", selectedForm.IsContainCoborrowerSignature);
        }

        private byte[] GetPdfBytesForShareRequest(Guid loanId, Guid appId)
        {
            byte[] content; 

            Guid formId = GetGuid("formId", Guid.Empty);
            Guid recordId = GetGuid("recordId", Guid.Empty);

            Guid documentId = GetGuid("EDocId", Guid.Empty);
            int documentVersion = GetInt("EDocVersion", -1);
            Guid edocKey = GetGuid("EdocKey", Guid.Empty);

            if (formId != Guid.Empty)
            {
                PdfForm form = PdfForm.LoadById(formId);
                PdfFormLayout layout = null;
                content = form.GeneratePrintPdfAndPdfLayout(loanId, appId, recordId, out layout);
            }
            else if (documentId != Guid.Empty && edocKey != Guid.Empty)
            {
                string compositeKey = BrokerUserPrincipal.CurrentPrincipal.UserId.ToString("N") + edocKey.ToString("N") + documentId.ToString("N") + ".PDF";

                // Pull from temporary file.
                content = AutoExpiredTextCache.GetBytesFromCache(compositeKey);

                if (null == content)
                {
                    // 6/16/2010 dd - Cache expire. Inform user to upload file again.
                    throw new CBaseException("Could not locate edoc file. Please try again.", "Could not locate upload file. Please try again.");
                }
            }
            else
            {
                throw new CBaseException("Could not locate pdf. Please try again.", "Could not locate pdf. Please try again.");
            }

            return content;
        }

        private DocumentRequestPdfInfo GetPdfInfoForSignRequest(Guid loanId, Guid appId, Guid recordId)
        {
            byte[] content = null;
            string layoutXmlContent = string.Empty;
            bool isContainBorrowerSignature = false;
            bool isContainCoborrowerSignature = false;

            Guid formId = GetGuid("formId", Guid.Empty);
            string uploadFormId = GetString("uploadFormId", string.Empty);
            if (formId != Guid.Empty)
            {
                PdfForm form = PdfForm.LoadById(formId);

                PdfFormLayout layout = null;
                content = form.GeneratePrintPdfAndPdfLayout(loanId, appId, recordId, out layout);
                layoutXmlContent = layout.GetXmlContent();
                isContainBorrowerSignature = form.IsContainBorrowerSignature;
                isContainCoborrowerSignature = form.IsContainCoborrowerSignature;
            }
            else if (uploadFormId != string.Empty)
            {
                uploadFormId = uploadFormId + "_" + BrokerUserPrincipal.CurrentPrincipal.UserId;
                // Pull from temporary file.
                content = AutoExpiredTextCache.GetBytesFromCache(uploadFormId);
                if (null == content)
                {
                    // 6/16/2010 dd - Cache expire. Inform user to upload file again.
                    throw new CBaseException("Could not locate upload file. Please try again.", "Could not locate upload file. Please try again.");
                }

                string uploadFormSignatureId = GetString("uploadFormSignatureId", string.Empty);

                layoutXmlContent = AutoExpiredTextCache.GetFromCache(uploadFormSignatureId);

                PdfFormLayout layout = new PdfFormLayout();
                layout.LoadContent(layoutXmlContent);
                isContainBorrowerSignature = layout.IsContainBorrowerSignature;
                isContainCoborrowerSignature = layout.IsContainCoborrowerSignature;

            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Could not determine if it is a select form or upload form");
            }

            string key = GetString("key", "");
            if (false == string.IsNullOrEmpty(key))
            {
                key = key + "_" + BrokerUserPrincipal.CurrentPrincipal.UserId.ToString();
                content = AutoExpiredTextCache.GetBytesFromCache(key);
                if (null == content)
                {
                    // 6/16/2010 dd - Cache expire. Inform user to upload file again.
                    throw new CBaseException("Could not locate signed pdf. Please try again.", "Could not locate signed pdf. Please try again.");
                }
            }

            return new DocumentRequestPdfInfo()
            {
                Content = content,
                LayoutXmlContent = layoutXmlContent,
                IsContainBorrowerSignature = isContainBorrowerSignature,
                IsContainCoborrowerSignature = isContainCoborrowerSignature
            };
        }

        private void SubmitRequestToQueue()
        {
            var requestType = GetString("requestType").ToLower();
            bool isShareDocumentRequest = requestType == "share";
            bool isSignDocumentRequest = requestType == "sign";
            bool isReceiveDocumentRequest = requestType == "send";

            if (!isShareDocumentRequest && !isSignDocumentRequest && !isReceiveDocumentRequest)
            {
                throw new CBaseException(ErrorMessages.Generic, "Unsupported document request type.");
            }

            Guid loanId = GetGuid("loanId");
            Guid appId = GetGuid("appId");
            Guid recordId = GetGuid("recordId", Guid.Empty);
            Guid titleBorrowerId = GetGuid("sTitleBorrowerId", Guid.Empty);
            int docTypeId = GetInt("m_DocTypePicker_m_selectedDocType");
            string description = GetString("m_DescriptionTB");

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(CreateRequestService));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(appId);

            PendingDocumentRequest request = null;

            if (isShareDocumentRequest)
            {
                BranchDB db = new BranchDB(dataLoan.sBranchId, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                db.Retrieve();

                if (!db.ConsumerPortalId.HasValue && dataLoan.BrokerDB.IsEnableNewConsumerPortal)
                {
                    throw new CBaseException("Loan does not belong to branch which is linked to the consumer portal.", "Loan not in cp branch");
                }

                request = new PendingDocumentRequest(DocumentRequestType.SendDocumentToBorrower, loanId, appId, BrokerUserPrincipal.CurrentPrincipal, description);

                var pdfBytes = this.GetPdfBytesForShareRequest(loanId, appId);
                request.SetDocument(pdfBytes);
            }
            else if (isReceiveDocumentRequest)
            {
                request = new PendingDocumentRequest(DocumentRequestType.ReceiveDocumentFromBorrower, loanId, appId, BrokerUserPrincipal.CurrentPrincipal, description);

                if (titleBorrowerId != Guid.Empty)
                {
                    SetTitleOnlyCoborrower(dataLoan, dataApp, titleBorrowerId, request);
                }

                request.SetDocType(docTypeId);

            }
            else 
            {
                request = new PendingDocumentRequest(DocumentRequestType.SendDocumentToBorrowerForSigning, loanId, appId, BrokerUserPrincipal.CurrentPrincipal, description);
                var pdfInfo = this.GetPdfInfoForSignRequest(loanId, appId, recordId);

                if (titleBorrowerId != Guid.Empty)
                {
                    SetTitleOnlyCoborrower(dataLoan, dataApp, titleBorrowerId, request);
                }

                request.SetDocument(
                    pdfInfo.Content,
                    pdfInfo.LayoutXmlContent,
                    docTypeId,
                    pdfInfo.IsContainBorrowerSignature,
                    pdfInfo.IsContainCoborrowerSignature);
            }

            request.Save();
        }

        private void SetTitleOnlyCoborrower(CPageData dataLoan, CAppData dataApp, Guid titleBorrowerId, PendingDocumentRequest request)
        {
            var titleOnlyCoborrower = dataLoan.sTitleBorrowers.FirstOrDefault(p => p.Id == titleBorrowerId);

            if (titleOnlyCoborrower != null)
            {
                if (titleOnlyCoborrower.Email.ToLower().Equals(dataApp.aBEmail.ToLower()))
                {
                    throw new CBaseException("Title only borrower must have a different email.", "Different email required for title borrower when making a signing request.");
                }
            }

            if (titleOnlyCoborrower != null)
            {
                request.SetTitleOnlyCoborrower(titleOnlyCoborrower);
            }
        }

        private void GenerateExistingStandardFormAndStoreInCache()
        {
            Guid LoanID = GetGuid("loanId");
            Guid AppID = GetGuid("appId");
            Guid recordId = GetGuid("recordId", Guid.Empty);

            Guid formId = GetGuid("formId");
            PdfForm form = PdfForm.LoadById(GetGuid("formId"));

            //should we store in file system? 
            PdfFormLayout layout = null;
            string key = Guid.NewGuid().ToString();
            string fullKey = key + "_" + BrokerUserPrincipal.CurrentPrincipal.UserId.ToString();
            byte[] content = form.GeneratePrintPdfAndPdfLayout(LoanID, AppID, recordId, out layout);
            AutoExpiredTextCache.AddToCache(content, TimeSpan.FromHours(1), fullKey); //this should give them enough time to create =/
            SetResult("Key", key);
        }

        private class DocumentRequestPdfInfo
        {
            public byte[] Content;
            public string LayoutXmlContent;
            public bool IsContainBorrowerSignature;
            public bool IsContainCoborrowerSignature;
        }
    }
}
