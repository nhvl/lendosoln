﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateRequest.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.CreateRequest" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc" TagName="DocTypePicker" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx"%>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Create Request</title>
	<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
</head>
<body>
    <script type="text/javascript">
        var formId = null;
        var formSelected = false;
        var borrowerSignable = false;
        var coborrowerSignable = false;
        var appValid = true;        
        var fileName = '';
        var formName = '&nbsp;'
        var isStandardForm = "False";
        var recordId = '';
        var uploadFormId = null;
        var uploadFormSignatureId = null;
        var edocInfo = null;
        
        function _init() {
            if(document.getElementsByName('appId').length == 0) {
                resize(550, 170);
                appValid = false;
            }
            else {
                resize(690, 420);
                document.getElementsByName('appId')[0].checked = true;
                validatePage();
            }
        }
        
        function validatePage() {

                var m_RequestTypeSign = document.getElementById('m_RequestTypeSign');
                var requestIsSend = document.getElementById('m_RequestTypeSend').checked; 
                var requestIsShare = document.getElementById('m_requestTypeShare').checked;
                var requestIsSign = m_RequestTypeSign.checked;
                var docTypeRow  = document.getElementById('DocTypeRow');
                var cbTitleRow = document.getElementById('titleCbRow');
                var docTypeInvalid = !requestIsShare && (<%= AspxTools.JsGetElementById(m_DocTypePicker.ValueCtrl)%>.value == -1 || document.getElementById('m_DescriptionTB').value.length == 0);
            
            if (!requestIsShare && edocInfo != null) {
                edocInfo = null;
                formSelected = false;
                formName = '&nbsp;';
            }
            
            docTypeRow.style.display = (!requestIsShare) ? '' : 'none';
            
            if (cbTitleRow) {
                cbTitleRow.style.display = requestIsShare ? 'none' : '';
                
            }
            if (!requestIsShare) {
                checkTitleDD(requestIsSign && coborrowerSignable);
            }
      
            
            document.getElementById('m_sendRequestBtn').disabled = !appValid || docTypeInvalid || ((requestIsSign || requestIsShare) && !formSelected);
            document.getElementById('m_FormNameR').style.display = (requestIsSign || requestIsShare)  && !formSelected ? '' : 'none';
            document.getElementById('m_DocTypeR').style.display = docTypeInvalid && !requestIsShare ? '' : 'none';
            document.getElementById('formRow').style.display = (requestIsSign || requestIsShare) ? '' : 'none';
            

            setDisabledAttr(document.getElementById('m_selectLink'), requestIsSend);
            setDisabledAttr(document.getElementById('m_previewLink'), requestIsSend  || formName == '&nbsp;' || formName == null);
            document.getElementById('m_uploadLink').style.display = (requestIsSend || requestIsShare) ? 'none' : '';
            
         
            
            if(requestIsSend) {
                formSelected = false;
                borrowerSignable = false;
                coborrowerSignable = false;
                formId = null;
                formName = '&nbsp;';
                recordId = null;
            }
            document.getElementById('m_FormName').innerHTML = formName || '';
        }

        function submitRequestToQueue() {
            var args = getAllFormValues();
            if( _key.length > 0 ) args.key = _key;
            if(formId) args['formId'] = formId;
            
            var appId = '';
            var rbApps = document.getElementsByName('appId');
            for (i = 0; i < rbApps.length; i++)
            {
                if (rbApps[i].checked) {
                    appId = rbApps[i].value;
                    break;
                }
            }
            var titleBorrower = document.getElementById('sTitleBorrower');
            
            if (titleBorrower) {
                args["sTitleBorrowerId"] = titleBorrower.value; 
            }
            
            args["appId"] = appId;
            if(recordId) args['recordId'] = recordId;
            
            var requestTypes = document.getElementsByName('m_RequestType');
            for (var i = 0; i < requestTypes.length; i++){
                if (requestTypes[i].checked){
                     args['requestType']  = requestTypes[i].value;
                     break;
                }
            }
            
            if (edocInfo != null) {
                args["EDocId"] = edocInfo.docid;
                args['EDocVersion'] = edocInfo.version;
                args['EdocKey'] = edocInfo.key;
            }
            
            args['signRequest'] = document.getElementById('m_RequestTypeSign').checked;
            args.uploadFormId = uploadFormId || '';
            args.uploadFormSignatureId = uploadFormSignatureId || '';
            var res = gService.service.call("SubmitRequestToQueue", args);
            
            if (!res.error)
                onClosePopup();
            else
                alert(res.UserMessage);
                
            validatePage();
        }
        function uploadForm() {
            if (!document.getElementById('m_RequestTypeSign').checked)
                return;
        
            formId = null;
            fileName = null;
            formName = null;
            borrowerSignable = false;
            coborrowerSignable = false;
            formSelected = false;

            var settings = {
                onReturn: function (args) {
                    if (args.OK) 
                    {
                      uploadFormId = args.id;
                      uploadFormSignatureId = args.SignatureCacheId;
                      formName = args.FileName;
                      formSelected = true;
                      borrowerSignable = args.ContainBorrowerSignature;
                      coborrowerSignable = args.ContainCoborrowerSignature;
                      resetKey();
                    }
                    validatePage();
                }
            };

            LQBPopup.Show(gVirtualRoot + "/newlos/ElectronicDocs/UploadForm.aspx?loanid=" + <%= AspxTools.JsString(LoanID) %>, settings);
        }
        
        function checkTitleDD(enable)
        {
            var sTitleBorrower = document.getElementById('sTitleBorrower');
            if (!sTitleBorrower) {
                return; //nothing to do
            }
            var hasCoborrower;
            
            var rb = document.getElementsByName('appId');
            for (index = 0; index < rb.length; index++)
            {
                if (rb[index].checked) {
                    hasCoborrower = rb[index].hasCoborr == 'True';
                }
            }
            if( hasCoborrower || !enable) {
                sTitleBorrower.style.backgroundColor = 'lightgrey';
                sTitleBorrower.disabled = true;
                sTitleBorrower.value = '';
            }
            else {
                sTitleBorrower.style.backgroundColor = 'white';
                sTitleBorrower.disabled = false;
            }
        }
        
        function selectForm() {
            if (document.getElementById('m_RequestTypeSend').checked )
                return;
                
            var includeE = document.getElementById('m_requestTypeShare').checked;
            var t = includeE ? '&ie=1&' : ''; 
            var appId = '';
            var rbApps = document.getElementsByName('appId');
            for (i = 0; i < rbApps.length; i++)
            {
                if (rbApps[i].checked) {
                    appId = rbApps[i].value;
                    break;
                }
            }
            
            uploadFormId = null;
            uploadFormSignatureId = null;                                
            showModal("/newlos/ElectronicDocs/SelectForm.aspx?loanid="  + <%= AspxTools.JsString(LoanID) %> + "&applicationid=" + appId + t  , null, null, null, function(args){ 
                if (args.formType != 'edoc' && args.formType != 'form')
                {             
                    return;
                }
                
                if (args.formType == 'form'){
                    resetKey();        
                    edocInfo = null;                
                    formId = args.id;
                    fileName = args.fileName;
                    isStandardForm = args.isStandardForm;
                    var formDescription = args.formDescription;
                    recordId = args.recordId;
                    var res = gService.service.call("SetForm", args);
                    if (!res.error) {
                        populateForm(res.value, null);
                        formId = args.id;
                        borrowerSignable = res.value['borrowerSignable'] == 'True';
                        coborrowerSignable = res.value['coborrowerSignable'] == 'True';
                        formName = document.getElementById('m_FormName').innerHTML = res.value['m_FormName'];
                        document.getElementById('m_DescriptionTB').value = formName;
                        formSelected = true;
                    }
                    else
                        alert(res.UserMessage);
                }
                else { //its an edoc
           
                    var res = gService.service.call("SetEdoc", {EDocId : args.docid});
                    if (!res.error){
                        edocInfo = {}; 
                        edocInfo.docid = args.docid; 
                        edocInfo.version = args.version;
                        edocInfo.key = args.key;
                        formName = res.value.Desc;
                        formSelected = true;
                        document.getElementById('m_DescriptionTB').value = res.value.Desc;
                        document.getElementById('m_FormName').innerHtml  = res.value.Desc;
                    }
                    else {
                        alert(res.UserMessage);
                    }
                }    
                
                validatePage();
            },{ hideCloseButton: true });
        }
        var _key = '';
        function resetKey() {
            _key = '';
        }
        // returns the key to pass to preview&sign
        function preparePreviewForm(appid) {
            if( _key != '' ) {
                return _key;
            }
            
            if( uploadFormId ) {
                _key = uploadFormId;
            }
            else if( isStandardForm == 'True' || (formId != null && formId.length > 0) ) {
                var data = {
                    'loanId' : ML.sLId, 
                    'appId'  : appid,
                    'recordId' : recordId,
                    'formId' : formId
                };
                
                var res = gService.service.call("CacheForm", data);
                if( res.error) {
                    alert(res.UserMessage);
                }
                
                _key = res.value.Key ;
            }       
            
            return _key;
        }
        
        function previewForm() {        
            var prLink = document.getElementById('m_previewLink');
            if(hasDisabledAttr(prLink)) {
                return;
            }
            if (document.getElementById('m_RequestTypeSend').checked)
                return;
            
            var appId = '';
            var rbApps = document.getElementsByName('appId');
            for (i = 0; i < rbApps.length; i++)
                if (rbApps[i].checked) {
                    appId = rbApps[i].value;
                    break;
                }

            var key = '', extraQuery = '';
            if (edocInfo == null){
                key = preparePreviewForm(appId);
            }
            else {
                key = edocInfo.key; 
                extraQuery = '&ie=1&docid=' + edocInfo.docid; 
            }
            
            showModal("/newlos/ElectronicDocs/PreviewAndSignPdf.aspx?loanid=" + <%= AspxTools.JsString(LoanID) %> + "&applicationid=" + appId + "&key=" + key + extraQuery, null, null, null, null,{ hideCloseButton: true });
            
        }
    </script>
    <h4 class="page-header">Create Request</h4>
    <form id="form1" runat="server">
        <asp:HiddenField ID="emailsUnique" runat="server" />
        <table width="100%" border="0" cellspacing="2" cellpadding="3">
			<tr>
				<td class="FormTableHeader" colspan="3">
					Send Request To
				</td>
			</tr>
			<asp:PlaceHolder ID="m_appsInvalid" runat="server">
			<tr class="FieldLabel">
			    <td colspan="3" align="center">
			        <ml:EncodedLabel id="m_noValidAppMsg" runat="server" style="color:Red"></ml:EncodedLabel><br /><br />
			        <input type="button" value="Close" onclick="onClosePopup();" />
			    </td>
			</tr>
			</asp:PlaceHolder>
			<asp:PlaceHolder ID="m_appsValid" runat="server">
			<tr class="FieldLabel">
			    <td colspan="3">
			        <ml:CommonDataGrid id="m_appList" runat="server">
						<alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
						<itemstyle cssclass="GridItem"></itemstyle>
						<headerstyle cssclass="GridHeader"></headerstyle>
						<Columns>
							<asp:TemplateColumn ItemStyle-Width="15px">
								<itemtemplate>
									<input type="radio" name="appId" value=<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "AppId").ToString()) %>
									    hasCoborr=<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "hasCoborrower").ToString()) %>
									    hasBorr=<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "hasBorrower").ToString()) %>
									    hasCoborrEmail=<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "hasCoborrowerEmail").ToString()) %>
									    hasBorrEmail=<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "hasBorrowerEmail").ToString()) %>
									    onclick="validatePage();" />
								</itemtemplate>
							</asp:TemplateColumn>
                            <asp:BoundColumn DataField="Name" HeaderText="Application" />
                            <asp:BoundColumn DataField="Email" HeaderText="E-mail" />
						</Columns>
					</ml:CommonDataGrid>
			    </td>
			</tr>
			</table>
			<table width="100%" border="0" cellspacing="2" cellpadding="3" style="table-layout: fixed">
			<tr>
				<td class="FormTableHeader" colspan="3">
					Need Recipient To
				</td>
			</tr>
			<tr class="FieldLabel">
			    <td colspan="3" nowrap>
			        <input type="radio" id="m_RequestTypeSign" value="sign" name="m_RequestType" onclick="validatePage();" checked />Sign a form
			        <input type="radio" id="m_RequestTypeSend" value="send" name="m_RequestType" onclick="validatePage();" />Send in a document
			        <input type="radio" id="m_requestTypeShare" value="share" name="m_RequestType" runat="server" onclick="validatePage();" />Receive a document
			    </td>
			</tr>
			<tr id="formRow">
			    <td class="FieldLabel" width="100">
			        Form
			    </td>
			    <td id="m_FormName" style="border:solid 1px black">
			        &nbsp;
			    </td>
			    <td style="width:170px" class="FieldLabel">
					<img id="m_FormNameR" src="../../images/error_icon.gif" style="display:none" />			    
			        <a id="m_selectLink" href="#" onclick="selectForm();">select</a>
			        <a id="m_uploadLink" href="#" onclick="uploadForm();">upload</a>
			        <a id="m_previewLink" href="#" onclick="previewForm();" runat="server">preview</a>
			    </td>
			    
			</tr>
			<tr class="FieldLabel" id="DocTypeRow">
			    <td width="100">
			        Doc Type
			    </td>
			    <td>
			        <uc:DocTypePicker ID="m_DocTypePicker" Width="400px" OnChange="validatePage();" runat="server"></uc:DocTypePicker>
			    </td>
			    <td>
					<img id="m_DocTypeR" src="../../images/error_icon.gif" />
			    </td>
			</tr>
			<tr class="FieldLabel" id="descriptRow">
			    <td width="100">
			        Description
			    </td>
			    <td width="200">
			        <asp:TextBox ID="m_DescriptionTB" onblur="validatePage()" runat="server" style="width:100%"></asp:TextBox>
			    </td>
			    <td>
			    <img id="Img1" src="../../images/error_icon.gif" />
			    </td>
			</tr>
			<tr class="FieldLabel" id="titleCbRow" runat="server">
			    <td colspan="1">
			        Title Only Coborrower 
			    </td>
			    
			    <td>
			        <asp:DropDownList runat="server" ID="sTitleBorrower" disabled="disabled"></asp:DropDownList>
			    </td>
			</tr>
			<tr>
			    <td colspan="3" nowrap>
			        <input type="button" id="m_sendRequestBtn" value="Add Request to Queue" onclick="submitRequestToQueue();" />
			        <input type="button" value="Cancel" onclick="onClosePopup();" />
			    </td>
			</tr>
			</asp:PlaceHolder>
		</table>
    </form>
</body>
</html>
