namespace LendersOfficeApp.newlos.ElectronicDocs
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.FileSystem;
    using LendersOffice.Edocs.DocMagic;
    using LendersOffice.Integration.DocumentCapture;
    using LendersOffice.ObjLib.DocumentConditionAssociation;
    using LendersOffice.Security;
    using LqbGrammar.Exceptions;

    public partial class EDocsUtilitiesService : BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "DeleteEDocument":
                    DeleteDocument();
                    break;
                case "DeleteGeneric":
                    DeleteGeneric();
                    break;
                case "DocsInShippingTemplate":
                    GetDocsInShippingTemplate();
                    break;
                case "GenerateShippingPackage":
                    GenerateShippingPackage();
                    break;
                case "PrintShippingPackageToEDocs":
                    PrintShippingPackageToEDocs();
                    break;
                case "PollStatus":
                    PollStatus();
                    break;
                case "ResetImageStatus":
                    ResetImageStatus();
                    break;
                case "DocMagicStatus":
                    GetDocMagicStatus();
                    break;
                case "DeleteDocMagicError":
                    RemoveDocMagicError();
                    break;
                case "BatchStore":
                    BatchStore();
                    break; 
                case "ClearAssociations":
                    ClearAssociations();
                    break;
                case "MigrateDocs":
                    MigrateDocs();
                    break;
                case "UnlinkDoc":
                    UnlinkDocFromTask();
                    break;
                case nameof(GetKtaPostbackData):
                    this.GetKtaPostbackData();
                    break;
                case nameof(PollForOcrStatus):
                    this.PollForOcrStatus();
                    break;
                case nameof(SetUploadCookie):
                    this.SetUploadCookie();
                    break;
                case nameof(CreateEditableCopies):
                    CreateEditableCopies();
                    break;
            }
        }

        private void CreateEditableCopies()
        {
            var repository = EDocumentRepository.GetUserRepository();
            var documentIds = SerializationHelper.JsonNetDeserialize<IEnumerable<Guid>>(this.GetString("DocumentIds"));

            foreach (var documentId in documentIds)
            {
                var existingDoc = repository.GetDocumentById(documentId);

                if (existingDoc.IsESigned)
                {
                    EDocument.CreateEditableCopyOfSignedDoc(repository, existingDoc);
                }
            }
        }

        private void MigrateDocs()
        {
            Guid slid = GetGuid("sLId");
            EDocsMigration.RequestMigrationForLegacyEdocs(PrincipalFactory.CurrentPrincipal.BrokerId, slid);
        }

        private void DeleteGeneric()
        {
            Guid docId = GetGuid("DocId");

            EDocumentRepository repository = EDocumentRepository.GetUserRepository();
            repository.DeleteGenericDoc(docId, BrokerUserPrincipal.CurrentPrincipal.UserId);
        }

        private void ClearAssociations()
        {
            Guid docId = GetGuid("DocumentId");
            Guid loanId = GetGuid("LoanId");
            var items = DocumentConditionAssociation.GetAssociationsByLoanDocumentHeavy(BrokerUserPrincipal.CurrentPrincipal.BrokerId, loanId, docId, PrincipalFactory.CurrentPrincipal);

            foreach (var item in items)
            {
                item.Delete();
            }
        }

        private void UnlinkDocFromTask()
        {
            string taskId = GetString("TaskId");
            Guid loanId = GetGuid("LoanId");
            Guid documentId = GetGuid("DocumentId");

            DocumentConditionAssociation docAssociation = DocumentConditionAssociation.GetAssociationsByLoanCondition(PrincipalFactory.CurrentPrincipal.BrokerId, loanId, taskId)
                .FirstOrDefault(association => association.DocumentId == documentId);

            if(docAssociation != null)
            {
                docAssociation.Delete();
            }
            else
            {
                throw new CBaseException("The document is not associated with the given condition.", "A document with an id of " + documentId.ToString() +
                    " is not associated with a condition with an id of " + taskId);
            }
        }

        private void BatchStore()
        {
            var key = AutoExpiredTextCache.AddToCache(GetString("ID"), new TimeSpan(0, 10, 0));
            SetResult("key", key);
        }

        private void RemoveDocMagicError()
        {
            DocMagicPDFManager.RemoveDocMagicError(BrokerUserPrincipal.CurrentPrincipal.BrokerId, GetGuid("Id"));
        }

        private void GetDocMagicStatus()
        {
            IList<DocMagicPDFStatusData> statuses = DocMagicPDFManager.GetUploadsForLoan(BrokerUserPrincipal.CurrentPrincipal.BrokerId, GetGuid("sLId"));

            List<DocMagicPDFStatusData> inQStatus = new List<DocMagicPDFStatusData>();
            List<DocMagicPDFStatusData> prepStatus = new List<DocMagicPDFStatusData>();
            List<DocMagicPDFStatusData> readBarStaturs = new List<DocMagicPDFStatusData>();
            List<DocMagicPDFStatusData> splitStatus = new List<DocMagicPDFStatusData>();
            List<DocMagicPDFStatusData> errorStatus = new List<DocMagicPDFStatusData>();

            foreach (DocMagicPDFStatusData status in statuses)
            {
                List<DocMagicPDFStatusData> list; 
                switch (status.Status)
                {
                    case E_DocMagicPDFStatus.IN_Q:
                        list = inQStatus;
                        break;
                    case E_DocMagicPDFStatus.CONVERT_TIFF:
                        list = prepStatus; 
                        break;
                    case E_DocMagicPDFStatus.SCAN_BARCODES:
                        list = readBarStaturs;
                        break;
                    case E_DocMagicPDFStatus.CREATING_EDOCS:
                        list = splitStatus;
                        break;
                    case E_DocMagicPDFStatus.ERROR:
                        list = errorStatus;
                        break;
                    case E_DocMagicPDFStatus.COMPLETE:
                        continue;
                    default:
                        throw new UnhandledEnumException(status.Status);
                }

                list.Add(status);
            }

            SetResult("InQStatus", ObsoleteSerializationHelper.JavascriptJsonSerialize(inQStatus));
            SetResult("ConvertStatus", ObsoleteSerializationHelper.JavascriptJsonSerialize(prepStatus));
            SetResult("ScanStatus", ObsoleteSerializationHelper.JavascriptJsonSerialize(readBarStaturs));
            SetResult("SplitStatus", ObsoleteSerializationHelper.JavascriptJsonSerialize(splitStatus));
            SetResult("ErrorStatus", ObsoleteSerializationHelper.JavascriptJsonSerialize(errorStatus));
            SetResult(
                "DocListBarcodeStatusRefreshIntervalSeconds",
                ConstStage.DocListBarcodeStatusRefreshIntervalSeconds);
        }


        private void ResetImageStatus()
        {
            Guid docId = GetGuid("DocumentId");
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            EDocument doc = repo.GetDocumentById(docId);
            doc.RequeueErrorDoc();
            SetResult("eta", doc.GetImageReadyETA());
        }

        private void GenerateShippingPackage()
        {
            string key = Guid.NewGuid().ToString();
            int downloadType = GetInt("DownloadType");
            string data = downloadType + "," + GetString("IDS");

            AutoExpiredTextCache.AddToCache(data, TimeSpan.FromMinutes(60), "SHIPPING_PACKAGE_REQUEST_" + key);
            SetResult("key", key);
        }

        private void PrintShippingPackageToEDocs()
        {
            Guid sLId = this.GetGuid("sLId");
            List<Guid> docList = this.GetString("IDS").Split(',').Select(Guid.Parse).ToList();
            Guid appId = this.GetGuid("EDoc_aAppId");
            int docTypeId = this.GetInt("EDoc_DocTypeId");
            string description = this.GetString("EDoc_Description");
            string internalComments = this.GetString("EDoc_InternalComments");

            EDocumentViewer.WriteSinglePdfToEDocument(sLId, docList, appId, docTypeId, description, internalComments);
            this.SetResult("Success", true);
        }

        private void GetDocsInShippingTemplate()
        {
            Guid sLId = GetGuid("LoanId");
            int shippingTemplateId = GetInt("ShippingTemplateId");

            EDocumentRepository repository = EDocumentRepository.GetUserRepository();
            var docs = repository.GetDocumentsByShippingTemplate(shippingTemplateId, sLId);

            SetResult("MatchCount", docs.Count);
            for (int i = 0; i < docs.Count; i++)
            {
                SetResult("Doc" + i, docs[i].DocumentId);
            }
        }

        private void DeleteDocument()
        {
            Guid docId = GetGuid("DocId");

            EDocumentRepository repository = EDocumentRepository.GetUserRepository();
            try
            {
                repository.DeleteDocument(docId, "Deleted by user.");
                SetResult("Success", "Success");
            }
            catch (NotFoundException)
            {
                SetResult("Success", "Success");
                return;
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                SetResult("ErrorMessage", e.UserMessage);
            }
            catch (Exception e)
            {
                SetResult("ErrorMessage", ErrorMessages.Generic);
                Tools.LogError(e);
            }
        }

        private class LightWeightEdocsForPolling
        {
            public Guid DocumentId { get; set; }
            public E_ImageStatus ImageStatus { get; set; }
            public E_EDocStatus DocStatus { get; set; }
            public int PageCount { get; set; }

        }

        private List<LightWeightEdocsForPolling> GetDocs(Guid sLId, HashSet<Guid> eDocsSet)
        {
            List<LightWeightEdocsForPolling> docs = new List<LightWeightEdocsForPolling>();
            Guid brokerId = ((AbstractUserPrincipal)this.User).BrokerId;
            SqlParameter[] parameters = {
                                                new SqlParameter("@sLId", sLId),
                                                new SqlParameter("@BrokerId", brokerId)
                                            };
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_FetchDocsForPolling", parameters))
            {
                while (reader.Read())
                {
                    Guid documentId = (Guid)reader["DocumentId"];

                    if (eDocsSet == null || eDocsSet.Contains(documentId))
                    {
                        // 8/5/2015 - Since this stored procedure does not factor in the read permission of the user. Therefore we only
                        // include document id that on the user Document list page.
                        var o = new LightWeightEdocsForPolling();

                        o.DocumentId = documentId;
                        o.ImageStatus = (E_ImageStatus)Convert.ToInt32(reader["ImageStatus"]);
                        o.DocStatus = (E_EDocStatus)Convert.ToInt32(reader["Status"]);
                        o.PageCount = (int)reader["NumPages"];


                        docs.Add(o);
                    }
                }
            }

            return docs;
        }

        private void PollStatus()
        {
            Guid slid = GetGuid("slid");
            string eDocsIds = GetString("EdocsIdsList", string.Empty);
            HashSet<Guid> eDocsSet = null;

            if (string.IsNullOrEmpty(eDocsIds) == false)
            {
                string[] parts = eDocsIds.Split(',');
                if (parts.Length > 0)
                {
                    eDocsSet = new HashSet<Guid>();
                    foreach (var s in parts)
                    {
                        eDocsSet.Add(new Guid(s));
                    }
                }

            }

            List<LightWeightEdocsForPolling> docs = new List<LightWeightEdocsForPolling>();
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            if (eDocsSet == null && !ConstStage.PollingReturnAllDocs)
            {
                // Perform heavy load of all the properties of eDocs.
                foreach (var edoc in repo.GetDocumentsByLoanId(slid))
                {
                    var o = new LightWeightEdocsForPolling();
                    o.DocumentId = edoc.DocumentId;
                    o.ImageStatus = edoc.ImageStatus;
                    o.DocStatus = edoc.DocStatus;
                    o.PageCount = edoc.PageCount;

                    docs.Add(o);
                }
            }
            else
            {
                docs = this.GetDocs(slid, eDocsSet);
            }

            var processingDocs = new List<string>();
            var processingDescriptions = new List<string>();
            var errorDocs = new List<string>();
            var noActiveDocs = true;

            foreach (var edoc in docs)
            {

                switch (edoc.ImageStatus)
                {
                    case E_ImageStatus.NoCachedImagesNotInQueue:
                    case E_ImageStatus.NoCachedImagesButInQueue:
                        processingDocs.Add(edoc.DocumentId.ToString());
                        // Calculate estimated time remaining
                        string eta = EDocument.GetImageReadyETA(edoc.ImageStatus, edoc.PageCount);
                        processingDescriptions.Add(eta);
                        break;
                    case E_ImageStatus.HasCachedImages:
                        break;
                    case E_ImageStatus.Error:
                        errorDocs.Add(edoc.DocumentId.ToString());
                        break;
                    default:
                        throw new UnhandledEnumException(edoc.ImageStatus);
                }

                if (edoc.DocStatus != E_EDocStatus.Rejected && edoc.DocStatus != E_EDocStatus.Obsolete)
                {
                    noActiveDocs = false;
                }
            }


            SetResult("processingDocs", ObsoleteSerializationHelper.JavascriptJsonSerialize(processingDocs)); // make sure you don't delimit by a character in the strings
            SetResult("processingDescriptions", ObsoleteSerializationHelper.JavascriptJsonSerialize(processingDescriptions));
            SetResult("errorDocs", ObsoleteSerializationHelper.JavascriptJsonSerialize(errorDocs));
            SetResult("DocCount", docs.Count);
            SetResult("noActiveDocs", noActiveDocs);
        }

        private void GetKtaPostbackData()
        {
            KtaUtilities.GenerateKtaPostbackData(this, PrincipalFactory.CurrentPrincipal, E_EDocOrigin.LO);
        }

        private void SetUploadCookie()
        {
            KtaUtilities.SetUploadCookie(this);
        }

        private void PollForOcrStatus()
        {
            KtaUtilities.PollForOcrStatus(this, PrincipalFactory.CurrentPrincipal);
        }
    }
}
