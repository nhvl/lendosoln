﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignatureTemplate.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.SignatureTemplate" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Signature Template</title>
	<style>
	#frmLoadSignatureTemplate,#frmSaveSignature 
	{
	  background-color:gainsboro;
	      font-family: Arial, Helvetica, sans-serif;
	      font-size:11px;
	}
	div.ui-dialog-titlebar
	{    
	background:maroon none;
	border:none;
	}
	div.ui-dialog {
	  padding:0px;
	  border:solid 1px black;
	}
	</style>    
</head>
<body bgcolor="gainsboro">
  <script type="text/javascript">
    var g_pdfEditor = null;
    var confirmRemoveFieldMsg = "Remove the selected signature/initials?";
    function f_onPdfFieldDrop(evt, o) {
      var data = $(o).data('pdfFieldData');
      if (null == data) {
        data = {};
        $(o).data('pdfFieldData');
      }
      data.Name = $(o).attr('id');
    }
    $(function() {
      resize(1024, 768);
      g_pdfEditor = new LendersOffice.PdfEditor('pdfeditor', PdfPages, {
        GenerateFullPageSrc: function(o) { return 'SignatureTemplatePngViewer.aspx?docid=' + <%=AspxTools.JsString(m_pdfFormId) %> + '&pg=' + o.Page; },
        OnPdfFieldDrop: f_onPdfFieldDrop,
	      OnRenderPdfFieldContent: f_renderPdfFieldContent,   
	      OnPdfFieldSelectedStop : _onPdfFieldSelectedStop,
	      PageBottomPadding : 40,
        OnError : function(evt, msg) { alert(msg); onClosePopup();},
        EmptyImgSrc : gVirtualRoot + '/images/pixel.gif'        	           
      });
      g_pdfEditor.Initialize();
      $('.pdf-field-template').draggable({ revert: 'invalid', helper: 'clone' });


      $('#frmSaveSignature').dialog( { 
        autoOpen: false, modal: true, width: 450, height: 150,
        open : f_resetFrmSaveSignature 
      });
      $('#frmLoadSignatureTemplate').dialog({ autoOpen: false, modal: true, width: 450, height: 100,open : f_resetFrmLoadSignatureTemplate  });

      autoBindButtons();
      
      $.each("#ddlSaveSignatureTemplateList #ddlLoadSignatureTemplateList".split(' '),
        function(idx, id) {
          var ddl = $(id);
          $.each(TemplateList, function() { ddl.append($("<option>").val(this.key).text(this.value)); });
        });
      
      $("#ddlLoadSignatureTemplateList")
          .change(function(evt) {
              $("#btnOkLoadSignatureTemplate").prop('disabled', $(evt.target).val() == <%=AspxTools.JsString(Guid.Empty) %>);
          });
          
      $("#ddlSaveSignatureTemplateList")
          .focus(function(evt) {
              $("input[name=rbSaveTemplateOption]").val(["existing"]);
          })
          .change(function(evt) {
            f_validateSaveOkBtn();
          });
        
      $("#tbTemplateName")
          .focus(function(evt) {
            $("input[name=rbSaveTemplateOption]").val(["new"]);
          })
          .keyup(function(evt) {
            f_validateSaveOkBtn();
          });
    });
    function f_validateSaveOkBtn() {
      var opt = $('input[type="radio"][name="rbSaveTemplateOption"]:checked').val();
      var bDisabled = true;
      
      if (opt === 'existing') 
      {
        bDisabled = $("#ddlSaveSignatureTemplateList").val() === <%=AspxTools.JsString(Guid.Empty) %>;
      } 
      else 
      {
        bDisabled = $("#tbTemplateName").val() === '';
      }
      $("#btnOkSignatureTemplate").prop('disabled', bDisabled);
    }
    function f_resetFrmSaveSignature() {
        $("#tbTemplateName").val('');
        $("#ddlSaveSignatureTemplateList").val(<%=AspxTools.JsString(Guid.Empty) %>);
        $("input[name=rbSaveTemplateOption]").val(["existing"]);    
        $("#btnOkSignatureTemplate").prop('disabled', true);
    }
    function f_resetFrmLoadSignatureTemplate() {
      $("#ddlLoadSignatureTemplateList").val(<%=AspxTools.JsString(Guid.Empty) %>);
      $("#btnOkLoadSignatureTemplate").prop('disabled', true);
    }
    function f_renderPdfFieldContent(o) {
      if (o.cssClass == 'pdf-field-signature') {
        var name = escape(o.Description);
        return $("<img>", {"src":<%= AspxTools.JsString(VirtualRoot) %> + "/los/custompdfforms/Signature.aspx?name=" + name});
      } else if (o.cssClass == 'pdf-field-initial') {
        var name = escape(o.Description);
        return $("<img>", {"src":<%= AspxTools.JsString(VirtualRoot) %> + "/los/custompdfforms/Signature.aspx?name=" + name + "&mode=initial"});
      } else {
        return null;
      }
    }
    function autoBindButtons() {
      // Automatically bind a button to a function, if there is a function with following format {buttonId}_Click().
      $("button, input[type='button']").each(function(idx, o) { var fn = window[$(o).attr('id') + '_Click']; if (typeof fn === 'function') $(o).click(fn); });
    }

    function btnOk_Click() {
        var fields = g_pdfEditor.GetPdfFields();
        var jsonFields = JSON.stringify(fields);
        
        var args = { JsonFields:jsonFields};
        var result = gService.editor.call("SaveSignaturePositions", args);
        if (!result.error) 
        {
          var data = window.dialogArguments || {};
          data.OK = true;
          data.FileName = <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("file")) %>;
          data.id = <%=AspxTools.JsString(m_pdfFormId) %>;
          data.SignatureCacheId = result.value.SignatureCacheId;
          data.ContainBorrowerSignature = result.value.ContainBorrowerSignature == "True";
          data.ContainCoborrowerSignature = result.value.ContainCoborrowerSignature == "True";
          
          onClosePopup(data);          
        } 
        else 
        {
          alert(result.UserMessage);
        }
    }
    function btnOkLoadSignatureTemplate_Click() {
      var templateId = $('#ddlLoadSignatureTemplateList').val();
      var args = {
        TemplateId:templateId
      };
      var result = gService.editor.call("LoadFromTemplate", args);
      if (!result.error) {
        var jsonFields = result.value.JsonFields;
        if (jsonFields != '') {
          var list = $.parseJSON(jsonFields);
          $.each(list, function() { g_pdfEditor.AddPdfField(this); });
        }
        $('#frmLoadSignatureTemplate').dialog('close');

      }
    }
    function btnCancelLoadSignatureTemplate_Click() {
      $('#frmLoadSignatureTemplate').dialog('close');
    }
    function btnPickFromExistingTemplate_Click() {
      $('#frmLoadSignatureTemplate').dialog('open');
    }
    function btnCancelSignatureTemplate_Click() {
      $('#frmSaveSignature').dialog('close');
    }
    function btnOkSignatureTemplate_Click() {
      
      var templateId = <%= AspxTools.JsString(Guid.Empty) %>;
      var templateName = '';
      
      var opt = $('input[type="radio"][name="rbSaveTemplateOption"]:checked').val();
      if (opt == 'existing') {
        templateId = $('#ddlSaveSignatureTemplateList').val();
      } else {
        templateName = $('#tbTemplateName').val();
      }
      
      var fields = g_pdfEditor.GetPdfFields();
      var jsonFields = JSON.stringify(fields);

      var args = {
        TemplateId: templateId,
        TemplateName : templateName,
        JsonFields : jsonFields
      };
      var result = gService.editor.call("SaveTemplate", args);
      if (!result.error) {
        if (result.value['Result'] == 'OK') {
          $.each("#ddlSaveSignatureTemplateList #ddlLoadSignatureTemplateList".split(' '),
            function(idx, id) {
              var ddl = $(id);
              if (result.value["isNew"] == "False") {
                ddl.children().remove("option[value='"+templateId+"']");
              }
              var newName = result.value["TemplateName"];
              var newValue = result.value["TemplateId"];
              
              // We can just do an O(n) traversal instead of binary search since we expect the ddl to be short
              var loc = 1;
              var options = ddl.children();
              var length = options.length;
              // Skip the first option as it is a placeholder
              for (var i=1; i < length; i++) {
                if (newName.toLowerCase() <= options.eq(i).text().toLowerCase()) {
                    break;
                }
                loc++;
              }
              
              if (loc == length) {
                ddl.append($("<option>").val(newValue).text(newName));
              } else {
                options.eq(loc).before($("<option>").val(newValue).text(newName));
              }
            });
        }
        $('#frmSaveSignature').dialog('close');
      } 
      else 
      {
        alert(result.UserMessage);
      }
    }
    function btnSave_Click() {

      $('#frmSaveSignature').dialog('open');
      var fields = g_pdfEditor.GetPdfFields();

      var jsonFields = JSON.stringify(fields);
      //alert(jsonFields);
    }
    function btnDeleteField_Click() {
      var list = g_pdfEditor.GetSelectedPdfFields();
      var ret = false;
      if (list.length > 0) {
        if (confirmRemove()) {
          $.each(list, function(idx, o) { g_pdfEditor.RemovePdfField(o); });
          ret = true;
        }
      }
      return ret;
    }
    function _onPdfFieldSelectedStop(evt, count) {
      $('#btnDeleteField').prop('disabled', count == 0);
    }
    function confirmRemove() {
      return confirm(confirmRemoveFieldMsg);
    }
    
  </script>
    <form id="form1" runat="server">
    <table width="100%">
      <tr><td class="MainRightHeader" colspan="4">Edit Signature</td></tr>
      <tr><td style="padding:5px" colspan="4">
          <input id="btnPickFromExistingTemplate" type="button" value="Pick from existing signature template ..." />
    &nbsp;&nbsp;<input id="btnSave" type="button" value="Save signature template ..." />
    </td>
      </tr>
      <tr><td style="font-weight:bold" colspan="4">Drag and drop signature fields on the document</td></tr>
      <tr>
      <td colspan="4">
      <table cellpadding="5">
        <tr><td>
        <div id="aBSignature" class="pdf-field-template pdf-field-signature" style="width:200px;height:20px;">
          <img src="~/los/custompdfforms/signature.aspx?name=Borrower+Signature" runat="server" border="0" class="pdf-field-content"/>
        </div>
        </td>
        <td>
        <div id="aCSignature" class="pdf-field-template pdf-field-signature" style="width:200px;height:20px;">
          <img src="~/los/custompdfforms/signature.aspx?name=CoBorrower+Signature" runat="server" border="0" class="pdf-field-content"/>
        </div> 
        </td>
        <td>
        <div id="aBInitials" class="pdf-field-template pdf-field-initial" style="width:130px;height:12px;">
          <img src="~/los/custompdfforms/signature.aspx?mode=initial&name=Borrower+Initials" runat="server" border="0" class="pdf-field-content"/>               
        </div>
        </td>
        <td>
        <div id="aCInitials" class="pdf-field-template pdf-field-initial" style="width:130px;height:12px;">
          <img id="Img4" src="~/los/custompdfforms/signature.aspx?mode=initial&name=CoBorrower+Initials" runat="server" border="0" class="pdf-field-content"/>        
        </div>
        </td>
        <td><input type="button" id="btnDeleteField" value="Delete Selected Signature/Initials" disabled="disabled" NoHighlight/>
        </td>
        </tr>
        
      </table>
        
      </td>
      </tr>
      <tr><td colspan="4" align="center"><input type="button" id="btnOk" value="OK" class="ButtonStyle" width="100px"/></td></tr>
    </table>
    			
    <div id="frmSaveSignature" title="Save signature template">
      <input type="radio" name="rbSaveTemplateOption" value="existing" checked="checked"/>Save into existing template 
      <select id="ddlSaveSignatureTemplateList" name="ddlSaveSignatureTemplateList"></select>
      <br /><input type="radio"  name="rbSaveTemplateOption" value="new"/>New Template <input type="text" id="tbTemplateName" style="width:250px" name="tbTemplateName"/>
      <br /><br />
      <div style="padding:5px;margin-left:auto;margin-right:auto;width:160px">
      <input type="button" id="btnOkSignatureTemplate" value="OK" style="width:70px"/>
      <input type="button" id="btnCancelSignatureTemplate" value ="Cancel" style="width:70px"/>
      
      </div>
    </div>
    <div id="frmLoadSignatureTemplate" title="Load from signature template">
      Pick from template <select id="ddlLoadSignatureTemplateList"></select>
      <div style="padding:5px;margin-left:auto;margin-right:auto;width:160px">
      <input type="button" id="btnOkLoadSignatureTemplate" value="OK" style="width:70px"/>&nbsp;&nbsp;
      <input type="button" id="btnCancelLoadSignatureTemplate" value="Cancel" style="width:70px"/>
      </div>
    </div>
    <div id="pdfeditor"></div>
    <uc1:cModalDlg id="CModalDlg1" runat="server" />    
    </form>
</body>
</html>
