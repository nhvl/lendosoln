﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CopyDocsToLoanFiles.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.CopyDocsToLoanFiles" %>
<%@ Import namespace="LendersOffice.Common"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Copy Docs to Loan Files</title>
    <style type="text/css">
        .headerBgColorOverride 
        {
            background-color: #003366 !important;
        }
        .TextInputPanel
        {
            padding: 5px;
        }
        .TextInputPanel label
        {
            font-weight: bold;
        }
        .TextInputPanel input
        {
            margin-right: 15px;
            margin-left: 5px;
        }
        .CheckboxPanel label
        {
            padding-right: 10px;
        }
    </style>
</head>
<body bgcolor="gainsboro" style="padding: 5px;">
<script type="text/javascript">
    var numFilesCopied = 0;

    $(document).ready(function() {

        $('.BatchCopyMaster').click(function() {
            var checked = !!$(this).prop('checked');
            $('#LoansTable input[type="checkbox"].BatchCopy').prop('checked', checked);
            UpdateBatchCopyGuids();
        });

        $('#BatchCopyButton').click(function() {
            CopyDocs();
        });

        $('.BatchCopy').click(function() {
            UpdateBatchCopyGuids();
        });

        UpdateBatchCopyGuids();

        $('#LoansTable').tablesorter({ headers: { 0: { sorter: false}} });
        $('#LoansTable').on('sortEnd', function() {
            UpdateDisplay();
        });

        var numRows = $('#LoansTable tr').length;
        $('#BatchCopyButton').toggle(numRows > 1);

        // Search if the user presses enter in any of the search boxes.
        $('.search').keydown(function(event) {
            if (event.which === 13) {
                $('#search1').focus().click();
            }
        });

        $('#loanNumberFilter').focus();
    });

    function UpdateBatchCopyGuids() {
        var guids = [];
        $('#LoansTable input[type="checkbox"].BatchCopy').filter(':checked').next().each(function() {
            guids.push($(this).val());
        });

        $('#BatchCopyGuids').val(JSON.stringify(guids));

        $('#BatchCopyButton').prop("disabled", !guids.length);
    }

    function UpdateDisplay() {
        var table = document.getElementById('LoansTable');
        var rows = table.tBodies[0].rows;
        var isAlt = false;
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            row.className = isAlt ? 'GridAlternatingItem' : 'GridItem';
            isAlt = !isAlt;
        }
    }

    // Send all of the doc ids and loan IDs to be added to the processing queue.
    function CopyDocs() {

        var loanIds = $('#BatchCopyGuids').val(); // serialized
        var docIds = $('#DocIds').val(); // serialized

        var args = {
            SrcLoanId: ML.sLId,
            DestLoanIds: loanIds,
            DocIds: docIds
        };

        var result = gService.copy.call('SubmitDocsToCopyQueue', args);

        if (result.error) {
            alert(result.UserMessage);
            onClosePopup();
            return;
        }

        alert('Documents have been submitted for copying. You will be notified upon completion.');
        onClosePopup();
        
    }
</script>
    
    <form id="form1" runat="server">
    <div class="MainRightHeader headerBgColorOverride">
        Copy Docs to Loan Files
    </div>
    <div id="SearchPanel">
        <div class="TextInputPanel">
            <label>Loan Number</label>
            <input type="text" id="loanNumberFilter" runat="server" class="search" />
            <label>Last Name</label>
            <input type="text" id="lastNameFilter" runat="server" class="search" />
            <label>First Name</label>
            <input type="text" id="firstNameFilter" runat="server" class="search" />
            <input type="button" id="search1" runat="server" onserverclick="Search" value="Search"/>
        </div>
        <div class="CheckboxPanel">
            <span style="font-weight: bold; padding-right: 5px;">Return partial matches for:</span>
            <input type="checkbox" id="loanNumPartialMatch" runat="server" class="search" /><label>Loan Number</label>
            <input type="checkbox" id="lastNamePartialMatch" runat="server" class="search" /><label>Last Name</label>
            <input type="checkbox" id="firstNamePartialMatch" runat="server" class="search" /><label>First Name</label>
        </div>
    </div>
    <div id="TablePanel" runat="server">
        <asp:Repeater runat="server" ID="Loans" OnItemDataBound="Loans_OnItemDataBound">
            <HeaderTemplate>
                <table id="LoansTable" class="FormTable">
                    <thead>
                        <tr class="GridHeader">
                            <th runat="server" align="left" id="CBColumn"><input type="checkbox" id="BatchCopyMaster" class="BatchCopyMaster" runat="server" /></th>
                            <th style="width: 100px;" runat="server" id="LoanNumberColumn"><a id="loanNumberSortLink" href="#">Loan Number</a></th>
                            <th style="width: 90px;" runat="server" id="LastNameColumn"><a id="lastNameSortLink" href="#">Last Name</a></th>
                            <th style="width: 90px;" runat="server" id="FirstNameColumn"><a id="firstNameSortLink" href="#">First Name</a></th>
                            <th style="width: 140px;" runat="server" id="PropertyAddressColumn"><a id="propertyAddressSortLink" href="#">Property Address</a></th>
                            <th style="width: 90px;" runat="server" id="LoanTypeColumn"><a id="loanTypeSortLink" href="#">Loan Type</a></th>
                            <th style="width: 90px;" runat="server" id="LoanStatusColumn"><a id="loanStatusSortLink" href="#">Loan Status</a></th>
                            <th style="width: 100px;" runat="server" id="StatusDateColumn"><a id="statusDateSortLink" href="#">Status Date</a></th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="GridItem" runat="server" id="Row">
                    <td align="left" runat="server" id="CBColumn">
                        <input type="checkbox" id="LoanCB" class="BatchCopy" runat="server" />
                        <input type="hidden" id="LoanId" runat="server" />
                    </td>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="LoanNumber"></ml:EncodedLiteral>
                    </td>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="LastName"></ml:EncodedLiteral>
                    </td>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="FirstName"></ml:EncodedLiteral>
                    </td>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="PropertyAddress"></ml:EncodedLiteral>
                    </td>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="LoanType"></ml:EncodedLiteral>
                    </td>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="LoanStatus"></ml:EncodedLiteral>
                    </td>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="StatusDate"></ml:EncodedLiteral>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <input type="button" id="BatchCopyButton" runat="server" value="Copy to selected loans"/>
        <input type="hidden" runat="server" id="BatchCopyGuids" />
        <input type="hidden" runat="server" id="DocIds" />
    </div>
    
    </form>
</body>
</html>
