﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDocs;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class DuplicateProtectedDialog : BaseLoanPage
    {
        private bool m_dataLoaded = false;
        private EDocumentRepository m_repo;
        private List<Guid> m_completeEditIds;
        private List<EDocBatchEditDetails> m_details;
        private Guid m_currentUIDocId;
        protected bool DataLoaded
        {
            get { return m_dataLoaded; }
        }
        protected void PageInit(object sender, EventArgs e)
        {
            m_repo = EDocumentRepository.GetUserRepository();
            base.DisplayCopyRight = false;
        }
        protected void PageLoad(object sender, EventArgs e)
        {
            //Mark data loaded on first post back so _init() doesn't repeatedly submit
            if (IsPostBack)
                m_dataLoaded = true;

            //Parse JSON data from dialogArguments
            var completeEditIds = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<Guid>>(completeEditList.Value);
            if (completeEditIds != null)
            {
                m_completeEditIds = completeEditIds;
            } 
            var details = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<EDocBatchEditDetails>>(detailList.Value);
            if (details != null)
            {
                m_details = details;
                BindData();
            }
        }

        private void BindData()
        {
            DocType dt = EDocumentDocType.GetDocTypeById(this.BrokerID, int.Parse(docTypeId.Value));
            DocType.Text = dt.FolderAndDocTypeName;
            var listData = new List<object>();

            //Add the batch saved documents and update batch modified fields
            foreach (EDocBatchEditDetails detail in m_details)
            {
                EDocument doc = m_repo.GetDocumentById(detail.DocumentId);
                doc.InternalDescription = detail.InternalDescription;
                doc.PublicDescription = detail.Description;
                listData.Add(doc);

                //Bind Borrower Name from first non-empty document
                if (string.IsNullOrEmpty(Borrower.Text))
                    Borrower.Text = doc.AppName;
            } 
            //Add duplicates that aren't part of this batch save
            foreach (EDocument doc in m_repo.GetDocumentsByLoanId(LoanID))
            {
                if (doc.DocStatus == E_EDocStatus.Approved && docTypeId.Value.Equals(doc.DocumentTypeId.ToString()) && !m_completeEditIds.Contains(doc.DocumentId))
                    listData.Add(doc);

                //Bind Borrower Name from first non-empty document
                if (string.IsNullOrEmpty(Borrower.Text))
                    Borrower.Text = doc.AppName;
            }
            DocumentList.DataSource = listData;
            DocumentList.DataBind();

            m_currentUIDocId = new Guid(currentDocId.Value);
        }

        protected void OnClick_Protect(object sender, EventArgs e)
        {
            bool currentUIDocToBlank = false;
            var docIdsToBlankStatus = new List<Guid>();
            foreach (DataGridItem item in DocumentList.Items)
            {
                CheckBox cb = (CheckBox) item.FindControl("checked");
                if (!cb.Checked)
                {
                    Guid docId = new Guid(((System.Web.UI.HtmlControls.HtmlInputHidden)item.FindControl("docid")).Value);
                    if (m_completeEditIds.Contains(docId))
                    {
                        //Part of Batch Edit:
                        //Will be saved once dialog returns; tell function to save "blank" instead
                        docIdsToBlankStatus.Add(docId);

                        //Track if the current doc needs to change status => update DDL on save
                        if (docId == m_currentUIDocId)
                            currentUIDocToBlank = true;
                    }
                    else
                    {
                        //Not Part of Batch Edit:
                        //Save Blank Now
                        EDocument doc = m_repo.GetDocumentById(docId);
                        doc.DocStatus = E_EDocStatus.Blank;
                        m_repo.Save(doc);
                    }
                }
                //else, status already marked as "Protected"
            }
            //Pass docIdsToBlankStatus and close modal
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "close",
                    "window.dialogArguments.ToBlankStatus = " + ObsoleteSerializationHelper.JsonSerialize(docIdsToBlankStatus)+"; "
                    + (currentUIDocToBlank?"window.dialogArguments.UIStatus = 0; ":"") //Remember to update DDL on save
                    + "onClosePopup();"
                    , true);
        }
        protected void OnClick_Continue(object sender, EventArgs e)
        {
            bool currentUIDocToBlank = false;
            var docIdsToBlankStatus = new List<Guid>();
            foreach (DataGridItem item in DocumentList.Items)
            {
                Guid docId = new Guid(((System.Web.UI.HtmlControls.HtmlInputHidden)item.FindControl("docid")).Value);
                if (m_completeEditIds.Contains(docId))
                {
                    //Part of Batch Edit:
                    //Will be saved once dialog returns; tell function to save "blank" instead
                    docIdsToBlankStatus.Add(docId);

                    //Track if the current doc needs to change status => update DDL on save
                    if (docId == m_currentUIDocId)
                        currentUIDocToBlank = true;
                }
                else
                {
                    //Not Part of Batch Edit:
                    //Save Blank Now
                    EDocument doc = m_repo.GetDocumentById(docId);
                    doc.DocStatus = E_EDocStatus.Blank;
                    m_repo.Save(doc);
                }
            }
            //Pass docIdsToBlankStatus and close modal
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "close",
                    "window.dialogArguments.ToBlankStatus = " + ObsoleteSerializationHelper.JsonSerialize(docIdsToBlankStatus) + "; "
                    + (currentUIDocToBlank ? "window.dialogArguments.UIStatus = 0; " : "") //Remember to update DDL on save
                    + "onClosePopup();"
                    , true);
        }
        protected void OnClick_Cancel(object sender, EventArgs e)
        {
            //Convert to List<Guid> DocumentId from List<EDocBatchEditDetails>
            var docidlist = m_details.Select<EDocBatchEditDetails, Guid>(doc => doc.DocumentId);

            //Tell save function to ignore the new status for these documents; use existing one
            //Close modal
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "close",
                    "window.dialogArguments.IgnoreStatus = " + ObsoleteSerializationHelper.JsonSerialize(docidlist) + "; onClosePopup();"
                    , true);
        }


        protected override void OnInit(EventArgs e)
        {
            RegisterJsScript("json.js");

            this.Init += new System.EventHandler(this.PageInit);
            this.Load += new System.EventHandler(this.PageLoad);
            base.OnInit(e);
        }
    }
}
