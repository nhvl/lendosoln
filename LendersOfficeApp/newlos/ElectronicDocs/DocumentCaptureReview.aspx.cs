﻿#region Generated Code
namespace LendersOfficeApp.newlos.ElectronicDocs
#endregion
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a means for reviewing document capture requests.
    /// </summary>
    public partial class DocumentCaptureReview : BasePage
    {
        /// <summary>
        /// Gets the permissions for loading the page.
        /// </summary>
        /// <value> 
        /// The permissions for loading the page.
        /// </value>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                if (this.IsCorporateAdmin)
                {
                    return new[] { Permission.AllowAccessingCorporateAdminDocumentCaptureReviewPage };
                }

                return new[] { Permission.AllowAccessingLoanEditorDocumentCaptureReviewPage };
            }
        }

        /// <summary>
        /// Gets a value indicating whether the capture review is being viewed
        /// from the corporate admin sidebar.
        /// </summary>
        /// <value>
        /// True if the page is being viewed from the corporate admin sidebar,
        /// false otherwise.
        /// </value>
        private bool IsCorporateAdmin => RequestHelper.GetBool("c");

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            var customerCode = principal.BrokerDB.CustomerCode;
            var portalUrl = customerCode == "PML0229" // JMAC
                ? ConstStage.KtaDocumentCaptureReviewPassthroughUrlForJmac
                : ConstStage.KtaDocumentCaptureReviewPortalUrl;

            RegisterJsGlobalVariables("portalUrl", portalUrl);
        }

        /// <summary>
        /// Gets the forced compatibility version.
        /// </summary>
        /// <returns>The compatibility version.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }
    }
}