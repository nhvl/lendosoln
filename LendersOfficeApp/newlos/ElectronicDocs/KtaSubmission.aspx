﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KtaSubmission.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.KtaSubmission" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Document Capture Submission</title>
    <style>
        input[type=file] {
            width: 100%;
        }
    </style>
    <script type="text/javascript">
        function submitForm(cookieData, ktaPayload, callback) {
            $('#KtaPayload').val(ktaPayload);

            var args = { CookieData: cookieData };
            parent.gService.edoc.callAsyncSimple('SetUploadCookie', args, function (result) {
                if (result.error) {
                    callback(false);
                }

                document.forms[0].submit();
                callback(true);
            });
        }
        
        function validate() {
            return Page_ClientValidate();
        }

        function hasFile() {
            return getFileName() !== '';
        }

        function getFileName() {
            var path = $.trim($('#FileToUpload').val());

            // StackOverflow provides this method for stripping a filename from a path: https://stackoverflow.com/a/423385
            var fileName = path.replace(/^.*[\\\/]/, '');
            return fileName;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField runat="server" ID="KtaPayload" />

        <input type="file" id="FileToUpload" runat="server" class="FileUpload"/>
        <asp:RegularExpressionValidator 
            runat="server" 
            ControlToValidate="FileToUpload" 
            ErrorMessage="Only PDF files are supported." 
            Display="Dynamic" 
            ValidationExpression=".*[.][Pp][Dd][Ff]$" />
    </form>
</body>
</html>
