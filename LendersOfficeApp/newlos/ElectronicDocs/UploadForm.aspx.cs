﻿namespace LendersOfficeApp.newlos.ElectronicDocs
{
    using LendersOffice.ConfigSystem.Operations;

    public partial class UploadForm : BaseLoanPage
    {
        /// <summary>
        /// Gets the extra workflow operations to check.
        /// </summary>
        /// <returns>The extra workflow operations to check.</returns>
        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[]
            {
                WorkflowOperations.UploadEDocs
            };
        }

        /// <summary>
        /// Gets the reasons for privileges that aren't allowed.
        /// </summary>
        /// <returns>The reasons for privileges that aren't allowed.</returns>
        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return this.GetExtraOpsToCheck();
        }


        protected void Page_Init(object sender, System.EventArgs e)
        {
            this.DisplayCopyRight = false;

            this.RegisterService("edoc", "/newlos/ElectronicDocs/EDocsUtilitiesService.aspx");

            this.RegisterJsScript("DragDropUpload.js");
            this.RegisterJsScript("DocumentUploadHelper.js");

            this.RegisterCSS("DragDropUpload.css");
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            this.RegisterJsGlobalVariables("CanUploadEdocs", this.UserHasWorkflowPrivilege(WorkflowOperations.UploadEDocs));
            this.RegisterJsGlobalVariables("UploadEdocsFailureMessage", this.GetReasonUserCannotPerformPrivilege(WorkflowOperations.UploadEDocs));
        }
    }
}
