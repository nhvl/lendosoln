﻿#region Generated Code
namespace LendersOfficeApp.newlos.ElectronicDocs
#endregion
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Common;
    using LendersOffice.Integration.DocumentCapture;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a means for viewing document capture audits.
    /// </summary>
    public partial class DocumentCaptureAudit : BasePage
    {
        /// <summary>
        /// Gets the permissions for loading the page.
        /// </summary>
        /// <value> 
        /// The permissions for loading the page.
        /// </value>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                if (this.IsCorporateAdmin)
                {
                    return new[] { Permission.AllowAccessingCorporateAdminDocumentCaptureAuditPage };
                }

                return new[] { Permission.AllowAccessingLoanEditorDocumentCaptureAuditPage };
            }
        }

        /// <summary>
        /// Gets a value indicating whether the capture audit is being viewed
        /// from the corporate admin sidebar.
        /// </summary>
        /// <value>
        /// True if the page is being viewed from the corporate admin sidebar,
        /// false otherwise.
        /// </value>
        private bool IsCorporateAdmin => RequestHelper.GetBool("c");

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("LQBPopup.js");
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var currentUser = PrincipalFactory.CurrentPrincipal;
            if (!currentUser.BrokerDB.EnableKtaIntegration)
            {
                ErrorUtilities.DisplayErrorPage(
                    "User attempted to access capture audit page when broker does not have the integration enabled.",
                    isSendEmail: false,
                    brokerID: currentUser.BrokerId,
                    employeeID: currentUser.EmployeeId);

                return;
            }

            var requests = this.GetRequests(currentUser.BrokerId);
            this.RequestGrid.DataSource = requests;
            this.RequestGrid.DataBind();

            // Only display the loan number column when viewing through corporate admin.
            this.RequestGrid.Columns[0].Visible = this.IsCorporateAdmin;
        }

        /// <summary>
        /// Gets the statuses to display on the page.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <returns>
        /// The list of statuses to display.
        /// </returns>
        private IEnumerable<OcrRequest> GetRequests(Guid brokerId)
        {
            if (this.IsCorporateAdmin)
            {
                return OcrRequest.ListByBrokerId(brokerId);
            }

            return OcrRequest.ListByLoanId(brokerId, RequestHelper.LoanID);
        }
    }
}
