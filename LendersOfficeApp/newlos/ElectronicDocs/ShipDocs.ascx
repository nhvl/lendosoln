﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShipDocs.ascx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.ShipDocs" %>

<%@ Register Src="~/newlos/ElectronicDocs/ShipDocSelectDocument.ascx" TagName="SelectDocument" TagPrefix="uc1" %>
<%@ Register Src="~/newlos/ElectronicDocs/ShipDocReview.ascx"  TagPrefix="uc1" TagName="ReviewSelection" %>
<%@ Register Src="~/newlos/ElectronicDocs/ShipDocStackingOrder.ascx"  TagPrefix="uc1" TagName="StackingOrder" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<%@ Register TagPrefix="ML" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>

<% %>
<script type="text/javascript">

    var initSort = true;
    var selectCBList = '';
    var folder = 'folder';
    var modifiedDate = 'modifiedDate';
    var docType = 'docType';
    var sortedColumn = docType;
    function folderSort() {
        document.getElementById('ShipDocs_sortedColumn').value = folder;
        setSortedOrder();
    }
    function docSort() {
        document.getElementById('ShipDocs_sortedColumn').value = docType;
        setSortedOrder();
    }
    function dateSort() {
        document.getElementById('ShipDocs_sortedColumn').value = modifiedDate;
        setSortedOrder();
    }
    function getSortedColumn() {
        return document.getElementById('ShipDocs_sortedColumn').value; 
    }
    function getSortedOrder() {
        return parseFloat(document.getElementById('ShipDocs_sortOrder').value); 
    }
    function setSortedOrder() {
        document.getElementById('ShipDocs_sortOrder').value = $('th.headerSortUp').length >= 1 ? 0 : 1;
    }
    function view(docid) {
        var c = window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + docid, '_parent'); 
    }
    
    function disableNextButton(disable) {
        var btn = document.getElementById('<%= AspxTools.ClientId(NextBtn) %>');
        
        btn.disabled = disable;   
    }
    
    function updateDirtyBit_callback() {
        clearDirty();
    }
    
</script>
<asp:HiddenField ID="sortedColumn" runat="server" Value="docType" />
<asp:HiddenField ID="sortOrder" runat="server" Value="0" />
<div style="padding-top: 5px; padding-bottom: 5px; font-weight: bold;">

<asp:LinkButton runat="server" ID="Step1Link" OnClick="Step1Link_Click" >
    Select Documents
</asp:LinkButton>

<asp:PlaceHolder runat="server" ID="Step2">
   &nbsp;&gt;&nbsp;<asp:LinkButton  runat="server" ID="Step2Link" OnClick="Step2Link_Click" Text="Review Selection"></asp:LinkButton>
</asp:PlaceHolder>

<asp:PlaceHolder runat="server" ID="Step3" >
    &nbsp;&gt;&nbsp; Stacking Order
</asp:PlaceHolder>
</div>
<div>
    
    <table cellpadding="4" cellspacing="0"  width="90%">
        <tbody>
            <tr>
                <td>
                    <asp:Button Text="&lt; Back" runat="server" OnClick="Back_Click" ID="BackBtn" />
                    <asp:Button Text="Next &gt;" runat="server" OnClick="Next_Click" ID="NextBtn" />
                    <input type="button" value="Download" style="display:none;" id="Download" onclick="if(typeof(MainDownload_onClick) === 'function') { MainDownload_onClick(event); } return false;" />
                    <input type="button" value="Print to EDocs" style="display:none;" id="PrintToEDocs" onclick="if(typeof(MainPrintToEDocs_onClick) === 'function') { MainPrintToEDocs_onClick(); } return false;" />
                </td>
                <td align="right">
                    <asp:PlaceHolder runat="server" ID="ShippingTemplatePH">Shipping Template :
                        <ml:EncodedLabel runat="server" ID="SelectedShippingTemplateNameLabel"></ml:EncodedLabel>
                    </asp:PlaceHolder>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:SelectDocument runat="server" ID="SelectDocument"></uc1:SelectDocument>
                    <uc1:ReviewSelection runat="server" ID="ReviewSelection"></uc1:ReviewSelection>
                    <uc1:StackingOrder runat="server" ID="StackingOrder"></uc1:StackingOrder>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<ML:cModalDlg runat="server" ID="modal" />






