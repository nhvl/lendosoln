﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentCaptureReview.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.DocumentCaptureReview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Capture Review</title>
    <style type="text/css">
        body {
            background-color: gainsboro;
            position: fixed;
            width: 100%;
            height: 100%;
            margin: 0;
        }
        .Content {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-content: center;
            font-size: 16px;
            text-align: center;
            height: 100%;
        }

        #HeaderTable {
            border-collapse: collapse;
            border-spacing: 0;
            border: 0;
            width: 100%;
        }

        #DocumentCaptureReviewPortalIFrame {
            margin: 0;
            padding: 0;
            border: none;
            position: fixed;
            width: 100%;
            height: 100%;
            overflow: scroll;
        }
    </style>
    <script>
        $j(function () {
            window.open(ML.portalUrl, 'CaptureReview', 'left=0,top=0,width=1080,height=800,resizable=yes');
        });
    </script>
</head>
<body>
    <div class="MainRightHeader">Capture Review</div>
    <div class="Content">
        <form id="form1" runat="server">
            Your Capture Review Dashboard should open in another window. </br>
            If the window does not open or has been closed, you can <input type="button" onclick="window.open(ML.portalUrl, 'CaptureReview', 'left=0,top=0,width=1080,height=800,resizable=yes'); return false;" value="Click Here" /> to open it again.
        </form>
    </div>
</body>
</html>
