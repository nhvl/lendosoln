﻿namespace LendersOfficeApp.newlos.ElectronicDocs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Security;
    using PdfRasterizerLib;

    public partial class SignatureTemplatePngViewer : BasePage
    {
        private string GetPngFileName(string docId, int page)
        {
            return string.Format("{0}\\{1}_{2}.png", ConstApp.TempFolder, docId, page);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string docId = RequestHelper.GetSafeQueryString("docid");
            int page = RequestHelper.GetInt("pg", -1);
            string mode = RequestHelper.GetSafeQueryString("mode");
            string fullKey = docId + "_" + BrokerUserPrincipal.CurrentPrincipal.UserId.ToString();
            if (mode == "pdf")
            {
                ProcessPdfMode(fullKey);
            }
            else
            {
                ProcessPngMode(fullKey, page);
            }

        }

        private void ProcessPdfMode(string docId)
        {
            byte[] buffer = AutoExpiredTextCache.GetBytesFromCache(docId);
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=\"preview.pdf\"");
            Response.Cache.SetExpires(DateTime.Now.AddDays(1));
            Response.Cache.SetLastModified(DateTime.Now);
            Response.OutputStream.Write(buffer, 0, buffer.Length);

            Response.Flush();
            Response.End();
        }
        private void ProcessPngMode(string docId, int page)
        {
            string fileName = GetPngFileName(docId, page);
            byte[] buffer = null;

            if (!FileOperationHelper.Exists(fileName))
            {
                byte[] pdfContent = AutoExpiredTextCache.GetBytesFromCache(docId);
                if (null == pdfContent)
                {
                    throw new NotFoundException("Not Found", "Could not load " + docId + " from cache");
                }
                IPdfRasterizer rasterizer = PdfRasterizerFactory.Create(ConstSite.PdfRasterizerHost);
                List<byte[]> pngContentList = rasterizer.ConvertToPng(pdfContent, ConstAppDavid.PdfPngScaling);

                if (null == pngContentList)
                {
                    throw new InvalidPDFFileException();
                }
                for (int i = 0; i < pngContentList.Count; i++)
                {
                    string outputFileName = GetPngFileName(docId, i + 1);
                    BinaryFileHelper.WriteAllBytes(outputFileName, pngContentList[i]);

                    if (i + 1 == page)
                    {
                        buffer = pngContentList[i];
                    }
                }
                if (page > pngContentList.Count)
                {
                    throw new PageOutOfRangeException();
                }

            }
            else
            {
                buffer = BinaryFileHelper.ReadAllBytes(fileName);
            }
            RenderPng(buffer);
        }
        private void RenderPng(byte[] buffer)
        {
            Response.Clear();
            Response.ContentType = "image/png";
            Response.Cache.SetExpires(DateTime.Now.AddDays(1));
            Response.Cache.SetLastModified(DateTime.Now);

            Response.OutputStream.Write(buffer, 0, buffer.Length);

            Response.Flush();
            Response.End();
        }
    }
}
