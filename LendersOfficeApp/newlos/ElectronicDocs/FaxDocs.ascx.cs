﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDocs;
using System.IO;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class FaxDocs : LendersOfficeApp.newlos.BaseLoanUserControl, IAutoLoadUserControl
    {

        private void SendFile(byte[] file, E_FaxCoverType type)
        {
            string name = type == E_FaxCoverType.PDFProcessor ? "attachment; filename=\"BarcodeCover.pdf\"" : "attachment; filename=\"FaxCover.pdf\"";
            Response.Clear();
            Response.ClearContent();
            Response.OutputStream.Write(file, 0, file.Length);
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", name);
            Response.Flush();
            Response.End();
        }

        protected void OnDownloadCommand(Object sender, CommandEventArgs e)
        {
            E_FaxCoverType type;
            switch (e.CommandName)
            {
                case "downloadFax":
                    type = E_FaxCoverType.EDocs;
                    break;
                case "downloadBar":
                    type = E_FaxCoverType.PDFProcessor;
                    break;
                default:
                    return;
            }

            string argument = e.CommandArgument.ToString();

            if (argument != "selected")
                CreatePdf(type, argument);
            else
                CreateBatchPdf(type);
        }

        private string GetLoanNumber()
        {
            CPageData dataloan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FaxDocs));
            dataloan.InitLoad();
            return dataloan.sLNm;
        }

        private List<DocType> x_DocTypes = null;
        private List<DocType> DocTypeList
        {
            get
            {
                if (x_DocTypes == null)
                {
                    x_DocTypes = EDocumentDocType.GetDocTypesByBroker(BrokerUser.BrokerId, E_EnforceFolderPermissions.True).ToList();
                }

                return x_DocTypes;
            }
        }

        private string GetDocTypeName( string docTypeId)
        {
            foreach (DocType docType in DocTypeList)
            {
                if (docType.DocTypeId == docTypeId)
                    return docType.DocTypeName; // Do not want folder name.
            }

            Tools.LogBug("Could not find doc type: " + docTypeId);
            return string.Empty;
        }

        private void CreatePdf(E_FaxCoverType type, string index)
        {

            string docType = string.Empty;
            string docTypeDescription = string.Empty;
            string description = string.Empty;
            Guid appId = Guid.Empty;
            string appDescription = string.Empty;

            switch (index)
            {
                case "0":
                    docType = DocPicker0.Value;
                    docTypeDescription = GetDocTypeName(DocPicker0.Value);
                    description = DescTb_0.Text;
                    appId = new Guid(AppDdl_0.SelectedValue);
                    appDescription = AppDdl_0.SelectedItem.Text;
                    break;
                case "1":
                    docType = DocPicker1.Value;
                    docTypeDescription = GetDocTypeName(DocPicker1.Value);
                    description = DescTb_1.Text;
                    appId = new Guid(AppDdl_1.SelectedValue);
                    appDescription = AppDdl_1.SelectedItem.Text;
                    break;
                case "2":
                    docType = DocPicker2.Value;
                    docTypeDescription = GetDocTypeName(DocPicker2.Value);
                    description = DescTb_2.Text;
                    appId = new Guid(AppDdl_2.SelectedValue);
                    appDescription = AppDdl_2.SelectedItem.Text;
                    break;
                case "3":
                    docType = DocPicker3.Value;
                    docTypeDescription = GetDocTypeName(DocPicker3.Value);
                    description = DescTb_3.Text;
                    appId = new Guid(AppDdl_3.SelectedValue);
                    appDescription = AppDdl_3.SelectedItem.Text;
                    break;
                case "4":
                    docType = DocPicker4.Value;
                    docTypeDescription = GetDocTypeName(DocPicker4.Value);
                    description = DescTb_4.Text;
                    appId = new Guid(AppDdl_4.SelectedValue);
                    appDescription = AppDdl_4.SelectedItem.Text;
                    break;
                case "5":
                    docType = DocPicker5.Value;
                    docTypeDescription = GetDocTypeName(DocPicker5.Value);
                    description = DescTb_5.Text;
                    appId = new Guid(AppDdl_5.SelectedValue);
                    appDescription = AppDdl_5.SelectedItem.Text;
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Bad fax cover row id: " + index);
            }

            EDocsFaxNumber number = EDocsFaxNumber.Retrieve(BrokerUser.BrokerId);

            EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
            {
                Type = type,
                LoanId = LoanID,
                AppId = appId,
                LenderName = BrokerUser.BrokerName,
                Description = description,
                DocTypeId = int.Parse(docType),
                DocTypeDescription = docTypeDescription,
                FaxNumber = number.FaxNumber,
                ApplicationDescription = appDescription,
                LoanNumber = GetLoanNumber(),
            };

            if (appId == Guid.Empty && type == E_FaxCoverType.PDFProcessor)
            {
                coverData.LoanNumber = "";
            }

            byte[] file = EdocFaxCover.GenerateBarcodePackagePdf(new EDocs.EdocFaxCover.FaxCoverData[] { coverData });

            SendFile(file, type);
        }
        protected void GenerateBarcodePackage(object sender, EventArgs args)
        {

            EDocsFaxNumber number = EDocsFaxNumber.Retrieve(BrokerUser.BrokerId);

            List<EdocFaxCover.FaxCoverData> covers = new List<EdocFaxCover.FaxCoverData>();
            foreach (var docType in DocTypeList)
            {
                covers.Add(new EdocFaxCover.FaxCoverData()
                {
                    Type = E_FaxCoverType.PDFProcessor,
                    LoanId = LoanID, 
                    AppId = Guid.Empty, 
                    LenderName = BrokerUser.BrokerName, 
                    Description = "",
                    DocTypeId = int.Parse(docType.DocTypeId),
                    DocTypeDescription = GetDocTypeName(docType.DocTypeId),
                    FaxNumber =  number.FaxNumber,
                    ApplicationDescription = "",
                    LoanNumber = ""
                });
            }


            // 02/08/12 mf. OPM 77831. Special processing of batch package to prevent OOM exception
            byte[] file = EdocFaxCover.GenerateBarcodePackagePdf(covers.ToArray());
            SendFile(file, E_FaxCoverType.PDFProcessor);
        }

        private void CreateBatchPdf(E_FaxCoverType type)
        {
            // Create a multi-page pdf based on the selected docs.

            List<EdocFaxCover.FaxCoverData> coverDataList = new List<EdocFaxCover.FaxCoverData>(6);
            EDocsFaxNumber number = EDocsFaxNumber.Retrieve(BrokerUser.BrokerId);
            string loanNumber = GetLoanNumber();

            if (batchSelectCb_0.Checked && DocPicker0.Value != "-1")
            {
                EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
                {
                    Type =type,
                    LoanId = LoanID,
                    AppId = new Guid(AppDdl_0.SelectedValue),
                    LenderName = BrokerUser.BrokerName,
                    Description = DescTb_0.Text,
                    DocTypeId = int.Parse(DocPicker0.Value),
                    DocTypeDescription = GetDocTypeName(DocPicker0.Value),
                    FaxNumber = number.FaxNumber,
                    ApplicationDescription = AppDdl_0.SelectedItem.Text,
                    LoanNumber = loanNumber,
                };

                if (coverData.AppId == Guid.Empty && type == E_FaxCoverType.PDFProcessor)
                {
                    coverData.LoanNumber = "";
                }


                coverDataList.Add(coverData);
            }
            if (batchSelectCb_1.Checked && DocPicker1.Value != "-1")
            {
                EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
                {
                    Type = type,
                    LoanId = LoanID,
                    AppId = new Guid(AppDdl_1.SelectedValue),
                    LenderName = BrokerUser.BrokerName,
                    Description = DescTb_1.Text,
                    DocTypeId = int.Parse(DocPicker1.Value),
                    DocTypeDescription = GetDocTypeName(DocPicker1.Value),
                    FaxNumber = number.FaxNumber,
                    ApplicationDescription = AppDdl_1.SelectedItem.Text,
                    LoanNumber = loanNumber,
                };
                if (coverData.AppId == Guid.Empty && type == E_FaxCoverType.PDFProcessor)
                {
                    coverData.LoanNumber = "";
                }
                coverDataList.Add(coverData);
            }
            if (batchSelectCb_2.Checked && DocPicker2.Value != "-1")
            {
                EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
                {
                    Type = type,
                    LoanId = LoanID,
                    AppId = new Guid(AppDdl_2.SelectedValue),
                    LenderName = BrokerUser.BrokerName,
                    Description = DescTb_2.Text,
                    DocTypeId = int.Parse(DocPicker2.Value),
                    DocTypeDescription = GetDocTypeName(DocPicker2.Value),
                    FaxNumber = number.FaxNumber,
                    ApplicationDescription = AppDdl_2.SelectedItem.Text,
                    LoanNumber = loanNumber,
                };

                if (coverData.AppId == Guid.Empty && type == E_FaxCoverType.PDFProcessor)
                {
                    coverData.LoanNumber = "";
                }
                coverDataList.Add(coverData);
            }
            if (batchSelectCb_3.Checked && DocPicker3.Value != "-1")
            {
                EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
                {
                    Type = type,
                    LoanId = LoanID,
                    AppId = new Guid(AppDdl_3.SelectedValue),
                    LenderName = BrokerUser.BrokerName,
                    Description = DescTb_3.Text,
                    DocTypeId = int.Parse(DocPicker3.Value),
                    DocTypeDescription = GetDocTypeName(DocPicker3.Value),
                    FaxNumber = number.FaxNumber,
                    ApplicationDescription = AppDdl_3.SelectedItem.Text,
                    LoanNumber = loanNumber,
                };

                if (coverData.AppId == Guid.Empty && type == E_FaxCoverType.PDFProcessor)
                {
                    coverData.LoanNumber = "";
                }
                coverDataList.Add(coverData);
            }
            if (batchSelectCb_4.Checked && DocPicker4.Value != "-1")
            {
                EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
                {
                    Type = type,
                    LoanId = LoanID,
                    AppId = new Guid(AppDdl_4.SelectedValue),
                    LenderName = BrokerUser.BrokerName,
                    Description = DescTb_4.Text,
                    DocTypeId = int.Parse(DocPicker4.Value),
                    DocTypeDescription = GetDocTypeName(DocPicker4.Value),
                    FaxNumber = number.FaxNumber,
                    ApplicationDescription = AppDdl_4.SelectedItem.Text,
                    LoanNumber = loanNumber,
                };

                if (coverData.AppId == Guid.Empty && type == E_FaxCoverType.PDFProcessor)
                {
                    coverData.LoanNumber = "";
                }
                coverDataList.Add(coverData);
            }
            if (batchSelectCb_5.Checked && DocPicker5.Value != "-1")
            {
                EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
                {
                    Type = type,
                    LoanId = LoanID,
                    AppId = new Guid(AppDdl_5.SelectedValue),
                    LenderName = BrokerUser.BrokerName,
                    Description = DescTb_5.Text,
                    DocTypeId = int.Parse(DocPicker5.Value),
                    DocTypeDescription = GetDocTypeName(DocPicker5.Value),
                    FaxNumber = number.FaxNumber,
                    ApplicationDescription = AppDdl_5.SelectedItem.Text,
                    LoanNumber = loanNumber,
                };

                if (coverData.AppId == Guid.Empty && type == E_FaxCoverType.PDFProcessor)
                {
                    coverData.LoanNumber = "";
                }

                coverDataList.Add(coverData);
            }

            if (coverDataList.Count == 0) return;

            byte[] file = EdocFaxCover.GenerateBarcodePackagePdf(coverDataList.ToArray());

            SendFile(file, type);
        }

        private void BindApps()
        {

            Dictionary<Guid, string> apps = new Dictionary<Guid, string>();

            CPageData dataloan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FaxDocs));
            dataloan.InitLoad();

            var count = dataloan.nApps;

            apps.Add(Guid.Empty, "");
            for (var i = 0; i < count; i++)
            {
                var appData = dataloan.GetAppData(i);

                string name = appData.aBNm;
                if (!string.IsNullOrEmpty(appData.aCNm))
                {
                    name += " & " + appData.aCNm;
                }

                apps.Add(appData.aAppId, name);
            }

            AppDdl_0.DataSource = apps;
            AppDdl_1.DataSource = apps;
            AppDdl_2.DataSource = apps;
            AppDdl_3.DataSource = apps;
            AppDdl_4.DataSource = apps;
            AppDdl_5.DataSource = apps;

            AppDdl_0.DataTextField = "Value";
            AppDdl_1.DataTextField = "Value";
            AppDdl_2.DataTextField = "Value";
            AppDdl_3.DataTextField = "Value";
            AppDdl_4.DataTextField = "Value";
            AppDdl_5.DataTextField = "Value";

            AppDdl_0.DataValueField = "Key";
            AppDdl_1.DataValueField = "Key";
            AppDdl_2.DataValueField = "Key";
            AppDdl_3.DataValueField = "Key";
            AppDdl_4.DataValueField = "Key";
            AppDdl_5.DataValueField = "Key";

            AppDdl_0.DataBind();
            AppDdl_1.DataBind();
            AppDdl_2.DataBind();
            AppDdl_3.DataBind();
            AppDdl_4.DataBind();
            AppDdl_5.DataBind();


        }

        private void BindDocTypeDropDowns()
        {

        }

        #region IAutoLoadUserControl Members

        public void LoadData()
        {
            if (!IsPostBack)
            {
                BindApps();
                BindDocTypeDropDowns();
            }
        }

        public void SaveData()
        {
        }

        #endregion
    }
}
