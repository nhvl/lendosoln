﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentCaptureAudit.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.DocumentCaptureAudit" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Capture Audit</title>
    <style type="text/css">
        body {
            background-color: gainsboro;
        }

        #HeaderTable {
            border-collapse: collapse;
            border-spacing: 0;
            border: 0;
            width: 100%;
        }

        .grid-container {
            padding-top: 5px;
            padding-left: 5px;
        }
    </style>
    <script type="text/javascript">
        function viewDetails(requestId) {
            var url = ML.VirtualRoot + "/newlos/ElectronicDocs/DocumentCaptureAuditDetails.aspx?id=" + requestId;
            LQBPopup.Show(url, {
                width: 500,
                height: 200,
                hideCloseButton: true
            });
        }
    </script>
</head>
<body>
    <div class="MainRightHeader">Capture Audit</div>
    <form id="form1" runat="server">
        <div class="grid-container">
            <ml:CommonDataGrid runat="server" ID="RequestGrid" CssClass="InsetSpacer" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundColumn DataField="LoanNumber" HeaderText="Loan Number"></asp:BoundColumn>
                    <asp:BoundColumn DataField="RequestId" HeaderText="Request ID"></asp:BoundColumn>
                    <asp:BoundColumn DataField="LatestStatus" HeaderText="Latest Status"></asp:BoundColumn>
                    <asp:BoundColumn DataField="LatestStatusDate" HeaderText="Latest Status Date and Time"></asp:BoundColumn>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            <a onclick="viewDetails(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "RequestId").ToString())%>)">details</a>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </ml:CommonDataGrid>
        </div>
    </form>
</body>
</html>
