﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BatchEditDocList.ascx.cs"
    Inherits="LendersOfficeApp.newlos.ElectronicDocs.BatchEditDocList" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="EDocs" %>

<div id="BatchEdit">
<div id="BatchEditSearch">
Search docs: <input type="text"/>
</div>
<div id="BatchEditNavigation">
    <asp:Repeater runat="server" ID="NavigationList" OnItemDataBound="NavigationList_OnDataBound">
        <HeaderTemplate>
            <ol class="folder-name-listing">
        </HeaderTemplate>
        <ItemTemplate>
            <li class="folder">
                <span class="doc-type-folder">
                    <ml:EncodedLiteral runat="server" ID="FolderName"></ml:EncodedLiteral>
                </span>
                <asp:Repeater runat="server" ID="Documents" OnItemDataBound="Document_OnDataBound">
                    <HeaderTemplate>
                        <ol class="document-name-listing">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li class="document drop-target" id='guid_<%# AspxTools.HtmlString(((EDocument)Container.DataItem).DocumentId.ToString()) %>' data-docid='<%# AspxTools.HtmlString(((EDocument)Container.DataItem).DocumentId.ToString()) %>'>
                            <div runat="server" id="ESignImage" class="image-div">
                                <img src="../../images/edocs/SigIcon.PNG" />
                            </div>
                            <div class="notes">
                                <div>
                                    <span class="doc-type-name">
                                        <ml:EncodedLiteral runat="server" ID="DocTypeName"></ml:EncodedLiteral>
                                    </span>
                                </div>
                                <div>
                                    <span class="doc-notes">
                                        <ml:EncodedLiteral runat="server" ID="Notes"></ml:EncodedLiteral>
                                    </span>
                                </div>
                                <div>
                                    <ml:PassthroughLiteral runat="server" ID="Status"></ml:PassthroughLiteral>
                                </div>
                            </div>
                            <div class="link">
                                <span class="loading search-ignore">Loading...</span><a href="#" class="open-pdf">open pdf</a>
                            </div>
                        </li>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ol>
                    </FooterTemplate>
                </asp:Repeater>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ol>
        </FooterTemplate>
    </asp:Repeater>
</div>

<div id="BatchEditSplitDocDialog">
    <input type="button" id="BatchEditAddToDoc" value="Add to Document"/>
    <input type="button" id="BatchEditSplitNewDoc" value="Create New Document"/>
</div>

</div>