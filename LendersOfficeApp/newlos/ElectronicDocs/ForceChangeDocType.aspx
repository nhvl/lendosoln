﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForceChangeDocType.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.ForceChangeDocType" %>
<%@ Register TagPrefix="uc" TagName="DocTypePicker" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Change DocType</title>
    <style type="text/css" >
        body { background-color: gainsboro; padding: 10px; font-weight: bold; }
    </style>
</head>
<body>
    <script type="text/javascript">
        window.resizeTo(500, 200);

        $(document).on("click", "a", function () {
            window.resizeTo(500, 700);
        });
    </script>
    <form id="form1" runat="server">
    <div>
        
        The doc type assigned to this document has been deleted, select another doc type for this document. 
        <br />
        <br />
        <uc:DocTypePicker runat="server" ID="DocType"></uc:DocTypePicker>
        
        <br />
        <br />
        
        <asp:Button runat="server" ID="SaveBtn" Text="Continue" OnClick="SaveBtnOnClick" OnClientClick="window.resizeTo(1024,864); document.getElementById('form1').style.display = 'none';" />
        <input type="button" value="Cancel Editing" onclick="onClosePopup();" /> 
        
        <br />
        <br />
        
        <ml:EncodedLabel ForeColor="Red" runat="server" ID="ErrorMsg" Text=""></ml:EncodedLabel>
    </div>
    </form>
</body>
</html>
