﻿#region Generated Code
namespace LendersOfficeApp.newlos.ElectronicDocs
#endregion
{
    using System;
    using LendersOffice.Common;
    using LendersOffice.Integration.DocumentCapture;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Provides a means for display the details of a document
    /// capture audit.
    /// </summary>
    public partial class DocumentCaptureAuditDetails : BasePage
    {
        /// <summary>
        /// Gets the broker ID.
        /// </summary>
        public Guid BrokerId
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal.BrokerId;
            }
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var requestId = this.GetRequestId();
            var request = OcrRequest.Retrieve(requestId, this.BrokerId);
            var status = OcrStatus.Retrieve(requestId, this.BrokerId);

            this.LoanNumber.Text = request.LoanNumber;
            this.RequestId.Text = requestId.Value.ToString();
            this.Status.Text = status?.Status.ToString() ?? "No status has been received";
            this.StatusTimestamp.Text = status?.Date.ToString() ?? "N/A";
            this.SupplementaryData.Text = string.IsNullOrEmpty(status?.SupplementalData?.Message)
                ? "No supplemental data has been received"
                : status.SupplementalData.Message;
        }

        /// <summary>
        /// Retrieves the OCR request ID from the request.
        /// </summary>
        /// <returns>An <see cref="OcrRequestId"/> object.</returns>
        /// <exception cref="DeveloperException">If the request doesn't contain a valid ID string.</exception>
        private OcrRequestId GetRequestId()
        {
            var idString = RequestHelper.GetQueryString("id");
            var nullableRequestId = OcrRequestId.Create(idString);

            if (nullableRequestId.HasValue)
            {
                return nullableRequestId.Value;
            }

            throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"{idString} is not a valid OCR request ID."));
        }
    }
}
