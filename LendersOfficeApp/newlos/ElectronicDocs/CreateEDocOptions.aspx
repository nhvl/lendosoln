﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateEDocOptions.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.CreateEDocOptions" %>
<%@ Register TagPrefix="uc" TagName="DocTypePicker" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Create EDoc</title>
    <style type="text/css">
  body {
      background-color: whitesmoke;
  }
  #CreateEDocOptions>div>div {
      padding: 5px;
  }
  #CreateEDocOptions textarea {
      width: 300px;
      height: 75px;
      overflow-y:auto;
  }
  #CreateEDocOptions .label {
      font-weight: bold;
  }
    </style>
</head>
<body>
    <h4 class="page-header">Print to EDocs</h4>
    <form id="CreateEDocOptions" runat="server">
    <div>
        <div>
            <div class="label">Doc Type <img src="~/images/require_icon.gif" runat="server"/></div>
            <div><uc:DocTypePicker runat="server" ID="DocTypePicker"></uc:DocTypePicker></div>
        </div>
        <div>
            <div class="label">Application</div>
            <div><asp:DropDownList ID="ApplicationPicker" runat="server"></asp:DropDownList></div>
        </div>
        <div>
            <div class="label">Description</div>
            <div><textarea id="Description" maxcharactercount="200" maxlength="200" NoHighlight="NoHighlight"></textarea></div>
        </div>
        <div>
            <div class="label">Internal Comments</div>
            <div><textarea id="InternalComments" maxcharactercount="200" maxlength="200" NoHighlight="NoHighlight"></textarea></div>
        </div>
        <div class="footer">
            <input type="button" value="Print to EDocs" id="PrintToEDocsBtn" disabled="disabled" /> <input type="button" value="Cancel" id="CloseBtn" />
        </div>
    </div>
    <script>
(function(){
    var docTypePicker = <%= AspxTools.JsGetElementById(this.DocTypePicker.ValueCtrl) %>;
    var appIdSelect = <%= AspxTools.JsGetElementById(this.ApplicationPicker) %>;
    var descriptionTextarea = document.getElementById('Description');
    var internalCommentsTextarea = document.getElementById('InternalComments');
    var printToEdocsBtn = document.getElementById('PrintToEDocsBtn');
    var closeBtn = document.getElementById('CloseBtn');
    function getValidatedData() {
        var data = {
            'docType': docTypePicker.value,
            'appId': appIdSelect.value,
            'description': descriptionTextarea.value,
            'internalComments': internalCommentsTextarea.value
        };

        var errorlist = [];
        if (data.docType < 0) {
            errorlist.push('Please select a valid Doc Type.');
        }
        if (!/^[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12}$/i.test(data.appId)) {
            errorlist.push('Please select a valid application.');
        }
        if (data.description.length > descriptionTextarea.getAttribute('maxlength')) {
            errorlist.push('Please ensure description meets the length requirement.');
        }
        if (data.internalComments.length > internalCommentsTextarea.getAttribute('maxlength')) {
            errorlist.push('Please ensure internal comments meets the length requirement.');
        }

        return  {
            'data': data,
            'errors': errorlist
        };
    }

    function SubmitForm() {
        var validatedData = getValidatedData();
        if (validatedData.errors.length > 0) {
            alert(validatedData.errors.join('\r\n'));
            return;
        }

        window.parent.LQBPopup.GetArguments().setEDocMetadata(validatedData.data);
        window.parent.LQBPopup.Return();
    }

    // _init
    TextareaUtilities.LengthCheckInit();
    $(printToEdocsBtn).on('click', SubmitForm);
    $(closeBtn).on('click', window.parent.LQBPopup.Hide);
    $(docTypePicker).on('change', function(){ printToEdocsBtn.disabled = false; });
})();
    </script>
    </form>
</body>
</html>
