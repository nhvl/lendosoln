﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectCustomPdfForm.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.SelectCustomPdfForm" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Select Custom PDF Form</title>
    <style type="text/css">
        body { background-color: gainsboro; }
        .text-center { text-align: center; }
        .padding-4 { padding: 4px; }
    </style>
    <script type="text/javascript">
        function _init() {
            $('#ClearSelectionRow').toggle(ML.DisplayClearButton);
            resize(600, 750);
        }

        function clearSelection() {
            var data = window.dialogArguments || {};
            data.ClearSelection = true;
            onClosePopup(data);
        }

        function selectForm(formId, formName) {
            var data = window.dialogArguments || {};
            data.ClearSelection = false;
            data.FormId = formId;
            data.FormName = formName;
            onClosePopup(data);
        }
    </script>
</head>
<body>
    <h4 class="page-header">Select Custom PDF Form</h4>
    <form id="form1" runat="server">
    <ml:CommonDataGrid id="FormList" runat="server">
		<alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
        <itemstyle cssclass="GridItem"></itemstyle>
        <headerstyle cssclass="GridHeader"></headerstyle>
		<Columns>
			<asp:TemplateColumn>
				<itemtemplate>
					<a href="#" onclick="selectForm(<%# AspxTools.JsString(Eval("FormId").ToString())%>, '<%# AspxTools.HtmlString(Eval("IsStandardForm").ToString()) == "True" ? Eval("Description").ToString() : AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Description").ToString())%>');">select</a>
				</itemtemplate>
			</asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Form">
				<itemtemplate>
					<%# AspxTools.HtmlString(Eval("IsStandardForm").ToString()) == "True" ? Eval("Description").ToString() : AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Description").ToString())%>
				</itemtemplate>
			</asp:TemplateColumn>
		</Columns>
	</ml:CommonDataGrid>
    <br />
    <div id="ClearSelectionRow" class="text-center">
        <input type="button" value="Clear Selection" onclick="clearSelection();" />
    </div>
    </form>
</body>
</html>
