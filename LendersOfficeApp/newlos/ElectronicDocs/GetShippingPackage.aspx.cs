﻿namespace LendersOfficeApp.newlos.ElectronicDocs
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.UI.WebControls;
    using DataAccess;
    using EDocs;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.HttpModule;
    using LendersOffice.Security;

    public partial class GetShippingPackage : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PerformanceMonitorItem item = PerformanceMonitorItem.Current;
            if (null != item)
            {
                // 6/23/2010 dd - Disabled Page Size Warning.
                item.IsEnabledPageSizeWarning = false;
            }
            Guid key = RequestHelper.GetGuid("key");

            HttpContext.Current.Server.ScriptTimeout = 15 * 60; 
            
            var csv = AutoExpiredTextCache.GetFromCache("SHIPPING_PACKAGE_REQUEST_" + key);

            string[] data = csv.Split(',');

            if (data.Length < 2)
            {
                return;
            }

            int downloadType = int.Parse(data[0]);

            List<Guid> docList = new List<Guid>(data.Length - 1);

            for (int i = 1; i < data.Length; i++)
            {
                docList.Add( new Guid(data[i]));
            }

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            if (principal.BrokerDB.IsTestingAmazonEdocs)
            {
                GenerateAmazonShippingPackageRequest(principal, downloadType, docList);
            }
            else
            {
                Tools.LogInfo("ShippingTemplateDocs", string.Join(",", docList));
                GenerateShippingPackage(downloadType, docList);
            }
        }

        private void GenerateAmazonShippingPackageRequest(AbstractUserPrincipal principal, int downloadType, List<Guid> docList)
        {
            Guid sLId = RequestHelper.GetGuid("sLId");

            string outputType = string.Empty;

            if (downloadType == 0)
            {
                outputType = "zip";
            }
            else if (downloadType == 1)
            {
                outputType = "pdf";
            }
            else if (downloadType == 2)
            {
                outputType = "zip_mergewithoutsig";
            }
            else
            {
                throw CBaseException.GenericException("Unsupported downloadType=[" + downloadType + "]");
            }

            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            Dictionary<Guid, EDocument> docsByLoanId = repo.GetDocumentsByLoanId(sLId).ToDictionary(p => p.DocumentId);

            List<Guid> documentIds = new List<Guid>();
            List<EDocument> documentList = new List<EDocument>();

            foreach (var docId in docList)
            {
                EDocument document = null;
                if (docsByLoanId.TryGetValue(docId, out document))
                {
                    documentList.Add(document);
                }
                else
                {
                    throw new FileNotFoundException("Document not found " + docId);
                }
            }

            string token = AmazonEDocHelper.GenerateShippingPackageRequest(principal, outputType, documentList);
            
            Response.Redirect(ConstStage.AmazonEdocsClientUrl + "/edocshippingpackage.aspx?key=" + token);
        }

        private void GenerateShippingPackage(int downloadType, List<Guid> docList)
        {
            Guid sLId = RequestHelper.GetGuid("sLId");
            string path;
            string fileName;
            string contentType;

            switch (downloadType)
            {
                case 0:
                    path = EDocumentViewer.WriteToZip(sLId, docList);
                    contentType = "application/zip";
                    fileName = "ShippingPackage.zip";
                    break;
                case 1:
                    path = EDocumentViewer.WriteSinglePdf(sLId, docList);
                    contentType = "application/pdf";
                    fileName = "ShippingPackage.pdf";
                    break;
                case 2:
                    path = EDocumentViewer.WriteToZipMergingDocsWithoutSignature(sLId, docList);
                    contentType = "application/zip";
                    fileName = "ShippingPackage.zip";
                    break;
                default:
                    return;
            }

            RequestHelper.SendFileToClient(HttpContext.Current, path, contentType, fileName);
            FileOperationHelper.Delete(path);
        }
    }
}
