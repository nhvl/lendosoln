﻿namespace LendersOfficeApp.newlos.ElectronicDocs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.ObjLib.ConsumerPortal;
    using LendersOffice.PdfForm;
    using MeridianLink.CommonControls;
    using LendersOffice.AntiXss;

    public partial class SelectForm : BaseLoanPage
    {
        private ILookup<Guid,SharedDocumentRequest> SharedItems { get; set; }

        protected Guid aAppId
        {
            get { return RequestHelper.GetGuid("applicationid"); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            EnableJqueryMigrate = false;
            this.DisplayCopyRight = false;
            m_formList.DataSource = PdfForm.RetrieveAllFormsOfCurrentBroker(LoanID, aAppId);
            m_formList.DataBind();

            if (RequestHelper.GetSafeQueryString("ie") == "1")
            {
                SharedItems = SharedDocumentRequest.GetRequestByLoanId(BrokerUser.BrokerId, LoanID, aAppId).Where(p => p.LinkedEDocId.HasValue).ToLookup(p => p.LinkedEDocId.Value);

                EdocTab.Visible = true;
                EDocumentRepository repository = EDocumentRepository.GetUserRepository();
                EDocSelector.DataSource =   repository.GetDocumentsByLoanId(LoanID);
                EDocSelector.DataBind();

            }

            IncludeStyleSheet("~/css/Tabs.css");

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);
        }

        protected void EDocSelector_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }

            EDocument doc = (EDocument)args.Item.DataItem;

            LinkButton select = (LinkButton)args.Item.FindControl("SelectDocument");
            HtmlAnchor preview = (HtmlAnchor)args.Item.FindControl("preview");
            preview.Attributes.Add("onclick", string.Format("previewDoc('{0}')", AspxTools.JsStringUnquoted(doc.DocumentId.ToString())));

            EncodedLiteral docType = (EncodedLiteral)args.Item.FindControl("DocType");
            EncodedLiteral description = (EncodedLiteral)args.Item.FindControl("Description");
            EncodedLiteral sharedPreviously = (EncodedLiteral)args.Item.FindControl("SharedPreviously");
            select.CommandArgument = doc.DocumentId.ToString();

            docType.Text = doc.FolderAndDocTypeName;
            description.Text = doc.PublicDescription;

            if (SharedItems.Contains(doc.DocumentId))
            {
                sharedPreviously.Text = "Yes";
            }
        }

        public static string GetEdocCacheKey(Guid userId, Guid docId, Guid rngKey)
        {
            string key = userId.ToString("N") + rngKey.ToString("N") + docId.ToString("N") + ".PDF";
            return key;
        }

        protected void SelectDocument_OnClick(object sender, EventArgs args)
        {
            LinkButton btn = (LinkButton)sender;

            var repo = EDocumentRepository.GetUserRepository();
            var doc = repo.GetDocumentById(new Guid(btn.CommandArgument));
            if (doc == null)
            {
                return;
            }

            if (doc.LoanId != LoanID)
            {
                return;
            }
            Guid randomGuid = Guid.NewGuid();
            string key = GetEdocCacheKey(BrokerUser.UserId, doc.DocumentId, randomGuid);
            string path = doc.GetPDFTempFile_Current();
            AutoExpiredTextCache.AddToCache(BinaryFileHelper.ReadAllBytes(path), TimeSpan.FromHours(2), key);
            ScriptManager.RegisterClientScriptBlock(this, typeof(SelectForm), "SelectEDoc", string.Format("selectEDoc('{0}', '{1}', '{2}');", doc.DocumentId, doc.Version, randomGuid), true); 
        }
    }
}
