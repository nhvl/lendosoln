﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadForm.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.UploadForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Upload Form</title>
    <style type="text/css">
        .upload-container {
            text-align: center;
        }
    </style>
</head>
<body bgcolor="gainsboro">
    <script type="text/javascript">
        function _init() {
            resizeForIE6And7(500, 195);

            var dragDropSettings = {
                blockUpload: !ML.CanUploadEdocs,
                blockUploadMsg: ML.UploadEdocsFailureMessage
            };

            registerDragDropUpload(document.getElementById('DragDropZone'), dragDropSettings);
        }

        function uploadDoc() {
            if (getFilesCount('DragDropZone') === 0) {
                LQBPopup.ShowString('Please select a file to upload.');
                return;
            }

            triggerOverlayImmediately();
            applyCallbackFileObjects('DragDropZone', function (file) {
                var args = {
                    LoanId: ML.sLId,
                };

                DocumentUploadHelper.Upload('UploadDocumentRequestForm', file, args, function (result) {
                    removeOverlay();

                    if (result.error) {
                        alert(result.UserMessage);
                        return;
                    }

                    var docId = result.value['DocId'];
                    var pageCount = result.value['PageCount'];
                    window.location.href = "SignatureTemplate.aspx?docid=" + docId + "&numpages=" + pageCount + "&file=" + encodeURIComponent(file.name);
                });
            });
        }
    </script>
    <h4 class="page-header">Upload PDF File</h4>
    <form id="form1" runat="server">
        <div class="InsetBorder full-width">
            <div id="DragDropZone" class="drag-drop-container" extensions=".pdf" limit="1"></div>
        </div>

        <div class="upload-container">
            <input type="button" onclick="uploadDoc();" value="Upload" />
        </div>
    </form>
</body>
</html>
