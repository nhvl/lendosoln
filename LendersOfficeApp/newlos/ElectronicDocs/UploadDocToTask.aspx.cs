﻿namespace LendersOfficeApp.newlos.ElectronicDocs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Security;
    using MeridianLink.CommonControls;

    public partial class UploadDocToTask : BasePage
    {
        private Dictionary<Guid, string> applicationNamesById;
        private int? condRequiredDocTypeId;

        protected void Page_Load(object sender, EventArgs e)
        {
            EnableJqueryMigrate = false;

            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("utilities.js");
            this.RegisterJsScript("UploadDocToTask.js");
            
            Task t = Task.Retrieve(PrincipalFactory.CurrentPrincipal.BrokerId, RequestHelper.GetSafeQueryString("TaskId"));
            TaskID.Text = t.TaskId;
            CategoryRep.Text = t.CondCategoryId_rep;
            RequiredDocType.Text = t.CondRequiredDocType_rep;
            DueDate.Text = t.DueDateDescription(true);
            Subject.Text = t.TaskSubject;

            this.condRequiredDocTypeId = t.CondRequiredDocTypeId;

            var loanId = RequestHelper.GetGuid("LoanId");
            CPageData loan = CPageData.CreateUsingSmartDependency(loanId, typeof(UploadDocToTask));
            loan.InitLoad();

            this.applicationNamesById = loan.Apps.ToDictionary(app => app.aAppId, app => app.aBNm_aCNm);

            var cacheKey = RequestHelper.GetSafeQueryString("CacheKey");
            var filenameJson = AutoExpiredTextCache.GetFromCacheByUser(PrincipalFactory.CurrentPrincipal, cacheKey);
            var filenames = SerializationHelper.JsonNetDeserialize<List<string>>(filenameJson);

            this.FileListingTable.DataSource = filenames;
            this.FileListingTable.DataBind();
        }

        protected void FileListingTable_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            var filename = e.Item.DataItem.ToString();
            var label = e.Item.FindControl("Filename") as EncodedLabel;
            label.Text = filename;

            var applicationDropdown = e.Item.FindControl("ApplicationDdl") as DropDownList;
            foreach (var appNameById in this.applicationNamesById)
            {
                applicationDropdown.Items.Add(new ListItem(appNameById.Value, appNameById.Key.ToString("N")));
            }

            if (this.condRequiredDocTypeId.HasValue)
            {
                var docTypePicker = e.Item.FindControl("DocType") as DocTypePickerControl;
                docTypePicker.Select(this.condRequiredDocTypeId.Value);
            }
        }
    }
}
