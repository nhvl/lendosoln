﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadDocToTask.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.UploadDocToTask" %>
<%@ Register TagPrefix="uc" TagName="DocTypePicker" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Condition-Document Upload</title>
    <style type="text/css">
       body { background-color: gainsboro; padding: 10px;  }
       table { table-layout: fixed; width: 100%;  }
       span.DocTypePicker { display: block; margin-top: 4px; }
       .file-row { text-align: center; }
       .ApplicationDdl { width: 200px; }
       .InvalidTextareaLength { color: red; }
       img.doc-type-required { vertical-align: top; }
       .max-width-cell { 
           max-width: 300px;
           word-wrap: break-word;
       }
    </style>
</head>
<body class="FieldLabel">
    <form id="form1" runat="server">
    <div>
        <table border="1" cellpadding="0" cellspacing="0" class="TaskDetails">
            <thead>
                <tr class="GridHeader">
                    <th>Condition</th>
                    <th>Category</th>
                    <th>Subject</th>
                    <th>Required DocType</th>
                    <th>Due Date</th>
                </tr>
            </thead>
            <tr class="GridItem">
                <td><ml:EncodedLiteral runat="server" ID="TaskID"></ml:EncodedLiteral></td>
                <td><ml:EncodedLiteral runat="server" ID="CategoryRep"></ml:EncodedLiteral></td>
                <td><ml:EncodedLiteral runat="server" ID="Subject"></ml:EncodedLiteral></td>
                <td><ml:EncodedLiteral runat="server" ID="RequiredDocType"></ml:EncodedLiteral></td>
                <td><ml:EncodedLiteral runat="server" ID="DueDate"></ml:EncodedLiteral>&nbsp;</td>
            </tr>
        </table>

        <br />

        <table>
            <tr>
                <th>File Name</th>
                <th>
                    Doc Type 
                    <img src="../../images/require_icon.gif" alt="Required" class="doc-type-required" />
                </th>
                <th>Description</th>
                <th>Application</th>
                <th>&nbsp;</th>
            </tr>
            <asp:Repeater runat="server" ID="FileListingTable" OnItemDataBound="FileListingTable_ItemDataBound">
                <ItemTemplate>
                    <tr class="file-row">
                        <td class="max-width-cell">
                            <ml:EncodedLabel runat="server" ID="Filename" CssClass="Filename"></ml:EncodedLabel>
                        </td>
                        <td class="max-width-cell">
                            <uc:DocTypePicker runat="server" ID="DocType" class="DocTypePicker" OnChange="checkUploadButtonState();" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="Description" TextMode="MultiLine" CssClass="Description" maxcharactercount="200"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ApplicationDdl" CssClass="ApplicationDdl"></asp:DropDownList>
                        </td>
                        <td>
                            <a class="remove-link">remove</a>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>

        <div class="align-right">
            <input type="button" id="UploadButton" value="Upload Document(s)" />
        </div>
    </div>

    <script type="text/javascript">
        $(function () {
            resizeForIE6And7(950, 400);
            TextareaUtilities.LengthCheckInit();
            checkUploadButtonState();

            $('.remove-link').click(removeDocument);
            $('#UploadButton').click(function () {
                uploadDocuments();
            });
        });
    </script>
    </form>
</body>
</html>
