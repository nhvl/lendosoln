﻿namespace LendersOfficeApp.newlos.ElectronicDocs
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;

    public partial class DocList : BaseLoanPage
    {
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode() => E_XUAComaptibleValue.IE10;

        /// <summary>
        /// Gets the extra workflow operations to check.
        /// </summary>
        /// <returns>The extra workflow operations to check.</returns>
        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[]
            {
                WorkflowOperations.UploadEDocs
            };
        }

        /// <summary>
        /// Gets the reasons for privileges that aren't allowed.
        /// </summary>
        /// <returns>The reasons for privileges that aren't allowed.</returns>
        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return this.GetExtraOpsToCheck();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData loanData = CPageData.CreateUsingSmartDependency(LoanID, typeof(DocList));
            loanData.InitLoad();
            CAppData app = loanData.GetAppData(0);
            this.Header.Title = string.Format("{0}, {1} - {2} - Documents", app.aBLastNm, app.aBFirstNm, loanData.sLNm);
            
            RegisterJsScript("json.js");
            EnableJqueryMigrate = false;
            RegisterJsScript("jquery.tablesorter.js");
            RegisterJsScript("utilities.js");
            this.RegisterJsScript("LQBPopup.js");

            RegisterService("edoc", "/newlos/ElectronicDocs/EDocsUtilitiesService.aspx");

            RejectedDocs.ShowOnlyActive = false;
            RejectedDocs.ShowOnlyInActive = true;
            ActiveDocs.ShowOnlyActive = true;
            ActiveDocs.ShowOnlyInActive = false;

            Tabs.IsInFrameset = false;
            IsAppSpecific = false;
            Tabs.AddToQueryString("loanid", LoanID.ToString());
            Tabs.RegisterControl("Active Docs", ActiveDocs);
            Tabs.RegisterControl("Rejected/Obsolete Docs", RejectedDocs);

            this.RegisterJsGlobalVariables("CanUploadEdocs", this.UserHasWorkflowPrivilege(WorkflowOperations.UploadEDocs));
            this.RegisterJsGlobalVariables("UploadEdocsFailureMessage", this.GetReasonUserCannotPerformPrivilege(WorkflowOperations.UploadEDocs));
        }
    }
}
