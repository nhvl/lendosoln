﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocuSignRecipientDashboard.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.DocuSignRecipientDashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" ng-app="DocuSignRecipientDashboard">
<head runat="server">
    <title>DocuSign Recipient Dashboard</title>
</head>
<body class="BackgroundGainsboro">
    <form id="form1" runat="server">
        <div id="RecipientTableControllerDiv" ng-controller="RecipientTableController">
            <div status-Details-Popup class="Hidden" id="StatusDetails" status-Details="StatusDetails"></div>
            <div class="Hidden" id="AuthResultDetails" auth-Details-Popup auth-Details="AuthResultDetails"></div>
            <div class="MainRightHeader">{{EnvelopeName}} Recipient Statuses</div>
            <table class="Table">
                <thead>
                    <tr class="LoanFormHeader">
                        <th class="NameColumn">Name</th>
                        <th class="TypeColumn">Type</th>
                        <th class="StatusColumn">Status</th>
                        <th class="SigningOrderColumn">Signing Order</th>
                        <th class="EmailColumn">Email</th>
                        <th class="AuthenticationResultsColumn">Authentication Results</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="ReverseGridAutoItem" ng-repeat="Recipient in Recipients">
                        <td>
                            <span class="Name">{{Recipient.Name}}</span>
                        </td>
                        <td>
                            <span class="Type">{{Recipient.Type}}</span>
                        </td>
                        <td>
                            <span class="Status">{{Recipient.Status}}</span>
                            <div ng-if="Recipient.StatusDates != null && Recipient.StatusDates.length > 0">
                                <a ng-click="ViewStatusDetails(Recipient)">view details</a>
                            </div>
                        </td>
                        <td>
                            <span class="SigningOrder">{{Recipient.SigningOrder}}</span>
                        </td>
                        <td>
                            <span class="Email">{{Recipient.Email}}</span>
                        </td>
                        <td>
                            <div ng-if="Recipient.AuthResults.length > 0">
                                <span>
                                    {{Recipient.AuthResults[0].Type}} - {{Recipient.AuthResults[0].Status}}
                                </span>
                            </div>
                            <a ng-click="ViewRecipientAuthResults(Recipient)">view details</a>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
    </form>
</body>
</html>
