﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using EDocs;
using LendersOffice.Common;
using LendersOfficeApp.ObjLib.Licensing;
using LendersOffice.Security;
using System.Diagnostics;
using LendersOffice.PdfLayout;
using CommonProjectLib.Common.Lib;
using System.Text;
using LendersOffice.ObjLib.DocumentConditionAssociation;
using LendersOffice.ObjLib.Task;
using System.Web.Services;
using EDocs.Utils;
using LendersOffice.Constants;
using LendersOffice.Admin.DocuSign;

namespace LendersOfficeApp.newlos.ElectronicDocs
{

    public partial class EditEDocService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        
        private class DeleteInsertedPagesItem
        {
            public int Version = -1;
            public Guid DocId = Guid.Empty;
            public string Json = null;
        }

        public class AppendPagesItem
        {
            public Guid AppendTo = Guid.Empty;
            public List<PdfPageItem> PagesToAppend = new List<PdfPageItem>();
            public List<EDocumentAnnotationItem> AnnotationsToAppend = new List<EDocumentAnnotationItem>();
        }

        protected override void Process(string methodName)
        {
            switch (methodName) 
            {
                case "SaveData":
                    SaveData();
                    break;
                case "SplitNewDoc":
                    SplitNewDoc();
                    break;
                case "PreviewDoc":
                    PreviewDoc();
                    break;
                case "SaveNonPdfData":
                    SaveNonPdfData();
                    break;
                case "StorePrintNoteData":
                    StorePrintNoteData();
                    break;
                case "GetEdocsPageInfo":
                    GetEdocsPageInfo();
                    break;
                case "RecordAuditEvent":
                    RecordAuditEvent();
                    break;
                case "Poll":
                    RefreshAudit();
                    break;
                case "GetPages":
                    GetPageInfo();
                    break;
                case "GetBatchPages":
                    GetBatchPageInfo();
                    break;
                case "SaveBulk":
                    SaveBulk();
                    break;
                case "GetBatchDocDetails":
                    GetDocumentInfo();
                    break;
                case "SaveSignOff":
                    SaveSignOff();
                    break;
                case "DeleteInsertedPages":
                    var docsToTrim = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<DeleteInsertedPagesItem>>(GetString("json"));
                    foreach (var doc in docsToTrim)
                    {
                        var pagesLeft = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<PdfPageItem>>(doc.Json);                        
                        SaveSimple(doc.DocId, pagesLeft, doc.Version);
                    }
                    break;
                case "CheckDuplicateProtectedDocs":
                    CheckDuplicateProtectedDocs();
                    break;
                case "CheckDuplicateProtectedDocsBulk":
                    CheckDuplicateProtectedDocsBulk();
                    break;
                case nameof(CreateEditableCopy):
                    this.CreateEditableCopy();
                    break;
            }
        }

        [WebMethod]
        public static void AppendPages(List<AppendPagesItem> docsToAppend)
        {
            foreach (var appendInfo in docsToAppend)
            {
                AppendToDoc(appendInfo.AppendTo,
                            appendInfo.PagesToAppend,
                            appendInfo.AnnotationsToAppend);
            }   
        }
        
        /// <summary>
        /// Takes a mapping of task Id : associated or not and saves it. Restricts what can be saved based on the current user's actual permissions.
        /// </summary>
        /// <param name="toSave"></param>
        private static void SaveConditionAssociations(Dictionary<string, bool> toSave, Guid LoanId, Guid DocId, E_EDocStatus DocStatus)
        {
            var allConditions = DocumentConditionAssociation.GetConditionsByLoanDocument(PrincipalFactory.CurrentPrincipal, LoanId, DocId, excludeThoseUserDoesntHaveAccessTo: false);

            IEnumerable<AssociatedTask> editableConditions = allConditions;

            var docIsProtected = DocStatus == E_EDocStatus.Approved;

            if (docIsProtected && !BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowEditingApprovedEDocs))
            {
                editableConditions = from assoc in allConditions
                                     where assoc.Association == null || assoc.Association.Status != E_DocumentConditionAssociationStatus.Satisfied
                                     select assoc;
            }

            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;

            var changes = new List<DocumentConditionAssociation>();
            foreach (var cond in editableConditions)
            {
                bool associate;

                if (!toSave.TryGetValue(cond.Condition.TaskId, out associate)) continue;

                if (cond.IsAssociated != associate)
                {
                    if (cond.IsAssociated)
                    {
                        cond.Association.Delete();
                    }
                    else
                    {
                        var temp = new DocumentConditionAssociation(brokerId, LoanId, cond.Condition.TaskId, DocId);
                        temp.Save();
                    }
                }
            }
        }

        private void SaveSignOff()
        {


            Permission[] requiredEditPermissions = new Permission[] {
                    Permission.AllowUnderwritingAccess,
                    Permission.CanViewEDocs,
                    Permission.CanEditEDocs,
                    Permission.AllowEditingApprovedEDocs
            };

            foreach (Permission p in requiredEditPermissions)
            {
                if (PrincipalFactory.CurrentPrincipal.HasPermission(p) == false)
                {
                    throw new AccessDenied();
                }
            }

            List<EDocumentAnnotationItem> items = null;
            List<EDocBatchEditDetails> details = ObsoleteSerializationHelper.JsonDeserialize<List<EDocBatchEditDetails>>(GetString("Data"));
            List<PdfField> signatures = ObsoleteSerializationHelper.JsonDeserialize<List<PdfField>>(GetString("Signatures", "[]"));
            List<Tuple<int, EDocBatchEditDetails>> pageToDocMap = new List<Tuple<int, EDocBatchEditDetails>>();
            Dictionary<Guid, List<PdfField>> signaturesByDoc = new Dictionary<Guid, List<PdfField>>();
            string taskd = GetString("TaskId");
            Guid loanId = GetGuid("LoanId");
            HashSet<Guid> processedDocIds = new HashSet<Guid>();
            Dictionary<Guid, DocumentConditionAssociation> associationsForTaskByEdocId = DocumentConditionAssociation.GetAssociationsByLoanCondition(BrokerUserPrincipal.CurrentPrincipal.BrokerId, loanId, taskd).ToDictionary(p => p.DocumentId);

            Task t = Task.Retrieve(BrokerUserPrincipal.CurrentPrincipal.BrokerId, taskd);
            t.CondCategoryId = GetInt("ConditionCategory");

            if(t.CondCategoryId.HasValue)
            {
                t.TaskPermissionLevelId = ConditionCategory.GetCategory(PrincipalFactory.CurrentPrincipal.BrokerId, t.CondCategoryId.Value).DefaultTaskPermissionLevelId;
            }

            t.TaskSubject = GetString("ConditionSubject");
            t.CondInternalNotes = GetString("ConditionInternalNotes");

            if (GetBool("CloseCondition"))
            {
                t.Close();
            }
            else
            {
                t.Save(false);
            }

            //Create a mapping between page and doc.
            foreach (EDocBatchEditDetails docDetails in details)
            {
                docDetails.NewAnnotations = new List<EDocumentAnnotationItem>();
                for (int i = 1; i <= docDetails.PageCount; i++)
                {
                    pageToDocMap.Add(Tuple.Create(i, docDetails));
                }
            }


            //fix up any new notes with page numbers.
            if (GetBool("LoadedAnnotations") && PrincipalFactory.CurrentPrincipal.HasPermission(Permission.CanEditEDocsInternalNotes))
            {
                items = ObsoleteSerializationHelper.JsonDeserialize<List<EDocumentAnnotationItem>>(GetString("NewAnnotations"));
                foreach (EDocumentAnnotationItem item in items)
                {
                    var pageInfo = pageToDocMap[item.PageNumber - 1]; //we need to update the page numbers 
                    item.PageNumber = pageInfo.Item1;
                    pageInfo.Item2.NewAnnotations.Add(item);
                }
            }

            //update the signature page numbers and seperate them by doc.
            foreach (PdfField signature in signatures)
            {
                var pageInfo = pageToDocMap[signature.PageNumber - 1];
                signature.PageNumber = pageInfo.Item1;
                 List<PdfField> signaturesForDoc;

                 if (false == signaturesByDoc.TryGetValue(pageInfo.Item2.DocumentId, out signaturesForDoc))
                 {
                     signaturesForDoc = new List<PdfField>();
                     signaturesByDoc.Add(pageInfo.Item2.DocumentId, signaturesForDoc);
                 }

                 signaturesForDoc.Add(signature);
            }

            
            bool canEditApprovedDocs = BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowEditingApprovedEDocs);
            bool canScreenDocs = BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowScreeningEDocs);
            bool canApproveRejectDocs = BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowApprovingRejectingEDocs);


            StringBuilder result = new StringBuilder();
            List<string> versionFailures = new List<string>();

            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            int[] finalVersions = new int[details.Count];  //populate with original

            //the actual save.
            for (int i = 0; i < details.Count; i++)
            {
                EDocBatchEditDetails doc = details[i];
                finalVersions[i] = doc.Version;
                
                EDocument edoc = repo.GetDocumentById(doc.DocumentId);
                if (processedDocIds.Contains(doc.DocumentId))
                {
                    result.AppendFormat("Ignoring changes for {0}. There were duplicates in the sign off editor.\n\n", edoc.FolderAndDocTypeName);
                    continue;
                }
                E_EDocStatus originalStatus = edoc.DocStatus;

                if (edoc.Version != doc.Version)
                {
                    versionFailures.Add(doc.DocTypeDescription);
                    continue;
                }

                edoc.DocumentTypeId = doc.DocTypeId;
                edoc.InternalDescription = doc.InternalDescription;
                edoc.PublicDescription = doc.Description;


                if (items != null)
                {
                    edoc.InternalAnnotations.Update(PrincipalFactory.CurrentPrincipal, doc.NewAnnotations);
                }
                edoc.DocStatus = doc.DocumentStatus;
                edoc.StatusDescription = doc.DocumentStatusReasonDescription;

                List<PdfField> signaturesForDoc;

                if (signaturesByDoc.TryGetValue(edoc.DocumentId, out signaturesForDoc) && signaturesForDoc.Count >0) 
                {
                    string nonDestructivePdfDocumentId = string.Empty;
                    try
                    {
                        nonDestructivePdfDocumentId = EDocumentViewer.AddSignatures(BrokerUserPrincipal.CurrentPrincipal, edoc, signaturesForDoc);
                    }
                    catch (CBaseException e) //should not happen.
                    {
                        result.Append("Document not saved : " + e.UserMessage);
                        continue;
                    }
                    repo.Save(edoc);

                    if (!string.IsNullOrEmpty(nonDestructivePdfDocumentId))
                    {
                        // 7/17/2017 - dd - OPM 453261 - Enqueue the document for pdf rasterizer after save. Otherwise the meta data from pdf rasterizer will get override in save.
                        EDocument.EnqueueNonDestructivePdfRasterizeRequest(edoc.BrokerId, edoc.DocumentId, nonDestructivePdfDocumentId, EDocument.E_NonDestructiveSourceT.OverwriteExisting, EDocument.POST_ACTION_UPDATE_METADATA_EXCLUDE_ORIGINAL);
                        EDocument.UpdateImageStatus(edoc.BrokerId, edoc.DocumentId, E_ImageStatus.NoCachedImagesButInQueue);
                    }
                }
                else
                {
                    repo.SaveNonPdfData(edoc);
                }


                DocumentConditionAssociation curr;

                if (false == associationsForTaskByEdocId.TryGetValue(edoc.DocumentId, out curr) || curr.Status != doc.DocAssocStatus)
                {
                    //they can always add new. Existing : if the status is approved and you cannot edit approved docs you cannot save associations
                    if (curr == null || canEditApprovedDocs || (canEditApprovedDocs == false && originalStatus != E_EDocStatus.Approved))
                    {
                        DocumentConditionAssociation y = new DocumentConditionAssociation(BrokerUserPrincipal.CurrentPrincipal.BrokerId, loanId, taskd, doc.DocumentId);
                        y.Status = doc.DocAssocStatus;
                        y.Save();
                    }
                }


                edoc = repo.GetDocumentById(doc.DocumentId);
                finalVersions[i] = edoc.Version;
            }

            SetResult("SaveResults", result.ToString());
            if (versionFailures.Count > 0)
            {
                SetResult("VersionMismatches", "Following eDocs failed to save because this editor no longer has their latest versions. \n\n" + String.Join("\n", versionFailures.ToArray()) + "\n\n Please reopen the batch editor.");
            }

            SetResult("NewVersions", ObsoleteSerializationHelper.JsonSerialize(finalVersions));
        }

        private void SaveBulk()
        {
            List<EDocBatchEditDetails> details = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<EDocBatchEditDetails>>(GetString("Data"));
            List<EDocumentAnnotationItem> annotations = null;
            List<EDocumentAnnotationItem> eSignTags = null;

            List<Tuple<int, EDocBatchEditDetails>> pages = new List<Tuple<int, EDocBatchEditDetails>>();
            foreach (EDocBatchEditDetails docDetails in details)
            {
                docDetails.NewAnnotations = new List<EDocumentAnnotationItem>();
                docDetails.NewESignTags = new List<EDocumentAnnotationItem>();
                for (int i = 1; i <= docDetails.PageCount; i++)
                {
                    pages.Add(Tuple.Create(i, docDetails));
                }
            }

            if (GetBool("LoadedAnnotations") && PrincipalFactory.CurrentPrincipal.HasPermission(Permission.CanEditEDocsInternalNotes))
            {
                annotations = ObsoleteSerializationHelper.JsonDeserialize<List<EDocumentAnnotationItem>>(GetString("NewAnnotations"));
                
                //we need to find out which docs the annotations belong to and store them.
                //Keep the edoc page the full page maps to in the document along with its document details.
                //we can then query  based on full page  pages[pg] { PageINDoc, Doc } 
                foreach( EDocumentAnnotationItem item in annotations )
                {
                    var pageInfo = pages[item.PageNumber-1];
                    item.PageNumber = pageInfo.Item1; 
                    pageInfo.Item2.NewAnnotations.Add(item);
                }
            }

            eSignTags = ObsoleteSerializationHelper.JsonDeserialize<List<EDocumentAnnotationItem>>(GetString("NewESignTags", null));
            foreach (var tag in eSignTags)
            {
                var pageInfo = pages[tag.PageNumber - 1];
                tag.PageNumber = pageInfo.Item1;
                pageInfo.Item2.NewESignTags.Add(tag);
            }

            bool canEditApprovedDocs = BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowEditingApprovedEDocs);
            bool canScreenDocs = BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowScreeningEDocs);
            bool canApproveRejectDocs = BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowApprovingRejectingEDocs);
            StringBuilder result = new StringBuilder();
            List<string> versionFailures = new List<string>();

            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            int[] finalVersions = new int[details.Count];  //populate with original
            int[] finalStatuses = new int[details.Count];

            var docuSignSettings = LenderDocuSignSettings.Retrieve(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            var canEditTags = BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowCreatingDocumentSigningEnvelopes) &&
                              docuSignSettings != null && docuSignSettings.IsSetupComplete() && docuSignSettings.DocuSignEnabled;

            for(int i = 0; i < details.Count; i++)
            {
                EDocBatchEditDetails doc = details[i];
                finalVersions[i] = doc.Version;
                EDocument edoc = repo.GetDocumentById(doc.DocumentId);
                if (!canEditApprovedDocs && edoc.DocStatus == E_EDocStatus.Approved)
                {
                    continue;
                }

                if (edoc.Version != doc.Version)
                {
                    versionFailures.Add(doc.DocTypeDescription);
                    continue;
                }

                //if (edoc.DocStatus == E_EDocStatus.Approved && canEditApprovedDocs == false )
                //{
                //    result.AppendFormat("Editing approved docs permission required to modify approved doc {0}.\n\n", edoc.FolderAndDocTypeName); 
                //    continue;
                //}

                edoc.DocumentTypeId = doc.DocTypeId;
                edoc.InternalDescription = doc.InternalDescription;
                edoc.PublicDescription = doc.Description;

                if (annotations != null) 
                {
                    edoc.InternalAnnotations.Update(PrincipalFactory.CurrentPrincipal, doc.NewAnnotations);
                }

                //Ignore saving status if it's invalid
				//Used for handling Duplicate Protected Documents (OPM 106903)
                if (Enum.IsDefined(typeof(E_EDocStatus), doc.DocumentStatus))
                {
                    if (((edoc.DocStatus != doc.DocumentStatus) && (doc.DocumentStatus == E_EDocStatus.Approved || doc.DocumentStatus == E_EDocStatus.Rejected)) && canApproveRejectDocs == false)
                    {
                        //they cannot approve or reject! how di this happen?    
                        result.AppendFormat("Cannot mark doc {0} as approved/rejected without can approve or reject permission.\n\n", edoc.FolderAndDocTypeName);

                    }
                    else if (edoc.DocStatus != doc.DocumentStatus && doc.DocumentStatus == E_EDocStatus.Screened && canScreenDocs == false)
                    {
                        //they cannot screen! how did this happen
                        result.AppendFormat("Cannot mark doc {0} as screen without screening permission.\n\n", edoc.FolderAndDocTypeName);
                    }
                    else
                    {
                        edoc.DocStatus = doc.DocumentStatus;
                        edoc.StatusDescription = doc.DocumentStatusReasonDescription;
                    }
                }

                if (canEditTags)
                {
                    edoc.ESignTags.Update(PrincipalFactory.CurrentPrincipal, doc.NewESignTags);
                }

                repo.SaveNonPdfData(edoc);
                edoc = repo.GetDocumentById(doc.DocumentId);
                finalVersions[i] = edoc.Version;
                finalStatuses[i] = (int) edoc.DocStatus;
            }

            SetResult("SaveResults", result.ToString());
            if( versionFailures.Count > 0 )
            {
                SetResult("VersionMismatches", "Following eDocs failed to save because this editor no longer has their latest versions. \n\n" + String.Join("\n", versionFailures.ToArray()) + "\n\n Please reopen the batch editor.");
            }

            SetResult("NewVersions", ObsoleteSerializationHelper.JavascriptJsonSerialize(finalVersions));
            SetResult("NewStatuses", ObsoleteSerializationHelper.JavascriptJsonSerialize(finalStatuses));
        }

        private void GetPageInfo()
        {
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            var edoc = repo.GetDocumentById(GetGuid("DocId"));

            List<PdfPageItem> items = edoc.GetPdfPageInfoList();
            if (items.Count > 0)
            {
                items[0].Name = edoc.FolderAndDocTypeName; 
            }
            string data = ObsoleteSerializationHelper.JavascriptJsonSerialize(items);
            SetResult("PagesJSON", data);
        }

        private void GetBatchPageInfo()
        {
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            List<Guid> docIds = (from docId in GetString("DocIds").Split(',')
                                    select new Guid(docId)).ToList();

            int pageCount = 0;
            List<PdfPageItem> pages = new List<PdfPageItem>();
            List<EDocBatchEditDetails> details = new List<EDocBatchEditDetails>();
            var canEditApprovedDocs = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowEditingApprovedEDocs);
            foreach (Guid docId in docIds)
            {
                var edoc = repo.GetDocumentById(docId);
                if (edoc.DocStatus == E_EDocStatus.Approved && !canEditApprovedDocs)
                {
                    // Old batch editor cannot edit protected docs.
                    return;
                }

                List<PdfPageItem> items = edoc.GetPdfPageInfoList();
                if (items.Count > 0)
                {
                    items[0].Name = edoc.FolderAndDocTypeName;
                }

                pages.AddRange(items);

                details.Add(new EDocBatchEditDetails(edoc, pageCount));
                pageCount += edoc.PageCount;
            }

            SetResult("PagesJson", ObsoleteSerializationHelper.JsonSerialize(pages));
            SetResult("BatchEDocSet", ObsoleteSerializationHelper.JsonSerialize(details));
        }

        private void GetDocumentInfo()
        {
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            var edoc = repo.GetDocumentById(GetGuid("DocId"));

            List<PdfPageItem> items = edoc.GetPdfPageInfoList();
            if (items.Count > 0)
            {
                items[0].Name = edoc.FolderAndDocTypeName; 
            }
            string data = ObsoleteSerializationHelper.JavascriptJsonSerialize(items);
            var details = new EDocBatchEditDetails(edoc, 0);
            details.Folder = edoc.Folder.FolderNm;
            details.DocType = edoc.DocTypeName;
            var annotations = edoc.InternalAnnotations.GenerateEditorList(BrokerUserPrincipal.CurrentPrincipal);
            SetResult("PagesJSON", data);
            SetResult("AnnotationsJSON", ObsoleteSerializationHelper.JsonSerialize(annotations));
            SetResult("ESignTags", ObsoleteSerializationHelper.JsonSerialize(edoc.ESignTags.GenerateEditorList(BrokerUserPrincipal.CurrentPrincipal)));
            SetResult("DocDetails", ObsoleteSerializationHelper.JsonSerialize(details));
        }

        private void RefreshAudit()
        {
            string key = GetString("StorageKey", "");
            if (string.IsNullOrEmpty(key))
            {
                return;
            }
            AutoExpiredTextCache.RefreshCacheDuration(key, TimeSpan.FromHours(4));
        }

        private void RecordAuditEvent()
        {
            Guid documentId = GetGuid("DocumentId");
            Guid userId = BrokerUserPrincipal.CurrentPrincipal.UserId;
            int eventId = GetInt("EventId");
            string storageKey = GetString("StorageKey", "");
            EDocumentIntermediateAuditTracker userActions;

            if (string.IsNullOrEmpty(storageKey))
            {
                userActions = new EDocumentIntermediateAuditTracker(documentId, BrokerUserPrincipal.CurrentPrincipal.UserId);
            }
            else
            {
                string data = AutoExpiredTextCache.GetFromCache(storageKey);
                userActions = ObsoleteSerializationHelper.JsonDeserialize<EDocumentIntermediateAuditTracker>(data);   
            }

            userActions.AddEvent((E_PdfUserAction)eventId);

            if (string.IsNullOrEmpty(storageKey))
            {
                storageKey = AutoExpiredTextCache.AddToCache(ObsoleteSerializationHelper.JsonSerialize(userActions), TimeSpan.FromHours(4));
            }
            else
            {
                AutoExpiredTextCache.UpdateCache(ObsoleteSerializationHelper.JsonSerialize(userActions), storageKey);
            }
            SetResult("StorageKey", storageKey);
        }

        private void CheckDuplicateProtectedDocs()
        {
            int docTypeId = GetInt("DocTypeId");
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();

            Guid sLId = GetGuid("LoanId");
            var docList = repo.GetDocumentsByLoanId(sLId);
            EDocument thisDoc = repo.GetDocumentById(GetGuid("DocId"));
                    
            foreach (EDocument doc in docList)
            {
                if (doc.DocumentTypeId == docTypeId && doc.DocStatus == E_EDocStatus.Approved && doc.DocumentId!=thisDoc.DocumentId)
                {
                    //If at least one other document with this DocType is also protected
                    //Javascript will delegate to "DuplicateProtectedDialog.aspx"
                    var details = new EDocBatchEditDetails(thisDoc, 0);
                    SetResult("DuplicateProtectedDoc", ObsoleteSerializationHelper.JsonSerialize(details));
                    return;
                }
            }
        }

        private void CheckDuplicateProtectedDocsBulk()
        {
            List<EDocBatchEditDetails> details = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<EDocBatchEditDetails>>(GetString("Data"));
            var numDocsPerType = new Dictionary<int, int>();

            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            Guid sLId = GetGuid("LoanId");
            var docList = repo.GetDocumentsByLoanId(sLId);

            //Count how many "Protected" documents exist for each DocType
            
            //(excluding the documents in the current edit batch)
            foreach (EDocument doc in docList)
            {
                if (doc.DocStatus == E_EDocStatus.Approved && details.Find(detail => detail.DocumentId == doc.DocumentId)==null)
                {
                    if (!numDocsPerType.ContainsKey(doc.DocumentTypeId))
                        numDocsPerType[doc.DocumentTypeId] = 0;
                    numDocsPerType[doc.DocumentTypeId] = numDocsPerType[doc.DocumentTypeId] + 1;
                }
            }
            //(now counting the documents in the current edit batch)
            foreach (EDocBatchEditDetails detail in details)
            {
                if (detail.DocumentStatus == E_EDocStatus.Approved)
                {
                    if (!numDocsPerType.ContainsKey(detail.DocTypeId))
                        numDocsPerType[detail.DocTypeId] = 0;
                    numDocsPerType[detail.DocTypeId] = numDocsPerType[detail.DocTypeId] + 1;
                }
            }

            var duplicateProtectedDocTypes = new Dictionary<int, List<EDocBatchEditDetails>>();
            var completeEditList = new List<Guid>();
            foreach (EDocBatchEditDetails detail in details)
            {
                //Store just the Guid DocId for each document in the current batch
                completeEditList.Add(detail.DocumentId);

                
                if (detail.DocumentStatus == E_EDocStatus.Approved && numDocsPerType[detail.DocTypeId]>1)
                {
                    //If a protected document is in a doctype with more than 1 protected count
                    //start/add a list of DocIds from the current batch
                    if (!duplicateProtectedDocTypes.ContainsKey(detail.DocTypeId))
                        duplicateProtectedDocTypes.Add(detail.DocTypeId, new List<EDocBatchEditDetails>());
                    duplicateProtectedDocTypes[detail.DocTypeId].Add(detail);
                }
            };

            //Save JSON for passing to DuplicateProtectedDialog.aspx
            SetResult("DuplicateProtectedDocList", ObsoleteSerializationHelper.JsonSerialize(duplicateProtectedDocTypes.Values.AsEnumerable()));
            SetResult("DuplicateProtectedDocTypeIdList", ObsoleteSerializationHelper.JsonSerialize(duplicateProtectedDocTypes.Keys.AsEnumerable()));
            SetResult("CompleteEditList", ObsoleteSerializationHelper.JsonSerialize(completeEditList));

        }

        private void GetEdocsPageInfo()
        {
            string sDocId = GetString("docid");
            Guid docId = new Guid(sDocId.Replace("'",""));
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            EDocument eDoc = repo.GetDocumentById(docId);

            var list = eDoc.GetPdfPageInfoList();

            SetResult("EDocPages", ObsoleteSerializationHelper.JsonSerialize(list));
            this.SetResult("IsSigned", eDoc.IsESigned);
        }
        private void StorePrintNoteData()
        {
            AbstractUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
            List<EDocumentAnnotationItem> annotations = ObsoleteSerializationHelper.JsonDeserialize<List<EDocumentAnnotationItem>>(GetString("Annotations"));
            EDocumentAnnotationContainer c = new EDocumentAnnotationContainer(principal, annotations);

            if (annotations != null && annotations.Any(p => p.LastModifiedDate == DateTime.MinValue))
            {
                Tools.LogWarning("[EDocAnnotations]  BadDates:" + GetString("Annotations"));
            }

            if (principal.BrokerDB.IsTestingAmazonEdocs)
            {
                var repository = EDocumentRepository.GetUserRepository();
                var document = repository.GetDocumentById(GetGuid("DocID"));
                List<PdfPageItem> pageList = ObsoleteSerializationHelper.JsonDeserialize<List<PdfPageItem>>(GetString("Pages"));
                string amazonToken = AmazonEDocHelper.GenerateViewPdfRequest(principal, document.DocTypeName, pageList, annotations);
                SetResult("Key", amazonToken);
            }
            else
            {
                string g = Tools.ShortenGuid(Guid.NewGuid());
                AutoExpiredTextCache.AddToCache(c.GetXmlContent(), TimeSpan.FromMinutes(5), g + "_AnnotationXml");
                AutoExpiredTextCache.AddToCache(GetString("Pages"), TimeSpan.FromMinutes(5), g + "_PdfPageItemListJson");

                SetResult("Key", g);
            }


        }

        private void PreviewDoc()
        {
            string json = GetString("EDocPages");

            string key = AutoExpiredTextCache.AddToCache(json, new TimeSpan(0, 5, 0)); // Cache information for 5 minutes.
            SetResult("Key", key);       
        }

        private void SplitNewDoc()
        {
            Guid sLId = GetGuid("LoanId");
            string json = GetString("Json");
            Guid sourceDocId = GetGuid("DocumentId");
            List<SplitDocumentJob> list = ObsoleteSerializationHelper.JsonDeserialize<List<SplitDocumentJob>>(json);

            if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowApprovingRejectingEDocs) && list.Any(p => p.Status == E_EDocStatus.Rejected || p.Status == E_EDocStatus.Approved))
            {
                //should look into moving this into datalayer.
                throw new CBaseException("Cannot approve/reject documents.", "User doesn't have permission to reject or approve.");
            }

            if (list.Count == 0)
            {
                return;
            }

            // 6/24/2015 dd - This could mean it is a non destructive edocs split.
            //                Check for non destructive edocs by look at version.
            // 3/6/2018 dd - All new eDocs are non destructive. However we still have significant amount
            //               of old legacy edocs. Ideally, we should convert those legacy to non destructive and remove
            //               code that do switch between non destructive and legay.
            PNGEncryptedData encryptedData = new PNGEncryptedData(list.First().EDocPages.First().PngKey);
            bool isNonDestructiveEdocs = encryptedData.Version == PNGEncryptedData.NonDestructiveEdocsVersion;

            EDocumentSplitter splitter = new EDocumentSplitter(PrincipalFactory.CurrentPrincipal, sourceDocId, list);
            Guid[] ids = splitter.PerformSplitAndCreateDocs(isNonDestructiveEdocs);

            for (int y = 0; y < ids.Length; y++)
            {
                SetResult("DocId" + y, ids[y]);
            }

            string reportOutput = PerformanceStopwatch.ReportOutput;
            Tools.LogInfo("SplitEDocTimings", reportOutput);
        }

        private void SaveNonPdfData()
        {
            Guid docId = GetGuid("DocId");
            int version = GetInt("Version");
            string publicDescription = GetString("PublicDescription");
            string description = GetString("Description");
            int docTypeId = GetInt("DocTypeId");
            string annotationsStr = GetString("Annotations", "[]");
            string eSignTagsStr = GetString("ESignTags", "[]");
            E_EDocStatus status = (E_EDocStatus)GetInt("Status");
            List<EDocumentAnnotationItem> annotations = null;
            bool HideFromPML = GetBool("HideFromPML");

            AbstractUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            EDocument eDoc = repo.GetDocumentById(docId);
            SaveConditionAssociations(eDoc);

            //If the user doesn't have permission to edit approved docs and this doc is approved, all they're allowed to do is save condition
            //associations.
            if (eDoc.DocStatus == E_EDocStatus.Approved &&
                principal.HasPermission(Permission.AllowEditingApprovedEDocs) == false)
            {
                SetResult("AssociationsOnly", true);
                return;
            }


            //in the rare case that the user was given permission to save annotations AFTER he loaded the page this will prevent
            //the updater from wiping all the annotations (since it does merging) and any mission annotations from this list will be deleted. 
            if (GetBool("CanEditAnnotations"))
            {
                annotations = ObsoleteSerializationHelper.JsonDeserialize<List<EDocumentAnnotationItem>>(annotationsStr);
            }

            List<EDocumentAnnotationItem> eSignTags = null;
            if (GetBool("CanEditESignTags"))
            {
                eSignTags = ObsoleteSerializationHelper.JsonDeserialize<List<EDocumentAnnotationItem>>(eSignTagsStr);
            }

            string storageKey = GetString("StorageKey", "");
            EDocumentIntermediateAuditTracker auditTracker = EDocumentIntermediateAuditTracker.Retrieve(storageKey, principal.UserId);
            EDocumentViewer.SaveNonPdfInfo(docId, version, status, docTypeId, description, publicDescription, HideFromPML, annotations, eSignTags, auditTracker);

            var items = eDoc.InternalAnnotations.GenerateEditorList(principal);
            SetResult("Annotations", ObsoleteSerializationHelper.JsonSerialize(items));
            SetResult("PagesWithAnnotations", ObsoleteSerializationHelper.JsonSerialize(eDoc.GetPagesWithAnnotationsAndTags()));
            SetResult("ESignTags", ObsoleteSerializationHelper.JsonSerialize(eDoc.ESignTags.GenerateEditorList(principal)));

            // Fetch the most recent version from the database, which will have been
            // updated by the non-PDF info save. The other access properties will
            // be automatically updated by SaveNonPdfInfo, so load only the version
            // instead of loading the entire document object.
            SetResult("Version", EDocument.GetDocumentVersionById(eDoc.DocumentId, eDoc.BrokerId));

            List<PdfPageItem> pages = eDoc.GetPdfPageInfoList();

            SetResult("EDocPages", ObsoleteSerializationHelper.JsonSerialize(pages));
        }

        private void SaveConditionAssociations(Guid docId)
        {
            SaveConditionAssociations(EDocumentRepository.GetUserRepository().GetDocumentById(docId));
        }

        private void SaveConditionAssociations(EDocument eDoc)
        {
            var cond = GetString("ConditionAssociations");
            Dictionary<string, bool> conditionAssociations = null;
            if (!string.IsNullOrEmpty(cond))
            {
                conditionAssociations = ObsoleteSerializationHelper.JavascriptJsonDeserializer<Dictionary<string, bool>>(cond);
            }

            if (conditionAssociations != null && conditionAssociations.Count > 0)
            {
                SaveConditionAssociations(conditionAssociations, eDoc.LoanId.Value, eDoc.DocumentId, eDoc.DocStatus);
            }
        }

        private static void SaveSimple(Guid docId, List<PdfPageItem> pages, int version)
        {
            var repo = EDocumentRepository.GetUserRepository();
            var doc = repo.GetDocumentById(docId);
            SaveSimple(docId, pages, version, doc.InternalAnnotations.AnnotationList.ToList(), doc.ESignTags.AnnotationList.ToList());
        }

        private static void SaveSimple(Guid docId, IEnumerable<PdfPageItem> pages, int version, IEnumerable<EDocumentAnnotationItem> annotations, IEnumerable<EDocumentAnnotationItem> eSignTags)
        {
            var repo = EDocumentRepository.GetUserRepository();
            var doc = repo.GetDocumentById(docId);
            bool updated;

            if (null == pages || pages.Count() == 0)
            {
                repo.DeleteDocument(docId, "User removed all pages.");
                return;
            }

            AbstractUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
            var temp = new EDocumentIntermediateAuditTracker(docId, principal.UserId);

            EDocumentViewer.SavePages(principal, doc.DocumentId,
                version,
                int.Parse(doc.DocType.DocTypeId),
                doc.InternalDescription,
                doc.PublicDescription,
                doc.DocStatus,
                doc.StatusDescription,
                doc.HideFromPMLUsers,
                annotations.ToList(),
                pages.ToList(),
                out updated,
                temp,
                null,
                eSignTags.ToList());

            temp.Save();
        }

        private static void AppendToDoc(Guid DocId, List<PdfPageItem> pages, IEnumerable<EDocumentAnnotationItem> annotations)
        {
            var repo = EDocumentRepository.GetUserRepository();
            var doc = repo.GetDocumentById(DocId);

            var currentPages = doc.GetPdfPageInfoList();
            var currentAnnotations = doc.InternalAnnotations.AnnotationList.ToList();

            Dictionary<int, int> pageRenumberMap = new Dictionary<int, int>();
            var offset = currentPages.Count;
            //var i = 0;

            //We don't necessarily know that the annotations will be in any sort of order,
            //so we need to remember how we mapped page numbers and do that for the annotations.
            /*foreach (var p in pages)
            {
                var newPageNumber = offset + i;
                pageRenumberMap[p.Page] = newPageNumber;
                p.Page = newPageNumber;
            }*/
            
            var appendedPages = currentPages.Concat(pages);
            var appendedAnnotations = currentAnnotations.Concat(annotations);
            /*
             * .Select(a =>
            {
                int newPage;
                if (pageRenumberMap.TryGetValue(a.PageNumber, out newPage))
                {
                    a.PageNumber = newPage;
                    return a;
                }

                return null;
            })).Where(a => a != null);
             */
            //SaveSimple(DocId, appendedPages, doc.Version, appendedAnnotations);

            SaveSimple(DocId, appendedPages, doc.Version, appendedAnnotations, doc.ESignTags.AnnotationList.ToList());
        }

        private void SaveData()
        {

            PerformanceStopwatch.ResetAll();
            AbstractUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            using (PerformanceStopwatch.Start("EditDocService.SaveData"))
            {
                Stopwatch sw = Stopwatch.StartNew();
                Guid docId = GetGuid("DocId");
                int version = GetInt("Version");
                string json = GetString("EDocPages");
                string publicDescription = GetString("PublicDescription");
                string description = GetString("Description");
                E_EDocStatus status = (E_EDocStatus)GetInt("Status");
                string statusDescription = GetString("StatusDescription");
                int docTypeId = GetInt("DocTypeId");
                string annotationsStr = GetString("Annotations", "[]");
                string eSignTagsStr = GetString("ESignTags", "[]");
                string signatures = GetString("Signatures", "[]");
                List<PdfField> signatureFields = ObsoleteSerializationHelper.JsonDeserialize<List<PdfField>>(signatures);
                List<EDocumentAnnotationItem> annotations = null;
                
                bool HideFromPML = GetBool("HideFromPML");

                if (GetBool("CanEditAnnotations"))
                {
                    annotations = ObsoleteSerializationHelper.JsonDeserialize<List<EDocumentAnnotationItem>>(annotationsStr);
                }

                List<EDocumentAnnotationItem> eSignTags = null;
                if (GetBool("CanEditESignTags"))
                {
                    eSignTags = ObsoleteSerializationHelper.JsonDeserialize<List<EDocumentAnnotationItem>>(eSignTagsStr);
                }

                List<PdfPageItem> list = null;
                EDocumentRepository repo = null;
                using (PerformanceStopwatch.Start("EditDocService.SaveData - Initial Load Edocs"))
                {
                    list = ObsoleteSerializationHelper.JsonDeserialize<List<PdfPageItem>>(json);

                    repo = EDocumentRepository.GetUserRepository();

                    if (null == list || list.Count == 0)
                    {
                        repo.DeleteDocument(docId, "User removed all pages.");
                        return;
                    }
                }
                string storageKey = GetString("StorageKey", "");
                EDocumentIntermediateAuditTracker tracker = EDocumentIntermediateAuditTracker.Retrieve(storageKey, principal.UserId);

                bool wasPdfUpdated;
                EDocumentViewer.SavePages(principal, docId, version, docTypeId, description, publicDescription, status, statusDescription, HideFromPML, annotations, list, out wasPdfUpdated, tracker, signatureFields, eSignTags);
                SaveConditionAssociations(docId);
                
                using (PerformanceStopwatch.Start("EditDocService.SaveData - Reload Edocs"))
                {
                    // Reload eDoc.
                    EDocument eDoc = repo.GetDocumentById(docId);

                    SetResult("Version", eDoc.Version);
                    SetResult("WasPdfUpdated", wasPdfUpdated);

                    // OPM 237259, 3/1/2016, ML
                    // The PDF needs to regenerate after a signature has been applied
                    // to prevent creating a race condition where a user applies a signature,
                    // saves, rotates the document, and then saves again: the rotation may
                    // overwrite the signature application, causing the signature to not be 
                    // correctly saved.
                    var cannotEditApprovedEdocs = eDoc.DocStatus == E_EDocStatus.Approved && 
                        !BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowEditingApprovedEDocs);

                    SetResult("CloseWindow", signatureFields.Count > 0 || cannotEditApprovedEdocs);

                    int pageCount = eDoc.PageCount;
                    List<PdfPageItem> pages = eDoc.GetPdfPageInfoList();

                    json = ObsoleteSerializationHelper.JsonSerialize(pages);

                    SetResult("EDocPages", json);
                    SetResult("Annotations", ObsoleteSerializationHelper.JsonSerialize(eDoc.InternalAnnotations.GenerateEditorList(principal)));
                    SetResult("ESignTags", ObsoleteSerializationHelper.JsonSerialize(eDoc.ESignTags.GenerateEditorList(principal)));
                    SetResult("PagesWithAnnotations", ObsoleteSerializationHelper.JsonSerialize(eDoc.GetPagesWithAnnotationsAndTags()));
                    sw.Stop();
                    Tools.LogInfo("[SaveData] Took (ms) " + sw.ElapsedMilliseconds + " DocId: " + docId + " PageCount: " + pageCount);
                }
            }
            Tools.LogInfo(PerformanceStopwatch.ReportOutput);
        }

        private void CreateEditableCopy()
        {
            var repository = EDocumentRepository.GetUserRepository();
            var documentId = this.GetGuid("DocId");
            var existingDoc = repository.GetDocumentById(documentId);

            if (!ConstStage.AllowESignedDocsInBatchEditor || !existingDoc.IsESigned)
            {
                throw new CBaseException(ErrorMessages.Generic, "Attempting to create editable copy of non-signed document: " + documentId);
            }

            var editableDocId = EDocument.CreateEditableCopyOfSignedDoc(repository, existingDoc);
            var docInfo = new
            {
                DocId = editableDocId,
                FolderName = existingDoc.Folder.FolderNm,
                DocTypeName = existingDoc.DocTypeName,
                Description = existingDoc.PublicDescription,
                Status = existingDoc.DocStatus.ToString("D")
            };

            this.SetResult("DocInfo", SerializationHelper.JsonNetAnonymousSerialize(docInfo));
        }
    }
}