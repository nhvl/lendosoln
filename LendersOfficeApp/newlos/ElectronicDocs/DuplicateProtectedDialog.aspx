﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DuplicateProtectedDialog.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.DuplicateProtectedDialog" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body style="font-size:14px; font-family:Arial; background:gainsboro">
    <h4 class="page-header">Duplicate Protected</h4>
    <form id="form1" runat="server">
    
    <%--Hidden fields for JSON Data--%>
    <input id="detailList" runat="server" type="hidden"/>
    <input id="docTypeId" runat="server" type="hidden"/>
    <input id="completeEditList" runat="server" type="hidden" />
    <input id="currentDocId" runat="server" type="hidden" />
    <div>
        <label style="display:block;padding:10px;">Duplicate protected documents were found for <ml:EncodedLabel ID="DocType" runat="server"/> on <ml:EncodedLabel ID="Borrower" runat="server"/>'s application.
        By default, these duplicates have been unprotected.
        </label>
        
        <%--Data table--%>
        <label style="font-weight:bold;padding-left:10px;">Duplicate documents:</label>
        <div style="margin-top:5px; padding:0px 10px;OVERFLOW-Y: scroll;height:150px;">
            <ml:CommonDataGrid ID="DocumentList" runat="server" CellPadding="2" AutoGenerateColumns="False" Width="100%" Height="50px" >
                <alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
                <itemstyle cssclass="GridItem"></itemstyle>
                <headerstyle cssclass="GridHeader"></headerstyle>
                <Columns>
                    <ASP:TemplateColumn ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader" HeaderText="<input type=checkbox runat=server onclick=f_selectAll(this);>" ItemStyle-Width="15px">
                        <ItemTemplate>
                            <asp:CheckBox ID="checked" runat="server" />
                            <input runat="server" ID="docid" value='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DocumentId").ToString()) %>' type="hidden" />
                        </ItemTemplate>
                    </ASP:TemplateColumn> 
                    <ASP:BoundColumn DataField="CreatedDate" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader" HeaderText="Created Date">
                    </ASP:BoundColumn> 
                    <ASP:BoundColumn DataField="PublicDescription" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader" HeaderText="Description">
                    </ASP:BoundColumn> 
                    <ASP:BoundColumn DataField="InternalDescription" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader" HeaderText="Comments">
                    </ASP:BoundColumn> 
                    <ASP:TemplateColumn HeaderText="" ItemStyle-CssClass="LookupRowLink" HeaderStyle-CssClass="LookupRowHeader">
                        <ItemTemplate>
                            <a href="#" onclick="view('<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DocumentId").ToString()) %>');">view pdf</a>
                        </ItemTemplate>
                    </ASP:TemplateColumn> 
                </Columns>
		    </ml:CommonDataGrid>
        </div>
        
        <%--Button panel--%>
        <div style="padding:10px;">
            <asp:Button runat="server" id="ProtectBtn" Text="Protect Selected Docs and Continue" OnClick="OnClick_Protect"/>
            <asp:Button runat="server" id="ContinueBtn" Text="Continue Without Protecting Duplicates" OnClick="OnClick_Continue" />
            <asp:Button runat="server" id="CancelBtn" Text="Cancel" OnClick="OnClick_Cancel"/>
        </div>
    </div>
    </form>
</body>
<script type="text/javascript">
    function _init() {
        //Data passed by javascript dialogArguments;
        //Copy to hidden fields and submit form to load server-side
        if(<%= AspxTools.JsBool(!DataLoaded) %>)
        {
            var args = getModalArgs();
            document.getElementById("detailList").value = JSON.stringify(args.List);
            document.getElementById("docTypeId").value = args.DocTypeId;
            document.getElementById("completeEditList").value = args.CompleteEditList;
            document.getElementById("currentDocId").value = args.CurrentUIDocumentId;
            document.getElementById("form1").submit();
        }
        addIFrame();
    }
    
    function addIFrame() {  // adding it to the source directly triggers an error.
        var i = document.createElement('iframe');
        i.id = 'downloadFileIframe';
        i.src = void (0);
        i.style.display = 'none';
        document.body.appendChild(i);
    }
    function view(docid) {
        var iframe = document.getElementById('downloadFileIframe');
        iframe.src = <%= AspxTools.SafeUrl(DataAccess.Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + docid;
    }
    
    function f_selectAll(cb)
	{
		var box = (cb.type=="checkbox")?cb:cb.children.item[0];
		var xState=box.checked;
		
		var elm = box.form.elements;
		for(i=0; i<elm.length; ++i)
		{
			if(elm[i].type == "checkbox" && elm[i] != box.id)
			{
				if(elm[i].checked != xState)
					elm[i].click();
			}
		}
	}
</script>

</html>
