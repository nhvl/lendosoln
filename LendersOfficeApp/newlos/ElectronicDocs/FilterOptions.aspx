﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FilterOptions.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.FilterOptions" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="EDocs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Filter</title>
    
    <script>
        function closeWindow() {
            var hasOne = false;
            var value = [];
            var checkboxes = document.getElementsByTagName("input");

            for (var i = 0; i < checkboxes.length; i++) {
                var val = checkboxes[i].getAttribute("data-docStatusVal");
                if (val != null && checkboxes[i].checked) {
                    value.push(parseInt(val));
                    hasOne = true;
                }
            }
            var result = { value: value };
            onClosePopup(result);
        }

        function init() {
            var args = getModalArgs();
            if (args !== null && args !== undefined) {
                var types = args.filterTypes;

                for (var i = 0; i < types.length; i++) {
                    var type = types[i];
                    var checkbox = document.getElementById("type" + type);
                    checkbox.checked = true;
                    
                }
            }
        }
        
    </script>
    
    <style type="text/css">
.MainRightHeader
{
    padding-left: 5px;
    font-weight: bold;
    font-size: 11px;
    padding-bottom: 5px;
    color: white;
    padding-top: 5px;
    font-family: Arial, Helvetica, sans-serif;
    background-color: maroon;
}
    </style>
    
</head>
<body style="background-color:gainsboro; font-size: 11pz; font-family: Arial, Helvetica, Sans-Serif;" onload="init();">
    <h4 class="page-header">Document Status</h4>
    <form id="form1" runat="server">
    <div style="margin-top:10px; margin-left: 5px;">
    <div><input type='checkbox' data-docStatusVal='<%=AspxTools.HtmlString((int)E_EDocStatus.Blank) %>' id='type<%=AspxTools.HtmlString((int)E_EDocStatus.Blank) %>'/>&lt;blank&gt;</div>
    <div><input type='checkbox' data-docStatusVal='<%=AspxTools.HtmlString((int)E_EDocStatus.Screened) %>' id='type<%=AspxTools.HtmlString((int)E_EDocStatus.Screened) %>'/>Screened</div>
    <div><input type='checkbox' data-docStatusVal='<%=AspxTools.HtmlString((int)E_EDocStatus.Approved) %>' id='type<%=AspxTools.HtmlString((int)E_EDocStatus.Approved) %>'/>Protected</div>
    <div><input type='checkbox' data-docStatusVal='<%=AspxTools.HtmlString((int)E_EDocStatus.Rejected) %>' id='type<%=AspxTools.HtmlString((int)E_EDocStatus.Rejected) %>'/>Rejected</div>
    <div><input type='checkbox' data-docStatusVal='<%=AspxTools.HtmlString((int)E_EDocStatus.Obsolete) %>' id='type<%=AspxTools.HtmlString((int)E_EDocStatus.Obsolete) %>'/>Obsolete</div>
     </div>
        <div style="text-align:center;">
            <input type="button" onclick="closeWindow()" value="Apply" />
        </div>

    </form>
</body>
</html>
