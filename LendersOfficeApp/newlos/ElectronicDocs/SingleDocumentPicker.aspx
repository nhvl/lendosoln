﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SingleDocumentPicker.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.SingleDocumentPicker" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="EDocs" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>EDocs Picker</title>
    <style type="text/css">
        .MainRightHeader {
            margin-bottom: 3px;
        }
        div.scrollable {
            height: 400px;
            overflow-y: scroll;
            width:100%;
        }

        div.buttonPanel  {
            text-align: center;
            margin-top: 10px;
        }

        th {
            color: Blue;
            text-decoration: underline;
            cursor: pointer;
        }

        th.none {
            text-decoration: none;
            cursor: auto;
        }
    </style>
</head>
<body class="EditBackground">
<script type="text/javascript">
var g_rows, timeoutID;
var searchData = [], returnObject = {};

function _init() {
    var resize = isLqbPopup(window) ? parent.LQBPopup.Resize : resizeForIE6And7;
    resize(600, 500);

    $('#DocTable').tablesorter({ headers: { 0: { sorter: false }, 6: { sorter: false}} })
        .on("sortEnd", function(){
                var alt = false;
                $('#DocTable tbody tr').each(function(i,o){
                    if ( alt ) { $(o).removeClass('GridItem').addClass('GridAlternatingItem');}
                    else { $(o).removeClass('GridAlternatingItem').addClass('GridItem'); }
                    alt = !alt;
                });
            });
    g_rows = $('#DocTable tbody tr');
    var isAlt = false;
    g_rows.each(function (i, o) {
        var _obj = $(o);
        var docData = JSON.parse(_obj.find('.docData').eq(0).val());
        _obj.data(docData);
        searchData.push(docData.DocTypeName.toLowerCase() + '~' + docData.Description.toLowerCase() + '~' + docData.Comments.toLowerCase());
        _obj.addClass(isAlt ? 'GridAlternatingItem' : 'GridItem');
        isAlt = !isAlt;
    });
    g_rows.find('a.view').click(function (e) { // Long term this will be .on('click', 'a.view', handler), but we don't have jQuery 1.7 here
        e.preventDefault();
        viewEdoc($(e.target).closest('tr').data('DocumentId'));
    });
    g_rows.find('a.select').click(function (e) {
        e.preventDefault();
        selectEdoc($(e.target).closest('tr').data());
    });
    $('#btnCancel').click(_close);
    $('#Query').keyup (function(){
        if (timeoutID) {
            clearTimeout(timeoutID);
        }

        timeoutID = setTimeout(searchTable, 500);
    });

      $('#DocTypeFilter').change(filterStatus);
}
function selectEdoc(documentObject) {
    returnObject.Document = documentObject;
    returnObject.OK = true;
    exit(returnObject);
}
function _close() {
    returnObject.OK = false;
    exit(returnObject);
}
function exit(returnValue) {
    if (isLqbPopup(window)) {
        parent.LQBPopup.Return(returnValue);
    }

    $.extend(window.dialogArguments, returnValue);
    onClosePopup();
}

function viewEdoc(docid) {
window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + docid , '_self');
}

function searchTable() {
    var value = $('#Query').val(), i = 0, rowCount = searchData.length, row;
    if (value.length == 0) {
        g_rows.show();
        return;
    }

    value = value.toLowerCase();

    for (i; i < rowCount; i++) {
        row = searchData[i];
        if (row.indexOf(value) == -1) {
            g_rows.eq(i).hide();
        }
        else {
            g_rows.eq(i).show();
        }
    }
}

function filterStatus() {
    var value = $('#DocTypeFilter').val(), i = 0, rowCount = searchData.length, rowType;
    if (value == 'All') {
        g_rows.show();
        return;
    }
    for (i; i < rowCount; i++) {
        rowType = g_rows.eq(i).children('td').eq(1).text().replace(/^\s+|\s+$/g, '');
        if (rowType.toLowerCase() == value.toLowerCase()) {
            g_rows.eq(i).show();
        }
        else {
            g_rows.eq(i).hide();
        }
    }
}
</script>
    <h4 class="page-header">Select document</h4>
    <form id="form1" runat="server">
    <div>
        <label>Search <input type="text" name="Query" id="Query" /> </label>
        <label>Folder <asp:DropDownList runat="server" ID="DocTypeFilter" /></label>
        <div class="scrollable"><div style="overflow-x:hidden">
        <asp:Repeater runat="server" ID="m_repeater">
            <HeaderTemplate>
                <table id="DocTable" width="100%">
                    <thead>
                        <tr class="GridHeader">
                            <th class="none">&nbsp;</th>
                            <th>Folder</th>
                            <th>DocType</th>
                            <th>Description</th>
                            <th>Comments</th>
                            <th>Last Modified</th>
                            <th class="none">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
            <tr>
                <td>
                    <%# AspxTools.HtmlControl(this.SelectAnchor((EDocument)Container.DataItem)) %>
                    <input type="hidden" class="docData" value=<%# AspxTools.HtmlAttribute(GetDocumentJson((EDocument)Container.DataItem)) %> />
                </td>
                <td>
                    <%# AspxTools.HtmlString( ((EDocument)Container.DataItem).Folder.FolderNm ) %>
                </td>
                <td>
                    <%# AspxTools.HtmlString( ((EDocument)Container.DataItem).DocTypeName ) %>
                </td>
                <td>
                    <%# AspxTools.HtmlString( ((EDocument)Container.DataItem).PublicDescription ) %>
                </td>
                <td>
                    <%# AspxTools.HtmlString( ((EDocument)Container.DataItem).InternalDescription ) %>
                </td>
                <td>
                    <%# AspxTools.HtmlString( ((EDocument)Container.DataItem).LastModifiedDate.ToString("M/d/yy h:mm tt")) %>
                </td>
                <td>
                    <a class="view">view</a>
                </td>
            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
 </div></div>
        <div class="buttonPanel">
            <input type="button" value="Cancel" id="btnCancel" />
        </div>
    </div>
    </form>
</body>
</html>
