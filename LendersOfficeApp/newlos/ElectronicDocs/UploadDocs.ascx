﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploadDocs.ascx.cs"
    Inherits="LendersOfficeApp.newlos.ElectronicDocs.UploadDocs" EnableViewState="True" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Constants" %>

<div id="DragDropZoneParent" class="upload-docs-container" ng-controller="dragDropAdditionalInfoController">
    <div>
        <% if (this.SupportedUploadCount > 1) { %>
        <span class="upload-count-label">You may upload up to <%=AspxTools.HtmlString(this.SupportedUploadCount.ToString()) %> documents at a time with a total upload size of 100MB.</span>
        <% } %>

        <div id="DragDropZoneContainer" class="InsetBorder full-width">
            <div id="DragDropZone" class="drag-drop-container" runat="server"></div>
        </div>

        <div drag-drop-additional-info-dir></div>

        <br />

        <div id="ErrorDiv"></div>
        <div id="UploadCountDiv"></div>

        <input id="UploadButton" type="button" value="Upload" onclick="uploadFiles();" />
    </div>
</div>

<script type="text/javascript">   
    var dropzoneId = '<%=AspxTools.ClientId(this.DragDropZone)%>';

    function _init() {
        $('#ErrorDiv, #UploadCountDiv').hide();
        setUploadButtonDisabled(true);

        var dragDropSettings = {
            hasAdditionalInfoParam: true,
            checkForUnprocessedUploadsBeforeUnload: true,
            isPmlParam: false,
            blockUpload: !ML.CanUploadEdocs,
            blockUploadMsg: ML.UploadEdocsFailureMessage,
            showBarcodesParam: ML.SupportsBarcodeScan,
            onDocTypeSelectCallback: onDocTypeSelect,
            onChangeCallback: function () {
                window.setTimeout(function () {
                    if (typeof onDragDropUiChanged === 'function') {
                        onDragDropUiChanged()
                    }
                }, 0);
            },
            onProcessAllFilesCallback: function () {
                window.setTimeout(function () {
                    TextareaUtilities.LengthCheckInit();

                    if (ML.RestrictToUcd) {
                        setDefaultDragDropDocType(dropzoneId, ML.UcdDefaultDocTypeId, ML.UcdDefaultDocTypeName);
                    }
                }, 0);
            }
        };

        if (typeof Apps !== 'undefined') {
             // Variable added on page load
            dragDropSettings.appNamesParam = Apps;
        }

        registerDragDropUpload(document.getElementById(dropzoneId), dragDropSettings);
    }

    function updateDirtyBit_callback() {
        clearDirty();
    }
</script>

