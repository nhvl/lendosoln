﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using EDocs;
using LendersOffice.Common;
using LendersOffice.AntiXss;
using LendersOfficeApp.los.admin;
using LendersOffice.ObjLib.DocumentConditionAssociation;
using LendersOffice.Admin.DocuSign;

namespace LendersOfficeApp.newlos.ElectronicDocs
{

    public class EDocBatchEditDetails
    {
        public Guid DocumentId { get; set; }
        public int DocTypeId { get; set; }
        public string DocTypeDescription { get; set; }
        public string Description { get; set; }
        public string InternalDescription { get; set; }
        public E_EDocStatus DocumentStatus { get; set; }
        public string DocumentStatusDescription { get; set; }
        public int Version { get; set; }
        public int PageCount { get; set; }
        public List<EDocumentAnnotationItem> NewAnnotations { get; set; }
        public List<EDocumentAnnotationItem> InitialAnnotations { get; set; }
        public string DocumentStatusReasonDescription { get; set; }
        public string Folder { get; set; }
        public string DocType { get; set; }
        public bool CanEditSource { get; set; }
        public E_DocumentConditionAssociationStatus DocAssocStatus { get; set; }
        public List<int> PagesWithAnnotations { get; set; }
        public List<EDocumentAnnotationItem> InitialESignTags { get; set; }
        public List<EDocumentAnnotationItem> NewESignTags { get; set; }
        public bool HideFromPmlUsers { get; set; }
        public bool HasPmlRoles { get; set; }
        public E_ImageStatus ImageStatus { get; set; }

        public EDocBatchEditDetails(EDocument doc,  int startPageIndex)
        {                                                                              
            DocumentId = doc.DocumentId;
            DocTypeId = doc.DocumentTypeId;
            DocTypeDescription = doc.FolderAndDocTypeName;
            DocumentStatus = doc.DocStatus;
            Description = doc.PublicDescription;
            InternalDescription = doc.InternalDescription;
            DocumentStatusDescription = EDocument.StatusText(DocumentStatus);
            InitialAnnotations = new List<EDocumentAnnotationItem>();
            InitialESignTags = new List<EDocumentAnnotationItem>();
            DocumentStatusReasonDescription = doc.StatusDescription;
            Folder = doc.Folder.FolderNm;
            DocType = doc.DocType.DocTypeName;
            HideFromPmlUsers = doc.HideFromPMLUsers;
            ImageStatus = doc.ImageStatus;

            PagesWithAnnotations = doc.GetPagesWithAnnotationsAndTags();

            foreach (var item in doc.InternalAnnotations.GenerateEditorList(PrincipalFactory.CurrentPrincipal))
            {
                item.PageNumber += startPageIndex;
                InitialAnnotations.Add(item);
            }

            foreach (var item in doc.ESignTags.GenerateEditorList(PrincipalFactory.CurrentPrincipal))
            {
                item.PageNumber += startPageIndex;
                InitialESignTags.Add(item);
            }

            PageCount = doc.PageCount;
            Version = doc.Version;
            DocAssocStatus = E_DocumentConditionAssociationStatus.Undefined;

            HasPmlRoles = false;
            foreach (EDocRoles edr in doc.Folder.AssociatedRoleIds)
            {
                if ((edr.RoleId == Role.Get(E_RoleT.Administrator).Id && edr.IsPmlUser)
                    || (edr.RoleId == Role.Get(E_RoleT.LoanOfficer).Id && edr.IsPmlUser))
                {
                    HasPmlRoles = true;
                    break;
                }
            }
        }

        public EDocBatchEditDetails()
        { }
    }


    public partial class EditEDocBatch : BaseLoanPage
    {
        private Guid? m_appId = Guid.Empty;


        public override bool IsReadOnly
        {
            get { return false; }
        }
        protected bool EnableDuplicateEDocProtectionChecking
        {
            get
            {
                return Broker.EnableDuplicateEDocProtectionChecking;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.BrokerUser.HasPermission(Permission.CanViewEDocs))
            {
                HandleException(new AccessDenied());
                return;
            }


            #region header
            CPageData loanData = CPageData.CreateUsingSmartDependency(LoanID, typeof(EditEDocBatch));
            loanData.InitLoad();
            CAppData app = loanData.GetAppData(0);
            this.Header.Title = string.Format("{0} - {1} - EDoc Editor", app.aBLastFirstNm, loanData.sLNm);
            #endregion

          

            #region setup status box 



            m_docStatus.Items.Add(new ListItem(EDocument.StatusText(E_EDocStatus.Blank), E_EDocStatus.Blank.ToString("d")));

            if (BrokerUser.HasPermission(Permission.AllowScreeningEDocs))
            {
                m_docStatus.Items.Add(new ListItem(EDocument.StatusText(E_EDocStatus.Obsolete), E_EDocStatus.Obsolete.ToString("d")));
            }

            if (BrokerUser.HasPermission(Permission.AllowApprovingRejectingEDocs))
            {
                m_docStatus.Items.Add(new ListItem(EDocument.StatusText(E_EDocStatus.Approved), E_EDocStatus.Approved.ToString("d")));
                m_docStatus.Items.Add(new ListItem(EDocument.StatusText(E_EDocStatus.Rejected), E_EDocStatus.Rejected.ToString("d")));
            }
            if (BrokerUser.HasPermission(Permission.AllowScreeningEDocs))
            {
                m_docStatus.Items.Add(new ListItem(EDocument.StatusText(E_EDocStatus.Screened), E_EDocStatus.Screened.ToString("d")));
            }

            m_docStatus.Enabled =  (BrokerUser.HasPermission(Permission.AllowScreeningEDocs) || BrokerUser.HasPermission(Permission.AllowApprovingRejectingEDocs));
            m_docStatusDescription.Enabled = (BrokerUser.HasPermission(Permission.AllowScreeningEDocs) || BrokerUser.HasPermission(Permission.AllowApprovingRejectingEDocs));
            if (m_docStatusDescription.Enabled == false)
            {
                m_docStatusDescription.BackColor = System.Drawing.Color.Gainsboro;
            }
        

            #endregion

            List<Guid> doocumentIds = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<Guid>>(AutoExpiredTextCache.GetFromCache(RequestHelper.GetSafeQueryString("key")));
            this.RegisterJsObject("DocIds", doocumentIds);
            m_docTypeList.OnChange = "UpdateDocType()";
        }

        private void HandleException(CBaseException exc)
        {
            //Register some empty pages and annotations so things don't chuck exceptions
            RegisterJsObject("FirstEDocAnnotations", new List<EDocumentAnnotationItem>());
            RegisterJsObject("FirstEDocPages", new List<PdfPageItem>());
            AddInitScriptFunction("displayError", "function displayError() { ForceQuit(" + AspxTools.JsString(exc.UserMessage) + ");}");

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            HtmlMeta meta = new HtmlMeta();
            meta.Attributes.Add("http-equiv", "X-UA-Compatible");
            meta.Attributes.Add("content", "IE=8");
            Header.Controls.AddAt(0, meta);
        }

        protected void Page_Init(object sender, System.EventArgs e)
        {
            this.RegisterService("loanedit", "/newlos/ElectronicDocs/EditEDocService.aspx");            	

            if (this.Header != null)
            {
                RegisterCSS("jquery-ui-1.11.custom.css");
                IncludeStyleSheet("~/css/PdfEditor.css");
                // 12/18/2009 dd - Only execute this block if <head runat="server"> is set in aspx.
            }
            this.RegisterJsScript("json.js");
            this.EnableJqueryMigrate = false;
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("PdfEditor.js");
            this.RegisterJsGlobalVariables("CanEditProtectedDocs", BrokerUser.HasPermission(Permission.AllowEditingApprovedEDocs));

            // Init your user controls here, i.e: bind drop down list.

            //this.PageTitle = "PAGE TITLE";
            //this.PageID = "PageId"; // Must match with PageId in LoanNavigation.xml.config
            //this.PDFPrintClass = typeof(PDFPrintClass); // Only if this page has a pdf printing class.
            UniqueKey.Value = Guid.NewGuid().ToString();
            CanEditAnnotations.Value = BrokerUser.HasPermission(Permission.CanEditEDocsInternalNotes).ToString();
            DragAndDropTable.Visible = BrokerUser.HasPermission(Permission.CanEditEDocsInternalNotes);
            InternalNoteTab.Visible = BrokerUser.HasPermission(Permission.CanViewEDocsInternalNotes);
            ViewInternalNotes.Visible = InternalNoteTab.Visible;
            CanEditEdocs.Value = BrokerUser.HasPermission(Permission.CanEditEDocs).ToString();
            LendersOffice.Rolodex.RolodexDB.PopulateAgentTypeDropDownList(AgentRole, useShortened: true);

            var docuSignSettings = LenderDocuSignSettings.Retrieve(BrokerUser.BrokerId);
            // This check is sufficient. This version of the batch editor, when it retrieves the doc list, will not open up if any selected edocs are protected and the user does not have permission to edit protected docs.
            // See EditEdocService.GetBatchPageInfo for the code that retrieves the batch list.
            var canEditTags = BrokerUser.HasPermission(Permission.AllowCreatingDocumentSigningEnvelopes) && docuSignSettings != null && docuSignSettings.IsSetupComplete() && docuSignSettings.DocuSignEnabled;
            ESignTagsTab.Visible = canEditTags;
            ViewESignTags.Visible = canEditTags;
        }
    }
}
