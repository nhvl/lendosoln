﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentCaptureUpload.ascx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.DocumentCaptureUpload" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<script type="text/javascript">
    $(document).ready(function () {
        validateWorkflowPermissions();
    });

    function validateWorkflowPermissions() {
        if (!ML.CanSubmitToKta) {
            $('#ClientSubmit').prop('disabled', true);
            $('.WarningIcon').show().attr('title', ML.KtaSubmissionDenialReason);
        }
    }

    function validate() {
        var errors = "";
        if (!ML.HasKtaServiceCredential) {
            if ($.trim($('#' + <%= AspxTools.JsString(KtaUsername.ClientID) %>).val()) === "") {
                errors += "Please enter a valid username.\n";
            }

            if ($.trim($('#' + <%= AspxTools.JsString(KtaPassword.ClientID) %>).val()) === "") {
                errors += "Please enter a valid password.\n";
            }
        }

        var failedValidation = false;
        var hasFileSelected = false;
        for (var i = 0; i < window.frames.length; ++i) {
            if (!window.frames[i].validate()) {
                failedValidation = true;
                break;
            }
            hasFileSelected = hasFileSelected || window.frames[i].hasFile();
        }

        if (failedValidation) {
            errors += 'Please correct the file errors before continuing.';
        }
        else if (!hasFileSelected) {
            errors += 'Please select at least one file to upload.';
        }

        if (errors !== "") {
            alert(errors);
            return false;
        }

        return true;
    }

    function clientSubmit() {
        if (!validate()) {
            return;
        }

        var args = {
            LoanId: ML.sLId,
            AppId: ML.aAppId
        };

        if (!ML.HasKtaServiceCredential) {
            args.Username = $('#' + <%= AspxTools.JsString(KtaUsername.ClientID) %>).val();
            args.Password = $('#' + <%= AspxTools.JsString(KtaPassword.ClientID) %>).val();
        }

        var frameDataSet = [];
        for (var i = 0; i < window.frames.length; ++i) {
            var frame = window.frames[i];
            if (typeof frame.hasFile === 'function' && frame.hasFile()) {
                var frameData = {};
                frameData.frame = frame;
                frameData.fileName = frame.getFileName();
                frameDataSet.push(frameData);
            }
        }

        args.RequestCount = frameDataSet.length;

        gService.edoc.callAsyncSimple('GetKtaPostbackData', args, function (result) {
            if (result.error) {
                alert('Unable to submit. Please try again.');
                return;
            }

            var cookies = JSON.parse(result.value.Cookies);
            var payloads = JSON.parse(result.value.KtaPayloads);
            var requestIds = JSON.parse(result.value.RequestIds);

            for (var i = 0; i < frameDataSet.length; i++) {
                var frameData = frameDataSet[i];
                frameData.cookie = cookies[i];
                frameData.payload = payloads[i];
                frameData.requestId = requestIds[i];
            }

            submitDocuments(frameDataSet);
        });
    }

    function submitDocuments(frameDataSet) {
        $('#FileUploadTable, #ClientSubmit').hide();
        $('#LoadingSection').show();
        highestParent.triggerOverlayImmediately();

        var uploadCounter = 0;
        var completedRequests = [];
        submitSingleDocument(uploadCounter, frameDataSet, completedRequests);
    }

    function submitSingleDocument(uploadCounter, frameDataSet, completedRequests) {
        if (uploadCounter >= frameDataSet.length) {
            summarizeUpload(completedRequests);
            return;
        }

        var frameData = frameDataSet[uploadCounter];
        $('#LoadingSectionDocumentName').text(frameData.fileName);
        frameData.frame.submitForm(frameData.cookie, frameData.payload, function (isSuccess) {
            if (isSuccess) {
                setupPolling(uploadCounter, frameDataSet, completedRequests);
            } else {
                // Nothing to poll for, queue up the next file
                recordCompletedRequest(completedRequests, frameData, /* success */ false);
                submitSingleDocument(++uploadCounter, frameDataSet, completedRequests);
            }
        });
    }

    function setupPolling(uploadCounter, frameDataSet, completedRequests) {
        var pollingId = window.setInterval(function () {
            pollForOcrStatus(uploadCounter, frameDataSet, completedRequests);
        }, ML.PollInterval);

        frameDataSet[uploadCounter].pollingId = pollingId;
    }

    function pollForOcrStatus(uploadCounter, frameDataSet, completedRequests) {
        var frameData = frameDataSet[uploadCounter];
        var args = { RequestId: frameData.requestId };
        gService.edoc.callAsyncSimple('PollForOcrStatus', args, function (result) {
            if (result.error) {
                window.clearTimeout(frameDataSet[uploadCounter].pollingId);
                recordCompletedRequest(completedRequests, frameData, /* success */ false);
                submitSingleDocument(++uploadCounter, frameDataSet, completedRequests);
                return;
            }

            if (result.value.Ready === "True") {
                window.clearTimeout(frameDataSet[uploadCounter].pollingId);
                recordCompletedRequest(completedRequests, frameData, /* success */ true);
                submitSingleDocument(++uploadCounter, frameDataSet, completedRequests);
            }
        });
    }

    function recordCompletedRequest(completedRequests, frameData, success) {
        for (var i = 0; i < completedRequests.length; i++) {
            // The async behavior can cause entries to be added more than once.
            // Verify that the requestId has not yet been added to the array.
            if (frameData.requestId === completedRequests[i].requestId) {
                return;
            }
        }

        completedRequests.push({
            fileName: frameData.fileName,
            requestId: frameData.requestId,
            success: success
        });
    }

    function summarizeUpload(completedRequests) {
        var successList = document.getElementById('successfulDocumentList');
        var failureList = document.getElementById('failedDocumentList');

        var hasSuccessfulDocuments = false;
        var hasFailedDocuments = false;

        for (var i = 0; i < completedRequests.length; i++) {
            var request = completedRequests[i];
            var entry = document.createElement('li');
            entry.innerText = request.fileName;

            if (request.success) {
                successList.appendChild(entry);
                hasSuccessfulDocuments = true;
            } else {
                failureList.appendChild(entry);
                hasFailedDocuments = true;
            }
        }

        if (!hasSuccessfulDocuments) {
            $('#SuccessfulDocumentSummary').hide();
        }

        if (!hasFailedDocuments) {
            $('#FailedDocumentSummary').hide();
        }

        $('#LoadingSection').hide();
        $('#SummarySection').show();
        highestParent.removeOverlay();
    }
</script>
<style type="text/css">
    .submit-frame {
        border: none;
        height: 35px;
    }

    #FileUploadTable {
        width: 30%;
    }

    #FileUploadTable td {
        padding: 5px;
    }

    #FileUploadTable input[type=file] {
        width: 100%;
    }

    #LoadingSection, #SummarySection {
        display: none;
    }

    #SuccessfulDocumentSummary {
        color: #000000;
    }

    #FailedDocumentSummary {
        color: #FF0000;
    }

    .CredentialTextbox {
        width: 125px;
    }

    .hidden {
        display: none;
    }
</style>

<table class="FormTable" id="CredentialTable" runat="server">
    <tr class="GridHeader">
        <th colspan="2">
            Capture Credentials
        </th>
    </tr>
    <tr>
        <td class="FieldLabel">
            Username
        </td>
        <td>
            <asp:TextBox ID="KtaUsername" CssClass="CredentialTextbox" runat="server" doesntdirty="true" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            Password
        </td>
        <td>
            <asp:TextBox ID="KtaPassword" CssClass="CredentialTextbox" runat="server" TextMode="Password" doesntdirty="true" />
        </td>
    </tr>
</table>

<table id="FileUploadTable" class="FormTable">
    <tr class="GridHeader">
        <th>File Upload</th>
    </tr>
    <tr class="GridAutoItem">
        <td>
            <iframe class="submit-frame" src="KtaSubmission.aspx"></iframe>
        </td>
    </tr>
    <tr class="GridAutoItem">
        <td>
            <iframe class="submit-frame" src="KtaSubmission.aspx"></iframe>
        </td>
    </tr>
    <tr class="GridAutoItem">
        <td>
            <iframe class="submit-frame" src="KtaSubmission.aspx"></iframe>
        </td>
    </tr>
    <tr class="GridAutoItem">
        <td>
            <iframe class="submit-frame" src="KtaSubmission.aspx"></iframe>
        </td>
    </tr>
    <tr class="GridAutoItem">
        <td>
            <iframe class="submit-frame" src="KtaSubmission.aspx"></iframe>
        </td>
    </tr>
    <tr class="GridAutoItem">
        <td>
            <iframe class="submit-frame" src="KtaSubmission.aspx"></iframe>
        </td>
    </tr>
</table>

<input type="button" id="ClientSubmit" onclick="clientSubmit();" value="Upload" />
<img src=<%= AspxTools.SafeUrl(this.Page.VirtualRoot + "/images/warning25x25.png") %> class="WarningIcon align-middle hidden" alt="Warning" />

<div id="LoadingSection">
    <label class="FieldLabel">Uploading <span id="LoadingSectionDocumentName" />, please wait.</label>
    <br />
    <img id="LoadingImage" src="../../images/status.gif" alt="Loading" />
</div>

<div id="SummarySection" class="FieldLabel">
    <div id="SuccessfulDocumentSummary">
        Successfully uploaded documents:
        <ul id="successfulDocumentList" />
    </div>
    <div id="FailedDocumentSummary" >
        Failed documents:
        <ul id="failedDocumentList" />
    </div>
</div>
