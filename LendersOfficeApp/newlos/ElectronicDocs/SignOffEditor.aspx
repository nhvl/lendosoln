﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignOffEditor.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.SignOffEditor" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<%@ Import Namespace="LendersOffice.ObjLib.DocumentConditionAssociation" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<%@ Register TagPrefix="uc" TagName="DocTypePicker" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx"%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Sign of Editor</title>

        <style type="text/css">
            html, body
            {
                height: 100%;
                overflow: hidden;
                background-color: #DCDCDC;
            }

            button
            {
                margin-left: 2px;
                margin-right: 2px;
            }
            button.op_button
            {
                height: 70px;
                width: 60px;
            }
            button.op_button_wide_2
            {
                width: 90px;
            }
            button.op_button_wide_3
            {
                width: 130px;
            }
            button.op_button_wide_4
            {
                width: 160px;
            }
            button.op_button_short
            {
                height: 20px;
            }
            button img.standard_img
            {
                width: 30px;
                height: 30px;
            }
            .paste-img
            {
                width: 20px;
            }
            .note-img
            {
                height: 58px;
                margin-bottom: -3px;
            }
            h1
            {
                margin: 0;
            }
            #AllTabFunctionality, #AllTabFunctionality2
            {
                float: right;
                display: block;
            }
            ul.fields
            {
                margin: 0;
                margin-top: .9em;
                padding: 0;
                list-style-type: none;
            }
            ul.fields li
            {
                clear: both;
                overflow: hidden;
                padding-bottom: 8px;
            }
            ul.fields li label
            {
                display: block;
                width: 120px;
                float: left;
            }
            ul.fields li input, ul.fields li select
            {
                display: block;
                float: left;
            }
            #tabs
            {
                min-width: 930px;
            }
            #AllTabSaveFunctionality
            {
                float: left;
                clear: none;
            }
            .tableCellHeight
            {
                height: 80px;
            }
            table td
            {
                font-weight: bold;
            }
            .Hidden
            {
                display: none;
            }
            .pdf-field-note-style
            {
                padding: 0;
                background-color: #FFFFA8;
                border: 1px solid black;
            }
            .pdf-field-note textarea
            {
                display: block;
                margin: 0 auto;
                clear: both;
                margin-left: 5px;
            }
            .pdf-field-note .minimize-note
            {
                float: right;
                width: 20px;
                height: 20px;
                margin-bottom: 2px;
                display: block;
                margin-right: 10px;
                margin-top: 3px;
                padding: 0px;
            }
            .pdf-field-note .maximize-item, .pdf-field-note .minimize-item
            {
                width: 100%;
                height: 100%;
            }
            .maximize-item span
            {
                display: block;
                float: left;
                margin-top: 2px;
                margin-left: 5px;
            }
            .pdf-field-note .maximize-note
            {
                margin: 5px;
                display: block;
                width: 25px;
                height: 25px;
                padding: 0px;
            }
            .maximize-note image, .minimize-note image
            {
                margin: 0 auto;
            }
            .highlight-field
            {
                background-color: Yellow;
                width: 120px;
                height: 20px;
                border: 1px solid black;
            }
            .pdf-field-thumbnail img
            {
                width: 100%;
                height: 100%;
            }
            #ViewInternalNotes, #ViewDocumentInfo, #Underwriting, #Signature, #AllTabSaveFunctionality
            {
                padding: 0 !important;

            }

            .button_container
            {
                height: 135px;
            }
            div.MaskedDiv
            {
                position: absolute;
                left: 0px;
                top: 0px;
                height: 100%;
                z-index: 900;
                width: 100%;
                filter: alpha(opacity=1);
                background-color: Red;
            }

            #editor {
                clear: none !important;
                float: left;
                overflow: hidden;
            }

            #ConditionSubject, #ConditionInternalNotes {
                width: 278px;
                height: 40px;
            }

        </style>
</head>
<body onunload="return page_unload()">
    <script type="text/javascript">
        function page_unload(){}
    </script>
    <h4 class="page-header">Condition Signoff Editor</h4>
    <form id="form1" runat="server">
    <asp:PlaceHolder runat="server" id="Viewer">
    <asp:HiddenField runat="server" ID="CanEditAnnotations" />
    <asp:HiddenField runat="server" ID="CanEditEdocs" />
    <!--[if IE 6]>
    <div style="background-color:yellow; color:     black; font-weight: bold; border: 1px solid black;padding:5px">
        We have detected you are currently using Internet Explorer 6.  For the best compatibility with LendingQB please upgrade to a newer version of Internet Explorer
    </div>
    <![endif]-->

    <div id="tabs">
        <ul>
            <li id="UnderwritingTab" class="tab">
                <a href="#Underwriting">Underwriting</a>
            </li>
            <li runat="server" id="InternalNoteTab" class="tab">
                <a href="#<%= AspxTools.ClientId(ViewInternalNotes) %>"> View Internal Notes</a>
            </li>
            <li id="ViewDocumentInfoTab" class="tab">
                <a href="#ViewDocumentInfo">View Document Info</a>
            </li>
            <li runat="server" id="SignatureTab" class="tab" Visible="false">
                <a href="#Signature">Sign Document</a>
            </li>
        </ul>
        <hr />
        <table id="AllTabSaveFunctionality" class="button_container" width="16.2%"   cellpadding="0" cellspacing="0" border="0"  >
            <tr>
                <td class="tableCellHeight" valign="top" >
                    <button class="op_button" id="btnSave" type="button" style="vertical-align: top" disabled="disabled">
                        <img id="Img1" src="~/images/edocs/save.png" class="standard_img" runat="server" alt="Save" />
                        <div>
                            Save
                        </div>
                    </button>
                </td>

            </tr>
            <tr>
                        <td  valign="bottom">
                    <button class="op_button_wide_3" runat="server" id="btnAdd" type="button">
                            Add Documents
                    </button>
                </td>
            </tr>
        </table>
        <div id="ViewInternalNotes" runat="server" class="ui-tabs-hide">
            <table class="button_container" style="float: left;" cellpadding="0" cellspacing="0" border="0">
                <tbody>
                    <tr>
                        <td class="tableCellHeight" valign="bottom" style="padding: 0; margin: 0">
                            <button id="btnMaximize" class="op_button op_button_wide_3 op_button_short" type="button">
                                <img id="imgMax" src="~/images/edocs/maximize.png" runat="server" />
                                Maximize all notes
                            </button>
                            <br />
                            <button id="btnMinimize" class="op_button op_button_wide_3 op_button_short"
                                type="button">
                                <img id="imgMin" src="~/images/edocs/minimize.png" runat="server" />
                                Minimize all notes
                            </button>
                        </td>
                        <td class="tableCellHeight" valign="bottom">
                            <button id="btnRemoveAnnotations" class="op_button op_button_wide_3 "
                                type="button" disabled="disabled">
                                <img id="Img4" src="~/images/edocs/remove_notation_disable.png" class="standard_img"
                                    runat="server" />
                                <div>
                                    Remove Notation(s)</div>
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="button_container" runat="server" id="DragAndDropTable" style="float: left;">
                <tr>
                       <td colspan="3" valign="bottom">
                            Drag and drop notations onto the document:
                        </td>
                </tr>
                                    <tr >

                        <td valign="bottom" align="center" >
                            <div class="pdf-field-template annotation-field pdf-field-highlight highlight-field" >
                            </div>
                        </td>
                        <td valign="bottom" align="center" >
                           <div class="pdf-field-template annotation-field pdf-field-note" >
                                <asp:Image ID="Image1" CssClass="note-img" runat="server" ImageUrl="~/images/edocs/note-field-drag.png" />
                            </div>
                        </td>
                        <td style="width:10px;">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            Highlight
                        </td>
                        <td align="center">
                            Note
                        </td>
                    </tr>
            </table>

        </div>
        <div id="Underwriting"  >
            <table  class="button_container" style="float: left;" cellpadding="0" cellspacing="0" border="0">
                <tbody>
                <tr>
                    <td>
                        Category
                     </td>

                     <td>
                        Condition Subject
                     </td>

                     <td>
                        Internal Notes
                     </td>
                </tr>
                <tr>
                    <td style="width: 250px">
                        <asp:DropDownList runat="server" id="Category" Width="200"></asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="ConditionSubject" Rows="10" Columns="10" TextMode="MultiLine" ></asp:TextBox>
                    </td>
                    <td>
                         <asp:TextBox runat="server" ID="ConditionInternalNotes" Rows="10" Columns="10" TextMode="MultiLine" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        Current document : <span id="CurrentDocumentName"></span>

                    </td>
                </tr>
                <tr>
                    <td colspan="3" valign="bottom">


                        <button  id="SatisfyBtn" class=" " >
                            <asp:Image runat="server" ID="Accept" ImageUrl="~/images/edocs/accept.png"  style="vertical-align:middle" />
                            Satisfies Condition
                        </button>
                        <button id="DoesNotSatisfyBtn" class="  "  >
                            <asp:Image runat="server" ID="Image2" ImageUrl="~/images/edocs/reject.png" style="vertical-align:middle"   />
                            Does not Satisfy Condition
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div id="ViewDocumentInfo" class="ui-tabs-hide">
            <table class="button_container" style="float: left;" cellpadding="0" cellspacing="0" border="0">
                <tbody>
                    <tr>
                        <td class="tableCellHeight">
                            <ul class="fields">
                                <li>
                                    <ml:EncodedLabel ID="Label1" AssociatedControlID="m_docTypeList" runat="server">Type:</ml:EncodedLabel>
                                    <uc:doctypepicker id="m_docTypeList" onchange="setDirtyBit();" runat="server"></uc:doctypepicker>
                                </li>
                                <li>
                                    <ml:EncodedLabel ID="Label2" AssociatedControlID="m_publicDescription" runat="server">Description:</ml:EncodedLabel>
                                    <asp:TextBox ID="m_publicDescription" runat="server" Width="200px" /></li>
                                <li>
                                    <ml:EncodedLabel ID="Label3" AssociatedControlID="m_docDescription" runat="server">Internal Comments:</ml:EncodedLabel>
                                    <asp:TextBox ID="m_docDescription" runat="server" Width="200px" /></li>
                                <li>
                                    <ml:EncodedLabel ID="Label4" AssociatedControlID="m_docStatus" runat="server">Status:</ml:EncodedLabel>
                                    <asp:DropDownList ID="m_docStatus" runat="server" Width="200px">
                                    </asp:DropDownList>
                                    <span id="pnlStatusDescription" style="text-align: left">
                                        <ml:EncodedLabel ID="Label5" AssociatedControlID="m_docStatusDescription" runat="server"
                                            Width="90px">&nbsp;&nbsp;&nbsp;&nbsp;Rejected for:</ml:EncodedLabel>
                                        <asp:TextBox ID="m_docStatusDescription" runat="server" Width="200px" />
                                    </span></li>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
       <div id="Signature" class="ui-tabs-hide" runat="server" Visible="false">
            <table class="button_container" style="float: left;" border="0">
                <tr>
                    <td colspan="2" >
                        Drag and drop signature and date into places on the document
                    </td>
                </tr>
                <tr>
                    <td  align="center">
                        <div class="pdf-field-template pdf-field-signature-img signature-field" id="sigDiv">
                        <asp:Image ID="SignatureImg" CssClass="pdf-field-content"  runat="server"   />
                        </div>
                    </td>
                    <td  align="center">
                        <div class="pdf-field-template pdf-field-date signature-field">
                           <asp:Image ID="DateIMg" CssClass="pdf-field-content" runat="server" ImageUrl="~/ViewEDocPdf.aspx?cmd=date" Width="100%" Height="100%" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="RejectedReasonDialog" title="Rejection Reason" style="display: none">
        <p>
            Why are you rejecting this document?
            <input type="text" id="rejectionReeasonDialog" style="width: 200px" />
        </p>
    </div>
    <div id="ClearConditionDialog" title="Condition Status" style="display:none">
        <p>
            All associated documents have been marked as satisfied for this condition. Should the condition be cleared?
        </p>
    </div>

    <div id="editor" style="z-index: 0; position: relative">
    </div>
    <uc1:cmodaldlg id="Cmodaldlg1" runat="server" />
    <script type="text/x-jquery-tmpl" id="noteTemplate">
        <div class="minimize-item">
            <button class="maximize-note">
                <img  src="../../images/edocs/maximize.png" alt="maximize" />
            </button>
        </div>
        <div class="maximize-item" >
            <textarea  class="pdfnotes"  rows="1" cols="1"  >${Text}</textarea>
            <span>Last updated: </span><span class="note_last_modified">${LastModifiedOn}</span>
            <button class="minimize-note" >
                <img src="../../images/edocs/minimize.png" alt="minimize" />
            </button>
        </div>
    </script>
    <div id="divStatusPanel" class="StatusPanel" style="display: none">
        <div>
            Loading...
        </div>
    </div>
    <script type="text/x-jquery-tmpl" id="noteThumbTemplate">
        <div>
            <img src="${src}" alt="" />
        </div>
    </script>
    <script type="text/javascript">
        var _dataKey = 'pdfFieldData';
        $(function(){
            $('form').submit(function(){
                return false;
            });
            resizeForIE6And7(1000,800);
            var $tabs = $('#tabs'), _pdfEditor = null, $thumb = null, currentDocDesc = $('#CurrentDocumentName'),
                $currentFolder = null, isDirty =  false,
                _canEditAnnotations = $('#<%= AspxTools.ClientId(CanEditAnnotations) %>').val() == 'True',
                _noteTemplate = $( "#noteTemplate" ).template(),
                _noteThumbTemplate = $( "#noteThumbTemplate" ).template(),
                $docStatus = $('#<%= AspxTools.ClientId(m_docStatus) %>'),
                __isDoneLoading = false,
                _clearConditionOnSave = false,
                _canClose = $('#CanCloseCondition').val() == 'True',
                _canEditApprovedDocs = $('#AllowEditingApprovedEDocs').val() == 'True';

            $('#divStatusPanel').dialog({
                title : 'Please wait..',
                modal: true,
                resizable: false,
                open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
                closeOnEscape : false,
                buttons: {}
            });

            var clearDialog = $('#ClearConditionDialog').dialog({
               modal : true,
               resizable: false,
               open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
               closeOnEscape : false,
               autoOpen : false,
               width: 400,
               buttons : {
                    'Keep Condition Open' : function() { _clearConditionOnSave = false; clearDialog.dialog( "close" );},
                    'Clear Condition' : function(){ _clearConditionOnSave = true; clearDialog.dialog("close"); setTimeout(_Save,0); }
               }
            });

             function displayStatusMessage(msg) {
                __isDoneLoading = false;


                $('#divStatusPanel div').text(msg);
                $('#divStatusPanel').dialog('open');
            }

            function hideStatusMessage() {
                __isDoneLoading = true;
                $('#divStatusPanel').dialog('close');
            }

            function AreConditionsCleared() {
                if (!_canClose) {
                    return false;
                }
                var i = 0, docs = BatchEDocSet, docCount = docs.length;
                var askClearCondition = true;
                for (i; i < docCount; i++) {
                    if (docs[i].DocAssocStatus != 1) {
                        askClearCondition = false;
                    }
                }

                return askClearCondition;
            }

            function setDirtyBit() {
                isDirty = true;
                $('#btnSave').prop('disabled', false);
            }

            function resetDirtyBit() {
                isDirty = false;
                $('#btnSave').prop('disabled', true);
            }
            window.resetDirtyBit = resetDirtyBit;
            window.setDirtyBit = setDirtyBit;

            //called when a field is drop onto the editor
            //this == div being dropped (element)
            function _onInitialDropRenderField() {
                if( $(this).hasClass('pdf-field-note') == false ) {
                    return;
                }
                var now = new Date();
                var date = [ now.getMonth() +1, '/', now.getDate(), '/',  now.getFullYear() ].join('');


                var data = $(this).data(_dataKey);
                if (null == data) {
                    data = {};
                    $(this).data(_dataKey, data);
                }

                data.NoteStateType = 'maximized';
                data.width = data.height = 170;
                var o = {'Text' : '', 'LastModifiedOn' : date, 'NoteStateType' : 'maximized', 'width' : data.width, 'height' :data.height};
                _renderNote.call(this, o);
            }

            //Based on state sets the image
            //thumb == jquery object
            function _setThumbNote(thumb, state) {
                thumb.children('img').attr('src', _getThumbNoteImg(state));
            }

            //based on state returns the img url for the thumbnail
            function _getThumbNoteImg(state) {
                return state == 'minimized' ? '../../images/edocs/note-thumb-min.png' :  '../../images/edocs/note-thumb-max.png';
            }

            //renders the note object expects data to be set with the needed info
            function _renderNote(o) {
                $(this).empty().attr({'min-resize-width': '80', 'min-resize-height' : '80'});
                $.tmpl(_noteTemplate,o).appendTo(this);
                $(this).addClass('pdf-field-note-style');

                $('.minimize-note, .maximize-note', this).click(function(e){
                    var maximize = $(this).hasClass('maximize-note');
                    e.preventDefault();
                    e.stopPropagation();
                    e.stopImmediatePropagation();

                    var parent = $(this).parent().parent().get(0);
                    var data = $(parent).data(_dataKey);

                    $('.maximize-item, .minimize-item', parent).toggle();

                    if( maximize ) {
                        $(parent).removeClass('disable-resize').width(data.width).height(data.height);
                        data.NoteStateType = 'maximized';
                    }
                    else {
                        $(parent).addClass('disable-resize').width(35).height(35);
                        data.NoteStateType = 'minimized';
                    }

                    _pdfEditor.EnforceBoundaries(parent);
                });
                $(this).resize(resizeTextArea);

                $('textarea', this).blur(function(){
                    var parent = $(this).parent().parent().get(0);
                    $(parent).data(_dataKey).Text = $(this).val();

                }).change(setDirtyBit);

                if( o.NoteStateType == 'maximized'){
                    $('.minimize-item', this).hide();
                    $(this).width(o.width).height(o.height);
                }
                else {
                    $('.maximize-item', this).hide();
                    $(this).addClass('disable-resize').width(35).height(35);
                }

                if( false === _canEditAnnotations ) {
                    $('textarea', this).prop({'readonly': true, 'disabled': true});
                }

                $(this).triggerHandler('resize');
            }

            //updates the remove notation  status based on how many pdf fields are selected
            function _onPdfFieldSelectedStop(e, count) {
                if( _canEditAnnotations === false ) {
                    return;
                }
                var fields = $('#btnRemoveAnnotations');
                if( count > 0 ) {
                    fields.prop('disabled', false).children('img').attr('src', <%=AspxTools.JsString(ResolveUrl("~/images/edocs/remove_notation.png"))%>);
                }
                else {
                    fields.prop('disabled', true).children('img').attr('src', <%=AspxTools.JsString(ResolveUrl("~/images/edocs/remove_notation_disable.png"))%>);
                }
            }


            function resizeTextArea() {
               var ta = $('textarea', this);
               var bIsHidden =$(this).data('__HIDDEN__');

               if (bIsHidden == null)
               {
                  //bIsHidden = ta.is(':hidden'); // 5/19/2011 dd - This is an expensive call.
                  // 5/19/2011 dd - Let assume it is not hidden when not set.
                  bIsHidden = false;
               }
               if( ta.length == 0 || bIsHidden) {
                return;
               }

               var parentHeight = 0; //$(this).height();
               var parentWidth = 0; //$(this).width();

               var taPositionTop = 5;
               var pdfFieldInfo = null; // this cache does not work well $(this).data('pdfFieldData');
               if (pdfFieldInfo != null)
               {
                parentHeight = pdfFieldInfo.height;
                parentWidth = pdfFieldInfo.width;
               }
               else
               {
                parentHeight = $(this).height();
                parentWidth = $(this).width();
               }
               //var taStart = ta.offset

               ta.height(parentHeight - 35 )
               ta.width(parentWidth - 20);
            }



            //called when a pdf field is rendered ( initial - on load  on drop calls diff method)
            function _renderPdfFieldContent(o) {
                //go ahead and disable movement on the fields
                if( false === _canEditAnnotations ) {
                    $(this).addClass('master-disable-resize disable-move');
                }
                if( o.cssClass == 'pdf-field-note' ) {
                    _renderNote.call(this, o);
                } else if( o.cssClass == 'pdf-field-highlight' ) {
                    $(this).addClass('highlight-field');
                }
            }


            function _onPdfFieldDrop(evt, o) {
                var data = $(o).data(_dataKey);
                var content = $('<div class="highlight-field highlight-field-thumb"></div>');

                if( data.cssClass == 'pdf-field-note' ) {
                    var tmplData = { src :  _getThumbNoteImg(data.NoteStateType) };
                    content = $.tmpl(_noteThumbTemplate,tmplData);
                }
            }

            $('#btnMinimize,#btnMaximize').on('click', function(){
                var selector = this.id === 'btnMinimize' ?  '.minimize-note:visible' : '.maximize-note:visible';
                $(selector).click();
            });
            $('#btnRemoveAnnotations').on('click', function(){
                var list = _pdfEditor.GetSelectedPdfFields();
                if (list.length < 1) {
                    return;
                }
                setDirtyBit();
                $.each(list, function(idx, o) { _pdfEditor.RemovePdfField(o);});
            });


            function GetPageUrl(o) {
                return <%= AspxTools.SafeUrl(Tools.GetEDocsPngLink()) %> + '&pngkey=' + encodeURIComponent(o.PngKey) + '&key=' + encodeURIComponent(o.Token)+'&pg=' + o.Page;
            }

            function getPageMap(page) {
                if (typeof getPageMap.pageMap == "undefined") {
                    getPageMap.pageMap = {};
                    var pg = 0, docs = BatchEDocSet, docCount = docs.length, tpageMap = getPageMap.pageMap;
                    for (var i = 0; i < docCount; i++) {
                        var doc = docs[i], pageCount = doc.PageCount, startPage = pg +1;

                        for (var y = 0; y < pageCount; y++) {
                            pg+= 1;
                            tpageMap[pg] = { Doc : doc, StartPage : startPage, Index : i };
                        }
                    }
                }
                return getPageMap.pageMap[page];
            }

            function getPageInDocument(page) {
                var details =  getPageMap(page);
                return page - details.StartPage;
            }

            function getDocument(page){
                return getPageMap(page).Doc;
            }

            function addNavHeader() {
               $('<li>').html('Document Navigation').addClass('header').appendTo($thumb);
            }


            function addDocument(folder, doctype, description, page, status) {
                var li = $('<li>', {'data-page': page}).addClass('s_'+status),
                    noteDiv = $('<div>').addClass('notes').append($('<div>').text(doctype))
                                .append($('<div>').text(description)),
                    anchorDiv = $('<div>').addClass('link').append($('<a>').html('open pdf'));

                if (!$currentFolder || $currentFolder.data('folderName') != folder){
                    var $li = $('<li>').append($('<span>').text(folder)).append($('<ol>', { 'data-folder-name' : folder }).addClass('document-name-listing'));
                    $thumb.append($li);
                    $currentFolder = $li.children('ol');
                }
                li.append(noteDiv).append(anchorDiv).appendTo($currentFolder);
            }

            function UpdateSelectedDocument($li) {
                $thumb.find('ol.document-name-listing li').removeClass('selected');
               $li.addClass('selected');
            }
            $('#<%= AspxTools.ClientId(m_publicDescription) %>').change(function(){
                getDocument(_pdfEditor.GetCurrentPageNumber()).Description = this.value;
                setDirtyBit();
            });
            $('#<%= AspxTools.ClientId(m_docDescription) %>').change(function(){
               getDocument(_pdfEditor.GetCurrentPageNumber()).InternalDescription = this.value;
                setDirtyBit();
            });
            $('#<%= AspxTools.ClientId(m_docStatusDescription) %>').change(function(){
               getDocument(_pdfEditor.GetCurrentPageNumber()).DocumentStatusReasonDescription = this.value;
                setDirtyBit();
            });

            function onStatusChange() {
                var status = $('#<%= AspxTools.ClientId(m_docStatus)%>').val();
                if (status == '3'){
                    $('#pnlStatusDescription').css('display', '');
                }
                else {
                    $('#pnlStatusDescription').css('display', 'none');
                }
            }
            function OnPageChange() {
                var page = _pdfEditor.GetCurrentPageNumber(),
                    pageMap = getPageMap(page),
                    docIndex = pageMap.Index ,
                    doc = pageMap.Doc;

                currentDocDesc.text(doc.DocTypeDescription);
                UpdateSelectedDocument($thumb.find('ol.document-name-listing li:eq('+ docIndex +')')); //skip header

                $('#<%= AspxTools.ClientId(m_publicDescription) %>').val(doc.Description);
                $('#<%= AspxTools.ClientId(m_docDescription) %>').val(doc.InternalDescription);
                $('#<%= AspxTools.ClientId(m_docStatus) %>').val(doc.DocumentStatus);
                $('#<%= AspxTools.ClientId(m_docStatusDescription) %>').val(doc.DocumentStatusReasonDescription);
                $('#<%=AspxTools.ClientId(m_docTypeList.DescriptionCtrl) %>').text(doc.DocTypeDescription);
                $('#<%=AspxTools.ClientId(m_docTypeList.ValueCtrl) %>').val(doc.DocTypeId);

                if (doc.DocumentStatus == 3) {
                    $('#DoesNotSatisfyBtn, #SatisfyBtn').prop({'disabled': true, 'title' : 'This edoc has been rejected.'});

                }
                else if( !_canEditApprovedDocs && doc.DocumentStatus == 2){
                    $('#DoesNotSatisfyBtn, #SatisfyBtn').prop({'disabled': true, 'title' : 'You cannot edit associations for approved docs.'});
                }
                else {
                    $('#DoesNotSatisfyBtn, #SatisfyBtn').prop({'disabled' : false, 'title' :''});
                }


                onStatusChange();
            }


            function OnSatisfyChange(status) {
                var page = _pdfEditor.GetCurrentPageNumber(),
                    pageMap = getPageMap(page),
                    docIndex = pageMap.Index ,
                    doc = pageMap.Doc,
                    args = null;

                if (status == 2){
                    showModal('/newlos/ElectronicDocs/DocumentRejection.aspx?docid=' + doc.DocumentId + '&loanid=' + ML.sLId +'&taskid=' + $('#TaskId').val(), null, null, null, function(args){ 
                        if (args.Status == 'All' ){
                            doc.DocumentStatus = 3; //set document to rejected and use the specified reason.
                            doc.DocumentStatusReasonDescription = args.Reason;
                            $('#<%= AspxTools.ClientId(m_docStatusDescription) %>').val(args.Reason);
                            $('#<%= AspxTools.ClientId(m_docStatus) %>').val(doc.DocumentStatus);
                        }
                        else if (args.Status == 'This'){
                            doc.InternalDescription +=  " Rejected for Condition " + $('#TaskId').val() + ": " + args.Reason;
                            $('#<%= AspxTools.ClientId(m_docDescription) %>').val(doc.InternalDescription);
                        }
                        else {
                            return;
                        }
                    },{ hideCloseButton: true });

                }
                else if(status  == 1) {
                    doc.DocumentStatus = 2;
                    doc.InternalDescription += " Accepted for Condition " + $('#TaskId').val();
                    $('#<%= AspxTools.ClientId(m_docStatus) %>').val(doc.DocumentStatus);      
                }

                doc.DocAssocStatus = status;
                $li = $thumb.children('ol.document-name-listing li:eq('+ docIndex +')');
                $li.removeClass('s_0 s_1 s_2').addClass('s_' + doc.DocAssocStatus );
                setDirtyBit();
                OnPageChange();
            }

            $('#SatisfyBtn').click(function(){
                OnSatisfyChange(1);

                if (AreConditionsCleared()) {
                    clearDialog.dialog('open');
                }
            });


            $('#DoesNotSatisfyBtn').click(function(){
                OnSatisfyChange(2);
            });

            window.OnChange_gDocTypePicker = function(doctypeid, docandfolder, folder, docDesc) {
                var pageMap =  getPageMap(_pdfEditor.GetCurrentPageNumber()), currentDoc = pageMap.Doc, oldFolder = currentDoc.Folder;

                setDirtyBit();

                currentDoc.DocTypeId = doctypeid;
                currentDoc.DocTypeDescription = docandfolder;
                currentDoc.DocType = docDesc;
                currentDoc.Folder = folder;

                if (oldFolder == folder) {
                    return; //we are done
                }

                BatchEDocSet.splice(pageMap.Index, 1); //remove element
                var index = pageMap.Index, docs = BatchEDocSet, docCount = docs.length,
                    i = 0, newIndex = -1, found = false, iFolder = null;

                //Assumption: docs is always ordered by folder.
                for (i; i < docCount; i++) {
                    iFolder = docs[i].Folder;

                    if (folder == iFolder) {
                        found = true; //a folder with this same name exist in navigation move doc to here
                        newIndex = i;
                        break;
                    }
                    else if (folder < iFolder) {
                        newIndex = i; //a folder who order is after
                        break;
                    }
                }

                if (newIndex == -1) { //the new folder is last.
                    BatchEDocSet.push(currentDoc); //push to end
                    newIndex =  _pdfEditor.GetTotalPages();
                }
                else {
                     BatchEDocSet.splice(newIndex, 0, currentDoc);
                }

                _pdfEditor.MovePages(pageMap.StartPage, currentDoc.PageCount, newIndex);
                delete getPageMap.pageMap; //need this to be rebuilt
                recreateNavigation();
                setTimeout(OnPageChange, 0);
            }


            function maintainNewDoc(doc, pages) {
                var i = 0, docs = BatchEDocSet, docCount = docs.length,
                    newIndex = -1, found = false, iFolder = null, folder = doc.Folder;

                for (i; i < docCount; i++) {
                    iFolder = docs[i].Folder;

                    if (folder == iFolder) {
                        found = true; //a folder with this same name exist in navigation move doc to here
                        newIndex = i;
                        break;
                    }
                    else if (folder < iFolder) {
                        newIndex = i; //a folder who order is after
                        break;
                    }
                }

                if (newIndex == -1) { //the new folder is last.
                    BatchEDocSet.push(doc); //push to end
                    newIndex =  _pdfEditor.GetTotalPages();
                }
                else {
                     BatchEDocSet.splice(newIndex, 0, doc);
                }

                _pdfEditor.InsertPages(pages, newIndex);

                $.each(doc.InitialAnnotations, function(i,o) {
                    this.pg += newIndex;
                    var oDiv = _pdfEditor.AddPdfField(this);
                        _onPdfFieldDrop(null, oDiv);
                        $(oDiv).data('__HIDDEN__', false); // Assume all visible
                });

                 $.each(_pdfEditor.GetFields(), function() {resizeTextArea.call(this);});
            }



            function recreateNavigation() {
                var docs = BatchEDocSet, docCount = docs.length, container = $('<ol/>'),
                    currentFolder = null, i = 0, page = 0;
                $thumb.empty();
                container.append($('<li>').addClass('header').html("Document Navigation"));

                for (i; i < docCount; i++) {
                    doc = docs[i];
                    if ( currentFolder == null || currentFolder.data('folderName') != doc.Folder) {
                        currentFolder = $('<li>')
                            .append($('<span>').text(doc.Folder))
                            .append($('<ol>', { 'data-folder-name' : doc.Folder })
                                .addClass('document-name-listing'));
                        currentFolder.appendTo(container);
                        currentFolder = currentFolder.children('ol');
                    }

                    $('<li>', {'data-page': page}).addClass('s_' + doc.DocAssocStatus).append(
                        $('<div>').addClass('notes').
                            append($('<div>').text(doc.DocType)).
                            append($('<div>').text(doc.Description)).
                            append($('<div>').addClass('link').append($('<a>').html('open pdf')))).appendTo(currentFolder);
                    page += doc.PageCount;

                }

                $thumb.append(container.children().remove());
            }

            function ForceQuit(evt, msg) {
                alert(msg);
                //If the doc list is still open, refresh it. Otherwise, redirect us to the doc list.
                if( window.opener && !window.opener.closed ) {
                    try
                    {
                    //Note that window.opener.location.reload() will not work, since for some reason it
                    //causes this function to terminate immediately (even if it's in a try/catch)
                        window.opener.location.href = window.opener.location.href;
                    }catch(e){}
                    onClosePopup();
                }
            }

            $('#btnAdd').click(function(){
                var existingDocs = [], i = 0, docs  = BatchEDocSet, docCount =  docs.length, doc = null;

                for (i; i < docCount; i++) {
                    doc = docs[i];
                    existingDocs.push(doc.DocumentId);
                }

                showModal("/newlos/ElectronicDocs/EDocPicker.aspx?loanid=" + <%= AspxTools.JsString(LoanID) %> + '&taskid=' + $('#TaskId').val(), { FilterDocsIds : existingDocs } , null, null, function(args){ 
                    var docIds = args.DocIds;
                    if (docIds && docIds.length > 0) {
                        $thumb.empty();
                        displayStatusMessage('Prepping editor for new documents.');
                        load (docIds, 0);
                    }
                },{ hideCloseButton: true });

                return false;
            });


            function load(pageSet, index) {
                if (pageSet.length == index){
                    delete getPageMap.pageMap;
                    recreateNavigation();
                    hideStatusMessage();
                    setTimeout(OnPageChange, 0);
                    setDirtyBit();
                    return;
                }
                getPages(pageSet[index], function(pages, doc){
                    maintainNewDoc(doc, pages);
                    load(pageSet, index+1);
                });
            }

            function getPages(documentId, onSuccess) {
                callWebMethodAsync({
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    url: 'EditEDocService.aspx?method=GetBatchDocDetails',
                    data: '{DocId:"'+ documentId +'"}',
                    dataType: 'json',
                    async : 'true'
                }).then(
                    function(msg) {
                        var pages = JSON.parse(msg.d.PagesJSON), doc = JSON.parse(msg.d.DocDetails);
                        onSuccess(pages,doc );
                    },
                    function(msg){
                        ForceQuit(null, 'There was an error loading the pages for document : ' + documentId);
                    }
                );
            }

            var _loaded = 1;  //pdf editor init loads the first doc.
        function loadRest(){
            if( _loaded >= BatchEDocSet.length ) {
                hideStatusMessage();
                return;
            }
            var doc = BatchEDocSet[_loaded];
            getPages(doc.DocumentId, InitialPageLoad);
        }

        function InitialPageLoad(pages, currentDoc) {
            var doc = BatchEDocSet[_loaded];
            if (doc.Version != currentDoc.Version) {
                ForceQuit(null, 'The document has change please reload.');
            }
            var startPage = _pdfEditor.GetTotalPages();
            _pdfEditor.InsertPages(pages, _pdfEditor.GetTotalPages() , 0);
            $.each(BatchEDocSet[_loaded].InitialAnnotations, function() {
                   var oDiv = _pdfEditor.AddPdfField(this);
                   _onPdfFieldDrop(null, oDiv);
                   $(oDiv).data('__HIDDEN__', false); // Assume all visible
            });
            toggleFields();
            $.each(_pdfEditor.GetFields(), function() {resizeTextArea.call(this);});
            addDocument(doc.Folder, doc.DocType, doc.Description, startPage, doc.DocAssocStatus);
            _loaded += 1;
            loadRest();

        }

        function toggleFields() {
            var id = $("#tabs div.ui-tabs-panel:not(.ui-tabs-hide)").prop('id');
            var showNotes = id == '<%= AspxTools.ClientId(ViewInternalNotes) %>';
            var showSignature =  id == '<%= AspxTools.ClientId(Signature) %>';

            _pdfEditor.GetFields().each(function() {
                var $this = $(this);
                if( _IsSignature(this) ){
                    _ToggleInteractive(this, showSignature);
                }
                else {
                    $this.data("__HIDDEN__", showNotes ? false : true);
                    var thumbData = $this.css('visibility', showNotes ? '' : 'hidden').data('pdfThumbData');
                    $this.removeClass('ui-selected pdf-first-selected');
                }
            });
        }


        $tabs.tabs({
            active: 0,
            activate: function(event, ui) {
                if (!_pdfEditor){
                    return;
                }
                toggleFields();
            }
        });

         $docStatus.change(function(){
            var num = this.value;
            var doc = getDocument(_pdfEditor.GetCurrentPageNumber());
            doc.DocumentStatus = num;
            if (this.value == '3') {
                $('#rejectionReeasonDialog').val('');
                $('#RejectedReasonDialog').dialog({
                    modal: true,
                    open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
                    closeOnEscape : false,
                    buttons: {
                        "OK": function() {
                                $( this ).dialog( "close" );
                                var reason = $('#rejectionReeasonDialog').val();
                                $('#<%= AspxTools.ClientId(m_docStatusDescription) %>').val(reason);
                                doc.DocumentStatusReasonDescription = reason;
                                onStatusChange();
                            }
                        }
                    });
                    var pageMap = _pdfEditor.GetCurrentPageNumber();
                    doc.DocAssocStatus = 2;
                    $li = $thumb.children('ol.document-name-listing li:eq('+  pageMap.Index +')');
                    $li.removeClass('s_0 s_1 s_2').addClass('s_' + doc.DocAssocStatus );
            }
            else {
                onStatusChange();
            }
            OnPageChange();
            setDirtyBit();
        });

        _pdfEditor = new LendersOffice.PdfEditor('editor', FirstEDocPages, {
            AllowEdit : function () { return true; },
            GenerateFullPageSrc : GetPageUrl,
            OnPageStatusChanged : OnPageChange,
            OnError : ForceQuit,

            OnInitialDropRenderField : _onInitialDropRenderField,
            OnPdfFieldSelectedStop : _onPdfFieldSelectedStop,
            OnRenderPdfFieldContent : _renderPdfFieldContent,
            OnPdfFieldDrop : function(e,t) { _onPdfFieldDrop(e,t); setDirtyBit(); },
            CanEditAnnotations : _canEditAnnotations,

            EmptyImgSrc : VRoot + '/images/pixel.gif',
            AddPagesWhileDisabled : true,
            GoToNewDocOnAddition : false,
            OnPdfFieldDrag : setDirtyBit,
            ManageThumbnailPanel : false,
            PageBottomPadding : 10
        });

            _pdfEditor.Initialize(function(){
                $.each(BatchEDocSet[0].InitialAnnotations, function() {
                    var oDiv = _pdfEditor.AddPdfField(this);
                    _onPdfFieldDrop(null, oDiv);
                    $(oDiv).data('__HIDDEN__', false); // Assume all visible
                });

                $.each(_pdfEditor.GetFields(), function() {resizeTextArea.call(this);});

                $thumb = $(_pdfEditor.GetThumbPanelSelector());
                $thumb.addClass('folder-name-listing');
                addNavHeader();
                var doc = BatchEDocSet[0];


                addDocument(doc.Folder, doc.DocType, doc.Description, 0, doc.DocAssocStatus);
                toggleFields();
            });

            $thumb.on('click', 'ol.document-name-listing > li', function(e){
                e.stopImmediatePropagation();
                var $li = $(this), index = $li.data('page');
                if (index == '-1'){
                    return;
                }
                _pdfEditor.MoveToPage(index);
                UpdateSelectedDocument($li);
            }).on('click', 'a', function(){
                var  id = parseInt($(this).parents('li').data('page')),
                    doc = getDocument(1+id);
                window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + doc.DocumentId , '_parent');

            }).on('click', 'li', function(){
                var $li = $(this);
                $li.toggleClass('close');
                $li.children('ol').toggle();
            });



            var div = $('#sigDiv'); //needed for th eimage to fill the div when resizing.
            if( div.length > 0 ){
                var width = div.children('img').width();
                var height = div.children('img').height();
                div.height(height);
                div.width(width);
                div.children('img').width('100%');
                div.children('img').height('100%');
            }

            //setup draggable
            var filter = '.signature-field';

            if (_canEditAnnotations) {
                filter += ', .annotation-field';
            }
            if (filter.length > 0) {
                $(filter).draggable({revert:'invalid', helper:'clone', zIndex: 2700});
            }

            loadRest();


            function _IsSignature(o){
                var $o = $(o);
                return $o.hasClass('pdf-field-signature-img') || $o.hasClass('pdf-field-date');
            }

            function _ToggleInteractive(o, enable) {
                var $o = $(o);
                if( $o.hasClass('saved') && enable === true ) {
                    return;
                }
                $o.toggleClass('disable-resize', !enable).toggleClass('disable-move', !enable).toggleClass('disable-selectable', !enable);
                $o.removeClass('ui-selected pdf-first-selected');
            }

             function saveOnExit() { return confirm("Do you want to save changes?"); }

             function page_unload() {
                if ( !__isDoneLoading) {
                    return;
                }
                if( isDirty )
                {
                    if( saveOnExit() ) {
                        _saveImpl();
                    }

                    isDirty = false; //reset it
                }
             }

            window.page_unload = page_unload;

            function _Save() {

                if (!__isDoneLoading) {
                    alert('Please wait until loading is complete.');
                    return false;
                }

                displayStatusMessage('Saving...');

                setTimeout(_saveImpl,0);

            }

            function _saveImpl() {

              var clearCondition = AreConditionsCleared() && _clearConditionOnSave;

              var data = {
                      'Data' :  BatchEDocSet,
                      'LoadedAnnotations' : _canEditAnnotations,
                      'NewAnnotations' : [],
                      'TaskId' : $('#TaskId').val(),
                      'Signatures' : [],
                      'LoanId' : ML.sLId,
                      'ConditionSubject ' : $('#ConditionSubject').val(),
                      'ConditionInternalNotes' : $('#ConditionInternalNotes').val(),
                      'ConditionCategory' :  $('#Category').val(),
                      'CloseCondition' : clearCondition
                };

                $.each(_pdfEditor.GetPdfFields(), function(i,o){
                    if (o.cssClass == 'pdf-field-highlight' || o.cssClass == 'pdf-field-note') {
                        data.NewAnnotations.push(o);
                    }
                    else if (o.cssClass == 'pdf-field-signature-img' || o.cssClass == 'pdf-field-date' ) {
                        data.Signatures.push(o);
                    }
                    else {
                        alert('Unhandled field. ' + o.cssClass);
                    }
                });

                data.Data =  JSON.stringify(data.Data);
                data.LoadedAnnotations =  JSON.stringify(data.LoadedAnnotations);
                data.NewAnnotations =  JSON.stringify(data.NewAnnotations);
                data.Signatures = JSON.stringify(data.Signatures);

                var results = gService.loanedit.call('SaveSignOff', data);

                if( results.error ) {
                    ForceQuit(null, results.UserMessage);
                    return;
                }
                else {
                    if( results.value.SaveResults.length > 0 ) {
                        alert( results.value.SaveResults ) ;
                }

                if( results.value.VersionMismatches ) {
                    alert(results.value.VersionMismatches);
                }

                resetDirtyBit();

                if ( typeof(opener.ConditionTable) !== "undefined" ) {
                    opener.ConditionTable.reload(); // TODO: better solution would be to just reload this single condition
                }

                if (clearCondition) {
                    onClosePopup();
                    return;
                }

                var versions = JSON.parse(results.value.NewVersions), count = BatchEDocSet.length;

                for( var i = 0; i < count; i++){
                    BatchEDocSet[i].Version = versions[i];
                }

                $.each(_pdfEditor.GetFields(), function(i,o){
                    if( _IsSignature(o) ) {
                        $(o).addClass('saved').removeClass('pdf-pdf-field pdf-field-template');
                        _ToggleInteractive(o, false);
                    }
                });
                hideStatusMessage();

                if (_clearConditionOnSave) {
                    onClosePopup();
                }
            }
            }

            $('#btnSave').click(function(){
                if ($(this).prop('disabled')) {
                    return false;
                }
                _Save();
            });


            $('#Category, #ConditionInternalNotes, #ConditionSubject').change(setDirtyBit);
        });
    </script>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="NotReadyDocs" Visible="false">
        The sign off editor cannot be opened until all associated eDocs have been prepared for the editor. Try again in a few minutes.
    </asp:PlaceHolder>
    </form>




</body>
</html>
