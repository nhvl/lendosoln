﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using EDocs;
using LendersOffice.Security;
using System.Web.UI.HtmlControls;
using LendersOffice.AntiXss;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class EDocumentAudit : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            EnableJqueryMigrate = false;
            RegisterJsScript("utilities.js");

            if (!BrokerUser.HasPermission(LendersOffice.Security.Permission.CanViewEDocs))
            {
                throw new AccessDenied();
            }

            DisplayCopyRight = false;
            Guid docId = RequestHelper.GetGuid("docid");

            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            EDocument doc = repo.GetDocumentById(docId);
            AuditHeader.Text = string.Format("Document Audit History - {0}", doc.DocTypeName);
            AuditEvents.DataSource = doc.AuditEvents;
            AuditEvents.DataBind();
            ViewOriginalLink.Attributes.Add("onclick", "viewOriginal('" + AspxTools.JsStringUnquoted(doc.DocumentId.ToString()) + "');");

        }

        protected void AuditEvents_OnItemDataBound(object sender, DataGridItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            EDocumentAuditEvent auditEntry = args.Item.DataItem as EDocumentAuditEvent;

            EncodedLabel description = (EncodedLabel)args.Item.FindControl("Description");
            description.Text = auditEntry.Description;

            HtmlAnchor anchor = (HtmlAnchor) args.Item.FindControl("Details");
            anchor.Visible = auditEntry.HasDetails;

            anchor.Attributes.Add("BeforeEdit", AspxTools.HtmlString(auditEntry.PreviousValue));
            anchor.Attributes.Add("AfterEdit", AspxTools.HtmlString(auditEntry.NewValue));
        }
    }
}
