﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using DataAccess;
using EDocs;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.ObjLib.DocumentConditionAssociation;
using LendersOffice.ObjLib.Task;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class SignOffEditor : BaseLoanPage
    {

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] {
                    Permission.AllowUnderwritingAccess,
                    Permission.CanViewEDocs,
                    Permission.CanEditEDocs,
                    Permission.AllowEditingApprovedEDocs
                };
            }
        }

        protected override bool EnableJQueryInitCode
        {
            get
            {
                return true;
            }
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.EmulateIE7;
        } 
        private ConditionCategoriesWithPermissionInfo Categories
        {
            get; set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (BrokerUser.HasLenderDefaultFeatures == false || Broker.IsUseNewTaskSystem == false)
            {
                throw new AccessDenied("HasLenderDefaultFeatures: " + BrokerUser.HasLenderDefaultFeatures + " IsUseNewTaskSystem: " + Broker.IsUseNewTaskSystem);
            }

            this.RegisterService("loanedit", "/newlos/ElectronicDocs/EditEDocService.aspx");
            DisplayCopyRight = false;
            //todo check for permissions

            RegisterJsScript("json.js");
            this.EnableJqueryMigrate = false;
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("LQBPopup.js");	
            RegisterJsScript("PdfEditor.js");
            IncludeStyleSheet("~/css/PdfEditor.css");
            

            if (this.Broker.EnableLqbNonCompliantDocumentSignatureStamp)
            {                
                EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(BrokerUser.BrokerId, BrokerUser.EmployeeId, BrokerUser.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);

                SignatureTab.Visible = signInfo.HasUploadedSignature && BrokerUser.HasPermission(Permission.CanEditEDocs);
                Signature.Visible = SignatureTab.Visible;
                SignatureImg.ImageUrl = "UserSignature.aspx?eid=" + BrokerUser.EmployeeId;

                if (signInfo.HasUploadedSignature)
                {
                    var sigDetails = signInfo.SignatureDetails;

                    if (sigDetails.Width > sigDetails.Height)
                    {
                        SignatureImg.Height = 50;
                        SignatureImg.Width = (int)(50 * sigDetails.AspectRatio);
                    }
                    else
                    {
                        SignatureImg.Width = 168;
                        SignatureImg.Height = (int)(168 / sigDetails.AspectRatio);
                    }

                    SignatureImg.Attributes.Add("ar", sigDetails.AspectRatio.ToString("0.00"));
                }
            }

            m_docStatus.Items.Add(new ListItem(EDocument.StatusText(E_EDocStatus.Blank), E_EDocStatus.Blank.ToString("d")));
            m_docStatus.Items.Add(new ListItem(EDocument.StatusText(E_EDocStatus.Obsolete), E_EDocStatus.Obsolete.ToString("d")));
            m_docStatus.Items.Add(new ListItem(EDocument.StatusText(E_EDocStatus.Approved), E_EDocStatus.Approved.ToString("d")));
            m_docStatus.Items.Add(new ListItem(EDocument.StatusText(E_EDocStatus.Rejected), E_EDocStatus.Rejected.ToString("d")));
            m_docStatus.Items.Add(new ListItem(EDocument.StatusText(E_EDocStatus.Screened), E_EDocStatus.Screened.ToString("d")));

            m_docStatus.Enabled = true;
            m_docStatusDescription.Enabled = true;
        
            CPageData loanData = CPageData.CreateUsingSmartDependency(LoanID, typeof(SignOffEditor));
            loanData.InitLoad();
            CAppData app = loanData.GetAppData(0);
            Header.Title = string.Format("{0} - {1} - Signoff Editor", app.aBLastFirstNm, loanData.sLNm);


            string taskid = RequestHelper.GetSafeQueryString("conditionid");
            List<DocumentConditionAssociation> associations = DocumentConditionAssociation.GetAssociationsByLoanCondition(Broker.BrokerID, LoanID, taskid);
            Dictionary<Guid, DocumentConditionAssociation> associationsByDDocId = associations.ToDictionary(p=>p.DocumentId);
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            List<EDocument> docsToLoad = new List<EDocument>();
            Task task = Task.Retrieve(Broker.BrokerID, taskid);

            var permLevel = task.GetPermissionLevelFor(BrokerUser);

            bool userCanEdit = (task.TaskOwnerUserId == BrokerUser.UserId || permLevel == E_UserTaskPermissionLevel.Manage) && (task.TaskStatus == E_TaskStatus.Active || task.TaskStatus == E_TaskStatus.Resolved);

            btnAdd.Disabled = task.TaskStatus == E_TaskStatus.Closed;

            Category.Enabled = userCanEdit;
            ConditionSubject.Enabled = userCanEdit;
            ConditionInternalNotes.Enabled = userCanEdit;

            ClientScript.RegisterHiddenField("CanCloseCondition", (permLevel == E_UserTaskPermissionLevel.Close || permLevel == E_UserTaskPermissionLevel.Manage).ToString());
            if (!task.TaskIsCondition)
            {
                throw CBaseException.GenericException("Task is not a condition. SignOff editor does not support regular task.");
            }

            this.Categories = new ConditionCategoriesWithPermissionInfo(PrincipalFactory.CurrentPrincipal);

            Tools.Bind_ConditionCategoryDdl(task.UserPermissionLevel, this.Categories, Category, task.CondCategoryId, task.CondCategoryId_rep);
            Tools.SetDropDownListValue(Category, task.CondCategoryId.Value);
            ConditionSubject.Text = task.TaskSubject;
            ClientScript.RegisterHiddenField("TaskId", task.TaskId);
            ConditionInternalNotes.Text = task.CondInternalNotes; 

            List<EDocBatchEditDetails> details = new List<EDocBatchEditDetails>();
      
            foreach (Guid id in associations.Select(p=>p.DocumentId))
            {
                EDocument doc = repo.GetDocumentById(id);
                docsToLoad.Add(doc);
                if (doc.ImageStatus != E_ImageStatus.HasCachedImages)
                {
                    NotReadyDocs.Visible = true;
                    Viewer.Visible = false;
                    return;
                }
               
            }
            int count = 0;
            docsToLoad = docsToLoad.OrderBy(p => p.Folder.FolderNm).ThenBy(p => p.DocType.DocTypeName).ToList();

            for( int i = 0; i < docsToLoad.Count; i++)
            {
                EDocument doc = docsToLoad[i];
                EDocBatchEditDetails docDetails = new EDocBatchEditDetails(doc, count);
                docDetails.DocAssocStatus = associationsByDDocId[doc.DocumentId].Status;
                details.Add(docDetails);

                count += doc.PageCount;
            }
            List<PdfPageItem> pages = docsToLoad.First().GetPdfPageInfoList();
            foreach (var item in pages)
            {
                item.Name = details[0].DocTypeDescription;
                item.Folder = details[0].Folder;
                item.DocType = details[0].DocType;
                item.CanEditSource = details[0].CanEditSource && SignatureTab.Visible;
                item.DocAssocStatus = associationsByDDocId[details[0].DocumentId].Status;
            }

            RegisterJsObject("FirstEDocPages", pages);
            RegisterJsObject("BatchEDocSet", details);

            ClientScript.RegisterHiddenField("AllowEditingApprovedEDocs", BrokerUser.HasPermission(Permission.AllowEditingApprovedEDocs).ToString());
            CanEditAnnotations.Value = BrokerUser.HasPermission(Permission.CanEditEDocsInternalNotes).ToString();
            DragAndDropTable.Visible = BrokerUser.HasPermission(Permission.CanEditEDocsInternalNotes);
            InternalNoteTab.Visible = BrokerUser.HasPermission(Permission.CanViewEDocsInternalNotes);
            ViewInternalNotes.Visible = InternalNoteTab.Visible;
            CanEditEdocs.Value = BrokerUser.HasPermission(Permission.CanEditEDocs).ToString();

        }
    }
}
