﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    //TODO: Rename this to SplitSingleEdoc, because that's what it does.
    public partial class SplitEdocV2 : BaseLoanPage
    {
        public override bool IsReadOnly
        {
            get
            {
                return false;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoan = new CPageData(LoanID, new[] { "aBFirstNm", "aBLastNm" });
            dataLoan.InitLoad();
            dataLoan.LoadAppNames(ApplicationSelect);
            EnableJqueryMigrate = false;
            DisplayCopyRight = false;
            RegisterJsScript("json.js");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
        }
    }
}
