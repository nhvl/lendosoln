﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ElectronicDocs.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.ElectronicDocs" %>
<%@ Register TagPrefix="uc1" TagName="DocsReceived" Src="~/newlos/ElectronicDocs/DocsReceived.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UploadDocs" Src="~/newlos/ElectronicDocs/UploadDocs.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ShipDocs" Src="~/newlos/ElectronicDocs/ShipDocs.ascx" %>
<%@ Register TagPrefix="uc1" TagName="FaxDocs" Src="~/newlos/ElectronicDocs/FaxDocs.ascx" %>
<%@ Register TagPrefix="uc1" TagName="DocRequests" Src="~/newlos/ElectronicDocs/DocRequests.ascx" %>
<%@ Register TagPrefix="uc1" TagName="DocumentCaptureUpload" Src="~/newlos/ElectronicDocs/DocumentCaptureUpload.ascx" %>
<%@ Register TagName="Tabs" TagPrefix="uc1" Src="~/common/BaseTabPage.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <style type="text/css" > 
        body { background-color: gainsboro; }
        
        th.headerSortUp {
            background-image: url(../../images/Tri_DESC.gif);
            background-repeat: no-repeat;
            background-position: right center;
        }
        
        th.headerSortDown {
            background-image: url(../../images/Tri_ASC.gif);
            background-repeat: no-repeat;
            background-position: right center;
        }
        
        div.EDocInfo { width: 500px; }
        div.EDocInfo, div.fileQueue { float: left; }
        div.EDocInfo td { padding-bottom: 15px; }
    </style>
</head>
<body>
    <script type="text/javascript">
        function setClass(tBody) {
            for (var i = 0; i < tBody.rows.length; i++) {
                var row = tBody.getElementsByTagName("tr")[i];
                if (i % 2 < 1)
                    row.className = 'GridItem';
                else
                    row.className = 'GridAlternatingItem';
            }
        }
    </script>
    <form id="form1" runat="server">
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="Tabs">
                <uc1:Tabs runat="server" ID="Tabs" />
            </td>
        </tr>
        <tr>
            <td style="padding-left: 5px">
                <uc1:DocRequests runat="server" ID="DocRequests" />
                <uc1:ShipDocs runat="server" ID="ShipDocs" />
                <uc1:FaxDocs runat="server" ID="FaxDocs" />
                <uc1:UploadDocs runat="server" ID="UploadDocs" />
                <uc1:DocumentCaptureUpload runat="server" ID="DocumentCaptureUpload" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
