﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDocs;
using System.Text;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class SplitEDoc : BaseLoanPage
    {

        protected void PageInit(object sender, EventArgs e)
        {
            EnableJqueryMigrate = false;
            this.RegisterJsScript("LQBPopup.js");
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            RegisterJsObject("ddlOptions", EDocumentDocType.GetDocTypesByBroker(BrokerUserPrincipal.CurrentPrincipal.BrokerId, E_EnforceFolderPermissions.True).ToList());
            RegisterJsScript("AdjustmentTable.js");
            RegisterJsScript("DynamicTable.js");
            RegisterJsScript("json.js");
            CPageData dataLoan = new CPageData(LoanID, new[] { "aBFirstNm", "aBLastNm" });
            dataLoan.InitLoad();
            dataLoan.LoadAppNames(ApplicationDDLTemplate);

            if (!BrokerUser.HasPermission(Permission.AllowApprovingRejectingEDocs))
            {
                ClientScript.RegisterHiddenField("FilterApprovalRejection", "true");
            }
        }

        protected override void OnInit(EventArgs e)
        {
            this.Init += new System.EventHandler(this.PageInit);
            this.Load += new System.EventHandler(this.PageLoad);
            DisplayCopyRight = false;
            base.OnInit(e);
        }
    }
}
