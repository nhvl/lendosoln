﻿namespace LendersOfficeApp.newlos.ElectronicDocs
{
    using System;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Security;

    public partial class ElectronicDocs : BaseLoanPage
    {
        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[]
            {
                WorkflowOperations.UseDocumentCapture
            };
        }

        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return new WorkflowOperation[]
            {
                WorkflowOperations.UseDocumentCapture
            };
        }

        override protected void OnInit(EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            UseNewFramework = true;
            
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.UseNewFramework = true;

            if (!BrokerUser.HasPermission(LendersOffice.Security.Permission.CanViewEDocs))
            {
                throw new AccessDenied();
            }

            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("jquery.tablesorter.min.js");
            this.RegisterJsScript("DragDropUpload.js");

            this.RegisterCSS("DragDropUpload.css");

            this.SetEnabledPerformancePageRenderTimeWarning(false);
            this.SetEnabledPerformancePageSizeWarning(false);
            RegisterService("edoc", "/newlos/ElectronicDocs/EDocsUtilitiesService.aspx");

            Tabs.RegisterControl("Doc Requests", DocRequests);
            Tabs.RegisterControl("Upload Docs", UploadDocs);
            Tabs.RegisterControl("Cover Sheets", FaxDocs);
            Tabs.RegisterControl("Ship Docs", ShipDocs);

            if (this.BrokerUser.BrokerDB.EnableKtaIntegration)
            {
                this.Tabs.RegisterControl("Document Capture", this.DocumentCaptureUpload);
            }
            else
            {
                this.DocumentCaptureUpload.Visible = false;
            }

            Tabs.AddToQueryString("loanid", LoanID.ToString());
            IncludeStyleSheet("~/css/Tabs.css");
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }


        protected override void OnPreRender(EventArgs e)
        {
            int index = Tabs.TabIndex;
            switch (index)
            {
                case 0:
                    PageID = "Document_Requests";
                    break;
                case 1:
                    PageID = "Upload_Documents";
                    break;
                case 2:
                    PageID = "Fax_Documents";
                    break;
                case 3:
                    PageID = "Ship_Documents";
                    break;
                default:
                    break;
            }
            base.OnPreRender(e);
        }
    }
}
