﻿<%@ Page
    Language="C#"
    AutoEventWireup="true"
    CodeBehind="DocTypePicker.aspx.cs"
    Inherits="LendersOfficeApp.newlos.ElectronicDocs.DocTypePicker"
    EnableViewState="false"
%>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Document Type Picker</title>
    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
    <style type="text/css">
        .AlignLeft
        {
        	text-align: left;
        }
        .Highlight
        {
        	background-color:Yellow;
        }

        #SearchDiv
        {
            margin-top: 1em;
        	margin-bottom: 1em;
            margin-left: 1em;
        }

        #ExplorerDiv
        {
        	height: 360px;
        	overflow-y: scroll;
        	overflow-x: hidden;
            margin-left: 1em;
        }

        #m_DocTypeExplorer
        {

        }

        #Buttons
        {
            text-align: center;
        }
        th.headerSortUp
	    {
            background-image: url(../../images/Tri_DESC.gif);
            background-repeat: no-repeat;
            background-position: right center;
	    }

	    th.headerSortDown
	    {
            background-image: url(../../images/Tri_ASC.gif);
            background-repeat: no-repeat;
            background-position: right center;
	    }
        .padding-4 { padding: 4px; }
        .p-b-FolderMessage { display: block; padding-bottom: .5em; }
    </style>
    <script src="../../inc/jquery.tablesorter.min.js"></script>
</head>
<body>
    <h4 class="page-header"><ml:EncodedLabel runat="server" ID="HeaderText">Select a Doc Type</ml:EncodedLabel></h4>
    <form id="form1" runat="server">
    <script type="text/javascript">
    <!--

        function _init() {
            resizeForIE6And7(425, 500);
            f_searchBoxKeyUp();
            setDefaultFormButton("form1", "SearchBtn");
            $("#m_SearchResults").tablesorter();
            $("#m_SearchResults").on("sortEnd", function() { setClass($('#m_SearchResults tbody')[0]); });
            <% if (LendersOffice.Common.RequestHelper.GetSafeQueryString("allowSearch") == "false"){ %>
            $("#SearchDiv").hide();
            <%} %>
        }

        // Retain alternating row style
        function setClass(tBody) {
            for (var i = 0; i < tBody.rows.length; i++) {
                var row = tBody.getElementsByTagName("tr")[i];
                if (i % 2 < 1)
                    row.className = 'GridItem';
                else
                    row.className = 'GridAlternatingItem';
            }
        }

        // ##### Codebehind! ####################
        // These functions will be called by the codebehind

        function highlight(word, docTypeId) {
            var regex = new RegExp(word, 'g');
            var replacement = "<span class='Highlight'>" + word + "</span>";
            var $elements = $('.'+docTypeId);
            $elements.each(function (i, element) {
                var text = $(element).html();
                text = text.replace(regex, replacement);
                $(element).html(text);
            });
        }

        function f_searchBoxKeyUp() { // this function is connected to the textbox in the codebehind
            var searchBox = document.getElementById('SearchBox');
            var searchBtn = document.getElementById('SearchBtn');
            var len = searchBox.value.length;

            if (len > 0) {
                searchBtn.disabled = false;
            } else {
                searchBtn.disabled = true;
            }
        }
        // ######################################

        function f_cancel() {
            onClosePopup();
        }

        function f_openFolder(folderId) {
            window.location = 'DocTypePicker.aspx' + '?folderid=' + encodeURIComponent(folderId)
                + '&showSystemFolders=' + encodeURIComponent(<%=AspxTools.JsGetElementById(ShowSystemFolders) %>.value)
                +'&brokerid='+ encodeURIComponent(<%=AspxTools.JsGetElementById(m_hfBrokerID) %>.value)
                + '&isDrive='+ encodeURIComponent(<%=AspxTools.JsGetElementById(IsDrive) %>.value)
                + '&isFlood='+ encodeURIComponent(<%=AspxTools.JsGetElementById(IsFlood) %>.value);
        }

        function f_toFolders() {
            window.location = 'DocTypePicker.aspx' +
                '?showSystemFolders=' + encodeURIComponent(<%=AspxTools.JsGetElementById(ShowSystemFolders) %>.value)
                +'&brokerid='+ encodeURIComponent(<%=AspxTools.JsGetElementById(m_hfBrokerID) %>.value)
                + '&isDrive='+ encodeURIComponent(<%=AspxTools.JsGetElementById(IsDrive) %>.value)
                + '&isFlood='+ encodeURIComponent(<%=AspxTools.JsGetElementById(IsFlood) %>.value);
        }

        function f_selectDocTypePackageType(){
            var args = window.dialogArguments || {};
            args.docTypeId = '0';
            args.docTypeName = 'DOCUMENT GENERATION : <PACKAGETYPE>';
            args.folder = 'DOCUMENT GENERATION';
            args.doconly = '<PACKAGETYPE>';
            onClosePopup(args);
        }

        function f_selectDocTypeESign(){
            var args = window.dialogArguments || {};
            args.docTypeId = '0';
            args.docTypeName = 'DOCUMENT GENERATION : ESIGNED UPLOAD';
            args.folder = 'DOCUMENT GENERATION';
            args.doconly = 'ESIGNED UPLOAD';
            onClosePopup(args);
        }

        function f_selectDocType(docTypeId, docTypeName, folder, doconly) {
            var args = window.dialogArguments || {};
            args.docTypeId = docTypeId;
            args.docTypeName = docTypeName;
            args.folder = folder;
            args.doconly = doconly;
            onClosePopup(args);
        }

    -->
    </script>
        <asp:HiddenField ID="m_hfBrokerID" runat="server" />
        <asp:HiddenField ID="IsDrive" runat="server" />
        <asp:HiddenField ID="IsFlood" runat="server" />
        <div id="ResetContainer" runat="server">
            Use document package type:
            <a onclick="f_selectDocTypePackageType()">GENERATED DOCUMENTS: &lt;PACKAGETYPE&gt;</a>
        </div>
        <div id="ESignResetDocTypeContainer" runat="server">
            Use default:
            <a onclick="f_selectDocTypeESign()">GENERATED DOCUMENTS: ESIGNED UPLOAD</a>
        </div>
        <div id="SearchDiv">
            Search for:
            <asp:TextBox runat="server" id="SearchBox" name="q"/>
            <asp:Button type="button" id="SearchBtn" Text="Search" OnClick="SearchBtn_Click" runat="server" NoHighlight />
        </div>
        <div id="ExplorerDiv">
            <div id="DocFolderDiv" runat="server">
                <ml:EncodedLabel class="p-b-FolderMessage" ID="m_FolderMessage" runat="server" Text="Choose a Doc Folder:"></ml:EncodedLabel>
                <asp:GridView ID="m_DocFolderExplorer" runat="server" AutoGenerateColumns="false" EnableViewState="false" Width="380px">
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAlternatingItem" />
                    <RowStyle CssClass="GridItem"/>
                    <Columns>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="AlignLeft" />
                            <HeaderTemplate>Folder</HeaderTemplate>
                            <ItemTemplate>
                                <a onclick="f_openFolder(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "FolderId").ToString()) %>)">
                                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "FolderNm").ToString()) %>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div id="DocTypeDiv" runat="server">
                <asp:PlaceHolder ID="phFoldersLink" runat="server">
                <a onclick="f_toFolders()">Folders</a> &gt <ml:EncodedLabel runat="server" ID="m_selectedFolder" Text="No Folder Selected"></ml:EncodedLabel>
                </asp:PlaceHolder>
                <asp:GridView ID="m_DocTypeExplorer" runat="server" AutoGenerateColumns="false" EnableViewState="false" Width="380px">
                    <HeaderStyle CssClass="GridHeader AlignLeft" />
                    <AlternatingRowStyle CssClass="GridAlternatingItem" />
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="AlignLeft" />
                            <HeaderTemplate>Doc Type</HeaderTemplate>
                            <ItemTemplate>
                                <a onclick="f_selectDocType(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DocTypeId").ToString()) %>,
                                <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "FolderAndDocTypeName").ToString())%>,
                                <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Folder.FolderNm").ToString())%>,
                                <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DocTypeName").ToString())%>)">
                                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DocTypeName").ToString())%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div id="SearchResultDiv" runat="server">
                <ml:EncodedLabel ID="m_SearchMessage" runat="server" Text="Matching Doc Types:"></ml:EncodedLabel>
                <asp:GridView ID="m_SearchResults" runat="server" AutoGenerateColumns="false" EnableViewState="false" Width="380px">
                    <HeaderStyle CssClass="GridHeader AlignLeft" />
                    <AlternatingRowStyle CssClass="GridAlternatingItem" />
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="AlignLeft" />
                            <HeaderTemplate>Doc Type</HeaderTemplate>
                            <ItemTemplate>
                                <a
                                    class='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DocTypeId").ToString()) %>'
                                    onclick="f_selectDocType(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DocTypeId").ToString()) %>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "FolderAndDocTypeName").ToString())%>)"
                                >
                                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DocTypeName").ToString())%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="AlignLeft" />
                            <HeaderTemplate>Folder</HeaderTemplate>
                            <ItemTemplate>
                                <a
                                    class='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "FolderId").ToString()) %>'
                                    onclick="f_openFolder(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "FolderId").ToString()) %>)"
                                >
                                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "FolderNm").ToString()) %>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div id="Buttons">
            <input type="button" value="Cancel" onclick="f_cancel();" NoHighlight />
        </div>
        <asp:HiddenField ID="ShowSystemFolders" runat="server" Value="false" />
    </form>
</body>
</html>
