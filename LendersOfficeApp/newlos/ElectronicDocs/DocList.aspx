﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocList.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.DocList" %>
<%@ Register  TagName="ReceivedDocs" TagPrefix="uc" Src="~/newlos/ElectronicDocs/DocsReceived.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Tabs" Src="~/common/BaseTabPage.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Available EDocs</title>
    <style type="text/css" >
        .DocMagicBar {  display:none;  background-color: #FFFFCC; padding: 5px; }
        .DocMagicBar ul { list-style: none; margin: 0 auto; display: block; width: 250px;}
        .DocMagicTable { background-color: gainsboro; }
        
	    .DocMagicTable { padding: 5px; overflow:auto; width:500px;  height: 100px; border:3px inset black; top: 200px;  margin-left: 32px; left: 0; right: 0;  display: none; position: absolute; background-color : whitesmoke;}
        .DocMagicTable table  { width: 470px; }
        .DocMagicTable a { text-align: center;  display: block; margin: 0 auto; }
        #blackbg { background-color: Black; overflow: auto; }
    </style>
    <script type="text/javascript">
        if (window.opener && !window.opener.closed && typeof window.opener.checkEDocStatus !== 'undefined') {
            var err;
            try {
            window.onunload = window.opener.checkEDocStatus;
            }
            catch(err) {}
        }
    </script>
</head>
<body bgcolor="gainsboro" style="padding: 5px;">
    <form id="form1" runat="server">
    


    <div id="blackbg">
    <uc1:Tabs runat="server" ID="Tabs" />
    </div>
    <uc:ReceivedDocs runat="server" ID="ActiveDocs" />
    <uc:ReceivedDocs runat="server" ID="RejectedDocs" />
    </form>
</body>
</html>
