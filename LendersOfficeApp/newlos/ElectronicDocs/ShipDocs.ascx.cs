﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDocs.UI;
using EDocs;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class ShipDocs : System.Web.UI.UserControl, IAutoLoadUserControl
    {
        protected Guid LoanID { get; set; }

        private bool IsUsingTemplate
        {
            get
            {
                return SelectedTemplateId > 0 ;  
            }
        }

        private int SelectedTemplateId
        {
            get
            {
                return  int.Parse(SelectDocument.SelectedShippingTemplateId);
            }
        }



        #region controls the UI based on steps.
        private void ShowStep1()
        {
            ReviewSelection.Visible = false;
            StackingOrder.Visible = false;
            Step3.Visible = false;
            Step2.Visible = false;
            DisableLink(Step1Link);
            BackBtn.Visible = false;
            SelectDocument.Visible = true;
            Step1Link.OnClientClick = "return false;";
            BackBtn.Visible = false;
            ShippingTemplatePH.Visible = false;
            Step3.Visible = false;
            NextBtn.Visible = true;
            SelectDocument.LoadData();
        }

        private void ShowStep2()
        {
            EnableLink(Step1Link);
            Step1Link.OnClientClick = "";
            Step2.Visible = true;
            ReviewSelection.Visible = true;
            SelectDocument.Visible = false;
            DisableLink(Step2Link);
            BackBtn.Visible = true;
            ShippingTemplatePH.Visible = true;
            SelectedShippingTemplateNameLabel.Text = SelectDocument.SelectedShippingTemplateName;
            ReviewSelection.DataSource = GenerateFinalReviewList();
            Step3.Visible = false;
            NextBtn.Visible = true;
            StackingOrder.Visible = false;
    
        }

        private void ShowStep3()
        {
            EnableLink(Step2Link);
            Step3.Visible = true;
            ReviewSelection.Visible = false;
            NextBtn.Visible = false;
            StackingOrder.Visible = true;
            SelectedShippingTemplateNameLabel.Text = SelectDocument.SelectedShippingTemplateName;
            StackingOrder.SetData(GetStep3Data());
        }
        #endregion

        protected void Step1Link_Click(object sender, EventArgs e)
        {
            ShowStep1();
        }

        protected void Step2Link_Click(object sender, EventArgs e)
        {
            ShowStep2();
        }

        protected void Next_Click(object sender, EventArgs args)
        {
            if (SelectDocument.Visible)
            {
                ShowStep2();
            }
            else
            {
                ShowStep3();
            }
        }

        protected void Back_Click(object sender, EventArgs args)
        {
            if (ReviewSelection.Visible)
            {
                ShowStep1();
            }
            else
            {
                ShowStep2();
            }
        }

        private void DisableLink(LinkButton link)
        {
            link.Style.Add("text-decoration", "none");
            link.Style.Add("color", "black");
            link.Style.Add("cursor", "default");
            link.OnClientClick = "return false;";
        }

        private void EnableLink(LinkButton link)
        {
            link.Style.Clear();
            link.OnClientClick = "";
        }

        private IList<ShippingReviewView> GetStep3Data()
        {
            // The proper sorting was already done at the Select Documents screen
            return ReviewSelection.GetSelectedDocumentIds();
        }



        //todo improve
        private IList<ShippingReviewView> GenerateFinalReviewList()
        {
            var selectedDocuments = new List<ShippingReviewView>(SelectDocument.GetSelectedDocumentIds());

            int id = int.Parse(SelectDocument.SelectedShippingTemplateId);
            UpdateDuplicatesInList(selectedDocuments);
            
            // If not using a template
            if (id < 0)
            {
                return selectedDocuments; 
            }            

            // The documents have already been sorted, but views may need to be added
            // for missing documents
            EDocumentShippingTemplate templates = new EDocumentShippingTemplate();
            List<DocType> docTypesInTemplate = templates.GetShippingTemplateData(id);

            List<ShippingReviewView> sortedList = new List<ShippingReviewView>(selectedDocuments.Count);

            foreach (DocType docType in docTypesInTemplate)
            {
                // If a doc is missing, add a view for it 
                if (selectedDocuments.Where(p => p.DocType.ToLower() == docType.DocTypeName.ToLower() && p.Folder.ToLower() == docType.Folder.FolderNm.ToLower()).Count() == 0)
                {
                    sortedList.Add(new ShippingReviewView()
                    {
                        DocType = docType.DocTypeName,
                        Folder = docType.Folder.FolderNm,
                        Status = E_ShippingReviewEntryStatus.MISSING
                    });
                }
                else // Add any that match in the appropriate order.
                {
                    sortedList.AddRange(selectedDocuments.Where(d => d.DocType.ToLower() == docType.DocTypeName.ToLower() && d.Folder.ToLower() == docType.Folder.FolderNm.ToLower()));
                    selectedDocuments.RemoveAll(d => d.DocType.ToLower() == docType.DocTypeName.ToLower() && d.Folder.ToLower() == docType.Folder.FolderNm.ToLower());
                }
            }

            sortedList.AddRange(selectedDocuments);

            return sortedList;            
        }

        private void UpdateDuplicatesInList(IList<ShippingReviewView> items)
        {
          
            foreach (ShippingReviewView view in items)
            {
                if (items.Where(p => view.DocType == p.DocType).Count() > 1)
                {
                    view.Status = E_ShippingReviewEntryStatus.DUPLICATE;
                }
                else
                {
                    view.Status = E_ShippingReviewEntryStatus.OK;
                }
            }
          
        }



        #region IAutoLoadUserControl Members

        public void LoadData()
        {
            if (Page.IsPostBack)
            {
                return;
            }
            ShowStep1();
        }

        public void SaveData()
        {
        }

        #endregion
    }


}