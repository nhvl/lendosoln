﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditEDocBatch.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.EditEDocBatch" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="uc" TagName="DocTypePicker" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <style type="text/css">
        #tbPageLabel {
          width:60px;
          text-align:center;
        }
        html, body { height: 100%; overflow:hidden;}
        .placeholder {
            height:5px;
            background-color:maroon;
        }
        button {
          margin-left:2px;
          margin-right:2px;
        }
        button div {
            _margin-left: 10px; /*Ie 6 Hack */
        }

        button.op_button {
            height: 70px;
            width: 60px;
        }

        button.op_button_wide_2 {
            width: 90px;
        }

        button.op_button_wide_3 {
            width: 120px;
        }

        button.op_button_short {
            height: 20px;
        }

        button img.standard_img {
            width: 30px;
            height: 30px;
        }
        .paste-img {
            width: 20px;
        }

        .note-img {
            height: 58px;
            margin-bottom: -3px;
        }
        h1 {
            margin: 0;
        }

        .right_hr_border {
            border-right: 2px inset gray;
        }

        #AllTabFunctionality, #AllTabFunctionality2 {
            float: right;
            display:block;
        }



        ul.fields {
            margin: 0;
            margin-top: .9em;
            padding:0;
            list-style-type: none;
        }
        ul.fields li {
            clear: both;
            overflow:hidden;
            padding-bottom: 8px;
        }

        ul.fields li label {
            display: block;
            width: 120px;
            float: left;
        }
        ul.fields li input, ul.fields li select {
            display: block;
            float: left;
        }

        #tabs {
            min-width: 930px;
        }

        #AllTabSaveFunctionality {
            float: left;
            clear:none;
        }

        #ViewDocumentInfo, #ViewInternalNotes, #ModifyPageLayout {
            padding-top: 0 !important;

        }

        .clear {
            clear: both;
        }
        .button_container {
        }

        .tableCellHeight {
            height: 80px;
            vertical-align: bottom;
        }
        #editor {
            clear:both;
        }
        table td { font-weight: bold; }


        .Hidden {
		    display: none;
		}

        .pdf-field-note {

        }

        .pdf-field-note-style {
            padding: 0;
            background-color: #FFFFA8;
            border: 1px solid black;
        }

		.pdf-field-note textarea {
		    display: block;
		    margin : 0 auto;
		    clear: both;
		    margin-left: 5px;
		}



		.pdf-field-note .minimize-note {
		    float: right;
		    width: 20px;
		    height: 20px;
		    margin-bottom: 2px;
		    display:block;
		    margin-right: 10px;
		    margin-top: 3px;
		    padding: 0px;
		}

		.pdf-field-note .maximize-item, .pdf-field-note  .minimize-item {
		    width: 100%;
		    height: 100%;
        }

        .maximize-item span {
            display: block;
            float :left;
            margin-top: 2px;
            margin-left: 5px;
        }


		.pdf-field-note .maximize-note{
		    margin: 5px;
		    display: block;
		    width: 25px;
		    height: 25px;
		    padding: 0px;
		}
		.maximize-note image, .minimize-note image {
		    margin: 0 auto;
		}
		.highlight-field {
            background-color: Yellow;
            width: 120px;
            height: 20px;
            border: 1px solid black;
        }

        .pdf-field-thumbnail img {
           width: 100%;
           height: 100%;

        }

        #ViewInternalNotes, #ViewDocumentInfo, #ModifyPageLayout {
            height: 115px;
        }

        div.MaskedDiv
        {
	        position:absolute;
	        left:0px;
	        top:0px;
            height: 100%;
            z-index: 900;
            width: 100%;
            filter:alpha(opacity=1);
            background-color: Red;

        }

        div.StatusPanel
{
    border: solid 2px black;
    background-color: yellow;
    padding: 15px;
    text-align: center;
    position: absolute;
    font-weight: bold;
    z-index:999;
    width:180px;
    height:25px;
    left:30px;
    top:30px;
}
div.StatusPanel div
{
  font-weight:bold;
  font-size:16px;
}
        div#LQBPopupDiv {
		}
		div.BlockMask {
			z-index: 2750;
		}
    </style>
    <title>EDocs Editor</title>
</head>
<body bgcolor="gainsboro" onbeforeunload="return page_unload()" >
    <script type="text/javascript" src="../../inc/ModelessDlg.js"> </script>
    <script type="text/x-jquery-tmpl" id="noteTemplate">
        <div class="minimize-item">
            <button class="maximize-note">
                <img  src="../../images/edocs/maximize.png" alt="maximize" />
            </button>
        </div>
        <div class="maximize-item" >
            <textarea  class="pdfnotes"  rows="1" cols="1"  >${Text}</textarea>
            <span>Last updated: </span><span class="note_last_modified">${LastModifiedOn}</span>
            <button class="minimize-note" >
                <img src="../../images/edocs/minimize.png" alt="minimize" />
            </button>
        </div>
    </script>
    <script type="text/x-jquery-tmpl" id="noteThumbTemplate">
        <div>
            <img src="${src}" alt="" />
        </div>
    </script>
    <script type="text/javascript" >
        resizeTo(1024,864);
        var _isLoading = true;
        var _pdfEditor = null;
        var _isDirty = false;
        var _isFinishedLoading = false;

        var BatchEDocSet;

        function setDirtyBit() {
            if(!_isFinishedLoading) {
                return;
            }

            _isDirty = true;
            setDirtyBit_callback();
         }

         function resetDirty() {
            _isDirty = false;
            resetDirty_callback();
         }

         function saveOnExit() { return confirm("Do you want to save changes?"); }

         function page_unload() {
            if(_childWindow)
            {
                _childWindow.close();
            }

            if( _isDirty )
            {
                return UnloadMessage;
            }

            

         }

        function resizeTextArea() {
           var ta = $('textarea', this);
           var bIsHidden =$(this).data('__HIDDEN__');

           if (bIsHidden == null)
           {
              //bIsHidden = ta.is(':hidden'); // 5/19/2011 dd - This is an expensive call.
              // 5/19/2011 dd - Let assume it is not hidden when not set.
              bIsHidden = false;
           }
           if( ta.length == 0 || bIsHidden) {
            return;
           }

           var parentHeight = 0; //$(this).height();
           var parentWidth = 0; //$(this).width();

           var taPositionTop = 5;
           var pdfFieldInfo = null; // this cache does not work well $(this).data('pdfFieldData');
           if (pdfFieldInfo != null)
           {
            parentHeight = pdfFieldInfo.height;
            parentWidth = pdfFieldInfo.width;
           }
           else
           {
            parentHeight = $(this).height();
            parentWidth = $(this).width();
           }
           //var taStart = ta.offset

           ta.height(parentHeight - 35 )
           ta.width(parentWidth - 20);
        }

        function saveMe(bRefreshScreen) {
            _Save(bRefreshScreen);
        }
        function setDirtyBit_callback() {
            $('#btnSave').prop('disabled', false).children('img').attr('src', <%= AspxTools.JsString(ResolveUrl("~/images/edocs/save.png")) %>);
        }

        function resetDirty_callback() {
            $('#btnSave').prop('disabled', true).children('img').attr('src', <%= AspxTools.JsString(ResolveUrl("~/images/edocs/save-disable.png")) %>);
        }

        function f_arrayContains(array, element)
        {
            if(array==null)
                return false;
            for(i = 0; i<array.length; i++)
            {
                if(array[i]==element)
                    return true;
            }
        }

        function _Save(refresh) {
            if( _isFinishedLoading == false ) {
                alert( "Cannot save until all documents have been loaded.");
                return;
            }
            var data = {
                'Data' :  BatchEDocSet,
                'LoadedAnnotations' : _canEditAnnotations,
                'NewAnnotations' : [],
                'NewESignTags': []};

            $.each(_pdfEditor.GetPdfFields(), function(i,o){
                if(o.cssClass == 'pdf-field-highlight' || o.cssClass == 'pdf-field-note'  ) {
                    data.NewAnnotations.push(o);
                }
                else if(o.cssClass == "pdf-field-esign-signature" || o.cssClass == 'pdf-field-esign-initial' || o.cssClass == 'pdf-field-esign-date') {
                    data.NewESignTags.push(o);
                }
            });

            data.Data =  JSON.stringify(data.Data);
            data.LoadedAnnotations =  JSON.stringify(data.LoadedAnnotations);
            data.NewAnnotations =  JSON.stringify(data.NewAnnotations);
            data.NewESignTags = JSON.stringify(data.NewESignTags);

            //OPM 106903
            var UIStatus = null, pageMap =  getPageMap(_pdfEditor.GetCurrentPageNumber()), doc = pageMap.Doc;
            if(<%= AspxTools.JsBool(EnableDuplicateEDocProtectionChecking) %>) //OPM 115047 - Broker Setting
            {
                var checkDuplicateResults = gService.loanedit.call('CheckDuplicateProtectedDocsBulk', {'Data':data.Data, 'LoanId':<%= AspxTools.JsString(LoanID) %>});
                if(checkDuplicateResults.value && checkDuplicateResults.value['DuplicateProtectedDocList'])
                {
                    //Duplicates found, separated by docType
                    var docList = JSON.parse(checkDuplicateResults.value['DuplicateProtectedDocList']);

                    //All documents in this batch edit
                    var completeEditList= checkDuplicateResults.value['CompleteEditList'];

                    //DocTypeId matching docList elements
                    var docTypeIdList = JSON.parse(checkDuplicateResults.value['DuplicateProtectedDocTypeIdList']);

                    var docListOverrideToBlank = [];
                    var docListIgnoreStatus = [];
                    for( var i = 0; i < docList.length; i++)
                    {
                        //For each DocType, determine user's desired action for duplicates
                        showModal('/newlos/ElectronicDocs/DuplicateProtectedDialog.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %>, 
                                {'List':docList[i], 'DocTypeId':docTypeIdList[i], 'CompleteEditList':completeEditList, 'CurrentUIDocumentId':doc.DocumentId}, 
                                "dialogHeight: 300px; dialogWidth: 600px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No",
                                null, 
                                function(modalArgs){ 
                                        
                                    if(modalArgs.ToBlankStatus)
                                    {
                                        docListOverrideToBlank = docListOverrideToBlank.concat(modalArgs.ToBlankStatus);
                                    }
                                    if(modalArgs.IgnoreStatus)
                                    {
                                        docListIgnoreStatus = docListIgnoreStatus.concat(modalArgs.IgnoreStatus);
                                    }
                                    if(modalArgs.ToBlankStatus == null && modalArgs.IgnoreStatus == null)
                                    {
                                        //Modal closed without button; use "Cancel" behavior
                                        for(j = 0; j<docList[i].length; j++)
                                        {
                                            docListIgnoreStatus.push(docList[i][j].DocumentId);
                                        }
                                    }
                                    if(modalArgs.UIStatus != null)
                                    {
                                        UIStatus = modalArgs.UIStatus;
                                    }
                                },{ hideCloseButton: true }
                            );
                    }
                    for(i = 0; i<BatchEDocSet.length; i++)
                    {
                        var doc = BatchEDocSet[i];
                        if(f_arrayContains(docListOverrideToBlank, doc.DocumentId))
                        {
                            //Set to "Blank" status
                            doc.DocumentStatus = 0;
                        }
                        if(f_arrayContains(docListIgnoreStatus, doc.DocumentId))
                        {
                            //Set to an invalid enum value; save function set to ignore these
                            doc.DocumentStatus = 5;
                        }
                    }
                    data.Data = JSON.stringify(BatchEDocSet);
                }
            }
            var results = gService.loanedit.call('SaveBulk', data);

            if( results.error ) {
                alert(results.UserMessage);
            }
            else {
                if( results.value.SaveResults.length > 0 ) {
                    alert( results.value.SaveResults ) ;
                }

                if( results.value.VersionMismatches ) {
                    alert(results.value.VersionMismatches);
                }

                resetDirty();
                RefreshOpener();
                var versions = JSON.parse(results.value.NewVersions), count = BatchEDocSet.length;

                for( var i = 0; i < count; i++){
                    BatchEDocSet[i].Version = versions[i];
                }

                var statuses = JSON.parse(results.value.NewStatuses);

                for( var i = 0; i < count; i++){
                    BatchEDocSet[i].DocumentStatus = statuses[i];
                }

                if(UIStatus != null)
                {
                    //Force field update to reflect new status
                    OnPageChange();
                }
            }
        }


        function _ToggleInteractive(o, enable) {
            var $o = $(o);
            if( $o.hasClass('saved') && enable === true ) {
                return;
            }
            $o.toggleClass('disable-resize', !enable).toggleClass('disable-move', !enable).toggleClass('disable-selectable', !enable);
            $o.removeClass('ui-selected pdf-first-selected');
        }

        function RefreshOpener()
        {
            try
            {
                if( window.opener && !window.opener.closed ) {
                    var loc = window.opener.location;
                    window.opener.location = loc;
                }
            }
            catch(e){/*this fails if the DocsReceived list is closed, which we can't handle.*/}
        }

        var _dataKey = null;
        var _canEditAnnotations = null;
        var _uniqueKey = null;
        var _noteTemplate = null;
        var _noteThumbTemplate = null;
        var _childWindow = null;
        var _closeWindow = false;
        jQuery(function($) {

            _dataKey  = 'pdfFieldData';
            _canEditAnnotations = $('#<%= AspxTools.ClientId(CanEditAnnotations) %>').val() == 'True';
            _uniqueKey =  $('#<%= AspxTools.ClientId(UniqueKey) %>').val();
            _noteTemplate = $( "#noteTemplate" ).template();
            _noteThumbTemplate = $( "#noteThumbTemplate" ).template();


            $('#btnSave').click(function(){_Save(false);});
            $("#btnPrevious").click(function() { _pdfEditor.MovePrevPage(); });
            $("#btnNext").click(function() { _pdfEditor.MoveNextPage(); });
            $('input[type=text][id!="rejectionReeasonDialog"]').change(setDirtyBit);

            $('.removeAnnotationsBtn').on('click', function(){
                var list = _pdfEditor.GetSelectedPdfFields();
                if (list.length < 1) {
                    return;
                }
                setDirtyBit();
                $.each(list, function(idx, o) { _pdfEditor.RemovePdfField(o);});
            });

            $('#btnMinimize,#btnMaximize').on('click', function(){
                var selector = this.id === 'btnMinimize' ?  '.minimize-note:visible' : '.maximize-note:visible';
                _pdfEditor.GetFields().each(function(idx,o){
                    $(selector).click();
                });
            });

            $(document).keyup(function(e){

                var keyPressed = e.which;
                if( keyPressed !== 46 && keyPressed !== 8 ) {
                    return;
                }
                $.each(_pdfEditor.GetSelectedPdfFields(),function(idx,o){
                    var $this  = $(o);
                    if( $this.hasClass('saved')) {
                        return; //cannot delete its already been saved.
                    }
                });
            });

        });
        function _hideStatusMessage()
        {
                $('#divStatusPanel').hide();
        }
         function _displayStatusMessage(msg)
        {
            var top = $(document).height() / 2 - 50;
            var left = $(document).width() / 2 - 150;
            $('#divStatusPanel div').text(msg);
            $('#divStatusPanel').css({ 'top': top + 'px', 'left': left + 'px' }).show();
        }

        function loadRest(){
            callWebMethodAsync({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: 'EditEDocService.aspx?method=GetBatchPages',
                data: JSON.stringify({ DocIds: DocIds.toString() }),
                datatype: 'json',
                async : 'true' <%-- In the future I need top look into turning this off --%>
            }).then(
                function(msg) {
                    var pages = JSON.parse(msg.d.PagesJson);
                    _pdfEditor.InsertPages(pages, _pdfEditor.GetTotalPages() , 0);

                    BatchEDocSet = JSON.parse(msg.d.BatchEDocSet);

                    $.each(BatchEDocSet, function() {
                        $.each(this.InitialAnnotations, function() {
                            var oDiv = _pdfEditor.AddPdfField(this);
                            _onPdfFieldDrop(null, oDiv);
                            $(oDiv).data('__HIDDEN__', false); // Assume all visible
                        });

                        $.each(this.InitialESignTags, function() {
                            var oDiv = _pdfEditor.AddPdfField(this);
                            _onPdfFieldDrop(null, oDiv);
                            $(oDiv).data('__HIDDEN__', false); // Assume all visible
                        });
                    });

                    $.each(_pdfEditor.GetFields(), function() {resizeTextArea.call(this);});

                    _isFinishedLoading = true;
                    _hideStatusMessage();
                    ToggleTabs();
                },
                function(msg) { alert('There was an error processing your request. Please try again.'); }
            );
        }

        function UpdateDocType()
        {
            var pageMap =  getPageMap(_pdfEditor.GetCurrentPageNumber()), doc = pageMap.Doc, index = pageMap.Index;
           doc.DocTypeId = $('#<%=AspxTools.ClientId(m_docTypeList.ValueCtrl) %>').val();
           doc.DocTypeDescription = $('#<%=AspxTools.ClientId(m_docTypeList.DescriptionCtrl) %>').text();
           $('.overlay_' + index).text(doc.DocTypeDescription);
           setDirtyBit();
        }

        function IsESignTag(item) {
            var $item = $(item);
            return $item.hasClass('pdf-field-esign-signature') || $item.hasClass('pdf-field-esign-date') || $item.hasClass('pdf-field-esign-initial');
        }

        function ToggleTabs(currentPanel) {
            var currentPanel = currentPanel || $($('#tabs').find('.ui-tabs-active').find("a").attr("href"))[0];
            if(!currentPanel) {
                return;
            }

            var showNotes = currentPanel.id === '<%= AspxTools.ClientId(ViewInternalNotes) %>';
            var showESignTags = currentPanel.id === 'ViewESignTags';

            _pdfEditor.GetFields().each(function() {
                var $this = $(this);
                var isESignTag = IsESignTag(this);
                var show = isESignTag ? showESignTags : showNotes;
                $this.toggleClass('disable-selectable', !show);
                $this.data("__HIDDEN__", show ? false : true);
                var thumbData = $this.css('visibility', show ? '' : 'hidden').data('pdfThumbData');
                $this.removeClass('ui-selected pdf-first-selected');
                if( thumbData ) {
                    thumbData.Thumb.toggle(show);
                }
            });

            _pdfEditor.ResetSelectedPdfFields();
            _onPdfFieldSelectedStop(null, 0);
        }

        $(window).on("load", function() {
            _displayStatusMessage('Loading...');
            $('#<%= AspxTools.ClientId(m_publicDescription) %>').change(function(){
                getDocument(_pdfEditor.GetCurrentPageNumber()).Description = this.value;
            });
            $('#<%= AspxTools.ClientId(m_docDescription) %>').change(function(){
               getDocument(_pdfEditor.GetCurrentPageNumber()).InternalDescription = this.value;
            });
            $('#<%= AspxTools.ClientId(m_docStatusDescription) %>').change(function(){
               getDocument(_pdfEditor.GetCurrentPageNumber()).DocumentStatusReasonDescription = this.value;
            });

            $('#<%= AspxTools.ClientId(m_docStatus) %>').change(function(){
                var reason = null;

                var value = this.value;

                if( value == '3' ){
                    $('#rejectionReeasonDialog').val('');

                    $('#RejectedReasonDialog').dialog({
                        modal: true,
                        open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
                        closeOnEscape : false,
                        dialogClass: "LQBDialogBox",
                        buttons: {
                            "Cancel" : function() {
                                cancelChanges(this);
                            },
                            "OK": function() {
                                $( this ).dialog( "close" );
                                reason = $('#rejectionReeasonDialog').val();
                                askApply(value, reason);
                            }
                        }
                    });
                }
                else {
                    askApply(value, reason);
                }
            });

            function askApply(status, reason){
              $( "#dialog" ).dialog({
                    modal: true,
                     open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
                    closeOnEscape : false,
                    width: 500,
                    dialogClass: "LQBDialogBox",
                    buttons: {
                        "Cancel": function() {
                            cancelChanges(this);
                        },
                        "Apply to current document": function() {
                            var pageNumber = _pdfEditor.GetCurrentPageNumber();

                            getDocument(pageNumber).DocumentStatus = status;

                            if (reason !== null) {
                                getDocument(pageNumber).DocumentStatusReasonDescription = reason;

                                $('#<%= AspxTools.ClientId(m_docStatusDescription) %>').val(reason);
                            }

                            completeApplyingStatus(status, reason, this);
                        },
				        "Apply to all documents": function() {
				            for (var i = 0; i < BatchEDocSet.length; i++) {
				                BatchEDocSet[i].DocumentStatus = status;

				                if (reason !== null) {
                                    BatchEDocSet[i].DocumentStatusReasonDescription = reason;

                                    $('#<%= AspxTools.ClientId(m_docStatusDescription) %>').val(reason);
                                }
				            }

				            completeApplyingStatus(status, reason, this);
				        }
                    }});
            }

            function cancelChanges(modal) {
                $(modal).dialog( "close" );

                var docStatusDropdown = $('#<%= AspxTools.ClientId(m_docStatus) %>');

                docStatusDropdown.val(docStatusDropdown.data('previousValue'));

                var docStatusDescription = $('#<%= AspxTools.ClientId(m_docStatusDescription) %>');

                docStatusDescription.val(docStatusDescription.data('previousReason'));

                onStatusChange();
            }

            function completeApplyingStatus(status, reason, dialog) {
                $('#<%= AspxTools.ClientId(m_docStatus) %>').data('previousValue', status);

                $('#<%= AspxTools.ClientId(m_docStatusDescription) %>').data('previousReason', reason);

                $( dialog ).dialog( "close" );

                setDirtyBit();

                onStatusChange();
            }

            //Note: Docs are not loaded until call to loadRest().
            _pdfEditor = new LendersOffice.PdfEditor('editor', [], {
                AllowEdit : function() { return true; },
                GenerateFullPageSrc : GetPageUrl,
                OnPageStatusChanged : OnPageChange,
                OnError : function(evt, msg) { ForceQuit(msg);},
                OnInitialDropRenderField : _onInitialDropRenderField,
                OnPdfFieldSelectedStop : _onPdfFieldSelectedStop,
                OnRenderPdfFieldContent : _renderPdfFieldContent,
                OnPdfFieldDrop : function(e,t) { _onPdfFieldDrop(e,t); setDirtyBit(); },
                CanEditAnnotations : _canEditAnnotations,
                EmptyImgSrc : gVirtualRoot + '/images/pixel.gif',
                AddPagesWhileDisabled : true,
                GoToNewDocOnAddition : false,
                OnPdfFieldDrag : setDirtyBit,
                OnPageModified : setDirtyBit
            });
            _pdfEditor.Initialize(function(){
                $('.MaskedDiv').hide();
                _isLoading = false;
            });

            _onPdfFieldSelectedStop(null, 0);

            $('#tabs').tabs({
                active: 0,
                activate: function(event, ui) {
                    ToggleTabs(ui.newPanel[0]);
                },
                create: function(event, ui) {
                    ToggleTabs(ui.panel[0]);
                }
            });

            $('#AssignRecipientBtn').click(function() {
                var selectedField = _pdfEditor.GetSelectedPdfFields();
                if(selectedField.length == 1) {
                    OpenRecipientPicker(selectedField[0]);
                }
            });

            $(document).on('dblclick', '.pdf-field-esign-signature.pdf-pdf-field, .pdf-field-esign-initial.pdf-pdf-field, .pdf-field-esign-date.pdf-pdf-field', function() {
                $('#AssignRecipientBtn').click();
            });

            $(document).on('click', '.RolePickerOkBtn', function() {
                var rolePicker = $(this).closest('.RecipientPicker');
                var checked = rolePicker.find('input[type="radio"][name="recipient"]:checked');
                if(checked.length == 0) {
                    alert("Please select a role");
                    return;
                }

                var recipient = checked.val();

                var role = null;
                var borrower = null;
                recipientDescription = "";
                
                if(recipient == 'role') {
                    role = rolePicker.find('.AgentRole').val();
                    recipientDescription = rolePicker.find('.AgentRole option:selected').text();
                }
                else {
                    recipientDescription = recipient;
                    if(recipient == "Borrower") {
                        borrower = <%=AspxTools.JsString(E_BorrowerModeT.Borrower.ToString("D")) %>;
                    }
                    else {
                        borrower = <%=AspxTools.JsString(E_BorrowerModeT.Coborrower.ToString("D")) %>
                    }
                }

                LQBPopup.Return({borrower: borrower, role: role, recipientDescription: recipientDescription });
            });

            $(document).on('click', '.RolePickerCancelBtn', function() {
                LQBPopup.Return(null);
            });

            $(document).on('click', '.recipient', function() {
                var rolePicker = $(this).closest('.RecipientPicker');
                var checked = rolePicker.find('input[type="radio"][name="recipient"]:checked');
                rolePicker.find('.AgentRole').prop('disabled', checked.val() != 'role');
            });

            var filter = ['.esign-tag'];
            if( _canEditAnnotations  ) {
                filter.push('.annotation-field');
            }
            if( filter.length > 0 ) {
                $(filter.join(',')).draggable({revert:'invalid', helper:'clone', zIndex: 2700,
                    start : function() {
                        $('#editor').css('z-index', '-1' ); //needed for 6 and 7 so the draggable doesnt end in the back of the editor
                    },
                    stop : function() {
                        $('#editor').css('z-index', '' );
                    }
                });
             }

            resetDirty();
            window._onPdfFieldDrop = _onPdfFieldDrop;
            $(window).resize(); //trgger resize for popup issues =/
            loadRest();
        });

        function handleError(result) {
            var errMsg = 'Unable to process data. Please try again.';

            if (!!result.UserMessage){
                errMsg = result.UserMessage;
            }
            ForceQuit(errMsg);
        }
        function ForceQuit(msg) {
            resetDirty();
            alert(msg);
            //If the doc list is still open, refresh it. Otherwise, redirect us to the doc list.
            if( window.opener && !window.opener.closed ) {
                try
                {
                //Note that window.opener.location.reload() will not work, since for some reason it
                //causes this function to terminate immediately (even if it's in a try/catch)
                    window.opener.location.href = window.opener.location.href;
                }catch(e){}
                onClosePopup();
            }
            else
            {
                window.location =  "DocList.aspx?loanid="+ <%= AspxTools.JsString(LoanID) %>;
            }
        }


        function updateDocTypeHeader(){
            $('#DocTypeHeader').text($('#<%=AspxTools.ClientId(m_docTypeList.DescriptionCtrl) %>').text());
        }

        function GetPageUrl(o) {
            return <%= AspxTools.SafeUrl(Tools.GetEDocsPngLink()) %> + '&pngkey=' + encodeURIComponent(o.PngKey) + '&key=' + encodeURIComponent(o.Token)+'&pg=' + o.Page;
        }






        function getPageMap(page){

            if( typeof getPageMap.pageMap == "undefined" ) {
                getPageMap.pageMap = {};

                var pg = 0, docs = BatchEDocSet, docCount = docs.length, tpageMap = getPageMap.pageMap;
                for( var i = 0; i < docCount; i++){
                    var doc = docs[i], pageCount = doc.PageCount, startPage = pg +1;

                    for( var y = 0; y < pageCount; y++){
                        pg+= 1;
                        tpageMap[pg] = { Doc : doc, StartPage : startPage, Index : i };
                    }
                }
            }
            return getPageMap.pageMap[page];
        }

        function getPageInDocument(page){
            var details =  getPageMap(page);
            return page - details.StartPage;
        }

        function getDocument(page){
            return getPageMap(page).Doc;
        }

        function OnPageChange () {
            var index = _pdfEditor.GetCurrentPageNumber(), max =  _pdfEditor.GetTotalPages(), doc = getDocument(index);
                $("#tbPageLabel").val( index + " of " + max );
                $('#<%= AspxTools.ClientId(m_publicDescription) %>').val(doc.Description);
                $('#<%= AspxTools.ClientId(m_docDescription) %>').val(doc.InternalDescription);
                $('#<%= AspxTools.ClientId(m_docStatus) %>').val(doc.DocumentStatus).data('previousValue', doc.DocumentStatus);
                $('#<%= AspxTools.ClientId(m_docStatusDescription) %>').val(doc.DocumentStatusReasonDescription).data('previousReason', doc.DocumentStatusReasonDescription);
                $('#<%=AspxTools.ClientId(m_docTypeList.DescriptionCtrl) %>').text(doc.DocTypeDescription);
                $('#<%=AspxTools.ClientId(m_docTypeList.ValueCtrl) %>').val(doc.DocTypeId);
                onStatusChange();
        }

        //called when a field is drop onto the editor
        //this == div being dropped (element)
        function _onInitialDropRenderField() {
            if( $(this).hasClass('pdf-field-note') == false ) {
                return;
            }
            var now = new Date();
            var date = [ now.getMonth() +1, '/', now.getDate(), '/',  now.getFullYear() ].join('');


            var data = $(this).data(_dataKey);
            if (null == data) {
                data = {};
                $(this).data(_dataKey, data);
            }

            data.NoteStateType = 'maximized';
            data.width = data.height = 170;
            var o = {'Text' : '', 'LastModifiedOn' : date, 'NoteStateType' : 'maximized', 'width' : data.width, 'height' :data.height};
            _renderNote.call(this, o);
        }

        //Based on state sets the image
        //thumb == jquery object
        function _setThumbNote(thumb, state) {
            thumb.children('img').attr('src', _getThumbNoteImg(state));
        }

        //based on state returns the img url for the thumbnail
        function _getThumbNoteImg(state) {
            return state == 'minimized' ? '../../images/edocs/note-thumb-min.png' :  '../../images/edocs/note-thumb-max.png';
        }

        //renders the note object expects data to be set with the needed info
        function _renderNote(o) {
            $(this).empty().attr({'min-resize-width': '80', 'min-resize-height' : '80'});
            $.tmpl(_noteTemplate,o).appendTo(this);
            $(this).addClass('pdf-field-note-style');
            $('.minimize-note, .maximize-note', this).click(function(e){
                var maximize = $(this).hasClass('maximize-note');
                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();

                var parent = $(this).parent().parent().get(0);
                var data = $(parent).data(_dataKey);

                $('.maximize-item, .minimize-item', parent).toggle();

                if( maximize ) {
                    $(parent).removeClass('disable-resize').width(data.width).height(data.height);
                    data.NoteStateType = 'maximized';
                }
                else {
                    $(parent).addClass('disable-resize').width(35).height(35);
                    data.NoteStateType = 'minimized';
                }

                var thumbData = $(parent).data('pdfThumbData');
                _setThumbNote(thumbData.Thumb,  data.NoteStateType  );
                _pdfEditor.EnforceBoundaries(parent);
            });

            $(this).resize(resizeTextArea);

            $('textarea', this).blur(function(){
                var parent = $(this).parent().parent().get(0);
                $(parent).data(_dataKey).Text = $(this).val();

            }).change(setDirtyBit);

            if( o.NoteStateType == 'maximized'){
                $('.minimize-item', this).hide();
                $(this).width(o.width).height(o.height);
            }
            else {
                $('.maximize-item', this).hide();
                $(this).addClass('disable-resize').width(35).height(35);
            }

            if( false === _canEditAnnotations ) {
                $('textarea', this).prop({'readonly': true, 'disabled' : true});
            }

            $(this).triggerHandler('resize');
        }

        //updates the remove notation  status based on how many pdf fields are selected
        function _onPdfFieldSelectedStop(e, count) {
            var currentPanel = $('.ui-tabs-panel:not(.ui-tabs-hide)', '#tabs')[0];
            var showInternalNotes = currentPanel ? currentPanel.id === 'ViewInternalNotes' : true;;
            
            var removeAnnotationsBtns = $('.removeAnnotationsBtn');
            var setRecipientButton = $('#AssignRecipientBtn');
            if(count == 1) {
                setRecipientButton.prop('disabled', false);
                $('#recipientImg').prop('src', <%=AspxTools.JsString(ResolveUrl("~/images/contacts.png"))%>)
            }
            else {
                setRecipientButton.prop('disabled', true);
                $('#recipientImg').prop('src', <%=AspxTools.JsString(ResolveUrl("~/images/contacts_disabled.png"))%>)
            }

            if((showInternalNotes && !_canEditAnnotations) || count <= 0) {
                removeAnnotationsBtns.prop('disabled', true).children('img').attr('src', <%=AspxTools.JsString(ResolveUrl("~/images/edocs/remove_notation_disable.png"))%>);
            }
            else {
                removeAnnotationsBtns.prop('disabled', false).children('img').attr('src', <%=AspxTools.JsString(ResolveUrl("~/images/edocs/remove_notation.png"))%>);
            }
        }

        //called when a pdf field is rendered ( initial - on load  on drop calls diff method)
        function _renderPdfFieldContent(o) {
            if( o.cssClass == 'pdf-field-note' ) {
                _renderNote.call(this, o);
                if( false === _canEditAnnotations ) {
                    $(this).addClass('master-disable-resize disable-move');
                }
            } else if( o.cssClass == 'pdf-field-highlight' ) {
                $(this).addClass('highlight-field');
                if( false === _canEditAnnotations ) {
                    $(this).addClass('master-disable-resize disable-move');
                }
            } else if( o.cssClass == 'pdf-field-esign-signature' || o.cssClass == 'pdf-field-esign-initial' || o.cssClass == 'pdf-field-esign-date') {
                $(this).addClass(o.cssClass);

                var description = o.RecipientDescription;
                if(o.cssClass == 'pdf-field-esign-signature') {
                    description += " Signature";
                }
                else if(o.cssClass == 'pdf-field-esign-initial') {
                    description += " Initials";
                }
                else if(o.cssClass == 'pdf-field-esign-date') {
                    description += " Sig Date";
                }

                var tooltipDescription = description;
                
                var pdfFieldTextContainer = $('<div>').addClass('pdf-field-content-text-container').prop('title', tooltipDescription);
                var pdfFieldText = $('<div>').addClass('pdf-field-content-text').text(description);
                pdfFieldTextContainer.append(pdfFieldText);

                return pdfFieldTextContainer;
            }
        }

        function _onPdfFieldDrop(evt, o) {
            var data = $(o).data(_dataKey);
            var content = $('<div class="highlight-field highlight-field-thumb"></div>');

            if( data.cssClass == 'pdf-field-note' ) {
                var tmplData = { src :  _getThumbNoteImg(data.NoteStateType) };
                content = $.tmpl(_noteThumbTemplate,tmplData);
            }
            else if(evt && (data.cssClass == 'pdf-field-esign-signature' || data.cssClass == 'pdf-field-esign-initial' || data.cssClass == 'pdf-field-esign-date')) {
                // Only the initial drop will call this with an evt object.
                OpenRecipientPicker(o);
            }

            _pdfEditor.AddDynamicThumbnail(o, content);
        }

        function OpenRecipientPicker(tag) {
            var picker = $('#RecipientPickerContainer').clone();
            picker.find('.RolePickerCancelBtn').show();
            var $tag = $(tag);
            
            var oldData = $tag.data(_dataKey);
            if(oldData) {
                var oldBorrower = oldData.AssociatedBorrower;
                var oldRole = oldData.AssociatedRole;
                if(oldBorrower != null) {
                    picker.find('.recipient[value="' + oldData.RecipientDescription + '"]').attr('checked', true).prop('checked', true);
                    picker.find('.AgentRole').attr('disabled', true).prop('disabled', true);
                }
                else if(oldRole != null) {
                    // Borrower isn't set, set the role instead.
                    picker.find('.recipient[value="role"]').attr('checked', true).prop('checked', true);
                    picker.find('.AgentRole').find('option[value="' + oldRole + '"]').attr('selected', true).prop('selected', true);
                    picker.find('.AgentRole').attr('disabled', false).prop('disabled', false);
                }
                else {
                    // Nothing is set. Hide the cancel button.
                    picker.find('.RolePickerCancelBtn').hide();
                }
            }

            var targetOffset = null;
            var marginLeft = null;
            var marginTop = null;
            var isTagInViewport = _pdfEditor.IsInEditorViewport(tag);
            if(isTagInViewport) {
                var targetOffsetLeft = $tag.offset().left;
                var targetOffsetTop = $tag.offset().top - 155;
                targetOffset = {top: targetOffsetTop, left: targetOffsetLeft};
            }
            else {
                marginLeft = 0;
                marginTop = 0;
            }

            LQBPopup.ShowElement(picker, {
                width: 300,
                height: 150,
                hideCloseButton: true,
                elementClasses: 'FullWidthHeight, z2800',
                dialogClasses: 'z2800',
                targetOffset: targetOffset,
                marginLeft: marginLeft,
                marginTop: marginTop,
                onReturn: function(recipient) {
                    if(recipient == null) {
                        return;
                    }

                    var data = $tag.data(_dataKey);
                    if (null == data) {
                        data = {};
                        $tag.data(_dataKey, data);
                    }

                    data.AssociatedRole = recipient.role;
                    data.AssociatedBorrower = recipient.borrower;
                    data.RecipientDescription = recipient.recipientDescription;

                    var description = recipient.recipientDescription;
                    if($tag.hasClass('pdf-field-esign-signature')) {
                        description += " Signature";
                    }
                    else if($tag.hasClass('pdf-field-esign-initial')) {
                        description += " Initials";
                    }
                    else {
                        description += " Sig Date";
                    }

                    var tooltipDescription = description;

                    $tag.find('.pdf-field-content').attr('title', tooltipDescription);
                    $tag.find('.pdf-field-content-text').text(description);
                    setDirtyBit();
                }
            });
        }

        function onStatusChange()
        {
            var status = $('#<%= AspxTools.ClientId(m_docStatus)%>').val();
             if( status == '3')
             {
                $('#pnlStatusDescription').css('display', '');
             }
             else
             {
                $('#pnlStatusDescription').css('display', 'none');
             }
        }
    </script>
    <form id="form1" runat="server">
    <asp:HiddenField runat="server" ID="Version" />
    <asp:HiddenField runat="server" id="UniqueKey" />
    <asp:HiddenField runat="server" ID="CanEditAnnotations" />
    <asp:HiddenField runat="server" id="CanEditEdocs" />

    <h1 class="MainRightHeader">
      Edit Document - <span id="DocTypeHeader" ></span>
    </h1>

    <!--[if IE 6]>
    <div style="background-color:yellow; color:     black; font-weight: bold; border: 1px solid black;padding:5px">
        We have detected you are currently using Internet Explorer 6.  For the best compatibility with LendingQB please upgrade to a newer version of Internet Explorer
    </div>
    <![endif]-->

    <div id="RecipientPickerContainer" class="DisplayNone">
        <div class="MainRightHeader">
            Select a Signer Role:
        </div>
        <div class="RecipientPicker Padding5">
            <ul class="UndecoratedList align-left Padding5">
                <li>
                    <label>
                        <input type="radio" class="recipient" name="recipient" value="Borrower" />Borrower
                    </label>
                </li>
                <li>
                    <label>
                        <input type="radio" class="recipient" name="recipient" value="Coborrower" />Coborrower
                    </label>
                </li>
                <li>
                    <label>
                        <input type="radio" class="recipient" name="recipient" value="role" />Role:
                    </label>
                    <asp:DropDownList Enabled="false" NotEditable="true" runat="server" ID="AgentRole" class="AgentRole"></asp:DropDownList>
                </li>
            </ul>
            <br />
            <input type="button" id="RolePickerOkBtn" class="RolePickerOkBtn" value="OK" />
            <input type="button" id="RolePickerCancelBtn" class="RolePickerCancelBtn" value="Cancel" />
        </div>
    </div>
    <div id="tabs">
    <div id="MaskedDiv" class ="MaskedDiv"></div>
        <ul>
            <li runat="server" id="InternalNoteTab" class="tab"><a href="#<%= AspxTools.ClientId(ViewInternalNotes) %>" >View Internal Notes</a></li>
            <li id="ViewDocumentInfoTab" class="tab"><a href="#ViewDocumentInfo">View Document Info</a></li>
            <li runat="server" id="ESignTagsTab" class="tab"><a href="#ViewESignTags">E-Sign Tags</a></li>
        </ul>
        <hr />
        <table id="AllTabSaveFunctionality" class="button_container"  >
            <tr>
                <td class="tableCellHeight" >
                    <button class="op_button" id="btnSave" type="button">
                        <img id="Img1" src="~/images/edocs/save.png" class="standard_img" runat="server" />
                        <div>
                            Save
                        </div>
                    </button>
                </td>
            </tr>
        </table>
        <table id="AllTabFunctionality" class="button_container">
            <tbody>
                <tr>
                    <td class="tableCellHeight" align="right">

                    </td>
                </tr>
                                <tr>
                    <td  valign="baseline">
                        <button id="btnPrevious" class="op_button_arrow" type="button" >
                         <img id="Img7" src="~/images/edocs/previous.png" runat="server" />
                        </button>
                        <input type="text" id="tbPageLabel" value="" readonly="readonly" type="button" />
                        <button id="btnNext"  class="op_button_arrow" type="button">
                            <img id="Img6" src="~/images/edocs/next.png" runat="server"  />
                        </button>
                    </td>
                </tr>
            </tbody>
        </table>


        <div id="ViewInternalNotes"  runat="server"  class="ui-tabs-hide"  >
            <table class="button_container" style="float: left;"  cellpadding="0" cellspacing="0" >
                <tbody>
                    <tr>
                        <td class="tableCellHeight"  valign="bottom" style="padding:0; margin:0" >
                                       <button id="btnMaximize" class="op_button op_button_wide_3 op_button_short" type="button">
                                <img id="imgMax" src="~/images/edocs/maximize.png" runat="server" />
                                    Maximize all notes
                            </button><br />
                            <button id="btnMinimize"  class="op_button op_button_wide_3 op_button_short"  style="margin-bottom:-6px;" type="button">
                                <img id="imgMin"  src="~/images/edocs/minimize.png" runat="server"/>
                                    Minimize all notes
                            </button></td>
                         <td   class="tableCellHeight" valign="bottom">
                            <button id="btnRemoveAnnotations" class="removeAnnotationsBtn op_button op_button_wide_3 " style="margin-bottom:-3px;" type="button" disabled="disabled">
                                <img id="Img4" src="~/images/edocs/remove_notation_disable.png" class="standard_img" runat="server" />
                                <div>
                                    Remove Notation(s)</div>
                            </button>
                        </td>
                    </tr>

                </tbody>
            </table>
            <table class="button_container" runat="server" id="DragAndDropTable" style="float: left;">
                <tr>
                       <td colspan="3" valign="bottom">
                            Drag and drop notations onto the document:
                        </td>
                </tr>
                                    <tr >

                        <td valign="bottom" align="center" >
                            <div class="pdf-field-template annotation-field pdf-field-highlight highlight-field" >
                            </div>
                        </td>
                        <td valign="bottom" align="center" >
                           <div class="pdf-field-template annotation-field pdf-field-note" >
                                <asp:Image ID="Image1" CssClass="note-img" runat="server" ImageUrl="~/images/edocs/note-field-drag.png" />
                            </div>
                        </td>
                        <td style="width:10px;">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            Highlight
                        </td>
                        <td align="center">
                            Note
                        </td>
                    </tr>
            </table>
                   <div class="clear"></div>
        </div>
        <div id="ViewESignTags" runat="server" class="ui-tabs-hide">
            <table class="button_container" style="float: left;">
                <tbody>
                    <tr>
                        <td valign="bottom">
                            <button id="removeTagsBtn" class="removeAnnotationsBtn op_button op_button_wide_3" type="button" disabled="disabled">
                                <img id="Img5" src="~/images/edocs/remove_notation_disable.png" class="standard_img" runat="server" />
                                <div>
                                    Remove Tag(s)</div>
                            </button>
                        </td>
                        <td valign="bottom">
                            <button type="button" id="AssignRecipientBtn" class="op_button_wide_3" style="height: 70px; text-align: center; vertical-align: middle;">
                                <div>
                                    <img id="recipientImg" src="~/images/contacts.png" class="standard_img" runat="server" style="width: 20px; height: 20px;" />
                                </div>
                                <div>Assign Role</div>
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="button_container" runat="server" id="Table1" style="float: left;">
                <tr>
                    <td>
                        <div>
                        Drag and drop onto the document:
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="bottom">
                        <div class="DragAndDropContainer">
                            <div class="pdf-field-template pdf-field-esign-signature esign-tag" style="width:200px; height:20px;" >
                                <div class="pdf-field-content pdf-field-content-text-container">
                                    <div class="pdf-field-content-text">Signature</div>
                                </div>
                            </div>
                            Signature
                        </div>
                    </td>
                    <td valign="bottom"> 
                        <div class="DragAndDropContainer">
                            <div class="pdf-field-template pdf-field-esign-initial esign-tag" style="width:130px; height:15px;" >
                                <div class="pdf-field-content pdf-field-content-text-container">
                                    <div class="pdf-field-content-text">Initials</div>
                                </div>
                            </div>
                            Initial
                        </div>
                    </td>
                    <td valign="bottom">
                        <div class="DragAndDropContainer">
                            <div class="pdf-field-template pdf-field-esign-date esign-tag" style="width:130px; height:15px;" >
                                <div class="pdf-field-content pdf-field-content-text-container">
                                    <div class="pdf-field-content-text">Sig Date</div>
                                </div>
                            </div>
                            Signed Date
                        </div>
                    </td>
                </tr>
            </table>
            <div class="clear"></div>
        </div>
        <div id="ViewDocumentInfo" class="ui-tabs-hide">
            <table class="button_container">
                <tbody>
                    <tr>
                        <td class="tableCellHeight">
                            <ul class="fields">
                                <li>
                                    <ml:EncodedLabel ID="Label1" AssociatedControlID="m_docTypeList" runat="server">Type:</ml:EncodedLabel>
                                    <uc:DocTypePicker ID="m_docTypeList" OnChange="setDirtyBit();" runat="server"></uc:DocTypePicker>
                                </li>
                                <li>
                                    <ml:EncodedLabel ID="Label2" AssociatedControlID="m_publicDescription" runat="server">Description:</ml:EncodedLabel>
                                    <asp:TextBox ID="m_publicDescription" runat="server" Width="200px" /></li>
                                <li>
                                    <ml:EncodedLabel ID="Label3" AssociatedControlID="m_docDescription" runat="server">Internal Comments:</ml:EncodedLabel>
                                    <asp:TextBox ID="m_docDescription" runat="server" Width="200px" /></li>

                               <li>
                                    <ml:EncodedLabel ID="Label4" AssociatedControlID="m_docStatus" runat="server">Status:</ml:EncodedLabel>
                                    <asp:DropDownList ID="m_docStatus" runat="server"  Width="200px"></asp:DropDownList>
                                    <span id="pnlStatusDescription" style="text-align:left">
                                        <ml:EncodedLabel ID="Label5" AssociatedControlID="m_docStatusDescription" runat="server" Width="90px">&nbsp;&nbsp;&nbsp;&nbsp;Rejected for:</ml:EncodedLabel>
                                        <asp:TextBox ID="m_docStatusDescription" runat="server" Width="200px"/>
                                    </span>
                              </li>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>


        <div class="clear"></div>
    </div>
    <div id='debugConsole'>
        <div id='debugConsoleMsg'>
        </div>
    </div>
    <div id="divStatusPanel" class="StatusPanel" style="display: none">
        <div>
        </div>
    </div>
    <div id="editor" style="z-index: 0; position: relative;">
    </div>
    <uc1:cmodaldlg id="Cmodaldlg1" runat="server" />
    <div id="dialog" title="Status Change" style="display: none">
        <p>
            Apply status to current document only or all documents in the batch?</p>
    </div>
    <div id="RejectedReasonDialog" title="Rejection Reason" style="display: none">
        <p>
            Why are you rejecting this document?
            <input type="text" id="rejectionReeasonDialog" style="width: 200px" />
        </p>
    </div>
    </form>
</body>
</html>
