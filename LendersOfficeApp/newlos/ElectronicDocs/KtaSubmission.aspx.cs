﻿#region Generated code
namespace LendersOfficeApp.newlos.ElectronicDocs
#endregion
{
    using System;
    using LendersOffice.Common;
    using LendersOffice.Constants;

    /// <summary>
    /// Provides a means for submitting to KTA.
    /// </summary>
    public partial class KtaSubmission : BasePage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Form.Action = ConstStage.KtaApiUrl;
        }
    }
}
