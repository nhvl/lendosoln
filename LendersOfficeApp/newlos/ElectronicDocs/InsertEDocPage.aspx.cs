﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DataAccess;
using EDocs;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class InsertEDocPage : LendersOffice.Common.BaseServicePage
    {
        protected Guid DocId
        {
            get
            {
                return RequestHelper.GetGuid("DocId", Guid.Empty);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            EnableJqueryMigrate = false;
            //Register the edoc service so we can ask it for pdf page information.
            this.RegisterService("loanedit", "/newlos/ElectronicDocs/EditEDocService.aspx");
            this.RegisterJsScript("json.js");
            this.RegisterJsScript("jquery.tablesorter.min.js");
            EDocumentRepository repository = EDocumentRepository.GetUserRepository();
            IList<EDocument> docList = repository.GetDocumentsByLoanId(RequestHelper.LoanID);
            m_warningMsg.Visible = docList.Count(p => p.ImageStatus != E_ImageStatus.HasCachedImages) > 0;
            m_eDocsList.DataSource = docList.Where(p => p.DocumentId != DocId).OrderBy(p => p.DocTypeName);
            m_eDocsList.DataBind();
            m_eDocsList.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        protected HtmlImage GetImageForDocList(bool isESigned)
        {
            if (isESigned)
            {
                return new HtmlImage()
                {
                    Src = Tools.VRoot + "/images/edocs/SigIcon.png",
                    Alt = "Signed"
                };
            }

            return null;
        }
    }
}
