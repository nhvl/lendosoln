﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using System.Web.UI.HtmlControls;
using EDocs;
using System.Drawing;
using LendersOffice.PdfLayout;
using LendersOffice.ObjLib.Security;
using LendersOffice.ObjLib.ConsumerPortal;
using CommonProjectLib.Common.Lib;
using LendersOffice.Security;
using LendersOffice.Constants;
using MeridianLink.CommonControls;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class DocRequests : LendersOfficeApp.newlos.BaseLoanUserControl, IAutoLoadUserControl
    {
        private CPageData dataloan;
        private bool NamesMismatchOnTable = false;
        private Dictionary<string, long> m_userIdLookup;
        private Dictionary<long, string> m_reverseUserIdLookup; 
        private void AcceptRequest()
        {
            ConsumerActionItem item = new ConsumerActionItem(Convert.ToInt64(m_requestId.Value));
            if (!string.IsNullOrEmpty(m_signatureData.Value) && m_signatureData.Value != "[]")
            {
                List<PdfField> signatureFields = ObsoleteSerializationHelper.JsonDeserialize<List<PdfField>>(m_signatureData.Value);
                EDocumentViewer.SignPdf(BrokerUser,item.PdfFileDbKey, signatureFields);
            }

            item.AcceptConsumerResponse();
            item.Save();
            m_userMsg.Text = "Document added to eDocs system";
        }

        private void CancelRequest()
        {
            ConsumerActionItem item = new ConsumerActionItem(Convert.ToInt64(m_requestId.Value));
            item.CancelRequest();
            item.Save();
            m_userMsg.Text = "Document request deleted";
        }

        private void RejectRequest()
        {
            ConsumerActionItem item = new ConsumerActionItem(Convert.ToInt64(m_requestId.Value));
            item.RejectRequest();
            item.Save();
            m_userMsg.Text = "Document request deleted";
        }

        private void ResendRequest(CPageData dataLoan)
        {
            BranchDB db = new BranchDB(dataloan.sBranchId, BrokerUser.BrokerId);
            db.Retrieve();
            if (!db.ConsumerPortalId.HasValue && dataloan.BrokerDB.IsEnableNewConsumerPortal)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"Error: This loan's branch does not belong to any consumer portal.\")", true);
                return;
            }
            EDocumentNotifier.SetupAccountsAndSendEmails(new ConsumerActionItem(Convert.ToInt64(m_requestId.Value)));
            m_userMsg.Text = "Document request sent";
        }

        private IEnumerable<int> GetRequestIdsForOperationFromUserSelection(bool isSending,
            IEnumerable<int> selectedRequestIds,
            out bool selectedRequestsAreBeingProcessed)
        {
            selectedRequestsAreBeingProcessed = false;

            HashSet<int> currentRequestIds = null;
            if (isSending)
            {
                string status = null;
                currentRequestIds = new HashSet<int>(this.PendingDocumentRequests
                                                        .Where(r => this.GetRequestStatus(r, out status) != PendingDocumentRequestStatus.Suspended)
                                                        .Select(r => r.Id));
            }
            else
                currentRequestIds = new HashSet<int>(this.PendingDocumentRequests.Select(r => r.Id));

            if (!currentRequestIds.Any())
            {
                return new List<int>();
            }

            // We want to make sure that the selected request ids have not already 
            // been processed.
            var requestIdsForOperation = selectedRequestIds
                .Where(id => currentRequestIds.Contains(id));

            // If the current time is past the queue send time, we need to make
            // sure that the user isn't operating on requests that are available
            // to be processed by the continuous service.
            if (DateTime.Now.CompareTo(this.PendingDocumentRequestQueueSendTime.Value) > 0)
            {
                string statusExplanation = null;

                // If the request status is ready, it may be getting processed 
                // by the queue.
                var requestIdsThatCouldBeProcessing = this.PendingDocumentRequests
                    .Where(r => this.GetRequestStatus(r, out statusExplanation) == PendingDocumentRequestStatus.Ready)
                    .Select(r => r.Id);

                var excludedIds = new HashSet<int>(requestIdsThatCouldBeProcessing);

                selectedRequestsAreBeingProcessed = excludedIds.Any();

                requestIdsForOperation = requestIdsForOperation
                    .Where(id => !excludedIds.Contains(id));
            }

            return requestIdsForOperation;
        }

        private void DeletePendingRequests()
        {
            var requestIdsRaw = m_requestId.Value.Split(
                new char[] { ',' },
                StringSplitOptions.RemoveEmptyEntries);

            var userSelectedRequestIds = requestIdsRaw.Select(id => int.Parse(id));

            bool selectedRequestsAreBeingProcessed = false;

            var requestIdsForOperation = this.GetRequestIdsForOperationFromUserSelection(false,
                userSelectedRequestIds,
                out selectedRequestsAreBeingProcessed);

            if (requestIdsForOperation.Any())
            {
                // When deleting the request before it is sent, we want to delete
                // the file from FileDB.
                PendingDocumentRequestHandler.DeleteRequestsById(
                    true,
                    requestIdsForOperation,
                    PrincipalFactory.CurrentPrincipal.BrokerId);

                // If we modify the pending document requests, they need to be
                // re-loaded before displaying the UI.
                this.cachedPendingDocumentRequests = null;
            }

            if (selectedRequestsAreBeingProcessed)
            {
                m_userMsg.Text = "The selected requests are in the process of being sent. " +
                    "Please refresh the page in a few minutes and try again.";
            }
        }

        private void SendPendingRequests()
        {
            var requestIdsRaw = m_requestId.Value.Split(
                new char[] { ',' },
                StringSplitOptions.RemoveEmptyEntries);

            var userSelectedRequestIds = requestIdsRaw.Select(id => int.Parse(id));

            bool selectedRequestsAreBeingProcessed = false;

            var requestIdsForOperation = this.GetRequestIdsForOperationFromUserSelection(true,
                userSelectedRequestIds,
                out selectedRequestsAreBeingProcessed);

            if (requestIdsForOperation.Any())
            {
                PendingDocumentRequestHandler.SendRequestsById(
                    requestIdsForOperation,
                    PrincipalFactory.CurrentPrincipal.BrokerId);

                // If we modify the pending document requests, they need to be
                // re-loaded before displaying the UI.
                this.cachedPendingDocumentRequests = null;
            }

            if (selectedRequestsAreBeingProcessed)
            {
                m_userMsg.Text = "The selected requests are in the process of being sent. " +
                    "Please refresh the page in a few minutes and try again.";
            }
        }

        protected void Documents_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            ReadOnlyConsumerActionItem actionItem = (ReadOnlyConsumerActionItem)args.Item.DataItem;

            HtmlAnchor ViewLink = (HtmlAnchor)args.Item.FindControl("ViewLink");
            HtmlAnchor ReviewLink = (HtmlAnchor)args.Item.FindControl("ReviewLink");
            HtmlAnchor CancelLink = (HtmlAnchor)args.Item.FindControl("CancelLink");
            HtmlAnchor ResendLink = (HtmlAnchor)args.Item.FindControl("ResendLink");

            var ViewLiteral = (EncodedLiteral)args.Item.FindControl("ViewLiteral");
            var FolderDescription = (EncodedLiteral)args.Item.FindControl("FolderDescription");
            var DocumentTypeDescription = (EncodedLiteral)args.Item.FindControl("DocumentTypeDescription");
            EncodedLabel Application = (EncodedLabel)args.Item.FindControl("Application");
            var RequestDescription = (EncodedLiteral)args.Item.FindControl("RequestDescription");
            var ConsumerCompletedDate = (EncodedLiteral)args.Item.FindControl("ConsumerCompletedDate");
            var DividerLiteral = (EncodedLiteral)args.Item.FindControl("DividerLiteral");

            HtmlTableRow tr = (HtmlTableRow)args.Item.FindControl("row");

            CAppData appData = dataloan.GetAppData(actionItem.AppId);
            if ((actionItem.IsApplicableToBorrower && appData.aBNm != actionItem.BorrowerName) ||
                (actionItem.IsApplicableToCoBorrower && appData.aCNm != actionItem.CoborrowerName))
            {
                Application.ForeColor = Color.Red;
                NamesMismatchOnTable = true;
            }

            FolderDescription.Text = actionItem.FolderDescription;
            DocumentTypeDescription.Text = actionItem.DocumentTypeDescription;
            Application.Text = actionItem.BorrowerName;
            if (!string.IsNullOrEmpty(actionItem.CoborrowerName))
            {
                Application.Text += " & " + actionItem.CoborrowerName;
            }

            RequestDescription.Text = actionItem.RequestDescription;
            ConsumerCompletedDate.Text = actionItem.ConsumerCompletedDate.ToString();

            ViewLink.HRef = "javascript:view('" + AspxTools.JsStringUnquoted(actionItem.Id.ToString()) + "');";
            CancelLink.HRef = "javascript:cancelRequest('" + AspxTools.JsStringUnquoted(actionItem.Id.ToString()) + "');";
            ReviewLink.Attributes.Add("onclick", "return reviewRequest('" + AspxTools.JsStringUnquoted(actionItem.Id.ToString()) + "');");
            ResendLink.HRef = "javascript:resendRequest('" + AspxTools.JsStringUnquoted(actionItem.Id.ToString()) + "');";
            ReviewLink.HRef = "#";
            if (actionItem.ResponseStatus == E_ConsumerResponseStatusT.WaitingForConsumer)
            {
                ViewLink.Visible = false;
                ReviewLink.Visible = false;
            }
            else
            {
                ViewLiteral.Visible = false;
                CancelLink.Visible = false;
                ResendLink.Visible = false;
                DividerLiteral.Visible = false;
            }

        }

        protected void NoAccountsConsumersRepeaters_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }

            Tuple<string, string> userInfo = (Tuple<string, string>)args.Item.DataItem;

            EncodedLiteral name = (EncodedLiteral)args.Item.FindControl("BorrowerName");
            EncodedLiteral email = (EncodedLiteral)args.Item.FindControl("Email");
            HtmlAnchor anchor = (HtmlAnchor)args.Item.FindControl("CreateLink");

            name.Text = userInfo.Item2;
            email.Text = userInfo.Item1;
            anchor.Attributes.Add("data-email", userInfo.Item1);
        }

        protected void ConsumerPermissionRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }

            Tuple<ConsumerPortalUser,LinkedLoanApp> userInfo = (Tuple<ConsumerPortalUser,LinkedLoanApp>)args.Item.DataItem;

            EncodedLiteral email = (EncodedLiteral)args.Item.FindControl("Email");
            EncodedLiteral lastLogin = (EncodedLiteral)args.Item.FindControl("LastLogin");
            HtmlAnchor tempPasswordLink = (HtmlAnchor)args.Item.FindControl("TempPasswordLink");

            email.Text = userInfo.Item1.Email;

            if (userInfo.Item1.LastLoginD.HasValue)
            {
                lastLogin.Text = Tools.GetDateTimeDescription(userInfo.Item1.LastLoginD.Value);
            }
            else
            {
                lastLogin.Text = "N/A";
            }

            tempPasswordLink.Attributes.Add("data-id", m_reverseUserIdLookup[userInfo.Item1.Id]);
        }

        protected void PendingDocumentRequestsTable_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }

            var pendingDocumentRequest = (ReadOnlyPendingDocumentRequest)args.Item.DataItem;

            var requestId = (HiddenField)args.Item.FindControl("RequestId");
            var requestType = (EncodedLiteral)args.Item.FindControl("Type");
            var section = (EncodedLiteral)args.Item.FindControl("Section");
            var docType = (EncodedLiteral)args.Item.FindControl("DocType");
            var application = (EncodedLiteral)args.Item.FindControl("Application");
            var description = (EncodedLiteral)args.Item.FindControl("Description");
            var status = (EncodedLiteral)args.Item.FindControl("Status");
            var explainLink = (HtmlAnchor)args.Item.FindControl("ExplainLink");
            var statusExplanation = (HiddenField)args.Item.FindControl("StatusExplanation");

            requestId.Value = pendingDocumentRequest.Id.ToString("D");
            requestType.Text = pendingDocumentRequest.RequestTypeDescription;
            section.Text = pendingDocumentRequest.FolderDescription;
            docType.Text = pendingDocumentRequest.DocTypeDescription;
            application.Text = pendingDocumentRequest.BorrowerFullName;
            if (!string.IsNullOrEmpty(pendingDocumentRequest.CoborrowerFullName))
            {
                application.Text += " & " + pendingDocumentRequest.CoborrowerFullName;
            }
            description.Text = pendingDocumentRequest.Description;

            string statusExplanationStr = null;
            var requestStatus = this.GetRequestStatus(pendingDocumentRequest, out statusExplanationStr);

            status.Text = this.GetStatusDescription(requestStatus);
            explainLink.Visible = requestStatus != PendingDocumentRequestStatus.Ready;
            statusExplanation.Value = statusExplanationStr;
        }

        private bool DoesBorrowerHaveAccessToLoanInNewConsumerPortal(string email)
        {
            // Check if any of the new CP users for this portal have access to the file.
            return this.NewConsumerPortalUsersWithAccessToFile
                .Any(tuple => tuple.Item1.Email.Equals(email, StringComparison.OrdinalIgnoreCase));
        }

        private PendingDocumentRequestStatus GetRequestStatus(ReadOnlyPendingDocumentRequest pendingDocumentRequest, out string explanation)
        {
            E_sStatusT[] statuses = { E_sStatusT.Lead_Canceled, E_sStatusT.Lead_Declined, E_sStatusT.Lead_New, E_sStatusT.Lead_Other };

            if (!string.IsNullOrEmpty(pendingDocumentRequest.ErrorMessage))
            {
                explanation = pendingDocumentRequest.ErrorMessage;
                return PendingDocumentRequestStatus.Error;
            }
            else
            {
                if (this.BrokerDB.IsEnableNewConsumerPortal)
                {
                    if (statuses.Contains(dataloan.sStatusT) && !(dataloan.sConsumerPortalSubmittedD.IsValid || dataloan.sConsumerPortalVerbalSubmissionD.IsValid))
                    {
                        explanation = "Request cannot be sent until this file is converted to a loan or the consumer has submitted their application.";
                        return PendingDocumentRequestStatus.Suspended;
                    }

                    // 6/14/2014 gf - For the new consumer portal, we want to send
                    // the requests in the queue when either the borrower or co-
                    // borrower has been granted access through the portal.
                    bool borrowerHasAccessToPortal = this.DoesBorrowerHaveAccessToLoanInNewConsumerPortal(pendingDocumentRequest.BorrowerEmail);
                    bool coborrowerHasAccessToPortal = this.DoesBorrowerHaveAccessToLoanInNewConsumerPortal(pendingDocumentRequest.CoborrowerEmail);

                    if (!borrowerHasAccessToPortal && !coborrowerHasAccessToPortal)
                    {
                        explanation = "At least one borrower belonging to this request's " +
                            "application must be given access to the portal.";
                        return PendingDocumentRequestStatus.Suspended;
                    }
                    else
                    {
                        explanation = "";
                        return PendingDocumentRequestStatus.Ready;
                    }
                }
                else
                {
                    // 6/14/2014 gf - For the old portal, we just want to send
                    // the requests in the queue when either the borrower or co-
                    // borrower has an email.
                    if (string.IsNullOrEmpty(pendingDocumentRequest.BorrowerEmail) &&
                        string.IsNullOrEmpty(pendingDocumentRequest.CoborrowerEmail))
                    {
                        explanation = "At least one borrower belonging to this request's " +
                            "application must have an email address.";
                        return PendingDocumentRequestStatus.Suspended;
                    }
                    else
                    {
                        explanation = "";
                        return PendingDocumentRequestStatus.Ready;
                    }
                }
            }
        }

        private string GetStatusDescription(PendingDocumentRequestStatus status)
        {
            switch (status)
            {
                case PendingDocumentRequestStatus.Ready:
                    return "Ready to send";
                case PendingDocumentRequestStatus.Suspended:
                    return "Suspended";
                case PendingDocumentRequestStatus.Error:
                    return "Error";
                default:
                    throw new UnhandledEnumException(status);
            }
        }

        private void ActivateCPortal()
        {
            if (0 != EDocumentNotifier.GetConsumersNeedingLoginNotification(LoanID).Count)
            {
                EDocumentNotifier.SendLoginNotifications(LoanID, BrokerUser.BrokerId);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Borrower portal invitations sent')", true);
            }
        }

        private void ResetPassword(CPageData dataLoan)
        {
            BranchDB db = new BranchDB(dataloan.sBranchId, BrokerUser.BrokerId);
            db.Retrieve();
            if (!db.ConsumerPortalId.HasValue && dataloan.BrokerDB.IsEnableNewConsumerPortal)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"Error: This loan's branch does not belong to any consumer portal.\")", true);
                return;
            }

            long id = m_userIdLookup[m_requestId.Value];
            ConsumerPortalUser user = ConsumerPortalUser.Retrieve(BrokerUser.BrokerId, id);
            user.InitializePasswordReset();
            user.Save();

            EDocumentNotifier.SendAccountInformation(BrokerUser.BrokerId, LoanID, user.FirstName + " " + user.LastName, user.Email, user.PasswordResetCode);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Temporary password sent')", true);
        }

        private ConsumerPortalUser CreateAccount(Guid brokerId, Guid loanId, string email, string firstName, string lastName, string fullName)
        {
            ConsumerPortalUser user = ConsumerPortalUser.Retrieve(brokerId, email);
            if (user == null)
            {
                user = ConsumerPortalUser.Create(dataloan.sBrokerId, email);
                user.FirstName = firstName;
                user.LastName = lastName;
                string password = user.GenerateTempPassword();
                user.Save();
                EDocumentNotifier.SendAccountInformation(BrokerUser.BrokerId, LoanID, fullName, email, password);
            }

            return user;


        }

        private void CreateAccount(CPageData dataLoan)
        {
            BranchDB db = new BranchDB(dataloan.sBranchId, BrokerUser.BrokerId);
            db.Retrieve();
            if (!db.ConsumerPortalId.HasValue && dataloan.BrokerDB.IsEnableNewConsumerPortal)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(\"Error: This loan's branch does not belong to any consumer portal.\")", true);
                return;
            }
            string email = m_requestId.Value;
            if (dataloan.BrokerDB.IsEnableNewConsumerPortal)
            {
                for (int i = 0; i < dataloan.nApps; i++)
                {
                    CAppData app = dataloan.GetAppData(i);
                    bool abMatches = email.Equals(app.aBEmail, StringComparison.OrdinalIgnoreCase);
                    bool aCMatches = email.Equals(app.aCEmail, StringComparison.OrdinalIgnoreCase);
                    if (abMatches || aCMatches)
                    {
                        string firstName, lastName, fullName;
                        if (abMatches)
                        {
                            firstName = app.aBFirstNm;
                            lastName = app.aBLastNm;
                            fullName = app.aBNm;
                        }
                        else
                        {
                            firstName = app.aCFirstNm;
                            lastName = app.aCLastNm;
                            fullName = app.aCNm;
                        }

                        ConsumerPortalUser user = CreateAccount(dataloan.sBrokerId, dataloan.sLId, email, firstName, lastName, fullName);
                        ConsumerPortalUser.LinkLoan(dataLoan.sBrokerId, user.Id, LoanID);
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Access granted')", true);
                        return;
                    }
                }

                // OPM 184017. Only allow this old-style title only borrowers in legacy mode.
                if (dataloan.BrokerDB.IsUseNewNonPurchaseSpouseFeature == false)
                {
                    TitleBorrower borrower = dataloan.sTitleBorrowers.FirstOrDefault(p => p.Email.Equals(email, StringComparison.OrdinalIgnoreCase));
                    if (borrower != null)
                    {
                        ConsumerPortalUser user = CreateAccount(dataloan.sBrokerId, dataloan.sLId, email, borrower.FirstNm, borrower.LastNm, borrower.FullName);
                        ConsumerPortalUser.LinkLoan(dataLoan.sBrokerId, user.Id, LoanID);
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Access granted')", true);
                        return;
                    }
                }
            }

            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error Creating Account')", true);
        }

        private IEnumerable<Tuple<ConsumerPortalUser,LinkedLoanApp>> cachedNewConsumerPortalUsersWithAccessToFile = null;

        /// <summary>
        /// Gets an enumerable of the consumer portal users with access to the file.
        /// </summary>
        /// <value>
        /// Returns empty list if there are no consumers with access to the file or the 
        /// broker does not have the new consumer portal enabled.
        /// </value>
        private IEnumerable<Tuple<ConsumerPortalUser,LinkedLoanApp>> NewConsumerPortalUsersWithAccessToFile 
        {
            get
            {
                if (this.cachedNewConsumerPortalUsersWithAccessToFile == null)
                {
                    if (this.BrokerDB.IsEnableNewConsumerPortal)
                    {
                        this.cachedNewConsumerPortalUsersWithAccessToFile = ConsumerPortalUser.GetConsumerPortalUsersWithAccessToLoan(PrincipalFactory.CurrentPrincipal.BrokerId, this.LoanID);
                    }
                    else
                    {
                        this.cachedNewConsumerPortalUsersWithAccessToFile = new List<Tuple<ConsumerPortalUser,LinkedLoanApp>>();
                    }
                }

                return this.cachedNewConsumerPortalUsersWithAccessToFile;
            }
        }

        private IEnumerable<ReadOnlyPendingDocumentRequest> cachedPendingDocumentRequests = null;

        /// <summary>
        /// Gets an enumerable of the pending document requets on file for this loan.
        /// </summary>
        private IEnumerable<ReadOnlyPendingDocumentRequest> PendingDocumentRequests
        {
            get
            {
                if (this.cachedPendingDocumentRequests == null)
                {
                    this.cachedPendingDocumentRequests = PendingDocumentRequestHandler.GetRequestsForLoanId(
                        this.LoanID,
                        PrincipalFactory.CurrentPrincipal.BrokerId
                        );
                }

                return this.cachedPendingDocumentRequests;
            }
        }

        /// <summary>
        /// Gets the date and time when the items in the document request queue will be sent.
        /// Null if there aren't any pending document requests.
        /// </summary>
        private DateTime? PendingDocumentRequestQueueSendTime
        {
            get
            {
                if (this.PendingDocumentRequests.Any())
                {
                    return this.PendingDocumentRequests.Max(r => r.ScheduledSendTime);
                }
                else
                {
                    return null;
                }
            }
        }

        #region IAutoLoadUserControl Members
        public void LoadData()
        {
            if (RequestHelper.GetSafeQueryString("action") == "previewShare")
            {
                    SharedDocumentRequest request = SharedDocumentRequest.GetRequestById(BrokerUser.BrokerId, long.Parse(RequestHelper.GetSafeQueryString("rid")));
                    Response.ClearHeaders();
                    Response.ContentType = "application/pdf";
                    Response.Headers.Add("Content-Disposition", "attachment; filename=\"preview.pdf\"");
                    Response.WriteFile(request.GetDocumentPath());
                    Response.End();
                    return; 
            }
            m_userIdLookup = ViewState["UserIdLookup"] as Dictionary<string, long>; 
            m_userMsg.Text = "";
            dataloan = CPageData.CreateUsingSmartDependency(LoanID, typeof(DocRequests));
            dataloan.InitLoad();

            if (!dataloan.sConsumerPortalCreationD.IsValid && !dataloan.sConsumerPortalSubmittedD.IsValid)
            {
                divTracking.Visible = false;
            }
            else
            {
                sConsumerPortalCreationD.Text = dataloan.sConsumerPortalCreationD_rep;
                sConsumerPortalSubmittedD.Text = dataloan.sConsumerPortalSubmittedD_rep;

                switch (dataloan.sConsumerPortalCreationT)
                {
                    case E_sConsumerPortalCreationT.NotApplicable:
                        sConsumerPortalCreationT.Text = "";
                        break;
                    case E_sConsumerPortalCreationT.FullApplication:
                        sConsumerPortalCreationT.Text = "Full Application";
                        break;
                    case E_sConsumerPortalCreationT.ShortApplication:
                        sConsumerPortalCreationT.Text = "Short Application";
                        break;
                    case E_sConsumerPortalCreationT.Respa6Only:
                        sConsumerPortalCreationT.Text = "RESPA 6 Only Application";
                        break;
                    default:
                        string msg = "Unknown Consumer Portal Application Creation Type: " + dataloan.sConsumerPortalCreationT.ToString();
                        throw new CBaseException(msg, msg);
                }
            }

            if (dataloan.sConsumerPortalCreationD.IsValid)
            {
                phsConsumerPortalVerbalSubmissionD.Visible = true;
                sConsumerPortalVerbalSubmissionD.Text = dataloan.sConsumerPortalVerbalSubmissionD_rep;
            }

            switch (m_requestAction.Value)
            {
                case "Cancel":
                    CancelRequest();
                    break;
                case "Accept":
                    AcceptRequest();
                    break;
                case "Reject":
                    RejectRequest();
                    break;
                case "Resend":
                    ResendRequest(dataloan);
                    break;
                case "ActivateCPortal":
                    ActivateCPortal();
                    break;
                case "Reset":
                    ResetPassword(dataloan);
                    break;
                case "Create":
                    CreateAccount(dataloan);
                    break;
                case "cancelShare":
                    SharedDocumentRequest.Delete(BrokerUser.BrokerId, long.Parse(m_requestId.Value));
                    break;
                case "DeletePending":
                    DeletePendingRequests();
                    break;
                case "SendPending":
                    SendPendingRequests();
                    break;
                default:
                    break;
            }

            m_requestAction.Value = "";

            PendingDocumentRequestsTable.DataSource = this.PendingDocumentRequests;
            PendingDocumentRequestsTable.DataBind();

            if (this.PendingDocumentRequests.Any())
            {
                var queueSendTime = this.PendingDocumentRequestQueueSendTime.Value;
                QueueSendTimeLabel.Text = string.Format(
                    "Requests within this queue will automatically be sent to the borrower/s at {0} on {1}.",
                    Tools.GetTimeDescription(queueSendTime),
                    queueSendTime.ToString("d"));
            }
            else
            {
                QueueSendTimeLabel.Text = "";
            }

            Documents.DataSource = ReadOnlyConsumerActionItem.GetConsumerActionItems(BrokerUser.BrokerId, new Guid(RequestHelper.GetSafeQueryString("loanid")));
            Documents.DataBind();

            SharedDocumentsRepeater.DataSource = SharedDocumentRequest.GetRequestByLoanId(BrokerUser.BrokerId, LoanID, null);
            SharedDocumentsRepeater.DataBind(); 

            NameMismatch.Visible = NamesMismatchOnTable;

            NameMismatchMessage.Text = ErrorMessages.ConsumerPortal.NameOnFileDoesNotMatchRequest;

            BrokerDB db = this.BrokerDB;
            if (db.IsEnableNewConsumerPortal)
            {
                var users = this.NewConsumerPortalUsersWithAccessToFile;
                List<Tuple<string, string>> missingBorrowers = new List<Tuple<string, string>>();

                SharedDocumentsNewPortal.Visible = true;

                m_userIdLookup = new Dictionary<string, long>();
                m_reverseUserIdLookup = new Dictionary<long, string>();
                foreach (long userId in users.Select(p=>p.Item1.Id).Distinct())
                {
                    string lookUpId = EncryptionHelper.GenerateRandomString(5);
                    m_userIdLookup.Add(lookUpId, userId);
                    m_reverseUserIdLookup.Add(userId, lookUpId);
                }

                for (int i = 0; i < dataloan.nApps; i++)
                {
                    CAppData app = dataloan.GetAppData(i);
                    if (!string.IsNullOrEmpty(app.aBEmail) && ConstApp.IsEmailValid(app.aBEmail) 
                        && !users.Any(p=>p.Item1.Email.Equals(app.aBEmail, StringComparison.OrdinalIgnoreCase))  )
                    {
                        missingBorrowers.Add(Tuple.Create(app.aBEmail, app.aBNm));
                    }
                    if (!string.IsNullOrEmpty(app.aCEmail) && ConstApp.IsEmailValid(app.aCEmail) 
                        && !users.Any(p => p.Item1.Email.Equals(app.aCEmail, StringComparison.OrdinalIgnoreCase)))
                    {
                        missingBorrowers.Add(Tuple.Create(app.aCEmail, app.aCNm));
                    }
                }

                // OPM 184017. Only allow this old-style title only borrowers in legacy mode.
                if (dataloan.BrokerDB.IsUseNewNonPurchaseSpouseFeature == false)
                {
                    foreach (TitleBorrower borrower in dataloan.sTitleBorrowers)
                    {
                        if (!string.IsNullOrEmpty(borrower.Email) && ConstApp.IsEmailValid(borrower.Email) &&
                            !users.Any(p => p.Item1.Email.Equals(borrower.Email, StringComparison.OrdinalIgnoreCase)))
                        {
                            missingBorrowers.Add(Tuple.Create(borrower.Email, borrower.FullName + " (Title Only)"));
                        }
                    }
                }
                ViewState.Add("UserIdLookup", m_userIdLookup);
                ActivateLink.Visible = false;
                ConsumerPermissionRepeater.Visible = true;
                ConsumerPermissionRepeater.DataSource = users;
                ConsumerPermissionRepeater.DataBind();
                ConsumerPermissionRepeater.Visible = ConsumerPermissionRepeater.Items.Count > 0;
                NoAccountsConsumersRepeaters.DataSource = missingBorrowers;
                NoAccountsConsumersRepeaters.DataBind();
                NoAccountsConsumersRepeaters.Visible = missingBorrowers.Count > 0;

            }
            else
            {
                ActivateLink.Disabled = EDocumentNotifier.GetConsumersNeedingLoginNotification(LoanID).Count == 0;
            }
        }

        public void SaveData()
        {
        }
        #endregion

        protected void SharedDocumentsRepeater_OnItemDataBound(object dataSrc, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            SharedDocumentRequest req = args.Item.DataItem as SharedDocumentRequest;

            HtmlAnchor cancel = (HtmlAnchor)args.Item.FindControl("cancel");
            HtmlAnchor preview = (HtmlAnchor)args.Item.FindControl("preview");

            var view = (EncodedLiteral)args.Item.FindControl("View");
            var application = (EncodedLiteral)args.Item.FindControl("Application");
            var description = (EncodedLiteral)args.Item.FindControl("Description");
            var sharedOn = (EncodedLiteral)args.Item.FindControl("SharedOn");
            HtmlTableCell appCell = (HtmlTableCell)args.Item.FindControl("appCell");
            switch (req.ShareStatusT)
            {
                case E_ShareStatusT.New:
                    view.Text = "New";
                    break;
                case E_ShareStatusT.BorrowerInformed:
                    view.Text = "Email Sent";
                    break;
                case E_ShareStatusT.BorrowerDownloaded:
                    view.Text = "Downloaded on " + req.DownloadedOn.Value.ToShortDateString();
                    break;
                default:
                    throw new UnhandledEnumException(req.ShareStatusT);
            }


            CAppData appData = dataloan.GetAppData(req.aAppId);
            if ((appData.aBFirstNm + " " + appData.aBLastNm != req.aBFullName ) ||
                (appData.aCFirstNm + " " + appData.aCLastNm != req.aCFullName))
            {
                appCell.Attributes.Add("class", "red");
                NamesMismatchOnTable = true;
            }


            application.Text = req.aBFullName;

            if (!string.IsNullOrEmpty(req.aCFullName))
            {
                application.Text += " & " + req.aCFullName;
            }

            description.Text = req.Description;
            sharedOn.Text = req.CreatedOn.ToShortDateString();
            cancel.HRef = string.Format("javascript:cancelShareDocument({0})", req.Id);
            string ur = Request.Url + "&action=previewShare&rid=" + req.Id;
            preview.HRef = ur;
        }
    }
}
