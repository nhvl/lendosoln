﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.PdfLayout;
using DataAccess;

namespace LendersOfficeApp.newlos.ElectronicDocs
{
    public partial class SignatureTemplateService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch(methodName) {
                case "SaveTemplate":
                    SaveTemplate();
                    break;
                case "LoadFromTemplate":
                    LoadFromTemplate();
                    break;
                case "SaveSignaturePositions":
                    SaveSignaturePositions();
                    break;
            }
        }
        private void LoadFromTemplate()
        {
            Guid templateId = GetGuid("TemplateId", Guid.Empty);

            if (templateId != Guid.Empty)
            {
                PdfSignatureTemplate template = PdfSignatureTemplate.RetrieveByTemplateId(templateId);

                List<PdfField> pdfFieldList = new List<PdfField>();

                foreach (var o in template.FormLayout.FieldList)
                {
                    pdfFieldList.Add(o);
                }
                SetResult("JsonFields", ObsoleteSerializationHelper.JsonSerialize(pdfFieldList));

            }
            else
            {
                SetResult("JsonFields", string.Empty);
            }
        }
        private void SaveTemplate()
        {
            Guid templateId = GetGuid("TemplateId", Guid.Empty);
            string templateName = GetString("TemplateName", string.Empty);

            string isNew = "False";

            PdfSignatureTemplate template = null;
            if (templateId == Guid.Empty && templateName != string.Empty)
            {
                template = PdfSignatureTemplate.Create();
                template.Name = templateName;
                isNew = "True";
            }
            else if (templateId != Guid.Empty) 
            {
                template = PdfSignatureTemplate.RetrieveByTemplateId(templateId);
                template.FormLayout.Clear();
            }

            if (null != template)
            {

                string json = GetString("JsonFields");

                List<PdfField> pdfFieldList = ObsoleteSerializationHelper.JsonDeserialize<List<PdfField>>(json);

                foreach (var o in pdfFieldList)
                {
                    template.FormLayout.Add(o);
                }

                template.Save();

                SetResult("TemplateId", template.TemplateId);
                SetResult("TemplateName", template.Name);
                SetResult("isNew", isNew);
                SetResult("Result", "OK");
            }
            else
            {
                SetResult("Result", "Fail");
            }

        }

        private void SaveSignaturePositions()
        {
            string json = GetString("JsonFields");
            List<PdfField> pdfFieldList = ObsoleteSerializationHelper.JsonDeserialize<List<PdfField>>(json);
            PdfFormLayout layout = new PdfFormLayout();
            foreach (var o in pdfFieldList)
            {
                layout.Add(o);
            }

            string xml = layout.GetXmlContent();

            string signatureCacheId = AutoExpiredTextCache.AddToCache(xml, new TimeSpan(1, 0, 0));
            SetResult("SignatureCacheId", signatureCacheId);
            SetResult("ContainBorrowerSignature", layout.IsContainBorrowerSignature);
            SetResult("ContainCoborrowerSignature", layout.IsContainCoborrowerSignature);
        }
    }
}
