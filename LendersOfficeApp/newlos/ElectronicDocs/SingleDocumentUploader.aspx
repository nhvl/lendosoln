﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SingleDocumentUploader.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.SingleDocumentUploader" %>
<%@ Register TagPrefix="uc1" TagName="UploadDocs" Src="~/newlos/ElectronicDocs/UploadDocs.ascx" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Document Upload</title>
    <style type="text/css">
        body {
            background-color: gainsboro;
        }
        textarea {
            height: 5em;
        }
        .FormTable {
            width: 100%;
        }
        .FormTable input[type=file] {
            width: 300px !important;<%-- UploadDocs.ascx makes this 360px, and there is not currently a way to pull out the inline style from the asp:FileUpload. --%>
        }
        .SelectedDocTypeName, .DocTypePickerLink {
            display: block;
            min-width: 120px;
        }
    </style>
</head>
<body>
    <h4 class="page-header">Document Upload</h4>
    <form id="form1" runat="server">
        <uc1:UploadDocs runat="server" ID="UploadDocs" SupportedUploadCount="1" SupportsBarcodeScan="false" RestrictToPdfForNonUcd="true" />
        <input type="button" id="CancelBtn" value="Cancel" />
    </form>
    <script type="text/javascript">
        <%--To be picked up by UploadDocs.ascx--%>
        var documentUploadHandler;
        var onDragDropUiChanged;

        (function ($) {
            var ourWindow = parent.LQBPopup,
                returnObj = { OK: false };

            function exit() {
                ourWindow.Return(returnObj);
            }

            function cancel() {
                returnObj.OK = true;
                exit();
            }

            function resizeWindow() {
                if (document.readyState == 'interactive')
                {
                    setTimeout(resizeWindow, 100);
                }
                else {
                    var thisScrollWidth = document.body.scrollWidth, thisScrollHeight = document.body.scrollHeight;
                    var finalWidth = Math.max(700, thisScrollWidth), finalHeight = Math.max(250, thisScrollHeight);
                    ourWindow.Resize(finalWidth, finalHeight);
                }
            }

            $('.SelectedDocTypeId').change(resizeWindow);
            $('#CancelBtn').click(cancel);

            documentUploadHandler = function (documents) {
                if (documents && documents.length == 1) {
                    returnObj.Document = documents[0];
                    returnObj.OK = true;
                }

                exit();
            };

            $(function () {
                // Place the cancel button for this popup on the same line 
                // as the "Upload" button for the control.
                $('#UploadButton').after($('#CancelBtn').detach());

                resizeWindow();
                onDragDropUiChanged = resizeWindow;
            });
        })(jQuery);
    </script>
</body>
</html>
