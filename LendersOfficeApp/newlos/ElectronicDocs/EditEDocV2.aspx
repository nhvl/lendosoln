﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="EditEDocV2.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.EditEDocV2" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<%@ Import Namespace="EDocs" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="uc" TagName="DocTypePicker" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx"%>
<%@ Register TagPrefix="batch" TagName="BatchEditDocList" Src="~/newlos/ElectronicDocs/BatchEditDocList.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>EDocs Editor</title>
    <style type="text/css">
		div[aria-describedby='LQBPopupDiv'] {
			z-index: 2800;
		}
		div.BlockMask {
			z-index: 2750;
		}
	</style>
</head>
<body bgcolor="gainsboro" onbeforeunload="return page_unload(true)" >
    <script type="text/javascript" src="../../inc/ModelessDlg.js"></script>
    <script type="text/x-jquery-tmpl" id="noteTemplate">
        <div class="minimize-item">
            <button class="maximize-note">
                <img  src="../../images/edocs/maximize.png" alt="maximize" />
            </button>
        </div>
        <div class="maximize-item" >
            <textarea  class="pdfnotes"  rows="1" cols="1"  >${Text}</textarea>
            <span>Last updated: </span><span class="note_last_modified">${LastModifiedOn}</span>
            <button class="minimize-note" >
                <img src="../../images/edocs/minimize.png" alt="minimize" />
            </button>
        </div>
    </script>
    <script type="text/x-jquery-tmpl" id="noteThumbTemplate">
        <div>
            <img src="${src}" alt="" />
        </div>
    </script>
    <script type="text/javascript" >
        //Gets set in the jquery init code
        var _docId;
        var filterTypes = [<%=AspxTools.JsNumeric(E_EDocStatus.Blank) %>,<%=AspxTools.JsNumeric(E_EDocStatus.Approved) %>,<%=AspxTools.JsNumeric(E_EDocStatus.Screened) %>];
        var _isLoading = true;
        var _pdfEditor = null;
        var _isDirty = false;
        var docListUpdate = false;
        var storageKey = '';
        var filtering = false;

        setInterval(_refresh, 10800000);

        function _refresh(){
            if( storageKey.length == 0 ) {
                return;
            }
            var data = {
                'StorageKey' : storageKey
            }

            var results = gService.loanedit.call('Poll', data);

        }
        function _recordEvent(eventid) {
            var data = {
                'DocumentId' : _docId,
                'StorageKey' : storageKey,
                'EventId' : eventid
            };

            var results = gService.loanedit.call('RecordAuditEvent', data);
            if( results.value && results.value['StorageKey']) {
                storageKey = results.value['StorageKey'];
            }
            else {
                alert('error recording audit entry');
            }
        }
        function _displayStatusMessage(msg)
        {
            var top = $(document).height() / 2 - 50;
            var left = $(document).width() / 2 - 150;
            $('#divStatusPanel div').text(msg);
            $('#divStatusPanel').css({ 'top': top + 'px', 'left': left + 'px' }).show();
        }
        function _hideStatusMessage()
        {
            $('#divStatusPanel').hide();
            if(_closeWindow){
                onClosePopup();
            }
        }

        function setDirtyBit() {
            _isDirty = true;
            setDirtyBit_callback();
        }

        function resetDirty() {
            _isDirty = false;
            resetDirty_callback();
        }

        function saveOnExit() { return confirm("Do you want to save changes?"); }

        function page_unload(isWindowClosing) {
            if (isWindowClosing) {
                if(_childWindow) {
                    _childWindow.close();
                }

                if( _isDirty )
                {
                    return UnloadMessage;
                }
            } else {
                if( _isDirty )
                {
                    if( saveOnExit() ) {
                        saveMe(false, true);
                    }
                }

                if(_childWindow)
                {
                    _childWindow.close();
                }
            }
        }

        function resizeTextArea() {
            var ta = $('textarea', this);
            var bIsHidden =$(this).data('__HIDDEN__');

            if (bIsHidden == null)
            {
                //bIsHidden = ta.is(':hidden'); // 5/19/2011 dd - This is an expensive call.
                // 5/19/2011 dd - Let assume it is not hidden when not set.
                bIsHidden = false;
            }
            if( ta.length == 0 || bIsHidden) {
                return;
            }

            var parentHeight = 0; //$(this).height();
            var parentWidth = 0; //$(this).width();

            var taPositionTop = 5;
            var pdfFieldInfo = null; // this cache does not work well $(this).data('pdfFieldData');
            if (pdfFieldInfo != null)
            {
                parentHeight = pdfFieldInfo.height;
                parentWidth = pdfFieldInfo.width;
            }
            else
            {
                parentHeight = $(this).height();
                parentWidth = $(this).width();
            }
            //var taStart = ta.offset

            ta.height(parentHeight - 35 )
            ta.width(parentWidth - 20);
        }

        function saveMe(bRefreshScreen, closing) {
            _Save(bRefreshScreen, closing);
        }

        function disableButton($button)
        {
            $button.prop('disabled', true);
            var $img = $button.children('img');
            var src = $img.attr('src');
            src.replace(".png", "-disabled.png");
            $img.attr('src', src);
        }

        function enableButton($button)
        {
            $button.prop('disabled', false);
            var $img = $button.children('img');
            var src = $img.attr('src');
            src.replace("-disabled", "");
            $img.attr('src', src);
        }

        function setDirtyBit_callback() {
            enableButton($('#btnSave'));
        }

        function resetDirty_callback() {
            disableButton($('#btnSave'));
        }

        function enableOperations() {
            $('.op_button').prop('disabled', false);

            enableButton($('#btnSave'));
            enableButton($('#btnPrint'));
            enableButton($('#btnInsertPages'));
            enableButton($('#btnDeletePages'));
            enableButton($('#btnExtractPages'));
            enableButton($('#btnMovePage'));
            enableButton($('#btnRotateLeft'));
            enableButton($('#btnRotateRight'));
            enableButton($('#btnBatchRotate'));
        }

        function disableOperations() {
            $('.op_button').prop('disabled', true);

            disableButton($('#btnSave'));
            disableButton($('#btnPrint'));
            disableButton($('#btnInsertPages'));
            disableButton($('#btnDeletePages'));
            disableButton($('#btnExtractPages'));
            disableButton($('#btnMovePage'));
            disableButton($('#btnRotateLeft'));
            disableButton($('#btnRotateRight'));
            disableButton($('#btnBatchRotate'));
        }

        function enableTabs() {
            var $tabs = $('.tab a');
            setDisabledAttr($tabs, false);
        }

        function disableTabs() {
            var $tabs = $('.tab a');
            setDisabledAttr($tabs, true);
        }

        function disableAll(){
            _pdfEditor.DisableInteraction();
            disableTabs();
            disableOperations();
        }

        function enableAll(){
            _pdfEditor.EnableInteraction();
            enableTabs();
            enableOperations();
        }

        function _GetAnnotationSaveData(bCanEditAnnotations) {

            if( bCanEditAnnotations ) {
                var annotations = [];
                $.each(_pdfEditor.GetPdfFields(), function(i,o){

                    if(o.cssClass == 'pdf-field-highlight' || o.cssClass == 'pdf-field-note'  ) {
                        annotations.push(o);
                    }
                });
                return JSON.stringify(annotations);
            }
            return '[]';
        }

        function _GetESignTagsSaveData() {
            var eSignTags = [];
            $.each(_pdfEditor.GetPdfFields(), function(i, o) {
                if(o.cssClass == "pdf-field-esign-signature" || o.cssClass == 'pdf-field-esign-initial' || o.cssClass == 'pdf-field-esign-date') {
                    eSignTags.push(o);
                }
            });

            return JSON.stringify(eSignTags);
        }

        function _GetSignatureSaveData(){
            var signatures = [];
            $.each(_pdfEditor.GetPdfFields(), function(i,o){

                if( o.cssClass == 'pdf-field-signature-img' || o.cssClass == 'pdf-field-date' ) {
                    signatures.push(o);
                }
            });
            return JSON.stringify(signatures);
        }

        var g_ConditionAssociations = {};
        //Gets called by the EditConditionAssociation window when that window is saved.
        //parameter is a map of [taskId] = associated? true : false.
        function OnConditionAssociationSave(partial, fullInfo, sPartial, sFullInfo)
        {
            //It's easier to handle the serialized versions, and we need to clone it anyway so use that.
            g_ConditionAssociations = JSON.parse(sFullInfo);
            setDirtyBit();
        }

        function GetSerializedConditionSettings()
        {
            return JSON.stringify(g_ConditionAssociations);
        }

        function f_arrayContains(array, element)
        {
            if (array == null)
            {
                return false;
            }

            for (var i = 0; i < array.length; ++i)
            {
                if (array[i] === element)
                {
                    return true;
                }
            }

            return false;
        }

        function updateDocListingPanelStatus(args) {
            var $document = $("li.document.selected");
            $document.find("span.doc-notes")[0].innerText = args["PublicDescription"];

            var $documentStatus = $document.find("span.docType");
            switch(args["Status"]) {
                case "<%=AspxTools.JsNumeric(EDocs.E_EDocStatus.Blank) %>": 
	                $documentStatus[0].innerText = "";
	                $documentStatus[0].style.display = "none"
	                break;
                case "<%=AspxTools.JsNumeric(EDocs.E_EDocStatus.Approved) %>": 
	                $documentStatus[0].innerText = "Protected";
	                $documentStatus[0].style.color = "darkgreen";
	                $documentStatus[0].style.display = ""
	                break;
                case "<%=AspxTools.JsNumeric(EDocs.E_EDocStatus.Screened) %>": 
	                $documentStatus[0].innerText = "Screened";
	                $documentStatus[0].style.color = "darkblue";
	                $documentStatus[0].style.display = ""
	                break;
                case "<%=AspxTools.JsNumeric(EDocs.E_EDocStatus.Rejected) %>": 
	                $documentStatus[0].innerText = "Rejected";
	                $documentStatus[0].style.color = "darkblue";
	                $documentStatus[0].style.display = ""
	                break;
                case "<%=AspxTools.JsNumeric(EDocs.E_EDocStatus.Obsolete) %>": 
	                $documentStatus[0].innerText = "Obsolete";
	                $documentStatus[0].style.color = "darkblue";
	                $documentStatus[0].style.display = ""
	                break;
            }

            $documentStatus[0].setAttribute("data-docStatusVal", args["Status"]);
        }

        function _SaveNonPdfData(bCanEditAnnotations, isPdfEditable, isBatch) {
            if( _isLoading ) {
                alert('Cannot save until document is done loading.');
                return;
            }
            var args = {
                'DocId' :  _docId,
                'Version' : $('#<%= AspxTools.ClientId(Version)%>').val(),
                'Description' : $('#<%= AspxTools.ClientId(m_docDescription) %>').val(),
                'PublicDescription' : $('#<%= AspxTools.ClientId(m_publicDescription) %>').val(),
                'DocTypeId' : $('#<%= AspxTools.ClientId(m_docTypeList.ValueCtrl) %>').val(),
                'Annotations' : _GetAnnotationSaveData(bCanEditAnnotations),
                "ESignTags" : _GetESignTagsSaveData(),
                'CanEditESignTags' : ML.CanEditESignTags,
                'CanEditAnnotations' : bCanEditAnnotations,
                'StorageKey' : storageKey,
                "Status"       : $('#<%= AspxTools.ClientId(m_docStatus) %>').val(),
                "StatusDescription" : $('#<%= AspxTools.ClientId(m_docStatusDescription) %>').val(),
                "ConditionAssociations": JSON.stringify(g_ConditionAssociations),
                "HideFromPML"       : $('#<%= AspxTools.ClientId(m_HideFromPML) %>').is(':checked')
            };

            var result = gService.loanedit.call("SaveNonPdfData", args);
            RefreshOpener();

            if (!result.error) {
                if(result.value.AssociationsOnly) {
                    resetDirty();
                    return true;
                }

              <%--
                // When save metadata the version of edoc is bump up. Need to update pngkey in page structure
                --%>
                var pages = _pdfEditor.GetPages();
                var length = pages.length;
                var version = result.value["Version"];

                var list = JSON.parse(result.value.EDocPages);

                for (var i = 0; i < length; i++)
                {
                  pages[i].Version = version;
                }

                $('#<%= AspxTools.ClientId(Version)%>').val(version);
                resetDirty();
                storageKey = ''; //reset so we dont double record audit events

                if (isBatch && length != 0) {
                    updateDocListingPanelStatus(args);
                }
            }else {
               var errMsg = 'Unable to save. Please try again.';
                if (null != result.UserMessage) {
                    errMsg = result.UserMessage;
                }
                alert(errMsg);
            }
            return true;
        }
        function _Save(refresh, closing)
        {
            if( _isLoading ) {
                alert('Cannot save until document is done loading.');
                return;
            }
            _displayStatusMessage('Saving ...');
            if(closing)
            {
                _SaveImplHideMessage();
            }
            else
            {
                setTimeout(_SaveImplHideMessage, 200);
            }
        }
        function _SaveImplHideMessage()
        {
            //OPM 106903
            var UIStatus = null;
            if($('#<%= AspxTools.ClientId(m_docStatus) %>').val() == 2 && <%= AspxTools.JsBool(EnableDuplicateEDocProtectionChecking) %>) //OPM 115047 - Broker Setting
            {
                //If DocStatus=Protected, check for duplicates
                var docTypeId = $('#<%= AspxTools.ClientId(m_docTypeList.ValueCtrl) %>').val();
                var args = {
                    "DocId" : _docId,
                    "DocTypeId" : docTypeId,
                    "LoanId" : <%= AspxTools.JsString(LoanID) %>
                };

                var results = gService.loanedit.call('CheckDuplicateProtectedDocs', args);
                if(results.value && results.value['DuplicateProtectedDoc'])
                {
                    //Duplicates found; retrieve *this* doc
                    var docDetails = JSON.parse(results.value['DuplicateProtectedDoc']);

                    //Temporarily update fields to possibly modified values
                    docDetails.InternalDescription = $('#<%= AspxTools.ClientId(m_docDescription) %>').val();
                    docDetails.Description = $('#<%= AspxTools.ClientId(m_publicDescription) %>').val();
                    docDetails.HideFromPmlUsers = $('#<%= AspxTools.ClientId(m_HideFromPML) %>').is(':checked');

                    //Determine user's desired behavior for each duplicate
                    args = {'List' : [docDetails] , 'DocTypeId':docTypeId, 'CompleteEditList':'["'+_docId+'"]', 'CurrentUIDocumentId':_docId};
                    showModal( '/newlos/ElectronicDocs/DuplicateProtectedDialog.aspx?loanid=' + <%= AspxTools.JsString(LoanID) %> , args,
                    "dialogHeight: 300px; dialogWidth: 600px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No", null, function(modalArgs){ 
                        UIStatus = modalArgs.UIStatus;
                    },{ hideCloseButton: true });
                }
            }

            _SaveImpl(UIStatus);
            _hideStatusMessage();
        }

        function IsESignTag(item) {
            var $item = $(item);
            return $item.hasClass('pdf-field-esign-signature') || $item.hasClass('pdf-field-esign-date') || $item.hasClass('pdf-field-esign-initial');
        }


        function _IsSignature(o){
            var $o = $(o);
            return $o.hasClass('pdf-field-signature-img') || $o.hasClass('pdf-field-date');
        }

        function _ToggleInteractive(o, enable) {
            var $o = $(o);
            if( $o.hasClass('saved') && enable === true ) {
                return;
            }
            $o.toggleClass('disable-resize', !enable).toggleClass('disable-move', !enable).toggleClass('disable-selectable', !enable);
            $o.removeClass('ui-selected pdf-first-selected');
        }

        function RefreshOpener()
        {
            try
            {
                if( window.opener && !window.opener.closed ) {
                    window.opener.location.reload();
                }
            }
            catch(e){/*this fails if the DocsReceived list is closed, which we can't handle.*/}
        }

        function _SaveImpl(UIStatus) {
            var bCanEditAnnotations = $('#<%= AspxTools.ClientId(CanEditAnnotations) %>').val() == 'True';
            var bRedirectToDocumentListing = false;
            var bIsPdfEditable = canEditDocument();

            var isBatch = <%=AspxTools.JsBool(IsBatch)%>;

            var pages = _pdfEditor.GetPages();
            if (pages.length == 0) {
                if (!confirm('The document has no pages. Saving now will cause the document to be deleted. Do you want to delete the document?')) {
                    return false;
                }
                bRedirectToDocumentListing = true;
                disableAll();
                $("#btnFilter").prop("disabled", false);
            }
            var signatureJson = _GetSignatureSaveData();
            var hasSignatures = signatureJson !== '[]';
            if(bIsPdfEditable == false && hasSignatures == false ) {
                return _SaveNonPdfData(bCanEditAnnotations, bIsPdfEditable, isBatch); //cannot use this if there are signatures
            }
            if( hasSignatures ) {
                var msg = 'Permanently apply signatures to this document? The EDocs editor will be saved & closed while the PDF is regenerated.' +
                    '\n\nClick OK to permanently apply signatures, save, and close. Click Cancel to keep the editor open.';

                if( false == confirm(msg) ){
                    return;
                }
            }

            var str = JSON.stringify(pages);
            var args = {
                "EDocPages"         :  str,
                "DocId"             : _docId,
                "Version"           : $('#<%= AspxTools.ClientId(Version)  %>').val(),
                "Description"       : $('#<%= AspxTools.ClientId(m_docDescription) %>').val(),
                "PublicDescription" : $('#<%= AspxTools.ClientId(m_publicDescription) %>').val(),
                "DocTypeId"         : $('#<%= AspxTools.ClientId(m_docTypeList.ValueCtrl) %>').val(),
                "Annotations"       : _GetAnnotationSaveData(bCanEditAnnotations),
                "ESignTags"      : _GetESignTagsSaveData(),
                "CanEditESignTags" : ML.CanEditESignTags,
                "CanEditAnnotations": bCanEditAnnotations,
                'StorageKey' : storageKey,
                'Signatures' : signatureJson,
                "Status"       : $('#<%= AspxTools.ClientId(m_docStatus) %>').val(),
                "StatusDescription" : $('#<%= AspxTools.ClientId(m_docStatusDescription) %>').val(),
                "ConditionAssociations": JSON.stringify(g_ConditionAssociations),
                "HideFromPML"       : $('#<%= AspxTools.ClientId(m_HideFromPML) %>').is(':checked')

            };
            if(UIStatus!=null)
                args["Status"] = UIStatus;

            var result = gService.loanedit.call("SaveData", args);

            RefreshOpener();

            if (!result.error) {

            if(isBatch && pages.length != 0)
            {
                updateDocListingPanelStatus(args);

                var folderAndDocType = $('.SelectedDocTypeName').text()
                    .split(':');

                if (folderAndDocType.length < 2) {
                    throw 'Unexpected doc type name format. Expected [Folder] : [DocType]';
                }

                var folderDescInMainEditor  = $.trim(folderAndDocType[0]);
                var docTypeDescInMainEditor = $.trim(folderAndDocType[1]);

                var $document = $("li.document.selected");
                var folderDescInNavPanel = $.trim($document.parent().siblings('span.doc-type-folder').text());
                var docTypeDescInNavPanel = $.trim($document.find('span.doc-type-name').text());

                var insertElementInList = function($list, $elemToInsert, elemValue, getComparisonValue) {
                    var insertedItem = false;
                    $list.children('li').each(function() {
                        var otherElemValue = $.trim(getComparisonValue(this));
                        if (elemValue <= otherElemValue) {
                            $elemToInsert.insertBefore(this);
                            insertedItem = true;
                            return false;
                        }
                    });

                    if (!insertedItem) {
                        $elemToInsert.appendTo($list);
                    }
                };

                var getDocTypeNameFromElem = function(documentElem) {
                    return $(documentElem).find('span.doc-type-name').text();
                };

                if (folderDescInMainEditor !== folderDescInNavPanel) {
                    var moveDocToNewFolder = function() {
                        var isOnlyDocumentInFolder = $document.siblings().length === 0;
                        var $folder = $document.closest('li.folder');
                        var $folderToAddDocTo = null;
                        var desc = args['PublicDescription'];
                        var status = args['Status'];
                        var $detachedDocument = $document.detach();
                        $detachedDocument.find('span.doc-type-name').text(docTypeDescInMainEditor);

                        if (isOnlyDocumentInFolder) {
                            $folder.remove();
                        }

                        $('li.folder').each(function() {
                            var otherFolderDesc = $.trim($(this).find('span.doc-type-folder').text());
                            if (folderDescInMainEditor === otherFolderDesc) {
                                $folderToAddDocTo = $(this);
                                return false;
                            }
                        });

                        if ($folderToAddDocTo) {
                            insertElementInList($folderToAddDocTo.find('ol.document-name-listing'),
                                $detachedDocument,
                                docTypeDescInMainEditor,
                                getDocTypeNameFromElem);
                        } else {
                            var selectDoc = true;
                            addDocumentToBatchEditList(_docId,
                                folderDescInMainEditor,
                                docTypeDescInMainEditor,
                                desc,
                                status,
                                selectDoc);
                        }
                    };

                    moveDocToNewFolder();
                } else if (docTypeDescInMainEditor !== docTypeDescInNavPanel) {
                    var moveDocWithinFolder = function() {
                        var $list = $document.closest('ol.document-name-listing');
                        var $detachedDocument = $document.detach();
                        $detachedDocument.find('span.doc-type-name').text(docTypeDescInMainEditor);

                        insertElementInList($list,
                            $detachedDocument,
                            docTypeDescInMainEditor,
                            getDocTypeNameFromElem);
                    }

                    moveDocWithinFolder();
                }

                filterDocs();
                resetDirty();
            }


                if(g_docsToTrimOnSave.length > 0)
                {
                    gService.loanedit.call("DeleteInsertedPages", {'Json': JSON.stringify(g_docsToTrimOnSave)});
                    g_docsToTrimOnSave = [];
                }
                if(UIStatus!=null)
                    $('#<%= AspxTools.ClientId(m_docStatus) %>').val(UIStatus);
                resetDirty();
                updateDocTypeHeader();

                if(result.value.CloseWindow === 'True'){
                    onClosePopup();
                    return false;
                }
                //same as if document was deleted
                if (bRedirectToDocumentListing) {
                    if(!isBatch)
                    {
                        onClosePopup();
                        return false;
                    }
                    else
                    {
                        $(".document.selected").remove();
                        filterDocs();
                        setFolderVisibility();
                        selectAvailableDoc();
                        return false;
                    }
                }
                $('#<%= AspxTools.ClientId(Version)%>').val(result.value.Version);

                $.each(_pdfEditor.GetFields(), function(i,o){
                    if( _IsSignature(o) ) {
                        $(o).addClass('saved').removeClass('pdf-pdf-field pdf-field-template');
                        _ToggleInteractive(o, false);
                    }
                });
                storageKey = ''; //reset
                if (result.value.WasPdfUpdated === "True") {
                    pages = JSON.parse(result.value.EDocPages);
                    annotations = JSON.parse(result.value.Annotations);
                    <%--
                    5/21/2011 - dd - We do not need to refresh all pages in ui
                    --%>
                    var currentPdfPages = _pdfEditor.GetPages();
                    var length = pages.length;
                    for (var i = 0; i < length; i++)
                    {
                        var currentPage = currentPdfPages[i];
                        var updatePage = pages[i];
                        currentPage.Version = updatePage.Version;

                        currentPage.Page = updatePage.Page;
                        currentPage.DocId = updatePage.DocId;
                        currentPage.Token = updatePage.Token;
                    }
                } else {
                    var pages = _pdfEditor.GetPages();

                    var updatedPngPages = JSON.parse(result.value.EDocPages);

                    var length = pages.length;
                    for (var i = 0; i < length; i++)
                    {
                      var currentPage = pages[i];
                      var updatePage = updatedPngPages[i];

                      currentPage.Version = updatePage.Version;

                      currentPage.Page = updatePage.Page;
                      currentPage.DocId = updatePage.DocId;
                    }
                    _isLoading = false;
                    $('.MaskedDiv').hide();
                }
            } else {
                setDirtyBit();
                var errMsg = 'Unable to save. Please try again.';
                if (null != result.UserMessage) {
                    errMsg = result.UserMessage;
                }
                alert(errMsg);
            }

            return false;
        }

        function filterDocs()
        {
            //filter according to the params received
            if(filterTypes !== null && filterTypes !== undefined)
            {
                $('.docType').each(function(){
                    var val = Number($(this).attr("data-docStatusVal"));
                    var index = $.inArray(val, filterTypes);

                    this.parentElement.parentElement.parentElement.style.display = index < 0 ? "none" : "";
                });
            }

            setFolderVisibility();
        }


        function selectAvailableDoc()
        {
            //check if the selected doc is hidden
            if($(".selected:visible")[0] == null)
            {
                var visibleDocs = $(".document:visible");
                if(visibleDocs[0] != null)
                {

                    setDisabledAttr($(".dialog-link"), false);
                    visibleDocs[0].click();
                    enableAll();
                    $(".fields").find("select, input").prop("disabled", false);
                }
                else
                {
                    //There are no docs to load, so we have to manually prompt whether any changes should be saved or not.
                    if(_isDirty)
                    {
                        if( saveOnExit())
                        {
                            _SaveImplHideMessage();
                        }
                        _isDirty = false;
                    }

                    //There are no visible docs, so we have to disable the toolbar and unload the pdf-editor's pages.
                    disableAll();
                    $(".fields").find("select, input").prop("disabled", true);
                    $("#btnFilter").prop("disabled", false);
                    $(".selected").removeClass("selected");

                    unloadDocument();
                }
            }
        }

        function unloadDocument()
        {
            //When all documents are hidden, the information is still contained in the header
            $(".fields.left").find("input").val("");
            _pdfEditor.Refresh([]);
            $("#DocTypeHeader").text("");
            setDisabledAttr($(".dialog-link"), true);
            $(".SelectDocTypeName").text("");
        }

        function setFolderVisibility()
        {

            //go through all the folders, those w/o children that are visible are hidden
            var $folders = $(".folder");  //get all the folders
            $folders.each(function()
            {
                this.style.display = ""; //the folder needs to be visible here, or jquery will always detect the children as not visible
                var $list = $(this).children("ol");
                if(!$(this).hasClass('close') && $list.children("li:visible")[0] == null)
                    this.style.display = "none";

            });

        }

        function getCurrentDocumentId() {
            if (!_isBatch) {
                return _docId;
            }

            // Grab the batch editor's current document ID. While the batch
            // editor will eventually set _docId, that will not happen until
            // after the entire PDF editor has been loading, resulting in us
            // potentially using the old document ID if we stick with just _docId.
            // Fall back to the primary document ID if the batch editor does not
            // have a doc currently selected. This can happen on page load before
            // the batch editor initializes.
            return $('#BatchEditNavigation').triggerHandler('GetCurrentDocumentId') || _docId;
        }

        function canEditDocument(docId) {
            if (ML.UseFallbackEditBehavior) {
                return ML.CanEdit;
            }

            var documentId = docId || getCurrentDocumentId();
            return !f_arrayContains(NonEditableDocIds, documentId);
        }

        function documentIsESigned() {
            if (ML.UseFallbackEditBehavior) {
                return false;
            }

            var documentId = getCurrentDocumentId();
            return f_arrayContains(SignedDocIds, documentId);
        }

        function setPageLayoutButtonsDisabled() {
            var canEdit = canEditDocument();
            $("#btnRotateLeft,#btnRotateRight,#btnMovePage,#btnExtractPages,#btnBatchRotate,#btnDeletePages,#btnInsertPages,#btnExtractPages#btnMovePage").prop('disabled', !canEdit);
        }

        function setCreateEditableCopyDisplay(isModifyPageLayoutTab) {
            $('#CreateEditableCopyCell').toggle(isModifyPageLayoutTab && documentIsESigned());
        }

        function createEditableCopy() {
            var data = {
                DocId: _docId
            };

            var result = gService.loanedit.call('CreateEditableCopy', data);
            if (result.error) {
                handleError(result);
                return;
            }

            // Remove the selected attribute for the doc we're editing
            // so the copy can be selected.
            $(".selected").removeClass('selected');

            var docInfo = JSON.parse(result.value.DocInfo);
            addDocumentToBatchEditList(docInfo.DocId, docInfo.FolderName, docInfo.DocTypeName, docInfo.Description, docInfo.Status, true /*selectInsertedDoc*/);

            $('li[data-docid="' + docInfo.DocId + '"]').click();
        }

        var _dataKey = null;
        var _canEditAnnotations = null;
        var _EditOnlyConditions = null;
        var _uniqueKey = null;
        var _isBatch = <%=AspxTools.JsBool(this.IsBatch)%>;
        var _noteTemplate = null;
        var _noteThumbTemplate = null;
        var _childWindow = null;
        var _closeWindow = false;
        var g_docsToTrimOnSave = [];
        jQuery(function($) {
            _docId = jQuery('#DocId').val();
            _dataKey  = 'pdfFieldData';
            _canEditAnnotations = $('#<%= AspxTools.ClientId(CanEditAnnotations) %>').val() == 'True';
            _uniqueKey =  $('#<%= AspxTools.ClientId(UniqueKey) %>').val();
            _noteTemplate = $( "#noteTemplate" ).template();
            _noteThumbTemplate = $( "#noteThumbTemplate" ).template();
            _EditOnlyConditions = $('#EditConditionsOnly').val() == 'True';

            var div = $('#sigDiv');
            if( div.length > 0 ){
                var width = div.children('img').width();
                var height = div.children('img').height();
                div.height(height);
                div.width(width);
                div.children('img').width('100%');
                div.children('img').height('100%');
            }

            $(document).keyup(function(e){
                //46 == delete key
                if(e.keyCode == 46 && $('#AllTabSaveFunctionality, #AllTabFunctionality').hasClass("Modify"))
                {
                    DeleteSelectedRanges();
                }
            });

            $('#btnSave').click(function(){_Save(false);});
            $('#btnPrint').click(function() {
                if( _isDirty )
                {
                    if( saveOnExit() ) {
                        saveMe(false);
                    }
                }
                window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + _docId, '_parent');
            });
            $("#btnPrevious").click(function() { _pdfEditor.MovePrevPage(); });
            $("#btnNext").click(function() { _pdfEditor.MoveNextPage(); });
            $("#btnRotateLeft").click(function() { _pdfEditor.RotateSelectedPages(-90); });
            $("#btnRotateRight").click(function() { _pdfEditor.RotateSelectedPages(90); });
            $('input[type=text],select').change(setDirtyBit);
            $('#PasteAnnotations').click(function(e){
                e.preventDefault();
                var list = _pdfEditor.GetSelectedPdfFields();
                if (list.length < 1) {
                    return false;
                }


                $.each(list, function(idx,o){
                    $('.maximize-note:visible', o).mousedown();
                });


                if(false == confirm("WARNING: This will permanently paste the selected note(s) and save any changes that you’ve made to the document. The EDocs editor will be saved & closed while the PDF is regenerated. \nClick OK to paste, save, and close. Click Cancel to keep the editor open.")){
                    return false;
                }

                $.each(list, function(idx,o){
                    var data = $(o).data(_dataKey);
                    if(data.cssClass == 'pdf-field-esign-signature' || 
                        data.cssClass == 'pdf-field-esign-initial' ||
                        data.cssClass == 'pdf-field-esign-date')
                    {
                        // Just in case
                        return true;
                    }
                   data.ApplyToPDF = true;
                });


                _closeWindow = true;
                setDirtyBit();
                _Save();
                return false;

            });

            $('.removeAnnotationsBtn').on('click', function(){
                var list = _pdfEditor.GetSelectedPdfFields();
                if (list.length < 1) {
                    return;
                }
                setDirtyBit();
                $.each(list, function(idx, o) { _pdfEditor.RemovePdfField(o);});
                _recordEvent(5);
            });

            $('#btnMinimize,#btnMaximize').on('click', function(){
                var selector = this.id === 'btnMinimize' ?  '.minimize-note:visible' : '.maximize-note:visible';
                var temp = $(selector);
                $(selector).mousedown();
            });

            $('#btnFilter').on('click', function(){
            filtering = true;
            showModal(<%= AspxTools.SafeUrl("/newlos/ElectronicDocs/FilterOptions.aspx") %>, {filterTypes: filterTypes}, "dialogHeight:200px; dialogWidth:250px;", null, function(result){
                var value = result.value;
                    if(value != null && value != undefined)
                        filterTypes = value;
                    filterDocs();
                    filtering = false;
                });

            });

            $('#btnPrintAnnotations').click(function(){
                var bCanEditAnnotations = $('#<%= AspxTools.ClientId(CanEditAnnotations) %>').val() == 'True';
                var pages = JSON.stringify(_pdfEditor.GetPages());
                var annotations = _GetAnnotationSaveData(bCanEditAnnotations);
                var args = {
                    'LoanID' : <%= AspxTools.JsString(LoanID) %>,
                    'AppID' : <%= AspxTools.JsString(AppId) %>,
                    'DocID' : _docId,
                    'Annotations' : annotations,
                    'Pages' : pages
                };

                var result = gService.loanedit.call("StorePrintNoteData", args);

                if( result.error) {
                    var errMsg = 'Unable to save. Please try again.';
                    if (null != result.UserMessage) {
                        errMsg = result.UserMessage;
                    }
                    alert(errMsg);
                    return;
                }

                window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docId=' + _docId + '&cmd=viewpdfwithnotes&k=' + result.value.Key, '_self');

                return false;
            });

            var $disableInsert = $('#DisableInsert');
            if($disableInsert.length == 0) {
                $('#btnInsertPages').click(function(){
                    modeless_popup('InsertEDocPage.aspx', function(ret){
                    if (!ret.OK) {
                        return;
		            }
                      var transDocToInsert = JSON.parse(ret.docToInsert);
                      _pdfEditor.InsertPages(transDocToInsert, ret.AfterPage, ret.ReplaceCount);
                      if(ret.DeleteFromSource)
                      {
                        var serviceArgs = {
                            'DocId': ret.DocId,
                            'Version': ret.DocVersion,
                            'Json': ret.NewSource

                        };
                        g_docsToTrimOnSave.push(serviceArgs);
                      }

                      _recordEvent(0);
                    });
                });
            }
            else {
                disableButton($('#btnInsertPages').attr('title', $disableInsert.val()));
            }

            $('#btnDeletePages').click(function(){
                modeless_popup('DeleteEDocPage.aspx', function(ret){
                    if (ret.OK) {
                        if(ret.Selected){
                            DeleteSelectedRanges();
                        }
                        else{
                            var word = ret.NumOfPagesDelete > 1 ? 'these'  : 'this';
                            if( _pagesHaveAnnotations(ret.FromPage, ret.NumOfPagesDelete ) &&
                                false == confirm('Deleting ' + word + ' will also delete notations placed on this page.' )){
                                return;
                            }
                            _recordEvent(2);
                            _pdfEditor.DeletePages(ret.FromPage, ret.NumOfPagesDelete);
                        }
                    }
                });
            });

            if (!ML.UseFallbackEditBehavior) {
                $('#btnCreateEditableCopy').click(createEditableCopy);
            }

            function DeleteSelectedRanges()
            {
                var ranges = _pdfEditor.GetSelectedRanges();
                var word = 'this page';
                if(ranges.length > 2 || ranges[0].count > 1) word = 'these pages';
                if( _pageRangesHaveAnnotations(ranges) &&
                    false == confirm('Deleting ' + word + ' will also delete notations placed on ' + word + '.' )){
                    return;
                }

                _recordEvent(2);
                _pdfEditor.DeleteSelectedPages(ranges);
            }

            $('#btnBatchRotate').click(function(){
                modeless_popup('BatchRotateEDoc.aspx', function(ret){
                if (ret.OK) {
                    _pdfEditor.BatchRotate(ret.From, ret.To, ret.rotation);
                }
                });
            });

            $('#btnExtractPages').click(function(){
                _splitDocClick();
            });

            $('#btnMovePage').click(function(){
                modeless_popup('MoveEDocPage.aspx', function(ret){
                if( !!!ret.OK  ){
                    return;
                }

                _pdfEditor.MovePages(ret.From, ret.PageCount, ret.AfterPage);
                });
            });

            $('#EditConditionAssociations').click(function(){
                var LoanId = ML.sLId;
                var DocId = _docId;
                var url = <%= AspxTools.SafeUrl(Page.ResolveUrl("~/newlos/ElectronicDocs/EditConditionAssociation.aspx")) %> + "?LoanId=" + LoanId + "&DocId=" + DocId;
                var w = window.open(url, "EditConditionAssociations", "width=500,height=670,resizable=yes,scrollbars=yes,status=yes");
                w.focus();
            });

            $(document).keyup(function(e){

                var keyPressed = e.which;
                if( keyPressed !== 46 && keyPressed !== 8 ) {
                    return;
                }
                $.each(_pdfEditor.GetSelectedPdfFields(),function(idx,o){
                    var $this  = $(o);
                    if( $this.hasClass('saved')) {
                        return; //cannot delete its already been saved.
                    }
                    if( $this.hasClass('pdf-field-signature-img') || $this.hasClass('pdf-field-date')) {
                        _pdfEditor.RemovePdfField(o);
                    }
                });
            });

        });
        $(window).on("load", function() {
            $('#AssignRecipientBtn').click(function() {
                var selectedField = _pdfEditor.GetSelectedPdfFields();
                if(selectedField.length == 1) {
                    OpenRecipientPicker(selectedField[0]);
                }
            });

            $(document).on('click', '.RolePickerOkBtn', function() {
                var rolePicker = $(this).closest('.RecipientPicker');
                var checked = rolePicker.find('input[type="radio"][name="recipient"]:checked');
                if(checked.length == 0) {
                    alert("Please select a role");
                    return;
                }

                var recipient = checked.val();

                var role = null;
                var borrower = null;
                recipientDescription = "";
                
                if(recipient == 'role') {
                    role = rolePicker.find('.AgentRole').val();
                    recipientDescription = rolePicker.find('.AgentRole option:selected').text();
                }
                else {
                    recipientDescription = recipient;
                    if(recipient == "Borrower") {
                        borrower = <%=AspxTools.JsString(E_BorrowerModeT.Borrower.ToString("D")) %>;
                    }
                    else {
                        borrower = <%=AspxTools.JsString(E_BorrowerModeT.Coborrower.ToString("D")) %>
                    }
                }

                LQBPopup.Return({borrower: borrower, role: role, recipientDescription: recipientDescription });
            });

            $(document).on('click', '.RolePickerCancelBtn', function() {
                LQBPopup.Return(null);
            });

            $(document).on('click', '.recipient', function() {
                var rolePicker = $(this).closest('.RecipientPicker');
                var checked = rolePicker.find('input[type="radio"][name="recipient"]:checked');
                rolePicker.find('.AgentRole').prop('disabled', checked.val() != 'role');
            });

            $(document).on('dblclick', '.pdf-field-esign-signature.pdf-pdf-field, .pdf-field-esign-initial.pdf-pdf-field, .pdf-field-esign-date.pdf-pdf-field', function() {
                $('#AssignRecipientBtn').click();
            });

            function _setupAnnotations(annotations, eSignTags){
                $.each(annotations, function() {
                    var oDiv = _pdfEditor.AddPdfField(this);
                    _onPdfFieldDrop(null, oDiv);
                    $(oDiv).data('__HIDDEN__', false); // Assume all visible
                });
                $.each(eSignTags, function() {
                    var oDiv = _pdfEditor.AddPdfField(this);
                    _onPdfFieldDrop(null, oDiv);
                    $(oDiv).data('__HIDDEN__', false); // Assume all visible
                });
                $.each(_pdfEditor.GetFields(), function() {resizeTextArea.call(this);});
            }

            //Note: EDocPages is magically inserted from the codebehind.
            _pdfEditor = new LendersOffice.PdfEditor('editor', EDocPages, {
                AllowEdit : canEditDocument,
                GenerateFullPageSrc : GetPageUrl,
                GenerateThumbPageSrc: GetThumbUrl,
                OnPageStatusChanged : OnPageChange,
                OnPageModified : setDirtyBit,
                OnPageMoved : function() { _recordEvent(4); },
                OnError : function(evt, msg) { ForceQuit(msg);},
                OnInitialDropRenderField : _onInitialDropRenderField,
                OnPdfFieldSelectedStop : _onPdfFieldSelectedStop,
                OnRenderPdfFieldContent : _renderPdfFieldContent,
                OnPdfFieldDrop : _onPdfFieldDrop,
                CanEditAnnotations : _canEditAnnotations,
                EmptyImgSrc : gVirtualRoot + '/images/pixel.gif',
                PageBottomPadding : 10,
                OnPageSelectionStop: function(evt, selectedIndexes){
                    var numSelected = selectedIndexes.length;
                    $('#NumSelectedPages span.value').text(numSelected);
                    //Using visibility: hidden keeps the text from twitching when switching back and forth between 1 page selected.
                    $('#NumSelectedPages span.plural').css({visibility: (numSelected == 1 ? "hidden" : "visible")});
                }
            });
            _pdfEditor.Initialize(function(){
                _setupAnnotations(Annotations, ESignTags);
                $('.MaskedDiv').hide();
                _isLoading = false;

            });

            _onPdfFieldSelectedStop(null, 0);

            $('#tabs').tabs({
                active: 0,
                activate: function(event, ui) {
                    setNoteVisibility(ui.newPanel[0]);
                },
                create: function(event, ui) {
                    setNoteVisibility(ui.panel[0]);
                }
            });

            function setNoteVisibility(currentPanel){
                currentPanel = currentPanel || $($('#tabs').find('.ui-tabs-active').find("a").attr("href"))[0];
                if (currentPanel == null) return;

                var showNotes = currentPanel.id === '<%= AspxTools.ClientId(ViewInternalNotes) %>';
                var showESignTags = currentPanel.id === 'ViewESignTags';
                var isModifyPageLayout = currentPanel.id === 'ModifyPageLayout';

                if (isModifyPageLayout) {
                    setupModifyPageLayout();
                }
                else {
                    removeModifyPageLayout();
                }

                setCreateEditableCopyDisplay(isModifyPageLayout);

                $('#btnPrint').css('display', showNotes ? "none" : ""); // hide the print button on ViewInternal

                _pdfEditor.GetFields().each(function() {
                    var $this = $(this);
                    if(_IsSignature(this) && showNotes){
                        _ToggleInteractive(this, showNotes);
                    }
                    else {
                        var isESignTag = IsESignTag(this);
                        var show = isESignTag ? showESignTags : showNotes;
                        $this.toggleClass('disable-selectable', !show);
                        $this.data("__HIDDEN__", show ? false : true);
                        var thumbData = $this.css('visibility', show ? '' : 'hidden').data('pdfThumbData');
                        $this.removeClass('ui-selected pdf-first-selected');
                        if( thumbData ) {
                            thumbData.Thumb.toggle(show);
                        }
                    }
                });

                _pdfEditor.ResetSelectedPdfFields();
                _onPdfFieldSelectedStop(null, 0);
            }

            var $BatchEditNav = $('#BatchEditNavigation');
            if($BatchEditNav.length > 0)
            {
                $BatchEditNav.trigger('Initialize', {
                    Editor: _pdfEditor,
                    DocInfo: DocDetails,
                    DocPages: EDocPages,
                    DocAnnotations: Annotations,
                    ESignTags: ESignTags,
                    TabSwitch: setNoteVisibility,
                    Header: $("#Header"),
                    BeforeDocSwitch: function(){
                        page_unload(false);
                        resetDirty();
                        return true;
                    },
                    AfterDocSwitch: function(docInfo){
                        _docId = docInfo.DocumentId;
                        $('#DocId').val(docInfo.DocumentId);
                        $('#Version').val(docInfo.Version);
                        $('#m_docDescription').val(docInfo.InternalDescription);
                        $('#m_publicDescription').val(docInfo.Description),
                        $('.SelectedDocTypeName', '.DocTypePicker').text(docInfo.Folder + " : " + docInfo.DocType);
                        $('.SelectedDocTypeId', '.DocTypePicker').val(docInfo.DocTypeId);

                        <% if (AllowHideEDocsFromPML) { %>
                        $('#ulHideFromPML').css('visibility', docInfo.HasPmlRoles ? '' : 'hidden');
                        $('#m_HideFromPML').prop('checked', docInfo.HideFromPmlUsers);
                        <% } %>

                        PagesWithAnnotations = docInfo.PagesWithAnnotations;
                        setNoteVisibility();
                        updateDocTypeHeader();
                        setPageLayoutButtonsDisabled();

                        g_ConditionAssociations = {};

                        // Whenever the displayed document changes, we want to
                        // reset the audit history key to avoid a situation
                        // where we try to record audits for the wrong doc.
                        storageKey = '';
                    },
                    SplitDocs: _splitSingleDoc,
                    SetupAnnotations: _setupAnnotations,
                    ViewDoc: function(docId){
                        window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + docId, "_self");
                    }
                });
                _pdfEditor.SetLeftMargin($BatchEditNav.width() + 20);
                _pdfEditor.ToggleThumbnailPanel();
                filterDocs();
                selectAvailableDoc();
            }

            function setupModifyPageLayout(){
                _pdfEditor.EnableFullscreenThumbnail();
                $('#AllTabSaveFunctionality, #AllTabFunctionality, #NumSelectedPages').addClass("Modify");
                $("#modifyTD").append($("#btnFilter"));
            }

            function removeModifyPageLayout(){
                _pdfEditor.DisableFullscreenThumbnail();
                $('#AllTabSaveFunctionality, #AllTabFunctionality, #NumSelectedPages').removeClass("Modify");
                $("#nonmodifyTD").append($("#btnFilter"));
            }

            var filter = ['.esign-tag'];
            if( canEditDocument() ) {
                filter.push('.signature-field');
            }
            if( _canEditAnnotations  ) {
                filter.push('.annotation-field');
            }

            if( filter.length > 0 ) {
                $(filter.join(',')).draggable({revert:'invalid', helper:'clone', zIndex: 2700,
                    start : function() {
                        $('#editor').css('z-index', '-1' ); //needed for 6 and 7 so the draggable doesnt end in the back of the editor
                    },
                    stop : function() {
                        $('#editor').css('z-index', '' );
                    }
                });
             }

            updateDocTypeHeader();
            setPageLayoutButtonsDisabled();

            if(_EditOnlyConditions){
                $('#<%= AspxTools.ClientId(m_docDescription) %>',
                    '#<%= AspxTools.ClientId(m_publicDescription) %>',
                    '#<%= AspxTools.ClientId(m_docStatus) %>',
                    '#<%= AspxTools.ClientId(m_HideFromPML) %>'
                ).prop('disabled', true);

            }

            onStatusChange();

            resetDirty();
            window._onPdfFieldDrop = _onPdfFieldDrop;

            // Call any resize events.
            $(window).resize();
        });

        function handleError(result) {
            var errMsg = 'Unable to process data. Please try again.';

            if (!!result.UserMessage){
                errMsg = result.UserMessage;
            }
            ForceQuit(errMsg);
        }
        function ForceQuit(msg) {
            resetDirty();
            alert(msg);
            //If the doc list is still open, refresh it. Otherwise, redirect us to the doc list.
            if( window.opener && !window.opener.closed ) {
                try
                {
                //Note that window.opener.location.reload() will not work, since for some reason it
                //causes this function to terminate immediately (even if it's in a try/catch)
                    window.opener.location.href = window.opener.location.href;
                }catch(e){}
                onClosePopup();
            }
            else
            {
                window.location =  "DocList.aspx?loanid="+ <%= AspxTools.JsString(LoanID) %>;
            }
        }

        function _setSelectedRanges(ranges)
        {
            _pdfEditor.SetSelectedRanges(ranges);
        }

        function _splitSingleDoc(ranges, defaultDocType, onClose)
        {
            var ranges = ranges || _pdfEditor.GetSelectedRanges();
            if(ranges.length == 0)
            {
                alert("Please select a range of pages to split into a new document.");
                return;
            }
            modeless_popup('SplitEDocV2.aspx', function(ret){
                if(ret.OK)
                {
                    _displayStatusMessage('Splitting ...');
                    window.setTimeout(function() { _splitDocImpl(ret.result);}, 200);
                }

                if(typeof(onClose) == 'function') onClose(ret.OK);

            }, defaultDocType, { selectedRanges: ranges });
        }

        function _splitDocClick()
        {
            if(_childWindow)
            {
                var temp = _childWindow;
                //If we have an active window, just update it and return
                var selected = _pdfEditor.GetSelectedRanges();
                //jQuery.trigger didn't work at all cross-window, so we'll use this instead.
                _childWindow.trigger && _childWindow.trigger("AddNewRange", JSON.stringify(selected['new']));
                return;
            }

            //Otherwise, we need to make a child window.
            var oldText;
            $textDiv = $('div', '#btnExtractPages');

            var settings = {
                disableAction: function(){
                    disableTabs();
                    disableOperations();
                    enableButton($('#btnExtractPages'));
                    _pdfEditor.TrackNewSelections(true);

                    oldText = $textDiv.text();
                    $textDiv.text('Add');

                },
                enableAction: function(){
                    enableTabs();
                    enableOperations();
                    _pdfEditor.TrackNewSelections(false);

                    $textDiv.text(oldText);
                }
            };

            var extraParams = {
                OnRangeChange: function(ranges){
                    _pdfEditor.ClearNewSelections();
                    _setSelectedRanges(JSON.parse(ranges));
                },
                OnAddFinish: function(){
                    _pdfEditor.ClearNewSelections();
                },
                OnRangeHover: function(range){
                    _pdfEditor.HighlightRange(range);
                }
            }


            modeless_popup('SplitEDoc.aspx', function(ret){
                if(ret.OK)
                {
                    _displayStatusMessage('Splitting ...');
                    window.setTimeout(function() { _splitDocImpl(ret.result);}, 200);
                }
            },
            extraParams,
            settings);
        }

        var _childWindowArgs = null;
        function SetupModelessEnvironment(dialogWindow){
            dialogWindow.dialogArguments = JSON.parse(dialogWindow.opener.GetChildWindowArgs());
            dialogWindow.dialogArguments.callback = function(){ 
                dialogWindow.opener.ChildWindowClosing(dialogWindow.dialogArguments);
            };
        }
        function GetChildWindowArgs()
        {
            return JSON.stringify(_childWindowArgs);
        }

        function GetChildWindowArgsRaw()
        {
            return _childWindowArgs;
        }

        function ChildWindowClosing() {
            //Placeholder, gets set when we create a child window.
        }


        var _nonModelessPages = ["SplitEDoc.aspx", "InsertEDocPage.aspx", "DeleteEDocPage.aspx", "MoveEDocPage.aspx", "BatchRotateEDoc.aspx"];
        var _nonModelessParams = { "SplitEDoc.aspx": "width=1000,height=365"};
        //Modeless dialog contract:
        //The parent window passes the child a callback function that takes at least one parameter (args)
        //The child must call the callback function as part of its <body onunload> event.
        //This covers every reasonable means of closing the child - hitting some sort of OK or cancel button,
        //or just closing the window by clicking the X close button or using a keyboard shortcut.
        //Unfortunately, it doesn't seem possible to bind the event from the parent :(
        //Also unfortunately, it means that we can't do any server-side postback processing without
        //triggering the callback function. Fortunately that's not an issue here.
        function modeless_popup(page, cb, extraParams, settings) {
            settings = settings || {};
            var disableAction = settings.disableAction || disableAll;
            var enableAction = settings.enableAction || enableAll;

            var args = {
               'TotalPages' : _pdfEditor.GetTotalPages(),
               'CurrentPage' : _pdfEditor.GetCurrentPageNumber(),
               'SelectedRanges': settings.selectedRanges || _pdfEditor.GetSelectedRanges(),
               'ExtraParameters': extraParams
            };

            disableAction();

            //The callback always takes args as its only parameter, so we wrap that call in a
            //function that takes no parameters here to simplify calling it from the child
            args.callback = function(explicitArgs){
                enableAction();

                if(explicitArgs)
                {
                    //If we got an explicitArgs, it'll go away as soon as the child does;
                    //make ourselves a copy.
                    var copy = $.extend(true, {}, explicitArgs);
                    cb(copy);
                }
                else
                {
                    cb(args);
                }
                _childWindow = null;
                _childWindowArgs = null;
                ChildWindowClosing = function() {};
            };

            //We can't notify the child window of events in the parent if it's modeless, so some need to be normal windows.
            if($.inArray(page, _nonModelessPages) != -1)
            {
                var params = (_nonModelessParams[page] ? _nonModelessParams[page] + "," : "") + "resizable=yes,scrollbars=yes,status=yes";
                var url = page + '?loanid=' + <%= AspxTools.JsString(LoanID) %> + '&docid=' + _docId;
                var childName = page.replace(".aspx", "");

                _childWindow = window.open(url, childName, params);

                _childWindowArgs = args;
                ChildWindowClosing = args.callback;
            }
            else
            {
                _childWindow = showModeless('/newlos/ElectronicDocs/' + page + '?loanid=' + <%= AspxTools.JsString(LoanID) %> + '&docid=' + _docId, null, null, args, true);
            }
        }

        function popup(page) {
              var args = {
               'TotalPages' : _pdfEditor.GetTotalPages(),
               'CurrentPage' : _pdfEditor.GetCurrentPageNumber()
            };

            return showModal('/newlos/ElectronicDocs/' + page + '?loanid=' + <%= AspxTools.JsString(LoanID) %>, args);
        }

        function updateDocTypeHeader(){
            $('#DocTypeHeader').text($('#<%=AspxTools.ClientId(m_docTypeList.DescriptionCtrl) %>').text());
        }

        function GetPageUrl(o) {
            return <%= AspxTools.SafeUrl(Tools.GetEDocsPngLink()) %> + '&pngkey=' + encodeURIComponent(o.PngKey) + '&key=' + encodeURIComponent(o.Token)+'&pg=' + o.Page;
        }

        function GetThumbUrl(o) {
            return <%= AspxTools.SafeUrl(Tools.GetEDocsThumbPngLink()) %> + '&pngkey=' + encodeURIComponent(o.PngKey) + '&key=' + encodeURIComponent(o.Token)+'&pg=' + o.Page;
        }

        function OnPageChange () {
            $("#tbPageLabel").val(_pdfEditor.GetCurrentPageNumber() + " of " + _pdfEditor.GetTotalPages())

        }

        //called when a field is drop onto the editor
        //this == div being dropped (element)
        function _onInitialDropRenderField() {
            if( $(this).hasClass('pdf-field-note')) {
                var date =$('#CurrentDate').val();


                var data = $(this).data(_dataKey);
                if (null == data) {
                    data = {};
                    $(this).data(_dataKey, data);
                }

                data.NoteStateType = 'maximized';
                data.width = data.height = 170;
                data.LastModifiedOn = date;

                var o = {'Text' : '', 'LastModifiedOn' : date, 'NoteStateType' : 'maximized', 'width' : data.width, 'height' :data.height};
                _renderNote.call(this, o);
            }
        }

        //Based on state sets the image
        //thumb == jquery object
        function _setThumbNote(thumb, state) {
            thumb.children('img').attr('src', _getThumbNoteImg(state));
        }

        //based on state returns the img url for the thumbnail
        function _getThumbNoteImg(state) {
            return state == 'minimized' ? '../../images/edocs/note-thumb-min.png' :  '../../images/edocs/note-thumb-max.png';
        }

        //renders the note object expects data to be set with the needed info
        //this must be set to the note's context.
        function _renderNote(o) {
            $(this).empty().attr({'min-resize-width': '80', 'min-resize-height' : '80'});
            $.tmpl(_noteTemplate,o).appendTo(this);
            $(this).addClass('pdf-field-note-style');
            $('.minimize-note, .maximize-note', this).click(function(e){
                $(this).mousedown();
            }).mousedown(function(){
                var maximize = $(this).hasClass('maximize-note');

                var parent = $(this).parent().parent().get(0);
                var data = $(parent).data(_dataKey);

                $('.maximize-item, .minimize-item', parent).toggle();

                if( maximize ) {
                    $(parent).removeClass('disable-resize').width(data.width).height(data.height);
                    data.NoteStateType = 'maximized';
                }
                else {
                    $(parent).addClass('disable-resize').width(35).height(35);
                    data.NoteStateType = 'minimized';
                }

                var thumbData = $(parent).data('pdfThumbData');
                _setThumbNote(thumbData.Thumb,  data.NoteStateType  );
                _pdfEditor.EnforceBoundaries(parent);
            });

            $(this).resize(resizeTextArea);

            $('textarea', this).blur(function(){
                var parent = $(this).parent().parent().get(0);
                $(parent).data(_dataKey).Text = $(this).val();

            }).change(setDirtyBit);

            if( o.NoteStateType == 'maximized'){
                $('.minimize-item', this).hide();
                $(this).width(o.width).height(o.height);
            }
            else {
                $('.maximize-item', this).hide();
                $(this).addClass('disable-resize').width(35).height(35);
            }

            if( false === _canEditAnnotations ) {
                setDisabledAttr($('textarea', this), true);
            }

            $(this).triggerHandler('resize');
        }

        //updates the remove notation  status based on how many pdf fields are selected
        function _onPdfFieldSelectedStop(e, count) {
            var currentPanel = $('.ui-tabs-panel:not(.ui-tabs-hide)', '#tabs')[0];
            var showInternalNotes = currentPanel ? currentPanel.id === 'ViewInternalNotes' : true;;
            
            var removeAnnotationsBtns = $('.removeAnnotationsBtn');
            var setRecipientButton = $('#AssignRecipientBtn');
            if(count == 1) {
                setRecipientButton.prop('disabled', false);
                $('#recipientImg').prop('src', <%=AspxTools.JsString(ResolveUrl("~/images/contacts.png"))%>)
            }
            else {
                setRecipientButton.prop('disabled', true);
                $('#recipientImg').prop('src', <%=AspxTools.JsString(ResolveUrl("~/images/contacts_disabled.png"))%>)
            }

            if(showInternalNotes && !_canEditAnnotations) {
                return;
            }

            if ((showInternalNotes && !_canEditAnnotations) || count <= 0) {
                removeAnnotationsBtns.prop('disabled', true).children('img').attr('src', <%=AspxTools.JsString(ResolveUrl("~/images/edocs/remove_notation_disable.png"))%>);
                $('#PasteAnnotations').prop('disabled', true);
                $('.paste-img').attr('src', <%=AspxTools.JsString(ResolveUrl("~/images/edocs/pasted-disabled.png"))%>);
            }
            else {
                removeAnnotationsBtns.prop('disabled', false).children('img').attr('src', <%=AspxTools.JsString(ResolveUrl("~/images/edocs/remove_notation.png"))%>);
                $('#PasteAnnotations').prop('disabled', false);
                $('.paste-img').attr('src', <%=AspxTools.JsString(ResolveUrl("~/images/edocs/pasted.png"))%>);
            }
        }

        //called when a pdf field is rendered ( initial - on load  on drop calls diff method)
        function _renderPdfFieldContent(o) {
            if( o.cssClass == 'pdf-field-note' ) {
                _renderNote.call(this, o);
                if( false === _canEditAnnotations ) {
                    $(this).addClass('master-disable-resize disable-move');
                }
            } else if( o.cssClass == 'pdf-field-highlight' ) {
                $(this).addClass('highlight-field');
                if( false === _canEditAnnotations ) {
                    $(this).addClass('master-disable-resize disable-move');
                }
            } else if( o.cssClass == 'pdf-field-esign-signature' || o.cssClass == 'pdf-field-esign-initial' || o.cssClass == 'pdf-field-esign-date') {
                $(this).addClass(o.cssClass);

                var description = o.RecipientDescription;
                if(o.cssClass == 'pdf-field-esign-signature') {
                    description += " Signature";
                }
                else if(o.cssClass == 'pdf-field-esign-initial') {
                    description += " Initials";
                }
                else if(o.cssClass == 'pdf-field-esign-date') {
                    description += " Sig Date";
                }

                var tooltipDescription = description;

                var pdfFieldTextContainer = $('<div>').addClass('pdf-field-content-text-container').prop('title', tooltipDescription);
                var pdfFieldText = $('<div>').addClass('pdf-field-content-text').text(description);
                pdfFieldTextContainer.append(pdfFieldText);

                return pdfFieldTextContainer;
            }
        }
        function _onPdfFieldDrop(evt, o) {
            var data = $(o).data(_dataKey);
            var content = $('<div class="highlight-field highlight-field-thumb"></div>');

            if( data.cssClass == 'pdf-field-note' ) {
                var tmplData = { src :  _getThumbNoteImg(data.NoteStateType) };
                content = $.tmpl(_noteThumbTemplate,tmplData);
            }
            else if(evt && (data.cssClass == 'pdf-field-esign-signature' || data.cssClass == 'pdf-field-esign-initial' || data.cssClass == 'pdf-field-esign-date')) {
                // Only the initial drop will call this with an evt object.
                OpenRecipientPicker(o);
            }

            _pdfEditor.AddDynamicThumbnail(o, content);
        }

        function OpenRecipientPicker(tag) {
            var picker = $('#RecipientPickerContainer').clone();
            picker.find('.RolePickerCancelBtn').show();
            var $tag = $(tag);

            var oldData = $tag.data(_dataKey);
            if(oldData) {
                var oldBorrower = oldData.AssociatedBorrower;
                var oldRole = oldData.AssociatedRole;
            
                if(oldBorrower != null) {
                    picker.find('.recipient[value="' + oldData.RecipientDescription + '"]').attr('checked', true).prop('checked', true);
                    picker.find('.AgentRole').attr('disabled', true).prop('disabled', true);
                }
                else if(oldRole != null) {
                    // Borrower isn't set, set the role instead.
                    picker.find('.recipient[value="role"]').attr('checked', true).prop('checked', true);
                    picker.find('.AgentRole').find('option[value="' + oldRole + '"]').attr('selected', true).prop('selected', true);
                    picker.find('.AgentRole').attr('disabled', false).prop('disabled', false);
                }
                else {
                    // Nothing is set. Hide the cancel button.
                    picker.find('.RolePickerCancelBtn').hide();
                }
            }

            var targetOffset = null;
            var marginLeft = null;
            var marginTop = null;
            var isTagInViewport = _pdfEditor.IsInEditorViewport(tag);
            if(isTagInViewport) {
                var targetOffsetLeft = $tag.offset().left;
                var targetOffsetTop = $tag.offset().top - 155;
                targetOffset = {top: targetOffsetTop, left: targetOffsetLeft};
            }
            else {
                marginLeft = 0;
                marginTop = 0;
            }

            LQBPopup.ShowElement(picker, {
                width: 300,
                height: 150,
                hideCloseButton: true,
                elementClasses: 'FullWidthHeight, z2800',
                dialogClasses: 'z2800',
                targetOffset: targetOffset,
                marginLeft: marginLeft,
                marginTop: marginTop,
                onReturn: function(recipient) {
                    if(recipient == null) {
                        return;
                    }

                    var data = $tag.data(_dataKey);
                    if (null == data) {
                        data = {};
                        $tag.data(_dataKey, data);
                    }

                    data.AssociatedRole = recipient.role;
                    data.AssociatedBorrower = recipient.borrower;
                    data.RecipientDescription = recipient.recipientDescription;

                    var description = recipient.recipientDescription;
                    if($tag.hasClass('pdf-field-esign-signature')) {
                        description += " Signature";
                    }
                    else if($tag.hasClass('pdf-field-esign-initial')) {
                        description += " Initials";
                    }
                    else if($tag.hasClass('pdf-field-esign-date')) {
                        description += " Sig Date";
                    }

                    var tooltipDescription = description;

                    $tag.find('.pdf-field-content').attr('title', tooltipDescription);
                    $tag.find('.pdf-field-content-text').text(description);
                    setDirtyBit();
                }
            });
        }

        function _pageRangesHaveAnnotations(ranges){
            for(var i = 0; i < ranges.length; i++){
                if(_pagesHaveAnnotations(ranges[i].Start + 1, ranges[i].Count)) return true;
            }
            return false;
        }

        function _pagesHaveAnnotations(pgStart, numOfPagesToDelete){
            var maxPage = pgStart + numOfPagesToDelete;
            var hasAnnotations = false;
            if( _canEditAnnotations ) {
                $.each(_pdfEditor.GetFields(), function(){
                    var position = $(this).position();
                    var offset = $(this).offset();

                    //if(console && console.log) console.log(
                    var annotationPage = $(this).data(_dataKey).pg;
                    if( annotationPage >= pgStart && annotationPage < maxPage ) {
                        hasAnnotations = true;
                        //Cancel the .each
                        return false;
                    }
                });
            }
            else {
                for( var i = 0; i<PagesWithAnnotations.length; i++) {
                    var pg = PagesWithAnnotations[i];
                    if( pg >= pgStart && pg < maxPage ) {
                        hasAnnotations = true;
                        break;
                    }
                }
            }
            return hasAnnotations;
        }

    function _splitDocImpl(input)
    {
        var splitInfo = $.parseJSON(input).sort(function(a, b) { return parseInt(b.StartPage) - parseInt(a.StartPage);});
        var length = splitInfo.length;
        for (var i = 0; i < length; i++)
        {
            var item = splitInfo[i];
            item.EDocPages = _pdfEditor.GetPages().slice(item.StartPage - 1, item.EndPage);
            if(!item.AppId) item.AppId = <%= AspxTools.JsString(AppId) %>;
            if(!item.Conditions) item.Conditions = [];

            splitInfo[i] = item;
        }

        var str = JSON.stringify(splitInfo);

        var args = {
            'LoanId' : <%= AspxTools.JsString(LoanID) %>,
            'Json' : str,
            'DocumentId' : getCurrentDocumentId()
        };

        var result = gService.loanedit.call("SplitNewDoc", args);
        if (result.error) {
            handleError(result);
            _hideStatusMessage();
            return;
        }

        if(<%=AspxTools.JsBool(IsBatch)%>)
        {
            //generate the List Items here
            for(var i = 0; i < splitInfo.length; i++)
            {
                docListUpdate = true;

                var docTypeDescription = splitInfo[i].DoctypeDescription.split(":");
                var docId = result.value["DocId" + i];
                var docTypeName = $.trim(docTypeDescription[1]);
                var docFolderName = $.trim(docTypeDescription[0]);
                var desc = splitInfo[i].Description;
                var status = splitInfo[i].Status;
                addDocumentToBatchEditList(docId, docFolderName, docTypeName, desc, status);
            }

            filterDocs();
            setFolderVisibility();
        }

        //This delete only works because of the sort we did up there.
        for (var i = 0; i < length; i++)
        {
            var item = splitInfo[i];
            _pdfEditor.DeletePages(parseInt(item.StartPage-1), parseInt(item.EndPage) - parseInt(item.StartPage) + 1);
        }



        _recordEvent(3);
        _hideStatusMessage();

        RefreshOpener();
    }

    function addDocumentToBatchEditList(docId, docFolderName, docTypeName, desc, status, selectInsertedDoc) {
        docListUpdate = true;

        var li = document.createElement("li");
        var classes = "document drop-target ui-droppable";
        if (selectInsertedDoc) {
            classes += " selected";
        }
        li.className = classes;
        li.id = "guid_"+ docId;
        li.setAttribute("data-docid", docId);

        var notesDiv = document.createElement("div");
        notesDiv.className = "notes";

        var nameDiv = document.createElement("div");
        var nameSpan = document.createElement("span");
        nameSpan.className = "doc-type-name";
        nameSpan.innerText = docTypeName;
        nameDiv.appendChild(nameSpan);

        var noteDiv = document.createElement("div");
        var noteSpan = document.createElement("span");
        noteSpan.className = "doc-notes";
        noteSpan.innerText = desc;
        noteDiv.appendChild(noteSpan);

        var typeDiv = document.createElement("div");
        var typeSpan = document.createElement("span");
        typeSpan.className = "docType";

        var color = "darkblue";
        var friendlyStatus = "";

        switch(status)
        {
            case "<%=AspxTools.JsNumeric(EDocs.E_EDocStatus.Approved) %>":
            friendlyStatus = "Protected";
            color="darkgreen";
            break;
            case "<%=AspxTools.JsNumeric(EDocs.E_EDocStatus.Screened) %>":
            friendlyStatus = "Screened";
            break;
            case "<%=AspxTools.JsNumeric(EDocs.E_EDocStatus.Rejected) %>":
            friendlyStatus = "Rejected";
            break;
            case "<%=AspxTools.JsNumeric(EDocs.E_EDocStatus.Obsolete) %>":
            friendlyStatus = "Obsolete";
            break;
        }
        typeSpan.setAttribute("style", "color: "+color+"; font-weight: bold;");
        typeSpan.innerText = friendlyStatus;
        typeSpan.setAttribute("data-docStatusVal", status);

        typeDiv.appendChild(typeSpan);
        notesDiv.appendChild(nameDiv);
        notesDiv.appendChild(noteDiv);
        notesDiv.appendChild(typeDiv);

        var linkDiv = document.createElement("div");
        linkDiv.className = "link";
        var linkSpan = document.createElement("span");
        linkSpan.className = "loading search-ignore";
        linkSpan.innerText = "Loading...";
        var linkAnchor = document.createElement("a");
        linkAnchor.className = "open-pdf";
        linkAnchor.href = "#";
        linkAnchor.innerText = "open pdf";

        linkDiv.appendChild(linkSpan);
        linkDiv.appendChild(linkAnchor);

        li.appendChild(notesDiv);
        li.appendChild(linkDiv);

        var created = false;
        $("span.doc-type-folder").each(
        function()
        {
            if($.trim(this.innerText) == docFolderName)
            {
                var $list = $(this).next();
                $list.find(".doc-type-name").each(function()
                    {
                        if(docTypeName <= $.trim(this.innerText))
                        {
                            $(li).insertBefore($(this).parent().parent().parent());
                            created = true;
                            return false;
                        }
                    });
            }
            else if(docFolderName < $.trim(this.innerText))
            {
                var folderLI = document.createElement("li");
                folderLI.className = "folder";

                var docTypeFolderSpan = document.createElement("span");
                docTypeFolderSpan.className = "doc-type-folder";
                docTypeFolderSpan.innerText = docFolderName;

                folderLI.appendChild(docTypeFolderSpan);

                var ol = document.createElement("ol");
                ol.className = "document-name-listing";

                ol.appendChild(li);

                folderLI.appendChild(ol);

                $(folderLI).insertBefore($(this).parent());
                created = true;
            }

            if(created)
                return false;
        });

        if(!created)
        {
            var folderLI = document.createElement("li");
                folderLI.className = "folder";

                var docTypeFolderSpan = document.createElement("span");
                docTypeFolderSpan.className = "doc-type-folder";
                docTypeFolderSpan.innerText = docFolderName;

                folderLI.appendChild(docTypeFolderSpan);

                var ol = document.createElement("ol");
                ol.className = "document-name-listing";

                ol.appendChild(li);

                folderLI.appendChild(ol);

            $(".folder-name-listing").append(folderLI);
        }

        $('#BatchEditNavigation').trigger('InitNewDoc', li);
    }

    function onStatusChange()
    {
        var status = $('#<%= AspxTools.ClientId(m_docStatus)%>').val();
         if( status == '3')
         {
            $('#pnlStatusDescription').css('display', '');
         }
         else
         {
            $('#pnlStatusDescription').css('display', 'none');
         }
    }
    </script>
<form id="form1" runat="server">
    <asp:HiddenField runat="server" ID="Version" />
    <asp:HiddenField runat="server" id="UniqueKey" />
    <asp:HiddenField runat="server" ID="CanEditAnnotations" />
    <asp:HiddenField runat="server" id="CanEditEdocs" />

    <div id="Header">
        <h4 class="page-header">Edit Document - <span id="DocTypeHeader" ></span></h4>
        <div id="tabs">
            <div id="MaskedDiv" class ="MaskedDiv"></div>
            <ul>
                <li runat="server" id="InternalNoteTab" class="tab"><a href="#<%= AspxTools.ClientId(ViewInternalNotes) %>" >Internal Notes</a></li>
                <li id="ViewDocumentInfoTab" class="tab"><a href="#ViewDocumentInfo">Document Info</a></li>
                <li id="ModifyPageLayoutTab" class="tab"><a href="#ModifyPageLayout">Page Layout</a></li>
                <li runat="server" id="ESignTagsTab" class="tab"><a href="#ViewESignTags">E-Sign Tags</a></li>
            </ul>
            <hr />
            <table id="AllTabSaveFunctionality" class="button_container">
                <tr>
                    <td class="tableCellHeight" >
                        <button class="op_button" id="btnSave" type="button">
                            <img id="Img1" src="~/images/edocs/save.png" class="standard_img" runat="server" />
                            <div>
                                Save
                            </div>
                        </button>
                    </td>
                    <td class="tableCellHeight">
                        <button class="op_button" type="button" id="btnPrint">
                            <img id="Img3" src="~/images/edocs/print.png" class="standard_img" runat="server" />
                            <div>
                                Print
                            </div>
                        </button>
                    </td>
                    <td id="CreateEditableCopyCell">
                        <button class="op_button" id="btnCreateEditableCopy" type="button">
                            <div>
                                Create Editable Copy
                            </div>
                        </button>
                    </td>
                    <td id="modifyTD"></td>
                </tr>
                <tr>
                    <td id="nonmodifyTD">
                        <input type="button" id="btnFilter" runat="server" class="op_button op_button_wide_3 op_button_short" style="width:60px; display:block;" value="Filter"/>
                    </td>
                </tr>
            </table>

            <div id="AllTabFunctionality" class="button_container">
                <button id="btnPrevious" class="op_button_arrow" type="button" >
                    <img id="Img7" src="~/images/edocs/previous.png" runat="server" />
                </button>

                <input type="text" id="tbPageLabel" value="" readonly="readonly" type="button" />

                <button id="btnNext"  class="op_button_arrow" type="button">
                    <img id="Img6" src="~/images/edocs/next.png" runat="server"  />
                </button>
            </div>
            
            <div id="RecipientPickerContainer" class="DisplayNone">
                <div class="MainRightHeader">
                Select a Signer Role:
                </div>
                <div class="RecipientPicker Padding5">
                    <ul class="UndecoratedList align-left Padding5">
                        <li>
                            <label>
                                <input type="radio" class="recipient" name="recipient" value="Borrower" />Borrower
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" class="recipient" name="recipient" value="Coborrower" />Coborrower
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" class="recipient" name="recipient" value="role" />Role:
                            </label>
                            <asp:DropDownList Enabled="false" NotEditable="true" runat="server" ID="AgentRole" class="AgentRole"></asp:DropDownList>
                        </li>
                    </ul>
                    <br />
                    <input type="button" id="RolePickerOkBtn" class="RolePickerOkBtn" value="OK" />
                    <input type="button" id="RolePickerCancelBtn" class="RolePickerCancelBtn" value="Cancel" />
                </div>
            </div>

            <div id="ViewESignTags" runat="server" class="ui-tabs-hide">
                <table class="button_container" style="float: left;">
                    <tbody>
                        <tr>
                            <td valign="bottom">
                                <button id="removeTagsBtn" class="removeAnnotationsBtn op_button op_button_wide_3" type="button" disabled="disabled">
                                    <img id="Img5" src="~/images/edocs/remove_notation_disable.png" class="standard_img" runat="server" />
                                    <div>
                                        Remove Tag(s)</div>
                                </button>
                            </td>
                            <td valign="bottom">
                                <button type="button" id="AssignRecipientBtn" class="op_button_wide_3" style="height: 70px; text-align: center; vertical-align: middle;">
                                    <div>
                                        <img id="recipientImg" src="~/images/contacts.png" class="standard_img" runat="server" style="width: 20px; height: 20px;" />
                                    </div>
                                    <div>Assign Role</div>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="button_container" runat="server" id="Table1" style="float: left;">
                    <tr>
                        <td>
                            <div>
                            Drag and drop onto the document:
                            </div>
                            <div class="DragAndDropContainer">
                                <div class="pdf-field-template pdf-field-esign-signature esign-tag" style="width:200px; height:20px;" >
                                    <div class="pdf-field-content pdf-field-content-text-container">
                                        <div class="pdf-field-content-text">Signature</div>
                                    </div>
                                </div>
                                Signature
                            </div>
                            <div class="DragAndDropContainer">
                                <div class="pdf-field-template pdf-field-esign-initial esign-tag" style="width:130px; height:15px;" >
                                    <div class="pdf-field-content pdf-field-content-text-container">
                                        <div class="pdf-field-content-text">Initials</div>
                                    </div>
                                </div>
                                Initials
                            </div>
                            <div class="DragAndDropContainer">
                                <div class="pdf-field-template pdf-field-esign-date esign-tag" style="width:130px; height:15px;" >
                                    <div class="pdf-field-content pdf-field-content-text-container">
                                        <div class="pdf-field-content-text">Sig Date</div>
                                    </div>
                                </div>
                                Signed Date
                            </div>
                        </td>
                    </tr>
                </table>
                <div class="clear"></div>
            </div>
            <div id="ViewInternalNotes"  runat="server"  class="ui-tabs-hide" >
                <table class="button_container" style="float: left;"  >
                    <tbody>
                        <tr>
                            <td class="tableCellHeight"  valign="bottom" class="pad">
                                <button class="op_button op_button_wide_3" type="button" id="btnPrintAnnotations">
                                    <img id="Img2" src="~/images/edocs/print.png" class="standard_img" runat="server" />
                                    <div>
                                        Print Annotations</div>
                                </button>
                            </td>
                            <td   class="tableCellHeight" valign="bottom">
                                <button id="btnRemoveAnnotations" class="removeAnnotationsBtn op_button op_button_wide_3" type="button" disabled="disabled">
                                    <img id="Img4" src="~/images/edocs/remove_notation_disable.png" class="standard_img" runat="server" />
                                    <div>
                                        Remove Notation(s)</div>
                                </button>
                            </td>
                            <td valign="bottom" align="center">
                            <button  id="PasteAnnotations" runat="server" class="op_button op_button_wide_3" NoHighlight="NoHighlight" disabled="disabled" value="Paste Annotations">
                                <asp:Image runat="server" ID="Image2" CssClass="paste-img standard_img"  ImageUrl="~/images/edocs/pasted-disabled.png" /> <br />
                                Paste Annotation
                            </button>

                            </td>

                        </tr>
                        <tr>
                            <td >
                                <button id="btnMinimize" class="op_button op_button_wide_3 op_button_short" type="button">
                                    <img  src="~/images/edocs/minimize.png" runat="server"/>
                                        Minimize all notes
                                </button>
                            </td>
                            <td>
                                <button id="btnMaximize" class="op_button op_button_wide_3 op_button_short" type="button">
                                    <img src="~/images/edocs/maximize.png" runat="server" />
                                        Maximize all notes
                                </button>
                            </td>

                        </tr>
                    </tbody>
                </table>
                <table class="button_container" runat="server" id="DragAndDropTable" style="float: left;">
                    <tr>
                        <td>
                            <div>
                            Drag and drop onto the document:
                            </div>
                            <%-- A spacer div, for when the annotation isn't around --%>
                                <div class="DragAndDropContainer" style="height: 75px; width: 0px"></div>
                            <asp:PlaceHolder runat="server" ID="Fields">
                                <div class="DragAndDropContainer">
                                    <div class="pdf-field-template annotation-field pdf-field-highlight highlight-field" >
                                    </div>
                                    Highlight
                                </div>
                                <div class="DragAndDropContainer">
                                    <div class="pdf-field-template annotation-field pdf-field-note" >
                                    <asp:Image ID="Image1" CssClass="note-img" runat="server" ImageUrl="~/images/edocs/note-field-drag.png" />
                                    </div>
                                    Note
                                </div>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder id="Signature" runat="server" Visible="False">
                                <div class="DragAndDropContainer">
                                    <div class="pdf-field-template pdf-field-signature-img signature-field" id="sigDiv">
                                    <asp:Image ID="SignatureImg" CssClass="pdf-field-content"  runat="server"   />
                                    </div>
                                    Signature
                                </div>
                                <div class="DragAndDropContainer">
                                    <div class="pdf-field-template pdf-field-date signature-field">
                                    <asp:Image ID="DateIMg" CssClass="pdf-field-content" runat="server" ImageUrl="~/ViewEDocPdf.aspx?cmd=date" Width="100%" Height="100%" />
                                    </div>
                                    Date
                                </div>
                            </asp:PlaceHolder>

                        </td>
                    </tr>
                </table>
                    <div class="clear"></div>
            </div>
            <div id="ViewDocumentInfo" class="ui-tabs-hide">
                <table class="button_container">
                    <tbody>
                        <tr>
                            <td class="tableCellHeight">
                                <ml:EncodedLabel ID="Label3" AssociatedControlID="m_docTypeList" runat="server">Type:</ml:EncodedLabel>
                                <uc:DocTypePicker ID="m_docTypeList" OnChange="setDirtyBit();" runat="server"></uc:DocTypePicker>
                                <div class="fields">
                                <ul class="fields left">
                                    <li>
                                        <ml:EncodedLabel AssociatedControlID="m_publicDescription" runat="server">Description:</ml:EncodedLabel>
                                        <asp:TextBox ID="m_publicDescription" runat="server" Width="200px" /></li>
                                    <li>
                                        <ml:EncodedLabel AssociatedControlID="m_docDescription" runat="server">Internal Comments:</ml:EncodedLabel>
                                        <asp:TextBox ID="m_docDescription" runat="server" Width="200px" /></li>
                                </ul>
                                <ul class="fields right">
                                    <li>
                                        <ml:EncodedLabel ID="Label1" AssociatedControlID="m_docStatus" runat="server">Status:</ml:EncodedLabel>
                                        <asp:DropDownList ID="m_docStatus" runat="server"  Width="200px"></asp:DropDownList>
                                </li>
                                <li id="pnlStatusDescription" style="text-align:left">
                                    <ml:EncodedLabel ID="Label2" AssociatedControlID="m_docStatusDescription" runat="server"> Rejected for:</ml:EncodedLabel>
                                    <asp:TextBox ID="m_docStatusDescription" runat="server" Width="200px"/>
                                </li>
                                <li runat="server" id="ConditionAssociations">
                                        <input type="button" value="Edit Condition Associations" id="EditConditionAssociations"  />
                                </li>
                                </ul>
                                <ul id="ulHideFromPML" runat="server" style="padding:0px;margin: 0.9em 5px 0px 0px;list-style-type:none;float:left">
                                    <li>
                                        <asp:CheckBox ID="m_HideFromPML" runat="server" Text="Hide from PriceMyLoan users" onclick="setDirtyBit();" />
                                    </li>
                                </ul>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="ModifyPageLayout" class="ui-tabs-hide Modify">
                <table class="button_container">
                    <tbody>
                        <tr>
                            <td class="tableCellHeight">
                                <button id="btnInsertPages" class="op_button op_button_wide_2" type="button">
                                    <img id="Img9" src="~/images/edocs/insert-pages.png" class="standard_img"  runat="server" />
                                    <div>
                                        Insert Pages</div>
                                </button>
                            </td>
                            <td class="tableCellHeight">
                                <button  id="btnDeletePages" class="op_button op_button_wide_2" type="button">
                                    <img id="Img10" src="~/images/edocs/delete-pages.png" class="standard_img" runat="server" />
                                    <div>
                                        Delete Pages</div>
                                </button>
                            </td>
                            <td class="tableCellHeight">
                                <button id="btnExtractPages" class="op_button op_button_wide_2" type="button">
                                    <img id="Img11" src="~/images/edocs/extract-pages.png" class="standard_img" runat="server" />
                                    <div>
                                        Split Doc</div>
                                </button>
                            </td>
                            <td class="tableCellHeight">
                                <button  id="btnMovePage" class="op_button op_button_wide_2" type="button">
                                    <img id="Img12" src="~/images/edocs/move-page.png" class="standard_img" runat="server" />
                                    <div>
                                        Move Page</div>
                                </button>
                            </td>

                            <td class="tableCellHeight">
                                <button id="btnRotateLeft" class="op_button op_button_wide_2" type="button">
                                    <img id="Img13" src="~/images/edocs/rotate-left.png" class="standard_img" runat="server" />
                                    <div>
                                        Rotate Left</div>
                                </button>
                            </td>
                            <td class="tableCellHeight">
                                <button id="btnRotateRight" class="op_button op_button_wide_2" type="button" >
                                    <img id="Img14" src="~/images/edocs/rotate-right.png" class="standard_img" runat="server" />
                                    <div>
                                        Rotate Right</div>
                                </button>
                            </td>
                            <td class="tableCellHeight" >
                                <button  id="btnBatchRotate" class="op_button op_button_wide_2" type="button">
                                    <img id="Img15" src="~/images/edocs/batch-rotate.png" class="standard_img" runat="server" />
                                    <div>
                                        Batch Rotate</div>
                                </button>
                            </td>
                            <td>
                                <div id="NumSelectedPages">
                                    <span><span class="value">0</span> Page<span class="plural">s</span> Selected</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div id="content">
        <div id='debugConsole'><div id='debugConsoleMsg'></div></div>
        <div id="divStatusPanel" class="StatusPanel" style="display:none">
            <div></div>
        </div>
        <batch:BatchEditDocList id="BatchEditList" runat="server" />

        <div id="editor" style="z-index: 0;position:relative;"></div>
        <div class="clear"></div>
    </div>
</form>

</body>
</html>
