﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShipDocReview.ascx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.ShipDocReview" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<script type="text/javascript">
    var currentSort  = getSortedOrder();
    $(document).ready(function() {
        var sort;

        $("#ReviewSelectionTable").tablesorter({
            headers: {
                0: { sorter: false }
               , 5: { sorter: false }
               , 6: { sorter: false }
               , 7: { sorter: false }
               , 9: { sorter: false }
               , 10: { sorter: false }
            }
        });

        $("#folderSortLink").click(function() {
            var sortedOrder = getSortedOrder();
            currentSort = [[[2, sortedOrder], [3, sortedOrder], [4, sortedOrder]]];
            $("table").trigger("sorton", currentSort);
            setSortedOrder();
            return false;
        });

        $("#modDateSortLink").click(function() {
            var sortedOrder = getSortedOrder();
            currentSort = [[[8, sortedOrder]]];
            $("table").trigger("sorton", currentSort);
            setSortedOrder();
            return false;
        });
        
       $("#statusSortLink").click(function() {
            var sortedOrder = getSortedOrder();
            currentSort = [[[1, sortedOrder]]];
            $("table").trigger("sorton", currentSort);
            setSortedOrder();
            return false;
        });

        $("#docTypeSortLink").click(function() {
            // By putting the current sort in the sorton trigger, it will effectively not sort the individual 
            // column
            // I need to do this instead of setting the header sorter to false because I want the column
            // to be sortable, but only when a different column is clicked, not when this column is clicked.
            // OPM 53189
            $("table").trigger("sorton", currentSort);
            return false;
        });

        $("#applicationSortLink").click(function() {
            // By putting the current sort in the sorton trigger, it will effectively not sort the individual 
            // column
            // I need to do this instead of setting the header sorter to false because I want the column
            // to be sortable, but only when a different column is clicked, not when this column is clicked.
            // OPM 53189
            $("table").trigger("sorton", currentSort);
            return false;
        });
        
        $("#ReviewSelectionTable").on("sortStart", function() {
            selectCBList = $('#ReviewSelectionTable tbody input:checked');
        });
        $("#ReviewSelectionTable").on("sortEnd", function() {
            setClass($('#ReviewSelectionTable tbody')[0]);
            if (initSort) {
                initSort = false;
            }
            else {
                $('#ReviewSelectionTable tbody input').each(function(i, e) {
                    var id = $(e).attr('id');
                    
                    var checked = false;
                    for (var i = 0; i < selectCBList.length; i++) {
                        if (selectCBList[i].id == id) {
                            checked = true;
                            break;
                        }
                    }
                    
                    $(e).prop('checked', checked);
                });
            }
        });
    });
</script>
 <ml:EncodedLabel  runat="server" ID="Warning" Visible ="false" Font-Bold="true" >
    Warning: Some documents required by the shipping template are missing.
</ml:EncodedLabel>
<asp:Repeater runat="server" ID="Documents" OnItemDataBound="Documents_OnItemDataBound">
    <HeaderTemplate>
        <table cellpadding="1" style="width: 100%;" id="ReviewSelectionTable">
            <thead>
                <tr class="GridHeader">
                    <th >
                        select
                    </th>
                    <th id="statusSortLink">
                        <a href="#" > Status</a>
                    </th>
                    <th id="folderSortLink">
                        <a href="#">Folder</a>
                    </th>
                    <th id="docTypeSortLink">
                        Doc Type
                    </th>
                    <th id="applicationSortLink">
                        Application
                    </th>
                    <th >
                        Description
                    </th>
                    <th >
                        Internal Comments
                    </th>
                    <th>
                        Pages
                    </th>
                    <th id="modDateSortLink" >
                        <a href="#">Last Modified</a>
                    </th>
                    <th>
                        &nbsp;
                    </th>
                    <th>
                        &nbsp;
                    </th>
                </tr>
             </thead>
            <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr class="GridItem" runat="server" id="row">
            <td align="center">
                <input type="checkbox" runat="server" id="SelectItem" onclick="RefreshNextButton()"
                   />
                <asp:HiddenField runat="server" ID="id" />
                <asp:HiddenField runat="server" ID="HasOwnerPassword" />
                <asp:HiddenField runat="server" ID="PdfHasUnsupportedFeatures" />
                <asp:HiddenField runat="server" ID="IsESigned" />
            </td>
            <td align="left">
                <ml:EncodedLabel runat="server" ID="DocStatus"></ml:EncodedLabel>
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="Folder"></ml:EncodedLabel>
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="DocType"></ml:EncodedLabel>
            </td>
            <td><ml:EncodedLabel runat="server" ID="Application"></ml:EncodedLabel></td>
            <td>
                <ml:EncodedLabel runat="server" ID="Description"></ml:EncodedLabel>
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="InternalDescription"></ml:EncodedLabel>
            </td>
            <td align="right">
                <ml:EncodedLabel runat="server" ID="PageCount"></ml:EncodedLabel>
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="LastModified"></ml:EncodedLabel>
            </td>
            <td>
                <ml:EncodedLiteral runat="server" ID="Status"></ml:EncodedLiteral>
            </td>
            <td align="center">
                <a runat="server" id="viewLink">view</a>
            </td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="GridAlternatingItem" runat="server" id="row">
            <td align="center">
                <input type="checkbox" runat="server" onclick="RefreshNextButton()" id="SelectItem" />
                <asp:HiddenField runat="server" ID="id" />
                <asp:HiddenField runat="server" ID="HasOwnerPassword" />
                <asp:HiddenField runat="server" ID="PdfHasUnsupportedFeatures" />
                <asp:HiddenField runat="server" ID="IsESigned" />
            </td>
            <td align="left">
                <ml:EncodedLabel runat="server" ID="DocStatus"></ml:EncodedLabel>
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="Folder"></ml:EncodedLabel>
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="DocType"></ml:EncodedLabel>
            </td>
            <td><ml:EncodedLabel runat="server" ID="Application"></ml:EncodedLabel></td>
            <td>
                <ml:EncodedLabel runat="server" ID="Description"></ml:EncodedLabel>
            </td>
            <td>
                <ml:EncodedLabel runat="server" ID="InternalDescription"></ml:EncodedLabel>
            </td>
            <td align="right">
                <ml:EncodedLabel runat="server" ID="PageCount"></ml:EncodedLabel>
            </td>
            <td style="width:100px">
                <ml:EncodedLabel runat="server" ID="LastModified"></ml:EncodedLabel>
            </td>
            <td>
                <ml:EncodedLiteral runat="server" ID="Status"></ml:EncodedLiteral>
            </td>
            <td align="center">
                <a runat="server" id="viewLink">view</a>
            </td>
        </tr>
    </AlternatingItemTemplate>
    <FooterTemplate>
        </tbody> </table>
    </FooterTemplate>
</asp:Repeater>

<script type="text/javascript">
    
     function _init() {
        RefreshNextButton();
     }
    function RefreshNextButton() {
        disableNextButton(true);
        var table = document.getElementById('ReviewSelectionTable');
        
        var inputs = table.getElementsByTagName('input');
        
        for( var i = 0; i < inputs.length; i++) {
            var input = inputs[i];
            if ( typeof(input.type) !== 'undefined' && input.type === 'checkbox' && input.checked ) {
                disableNextButton(false);
                return;
            }
        }
        
    }
    

</script>
