﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeletedDocs.aspx.cs" Inherits="LendersOfficeApp.newlos.ElectronicDocs.DeletedDocs" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Deleted Documents</title>
    <style type="text/css">
        body { background-color: gainsboro; padding: 0 10px; margin: 0;}
        h1 { font-size: 145%; }
        .ddocs { width: 100%;  margin-bottom: 20px;  }
        th.sortable { color: Blue; text-decoration: underline; }
        th.sortup { background-image: url(../../images/Tri_Asc.gif);  background-repeat: no-repeat; background-position: right;}
        th.sortdown { background-image: url(../../images/Tri_Desc.gif);  background-repeat: no-repeat; background-position: right;}
        .hide { display: none; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <script type="text/javascript">
            $(function() {
                var cbs = $('input.restore_cb');
                var btn = $('#RestoreBtn');

                cbs.click(function() {
                    btn.prop('disabled', !this.checked && (cbs.filter(':checked').length == 0));
                });

                $('#closeBtn').click(function() { onClosePopup(); });
                btn.prop('disabled', true);

                btn.click(function() {
                    btn.prop('disabled', true);
                    var restore = [];
                    var currentFilter = window.opener.location.hash.substring(1, window.opener.location.hash.length)
                    $.each(cbs, function(i, o) {
                        if (o.checked) {
                            restore.push($(o).attr('docid'));
                            //We only care if there's some filtering in the parent window
                            if (currentFilter != -1) {
                                //Ideally, we only want to refresh the opening window if the user undeleted something
                                //from a folder that they don't currently have filtered. So, to do that, we get the
                                //folder that the user currently has filtered, and compare it to the folders
                                //for everything that is being undeleted. If any of them don't match,
                                //we change the original filter to "all folders"; if they all match, we keep
                                //the original filter where it was. 

                                //Go up a level from the check box into the table, get the next element that has
                                //the folder class (there's only one). This contains the name of our folder.
                                var foldername = $.trim($(o).parent().nextAll(".folder:first").text());
                                //Then take the name of our folder, and out of the drop down list's options, grab
                                //the one which contains the folder name, and take its value.
                                var ddlValue = $('.folderdd').children(":contains('" + foldername + "')").get(0).value;
                                //(This works because the ddl on the opener page is generated the same way as the ddl on this page
                                //so the ddl values match)

                                //If we're deleting something outside of the category filtered for in the parent,
                                //set the parent to not filter anything.
                                if (ddlValue != currentFilter) {
                                    currentFilter = -1;
                                }
                            }
                        }
                    });

                    if (restore.length == 0) {
                        return;
                    }

                    var jSonData = {
                        'ids': restore,
                        'loanid': ML.sLId
                    };

                    var data = JSON.stringify(jSonData);

                    callWebMethodAsync({
                        'type': "POST",
                        'url': 'DeletedDocs.aspx/RestoreDocs',
                        'data': data,
                        'contentType': "application/json; charset=utf-8",
                        'dataType': "json"
                    }).then(function(msg) {
                        if (window.opener && window.opener.closed == false) {
                            //OPM 69366: Maintain the current filtering state on reload.
                            //If you want to always maintain it, comment out the next line of code.
                            //(if you want to always go back to All Folders, replace the right side with -1)
                            //Currently, this changes the filtering on the parent window if 
                            //at least one of the restored items won't show up after restoration due to being filtered out.
                            window.opener.location.hash = currentFilter;
                            
                            window.opener.location.reload();
                        }
                        onClosePopup();
                    });

                });

                $('.folderdd').change(function() {
                    var folderid = this.value;
                    var alt = false
                    $.each(cbs, function(i, o) {
                        var show = $(o).attr('folderid') == folderid || folderid == '-1';
                        var tr = $(o).parents('tr');
                        tr.toggle(show);
                        tr.addClass(alt ? 'GridAlternatingItem' : 'GridItem');
                        tr.removeClass(alt ? 'GridItem' : 'GridAlternatingItem');
                        if (cbs.checked && !show) {
                            cbs.checked = false;
                        }
                        alt = !alt;
                    });
                }).change();

                $('table.ddocs').tablesorter({
                    headers: {
                        0: { sorter: false },
                        1: { sorter: false }
                    },
                    cssAsc: 'sortup',
                    cssDesc: 'sortdown'
                });

            });
        </script>
        <h1 class="MainRightHeader">Deleted Documents</h1>
        Show documents from <asp:DropDownList runat="server" ID="FolderDD" class="folderdd"></asp:DropDownList>
    
        <asp:Repeater runat="server" ID="DeletedDocuments" OnItemDataBound="DeletedDocuments_OnItemDataBound">
            <HeaderTemplate>
                <table class="ddocs">
                    <thead>
                        <tr class="GridHeader">
                            <th>
                                &nbsp;
                            </th>
                            <th>
                            </th>
                            <th class="sortable">
                                Folder
                            </th>
                            <th class="sortable">
                                Doc Type
                            </th>
                            <th class="sortable">
                                Application
                            </th>
                            <th class="sortable">
                                Description
                            </th>
                            <th class="sortable">
                                Pages
                            </th>
                            <th class="sortable">
                                Internal Comments
                            </th>
                            <th class="sortable">
                                Deleted By
                            </th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
            <ItemTemplate>
                <tr class="GridItem">
                    <td>
                        <input type="checkbox" class="restore_cb" runat="server" id="restore" />
                    </td>
                    <td>
                        <a href="#" target="downloadframe" runat="server" id="view" >view</a>
                    </td>
                    <td class="folder">
                        <ml:EncodedLiteral runat="server" ID="Folder"></ml:EncodedLiteral>
                    </td>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="DocType"></ml:EncodedLiteral>
                    </td>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="Application"></ml:EncodedLiteral>
                    </td>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="Description"></ml:EncodedLiteral>
                    </td>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="Pages"></ml:EncodedLiteral>
                    </td>
                    <td>
                        <ml:EncodedLiteral runat="server" ID="InternalComments"></ml:EncodedLiteral>
                    </td>
                    <td>
                        <ml:PassthroughLiteral runat="server" ID="DeletedBy"></ml:PassthroughLiteral>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        
        <input type="button" id="RestoreBtn"  value="Restore Selected Documents" />
        <input type="button" value="Cancel" id="closeBtn" />
        <iframe name="downloadframe" class="hide" ></iframe>
    </form>
</body>
</html>
