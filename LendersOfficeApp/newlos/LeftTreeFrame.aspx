<%@ Page language="c#" Codebehind="LeftTreeFrame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.LeftTreeFrame" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import namespace="LendersOffice.Security" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
  <head runat="server">
    <title>LeftTreeFrame</title>
    <style type="text/css">
        ul.ui-autocomplete { font-family: Arial, Helvetica, sans-serif; font-size: 10px;}
        div { white-space: nowrap; }
        .ui-widget { font-size: 10px; font-family: Arial, Helvetica, sans-serif; }
        #searchlbl img {  border:none; } 
        #Search { margin: 4px 4px; }
        .loadsQuickReply { padding-left:5px; width:16px;}
        .loadsQuickReply:hover { cursor:pointer; width:17px; }
        #pageNavigation .dynatree-container { background-color: gray; color: white;}

        .hide-edoc-batch .edoc-batch-editor { display: none; }

        #pageNavigation {
            width: auto;
            display: inline-block;
        }

        #pageNavigation .dynatree-container a {
            border: none;
            color: white;
            padding-top: 0;
            height: 20px;
            font-size: 13px;
        }

        #pageNavigation .dynatree-container li {
            padding-top: 0px;
        }

        #pageNavigation .dynatree-folder a {
            color: gold;
            font-weight: bold;
            font-size: 13px;
        }

        #pageNavigation span.dynatree-active a, #pageNavigation ul.dynatree-container a:hover, #pageNavigation span.dynatree-focused a:link {
            background-color: transparent;
            border: none;
        }

        html, body, form {
            height: 100%;
        }
    </style>
      
  </head>
  <body MS_POSITIONING="FlowLayout" class="LeftBackground">
<script type="text/javascript">


<!--
    <%-- When updating these mappings, also update the mappings in LeadLeftFrame.aspx --%>
    var uladPagesMappingsByPageName = {
        'Assets': { Legacy: 'BorrowerInfo.aspx?pg=5', Ulad: 'Ulad/Assets.aspx', CollectionVersion: 1 },
        'ApplicationManagement': { Legacy: 'BorrowerList.aspx', Ulad: 'Ulad/ApplicationManagement.aspx', CollectionVersion: 1 },
        'MonthlyIncome': { Legacy: 'BorrowerInfo.aspx?pg=3', Ulad: 'Ulad/IncomeSources.aspx', CollectionVersion: 2 },
        'REO': { Legacy: 'BorrowerInfo.aspx?pg=6', Ulad: 'Ulad/RealProperties.aspx', CollectionVersion: 1 },
        'Liabilities': { Legacy: 'BorrowerInfo.aspx?pg=4', Ulad: 'Ulad/Liabilities.aspx', CollectionVersion: 1 },
        'Declarations': { Legacy: 'Declarations.aspx', Ulad: 'Ulad/Declarations.aspx', RequireUlad2019TargetApp: true }
    };

    function isUladLoan() {
        return ML.sBorrowerApplicationCollectionT > 0;
    }

    function getUladPageNameFromUrl(url) {
        var keys = Object.keys(uladPagesMappingsByPageName);

        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            if (url.indexOf(uladPagesMappingsByPageName[key].Legacy) != -1) {
                return key;
            }
        }

        return '';
    }

    function loadUladPage(pageName, appId) {
        var pageMappings = uladPagesMappingsByPageName[pageName];
        if (pageMappings == null) {
            throw 'Unable to open page ' + pageName;
        }

        if (pageMappings.RequireUlad2019TargetApp && ML.IsTargeting2019Ulad) {
            return load(pageMappings.Ulad);
        }
        if (pageMappings.CollectionVersion && pageMappings.CollectionVersion <= ML.sBorrowerApplicationCollectionT) {
            return load(pageMappings.Ulad);
        }
        else {
            var appParam = '';
            if (appId != null) {
                appParam = pageMappings.Legacy.indexOf('?') == -1 ? '?' : '&';
                appParam += 'appid=' + appId;
            }

            return load(pageMappings.Legacy + appParam);
        }
    }

    function f_refreshAfterTimeout(timeoutInMilliseconds) {
        window.setTimeout(function() {
            this.location = this.location;
        }, timeoutInMilliseconds);
    }

    function f_getDocumentVendorPortalUrl(vendorId) {
        var args = {
            VendorId : vendorId,
            LoanId : ML.sLId
        }

        var result = gService.utils.call("GetIdsLoanFileLink", args);

        if (!result.error && result.value.success === "True")
        {
            window.open(result.value.url, "Vendor Portal");
        }
        else if (!result.error)
        {
            alert(result.value.error);
        }
        else
        {
            alert('An error occurred: "'+ result.UserMessage + '"');
        }

        return false;
    }

function checkEDocStatus() {
    // Make async call to see if we need to display/hide the Batch Editor link.
    var xml = buildXmlRequest({'slid':ML.sLId}); 
    callWebMethodAsync({
        type: 'POST', 
        url: gService.edoc.url + '?method=PollStatus',
        data : xml,
        contentType: 'text/xml',
        dataType: 'xml',
        cache: false,
        success: function(response) {
            var data = $(response).find('data');
            if( data.length > 0 ) {
                var processingDocs = JSON.parse(data.attr('processingDocs'));
                var errorDocs = JSON.parse(data.attr('errorDocs'));
                var docCount = data.attr('DocCount');
                var noActiveDocs = data.attr('noActiveDocs') != 'False';
                
                // If there are no editable docs, then hide batch editor link
                var noDocs = (docCount == 0);
                var noDocsEditable = noActiveDocs || noDocs || (docCount > 0 && ((processingDocs.length + errorDocs.length) == docCount));
                
                $('#pageNavigation').toggleClass('hide-edoc-batch', noDocsEditable);
            }
        }
    });
    
}

function getAppID() {
  var appID = '';
  if (typeof(parent.info.f_getCurrentApplicationID) == 'function')
    appID = parent.info.f_getCurrentApplicationID();
  return appID;
}

function downloadJetDocsMismo()
{
  <% if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanExportLoan)) { %>  
    window.open(ML.VirtualRoot + '/newlos/ExportToJetDocs.aspx?loanid=' + ML.sLId,'_parent');   
  <% } %>

}

function downloadMismoClosing26()
{
  <% if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanExportLoan)) { %>  
    window.open(ML.VirtualRoot + '/newlos/GenericMismoClosing26.aspx?loanid=' + ML.sLId,'_parent');   
  <% } %>

}

function downloadMismoClosing33()
{
  <% if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanExportLoan)) { %>
    window.open(ML.VirtualRoot + '/newlos/MismoClosing3Branch.aspx?sub=t&loanid=' + ML.sLId, '_parent');
  <% } %>
}

function downloadMismoClosing34()
{
  <% if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanExportLoan)) { %>
    window.open(ML.VirtualRoot + '/newlos/MismoClosing3Branch.aspx?sub=f&loanid=' + ML.sLId, '_parent');
  <% } %>
}

function downloadUlddMismo30()
{
  <% if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanExportLoan)) { %>  
    window.open(ML.VirtualRoot + '/newlos/ExportUlddMismo30.aspx?loanid=' + ML.sLId + '&appid=' + getAppID(),'ULDDExport', "menubar=no,resizable=yes");   
  <% } %>

}

    function onResourcesClick()
    {
        window.open(document.getElementById("EmployeeResourcesUrl").value,
                    'Resources', 
                    "center=yes,resizable=yes,scrollbars=yes,status=yes,help=no");
        return false;       
    }

function downloadLoanQualityAdvisor()
{
  <% if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanExportLoan)) { %>  
    window.open(ML.VirtualRoot + '/newlos/FreddieLoanQualityAdvisorExport.aspx?cmd=download&loanid=' + ML.sLId + '&appid=' + getAppID(),'_parent', "menubar=no,resizable=yes");   
  <% } %>
    return false;
}
// Custom scripts.
function load(href, useFullEditor) {
    var ch = href.indexOf('?') > 0 ? '&' : '?';
    var printID = '';
    var appID = '';
    var finalUrl = '';


    if (typeof(parent.body.f_getPrintID) == 'function') printID = parent.body.f_getPrintID();
    if (typeof(parent.info.f_getCurrentApplicationID) == 'function') appID = encodeURIComponent(parent.info.f_getCurrentApplicationID());

    var virtualRoot = <%=AspxTools.SafeUrl(VirtualRoot)%>;
    var url = virtualRoot + '/newlos/' + href + ch + 'loanid=' + <%=AspxTools.JsString(RequestHelper.LoanID)%> + '&printid=' + printID;

    if (typeof(parent.body.f_load) == 'function') {
        parent.body.f_load(url, useFullEditor);
    } else {
        finalUrl = url + '&appid=' + appID + '&printid=' + printID;

        if (useFullEditor) {
            window.top.location.href = finalUrl;
        } else {
            parent.body.location = finalUrl;
        }
    };

    checkEDocStatus();
    if(!useFullEditor) { loadConvLogIfNeeded(url); }
    return false;
}

// This function will load url that does NOT belong to /newlos
function loadRaw(href) {
  var ch = href.indexOf('?') > 0 ? '&' : '?';
  var url = <%=AspxTools.SafeUrl(VirtualRoot)%> + href + ch + 'loanid=' + <%=AspxTools.JsString(RequestHelper.LoanID)%>;
  if (typeof(parent.body.f_load) == 'function')
    parent.body.f_load(url);
  else parent.body.location = url + '&appid=' + encodeURIComponent(parent.info.f_getCurrentApplicationID());
  
  checkEDocStatus();
  loadConvLogIfNeeded();
  return false;
}
function loadConvLogIfNeeded()
{
    if(typeof QRI !== 'undefined')
    {
        if (typeof QRI.injectWhenIframeReady === 'function')
        {
            QRI.injectWhenIframeReady();
        }
    }        
}


function displayCreditReport()
{
	var report_url = <%=AspxTools.SafeUrl(VirtualRoot + "/newlos/services/ViewCreditFrame.aspx?loanid=" + RequestHelper.LoanID + "&applicationid=")%> + parent.info.f_getCurrentApplicationID();
	var win = openWindowWithArguments(report_url, "report", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=780,height=600") ;
	win.focus() ;
	
	return false;
  
}
function displayDUFindings() {
	var report_url = <%=AspxTools.SafeUrl(VirtualRoot + "/newlos/services/ViewDUFindingFrame.aspx?loanid=" + RequestHelper.LoanID)%>;
	var win = openWindowWithArguments(report_url, "report", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=780,height=600") ;
	win.focus() ;
	
	return false;

}
function displayLPFeedback() {
	var report_url = <%=AspxTools.SafeUrl(VirtualRoot + "/newlos/services/ViewLPFeedbackFrame.aspx?loanid=" + RequestHelper.LoanID)%>;
	var win = openWindowWithArguments(report_url, "report", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=780,height=600") ;
	win.focus() ;
	
	return false;

}
function displayFhaTotalFindings() {
    var report_url = <%=AspxTools.SafeUrl(VirtualRoot + "/newlos/services/FHATotalFindings.aspx?loanid=" + RequestHelper.LoanID)%>;
    var win =  openWindowWithArguments(report_url, "report", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=780,height=600") ;
    win.focus();
    return false;
}
function f_viewSavedCertificate() {
 var body_url = <%=AspxTools.JsNumeric(E_UrlOption.Page_Certificate) %>;
 var params = encodeURIComponent("loanid=" + <%=AspxTools.JsString(RequestHelper.LoanID)%>);
  var win = openWindowWithArguments(<%=AspxTools.SafeUrl(VirtualRoot + "/common/PrintView_Frame.aspx?body_url=")%> + body_url + "&menu_param=print&params=" + params, "", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=780,height=600");
   win.focus();
  return false;

}
var edocWin2 = null;
function f_viewBatchDocList(){
    var body_url = <%=AspxTools.SafeUrl(Tools.GetEDocsLink(Tools.VRoot +"/newlos/ElectronicDocs/EditEdocV2.aspx?loanid="+RequestHelper.LoanID+"&key=all"))%>;
    if( edocWin2 == null || edocWin2.closed) 
    {
        edocWin2 = window.open( body_url, "_blank", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes,width=950,height=600");
    }
    else {
        edocWin2.location.reload();
    }
    edocWin2.focus();
    return false;
}
var edocWin = null;
function f_viewDocList() {
    var body_url = document.getElementById('DocListUrl').value;
    if( edocWin == null || edocWin.closed) 
    {
        edocWin = window.open( body_url, "_blank", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes,width=950,height=600");
    }
    else {
        edocWin.location.reload();
    }
    edocWin.focus();
    return false;
}

var changeOfCircumstancesWindow = null;
function f_viewChangeOfCircumstancesForArchives() {
    var useNewCoC = false;
    return displayCoCWindow(useNewCoC);
}

function f_viewChangeOfCircumstancesForCcArchive() {
    var useNewCoC = true;
    return displayCoCWindow(useNewCoC);
}

function displayCoCWindow(useNewCoC) {
    var openCoCWindow = function() {
        var url = useNewCoC ?
            <%=AspxTools.SafeUrl(VirtualRoot + "/newlos/Forms/ChangeOfCircumstancesNew.aspx?loanid=" + RequestHelper.LoanID)%> :
            <%=AspxTools.SafeUrl(VirtualRoot + "/newlos/Forms/ChangeOfCircumstances.aspx?loanid=" + RequestHelper.LoanID + "&IsArchivePage=true")%>;

        if (changeOfCircumstancesWindow == null || changeOfCircumstancesWindow.closed) {
            changeOfCircumstancesWindow = window.open(url, "_blank", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes,width=950,height=600");
        } else {
            changeOfCircumstancesWindow.location.reload();
        }

        changeOfCircumstancesWindow.focus();
    };

    parent.body.PolyShouldShowConfirmSave(parent.body.isDirty(),openCoCWindow, parent.info.f_save, function(){
        parent.body.clearDirty();
        parent.body.location.reload();
    });

    return false;
}


var conditionSignoffWin = null;
function f_viewConditionSignoff() {
    var body_url = <%=AspxTools.SafeUrl(VirtualRoot + "/newlos/Underwriting/Conditions/ConditionSignoff.aspx?loanid=" + RequestHelper.LoanID)%>;
    if( conditionSignoffWin == null || conditionSignoffWin.closed) 
    {
        conditionSignoffWin = window.open( body_url, "_blank", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes,width=970,height=600");
    }
    else {
        conditionSignoffWin.location.reload();
    }
    conditionSignoffWin.focus();
    return false;
}

var conditionWin = null;
function f_viewNewCondition() {
    return f_viewCondition("/newlos/Underwriting/Conditions/TaskConditions.aspx");
}
function f_viewOldCondition() {
    return f_viewCondition("/newlos/Underwriting/TaskConditions.aspx");
}
function f_viewCondition(url) {
    var body_url = <%=AspxTools.SafeUrl(VirtualRoot)%> + url +"?loanid=" + <%=AspxTools.JsString(RequestHelper.LoanID)%>;
    if( conditionWin == null || conditionWin.closed) 
    {
        conditionWin = window.open( body_url, "_blank", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes,width=1030,height=768");
    }
    else {
    }
    conditionWin.focus();
    return false;
    }

function f_viewDocutech() {
    var data = { loanid : <%=AspxTools.JsString(RequestHelper.LoanID.ToString("N"))%>};
    var results = gService.Docutech.call("AutoExport", data);
    if( results.error ) {
        alert (results.UserMessage);
    }
    else if (results.value.URL) {
        var opts = "dialogWidth: 610px; dialogHeight: 515px; center: yes; resizable: yes; scroll: no; status: yes; toolbar:no; menubar:no; help: no;"
        window.open(results.value.URL, "_blank");
    }
    else {
        alert(results.value.Error);
    }
    return false;
}

function _showGenerateDocumentPopup(vendorId, opts){
    var body_url = <%=AspxTools.SafeUrl(DocumentGenerationUrlRoot + "/newlos/Services/SeamlessDocumentGeneration.aspx?loanid=" + RequestHelper.LoanID + "&appid=")%> + encodeURIComponent(parent.info.f_getCurrentApplicationID()) + "&vendorId=" + encodeURIComponent(vendorId);
     
    if ( window.DocGenerationWindow == null || window.DocGenerationWindow.closed){
        window.DocGenerationWindow = showModeless(body_url, opts, null,  window.parent);
    } else {
        if (window.DocGenerationOriginalUrl != body_url){
            window.DocGenerationWindow.document.body.style.display = "none"; // Show blank page for user to know the window is redirceting.
            window.DocGenerationWindow.location.href = body_url;
        }

        window.DocGenerationWindow.focus();
    }

    window.DocGenerationOriginalUrl = body_url;
    return window.DocGenerationWindow;
}

function f_viewMultiDocGeneration(vendorId) {
    var opts = "dialogWidth: 610px; dialogHeight: 550px; center: yes; resizable: yes; scroll: no; status: yes; toolbar:no; menubar:no; help: no;";
    window.parent.AuditWindow = _showGenerateDocumentPopup(vendorId, opts);
    
    return false;
}

function f_viewGenericFramework(providerID) {
    parent.body.PolyShouldShowConfirmSave(parent.body.isDirty(),function(){
        var args = 
        {
            sLId: ML.sLId,
            ProviderID: providerID
        };
        
        var result = gService.main.call("LoadGenericFrameworkVendor", args);
        if (result.error) {
            alert(result.UserMessage);
        } else if (result.value["HasError"] === 'True') {
            alert(result.value["ErrorMessage"]);
        } else {
            var url = result.value["PopupURL"];
            var height = result.value["PopupHeight"];
            var width = result.value["PopupWidth"];
            var isModal = result.value["PopupIsModal"] === 'True';
            if (isModal) {
                parent.body.showModal(url, null, 'dialogHeight:' + height + 'px; dialogWidth:' + width + 'px; center: yes; resizable: yes;');
            } else {
                window.open(url, '_blank', 'height=' + height + ', width=' + width + ', resizable=yes, scrollbars=yes');
            }
        }
    }, parent.info.f_save);
 
    return false;
}

function f_viewPmlSummary() {
  var body_url = <%=AspxTools.SafeUrl(VirtualRoot + "/newlos/Underwriting/PmlLoanSummaryView_Frame.aspx?loanid=" + RequestHelper.LoanID)%>;
  var win = openWindowWithArguments(body_url, "", "toolbar=no,menubar=no,location=no,status=no,resizable=yes");
  
  win.focus();
  return false;

}
function f_viewBrokerNotes() {
    //var body_url = <%=AspxTools.SafeUrl(VirtualRoot)%> + "/newlos/underwriting/BrokerToUnderwritingNotes.aspx?loanid=<%=AspxTools.JsStringUnquoted(RequestHelper.LoanID.ToString())%>";
    var body_url = <%=AspxTools.JsNumeric(E_UrlOption.BrokerToUnderwritingNotes) %>;
    var params = encodeURIComponent("loanid=" + <%=AspxTools.JsString(RequestHelper.LoanID)%>);

    window.open(<%=AspxTools.SafeUrl(VirtualRoot)%> + "/common/PrintView_Frame.aspx?body_url=" + body_url + "&menu_param=print&params=" + params, "", "toolbar=no,menubar=no,location=no,status=no,resizable=yes");
    return false;
    
}
var gPreviousPageID = null;
function selectPageID(id) { // id is the PageID from the codebehind
  if (id == gPreviousPageID) return; 
  
  var coll = document.getElementsByTagName('a');
  var length = coll.length;
  for (var i = 0; i < length; i++) {
    var o = coll[i];
    if (o.getAttribute("pageid") == id) {  // o.pageid is the from the config file
      o.style.backgroundColor = 'red';
      o.style.color='white';
      o.style.fontWeight='bold';
      if(o.navDupInd == gPreviousPageNavDupInd)
        {
            __expandAndScrollVisible(o);
        }
    }    
    if (null != gPreviousPageID && o.getAttribute("pageid") == gPreviousPageID) {
      o.style.backgroundColor = '';
      o.style.color = '';
      o.style.fontWeight='';
    }
  }
  gPreviousPageID = id;
}

function findPageID(id) {
  var coll = document.getElementsByTagName('a');
  var length = coll.length;
  for (var i = 0; i < length; i++) {
    var o = coll[i];
    if (o.getAttribute("pageid") == id)
      return o;
  }
  
  return null;
}

function testGotoPage(id) {
  var p = findPageID(id);
  if (null != p) p.click();

}
function testScript() {
  var t = 10000;
  var delay = 5000;

  for (var i = 0; i < testpages.length; i++) {
    window.setTimeout("testGotoPage('" + testpages[i] + "')", t);
    window.setTimeout("testRefreshCalculationAndSaveData()", t + 500);
    t += delay;
  }
  window.setTimeout("testGotoPage('Print')", t);
  window.setTimeout("parent.body.testBatchPrint()", t + delay);  
}
function createNewTask()
{
    parent.body.showModal( <%=AspxTools.SafeUrl("/los/reminders/TaskEditor.aspx?loanId="+ RequestHelper.LoanID + "&hideViewEditLoan=t")%> , null, null, null, function(args){
        if( args.OK )
        {
            return true;
        }
     },{hideCloseButton:true});
	return false;
}
function testRefreshCalculationAndSaveData() {

  if (typeof(parent.body.refreshCalculation) == 'function') parent.body.refreshCalculation();
  if (typeof(parent.body.saveMe) == 'function') parent.body.saveMe();
  
}
function __onNodeItemClick(node) {
  
}
function exportLoan() {
  parent.body.f_exportLoan(<%=AspxTools.JsString(RequestHelper.LoanID)%>);
}

function f_customEventKeyup(event) {

  // Ctrl+Alt+G or Ctrl+Alt+H to run through each page.
  if (event.ctrlKey && event.altKey && (event.keyCode == 71 || event.keyCode == 72)) {
    testScript();
  } else if (event.ctrlKey && event.altKey && event.keyCode == 67) {
    <%-- Ctrl+Alt+C to display test credit page --%>
    <% if (IsInternalUser) { %> 
      f_displayTestCredit();
    <% } %>
  } else if (event.ctrlKey && event.altKey && event.keyCode == 88) {
    <%-- Ctrl+Alt+X to display debug page for Xis response --%>
    f_displayExternalBugTool();
  } else if (event.ctrlKey && event.altKey && event.keyCode == 73) {
    <%-- Ctrl+Alt+I to display debug page for External Bug tool --%>
    if(<%=AspxTools.JsBool(HasPermit())%>)
    {
        f_displayDebugDu();
    }
  }
}
    function f_saveMenuSetting() {
        var frWidth = parent.document.getElementsByName("treeview")[0].clientWidth;
        var xml = '<root width="' + frWidth + '">';
        var coll = document.getElementsByTagName("div");

        $('.dynatree-folder').each(function () {
            var $folder = $(this);
            var path = $folder.find('a').attr('path');
            var isCollapse = !$folder.hasClass('dynatree-expanded');
            xml += "<folder path=\"" + path + "\" collapse=\"" + isCollapse + "\"/>\n\r";

        });

        xml += "</root>";
        parent.info.f_updateLoanEditorMenuTreeState(xml, $("#loanEditorMenuTreeStateXmlContent").val());
    }

function f_displayDebugDu() {
  load('test/testviewxisresponse.aspx');
}


function f_displayExternalBugTool() {
  load('test/ExternalDebugTool.aspx');
}

<%-- Inline conditional rendering here instead of using JsBool
     because we do not even want to expose the location of this
     sensitive protected resource. --%>
<% if (IsInternalUser) { %>  
function f_displayTestCredit() {
  load('test/testcreditreport.aspx');
}
<% } %>

function closeFile() {
  parent.header.closeMe();
}

function f_createSecondLoan() {
    parent.body.PolyShouldShowConfirmSave(parent.body.isDirty(),function(){
        if (confirm('Do you want to create subfinancing loan file?')) {
            var args = new Object();
            args["LoanID"] = <%=AspxTools.JsString(RequestHelper.LoanID)%>;
        
            var result = gService.main.call("CreateSecondLoan", args);
            if (!result.error) {
                var errorMessage = result.value["Error"];
                if (null == errorMessage) {
                    var secondLoanID = result.value["SecondLoanID"];
                    var secondLoanName = result.value["SecondLoanName"];
                    parent.body.f_openLoan(secondLoanID, secondLoanName);
                } else {
                    alert(errorMessage);
                }
            }
        }
    }, parent.info.f_save);
    return false;
}

function f_changeFrameSize(newSize) {
    parent.changeTreeviewSize(newSize);
    return false;
}

//-->
</script>	

      
      <asp:PlaceHolder runat="server" ID="LoadsQuickReplyInjector">
      <script type="text/javascript">
        var QRI;
        $(function () {
            QRI = QuickReplyInjector(
                <%=AspxTools.JsString(Tools.VRoot)%>,
                <%=AspxTools.JsString(RequestHelper.LoanID)%>,
                <%=AspxTools.JsString(BasePage.ReadonlyIncludeVersion)%>,
                $('.loadsQuickReply'), 
                parent.frames['body']);
        });
      </script>
      </asp:PlaceHolder>
          
    <div class="ui-widget" style="position: fixed; top: 0; background-color: gray;">
	    <input doesntDirty id="Search" name="Search"> 
        <a id="searchlbl" href="#"><img  src="../images/edocs/magnify.png" title="Go To Page" style=" vertical-align: middle;" /></a>
    </div>
    <br />
    <br />
    <form id="LeftTreeFrame" method="post" runat="server">
       <asp:HiddenField runat="server" ID="DocListUrl" />
        <ml:PassthroughLiteral ID="m_tree" runat="server" EnableViewState="false" />
     </form>
	
  </body>
  <script type="text/javascript">
      var gPreviousPageNavDupInd = null;
      
      // making links so they self report when clicked.
      for (var i = 0; i < document.links.length; i++) {
          if (document.links[i].pageid == null) {
              continue;
          }
          var lnk = document.links[i];
          var oldOnclick = document.links[i].onclick;
          document.links[i].onclick = f_oldOnlickAndUpdatePreviousPageNavDupInd(i, oldOnclick);
      }
      
      function f_oldOnlickAndUpdatePreviousPageNavDupInd(i, oldOnclick) {
          return function() {
              gPreviousPageNavDupInd = document.links[i].navDupInd;
              return oldOnclick();
          }
      }



      $(window).ready( function() {

          $(document).keyup(f_customEventKeyup);

          var data = [];
          var dupeCheck = {};
          $('a.TreeNodeLink').each(function(i, o) {
              var $a = $(this), txt = $.trim($a.text()), $cDiv = $a.parents('.dynatree-folder + ul');
              $a.prop('title', txt);


              $cDiv.each(function() {
                  var $prev = $(this).prev();
                  txt = $prev.text() + '/' + txt;
              });

              if (dupeCheck.hasOwnProperty(txt)) {
                  return;
              }

              data.push({
                  'label': txt,
                  'anchor': $a
              });

              dupeCheck[txt] = true;
          });

          delete dupeCheck;

          for (var i = 0; i < nodesToCollapse.length; i++) {
              nodesToCollapse[i].expand(false);
          }

          $("#Search").autocomplete({
              source: data,
              select: function(e, u) {
                  u.item.anchor.click();
              
                  var node = u.item.anchor.parents('div[path]').first().prev();
                  __expandCollapseNode(node[0], false);
                  var offset = node.offset(); 
                  var top = 0;

                  if (offset) {
                      top = offset.top - 50;
                  }

                  if (top < 0) {
                    top = 0;
                  }
                  window.scrollTo(0, top );
                                    
              },
              position: { my: 'left top', at: 'left bottom', of: '#Search' }
          });

          $('#searchlbl').click(function() {
              var el = $('#Search'), txt = el.val(), i = 0, items = data, count = items.length, obj;
              if (txt.length > 0) {
                  for (i; i < count; i++) {
                      obj = items[i];
                      if (obj.label === txt) {
                          var node = $(obj.anchor).parents('div[path]').first().prev();
                          __expandCollapseNode(node[0], false);
                          var offset = node.offset(); 
                          var top = 0;

                          if (offset) {
                              top = offset.top - 50;
                          }

                          if (top < 0) {
                            top = 0;
                          }
                          window.scrollTo(0, top );
                        
                          obj.anchor.click();
                          return false;
                      }
                  }
              }
          });
          
          
          initFavorites(<%=AspxTools.JsBool(IsLeadInFullEditor) %>);
          
          checkEDocStatus();
      });
      
  </script>
</html>
