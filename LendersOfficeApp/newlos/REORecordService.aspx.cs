using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos
{
	public partial class REORecordService : AbstractSingleEditService2
	{
        protected override void SaveData(CPageData dataLoan, CAppData dataApp, IRecordCollection collection, ICollectionItemBase2 record) 
        {
            var recordList = (IReCollection) collection;
            var field = (IRealEstateOwned) record;

            if (null != field) 
            {
                field.HExp_rep = GetString("HExp") ;
                field.MPmt_rep = GetString("MPmt") ;
                field.GrossRentI_rep = GetString("GrossRentI") ;
                field.MAmt_rep = GetString("MAmt") ;
                field.Val_rep = GetString("Val") ;
                field.OccR_rep = GetString("OccR") ;
                field.TypeT = (E_ReoTypeT) GetInt("Type");
                field.Stat = GetString("Stat");
                field.IsSubjectProp = GetBool("IsSubjectProp" );
                field.Zip = GetString("Zip") ;
                field.State = GetString("State") ;
                field.City = GetString("City") ;
                field.Addr = GetString("Addr") ;
                field.ReOwnerT = (E_ReOwnerT) GetInt("ReOwnerT") ;
                field.IsForceCalcNetRentalI = GetBool("IsForceCalcNetRentalI");
                field.NetRentILckd = GetBool("NetRentILckd");
                field.NetRentI_rep = GetString("NetRentI");
                field.IsPrimaryResidence = GetBool("IsPrimaryResidence");
            }
        }
        protected override CPageData DataLoan 
        {
            get { return CPageData.CreateUsingSmartDependency(LoanID, typeof(REORecordService)); }
        }

        protected override IRecordCollection LoadRecordList(CPageData dataLoan, CAppData dataApp) 
        {
            return dataApp.aReCollection;
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp, ICollectionItemBase2 record) 
        {
            var field = (IRealEstateOwned) record;

            decimal grossRentRental = 0;
            decimal mPmtRental = 0;
            decimal hExpRental = 0;
            decimal netRentRental = 0;

            decimal grossRentRetained = 0;
            decimal mPmtRetained = 0;
            decimal hExpRetained = 0;
            decimal netRentRetained = 0;

            if (null != field) 
            {

                SetResult("RecordID", field.RecordId);

                SetResult("NetRentI", field.NetRentI_rep);
                SetResult("HExp", field.HExp_rep);
                SetResult("MPmt", field.MPmt_rep);
                SetResult("GrossRentI", field.GrossRentI_rep);
                SetResult("MAmt", field.MAmt_rep);
                SetResult("Val", field.Val_rep);
                SetResult("OccR", field.OccR_rep);
                SetResult("Type", field.TypeT);
                SetResult("Stat", field.Stat);
                SetResult("IsSubjectProp", field.IsSubjectProp);
                SetResult("Zip", field.Zip);
                SetResult("State", field.State);
                SetResult("City", field.City);
                SetResult("Addr", field.Addr);
                SetResult("ReOwnerT", field.ReOwnerT);
                SetResult("IsForceCalcNetRentalI", field.IsForceCalcNetRentalI);
                SetResult("NetRentILckd", field.NetRentILckd);
                SetResult("IsPrimaryResidence", field.IsPrimaryResidence);
            }

            // Retrieve special values.
            SetResult("aReTotVal", dataApp.aReTotVal_rep);
            SetResult("aReTotMAmt", dataApp.aReTotMAmt_rep);

            // opm 174375 - these values need to be split out between rental and retained
            var recordsList = (IReCollection)LoadRecordList(dataLoan, dataApp);
            for (int i = 0; i < recordsList.CountRegular; i++)
            {
                var reField = recordsList.GetRegularRecordAt(i);
                if (reField.StatT == E_ReoStatusT.Rental)
                {
                    grossRentRental += reField.GrossRentI;
                    mPmtRental += reField.MPmt;
                    hExpRental += reField.HExp;
                    netRentRental += reField.NetRentI;
                }

                if (reField.StatT == E_ReoStatusT.Residence)
                {
                    grossRentRetained += reField.GrossRentI;
                    mPmtRetained += reField.MPmt;
                    hExpRetained += reField.HExp;
                    netRentRetained += reField.NetRentI;
                }
            }

            SetResult("aReRentalTotGrossRentI", m_losConvert.ToMoneyString(grossRentRental, FormatDirection.ToRep));
            SetResult("aReRentalTotMPmt",  m_losConvert.ToMoneyString(mPmtRental, FormatDirection.ToRep));
            SetResult("aReRentalTotHExp",  m_losConvert.ToMoneyString(hExpRental, FormatDirection.ToRep));
            SetResult("aReRentalTotNetRentI",  m_losConvert.ToMoneyString(netRentRental, FormatDirection.ToRep));

            SetResult("aReRetainedTotGrossRentI",  m_losConvert.ToMoneyString(grossRentRetained, FormatDirection.ToRep));
            SetResult("aReRetainedTotMPmt",  m_losConvert.ToMoneyString(mPmtRetained, FormatDirection.ToRep));
            SetResult("aReRetainedTotHExp",  m_losConvert.ToMoneyString(hExpRetained, FormatDirection.ToRep));
            SetResult("aReRetainedTotNetRentI",  m_losConvert.ToMoneyString(netRentRetained, FormatDirection.ToRep));
        }



	

	}
}
