﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BatchOperationErrorSummary.aspx.cs" Inherits="LendersOfficeApp.newlos.BatchOperationErrorSummary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Error Summary</title>
    <style type="text/css">
        #ButtonContainer
        {
            margin-top: 10px;
            text-align: center;
        }
    </style>
</head>
<body>
    <h4 class="page-header"><ml:EncodedLiteral runat="server" ID="HeaderMessage"></ml:EncodedLiteral></h4>
    <form id="form1" runat="server">
    <div class="Grid">
        <asp:GridView runat="server" ID="ErrorList"  CellPadding="5" AutoGenerateColumns="false" Width="100%"> 
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle  CssClass="GridAlternatingItem" />
            <RowStyle CssClass="GridItem" />
            <Columns>
                <asp:BoundField HeaderText="Loan" DataField="LoanNumber" />
                <asp:BoundField HeaderText="Error"  DataField="ErrorMessage" />
            </Columns>
        </asp:GridView>
    </div>
    <div id="ButtonContainer">
        <input type="button" value="OK" onclick="onClosePopup();" />
    </div>
    </form>
</body>
</html>
