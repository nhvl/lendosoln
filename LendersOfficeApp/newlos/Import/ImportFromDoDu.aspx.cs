﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.ObjLib.ServiceCredential;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Import
{
    public partial class ImportFromDoDu : BaseLoanPage
    {
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ImportFromDoDu));
            dataLoan.InitLoad();

            sDuCaseId.Text = dataLoan.sDuCaseId;

            IsImportCreditDo.Checked = true;

            bool hasAutoDoDuLogin = HasAutoDu(dataLoan, BrokerUser);


            LoginPanel.Visible = hasAutoDoDuLogin == false;
            if (hasAutoDoDuLogin == false)
            {
                if (string.IsNullOrEmpty(BrokerUser.DoLastLoginNm) == false)
                {
                    RememberLogin.Checked = true;
                    FannieMaeMORNETUserID.Text = BrokerUser.DoLastLoginNm;
                }
                else if (string.IsNullOrEmpty(BrokerUser.DuLastLoginNm) == false)
                {
                    RememberLogin.Checked = true;
                    FannieMaeMORNETUserID.Text = BrokerUser.DuLastLoginNm;
                }
                else
                {
                    RememberLogin.Checked = false;
                }
            }
        }

        private bool HasAutoDu(CPageData dataLoan, AbstractUserPrincipal principal)
        {
            if (principal.HasDoAutoLogin)
            {
                return true;
            }
            else if (principal.BrokerDB.UsingLegacyDUCredentials)
            {
                return principal.HasDuAutoLogin;
            }
            else
            {
                var savedCredential = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, ServiceCredentialService.AusSubmission)
                                                .FirstOrDefault(credential => credential.ChosenAusOption.HasValue && credential.ChosenAusOption.Value == AusOption.DesktopUnderwriter && credential.IsEnabledForNonSeamlessDu);
                return savedCredential != null;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.PageTitle = "Import From DO/DU";
            this.PageID = "ImportFromDoDu";


        }
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            base.OnInit(e);
        }
    }
}
