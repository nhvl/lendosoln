﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportFromLpa.aspx.cs" Inherits="LendersOfficeApp.newlos.Import.ImportFromLpa" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Import from LPA</title>
    <style type="text/css">
        body {
            background-color: gainsboro;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        jQuery(function ($) {
            $('body').on('change', '.RequiredInput', function () {
                $(this).siblings('.RequiredImg').toggle(this.value === '');
            });

            $('#ImportBtn').click(function () {
                var inputsOk = $('.RequiredInput:visible').filter(function () { return $(this).val() === '' }).length === 0;
                if (!inputsOk) {
                    alert("Please enter all required fields.");
                    return;
                }

                var data = {
                    LoanId: ML.sLId,
                    LpaUserId: $('#LpaUserId').val(),
                    LpaUserPassword: $('#LpaUserPassword').val(),
                    sFreddieLoanId: $('#sFreddieLoanId').val(),
                    sFreddieTransactionId: $('#sFreddieTransactionId').val(),
                    sLpAusKey: $('#sLpAusKey').val(),
                    sFreddieSellerNum: $('#sFreddieSellerNum').val(),
                    sFreddieNotpNum: $('#sFreddieNotpNum').val(),
                    sFreddieLpPassword: $('#sFreddieLpPassword').val(),
                    ShouldImportCredit: $('#ShouldImportCredit').is(':checked')
                };

                var loadingPopup = SimplePopups.CreateLoadingPopup("Importing file...");
                ShowPopup(loadingPopup, null);
                var result = gService.loanedit.callAsync("ImportLpaFile", data, true, false, true, true,
                    function(results) {
                        LQBPopup.Return(null);
                        HandleResults(results);
                    },
                    function () {
                        LQBPopup.Return(null);
                        alert(result.UserMessage);
                    }, null);
            });

            function HandleResults(results) {
                if (results.error) {
                    alert(results.UserMessage);
                }
                else if (results.value.Success.toLowerCase() === 'false') {
                    var errors = JSON.parse(results.value.Errors);
                    var errorPopup = SimplePopups.CreateErrorPopup("Something went wrong", errors);
                    ShowPopup(errorPopup, null);
                }
                else {
                    var completedPopup = SimplePopups.CreateAlertPopup("File has been imported.", "", '../../images/success.png');
                    ShowPopup(completedPopup,
                        function () {
                            $('#sFreddieLoanId').val(results.value.sFreddieLoanId);
                            $('#sFreddieTransactionId').val(results.value.sFreddieTransactionId);
                            $('#sLpAusKey').val(results.value.sLpAusKey);
                            $('#sFreddieNotpNum').val(results.value.sFreddieNotpNum);

                            clearDirty();
                        });
                }
            }

            function ShowPopup(popupContent, onReturnCallback) {
                LQBPopup.ShowElement($('<div>').append(popupContent), {
                    width: 350,
                    height: 200,
                    hideCloseButton: true,
                    elementClasses: 'FullWidthHeight',
                    onReturn: onReturnCallback
                });
            }

            function Initialize() {
                $('#LpaUserIdRow, #LpaUserPasswordRow').toggle(!ML.HasUserServiceCredentials);
                $('#LpaSellerNumRow, #LpaSellerPassRow').toggle(!ML.HasSellerServiceCredentials);

                if (ML.Has_sFreddieLpPassword) {
                    $('#sFreddieLpPassword').removeClass('RequiredInput');
                    $('#LpaPasswordImg').hide();
                }
                else {
                    $('#PasswordExistsMessage').hide();
                }

                $('.RequiredInput:visible').each(function () {
                    $(this).siblings('.RequiredImg').toggle(this.value === '');
                });
            }

            Initialize();
        });
    </script>
    <form id="form1" runat="server">
        <div class="MainRightHeader">
            Import Data from Loan Product Advisor
        </div>
        <div class="PaddingLeftRight5">
            <table>
                <tr id="LpaUserIdRow">
                    <td class="FieldLabel">LPA User ID</td>
                    <td>
                        <input type="text" id="LpaUserId" class="Input RequiredInput" />
                        <img alt="Required" class="RequiredImg" id="LpaUserIdImg" runat="server" src="../../images/require_icon_red.gif" />
                    </td>
                </tr>
                <tr id="LpaUserPasswordRow">
                    <td class="FieldLabel">LPA User Password</td>
                    <td>
                        <input type="password" id="LpaUserPassword" class="Input RequiredInput" />
                        <img alt="Required" class="RequiredImg" id="LpaUserPasswordImg" runat="server" src="../../images/require_icon_red.gif" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">LPA Loan ID</td>
                    <td>
                        <input type="text" runat="server" id="sFreddieLoanId" class="Input RequiredInput" />
                        <img alt="Required" class="RequiredImg" id="LpaLoanIdImg" runat="server" src="../../images/require_icon_red.gif" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">LPA Transaction ID</td>
                    <td>
                        <input type="text" runat="server" id="sFreddieTransactionId" class="Input" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">LPA AUS Key Number</td>
                    <td>
                        <input type="text" runat="server" id="sLpAusKey" class="Input" />
                    </td>
                </tr>
                <tr id="LpaSellerNumRow">
                    <td class="FieldLabel">LPA Seller Number</td>
                    <td>
                        <input type="text" runat="server" id="sFreddieSellerNum" class="Input RequiredInput" />
                        <img alt="Required" class="RequiredImg" id="LpaSellerNumImg" runat="server" src="../../images/require_icon_red.gif" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">LPA NOTP Number</td>
                    <td>
                        <input type="text" runat="server" id="sFreddieNotpNum" class="Input" />
                    </td>
                </tr>
                <tr id="LpaSellerPassRow">
                    <td class="FieldLabel">LPA Password</td>
                    <td>
                        <input type="password" runat="server" id="sFreddieLpPassword" class="Input RequiredInput" />
                        <img alt="Required" class="RequiredImg" id="LpaPasswordImg" runat="server" src="../../images/require_icon_red.gif" />
                        <span id="PasswordExistsMessage">(Leave the password blank if it's not being changed.)</span>
                    </td>
                </tr>
            </table>
            <div>
                <div>
                    <label class="FieldLabel">
                        <input type="checkbox" id="ShouldImportCredit" />Import Credit Report
                    </label>
                </div>
                <br />
                <div>
                    <input type="button" class="FieldLabel" id="ImportBtn" value="Import Data" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
