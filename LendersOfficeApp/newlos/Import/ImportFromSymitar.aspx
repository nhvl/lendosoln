﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportFromSymitar.aspx.cs" Inherits="LendersOfficeApp.newlos.Import.ImportFromSymitar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Import From Symitar</title>
    <style type="text/css">
        body {
            background-color: gainsboro;
        }

        .Center 
        {
            text-align: center;
        }

        span.LargeSpacer 
        {
            padding-right: 30px;
        }

        input.FieldDescriptionFullWidth 
        {
            width: 98%;
        }

        table.InsetSpacer
        {
            padding-left: 5px;
        }

        tr.ValueDiscrepancy
        {
            background-color: red;
        }

        td.Bold
        {
            font-weight: bold;
        }

        td.ExtraPadding
        {
            padding-left: 15px;
            padding-right: 15px;
        }

        td.SelectAllColumn
        {
            padding-right: 5px;
        }

        td.SelectAllColumn input
        {
            vertical-align: bottom;
        }

        td.SelectAllColumn label
        {
            line-height: 175%;
        }

        #LoadBtn:active, #ApplyBtn:active
        {
            background-color: transparent;
        }

        #ErrorMessage
        {
            color: red; 
            font-weight: bold;
        }

        #HeaderTable {
            border-collapse: collapse;
            border-spacing: 0;
            border: 0;
            width: 100%;
        }
    </style>
</head>
<body>
    <div ng-app="ImportFromSymitar">
        <div id="ImportController" ng-controller="ImportFromSymitarController">
            <form id="form1" runat="server">
                <script type="text/javascript">
                    // We override getAllFormValues() to pull only the borrower and coborrower 
                    // member id inputs along with the normal static inputs.
                    function getAllFormValues() {
                        return angular.element($('#ImportController')).scope().getAllFormValues();
                    }
                </script>

                <table id="HeaderTable">
		            <tr>
			            <td class="MainRightHeader" colspan="6">Import Data from Symitar</td>
		            </tr>
                </table>

                <table class="InsetSpacer">
		            <tr>
						<td class="FieldLabel">Borrower Member ID</td>
						<td class="Center">
							<input id="aBCoreSystemId" runat="server" type="text" size="11" maxlength="15" NoHighlight="true" />
							<img src="~/images/require_icon.gif" runat="server" />
						</td>
						<td colspan="2">&nbsp;</td>
						<td class="FieldLabel">Co-Borrower Member ID</td>
						<td class="Center">
							<input id="aCCoreSystemId" runat="server" type="text" size="11" maxlength="15" NoHighlight="true" />
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td class="Center">
							<input id="LoadBtn" type="button" value="Load Data" ng-click="load();" NoHighlight="true" />
						</td>
						<td colspan="4">&nbsp;</td>
						<td class="Center">
							<input id="ApplyBtn" type="button" value="Apply to Loan File" ng-click="apply();" NoHighlight="true" />
						</td>
					</tr>
					<tr>
						<td colspan="6"><p id="ErrorMessage" class="Center"></p></td>
					</tr>
                </table>

                <import-table type="Borrower" datasource="viewModel.BorrowerImportTableList"></import-table>

                <br />

                <import-table type="Coborrower" datasource="viewModel.CoborrowerImportTableList"></import-table>
            </form>
        </div>
    </div>
</body>
</html>
