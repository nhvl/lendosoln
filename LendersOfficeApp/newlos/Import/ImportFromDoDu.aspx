﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportFromDoDu.aspx.cs" Inherits="LendersOfficeApp.newlos.Import.ImportFromDoDu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Import From DO/DU</title>
</head>
<body bgcolor="gainsboro">
<script type="text/javascript">
  function btnImport_onclick()
  {
    var args = getAllFormValues();
    document.getElementById("ErrorMessage").innerHTML = "";
    var result = gService.loanedit.call("ImportToLO", args);

    if (!result.error)
    {
      if (result.value["Status"] === "OK")
      {
        alert('Imported Successfully.');
      }
      else
      {
        document.getElementById("ErrorMessage").innerHTML = result.value["ErrorMessage"];
      }
    } 
    else
    {
      var errMsg = result.UserMessage || 'Unable to import data from DO/DU. Please try again.';
      alert(errMsg);
      return false;
    }
  }
</script>
    <form id="form1" runat="server">
    <div>
    <table cellpadding="0" cellspacing="0" width="100%" border="0">
    <tr><td class="MainRightHeader">Import Data from DO/DU Casefile</td></tr>
    <tr>
      <td style="padding-left:5px">
        <table>
          <tr>
            <td class="FieldLabel">Casefile ID</td>
            <td><asp:TextBox ID="sDuCaseId" runat="server" width="150px" NotForEdit="true"/><img id="Img1" src="~/images/require_icon.gif" runat="server"/></td>
          </tr>
          <tbody id="LoginPanel" runat="server">
          <tr>
            <td class="FieldLabel">DO / DU User ID</td>
            <td><asp:TextBox ID="FannieMaeMORNETUserID" runat="server" width="150px" NotForEdit="true"/><img src="~/images/require_icon.gif" runat="server"/></td>
          </tr>
          <tr>
            <td class="FieldLabel">DO / DU Password</td>
            <td><asp:TextBox ID="FannieMaeMORNETPassword" runat="server" Width="150px" TextMode="Password"  NotForEdit="true"/><img id="Img2" src="~/images/require_icon.gif" runat="server"/></td>
          </tr>
          <tr>
            <td></td>
            <td class="FieldLabel"><asp:CheckBox ID="RememberLogin" runat="server" Text="Remember my DO / DU User ID"  NotForEdit="true"/></td>
          </tr>
          </tbody>
          <tr>
            <td></td>
            <td class="FieldLabel"><asp:CheckBox ID="IsImportCreditDo" runat="server" Text="Get credit report from casefile (if any)"  NotForEdit="true"/></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><input type="button" id="btnImport" value="Import Data" class="ButtonStyle" onclick="btnImport_onclick();"/></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><a href="#" onclick="window.open('https://profile-manager.efanniemae.com/integration/service/password/UserSelfService', 'FannieMae', 'width=740,height=420,menubar=no,status=yes,resizable=yes');">Forgot your User ID or Password?</a></td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
    </table>
    <div id="ErrorMessage" style="color:Red;font-weight:bold"></div>
    </div>
    </form>
</body>
</html>
