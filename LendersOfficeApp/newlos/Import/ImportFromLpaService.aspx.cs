﻿#region Auto Generated code
namespace LendersOfficeApp.newlos.Import
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.LoanProductAdvisor;
    using LendersOffice.Security;

    /// <summary>
    /// Service item for ImportFromLpa.
    /// </summary>
    public class ImportFromLpaServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        /// <summary>
        /// Creates the data loan object.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <returns>The loan data object.</returns>
        protected override CPageData ConstructPageDataClass(Guid loanId)
        {
            return CPageData.CreateUsingSmartDependency(loanId, typeof(ImportFromLpaServiceItem));
        }

        /// <summary>
        /// Chooses the service method to run.
        /// </summary>
        /// <param name="methodName">The service method name.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(this.ImportLpaFile):
                    this.ImportLpaFile();
                    break;
                default:
                    throw new UnhandledCaseException(methodName);
            }
        }

        /// <summary>
        /// Imports the LPA file into the system.
        /// </summary>
        protected void ImportLpaFile()
        {
            CPageData dataLoan = this.ConstructPageDataClass(this.sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            this.BindData(dataLoan, null);

            // We want to save before doing the request. 
            dataLoan.Save();

            var request = new LoanTransferRequestInput();
            request.Principal = PrincipalFactory.CurrentPrincipal;
            request.LoanResolution = LoanTransferLoanResolver.ExistingLoan(this.sLId);
            request.LpaUserId = this.GetString("LpaUserId");
            request.LpaUserPassword = this.GetString("LpaUserPassword");
            request.ShouldImportCredit = this.GetBool("ShouldImportCredit");
            request.LpaSellerNumber = dataLoan.sFreddieSellerNum;
            request.LpaSellerPassword = dataLoan.sFreddieLpPassword.Value;
            request.LpaLoanId = dataLoan.sFreddieLoanId;
            request.LpaTransactionId = dataLoan.sFreddieTransactionId;
            request.LpaAusKey = dataLoan.sLpAusKey;
            request.LpaNotpNumber = dataLoan.sFreddieNotpNum;
            request.LoanBranchId = dataLoan.sBranchId;

            LoanTransferRequestOutput result = LoanTransferRequest.RunLoanTransferRequest(request);

            this.SetResult("Success", result.Success);
            if (result.Success)
            {
                // The import has updated the loan file so we have to recreate it.
                dataLoan = CPageData.CreateUsingSmartDependency(this.sLId, typeof(ImportFromLpaServiceItem));
                dataLoan.InitLoad();

                this.SetResult("sFreddieLoanId", dataLoan.sFreddieLoanId);
                this.SetResult("sFreddieTransactionId", dataLoan.sFreddieTransactionId);
                this.SetResult("sLpAusKey", dataLoan.sLpAusKey);
                this.SetResult("sFreddieNotpNum", dataLoan.sFreddieNotpNum);
            }
            else
            {
                this.SetResult("Errors", SerializationHelper.JsonNetAnonymousSerialize(result.Errors));
            }
        }

        /// <summary>
        /// Saves the data to the loan.
        /// </summary>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="dataApp">The data app.</param>
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sFreddieLoanId = this.GetString("sFreddieLoanId");
            dataLoan.sFreddieTransactionId = this.GetString("sFreddieTransactionId");
            dataLoan.sLpAusKey = this.GetString("sLpAusKey");
            dataLoan.sFreddieNotpNum = this.GetString("sFreddieNotpNum");

            var freddieSellerNum = this.GetString("sFreddieSellerNum");
            var freddieSellerPassword = this.GetString("sFreddieLpPassword");
            if (!string.IsNullOrEmpty(freddieSellerNum))
            {
                dataLoan.sFreddieSellerNum = freddieSellerNum;
            }

            if (!string.IsNullOrEmpty(freddieSellerPassword))
            {
                dataLoan.sFreddieLpPassword = freddieSellerPassword;
            }
        }

        /// <summary>
        /// Loads data to send back to the page. Not used.
        /// </summary>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="dataApp">The data app.</param>
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    /// <summary>
    /// Import From LPA service.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "I'm following the convention for adding service pages.")]
    public partial class ImportFromLpaService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Initializes the service.
        /// </summary>
        protected override void Initialize()
        {
            this.AddBackgroundItem(string.Empty, new ImportFromLpaServiceItem());
        }
    }
}