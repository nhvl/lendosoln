﻿#region Auto Generated code
namespace LendersOfficeApp.newlos.Import
#endregion
{
    using System;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;

    /// <summary>
    /// Page for importing data from LPA.
    /// </summary>
    public partial class ImportFromLpa : BaseLoanPage
    {
        /// <summary>
        /// Loads default data to the page.
        /// </summary>
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(ImportFromLpa));
            dataLoan.InitLoad();

            this.sFreddieLoanId.Value = dataLoan.sFreddieLoanId;
            this.sLpAusKey.Value = dataLoan.sLpAusKey;
            this.sFreddieTransactionId.Value = dataLoan.sFreddieTransactionId;
            this.sFreddieNotpNum.Value = dataLoan.sFreddieNotpNum;

            var ausServiceCredentials = ServiceCredential.ListAvailableServiceCredentials(PrincipalFactory.CurrentPrincipal, dataLoan.sBranchId, ServiceCredentialService.AusSubmission);
            var sellerCredentials = ausServiceCredentials.FirstOrDefault(credential => credential.ChosenAusOption == AusOption.LpaSellerCredential);
            var userCredentials = ausServiceCredentials.FirstOrDefault(credential => credential.ChosenAusOption == AusOption.LpaUserLogin);

            if (sellerCredentials == null)
            {
                this.sFreddieSellerNum.Value = dataLoan.sFreddieSellerNum;
            }

            this.RegisterJsGlobalVariables("HasUserServiceCredentials", userCredentials != null);
            this.RegisterJsGlobalVariables("HasSellerServiceCredentials", sellerCredentials != null);
            this.RegisterJsGlobalVariables("Has_sFreddieLpPassword", !string.IsNullOrEmpty(dataLoan.sFreddieLpPassword.Value));
        }

        /// <summary>
        /// On init function.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;
            this.PageTitle = "Import From LPA";
            this.PageID = "ImportFromLpa";

            this.RegisterJsScript("LQBPopup.js");
            this.RegisterCSS("font-awesome.css");
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("SimplePopups.js");
            this.EnableJqueryMigrate = false;

            base.OnInit(e);
        }
    }
}