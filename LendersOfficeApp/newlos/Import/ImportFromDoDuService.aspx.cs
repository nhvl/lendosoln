﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.DU;
using System.Data.SqlClient;
using LendersOfficeApp.los.admin;
using LendersOffice.ObjLib.ServiceCredential;

namespace LendersOfficeApp.newlos.Import
{
    public class ImportFromDoDuServiceItem : AbstractBackgroundServiceItem
    {
        protected override void Process(string methodName)
        {
            switch (methodName) 
            {
                case "ImportToLO":
                    ImportToLO();
                    break;
            }
        }

        private void ImportToLO()
        {
            Guid sLId = GetGuid("LoanID");
            string sDuCaseId = GetString("sDuCaseId");
            string errorMessage = string.Empty;

            try
            {
                bool isDone = false;
                string globallyUniqueIdentifier = string.Empty;
                while (!isDone)
                {
                    AbstractFnmaXisRequest request = CreateCasefileExportRequest(globallyUniqueIdentifier);
                    AbstractFnmaXisResponse response = DUServer.Submit(request);

                    if (response.IsReady)
                    {
                        isDone = true;
                        if (response.HasError)
                        {
                            errorMessage = response.ErrorMessage;
                        }
                        else if (response.HasBusinessError)
                        {
                            errorMessage = response.BusinessErrorMessage + "<br><pre>" + response.FnmaStatusLog + "</pre>";
                        }
                        else
                        {
                            SaveCasefileExportResponse((FnmaXisCasefileExportResponse)response);
                            SetResult("Status", "OK");
                        }
                    }
                    globallyUniqueIdentifier = response.GloballyUniqueIdentifier;
                }
            }
            catch (CBaseException exc)
            {
                errorMessage = exc.UserMessage;
            }
            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                SetResult("Status", "Error");
                SetResult("ErrorMessage", errorMessage);
            }
        }
        private FnmaXisCasefileExportRequest CreateCasefileExportRequest(string globallyUniqueIdentifier)
        {
            Guid sLId = GetGuid("LoanID");

            FnmaXisCasefileExportRequest request = new FnmaXisCasefileExportRequest(sLId);
            SetAuthenticationInformation(request, sLId);
            request.GloballyUniqueIdentifier = globallyUniqueIdentifier;
            request.MornetPlusCasefileIdentifier = GetString("sDuCaseId");

            return request;
        }
        private void SetAuthenticationInformation(AbstractFnmaXisRequest request, Guid loanId)
        {

            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            string duAutoLogin;
            string duAutoPassword;

            if (principal.HasDoAutoLogin)
            {
                request.FannieMaeMORNETUserID = principal.DoAutoLoginNm;
                request.FannieMaeMORNETPassword = principal.DoAutoPassword;
            }
            else if (HasAutoDuLogin(principal, loanId, out duAutoLogin, out duAutoPassword))
            {
                request.FannieMaeMORNETUserID = duAutoLogin;
                request.FannieMaeMORNETPassword = duAutoPassword;
            }
            else
            {
                bool isRememberLogin = GetBool("RememberLogin");

                request.FannieMaeMORNETUserID = GetString("FannieMaeMORNETUserID").TrimWhitespaceAndBOM();
                request.FannieMaeMORNETPassword = GetString("FannieMaeMORNETPassword").TrimWhitespaceAndBOM();

                string fnmaUserName = isRememberLogin ? request.FannieMaeMORNETUserID : "";

                SqlParameter[] parameters = {
                                                new SqlParameter("@UserId", BrokerUserPrincipal.CurrentPrincipal.UserId),
                                                new SqlParameter("@DOLastLoginNm", fnmaUserName),
                                                new SqlParameter("@DULastLoginNm", fnmaUserName)
                                        };
                StoredProcedureHelper.ExecuteNonQuery(BrokerUserPrincipal.CurrentPrincipal.BrokerId, "UpdateLastDoDuLpLoginNm", 3, parameters);
            }
        }

        private bool HasAutoDuLogin(AbstractUserPrincipal principal, Guid loanId, out string userId, out string password)
        {
            if (principal.BrokerDB.UsingLegacyDUCredentials)
            {
                if (principal.HasDuAutoLogin)
                {
                    userId = principal.DuAutoLoginNm;
                    password = principal.DuAutoPassword;
                    return true;
                }
            }
            else
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(ImportFromDoDuService));
                dataLoan.InitLoad();

                var savedCredential = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, ServiceCredentialService.AusSubmission)
                                                .FirstOrDefault(credential => credential.ChosenAusOption.HasValue && credential.ChosenAusOption.Value == AusOption.DesktopUnderwriter && credential.IsEnabledForNonSeamlessDu);
                if (savedCredential != null)
                {
                    userId = savedCredential.UserName;
                    password = savedCredential.UserPassword.Value;
                    return true;
                }
            }

            userId = null;
            password = null;
            return false;
        }

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(ImportFromDoDuServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }

        private void SaveCasefileExportResponse(FnmaXisCasefileExportResponse response)
        {
            Guid sLId = GetGuid("LoanID");
            bool isImport1003 = true;
            bool isImportDuFindings = true;
            bool isImportCreditReport = GetBool("IsImportCreditDo");

            response.ImportToLoanFile(sLId, BrokerUserPrincipal.CurrentPrincipal, isImport1003, isImportDuFindings, isImportCreditReport);

        }


    }
    public partial class ImportFromDoDuService : BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new ImportFromDoDuServiceItem());
        }
    }
}
