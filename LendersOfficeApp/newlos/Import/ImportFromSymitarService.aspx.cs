﻿// <copyright file="ImportFromSymitarService.aspx.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Michael Leinweaver
//  Date:   2015-12-04 09:12:55 -0800
// </summary>
#region Generated Code
namespace LendersOfficeApp.newlos.Import
#endregion
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.Symitar;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a service for the "Import from Symitar" page.
    /// </summary>
    public partial class ImportFromSymitarService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Initializes the service by adding a new <see cref="ImportFromSymitarServiceItem"/>.
        /// </summary>
        protected override void Initialize()
        {
            AddBackgroundItem(string.Empty, new ImportFromSymitarServiceItem());
        }
    }

    /// <summary>
    /// Provides a service item for the "Import from Symitar" page.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed.")]
    public class ImportFromSymitarServiceItem : AbstractBackgroundServiceItem
    {
        /// <summary>
        /// Constructs a new <see cref="CPageData"/> object using the
        /// specific <paramref name="loanId"/>.
        /// </summary>
        /// <param name="loanId">
        /// The <see cref="Guid"/> for the loan.
        /// </param>
        /// <returns>
        /// A <see cref="CPageData"/> instance with the specified
        /// loan id.
        /// </returns>
        protected override CPageData ConstructPageDataClass(Guid loanId)
        {
            return CPageData.CreateUsingSmartDependency(loanId, typeof(ImportFromSymitarServiceItem));
        }

        /// <summary>
        /// Binds data received by the service to the loan.
        /// </summary>
        /// <param name="dataLoan">
        /// The <see cref="CPageData"/> to bind to.
        /// </param>
        /// <param name="dataApp">
        /// The <see cref="CAppData"/> to bind to.
        /// </param>
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.aBCoreSystemId = this.GetString("aBCoreSystemId");
            dataApp.aCCoreSystemId = this.GetString("aCCoreSystemId");
        }

        /// <summary>
        /// Loads data for the import page.
        /// </summary>
        /// <param name="dataLoan">
        /// The <see cref="CPageData"/> to load from.
        /// </param>
        /// <param name="dataApp">
        /// The <see cref="CAppData"/> to load from.
        /// </param>
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            this.SetResult("aBCoreSystemId", dataApp.aBCoreSystemId);
            this.SetResult("aCCoreSystemId", dataApp.aCCoreSystemId);
        }

        /// <summary>
        /// Processes the method with the specified <paramref name="methodName"/>.
        /// </summary>
        /// <param name="methodName">
        /// The method to process.
        /// </param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "LoadDataFromSymitar":
                    LoadDataFromSymitar();
                    break;
                case "ApplyDataToLoanFile":
                    ApplyDataToLoanFile();
                    break;
                default:
                    throw new GenericUserErrorMessageException(
                        "Unhandled import from Symitar service method: " + methodName);
            }
        }

        /// <summary>
        /// Retrieves the file type of the current loan via the loan object.
        /// </summary>
        /// <returns>The loan file type.</returns>
        private E_sLoanFileT RetrieveLoanType()
        {
            var loan = new CFullAccessPageData(this.sLId, new[] { nameof(CPageData.sLoanFileT) });
            loan.InitLoad();
            return loan.sLoanFileT;
        }

        /// <summary>
        /// Imports loan data from Symitar.
        /// </summary>
        private void LoadDataFromSymitar()
        {
            var borrowerCoreSystemId = this.GetString("aBCoreSystemId");

            if (string.IsNullOrWhiteSpace(borrowerCoreSystemId))
            {
                throw new CBaseException(
                    "Invalid Member ID. Please verify the borrower’s member ID.",
                    "Invalid Symitar borrower member ID: " + borrowerCoreSystemId);
            }

            var loanId = this.sLId;

            var appId = this.aAppId;

            var currentPrincipal = PrincipalFactory.CurrentPrincipal;

            var lenderConfig = currentPrincipal.BrokerDB.GetEnabledSymitarLenderConfig(this.RetrieveLoanType());

            var borrowerResult = SymitarManager.LoadImportFromSymitar(
                lenderConfig,
                borrowerCoreSystemId,
                isForBorrower: true);

            string borrowerImportMappingKey = borrowerResult.SaveCopy(currentPrincipal);

            var returnViewModel = new ImportFromSymitarViewModel()
            {
                BorrowerImportMappingKey = borrowerImportMappingKey,
                BorrowerImportTableList = SymitarManager.MapToDisplayLogic(borrowerResult, loanId, appId)
            };

            var coborrowerCoreSystemId = this.GetString("aCCoreSystemId");

            if (!string.IsNullOrWhiteSpace(coborrowerCoreSystemId))
            {
                var coborrowerResult = SymitarManager.LoadImportFromSymitar(
                    lenderConfig,
                    coborrowerCoreSystemId,
                    isForBorrower: false);

                string coborrowerImportMappingKey = coborrowerResult.SaveCopy(currentPrincipal);

                returnViewModel.CoborrowerImportTableList = SymitarManager.MapToDisplayLogic(coborrowerResult, loanId, appId);

                returnViewModel.CoborrowerImportMappingKey = coborrowerImportMappingKey;
            }

            var hasBorrowerError = returnViewModel.BorrowerImportTableList.Count == 0;
            var hasCoborrowerError = !string.IsNullOrWhiteSpace(coborrowerCoreSystemId) && returnViewModel.CoborrowerImportTableList.Count == 0;

            if (hasBorrowerError && hasCoborrowerError)
            {
                throw new CBaseException(
                    "No data was returned for both borrower and co-borrower; please verify the member IDs.",
                    "No data was returned for both borrower and co-borrower; please verify the member IDs.");
            }
            else if (hasBorrowerError)
            {
                throw new CBaseException(
                    "No data was returned for borrower; please verify the member ID.",
                    "No data was returned for borrower; please verify the member ID.");
            }
            else if (hasCoborrowerError)
            {
                throw new CBaseException(
                    "No data was returned for co-borrower; please verify the member ID.",
                    "No data was returned for co-borrower; please verify the member ID.");
            }

            this.SetResult("Status", "OK");
            this.SetResult("ReturnViewModel", SerializationHelper.JsonNetSerialize(returnViewModel));
        }

        /// <summary>
        /// Applies data imported from Symitar to the loan.
        /// </summary>
        private void ApplyDataToLoanFile()
        {
            var receivedViewModel = SerializationHelper.JsonNetDeserialize<ImportFromSymitarViewModel>(
                this.GetString("viewModelJson"));

            if (string.IsNullOrWhiteSpace(receivedViewModel.BorrowerImportMappingKey))
            {
                throw new GenericUserErrorMessageException(
                    "Could not retrieve saved Symitar borrower mapping due to empty key.");
            }

            var loanId = this.sLId;

            var appId = this.aAppId;
            
            var dataToApplyIndices = receivedViewModel.BorrowerImportTableList.SelectIndicesWhere(entry => entry.Include);

            var principal = PrincipalFactory.CurrentPrincipal;
            var importMapping = MappedSymitarImport.ConstructFromSavedCopy(principal, receivedViewModel.BorrowerImportMappingKey);

            SymitarManager.SaveToLoan(principal, loanId, appId, importMapping, dataToApplyIndices);

            var updatedBorrowerTableList = this.UpdateViewModelTableList(receivedViewModel.BorrowerImportTableList, importMapping, loanId, appId);

            var returnViewModel = new ImportFromSymitarViewModel()
            {
                BorrowerImportMappingKey = receivedViewModel.BorrowerImportMappingKey,
                BorrowerImportTableList = updatedBorrowerTableList
            };

            if (!string.IsNullOrWhiteSpace(receivedViewModel.CoborrowerImportMappingKey))
            {
                dataToApplyIndices = receivedViewModel.CoborrowerImportTableList.SelectIndicesWhere(entry => entry.Include);

                importMapping = MappedSymitarImport.ConstructFromSavedCopy(principal, receivedViewModel.CoborrowerImportMappingKey);

                SymitarManager.SaveToLoan(principal, loanId, appId, importMapping, dataToApplyIndices);

                returnViewModel.CoborrowerImportMappingKey = receivedViewModel.CoborrowerImportMappingKey;

                returnViewModel.CoborrowerImportTableList = this.UpdateViewModelTableList(receivedViewModel.CoborrowerImportTableList, importMapping, loanId, appId);
            }

            this.SetResult("Status", "OK");
            this.SetResult("ReturnViewModel", SerializationHelper.JsonNetSerialize(returnViewModel));
        }

        /// <summary>
        /// Updates the list of import entries to reflect the new values on
        /// the loan, preserving which value the user chose to import.
        /// </summary>
        /// <param name="receivedViewModelList">
        /// The list of import entries received from the page.
        /// </param>
        /// <param name="importMapping">
        /// The <see cref="MappedSymitarImport"/> used for the import.
        /// </param>
        /// <param name="loanId">
        /// The <see cref="Guid"/> of the loan that was saved.
        /// </param>
        /// <param name="appId">
        /// The <see cref="Guid"/> of the application that was saved.
        /// </param>
        /// <returns>
        /// An updated list of import entries that reflect the values
        /// saved to the loan.
        /// </returns>
        private List<SymitarImportEntry> UpdateViewModelTableList(
            List<SymitarImportEntry> receivedViewModelList,
            MappedSymitarImport importMapping,
            Guid loanId,
            Guid appId)
        {
            var importList = SymitarManager.MapToDisplayLogic(importMapping, loanId, appId);

            // We want to preserve the user's choice for which value to include.
            for (int i = 0; i < receivedViewModelList.Count; i++)
            {
                importList[i].Include = receivedViewModelList[i].Include;
            }

            return importList;
        }
    }
}
