﻿// <copyright file="ImportFromSymitar.aspx.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Michael Leinweaver
//  Date:   2015-12-04 09:12:55 -0800
// </summary>
#region Generated Code
namespace LendersOfficeApp.newlos.Import
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Provides a means for users to import data from Symitar and apply
    /// that data to a loan file.
    /// </summary>
    public partial class ImportFromSymitar : BaseLoanPage
    {
        /// <summary>
        /// Performs initial steps for page load.
        /// </summary>
        /// <param name="e">
        /// The event arguments for the initial page load steps.
        /// </param>
        protected override void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;
            base.OnInit(e);
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The activator for the page initialization.
        /// </param>
        /// <param name="e">
        /// The event arguments for the page initialization.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.RegisterCSS("stylesheetnew.css");
            this.RegisterCSS("jquery-ui-1.11.custom.css");

            this.RegisterJsScript("mask.js");
            this.RegisterJsScript("angular-1.4.8.min.js");
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterJsScript("ImportFromSymitar.js");
            this.EnableJqueryMigrate = false;
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The activator of the page load.
        /// </param>
        /// <param name="e">
        /// The event arguments for the page load.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(ImportFromSymitar));
            dataLoan.InitLoad();

            if (!this.Broker.IsSymitarIntegrationEnabled(dataLoan.sLoanFileT))
            {
                var message = string.Format(
                    "User {0} with broker {1} attempted to access the Symitar import page when the broker does not have Symitar enabled.",
                    this.EmployeeID,
                    this.BrokerID);

                ErrorUtilities.DisplayErrorPage(
                    new GenericUserErrorMessageException(message),
                    isSendEmail: false,
                    brokerID: this.BrokerID,
                    employeeID: this.EmployeeID);
            }

            var application = dataLoan.GetAppData(this.ApplicationID);

            this.aBCoreSystemId.Value = application.aBCoreSystemId;

            this.aCCoreSystemId.Value = application.aCCoreSystemId;
        }

        /// <summary>
        /// Obtains the compatibility mode required for Internet Explorer to display the page.
        /// </summary>
        /// <returns>
        /// The forced compatibility mode <see cref="E_XUAComaptibleValue.Edge"/>.
        /// </returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }
    }
}
