using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos
{
	public partial class PropertyInfo : BaseLoanPage
	{
        // Subject Property Description
        protected PropertyInfoUserControl PropertyInfoUserControl;
        
        // Subject Property Details
        protected PropertyDetailsUserControl PropertyDetailsUserControl;
        
        // Rental Income
        protected SubjPropInvestment SubjPropInvestment;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
            IsAppSpecific = true;
            this.EnableJqueryMigrate = false;

            Tabs.AddToQueryString("loanid", LoanID.ToString());
            Tabs.RegisterControl("Subject Property Description", PropertyInfoUserControl);
            Tabs.RegisterControl("Subject Property Details", PropertyDetailsUserControl);
            Tabs.RegisterControl("Rental Income", SubjPropInvestment);
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);

            Tabs.Visible = false;
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

        }
		#endregion


        override protected void OnPreRender(EventArgs e) 
        {

            switch (Tabs.TabIndex) 
            {
                case 0:
                    this.PageTitle = "Property Information";
                    this.PageID = "PropertyInformation";
                    break;
                case 1:
                    this.PageTitle = "Property Details";
                    this.PageID = "PropertyDetails";
                    break;
                case 2:
                    this.PageTitle = "Subject property investment";
                    this.PageID = "SubjectPropertyInvestment";
                    break;
            }
            base.OnPreRender(e);
        }
	}
}
