﻿using System;
using System.Linq;
using LendersOffice.Common;
using LendersOffice.Constants;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos
{
    public class ManageNewFeaturesServiceItem : AbstractBackgroundServiceItem
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "MigrateToLatestUladDataLayer":
                    this.MigrateToLatestUladDataLayer();
                    break;
                default:
                    throw new UnhandledCaseException(methodName);
            }
        }

        protected override DataAccess.CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(ManageNewFeaturesServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            if (!BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowManageNewFeatures))
            {
                return;
            }
            
            dataLoan.sGfeIsTPOTransactionIsCalculated = GetBool("sGfeIsTPOTransactionIsCalculated");
            dataLoan.sExcludeToleranceCureFromFinal1003LenderCredit = GetBool("sExcludeToleranceCureFromFinal1003LenderCredit");

            var sConfLoanLimitDeterminationDLckd = GetBool("sConfLoanLimitDeterminationDLckd");
            var sConfLoanLimitDeterminationD_rep = GetString("sConfLoanLimitDeterminationD");

            // OPM 365584. 
            // The main PageData class uses a security bypass object because the TRID migrations
            // are considered system actions.  sConfLoanLimitDeterminationDLckd saving is not.  
            // Save them on their own, so workflow protections are honored. (They are still a part
            // of the main save so the UI is updated correctly).
            if (dataLoan.DataState == E_DataState.InitSave &&
                ( sConfLoanLimitDeterminationDLckd != dataLoan.sConfLoanLimitDeterminationDLckd
                || sConfLoanLimitDeterminationD_rep != dataLoan.sConfLoanLimitDeterminationD_rep ) )
            {
                SaveConfLoanLimitDetermination(sLId, sConfLoanLimitDeterminationDLckd, sConfLoanLimitDeterminationD_rep);
            }

            dataLoan.sConfLoanLimitDeterminationDLckd = sConfLoanLimitDeterminationDLckd;
            dataLoan.sConfLoanLimitDeterminationD_rep = sConfLoanLimitDeterminationD_rep;

            if (dataLoan.BrokerDB.EnableAutoPriceEligibilityProcess)
            {
                dataLoan.sIncludeInAutoPriceEligibilityProcessT = (E_IncludeInAutoPriceEligibilityProcessT)GetInt("sIncludeInAutoPriceEligibilityProcessT");
            }

            dataLoan.sUseInitialLeSignedDateAsIntentToProceedDate = this.GetBool("sUseInitialLeSignedDateAsIntentToProceedDate");

            E_sClosingCostFeeVersionT newVersion = GetEnum<E_sClosingCostFeeVersionT>("CFPBUISettings");

            // If going from legacy to one of the new, then migrate.
            if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy &&
               newVersion != E_sClosingCostFeeVersionT.Legacy)
            {
                Migrate2015CFPB(newVersion);
            }
            else
            {
                //at least ensure the user and the loan are the same since we turned off the access checks.
                if (dataLoan.sBrokerId !=  PrincipalFactory.CurrentPrincipal.BrokerId)
                {
                    throw new AccessDenied();
                }

                // OPM 228640. If any archive exists currently that is not in the legacy archive,
                // we cannot let users go back to legacy.
                // To do this correctly, we should have this as a datalayer check in the setter
                // of sClosingCostFeeVersionT.  For performance, I am refusing to do that because
                // I do not want to add the entire archive history to the dependency list of the
                // field that commonly checks what mode the loan file is in for this rare situation.

                if (!dataLoan.BrokerDB.AllowMovingFilesBackToLegacyWithMigratedArchives &&
                    dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && 
                    newVersion == E_sClosingCostFeeVersionT.Legacy && 
                    !dataLoan.sClosingCostArchive.TrueForAll(newArchive => dataLoan.GFEArchives.Any(oldArchive => oldArchive.DateArchived == newArchive.DateArchived)))
                {
                    throw new CBaseException("This loan is only allowed to be in migrated statuses because it has been disclosed in a migrated mode.", "This loan is only allowed to be in migrated statuses because it has been disclosed in a migrated mode.");
                }

                dataLoan.sClosingCostFeeVersionT = newVersion;

                if (dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy)
                {
                    dataLoan.sIsHousingExpenseMigrated = true;
                }
                else
                {
                    dataLoan.sIsHousingExpenseMigrated = false;
                }
            }
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            if (!BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowManageNewFeatures))
            {
                return;
            }

            if (dataLoan.BrokerDB.EnableAutoPriceEligibilityProcess)
            {
                SetResult("sIsIncludeInAutoPriceEligibilityProcess", dataLoan.sIsIncludeInAutoPriceEligibilityProcess);
            }

            SetResult("sConfLoanLimitDeterminationDLckd", dataLoan.sConfLoanLimitDeterminationDLckd);
            SetResult("sConfLoanLimitDeterminationD", dataLoan.sConfLoanLimitDeterminationD);
        }

        private void Migrate2015CFPB(E_sClosingCostFeeVersionT version)
        {
            if (!BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowManageNewFeatures))
            {
                return;
            }

            Guid sLId = GetGuid("loanid");


            ClosingCostFeeMigration.Migrate(PrincipalFactory.CurrentPrincipal, sLId, version);

            // Housing expense migration already in ClosingCostFeeMigration
        }

        private void SaveConfLoanLimitDetermination(Guid sLId, bool sConfLoanLimitDeterminationDLckd, string sConfLoanLimitDeterminationD_rep)
        {
            var dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(ManageNewFeaturesServiceItem));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sConfLoanLimitDeterminationDLckd = sConfLoanLimitDeterminationDLckd;
            dataLoan.sConfLoanLimitDeterminationD_rep = sConfLoanLimitDeterminationD_rep;
            dataLoan.Save();
        }

        private void MigrateToLatestUladDataLayer()
        {
            var loanId = this.GetGuid("loanid");
            var loan = CPageData.CreateUsingSmartDependency(loanId, typeof(ManageNewFeaturesServiceItem));
            loan.InitSave();
            loan.MigrateToLatestUladCollectionVersion();
            loan.Save();
        }
    }

    public partial class ManageNewFeaturesService : BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new ManageNewFeaturesServiceItem());
        }
    }
}
