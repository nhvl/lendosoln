<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="AuditTrail.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.AuditTrail" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>AuditTrail</title>
    <style>
      .trackItem { padding-right:10px; }
      table.DataGrid td { border: 1px solid #FFFFFF;  }
      .SortHidden { display: none; }
      .CategoryHidden { display: none; }
      #InputPanel { padding: 5px; }
      #InputPanel label { width: 50px; padding-bottom: .15em; }
      .highlight { background-color: yellow; }
    </style>
  </HEAD>
<body  MS_POSITIONING="FlowLayout" scroll=yes bgcolor=gainsboro>
<script language=javascript>
<!--
function f_viewAuditDetail(id) {
    var url = <%= AspxTools.SafeUrl("AuditDetail.aspx?loanid=" + LoanID + "&auditid=")%> + id;
    window.open(url, "AuditDetail", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes");
  return false;
}

function f_viewAuditDetailVer2(key) {
    var url = <%= AspxTools.SafeUrl("AuditDetail.aspx?key=")%> + key;
    window.open(url, "AuditDetail", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes");
  return false;
}

function f_viewTrackedEvents() {
    window.open("https://support.lendingqb.com/index.php?/Knowledgebase/Article/View/321", "TrackedEvents", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes");
    return false;
}


// Add case insensitive contains for making highlighting easier.
// https://github.com/jquery/sizzle/wiki/Sizzle-Documentation
function icontains(elem, text) {
    return (
        elem.textContent ||
        elem.innerText ||
        $(elem).text() ||
        ""
    ).toLowerCase().indexOf((text || "").toLowerCase()) > -1;
}

$.expr[':'].icontains = $.expr.createPseudo ?
    $.expr.createPseudo(function(text) {
        return function (elem) {
            return icontains(elem, text);
        };
    }) :
    function(elem, i, match) {
        return icontains(elem, match[3]);
    };

// For escaping special characters that the user may search for
RegExp.escape = function(str) {
    var specials = /[.*+?|()\[\]{}\\$^]/g; // .*+?|()[]{}\$^
    return str.replace(specials, '\\$&');
}

function highlightMatchingString(line, searchText) {
    var regex = new RegExp('(' + RegExp.escape(searchText) + ')', 'gi');
    return line.replace(regex, '<span class="highlight">$1</span>');
}

$(document).ready(function() {

    resizeForIE6And7(800, 600);

    $.tablesorter.defaults.widgetZebra = {
        css: ["GridItem", "GridAlternatingItem"]
    };
    
    var $table = $('#AuditTable');
    
    var sorting = [[3, 1]]; // 6/4/2014 dd - Default the sort order to timestamp desc
    if ($table.find('tbody').children().length !== 0) {
        $table.tablesorter({
            sortList: sorting,
            widgets: ['zebra']
        });
    }
    
    var $rows = $('tbody tr', $table);
    var searchDataCache = [];
    $rows.each(function() {
        var temp = [];
        $('td', this).each(function() {
            temp.push($(this).text().toLowerCase());
        });
        searchDataCache.push(temp.join(' '));
    });
    
    function doSearch(val) {
        val = val.toLowerCase();
        $('.SortHidden').removeClass('SortHidden');
        // Remove all of the highlight spans added for the previous search
        $('.highlight').contents().unwrap();

        if ($.trim(val) === '') return;

        var len = searchDataCache.length;
        for (var i = 0; i < len; i++) {
            if (searchDataCache[i].indexOf(val) == -1) {
                $rows[i].className += ' SortHidden';
            }
            else {
                // Highlight the matching text.
                // NOTE: we are only expecting the 'view detail' anchor. If there
                // is unexpected markup, don't highlight. Otherwise, invalid
                // markup can result.
                $rows.eq(i).find('td:icontains("' + val + '")').each(function(index, tdElement) {
                    var $td = $(tdElement);
                    var $children = $td.children();
                    var $anchor = $children.filter('a');
                    
                    if ($children.length === 0
                        || ($children.length === 1 && $anchor.length === 1 && $anchor.text() === 'view detail')) {
                        $anchor.detach();
                        $td.html(highlightMatchingString($td.html(), val));
                        $td.append($anchor);
                    }
                });
            }
        }
    }
    
    $('#m_searchTB').keypress(function(event) {
        if (event.keyCode === 13) {
            $('#m_searchBtn').focus().click();
            return false;
        }
    });
    
    $('#m_searchBtn').click(function() {
        if($table.length > 0)
        {
            doSearch($('#m_searchTB').val());
            // Trigger table view update.
            $table.trigger("sorton", [$table[0].config.sortList]); 
        } 
    });
    
    $('#m_categoryDDL').change(function() {
        var val = $(this).val();
        if (val === 'All') {
            $rows.removeClass('CategoryHidden');
        }
        else if (val === 'DisclosuresArchived') {
            $rows.addClass('CategoryHidden');
            $table.find('.GFEArchived').removeClass('CategoryHidden');
            $table.find('.LoanEstimateArchived').removeClass('CategoryHidden');
            $table.find('.ClosingDisclosureArchived').removeClass('CategoryHidden');
        }
        else {
            $rows.addClass('CategoryHidden');
            $table.find('.' + val).removeClass('CategoryHidden');
        }
        // Trigger table view update.
        $table.trigger("sorton", [$table[0].config.sortList]);
    });
    if($table.length > 0)
        $('#m_categoryDDL').change();
});

//-->
</script>

<form id="AuditHistory" runat="server">
  <div class="MainRightHeader">Audit History</div>
  <div id="InputPanel">
      <input type="text" id="m_searchTB" NotForEdit />
      <input type="button" id="m_searchBtn" value="Search" />
      <label>Category</label><asp:DropDownList ID="m_categoryDDL" runat="server" NotForEdit></asp:DropDownList>
  </div>
  <asp:Repeater runat="server" ID="AuditItems" OnItemDataBound="AuditItems_OnItemDataBound">
        <HeaderTemplate>
            <table id="AuditTable" class="tablesorter" style="width: 100%;">
            <thead>
                <tr class="GridHeader">
                    <th>Category</th>
                    <th>Event</th>
                    <th>User Name</th>
                    <th>Timestamp</th>
                </tr>
            </thead>
            <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr runat="server" id="Row">
                <td>
                    <ml:EncodedLiteral runat="server" ID="Category"></ml:EncodedLiteral>
                </td>
                <td>
                    <ml:EncodedLiteral runat="server" ID="Event"></ml:EncodedLiteral>
                    <%# AspxTools.HtmlControl(RenderDetailsLink((LendersOffice.Audit.LightWeightAuditItem)Container.DataItem)) %>
                </td>
                <td>
                    <ml:EncodedLiteral runat="server" ID="UserName"></ml:EncodedLiteral>
                </td>
                <td>
                    <ml:EncodedLiteral runat="server" ID="Timestamp"></ml:EncodedLiteral>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
  </asp:Repeater>

&nbsp;&nbsp;<a href="#" onclick="return f_viewTrackedEvents();" >What fields are currently tracked?</a>
</form>
</body>
</HTML>
