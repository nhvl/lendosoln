using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Admin;

namespace LendersOfficeApp.newlos
{
	public partial class BorrowerLiabilityList : BaseListEditPage
	{
        protected string recordIDREOMatch
        {
            get { return Request["RecordID"]; }
        }

        protected bool fromREO
        {
            get { return Request["RecordID"] != "";}
        }

        protected bool HasPmlEnabled
        {
            get { return BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan); }
        }
        private void PageLoad(object sender, System.EventArgs e)
        {

        }

		protected string displayOwnerType(string type) 
		{
			try
			{
				return CLiaFields.DisplayStringOfOwnerT( (E_LiaOwnerT) int.Parse( type ) );
			}
			catch
			{	return "B"; }// Make unknown type and default become borrower.
		}

        protected string displayLiabilityType(string type) 
        {
            string str = "";
            try 
            {
                E_DebtRegularT debtType = (E_DebtRegularT) int.Parse(type);
                return CLiaRegular.DisplayStringOfDebtT( debtType );
            } 
            catch {}
            return str;
        }
        protected string displayMoneyString(string value) 
        {

            if (value == null || value == "") 
            {
                return "";
            }
            try 
            {
                value = value.Replace("*", "");
                return decimal.Parse(value).ToString("C");
            } 
            catch {}
            // Liability value import from credit not always return correct format.
            return value;
        }

        protected string displayPayoffCheckbox(object value, object id) 
        {
            return CLiaRegular.DisplayWillBePdOff( value != null && value.ToString() == "True");
        }
        protected string displayUsedInRatiodCheckbox(object value, object id, object t) 
        {
            E_DebtT debtT = E_DebtT.Other;

            try 
            {
                if (t != null) 
                {
                    debtT = (E_DebtT) int.Parse(t.ToString());
                }
            } 
            catch {}

            string str = "";
            if (debtT == E_DebtT.Mortgage) 
            {
                str = "See REO";
            }
            else 
            {
				str = CLiaRegular.DisplayUsedInRatio( value == null || value.ToString() != "True" );
            }
            return str;
        }
        protected string displayIsPmlAltered(object value) 
        {
            if (null == value)
                return "No";

            string s = value.ToString();
            if (s == "" || s == null)
                return "No";
            else
                return "Yes";
        }
        protected override void LoadData() 
        {

            CPageData dataLoan = new CLiaData(LoanID, false);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            ILiaCollection recordList = dataApp.aLiaCollection;
            DataView sortedView = recordList.SortedView;
            m_dg.DataSource = sortedView;
            m_dg.DataBind();

            m_dg.Columns[0].Visible = HasPmlEnabled;


        }

        public override bool IsReadOnly 
        {
            // 7/13/2005 dd - OPM 2351: Protect liabilities from being edited by AE.
            get { return base.IsReadOnly || BrokerUser.IsAccountExecutiveOnly; }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.m_dg.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DataGrid_ItemCreated);

        }
		#endregion

	}
}
