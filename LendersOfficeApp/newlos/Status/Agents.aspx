<%@ Page Language="c#" CodeBehind="Agents.aspx.cs" AutoEventWireup="True" Inherits="LendersOfficeApp.newlos.Status.Agents" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<html>
<head runat="server">
    <title>Agents</title>
    <style type="text/css">
        .SideTable { float: left; display:block; }
        .SideMargin  { margin-left: 50px; }
        .MiniSideMargin { margin-left: 58px; }
        .box { border: 1px solid black;  padding-right: 5px; padding-left:5px; }
        .modalbox {  height: 100px; padding: 10px; border:3px inset black; top: 200px;  margin-left: 32px; left: 0; right: 0;  width: 675px; display: none; position: absolute; background-color : whitesmoke;}
        .ErrorColor { color: Red; }
        .HeaderPadding { padding-bottom: 6px; padding-left: 3px;}
        .OCLabel { float: left; width: 12.5em; }
        .SideTable td{ padding-bottom: 10px; }
        #Table3, #Table5 { margin-left: 2px; }
        .FormTableSubheader { padding: 3px 5px; }
    </style>

</head>
<body  class="RightBackground">
    <form id="Agents" method="post" runat="server">
        <div id="HelpDiv" style="z-index: 900" class="modalbox" style="width: 300px">
            The Originating Company is applied when the Loan Officer, Processor (External),
            Secondary (External), or Post-Closer (External) are
            assigned. Note that the Loan Officer, Processor (External), Secondary (External),
            and Post-Closer (External) must belong to the same Originating
            Company.
            <br />
            <br />
            <div style="text-align: center">
                [<a href="#" id='wmsgClose' onclick="Modal.Hide()"> Close </a>]</div>
        </div>
        <asp:HiddenField runat="server" ID="IsPageReadOnly" />
        <table class="FormTable" id="Table1" cellspacing="0" cellpadding="0" width="98%"
            border="0">
            <tr>
                <td class="MainRightHeader" valign="top" colspan="2">
                    Agents
                </td>
            </tr>
            <tr>
                <td class="FormTableSubheader" valign="top" colspan="2">
                    INTERNAL ASSIGNMENT
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="2">
                <div>
                    <ul class="tabnav">
                        <li id="tab0"><a onclick="switchAssignmentTab(0); return false;" href="#" class="tabLink" class="selected">Roles</a></li>
                        <li runat="server" id="tab1"><a class="tabLink" id="tabLink1" onclick="switchAssignmentTab(1); return false;" href="#">Teams</a></li>
                    </ul>
                </div>
                <div id="AgentAssignment">
                    <div id="InternalAgentAssignment">
                        <table id="Table5"  class="SideTable" cellspacing="0" cellpadding="2" border="0">
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Loan Officer
                            </td>
                            <td valign="top" nowrap="nowrap">
                                <span>
                                <a ID="m_agentLink" runat="server" style="margin:0; "></a>
                                &nbsp;<a id="m_agentAssignLink" runat="server" class="assignLink" onclick="AgentsPage.displayPeopleInRole('Agent', true);" href="#">(assign)</a>&nbsp;&nbsp;
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Processor
                            </td>
                            <td valign="top">
                                <a ID="m_processorLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('Processor');"
                                    href="#">(assign)</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Underwriter
                            </td>
                            <td valign="top">
                                <a ID="m_underwriterLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('Underwriter');"
                                    href="#">(assign) </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Doc Drawer
                            </td>
                            <td valign="top">
                                <a ID="m_docDrawerLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('DocDrawer');" href="#">(assign)
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Lock Desk
                            </td>
                            <td valign="top">
                                <a ID="m_lockDeskLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('LockDesk');"
                                    href="#">(assign) </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Credit Auditor
                            </td>
                            <td valign="top">
                                <a ID="m_creditAuditorLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('CreditAuditor');" href="#">(assign)
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Secondary
                            </td>
                            <td valign="top">
                                <a ID="m_secondaryLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('Secondary');" href="#">(assign)
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Insuring
                            </td>
                            <td valign="top">
                                <a ID="m_insuringLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('Insuring');"
                                    href="#">(assign)</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Manager
                            </td>
                            <td valign="top">
                                <a ID="m_managerLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('Manager');"
                                    href="#">(assign)</a>
                            </td>
                        </tr>
                    </table>
                        <table class="SideTable SideMargin" cellspacing="0" cellpadding="2" border="0" id="Table6">
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Loan Officer Assistant
                            </td>
                            <td valign="top">
                                <a ID="m_loanOfficerAssistantLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('LoanOfficerAssistant');" href="#">(assign)
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Junior Processor
                            </td>
                            <td valign="top">
                                <a ID="m_juniorProcessorLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('JuniorProcessor');" href="#">(assign)
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Junior Underwriter
                            </td>
                            <td valign="top">
                                <a ID="m_juniorUnderwriterLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('JuniorUnderwriter');" href="#">(assign)
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Collateral Agent
                            </td>
                            <td valign="top">
                                <a ID="m_collateralAgentLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('CollateralAgent');"
                                    href="#">(assign)</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Disclosure Desk
                            </td>
                            <td valign="top">
                                <a ID="m_disclosureDeskLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('DisclosureDesk');" href="#">(assign)
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Legal Auditor
                            </td>
                            <td valign="top">
                                <a ID="m_legalAuditorLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('LegalAuditor');" href="#">(assign)
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Shipper
                            </td>
                            <td valign="top">
                                <a ID="m_shipperLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('Shipper');"
                                    href="#">(assign)</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Post-Closer
                            </td>
                            <td valign="top">
                                <a ID="m_postCloserLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('PostCloser');"
                                    href="#">(assign)</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Real Estate&nbsp;Agent&nbsp;
                            </td>
                            <td valign="top">
                                <a ID="m_realEstateLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('RealEstateAgent');"
                                    href="#">(assign)</a>
                            </td>
                        </tr>
                    </table>
                        <table class="SideTable SideMargin" cellspacing="0" cellpadding="2" border="0" id="Table7">
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap>
                                Loan Opener
                            </td>
                            <td valign="top">
                                <a ID="m_loanOpenerLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('LoanOpener');"
                                    href="#">(assign)</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                QC Compliance
                            </td>
                            <td valign="top">
                                <a ID="m_qcComplianceLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('QCCompliance');" href="#">(assign)
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Funder
                            </td>
                            <td valign="top">
                                <a ID="m_funderLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('Funder');"
                                    href="#">(assign)</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap>
                                Lender Account Executive
                            </td>
                            <td valign="top">
                                <a ID="m_lenderAcctExecLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('LenderAccountExec');"
                                    href="#">(assign) </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Purchaser
                            </td>
                            <td valign="top">
                                <a ID="m_purchaserLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('Purchaser');" href="#">(assign)
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Closer
                            </td>
                            <td valign="top">
                                <a ID="m_closerLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('Closer');" href="#">(assign)
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Servicing
                            </td>
                            <td valign="top">
                                <a ID="m_servicingLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('Servicing');" href="#">(assign)
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Call Center Agent&nbsp;&nbsp;&nbsp;
                            </td>
                            <td valign="top">
                                <a ID="m_telemarketerLink" runat="server"></a>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayPeopleInRole('Telemarketer');"
                                    href="#">(assign)</a>
                            </td>
                        </tr>
                    </table>
                    </div>
                    <div style="clear: both;"></div>
                    <div id="ExternalAgentAssignment">
                        <div class="FormTableSubHeader">External Agent Assignment</div>
                        <asp:Placeholder runat="server" visible="false" id="OriginatingCompanyNameRow">
                        <table class="SideTable">
                            <tr>
                                <td class="FieldLabel HeaderPadding">
                                    <label class="OCLabel">Originating Company:</label>
                                </td>
                                <td class="FieldLabel">
                                    <ml:EncodedLiteral runat="server" ID="OriginatingCompanyName"></ml:EncodedLiteral>
                                    &nbsp;&nbsp; <a onclick="Modal.Show('HelpDiv', 'wmsgClose')" class="box">?</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel HeaderPadding">
                                    <label class="OCLabel">Originating Company Index Number:</label>
                                </td>
                                <td class="FieldLabel">
                                    <ml:EncodedLiteral runat="server" ID="OriginatingCompanyIndexNumber"></ml:EncodedLiteral>
                                </td>
                                <td class="FieldLabel HeaderPadding">
                                    <label class="OCLabel">Originating Company ID:</label>
                                </td>
                                <td class="FieldLabel">
                                    <ml:EncodedLiteral runat="server" ID="OriginatingCompanyID"></ml:EncodedLiteral>
                                </td>
                            </tr>
                        </table>
                        <table class="SideTable MiniSideMargin">
                            <tr>
                                <td class="fieldLabel">
                                    Company Status for this Loan Channel:
                                </td>
                                <td valign="top" nowrap="nowrap">
                                    <span>
                                        <p runat="server" id="sPmlBrokerStatusT" style="margin: auto;" class="FieldLabel"></p>
                                    </span>
                                </td>
                            </tr>
                        </table>
                        </asp:Placeholder>
                        <table style="clear: both;">
                            <tr id="trPmlCompanyTierDesc" runat="server">
                                <td colspan="2" class="FieldLabel HeaderPadding" nowrap>
                                    <label class="OCLabel">
                                        Originating Company Tier:
                                    </label>
                                    <ml:EncodedLiteral runat="server" ID="sPmlCompanyTierDesc"></ml:EncodedLiteral>
                                </td>
                            </tr>
                            <tr runat="server" id="mOCError" class="FieldLabel" visible="false">
                                <td colspan="2" class="HeaderPadding">
                                    <span class="ErrorColor">The assigned
                                        <ml:EncodedLiteral runat="server" ID="OCRoles"></ml:EncodedLiteral>
                                        does not belong to
                                        <ml:EncodedLiteral runat="server" ID="OriginatingCompanyName1"></ml:EncodedLiteral>
                                        and therefore does not have access to this loan.</span>
                                    <br />
                                    To give access to the assigned
                                    <ml:EncodedLiteral runat="server" ID="OCRoles1"></ml:EncodedLiteral>, re-assign them to the
                                    loan file. Otherwise please assign to another <ml:EncodedLiteral runat="server" ID="OCRoles11"/>. Note that the Loan
                                    Officer, Processor (External), Secondary (External), and Post-Closer (External) must belong to the same Originating Company.
                                </td>
                            </tr>
                        </table>
                        <table class="SideTable">
                            <tr>
                                <td class="FieldLabel">Loan Officer (External)</td>
                                <td>
                                    <span>
                                        <a ID="m_brokerAgentLink" runat="server" style="margin:0; "></a>
                                        &nbsp;
                                        <a class="assignLink" onclick="AgentsPage.displayPeopleInRole('Agent', false, true);" href="#">(assign)</a>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">Secondary (External)</td>
                                <td>
                                    <span>
                                        <a ID="m_externalSecondaryLink" runat="server" style="margin:0; "></a>
                                        &nbsp;
                                        <a class="assignLink" runat="server" id="m_externalSecondaryAssignLink" onclick="AgentsPage.displayPeopleInRole('ExternalSecondary');" href="#">(assign)</a>
                                    </span>
                                </td>
                            </tr>
                        </table>
                        <table class="SideTable SideMargin">
                            <tr>
                                <td class="fieldLabel">Processor (External)</td>
                                <td valign="top" nowrap="nowrap">
                                    <span>
                                        <a ID="m_brokerprocessorLink" runat="server" style="margin:auto"></a>
                                        &nbsp;
                                        <a runat="server" id="bp_assign_link" class="assignLink" onclick="AgentsPage.displayPeopleInRole('BrokerProcessor');" href="#">(assign)</a>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">Post-Closer (External)</td>
                                <td>
                                    <span>
                                        <a ID="m_externalPostCloserLink" runat="server" style="margin:0; "></a>
                                        &nbsp;
                                        <a class="assignLink" runat="server" id="m_externalPostCloserAssignLink" onclick="AgentsPage.displayPeopleInRole('ExternalPostCloser');" href="#">(assign)</a>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div id="TeamAssignment">
                    <table id="Table3"  class="SideTable" cellspacing="0" cellpadding="2" border="0">
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Loan Officer
                            </td>
                            <td valign="top" nowrap="nowrap">
                                <span>
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('Agent');" href="#"><span id="m_agentTeam" runat="server">(assign)</span></a>&nbsp;&nbsp;
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Processor
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('Processor');"
                                    href="#"><span id="m_processorTeam" runat="server">(assign)</span></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Underwriter
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('Underwriter');"
                                    href="#"><span id="m_underwriterTeam" runat="server">(assign)</span> </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Doc Drawer
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('DocDrawer');" href="#"><span id="m_docDrawerTeam" runat="server">(assign)</span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Lock Desk
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('LockDesk');"
                                    href="#"><span id="m_lockDeskTeam" runat="server">(assign)</span> </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Credit Auditor
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('CreditAuditor');" href="#"><span id="m_creditAuditorTeam" runat="server">(assign)</span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Secondary
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('Secondary');" href="#"><span id="m_secondaryTeam" runat="server">(assign)</span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Insuring
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('Insuring');"
                                    href="#"><span id="m_insuringTeam" runat="server">(assign)</span></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Manager
                            </td>
                            <td valign="top">
                                &nbsp;<a class="m_managerTeam" onclick="AgentsPage.displayTeamsInRole('Manager');"
                                    href="#"><span id="m_managerTeam" runat="server">(assign)</span></a>
                            </td>
                        </tr>
                    </table>
                    <table class="SideTable SideMargin" cellspacing="0" cellpadding="2" border="0" id="Table2">
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Loan Officer Assistant
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('LoanOfficerAssistant');" href="#"><span id="m_loanOfficerAssistantTeam" runat="server">(assign)</span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Junior Processor
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('JuniorProcessor');" href="#"><span id="m_juniorProcessorTeam" runat="server">(assign)</span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Junior Underwriter
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('JuniorUnderwriter');" href="#"><span id="m_juniorUnderwriterTeam" runat="server">(assign)</span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Collateral Agent
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('CollateralAgent');"
                                    href="#"><span id="m_collateralAgentTeam" runat="server">(assign)</span></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Disclosure Desk
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('DisclosureDesk');" href="#"><span id="m_disclosureDeskTeam" runat="server">(assign)</span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Legal Auditor
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('LegalAuditor');" href="#"><span id="m_legalAuditorTeam" runat="server">(assign)</span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Shipper
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('Shipper');"
                                    href="#"><span id="m_shipperTeam" runat="server">(assign)</span></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Post-Closer
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('PostCloser');"
                                    href="#"><span id="m_postCloserTeam" runat="server">(assign)</span></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Real Estate&nbsp;Agent&nbsp;
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('RealEstateAgent');"
                                    href="#"><span id="m_realEstateTeam" runat="server">(assign)</span></a>
                            </td>
                        </tr>
                    </table>
                    <table class="SideTable SideMargin" cellspacing="0" cellpadding="2" border="0" id="Table4">
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap>
                                Loan Opener
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('LoanOpener');"
                                    href="#"><span id="m_loanOpenerTeam" runat="server">(assign)</span></a>
                            </td>
                        </tr>
                        <asp:PlaceHolder runat="server" ID="BrokerProcessorSettings">
                            <tr>
                                <td class="fieldLabel" valign="top" nowrap="nowrap">
                                    Processor (External)
                                </td>
                                <td valign="top" nowrap="nowrap">
                                    <span>
                                    &nbsp;<a runat="server" id="bp_assign_TeamLink" class="assignLink" onclick="AgentsPage.displayTeamsInRole('BrokerProcessor');"
                                        href="#"><span id="m_brokerprocessorTeam" runat="server">(assign)</span></a>
                                        </span>
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                QC Compliance
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('QCCompliance');" href="#"><span id="m_qcComplianceTeam" runat="server">(assign)</span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Funder
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('Funder');"
                                    href="#"><span id="m_funderTeam" runat="server">(assign)</span></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap>
                                Lender Account Executive
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('LenderAccountExec');"
                                    href="#"><span id="m_lenderAcctExecTeam" runat="server">(assign)</span> </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Purchaser
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('Purchaser');" href="#"><span id="m_purchaserTeam" runat="server">(assign)</span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Closer
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('Closer');" href="#"><span id="m_closerTeam" runat="server">(assign)</span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top">
                                Servicing
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('Servicing');" href="#"><span id="m_servicingTeam" runat="server">(assign)</span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" valign="top" nowrap="nowrap">
                                Call Center Agent&nbsp;&nbsp;&nbsp;
                            </td>
                            <td valign="top">
                                &nbsp;<a class="assignLink" onclick="AgentsPage.displayTeamsInRole('Telemarketer');"
                                    href="#"><span id="m_telemarketerTeam" runat="server">(assign)</span></a>
                            </td>
                        </tr>
                    </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr class="picker">
                <td valign="top" colspan="2">
                    <p class="FormTableSubheader" align="left">
                        OFFICIAL CONTACT LIST FOR THIS LOAN</p>
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="2" style="height: 20px">
                    <input onclick="displayPeopleRecord(<%= AspxTools.JsString(Guid.Empty.ToString()) %>);" type="button" value="Add People..."
                        accesskey="A" id="btnAddPeople">&nbsp;&nbsp;&nbsp;
                        <input onclick="AgentsPage.confirmDelete();" type="button" value="Delete checked" id="btnDelete" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr class="picker">
                <td valign="top" align="left" colspan="2">
                    <ml:CommonDataGrid ID="m_agentsDG" runat="server">
                        <AlternatingItemStyle CssClass="GridAlternatingItem picker"></AlternatingItemStyle>
                        <ItemStyle CssClass="GridItem picker"></ItemStyle>
                        <HeaderStyle CssClass="GridHeader picker"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Select">
                                <ItemTemplate>
                                    <% if (IsPickerDialog)
                                       { %>
                                    <a href="#" class="select" data-id='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "RecordId").ToString()) %>'>select</a>
                                    <% }
                                       else
                                       { %>
                                    <input type="checkbox" name="del" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "RecordId").ToString()) %>"
                                        onclick='highlightRowByCheckbox(this);'>
                                    <% } %>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Type" SortExpression="AgentRoleDescription">
                                <ItemTemplate>
                                    <%# AspxTools.HtmlString(Eval("AgentRoleDescription").ToString())%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Agent Name" SortExpression="AgentName">
                                <ItemTemplate>
                                    <%# AspxTools.HtmlString(Eval("AgentName").ToString())%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Company Name" SortExpression="CompanyName" ItemStyle-CssClass="compName">
                                <ItemTemplate>
                                    <%# AspxTools.HtmlString(GetAgentCompanyName(DataBinder.Eval(Container.DataItem, "RecordId").ToString()))%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Phone" HeaderText="Phone"></asp:BoundColumn>
                            <asp:HyperLinkColumn DataTextField="EmailAddr" DataNavigateUrlField="EmailAddr" DataNavigateUrlFormatString="mailto:{0}" HeaderText="Email" />
                             <asp:TemplateColumn HeaderText="Lender Affiliate?">
                                <ItemTemplate>
                                    <%# AspxTools.HtmlString(displayYesNo(Eval("IsLenderAffiliate")))%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Originator Affiliate?">
                                <ItemTemplate>
                                    <%# AspxTools.HtmlString(displayYesNo(Eval("IsOriginatorAffiliate")))%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderStyle-CssClass="editCol" ItemStyle-CssClass="editCol" >
                                <ItemTemplate>
                                    <a href="#" onclick=<%#AspxTools.HtmlAttribute("displayPeopleRecord(" + AspxTools.JsString(DataBinder.Eval(Container.DataItem, "RecordId").ToString()) + ")")%>>
                                        edit</a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </ml:CommonDataGrid>
                </td>
            </tr>
            <tr class="picker">
                <td class="FieldLabel" valign="top" align="center" colspan="2">
                    <input type="button" id="ClearAgent" class="clearAgent" value="Clear Agent" />
                    &nbsp;
                </td>
            </tr>
        </table>

        <script type="text/javascript">

            var AgentsPage = (function(){
                var m_isReadOnly = false;

                function init(){
                    var IsPageReadOnly = document.getElementById('<%= AspxTools.ClientId(IsPageReadOnly) %>');

                    m_isReadOnly = IsPageReadOnly.value == 'true';
                    document.getElementById("btnAddPeople").disabled = m_isReadOnly;
                    document.getElementById("btnDelete").disabled = m_isReadOnly;


                }

                function confirmDelete(){
                   if(m_isReadOnly){
                        return false;
                    }

                    var collection = document.getElementsByName('del');
                    var length = collection.length;

                    var isChecked = false;
                    var checkList = [];
                    for (var i = 0; i < length; i++) {
                        if (collection[i].checked == true) {
                            isChecked = true;
                            checkList.push(collection[i].value);
                            //break;
                        }
                    }

                    if (!isChecked) {
                        alert('No agent selected.');
                        return false;
                    }

                    var bConfirm = confirm('Do you want to delete these agents?');
                    if (bConfirm == false) {
                      return false;
                    }

                    length = checkList.length;
                    var args = {};
                    args.LoanID = ML.sLId;
                    args.Count = length + '';
                    args.sFileVersion = document.getElementById("sFileVersion").value;
                    for (var i = 0; i < length; i++) {
                      args["id" + i] = checkList[i];
                    }
                    var result = gService.loanedit.call("DeleteOfficialContact", args);
                    if (!result.error)
                    {
                      location.href = location.href;
                    } else {
                        if (result.ErrorType === 'VersionMismatchException')
                        {
                          f_displayVersionMismatch();
                        }
                        else if (result.ErrorType === 'LoanFieldWritePermissionDenied')
                        {
                          f_displayFieldWriteDenied(result.UserMessage);
                        }
                        else
                        {
                          var errMsg = result.UserMessage || 'Unable to delete agent. Please try again';
                          alert(errMsg);
                        }

                    }

                }

                function displayTeamsInRole(role)
                {
                if(m_isReadOnly){
                        alert('Cannot perform assignment because this loan file is read-only.');
                        return;
                    }
                    var name;
                    var roledesc;
                    var show;
                    var elementName = "";

                    switch(role) {
                        case <%=AspxTools.JsString(ConstApp.ROLE_LOAN_OFFICER)%>:
                            name = '<%=AspxTools.ClientId(m_agentLink)%>';
                            roledesc = 'Loan+Officer';
                            elementName = "m_agentTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_BROKERPROCESSOR)%>:
                            name = '<%=AspxTools.ClientId(m_brokerprocessorLink)%>';
                            roledesc = 'Processor+(External)';
                             elementName = "brokerprocessorTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_PROCESSOR)%>:
                            name = '<%=AspxTools.ClientId(m_processorLink)%>';
                            roledesc = 'Processor';
                             elementName = "m_processorTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_MANAGER)%>:
                            name = '<%=AspxTools.ClientId(m_managerLink)%>';
                            roledesc = 'Manager';
                             elementName = "m_managerTeam";
                        break;
                        case <%=AspxTools.JsString(ConstApp.ROLE_REAL_ESTATE_AGENT)%> :
                            name = '<%=AspxTools.ClientId(m_realEstateLink)%>';
                            roledesc = 'Real+Estate+Agent';
                            elementName = "m_realEstateTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_CALL_CENTER_AGENT)%> :
                            name = '<%=AspxTools.ClientId(m_telemarketerLink)%>';
                            roledesc = 'Call+Center+Agent';
                            elementName = "m_telemarketerTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_LENDER_ACCOUNT_EXEC)%> :
                            name = '<%=AspxTools.ClientId(m_lenderAcctExecLink)%>';
                            roledesc = 'Lender+Account+Executive';
                            elementName = "m_lenderAcctExecTeam";
                            break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_LOCK_DESK)%> :
                            name = '<%=AspxTools.ClientId(m_lockDeskLink)%>';
                            roledesc = 'Lock+Desk+Agent';
                            elementName = "m_lockDeskTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_UNDERWRITER)%> :
                            name = '<%=AspxTools.ClientId(m_underwriterLink)%>';
                            roledesc = 'Underwriter';
                            elementName = "m_underwriterTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_LOAN_OPENER)%> :
                            name = '<%=AspxTools.ClientId(m_loanOpenerLink)%>';
                            roledesc = 'Loan+Opener';
                            elementName = "m_loanOpenerTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_CLOSER) %>:
                            name = '<%=AspxTools.ClientId(m_closerLink)%>';
                            roledesc = 'Closer';
                            elementName = "m_closerTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_SHIPPER) %>:
                            name = '<%=AspxTools.ClientId(m_shipperLink)%>';
                            roledesc = 'Shipper';
                            elementName = "m_shipperTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_FUNDER) %>:
                            name = '<%=AspxTools.ClientId(m_funderLink)%>';
                            roledesc = 'Funder';
                            elementName = "m_funderTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_POSTCLOSER) %>:
                            name = '<%=AspxTools.ClientId(m_postCloserLink)%>';
                            roledesc = 'Post+Closer';
                            elementName = "m_postCloserTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_INSURING) %>:
                            name = '<%=AspxTools.ClientId(m_insuringLink)%>';
                            roledesc = 'Insuring';
                            elementName = "m_insuringTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_COLLATERALAGENT) %>:
                            name = '<%=AspxTools.ClientId(m_collateralAgentLink)%>';
                            roledesc = 'Collateral+Agent';
                            elementName = "m_collateralAgentTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_DOCDRAWER) %>:
                            name = '<%=AspxTools.ClientId(m_docDrawerLink)%>';
                            roledesc = 'Doc+Drawer';
                            elementName = "m_docDrawerTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_CREDITAUDITOR) %>:
                            name = '<%=AspxTools.ClientId(m_creditAuditorLink)%>';
                            roledesc = 'Credit+Auditor';
                            elementName = "m_creditAuditorTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_DISCLOSUREDESK) %>:
                            name = '<%=AspxTools.ClientId(m_disclosureDeskLink)%>';
                            roledesc = 'Disclosure+Desk';
                            elementName = "m_disclosureDeskTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_JUNIORPROCESSOR) %>:
                            name = '<%=AspxTools.ClientId(m_juniorProcessorLink)%>';
                            roledesc = 'Junior+Processor';
                            elementName = "m_juniorProcessorTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_JUNIORUNDERWRITER) %>:
                            name = '<%=AspxTools.ClientId(m_juniorUnderwriterLink)%>';
                            roledesc = 'Junior+Underwriter';
                            elementName = "m_juniorUnderwriterTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_LEGALAUDITOR) %>:
                            name = '<%=AspxTools.ClientId(m_legalAuditorLink)%>';
                            roledesc = 'Legal+Auditor';
                            elementName = "m_legalAuditorTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_LOANOFFICERASSISTANT) %>:
                            name = '<%=AspxTools.ClientId(m_loanOfficerAssistantLink)%>';
                            roledesc = 'Loan+Officer+Assistant';
                            elementName = "m_loanOfficerAssistantTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_PURCHASER) %>:
                            name = '<%=AspxTools.ClientId(m_purchaserLink)%>';
                            roledesc = 'Purchaser';
                            elementName = "m_purchaserTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_QCCOMPLIANCE) %>:
                            name = '<%=AspxTools.ClientId(m_qcComplianceLink)%>';
                            roledesc = 'QC+Compliance';
                             elementName = "m_qcComplianceTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_SECONDARY) %>:
                            name = '<%=AspxTools.ClientId(m_secondaryLink)%>';
                            roledesc = 'Secondary';
                             elementName = "m_secondaryTeam";
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_SERVICING) %>:
                            name = '<%=AspxTools.ClientId(m_servicingLink)%>';
                            roledesc = 'Servicing';
                             elementName = "m_servicingTeam";
                        break;

                        default:
                            return;

                    }

                    var urlTeamPicker = '/los/TeamPicker.aspx?role=' + role + '&roledesc=' + roledesc + '&loanid=' + <%=AspxTools.JsString(LoanID)%> + '&assignType=loan&tab=1';
                    showModal(urlTeamPicker, null, "dialogWidth:400, dialogHeight:400", null, function(arg){
                        if (arg == null || !arg.OK) return;
                        document.getElementById(elementName).innerText = arg.label;
                    },{height:470,hideCloseButton:true});
                }



                function displayPeopleInRole(role, isOnlyDisplayLqbUsers, isOnlyDisplayPmlUsers) {
                    if(m_isReadOnly){
                        alert('Cannot perform assignment because this loan file is read-only.');
                        return;
                    }
                    var name;
                    var roledesc;
                    var show;

                    switch(role) {
                        case <%=AspxTools.JsString(ConstApp.ROLE_LOAN_OFFICER)%>:
                            name = '<%=AspxTools.ClientId(m_agentLink)%>';
                            roledesc = 'Loan+Officer';
                            show = 'full';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_BROKERPROCESSOR)%>:
                            name = '<%=AspxTools.ClientId(m_brokerprocessorLink)%>';
                            roledesc = 'Processor+(External)';
                            show = 'full';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_PROCESSOR)%>:
                            name = '<%=AspxTools.ClientId(m_processorLink)%>';
                            roledesc = 'Processor';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_MANAGER)%>:
                            name = '<%=AspxTools.ClientId(m_managerLink)%>';
                            roledesc = 'Manager';
                            show = 'simple';
                        break;
                        case <%=AspxTools.JsString(ConstApp.ROLE_REAL_ESTATE_AGENT)%> :
                            name = '<%=AspxTools.ClientId(m_realEstateLink)%>';
                            roledesc = 'Real+Estate+Agent';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_CALL_CENTER_AGENT)%> :
                            name = '<%=AspxTools.ClientId(m_telemarketerLink)%>';
                            roledesc = 'Call+Center+Agent';
                            show = 'simple';
                        break;


                        case <%=AspxTools.JsString(ConstApp.ROLE_LENDER_ACCOUNT_EXEC)%> :
                            name = '<%=AspxTools.ClientId(m_lenderAcctExecLink)%>';
                            roledesc = 'Lender+Account+Executive';
                            show = 'simple';
                            break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_LOCK_DESK)%> :
                            name = '<%=AspxTools.ClientId(m_lockDeskLink)%>';
                            roledesc = 'Lock+Desk+Agent';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_UNDERWRITER)%> :
                            name = '<%=AspxTools.ClientId(m_underwriterLink)%>';
                            roledesc = 'Underwriter';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_LOAN_OPENER)%> :
                            name = '<%=AspxTools.ClientId(m_loanOpenerLink)%>';
                            roledesc = 'Loan+Opener';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_CLOSER) %>:
                            name = '<%=AspxTools.ClientId(m_closerLink)%>';
                            roledesc = 'Closer';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_SHIPPER) %>:
                            name = '<%=AspxTools.ClientId(m_shipperLink)%>';
                            roledesc = 'Shipper';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_FUNDER) %>:
                            name = '<%=AspxTools.ClientId(m_funderLink)%>';
                            roledesc = 'Funder';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_POSTCLOSER) %>:
                            name = '<%=AspxTools.ClientId(m_postCloserLink)%>';
                            roledesc = 'Post+Closer';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_INSURING) %>:
                            name = '<%=AspxTools.ClientId(m_insuringLink)%>';
                            roledesc = 'Insuring';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_COLLATERALAGENT) %>:
                            name = '<%=AspxTools.ClientId(m_collateralAgentLink)%>';
                            roledesc = 'Collateral+Agent';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_DOCDRAWER) %>:
                            name = '<%=AspxTools.ClientId(m_docDrawerLink)%>';
                            roledesc = 'Doc+Drawer';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_CREDITAUDITOR) %>:
                            name = '<%=AspxTools.ClientId(m_creditAuditorLink)%>';
                            roledesc = 'Credit+Auditor';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_DISCLOSUREDESK) %>:
                            name = '<%=AspxTools.ClientId(m_disclosureDeskLink)%>';
                            roledesc = 'Disclosure+Desk';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_JUNIORPROCESSOR) %>:
                            name = '<%=AspxTools.ClientId(m_juniorProcessorLink)%>';
                            roledesc = 'Junior+Processor';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_JUNIORUNDERWRITER) %>:
                            name = '<%=AspxTools.ClientId(m_juniorUnderwriterLink)%>';
                            roledesc = 'Junior+Underwriter';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_LEGALAUDITOR) %>:
                            name = '<%=AspxTools.ClientId(m_legalAuditorLink)%>';
                            roledesc = 'Legal+Auditor';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_LOANOFFICERASSISTANT) %>:
                            name = '<%=AspxTools.ClientId(m_loanOfficerAssistantLink)%>';
                            roledesc = 'Loan+Officer+Assistant';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_PURCHASER) %>:
                            name = '<%=AspxTools.ClientId(m_purchaserLink)%>';
                            roledesc = 'Purchaser';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_QCCOMPLIANCE) %>:
                            name = '<%=AspxTools.ClientId(m_qcComplianceLink)%>';
                            roledesc = 'QC+Compliance';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_SECONDARY) %>:
                            name = '<%=AspxTools.ClientId(m_secondaryLink)%>';
                            roledesc = 'Secondary';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_SERVICING) %>:
                            name = '<%=AspxTools.ClientId(m_servicingLink)%>';
                            roledesc = 'Servicing';
                            show = 'simple';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_EXTERNAL_SECONDARY)%>:
                            name = '<%=AspxTools.ClientId(m_externalSecondaryLink)%>';
                            roledesc = 'Secondary+(External)';
                            show = 'full';
                        break;

                        case <%=AspxTools.JsString(ConstApp.ROLE_EXTERNAL_POST_CLOSER)%>:
                            name = '<%=AspxTools.ClientId(m_externalPostCloserLink)%>';
                            roledesc = 'Post-Closer+(External)';
                            show = 'full';
                        break;

                        default:
                            return;

                    }

                    var url = '/los/EmployeeRole.aspx?role=' + role + '&roledesc=' + roledesc + '&loanid=' + <%=AspxTools.JsString(LoanID)%> + '&show=' + show;
                    if (isOnlyDisplayLqbUsers) {
                        url = url + '&isLqbOnly=t';
                    }
                    if (isOnlyDisplayPmlUsers) {
                        url = url + '&isPmlOnly=t'
                    }

                    showModal(url, null, null, null, function(arg){
                        if (arg.OK) {
                            if (role == 'Agent') {
                                parent.info.f_updateLoanOfficer(arg.EmployeeName);
                            }

                            parent.body.f_load(VRoot + '/newlos/Status/Agents.aspx?loanid='  + <%=AspxTools.JsString(LoanID)%>);
                        }
                    },{hideCloseButton:true});
                }

                return {
                    init : init,
                    confirmDelete : confirmDelete,
                    displayPeopleInRole : displayPeopleInRole,
                    displayTeamsInRole : displayTeamsInRole
                }

                })();

                function _init() {
                    AgentsPage.init();
                    var tabIndex = <%=AspxTools.JsNumeric(tabIndex) %>;
                    switchAssignmentTab(tabIndex);

                    <% if (IsPickerDialog) { %>
                    $("tr").not(".picker").hide();
                    $(".editCol").hide();

                    <% if ( !Page.IsPostBack ) { %>
                    resize( 1000 , 400 );
                    <% } %>
                    <% } else {%>
                    $(".clearAgent").hide();
                    <% } %>
                }

                function switchAssignmentTab(index)
                {
                    if(index == 0)
                    {
                        document.getElementById("tab0").className = "selected";
                        document.getElementById("AgentAssignment").style.display = "";

                        document.getElementById("tab1").className = "";
                        document.getElementById("TeamAssignment").style.display = "none";
                    }
                    else
                    {
                        document.getElementById("tab0").className = "";
                        document.getElementById("AgentAssignment").style.display = "none";

                        document.getElementById("tab1").className = "selected";
                        document.getElementById("TeamAssignment").style.display = "";
                    }
                }



    function displayPeopleRecord(id) {
      location.href="AgentRecord.aspx?loanid=" +<%=AspxTools.JsString(LoanID)%> + "&appid="+<%=AspxTools.JsString(ApplicationID)%>+"&recordid=" + encodeURIComponent(id) + "&orderby=" + <%=AspxTools.JsString(m_sortExpression)%>;

    }


    $(document).on("click", ".select", function() {
        var $this = $(this);
        var $tr = $($this.parents("tr")[0]);

        // Set output
        var args = window.dialogArguments || {};
        args.id = $this.data("id");
        args.compName = $.trim($tr.find(".compName").text());
        args.OK = true;

        // close output
        onClosePopup(args);
    });

    $(document).on("click", ".clearAgent", function() {
        // Set output
        var args = window.dialogArguments || {};
        args.id = "CLEAR";
        args.compName = "";
        args.OK = true;

        // close output
        onClosePopup(args);
    });

    -->
        </script>
    </form>
</body>
</html>
