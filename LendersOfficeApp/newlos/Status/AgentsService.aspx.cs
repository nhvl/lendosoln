﻿namespace LendersOfficeApp.newlos.Status
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using DataAccess;
    using LendersOffice.Audit;

    public partial class AgentsService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch(methodName) 
            {
                case "DeleteOfficialContact":
                    DeleteOfficialContact();
                    break;
            };
        }
        private void DeleteOfficialContact()
        {
            Guid sLId = GetGuid("LoanID");
            int sFileVersion = GetInt("sFileVersion");
            int idCount = GetInt("Count");

            HashSet<Guid> ids = new HashSet<Guid>();

            for (int i = 0; i < idCount; i++)
            {
                ids.Add(Guid.Parse(GetString("id" + i)));
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(AgentsService));
            dataLoan.InitSave(sFileVersion);

            HashSet<Guid> idsNotDeleted = new HashSet<Guid>();
            dataLoan.DeleteAgents(ids, out idsNotDeleted);
            dataLoan.Save();
        }
    }
}
