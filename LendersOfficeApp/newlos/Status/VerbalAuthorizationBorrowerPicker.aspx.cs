﻿#region Generated Code
namespace LendersOfficeApp.newlos.Status
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Provides a means for selecting borrowers for verbal credit report authorization.
    /// </summary>
    public partial class VerbalAuthorizationBorrowerPicker : BaseLoanPage
    {
        /// <summary>
        /// Gets a value indicating whether the picker was opened for the
        /// co-borrower credit report authorization.
        /// </summary>
        private bool IsCoborr => RequestHelper.GetBool("isCoborr");

        /// <summary>
        /// Gets the ID of the application for the verbal authorization.
        /// </summary>
        private Guid AppId => RequestHelper.GetGuid("appid");

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected override void OnInit(EventArgs e)
        {
            this.AuthorizationDate.Text = DateTime.Today.ToShortDateString();

            this.DisplayCopyRight = false;
            this.UseNewFramework = true;

            base.OnInit(e);
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var loan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(VerbalAuthorizationBorrowerPicker));
            loan.InitLoad();

            var app = loan.GetAppData(this.AppId);

            if (!app.aBIsValidNameSsn && !app.aBHasSpouse)
            {
                this.NoBorrowerDataEntered.Visible = true;
                this.VerbalAuthorizationSection.Visible = false;
                this.ButtonRow.Visible = false;
                return;
            }

            this.NoBorrowerDataEntered.Visible = false;

            this.BorrowerName.InnerText = app.aBNm;
            this.BorrowerAuthorized.Checked = !this.IsCoborr;

            if (app.aBHasSpouse)
            {
                this.CoborrowerName.InnerText = app.aCNm;
                this.CoborrowerAuthorized.Checked = this.IsCoborr;
            }
            else
            {
                this.CoborrowerAuthorizationRow.Attributes["class"] += " hidden";
            }
        }
    }
}