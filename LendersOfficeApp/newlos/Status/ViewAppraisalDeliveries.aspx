﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewAppraisalDeliveries.aspx.cs" Inherits="LendersOfficeApp.newlos.Status.ViewAppraisalDeliveries" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Appraisal Deliveries</title>
    <style type="text/css">
        body  {
            background-color: gainsboro; 
        }
        .SentDateColumn {
            width: 120px;
        }
        .DocumentColumn {
            width: 14%;
        }
        .EDocDiv {
            padding-bottom: 5px;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        jQuery(function ($) {
            $('img[data-docmetadata]').docmetadatatooltip();

            $('#OkBtn').click(function () {
                parent.LQBPopup.Return();
            });

            $('.FileLink').click(function () {
                var docId = $(this).closest('.EDocDiv').find('.EDocId').val();
                window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?docid=" + encodeURIComponent(docId), '_self');
            });

            $('.XmlLink').click(function () {
                var docId = $(this).closest('.EDocDiv').find('.EDocId').val();
                window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?cmd=xml&docid=" + encodeURIComponent(docId), 'xmldoc');
            });
        });
    </script>
    <h4 class="page-header">Appraisal Deliveries</h4>
    <form id="form1" runat="server">
        <div>
            <div class="Padding5">
                <table class="Table">
                    <thead>
                        <tr class="LoanFormHeader">
                            <td class="SentDateColumn">Sent Date</td>
                            <td>Sent By</td>
                            <td>Websheet Number</td>
                            <td class="DocumentColumn">Document</td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="DeliveriesRepeater" runat="server" OnItemDataBound="DeliveriesRepeater_ItemDataBound">
                            <ItemTemplate>
                                <tr class="ReverseGridAutoItem DeliveryRow">
                                    <td>
                                        <span runat="server" id="SentDate"></span>
                                    </td>
                                    <td>
                                        <span runat="server" id="SentBy"></span>
                                    </td>
                                    <td>
                                        <span runat="server" id="WebsheetNumber"></span>
                                    </td>
                                    <td>
                                        <asp:Repeater ID="DeliveryDocumentsRepeater" runat="server">
                                            <ItemTemplate>
                                        <div id="EDocDiv" class="EDocDiv" runat="server">
                                            <input type="hidden" id="EDocId" class="EDocId" runat="server" />
                                            <img runat="server" id="FileInfo" visible="false" class="FloatRight" />
                                            <div>
                                                <a runat="server" id="FileLink" class="FileLink">view pdf</a>
                                            </div>
                                            <div>
                                                <a runat="server" id="XmlLink" class="XmlLink">view xml</a>
                                            </div>
                                        </div>
                                        <div id="UnavailableDiv" runat="server">
                                            Document unavailable
                                        </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="align-center">
            <input type="button" id="OkBtn" value="OK" />
        </div>
    </form>
</body>
</html>
