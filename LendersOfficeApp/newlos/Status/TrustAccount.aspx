<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="TrustAccount.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Status.TrustAccount" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>TrustAccount</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
		
		<script language="javascript">
        <!--
		    function _init() {
		        if (document.getElementById("_IsTemplate").value == "True") {
		            disableFields(true);
		        }
		    }
		    //-->
		    </script>
  </HEAD>
	<body MS_POSITIONING="FlowLayout" class="RightBackground" onload="_init();">
		<script language="javascript">
<!--		    
function updateTotal() {
  var payableTotal = 0;
  var receivableTotal = 0;
  for (var i = 1; i < 17; i++) {
    payableTotal += parseMoneyFloat(document.getElementById("sTrust" + i + "PayableAmt").value);
    receivableTotal += parseMoneyFloat(document.getElementById("sTrust" + i + "ReceivableAmt").value);
  }
    
  document.getElementById("sTrustPayableTotal").value = payableTotal;
  document.getElementById("sTrustReceivableTotal").value = receivableTotal;
  document.getElementById("sTrustBalance").value = receivableTotal - payableTotal;
  
  format_money(document.getElementById("sTrustPayableTotal"));
  format_money(document.getElementById("sTrustReceivableTotal"));
  format_money(document.getElementById("sTrustBalance"));
  
  
}
function lockDown() {
    disableFields(false);
}

function disableFields(isTemplate) {
    for (var i = 1; i < 17; i++) {
        if (!isTemplate) {
            document.getElementById("sTrust" + i + "Desc").readOnly = true;
            document.getElementById("sTrust" + i + "ReceivableN").readOnly = true;
        }
        document.getElementById("sTrust" + i + "D").readOnly = true;
        document.getElementById("sTrust" + i + "PayableChkNum").readOnly = true;
        document.getElementById("sTrust" + i + "PayableAmt").readOnly = true;
        document.getElementById("sTrust" + i + "ReceivableChkNum").readOnly = true;
        document.getElementById("sTrust" + i + "ReceivableAmt").readOnly = true;
    }
    if (!isTemplate) {
        document.getElementById("MsgLabel").style.display = "";
    }
}
//-->
		</script>
		<form id="TrustAccount" method="post" runat="server">
			<table id="Table1" cellSpacing="0" cellPadding="0" border="0">
				<tr>
					<td class="MainRightHeader" colSpan="7">Trust Account</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
				  <td class="FieldLabel" colspan="7" align="center"><div id="MsgLabel" style="DISPLAY:none;COLOR:red">Only Accountant users can modify trust account info.<br><br></div></td>
				</tr>
				<tr>
					<td class="FieldLabel"></td>
					<td class="FieldLabel" vAlign="top">Item Description</td>
					<td align="center" class="FieldLabel" vAlign="top">Date</td>
					<td align="center" colSpan="2" class="FieldLabel" vAlign="top">Account Payable</td>
					<td align="center" colSpan="2" class="FieldLabel" vAlign="top">Account Receivable</td>
				</tr>
				<tr>
					<td colspan="3"></td>
					<td class="FieldLabel">
						Chk#/Credit Card #</td>
					<td class="FieldLabel">Amount</td>
					<td class="FieldLabel">Chk# / Credit Card #</td>
					<td class="FieldLabel">Amount</td>
				</tr>
				<tr>
					<td align="center" class="FieldLabel">1</td>
					<td><asp:textbox id="sTrust1Desc" runat="server" Width="167px" MaxLength="36"></asp:textbox></td>
					<td noWrap><ml:DateTextBox id="sTrust1D" runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
					<td><asp:textbox id="sTrust1PayableChkNum" runat="server" Width="114" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust1PayableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
					<td><asp:textbox id="sTrust1ReceivableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust1ReceivableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td></td>
					<td class="FieldLabel">Notes:</td>
					<td colSpan="6"><asp:textbox id="sTrust1ReceivableN" runat="server" Width="509" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td align="center" class="FieldLabel">2</td>
					<td><asp:textbox id="sTrust2Desc" runat="server" Width="167px" MaxLength="36"></asp:textbox></td>
					<td><ml:DateTextBox id="sTrust2D" runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
					<td><asp:textbox id="sTrust2PayableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust2PayableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
					<td><asp:textbox id="sTrust2ReceivableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust2ReceivableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td></td>
					<td class="FieldLabel">Notes:</td>
					<td colSpan="6"><asp:textbox id="sTrust2ReceivableN" runat="server" Width="509px" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td align="center" class="FieldLabel">3</td>
					<td><asp:textbox id="sTrust3Desc" runat="server" Width="167px" MaxLength="36"></asp:textbox></td>
					<td><ml:DateTextBox id="sTrust3D" runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
					<td><asp:textbox id="sTrust3PayableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust3PayableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
					<td><asp:textbox id="sTrust3ReceivableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust3ReceivableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td></td>
					<td class="FieldLabel">Notes:</td>
					<td colSpan="6"><asp:textbox id="sTrust3ReceivableN" runat="server" Width="509px" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td align="center" class="FieldLabel">4</td>
					<td><asp:textbox id="sTrust4Desc" runat="server" Width="167px" MaxLength="36"></asp:textbox></td>
					<td><ml:DateTextBox id="sTrust4D" runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
					<td><asp:textbox id="sTrust4PayableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust4PayableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
					<td><asp:textbox id="sTrust4ReceivableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust4ReceivableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td></td>
					<td class="FieldLabel">Notes:</td>
					<td colSpan="6"><asp:textbox id="sTrust4ReceivableN" runat="server" Width="509px" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td align="center" class="FieldLabel">5</td>
					<td><asp:textbox id="sTrust5Desc" runat="server" Width="167px" MaxLength="36"></asp:textbox></td>
					<td><ml:DateTextBox id="sTrust5D" runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
					<td><asp:textbox id="sTrust5PayableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust5PayableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
					<td><asp:textbox id="sTrust5ReceivableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust5ReceivableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td></td>
					<td class="FieldLabel">Notes:</td>
					<td colSpan="6"><asp:textbox id="sTrust5ReceivableN" runat="server" Width="509px" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td align="center" class="FieldLabel">6</td>
					<td><asp:textbox id="sTrust6Desc" runat="server" Width="167px" MaxLength="36"></asp:textbox></td>
					<td><ml:DateTextBox id="sTrust6D" runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
					<td><asp:textbox id="sTrust6PayableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust6PayableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
					<td><asp:textbox id="sTrust6ReceivableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust6ReceivableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td></td>
					<td class="FieldLabel">Notes:</td>
					<td colSpan="6"><asp:textbox id="sTrust6ReceivableN" runat="server" Width="509px" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td align="center" class="FieldLabel">7</td>
					<td><asp:textbox id="sTrust7Desc" runat="server" Width="167px"></asp:textbox></td>
					<td><ml:DateTextBox id="sTrust7D" runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
					<td><asp:textbox id="sTrust7PayableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust7PayableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
					<td><asp:textbox id="sTrust7ReceivableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust7ReceivableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td></td>
					<td class="FieldLabel">Notes:</td>
					<td colSpan="6"><asp:textbox id="sTrust7ReceivableN" runat="server" Width="509px" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td align="center" class="FieldLabel">8</td>
					<td><asp:textbox id="sTrust8Desc" runat="server" Width="167px" MaxLength="36"></asp:textbox></td>
					<td><ml:DateTextBox id="sTrust8D" runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
					<td><asp:textbox id="sTrust8PayableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust8PayableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
					<td><asp:textbox id="sTrust8ReceivableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust8ReceivableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td></td>
					<td class="FieldLabel">Notes:</td>
					<td colSpan="6"><asp:textbox id="sTrust8ReceivableN" runat="server" Width="509px" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td align="center" class="FieldLabel">9</td>
					<td><asp:textbox id="sTrust9Desc" runat="server" Width="167px" MaxLength="36"></asp:textbox></td>
					<td><ml:DateTextBox id="sTrust9D" runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
					<td><asp:textbox id="sTrust9PayableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust9PayableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
					<td><asp:textbox id="sTrust9ReceivableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust9ReceivableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td></td>
					<td class="FieldLabel">Notes:</td>
					<td colSpan="6"><asp:textbox id="sTrust9ReceivableN" runat="server" Width="509px" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td align="center" class="FieldLabel">10</td>
					<td><asp:textbox id="sTrust10Desc" runat="server" Width="167px" MaxLength="36"></asp:textbox></td>
					<td><ml:DateTextBox id="sTrust10D" runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
					<td><asp:textbox id="sTrust10PayableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust10PayableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
					<td><asp:textbox id="sTrust10ReceivableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust10ReceivableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td></td>
					<td class="FieldLabel">Notes:</td>
					<td colSpan="6"><asp:textbox id="sTrust10ReceivableN" runat="server" Width="509px" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td align="center" class="FieldLabel">11</td>
					<td><asp:textbox id="sTrust11Desc" runat="server" Width="167px" MaxLength="36"></asp:textbox></td>
					<td><ml:DateTextBox id="sTrust11D" runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
					<td><asp:textbox id="sTrust11PayableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust11PayableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
					<td><asp:textbox id="sTrust11ReceivableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust11ReceivableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td></td>
					<td class="FieldLabel">Notes:</td>
					<td colSpan="6"><asp:textbox id="sTrust11ReceivableN" runat="server" Width="509px" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td align="center" class="FieldLabel">12</td>
					<td><asp:textbox id="sTrust12Desc" runat="server" Width="167px" MaxLength="36"></asp:textbox></td>
					<td><ml:DateTextBox id="sTrust12D" runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
					<td><asp:textbox id="sTrust12PayableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust12PayableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
					<td><asp:textbox id="sTrust12ReceivableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust12ReceivableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td></td>
					<td class="FieldLabel">Notes:</td>
					<td colSpan="6"><asp:textbox id="sTrust12ReceivableN" runat="server" Width="509px" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td align="center" class="FieldLabel">13</td>
					<td><asp:textbox id="sTrust13Desc" runat="server" Width="167px" MaxLength="36"></asp:textbox></td>
					<td><ml:DateTextBox id="sTrust13D" runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
					<td><asp:textbox id="sTrust13PayableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust13PayableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
					<td><asp:textbox id="sTrust13ReceivableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust13ReceivableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td></td>
					<td class="FieldLabel">Notes:</td>
					<td colSpan="6"><asp:textbox id="sTrust13ReceivableN" runat="server" Width="509px" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td align="center" class="FieldLabel">14</td>
					<td><asp:textbox id="sTrust14Desc" runat="server" Width="167px" MaxLength="36"></asp:textbox></td>
					<td><ml:DateTextBox id="sTrust14D" runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
					<td><asp:textbox id="sTrust14PayableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust14PayableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
					<td><asp:textbox id="sTrust14ReceivableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust14ReceivableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td></td>
					<td class="FieldLabel">Notes:</td>
					<td colSpan="6"><asp:textbox id="sTrust14ReceivableN" runat="server" Width="509px" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td align="center" class="FieldLabel">15</td>
					<td><asp:textbox id="sTrust15Desc" runat="server" Width="167px" MaxLength="36"></asp:textbox></td>
					<td><ml:DateTextBox id="sTrust15D" runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
					<td><asp:textbox id="sTrust15PayableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust15PayableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
					<td><asp:textbox id="sTrust15ReceivableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust15ReceivableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td></td>
					<td class="FieldLabel">Notes:</td>
					<td colSpan="6"><asp:textbox id="sTrust15ReceivableN" runat="server" Width="509px" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td align="center" class="FieldLabel">16</td>
					<td><asp:textbox id="sTrust16Desc" runat="server" Width="167px" MaxLength="36"></asp:textbox></td>
					<td><ml:DateTextBox id="sTrust16D" runat="server" width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
					<td><asp:textbox id="sTrust16PayableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust16PayableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
					<td><asp:textbox id="sTrust16ReceivableChkNum" runat="server" Width="114px" MaxLength="36"></asp:textbox></td>
					<td><ml:MoneyTextBox id="sTrust16ReceivableAmt" runat="server" width="90" preset="money" CssClass="mask" onblur="updateTotal();"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td></td>
					<td class="FieldLabel">Notes:</td>
					<td colSpan="6"><asp:textbox id="sTrust16ReceivableN" runat="server" Width="509px" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td colspan="7"></td>
				</tr>
				<tr>
					<td colspan="3"></td>
					<td class="FieldLabel">Total</td>
					<td><ml:MoneyTextBox id="sTrustPayableTotal" runat="server" preset="money" width="90" ReadOnly="True"></ml:MoneyTextBox></td>
					<td></td>
					<td><ml:MoneyTextBox id="sTrustReceivableTotal" runat="server" preset="money" width="90" ReadOnly="True"></ml:MoneyTextBox></td>
				</tr>
				<tr>
					<td colspan="3"></td>
					<td class="FieldLabel">Balance</td>
					<td></td>
					<td></td>
					<td><ml:MoneyTextBox id="sTrustBalance" runat="server" preset="money" width="90" ReadOnly="True"></ml:MoneyTextBox></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
