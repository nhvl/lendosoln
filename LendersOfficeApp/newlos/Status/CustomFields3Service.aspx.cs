///
/// Author: Matthew Flynn
/// 
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos.Status
{
    public class CustomFields3ServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(CustomFields3ServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sCustomField41Desc = GetString("sCustomField41Desc");
            dataLoan.sCustomField42Desc = GetString("sCustomField42Desc");
            dataLoan.sCustomField43Desc = GetString("sCustomField43Desc");
            dataLoan.sCustomField44Desc = GetString("sCustomField44Desc");
            dataLoan.sCustomField45Desc = GetString("sCustomField45Desc");
            dataLoan.sCustomField46Desc = GetString("sCustomField46Desc");
            dataLoan.sCustomField47Desc = GetString("sCustomField47Desc");
            dataLoan.sCustomField48Desc = GetString("sCustomField48Desc");
            dataLoan.sCustomField49Desc = GetString("sCustomField49Desc");
            dataLoan.sCustomField50Desc = GetString("sCustomField50Desc");
            dataLoan.sCustomField51Desc = GetString("sCustomField51Desc");
            dataLoan.sCustomField52Desc = GetString("sCustomField52Desc");
            dataLoan.sCustomField53Desc = GetString("sCustomField53Desc");
            dataLoan.sCustomField54Desc = GetString("sCustomField54Desc");
            dataLoan.sCustomField55Desc = GetString("sCustomField55Desc");
            dataLoan.sCustomField56Desc = GetString("sCustomField56Desc");
            dataLoan.sCustomField57Desc = GetString("sCustomField57Desc");
            dataLoan.sCustomField58Desc = GetString("sCustomField58Desc");
            dataLoan.sCustomField59Desc = GetString("sCustomField59Desc");
            dataLoan.sCustomField60Desc = GetString("sCustomField60Desc");
            dataLoan.sCustomField41D_rep = GetString("sCustomField41D");
            dataLoan.sCustomField42D_rep = GetString("sCustomField42D");
            dataLoan.sCustomField43D_rep = GetString("sCustomField43D");
            dataLoan.sCustomField44D_rep = GetString("sCustomField44D");
            dataLoan.sCustomField45D_rep = GetString("sCustomField45D");
            dataLoan.sCustomField46D_rep = GetString("sCustomField46D");
            dataLoan.sCustomField47D_rep = GetString("sCustomField47D");
            dataLoan.sCustomField48D_rep = GetString("sCustomField48D");
            dataLoan.sCustomField49D_rep = GetString("sCustomField49D");
            dataLoan.sCustomField50D_rep = GetString("sCustomField50D");
            dataLoan.sCustomField51D_rep = GetString("sCustomField51D");
            dataLoan.sCustomField52D_rep = GetString("sCustomField52D");
            dataLoan.sCustomField53D_rep = GetString("sCustomField53D");
            dataLoan.sCustomField54D_rep = GetString("sCustomField54D");
            dataLoan.sCustomField55D_rep = GetString("sCustomField55D");
            dataLoan.sCustomField56D_rep = GetString("sCustomField56D");
            dataLoan.sCustomField57D_rep = GetString("sCustomField57D");
            dataLoan.sCustomField58D_rep = GetString("sCustomField58D");
            dataLoan.sCustomField59D_rep = GetString("sCustomField59D");
            dataLoan.sCustomField60D_rep = GetString("sCustomField60D");
            dataLoan.sCustomField41Money_rep = GetString("sCustomField41Money");
            dataLoan.sCustomField42Money_rep = GetString("sCustomField42Money");
            dataLoan.sCustomField43Money_rep = GetString("sCustomField43Money");
            dataLoan.sCustomField44Money_rep = GetString("sCustomField44Money");
            dataLoan.sCustomField45Money_rep = GetString("sCustomField45Money");
            dataLoan.sCustomField46Money_rep = GetString("sCustomField46Money");
            dataLoan.sCustomField47Money_rep = GetString("sCustomField47Money");
            dataLoan.sCustomField48Money_rep = GetString("sCustomField48Money");
            dataLoan.sCustomField49Money_rep = GetString("sCustomField49Money");
            dataLoan.sCustomField50Money_rep = GetString("sCustomField50Money");
            dataLoan.sCustomField51Money_rep = GetString("sCustomField51Money");
            dataLoan.sCustomField52Money_rep = GetString("sCustomField52Money");
            dataLoan.sCustomField53Money_rep = GetString("sCustomField53Money");
            dataLoan.sCustomField54Money_rep = GetString("sCustomField54Money");
            dataLoan.sCustomField55Money_rep = GetString("sCustomField55Money");
            dataLoan.sCustomField56Money_rep = GetString("sCustomField56Money");
            dataLoan.sCustomField57Money_rep = GetString("sCustomField57Money");
            dataLoan.sCustomField58Money_rep = GetString("sCustomField58Money");
            dataLoan.sCustomField59Money_rep = GetString("sCustomField59Money");
            dataLoan.sCustomField60Money_rep = GetString("sCustomField60Money");
            dataLoan.sCustomField41Pc_rep = GetString("sCustomField41Pc");
            dataLoan.sCustomField42Pc_rep = GetString("sCustomField42Pc");
            dataLoan.sCustomField43Pc_rep = GetString("sCustomField43Pc");
            dataLoan.sCustomField44Pc_rep = GetString("sCustomField44Pc");
            dataLoan.sCustomField45Pc_rep = GetString("sCustomField45Pc");
            dataLoan.sCustomField46Pc_rep = GetString("sCustomField46Pc");
            dataLoan.sCustomField47Pc_rep = GetString("sCustomField47Pc");
            dataLoan.sCustomField48Pc_rep = GetString("sCustomField48Pc");
            dataLoan.sCustomField49Pc_rep = GetString("sCustomField49Pc");
            dataLoan.sCustomField50Pc_rep = GetString("sCustomField50Pc");
            dataLoan.sCustomField51Pc_rep = GetString("sCustomField51Pc");
            dataLoan.sCustomField52Pc_rep = GetString("sCustomField52Pc");
            dataLoan.sCustomField53Pc_rep = GetString("sCustomField53Pc");
            dataLoan.sCustomField54Pc_rep = GetString("sCustomField54Pc");
            dataLoan.sCustomField55Pc_rep = GetString("sCustomField55Pc");
            dataLoan.sCustomField56Pc_rep = GetString("sCustomField56Pc");
            dataLoan.sCustomField57Pc_rep = GetString("sCustomField57Pc");
            dataLoan.sCustomField58Pc_rep = GetString("sCustomField58Pc");
            dataLoan.sCustomField59Pc_rep = GetString("sCustomField59Pc");
            dataLoan.sCustomField60Pc_rep = GetString("sCustomField60Pc");
            dataLoan.sCustomField41Bit = GetBool("sCustomField41Bit");
            dataLoan.sCustomField42Bit = GetBool("sCustomField42Bit");
            dataLoan.sCustomField43Bit = GetBool("sCustomField43Bit");
            dataLoan.sCustomField44Bit = GetBool("sCustomField44Bit");
            dataLoan.sCustomField45Bit = GetBool("sCustomField45Bit");
            dataLoan.sCustomField46Bit = GetBool("sCustomField46Bit");
            dataLoan.sCustomField47Bit = GetBool("sCustomField47Bit");
            dataLoan.sCustomField48Bit = GetBool("sCustomField48Bit");
            dataLoan.sCustomField49Bit = GetBool("sCustomField49Bit");
            dataLoan.sCustomField50Bit = GetBool("sCustomField50Bit");
            dataLoan.sCustomField51Bit = GetBool("sCustomField51Bit");
            dataLoan.sCustomField52Bit = GetBool("sCustomField52Bit");
            dataLoan.sCustomField53Bit = GetBool("sCustomField53Bit");
            dataLoan.sCustomField54Bit = GetBool("sCustomField54Bit");
            dataLoan.sCustomField55Bit = GetBool("sCustomField55Bit");
            dataLoan.sCustomField56Bit = GetBool("sCustomField56Bit");
            dataLoan.sCustomField57Bit = GetBool("sCustomField57Bit");
            dataLoan.sCustomField58Bit = GetBool("sCustomField58Bit");
            dataLoan.sCustomField59Bit = GetBool("sCustomField59Bit");
            dataLoan.sCustomField60Bit = GetBool("sCustomField60Bit");
            dataLoan.sCustomField41Notes = GetString("sCustomField41Notes");
            dataLoan.sCustomField42Notes = GetString("sCustomField42Notes");
            dataLoan.sCustomField43Notes = GetString("sCustomField43Notes");
            dataLoan.sCustomField44Notes = GetString("sCustomField44Notes");
            dataLoan.sCustomField45Notes = GetString("sCustomField45Notes");
            dataLoan.sCustomField46Notes = GetString("sCustomField46Notes");
            dataLoan.sCustomField47Notes = GetString("sCustomField47Notes");
            dataLoan.sCustomField48Notes = GetString("sCustomField48Notes");
            dataLoan.sCustomField49Notes = GetString("sCustomField49Notes");
            dataLoan.sCustomField50Notes = GetString("sCustomField50Notes");
            dataLoan.sCustomField51Notes = GetString("sCustomField51Notes");
            dataLoan.sCustomField52Notes = GetString("sCustomField52Notes");
            dataLoan.sCustomField53Notes = GetString("sCustomField53Notes");
            dataLoan.sCustomField54Notes = GetString("sCustomField54Notes");
            dataLoan.sCustomField55Notes = GetString("sCustomField55Notes");
            dataLoan.sCustomField56Notes = GetString("sCustomField56Notes");
            dataLoan.sCustomField57Notes = GetString("sCustomField57Notes");
            dataLoan.sCustomField58Notes = GetString("sCustomField58Notes");
            dataLoan.sCustomField59Notes = GetString("sCustomField59Notes");
            dataLoan.sCustomField60Notes = GetString("sCustomField60Notes");

        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

	public partial class CustomFields3Service : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize()
        {
            AddBackgroundItem("", new CustomFields3ServiceItem());
        }
        //protected override void Process(string methodName) 
        //{
        //    switch (methodName) 
        //    {
        //        case "SaveData":
        //            SaveData();
        //            break;
        //    }
        //}

        //private void SaveData() 
        //{
        //    Guid loanID = GetGuid("LoanID");
        //    int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
        //    CPageBase dataLoan = new CCustomFieldData(loanID);
        //    dataLoan.InitSave(sFileVersion);

        //    dataLoan.sCustomField1Desc = GetString("sCustomField1Desc");
        //    dataLoan.sCustomField2Desc = GetString("sCustomField2Desc");
        //    dataLoan.sCustomField3Desc = GetString("sCustomField3Desc");
        //    dataLoan.sCustomField4Desc = GetString("sCustomField4Desc");
        //    dataLoan.sCustomField5Desc = GetString("sCustomField5Desc");
        //    dataLoan.sCustomField6Desc = GetString("sCustomField6Desc");
        //    dataLoan.sCustomField7Desc = GetString("sCustomField7Desc");
        //    dataLoan.sCustomField8Desc = GetString("sCustomField8Desc");
        //    dataLoan.sCustomField9Desc = GetString("sCustomField9Desc");
        //    dataLoan.sCustomField10Desc = GetString("sCustomField10Desc");
        //    dataLoan.sCustomField11Desc = GetString("sCustomField11Desc");
        //    dataLoan.sCustomField12Desc = GetString("sCustomField12Desc");
        //    dataLoan.sCustomField13Desc = GetString("sCustomField13Desc");
        //    dataLoan.sCustomField14Desc = GetString("sCustomField14Desc");
        //    dataLoan.sCustomField15Desc = GetString("sCustomField15Desc");
        //    dataLoan.sCustomField16Desc = GetString("sCustomField16Desc");
        //    dataLoan.sCustomField17Desc = GetString("sCustomField17Desc");
        //    dataLoan.sCustomField18Desc = GetString("sCustomField18Desc");
        //    dataLoan.sCustomField19Desc = GetString("sCustomField19Desc");
        //    dataLoan.sCustomField20Desc = GetString("sCustomField20Desc");
        //    dataLoan.sCustomField1D_rep = GetString("sCustomField1D");
        //    dataLoan.sCustomField2D_rep = GetString("sCustomField2D");
        //    dataLoan.sCustomField3D_rep = GetString("sCustomField3D");
        //    dataLoan.sCustomField4D_rep = GetString("sCustomField4D");
        //    dataLoan.sCustomField5D_rep = GetString("sCustomField5D");
        //    dataLoan.sCustomField6D_rep = GetString("sCustomField6D");
        //    dataLoan.sCustomField7D_rep = GetString("sCustomField7D");
        //    dataLoan.sCustomField8D_rep = GetString("sCustomField8D");
        //    dataLoan.sCustomField9D_rep = GetString("sCustomField9D");
        //    dataLoan.sCustomField10D_rep = GetString("sCustomField10D");
        //    dataLoan.sCustomField11D_rep = GetString("sCustomField11D");
        //    dataLoan.sCustomField12D_rep = GetString("sCustomField12D");
        //    dataLoan.sCustomField13D_rep = GetString("sCustomField13D");
        //    dataLoan.sCustomField14D_rep = GetString("sCustomField14D");
        //    dataLoan.sCustomField15D_rep = GetString("sCustomField15D");
        //    dataLoan.sCustomField16D_rep = GetString("sCustomField16D");
        //    dataLoan.sCustomField17D_rep = GetString("sCustomField17D");
        //    dataLoan.sCustomField18D_rep = GetString("sCustomField18D");
        //    dataLoan.sCustomField19D_rep = GetString("sCustomField19D");
        //    dataLoan.sCustomField20D_rep = GetString("sCustomField20D");
        //    dataLoan.sCustomField1Money_rep = GetString("sCustomField1Money");
        //    dataLoan.sCustomField2Money_rep = GetString("sCustomField2Money");
        //    dataLoan.sCustomField3Money_rep = GetString("sCustomField3Money");
        //    dataLoan.sCustomField4Money_rep = GetString("sCustomField4Money");
        //    dataLoan.sCustomField5Money_rep = GetString("sCustomField5Money");
        //    dataLoan.sCustomField6Money_rep = GetString("sCustomField6Money");
        //    dataLoan.sCustomField7Money_rep = GetString("sCustomField7Money");
        //    dataLoan.sCustomField8Money_rep = GetString("sCustomField8Money");
        //    dataLoan.sCustomField9Money_rep = GetString("sCustomField9Money");
        //    dataLoan.sCustomField10Money_rep = GetString("sCustomField10Money");
        //    dataLoan.sCustomField11Money_rep = GetString("sCustomField11Money");
        //    dataLoan.sCustomField12Money_rep = GetString("sCustomField12Money");
        //    dataLoan.sCustomField13Money_rep = GetString("sCustomField13Money");
        //    dataLoan.sCustomField14Money_rep = GetString("sCustomField14Money");
        //    dataLoan.sCustomField15Money_rep = GetString("sCustomField15Money");
        //    dataLoan.sCustomField16Money_rep = GetString("sCustomField16Money");
        //    dataLoan.sCustomField17Money_rep = GetString("sCustomField17Money");
        //    dataLoan.sCustomField18Money_rep = GetString("sCustomField18Money");
        //    dataLoan.sCustomField19Money_rep = GetString("sCustomField19Money");
        //    dataLoan.sCustomField20Money_rep = GetString("sCustomField20Money");
        //    dataLoan.sCustomField1Pc_rep = GetString("sCustomField1Pc");
        //    dataLoan.sCustomField2Pc_rep = GetString("sCustomField2Pc");
        //    dataLoan.sCustomField3Pc_rep = GetString("sCustomField3Pc");
        //    dataLoan.sCustomField4Pc_rep = GetString("sCustomField4Pc");
        //    dataLoan.sCustomField5Pc_rep = GetString("sCustomField5Pc");
        //    dataLoan.sCustomField6Pc_rep = GetString("sCustomField6Pc");
        //    dataLoan.sCustomField7Pc_rep = GetString("sCustomField7Pc");
        //    dataLoan.sCustomField8Pc_rep = GetString("sCustomField8Pc");
        //    dataLoan.sCustomField9Pc_rep = GetString("sCustomField9Pc");
        //    dataLoan.sCustomField10Pc_rep = GetString("sCustomField10Pc");
        //    dataLoan.sCustomField11Pc_rep = GetString("sCustomField11Pc");
        //    dataLoan.sCustomField12Pc_rep = GetString("sCustomField12Pc");
        //    dataLoan.sCustomField13Pc_rep = GetString("sCustomField13Pc");
        //    dataLoan.sCustomField14Pc_rep = GetString("sCustomField14Pc");
        //    dataLoan.sCustomField15Pc_rep = GetString("sCustomField15Pc");
        //    dataLoan.sCustomField16Pc_rep = GetString("sCustomField16Pc");
        //    dataLoan.sCustomField17Pc_rep = GetString("sCustomField17Pc");
        //    dataLoan.sCustomField18Pc_rep = GetString("sCustomField18Pc");
        //    dataLoan.sCustomField19Pc_rep = GetString("sCustomField19Pc");
        //    dataLoan.sCustomField20Pc_rep = GetString("sCustomField20Pc");
        //    dataLoan.sCustomField1Bit = GetBool("sCustomField1Bit");
        //    dataLoan.sCustomField2Bit = GetBool("sCustomField2Bit");
        //    dataLoan.sCustomField3Bit = GetBool("sCustomField3Bit");
        //    dataLoan.sCustomField4Bit = GetBool("sCustomField4Bit");
        //    dataLoan.sCustomField5Bit = GetBool("sCustomField5Bit");
        //    dataLoan.sCustomField6Bit = GetBool("sCustomField6Bit");
        //    dataLoan.sCustomField7Bit = GetBool("sCustomField7Bit");
        //    dataLoan.sCustomField8Bit = GetBool("sCustomField8Bit");
        //    dataLoan.sCustomField9Bit = GetBool("sCustomField9Bit");
        //    dataLoan.sCustomField10Bit = GetBool("sCustomField10Bit");
        //    dataLoan.sCustomField11Bit = GetBool("sCustomField11Bit");
        //    dataLoan.sCustomField12Bit = GetBool("sCustomField12Bit");
        //    dataLoan.sCustomField13Bit = GetBool("sCustomField13Bit");
        //    dataLoan.sCustomField14Bit = GetBool("sCustomField14Bit");
        //    dataLoan.sCustomField15Bit = GetBool("sCustomField15Bit");
        //    dataLoan.sCustomField16Bit = GetBool("sCustomField16Bit");
        //    dataLoan.sCustomField17Bit = GetBool("sCustomField17Bit");
        //    dataLoan.sCustomField18Bit = GetBool("sCustomField18Bit");
        //    dataLoan.sCustomField19Bit = GetBool("sCustomField19Bit");
        //    dataLoan.sCustomField20Bit = GetBool("sCustomField20Bit");
        //    dataLoan.sCustomField1Notes = GetString("sCustomField1Notes");
        //    dataLoan.sCustomField2Notes = GetString("sCustomField2Notes");
        //    dataLoan.sCustomField3Notes = GetString("sCustomField3Notes");
        //    dataLoan.sCustomField4Notes = GetString("sCustomField4Notes");
        //    dataLoan.sCustomField5Notes = GetString("sCustomField5Notes");
        //    dataLoan.sCustomField6Notes = GetString("sCustomField6Notes");
        //    dataLoan.sCustomField7Notes = GetString("sCustomField7Notes");
        //    dataLoan.sCustomField8Notes = GetString("sCustomField8Notes");
        //    dataLoan.sCustomField9Notes = GetString("sCustomField9Notes");
        //    dataLoan.sCustomField10Notes = GetString("sCustomField10Notes");
        //    dataLoan.sCustomField11Notes = GetString("sCustomField11Notes");
        //    dataLoan.sCustomField12Notes = GetString("sCustomField12Notes");
        //    dataLoan.sCustomField13Notes = GetString("sCustomField13Notes");
        //    dataLoan.sCustomField14Notes = GetString("sCustomField14Notes");
        //    dataLoan.sCustomField15Notes = GetString("sCustomField15Notes");
        //    dataLoan.sCustomField16Notes = GetString("sCustomField16Notes");
        //    dataLoan.sCustomField17Notes = GetString("sCustomField17Notes");
        //    dataLoan.sCustomField18Notes = GetString("sCustomField18Notes");
        //    dataLoan.sCustomField19Notes = GetString("sCustomField19Notes");
        //    dataLoan.sCustomField20Notes = GetString("sCustomField20Notes");

        //    dataLoan.Save();
        //}

	}
}
