using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.los.admin;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;


namespace LendersOfficeApp.newlos.Status
{
	public partial class Conditions : BaseLoanPage
	{

        private CPageData CreatePageData(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(Conditions));
        }

		private BrokerUserPrincipal CurrentUser
		{
			// Get user principal.
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		protected void PageInit(object sender, System.EventArgs e)
		{
			this.PageTitle = "Conditions";
			this.PageID = "StatusConditions";
			this.PDFPrintClass = typeof(LendersOffice.Pdf.CConditionPDF);
			this.m_StatusLabel.Text = "";
			this.m_StatusLabel.CssClass = "hidden";
			
		}

		private void BindDataGrid(CPageData dataLoan) 
		{
			DataSet ds = dataLoan.sCondDataSet.Value;
			m_conditionDG.DataSource = ds.Tables[0].DefaultView;
			m_conditionDG.DataBind();
		}
		protected override void LoadData() 
		{
			CPageData dataLoan = CreatePageData( LoanID );
			dataLoan.InitLoad();
            sFileVersion = dataLoan.sFileVersion;
			BindDataGrid(dataLoan);
		}
		protected override void SaveData() 
		{
			int cnt = m_conditionDG.Items.Count;
			// Go through a list of conditions and save.
			CPageData dataLoan = CreatePageData(LoanID);
			dataLoan.InitSave(sFileVersion);


			for (int row = 0; row < cnt; row++) 
			{
				string id = (string) m_conditionDG.DataKeys[row];
				CCondFieldsObsolete field = dataLoan.GetCondFieldsObsolete(new Guid(id));

				DataGridItem item = m_conditionDG.Items[row];
				CheckBox isDoneCB = (CheckBox) item.FindControl("IsDone");
				TextBox doneDateTF = (TextBox) item.FindControl("DoneDate");
				CheckBox isRequiredCB = (CheckBox) item.FindControl("IsRequired");
				TextBox priorToTF = (TextBox) item.FindControl("PriorTo");
				TextBox condDescTF = (TextBox) item.FindControl("CondDesc");
				field.IsDone = isDoneCB.Checked;
				field.DoneDate_rep = doneDateTF.Text;
				field.IsRequired = isRequiredCB.Checked;
				field.CondDesc = condDescTF.Text;
				field.PriorToEventDesc = priorToTF.Text;
				field.Update();

			}

			dataLoan.Save();

		}

		private void Condition_grid_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{

			CPageData dataLoan = CreatePageData( LoanID );
			dataLoan.InitSave(sFileVersion);

			Guid recordId = new Guid((string) m_conditionDG.DataKeys[e.Item.ItemIndex]);

			DataSet ds = dataLoan.sCondDataSet.Value;
			DataRowCollection rows = ds.Tables[0].Rows;
			foreach( DataRow row in rows )
			{
				Guid id = new Guid( row["RecordId"].ToString() );
				if( id == recordId )
				{
					row.Delete();
					break;
				}							
			}

			dataLoan.sCondDataSet = ds;
			dataLoan.Save();
			
			BindDataGrid(dataLoan);

		}


		protected void AddCondition_btn_Click(object sender, System.EventArgs e)
		{
			CPageData dataLoan = CreatePageData( LoanID );
			dataLoan.InitSave(sFileVersion);

			// When adding new condition, default is not done.

            CCondFieldsObsolete f = dataLoan.GetCondFieldsObsolete(-1);
			f.CondDesc = "";
			f.IsDone = false;
			f.IsRequired = true;
			f.PriorToEventDesc = "";
			f.Update();

			dataLoan.Save();
			BindDataGrid(dataLoan);
		}


		/// <summary>
		/// 08-06-07 av Added import functionaluty. This method adds the items in the selected template 
		/// even if another condition exist and it has the same name. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void ImportClick(object sender, System.EventArgs e)
		{
			if ( Request.Params["m_ImportTemplateId"] != null ) 
			{
				CPageData currentDataLoan = CreatePageData( this.LoanID );
				CPageData dataLoan = CreatePageData( new Guid(Request.Params["m_ImportTemplateId"] ));
				currentDataLoan.InitSave(sFileVersion);
				dataLoan.InitLoad();

				foreach(  DataRow row in dataLoan.sCondDataSet.Value.Tables[0].Rows  ) 
				{
                    CCondFieldsObsolete f = currentDataLoan.GetCondFieldsObsolete(-1);
					f.CondDesc = row["CondDesc"].ToString();
					f.IsDone = false;
					f.IsRequired = true;
					f.PriorToEventDesc = "";
					f.Update();
				}
				this.m_StatusLabel.Text = String.Format("Imported {0} conditions from Template {1}.", dataLoan.sCondDataSet.Value.Tables[0].Rows.Count, Request.Params["m_ImportTemplateName"]);
				this.m_StatusLabel.CssClass = "warning";
				currentDataLoan.Save();
				BindDataGrid(currentDataLoan);
			}
		}

		



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.m_conditionDG.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.m_conditionDG_ItemCreated);
            this.m_conditionDG.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.Condition_grid_DeleteCommand);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

		private void m_conditionDG_ItemCreated(object sender, DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer)
            {
                return;
            }

			object value = null;

			CheckBox isDoneCB = new CheckBox();
			isDoneCB.ID = "IsDone";
			value = DataBinder.Eval(e.Item.DataItem, "IsDone");
			isDoneCB.Checked =  (value != null &&  value.ToString() == "True");
			isDoneCB.Attributes.Add("onclick", "performCheck(this);");
			e.Item.Cells[0].Controls.Add(isDoneCB);

			MeridianLink.CommonControls.DateTextBox dt = new MeridianLink.CommonControls.DateTextBox();
			dt.ID = "DoneDate";
			dt.Width = new Unit("75px");
			dt.Attributes.Add("preset", "date");
			value = DataBinder.Eval(e.Item.DataItem, "DoneDate");
			dt.Text = value == null ? "" : value.ToString();
			e.Item.Cells[1].Controls.Add(dt);

			CheckBox isRequiredCB = new CheckBox();
			isRequiredCB.ID = "IsRequired";
			e.Item.Cells[2].Controls.Add(isRequiredCB);
			value = DataBinder.Eval(e.Item.DataItem, "IsRequired");
			isRequiredCB.Checked =  (value != null &&  value.ToString() == "True");

			MeridianLink.CommonControls.ComboBox cbl = new MeridianLink.CommonControls.ComboBox();
			value = DataBinder.Eval(e.Item.DataItem, "PriorToEventDesc");
			cbl.Text = value == null ? "" : value.ToString();
			cbl.ID = "PriorTo";
			cbl.Width = new Unit("110px");
			cbl.Items.Add("doc");
			cbl.Items.Add("fund");
			cbl.Items.Add("approval");
			cbl.Items.Add("broker hold released");
			cbl.Items.Add("sub");
			cbl.Items.Add("suspend");
			e.Item.Cells[3].Controls.Add(cbl);

            TextBox condDescTextBox = new TextBox();
            condDescTextBox.ID = "CondDesc";
            value = DataBinder.Eval(e.Item.DataItem, "CondDesc");
            condDescTextBox.Text = value?.ToString() ?? string.Empty;
            condDescTextBox.TextMode = TextBoxMode.MultiLine;
            condDescTextBox.Rows = 2;
            condDescTextBox.Width = new Unit("100%");
            e.Item.Cells[4].Controls.Add(condDescTextBox);

            if (this.IsReadOnly)
            {
                e.Item.Cells[5].Controls.Clear();
            }
			else
            {
                e.Item.Cells[5].Attributes.Add("onclick", "return confirm('Do you want to delete this condition?');");
            }
		}
	}
}
