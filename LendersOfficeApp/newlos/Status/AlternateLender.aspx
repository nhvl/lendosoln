﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AlternateLender.aspx.cs" Inherits="LendersOfficeApp.newlos.Status.AlternateLender" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="System.Data" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title> Alternate Lender List</title>
</head>

<script type="text/javascript">
    function _init() {
        resize(470, 300);
    }  
    
    function onEditLender(id) {
        showModal('/newlos/Status/AlternateLenderPopup.aspx?lenderID=' + id, null, null, null, function(){ __doPostBack('', '');},{hideCloseButton:true});
    }

    function onDeleteLender(id) {
        document.getElementById("m_hiddenId").value = id;
        document.getElementById("m_hiddenCmd").value = "deleteLender";
        __doPostBack('','');                 
    }
    
</script> 
     
<body style="BACKGROUND-COLOR: gainsboro">
    <h4 class="page-header">Alternate Lender List</h4>   
    <form id="form1" runat="server"> 
    <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
    <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />  
    <input type="hidden" value="" runat="server" id="m_hiddenCmd" name="m_hiddenCmd"/>
    <input type="hidden" value="" runat="server" id="m_hiddenId" name="m_hiddenId"/>
    <script type="text/javascript">
        var theForm = document.forms['form1'];
        if (!theForm) {
            theForm = document.form1;
        }
        function __doPostBack(eventTarget, eventArgument) {
            if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
                theForm.__EVENTTARGET.value = eventTarget;
                theForm.__EVENTARGUMENT.value = eventArgument;
                theForm.submit();
            }
        }
    </script>
	<table cellspacing="2" cellpadding="3" width="95%" border="0">	    
	    <tr>
	        <td>
	            <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Width="450" Height="200" BorderWidth="0px" BorderColor="Gray"> 
                    <asp:DataGrid runat="server" id="m_dgLenders"  OnItemDataBound="OnDataBound_m_dgLenders" Width="95%"  AutoGenerateColumns="false">
                        <AlternatingItemStyle CssClass="GridAlternatingItem" />
                        <ItemStyle CssClass="GridItem" />
                        <HeaderStyle CssClass="GridHeader" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="Lender Name">
                                <ItemTemplate>
                                    <ml:EncodedLiteral runat="server" id="LenderName"></ml:EncodedLiteral>
                                </ItemTemplate>
                            </asp:TemplateColumn>            
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <a runat="server" id="EditLender" >edit</a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <a runat="server" id="DeleteLender">delete</a>				                                
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </asp:Panel>
            </td>
        </tr>         
        <tr align="left">
            <td>
                <input type="button" value="Add New" onclick="onEditLender(null)" />
            </td>
        </tr>
        <tr align = "center">
            <td>
                <input type="button" value="Close" onclick="onClosePopup()" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
