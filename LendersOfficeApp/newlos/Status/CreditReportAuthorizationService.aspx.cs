﻿#region Generated Code
namespace LendersOfficeApp.newlos.Status
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.ObjLib.CreditReportAuthorization;
    using LendersOffice.Security;

    /// <summary>
    /// Provides an interface to save extra-loan data data in the <seealso cref="CreditReportAuthorization"/> user control.
    /// </summary>
    public partial class CreditReportAuthorizationService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Processes the method specified by <paramref name="methodName"/>.
        /// </summary>
        /// <param name="methodName">The name of the action being requested.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "SaveDocumentAssociation":
                    this.SaveDocumentAssociation();
                    break;
                default:
                    throw CBaseException.GenericException($"Unhandled method [{methodName}]");
            }
        }

        /// <summary>
        /// Saves a document association.
        /// </summary>
        private void SaveDocumentAssociation()
        {
            var appId = this.GetGuid("AppId");
            var isCoborrower = this.GetBool("IsCoborr");
            var documentId = this.GetGuid("DocumentId", Guid.Empty);
            var brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            if (documentId == Guid.Empty)
            {
                this.ClearVerbalCreditAuthorization(appId, isCoborrower);
                DocumentAssociation.Clear(brokerId, appId, isCoborrower);
            }
            else
            {
                DocumentAssociation.Save(brokerId, appId, isCoborrower, documentId);
            }
        }

        /// <summary>
        /// Clears the verbal credit report authorization for the borrower.
        /// </summary>
        /// <param name="appId">
        /// The ID of the application.
        /// </param>
        /// <param name="isCoborrower">
        /// True to clear the co-borrower's verbal authorization, false otherwise.
        /// </param>
        /// <remarks>
        /// Per PDE, this should clear the verbal authorization indicators regardless 
        /// of whether the lender has verbal authorization automation enabled.
        /// </remarks>
        private void ClearVerbalCreditAuthorization(Guid appId, bool isCoborrower)
        {
            var loanId = this.GetGuid("LoanId");

            var loan = CPageData.CreateUsingSmartDependency(loanId, typeof(CreditReportAuthorizationService));
            loan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);

            var app = loan.GetAppData(appId);

            if (isCoborrower)
            {
                app.aCCreditReportAuthorizationProvidedVerbally = false;
            }
            else
            {
                app.aBCreditReportAuthorizationProvidedVerbally = false;
            }

            loan.Save();
        }
    }
}
