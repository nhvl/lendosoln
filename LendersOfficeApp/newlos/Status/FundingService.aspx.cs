using System;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Status
{
    public class FundingServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FundingServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataLoan.sInternalFundingSrcDesc = GetString("sInternalFundingSrcDesc").TrimWhitespaceAndBOM();
            dataLoan.sExternalFundingSrcDesc = GetString("sExternalFundingSrcDesc").TrimWhitespaceAndBOM();
            dataLoan.sWarehouseFunderDesc = GetString("sWarehouseFunderDesc");
            dataLoan.sIsTexasHomeEquity = GetBool("sIsTexasHomeEquity");
            dataLoan.sIsDocDrawExternal = GetBool("sIsDocDrawExternal");

        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
        }
    }
	/// <summary>
	/// Summary description for FundingService.
	/// </summary>
	public partial class FundingService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new FundingServiceItem());
        }

	}
}
