using System;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using System.Data;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using System.Drawing;

namespace LendersOfficeApp.newlos.Status
{
    public class AgentCommission
    {
       public string AgentType;
       public string AgentName;
       public string CompanyName;
       public string CommissionAmount;
       public bool   IsPaid;
       public string Notes;
       public string RecordId;
    }

    public partial class Commission : BaseLoanPage
    {

        private bool UserHasReadPermission
        {
            get
            {
                return BrokerUser.HasPermission(Permission.AllowCloserRead) && BrokerUser.HasPermission(Permission.AllowAccountantRead);
            }
        }

        private bool UserHasWritePermission
        {
            get
            {
                return BrokerUser.HasPermission(Permission.AllowCloserWrite) && BrokerUser.HasPermission(Permission.AllowAccountantWrite);
            }
        }

        public override bool IsReadOnly
        {
            get
            {
                if (!base.IsReadOnly)
                {
                    if (UserHasWritePermission)
                        return false;
                    else if (UserHasReadPermission)
                        return true;
                }

                return base.IsReadOnly;
            }
        }

        protected override void LoadData() 
        {
            if (!UserHasReadPermission)
                throw new AccessDenied();

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(RequestHelper.LoanID, typeof(Commission));
            dataLoan.InitLoad();
            sProfitGrossBorPnt.Text = dataLoan.sProfitGrossBorPnt_rep;
            sProfitGrossBorMb.Text = dataLoan.sProfitGrossBorMb_rep;
            sProfitGrossBor.Text = dataLoan.sProfitGrossBor_rep;
            sGrossProfit.Text = dataLoan.sGrossProfit_rep;
            sNetProfit.Text = dataLoan.sNetProfit_rep;
            sCommissionTotal.Text = dataLoan.sCommissionTotal_rep;

            sProfitRebatePnt.Text = dataLoan.sProfitRebatePnt_rep;
            sProfitRebateMb.Text = dataLoan.sProfitRebateMb_rep;
            sProfitRebate.Text = dataLoan.sProfitRebate_rep;
            sProfitAncillaryFee.Text = dataLoan.sProfitAncillaryFee_rep;
            sProfitAncillaryFeeLckd.Checked = dataLoan.sProfitAncillaryFeeLckd;
            sProfitExternalFee.Text = dataLoan.sProfitExternalFee_rep;
            sProfitTotalFee.Text = dataLoan.sProfitTotalFee_rep;
            sRLckdD.Text = dataLoan.sRLckdD_rep;
            sRLckdExpiredD.Text = dataLoan.sRLckdExpiredD_rep;

            Tools.SetDropDownListValue(sAccPeriodMon, dataLoan.sAccPeriodMon_rep);
            Tools.SetDropDownListValue(sAccPeriodYr, dataLoan.sAccPeriodYr_rep);
            sCloseProbability.Text = dataLoan.sCloseProbability_rep;

            sCommissionTotalPaid.Text = dataLoan.sCommissionTotalPaid_rep;
            sCommissionTotalOutstanding.Text = dataLoan.sCommissionTotalOutstanding_rep;

            RegisterJsObject("AgentCommissions", Commission.SerializeAgents(
                dataLoan.sAgentCollection)
                );
        }

        public static List<AgentCommission> SerializeAgents(CAgentCollection agCollection)
        {
            if (agCollection == null)
                return new List<AgentCommission>();

            List<AgentCommission> agents = new List<AgentCommission>();
            int nAgents = agCollection.GetAgentRecordCount();
            AgentCommission ag = null;

            for (int i = 0; i < nAgents; ++i)
            {
                CAgentFields f = agCollection.GetAgentFields(i);
                if (f == null || f.IsValid == false)
                {
                    continue;
                }

                if (f.Commission != 0)
                {
                    ag = ConvertToCommission(f);
                    agents.Add(ag);
                }
            }

            return agents;

        }

        private static AgentCommission ConvertToCommission(CAgentFields agent)
        {
           AgentCommission ag = new AgentCommission();
           ag.AgentType = agent.AgentRoleDescription;
           ag.AgentName = agent.AgentName;
           ag.CompanyName = agent.CompanyName;
           ag.CommissionAmount = agent.Commission_rep;
           ag.IsPaid = agent.IsCommissionPaid;
           ag.Notes = agent.PaymentNotes;
           ag.RecordId = agent.RecordId.ToString();
            return ag;
        }

        protected override void SaveData() 
        {

        }


        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "Commissions";
            this.PageID = "StatusCommissions";

            sAccPeriodMon.Items.Add(new ListItem("", ""));
            sAccPeriodMon.Items.Add(new ListItem("January", "1"));
            sAccPeriodMon.Items.Add(new ListItem("February", "2"));
            sAccPeriodMon.Items.Add(new ListItem("March", "3"));
            sAccPeriodMon.Items.Add(new ListItem("April", "4"));
            sAccPeriodMon.Items.Add(new ListItem("May", "5"));
            sAccPeriodMon.Items.Add(new ListItem("June", "6"));
            sAccPeriodMon.Items.Add(new ListItem("July", "7"));
            sAccPeriodMon.Items.Add(new ListItem("August", "8"));
            sAccPeriodMon.Items.Add(new ListItem("September", "9"));
            sAccPeriodMon.Items.Add(new ListItem("October", "10"));
            sAccPeriodMon.Items.Add(new ListItem("November", "11"));
            sAccPeriodMon.Items.Add(new ListItem("December", "12"));

            sAccPeriodYr.Items.Add(new ListItem("", ""));
            for (int i = 2000; i < 2011; i++) 
            {
                sAccPeriodYr.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            IsAppSpecific = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
    }
}
