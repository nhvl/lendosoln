﻿#region Auto Generated Code
namespace LendersOfficeApp.newlos.Status
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.ObjLib.Appraisal;
    using LendersOffice.ObjLib.Conversions.GlobalDMS;
    using LendersOffice.ObjLib.Conversions.LQBAppraisal;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using MeridianLink.CommonControls;

    /// <summary>
    /// Page for configuring appraisal delivery.
    /// </summary>
    public partial class DeliverAppraisal : BaseLoanPage
    {
        /// <summary>
        /// The available generic docs.
        /// </summary>
        private Lazy<HashSet<Guid>> genericDocs;

        /// <summary>
        /// The permissions to load the page.
        /// </summary>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowAppraisalDelivery
                };
            }
        }

        /// <summary>
        /// Gets the appraisal order type.
        /// </summary>
        protected AppraisalOrderType? AppraisalType
        {
            get
            {
                return RequestHelper.GetNullableEnum<AppraisalOrderType>("aot", ignoreCase: true);
            }
        }

        /// <summary>
        /// Gets the appraisal order id.
        /// </summary>
        protected string AppraisalOrderId
        {
            get
            {
                return RequestHelper.GetSafeQueryString("aid");
            }
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        protected override void LoadData()
        {
            if (string.IsNullOrEmpty(this.AppraisalOrderId) || !this.AppraisalType.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid AppraisalOrderId or AppraisalType"));
            }

            var appraisalOrder = this.LoadAppraisalOrder();
            this.RegisterJsGlobalVariables("AppraisalOrderId", appraisalOrder.AppraisalOrderId);
            this.PopulateTargetVendors();
            this.PopulateDocuments(appraisalOrder);

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(DeliverAppraisal));
            dataLoan.InitLoad();

            this.sDocMagicFileId.Value = dataLoan.sDocMagicFileId;
        }

        /// <summary>
        /// Populates the documents repeater.
        /// </summary>
        /// <param name="appraisalOrder">The appraisal order to populate documents from.</param>
        protected void PopulateDocuments(AppraisalOrderBase appraisalOrder)
        {
            var edocViews = appraisalOrder.GetEdocDocumentViews(PrincipalFactory.CurrentPrincipal).CoalesceWithEmpty().Where(doc => !doc.IsDeleted);
            this.DocRepeater.DataSource = edocViews;
            this.DocRepeater.DataBind();
        }

        /// <summary>
        /// Loads an appraisal order.
        /// </summary>
        /// <returns>The loaded appraisal order.</returns>
        protected AppraisalOrderBase LoadAppraisalOrder()
        {
            AppraisalOrderType type = this.AppraisalType.Value;
            string id = this.AppraisalOrderId;
            AppraisalOrderBase appraisalOrder = null;
            if (type == AppraisalOrderType.Gdms)
            {
                int fileNumber;
                if (!int.TryParse(id, out fileNumber))
                {
                    throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid GDMS appraisal order file number: {id}"));
                }

                appraisalOrder = GDMSAppraisalOrderInfo.Load(fileNumber);
            }
            else if (type == AppraisalOrderType.Lqb)
            {
                appraisalOrder = LQBAppraisalOrder.Load(this.LoanID.ToString(), id);
            }
            else if (type == AppraisalOrderType.Manual)
            {
                Guid orderId;
                if (!Guid.TryParse(id, out orderId))
                {
                    throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid Manual appraisal order id: {id}"));
                }

                appraisalOrder = ManualAppraisalOrder.GetOrderById(orderId, this.BrokerID);
            }

            if (appraisalOrder == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Unable to load appraisal order. Type {type} Id: {id}"));
            }

            return appraisalOrder;
        }

        /// <summary>
        /// Populates the target vendors DDL.
        /// </summary>
        protected void PopulateTargetVendors()
        {
            this.TargetVendor.Items.Add(new ListItem(string.Empty, string.Empty));
            var activeDocVendors = this.Broker.AllActiveDocumentVendors.Where(vendor => vendor.EnableAppraisalDelivery);
            foreach (var vendorSettings in activeDocVendors)
            {
                IDocumentVendor vendor = DocumentVendorFactory.Create(PrincipalFactory.CurrentPrincipal.BrokerId, vendorSettings.VendorId);
                if (vendor.Config.PlatformType != E_DocumentVendor.DocMagic && !(vendor is DocMagicDocumentVendor))
                {
                    continue;
                }

                this.TargetVendor.Items.Add(new ListItem(vendor.Config.VendorName, vendorSettings.VendorId.ToString()));
            }
        }

        /// <summary>
        /// Page init function.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void PageInit(object sender, System.EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("SimplePopups.js");
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterCSS("jquery-ui-1.11.custom.css");
            this.RegisterJsScript("widgets/jquery.docmetadatatooltip.js");
            this.RegisterService("appraisal", "/newlos/Status/DeliverAppraisalService.aspx");
        }

        /// <summary>
        /// Initialize function.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.genericDocs = new Lazy<HashSet<Guid>>(() =>
            {
                EDocumentRepository repo = EDocumentRepository.GetUserRepository(PrincipalFactory.CurrentPrincipal);
                return new HashSet<Guid>(repo.GetGenericDocumentsByLoanId(this.LoanID).Select(doc => doc.DocumentId));
            });

            this.UseNewFramework = true;
            this.EnableJquery = true;
            this.InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Overrides the jquery version.
        /// </summary>
        /// <returns>The jquery version.</returns>
        protected override E_JqueryVersion GetJQueryVersion()
        {
            return E_JqueryVersion._1_11_2;
        }

        /// <summary>
        /// Binds the documents repeater.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The events arguments.</param>
        protected void DocRepeater_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            HtmlInputHidden edocId = (HtmlInputHidden)e.Item.FindControl("EDocId");
            HtmlGenericControl folder = (HtmlGenericControl)e.Item.FindControl("Folder");
            HtmlGenericControl docType = (HtmlGenericControl)e.Item.FindControl("DocType");
            HtmlGenericControl description = (HtmlGenericControl)e.Item.FindControl("Description");
            HtmlGenericControl comments = (HtmlGenericControl)e.Item.FindControl("Comments");
            HtmlGenericControl lastModified = (HtmlGenericControl)e.Item.FindControl("LastModified");
            HtmlImage fileInfo = (HtmlImage)e.Item.FindControl("FileInfo");
            HtmlAnchor fileLink = (HtmlAnchor)e.Item.FindControl("FileLink");
            HtmlAnchor xmlLink = (HtmlAnchor)e.Item.FindControl("XmlLink");

            var docView = (AppraisalDocumentView)e.Item.DataItem;

            edocId.Value = docView.EDocId?.ToString() ?? string.Empty;
            folder.InnerText = docView.Folder;
            docType.InnerText = docView.DocType;
            description.InnerText = docView.Description;
            comments.InnerText = docView.Comments;
            lastModified.InnerText = docView.LastModifiedDate;

            if (string.IsNullOrEmpty(docView.EdocMetaData))
            {
                fileInfo.Visible = false;
            }
            else
            {
                Tools.ApplyDocMetaDataTooltip(fileInfo, docView.EdocMetaData);
            }

            if (!genericDocs.Value.Contains(docView.EDocId.Value))
            {
                xmlLink.Visible = false;
            }
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
        }
    }
}