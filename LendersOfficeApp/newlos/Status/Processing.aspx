﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="Processing.aspx.cs" Inherits="LendersOfficeApp.newlos.Status.Processing" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CreditReportAuthorization" Src="~/newlos/Status/CreditReportAuthorization.ascx" %>
<%@ Register TagPrefix="uc" TagName="RespaDates" Src="~/newlos/Disclosure/RespaDates.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML>
<html>
<head runat="server">
    <title>Processing</title>
    <style>
        fieldset.RespaDates {
            width: 400px;
        }
        .w-input-date {
            width: 75px;
        }
        @media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
            .w-input-date:focus {
                width: 73px !important;
            }
        }
    </style>
</head>

    <script language="javascript">

        function _init() {
            lockField(<%= AspxTools.JsGetElementById(sAppSubmittedDLckd) %>, 'sAppSubmittedD');
            lockField(<%= AspxTools.JsGetElementById(sAppReceivedByLenderDLckd) %>, 'sAppReceivedByLenderD');
            lockField(document.getElementById('sDocMagicApplicationDLckd'), 'sDocMagicApplicationD');
            lockField(document.getElementById('sDocMagicGFEDLckd'), 'sDocMagicGFED');
            lockField(document.getElementById('sDocMagicEstAvailableThroughDLckd'), 'sDocMagicEstAvailableThroughD');
            lockField(document.getElementById('sDocMagicDocumentDLckd'), 'sDocMagicDocumentD');
            lockField(document.getElementById('sDocMagicClosingDLckd'), 'sDocMagicClosingD');
            lockField(document.getElementById('sDocMagicSigningDLckd'), 'sDocMagicSigningD');
            lockField(document.getElementById('sDocMagicCancelDLckd'), 'sDocMagicCancelD');
            lockField(document.getElementById('sDocMagicDisbursementDLckd'), 'sDocMagicDisbursementD');
            lockField(document.getElementById('sDocMagicRateLockDLckd'), 'sDocMagicRateLockD');
            lockField(document.getElementById('sDocMagicPreZSentDLckd'), 'sDocMagicPreZSentD');
            lockField(document.getElementById('sDocMagicReDiscSendDLckd'), 'sDocMagicReDiscSendD');
            lockField(document.getElementById('sDocMagicReDiscReceivedDLckd'), 'sDocMagicReDiscReceivedD');
            lockField(document.getElementById('sEstCloseDLckd'), 'sEstCloseD');
            var b = document.getElementById('sDocMagicRedisclosureMethodTLckd').checked;
            disableDDL(document.getElementById('sDocMagicRedisclosureMethodT'), !b);

        }
        
         function onDateStampClick() {
            var user = <%= AspxTools.JsString(BrokerUser.FirstName + " " + BrokerUser.LastName) %>;
                        
            var timeStamp = getTimeStamp(user);

            var notes = <%= AspxTools.JsGetElementById(sTrNotes) %>;

            notes.value = timeStamp + notes.value;

            updateDirtyBit();
        }

        var oldSaveMe = window.saveMe;
        window.saveMe = function(bRefreshScreen)
        {
            var resultSave = oldSaveMe(bRefreshScreen);
            if(resultSave)
            {
                window.parent.treeview.location = window.parent.treeview.location;
            }
            return resultSave;
        }

        function doAfterDateFormat(e)
        {
            if(e.id === "sAppSubmittedD" ||
                e.id === "sEstCloseD" ||
                e.id === "sQCCompDate")
            {
                refreshCalculation();
            }
        }
    </script>

<body class="RightBackground" >
<form id="form1" runat="server">
<div>
<table class="FormTable" cellSpacing="0" cellPadding="1" border="0">
    <tr>
        <td class="MainRightHeader" noWrap>
            Processing
        </td>
    </tr>
    <tr>
        <td>
            <table class="FormTable" cellSpacing="0" cellPadding="2" border="0">
                <tr class="bg-subheader">
                    <td>
                        Event
                    </td>
                    <td>
                        Date
                    </td>
                    <td>
                        Comments
                    </td>
                    <td></td>
                </tr>
                <tr  class="FieldLabel" nowrap>
                    <td>
                        Application Submitted
                    </td>
                    <td>
                        <ml:DateTextBox ID="sAppSubmittedD" runat="server" CssClass="mask w-input-date" preset="date" onchange="date_onblur(null, this);" ></ml:DateTextBox>
                    </td>
                    <td>
                        <asp:CheckBox ID="sAppSubmittedDLckd" runat="server" onclick="refreshCalculation();" />Lock
                    </td>
                </tr>
                <tr  class="FieldLabel" nowrap>
                    <td>
                        App Received by Lender
                    </td>
                    <td>
                        <ml:DateTextBox ID="sAppReceivedByLenderD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                    </td>
                    <td>
                        <asp:CheckBox ID="sAppReceivedByLenderDLckd" runat="server" onclick="refreshCalculation();" />Lock
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" style="WIDTH: 149px">
                        Estimated Closing
                    </td>
                    <td>
                        <ml:DateTextBox ID="sEstCloseD" runat="server" preset="date" CssClass="mask w-input-date" onchange="date_onblur(null, this);" ></ml:DateTextBox>
                    </td>
                    <td class="FieldLabel">
                        <asp:CheckBox ID="sEstCloseDLckd" runat="server" onclick="refreshCalculation();" />Lock
                    </td>
                    <td>
                        <asp:textbox id="sEstCloseN" runat="server" Width="135px" MaxLength="21" ></asp:textbox>
                    </td>
                </tr>
                <tr id="QC_Comp_Date">
                    <td class="FieldLabel" nowrap>
                        QC Completed Date
                    </td>
                    <td nowrap>
                        <ml:DateTextBox ID="sQCCompDate" runat="server" CssClass="mask w-input-date" preset="date" ></ml:DateTextBox>
                        <asp:TextBox runat="server" ID="sQCCompDateTime" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <uc:RespaDates runat="server" ID="RespaDates"></uc:RespaDates>
            <table cellSpacing="0" cellPadding="2" border="0">
                <tr class="bg-subheader">
                  <td style="WIDTH: 149px">Custom Events</td>
                  <td style="WIDTH: 130px" colSpan=2>Event Description</td>
                  <td style="WIDTH: 74px">Date</td>
                  <td>Comments</td></tr>
                <tr>
                  <td class=FieldLabel style="WIDTH: 149px">Custom Event #1</td>
                  <td colSpan=2><asp:textbox id=sU1LStatDesc runat="server" Width="200px" MaxLength="21"></asp:textbox></td>
                  <td noWrap><ml:datetextbox id=sU1LStatD runat="server" onchange="refreshCalculation();" preset="date" CssClass="mask w-input-date"></ml:datetextbox></td>
                  <td><asp:textbox id=sU1LStatN runat="server" Width="135px"  MaxLength="21"></asp:textbox></td></tr>
                <tr>
                  <td class=FieldLabel style="WIDTH: 149px">Custom Event #2</td>
                  <td colSpan=2><asp:textbox id=sU2LStatDesc runat="server" Width="200px" MaxLength="21"></asp:textbox></td>
                  <td><ml:datetextbox id=sU2LStatD runat="server" onchange="refreshCalculation();" preset="date" CssClass="mask w-input-date"></ml:datetextbox></td>
                  <td><asp:textbox id=sU2LStatN runat="server" Width="135px"  MaxLength="21"></asp:textbox></td></tr>
                <tr>
                  <td class=FieldLabel style="WIDTH: 149px">Custom Event #3</td>
                  <td colSpan=2><asp:textbox id=sU3LStatDesc runat="server" Width="200px" MaxLength="21"></asp:textbox></td>
                  <td><ml:datetextbox id=sU3LStatD runat="server" onchange="refreshCalculation();" preset="date" CssClass="mask w-input-date"></ml:datetextbox></td>
                  <td><asp:textbox id=sU3LStatN runat="server" Width="135px"  MaxLength="21"></asp:textbox></td></tr>
                <tr>
                  <td class=FieldLabel style="WIDTH: 149px">Custom Event #4</td>
                  <td colSpan=2><asp:textbox id=sU4LStatDesc runat="server" Width="200px" MaxLength="21"></asp:textbox></td>
                  <td><ml:datetextbox id=sU4LStatD runat="server" onchange="refreshCalculation();" preset="date" CssClass="mask w-input-date"></ml:datetextbox></td>
                  <td><asp:textbox id=sU4LStatN runat="server" Width="135px"  MaxLength="21"></asp:textbox></td>
                </tr>
                
                 <tr class="bg-subheader">
                    <td colspan="5">
                        Disclosure/Document Dates
                    </td>
                </tr>
                    <tr class="FieldLabel">
                        <td>
                            Application Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicApplicationD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicApplicationDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            GFE Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicGFED" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicGFEDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Est. Available Through
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicEstAvailableThroughD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicEstAvailableThroughDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Document Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicDocumentDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Closing Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicClosingD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicClosingDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Signing Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicSigningD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicSigningDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Cancel Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicCancelD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicCancelDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Disbursement Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicDisbursementD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicDisbursementDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Rate Lock Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicRateLockD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicRateLockDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Pre-Z Send Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicPreZSentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicPreZSentDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Re-Disc Send Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicReDiscSendD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicReDiscSendDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Re-Disc Method
                        </td>
                        <td>
                            <asp:DropDownList ID="sDocMagicRedisclosureMethodT" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicRedisclosureMethodTLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Re-Disc Received Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicReDiscReceivedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicReDiscReceivedDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="bg-subheader">
                        <td colspan="5">
                            Credit Report Authorization
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <uc1:CreditReportAuthorization runat="server" ID="CreditReportAuthorizationCtrl"></uc1:CreditReportAuthorization>
                        </td>
                    </tr>
                    <tr class="bg-subheader">
                        <td colspan="5">
                            Purchase Contract Dates
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Purchase Contract Date
                        </td>
                        <td colspan="2">
                            <ml:DateTextBox ID="sPurchaseContractDate" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Financing Contingency Exp. Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFinancingContingencyExpD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            Extension Date
                        </td>
                        <td colspan="2">
                            <ml:DateTextBox ID="sFinancingContingencyExtensionExpD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                    </tr>
                <tr class="bg-subheader">
	                <td style="width: 178px">Other Documents</td>
	                <td style="width: 100px">Ordered</td>
	                <td noWrap>Due</td>
	                <td>
                        Document Date
                    </td>
	                <td >Received</td>
	                <td>Comments</td>
                </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px">
                            Preliminary Title Report
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sPrelimRprtOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sPrelimRprtDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPrelimRprtDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPrelimRprtRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPrelimRprtN" runat="server" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            Closing Services
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sClosingServOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sClosingServDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sClosingServDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sClosingServRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sClosingServN" runat="server" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px">
                            Appraisal Report
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sApprRprtOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sApprRprtDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sSpValuationEffectiveD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sApprRprtRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sApprRprtN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px">
                            TIL Disclosure / GFE
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sTilGfeOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sTilGfeDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sTilGfeDocumentD" runat="server" CssClass="mask w-input-date" ReadOnly="true" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sTilGfeRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sTilGfeN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            Flood Certificate
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFloodCertOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFloodCertDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFloodCertificationDeterminationD" runat="server" CssClass="mask w-input-date" ReadOnly="true" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFloodCertRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFloodCertN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            USPS Check  
                        </td>
                        <td>
                            <ml:DateTextBox ID="sUSPSCheckOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sUSPSCheckDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sUSPSCheckDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sUSPSCheckRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sUSPSCheckN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            Payoff/Demand Statement  
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPayDemStmntOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPayDemStmntDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPayDemStmntDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPayDemStmntRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPayDemStmntN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            CAIVRS  
                        </td>
                        <td>
                            <ml:DateTextBox ID="sCAIVRSOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sCAIVRSDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sCAIVRSDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sCAIVRSRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sCAIVRSN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            Fraud Services  
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFraudServOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFraudServDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFraudServDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFraudServRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFraudServN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            AVM
                        </td>
                        <td>
                            <ml:DateTextBox ID="sAVMOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sAVMDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sAVMDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sAVMRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sAVMN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            HOA Certification
                        </td>
                        <td>
                            <ml:DateTextBox ID="sHOACertOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sHOACertDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sHOACertDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sHOACertRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sHOACertN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            Estimated HUD
                        </td>
                        <td>
                            <ml:DateTextBox ID="sEstHUDOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sEstHUDDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sEstHUDDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sEstHUDRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sEstHUDN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            CPL/ICL
                        </td>
                        <td>
                            <ml:DateTextBox ID="sCPLAndICLOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sCPLAndICLDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sCPLAndICLDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sCPLAndICLRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sCPLAndICLN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            Wire Instructions
                        </td>
                        <td>
                            <ml:DateTextBox ID="sWireInstructOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sWireInstructDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sWireInstructDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sWireInstructRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sWireInstructN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            Insurance(s)
                        </td>
                        <td>
                            <ml:DateTextBox ID="sInsurancesOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sInsurancesDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sInsurancesDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sInsurancesRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sInsurancesN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 149px">
                            <asp:TextBox ID="sU1DocStatDesc" runat="server" Width="131" MaxLength="21"></asp:TextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU1DocStatOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU1DocStatDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU1DocDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU1DocStatRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sU1DocStatN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 149px">
                            <asp:TextBox ID="sU2DocStatDesc" runat="server" Width="131px" MaxLength="21"></asp:TextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU2DocStatOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU2DocStatDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU2DocDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU2DocStatRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sU2DocStatN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 149px">
                            <asp:TextBox ID="sU3DocStatDesc" runat="server" Width="131px" MaxLength="21"></asp:TextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU3DocStatOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU3DocStatDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU3DocDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU3DocStatRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sU3DocStatN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 149px">
                            <asp:TextBox ID="sU4DocStatDesc" runat="server" Width="131px" MaxLength="21"></asp:TextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU4DocStatOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU4DocStatDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU4DocDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU4DocStatRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sU4DocStatN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 149px">
                            <asp:TextBox ID="sU5DocStatDesc" runat="server" Width="131px" MaxLength="21"></asp:TextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU5DocStatOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU5DocStatDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU5DocDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU5DocStatRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sU5DocStatN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Loan Package
                        </td>
                        <td>
                            <ml:DateTextBox ID="sLoanPackageOrderedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td>
                            <ml:DateTextBox ID="sLoanPackageDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sLoanPackageReceivedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sLoanPackageN" Width="135px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Mortgage / Deed of Trust
                        </td>
                        <td>
                            <ml:DateTextBox ID="sMortgageDOTOrderedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td>
                            <ml:DateTextBox ID="sMortgageDOTDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sMortgageDOTReceivedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sMortgageDOTN" Width="135px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px">
                            Note
                        </td>
                        <td>
                            <ml:DateTextBox ID="sNoteOrderedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td>
                            <ml:DateTextBox ID="sDocumentNoteD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sNoteReceivedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNoteN" Width="135px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Final HUD-1 Settlement Stmt
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFinalHUD1SttlmtStmtOrderedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td>
                            <ml:DateTextBox ID="sFinalHUD1SttlmtStmtDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFinalHUD1SttlmtStmtReceivedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFinalHUD1SttlmtStmtN" Width="135px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Title Insurance Policy
                        </td>
                        <td>
                            <ml:DateTextBox ID="sTitleInsPolicyOrderedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td>
                            <ml:DateTextBox ID="sTitleInsPolicyDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sTitleInsPolicyReceivedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sTitleInsPolicyN" Width="135px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Mortgage Ins. Certificate
                        </td>
                        <td>
                            <ml:DateTextBox ID="sMiCertOrderedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td>
                            <ml:DateTextBox ID="sMiCertIssuedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sMiCertReceivedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sMiCertN" Width="135px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Final Hazard Ins Policy
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFinalHazInsPolicyOrderedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td>
                            <ml:DateTextBox ID="sFinalHazInsPolicyDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFinalHazInsPolicyReceivedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFinalHazInsPolicyN" Width="135px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Final Flood Ins Policy
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFinalFloodInsPolicyOrderedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td>
                            <ml:DateTextBox ID="sFinalFloodInsPolicyDocumentD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFinalFloodInsPolicyReceivedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFinalFloodInsPolicyN" Width="135px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                            <td class="FieldLabel" style="width: 149px" nowrap>
                              Lead Source
                             </td>
                            <td nowrap colspan="5">
                               <asp:DropDownList ID="sLeadSrcDesc" runat="server">
                                </asp:DropDownList>
                            </td>
            </tr>
                <tr>
	                <td class="LoanFormHeader" colSpan="5" rowSpan="1">For Borrower
		                <ml:EncodedLiteral id="aBNm" runat="server" ></ml:EncodedLiteral></td>
                </tr>
                <tr class="bg-subheader">
	                <td style="WIDTH: 149px">Report Type</td>
	                <td style="WIDTH: 75px">Ordered</td>
	                <td noWrap>Due</td>
	                <td style="WIDTH: 75px">Received</td>
	                <td Comments</td>
                </tr>
                <tr>
	                <td class="FieldLabel" style="WIDTH: 149px">Credit Report</td>
	                <td noWrap><ml:datetextbox id="aCrOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:datetextbox></td>
	                <td noWrap><ml:datetextbox id="aCrDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:datetextbox></td>
	                <td noWrap><ml:datetextbox id="aCrRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:datetextbox></td>
	                <td><asp:textbox id="aCrN" runat="server" Width="135px" MaxLength="36" ></asp:textbox></td>
                </tr>
                <tr>
	                <td class="FieldLabel" style="WIDTH: 149px">LQI Credit Report</td>
	                <td noWrap><ml:datetextbox id="aLqiCrOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:datetextbox></td>
	                <td noWrap><ml:datetextbox id="aLqiCrDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:datetextbox></td>
	                <td noWrap><ml:datetextbox id="aLqiCrRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:datetextbox></td>
	                <td><asp:textbox id="aLqiCrN" runat="server" Width="135px" MaxLength="36" ></asp:textbox></td>
                </tr>
                <tr>
	                <td class="FieldLabel" style="WIDTH: 149px">Business Credit Report</td>
	                <td noWrap><ml:datetextbox id="aBusCrOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:datetextbox></td>
	                <td noWrap><ml:datetextbox id="aBusCrDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:datetextbox></td>
	                <td noWrap><ml:datetextbox id="aBusCrRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:datetextbox></td>
	                <td><asp:textbox id="aBusCrN" runat="server" Width="135px" MaxLength="36" ></asp:textbox></td>
                </tr>
                <tr>
                    <td style="width: 170px">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td class="FieldLabel" width="98px">
                                    Pre-Funding VOE
                                </td>
                                <td>
                                    <asp:DropDownList ID="aBPreFundVoeTypeT" Width="71px" runat="server"></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td nowrap>
                        <ml:DateTextBox ID="aBPreFundVoeOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                    </td>
                    <td nowrap>
                        <ml:DateTextBox ID="aBPreFundVoeDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                    </td>
                    <td nowrap>
                        <ml:DateTextBox ID="aBPreFundVoeRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="aBPreFundVoeN" runat="server" Width="135" MaxLength="36" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
	                <td class="FieldLabel" style="WIDTH: 170px"><asp:textbox id="aU1DocStatDesc" runat="server" Width="169" MaxLength="21"></asp:textbox></td>
	                <td noWrap><ml:datetextbox id="aU1DocStatOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:datetextbox></td>
	                <td noWrap><ml:datetextbox id="aU1DocStatDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:datetextbox></td>
	                <td noWrap><ml:datetextbox id="aU1DocStatRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:datetextbox></td>
	                <td><asp:textbox id="aU1DocStatN" runat="server" Width="135px" MaxLength="36" ></asp:textbox></td>
                </tr>
                <tr>
	                <td class="FieldLabel" style="WIDTH: 170px"><asp:textbox id="aU2DocStatDesc" runat="server" Width="169px" MaxLength="21"></asp:textbox></td>
	                <td noWrap><ml:datetextbox id="aU2DocStatOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:datetextbox></td>
	                <td noWrap><ml:datetextbox id="aU2DocStatDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:datetextbox></td>
	                <td noWrap><ml:datetextbox id="aU2DocStatRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:datetextbox></td>
	                <td><asp:textbox id="aU2DocStatN" runat="server" Width="135px" MaxLength="36" ></asp:textbox></td>
                </tr>
                 <tr class="bg-subheader">
                    <td style="width: 149px">
                        Service
                    </td>
                    <td style="width: 73px">
                        Ordered
                    </td>
                    <td nowrap>
                        Deactivated
                    </td>
                    <td style="width: 74px">
                        File Number
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="FieldLabel" style="width: 149px">
                        UDN
                    </td>
                    <td nowrap>
                        <ml:DateTextBox ID="aUDNOrderedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                    </td>
                    <td nowrap>
                        <ml:DateTextBox ID="aUDNDeactivatedD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                    </td>
                    <td nowrap>
                        <asp:TextBox ID="aUdnOrderId" runat="server" Width="96"></asp:TextBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        &nbsp;
                    </td>
                    <td colspan="3">
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="LoanFormHeader" colspan="5" rowspan="1">
                        For Co-Borrower <ml:EncodedLiteral ID="aCNm" runat="server" ></ml:EncodedLiteral>
                    </td> 
                </tr>
                <tr class="bg-subheader">
                    <td style="width: 170px">
                        Report Type
                    </td>
                    <td style="width: 73px">
                        Ordered
                    </td>
                    <td nowrap>
                        Due
                    </td>
                    <td style="width: 74px">
                        Received
                    </td>
                    <td>
                        Comments
                    </td>
                </tr>
                <tr>
                    <td style="width: 170px">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td class="FieldLabel" width="98px">
                                    Pre-Funding VOE
                                </td>
                                <td>
                                    <asp:DropDownList ID="aCPreFundVoeTypeT" Width="71px" runat="server"></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td nowrap>
                        <ml:DateTextBox ID="aCPreFundVoeOd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                    </td>
                    <td nowrap>
                        <ml:DateTextBox ID="aCPreFundVoeDueD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                    </td>
                    <td nowrap>
                        <ml:DateTextBox ID="aCPreFundVoeRd" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="aCPreFundVoeN" runat="server" Width="135" MaxLength="36" ></asp:TextBox>
                    </td>
                </tr> 
                <tr>
	                <td class="FieldLabel">&nbsp;</td>
	                <td colSpan="3"></td>
	                <td></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</div>
<div>
        <span class="FieldLabel">Notes</span>
        <input type="button" value="Date &amp; time stamp" onclick="onDateStampClick();">
        <div>
            <asp:TextBox ID="sTrNotes" runat="server" Width="578px" Height="90px" TextMode="MultiLine">
            </asp:TextBox>
        </div>
    </div>
    </form>
    <div class="WarningLabelStyle" id="CloseWarningLabel">
        WARNING: &nbsp;Modifying the closed date could remove your permission to edit this loan.
    </div>
    <uc1:cModalDlg ID="CModalDlg1" runat="server"></uc1:cModalDlg>
</form>
</body>
</html>
