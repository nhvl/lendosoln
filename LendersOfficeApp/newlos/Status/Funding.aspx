<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="Funding.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Status.Funding" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
    <title>Funding</title>
  </head>
  <body MS_POSITIONING="FlowLayout" class="RightBackground" style="MARGIN-LEFT:0px">
	<script language=javascript>
<!--
function f_onFundingSourceChange(o) {
  if (o.id == <%=AspxTools.JsGetClientIdString(sInternalFundingSrcDesc) %> && o.value != " ")
    <%= AspxTools.JsGetElementById(sExternalFundingSrcDesc) %>.value = "";
  else if (o.id == <%= AspxTools.JsGetClientIdString(sExternalFundingSrcDesc) %> && o.value != " ")
    <%= AspxTools.JsGetElementById(sInternalFundingSrcDesc) %>.value = "";
}
//-->
</script>

    <form id="Funding" method="post" runat="server">
    <table>
  <tr>
    <td class=MainRightHeader nowrap >Funding</td></tr>
    <tr>
    <td>
<table id=Table1 cellspacing=0 cellpadding=0 border=0 class=InsetBorder>
        <tr>

    <td class=FieldLabel style="PADDING-LEFT: 5px" 
      nowrap>Internal Funding Source</td>
    <td 
  nowrap><ml:ComboBox id=sInternalFundingSrcDesc runat="server" iseditable="False" onchange="f_onFundingSourceChange(this);" width="300px"></ml:ComboBox></td></tr>
  <tr>
    <td class=FieldLabel style="PADDING-LEFT: 5px" nowrap>External/Wholesale Funding 
      Source&nbsp;&nbsp;&nbsp;</td>
    <td nowrap><ml:ComboBox id=sExternalFundingSrcDesc runat="server" IsEditable="False" onchange="f_onFundingSourceChange(this);" width="300px"></ml:ComboBox></td></tr>
        <tr>
          <td class=FieldLabel style="PADDING-LEFT: 5px" nowrap>&nbsp;</td>
          <td nowrap></td></tr>
  <tr>
    <td class=FieldLabel style="PADDING-LEFT: 5px" nowrap> Warehouse 
      Funder</td>
    <td nowrap><ml:ComboBox id=sWarehouseFunderDesc runat="server" IsEditable="False" Width="300px"></ml:ComboBox></td></tr>
  <tr>
    <td class=FieldLabel style="PADDING-LEFT: 5px" nowrap><asp:CheckBox id=sIsTexasHomeEquity runat="server" Text="Is Texas Home Equity"></asp:CheckBox></td>
    <td nowrap></td></tr>
  <tr>
    <td class=FieldLabel style="PADDING-LEFT: 5px" nowrap><asp:CheckBox id=sIsDocDrawExternal runat="server" Text="Is External Doc Draw"></asp:CheckBox></td>
    <td nowrap></td></tr></table></td></tr></table>
     </form>
	
  </body>
</html>
