<%@ Page language="c#" Codebehind="CustomFields.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Status.CustomFields" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head runat="server">
    <title>CustomFields</title>
  </head>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro">
	
    <form id="CustomFields" method="post" runat="server">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
		<td nowrap class="MainRightHeader">Custom Fields</td>
    </tr>
    <tr>
		<td>
		
			<table class="FormTable" cellSpacing="0" cellpadding="2" border="1" rules="all" style="border-collapse:collapse;">
			<tr class="GridHeader">
				<td></td>
				<td style="white-space: nowrap">Item Description</td>
				<td>Date</td>
				<td>Amount</td>
				<td>Percentage</td>
				<td>Checked?</td>
				<td>Notes</td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel"  style="white-space: nowrap">Field Set 1</td>
				<td style="white-space: nowrap"><asp:TextBox id="sCustomField1Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td style="white-space: nowrap"><ml:DateTextBox id="sCustomField1D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td style="white-space: nowrap"><ml:MoneyTextBox id="sCustomField1Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td style="white-space: nowrap"><ml:percenttextbox id="sCustomField1Pc" runat="server" width="70" preset="percent" /></td>
				<td style="white-space: nowrap"><asp:CheckBox id="sCustomField1Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField1Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 2</td>
				<td><asp:TextBox id="sCustomField2Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField2D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField2Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField2Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField2Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField2Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 3</td>
				<td><asp:TextBox id="sCustomField3Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField3D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField3Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField3Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField3Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField3Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 4</td>
				<td><asp:TextBox id="sCustomField4Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField4D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField4Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField4Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField4Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField4Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 5</td>
				<td><asp:TextBox id="sCustomField5Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField5D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField5Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField5Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField5Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField5Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 6</td>
				<td><asp:TextBox id="sCustomField6Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField6D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField6Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField6Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField6Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField6Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 7</td>
				<td><asp:TextBox id="sCustomField7Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField7D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField7Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField7Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField7Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField7Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 8</td>
				<td><asp:TextBox id="sCustomField8Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField8D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField8Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField8Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField8Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField8Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 9</td>
				<td><asp:TextBox id="sCustomField9Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField9D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField9Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField9Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField9Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField9Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel" style="white-space: nowrap">Field Set 10</td>
				<td><asp:TextBox id="sCustomField10Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField10D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField10Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField10Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField10Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField10Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 11</td>
				<td><asp:TextBox id="sCustomField11Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField11D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField11Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField11Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField11Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField11Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 12</td>
				<td><asp:TextBox id="sCustomField12Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField12D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField12Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField12Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField12Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField12Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 13</td>
				<td><asp:TextBox id="sCustomField13Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField13D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField13Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField13Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField13Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField13Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 14</td>
				<td><asp:TextBox id="sCustomField14Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField14D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField14Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField14Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField14Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField14Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 15</td>
				<td><asp:TextBox id="sCustomField15Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField15D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField15Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField15Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField15Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField15Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 16</td>
				<td><asp:TextBox id="sCustomField16Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField16D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField16Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField16Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField16Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField16Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 17</td>
				<td><asp:TextBox id="sCustomField17Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField17D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField17Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField17Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField17Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField17Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 18</td>
				<td><asp:TextBox id="sCustomField18Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField18D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField18Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField18Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField18Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField18Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 19</td>
				<td><asp:TextBox id="sCustomField19Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField19D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField19Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField19Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField19Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField19Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 20</td>
				<td><asp:TextBox id="sCustomField20Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField20D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField20Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField20Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField20Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField20Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
    
    

     </form>
	
  </body>
</html>
