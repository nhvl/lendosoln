﻿namespace LendersOfficeApp.newlos.Status
{
    using System;
    using DataAccess;
    using DataAccess.HomeownerCounselingOrganizations;
    using DataAccess.CounselingOrganization;
    using LendersOffice.Common;

    public class HomeownerCounselingOrganizationListServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sHomeownerCounselingOrganizationLastModifiedD_rep = GetString("sHomeownerCounselingOrganizationLastModifiedD");
            //Using separate containers due to concern on the capacity of input[type="hidden"].value
            var org0 = GetString("Org0InfoHolder");
            var org1 = GetString("Org1InfoHolder");
            var org2 = GetString("Org2InfoHolder");
            var org3 = GetString("Org3InfoHolder");
            var org4 = GetString("Org4InfoHolder");
            var org5 = GetString("Org5InfoHolder");
            var org6 = GetString("Org6InfoHolder");
            var org7 = GetString("Org7InfoHolder");
            var org8 = GetString("Org8InfoHolder");
            var org9 = GetString("Org9InfoHolder");

            HomeownerCounselingOrganizationCollection orgList = new HomeownerCounselingOrganizationCollection();

            orgList.Add(ObsoleteSerializationHelper.JavascriptJsonDeserializer<HomeownerCounselingOrganization>(org0));
            orgList.Add(ObsoleteSerializationHelper.JavascriptJsonDeserializer<HomeownerCounselingOrganization>(org1));
            orgList.Add(ObsoleteSerializationHelper.JavascriptJsonDeserializer<HomeownerCounselingOrganization>(org2));
            orgList.Add(ObsoleteSerializationHelper.JavascriptJsonDeserializer<HomeownerCounselingOrganization>(org3));
            orgList.Add(ObsoleteSerializationHelper.JavascriptJsonDeserializer<HomeownerCounselingOrganization>(org4));
            orgList.Add(ObsoleteSerializationHelper.JavascriptJsonDeserializer<HomeownerCounselingOrganization>(org5));
            orgList.Add(ObsoleteSerializationHelper.JavascriptJsonDeserializer<HomeownerCounselingOrganization>(org6));
            orgList.Add(ObsoleteSerializationHelper.JavascriptJsonDeserializer<HomeownerCounselingOrganization>(org7));
            orgList.Add(ObsoleteSerializationHelper.JavascriptJsonDeserializer<HomeownerCounselingOrganization>(org8));
            orgList.Add(ObsoleteSerializationHelper.JavascriptJsonDeserializer<HomeownerCounselingOrganization>(org9));

            dataLoan.sHomeownerCounselingOrganizationCollection = orgList;
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            this.SetResult(nameof(dataLoan.sHasCompletedHomeownerCounselingOrganizationList), dataLoan.sHasCompletedHomeownerCounselingOrganizationList);
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "FetchCounselingData":
                    FetchCounselingData();
                    break;
            }
        }

        private void FetchCounselingData()
        {
            string zipcode = GetString("aBZip");

            var cfpbData = this.FetchDataFromCFPB(zipcode);
            this.SetResult("CFPB_data", cfpbData);
            this.SetResult("IsDataComplete", Tools.IsHomeownerCounselingOrganizationListComplete(cfpbData));
        }

        private string FetchDataFromCFPB(string zipcode)
        {
            string cfpbData;
            string errors;
            if (CounselingOrganizationUtilities.FetchDataFromCFPB(zipcode, out errors, out cfpbData))
            {
                return cfpbData;
            }

            return this.CreateServerError(errors);
        }
        private string CreateServerError(string errorMessage)
        {
            return "{\"server_error\":\"" + errorMessage + "\"}";
        }

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(HomeownerCounselingOrganizationListServiceItem));
        }
    }
    public partial class HomeownerCounselingOrganizationListService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new HomeownerCounselingOrganizationListServiceItem());
        }
    }
}