﻿#region Generated Code
namespace LendersOfficeApp.newlos.Status
#endregion
{
    using System;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.StatusEvents;
    using LendersOffice.Security;
    using DataAccess;

    /// <summary>
    /// The class for the StatusEvents page.  Handles the initialization.
    /// </summary>
    public partial class StatusEvents : BaseLoanPage
    {        
        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">The object that triggered the postback, if any.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageID = "StatusEvents";

            this.EnableJqueryMigrate = false;
            this.m_loadDefaultStylesheet = true;
            this.RegisterJsScript("angular-1.5.5.min.js");

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(StatusEvents));
            dataLoan.InitLoad();

            MigrationBtn.Visible = !(dataLoan.sStatusEventMigrationVersion == StatusEventMigrationVersion.MigrationWithUserName);
            RegisterJsGlobalVariables("IsStatusEventMigratedWithUserName", dataLoan.sStatusEventMigrationVersion == StatusEventMigrationVersion.MigrationWithUserName);

            RegisterJsGlobalVariables("CanExclude", PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowQualityControlAccess));
            RegisterJsGlobalVariables("StatusEventJson", SerializationHelper.JsonNetSerialize(StatusEvent.RetrieveAllLoanEvents(LoanID, PrincipalFactory.CurrentPrincipal.BrokerId)));
            RegisterService("main", "/newlos/status/StatusEventsService.aspx");
        }
    }
}
