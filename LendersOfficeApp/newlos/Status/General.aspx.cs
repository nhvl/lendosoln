namespace LendersOfficeApp.newlos.Status
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using CommonProjectLib.Common.Lib;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.DocMagicLib;
    using LendersOffice.ObjLib.StatusEvents;
    using LendersOfficeApp.los.admin;
    using LendersOfficeApp.newlos.Disclosure;
    using MeridianLink.CommonControls;

    public partial class General : BaseLoanPage
	{
        private CPageData m_dataLoan = null;
        private CPageData DataLoan
        {
            get
            {
                if (m_dataLoan == null)
                {
                    m_dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(General));
                }

                return m_dataLoan;
            }

            set
            {
                m_dataLoan = value;
            }
        }

        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new[] { WorkflowOperations.ClearRespaFirstEnteredDates };
        }

        /// <summary>
        /// Indicates whether DocMagic is enabled on this account.
        /// </summary>
        public bool IsDocMagicEnabled
        {
            get
            {
                return DocumentVendorBrokerSettings.GetDocMagicSettings(Broker.BrokerID).BrokerId != Guid.Empty;
            }
        }

        internal static void BindDataFromControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem serviceItem)
        {
            CreditReportAuthorization.BindDataToLoan(loan, application, serviceItem, nameof(CreditReportAuthorizationCtrl) + "_");
            RespaDates.BindData(loan, serviceItem, nameof(RespaDates));
        }

        internal static void LoadDataForControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem serviceItem)
        {
            CreditReportAuthorization.LoadDataFromLoan(loan, application, serviceItem, nameof(CreditReportAuthorizationCtrl) + "_");
            RespaDates.LoadData(loan, serviceItem, nameof(RespaDates));
        }

        private void BindDataObject(CPageData dataLoan) 
        {
            TILGFERow.Style.Add("display", dataLoan.sIsInTRID2015Mode ? "none" : "");
            HUD1SettlementRow.Style.Add("display", dataLoan.sIsInTRID2015Mode ? "none" : "");

            // If you're adding fields to the General UI, please ask the PDEs if the changes should be mirrored
            // in the Processing UI. Thanks.

            bool hasPricingEngine = BrokerUser.HasFeatures(E_BrokerFeatureT.PricingEngine);

            CAppData dataApp = null;
            if (ApplicationID == Guid.Empty)
                dataApp = dataLoan.GetAppData(0);
            else
                dataApp = dataLoan.GetAppData( ApplicationID );

            sStatusT.Value			= dataLoan.sStatusT.ToString("D");
            sStatus.Text			= dataLoan.sStatusT_rep;
                
            sRLckdD.Text			= dataLoan.sRLckdD_rep;
            sRLckdDTime.Text = dataLoan.sRLckdDTime_rep;
            sRLckdD.ReadOnly		= dataLoan.sIsRateLocked || IsReadOnly || hasPricingEngine || Broker.HasLenderDefaultFeatures;
			sRLckdD.IsDisplayCalendarHelper = !sRLckdD.ReadOnly;
            sRLckdN.Text			= dataLoan.sRLckdN;
            sRLckdExpiredD.Text		= dataLoan.sRLckdExpiredD_rep;
            sRLckdExpiredD.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly || hasPricingEngine || Broker.HasLenderDefaultFeatures;;
			sRLckdExpiredD.IsDisplayCalendarHelper = !sRLckdExpiredD.ReadOnly;
            sRLckdExpiredN.Text		= dataLoan.sRLckdExpiredN;

            sAppSubmittedD.Text = dataLoan.sAppSubmittedD_rep;
            sAppSubmittedDLckd.Checked = dataLoan.sAppSubmittedDLckd;
            sAppReceivedByLenderD.Text = dataLoan.sAppReceivedByLenderD_rep;
            sAppReceivedByLenderDLckd.Checked = dataLoan.sAppReceivedByLenderDLckd;

            sLeadD.Text = dataLoan.sLeadD_rep;
            sLeadDTime.Text = dataLoan.sLeadDTime_rep;
            sLeadN.Text = dataLoan.sLeadN;

            sOpenedD.Text = dataLoan.sOpenedD_rep;
            sOpenedDTime.Text = dataLoan.sOpenedDTime_rep;
            sOpenedN.Text = dataLoan.sOpenedN;

            sPreQualD.Text = dataLoan.sPreQualD_rep;
            sPreQualDTime.Text = dataLoan.sPreQualDTime_rep;
            sPreQualN.Text = dataLoan.sPreQualN;

            sSubmitD.Text = dataLoan.sSubmitD_rep; // registered
            sSubmitDTime.Text = dataLoan.sSubmitDTime_rep;
            sSubmitD.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sSubmitD.IsDisplayCalendarHelper = !sSubmitD.ReadOnly;
            sSubmitN.Text = dataLoan.sSubmitN;

            sProcessingD.Text = dataLoan.sProcessingD_rep;
            sProcessingDTime.Text = dataLoan.sProcessingDTime_rep;
            sProcessingN.Text = dataLoan.sProcessingN;

            sPreApprovD.Text = dataLoan.sPreApprovD_rep;
            sPreApprovDTime.Text = dataLoan.sPreApprovDTime_rep;
            sPreApprovN.Text = dataLoan.sPreApprovN;

            sEstCloseD.Text = dataLoan.sEstCloseD_rep;
            sEstCloseDLckd.Checked = dataLoan.sEstCloseDLckd;
            sEstCloseN.Text = dataLoan.sEstCloseN;

            sLoanSubmittedD.Text = dataLoan.sLoanSubmittedD_rep;
            sLoanSubmittedDTime.Text = dataLoan.sLoanSubmittedDTime_rep;
            sLoanSubmittedN.Text = dataLoan.sLoanSubmittedN;

            // 2/2/2005 dd - Added for Unprime
            sUnderwritingD.Text = dataLoan.sUnderwritingD_rep;
            sUnderwritingDTime.Text = dataLoan.sUnderwritingDTime_rep;
            sUnderwritingN.Text = dataLoan.sUnderwritingN;

            sApprovD.Text = dataLoan.sApprovD_rep;
            sApprovDTime.Text = dataLoan.sApprovDTime_rep;
            sApprovN.Text = dataLoan.sApprovN;

            sFinalUnderwritingD.Text = dataLoan.sFinalUnderwritingD_rep;
            sFinalUnderwritingDTime.Text = dataLoan.sFinalUnderwritingDTime_rep;
            sFinalUnderwritingN.Text = dataLoan.sFinalUnderwritingN;

            sClearToCloseD.Text = dataLoan.sClearToCloseD_rep; // 7/22/2009 dd - OPM 5663
            sClearToCloseDTime.Text = dataLoan.sClearToCloseDTime_rep;
            sClearToCloseN.Text = dataLoan.sClearToCloseN; // 7/22/2009 dd - OPM 5663

            sDocsD.Text = dataLoan.sDocsD_rep;
            sDocsDTime.Text = dataLoan.sDocsDTime_rep;
            sDocsN.Text = dataLoan.sDocsN;

            sDocsBackD.Text = dataLoan.sDocsBackD_rep;
            sDocsBackDTime.Text = dataLoan.sDocsBackDTime_rep;
            sDocsBackN.Text = dataLoan.sDocsBackN;

            sSchedFundD.Text = dataLoan.sSchedFundD_rep;
            sSchedFundDTime.Text = dataLoan.sSchedFundDTime_rep;
            sSchedFundN.Text = dataLoan.sSchedFundN;

            sFundingConditionsD.Text = dataLoan.sFundingConditionsD_rep;
            sFundingConditionsDTime.Text = dataLoan.sFundingConditionsDTime_rep;
            sFundingConditionsN.Text = dataLoan.sFundingConditionsN;

            sFundD.Text = dataLoan.sFundD_rep;
            sFundDTime.Text = dataLoan.sFundDTime_rep;
            sFundN.Text = dataLoan.sFundN;

            sRecordedD.Text = dataLoan.sRecordedD_rep;
            sRecordedDTime.Text = dataLoan.sRecordedDTime_rep;
            sRecordedN.Text = dataLoan.sRecordedN;

            sFinalDocsD.Text = dataLoan.sFinalDocsD_rep;
            sFinalDocsDTime.Text = dataLoan.sFinalDocsDTime_rep;
            sFinalDocsN.Text = dataLoan.sFinalDocsN;

            sClosedD.Text = dataLoan.sClosedD_rep;
            sClosedDTime.Text = dataLoan.sClosedDTime_rep;
            sClosedN.Text = dataLoan.sClosedN;


            sOnHoldD.Text			= dataLoan.sOnHoldD_rep;
            sOnHoldDTime.Text = dataLoan.sOnHoldDTime_rep;
            sOnHoldN.Text			= dataLoan.sOnHoldN;

            sCanceledD.Text			= dataLoan.sCanceledD_rep;
            sCanceledDTime.Text = dataLoan.sCanceledDTime_rep;
            sCanceledN.Text			= dataLoan.sCanceledN;

            sRejectD.Text			= dataLoan.sRejectD_rep;
            sRejectDTime.Text = dataLoan.sRejectDTime_rep;
            sRejectN.Text			= dataLoan.sRejectN;

            sSuspendedD.Text		= dataLoan.sSuspendedD_rep;
            sSuspendedDTime.Text = dataLoan.sSuspendedDTime_rep;
            sSuspendedN.Text		= dataLoan.sSuspendedN;


            sPreProcessingD.Text = dataLoan.sPreProcessingD_rep;
            sPreProcessingDTime.Text = dataLoan.sPreProcessingDTime_rep;
            sPreProcessingN.Text = dataLoan.sPreProcessingN;

            sDocumentCheckD.Text = dataLoan.sDocumentCheckD_rep;
            sDocumentCheckDTime.Text = dataLoan.sDocumentCheckDTime_rep;
            sDocumentCheckN.Text = dataLoan.sDocumentCheckN;

            sDocumentCheckFailedD.Text = dataLoan.sDocumentCheckFailedD_rep;
            sDocumentCheckFailedDTime.Text = dataLoan.sDocumentCheckFailedDTime_rep;
            sDocumentCheckFailedN.Text = dataLoan.sDocumentCheckFailedN;

            sPreUnderwritingD.Text = dataLoan.sPreUnderwritingD_rep;
            sPreUnderwritingDTime.Text = dataLoan.sPreUnderwritingDTime_rep;
            sPreUnderwritingN.Text = dataLoan.sPreUnderwritingN;

            sConditionReviewD.Text = dataLoan.sConditionReviewD_rep;
            sConditionReviewDTime.Text = dataLoan.sConditionReviewDTime_rep;
            sConditionReviewN.Text = dataLoan.sConditionReviewN;

            sPreDocQCD.Text = dataLoan.sPreDocQCD_rep;
            sPreDocQCDTime.Text = dataLoan.sPreDocQCDTime_rep;
            sPreDocQCN.Text = dataLoan.sPreDocQCN;

            sDocsOrderedD.Text = dataLoan.sDocsOrderedD_rep;
            sDocsOrderedDTime.Text = dataLoan.sDocsOrderedDTime_rep;
            sDocsOrderedN.Text = dataLoan.sDocsOrderedN;

            sDocsDrawnD.Text = dataLoan.sDocsDrawnD_rep;
            sDocsDrawnDTime.Text = dataLoan.sDocsDrawnDTime_rep;
            sDocsDrawnN.Text = dataLoan.sDocsDrawnN;

            sReadyForSaleD.Text = dataLoan.sReadyForSaleD_rep;
            sReadyForSaleDTime.Text = dataLoan.sReadyForSaleDTime_rep;
            sReadyForSaleN.Text = dataLoan.sReadyForSaleN;

            sSubmittedForPurchaseReviewD.Text = dataLoan.sSubmittedForPurchaseReviewD_rep;
            sSubmittedForPurchaseReviewDTime.Text = dataLoan.sSubmittedForPurchaseReviewDTime_rep;
            sSubmittedForPurchaseReviewN.Text = dataLoan.sSubmittedForPurchaseReviewN;

            sInPurchaseReviewD.Text = dataLoan.sInPurchaseReviewD_rep;
            sInPurchaseReviewDTime.Text = dataLoan.sInPurchaseReviewDTime_rep;
            sInPurchaseReviewN.Text = dataLoan.sInPurchaseReviewN;

            sPrePurchaseConditionsD.Text = dataLoan.sPrePurchaseConditionsD_rep;
            sPrePurchaseConditionsDTime.Text = dataLoan.sPrePurchaseConditionsDTime_rep;
            sPrePurchaseConditionsN.Text = dataLoan.sPrePurchaseConditionsN;

            sSubmittedForFinalPurchaseD.Text = dataLoan.sSubmittedForFinalPurchaseD_rep;
            sSubmittedForFinalPurchaseDTime.Text = dataLoan.sSubmittedForFinalPurchaseDTime_rep;
            sSubmittedForFinalPurchaseN.Text = dataLoan.sSubmittedForFinalPurchaseN;

            sInFinalPurchaseReviewD.Text = dataLoan.sInFinalPurchaseReviewD_rep;
            sInFinalPurchaseReviewDTime.Text = dataLoan.sInFinalPurchaseReviewDTime_rep;
            sInFinalPurchaseReviewN.Text = dataLoan.sInFinalPurchaseReviewN;

            sClearToPurchaseD.Text = dataLoan.sClearToPurchaseD_rep;
            sClearToPurchaseDTime.Text = dataLoan.sClearToPurchaseDTime_rep;
            sClearToPurchaseN.Text = dataLoan.sClearToPurchaseN;

            sPurchasedD.Text = dataLoan.sPurchasedD_rep;
            sPurchasedDTime.Text = dataLoan.sPurchasedDTime_rep;
            sPurchasedN.Text = dataLoan.sPurchasedN;

            sCounterOfferD.Text = dataLoan.sCounterOfferD_rep;
            sCounterOfferDTime.Text = dataLoan.sCounterOfferDTime_rep;
            sCounterOfferN.Text = dataLoan.sCounterOfferN;

            sWithdrawnD.Text = dataLoan.sWithdrawnD_rep;
            sWithdrawnDTime.Text = dataLoan.sWithdrawnDTime_rep;
            sWithdrawnN.Text = dataLoan.sWithdrawnN;

            sArchivedD.Text = dataLoan.sArchivedD_rep;
            sArchivedDTime.Text = dataLoan.sArchivedDTime_rep;
            sArchivedN.Text = dataLoan.sArchivedN;

            
            
            
            sU1LStatDesc.Text		= dataLoan.sU1LStatDesc;
            UpdateTextBox("sU1LStatDesc", sU1LStatDesc, dataLoan);
            sU1LStatD.Text			= dataLoan.sU1LStatD_rep;
            sU1LStatN.Text			= dataLoan.sU1LStatN;
            sU2LStatDesc.Text		= dataLoan.sU2LStatDesc;
            UpdateTextBox("sU2LStatDesc", sU2LStatDesc, dataLoan);
            sU2LStatD.Text			= dataLoan.sU2LStatD_rep;
            sU2LStatN.Text			= dataLoan.sU2LStatN;
            sU3LStatDesc.Text		= dataLoan.sU3LStatDesc;
            UpdateTextBox("sU3LStatDesc", sU3LStatDesc, dataLoan);
            sU3LStatD.Text			= dataLoan.sU3LStatD_rep;
            sU3LStatN.Text			= dataLoan.sU3LStatN;
            UpdateTextBox("sU4LStatDesc", sU4LStatDesc, dataLoan);
            sU4LStatDesc.Text		= dataLoan.sU4LStatDesc;
            sU4LStatD.Text			= dataLoan.sU4LStatD_rep;
            sU4LStatN.Text			= dataLoan.sU4LStatN;
            sPrelimRprtOd.Text		= dataLoan.sPrelimRprtOd_rep;
            sPrelimRprtRd.Text		= dataLoan.sPrelimRprtRd_rep;
            sPrelimRprtN.Text		= dataLoan.sPrelimRprtN;
            
            sClosingServOd.Text     = dataLoan.sClosingServOd_rep;
            sClosingServDueD.Text   = dataLoan.sClosingServDueD_rep;
            sClosingServDocumentD.Text = dataLoan.sClosingServDocumentD_rep;
            sClosingServRd.Text     = dataLoan.sClosingServRd_rep;
            sClosingServN.Text      = dataLoan.sClosingServN;
            
            sApprRprtOd.Text		= dataLoan.sApprRprtOd_rep;
            sApprRprtRd.Text		= dataLoan.sApprRprtRd_rep;
            sApprRprtN.Text         = dataLoan.sApprRprtN;
            sDocumentNoteD.Text     = dataLoan.sDocumentNoteD_rep;
            sTilGfeOd.Text			= dataLoan.sTilGfeOd_rep;
            sTilGfeRd.Text			= dataLoan.sTilGfeRd_rep;
            sTilGfeN.Text			= dataLoan.sTilGfeN;

            sFloodCertOd.Text = dataLoan.sFloodCertOd_rep;
            sFloodCertDueD.Text = dataLoan.sFloodCertDueD_rep;
            sFloodCertificationDeterminationD.Text = dataLoan.sFloodCertificationDeterminationD_rep;
            sFloodCertRd.Text = dataLoan.sFloodCertRd_rep;
            sFloodCertN.Text = dataLoan.sFloodCertN;

            sUSPSCheckOd.Text = dataLoan.sUSPSCheckOd_rep;
            sUSPSCheckDueD.Text = dataLoan.sUSPSCheckDueD_rep;
            sUSPSCheckDocumentD.Text = dataLoan.sUSPSCheckDocumentD_rep;
            sUSPSCheckRd.Text = dataLoan.sUSPSCheckRd_rep;
            sUSPSCheckN.Text = dataLoan.sUSPSCheckN;

            sPayDemStmntOd.Text = dataLoan.sPayDemStmntOd_rep;
            sPayDemStmntDueD.Text = dataLoan.sPayDemStmntDueD_rep;
            sPayDemStmntDocumentD.Text = dataLoan.sPayDemStmntDocumentD_rep;
            sPayDemStmntRd.Text = dataLoan.sPayDemStmntRd_rep;
            sPayDemStmntN.Text = dataLoan.sPayDemStmntN;

            sCAIVRSOd.Text = dataLoan.sCAIVRSOd_rep;
            sCAIVRSDueD.Text = dataLoan.sCAIVRSDueD_rep;
            sCAIVRSDocumentD.Text = dataLoan.sCAIVRSDocumentD_rep;
            sCAIVRSRd.Text = dataLoan.sCAIVRSRd_rep;
            sCAIVRSN.Text = dataLoan.sCAIVRSN;

            sFraudServOd.Text = dataLoan.sFraudServOd_rep;
            sFraudServDueD.Text = dataLoan.sFraudServDueD_rep;
            sFraudServDocumentD.Text = dataLoan.sFraudServDocumentD_rep;
            sFraudServRd.Text = dataLoan.sFraudServRd_rep;
            sFraudServN.Text = dataLoan.sFraudServN;

            sAVMOd.Text = dataLoan.sAVMOd_rep;
            sAVMDueD.Text = dataLoan.sAVMDueD_rep;
            sAVMDocumentD.Text = dataLoan.sAVMDocumentD_rep;
            sAVMRd.Text = dataLoan.sAVMRd_rep;
            sAVMN.Text = dataLoan.sAVMN;

            sHOACertOd.Text = dataLoan.sHOACertOd_rep;
            sHOACertDueD.Text = dataLoan.sHOACertDueD_rep;
            sHOACertDocumentD.Text = dataLoan.sHOACertDocumentD_rep;
            sHOACertRd.Text = dataLoan.sHOACertRd_rep;
            sHOACertN.Text = dataLoan.sHOACertN;

            sEstHUDOd.Text = dataLoan.sEstHUDOd_rep;
            sEstHUDDueD.Text = dataLoan.sEstHUDDueD_rep;
            sEstHUDDocumentD.Text = dataLoan.sEstHUDDocumentD_rep;
            sEstHUDRd.Text = dataLoan.sEstHUDRd_rep;
            sEstHUDN.Text = dataLoan.sEstHUDN;

            sCPLAndICLOd.Text = dataLoan.sCPLAndICLOd_rep;
            sCPLAndICLDueD.Text = dataLoan.sCPLAndICLDueD_rep;
            sCPLAndICLDocumentD.Text = dataLoan.sCPLAndICLDocumentD_rep;
            sCPLAndICLRd.Text = dataLoan.sCPLAndICLRd_rep;
            sCPLAndICLN.Text = dataLoan.sCPLAndICLN;

            sWireInstructOd.Text = dataLoan.sWireInstructOd_rep;
            sWireInstructDueD.Text = dataLoan.sWireInstructDueD_rep;
            sWireInstructDocumentD.Text = dataLoan.sWireInstructDocumentD_rep;
            sWireInstructRd.Text = dataLoan.sWireInstructRd_rep;
            sWireInstructN.Text = dataLoan.sWireInstructN;

            sInsurancesOd.Text = dataLoan.sInsurancesOd_rep;
            sInsurancesDueD.Text = dataLoan.sInsurancesDueD_rep;
            sInsurancesDocumentD.Text = dataLoan.sInsurancesDocumentD_rep;
            sInsurancesRd.Text = dataLoan.sInsurancesRd_rep;
            sInsurancesN.Text = dataLoan.sInsurancesN;

            sU1DocStatDesc.Text		= dataLoan.sU1DocStatDesc;
            sU1DocStatOd.Text		= dataLoan.sU1DocStatOd_rep;
            sU1DocStatRd.Text		= dataLoan.sU1DocStatRd_rep;
            sU1DocStatN.Text		= dataLoan.sU1DocStatN;
            sU2DocStatDesc.Text		= dataLoan.sU2DocStatDesc;
            sU2DocStatOd.Text		= dataLoan.sU2DocStatOd_rep;
            sU2DocStatRd.Text		= dataLoan.sU2DocStatRd_rep;
            sU2DocStatN.Text		= dataLoan.sU2DocStatN;
            sU3DocStatDesc.Text		= dataLoan.sU3DocStatDesc;
            sU3DocStatOd.Text		= dataLoan.sU3DocStatOd_rep;
            sU3DocStatRd.Text		= dataLoan.sU3DocStatRd_rep;
            sU3DocStatN.Text		= dataLoan.sU3DocStatN;
            sU4DocStatDesc.Text		= dataLoan.sU4DocStatDesc;
            sU4DocStatOd.Text		= dataLoan.sU4DocStatOd_rep;
            sU4DocStatRd.Text		= dataLoan.sU4DocStatRd_rep;
            sU4DocStatN.Text		= dataLoan.sU4DocStatN;
            sU5DocStatDesc.Text		= dataLoan.sU5DocStatDesc;
            sU5DocStatOd.Text		= dataLoan.sU5DocStatOd_rep;
            sU5DocStatRd.Text		= dataLoan.sU5DocStatRd_rep;
            sU5DocStatN.Text		= dataLoan.sU5DocStatN;
            sTrNotes.Text			= dataLoan.sTrNotes	;
            sApprRprtN.Text			= dataLoan.sApprRprtN;
            sLenderCaseNum.Text		= dataLoan.sLenderCaseNum;
            sLenderCaseNumLckd.Checked = dataLoan.sLenderCaseNumLckd;
            sAgencyCaseNum.Text		= dataLoan.sAgencyCaseNum;
            sMersMin.Text           = dataLoan.sMersMin;
            sPrelimRprtDueD.Text	= dataLoan.sPrelimRprtDueD_rep;
            sApprRprtDueD.Text		= dataLoan.sApprRprtDueD_rep;
            sTilGfeDueD.Text		= dataLoan.sTilGfeDueD_rep;
            sU1DocStatDueD.Text		= dataLoan.sU1DocStatDueD_rep;
            sU2DocStatDueD.Text		= dataLoan.sU2DocStatDueD_rep;
            sU3DocStatDueD.Text		= dataLoan.sU3DocStatDueD_rep;
            sU4DocStatDueD.Text		= dataLoan.sU4DocStatDueD_rep;
            sU5DocStatDueD.Text		= dataLoan.sU5DocStatDueD_rep;
			sStatusLckd.Checked		= dataLoan.sStatusLckd;
            aCrDueD.Text			= dataApp.aCrDueD_rep;
            aBusCrDueD.Text			= dataApp.aBusCrDueD_rep;
            aU1DocStatDueD.Text		= dataApp.aU1DocStatDueD_rep;
            aU2DocStatDueD.Text		= dataApp.aU2DocStatDueD_rep;
            aU1DocStatDesc.Text		= dataApp.aU1DocStatDesc;
            aU1DocStatOd.Text		= dataApp.aU1DocStatOd_rep;
            aU1DocStatRd.Text		= dataApp.aU1DocStatRd_rep;
            aU1DocStatN.Text		= dataApp.aU1DocStatN;
            aU2DocStatDesc.Text		= dataApp.aU2DocStatDesc;
            aU2DocStatOd.Text		= dataApp.aU2DocStatOd_rep;
            aU2DocStatRd.Text		= dataApp.aU2DocStatRd_rep;
            aU2DocStatN.Text		= dataApp.aU2DocStatN;
            aUDNOrderedD.Text		= dataApp.aUDNOrderedD_rep;
            aUDNDeactivatedD.Text	= dataApp.aUDNDeactivatedD_rep;
            aUdnOrderId.Text        = dataApp.aUdnOrderId;
            aCrOd.Text				= dataApp.aCrOd_rep;
            aCrRd.Text				= dataApp.aCrRd_rep;
            aCrN.Text				= dataApp.aCrN;
            aBusCrOd.Text			= dataApp.aBusCrOd_rep;
            aBusCrRd.Text			= dataApp.aBusCrRd_rep;
            aBusCrN.Text			= dataApp.aBusCrN;
            aBNm.Text				= dataApp.aBNm;
            aCNm.Text               = dataApp.aCNm;
            aLqiCrOd.Text           = dataApp.aLqiCrOd_rep;
            aLqiCrDueD.Text         = dataApp.aLqiCrDueD_rep;
            aLqiCrRd.Text           = dataApp.aLqiCrRd_rep;
            aLqiCrN.Text            = dataApp.aLqiCrN;

            Tools.SetDropDownListValue(aBPreFundVoeTypeT, dataApp.aBPreFundVoeTypeT);
            aBPreFundVoeOd.Text = dataApp.aBPreFundVoeOd_rep;
            aBPreFundVoeDueD.Text = dataApp.aBPreFundVoeDueD_rep;
            aBPreFundVoeRd.Text = dataApp.aBPreFundVoeRd_rep;
            aBPreFundVoeN.Text = dataApp.aBPreFundVoeN;

            Tools.SetDropDownListValue(aCPreFundVoeTypeT, dataApp.aCPreFundVoeTypeT);
            aCPreFundVoeOd.Text = dataApp.aCPreFundVoeOd_rep;
            aCPreFundVoeDueD.Text = dataApp.aCPreFundVoeDueD_rep;
            aCPreFundVoeRd.Text = dataApp.aCPreFundVoeRd_rep;
            aCPreFundVoeN.Text = dataApp.aCPreFundVoeN;

            sLPurchasedN.Text = dataLoan.sLPurchasedN;

            sDocMagicApplicationD.Text = dataLoan.sDocMagicApplicationD_rep;
            sDocMagicApplicationDLckd.Checked = dataLoan.sDocMagicApplicationDLckd;
            sDocMagicGFED.Text = dataLoan.sDocMagicGFED_rep;
            sDocMagicGFEDLckd.Checked = dataLoan.sDocMagicGFEDLckd;
            sDocMagicEstAvailableThroughD.Text = dataLoan.sDocMagicEstAvailableThroughD_rep;
            sDocMagicEstAvailableThroughDLckd.Checked = dataLoan.sDocMagicEstAvailableThroughDLckd;
            sDocMagicDocumentD.Text = dataLoan.sDocMagicDocumentD_rep;
            sDocMagicDocumentDLckd.Checked = dataLoan.sDocMagicDocumentDLckd;
            sDocMagicClosingD.Text = dataLoan.sDocMagicClosingD_rep;
            sDocMagicClosingDLckd.Checked = dataLoan.sDocMagicClosingDLckd;
            sDocMagicSigningD.Text = dataLoan.sDocMagicSigningD_rep;
            sDocMagicSigningDLckd.Checked = dataLoan.sDocMagicSigningDLckd;
            sDocMagicCancelD.Text = dataLoan.sDocMagicCancelD_rep;
            sDocMagicCancelDLckd.Checked = dataLoan.sDocMagicCancelDLckd;
            sDocMagicDisbursementD.Text = dataLoan.sDocMagicDisbursementD_rep;
            sDocMagicDisbursementDLckd.Checked = dataLoan.sDocMagicDisbursementDLckd;
            sDocExpirationD.Text = dataLoan.sDocExpirationD_rep;
            sDocExpirationDLckd.Checked = dataLoan.sDocExpirationDLckd;
            sDocMagicRateLockD.Text = dataLoan.sDocMagicRateLockD_rep;
            sDocMagicRateLockDLckd.Checked = dataLoan.sDocMagicRateLockDLckd;
            sDocMagicPreZSentD.Text = dataLoan.sDocMagicPreZSentD_rep;
            sDocMagicPreZSentDLckd.Checked = dataLoan.sDocMagicPreZSentDLckd;
            sDocMagicReDiscSendD.Text = dataLoan.sDocMagicReDiscSendD_rep;
            sDocMagicReDiscSendDLckd.Checked = dataLoan.sDocMagicReDiscSendDLckd;
            sDocMagicRedisclosureMethodTLckd.Checked = dataLoan.sDocMagicRedisclosureMethodTLckd;
            sDocMagicReDiscReceivedD.Text = dataLoan.sDocMagicReDiscReceivedD_rep;
            sDocMagicReDiscReceivedDLckd.Checked = dataLoan.sDocMagicReDiscReceivedDLckd;
            sHomeownerCounselingOrganizationDisclosureD.Text = dataLoan.sHomeownerCounselingOrganizationDisclosureD_rep;
            sDisclosuresMailedD.Text = dataLoan.sDisclosuresMailedD_rep;

            Tools.SetDropDownListValue(sHasESignedDocumentsT, dataLoan.sHasESignedDocumentsT);
            Tools.SetDropDownListValue(sLeadSrcDesc, dataLoan.sLeadSrcDesc);
            Tools.SetDropDownListValue(sDocMagicRedisclosureMethodT, dataLoan.sDocMagicRedisclosureMethodT);

            this.CreditReportAuthorizationCtrl.BindDataToControl(dataLoan, dataApp);

            // OPM 175355, 12/24/2015, ML
            sPurchaseContractDate.Text = dataLoan.sPurchaseContractDate_rep;
            sFinancingContingencyExpD.Text = dataLoan.sFinancingContingencyExpD_rep;
            sFinancingContingencyExtensionExpD.Text = dataLoan.sFinancingContingencyExtensionExpD_rep;

            if (sAlternateLender.Items.FindByValue(dataLoan.sDocMagicAlternateLenderId.ToString()) == null)
            {
                DocMagicAlternateLender altLender = new DocMagicAlternateLender(dataLoan.sBrokerId, dataLoan.sDocMagicAlternateLenderId);
                sAlternateLender.Items.Add(new ListItem(altLender.LenderName, altLender.DocMagicAlternateLenderId.ToString()));
            }
            Tools.SetDropDownListValue(sAlternateLender, dataLoan.sDocMagicAlternateLenderId.ToString());

            // 9/3/2013 AV - 137485 Investigate Lead Source update problem
            if (dataLoan.sLeadSrcDesc.Length > 0 && sLeadSrcDesc.Items.FindByText(dataLoan.sLeadSrcDesc) == null) 
            {
                // 7/15/2005 dd - If current description no longer in description, just add to the drop downlist instead of clear out.
                sLeadSrcDesc.Items.Add(new ListItem(dataLoan.sLeadSrcDesc, dataLoan.sLeadSrcDesc));
            }

            Tools.SetDropDownListValue(sLeadSrcDesc, dataLoan.sLeadSrcDesc);

            // Compute days since opened.

			DateTime dt = DateTime.Now;
            DateTime dt1 = DateTime.Now;
            try 
            {
                // Set dt to closing date if possible.

				if (sClosedD.Text.TrimWhitespaceAndBOM() != "") 
                {
                    dt = DateTime.Parse(sClosedD.Text);
                }
            } 
            catch {}
            try 
            {
                dt1 = DateTime.Parse(sOpenedD.Text);
            } 
            catch {}
            TimeSpan ts = dt.Subtract(dt1);
            daySinceOpened.Text = ts.Days.ToString();

            sBranchName.Text = dataLoan.BranchNm;
            sBranchCode.Text = dataLoan.BranchCode;    //opm 28432 fs 03/11/09

            if (IsDocMagicEnabled)
            {
                TransferToRow.Visible = true;

                if (!string.IsNullOrWhiteSpace(dataLoan.sDocMagicTransferToInvestorNm) &
                    !string.IsNullOrWhiteSpace(dataLoan.sDocMagicTransferToInvestorCode))
                {
                    TransferTo.Text = string.Format(
                        "{0} ({1})",
                        dataLoan.sDocMagicTransferToInvestorNm,
                        dataLoan.sDocMagicTransferToInvestorCode);
                }
            }
            else
            {
                TransferToRow.Visible = false;
            }

            // 12/9/2004 dd - Added for APB.
            sShippedToInvestorD.Text = dataLoan.sShippedToInvestorD_rep;
            sShippedToInvestorDTime.Text = dataLoan.sShippedToInvestorDTime_rep;
            sShippedToInvestorN.Text = dataLoan.sShippedToInvestorN;

            sSuspendedByInvestorD.Text = dataLoan.sSuspendedByInvestorD_rep; // opm 90006
            sSuspendedByInvestorDTime.Text = dataLoan.sSuspendedByInvestorDTime_rep;
            sSuspendedByInvestorN.Text = dataLoan.sSuspendedByInvestorN;

            sCondSentToInvestorD.Text = dataLoan.sCondSentToInvestorD_rep; // opm 90006
            sCondSentToInvestorDTime.Text = dataLoan.sCondSentToInvestorDTime_rep;
            sCondSentToInvestorN.Text = dataLoan.sCondSentToInvestorN;

            sAdditionalCondSentD.Text = dataLoan.sAdditionalCondSentD_rep; // opm 90006
            
            sLPurchaseD.Text = dataLoan.sLPurchaseD_rep;
            sLPurchaseDTime.Text = dataLoan.sLPurchaseDTime_rep;

            sQCCompDate.Text = dataLoan.sQCCompDate_rep;
            sQCCompDateTime.Text = dataLoan.sQCCompDateTime_rep;
            sCreditDecisionDLckd.Checked = dataLoan.sCreditDecisionDLckd;
            sCreditDecisionD.Text = dataLoan.sCreditDecisionD_rep;
            sCreditDecisionDTime.Text = dataLoan.sCreditDecisionDTime_rep;
            sGoodByLetterD.Text = dataLoan.sGoodByLetterD_rep;

			// 2/22/2007 mf - OPM 9545
			sServicingStartD.Text = dataLoan.sServicingStartD_rep;
            sCaseAssignmentD.Text = dataLoan.sCaseAssignmentD_rep;

            sBranchChannelT.Text = Tools.GetDescription(dataLoan.sBranchChannelT);

            // OPM 185961 - Remove Bulk and Mini-Bulk from dropdown if not enables
            if (!Broker.ExposeBulkMiniBulkCorrProcessTypeEnums)
            {
                if (dataLoan.sCorrespondentProcessT != E_sCorrespondentProcessT.Bulk)
                {
                    sCorrespondentProcessT.Items.Remove(sCorrespondentProcessT.Items.FindByValue(E_sCorrespondentProcessT.Bulk.ToString("D")));
                }

                if (dataLoan.sCorrespondentProcessT != E_sCorrespondentProcessT.MiniBulk)
                {
                    sCorrespondentProcessT.Items.Remove(sCorrespondentProcessT.Items.FindByValue(E_sCorrespondentProcessT.MiniBulk.ToString("D")));
                }
            }

            Tools.SetDropDownListValue(sCorrespondentProcessT, dataLoan.sCorrespondentProcessT);

            // 2/6/13 gf - OPM 80416
            sPrelimRprtDocumentD.Text = dataLoan.sPrelimRprtDocumentD_rep;
            sSpValuationEffectiveD.Text = dataLoan.sSpValuationEffectiveD_rep;
            //sAppSubmittedN.Text = dataLoan.sAppSubmittedN;
            sTilGfeDocumentD.Text = dataLoan.sTilGfeDocumentD_rep;
            sU1DocDocumentD.Text = dataLoan.sU1DocDocumentD_rep;
            sU2DocDocumentD.Text = dataLoan.sU2DocDocumentD_rep;
            sU3DocDocumentD.Text = dataLoan.sU3DocDocumentD_rep;
            sU4DocDocumentD.Text = dataLoan.sU4DocDocumentD_rep;
            sU5DocDocumentD.Text = dataLoan.sU5DocDocumentD_rep;

            sLoanPackageOrderedD.Text = dataLoan.sLoanPackageOrderedD_rep;
            sLoanPackageDocumentD.Text = dataLoan.sLoanPackageDocumentD_rep;
            sLoanPackageReceivedD.Text = dataLoan.sLoanPackageReceivedD_rep;
            sLoanPackageN.Text = dataLoan.sLoanPackageN;

            sMortgageDOTOrderedD.Text = dataLoan.sMortgageDOTOrderedD_rep;
            sMortgageDOTDocumentD.Text = dataLoan.sMortgageDOTDocumentD_rep;
            sMortgageDOTReceivedD.Text = dataLoan.sMortgageDOTReceivedD_rep;
            sMortgageDOTN.Text = dataLoan.sMortgageDOTN;

            sNoteOrderedD.Text = dataLoan.sNoteOrderedD_rep;
            sNoteReceivedD.Text = dataLoan.sNoteReceivedD_rep;
            sNoteN.Text = dataLoan.sNoteN;

            sFinalHUD1SttlmtStmtOrderedD.Text = dataLoan.sFinalHUD1SttlmtStmtOrderedD_rep;
            sFinalHUD1SttlmtStmtDocumentD.Text = dataLoan.sFinalHUD1SttlmtStmtDocumentD_rep;
            sFinalHUD1SttlmtStmtReceivedD.Text = dataLoan.sFinalHUD1SttlmtStmtReceivedD_rep;
            sFinalHUD1SttlmtStmtN.Text = dataLoan.sFinalHUD1SttlmtStmtN;

            sTitleInsPolicyOrderedD.Text = dataLoan.sTitleInsPolicyOrderedD_rep;
            sTitleInsPolicyDocumentD.Text = dataLoan.sTitleInsPolicyDocumentD_rep;
            sTitleInsPolicyReceivedD.Text = dataLoan.sTitleInsPolicyReceivedD_rep;
            sTitleInsPolicyN.Text = dataLoan.sTitleInsPolicyN;

            sMiCertOrderedD.Text = dataLoan.sMiCertOrderedD_rep;
            sMiCertIssuedD.Text = dataLoan.sMiCertIssuedD_rep;
            sMiCertReceivedD.Text = dataLoan.sMiCertReceivedD_rep;
            sMiCertN.Text = dataLoan.sMiCertN;

            sFinalHazInsPolicyOrderedD.Text = dataLoan.sFinalHazInsPolicyOrderedD_rep;
            sFinalHazInsPolicyDocumentD.Text = dataLoan.sFinalHazInsPolicyDocumentD_rep;
            sFinalHazInsPolicyReceivedD.Text = dataLoan.sFinalHazInsPolicyReceivedD_rep;
            sFinalHazInsPolicyN.Text = dataLoan.sFinalHazInsPolicyN;

            sFinalFloodInsPolicyOrderedD.Text = dataLoan.sFinalFloodInsPolicyOrderedD_rep;
            sFinalFloodInsPolicyDocumentD.Text = dataLoan.sFinalFloodInsPolicyDocumentD_rep;
            sFinalFloodInsPolicyReceivedD.Text = dataLoan.sFinalFloodInsPolicyReceivedD_rep;
            sFinalFloodInsPolicyN.Text = dataLoan.sFinalFloodInsPolicyN;

            sStatusD.Value = dataLoan.sStatusD_rep;

            sAppSubmitedToUsdaD.Text = dataLoan.sAppSubmitedToUsdaD_rep;
            UsdaAppDateRow.Visible = dataLoan.sLT == E_sLT.UsdaRural;

            sIntentToProceedD.ReadOnly = dataLoan.sUseInitialLeSignedDateAsIntentToProceedDate;
            sIntentToProceedD.Text = dataLoan.sIntentToProceedD_rep;
            sIntentToProceedN.Text = dataLoan.sIntentToProceedN;

            sIsBranchActAsOriginatorForFileLabel.Text = dataLoan.sIsBranchActAsOriginatorForFileLabel;
            sIsBranchActAsLenderForFileLabel.Text = dataLoan.sIsBranchActAsLenderForFileLabel;
            Tools.Set_ListControl(sIsBranchActAsLenderForFileTri, dataLoan.sIsBranchActAsLenderForFileTri);
            Tools.Set_ListControl(sIsBranchActAsOriginatorForFileTri, dataLoan.sIsBranchActAsOriginatorForFileTri);

            this.RespaDates.LoadData(
                dataLoan,
                this.UserHasWorkflowPrivilege(WorkflowOperations.ClearRespaFirstEnteredDates));

            sNumTouchesLeadNew.Text = dataLoan.sNumTouchesLeadNew_rep;
            sNumTouchesLoanOpen.Text = dataLoan.sNumTouchesLoanOpen_rep;
            sNumTouchesPrequal.Text = dataLoan.sNumTouchesPrequal_rep;
            sNumTouchesRegistered.Text = dataLoan.sNumTouchesRegistered_rep;
            sNumTouchesPreProcessing.Text = dataLoan.sNumTouchesPreProcessing_rep;
            sNumTouchesProcessing.Text = dataLoan.sNumTouchesProcessing_rep;
            sNumTouchesDocumentCheck.Text = dataLoan.sNumTouchesDocumentCheck_rep;
            sNumTouchesDocumentCheckFailed.Text = dataLoan.sNumTouchesDocumentCheckFailed_rep;
            sNumTouchesSubmitted.Text = dataLoan.sNumTouchesSubmitted_rep;
            sNumTouchesPreUnderwriting.Text = dataLoan.sNumTouchesPreUnderwriting_rep;
            sNumTouchesUnderwriting.Text = dataLoan.sNumTouchesUnderwriting_rep;
            sNumTouchesPreapproval.Text = dataLoan.sNumTouchesPreapproval_rep;
            sNumTouchesApproved.Text = dataLoan.sNumTouchesApproved_rep;
            sNumTouchesConditionReview.Text = dataLoan.sNumTouchesConditionReview_rep;
            sNumTouchesFinalUnderwriting.Text = dataLoan.sNumTouchesFinalUnderwriting_rep;
            sNumTouchesPreDocQC.Text = dataLoan.sNumTouchesPreDocQC_rep;
            sNumTouchesClearToClose.Text = dataLoan.sNumTouchesClearToClose_rep;
            sNumTouchesDocsOrdered.Text = dataLoan.sNumTouchesDocsOrdered_rep;
            sNumTouchesDocsDrawn.Text = dataLoan.sNumTouchesDocsDrawn_rep;
            sNumTouchesLoanDocs.Text = dataLoan.sNumTouchesLoanDocs_rep;
            sNumTouchesDocsBack.Text = dataLoan.sNumTouchesDocsBack_rep;
            sNumTouchesFundingConditions.Text = dataLoan.sNumTouchesFundingConditions_rep;
            sNumTouchesFunded.Text = dataLoan.sNumTouchesFunded_rep;
            sNumTouchesRecorded.Text = dataLoan.sNumTouchesRecorded_rep;
            sNumTouchesFinalDocs.Text = dataLoan.sNumTouchesFinalDocs_rep;
            sNumTouchesClosed.Text = dataLoan.sNumTouchesClosed_rep;
            sNumTouchesSubmittedForPurchaseReview.Text = dataLoan.sNumTouchesSubmittedForPurchaseReview_rep;
            sNumTouchesInPurchaseReview.Text = dataLoan.sNumTouchesInPurchaseReview_rep;
            sNumTouchesPrePurchaseConditions.Text = dataLoan.sNumTouchesPrePurchaseConditions_rep;
            sNumTouchesSubmittedForFinalPurchaseReview.Text = dataLoan.sNumTouchesSubmittedForFinalPurchaseReview_rep;
            sNumTouchesInFinalPurchaseReview.Text = dataLoan.sNumTouchesInFinalPurchaseReview_rep;
            sNumTouchesClearToPurchase.Text = dataLoan.sNumTouchesClearToPurchase_rep;
            sNumTouchesPurchased.Text = dataLoan.sNumTouchesPurchased_rep;
            sNumTouchesReadyForSale.Text = dataLoan.sNumTouchesReadyForSale_rep;
            sNumTouchesShipped.Text = dataLoan.sNumTouchesShipped_rep;
            sNumTouchesLoanPurchased.Text = dataLoan.sNumTouchesLoanPurchased_rep;
            sNumTouchesCounterOffer.Text = dataLoan.sNumTouchesCounterOffer_rep;
            sNumTouchesOnHold.Text = dataLoan.sNumTouchesOnHold_rep;
            sNumTouchesCanceled.Text = dataLoan.sNumTouchesCanceled_rep;
            sNumTouchesSuspended.Text = dataLoan.sNumTouchesSuspended_rep;
            sNumTouchesRejected.Text = dataLoan.sNumTouchesRejected_rep;
            sNumTouchesWithdrawn.Text = dataLoan.sNumTouchesWithdrawn_rep;
            sNumTouchesArchived.Text = dataLoan.sNumTouchesArchived_rep;
            sNumTouchesInvestorConditions.Text = dataLoan.sNumTouchesInvestorConditions_rep;
            sNumTouchesInvestorConditionsSent.Text = dataLoan.sNumTouchesInvestorConditionsSent_rep;
        }

        protected override void LoadData() 
        {
            DataLoan.InitLoad();
            BindDataObject(DataLoan);
            serializeStatuses(DataLoan);

            if (ConstStage.DisableStatusEventFeature)
            {
                body.Attributes.Add("hide-status-events", "true");
            }

            this.RegisterJsGlobalVariables("DocMagicPlanCode", this.DataLoan.sDocMagicPlanCodeNm);
            this.RegisterJsGlobalVariables("DocMagicVendorId", DocumentVendorBrokerSettings.GetDocMagicSettings(Broker.BrokerID).VendorId.ToString());

            ClientScript.RegisterHiddenField("sLastDisclosedGFEArchiveId", DataLoan.sLastDisclosedGFEArchiveId_rep);
            ClientScript.RegisterHiddenField("sLastDisclosedClosingDisclosureArchiveId", DataLoan.sLastDisclosedClosingDisclosureArchiveId_rep);
        }

        protected void serializeStatuses(CPageData dataLoan)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            HashSet<E_sStatusT> retailSet = Broker.GetEnabledStatusesByChannel(E_BranchChannelT.Retail);
            HashSet<String> retailNames = new HashSet<string>();
            foreach (E_sStatusT status in retailSet)
            {
                retailNames.Add(status.ToString());
            }
            String retail = serializer.Serialize(retailNames);

            HashSet<E_sStatusT> wholesaleSet = Broker.GetEnabledStatusesByChannel(E_BranchChannelT.Wholesale);
            HashSet<String> wholesaleNames = new HashSet<string>();
            foreach (E_sStatusT status in wholesaleSet)
            {
                wholesaleNames.Add(status.ToString());
            }
            String wholesale = serializer.Serialize(wholesaleNames);

            //HashSet<E_sStatusT> correspondentSet = Broker.GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT.Correspondent, dataLoan.sCorrespondentProcessT);
            //HashSet<String> correspondentNames = new HashSet<string>();
            //foreach (E_sStatusT status in correspondentSet)
            //{
            //    correspondentNames.Add(status.ToString());
            //}
            //String correspondent = serializer.Serialize(correspondentNames);

            HashSet<E_sStatusT> brokerSet = Broker.GetEnabledStatusesByChannel(E_BranchChannelT.Broker);
            HashSet<String> brokerNames = new HashSet<string>();
            foreach (E_sStatusT status in brokerSet)
            {
                brokerNames.Add(status.ToString());
            }
            String broker = serializer.Serialize(brokerNames);

            HashSet<E_sStatusT> channelNotSpecifiedSet = Broker.GetEnabledStatusesByChannel(E_BranchChannelT.Blank);
            HashSet<String> channelNotSpecifiedNames = new HashSet<string>();
            foreach (E_sStatusT status in channelNotSpecifiedSet)
            {
                channelNotSpecifiedNames.Add(status.ToString());
            }
            String channelNotSpecified = serializer.Serialize(channelNotSpecifiedNames);

            // OPM 185961 - Add Correspondent channel sub channels.
            HashSet<E_sStatusT> correspondentSet_Blank = Broker.GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT.Correspondent, E_sCorrespondentProcessT.Blank);
            HashSet<String> correspondentNames_Blank = new HashSet<string>();
            foreach (E_sStatusT status in correspondentSet_Blank)
            {
                correspondentNames_Blank.Add(status.ToString());
            }
            String correspondent_Blank = serializer.Serialize(correspondentNames_Blank);

            HashSet<E_sStatusT> correspondentSet_Delegated = Broker.GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT.Correspondent, E_sCorrespondentProcessT.Delegated);
            HashSet<String> correspondentNames_Delegated = new HashSet<string>();
            foreach (E_sStatusT status in correspondentSet_Delegated)
            {
                correspondentNames_Delegated.Add(status.ToString());
            }
            String correspondent_Delegated = serializer.Serialize(correspondentNames_Delegated);

            HashSet<E_sStatusT> correspondentSet_PriorApproved = Broker.GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT.Correspondent, E_sCorrespondentProcessT.PriorApproved);
            HashSet<String> correspondentNames_PriorApproved = new HashSet<string>();
            foreach (E_sStatusT status in correspondentSet_PriorApproved)
            {
                correspondentNames_PriorApproved.Add(status.ToString());
            }
            String correspondent_PriorApproved = serializer.Serialize(correspondentNames_PriorApproved);

            HashSet<E_sStatusT> correspondentSet_MiniCorrespondent = Broker.GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT.Correspondent, E_sCorrespondentProcessT.MiniCorrespondent);
            HashSet<String> correspondentNames_MiniCorrespondent = new HashSet<string>();
            foreach (E_sStatusT status in correspondentSet_MiniCorrespondent)
            {
                correspondentNames_MiniCorrespondent.Add(status.ToString());
            }
            String correspondent_MiniCorrespondent = serializer.Serialize(correspondentNames_MiniCorrespondent);

            HashSet<E_sStatusT> correspondentSet_Bulk = Broker.GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT.Correspondent, E_sCorrespondentProcessT.Bulk);
            HashSet<String> correspondentNames_Bulk = new HashSet<string>();
            foreach (E_sStatusT status in correspondentSet_Bulk)
            {
                correspondentNames_Bulk.Add(status.ToString());
            }
            String correspondent_Bulk = serializer.Serialize(correspondentNames_Bulk);

            HashSet<E_sStatusT> correspondentSet_MiniBulk = Broker.GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT.Correspondent, E_sCorrespondentProcessT.MiniBulk);
            HashSet<String> correspondentNames_MiniBulk = new HashSet<string>();
            foreach (E_sStatusT status in correspondentSet_MiniBulk)
            {
                correspondentNames_MiniBulk.Add(status.ToString());
            }
            String correspondent_MiniBulk = serializer.Serialize(correspondentNames_MiniBulk);


            ClientScript.RegisterHiddenField("retailArray", retail);
            ClientScript.RegisterHiddenField("wholesaleArray", wholesale);
            //ClientScript.RegisterHiddenField("correspondentArray", correspondent);
            ClientScript.RegisterHiddenField("channelNotSpecifiedArray", channelNotSpecified);
            ClientScript.RegisterHiddenField("brokerArray", broker);
            ClientScript.RegisterHiddenField("masterStatusArray", serializer.Serialize(Enum.GetNames(typeof(E_sStatusT))));

            ClientScript.RegisterHiddenField("correspondentArray_Blank", correspondent_Blank);
            ClientScript.RegisterHiddenField("correspondentArray_Delegated", correspondent_Delegated);
            ClientScript.RegisterHiddenField("correspondentArray_PriorApproved", correspondent_PriorApproved);
            ClientScript.RegisterHiddenField("correspondentArray_MiniCorrespondent", correspondent_MiniCorrespondent);
            ClientScript.RegisterHiddenField("correspondentArray_Bulk", correspondent_Bulk);
            ClientScript.RegisterHiddenField("correspondentArray_MiniBulk", correspondent_MiniBulk);
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
			this.PageTitle = "Tracking general";
            this.PageID = "StatusGeneral";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CLoanTrackingPDF);
            BindLeadSource();
            Tools.Bind_sRedisclosureMethodT(sDocMagicRedisclosureMethodT);
            Tools.Bind_sHasESignedDocumentT(sHasESignedDocumentsT);
            Tools.Bind_sCorrespondentProcessT(sCorrespondentProcessT);
            
            Tools.Bind_aPreFundVoeTypeT(aBPreFundVoeTypeT);
            Tools.Bind_aPreFundVoeTypeT(aCPreFundVoeTypeT);
            
            BindTriState(sIsBranchActAsLenderForFileTri);
            BindTriState(sIsBranchActAsOriginatorForFileTri);
            BindAlterateLender();
        }

        private void BindAlterateLender()
        {
            sAlternateLender.Items.Add(new ListItem("", Guid.Empty.ToString()));
            List<Tuple<string, Guid>> alternateLenderList = DocMagicAlternateLender.GetAlternateLenderListByBroker(BrokerUser.BrokerId);
            foreach (Tuple<string, Guid> lender in alternateLenderList)
                sAlternateLender.Items.Add(new ListItem(lender.Item1, lender.Item2.ToString()));
        }

        private void BindLeadSource() 
        {
            // 5/5/14 gf - opm 172380, always add a blank option for the default
            // value. We do not want the UI to display misleading data before the
            // page is saved because there are now pricing rules and fee service
            // rules which depend on this field.
            sLeadSrcDesc.Items.Add(new ListItem("", ""));

            foreach (LeadSource desc in Broker.LeadSources) 
            {
                sLeadSrcDesc.Items.Add(new ListItem(desc.Name, desc.Name));
            }
        }

        private void BindTriState(RadioButtonList rbl)
        {
            rbl.Items.Add(new ListItem("Yes", E_TriState.Yes.ToString("D")));
            rbl.Items.Add(new ListItem("No", E_TriState.No.ToString("D")));
            rbl.Items.Add(new ListItem("Not Sure", E_TriState.Blank.ToString("D")));
        }
        private void UpdateTextBox(string id, TextBox tb, CPageData dataLoan)
        {
            if (dataLoan.IsCustomFieldGloballyDefined(id))
            {
                tb.ReadOnly = true;
                tb.BackColor = Color.Gainsboro;
                tb.ForeColor = Color.Black;
            }
        }                
                
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

	}

}
