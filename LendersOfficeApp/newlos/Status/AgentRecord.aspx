<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="AgentRecord.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Status.AgentRecord" validateRequest=true %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="uc" TagName="LendingLicenses" Src="../../los/BrokerAdmin/LendingLicensesPanel.ascx" %>
<%@ Register TagPrefix="uc" TagName="AgentRecord" Src="~/newlos/Status/AgentRecord.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="LendersOffice.Common" %>
<!DOCTYPE HTML>
<HTML>
	<HEAD runat="server">
		<title>Agent Record</title>
		<style type="text/css">
		.TabBody {
		  border-left: groove 2px;
		  border-right: groove 2px;
          border-bottom: groove 2px;
		  padding:5px;
		  width:700px;
		}
		.NormalTab {
		  width:150px;
		  border:groove 2px;
		  padding:5px;
		  margin:0px;
		  font-weight:bold;
		  background-color:darkgray;
		  color:whitesmoke;
		  cursor: default;
		}
		.SelectedTab {
		  width:150px;
		  border-bottom:none;
		  padding:5px;
		  margin:0px;
		  color:Black;
		  background-color:gainsboro;
		}
        #ContactInformationPnl td, #ContactInformationPnl td.FieldLabel {
            padding-bottom: 4px;
        }
		</style>
	</HEAD>
	<body style="background-color:gainsboro">
		<script type="text/javascript">
      <!--
      var oRolodex;
      
      function _init() {
        g_oPreviousTab = document.getElementById("ContactInformationTab");

        oRolodex = new cRolodex();
        if (document.getElementById("RecordID").value == <%= AspxTools.JsString(Guid.Empty) %>) 
        {
          document.getElementById('IndexLabel').style.visibility = 'hidden';
        }
        <%-- //overwrite the saveMe function from 'singleedit.js' in order to check if the Licenses Panel is Valid --%>
        this.saveMe = __saveMe;
        
        if (typeof _initControl === 'function') {
            _initControl();
        }
      }
  
  function resetForm() {    
    if (typeof clearAllLicenses === 'function'){
        clearAllLicenses();
    }
    
    if (typeof resetAgentFields === 'function'){
        resetAgentFields();
    }
    
    document.getElementById('IndexLabel').style.visibility = 'hidden'; 
  }
   
  function updateDirtyBit_callback() {

    document.getElementById("btnSave").disabled = false;
  }
  
    function f_displayTab(iTabIndex) {
      var list = ["ContactInformationPnl", "LendingLicensePnl", "CompanyLicensesPnl"];
      for (var i = 0; i < list.length; i++) {
        var oTab = document.getElementById(list[i]);
        oTab.style.display = i == iTabIndex ? "" : "none";
      }
    }
    var g_oPreviousTab = null;
    function f_onTabClick(oTab, iTabIndex) {
      if (oTab == g_oPreviousTab)
        return;
        
      f_displayTab(iTabIndex);
      
      if (null != g_oPreviousTab) {
        g_oPreviousTab.style.backgroundColor = "darkgray";
        g_oPreviousTab.style.borderBottom = "groove 2px";
        g_oPreviousTab.style.color = "whitesmoke";
      }
      oTab.style.backgroundColor = "gainsboro";
      oTab.style.borderBottom = "none";
      oTab.style.color = "black";
      
      g_oPreviousTab = oTab;
    }
    
    function showLicensesError(){
            var bSuccess = true;
            if (typeof AreLicensesValid === 'function'){
               bSuccess = AreLicensesValid();
            }
            var outMsg = '';
            if (bSuccess == false){
                var msg1 = '';
                var msg2 = '';
                var oErr1 = document.getElementById('UserlicErrMsg');
                var oErr2 = document.getElementById('CompanylicErrMsg');
                msg1 = oErr1.innerText;
                msg2 = oErr2.innerText;
                
                if ( (msg1.length != 0) || (msg2.length != 0) ){
                    outMsg = 'Changes were not saved.<br />';
                    if (msg1.length != 0){
                        outMsg += ' - Review the "Loan Officer Licenses" tab. ' + msg1 + '<br />';
                    }
                    if (msg2.length != 0){
                        outMsg += ' - Review the "Company Licenses" tab. ' + msg2;
                    }
                }
            }
            document.getElementById('errMsg').innerHTML = outMsg;
            return bSuccess;
    }

    <%-- //overwrite the saveMe function from 'singleedit.js' in order to check if the Licenses Panel is Valid --%>
    function __saveMe(href) {
        try{
            if (null == gService.singleedit)
                return false;
            
            if (!showLicensesError())
                return false;
            
            if (isPageValid() == false)
                return false;
            
            var args = getAllFormValues();
            
            if (typeof (executePostGetAllFormValuesCallback) == "function")
                executePostGetAllFormValuesCallback(args);  
            
            var result = invokeService("SaveData", args, true);
            if (!result.error) 
            {
              document.getElementById('IndexLabel').style.visibility = 'visible';
              if (typeof(parent.parent_disableBtnWhenNew) == "function")
                parent.parent_disableBtnWhenNew(false);
            
              parent_disableSaveBtn(true);
              return true;
            } 
            else 
            {
              if (result.ErrorType == 'VersionMismatchException') 
              {
                goToList();
              }
              return false;
            }
        }catch(e){
            return false;
        }
    }
    
    function __goPrevious(){
        if (!showLicensesError())
                return;
        goPrevious();
    }
    function __goNext(){
        if (!showLicensesError())
                return;
        goNext();
    }
    
    function goToUrl(url) {
        try{
            PolyShouldShowConfirmSave(isDirty(), function(confirmed){
                if (confirmed && !saveMe()){
                    return;
                }

                location.href= url;
            });
        }catch(e){}
    }
//-->
		</script>
		
	    <form id="AgentRecord" method="post" runat="server">
	        <div style="padding-left:2px;">
                <table class="FormTable" id="Table3" cellspacing="0" cellpadding="0" border="0" width="700px">
                    <tr>
                        <td class="MainRightHeader" nowrap>Contact Information <span id="IndexLabel"></span></td>
                    </tr>
                </table>
                <br />
                <table cellpadding="0" cellspacing="0" border="0" width="700px">
                    <tr>
                        <td id="ContactInformationTab" class="NormalTab SelectedTab" style="margin-right:6px;" onclick="f_onTabClick(this, 0);">Contact Information</td>
                        <td style="border-bottom:groove 2px;">&nbsp;</td>
                        <td class="NormalTab" style="margin-right:6px;" onclick="f_onTabClick(this, 1);">Loan Officer Licenses</td>
                        <td style="border-bottom:groove 2px;">&nbsp;</td>
                        <td class="NormalTab" style="margin-right:6px;" onclick="f_onTabClick(this, 2);">Company Licenses</td>
                        <td style="border-bottom:groove 2px;width:30%">&nbsp;</td>
                    </tr>
                    <tr >
                        <td colspan="6" class="TabBody">
                            <div id="ContactInformationPnl">
                                <uc:AgentRecord id="ContactInfo" runat="server" />
                            </div>
                        
                            <div id="LendingLicensePnl" style="display:none">
                                <table cellpadding="0" cellspacing="0" border="0" style="width:100%">
                                    <tr>
                                        <td>
                                            <div>
                                                <uc:LendingLicenses id="LicensesPanel" NamingPrefix="User" DisplayLosIdentifier="false" runat="server"></uc:LendingLicenses>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div id="CompanyLicensesPnl" style="display:none">
                                <table cellpadding="0" cellspacing="0" border="0" style="width:100%">
                                    <tr>
                                        <td>
                                            <div>
                                                <uc:LendingLicenses id="CompanyLicenses" NamingPrefix="Company" DisplayLosIdentifier="false" runat="server"></uc:LendingLicenses>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" width="700px">
                    <tr>
                        <td nowrap style="padding:3px;"><span id="errMsg" style="color:Red;font-size: 11px;font-family: Arial; font-weight:bold"></span>&nbsp;</td>
                    </tr>
                    <tr>
                        <td nowrap align="center">
                            <input id="btnPrevious" onclick="__goPrevious();" type="button" value="Previous" tabindex="9" accesskey="P">&nbsp; 
                            <input id="btnNext" onclick="__goNext();" type="button" value="Next" tabindex="9" accesskey="N">&nbsp;&nbsp;&nbsp;&nbsp; 
                            <input id="btnBack" type="button" tabindex="9" accesskey="B" runat="server">&nbsp;&nbsp;&nbsp;&nbsp; 
                            <input id="btnSave" type="button" value="Save" onclick="saveMe();" disabled>&nbsp; 
                            <input onclick="saveAndCreateNew();" type="button" value="Save and Add other" tabindex="9" accesskey="A"> 
                        </td>
                    </tr>
                </table>
            </div>
            <input type="checkbox" id="ShouldUpdateCompanyInfo" runat="server" style="display:none;" />
		    <uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg>
	    </form>
	</body>
</HTML>
