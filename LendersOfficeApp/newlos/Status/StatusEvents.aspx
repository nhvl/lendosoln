﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StatusEvents.aspx.cs" Inherits="LendersOfficeApp.newlos.Status.StatusEvents" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title></title>
    <style>
        body {
            background-color: gainsboro;
        }

        .container {
            padding-left: 10px;
        }

        #MigrationBtn, .MainRightHeader {
            margin-bottom: 10px;
        }

        #EventsTable {
            border-collapse: collapse;
            min-width: 350px;
            text-align: center;
        }

        #EventsTable tr {
            border: 1px solid black;
        }

            #EventsTable td, #EventsTable th {
                padding: 3px;
            }
    </style>
    <script>
        var scope;

        $(function () {
            var app = angular.module("statusEventsTable", []);
            app.controller("statusEventsController", ['$scope', '$compile', '$element', function ($scope, $compile, $element) {
                scope = $scope;
                $scope.statusEvents = JSON.parse(ML.StatusEventJson);
                $scope.CanExclude = ML.CanExclude;
                $scope.IsStatusEventMigratedWithUserName = ML.IsStatusEventMigratedWithUserName;

                $scope.toggleDirty = function (statusEvent) {
                    statusEvent.isDirty = !statusEvent.isDirty;
                }
            }]);

            $("#MigrationBtn").click(Migrate);

            function Migrate() {
                var args = {
                    loanId: ML.sLId
                };

                var result = gService.main.call("migrate", args);
                if (!result.error && result.value.OK == "True")
                {
                    location.reload();
                }
                else if(result.error) {
                    alert(result.UserMessage || "Unable to migrate.  Please try again.");
                }
            }
        });

        function saveMe() {
            var dirtyStatusEvents = scope.statusEvents
                .filter(function (statusEvent) { return statusEvent.isDirty; });
            var dirtyIds = dirtyStatusEvents.map(function (statusEvent) { return statusEvent.Id });

            var args = {
                loanId: ML.sLId,
                dirtyIdsJson: JSON.stringify(dirtyIds)
            };

            var result = gService.main.call("save", args);

            if (!result.error) {
                dirtyStatusEvents.every(function (statusEvent) { statusEvent.isDirty = false; return true; });
                clearDirty();
                return true;
            }
            else {
                alert(result.UserMessage || "Unable to save data.  Please try again.");
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="MainRightHeader">Status Events</div>
        <div class="container">
            <button id="MigrationBtn" runat="server">Migrate</button>
            <div ng-app="statusEventsTable" ng-controller="statusEventsController">
                <table id="EventsTable">
                    <thead>
                        <tr class="GridHeader">
                            <th>Status</th>
                            <th>Status Date</th>
                            <th>Timestamp</th>
                            <th ng-show="IsStatusEventMigratedWithUserName">User Name</th>
                            <th>Exclude</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="GridAutoItem" ng-repeat="statusEvent in statusEvents | orderBy : 'Timestamp.DateTimeForComputationWithTime'">
                            <td ng-bind="statusEvent.StatusDescription"></td>
                            <td ng-bind="statusEvent.StatusD_rep"></td>
                            <td ng-bind="statusEvent.Timestamp_rep"></td>
                            <td ng-show="IsStatusEventMigratedWithUserName" ng-bind="statusEvent.UserName"></td>
                            <td>
                                <input ng-change="toggleDirty(statusEvent);" ng-disabled="!CanExclude" type="checkbox" ng-model="statusEvent.IsExclude" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
