﻿namespace LendersOfficeApp.newlos.Status
{
    using System;
    using DataAccess;
    using LendersOffice.Security;
    using System.Web.UI.WebControls;
    using System.Drawing;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOfficeApp.newlos.Disclosure;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Migration;

    public partial class Processing : BaseLoanPage
    {
        public override bool IsReadOnly
        {
            get
            {
                // 9/22/2011 dd - Base from OPM 70822 - We will allow processor to edit this page regardless of their
                // write privileges.
                bool bIsReadOnly = base.IsReadOnly;
                if (bIsReadOnly == true && BrokerUser.HasRole(E_RoleT.Processor))
                {
                    bIsReadOnly = false;
                }

                return bIsReadOnly;
            }
        }

        internal static void BindDataFromControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem serviceItem)
        {
            CreditReportAuthorization.BindDataToLoan(loan, application, serviceItem, nameof(CreditReportAuthorizationCtrl) + "_");
            RespaDates.BindData(loan, serviceItem, nameof(RespaDates));
        }

        internal static void LoadDataForControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem serviceItem)
        {
            CreditReportAuthorization.LoadDataFromLoan(loan, application, serviceItem, nameof(CreditReportAuthorizationCtrl) + "_");
            RespaDates.LoadData(loan, serviceItem, nameof(RespaDates));
        }

        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new[] { WorkflowOperations.ClearRespaFirstEnteredDates };
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Processing));
            dataLoan.InitLoad();

            CAppData dataApp;
            dataApp = ApplicationID == Guid.Empty ? dataLoan.GetAppData(0) : dataLoan.GetAppData(ApplicationID);

            aBNm.Text = dataApp.aBNm;
            aCNm.Text = dataApp.aCNm;

            Tools.SetDropDownListValue(aBPreFundVoeTypeT, dataApp.aBPreFundVoeTypeT);
            aBPreFundVoeOd.Text = dataApp.aBPreFundVoeOd_rep;
            aBPreFundVoeDueD.Text = dataApp.aBPreFundVoeDueD_rep;
            aBPreFundVoeRd.Text = dataApp.aBPreFundVoeRd_rep;
            aBPreFundVoeN.Text = dataApp.aBPreFundVoeN;
            
            Tools.SetDropDownListValue(aCPreFundVoeTypeT, dataApp.aCPreFundVoeTypeT);
            aCPreFundVoeOd.Text = dataApp.aCPreFundVoeOd_rep;
            aCPreFundVoeDueD.Text = dataApp.aCPreFundVoeDueD_rep;
            aCPreFundVoeRd.Text = dataApp.aCPreFundVoeRd_rep;
            aCPreFundVoeN.Text = dataApp.aCPreFundVoeN;
         
            sU1LStatDesc.Text = dataLoan.sU1LStatDesc;
            this.UpdateTextBox("sU1LStatDesc", sU1LStatDesc, dataLoan);
            sU1LStatD.Text = dataLoan.sU1LStatD_rep;
            sU1LStatN.Text = dataLoan.sU1LStatN;
            sU2LStatDesc.Text = dataLoan.sU2LStatDesc;
            this.UpdateTextBox("sU2LStatDesc", sU2LStatDesc, dataLoan);
            sU2LStatD.Text = dataLoan.sU2LStatD_rep;
            sU2LStatN.Text = dataLoan.sU2LStatN;
            sU3LStatDesc.Text = dataLoan.sU3LStatDesc;
            this.UpdateTextBox("sU3LStatDesc", sU3LStatDesc, dataLoan);
            sU3LStatD.Text = dataLoan.sU3LStatD_rep;
            sU3LStatN.Text = dataLoan.sU3LStatN;
            sU4LStatDesc.Text = dataLoan.sU4LStatDesc;
            this.UpdateTextBox("sU4LStatDesc", sU4LStatDesc, dataLoan);
            sU4LStatD.Text = dataLoan.sU4LStatD_rep;
            sU4LStatN.Text = dataLoan.sU4LStatN;

            sDocMagicApplicationD.Text = dataLoan.sDocMagicApplicationD_rep;
            sDocMagicApplicationDLckd.Checked = dataLoan.sDocMagicApplicationDLckd;
            sDocMagicGFED.Text = dataLoan.sDocMagicGFED_rep;
            sDocMagicGFEDLckd.Checked = dataLoan.sDocMagicGFEDLckd;
            sDocMagicEstAvailableThroughD.Text = dataLoan.sDocMagicEstAvailableThroughD_rep;
            sDocMagicEstAvailableThroughDLckd.Checked = dataLoan.sDocMagicEstAvailableThroughDLckd;
            sDocMagicDocumentD.Text = dataLoan.sDocMagicDocumentD_rep;
            sDocMagicDocumentDLckd.Checked = dataLoan.sDocMagicDocumentDLckd;
            sDocMagicClosingD.Text = dataLoan.sDocMagicClosingD_rep;
            sDocMagicClosingDLckd.Checked = dataLoan.sDocMagicClosingDLckd;
            sDocMagicSigningD.Text = dataLoan.sDocMagicSigningD_rep;
            sDocMagicSigningDLckd.Checked = dataLoan.sDocMagicSigningDLckd;
            sDocMagicCancelD.Text = dataLoan.sDocMagicCancelD_rep;
            sDocMagicCancelDLckd.Checked = dataLoan.sDocMagicCancelDLckd;
            sDocMagicDisbursementD.Text = dataLoan.sDocMagicDisbursementD_rep;
            sDocMagicDisbursementDLckd.Checked = dataLoan.sDocMagicDisbursementDLckd;
            sDocMagicRateLockD.Text = dataLoan.sDocMagicRateLockD_rep;
            sDocMagicRateLockDLckd.Checked = dataLoan.sDocMagicRateLockDLckd;
            sDocMagicPreZSentD.Text = dataLoan.sDocMagicPreZSentD_rep;
            sDocMagicPreZSentDLckd.Checked = dataLoan.sDocMagicPreZSentDLckd;
            sDocMagicReDiscSendD.Text = dataLoan.sDocMagicReDiscSendD_rep;
            sDocMagicReDiscSendDLckd.Checked = dataLoan.sDocMagicReDiscSendDLckd;
            sDocMagicRedisclosureMethodTLckd.Checked = dataLoan.sDocMagicRedisclosureMethodTLckd;
            sDocMagicReDiscReceivedD.Text = dataLoan.sDocMagicReDiscReceivedD_rep;
            sDocMagicReDiscReceivedDLckd.Checked = dataLoan.sDocMagicReDiscReceivedDLckd;

            sTrNotes.Text = dataLoan.sTrNotes;

            sPurchaseContractDate.Text = dataLoan.sPurchaseContractDate_rep;
            sFinancingContingencyExpD.Text = dataLoan.sFinancingContingencyExpD_rep;
            sFinancingContingencyExtensionExpD.Text = dataLoan.sFinancingContingencyExtensionExpD_rep;

            Tools.SetDropDownListValue(sDocMagicRedisclosureMethodT, dataLoan.sDocMagicRedisclosureMethodT);
            // 9/3/2013 AV - 137485 Investigate Lead Source update problem
            if (dataLoan.sLeadSrcDesc.Length > 0 && sLeadSrcDesc.Items.FindByText(dataLoan.sLeadSrcDesc) == null) 
            {
                // 7/15/2005 dd - If current description no longer in description, just add to the drop downlist instead of clear out.
                sLeadSrcDesc.Items.Add(new ListItem(dataLoan.sLeadSrcDesc, dataLoan.sLeadSrcDesc));
            }

            Tools.SetDropDownListValue(sLeadSrcDesc, dataLoan.sLeadSrcDesc);

            sAppSubmittedD.Text = dataLoan.sAppSubmittedD_rep;
            sAppSubmittedDLckd.Checked = dataLoan.sAppSubmittedDLckd;
            sAppReceivedByLenderD.Text = dataLoan.sAppReceivedByLenderD_rep;
            sAppReceivedByLenderDLckd.Checked = dataLoan.sAppReceivedByLenderDLckd;

            sPrelimRprtOd.Text = dataLoan.sPrelimRprtOd_rep;
            sPrelimRprtDueD.Text = dataLoan.sPrelimRprtDueD_rep;
            sPrelimRprtRd.Text = dataLoan.sPrelimRprtRd_rep;
            sPrelimRprtN.Text = dataLoan.sPrelimRprtN;
            sApprRprtOd.Text = dataLoan.sApprRprtOd_rep;
            sApprRprtDueD.Text = dataLoan.sApprRprtDueD_rep;
            sApprRprtRd.Text = dataLoan.sApprRprtRd_rep;
            sApprRprtN.Text = dataLoan.sApprRprtN;
            sDocumentNoteD.Text = dataLoan.sDocumentNoteD_rep;
            sNoteOrderedD.Text = dataLoan.sNoteOrderedD_rep;
            sNoteReceivedD.Text = dataLoan.sNoteReceivedD_rep;
            sNoteN.Text = dataLoan.sNoteN;
            sTilGfeOd.Text = dataLoan.sTilGfeOd_rep;
            sTilGfeDueD.Text = dataLoan.sTilGfeDueD_rep;
            sTilGfeRd.Text = dataLoan.sTilGfeRd_rep;
            sTilGfeN.Text = dataLoan.sTilGfeN;
            sU1DocStatDesc.Text = dataLoan.sU1DocStatDesc;
            sU1DocStatOd.Text = dataLoan.sU1DocStatOd_rep;
            sU1DocStatDueD.Text = dataLoan.sU1DocStatDueD_rep;
            sU1DocStatRd.Text = dataLoan.sU1DocStatRd_rep;
            sU1DocStatN.Text = dataLoan.sU1DocStatN;
            sU2DocStatDesc.Text = dataLoan.sU2DocStatDesc;
            sU2DocStatOd.Text = dataLoan.sU2DocStatOd_rep;
            sU2DocStatDueD.Text = dataLoan.sU2DocStatDueD_rep;
            sU2DocStatRd.Text = dataLoan.sU2DocStatRd_rep;
            sU2DocStatN.Text = dataLoan.sU2DocStatN;
            sU3DocStatDesc.Text = dataLoan.sU3DocStatDesc;
            sU3DocStatOd.Text = dataLoan.sU3DocStatOd_rep;
            sU3DocStatDueD.Text = dataLoan.sU3DocStatDueD_rep;
            sU3DocStatRd.Text = dataLoan.sU3DocStatRd_rep;
            sU3DocStatN.Text = dataLoan.sU3DocStatN;
            sU4DocStatDesc.Text = dataLoan.sU4DocStatDesc;
            sU4DocStatOd.Text = dataLoan.sU4DocStatOd_rep;
            sU4DocStatDueD.Text = dataLoan.sU4DocStatDueD_rep;
            sU4DocStatRd.Text = dataLoan.sU4DocStatRd_rep;
            sU4DocStatN.Text = dataLoan.sU4DocStatN;
            sU5DocStatDesc.Text = dataLoan.sU5DocStatDesc;
            sU5DocStatOd.Text = dataLoan.sU5DocStatOd_rep;
            sU5DocStatDueD.Text = dataLoan.sU5DocStatDueD_rep;
            sU5DocStatRd.Text = dataLoan.sU5DocStatRd_rep;
            sU5DocStatN.Text = dataLoan.sU5DocStatN;

            sPrelimRprtDocumentD.Text = dataLoan.sPrelimRprtDocumentD_rep;
            sSpValuationEffectiveD.Text = dataLoan.sSpValuationEffectiveD_rep;
            sTilGfeDocumentD.Text = dataLoan.sTilGfeDocumentD_rep;
            sU1DocDocumentD.Text = dataLoan.sU1DocDocumentD_rep;
            sU2DocDocumentD.Text = dataLoan.sU2DocDocumentD_rep;
            sU3DocDocumentD.Text = dataLoan.sU3DocDocumentD_rep;
            sU4DocDocumentD.Text = dataLoan.sU4DocDocumentD_rep;
            sU5DocDocumentD.Text = dataLoan.sU5DocDocumentD_rep;

            sLoanPackageOrderedD.Text = dataLoan.sLoanPackageOrderedD_rep;
            sLoanPackageDocumentD.Text = dataLoan.sLoanPackageDocumentD_rep;
            sLoanPackageReceivedD.Text = dataLoan.sLoanPackageReceivedD_rep;
            sLoanPackageN.Text = dataLoan.sLoanPackageN;

            sMortgageDOTOrderedD.Text = dataLoan.sMortgageDOTOrderedD_rep;
            sMortgageDOTDocumentD.Text = dataLoan.sMortgageDOTDocumentD_rep;
            sMortgageDOTReceivedD.Text = dataLoan.sMortgageDOTReceivedD_rep;
            sMortgageDOTN.Text = dataLoan.sMortgageDOTN;

            sFinalHUD1SttlmtStmtOrderedD.Text = dataLoan.sFinalHUD1SttlmtStmtOrderedD_rep;
            sFinalHUD1SttlmtStmtDocumentD.Text = dataLoan.sFinalHUD1SttlmtStmtDocumentD_rep;
            sFinalHUD1SttlmtStmtReceivedD.Text = dataLoan.sFinalHUD1SttlmtStmtReceivedD_rep;
            sFinalHUD1SttlmtStmtN.Text = dataLoan.sFinalHUD1SttlmtStmtN;

            sTitleInsPolicyOrderedD.Text = dataLoan.sTitleInsPolicyOrderedD_rep;
            sTitleInsPolicyDocumentD.Text = dataLoan.sTitleInsPolicyDocumentD_rep;
            sTitleInsPolicyReceivedD.Text = dataLoan.sTitleInsPolicyReceivedD_rep;
            sTitleInsPolicyN.Text = dataLoan.sTitleInsPolicyN;

            sMiCertOrderedD.Text = dataLoan.sMiCertOrderedD_rep;
            sMiCertIssuedD.Text = dataLoan.sMiCertIssuedD_rep;
            sMiCertReceivedD.Text = dataLoan.sMiCertReceivedD_rep;
            sMiCertN.Text = dataLoan.sMiCertN;

            sFinalHazInsPolicyOrderedD.Text = dataLoan.sFinalHazInsPolicyOrderedD_rep;
            sFinalHazInsPolicyDocumentD.Text = dataLoan.sFinalHazInsPolicyDocumentD_rep;
            sFinalHazInsPolicyReceivedD.Text = dataLoan.sFinalHazInsPolicyReceivedD_rep;
            sFinalHazInsPolicyN.Text = dataLoan.sFinalHazInsPolicyN;

            sFinalFloodInsPolicyOrderedD.Text = dataLoan.sFinalFloodInsPolicyOrderedD_rep;
            sFinalFloodInsPolicyDocumentD.Text = dataLoan.sFinalFloodInsPolicyDocumentD_rep;
            sFinalFloodInsPolicyReceivedD.Text = dataLoan.sFinalFloodInsPolicyReceivedD_rep;
            sFinalFloodInsPolicyN.Text = dataLoan.sFinalFloodInsPolicyN;

            aCrOd.Text = dataApp.aCrOd_rep;
            aCrDueD.Text = dataApp.aCrDueD_rep;
            aCrRd.Text = dataApp.aCrRd_rep;
            aCrN.Text = dataApp.aCrN;

            aLqiCrOd.Text = dataApp.aLqiCrOd_rep;
            aLqiCrDueD.Text = dataApp.aLqiCrDueD_rep;
            aLqiCrRd.Text = dataApp.aLqiCrRd_rep;
            aLqiCrN.Text = dataApp.aLqiCrN;

            aBusCrOd.Text = dataApp.aBusCrOd_rep;
            aBusCrDueD.Text = dataApp.aBusCrDueD_rep;
            aBusCrRd.Text = dataApp.aBusCrRd_rep;
            aBusCrN.Text = dataApp.aBusCrN;

            aU1DocStatDesc.Text = dataApp.aU1DocStatDesc;
            aU1DocStatOd.Text = dataApp.aU1DocStatOd_rep;
            aU1DocStatDueD.Text = dataApp.aU1DocStatDueD_rep;
            aU1DocStatRd.Text = dataApp.aU1DocStatRd_rep;
            aU1DocStatN.Text = dataApp.aU1DocStatN;

            aU2DocStatDesc.Text = dataApp.aU2DocStatDesc;
            aU2DocStatOd.Text = dataApp.aU2DocStatOd_rep;
            aU2DocStatDueD.Text = dataApp.aU2DocStatDueD_rep;
            aU2DocStatRd.Text = dataApp.aU2DocStatRd_rep;
            aU2DocStatN.Text = dataApp.aU2DocStatN;

            aUDNOrderedD.Text = dataApp.aUDNOrderedD_rep;
            aUDNDeactivatedD.Text = dataApp.aUDNDeactivatedD_rep;
            aUdnOrderId.Text = dataApp.aUdnOrderId;

            sEstCloseD.Text = dataLoan.sEstCloseD_rep;
            sEstCloseDLckd.Checked = dataLoan.sEstCloseDLckd;
            sEstCloseD.ToolTip = "Hint:  Enter 't' for today's date.";
            sEstCloseN.Text = dataLoan.sEstCloseN;

            sClosingServOd.Text = dataLoan.sClosingServOd_rep;
            sClosingServDueD.Text = dataLoan.sClosingServDueD_rep;
            sClosingServDocumentD.Text = dataLoan.sClosingServDocumentD_rep;
            sClosingServRd.Text = dataLoan.sClosingServRd_rep;
            sClosingServN.Text = dataLoan.sClosingServN;

            sFloodCertOd.Text = dataLoan.sFloodCertOd_rep;
            sFloodCertDueD.Text = dataLoan.sFloodCertDueD_rep;
            sFloodCertificationDeterminationD.Text = dataLoan.sFloodCertificationDeterminationD_rep;
            sFloodCertRd.Text = dataLoan.sFloodCertRd_rep;
            sFloodCertN.Text = dataLoan.sFloodCertN;

            sUSPSCheckOd.Text = dataLoan.sUSPSCheckOd_rep;
            sUSPSCheckDueD.Text = dataLoan.sUSPSCheckDueD_rep;
            sUSPSCheckDocumentD.Text = dataLoan.sUSPSCheckDocumentD_rep;
            sUSPSCheckRd.Text = dataLoan.sUSPSCheckRd_rep;
            sUSPSCheckN.Text = dataLoan.sUSPSCheckN;

            sPayDemStmntOd.Text = dataLoan.sPayDemStmntOd_rep;
            sPayDemStmntDueD.Text = dataLoan.sPayDemStmntDueD_rep;
            sPayDemStmntDocumentD.Text = dataLoan.sPayDemStmntDocumentD_rep;
            sPayDemStmntRd.Text = dataLoan.sPayDemStmntRd_rep;
            sPayDemStmntN.Text = dataLoan.sPayDemStmntN;

            sCAIVRSOd.Text = dataLoan.sCAIVRSOd_rep;
            sCAIVRSDueD.Text = dataLoan.sCAIVRSDueD_rep;
            sCAIVRSDocumentD.Text = dataLoan.sCAIVRSDocumentD_rep;
            sCAIVRSRd.Text = dataLoan.sCAIVRSRd_rep;
            sCAIVRSN.Text = dataLoan.sCAIVRSN;

            sFraudServOd.Text = dataLoan.sFraudServOd_rep;
            sFraudServDueD.Text = dataLoan.sFraudServDueD_rep;
            sFraudServDocumentD.Text = dataLoan.sFraudServDocumentD_rep;
            sFraudServRd.Text = dataLoan.sFraudServRd_rep;
            sFraudServN.Text = dataLoan.sFraudServN;

            sAVMOd.Text = dataLoan.sAVMOd_rep;
            sAVMDueD.Text = dataLoan.sAVMDueD_rep;
            sAVMDocumentD.Text = dataLoan.sAVMDocumentD_rep;
            sAVMRd.Text = dataLoan.sAVMRd_rep;
            sAVMN.Text = dataLoan.sAVMN;

            sHOACertOd.Text = dataLoan.sHOACertOd_rep;
            sHOACertDueD.Text = dataLoan.sHOACertDueD_rep;
            sHOACertDocumentD.Text = dataLoan.sHOACertDocumentD_rep;
            sHOACertRd.Text = dataLoan.sHOACertRd_rep;
            sHOACertN.Text = dataLoan.sHOACertN;

            sEstHUDOd.Text = dataLoan.sEstHUDOd_rep;
            sEstHUDDueD.Text = dataLoan.sEstHUDDueD_rep;
            sEstHUDDocumentD.Text = dataLoan.sEstHUDDocumentD_rep;
            sEstHUDRd.Text = dataLoan.sEstHUDRd_rep;
            sEstHUDN.Text = dataLoan.sEstHUDN;

            sCPLAndICLOd.Text = dataLoan.sCPLAndICLOd_rep;
            sCPLAndICLDueD.Text = dataLoan.sCPLAndICLDueD_rep;
            sCPLAndICLDocumentD.Text = dataLoan.sCPLAndICLDocumentD_rep;
            sCPLAndICLRd.Text = dataLoan.sCPLAndICLRd_rep;
            sCPLAndICLN.Text = dataLoan.sCPLAndICLN;

            sWireInstructOd.Text = dataLoan.sWireInstructOd_rep;
            sWireInstructDueD.Text = dataLoan.sWireInstructDueD_rep;
            sWireInstructDocumentD.Text = dataLoan.sWireInstructDocumentD_rep;
            sWireInstructRd.Text = dataLoan.sWireInstructRd_rep;
            sWireInstructN.Text = dataLoan.sWireInstructN;

            sInsurancesOd.Text = dataLoan.sInsurancesOd_rep;
            sInsurancesDueD.Text = dataLoan.sInsurancesDueD_rep;
            sInsurancesDocumentD.Text = dataLoan.sInsurancesDocumentD_rep;
            sInsurancesRd.Text = dataLoan.sInsurancesRd_rep;
            sInsurancesN.Text = dataLoan.sInsurancesN;

            sQCCompDate.Text = dataLoan.sQCCompDate_rep;
            sQCCompDateTime.Text = dataLoan.sQCCompDateTime_rep;

            this.CreditReportAuthorizationCtrl.BindDataToControl(dataLoan, dataApp);

            this.RespaDates.LoadData(
                dataLoan,
                this.UserHasWorkflowPrivilege(WorkflowOperations.ClearRespaFirstEnteredDates));
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.PageID = "StatusProcessing";
            this.PageTitle = "Processing";
            this.EnableJqueryMigrate = false;
            Tools.Bind_sRedisclosureMethodT(sDocMagicRedisclosureMethodT);
            Tools.Bind_aPreFundVoeTypeT(aBPreFundVoeTypeT);
            Tools.Bind_aPreFundVoeTypeT(aCPreFundVoeTypeT);
            BindLeadSource();
        }

        private void BindLeadSource()
        {
            // 5/5/14 gf - opm 172380, always add a blank option for the default
            // value. We do not want the UI to display misleading data before the
            // page is saved because there are now pricing rules and fee service
            // rules which depend on this field.
            sLeadSrcDesc.Items.Add(new ListItem("", ""));

            foreach (LeadSource desc in Broker.LeadSources)
            {
                sLeadSrcDesc.Items.Add(new ListItem(desc.Name, desc.Name));
            }
        }

        private void UpdateTextBox(string id, TextBox tb, CPageData dataLoan)
        {
            if (dataLoan.IsCustomFieldGloballyDefined(id))
            {
                tb.ReadOnly = true;
                tb.BackColor = Color.Gainsboro;
                tb.ForeColor = Color.Black;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
