﻿#region Generated Code
namespace LendersOfficeApp.newlos.Status
#endregion
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.CreditReportAuthorization;
    using LendersOffice.ObjLib.Edocs.AutoSaveDocType;
    using LendersOffice.PdfForm;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a service for processing data for the verbal authorization borrower picker.
    /// </summary>
    public partial class VerbalAuthorizationBorrowerPickerService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Gets the user for the service request.
        /// </summary>
        private AbstractUserPrincipal BrokerUser => PrincipalFactory.CurrentPrincipal;

        /// <summary>
        /// Gets the loan ID for the service request.
        /// </summary>
        private Guid LoanId => this.GetGuid("loanid");

        /// <summary>
        /// Gets the application ID for the service request.
        /// </summary>
        private Guid AppId => this.GetGuid("applicationid");

        /// <summary>
        /// Processes the method specified by <paramref name="methodName"/>.
        /// </summary>
        /// <param name="methodName">The name of the action being requested.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "SaveVerbalAuthorization":
                    this.SaveVerbalAuthorization();
                    break;
                default:
                    throw new UnhandledCaseException(methodName);
            }
        }

        /// <summary>
        /// Saves verbal authorization data.
        /// </summary>
        private void SaveVerbalAuthorization()
        {
            var broker = this.BrokerUser.BrokerDB;
            var loan = CPageData.CreateUsingSmartDependency(this.LoanId, typeof(VerbalAuthorizationBorrowerPickerService));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            var app = loan.GetAppData(this.AppId);

            var authorizationDate = this.GetString("AuthorizationDate");
            var borrowerAuthorized = this.GetBool("BorrowerAuthorized");
            var coborrowerAuthorized = this.GetBool("CoborrowerAuthorized");

            var auditItem = new VerbalCreditAuthorizationAuditItem(
                this.BrokerUser,
                authorizationDate,
                borrowerAuthorized ? app.aBNm : null,
                coborrowerAuthorized ? app.aCNm : null);

            Guid documentId;
            if (broker.VerbalCreditReportAuthorizationCustomPdfId.HasValue)
            {
                documentId = SaveVerbalAuthorizationFromBrokerCustomPdf(broker);
            }
            else
            {
                documentId = SaveDefaultAuthorizationDocument(auditItem, broker);
            }

            if (borrowerAuthorized)
            {
                app.aBCreditAuthorizationD_rep = authorizationDate;
                app.aBCreditReportAuthorizationProvidedVerbally = true;
                DocumentAssociation.Save(broker.BrokerID, this.AppId, isCoborrower: false, documentId: documentId);
            }

            if (coborrowerAuthorized)
            {
                app.aCCreditAuthorizationD_rep = authorizationDate;
                app.aCCreditReportAuthorizationProvidedVerbally = true;
                DocumentAssociation.Save(broker.BrokerID, this.AppId, isCoborrower: true, documentId: documentId);
            }

            loan.RecordAuditOnSave(auditItem);
            loan.Save();
        }

        /// <summary>
        /// Saves the authorization document using the broker's selected custom PDF.
        /// </summary>
        /// <param name="broker">
        /// The broker for the user saving the verbal authorization.
        /// </param>
        /// <returns>
        /// The ID of the created document.
        /// </returns>
        private Guid SaveVerbalAuthorizationFromBrokerCustomPdf(BrokerDB broker)
        {
            var pdfForm = PdfForm.LoadById(broker.VerbalCreditReportAuthorizationCustomPdfId.Value);

            LendersOffice.PdfLayout.PdfFormLayout unused;
            var pdfData = pdfForm.GeneratePrintPdfAndPdfLayout(this.LoanId, this.AppId, recordId: Guid.Empty, pdfLayout: out unused);

            Guid? savedPdfId = AutoSaveDocTypeFactory.SavePdfDocAsSystem(
                E_AutoSavePage.VerbalCreditAuthorization, 
                pdfData, 
                broker.BrokerID,
                this.LoanId, 
                this.AppId, 
                description: null,
                enforceFolderPermissionsT: E_EnforceFolderPermissions.False);

            if (savedPdfId.HasValue)
            {
                return savedPdfId.Value;
            }

            throw new CBaseException(ErrorMessages.FailedToSave, ErrorMessages.FailedToSave);
        }

        /// <summary>
        /// Saves the authorization document using the LQB default verbal authorization form.
        /// </summary>
        /// <param name="auditItem">
        /// The audit item used to create the default verbal authorization form.
        /// </param>
        /// <param name="broker">
        /// The broker for the user saving the verbal authorization.
        /// </param>
        /// <returns>
        /// The ID of the created document.
        /// </returns>
        private Guid SaveDefaultAuthorizationDocument(VerbalCreditAuthorizationAuditItem auditItem, BrokerDB broker)
        {
            var builder = new StringBuilder();
            using (var writer = new XmlTextWriter(new StringWriter(builder)))
            {
                // Set data specific for the PDF, which hides the close button
                // and adds the lender's name. This data is reset before the
                // audit is stored to the loan file.
                auditItem.RenderCloseButton = false;
                auditItem.LenderName = broker.Name;

                auditItem.GenerateXml(writer);

                auditItem.RenderCloseButton = true;
                auditItem.LenderName = null;
            }

            var args = new System.Xml.Xsl.XsltArgumentList();
            args.AddParam("Timestamp", string.Empty, Tools.GetDateTimeDescription(DateTime.Now));

            Func<string> htmlGetter = () => XslTransformHelper.TransformFromEmbeddedResource(
                VerbalCreditAuthorizationAuditItem.XsltResourceName,
                builder.ToString(),
                args);

            Guid? savedPdfId = AutoSaveDocTypeFactory.SaveHtmlDocAsSystem(
                E_AutoSavePage.VerbalCreditAuthorization, 
                htmlGetter, 
                broker.BrokerID,
                this.LoanId, 
                this.AppId, 
                description: null,
                enforceFolderPermissionsT: E_EnforceFolderPermissions.False);

            if (savedPdfId.HasValue)
            {
                return savedPdfId.Value;
            }

            throw new CBaseException(ErrorMessages.FailedToSave, ErrorMessages.FailedToSave);
        }
    }
}