namespace LendersOfficeApp.newlos.Status
{
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.Hmda;
    using LendersOffice.Security;
    using System;

    public class NMLSCallReportServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(NMLSCallReportServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            dataLoan.ByPassFieldSecurityCheck = true;

            // Application Loan Data
            dataLoan.sIsBranchActAsLenderForFileTri = (E_TriState)GetInt("sIsBranchActAsLenderForFileTri");
            dataLoan.sIsBranchActAsOriginatorForFileTri = (E_TriState)GetInt("sIsBranchActAsOriginatorForFileTri");
            dataLoan.sOpenedD_rep = GetString("sOpenedD");
            dataLoan.sClosedD_rep = GetString("sClosedD");
            dataLoan.sHmdaExcludedFromReport = GetBool("sHmdaExcludedFromReport");

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
            {
                dataLoan.sHmdaActionTakenT = GetEnum<HmdaActionTaken>("sHmdaActionTakenT");
            }
            else
            {
                dataLoan.sHmdaActionTaken = GetString("sHmdaActionTaken");
            }

            dataLoan.sHmdaActionD_rep = GetString("sHmdaActionD");

            dataLoan.sNMLSApplicationAmountLckd = GetBool("sNMLSApplicationAmountLckd");
            dataLoan.sNMLSApplicationAmount_rep = GetString("sNMLSApplicationAmount");

            dataLoan.sServicingByUsStartDLckd = GetBool("sServicingByUsStartDLckd");
            dataLoan.sServicingByUsStartD_rep = GetString("sServicingByUsStartD");

            dataLoan.sServicingByUsEndDLckd = GetBool("sServicingByUsEndDLckd");
            dataLoan.sServicingByUsEndD_rep = GetString("sServicingByUsEndD");

            dataLoan.sNMLSServicingTransferInDLckd = GetBool("sNMLSServicingTransferInDLckd");
            dataLoan.sNMLSServicingTransferInD_rep = GetString("sNMLSServicingTransferInD");

            dataLoan.sNMLSServicingTransferOutDLckd = GetBool("sNMLSServicingTransferOutDLckd");
            dataLoan.sNMLSServicingTransferOutD_rep = GetString("sNMLSServicingTransferOutD");

            dataLoan.sNMLSPeriodForDaysDelinquentLckd = GetBool("sNMLSPeriodForDaysDelinquentLckd");
            dataLoan.sNMLSPeriodForDaysDelinquentT = (LendersOffice.NmlsCallReport.E_McrQuarterT)GetInt("sNMLSPeriodForDaysDelinquentT");

            dataLoan.sNMLSDaysDelinquentAsOfPeriodEnd_rep = GetString("sNMLSDaysDelinquentAsOfPeriodEnd");

            dataLoan.sNMLSServicingIntentLckd = GetBool("sNMLSServicingIntentLckd");
            dataLoan.sNMLSServicingIntentT = (E_ServicingStatus)GetInt("sNMLSServicingIntentT");

            // sLT, sLPurposeT, sLienPostT can cause a re-calc of sMortgageLoanT, and sNMLSLoanPurposeT.
            // Order dependency.
            //*--
            dataLoan.sMortgageLoanT = (E_MortgageLoanT)GetInt("sMortgageLoanT");
            dataLoan.sNMLSLoanPurposeT = (E_NMLSLoanPurposeT)GetInt("sNMLSLoanPurposeT");
            dataLoan.sLienPosT = (E_sLienPosT)GetInt("sLienPosT");
            dataLoan.sLPurposeT = (E_sLPurposeT)GetInt("sLPurposeT");
            dataLoan.sLT = (E_sLT)GetInt("sLT");
            //*---

            // Closed Loan Data
            dataLoan.sHmdaPropT = (E_sHmdaPropT)GetInt("sHmdaPropT");
            dataLoan.sHmdaPropTLckd = GetBool("sHmdaPropTLckd");
            dataLoan.sHmdaReportAsHomeImprov = GetBool("sHmdaReportAsHomeImprov");
            dataLoan.sHmdaReportAsHoepaLoan = GetBool("sHmdaReportAsHoepaLoan");


            dataLoan.sLenderFeesCollected_rep = GetString("sLenderFeesCollected");
            dataLoan.sBrokerFeesCollected_rep = GetString("sBrokerFeesCollected");

            dataLoan.sLenderFeesCollectedLckd = GetBool("sLenderFeesCollectedLckd");
            dataLoan.sBrokerFeesCollectedLckd = GetBool("sBrokerFeesCollectedLckd");

            // Expanded Call Report Data
            dataLoan.sLpTemplateNm = GetString("sLpTemplateNm");
            dataLoan.sFinMethT = (E_sFinMethT)GetInt("sFinMethT");

            dataLoan.sJumboT = (E_JumboT)GetInt("sJumboT");
            dataLoan.sDocumentationT = (E_DocumentationT)GetInt("sDocumentationT");

            dataLoan.sIOnlyMon_rep = GetString("sIOnlyMon");
            dataLoan.sIsOptionArm = GetBool("sIsOptionArm");
            dataLoan.sNmlsApplicationDate_rep = GetString("sNmlsApplicationDate");
            dataLoan.sNmlsApplicationDateLckd = GetBool("sNmlsApplicationDateLckd");
            
            
            if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowUnderwritingAccess))
            {
                dataLoan.sCreditScoreLpeQual_rep = GetString("sCreditScoreLpeQual");
            }

            if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(LendersOffice.Security.Permission.AllowAccountantWrite)
                && BrokerUserPrincipal.CurrentPrincipal.HasPermission(LendersOffice.Security.Permission.AllowAccountantRead))
            {
                dataLoan.sLoanSaleDispositionT = (E_LoanSaleDispositionT)GetInt("sLoanSaleDispositionT");
                dataLoan.sPurchaseAdviceSummaryServicingStatus = GetString("sPurchaseAdviceSummaryServicingStatus");
                dataLoan.sDisbursementDaysOfInterestLckd = GetBool("sDisbursementDaysOfInterestLckd");
                dataLoan.sDisbursementDaysOfInterest_rep = GetString("sDisbursementDaysOfInterest");
            }

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            // Application Loan Data
            SetResult("sBranchChannelT", Tools.GetDescription(dataLoan.sBranchChannelT));
            SetResult("sGseSpT", dataLoan.sGseSpT);
            SetResult("sHmdaPropT", dataLoan.sHmdaPropT);
            SetResult("sNMLSNetChangeApplicationAmount", dataLoan.sNMLSNetChangeApplicationAmount_rep);
            SetResult("sNmlsApplicationDate", dataLoan.sNmlsApplicationDate_rep);

            // Closed Loan Data
            SetResult("sLT", dataLoan.sLT);
            SetResult("sLPurposeT", dataLoan.sLPurposeT);
            SetResult("sLienPosT", dataLoan.sLienPosT);
            SetResult("sLenderFeesCollected", dataLoan.sLenderFeesCollected_rep);
            SetResult("sBrokerFeesCollected", dataLoan.sBrokerFeesCollected_rep);

            // Expanded Call Report Loan Data
            SetResult("sLpTemplateNm", dataLoan.sLpTemplateNm);
            SetResult("sFinMethT", dataLoan.sFinMethT);
            SetResult("sMortgageLoanT", dataLoan.sMortgageLoanT);
            SetResult("sJumboT", dataLoan.sJumboT);
            SetResult("sDocumentationT", dataLoan.sDocumentationT);
            SetResult("sIOnlyMon", dataLoan.sIOnlyMon_rep);
            SetResult("sIsOptionArm", dataLoan.sIsOptionArm);
            SetResult("sHasPrepaymentPenalty", dataLoan.sHasPrepaymentPenalty);
            SetResult("sNMLSLoanPurposeT", dataLoan.sNMLSLoanPurposeT);
            SetResult("sOccT", dataLoan.sOccT);
            SetResult("sHasPrivateMortgageInsurance", dataLoan.sHasPrivateMortgageInsurance);
            SetResult("sHasPiggybackFinancing", dataLoan.sHasPiggybackFinancing);
            SetResult("sCreditScoreLpeQual", dataLoan.sCreditScoreLpeQual_rep);
            SetResult("sNMLSLtvR", dataLoan.sNMLSLtvR_rep);
            SetResult("sNMLSCLtvR", dataLoan.sNMLSCLtvR_rep);
            SetResult("sIsBranchActAsOriginatorForFileTri", dataLoan.sIsBranchActAsOriginatorForFileTri.ToString("D"));
            SetResult("sIsBranchActAsLenderForFileTri", dataLoan.sIsBranchActAsLenderForFileTri.ToString("D"));
            if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowAccountantRead))
            {
                SetResult("sLoanSaleDispositionT", dataLoan.sLoanSaleDispositionT);
                SetResult("sPurchaseAdviceSummaryServicingStatus", dataLoan.sPurchaseAdviceSummaryServicingStatus);
                SetResult("sDisbursementDaysOfInterestLckd", dataLoan.sDisbursementDaysOfInterestLckd);
                SetResult("sDisbursementDaysOfInterest", dataLoan.sDisbursementDaysOfInterest_rep);
            }
        }
    }
	/// <summary>
	/// Summary description for HMDAService.
	/// </summary>
    public partial class NMLSCallReportService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new NMLSCallReportServiceItem());
        }


	}
}
