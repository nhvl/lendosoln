﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HomeownerCounselingOrganizationList.aspx.cs" Inherits="LendersOfficeApp.newlos.Status.HomeownerCounselingOrganizationList" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<html>
<head runat="server">
    <title>Homeowner Counseling Organization List</title>
    <style type="text/css">
        .FullWidth
        {
            width: 34em;
        }
        .TitleWidth
        {
            vertical-align: top;
            width: 15em;
            padding: 3px;
            font-weight: bold;
            display: inline-block;
        }
        .City
        {
            width: 24.6em;
        }
        .ZipCode
        {
            width: 3em;
        }
        .Phone, .Email, .Distance
        {
            width: 15em;
        }
        .FormTableSubheader
        {
            padding:3px;
        }
        #GenerateOrgs
        {
            margin-top: 5px;
            margin-left: 5px;
            margin-right: 10px;
        }
        .Row div
        {
            display: inline-block;
        }
        .Row
        {
            padding: .25em;
        }
        .Services, .Languages
        {
            width: 29.4em;
        }
        table
        {
            width: 775px;
        }
        tr.spacer-row td
        {
            padding-bottom: 5px;
        }
    </style>
</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
    <input type="hidden" id="Org0InfoHolder" name="Org0InfoHolder" />
    <input type="hidden" id="Org1InfoHolder" name="Org1InfoHolder" />
    <input type="hidden" id="Org2InfoHolder" name="Org2InfoHolder" />
    <input type="hidden" id="Org3InfoHolder" name="Org3InfoHolder" />
    <input type="hidden" id="Org4InfoHolder" name="Org4InfoHolder" />
    <input type="hidden" id="Org5InfoHolder" name="Org5InfoHolder" />
    <input type="hidden" id="Org6InfoHolder" name="Org6InfoHolder" />
    <input type="hidden" id="Org7InfoHolder" name="Org7InfoHolder" />
    <input type="hidden" id="Org8InfoHolder" name="Org8InfoHolder" />
    <input type="hidden" id="Org9InfoHolder" name="Org9InfoHolder" />
    <div>
    <table cellspacing="0" cellpadding="0">
        <tr>
            <td class="MainRightHeader" nowrap>
                Homeowner Counseling Organization List (10 Closest)
            </td>
        </tr>
        <tr class="spacer-row">
            <td>
                <input type="button" id="GenerateOrgs" value="Retrieve Closest Organizations from CFPB" />
                Last Modified Date: <ml:DateTextBox ID="sHomeownerCounselingOrganizationLastModifiedD" runat="server"></ml:DateTextBox>
                <input type="checkbox" id="sHasCompletedHomeownerCounselingOrganizationList" runat="server" disabled="disabled" /> Has Completed Homeowner Counseling Organization List
            </td>
        </tr>
<asp:Repeater ID="HomeownerCounselingOrgs" runat="server" OnItemDataBound="CounselOrgs_OnItemDataBound">
    <ItemTemplate>
        <tr>
            <td class="HomeownerCounselingOrg">
                <hr />
                <div class="FormTableSubheader">Organization <ml:EncodedLiteral ID="Index" runat="server"></ml:EncodedLiteral></div>
                <div class="Row">
                    <span class="TitleWidth FieldLabel">Name</span>
                    <div><asp:TextBox ID="Name" class="FullWidth Name" runat="server" /></div>
                    <input type="hidden" id="Id" class="Id" runat="server" />
                </div>
                <div class="Row Address">
                    <div>
                        <span class="TitleWidth FieldLabel">Address</span>
                        <asp:TextBox ID="StreetAddress" class="FullWidth StreetAddress" runat="server" />
                    </div>
                    <div>
                        <span class="TitleWidth FieldLabel">&nbsp;</span>
                        <asp:TextBox ID="StreetAddress2" class="FullWidth StreetAddress2" runat="server" />
                    </div>
                    <div>
                        <span class="TitleWidth FieldLabel">&nbsp;</span>
                        <asp:TextBox ID="City" class="City" runat="server" />
                        <ml:StateDropDownList ID="State" class="State" runat="server"></ml:StateDropDownList>
                        <ml:ZipcodeTextBox ID="Zipcode" class="Zipcode" runat="server" preset="zipcode"></ml:ZipcodeTextBox>
                    </div>
                </div>
                <div class="Row">
                    <div class="TitleWidth FieldLabel">Phone</div>
                    <ml:PhoneTextBox ID="Phone" class="Phone" runat="server" preset="phone"></ml:PhoneTextBox>
                </div>
                <div class="Row">
                    <div class="TitleWidth FieldLabel">Email</div>
                    <asp:TextBox ID="Email" class="Email" runat="server" />
                </div>
                <div class="Row">
                    <span class="TitleWidth FieldLabel">Website</span>
                    <asp:TextBox ID="Website" class="FullWidth Website" runat="server" />
                </div>
                <div class="Row">
                    <span class="TitleWidth FieldLabel">Services</span>
                    <asp:TextBox ID="Services" class="Services" runat="server" TextMode="MultiLine" Rows="3" />
                </div>
                <div class="Row">
                    <div class="TitleWidth FieldLabel">Languages</div>
                    <asp:TextBox ID="Languages" class="Languages" runat="server" TextMode="MultiLine" Rows="3" />
                </div>
                <div class="Row">
                    <span class="TitleWidth FieldLabel">Distance</span>
                    <asp:TextBox ID="Distance" class="Distance" runat="server" />&nbsp;miles
                </div>
            </td>
        </tr>
    </ItemTemplate>
</asp:Repeater>
    </table>
    </div>
    </form>

<script type="text/javascript">
    var HousingCounselorDataFromCFPB;
    $(window).on("load", function() {
        $('#GenerateOrgs').click(GenerateOrgs_Click);
        $('.Name, .StreetAddress, .StreetAddress2, .City, .State, .Zipcode,' +
        ' .Phone, .Email, .Website, .Services, .Languages, .Distance').change(refreshCalculation);
    });

    $(function($) {
        //Stuff the contents of our lists into inputs before save
        function updateOrganizationHiddenInputs() {
            var orgs = [];
            $('.HomeownerCounselingOrg').each(function() {
                var name = $(this).find('.Name').val();

                var streetAddr = $(this).find('.StreetAddress').val();
                var streetAddr2 = $(this).find('.StreetAddress2').val();
                var city = $(this).find('.City').val();
                var state = $(this).find('.State').val();
                var zip = $(this).find('.Zipcode').val();

                var phone = $(this).find('.Phone').val();
                var email = $(this).find('.Email').val();
                var website = $(this).find('.Website').val();

                var services = $(this).find('.Services').val();
                var languages = $(this).find('.Languages').val();
                var distance = $(this).find('.Distance').val();
                var id = $(this).find('.Id').val();

                var address =
                    {
                        'StreetAddress': streetAddr,
                        'StreetAddress2': streetAddr2,
                        'City': city,
                        'State': state,
                        'Zipcode': zip
                    };

                var current = {
                    'Name': name,
                    'Address': address,
                    'Phone': phone,
                    'Email': email,
                    'Website': website,
                    'Services': services,
                    'Languages': languages,
                    'Distance': distance,
                    'Id': id
                };

                orgs.push(current);
            });

            for (var i = 0; i < 10; i++)
            {
                $('#Org' + i + 'InfoHolder').val(JSON.stringify(orgs[i]));
            }
        }

        var oldSaveMe = saveMe;
        saveMe = function(bRefresh) {
            updateOrganizationHiddenInputs();
            return oldSaveMe(bRefresh);
        }

        var oldRefreshCalculation = refreshCalculation;
        refreshCalculation = function () {
            updateOrganizationHiddenInputs();
            oldRefreshCalculation();
            updateLastModifiedDate();
        }
    });

    function GenerateOrgs_Click() {
        
        if (ML.aBZip == null || ML.aBZip.length === 0) {
            alert("Borrower does not have a present address zip code filled out.");
            return;
        }

        var isValidZip = /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(ML.aBZip);

        if (!isValidZip) {
            alert("Borrower's present address zip code " + ML.aBZip + " is not valid.");
            return;
        }

        reloadDataFromCFPB();
        GenerateOrgsFromCFPB();
    }

    function GenerateOrgsFromCFPB() {
        var errorMessage = '';
        if (typeof HousingCounselorDataFromCFPB !== 'object') {
            errorMessage = "Unable to load data from CFPB";
        } else if (UnableToFindAgencies(HousingCounselorDataFromCFPB.error, HousingCounselorDataFromCFPB.counseling_agencies)) {
            errorMessage = "CFPB was unable to find housing counselor data for zip code " + ML.aBZip + " of the borrower's present address.";
        } else if (HousingCounselorDataFromCFPB.counseling_agencies) {
            var orgs = HousingCounselorDataFromCFPB.counseling_agencies;
            if (orgs.length < 10) {
                errorMessage = "We were unable to load all 10 Counseling Organizations from CFPB";
            }
            var i = 0;
            $('.HomeownerCounselingOrg').each(function() {
                if (i < orgs.length)
                    PopulateToUI(this, orgs[i++]);
            });
            updateLastModifiedDate();
            updateDirtyBit();
        } else if (HousingCounselorDataFromCFPB.server_error) {
            errorMessage = HousingCounselorDataFromCFPB.server_error;
        } else {
            errorMessage = "An unexpected error occurred.";
        }
        if (errorMessage != '') {
            alert(errorMessage);
        }
    }

    function UnableToFindAgencies(error, agencies) {
        var noAgenciesReturned = typeof agencies === "undefined" || agencies.length === 0;

        return typeof error !== "undefined" || noAgenciesReturned;
    }

    function PopulateToUI(item, organization) {
        $(item).find('.Name').val(organization.nme);

        $(item).find('.StreetAddress').val(organization.adr1 || organization.mailingadr1);
        $(item).find('.StreetAddress2').val(organization.adr2 || organization.mailingadr2);
        $(item).find('.City').val(organization.city || organization.mailingcity);
        $(item).find('.State').val(organization.statecd || organization.mailingstatecd);
        $(item).find('.Zipcode').val(organization.zipcd.split('-')[0] || organization.mailingzipcd.split('-')[0]); //XXXXX-XXXX => XXXXX

        var phone = organization.phone1 || (organization.phone2 || organization.fax); //XXX-XXX-XXXX, we want (XXX) XXX-XXXX
        var firstDash = phone.indexOf('-');
        var formattedPhone = '';
        if (firstDash > 0) {
            formattedPhone = '(' + phone.substring(0, firstDash) + ') ' + phone.substring(firstDash + 1);
        }
        $(item).find('.Phone').val(formattedPhone || phone);
        $(item).find('.Email').val(organization.email);
        $(item).find('.Website').val(organization.weburl);

        $(item).find('.Services').val(organization.services);
        $(item).find('.Languages').val(organization.languages);
        $(item).find('.Distance').val(organization.distance);
    }

    function reloadDataFromCFPB() {     
        $j.ajax({
            url: 'https://s3.amazonaws.com/files.consumerfinance.gov/a/assets/hud/jsons/' + ML.aBZip + '.json',
            dataType: 'json',
            async: false
        }).then(function(d){
            HousingCounselorDataFromCFPB = d;
        });
    }

    function updateLastModifiedDate() {
        $('#sHomeownerCounselingOrganizationLastModifiedD').val(dtToString(new Date));
    }
</script>
</body>
</html>
