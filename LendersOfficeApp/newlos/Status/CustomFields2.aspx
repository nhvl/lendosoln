<%@ Page language="c#" Codebehind="CustomFields2.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Status.CustomFields2" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head runat="server">
    <title>CustomFields</title>
  </head>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro">
	
    <form id="CustomFields" method="post" runat="server">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
		<td nowrap class="MainRightHeader">Custom Fields</td>
    </tr>
    <tr>
		<td>
		
			<table class="FormTable" cellSpacing="0" cellpadding="2" border="1" rules="all" style="border-collapse:collapse;">
			<tr class="GridHeader">
				<td></td>
				<td style="white-space: nowrap">Item Description</td>
				<td>Date</td>
				<td>Amount</td>
				<td>Percentage</td>
				<td>Checked?</td>
				<td>Notes</td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel"  style="white-space: nowrap">Field Set 21</td>
				<td style="white-space: nowrap"><asp:TextBox id="sCustomField21Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td style="white-space: nowrap"><ml:DateTextBox id="sCustomField21D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td style="white-space: nowrap"><ml:MoneyTextBox id="sCustomField21Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td style="white-space: nowrap"><ml:percenttextbox id="sCustomField21Pc" runat="server" width="70" preset="percent" /></td>
				<td style="white-space: nowrap"><asp:CheckBox id="sCustomField21Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField21Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 22</td>
				<td><asp:TextBox id="sCustomField22Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField22D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField22Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField22Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField22Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField22Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 23</td>
				<td><asp:TextBox id="sCustomField23Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField23D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField23Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField23Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField23Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField23Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 24</td>
				<td><asp:TextBox id="sCustomField24Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField24D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField24Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField24Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField24Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField24Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 25</td>
				<td><asp:TextBox id="sCustomField25Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField25D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField25Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField25Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField25Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField25Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 26</td>
				<td><asp:TextBox id="sCustomField26Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField26D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField26Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField26Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField26Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField26Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 27</td>
				<td><asp:TextBox id="sCustomField27Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField27D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField27Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField27Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField27Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField27Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 28</td>
				<td><asp:TextBox id="sCustomField28Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField28D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField28Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField28Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField28Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField28Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 29</td>
				<td><asp:TextBox id="sCustomField29Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField29D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField29Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField29Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField29Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField29Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel" style="white-space: nowrap">Field Set 30</td>
				<td><asp:TextBox id="sCustomField30Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField30D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField30Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField30Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField30Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField30Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 31</td>
				<td><asp:TextBox id="sCustomField31Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField31D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField31Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField31Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField31Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField31Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 32</td>
				<td><asp:TextBox id="sCustomField32Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField32D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField32Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField32Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField32Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField32Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 33</td>
				<td><asp:TextBox id="sCustomField33Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField33D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField33Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField33Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField33Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField33Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 34</td>
				<td><asp:TextBox id="sCustomField34Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField34D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField34Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField34Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField34Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField34Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 35</td>
				<td><asp:TextBox id="sCustomField35Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField35D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField35Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField35Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField35Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField35Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 36</td>
				<td><asp:TextBox id="sCustomField36Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField36D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField36Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField36Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField36Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField36Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 37</td>
				<td><asp:TextBox id="sCustomField37Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField37D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField37Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField37Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField37Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField37Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 38</td>
				<td><asp:TextBox id="sCustomField38Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField38D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField38Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField38Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField38Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField38Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 39</td>
				<td><asp:TextBox id="sCustomField39Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField39D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField39Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField39Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField39Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField39Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 40</td>
				<td><asp:TextBox id="sCustomField40Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField40D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField40Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField40Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField40Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField40Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
    
    

     </form>
	
  </body>
</html>
