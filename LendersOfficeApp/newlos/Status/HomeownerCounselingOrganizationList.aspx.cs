﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using MeridianLink.CommonControls;
using DataAccess.HomeownerCounselingOrganizations;
using System.Net;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;

namespace LendersOfficeApp.newlos.Status
{
    public partial class HomeownerCounselingOrganizationList : BaseLoanPage
    {
        protected void PageInit(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            this.PageTitle = "Homeowner Counseling Organization List";
            this.PageID = "HomeownerCounselingOrganizationList";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(HomeownerCounselingOrganizationList));
            dataLoan.InitLoad();

            sHomeownerCounselingOrganizationLastModifiedD.Text = dataLoan.sHomeownerCounselingOrganizationLastModifiedD_rep;
            this.sHasCompletedHomeownerCounselingOrganizationList.Checked = dataLoan.sHasCompletedHomeownerCounselingOrganizationList;

            HomeownerCounselingOrganizationCollection orgs = dataLoan.sHomeownerCounselingOrganizationCollection;
            while (orgs.Count < 10)
                orgs.Add(new HomeownerCounselingOrganization(shouldSetNewId: true));
            HomeownerCounselingOrgs.DataSource = orgs;
            HomeownerCounselingOrgs.DataBind();

            string PrimaryBorrowerZipcode = dataLoan.GetAppData(0).aBZip;
            RegisterJsGlobalVariables("aBZip", PrimaryBorrowerZipcode);
        }
        protected void CounselOrgs_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            Literal index = (Literal)args.Item.FindControl("Index");
            TextBox name = (TextBox)args.Item.FindControl("Name");
            TextBox streetAddress = (TextBox)args.Item.FindControl("StreetAddress");
            TextBox streetAddress2 = (TextBox)args.Item.FindControl("StreetAddress2");
            TextBox city = (TextBox)args.Item.FindControl("City");
            StateDropDownList state = (StateDropDownList)args.Item.FindControl("State");
            ZipcodeTextBox zipcode = (ZipcodeTextBox)args.Item.FindControl("Zipcode");
            PhoneTextBox phone = (PhoneTextBox)args.Item.FindControl("Phone");
            TextBox email = (TextBox)args.Item.FindControl("Email");
            TextBox website = (TextBox)args.Item.FindControl("Website");
            TextBox services = (TextBox)args.Item.FindControl("Services");
            TextBox languages = (TextBox)args.Item.FindControl("Languages");
            TextBox distance = (TextBox)args.Item.FindControl("Distance");
            HtmlInputHidden id = (HtmlInputHidden)args.Item.FindControl("Id");

            zipcode.SmartZipcode(city, state);

            HomeownerCounselingOrganization org = args.Item.DataItem as HomeownerCounselingOrganization;

            index.Text = (args.Item.ItemIndex + 1).ToString();
            name.Text = org.Name;
            streetAddress.Text = org.Address.StreetAddress;
            streetAddress2.Text = org.Address.StreetAddress2;
            city.Text = org.Address.City;
            state.Value = org.Address.State;
            zipcode.Text = org.Address.Zipcode;
            phone.Text = org.Phone;
            email.Text = org.Email;
            website.Text = org.Website;
            services.Text = org.Services;
            languages.Text = org.Languages;
            distance.Text = org.Distance;
            id.Value = org.Id.ToString();
        }
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
