<%@ Import Namespace="LendersOffice.Common" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="Conditions.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Status.Conditions"  validateRequest=true%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Conditions</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
		<style type="text/css">
			.hidden { DISPLAY: none }
			.warning { BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; FONT-WEIGHT: bold; FONT-SIZE: 100%; BORDER-LEFT: black 1px solid; WIDTH: 100%; BORDER-BOTTOM: black 1px solid; BACKGROUND-COLOR: yellow; TEXT-ALIGN: center }
			</style>
</HEAD>
	<body class="RightBackground" MS_POSITIONING="FlowLayout">
		<form id="Conditions" method="post" runat="server">
			<script language="javascript">
				function _init() {
					<% if (IsReadOnly) { %>
						<%= AspxTools.JsGetElementById(AddCondition_btn) %>.disabled = true;
					<% } %>
				} 
				
				function performCheck(o) {
					var id = o.id.replace(/_IsDone/, "_DoneDate");
					if (o.checked) {
						var d = new Date();
						document.getElementById(id).value = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
						} else {
							document.getElementById(id).value = "";
						}
				}
      

				<%--
				// 07/26/2007 av opm 6489 Opens up a dialog that returns the template guid the use chose to import from. 
				// Running javascript from a submit button is not allowed so I added a regular button that calls this when its clicked. 
				// After the moddal is gone and good values are return then this method clicks on the hidden asp submit button to import 
				// the conditions.
				--%>
				function onClickImport() 
				{
					var dlg = new cModalDlg(); 
					var arg = new cGenericArgumentObject();
					dlg.Open(dlg.VRoot + '/newlos/Underwriting/TemplateChooser.aspx', arg, "dialogHeight:375px; dialogWidth:400px;center:yes; resizable:no; scroll:no; status=no; help=no;");
				
					if ( arg.OK ) 
					{
						document.getElementById( "m_ImportTemplateId" ).value = arg.TemplateGuid;
						document.getElementById( "m_ImportTemplateName" ).value = arg.TemplateName;
						document.getElementById( "<%= AspxTools.ClientId(m_ImportFromTemplate) %>").click();
					}			
				
					return false;	
				}
			
      //-->
			</script>
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="MainRightHeader" noWrap>Conditions</TD>
				</TR>
				<TR>
					<TD noWrap><ml:EncodedLabel ID="m_StatusLabel" Runat="server" EnableViewState="False" /></TD>
				</TR>
				<TR>
					<TD noWrap>
						<asp:DataGrid id="m_conditionDG" runat="server" Width="100%" AutoGenerateColumns="False" DataKeyField="RecordId" CssClass="DataGrid">
							<alternatingitemstyle cssclass="GridAlternatingItem"></AlternatingItemStyle>
							<itemstyle cssclass="GridItem"></ItemStyle>
							<headerstyle cssclass="GridHeader"></HeaderStyle>
							<columns>
								<asp:TemplateColumn HeaderText="Done?">
									<itemstyle horizontalalign="Center" width="40px" verticalalign="Top"></ItemStyle>
									<itemtemplate>
									</ItemTemplate>
								</asp:TemplateColumn>
		
								<asp:TemplateColumn HeaderText="Date Done">
									<headerstyle wrap="False"></HeaderStyle>
									<itemstyle wrap="False" width="100px" verticalalign="Top"></ItemStyle>
									<itemtemplate>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Required?">
									<itemstyle horizontalalign="Center" width="65px" verticalalign="Top"></ItemStyle>
									<itemtemplate>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Prior To">
									<itemstyle wrap="False" width="135px" verticalalign="Top"></ItemStyle>
									<itemtemplate>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Description">
									<itemtemplate>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:ButtonColumn Text=" delete " CommandName="Delete">
									<itemstyle width="40px" verticalalign="Top"></ItemStyle>
								</asp:ButtonColumn>
							</Columns>
						</asp:DataGrid>
					</TD>
				</TR>
			</TABLE>
			<asp:Button ID="m_ImportFromTemplate" Runat="server" CssClass="hidden" OnClick="ImportClick" />
			<input type="hidden" id="m_ImportTemplateName" runat="server" NAME="m_ImportTemplateName"> 
			<input type="hidden" id="m_ImportTemplateId" runat="server" NAME="m_ImportTemplateId"> 
			<asp:button id="AddCondition_btn" runat="server" Width="136px" Text="Add new condition" onclick="AddCondition_btn_Click"></asp:button>
			<input type="button" value="Import From Loan Template" onclick="javascript:onClickImport()"> 
			<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg></form></FORM>
	</body>
</HTML>
