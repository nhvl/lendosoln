<%@ Page language="c#" Codebehind="Commission.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Status.Commission" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>Commission</title>
    <style type="text/css">
    table.AgentsTable th { font-size:11px; color:#FFF; font-weight:bold; background-color:#A0A0A0; text-align:left; }
    table.AgentsTable td, table.AgentsTable th { border:solid 1px #808080; padding: 1px 5px; }
    table.AgentsTable { border-collapse:collapse; }
    table.AgentsTable tr.white td { background-color:#FFF; color:#000; }
    table.AgentsTable td a { font-weight:bold; }
    
    #AgentsCommissionsTable th { font-size:11px; color:#FFF; font-weight:bold; background-color:#A0A0A0; text-align:left; }
    #AgentsCommissionsTable td, #AgentsCommissionsTable th { border:solid 1px #808080; padding: 1px 5px; }
    #AgentsCommissionsTable { border-collapse:collapse; }
    #AgentsCommissionsTable td a { font-weight:bold; }
    </style>

  </HEAD>
  <body MS_POSITIONING="FlowLayout" class="EditBackground">
      <script type="text/javascript" src="../../inc/json.js"></script>
<script language=javascript>
<!--
var bInit = true;
function _init() {
  bIsReadOnly     = document.getElementById("_ReadOnly").value === "True"; 
  lockField(document.getElementById("<%= AspxTools.ClientId(sProfitAncillaryFeeLckd) %>"), "sProfitAncillaryFee"); 
  if (bInit){
    AgentsTable.InstantiateTable('AgentsCommissionsTable', AgentCommissions, bIsReadOnly, "AgentHolder")
    bInit = false;
  }
}

function displayPeopleRecord(id) {
  linkMe("AgentRecord.aspx?" + "recordid=" + id + "&appid=" + ML.aAppId + "&source=commissions")
}

    function _postGetAllFormValues(args){
        if (!args) return;        
        args.AgentsTable = AgentsTable.Serialize();
    }
    
    function _postRefreshCalculation(result){
        if (!result) return;
        
        var AgentsCommissionsTable = document.getElementById("AgentsCommissionsTable");
                
        if (AgentsCommissionsTable != null && typeof(result["AgentsTable"]) != "undefined"){
            AgentsTable.InstantiateTable("AgentsCommissionsTable", JSON.parse(result["AgentsTable"]), bIsReadOnly, "AgentHolder");
        }
        
        
    }
//-->
</script>

    <form id="Commission" method="post" onreset="return false;" runat="server">
    <TABLE id=Table1 cellSpacing=0 cellPadding=0 width="750" border=0 class=FormTable>
  <TR>
    <TD class=MainRightHeader noWrap>Commissions</TD></TR>
  <tr>
    <td nowrap style="PADDING-LEFT:5px;" >
    <div style="width:700px;" class="InsetBorder">
      <table  id=Table2 cellspacing=0 cellpadding=0 border=0 width=500>
        <tr>
          <td nowrap>a.</td>
          <td nowrap>Borrower Points</td>
          <td nowrap><ml:PercentTextBox id=sProfitGrossBorPnt runat="server" CssClass="mask" preset="percent" width="70" onchange="refreshCalculation();"></ml:PercentTextBox>&nbsp;+ 
<ml:MoneyTextBox id=sProfitGrossBorMb runat="server" CssClass="mask" preset="money" width="90" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
          <td nowrap>=<ml:MoneyTextBox id=sProfitGrossBor runat="server" CssClass="mask" preset="money" width="90" ReadOnly="True"></ml:MoneyTextBox></td></tr>
        <tr>
          <td nowrap>b.</td>
          <td nowrap>Rebate Fee</td>
          <td nowrap><ml:PercentTextBox id=sProfitRebatePnt runat="server" CssClass="mask" preset="percent" width="70" onchange="refreshCalculation();"></ml:PercentTextBox>&nbsp;+ 
<ml:MoneyTextBox id=sProfitRebateMb runat="server" CssClass="mask" preset="money" width="90" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
          <td nowrap>=<ml:MoneyTextBox id=sProfitRebate runat="server" CssClass="mask" preset="money" width="90" ReadOnly="True"></ml:MoneyTextBox></td></tr>

        <tr>
          <td class=FieldLabel nowrap>c.</td>
          <td class=FieldLabel nowrap>Gross Profit Total (a + b)</td>
          <td nowrap></td>
          <td nowrap style="BORDER-TOP:black 1px solid">=<ml:MoneyTextBox id=sGrossProfit runat="server" CssClass="mask" preset="money" width="90" ReadOnly="True" font-bold="True"></ml:MoneyTextBox></td></tr>
        <tr>
          <td nowrap>d.</td>
          <td nowrap>Total Rep Commission</td>
          <td nowrap align=right>subtract</td>
          <td nowrap>&nbsp; <ml:MoneyTextBox id=sCommissionTotal runat="server" CssClass="mask" preset="money" width="90" ReadOnly="True"></ml:MoneyTextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>e.</td>
          <td class=FieldLabel nowrap>Net Profit (c - d)</td>
          <td nowrap align=right class=FieldLabel></td>
          <td nowrap style="BORDER-TOP:black 1px solid">=<ml:MoneyTextBox id=sNetProfit runat="server" CssClass="mask" preset="money" width="90" ReadOnly="True" font-bold="True"></ml:MoneyTextBox></td></tr>
        <tr>
          <td nowrap colspan=4>&nbsp;</td></tr>
        <tr>
          <td nowrap>f.</td>
          <td nowrap>Ancillary Fees (GFE 805 to 812)</td>
          <td nowrap align=right class=FieldLabel><asp:CheckBox id=sProfitAncillaryFeeLckd runat="server" Text="overwrite" onclick="refreshCalculation();"></asp:CheckBox></td>
          <td nowrap>=<ml:MoneyTextBox id=sProfitAncillaryFee runat="server" width="90" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:MoneyTextBox></td></tr>
        <tr>
          <td nowrap>g.</td>
          <td nowrap>Additional Fees</td>
          <td nowrap align=right></td>
          <td nowrap>=<ml:MoneyTextBox id=sProfitExternalFee runat="server" width="90" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:MoneyTextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>h.</td>
          <td class=FieldLabel nowrap>Total Fees (a + b + f + g)</td>
          <td nowrap align=right class=FieldLabel></td>
          <td nowrap>=<ml:MoneyTextBox id=sProfitTotalFee runat="server" width="90" preset="money" CssClass="mask" readonly="True" font-bold="True"></ml:MoneyTextBox></td>
        </tr>
        </table></div>
        </td>
        </tr>
        
        <tr>
            <td nowrap style="PADDING-LEFT:5px">
                <table class="Insetborder" width="700">
                    <tbody id="AgentHolder">
                        <tr>
                          <td><b>Additional agent commission payments</b></td>
                        </tr>
                        <tr>
                          <td>
                                <table cellpadding="0" cellspacing="0" id="AgentsCommissionsTable">
                                    <thead>
                                        <tr class="GridHeader">                                             
                                            <th>Type</th><th>Agent Name</th><th>Company Name</th><th>Commission Amount</th>
                                            <th>Paid?</th><th>Notes</th><th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                          </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody>
                    <tr>
                      <td>
                        <span style="padding-right:5px;">Total Commission Outstanding</span>
                        <ml:MoneyTextBox ID="sCommissionTotalOutstanding" width="90" preset="money" ReadOnly="true" CssClass="mask" runat="server"></ml:MoneyTextBox>
                        <span style="padding-left:20px; padding-right:5px;">Total Commission Paid</span>
                        <ml:MoneyTextBox ID="sCommissionTotalPaid" width="90" preset="money" ReadOnly="true" CssClass="mask" runat="server"></ml:MoneyTextBox>
                      </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr><td nowrap style="PADDING-LEFT:5px"><table class=Insetborder width=700>
        <tr><td class=FieldLabel nowrap>Event Dates Related to Rebate</td></tr>
        <tr>
          <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp; <a href="javascript:linkMe('General.aspx');" title="Go to General page">Rate 
            Locked Date</a> <ml:datetextbox id=sRLckdD runat="server" width="75" preset="date" readonly="True" IsDisplayCalendarHelper="False"></ml:datetextbox> 
            &nbsp;&nbsp;&nbsp;&nbsp; <a href="javascript:linkMe('General.aspx');" title="Go to General page">Rate Lock Expiration Date</a> <ml:datetextbox id=sRLckdExpiredD runat="server" width="75" preset="date" readonly="True" IsDisplayCalendarHelper="False"></ml:datetextbox></td></tr>          
</table></td></tr>
  <tr>
    <td nowrap style="PADDING-LEFT:5px">
      <table class=InsetBorder id=Table3 cellspacing=0 cellpadding=0 width=700 border=0>
        <tr>
          <td class=FieldLabel nowrap>Accounting Period</td>
          <td nowrap width="100%"><asp:DropDownList id=sAccPeriodMon runat="server"></asp:DropDownList><asp:DropDownList id=sAccPeriodYr runat="server"></asp:DropDownList></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Probability of closing (%)&nbsp; </td>
          <td nowrap><ml:PercentTextBox id=sCloseProbability runat="server" width="70" preset="percent"></ml:PercentTextBox></td>
        </tr>
      </table>
    </td>
  </tr>            
  </TABLE>
 </form>
	<script type="text/javascript">
    <!--
	    var AgentsTable = {

	        currentIndex: 0,
	        alternate: false,
	        tableData: null,
	        table: null,
	        isReadOnly: false,
	        container: null,

	        InstantiateTable: function(tableId, data, isReadOnly, oContainer) {
	            this.container = document.getElementById(oContainer);
	            this.table = document.getElementById(tableId);
	            this.tableData = this.table.tBodies[0];
	            this.isReadOnly = isReadOnly;
	            this.alternate = false;

	            var isDirty = false;
	            if (typeof (document.getElementById("IsUpdate")) != "undefined" && typeof (document.getElementById("IsUpdate").value) != "undefined") {
	                isDirty = document.getElementById("IsUpdate").value == "1";
	            }

	            this.container.style.display = "none";

	            if (!data || data.length < 1) {
	                return;
	            }

	            //clear table
	            while (this.tableData.childNodes.length > 0) {
	                this.tableData.removeChild(this.tableData.childNodes[0]);
	            }

	            for (var i = 0; i < data.length; i++) {
	                this.__createAndAppendAdjustmentRow(i, data[i]);
	            }

	            this.currentIndex = data.length - 1;
	            this.container.style.display = "";

	            if (isDirty && typeof (updateDirtyBit == "function")) updateDirtyBit();
	        },

	        __createAndAppendAdjustmentRow: function(index, data) {

	            if (typeof (readOnly) === 'undefined') {
	                readOnly = false;
	            }

	            var oTBody, oTR, oTD, oInput;
	            oTBody = this.tableData;
	            var cssClass = this.alternate ? 'GridAlternatingItem' : 'GridItem';
	            this.alternate = !this.alternate;
	            oTR = this.__createAndAppendElement(oTBody, 'tr', { 'AgentIndex': index }, cssClass);

	            /*AgentType*/
	            oTD = this.__createAndAppendElement(oTR, 'td', {});
	            oInput = this.__createAndAppendElement(oTD, 'span', {
	                'id': 'AgentType' + index,
	                'innerHTML': data.AgentType
	            });

	            /*AgentName*/
	            oTD = this.__createAndAppendElement(oTR, 'td', {});
	            this.__createAndAppendElement(oTD, 'span', {
	                'id': 'AgentName' + index,
	                'name': 'AgentName' + index,
	                'innerHTML': data.AgentName
	            });

	            /*CompanyName*/
	            oTD = this.__createAndAppendElement(oTR, 'td', {});
	            this.__createAndAppendElement(oTD, 'span', {
	                'id': 'CompanyName' + index,
	                'name': 'CompanyName' + index,
	                'innerHTML': data.CompanyName
	            });

	            /*CommAmt*/
	            oTD = this.__createAndAppendElement(oTR, 'td', {});
	            this.__createAndAppendElement(oTD, 'span', {
	                'id': 'AgentName' + index,
	                'name': 'AgentName' + index,
	                'innerHTML': data.CommissionAmount
	            });

	            //Paid?
	            oTD = this.__createAndAppendElement(oTR, 'td', {});
	            oInput = this.__createAndAppendElement(oTD, 'input', {
	                'id': 'IsPaid' + index,
	                'name': 'IsPaid' + index,
	                'type': 'checkbox'
	            });

	            if (data.IsPaid) {
	                oInput.checked = 'checked';
	            }
	            oInput.disabled = this.isReadOnly;

	                addEventHandler(oInput, 'click', function() { refreshCalculation(); }, false);

	            /*Notes*/
	            oTD = this.__createAndAppendElement(oTR, 'td', {});
	            oInput = this.__createAndAppendElement(oTD, 'input', {
	                'SkipMe': 'SkipMe',
	                'id': 'Notes' + index,
	                'name': 'Notes' + index,
	                'value': data.Notes,
	                'type': 'text'
	            });

	            oInput.readOnly = this.isReadOnly;


	            /*editLink*/
	            oTD = this.__createAndAppendElement(oTR, 'td', {});
	            oInput = this.__createAndAppendElement(oTD, 'a', {
	                'id': 'Record' + index,
	                'innerHTML': 'edit',
	                'href': "#",
	                'recordId': data.RecordId
	            });

	            setDisabledAttr(oInput, this.isReadOnly);
	            if (this.isReadOnly) {
	                    addEventHandler(oInput, 'click', function() { return false; }, false);
	            }
	            else {
	                    addEventHandler(oInput, 'click',
	                        (function(recordId) {
	                            return function() {
	                                displayPeopleRecord(recordId);
	                                return false;
	                            }
	                        })(data.RecordId), false);
	            }


	        },

	        __createAndAppendElement: function(oParent, tag, attributes, className) {
	            var item = document.createElement(tag);

	            for (var attr in attributes) {
	                if (attributes.hasOwnProperty(attr)) {
	                    item.setAttribute(attr, attributes[attr]);
	                    item[attr] = attributes[attr]; // because some attributes don't get picked up with setAttribute and vice versa.
	                }
	            }

	            if (className) {
	                item.className = className;
	            }
	            oParent.appendChild(item);

	            if (tag === 'input') {
	                _initDynamicInput(item);
	            }

	            return item;
	        },

	        __IE8NullFix: function(v) {
	            return v === "" ? "" : v; //http://blogs.msdn.com/jscript/archive/2009/06/23/serializing-the-value-of-empty-dom-elements-using-native-json-in-ie8.aspx
	        },

	        Serialize: function(data) {
	            var g = function(id) { return document.getElementById(id); };
	            var agents = [];
	            for (var i = 0; i < this.tableData.rows.length; i++) {
	                var agent = {
	                    IsPaid: this.__IE8NullFix(g('IsPaid' + i).checked),
	                    Notes: this.__IE8NullFix(g('Notes' + i).value),
	                    RecordId: this.__IE8NullFix(g('Record' + i).getAttribute('recordId'))
	                };

	                agents.push(agent);
	            }
	            return JSON.stringify(agents);
	        }

	    }

    //-->
    </script>
</body>
</HTML>
