﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreditReportAuthorization.ascx.cs" Inherits="LendersOfficeApp.newlos.Status.CreditReportAuthorization" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<style type="text/css">
    .CreditReportAuthorizationControl a.action, .CreditReportAuthorizationControl .verbal-auth-checkbox {
        margin-left: 10px;
    }
    .CreditReportAuthorizationControl .docDescription {
        overflow: auto;
        width: 250px;
        height: 50px;
    }
</style>
<asp:HiddenField ID="CurrentApplicationID" runat="server" />
<asp:HiddenField ID="AllowIndicatingVerbalCreditReportAuthorization" runat="server" />
<table class="CreditReportAuthorizationControl">
    <tr class="borr">
        <td class="FieldLabel">Borrower Credit Report Authorization</td>
        <td>
            <ml:DateTextBox ID="aBCreditAuthorizationD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
            <asp:HiddenField ID="BorrowerCreditReportAuthorizationDocumentId" runat="server" />
            <a class="action">upload doc</a>
            <a class="action">associate doc</a>
            <a class="action verbal-auth">verbal authorization</a>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">Borrower Document</td>
        <td>
            <asp:TextBox ID="BorrowerCreditReportAuthorizationDocumentDescription" runat="server" TextMode="MultiLine" ReadOnly="true" class="docDescription" />
            <a class="action">view doc</a>
            <a class="action">clear doc</a>
            <asp:CheckBox ID="aBCreditReportAuthorizationProvidedVerbally" runat="server" Text="Verbal Authorization" CssClass="verbal-auth-checkbox" />
        </td>
    </tr>
    <tr class="coborr">
        <td class="FieldLabel">Co-Borrower Credit Report Authorization</td>
        <td>
            <ml:DateTextBox ID="aCCreditAuthorizationD" runat="server" CssClass="mask w-input-date" preset="date"></ml:DateTextBox>
            <asp:HiddenField ID="CoBorrowerCreditReportAuthorizationDocumentId" runat="server" />
            <a class="action">upload doc</a>
            <a class="action">associate doc</a>
            <a class="action verbal-auth">verbal authorization</a>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">Co-Borrower Document</td>
        <td>
            <asp:TextBox ID="CoBorrowerCreditReportAuthorizationDocumentDescription" runat="server" TextMode="MultiLine" ReadOnly="true" class="docDescription" />
            <a class="action">view doc</a>
            <a class="action">clear doc</a>
            <asp:CheckBox ID="aCCreditReportAuthorizationProvidedVerbally" runat="server" Text="Verbal Authorization" CssClass="verbal-auth-checkbox" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label class="FieldLabel">
                <asp:CheckBox ID="sAllBorrowersHaveAuthorizedCredit" runat="server" Enabled="false" />
                All Borrowers Have Authorized Credit Check
            </label>
        </td>
    </tr>
</table>
<script type="text/javascript">
    (function () {
        var allowIndicatingVerbalCreditReportAuthorization = <%=AspxTools.JsGetElementById(this.AllowIndicatingVerbalCreditReportAuthorization)%>.value === 'True';

        function saveDocumentAssociation(appId, isCoborr, documentId) {
            var args = {
                LoanId: <%=AspxTools.JsString(this.LoanID)%>,
                AppId: appId,
                IsCoborr: isCoborr,
                DocumentId: documentId
            };
            var result = gService.CreditReportAuthorization.call("SaveDocumentAssociation", args);
            return { success: !result.error, errorMessage: result.UserMessage };
        }

        function viewEdoc(docid) {
            window.open(<%= AspxTools.SafeUrl(DataAccess.Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + docid , '_self');
        }

        function displayVerbalAuthorizationPopupCallback(result) {
            if (result.OK) {
                window.location.reload(true);
            }
        }

        function displayVerbalAuthorizationPopup(isCoborr, appId) {
            if (!allowIndicatingVerbalCreditReportAuthorization) {
                return;
            }

            var url = <%= AspxTools.SafeUrl(Page.ResolveUrl("~/newlos/Status/VerbalAuthorizationBorrowerPicker.aspx?loanId=" + this.LoanID)) %>;
            LQBPopup.Show(url + '&appid=' + appId + '&isCoborr=' + isCoborr, { 
                hideCloseButton: true,
                onReturn: displayVerbalAuthorizationPopupCallback,
                width: 400,
                height: 300
            });
        }

        function BindBorrowerCreditAuthorizationControls(type, $authorizationDocIdInput, appId) {
            var isCoborr = type === 'C',
                $primaryRow = $authorizationDocIdInput.closest('tr'),
                $docInfoRow = $primaryRow.next(),
                $newDocAssociationLinks = $primaryRow.find('a.action'),
                $verbalAuthLinks = $primaryRow.find('a.verbal-auth'),
                $verbalAuthCheckboxes = $docInfoRow.find('.verbal-auth-checkbox');

            function getDocumentId() {
                return $authorizationDocIdInput.val();
            }

            function updateUIForDocAssociation() {
                var documentId = getDocumentId();
                $newDocAssociationLinks.toggle(!documentId);
                $docInfoRow.toggle(!!documentId);<%-- Only display the doc info when there is a doc --%>
                $verbalAuthLinks.toggle(allowIndicatingVerbalCreditReportAuthorization && !documentId);
                $verbalAuthCheckboxes.toggle(allowIndicatingVerbalCreditReportAuthorization);
            }

            function updateDocAssociation(doc) {
                var save = !!doc;
                var action = save ? 'save' : 'clear';
                var docId = save ? doc.DocumentId : '';
                var docDescription = save ? doc.Description : '';
                var result = saveDocumentAssociation(appId, isCoborr, docId);
                if (result.success) {
                    $authorizationDocIdInput.val(docId);
                    $docInfoRow.find('.docDescription').val(docDescription);

                    if (!save && allowIndicatingVerbalCreditReportAuthorization) {
                        if (isCoborr) {
                            <%=AspxTools.JsGetElementById(this.aCCreditReportAuthorizationProvidedVerbally)%>.checked = false;
                        }
                        else {
                            <%=AspxTools.JsGetElementById(this.aBCreditReportAuthorizationProvidedVerbally)%>.checked = false;
                        }
                    }

                    updateUIForDocAssociation();
                } else {
                    alert('Unable to ' + action + ' document association.' + (result.errorMessage ? '\n' + result.errorMessage : ''));
                }
            }

            $newDocAssociationLinks.filter(':contains(upload)').click(function(e) {
                e.preventDefault();
                LQBPopup.Show(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/newlos/ElectronicDocs/SingleDocumentUploader.aspx?loanid=" + this.LoanID + "&appid=")) %> + appId, {
                    onReturn: function (dialogArgs) {
                        if (dialogArgs.OK) {
                            updateDocAssociation(dialogArgs.Document);
                        }
                    },
                    hideCloseButton: true
                });
            });
            $newDocAssociationLinks.filter(':contains(associate)').click(function(e) {
                e.preventDefault();
                LQBPopup.Show(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/newlos/ElectronicDocs/SingleDocumentPicker.aspx?loanid=" + this.LoanID)) %>, {
                    onReturn: function (dialogArgs) {
                        if (dialogArgs.OK) {
                            updateDocAssociation(dialogArgs.Document);
                        }
                    },
                    hideCloseButton: true
                });
            });
            $verbalAuthLinks.click(function(e) {
                e.preventDefault();
                displayVerbalAuthorizationPopup(isCoborr, appId);
            });
            $docInfoRow.find('a.action:contains(view)').click(function(e) {
                e.preventDefault();
                viewEdoc(getDocumentId());
            });
            $docInfoRow.find('a.action:contains(clear)').click(function(e) {
                e.preventDefault();
                updateDocAssociation(null);
            });

            updateUIForDocAssociation();
        }

        var appId = <%= AspxTools.JsGetElementById(CurrentApplicationID) %>.value;
        BindBorrowerCreditAuthorizationControls('B', <%= AspxTools.JQuery(this.BorrowerCreditReportAuthorizationDocumentId) %>, appId);
        BindBorrowerCreditAuthorizationControls('C', <%= AspxTools.JQuery(this.CoBorrowerCreditReportAuthorizationDocumentId) %>, appId);
        if (typeof refreshCalculation === 'function')
        {
            <%= AspxTools.JQuery(this.aBCreditAuthorizationD) %>.change(refreshCalculation);
            <%= AspxTools.JQuery(this.aCCreditAuthorizationD) %>.change(refreshCalculation);
        }
    })();
</script>
