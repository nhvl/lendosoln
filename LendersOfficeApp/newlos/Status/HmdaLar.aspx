﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HmdaLar.aspx.cs" Inherits="LendersOfficeApp.newlos.Status.HmdaLar" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>HMDA LAR Data</title>
    <style>
        #DataTable {
            margin-top: 10px;
        }
        .FormTableHeader th {
            white-space: nowrap;
            line-height: 16px;
        }
        input[type='text'] {
            width: 200px;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
    <table class="FormTable">
        <tr class="InsetSpacer">
            <td class="MainRightHeader">HMDA LAR Data</td>
        </tr>
    </table>

    <table id="DataTable">
        <tr class="FormTableHeader">
            <th>Data Field Number</th>
            <th>Data Field Name</th>
            <th>Value</th>
        </tr>
        <tr>
            <td>2</td>
            <td>Legal Entity Identifier (LEI)</td>
            <td><input id="sLegalEntityIdentifier" type="text" readonly="true" runat="server" /></td>
        </tr>
        <tr>
            <td>3</td>
            <td>Universal Loan Identifier (ULI)</td>
            <td>
                <input id="sUniversalLoanIdentifier" type="text" maxlength="45" runat="server" /> 
                <input id="sUniversalLoanIdentifierLckd" type="checkbox" runat="server" onclick="refreshCalculation();" /> 
                <label class="vertical-align-label">Lock</label>
            </td>
        </tr>
        <tr>
            <td>4</td>
            <td>HMDA Application Date</td>
            <td>
                <ml:DateTextBox ID="sHmdaApplicationDate" runat="server" allowna="true" />
                <input id="sHmdaApplicationDateLckd" type="checkbox" runat="server" onclick="refreshCalculation();" /> 
                <label class="vertical-align-label">Lock</label>
            </td>
        </tr>
        <tr>
            <td>5</td>
            <td>Loan Type</td>
            <td><asp:DropDownList ID="sHmdaLoanTypeT" runat="server" Enabled="false"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>6</td>
            <td>Loan Purpose</td>
            <td><asp:DropDownList ID="sHmdaLoanPurposeT" runat="server" Enabled="false"></asp:DropDownList>
                <input id="sHmdaLoanPurposeTLckd" type="checkbox" runat="server" onclick="refreshCalculation();"/> 
                <label class="vertical-align-label">Lock</label>
            </td>
        </tr>
        <tr>
            <td>7</td>
            <td>Preapproval</td>
            <td>
                <asp:DropDownList ID="sHmdaPreapprovalT" runat="server" Enabled="false"></asp:DropDownList>
                <input id="sHmdaPreapprovalTLckd" disabled="disabled" type="checkbox" runat="server" /> 
                <label class="vertical-align-label">Lock</label>
            </td>
        </tr>
        <tr>
            <td>8</td>
            <td>Construction Method</td>
            <td><asp:DropDownList ID="sHmdaConstructionMethT" runat="server" Enabled="false"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>9</td>
            <td>Occupancy Type</td>
            <td><asp:DropDownList ID="sOccT" runat="server" Enabled="false"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>10</td>
            <td>Final Loan Amount</td>
            <td>
                <input id="sHmdaLoanAmount" type="text" runat="server" preset="money" />
                <input id="sHmdaLoanAmountLckd" type="checkbox" runat="server" onclick="refreshCalculation();" /> 
                <label class="vertical-align-label">Lock</label>
            </td>
        </tr>
        <tr>
            <td>11</td>
            <td>Action Taken</td>
            <td>
                <ml:combobox id="sHmdaActionTaken" runat="server" ReadOnly="true" CssClass="sHmdaActionTaken"></ml:combobox>
                <asp:DropDownList Enabled="false" ID="sHmdaActionTakenT" runat="server" CssClass="sHmdaActionTakenT"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>12</td>
            <td>Action Taken Date</td>
            <td><ml:DateTextBox ID="sHmdaActionD" runat="server" ReadOnly="true"></ml:DateTextBox></td>
        </tr>
        <tr>
            <td>13</td>
            <td>Subject Property Street Address</td>
            <td>
                <input id="sHmdaSpAddr" type="text" readonly="true" runat="server" />
                <input id="sHmdaSpAddrNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>14</td>
            <td>Subject Property City</td>
            <td>
                <input id="sHmdaSpCity" type="text" readonly="true" runat="server" />
                <input id="sHmdaSpCityNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>15</td>
            <td>Subject Property State</td>
            <td>
                <input id="sHmdaSpState" type="text" readonly="true" runat="server" />
                <input id="sHmdaSpStateNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>16</td>
            <td>Subject Property ZIP Code</td>
            <td>
                <input id="sHmdaSpZip" type="text" readonly="true" runat="server" />
                <input id="sHmdaSpZipNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>17</td>
            <td>Subject Property County</td>
            <td>
                <input id="sHmda2018CountyCode" type="text" readonly="true" runat="server" />
                <input id="sHmda2018CountyCodeNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>18</td>
            <td>Subject Property Census Tract</td>
            <td>
                <input id="sHmda2018CensusTract" type="text" readonly="true" runat="server" />
                <input id="sHmda2018CensusTractNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>19</td>
            <td>Ethnicity of Applicant or Borrower 1</td>
            <td><input id="sHmdaBEthnicity1T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>20</td>
            <td>Ethnicity of Applicant or Borrower 2</td>
            <td><input id="sHmdaBEthnicity2T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>21</td>
            <td>Ethnicity of Applicant or Borrower 3</td>
            <td><input id="sHmdaBEthnicity3T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>22</td>
            <td>Ethnicity of Applicant or Borrower 4</td>
            <td><input id="sHmdaBEthnicity4T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>23</td>
            <td>Ethnicity of Applicant or Borrower 5</td>
            <td><input id="sHmdaBEthnicity5T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>24</td>
            <td>Ethnicity of Applicant or Borrower Description</td>
            <td><input id="sHmdaBEthnicityOtherDescription" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>25</td>
            <td>Ethnicity of Co-Applicant or Co-Borrower 1</td>
            <td><input id="sHmdaCEthnicity1T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>26</td>
            <td>Ethnicity of Co-Applicant or Co-Borrower 2</td>
            <td><input id="sHmdaCEthnicity2T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>27</td>
            <td>Ethnicity of Co-Applicant or Co-Borrower 3</td>
            <td><input id="sHmdaCEthnicity3T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>28</td>
            <td>Ethnicity of Co-Applicant or Co-Borrower 4</td>
            <td><input id="sHmdaCEthnicity4T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>29</td>
            <td>Ethnicity of Co-Applicant or Co-Borrower 5</td>
            <td><input id="sHmdaCEthnicity5T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>30</td>
            <td>Ethnicity of Co-Applicant or Co-Borrower Description</td>
            <td>
                <input id="sHmdaCEthnicityOtherDescription" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>31</td>
            <td>Ethnicity of Applicant or Borrower Collected on Basis of Visual Observation or Surname</td>
            <td> <input id="sHmdaBEthnicityCollectedByObservationOrSurname" runat="server" type="text" readonly /></td>
        </tr>

        <tr>
            <td>32</td>
            <td>Ethnicity of Co-Applicant or Co-Borrower Collected on Basis of Visual Observation or Surname</td>
            <td> <input id="sHmdaCEthnicityCollectedByObservationOrSurname" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>33</td>
            <td>Race of Applicant or Borrower: 1</td>
            <td><input readonly id="sHmdaBRace1T" type="text" runat="server" /></td>
        </tr>
        <tr>
            <td>34</td>
            <td>Race of Applicant or Borrower: 2</td>
            <td><input readonly id="sHmdaBRace2T" type="text" runat="server" /></td>
        </tr>
        <tr>
            <td>35</td>
            <td>Race of Applicant or Borrower: 3</td>
            <td><input readonly id="sHmdaBRace3T" type="text" runat="server" /></td>
        </tr>
        <tr>
            <td>36</td>
            <td>Race of Applicant or Borrower: 4</td>
            <td><input readonly id="sHmdaBRace4T" type="text" runat="server" /></td>
        </tr>
        <tr>
            <td>37</td>
            <td>Race of Applicant or Borrower: 5</td>
            <td><input readonly id="sHmdaBRace5T" type="text" runat="server" /></td>
        </tr>
        <tr>
            <td>38</td>
            <td>Race of Applicant or Borrower: Conditional Free Form Text Field for Code 1</td>
            <td><input readonly id="sHmdaBEnrolledOrPrincipalTribe" type="text" runat="server" /></td>
        </tr>
        <tr>
            <td>39</td>
            <td>Race of Applicant or Borrower: Conditional Free Form Text Field for Code 27</td>
            <td><input readonly id="sHmdaBOtherAsianDescription" type="text" runat="server" /></td>
        </tr>
        <tr>
            <td>40</td>
            <td>Race of Applicant or Borrower: Conditional Free Form Text Field for Code 44</td>
            <td><input readonly id="sHmdaBOtherPacificIslanderDescription" type="text" runat="server" /></td>
        </tr>
        <tr>
            <td>41</td>
            <td>Race of Co-Applicant or Co-Borrower: 1</td>
            <td><input readonly id="sHmdaCRace1T" type="text" runat="server" /></td>
        </tr>
        <tr>
            <td>42</td>
            <td>Race of Co-Applicant or Co-Borrower: 2</td>
            <td><input readonly id="sHmdaCRace2T" type="text" runat="server" /></td>
        </tr>
        <tr>
            <td>43</td>
            <td>Race of Co-Applicant or Co-Borrower: 3</td>
            <td><input readonly id="sHmdaCRace3T" type="text" runat="server" /></td>
        </tr>
        <tr>
            <td>44</td>
            <td>Race of Co-Applicant or Co-Borrower: 4</td>
            <td><input readonly id="sHmdaCRace4T" type="text" runat="server" /></td>
        </tr>
        <tr>
            <td>45</td>
            <td>Race of Co-Applicant or Co-Borrower: 5</td>
            <td><input readonly id="sHmdaCRace5T" type="text" runat="server" /></td>
        </tr>
        <tr>
            <td>46</td>
            <td>Race of Co-Applicant or Co-Borrower: Conditional Free Form Text Field for Code 1</td>
            <td><input readonly id="sHmdaCEnrolledOrPrincipalTribe" type="text" runat="server" /></td>
        </tr>
        <tr>
            <td>47</td>
            <td>Race of Co-Applicant or Co-Borrower: Conditional Free Form Text Field for Code 27</td>
            <td><input readonly id="sHmdaCOtherAsianDescription" type="text" runat="server" /></td>
        </tr>
        <tr>
            <td>48</td>
            <td>Race of Co-Applicant or Co-Borrower: Conditional Free Form Text Field for Code 44</td>
            <td><input readonly id="sHmdaCOtherPacificIslanderDescription" type="text" runat="server" /></td>
        </tr>
        <tr>
            <td>49</td>
            <td> Race of Applicant or Borrower Collected on the Basis of Visual Observation or Surname</td>
            <td><input readonly id="sHmdaBRaceCollectedByObservationOrSurname" type="text" runat="server" /></td>
        </tr>
        <tr>
            <td>50</td>
            <td> Race of Co-Applicant or Co-Borrower Collected on the Basis of Visual Observation or Surname</td>
            <td><input readonly id="sHmdaCRaceCollectedByObservationOrSurname" type="text" runat="server" /></td>
        </tr>

        <tr>
            <td>51</td>
            <td> Sex of Applicant or Borrower</td>
            <td><input readonly id="sHmdaBSexT" runat="server" type="text" /></td>
        </tr>
        <tr>
            <td>52</td>
            <td> Sex of Co-Applicant or Co-Borrower</td>
            <td><input readonly id="sHmdaCSexT" runat="server" type="text" /></td>
        </tr>
        <tr>
            <td>53</td>
            <td> Sex of Applicant or Borrower Collected on the Basis of Visual Observation or Surname</td>
            <td><input readonly id="sHmdaBSexCollectedByObservationOrSurname" runat="server" type="text" /></td>
        </tr>
        <tr>
            <td>54</td>
            <td> Sex of Co-Applicant or Co-Borrower Collected on the Basis of Visual Observation or Surname</td>
            <td><input readonly id="sHmdaCSexCollectedByObservationOrSurname" runat="server" type="text" /></td>
        </tr>

        <tr>
            <td>55</td>
            <td>Age of Applicant or Borrower</td>
            <td>
                <input id="sHmdaBAge" type="text" readonly="true" runat="server" />
                <input id="sHmdaBAgeNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>56</td>
            <td>Age of Co-Applicant or Co-Borrower</td>
            <td>
                <input id="sHmdaCAge" type="text" readonly="true" runat="server" />
                <input id="sHmdaCAgeNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>57</td>
            <td>Total Income of All Borrowers</td>
            <td>
                <input id="sHmdaIncome" type="text" readonly="true" runat="server" />
                <input id="sHmdaIncomeNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>58</td>
            <td>Type of Purchaser</td>
            <td>
                <asp:DropDownList Enabled="false" runat="server" id="sHmdaPurchaser2015T"></asp:DropDownList>
                <input runat="server" type="checkbox" id="sHmdaPurchaser2015TLckd" disabled="disabled" />
            </td>
        </tr>
        <tr>
            <td>59</td>
            <td>Rate Spread</td>
            <td>
                <input id="sHmda2018AprRateSpread" type="text" readonly="true" runat="server" />
                <input id="sHmda2018AprRateSpreadNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>60</td>
            <td>HOEPA Status</td>
            <td><asp:DropDownList ID="sHmdaHoepaStatusT" runat="server"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>61</td>
            <td>Lien Status</td>
            <td><asp:DropDownList ID="sHmdaLienT" Enabled="false" runat="server"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>62</td>
            <td>Credit Score of Applicant or Borrower</td>
            <td>
                <input id="sHmdaBCreditScore" type="text" readonly="true" runat="server" />
                <input id="sHmdaBCreditScoreNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>63</td>
            <td>Credit Score of Co-Applicant or Co-Borrower</td>
            <td>
                <input id="sHmdaCCreditScore" type="text" readonly="true" runat="server" />
                <input id="sHmdaCCreditScoreNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>64</td>
            <td>Credit Score Model of Applicant or Borrower</td>
            <td>
                <asp:DropDownList ID="sHmdaBCreditScoreModelT" runat="server" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>65</td>
            <td>Credit Score Other Model Name and Version of Applicant or Borrower</td>
            <td>
                <ml:ComboBox ID="sHmdaBCreditScoreModelOtherDescription" runat="server" ReadOnly="true" CssClass="credit-model-name-other-description"></ml:ComboBox>
            </td>
        </tr>
        <tr>
            <td>66</td>
            <td>Credit Score Model of Co-Applicant or Co-Borrower</td>
            <td>
                <asp:DropDownList ID="sHmdaCCreditScoreModelT" runat="server" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>67</td>
            <td>Credit Score Other Model Name and Version of Co-Applicant or Co-Borrower</td>
            <td>
                <ml:ComboBox ID="sHmdaCCreditScoreModelOtherDescription" runat="server" ReadOnly="true" CssClass="credit-model-name-other-description"></ml:ComboBox>
            </td>
        </tr>
        <tr>
            <td>68</td>
            <td>Denial Reason #1</td>
            <td><input type="text" id="sHmdaDenialReason1" runat="server" class="HmdaDenialReason" readonly="true" /></td>
        </tr>
        <tr>
            <td>69</td>
            <td>Denial Reason #2</td>
            <td><input type="text" id="sHmdaDenialReason2" runat="server" class="HmdaDenialReason" readonly="true" /></td>
        </tr>
        <tr>
            <td>70</td>
            <td>Denial Reason #3</td>
            <td><input type="text" id="sHmdaDenialReason3" runat="server" class="HmdaDenialReason" readonly="true" /></td>
        </tr>
        <tr>
            <td>71</td>
            <td>Denial Reason #4</td>
            <td><input type="text" id="sHmdaDenialReason4" runat="server" class="HmdaDenialReason" readonly="true" /></td>
        </tr>
        <tr>
            <td>72</td>
            <td>Other Denial Reason Description</td>
            <td><input type="text" id="sHmdaDenialReasonOtherDescription" runat="server" class="HmdaDenialReason OtherReason" readonly="true" /></td>
        </tr>
        <tr>
            <td><span id="TotalLoanCostNum" runat="server">73</span></td>
            <td><label id="sHmdaTotalLoanCostsLabel" runat="server">Loan Costs</label></td>
            <td>
                <input id="sHmdaTotalLoanCosts" type="text" readonly="true" runat="server" />
                <input id="sHmdaTotalLoanCostsNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>75</td>
            <td>Origination Charges</td>
            <td>
                <input id="sHmdaOriginationCharge" type="text" readonly="true" runat="server" />
                <input id="sHmdaOriginationChargeNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>76</td>
            <td>Discount Points</td>
            <td>
                <input id="sHmdaDiscountPoints" type="text" readonly="true" runat="server" />
                <input id="sHmdaDiscountPointsNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>77</td>
            <td>Lender Credits</td>
            <td>
                <input id="sHmdaLenderCredits" type="text" readonly="true" runat="server" />
                <input id="sHmdaLenderCreditsNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>78</td>
            <td>Interest Rate</td>
            <td>
                <input id="sHmdaInterestRate" type="text" readonly="true" runat="server" />
                <input id="sHmdaInterestRateNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>79</td>
            <td>Prepayment Penalty Term</td>
            <td>
                <input id="sHmdaPrepaymentTerm" type="text" readonly="true" runat="server" />
                <input id="sHmdaPrepaymentTermNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>80</td>
            <td>Debt-to-Income Ratio</td>
            <td>
                <input id="sHmdaDebtRatio" type="text" readonly="true" runat="server" />
                <input id="sHmdaDebtRatioNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>81</td>
            <td>Combined Loan-to-Value Ratio</td>
            <td>
                <input id="sHmdaCombinedRatio" type="text" readonly="true" runat="server" />
                <input id="sHmdaCombinedRatioNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>82</td>
            <td>Loan Term</td>
            <td>
                <input id="sHmdaTerm" type="text" readonly="true" runat="server" />
                <input id="sHmdaTermNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>83</td>
            <td>Introductory Rate Period</td>
            <td>
                <input id="sHmdaIntroductoryPeriod" type="text" readonly="true" runat="server" />
                <input id="sHmdaIntroductoryPeriodNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>84</td>
            <td>Balloon Payment</td>
            <td><asp:DropDownList ID="sHmdaBalloonPaymentT" runat="server" disabled="disabled"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>85</td>
            <td>Interest-Only Payments</td>
            <td><asp:DropDownList ID="sHmdaInterestOnlyPaymentT" runat="server" disabled="disabled"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>86</td>
            <td>Negative Amortization</td>
            <td><asp:DropDownList ID="sHmdaNegativeAmortizationT" runat="server" disabled="disabled"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>87</td>
            <td>Other Non-Amortizing Features</td>
            <td><asp:DropDownList ID="sHmdaOtherNonAmortFeatureT" runat="server"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>88</td>
            <td>Property Value</td>
            <td>
                <input id="sHmdaPropertyValue" type="text" readonly="true" runat="server" />
                <input id="sHmdaPropertyValueNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>89</td>
            <td>Manufactured Home Secured Property Type</td>
            <td><asp:DropDownList ID="sHmdaManufacturedTypeT" runat="server"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>90</td>
            <td>Manufactured Home Land Property Interest</td>
            <td><asp:DropDownList ID="sHmdaManufacturedInterestT" runat="server"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>91</td>
            <td>Total Units</td>
            <td>
                <input preset="numeric" id="sHmdaTotalUnits" type="text" runat="server" />
                <label>
                    Lock
                    <input type="checkbox" runat="server" id="sHmdaTotalUnitsLckd" onclick="refreshCalculation();" />
                </label>
            </td>
        </tr>
        <tr>
            <td>92</td>
            <td>Multifamily Affordable Units</td>
            <td>
                <input id="sHmdaMultifamilyUnits" type="text" runat="server" class="data-refresh" />
                <input id="sHmdaMultifamilyUnitsNotApplicable" type="checkbox" runat="server" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>93</td>
            <td>Submission of Application</td>
            <td><asp:DropDownList ID="sHmdaSubmissionApplicationT" runat="server" Enabled="false"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>94</td>
            <td>Initially Payable to Your Institution</td>
            <td><asp:DropDownList ID="sHmdaInitiallyPayableToInstitutionT" runat="server" Enabled="false"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>95</td>
            <td>Mortgage Loan Originator NMLS Identifier</td>
            <td>
                <input id="sHmdaApp1003InterviewerLoanOriginatorIdentifier" type="text" readonly="true" runat="server" />
                <input id="sHmdaApp1003InterviewerLoanOriginatorIdentifierNotApplicable" type="checkbox" runat="server" disabled="disabled" /> 
                <label class="vertical-align-label">NA</label>
            </td>
        </tr>
        <tr>
            <td>96</td>
            <td>Automated Underwriting System: 1</td>
            <td><input id="sHmdaAutomatedUnderwritingSystem1T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>97</td>
            <td>Automated Underwriting System: 2</td>
            <td><input id="sHmdaAutomatedUnderwritingSystem2T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>98</td>
            <td>Automated Underwriting System: 3</td>
            <td><input id="sHmdaAutomatedUnderwritingSystem3T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>99</td>
            <td>Automated Underwriting System: 4</td>
            <td><input id="sHmdaAutomatedUnderwritingSystem4T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>100</td>
            <td>Automated Underwriting System: 5</td>
            <td><input id="sHmdaAutomatedUnderwritingSystem5T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>101</td>
            <td>Automated Underwriting System: Conditional Free Form Text Field for Code 5</td>
            <td><input id="sHmdaAutomatedUnderwritingSystemOtherDescription" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>102</td>
            <td>Automated Underwriting System Result: 1</td>
            <td><input id="sHmdaAutomatedUnderwritingSystemResult1T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>103</td>
            <td>Automated Underwriting System Result: 2</td>
            <td><input id="sHmdaAutomatedUnderwritingSystemResult2T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>104</td>
            <td>Automated Underwriting System Result: 3</td>
            <td><input id="sHmdaAutomatedUnderwritingSystemResult3T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>105</td>
            <td>Automated Underwriting System Result: 4</td>
            <td><input id="sHmdaAutomatedUnderwritingSystemResult4T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>106</td>
            <td>Automated Underwriting System Result: 5</td>
            <td><input id="sHmdaAutomatedUnderwritingSystemResult5T" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>107</td>
            <td>Automated Underwriting System Result: Conditional Free Form Text Field for Code 16</td>
            <td><input id="sHmdaAutomatedUnderwritingSystemResultOtherDescription" runat="server" type="text" readonly /></td>
        </tr>
        <tr>
            <td>108</td>
            <td>Reverse Mortgage</td>
            <td><asp:DropDownList ID="sHmdaReverseMortgageT" runat="server" CssClass="data-refresh"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>109</td>
            <td>Open-End Line of Credit</td>
            <td><input id="sIsLineOfCredit" type="checkbox" disabled="disabled" runat="server" /></td>
        </tr>
        <tr>
            <td>110</td>
            <td>Business or Commercial Purpose</td>
            <td><asp:DropDownList ID="sHmdaBusinessPurposeT" runat="server" CssClass="data-refresh"></asp:DropDownList></td>
        </tr>
    </table>
    </form>
</body>
</html>
