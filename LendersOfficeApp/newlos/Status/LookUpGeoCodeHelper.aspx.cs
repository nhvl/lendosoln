using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Status
{
	public partial class LookUpGeoCodeHelper : BasePage
	{
		//do not change these hiddne input variable names, as they correspond with the text boxes on the geocode webpage
		
		protected void PageLoad(object sender, System.EventArgs e)
		{
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(RequestHelper.LoanID, typeof(LookUpGeoCodeHelper));
			dataLoan.InitLoad();

			txtAddress.Value= dataLoan.sSpAddr;
			txtCity.Value =dataLoan.sSpCity;
			txtZipCode.Value=dataLoan.sSpZip.TrimWhitespaceAndBOM();
            ddlbState.Value = dataLoan.sSpState;
            //ddlbYear.Value = DateTime.Now.Year.ToString(); // SK not sending year appears to default to current year.
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}