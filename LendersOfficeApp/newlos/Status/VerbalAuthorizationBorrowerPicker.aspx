﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VerbalAuthorizationBorrowerPicker.aspx.cs" Inherits="LendersOfficeApp.newlos.Status.VerbalAuthorizationBorrowerPicker" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Select Borrowers for Verbal Authorization</title>
    <style>
        .full-width {
            width: 100%;
        }
        .hidden { 
            display: none;
        }
        .center {
            text-align: center;
        }
        .left-spacer {
            margin-left: 10px;
        }
        .top-padding {
            padding-top: 10px;
        }
        .align-bottom {
            margin-top: 40%;
        }
    </style>
    <script type="text/javascript">
        function validate() {
            var borrowerAuthorized = document.getElementById('BorrowerAuthorized').checked;
            var coborrowerAuthorized = document.getElementById('CoborrowerAuthorized').checked;

            if (!borrowerAuthorized && !coborrowerAuthorized) {
                alert('Please select at least one borrower for verbal authorization.');
                return false;
            }

            return true;
        }

        function onPickVerbalAuthorizationBorrowersCallback(result) {
            if (result.error) {
                alert(result.UserMessage);
                return;
            }

            var args = window.dialogArguments || {};
            args.OK = true;
            onClosePopup(args);
        }

        function onPickVerbalAuthorizationBorrowers() {
            if (!validate()) {
                return;
            }

            var args = getAllFormValues();
            gService.loanedit.callAsyncSimple('SaveVerbalAuthorization', args, onPickVerbalAuthorizationBorrowersCallback);
        }

        function onCancel() {
            var args = window.dialogArguments || {};
            args.OK = false;
            onClosePopup(args);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="MainRightHeader">
        Select borrowers for verbal authorization:
    </div>

    <div id="NoBorrowerDataEntered" runat="server">
        <div class="center top-padding">
            No borrower information on file.
        </div>
        <div class="center align-bottom">
            <input type="button" value="OK" onclick="onCancel()" />
        </div>
    </div>

    <div id="VerbalAuthorizationSection" runat="server" class="full-width">
        <div class="top-padding left-spacer">
            Verbal authorization date:
            <ml:DateTextBox ID="AuthorizationDate" runat="server" CssClass="mask left-spacer" Width="75" preset="date"></ml:DateTextBox>
        </div>
        <div class="left-spacer">
            <input id="BorrowerAuthorized" type="checkbox" runat="server" />
            <label for="BorrowerAuthorized">
                <span id="BorrowerName" runat="server">&nbsp;</span>
            </label>
        </div>
        <div runat="server" id="CoborrowerAuthorizationRow" class="left-spacer">
            <input id="CoborrowerAuthorized" type="checkbox" runat="server" />
            <label for="CoborrowerAuthorized">
                <span id="CoborrowerName" runat="server">&nbsp;</span>
            </label>
        </div>
    </div>

    <div runat="server" id="ButtonRow" class="center align-bottom">
        <input type="button" value="OK" onclick="onPickVerbalAuthorizationBorrowers()" />
        <input type="button" value="Cancel" onclick="onCancel()" />
    </div>
    </form>
</body>
</html>
