///
/// Author: Matthew Flynn
/// 
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MeridianLink.CommonControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Status
{
	public partial class CustomFields : LendersOfficeApp.newlos.BaseLoanPage
	{

        protected void PageLoad(object sender, System.EventArgs e)
        {

        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "Custom Fields 1-20";
            this.PageID = "StatusCustomFields";
            // this.PDFPrintClass = typeof(LendersOffice.Pdf._____);
        }

		protected override void LoadData()
		{
			CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(CustomFields));
			dataLoan.InitLoad();

			sCustomField1Desc.Text = dataLoan.sCustomField1Desc;
			sCustomField2Desc.Text = dataLoan.sCustomField2Desc;
			sCustomField3Desc.Text = dataLoan.sCustomField3Desc;
			sCustomField4Desc.Text = dataLoan.sCustomField4Desc;
			sCustomField5Desc.Text = dataLoan.sCustomField5Desc;
			sCustomField6Desc.Text = dataLoan.sCustomField6Desc;
			sCustomField7Desc.Text = dataLoan.sCustomField7Desc;
			sCustomField8Desc.Text = dataLoan.sCustomField8Desc;
			sCustomField9Desc.Text = dataLoan.sCustomField9Desc;
			sCustomField10Desc.Text = dataLoan.sCustomField10Desc;
			sCustomField11Desc.Text = dataLoan.sCustomField11Desc;
			sCustomField12Desc.Text = dataLoan.sCustomField12Desc;
			sCustomField13Desc.Text = dataLoan.sCustomField13Desc;
			sCustomField14Desc.Text = dataLoan.sCustomField14Desc;
			sCustomField15Desc.Text = dataLoan.sCustomField15Desc;
			sCustomField16Desc.Text = dataLoan.sCustomField16Desc;
			sCustomField17Desc.Text = dataLoan.sCustomField17Desc;
			sCustomField18Desc.Text = dataLoan.sCustomField18Desc;
			sCustomField19Desc.Text = dataLoan.sCustomField19Desc;
			sCustomField20Desc.Text = dataLoan.sCustomField20Desc;



            UpdateTextBox("sCustomField1Desc", sCustomField1Desc, dataLoan);
            UpdateTextBox("sCustomField2Desc", sCustomField2Desc, dataLoan);
            UpdateTextBox("sCustomField3Desc", sCustomField3Desc, dataLoan);
            UpdateTextBox("sCustomField4Desc", sCustomField4Desc, dataLoan);
            UpdateTextBox("sCustomField5Desc", sCustomField5Desc, dataLoan);
            UpdateTextBox("sCustomField6Desc", sCustomField6Desc, dataLoan);
            UpdateTextBox("sCustomField7Desc", sCustomField7Desc, dataLoan);
            UpdateTextBox("sCustomField8Desc", sCustomField8Desc, dataLoan);
            UpdateTextBox("sCustomField9Desc", sCustomField9Desc, dataLoan);
            UpdateTextBox("sCustomField10Desc", sCustomField10Desc, dataLoan);
            UpdateTextBox("sCustomField11Desc", sCustomField11Desc, dataLoan);
            UpdateTextBox("sCustomField12Desc", sCustomField12Desc, dataLoan);
            UpdateTextBox("sCustomField13Desc", sCustomField13Desc, dataLoan);
            UpdateTextBox("sCustomField14Desc", sCustomField14Desc, dataLoan);
            UpdateTextBox("sCustomField15Desc", sCustomField15Desc, dataLoan);
            UpdateTextBox("sCustomField16Desc", sCustomField16Desc, dataLoan);
            UpdateTextBox("sCustomField17Desc", sCustomField17Desc, dataLoan);
            UpdateTextBox("sCustomField18Desc", sCustomField18Desc, dataLoan);
            UpdateTextBox("sCustomField19Desc", sCustomField19Desc, dataLoan);
            UpdateTextBox("sCustomField20Desc", sCustomField20Desc, dataLoan);
            
    
			sCustomField1D.Text = dataLoan.sCustomField1D_rep;
			sCustomField2D.Text = dataLoan.sCustomField2D_rep;
			sCustomField3D.Text = dataLoan.sCustomField3D_rep;
			sCustomField4D.Text = dataLoan.sCustomField4D_rep;
			sCustomField5D.Text = dataLoan.sCustomField5D_rep;
			sCustomField6D.Text = dataLoan.sCustomField6D_rep;
			sCustomField7D.Text = dataLoan.sCustomField7D_rep;
			sCustomField8D.Text = dataLoan.sCustomField8D_rep;
			sCustomField9D.Text = dataLoan.sCustomField9D_rep;
			sCustomField10D.Text = dataLoan.sCustomField10D_rep;
			sCustomField11D.Text = dataLoan.sCustomField11D_rep;
			sCustomField12D.Text = dataLoan.sCustomField12D_rep;
			sCustomField13D.Text = dataLoan.sCustomField13D_rep;
			sCustomField14D.Text = dataLoan.sCustomField14D_rep;
			sCustomField15D.Text = dataLoan.sCustomField15D_rep;
			sCustomField16D.Text = dataLoan.sCustomField16D_rep;
			sCustomField17D.Text = dataLoan.sCustomField17D_rep;
			sCustomField18D.Text = dataLoan.sCustomField18D_rep;
			sCustomField19D.Text = dataLoan.sCustomField19D_rep;
			sCustomField20D.Text = dataLoan.sCustomField20D_rep;
			sCustomField1Money.Text = dataLoan.sCustomField1Money_rep;
			sCustomField2Money.Text = dataLoan.sCustomField2Money_rep;
			sCustomField3Money.Text = dataLoan.sCustomField3Money_rep;
			sCustomField4Money.Text = dataLoan.sCustomField4Money_rep;
			sCustomField5Money.Text = dataLoan.sCustomField5Money_rep;
			sCustomField6Money.Text = dataLoan.sCustomField6Money_rep;
			sCustomField7Money.Text = dataLoan.sCustomField7Money_rep;
			sCustomField8Money.Text = dataLoan.sCustomField8Money_rep;
			sCustomField9Money.Text = dataLoan.sCustomField9Money_rep;
			sCustomField10Money.Text = dataLoan.sCustomField10Money_rep;
			sCustomField11Money.Text = dataLoan.sCustomField11Money_rep;
			sCustomField12Money.Text = dataLoan.sCustomField12Money_rep;
			sCustomField13Money.Text = dataLoan.sCustomField13Money_rep;
			sCustomField14Money.Text = dataLoan.sCustomField14Money_rep;
			sCustomField15Money.Text = dataLoan.sCustomField15Money_rep;
			sCustomField16Money.Text = dataLoan.sCustomField16Money_rep;
			sCustomField17Money.Text = dataLoan.sCustomField17Money_rep;
			sCustomField18Money.Text = dataLoan.sCustomField18Money_rep;
			sCustomField19Money.Text = dataLoan.sCustomField19Money_rep;
			sCustomField20Money.Text = dataLoan.sCustomField20Money_rep;
			sCustomField1Pc.Text = dataLoan.sCustomField1Pc_rep;
			sCustomField2Pc.Text = dataLoan.sCustomField2Pc_rep;
			sCustomField3Pc.Text = dataLoan.sCustomField3Pc_rep;
			sCustomField4Pc.Text = dataLoan.sCustomField4Pc_rep;
			sCustomField5Pc.Text = dataLoan.sCustomField5Pc_rep;
			sCustomField6Pc.Text = dataLoan.sCustomField6Pc_rep;
			sCustomField7Pc.Text = dataLoan.sCustomField7Pc_rep;
			sCustomField8Pc.Text = dataLoan.sCustomField8Pc_rep;
			sCustomField9Pc.Text = dataLoan.sCustomField9Pc_rep;
			sCustomField10Pc.Text = dataLoan.sCustomField10Pc_rep;
			sCustomField11Pc.Text = dataLoan.sCustomField11Pc_rep;
			sCustomField12Pc.Text = dataLoan.sCustomField12Pc_rep;
			sCustomField13Pc.Text = dataLoan.sCustomField13Pc_rep;
			sCustomField14Pc.Text = dataLoan.sCustomField14Pc_rep;
			sCustomField15Pc.Text = dataLoan.sCustomField15Pc_rep;
			sCustomField16Pc.Text = dataLoan.sCustomField16Pc_rep;
			sCustomField17Pc.Text = dataLoan.sCustomField17Pc_rep;
			sCustomField18Pc.Text = dataLoan.sCustomField18Pc_rep;
			sCustomField19Pc.Text = dataLoan.sCustomField19Pc_rep;
			sCustomField20Pc.Text = dataLoan.sCustomField20Pc_rep;
			sCustomField1Bit.Checked = dataLoan.sCustomField1Bit;
			sCustomField2Bit.Checked = dataLoan.sCustomField2Bit;
			sCustomField3Bit.Checked = dataLoan.sCustomField3Bit;
			sCustomField4Bit.Checked = dataLoan.sCustomField4Bit;
			sCustomField5Bit.Checked = dataLoan.sCustomField5Bit;
			sCustomField6Bit.Checked = dataLoan.sCustomField6Bit;
			sCustomField7Bit.Checked = dataLoan.sCustomField7Bit;
			sCustomField8Bit.Checked = dataLoan.sCustomField8Bit;
			sCustomField9Bit.Checked = dataLoan.sCustomField9Bit;
			sCustomField10Bit.Checked = dataLoan.sCustomField10Bit;
			sCustomField11Bit.Checked = dataLoan.sCustomField11Bit;
			sCustomField12Bit.Checked = dataLoan.sCustomField12Bit;
			sCustomField13Bit.Checked = dataLoan.sCustomField13Bit;
			sCustomField14Bit.Checked = dataLoan.sCustomField14Bit;
			sCustomField15Bit.Checked = dataLoan.sCustomField15Bit;
			sCustomField16Bit.Checked = dataLoan.sCustomField16Bit;
			sCustomField17Bit.Checked = dataLoan.sCustomField17Bit;
			sCustomField18Bit.Checked = dataLoan.sCustomField18Bit;
			sCustomField19Bit.Checked = dataLoan.sCustomField19Bit;
			sCustomField20Bit.Checked = dataLoan.sCustomField20Bit;
			sCustomField1Notes.Text = dataLoan.sCustomField1Notes;
			sCustomField2Notes.Text = dataLoan.sCustomField2Notes;
			sCustomField3Notes.Text = dataLoan.sCustomField3Notes;
			sCustomField4Notes.Text = dataLoan.sCustomField4Notes;
			sCustomField5Notes.Text = dataLoan.sCustomField5Notes;
			sCustomField6Notes.Text = dataLoan.sCustomField6Notes;
			sCustomField7Notes.Text = dataLoan.sCustomField7Notes;
			sCustomField8Notes.Text = dataLoan.sCustomField8Notes;
			sCustomField9Notes.Text = dataLoan.sCustomField9Notes;
			sCustomField10Notes.Text = dataLoan.sCustomField10Notes;
			sCustomField11Notes.Text = dataLoan.sCustomField11Notes;
			sCustomField12Notes.Text = dataLoan.sCustomField12Notes;
			sCustomField13Notes.Text = dataLoan.sCustomField13Notes;
			sCustomField14Notes.Text = dataLoan.sCustomField14Notes;
			sCustomField15Notes.Text = dataLoan.sCustomField15Notes;
			sCustomField16Notes.Text = dataLoan.sCustomField16Notes;
			sCustomField17Notes.Text = dataLoan.sCustomField17Notes;
			sCustomField18Notes.Text = dataLoan.sCustomField18Notes;
			sCustomField19Notes.Text = dataLoan.sCustomField19Notes;
			sCustomField20Notes.Text = dataLoan.sCustomField20Notes;

		}

        private void UpdateTextBox(string id, TextBox tb, CPageData dataLoan)
        {
            if (dataLoan.IsCustomFieldGloballyDefined(id))
            {
                tb.ReadOnly = true;
                tb.BackColor = Color.Gainsboro;
                tb.ForeColor = Color.Black;
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
		}
		#endregion
	}
}
