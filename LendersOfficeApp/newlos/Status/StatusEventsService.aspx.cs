﻿#region Generated Code
namespace LendersOfficeApp.newlos.Status
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.StatusEvents;
    using LendersOffice.Security;
    using LendersOffice.Constants;
    using System.Collections.Generic;

    /// <summary>
    /// Service for the Status Events page.
    /// </summary>
    public partial class StatusEventsService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Process the provided method name and calls the corresponding function.
        /// </summary>
        /// <param name="methodName">The name of the method to call.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "migrate":
                    Migrate();
                    break;
                case "save":
                    Save();
                    break;
                default:
                    throw new UnhandledCaseException(methodName);
            }
        }

        /// <summary>
        /// Saves the IsExclude bits for changes.
        /// </summary>
        protected void Save()
        {
            Guid loanId = GetGuid("loanId");
            List<int> dirtyIds = ObsoleteSerializationHelper.JsonDeserialize<List<int>>(GetString("dirtyIdsJson"));

            if (PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowQualityControlAccess))
            {
                StatusEvent.UpdateExclude(loanId, PrincipalFactory.CurrentPrincipal.BrokerId, dirtyIds);
            }
        }

        /// <summary>
        /// Calls the function for Migrating a loan's audit history into StatusEvent items.
        /// </summary>
        protected void Migrate()
        {
            Guid loanId = GetGuid("loanId");
            var isMigratedSuccessfully = StatusEvent.MigrateStatusEvents(loanId);
            SetResult("OK", isMigratedSuccessfully);
        }
    }
}