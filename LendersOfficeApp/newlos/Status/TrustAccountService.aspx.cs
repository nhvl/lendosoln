using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;

namespace LendersOfficeApp.newlos.Status
{
    public class TrustAccountServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(TrustAccountServiceItem));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("sTrustPayableTotal", dataLoan.sTrustPayableTotal_rep);
            SetResult("sTrustReceivableTotal", dataLoan.sTrustReceivableTotal_rep);
            SetResult("sTrustBalance", dataLoan.sTrustBalance_rep);
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataLoan.sTrust10D_rep             = GetString("sTrust10D");
            dataLoan.sTrust10Desc              = GetString("sTrust10Desc");
            dataLoan.sTrust10PayableAmt_rep    = GetString("sTrust10PayableAmt");
            dataLoan.sTrust10PayableChkNum     = GetString("sTrust10PayableChkNum");
            dataLoan.sTrust10ReceivableAmt_rep = GetString("sTrust10ReceivableAmt");
            dataLoan.sTrust10ReceivableChkNum  = GetString("sTrust10ReceivableChkNum");
            dataLoan.sTrust10ReceivableN       = GetString("sTrust10ReceivableN");
            dataLoan.sTrust11D_rep             = GetString("sTrust11D");
            dataLoan.sTrust11Desc              = GetString("sTrust11Desc");
            dataLoan.sTrust11PayableAmt_rep    = GetString("sTrust11PayableAmt");
            dataLoan.sTrust11PayableChkNum     = GetString("sTrust11PayableChkNum");
            dataLoan.sTrust11ReceivableAmt_rep = GetString("sTrust11ReceivableAmt");
            dataLoan.sTrust11ReceivableChkNum  = GetString("sTrust11ReceivableChkNum");
            dataLoan.sTrust11ReceivableN       = GetString("sTrust11ReceivableN");
            dataLoan.sTrust12D_rep             = GetString("sTrust12D");
            dataLoan.sTrust12Desc              = GetString("sTrust12Desc");
            dataLoan.sTrust12PayableAmt_rep    = GetString("sTrust12PayableAmt");
            dataLoan.sTrust12PayableChkNum     = GetString("sTrust12PayableChkNum");
            dataLoan.sTrust12ReceivableAmt_rep = GetString("sTrust12ReceivableAmt");
            dataLoan.sTrust12ReceivableChkNum  = GetString("sTrust12ReceivableChkNum");
            dataLoan.sTrust12ReceivableN       = GetString("sTrust12ReceivableN");
            dataLoan.sTrust13D_rep             = GetString("sTrust13D");
            dataLoan.sTrust13Desc              = GetString("sTrust13Desc");
            dataLoan.sTrust13PayableAmt_rep    = GetString("sTrust13PayableAmt");
            dataLoan.sTrust13PayableChkNum     = GetString("sTrust13PayableChkNum");
            dataLoan.sTrust13ReceivableAmt_rep = GetString("sTrust13ReceivableAmt");
            dataLoan.sTrust13ReceivableChkNum  = GetString("sTrust13ReceivableChkNum");
            dataLoan.sTrust13ReceivableN       = GetString("sTrust13ReceivableN");
            dataLoan.sTrust14D_rep             = GetString("sTrust14D");
            dataLoan.sTrust14Desc              = GetString("sTrust14Desc");
            dataLoan.sTrust14PayableAmt_rep    = GetString("sTrust14PayableAmt");
            dataLoan.sTrust14PayableChkNum     = GetString("sTrust14PayableChkNum");
            dataLoan.sTrust14ReceivableAmt_rep = GetString("sTrust14ReceivableAmt");
            dataLoan.sTrust14ReceivableChkNum  = GetString("sTrust14ReceivableChkNum");
            dataLoan.sTrust14ReceivableN       = GetString("sTrust14ReceivableN");
            dataLoan.sTrust15D_rep             = GetString("sTrust15D");
            dataLoan.sTrust15Desc              = GetString("sTrust15Desc");
            dataLoan.sTrust15PayableAmt_rep    = GetString("sTrust15PayableAmt");
            dataLoan.sTrust15PayableChkNum     = GetString("sTrust15PayableChkNum");
            dataLoan.sTrust15ReceivableAmt_rep = GetString("sTrust15ReceivableAmt");
            dataLoan.sTrust15ReceivableChkNum  = GetString("sTrust15ReceivableChkNum");
            dataLoan.sTrust15ReceivableN       = GetString("sTrust15ReceivableN");
            dataLoan.sTrust16D_rep             = GetString("sTrust16D");
            dataLoan.sTrust16Desc              = GetString("sTrust16Desc");
            dataLoan.sTrust16PayableAmt_rep    = GetString("sTrust16PayableAmt");
            dataLoan.sTrust16PayableChkNum     = GetString("sTrust16PayableChkNum");
            dataLoan.sTrust16ReceivableAmt_rep = GetString("sTrust16ReceivableAmt");
            dataLoan.sTrust16ReceivableChkNum  = GetString("sTrust16ReceivableChkNum");
            dataLoan.sTrust16ReceivableN       = GetString("sTrust16ReceivableN");
            dataLoan.sTrust1D_rep              = GetString("sTrust1D");
            dataLoan.sTrust1Desc               = GetString("sTrust1Desc");
            dataLoan.sTrust1PayableAmt_rep     = GetString("sTrust1PayableAmt");
            dataLoan.sTrust1PayableChkNum      = GetString("sTrust1PayableChkNum");
            dataLoan.sTrust1ReceivableAmt_rep  = GetString("sTrust1ReceivableAmt");
            dataLoan.sTrust1ReceivableChkNum   = GetString("sTrust1ReceivableChkNum");
            dataLoan.sTrust1ReceivableN        = GetString("sTrust1ReceivableN");
            dataLoan.sTrust2D_rep              = GetString("sTrust2D");
            dataLoan.sTrust2Desc               = GetString("sTrust2Desc");
            dataLoan.sTrust2PayableAmt_rep     = GetString("sTrust2PayableAmt");
            dataLoan.sTrust2PayableChkNum      = GetString("sTrust2PayableChkNum");
            dataLoan.sTrust2ReceivableAmt_rep  = GetString("sTrust2ReceivableAmt");
            dataLoan.sTrust2ReceivableChkNum   = GetString("sTrust2ReceivableChkNum");
            dataLoan.sTrust2ReceivableN        = GetString("sTrust2ReceivableN");
            dataLoan.sTrust3D_rep              = GetString("sTrust3D");
            dataLoan.sTrust3Desc               = GetString("sTrust3Desc");
            dataLoan.sTrust3PayableAmt_rep     = GetString("sTrust3PayableAmt");
            dataLoan.sTrust3PayableChkNum      = GetString("sTrust3PayableChkNum");
            dataLoan.sTrust3ReceivableAmt_rep  = GetString("sTrust3ReceivableAmt");
            dataLoan.sTrust3ReceivableChkNum   = GetString("sTrust3ReceivableChkNum");
            dataLoan.sTrust3ReceivableN        = GetString("sTrust3ReceivableN");
            dataLoan.sTrust4D_rep              = GetString("sTrust4D");
            dataLoan.sTrust4Desc               = GetString("sTrust4Desc");
            dataLoan.sTrust4PayableAmt_rep     = GetString("sTrust4PayableAmt");
            dataLoan.sTrust4PayableChkNum      = GetString("sTrust4PayableChkNum");
            dataLoan.sTrust4ReceivableAmt_rep  = GetString("sTrust4ReceivableAmt");
            dataLoan.sTrust4ReceivableChkNum   = GetString("sTrust4ReceivableChkNum");
            dataLoan.sTrust4ReceivableN        = GetString("sTrust4ReceivableN");
            dataLoan.sTrust5D_rep              = GetString("sTrust5D");
            dataLoan.sTrust5Desc               = GetString("sTrust5Desc");
            dataLoan.sTrust5PayableAmt_rep     = GetString("sTrust5PayableAmt");
            dataLoan.sTrust5PayableChkNum      = GetString("sTrust5PayableChkNum");
            dataLoan.sTrust5ReceivableAmt_rep  = GetString("sTrust5ReceivableAmt");
            dataLoan.sTrust5ReceivableChkNum   = GetString("sTrust5ReceivableChkNum");
            dataLoan.sTrust5ReceivableN        = GetString("sTrust5ReceivableN");
            dataLoan.sTrust6D_rep              = GetString("sTrust6D");
            dataLoan.sTrust6Desc               = GetString("sTrust6Desc");
            dataLoan.sTrust6PayableAmt_rep     = GetString("sTrust6PayableAmt");
            dataLoan.sTrust6PayableChkNum      = GetString("sTrust6PayableChkNum");
            dataLoan.sTrust6ReceivableAmt_rep  = GetString("sTrust6ReceivableAmt");
            dataLoan.sTrust6ReceivableChkNum   = GetString("sTrust6ReceivableChkNum");
            dataLoan.sTrust6ReceivableN        = GetString("sTrust6ReceivableN");
            dataLoan.sTrust7D_rep              = GetString("sTrust7D");
            dataLoan.sTrust7Desc               = GetString("sTrust7Desc");
            dataLoan.sTrust7PayableAmt_rep     = GetString("sTrust7PayableAmt");
            dataLoan.sTrust7PayableChkNum      = GetString("sTrust7PayableChkNum");
            dataLoan.sTrust7ReceivableAmt_rep  = GetString("sTrust7ReceivableAmt");
            dataLoan.sTrust7ReceivableChkNum   = GetString("sTrust7ReceivableChkNum");
            dataLoan.sTrust7ReceivableN        = GetString("sTrust7ReceivableN");
            dataLoan.sTrust8D_rep              = GetString("sTrust8D");
            dataLoan.sTrust8Desc               = GetString("sTrust8Desc");
            dataLoan.sTrust8PayableAmt_rep     = GetString("sTrust8PayableAmt");
            dataLoan.sTrust8PayableChkNum      = GetString("sTrust8PayableChkNum");
            dataLoan.sTrust8ReceivableAmt_rep  = GetString("sTrust8ReceivableAmt");
            dataLoan.sTrust8ReceivableChkNum   = GetString("sTrust8ReceivableChkNum");
            dataLoan.sTrust8ReceivableN        = GetString("sTrust8ReceivableN");
            dataLoan.sTrust9D_rep              = GetString("sTrust9D");
            dataLoan.sTrust9Desc               = GetString("sTrust9Desc");
            dataLoan.sTrust9PayableAmt_rep     = GetString("sTrust9PayableAmt");
            dataLoan.sTrust9PayableChkNum      = GetString("sTrust9PayableChkNum");
            dataLoan.sTrust9ReceivableAmt_rep  = GetString("sTrust9ReceivableAmt");
            dataLoan.sTrust9ReceivableChkNum   = GetString("sTrust9ReceivableChkNum");
            dataLoan.sTrust9ReceivableN        = GetString("sTrust9ReceivableN");

        }
    }
	/// <summary>
	/// Summary description for TrustAccountService.
	/// </summary>
	public partial class TrustAccountService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new TrustAccountServiceItem());
        }
	}
}
