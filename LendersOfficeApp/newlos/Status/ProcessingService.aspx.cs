﻿namespace LendersOfficeApp.newlos.Status
{
    using System;
    using DataAccess;
    using LendersOffice.Security;

    public class ProcessingServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(ProcessingServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sU1LStatDesc = GetString("sU1LStatDesc");
            dataLoan.sU1LStatD_rep = GetString("sU1LStatD");
            dataLoan.sU1LStatN = GetString("sU1LStatN");
            dataLoan.sU2LStatDesc = GetString("sU2LStatDesc");
            dataLoan.sU2LStatD_rep = GetString("sU2LStatD");
            dataLoan.sU2LStatN = GetString("sU2LStatN");
            dataLoan.sU3LStatDesc = GetString("sU3LStatDesc");
            dataLoan.sU3LStatD_rep = GetString("sU3LStatD");
            dataLoan.sU3LStatN = GetString("sU3LStatN");
            dataLoan.sU4LStatDesc = GetString("sU4LStatDesc");
            dataLoan.sU4LStatD_rep = GetString("sU4LStatD");
            dataLoan.sU4LStatN = GetString("sU4LStatN");

            dataLoan.sPrelimRprtOd_rep = GetString("sPrelimRprtOd");
            dataLoan.sPrelimRprtDueD_rep = GetString("sPrelimRprtDueD");
            dataLoan.sPrelimRprtRd_rep = GetString("sPrelimRprtRd");
            dataLoan.sPrelimRprtN = GetString("sPrelimRprtN");

            dataLoan.sApprRprtOd_rep = GetString("sApprRprtOd");
            dataLoan.sApprRprtDueD_rep = GetString("sApprRprtDueD");
            dataLoan.sApprRprtRd_rep = GetString("sApprRprtRd");
            dataLoan.sApprRprtN = GetString("sApprRprtN");

            dataLoan.sDocumentNoteD_rep = GetString("sDocumentNoteD");

            dataLoan.sAppSubmittedD_rep = GetString("sAppSubmittedD");
            dataLoan.sAppSubmittedDLckd = GetBool("sAppSubmittedDLckd");
            dataLoan.sAppReceivedByLenderD_rep = GetString("sAppReceivedByLenderD");
            dataLoan.sAppReceivedByLenderDLckd = GetBool("sAppReceivedByLenderDLckd");

            dataLoan.sTilGfeOd_rep = GetString("sTilGfeOd");
            dataLoan.sTilGfeDueD_rep = GetString("sTilGfeDueD");
            dataLoan.sTilGfeRd_rep = GetString("sTilGfeRd");
            dataLoan.sTilGfeN = GetString("sTilGfeN");
            
            dataLoan.sU1DocStatDesc = GetString("sU1DocStatDesc");
            dataLoan.sU1DocStatOd_rep = GetString("sU1DocStatOd");
            dataLoan.sU1DocStatDueD_rep = GetString("sU1DocStatDueD");
            dataLoan.sU1DocStatRd_rep = GetString("sU1DocStatRd");
            dataLoan.sU1DocStatN = GetString("sU1DocStatN");
            dataLoan.sU2DocStatDesc = GetString("sU2DocStatDesc");
            dataLoan.sU2DocStatOd_rep = GetString("sU2DocStatOd");
            dataLoan.sU2DocStatDueD_rep = GetString("sU2DocStatDueD");
            dataLoan.sU2DocStatRd_rep = GetString("sU2DocStatRd");
            dataLoan.sU2DocStatN = GetString("sU2DocStatN");
            dataLoan.sU3DocStatDesc = GetString("sU3DocStatDesc");
            dataLoan.sU3DocStatOd_rep = GetString("sU3DocStatOd");
            dataLoan.sU3DocStatDueD_rep = GetString("sU3DocStatDueD");
            dataLoan.sU3DocStatRd_rep = GetString("sU3DocStatRd");
            dataLoan.sU3DocStatN = GetString("sU3DocStatN");
            dataLoan.sU4DocStatDesc = GetString("sU4DocStatDesc");
            dataLoan.sU4DocStatOd_rep = GetString("sU4DocStatOd");
            dataLoan.sU4DocStatDueD_rep = GetString("sU4DocStatDueD");
            dataLoan.sU4DocStatRd_rep = GetString("sU4DocStatRd");
            dataLoan.sU4DocStatN = GetString("sU4DocStatN");
            dataLoan.sU5DocStatDesc = GetString("sU5DocStatDesc");
            dataLoan.sU5DocStatOd_rep = GetString("sU5DocStatOd");
            dataLoan.sU5DocStatDueD_rep = GetString("sU5DocStatDueD");
            dataLoan.sU5DocStatRd_rep = GetString("sU5DocStatRd");
            dataLoan.sU5DocStatN = GetString("sU5DocStatN");

            dataLoan.sPrelimRprtDocumentD_rep = GetString("sPrelimRprtDocumentD");
            dataLoan.sSpValuationEffectiveD_rep = GetString("sSpValuationEffectiveD");
            //dataLoan.sAppSubmittedN = GetString("sAppSubmittedN");
            dataLoan.sU1DocDocumentD_rep = GetString("sU1DocDocumentD");
            dataLoan.sU2DocDocumentD_rep = GetString("sU2DocDocumentD");
            dataLoan.sU3DocDocumentD_rep = GetString("sU3DocDocumentD");
            dataLoan.sU4DocDocumentD_rep = GetString("sU4DocDocumentD");
            dataLoan.sU5DocDocumentD_rep = GetString("sU5DocDocumentD");

            dataLoan.sDocMagicApplicationD_rep = GetString("sDocMagicApplicationD");
            dataLoan.sDocMagicApplicationDLckd = GetBool("sDocMagicApplicationDLckd");
            dataLoan.sDocMagicGFED_rep = GetString("sDocMagicGFED");
            dataLoan.sDocMagicGFEDLckd = GetBool("sDocMagicGFEDLckd");
            dataLoan.sDocMagicEstAvailableThroughD_rep = GetString("sDocMagicEstAvailableThroughD");
            dataLoan.sDocMagicEstAvailableThroughDLckd = GetBool("sDocMagicEstAvailableThroughDLckd");
            dataLoan.sDocMagicDocumentD_rep = GetString("sDocMagicDocumentD");
            dataLoan.sDocMagicDocumentDLckd = GetBool("sDocMagicDocumentDLckd");
            dataLoan.sDocMagicClosingD_rep = GetString("sDocMagicClosingD");
            dataLoan.sDocMagicClosingDLckd = GetBool("sDocMagicClosingDLckd");
            dataLoan.sDocMagicSigningD_rep = GetString("sDocMagicSigningD");
            dataLoan.sDocMagicSigningDLckd = GetBool("sDocMagicSigningDLckd");
            dataLoan.sDocMagicCancelD_rep = GetString("sDocMagicCancelD");
            dataLoan.sDocMagicCancelDLckd = GetBool("sDocMagicCancelDLckd");
            dataLoan.sDocMagicDisbursementD_rep = GetString("sDocMagicDisbursementD");
            dataLoan.sDocMagicDisbursementDLckd = GetBool("sDocMagicDisbursementDLckd");
            dataLoan.sDocMagicRateLockD_rep = GetString("sDocMagicRateLockD");
            dataLoan.sDocMagicRateLockDLckd = GetBool("sDocMagicRateLockDLckd");
            dataLoan.sDocMagicPreZSentD_rep = GetString("sDocMagicPreZSentD");
            dataLoan.sDocMagicPreZSentDLckd = GetBool("sDocMagicPreZSentDLckd");
            dataLoan.sDocMagicReDiscSendD_rep = GetString("sDocMagicReDiscSendD");
            dataLoan.sDocMagicReDiscSendDLckd = GetBool("sDocMagicReDiscSendDLckd");
            dataLoan.sDocMagicRedisclosureMethodT = (E_RedisclosureMethodT)GetInt("sDocMagicRedisclosureMethodT");
            dataLoan.sDocMagicRedisclosureMethodTLckd = GetBool("sDocMagicRedisclosureMethodTLckd");
            dataLoan.sDocMagicReDiscReceivedD_rep = GetString("sDocMagicReDiscReceivedD");
            dataLoan.sDocMagicReDiscReceivedDLckd = GetBool("sDocMagicReDiscReceivedDLckd");
            dataLoan.sPurchaseContractDate_rep = GetString("sPurchaseContractDate");
            dataLoan.sFinancingContingencyExpD_rep = GetString("sFinancingContingencyExpD");
            dataLoan.sFinancingContingencyExtensionExpD_rep = GetString("sFinancingContingencyExtensionExpD");

            dataLoan.sLoanPackageOrderedD_rep = GetString("sLoanPackageOrderedD");
            dataLoan.sLoanPackageDocumentD_rep = GetString("sLoanPackageDocumentD");
            dataLoan.sLoanPackageReceivedD_rep = GetString("sLoanPackageReceivedD");
            dataLoan.sLoanPackageN = GetString("sLoanPackageN");

            dataLoan.sMortgageDOTOrderedD_rep = GetString("sMortgageDOTOrderedD");
            dataLoan.sMortgageDOTDocumentD_rep = GetString("sMortgageDOTDocumentD");
            dataLoan.sMortgageDOTReceivedD_rep = GetString("sMortgageDOTReceivedD");
            dataLoan.sMortgageDOTN = GetString("sMortgageDOTN");

            dataLoan.sNoteOrderedD_rep = GetString("sNoteOrderedD");
            dataLoan.sNoteReceivedD_rep = GetString("sNoteReceivedD");
            dataLoan.sNoteN = GetString("sNoteN");

            dataLoan.sFinalHUD1SttlmtStmtOrderedD_rep = GetString("sFinalHUD1SttlmtStmtOrderedD");
            dataLoan.sFinalHUD1SttlmtStmtDocumentD_rep = GetString("sFinalHUD1SttlmtStmtDocumentD");
            dataLoan.sFinalHUD1SttlmtStmtReceivedD_rep = GetString("sFinalHUD1SttlmtStmtReceivedD");
            dataLoan.sFinalHUD1SttlmtStmtN = GetString("sFinalHUD1SttlmtStmtN");

            dataLoan.sTitleInsPolicyOrderedD_rep = GetString("sTitleInsPolicyOrderedD");
            dataLoan.sTitleInsPolicyDocumentD_rep = GetString("sTitleInsPolicyDocumentD");
            dataLoan.sTitleInsPolicyReceivedD_rep = GetString("sTitleInsPolicyReceivedD");
            dataLoan.sTitleInsPolicyN = GetString("sTitleInsPolicyN");

            dataLoan.sMiCertOrderedD_rep = GetString("sMiCertOrderedD");
            dataLoan.sMiCertIssuedD_rep = GetString("sMiCertIssuedD");
            dataLoan.sMiCertReceivedD_rep = GetString("sMiCertReceivedD");
            dataLoan.sMiCertN = GetString("sMiCertN");

            dataLoan.sFinalHazInsPolicyOrderedD_rep = GetString("sFinalHazInsPolicyOrderedD");
            dataLoan.sFinalHazInsPolicyDocumentD_rep = GetString("sFinalHazInsPolicyDocumentD");
            dataLoan.sFinalHazInsPolicyReceivedD_rep = GetString("sFinalHazInsPolicyReceivedD");
            dataLoan.sFinalHazInsPolicyN = GetString("sFinalHazInsPolicyN");

            dataLoan.sFinalFloodInsPolicyOrderedD_rep = GetString("sFinalFloodInsPolicyOrderedD");
            dataLoan.sFinalFloodInsPolicyDocumentD_rep = GetString("sFinalFloodInsPolicyDocumentD");
            dataLoan.sFinalFloodInsPolicyReceivedD_rep = GetString("sFinalFloodInsPolicyReceivedD");
            dataLoan.sFinalFloodInsPolicyN = GetString("sFinalFloodInsPolicyN");

            dataLoan.sLeadSrcDesc = GetString("sLeadSrcDesc");

            dataLoan.sTrNotes = GetString("sTrNotes");

            dataApp.aCrOd_rep = GetString("aCrOd");
            dataApp.aCrDueD_rep = GetString("aCrDueD");
            dataApp.aCrRd_rep = GetString("aCrRd");
            dataApp.aCrN = GetString("aCrN");

            dataApp.aLqiCrOd_rep = GetString("aLqiCrOd");
            dataApp.aLqiCrDueD_rep = GetString("aLqiCrDueD");
            dataApp.aLqiCrRd_rep = GetString("aLqiCrRd");
            dataApp.aLqiCrN = GetString("aLqiCrN");

            dataApp.aBusCrOd_rep = GetString("aBusCrOd");
            dataApp.aBusCrDueD_rep = GetString("aBusCrDueD");
            dataApp.aBusCrRd_rep = GetString("aBusCrRd");
            dataApp.aBusCrN = GetString("aBusCrN");

            dataApp.aU1DocStatDesc = GetString("aU1DocStatDesc");
            dataApp.aU1DocStatOd_rep = GetString("aU1DocStatOd");
            dataApp.aU1DocStatDueD_rep = GetString("aU1DocStatDueD");
            dataApp.aU1DocStatRd_rep = GetString("aU1DocStatRd");
            dataApp.aU1DocStatN = GetString("aU1DocStatN");

            dataApp.aU2DocStatDesc = GetString("aU2DocStatDesc");
            dataApp.aU2DocStatOd_rep = GetString("aU2DocStatOd");
            dataApp.aU2DocStatDueD_rep = GetString("aU2DocStatDueD");
            dataApp.aU2DocStatRd_rep = GetString("aU2DocStatRd");
            dataApp.aU2DocStatN = GetString("aU2DocStatN");

            dataApp.aUDNOrderedD_rep = GetString("aUDNOrderedD");
            dataApp.aUDNDeactivatedD_rep = GetString("aUDNDeactivatedD");
            dataApp.aUdnOrderId = GetString("aUdnOrderId");

            dataApp.aBPreFundVoeTypeT = (E_aPreFundVoeTypeT)GetInt("aBPreFundVoeTypeT");
            dataApp.aBPreFundVoeOd_rep = GetString("aBPreFundVoeOd");
            dataApp.aBPreFundVoeDueD_rep = GetString("aBPreFundVoeDueD");
            dataApp.aBPreFundVoeRd_rep = GetString("aBPreFundVoeRd");
            dataApp.aBPreFundVoeN = GetString("aBPreFundVoeN");

            dataApp.aCPreFundVoeTypeT = (E_aPreFundVoeTypeT)GetInt("aCPreFundVoeTypeT");
            dataApp.aCPreFundVoeOd_rep = GetString("aCPreFundVoeOd");
            dataApp.aCPreFundVoeDueD_rep = GetString("aCPreFundVoeDueD");
            dataApp.aCPreFundVoeRd_rep = GetString("aCPreFundVoeRd");
            dataApp.aCPreFundVoeN = GetString("aCPreFundVoeN");

            dataLoan.sEstCloseD_rep = GetString("sEstCloseD");
            dataLoan.sEstCloseDLckd = GetBool("sEstCloseDLckd");
            dataLoan.sEstCloseN = GetString("sEstCloseN");

            dataLoan.sQCCompDate_rep = this.GetString("sQCCompDate");

            dataLoan.sClosingServOd_rep = GetString("sClosingServOd");
            dataLoan.sClosingServDueD_rep = GetString("sClosingServDueD");
            dataLoan.sClosingServDocumentD_rep = GetString("sClosingServDocumentD");
            dataLoan.sClosingServRd_rep = GetString("sClosingServRd");
            dataLoan.sClosingServN = GetString("sClosingServN");

            dataLoan.sFloodCertOd_rep = GetString("sFloodCertOd");
            dataLoan.sFloodCertDueD_rep = GetString("sFloodCertDueD");
            dataLoan.sFloodCertRd_rep = GetString("sFloodCertRd");
            dataLoan.sFloodCertN = GetString("sFloodCertN");

            dataLoan.sUSPSCheckOd_rep = GetString("sUSPSCheckOd");
            dataLoan.sUSPSCheckDueD_rep = GetString("sUSPSCheckDueD");
            dataLoan.sUSPSCheckDocumentD_rep = GetString("sUSPSCheckDocumentD");
            dataLoan.sUSPSCheckRd_rep = GetString("sUSPSCheckRd");
            dataLoan.sUSPSCheckN = GetString("sUSPSCheckN");

            dataLoan.sPayDemStmntOd_rep = GetString("sPayDemStmntOd");
            dataLoan.sPayDemStmntDueD_rep = GetString("sPayDemStmntDueD");
            dataLoan.sPayDemStmntDocumentD_rep = GetString("sPayDemStmntDocumentD");
            dataLoan.sPayDemStmntRd_rep = GetString("sPayDemStmntRd");
            dataLoan.sPayDemStmntN = GetString("sPayDemStmntN");

            dataLoan.sCAIVRSOd_rep = GetString("sCAIVRSOd");
            dataLoan.sCAIVRSDueD_rep = GetString("sCAIVRSDueD");
            dataLoan.sCAIVRSDocumentD_rep = GetString("sCAIVRSDocumentD");
            dataLoan.sCAIVRSRd_rep = GetString("sCAIVRSRd");
            dataLoan.sCAIVRSN = GetString("sCAIVRSN");

            dataLoan.sFraudServOd_rep = GetString("sFraudServOd");
            dataLoan.sFraudServDueD_rep = GetString("sFraudServDueD");
            dataLoan.sFraudServDocumentD_rep = GetString("sFraudServDocumentD");
            dataLoan.sFraudServRd_rep = GetString("sFraudServRd");
            dataLoan.sFraudServN = GetString("sFraudServN");

            dataLoan.sAVMOd_rep = GetString("sAVMOd");
            dataLoan.sAVMDueD_rep = GetString("sAVMDueD");
            dataLoan.sAVMDocumentD_rep = GetString("sAVMDocumentD");
            dataLoan.sAVMRd_rep = GetString("sAVMRd");
            dataLoan.sAVMN = GetString("sAVMN");

            dataLoan.sHOACertOd_rep = GetString("sHOACertOd");
            dataLoan.sHOACertDueD_rep = GetString("sHOACertDueD");
            dataLoan.sHOACertDocumentD_rep = GetString("sHOACertDocumentD");
            dataLoan.sHOACertRd_rep = GetString("sHOACertRd");
            dataLoan.sHOACertN = GetString("sHOACertN");

            dataLoan.sEstHUDOd_rep = GetString("sEstHUDOd");
            dataLoan.sEstHUDDueD_rep = GetString("sEstHUDDueD");
            dataLoan.sEstHUDDocumentD_rep = GetString("sEstHUDDocumentD");
            dataLoan.sEstHUDRd_rep = GetString("sEstHUDRd");
            dataLoan.sEstHUDN = GetString("sEstHUDN");

            dataLoan.sCPLAndICLOd_rep = GetString("sCPLAndICLOd");
            dataLoan.sCPLAndICLDueD_rep = GetString("sCPLAndICLDueD");
            dataLoan.sCPLAndICLDocumentD_rep = GetString("sCPLAndICLDocumentD");
            dataLoan.sCPLAndICLRd_rep = GetString("sCPLAndICLRd");
            dataLoan.sCPLAndICLN = GetString("sCPLAndICLN");

            dataLoan.sWireInstructOd_rep = GetString("sWireInstructOd");
            dataLoan.sWireInstructDueD_rep = GetString("sWireInstructDueD");
            dataLoan.sWireInstructDocumentD_rep = GetString("sWireInstructDocumentD");
            dataLoan.sWireInstructRd_rep = GetString("sWireInstructRd");
            dataLoan.sWireInstructN = GetString("sWireInstructN");

            dataLoan.sInsurancesOd_rep = GetString("sInsurancesOd");
            dataLoan.sInsurancesDueD_rep = GetString("sInsurancesDueD");
            dataLoan.sInsurancesDocumentD_rep = GetString("sInsurancesDocumentD");
            dataLoan.sInsurancesRd_rep = GetString("sInsurancesRd");
            dataLoan.sInsurancesN = GetString("sInsurancesN");

            Processing.BindDataFromControls(dataLoan, dataApp, this);
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            // "Event" Section
            this.SetResult("sAppSubmittedD", dataLoan.sAppSubmittedD_rep);
            this.SetResult("sAppSubmittedDLckd", dataLoan.sAppSubmittedDLckd);
            this.SetResult("sAppReceivedByLenderD", dataLoan.sAppReceivedByLenderD_rep);
            this.SetResult("sAppReceivedByLenderDLckd", dataLoan.sAppReceivedByLenderDLckd);
            this.SetResult("sEstCloseD", dataLoan.sEstCloseD_rep);
            this.SetResult("sEstCloseDLckd", dataLoan.sEstCloseDLckd);
            this.SetResult("sEstCloseN", dataLoan.sEstCloseN);

            this.SetResult("sQCCompDate", dataLoan.sQCCompDate_rep);
            this.SetResult("sQCCompDateTime", dataLoan.sQCCompDateTime_rep);

            // "Custom Events" Section
            this.SetResult("sU1LStatDesc", dataLoan.sU1LStatDesc);
            this.SetResult("sU1LStatD", dataLoan.sU1LStatD_rep);
            this.SetResult("sU1LStatN", dataLoan.sU1LStatN);

            this.SetResult("sU2LStatDesc", dataLoan.sU2LStatDesc);
            this.SetResult("sU2LStatD", dataLoan.sU2LStatD_rep);
            this.SetResult("sU2LStatN", dataLoan.sU2LStatN);

            this.SetResult("sU3LStatDesc", dataLoan.sU3LStatDesc);
            this.SetResult("sU3LStatD", dataLoan.sU3LStatD_rep);
            this.SetResult("sU3LStatN", dataLoan.sU3LStatN);

            this.SetResult("sU4LStatDesc", dataLoan.sU4LStatDesc);
            this.SetResult("sU4LStatD", dataLoan.sU4LStatD_rep);
            this.SetResult("sU4LStatN", dataLoan.sU4LStatN);

            // "Disclosure/Document Dates" Section
            this.SetResult("sDocMagicApplicationD", dataLoan.sDocMagicApplicationD_rep);
            this.SetResult("sDocMagicApplicationDLckd", dataLoan.sDocMagicApplicationDLckd);
            this.SetResult("sDocMagicGFED", dataLoan.sDocMagicGFED_rep);
            this.SetResult("sDocMagicGFEDLckd", dataLoan.sDocMagicGFEDLckd);
            this.SetResult("sDocMagicEstAvailableThroughD", dataLoan.sDocMagicEstAvailableThroughD_rep);
            this.SetResult("sDocMagicEstAvailableThroughDLckd", dataLoan.sDocMagicEstAvailableThroughDLckd);
            this.SetResult("sDocMagicDocumentD", dataLoan.sDocMagicDocumentD_rep);
            this.SetResult("sDocMagicDocumentDLckd", dataLoan.sDocMagicDocumentDLckd);
            this.SetResult("sDocMagicClosingD", dataLoan.sDocMagicClosingD_rep);
            this.SetResult("sDocMagicClosingDLckd", dataLoan.sDocMagicClosingDLckd);
            this.SetResult("sDocMagicSigningD", dataLoan.sDocMagicSigningD_rep);
            this.SetResult("sDocMagicSigningDLckd", dataLoan.sDocMagicSigningDLckd);
            this.SetResult("sDocMagicCancelD", dataLoan.sDocMagicCancelD_rep);
            this.SetResult("sDocMagicCancelDLckd", dataLoan.sDocMagicCancelDLckd);
            this.SetResult("sDocMagicDisbursementD", dataLoan.sDocMagicDisbursementD_rep);
            this.SetResult("sDocMagicDisbursementDLckd", dataLoan.sDocMagicDisbursementDLckd);
            this.SetResult("sDocMagicRateLockD", dataLoan.sDocMagicRateLockD_rep);
            this.SetResult("sDocMagicRateLockDLckd", dataLoan.sDocMagicRateLockDLckd);
            this.SetResult("sDocMagicPreZSentD", dataLoan.sDocMagicPreZSentD_rep);
            this.SetResult("sDocMagicPreZSentDLckd", dataLoan.sDocMagicPreZSentDLckd);
            this.SetResult("sDocMagicReDiscSendD", dataLoan.sDocMagicReDiscSendD_rep);
            this.SetResult("sDocMagicReDiscSendDLckd", dataLoan.sDocMagicReDiscSendDLckd);
            this.SetResult("sDocMagicRedisclosureMethodT", dataLoan.sDocMagicRedisclosureMethodT);
            this.SetResult("sDocMagicRedisclosureMethodTLckd", dataLoan.sDocMagicRedisclosureMethodTLckd);
            this.SetResult("sDocMagicReDiscReceivedD", dataLoan.sDocMagicReDiscReceivedD_rep);
            this.SetResult("sDocMagicReDiscReceivedDLckd", dataLoan.sDocMagicReDiscReceivedDLckd);

            // "Purchase Contract Dates" Section
            this.SetResult("sPurchaseContractDate", dataLoan.sPurchaseContractDate_rep);

            this.SetResult("sFinancingContingencyExpD", dataLoan.sFinancingContingencyExpD_rep);
            this.SetResult("sFinancingContingencyExtensionExpD", dataLoan.sFinancingContingencyExtensionExpD_rep);

            // "Other Documents" Section
            this.LoadOtherDocuments(dataLoan);

            // "For Borrower" Section
            this.SetResult("aCrOd", dataApp.aCrOd_rep);
            this.SetResult("aCrDueD", dataApp.aCrDueD_rep);
            this.SetResult("aCrRd", dataApp.aCrRd_rep);
            this.SetResult("aCrN", dataApp.aCrN);

            this.SetResult("aLqiCrOd", dataApp.aLqiCrOd_rep);
            this.SetResult("aLqiCrDueD", dataApp.aLqiCrDueD_rep);
            this.SetResult("aLqiCrRd", dataApp.aLqiCrRd_rep);
            this.SetResult("aLqiCrN", dataApp.aLqiCrN);

            this.SetResult("aBusCrOd", dataApp.aBusCrOd_rep);
            this.SetResult("aBusCrDueD", dataApp.aBusCrDueD_rep);
            this.SetResult("aBusCrRd", dataApp.aBusCrRd_rep);
            this.SetResult("aBusCrN", dataApp.aBusCrN);

            this.SetResult("aBPreFundVoeTypeT", dataApp.aBPreFundVoeTypeT);
            this.SetResult("aBPreFundVoeOd", dataApp.aBPreFundVoeOd_rep);
            this.SetResult("aBPreFundVoeDueD", dataApp.aBPreFundVoeDueD_rep);
            this.SetResult("aBPreFundVoeRd", dataApp.aBPreFundVoeRd_rep);
            this.SetResult("aBPreFundVoeN", dataApp.aBPreFundVoeN);

            this.SetResult("aU1DocStatDesc", dataApp.aU1DocStatDesc);
            this.SetResult("aU1DocStatOd", dataApp.aU1DocStatOd_rep);
            this.SetResult("aU1DocStatDueD", dataApp.aU1DocStatDueD_rep);
            this.SetResult("aU1DocStatRd", dataApp.aU1DocStatRd_rep);
            this.SetResult("aU1DocStatN", dataApp.aU1DocStatN);

            this.SetResult("aU2DocStatDesc", dataApp.aU2DocStatDesc);
            this.SetResult("aU2DocStatOd", dataApp.aU2DocStatOd_rep);
            this.SetResult("aU2DocStatDueD", dataApp.aU2DocStatDueD_rep);
            this.SetResult("aU2DocStatRd", dataApp.aU2DocStatRd_rep);
            this.SetResult("aU2DocStatN", dataApp.aU2DocStatN);

            this.SetResult("aUDNOrderedD", dataApp.aUDNOrderedD_rep);
            this.SetResult("aUDNDeactivatedD", dataApp.aUDNDeactivatedD_rep);
            this.SetResult("aUdnOrderId", dataApp.aUdnOrderId);

            // "For Co-Borrower"
            this.SetResult("aCPreFundVoeTypeT", dataApp.aCPreFundVoeTypeT);
            this.SetResult("aCPreFundVoeOd", dataApp.aCPreFundVoeOd_rep);
            this.SetResult("aCPreFundVoeDueD", dataApp.aCPreFundVoeDueD_rep);
            this.SetResult("aCPreFundVoeRd", dataApp.aCPreFundVoeRd_rep);
            this.SetResult("aCPreFundVoeN", dataApp.aCPreFundVoeN);

            this.SetResult("sTrNotes", dataLoan.sTrNotes);

            Processing.LoadDataForControls(dataLoan, dataApp, this);
        }

        /// <summary>
        /// Loads the data on the "Other Documents" portion of the page.
        /// </summary>
        private void LoadOtherDocuments(CPageData dataLoan)
        {
            this.SetResult("sPrelimRprtOd", dataLoan.sPrelimRprtOd_rep);
            this.SetResult("sPrelimRprtDueD", dataLoan.sPrelimRprtDueD_rep);
            this.SetResult("sPrelimRprtDocumentD", dataLoan.sPrelimRprtDocumentD_rep);
            this.SetResult("sPrelimRprtRd", dataLoan.sPrelimRprtRd_rep);
            this.SetResult("sPrelimRprtN", dataLoan.sPrelimRprtN);

            this.SetResult("sClosingServOd", dataLoan.sClosingServOd_rep);
            this.SetResult("sClosingServDueD", dataLoan.sClosingServDueD_rep);
            this.SetResult("sClosingServDocumentD", dataLoan.sClosingServDocumentD_rep);
            this.SetResult("sClosingServRd", dataLoan.sClosingServRd_rep);
            this.SetResult("sClosingServN", dataLoan.sClosingServN);

            this.SetResult("sApprRprtOd", dataLoan.sApprRprtOd_rep);
            this.SetResult("sApprRprtDueD", dataLoan.sApprRprtDueD_rep);
            this.SetResult("sSpValuationEffectiveD", dataLoan.sSpValuationEffectiveD_rep);
            this.SetResult("sApprRprtRd", dataLoan.sApprRprtRd_rep);
            this.SetResult("sApprRprtN", dataLoan.sApprRprtN);

            this.SetResult("sTilGfeOd", dataLoan.sTilGfeOd_rep);
            this.SetResult("sTilGfeDueD", dataLoan.sTilGfeDueD_rep);
            this.SetResult("sTilGfeDocumentD", dataLoan.sTilGfeDocumentD_rep);
            this.SetResult("sTilGfeRd", dataLoan.sTilGfeRd_rep);
            this.SetResult("sTilGfeN", dataLoan.sTilGfeN);

            this.SetResult("sFloodCertOd", dataLoan.sFloodCertOd_rep);
            this.SetResult("sFloodCertDueD", dataLoan.sFloodCertDueD_rep);
            this.SetResult("sFloodCertificationDeterminationD", dataLoan.sFloodCertificationDeterminationD_rep);
            this.SetResult("sFloodCertRd", dataLoan.sFloodCertRd_rep);
            this.SetResult("sFloodCertN", dataLoan.sFloodCertN);

            this.SetResult("sUSPSCheckOd", dataLoan.sUSPSCheckOd_rep);
            this.SetResult("sUSPSCheckDueD", dataLoan.sUSPSCheckDueD_rep);
            this.SetResult("sUSPSCheckDocumentD", dataLoan.sUSPSCheckDocumentD_rep);
            this.SetResult("sUSPSCheckRd", dataLoan.sUSPSCheckRd_rep);
            this.SetResult("sUSPSCheckN", dataLoan.sUSPSCheckN);

            this.SetResult("sPayDemStmntOd", dataLoan.sPayDemStmntOd_rep);
            this.SetResult("sPayDemStmntDueD", dataLoan.sPayDemStmntDueD_rep);
            this.SetResult("sPayDemStmntDocumentD", dataLoan.sPayDemStmntDocumentD_rep);
            this.SetResult("sPayDemStmntRd", dataLoan.sPayDemStmntRd_rep);
            this.SetResult("sPayDemStmntN", dataLoan.sPayDemStmntN);

            this.SetResult("sCAIVRSOd", dataLoan.sCAIVRSOd_rep);
            this.SetResult("sCAIVRSDueD", dataLoan.sCAIVRSDueD_rep);
            this.SetResult("sCAIVRSDocumentD", dataLoan.sCAIVRSDocumentD_rep);
            this.SetResult("sCAIVRSRd", dataLoan.sCAIVRSRd_rep);
            this.SetResult("sCAIVRSN", dataLoan.sCAIVRSN);

            this.SetResult("sFraudServOd", dataLoan.sFraudServOd_rep);
            this.SetResult("sFraudServDueD", dataLoan.sFraudServDueD_rep);
            this.SetResult("sFraudServDocumentD", dataLoan.sFraudServDocumentD_rep);
            this.SetResult("sFraudServRd", dataLoan.sFraudServRd_rep);
            this.SetResult("sFraudServN", dataLoan.sFraudServN);

            this.SetResult("sAVMOd", dataLoan.sAVMOd_rep);
            this.SetResult("sAVMDueD", dataLoan.sAVMDueD_rep);
            this.SetResult("sAVMDocumentD", dataLoan.sAVMDocumentD_rep);
            this.SetResult("sAVMRd", dataLoan.sAVMRd_rep);
            this.SetResult("sAVMN", dataLoan.sAVMN);

            this.SetResult("sHOACertOd", dataLoan.sHOACertOd_rep);
            this.SetResult("sHOACertDueD", dataLoan.sHOACertDueD_rep);
            this.SetResult("sHOACertDocumentD", dataLoan.sHOACertDocumentD_rep);
            this.SetResult("sHOACertRd", dataLoan.sHOACertRd_rep);
            this.SetResult("sHOACertN", dataLoan.sHOACertN);

            this.SetResult("sEstHUDOd", dataLoan.sEstHUDOd_rep);
            this.SetResult("sEstHUDDueD", dataLoan.sEstHUDDueD_rep);
            this.SetResult("sEstHUDDocumentD", dataLoan.sEstHUDDocumentD_rep);
            this.SetResult("sEstHUDRd", dataLoan.sEstHUDRd_rep);
            this.SetResult("sEstHUDN", dataLoan.sEstHUDN);

            this.SetResult("sCPLAndICLOd", dataLoan.sCPLAndICLOd_rep);
            this.SetResult("sCPLAndICLDueD", dataLoan.sCPLAndICLDueD_rep);
            this.SetResult("sCPLAndICLDocumentD", dataLoan.sCPLAndICLDocumentD_rep);
            this.SetResult("sCPLAndICLRd", dataLoan.sCPLAndICLRd_rep);
            this.SetResult("sCPLAndICLN", dataLoan.sCPLAndICLN);

            this.SetResult("sWireInstructOd", dataLoan.sWireInstructOd_rep);
            this.SetResult("sWireInstructDueD", dataLoan.sWireInstructDueD_rep);
            this.SetResult("sWireInstructDocumentD", dataLoan.sWireInstructDocumentD_rep);
            this.SetResult("sWireInstructRd", dataLoan.sWireInstructRd_rep);
            this.SetResult("sWireInstructN", dataLoan.sWireInstructN);

            this.SetResult("sInsurancesOd", dataLoan.sInsurancesOd_rep);
            this.SetResult("sInsurancesDueD", dataLoan.sInsurancesDueD_rep);
            this.SetResult("sInsurancesDocumentD", dataLoan.sInsurancesDocumentD_rep);
            this.SetResult("sInsurancesRd", dataLoan.sInsurancesRd_rep);
            this.SetResult("sInsurancesN", dataLoan.sInsurancesN);

            this.SetResult("sU1DocStatDesc", dataLoan.sU1DocStatDesc);
            this.SetResult("sU1DocStatOd", dataLoan.sU1DocStatOd_rep);
            this.SetResult("sU1DocStatDueD", dataLoan.sU1DocStatDueD_rep);
            this.SetResult("sU1DocDocumentD", dataLoan.sU1DocDocumentD_rep);
            this.SetResult("sU1DocStatRd", dataLoan.sU1DocStatRd_rep);
            this.SetResult("sU1DocStatN", dataLoan.sU1DocStatN);

            this.SetResult("sU2DocStatDesc", dataLoan.sU2DocStatDesc);
            this.SetResult("sU2DocStatOd", dataLoan.sU2DocStatOd_rep);
            this.SetResult("sU2DocStatDueD", dataLoan.sU2DocStatDueD_rep);
            this.SetResult("sU2DocDocumentD", dataLoan.sU2DocDocumentD_rep);
            this.SetResult("sU2DocStatRd", dataLoan.sU2DocStatRd_rep);
            this.SetResult("sU2DocStatN", dataLoan.sU2DocStatN);

            this.SetResult("sU3DocStatDesc", dataLoan.sU3DocStatDesc);
            this.SetResult("sU3DocStatOd", dataLoan.sU3DocStatOd_rep);
            this.SetResult("sU3DocStatDueD", dataLoan.sU3DocStatDueD_rep);
            this.SetResult("sU3DocDocumentD", dataLoan.sU3DocDocumentD_rep);
            this.SetResult("sU3DocStatRd", dataLoan.sU3DocStatRd_rep);
            this.SetResult("sU3DocStatN", dataLoan.sU3DocStatN);

            this.SetResult("sU4DocStatDesc", dataLoan.sU4DocStatDesc);
            this.SetResult("sU4DocStatOd", dataLoan.sU4DocStatOd_rep);
            this.SetResult("sU4DocStatDueD", dataLoan.sU4DocStatDueD_rep);
            this.SetResult("sU4DocDocumentD", dataLoan.sU4DocDocumentD_rep);
            this.SetResult("sU4DocStatRd", dataLoan.sU4DocStatRd_rep);
            this.SetResult("sU4DocStatN", dataLoan.sU4DocStatN);

            this.SetResult("sU5DocStatDesc", dataLoan.sU5DocStatDesc);
            this.SetResult("sU5DocStatOd", dataLoan.sU5DocStatOd_rep);
            this.SetResult("sU5DocStatDueD", dataLoan.sU5DocStatDueD_rep);
            this.SetResult("sU5DocDocumentD", dataLoan.sU5DocDocumentD_rep);
            this.SetResult("sU5DocStatRd", dataLoan.sU5DocStatRd_rep);
            this.SetResult("sU5DocStatN", dataLoan.sU5DocStatN);

            this.SetResult("sLoanPackageOrderedD", dataLoan.sLoanPackageOrderedD_rep);
            this.SetResult("sLoanPackageDocumentD", dataLoan.sLoanPackageDocumentD_rep);
            this.SetResult("sLoanPackageReceivedD", dataLoan.sLoanPackageReceivedD_rep);
            this.SetResult("sLoanPackageN", dataLoan.sLoanPackageN);

            this.SetResult("sMortgageDOTOrderedD", dataLoan.sMortgageDOTOrderedD_rep);
            this.SetResult("sMortgageDOTDocumentD", dataLoan.sMortgageDOTDocumentD_rep);
            this.SetResult("sMortgageDOTReceivedD", dataLoan.sMortgageDOTReceivedD_rep);
            this.SetResult("sMortgageDOTN", dataLoan.sMortgageDOTN);

            this.SetResult("sNoteOrderedD", dataLoan.sNoteOrderedD_rep);
            this.SetResult("sDocumentNoteD", dataLoan.sDocumentNoteD_rep);
            this.SetResult("sNoteReceivedD", dataLoan.sNoteReceivedD_rep);
            this.SetResult("sNoteN", dataLoan.sNoteN);

            this.SetResult("sFinalHUD1SttlmtStmtOrderedD", dataLoan.sFinalHUD1SttlmtStmtOrderedD_rep);
            this.SetResult("sFinalHUD1SttlmtStmtDocumentD", dataLoan.sFinalHUD1SttlmtStmtDocumentD_rep);
            this.SetResult("sFinalHUD1SttlmtStmtReceivedD", dataLoan.sFinalHUD1SttlmtStmtReceivedD_rep);
            this.SetResult("sFinalHUD1SttlmtStmtN", dataLoan.sFinalHUD1SttlmtStmtN);

            this.SetResult("sTitleInsPolicyOrderedD", dataLoan.sTitleInsPolicyOrderedD_rep);
            this.SetResult("sTitleInsPolicyDocumentD", dataLoan.sTitleInsPolicyDocumentD_rep);
            this.SetResult("sTitleInsPolicyReceivedD", dataLoan.sTitleInsPolicyReceivedD_rep);
            this.SetResult("sTitleInsPolicyN", dataLoan.sTitleInsPolicyN);

            this.SetResult("sMiCertOrderedD", dataLoan.sMiCertOrderedD_rep);
            this.SetResult("sMiCertIssuedD", dataLoan.sMiCertIssuedD_rep);
            this.SetResult("sMiCertReceivedD", dataLoan.sMiCertReceivedD_rep);
            this.SetResult("sMiCertN", dataLoan.sMiCertN);

            this.SetResult("sFinalHazInsPolicyOrderedD", dataLoan.sFinalHazInsPolicyOrderedD_rep);
            this.SetResult("sFinalHazInsPolicyDocumentD", dataLoan.sFinalHazInsPolicyDocumentD_rep);
            this.SetResult("sFinalHazInsPolicyReceivedD", dataLoan.sFinalHazInsPolicyReceivedD_rep);
            this.SetResult("sFinalHazInsPolicyN", dataLoan.sFinalHazInsPolicyN);

            this.SetResult("sFinalFloodInsPolicyOrderedD", dataLoan.sFinalFloodInsPolicyOrderedD_rep);
            this.SetResult("sFinalFloodInsPolicyDocumentD", dataLoan.sFinalFloodInsPolicyDocumentD_rep);
            this.SetResult("sFinalFloodInsPolicyReceivedD", dataLoan.sFinalFloodInsPolicyReceivedD_rep);
            this.SetResult("sFinalFloodInsPolicyN", dataLoan.sFinalFloodInsPolicyN);

            this.SetResult("sLeadSrcDesc", dataLoan.sLeadSrcDesc);
        }
    }

    public partial class ProcessingService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new ProcessingServiceItem());
        }
    }
}
