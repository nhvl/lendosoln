﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="NMLSCallReport.aspx.cs" Inherits="LendersOfficeApp.newlos.Status.NMLSCallReport"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import namespace="DataAccess"%>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML>
<html>
<head runat="server">
<title> </title>
<style type="text/css">
    .WarningLabelStyle 
    {
    	border: solid 1px black;
    	font-weight: bold;
    	margin: 5px;
    	color: red;
    	padding: 5px;
    	background-color: white;
    }
    .FormTable
    {
    	border: solid 1px black;
    	width: 100%;
    	padding: 0px;
    }
    .FormTable tbody tr:first-child td {
        font-weight: bold;
        font-size: 11px;
        color: navy;
        font-family: Arial, Helvetica, sans-serif;
        background-color: silver;
    }
    td.FieldLabel
    {
    	width: 20em;
    }
</style>
</head>
<body class="EditBackground">
<script type="text/javascript">
function f_displayWarning(id, bVisible, o) {
    $('#'+id).css('display', bVisible ? '' : 'none');
    if (typeof o != 'undefined') {
        try { o.focus(); } catch (e) { }
    }
}

function lock_HmdaPropT() {
    var bLocked = <%= AspxTools.JsGetElementById(sHmdaPropTLckd) %>.checked;
    disableDDL(<%= AspxTools.JsGetElementById(sHmdaPropT) %>, !bLocked);
    var bShowWarning = <%= AspxTools.JsGetElementById(sGseSpT) %>.value  == <%= AspxTools.JsString(E_sGseSpT.Modular.ToString("D")) %>;
    f_displayWarning('sHmdaPropTWarning', bShowWarning);
}

function lock_sLenderFeesCollected() {
    var bLocked = $('#sLenderFeesCollectedLckd').prop('checked');
    $('#sLenderFeesCollected').prop('readonly', !bLocked);
}
function lock_sBrokerFeesCollected() {
    var bLocked = $('#sBrokerFeesCollectedLckd').prop('checked');
    $('#sBrokerFeesCollected').prop('readonly', !bLocked);
}
function makeReadOnlyByPermissions()
{
    // Finance: if can read but can't write 
    <%if(BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowAccountantRead)
        && false == BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowAccountantWrite))
        { %>
        disableDDL(<%=AspxTools.JsGetElementById(sLoanSaleDispositionT) %>, true);
        disableDDL(<%=AspxTools.JsGetElementById(sPurchaseAdviceSummaryServicingStatus) %>, true);
        $('#sDaysInWarehouse').prop('readonly', true);
    <%} %>
    
    // UW: if no access
    <% if (false == BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowUnderwritingAccess))
    { %>
        $('#sCreditScoreLpeQual').prop('readonly', true);
    <%} %>
}

function f_onclick_sDisbursementDaysOfInterestLckd(bRefreshCalc) {
    var bReadOnlyDaysOfInterest = !document.getElementById('sDisbursementDaysOfInterestLckd').checked;
    document.getElementById('sDisbursementDaysOfInterest').readOnly = bReadOnlyDaysOfInterest;

    if (bRefreshCalc) {
        refreshCalculation();
    }
}


$(function() {
    f_displayWarning('CloseWarningLabel', false);
    lock_HmdaPropT();
    lock_sLenderFeesCollected();
    lock_sBrokerFeesCollected();
    makeReadOnlyByPermissions();
    disableDDL(<%=AspxTools.JsGetElementById(sOccT) %>, true);
    f_onclick_sDisbursementDaysOfInterestLckd(false);
    
    lockElement('sServicingByUsStartD');
    lockElement('sServicingByUsEndD');
    lockElement('sNMLSServicingTransferInD');
    lockElement('sNMLSServicingTransferOutD');
    lockElement('sNMLSPeriodForDaysDelinquentT');
    lockElement('sNMLSServicingIntentT');
    lockElement('sNMLSApplicationAmount');
    lockElement('sNmlsApplicationDate');

    $("#sNMLSDaysDelinquentAsOfPeriodEnd").keydown(preventNegative);
    $("#sNMLSDaysDelinquentAsOfPeriodEnd").change(ensurePositive);
    $("#sNmlsApplicationDateLckd").change(function(){lockElement('sNmlsApplicationDate'); refreshCalculation();});

    if (ML.LoanVersionTCurrent < <%=AspxTools.JsNumeric(LendersOffice.Migration.LoanVersionT.V22_HmdaDataPointImprovement) %>) {
        $(".sHmdaActionTakenT").hide();
    } else {
        var combo = $(".sHmdaActionTaken");
        combo.hide();
        combo.next(".combobox_img").hide();
    }
});

function lockElement(id)
{
    var test = $("#" + id + "Lckd");

    var elem = $("#" + id);
    if(id == "sNMLSPeriodForDaysDelinquentT")
    {
        id="sNMLSPeriodForDaysDelinquent";
    }
    else if(id == "sNMLSServicingIntentT")
    {
        id="sNMLSServicingIntent";
    }
    var isChecked = $("#" + id + "Lckd").prop("checked");
    if(elem.prop("tagName") == "SELECT"){
        elem.prop("disabled", !isChecked);
        
        if(isChecked)
        {
            elem.css("background-color", "");
        }
        else
        {
            elem.css("background-color","lightgrey");
        }
    }
    else
    {
        elem.prop("readonly", !isChecked);
    }
}

function preventNegative(event)
{
    var keycode = event.which;
    if(keycode == 189)
    {
        event.preventDefault();
    }
}

function ensurePositive(event)
{
    var el = $(event.target);
    el.val(el.val().replace('-', ''));
}

var oldSaveMe = window.saveMe;
window.saveMe = function(bRefreshScreen)
{
    var resultSave = oldSaveMe(bRefreshScreen);
    if (resultSave)
    {
        window.parent.treeview.location = window.parent.treeview.location;
    }
    return resultSave; 
}
//-->
</script>
<form id="NMLSCallReport" method="post" runat="server">
<input type="hidden" id="sGseSpT" runat="server" value="" />
<table>
    <tr>
        <td class="MainRightHeader" colspan=2>
            NMLS Call Report
        </td>
    </tr>
    <tr>
        <td>
            <table class="FormTable">
                <tr>
                    <td class="FormTableSubHeader" colspan="2">
                        Application Data
                    </td>
                </tr>
                <tr>
                    <td colspan="2" nowrap="nowrap" onclick="refreshCalculation();">
                    <span class="FieldLabel">
                    <ml:EncodedLiteral ID="sIsBranchActAsOriginatorForFileLabel" runat="server"  /></span>&nbsp;<asp:RadioButtonList ID="sIsBranchActAsOriginatorForFileTri" runat="server" RepeatDirection="Horizontal" RepeatLayout=Flow/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" nowrap="nowrap" onclick="refreshCalculation();">
                    <span class="FieldLabel"><ml:EncodedLiteral ID="sIsBranchActAsLenderForFileLabel" runat="server" /></span>&nbsp;<asp:RadioButtonList ID="sIsBranchActAsLenderForFileTri" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"/>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Channel
                    </td>
                    <td>
                        <asp:TextBox ID="sBranchChannelT" runat="server" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Loan opened date
                    </td>
                    <td>
                        <ml:datetextbox id="sOpenedD" CssClass="mask" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        NMLS Application Date
                    </td>
                    <td>
                        <ml:DateTextBox ID="sNmlsApplicationDate" runat="server" CssClass="mask"></ml:DateTextBox>
                        <asp:CheckBox ID="sNmlsApplicationDateLckd" runat="server" Text="Locked" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Loan closed date
                    </td>
                    <td>
                        <ml:datetextbox
                            id="sClosedD"
                            onblur="f_displayWarning('CloseWarningLabel', false);"
                            onfocus="f_displayWarning('CloseWarningLabel', true);"
                            onchange="f_displayWarning('CloseWarningLabel', true, this);"
                            runat="server"
                            CssClass="mask"
                            preset="date"
                            width="75">
                        </ml:datetextbox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:checkbox id="sHmdaExcludedFromReport" runat="server" Text="Exclude loan from HMDA report" CssClass="FieldLabel" ></asp:checkbox>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Action taken
                    </td>
                    <td>
                        <ml:combobox id="sHmdaActionTaken" runat="server" Width="230px" CssClass="sHmdaActionTaken"></ml:combobox>
                        <asp:DropDownList ID="sHmdaActionTakenT" runat="server" CssClass="sHmdaActionTakenT"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Action date
                    </td>
                    <td>
                        <ml:datetextbox id="sHmdaActionD" CssClass="mask" runat="server"></ml:datetextbox>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table class="FormTable">
                            <tr>
                                <td class="FormTableSubHeader" colspan="2">
                                    Net Change in Application Amount Calculation
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp; Total Loan Amount
                                </td>
                                <td>
                                    &nbsp; <ml:MoneyTextBox ID="sFinalLAmt" runat="server" ReadOnly="true"></ml:MoneyTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    - NMLS Application Amount
                                </td>
                                <td>
                                    - <ml:MoneyTextBox ID="sNMLSApplicationAmount" runat="server" onblur="refreshCalculation();"></ml:MoneyTextBox>
                                    <asp:CheckBox runat="server" onclick="lockElement('sNMLSApplicationAmount');" 
                                        ID="sNMLSApplicationAmountLckd" Text="Locked" CssClass="FieldLabel" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <hr style="text-align: left; width: 75%;" />
                                </td>
                                <td>
                                    <hr style="text-align: left; width: 75%;" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp; Net Change
                                </td>
                                <td>
                                    &nbsp; <ml:MoneyTextBox ID="sNMLSNetChangeApplicationAmount" runat="server" ReadOnly="true"></ml:MoneyTextBox>
                                </td>
                            </tr>
                        </table>
                    </td>                    
                </tr>
            </table>
        </td>
        <td rowspan=2>
            <table class="FormTable" style="margin-top: 9px;">
                <tr>
                    <td class="FormTableSubHeader" colspan="3">
                        Expanded Call Report Loan Data
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Loan Program
                    </td>
                    <td>
                        <asp:textbox runat="server" ID="sLpTemplateNm" width="249px"></asp:textbox>
                    </td>
                    <td style="width:100">
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Amortization Type
                    </td>
                    <td>
                        <asp:dropdownlist runat="server" ID="sFinMethT"></asp:dropdownlist>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Mortgage Loan Type
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="sMortgageLoanT"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                       Jumbo Indicator 
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="sJumboT"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Documentation
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="sDocumentationT"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Interest-Only
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="sIOnlyMon" Width="40px"></asp:TextBox> months
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" colspan=2>
                        <asp:CheckBox runat="server" ID="sIsOptionArm" Text="Loan is an Option ARM" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" colspan=2>
                        <asp:Checkbox runat="server" ID="sHasPrepaymentPenalty" Text="Loan has a Prepayment Penalty" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        NMLS Loan Purpose
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="sNMLSLoanPurposeT"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Subj Prop Occ.
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="sOccT"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" colspan=2>
                        <asp:CheckBox runat="server" ID="sHasPrivateMortgageInsurance" Text="Loan has Private Mortgage Insurance" Enabled="false"/>
                    </td>                    
                </tr>
                <tr>
                    <td class="FieldLabel" colspan=2>
                        <asp:CheckBox runat="server" ID="sHasPiggybackFinancing" Text="Loan has Piggyback financing" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Qualifying Score
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="sCreditScoreLpeQual" Width="40px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        NMLS LTV / CLTV
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="sNMLSLtvR" Width="55px" ReadOnly="true"></asp:TextBox> / <asp:TextBox runat="server" ID="sNMLSCLtvR"  Width="55px" readonly></asp:TextBox>
                    </td>
                </tr>
                <tr runat="server" id="rowLoanSaleDisposition">
                    <td class="FieldLabel">
                        Loan Sale Disposition
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="sLoanSaleDispositionT"></asp:DropDownList>
                    </td>
                </tr>
                <tr runat="server" id="rowServicing">
                    <td class="FieldLabel">
                        Servicing
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="sPurchaseAdviceSummaryServicingStatus"></asp:DropDownList>
                    </td>
                </tr>
                <tr runat="server" id="rowDaysOfInterest">
                <td class="FieldLabel">
                    <label>
                        Days of interest
                        <asp:CheckBox ID="sDisbursementDaysOfInterestLckd" onclick="f_onclick_sDisbursementDaysOfInterestLckd(true);" class="floatRight" runat="server" />
                    </label>
                </td>
                <td>
                    <asp:TextBox ID="sDisbursementDaysOfInterest" onchange="refreshCalculation();" ReadOnly="true" runat="server" Width="40px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">Servicing by us start date</td>
                <td><ml:DateTextBox CssClass="mask" runat="server" ID="sServicingByUsStartD"></ml:DateTextBox>
                <asp:CheckBox runat="server" ID="sServicingByUsStartDLckd" onclick="lockElement('sServicingByUsStartD')" Text="Locked" CssClass="FieldLabel" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">Servicing by us end date</td>
                <td><ml:DateTextBox CssClass="mask" runat="server" ID="sServicingByUsEndD"></ml:DateTextBox>
                <asp:CheckBox runat="server" ID="sServicingByUsEndDLckd" onclick="lockElement('sServicingByUsEndD')" Text="Locked" CssClass="FieldLabel" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">Subservicing transfer effective date</td>
                <td><ml:DateTextBox CssClass="mask" runat="server" ID="sGLServTransEffD" ReadOnly="true"></ml:DateTextBox>
            </tr>
            <tr>
                <td class="FieldLabel">Servicing transfer in date</td>
                <td><ml:DateTextBox CssClass="mask" runat="server" ID="sNMLSServicingTransferInD"></ml:DateTextBox>
                <asp:CheckBox runat="server" ID="sNMLSServicingTransferInDLckd" onclick="lockElement('sNMLSServicingTransferInD')" Text="Locked" CssClass="FieldLabel" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">Servicing transfer out date</td>
                <td><ml:DateTextBox CssClass="mask" runat="server" ID="sNMLSServicingTransferOutD"></ml:DateTextBox>
                <asp:CheckBox runat="server" ID="sNMLSServicingTransferOutDLckd" onclick="lockElement('sNMLSServicingTransferOutD')" Text="Locked" CssClass="FieldLabel" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">Days delinquent period</td>
                <td><asp:DropDownList runat="server" NotEditable="true" ID="sNMLSPeriodForDaysDelinquentT"></asp:DropDownList>
                <asp:CheckBox runat="server" ID="sNMLSPeriodForDaysDelinquentLckd" onclick="lockElement('sNMLSPeriodForDaysDelinquentT')" Text="Locked" CssClass="FieldLabel" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">Days delinquent as of period end</td>
                <td><asp:TextBox runat="server" style="width:40px;" ID="sNMLSDaysDelinquentAsOfPeriodEnd"></asp:TextBox>
                </td>
            </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table class="FormTable">
                <tr>
                    <td class="FormTableSubHeader" colspan="2">
                        Closed Loan Data
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Loan type
                    </td>
                    <td>
                        <asp:dropdownlist id="sLT" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Property type
                    </td>
                    <td>
                        <asp:dropdownlist id="sHmdaPropT" runat="server"></asp:dropdownlist>
                        <asp:checkbox id="sHmdaPropTLckd" onclick="lock_HmdaPropT();" runat="server" Text="Locked" CssClass="FieldLabel" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Loan purpose
                    </td>
                    <td>
                        <asp:dropdownlist id="sLPurposeT" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:checkbox id="sHmdaReportAsHomeImprov" runat="server" Text="Report the purpose of this loan as home improvement (1-4 family)" CssClass="FieldLabel"></asp:checkbox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:checkbox id="sHmdaReportAsHoepaLoan" runat="server" Text="HOEPA loan" CssClass="FieldLabel"></asp:checkbox>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Lien position
                    </td>
                    <td>
                        <asp:dropdownlist id="sLienPosT" width="90px" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Lender fees collected
                    </td>
                    <td>
                        <ml:MoneyTextBox ID="sLenderFeesCollected" runat="server"></ml:MoneyTextBox>
                        <asp:checkbox id="sLenderFeesCollectedLckd" runat="server" onclick="lock_sLenderFeesCollected();" Text="Locked" CssClass="FieldLabel" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Broker fees collected
                    </td>
                    <td>
                        <ml:MoneyTextBox ID="sBrokerFeesCollected" runat="server"></ml:MoneyTextBox>
                        <asp:checkbox id="sBrokerFeesCollectedLckd" runat="server" onclick="lock_sBrokerFeesCollected();" Text="Locked" CssClass="FieldLabel" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        QM Status
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="sQMStatusT" disabled></asp:DropDownList>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table class="FormTable">
                <tr>
                    <td class="FormTableSubHeader" colspan="2">
                        Revenue
                    </td>
               </tr>
               <tr>
                    <td>
                        We intend to <asp:DropDownList runat="server" NotEditable="true" ID="sNMLSServicingIntentT"></asp:DropDownList> the Mortgage Servicing Rights.
                        <asp:CheckBox runat="server" onclick="lockElement('sNMLSServicingIntentT')" class="FieldLabel" ID="sNMLSServicingIntentLckd" Text="Locked" />
                    </td>
               </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <div id="CloseWarningLabel" class="WarningLabelStyle">
                WARNING: Modifying the closed date 
                could remove your permission to edit this loan.
            </div>
            <div id="sHmdaPropTWarning" class="WarningLabelStyle">
                WARNING: Modular properties that meet HUD standards may fall under the Manufactured category. See the HMDA LAR guide for more info.
            </div>   
        </td>
    </tr>
</table>

</form>

</body>
</html>
