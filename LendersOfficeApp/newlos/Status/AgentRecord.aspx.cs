namespace LendersOfficeApp.newlos.Status
{
    using System;
    using System.Data;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOfficeApp.los.BrokerAdmin;

    public partial class AgentRecord : BaseSingleEditPage<CAgentFields>
    {
        private CPageData m_dataLoan;
        protected LendingLicensesPanel LicensesPanel;
        protected LendingLicensesPanel CompanyLicenses;

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");

            m_dataLoan = new CPeopleData(LoanID);
            m_dataLoan.InitLoad();
            this.PageTitle = "Agent record";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CAgentListPDF);

            string source = RequestHelper.GetSafeQueryString("source");
            if (source != null && source == "commissions")
            {
                btnBack.Value = "Back to Commission screen";
                btnBack.Attributes.Add("onclick", "goToUrl('Commission.aspx?loanid=" + LoanID.ToString() + "&appid=" + ApplicationID.ToString() + "');");
            }
            else if (source == "leadAssignment")
            {
                btnBack.Value = "Back to Lead Assignment";
                btnBack.Attributes.Add("onclick", "goToUrl('" + VirtualRoot + "/los/lead/LeadAssignment.aspx?loanid=" + LoanID.ToString() + "&appid=" + ApplicationID.ToString() + "');");
            }
            else
            {
                btnBack.Value = "Back to agents list";
                btnBack.Attributes.Add("onclick", "goToList();");
            }
        }
        protected override void LoadData()
        {
            EnableJqueryMigrate = false;

            base.LoadData();
        }

        protected override string ListLocation
        {
            get { return VirtualRoot + "/newlos/Status/Agents.aspx?loanid=" + LoanID; }
        }

        protected override DataView GetDataView()
        {
            var dataset = m_dataLoan.sAgentDataSet;
            Agents.AddRoleDescriptionColumn(dataset);
            return dataset.Tables[0].DefaultView;
        }

        protected override CAgentFields RetrieveRecord()
        {
            return m_dataLoan.GetAgentFields(RecordID);
        }

        protected override void BindSingleRecord(CAgentFields field)
        {
            ContactInfo.BindSingleRecord(field);

            LendingLicensesPanel.RegisterLicensesObject(this.Page, "User", field.LicenseInfoList);
            LendingLicensesPanel.RegisterLicensesObject(this.Page, "Company", field.CompanyLicenseInfoList);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            EnableJqueryMigrate = false;
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
