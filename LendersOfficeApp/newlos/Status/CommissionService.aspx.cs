using System;
using DataAccess;
using System.Collections.Generic;
using LendersOffice.Common;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos.Status
{
    public class CommissionServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(CommissionServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sProfitGrossBorPnt_rep = GetString("sProfitGrossBorPnt");
            dataLoan.sProfitGrossBorMb_rep = GetString("sProfitGrossBorMb");
            dataLoan.sProfitRebatePnt_rep = GetString("sProfitRebatePnt");
            dataLoan.sProfitRebateMb_rep = GetString("sProfitRebateMb");
            dataLoan.sProfitAncillaryFee_rep = GetString("sProfitAncillaryFee");
            dataLoan.sProfitAncillaryFeeLckd = GetBool("sProfitAncillaryFeeLckd");
            dataLoan.sProfitExternalFee_rep = GetString("sProfitExternalFee");

            string sAgentsTable = GetString("AgentsTable");
            List<AgentCommission> agents = LendersOffice.Common.ObsoleteSerializationHelper.JsonDeserialize<List<AgentCommission>>(sAgentsTable);
            foreach (AgentCommission ag in agents)
            {
                CAgentFields fields = dataLoan.GetAgentFields(new Guid(ag.RecordId));
                fields.IsCommissionPaid = ag.IsPaid;
                fields.PaymentNotes = ag.Notes;
                fields.Update();
            }
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sProfitGrossBorPnt", dataLoan.sProfitGrossBorPnt_rep);
            SetResult("sProfitGrossBorMb", dataLoan.sProfitGrossBorMb_rep);
            SetResult("sProfitGrossBor", dataLoan.sProfitGrossBor_rep);
            SetResult("sGrossProfit", dataLoan.sGrossProfit_rep);
            SetResult("sNetProfit", dataLoan.sNetProfit_rep);
            SetResult("sCommissionTotal", dataLoan.sCommissionTotal_rep);

            SetResult("sProfitRebatePntp", dataLoan.sProfitRebatePnt_rep);
            SetResult("sProfitRebateMb", dataLoan.sProfitRebateMb_rep);
            SetResult("sProfitRebate", dataLoan.sProfitRebate_rep);
            SetResult("sProfitAncillaryFee", dataLoan.sProfitAncillaryFee_rep);
            SetResult("sProfitAncillaryFeeLckd", dataLoan.sProfitAncillaryFeeLckd);
            SetResult("sProfitExternalFee", dataLoan.sProfitExternalFee_rep);
            SetResult("sProfitTotalFee", dataLoan.sProfitTotalFee_rep);

            string agents = ObsoleteSerializationHelper.JsonSerialize(
                Commission.SerializeAgents(
                    dataLoan.sAgentCollection)
                );

            SetResult("AgentsTable", agents);

            SetResult("sCommissionTotalPaid", dataLoan.sCommissionTotalPaid_rep);
            SetResult("sCommissionTotalOutstanding", dataLoan.sCommissionTotalOutstanding_rep);
        }
    }

	public partial class CommissionService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize()
        {

            AddBackgroundItem("", new CommissionServiceItem());
        }

 
	}
}
