﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOfficeApp.newlos.Underwriting;
using LendersOffice.Constants;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos.Status
{
    /// <summary>
    /// OPM 48185
    /// </summary>
    public partial class ViewBrokerRateLock : BaseLoanPage
    {
        public override bool IsReadOnly
        {
            get
            {
                return true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            PageID = "ViewBrokerRateLock";
            CPageData pageData = CPageData.CreateUsingSmartDependency(LoanID, typeof(ViewBrokerRateLock));
            //this page should be accessible to everyone need to bypass field security 
            pageData.ByPassFieldSecurityCheck = true;
            pageData.InitLoad();
            pageData.LoadBrokerLockData();
            sLpTemplateNm.Text = pageData.sLpTemplateNm;
            sStatusT.Text = pageData.sStatusT_rep;
            sSubmitD.Text = pageData.sSubmitD_rep;
            sSubmitN.Text = pageData.sSubmitN;
            sRateLockStatusT.Text = pageData.sRateLockStatusT_rep;
            sRLckdDays.Text = pageData.sRLckdDays_rep;
            sRLckdD.Text = pageData.sRLckdD_rep;
            sRLckdDTime.Text = pageData.sRLckdDTime_rep;
            sRLckdN.Text = pageData.sRLckdN;
            sRLckdExpiredD.Text = pageData.sRLckdExpiredD_rep;
            sRLckdExpiredN.Text = pageData.sRLckdExpiredN;
            sLpTemplateNmSubmitted.Text = pageData.sLpTemplateNmSubmitted;

   
            sBrokerLockBrokerBaseNoteIR.Text = pageData.sBrokerLockBrokerBaseNoteIR_rep;
            sBrokerLockBrokerBaseBrokComp1PcPrice.Text = pageData.sBrokerLockBrokerBaseBrokComp1PcPrice;
            sBrokerLockBrokerBaseBrokComp1PcFee.Text = pageData.sBrokerLockBrokerBaseBrokComp1PcFee_rep;
            sBrokerLockBrokerBaseRAdjMarginR.Text = pageData.sBrokerLockBrokerBaseRAdjMarginR_rep;
            sBrokerLockBrokerBaseOptionArmTeaserR.Text = pageData.sBrokerLockBrokerBaseOptionArmTeaserR_rep;

   
            sBrokerLockTotVisibleAdjNoteIR.Text = pageData.sBrokerLockTotVisibleAdjNoteIR_rep;
            sBrokerLockTotVisibleAdjBrokComp1PcPrice.Text = pageData.sBrokerLockTotVisibleAdjBrokComp1PcPrice;
            sBrokerLockTotVisibleAdjBrokComp1PcFee.Text = pageData.sBrokerLockTotVisibleAdjBrokComp1PcFee_rep;
            sBrokerLockTotVisibleAdjRAdjMarginR.Text = pageData.sBrokerLockTotVisibleAdjRAdjMarginR_rep;
            sBrokerLockTotVisibleAdjOptionArmTeaserR.Text = pageData.sBrokerLockTotVisibleAdjOptionArmTeaserR_rep;
            sBrokerLockFinalBrokComp1PcPrice.Text = pageData.sBrokerLockFinalBrokComp1PcPrice_rep;

            // OPM 116086
            bool displayOriginatorCompLines = pageData.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;
            if (displayOriginatorCompLines)
            {
                finalPriceLabel.InnerText = "Originator Price";
                originatorAdjRow.Attributes["style"] = "";
                originatorPriceRow.Attributes["style"] = "";

                sBrokerLockOriginatorCompAdjNoteIR.Text = pageData.sBrokerLockOriginatorCompAdjNoteIR_rep;
                sBrokerLockOriginatorCompAdjBrokComp1PcFee.Text = pageData.sBrokerLockOriginatorCompAdjBrokComp1PcFee_rep;
                sBrokerLockOriginatorCompAdjBrokComp1PcPrice.Text = pageData.sBrokerLockOriginatorCompAdjBrokComp1PcPrice;
                sBrokerLockOriginatorCompAdjRAdjMarginR.Text = pageData.sBrokerLockOriginatorCompAdjRAdjMarginR_rep;
                sBrokerLockOriginatorCompAdjOptionArmTeaserR.Text = pageData.sBrokerLockOriginatorCompAdjOptionArmTeaserR_rep;

                sBrokerLockOriginatorPriceNoteIR.Text = pageData.sBrokerLockOriginatorPriceNoteIR_rep;
                sBrokerLockOriginatorPriceBrokComp1PcFee.Text = pageData.sBrokerLockOriginatorPriceBrokComp1PcFee_rep;
                sBrokerLockOriginatorPriceBrokComp1PcPrice.Text = pageData.sBrokerLockOriginatorPriceBrokComp1PcPrice;
                sBrokerLockOriginatorPriceRAdjMarginR.Text = pageData.sBrokerLockOriginatorPriceRAdjMarginR_rep;
                sBrokerLockOriginatorPriceOptionArmTeaserR.Text = pageData.sBrokerLockOriginatorPriceOptionArmTeaserR_rep;
            }

            sNoteIR.Text = pageData.sNoteIR_rep;
            sRAdjMarginR.Text = pageData.sRAdjMarginR_rep;
            sOptionArmTeaserR.Text = !pageData.sIsOptionArm ? "0.000%" : pageData.sOptionArmTeaserR_rep;
            sBrokComp1Pc.Text = pageData.sBrokComp1Pc_rep;
            
            Tools.Bind_sPpmtPenaltyMon(sPpmtPenaltyMon);
            Tools.SetDropDownListValue(sPpmtPenaltyMon, pageData.sPpmtPenaltyMon);

            Tools.Bind_PercentBaseLoanAmountsT(sOriginatorCompensationBaseT);

            switch (pageData.sOriginatorCompensationPaymentSourceT)
            {
                case E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified:
                    sOriginatorCompensationPaymentSourceT.Text = "Payment source not specified";
                    break;
                case E_sOriginatorCompensationPaymentSourceT.BorrowerPaid:
                    sOriginatorCompensationPaymentSourceT.Text = "Borrower paid";
                    break;
                case E_sOriginatorCompensationPaymentSourceT.LenderPaid:
                    sOriginatorCompensationPaymentSourceT.Text = "Lender paid";

                    sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees.Visible = true;
                    sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees.Checked = pageData.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;

                    if (pageData.sHasOriginatorCompensationPlan)
                    {
                        OriginatorPlanDetails.Text = Tools.GetOrigCompPlanDesc(pageData.sOriginatorCompensationPlanT, pageData.sOriginatorCompensationAppliedBy, pageData.sOriginatorCompensationPlanSourceEmployeeName);
                        sOriginatorCompensationPlanAppliedD.Text = pageData.sOriginatorCompensationPlanAppliedD_rep;
                        sOriginatorCompensationEffectiveD.Text = pageData.sOriginatorCompensationEffectiveD_rep;
                        sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo.Checked = pageData.sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo;
                        sOriginatorCompensationPercent.Text = pageData.sOriginatorCompensationPercent_rep;

                        sOriginatorCompensationAdjBaseAmount.Text = pageData.sOriginatorCompensationAdjBaseAmount_rep;
                        sOriginatorCompensationMinAmount.Text = pageData.sOriginatorCompensationMinAmount_rep;
                        sOriginatorCompensationMaxAmount.Text = pageData.sOriginatorCompensationMaxAmount_rep;
                        sOriginatorCompensationFixedAmount.Text = pageData.sOriginatorCompensationFixedAmount_rep;
                        sOriginatorCompensationTotalAmount.Text = pageData.sOriginatorCompensationTotalAmount_rep;
                        sOriginatorCompensationHasMinAmount.Checked = pageData.sOriginatorCompensationHasMinAmount;
                        sOriginatorCompensationHasMaxAmount.Checked = pageData.sOriginatorCompensationHasMaxAmount;
                        OrigCompLenderDetailsLink.Visible = true;
                        Tools.SetDropDownListValue(sOriginatorCompensationBaseT, pageData.sOriginatorCompensationBaseT);
                        OriginatorCompLenderDetails.Visible = true;
                    }
                    break;
            }

            sOriginatorCompensationAmount.Text = pageData.sOriginatorCompensationAmount_rep;
            sOriginatorCompNetPoints.Text = pageData.sOriginatorCompNetPoints_rep;

            sRateLockHistoryXmlContent.Text = RateLockHistoryTable.BuildBrokerRateLockHistoryTable(pageData.sRateLockHistoryXmlContent, false);

            var visibleAdjustmentData = pageData.sVisibleBrokerAdjustmentData;

            AdjustmentsRepeater.DataSource = visibleAdjustmentData.VisibleAdjustments;
            AdjustmentsRepeater.DataBind();
        }


        protected void AdjustmentRepeater_BindItem(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            PricingAdjustment adjustment = (PricingAdjustment)args.Item.DataItem;

            var desc = (TextBox)args.Item.FindControl("description");
            var rate = (TextBox)args.Item.FindControl("rate");
            var price = (TextBox)args.Item.FindControl("price");
            var fee = (TextBox)args.Item.FindControl("fee");
            var margin = (TextBox)args.Item.FindControl("margin");
            var trate = (TextBox)args.Item.FindControl("trate");

            desc.Text = adjustment.Description;
            rate.Text = adjustment.Rate;
            price.Text = adjustment.Price;
            fee.Text = adjustment.Fee;
            margin.Text = adjustment.Margin;
            trate.Text = adjustment.TeaserRate; 

        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            // Init your user controls here, i.e: bind drop down list.

            this.PageTitle = "View Broker Rate Lock";
            this.PageID = "ViewBrokerRateLock";
            this.EnableJqueryMigrate = false;
            RegisterJsScript("utilities.js");
        }
    }
}
