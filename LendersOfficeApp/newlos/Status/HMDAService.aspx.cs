namespace LendersOfficeApp.newlos.Status
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.Hmda;
    using LendersOffice.Security;

    public class HMDAServiceItem : AbstractBackgroundServiceItem
    {
        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal;  }
        }
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(HMDAServiceItem));
        }

        protected bool IsHmdaPurchaser2004Editable(CPageData dataLoan)
        {
            var hmdaDataCollectionRequirementChangeDate = CDateTime.Create(new DateTime(2018, 1, 1));
            return dataLoan.sHmdaActionD.CompareTo(hmdaDataCollectionRequirementChangeDate, true) <= 0;
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataLoan.sHmdaCountyCode = GetString("sHmdaCountyCode");
            dataLoan.sHmdaCensusTract = GetString("sHmdaCensusTract");
            dataLoan.sHmdaLoanDenied = GetBool("sHmdaLoanDenied");
            dataLoan.sHmdaDeniedFormDone = GetBool("sHmdaDeniedFormDone");
            dataLoan.sHmdaCounterOfferMade = GetBool("sHmdaCounterOfferMade");
            dataLoan.sHmdaLoanDeniedBy = GetString("sHmdaLoanDeniedBy");
            dataLoan.sHmdaDeniedFormDoneBy = GetString("sHmdaDeniedFormDoneBy");
            dataLoan.sHmdaCounterOfferMadeBy = GetString("sHmdaCounterOfferMadeBy");
            dataLoan.sHmdaCounterOfferDetails = GetString("sHmdaCounterOfferDetails");
            dataLoan.sHmdaExcludedFromReport = GetBool("sHmdaExcludedFromReport");
            dataLoan.sHmdaReportAsHomeImprov = GetBool("sHmdaReportAsHomeImprov");
            dataLoan.sHmdaActionD_rep = GetString("sHmdaActionD");
            dataLoan.sRejectD_rep = GetString("sRejectD");
            dataLoan.sHmdaDeniedFormDoneD_rep = GetString("sHmdaDeniedFormDoneD");
            dataLoan.sHmdaCounterOfferMadeD_rep = GetString("sHmdaCounterOfferMadeD");
            dataLoan.sHmdaMsaNum = GetString("sHmdaMsaNum");
            dataLoan.sHmdaStateCode = GetString("sHmdaStateCode");
            dataLoan.sFannieFips = GetString("sFannieFips");
            dataLoan.sFannieFipsLckd = GetBool("sFannieFipsLckd");

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
            {
                dataLoan.sHmdaActionTakenT = GetEnum<HmdaActionTaken>("sHmdaActionTakenT");                
                dataLoan.sHmdaDenialReasonOtherDescription = GetString("sHmdaDenialReasonOtherDescription");

                // Get List of reasons.
                string listJson = GetString("sHmdaDenialReasonsList");
                dataLoan.sHmdaDenialReasonsList = SerializationHelper.JsonNetDeserialize<List<string>>(listJson).Select(s => Tools.MapStringToHmdaDenialReason(s)).ToList();
            }
            else
            {
                dataLoan.sHmdaActionTaken = GetString("sHmdaActionTaken");
                dataLoan.sHmdaDenialReason1 = GetString("sHmdaDenialReason1");
                dataLoan.sHmdaDenialReason2 = GetString("sHmdaDenialReason2");
                dataLoan.sHmdaDenialReason3 = GetString("sHmdaDenialReason3");
                dataLoan.sHmdaDenialReason4 = GetString("sHmdaDenialReason4");
            }

            // New fields for 2004 HMDA
            dataLoan.sHmdaAprRateSpreadLckd = GetBool("sHmdaAprRateSpreadLckd");
            dataLoan.sHmdaAprRateSpread = GetString("sHmdaAprRateSpread");
            dataLoan.sHmdaReportAsHoepaLoan = GetBool("sHmdaReportAsHoepaLoan");
            dataLoan.sHmdaLienT = (E_sHmdaLienT) GetInt("sHmdaLienT");
            dataLoan.sHmdaLienTLckd = GetBool("sHmdaLienTLckd");
            dataLoan.sHmdaPropT = (E_sHmdaPropT) GetInt("sHmdaPropT");
            dataLoan.sHmdaPropTLckd = GetBool("sHmdaPropTLckd");


            if(this.IsHmdaPurchaser2004Editable(dataLoan))
            {
                dataLoan.sHmdaPurchaser2004T = (E_sHmdaPurchaser2004T) GetInt("sHmdaPurchaser2004T");
                dataLoan.sHmdaPurchaser2004TLckd = GetBool("sHmdaPurchaser2004TLckd");
            }

            dataLoan.sHmdaPurchaser2015T = (HmdaPurchaser2015T) GetInt("sHmdaPurchaser2015T");
            dataLoan.sHmdaPurchaser2015TLckd = GetBool("sHmdaPurchaser2015TLckd");


            if (dataLoan.sSpState == "NC" || dataLoan.IsTemplate)
            {
                // OPM 81487 - NC Call Report fields
                dataLoan.sAppSubmittedD_rep = GetString("sAppSubmittedD");
                dataLoan.sAppSubmittedDLckd = GetBool("sAppSubmittedDLckd");
                dataLoan.sAppReceivedByLenderD_rep = GetString("sAppReceivedByLenderD");
                dataLoan.sAppReceivedByLenderDLckd = GetBool("sAppReceivedByLenderDLckd");
                dataLoan.sTilInitialDisclosureD_rep = GetString("sTilInitialDisclosureD");
                if (false == dataLoan.BrokerDB.IsProtectDisclosureDates)
                {
                    dataLoan.sTilInitialDisclosureD_rep = GetString("sTilInitialDisclosureD");
                    dataLoan.sCHARMBookletProvidedD_rep = GetString("sCHARMBookletProvidedD");
                }
                dataLoan.sHUDSpecialInfoBookletProvidedD_rep = GetString("sHUDSpecialInfoBookletProvidedD");
                dataLoan.sAffiliatedBusinessDisclosureProvidedD_rep = GetString("sAffiliatedBusinessDisclosureProvidedD");
                dataLoan.sBranchManagerNMLSIdentifier = GetString("sBranchManagerNMLSIdentifier");

                CAgentFields lender = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.CreateNew);
                string lenderNmlsId = GetString("LenderNMLSId");
                if(!lender.IsNewRecord || !string.IsNullOrEmpty(lenderNmlsId))
                {
                    lender.CompanyLoanOriginatorIdentifier = lenderNmlsId;
                    lender.Update();
                }

                dataLoan.sPpmtPenaltyPc_rep = GetString("sPpmtPenaltyPc");
                dataLoan.sUndiscountedIR_rep = GetString("sUndiscountedIR");
                dataLoan.sYSPAmt_rep = GetString("sYSPAmt");
                dataLoan.sNCBorrWaiveRescT = (E_NCCallRprtResponseT)GetInt("sNCBorrWaiveRescT");
                dataLoan.sNCBorrRecvCounselT = (E_NCCallRprtResponseT)GetInt("sNCBorrRecvCounselT");
                dataLoan.sNCHomeImprovPmtsMadeT = (E_NCCallRprtResponseT)GetInt("sNCHomeImprovPmtsMadeT");
                dataLoan.sNCAllowIRToRiseT = (E_NCCallRprtResponseT)GetInt("sNCAllowIRToRiseT");
                dataLoan.sNC25OrMoreAcresT = (E_NCCallRprtResponseT)GetInt("sNC25OrMoreAcresT");
                dataLoan.sNCRefiWithSameLenderT = (E_NCCallRprtResponseT)GetInt("sNCRefiWithSameLenderT");
                if (dataLoan.sIsRefinancing)
                {
                    dataLoan.sRefPurpose = GetString("sRefPurpose");
                }
                if (false == dataLoan.BrokerDB.IsProtectDisclosureDates)
                {
                    dataLoan.Set_sGfeInitialDisclosureD_rep(GetString("sGfeInitialDisclosureD"), E_GFEArchivedReasonT.InitialDislosureDateSetViaHMDA);
                }
            }

            dataLoan.sHmdaLenderHasPreapprovalProgram = GetBool("sHmdaLenderHasPreapprovalProgram");
            dataLoan.sHmdaBorrowerRequestedPreapproval = GetBool("sHmdaBorrowerRequestedPreapproval");
            dataLoan.sHmdaPreapprovalTLckd = GetBool("sHmdaPreapprovalTLckd");
            dataLoan.sHmdaPreapprovalT = (E_sHmdaPreapprovalT)GetInt("sHmdaPreapprovalT");
            dataLoan.sReportFhaGseAusRunsAsTotalRun = GetBool("sReportFhaGseAusRunsAsTotalRun");

            dataLoan.sHmdaCoApplicantSourceTLckd = this.GetBool(nameof(dataLoan.sHmdaCoApplicantSourceTLckd));
            dataLoan.sHmdaCoApplicantSourceT = this.GetEnum<sHmdaCoApplicantSourceT>(nameof(dataLoan.sHmdaCoApplicantSourceT));
            dataLoan.sHmdaCoApplicantSourceSsnLckd = this.GetBool(nameof(dataLoan.sHmdaCoApplicantSourceSsnLckd));
            dataLoan.sHmdaCoApplicantSourceSsn = this.GetString(nameof(dataLoan.sHmdaCoApplicantSourceSsn));

            dataLoan.sLegalEntityIdentifier = GetString("sLegalEntityIdentifier");
            dataLoan.sUniversalLoanIdentifierLckd = GetBool("sUniversalLoanIdentifierLckd");
            dataLoan.sUniversalLoanIdentifier = GetString("sUniversalLoanIdentifier");

            HMDA.BindDataFromControls(dataLoan, dataApp, this);
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("sHmdaPropT", dataLoan.sHmdaPropT);
            SetResult("sHmdaLienT", dataLoan.sHmdaLienT);
            SetResult("sGseSpT", dataLoan.sGseSpT);
            SetResult("sSpAddr", dataLoan.sSpAddr);
            SetResult("sSpCity", dataLoan.sSpCity);
            SetResult("sSpCounty", dataLoan.sSpCounty);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sSpZip", dataLoan.sSpZip);
            SetResult("sFannieFips", dataLoan.sFannieFips);
            SetResult("sFannieFipsLckd", dataLoan.sFannieFipsLckd);

            SetResult("sHmdaAprRateSpreadLckd", dataLoan.sHmdaAprRateSpreadLckd);
            SetResult("sHmdaAprRateSpread", dataLoan.sHmdaAprRateSpread);

            SetResult("sHmdaAprRateSpreadCalcMessage_Hidden", dataLoan.sHmdaAprRateSpreadCalcMessage);
            
            if(this.IsHmdaPurchaser2004Editable(dataLoan))
            {
                SetResult("sHmdaPurchaser2004TLckd", dataLoan.sHmdaPurchaser2004TLckd);
                SetResult("sHmdaPurchaser2004T", dataLoan.sHmdaPurchaser2004T);
            }

            SetResult("sHmdaPurchaser2015TLckd", dataLoan.sHmdaPurchaser2015TLckd);
            SetResult("sHmdaPurchaser2015T", dataLoan.sHmdaPurchaser2015T);

            SetResult("sAppSubmittedD", dataLoan.sAppSubmittedD_rep);
            SetResult("sAppSubmittedDLckd", dataLoan.sAppSubmittedDLckd);
            SetResult("sAppReceivedByLenderD", dataLoan.sAppReceivedByLenderD_rep);
            SetResult("sAppReceivedByLenderDLckd", dataLoan.sAppReceivedByLenderDLckd);

            SetResult("sHmdaLenderHasPreapprovalProgram", dataLoan.sHmdaLenderHasPreapprovalProgram);
            SetResult("sHmdaBorrowerRequestedPreapproval", dataLoan.sHmdaBorrowerRequestedPreapproval);
            SetResult("sHmdaPreapprovalTLckd", dataLoan.sHmdaPreapprovalTLckd);
            SetResult("sHmdaPreapprovalT", dataLoan.sHmdaPreapprovalT);

            SetResult("sLegalEntityIdentifier", dataLoan.sLegalEntityIdentifier);
            SetResult("sUniversalLoanIdentifierLckd", dataLoan.sUniversalLoanIdentifierLckd);
            SetResult("sUniversalLoanIdentifier", dataLoan.sUniversalLoanIdentifier);

            this.SetResult(nameof(dataLoan.sHmdaCoApplicantSourceT), dataLoan.sHmdaCoApplicantSourceT);
            this.SetResult(nameof(dataLoan.sHmdaCoApplicantSourceTLckd), dataLoan.sHmdaCoApplicantSourceTLckd);
            this.SetResult(nameof(dataLoan.sHmdaCoApplicantSourceSsn), dataLoan.sHmdaCoApplicantSourceSsn);
            this.SetResult(nameof(dataLoan.sHmdaCoApplicantSourceSsnLckd), dataLoan.sHmdaCoApplicantSourceSsnLckd);

            HMDA.LoadDataForControls(dataLoan, dataApp, this);
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "SetDeniedBy":
                    SetDeniedBy();
                    break;
                case nameof(GetCoapplicantsForSourceSsn):
                    this.GetCoapplicantsForSourceSsn();
                    break;
                case "UpdateLei":
                    UpdateLei();
                    break;
                default:
                    throw new ArgumentException("unknown method " + methodName);
            }
        }

        protected void SetDeniedBy()
        {
            SetResult("sHmdaLoanDenied", true);
            SetResult("sRejectD", System.DateTime.Today.ToString("d"));
            SetResult("sHmdaLoanDeniedBy", PrincipalFactory.CurrentPrincipal.DisplayName);
        }

        private void GetCoapplicantsForSourceSsn()
        {
            var dataloan = this.ConstructPageDataClass(this.sLId);
            dataloan.InitLoad();

            // Tuple format: name, SSN, selected
            var coapplicantTuples = new List<Tuple<string, string, bool>>(dataloan.nApps);

            var coapplicantSource = this.GetEnum<sHmdaCoApplicantSourceT>(nameof(sHmdaCoApplicantSourceT));
            switch (coapplicantSource)
            {
                case sHmdaCoApplicantSourceT.NoCoApplicant:
                    // No coapplicant -> dropdown will be blank.
                    coapplicantTuples.Add(Tuple.Create(string.Empty, string.Empty, true));
                    break;
                case sHmdaCoApplicantSourceT.CoborrowerOnPrimaryApp:
                    var primaryApp = dataloan.GetAppData(0);
                    if (primaryApp.aCIsValidHmdaApplication)
                    {
                        coapplicantTuples.Add(Tuple.Create(primaryApp.aCNm, primaryApp.aCSsn, true));
                    }
                    else
                    {
                        coapplicantTuples.Add(Tuple.Create(string.Empty, string.Empty, true));
                    }
                    break;
                case sHmdaCoApplicantSourceT.ApplicantOnSecondaryApp:
                    var secondaryApps = this.GetSecondaryAppApplicants(dataloan);
                    if (secondaryApps.Any())
                    {
                        coapplicantTuples.AddRange(secondaryApps);
                    }
                    else
                    {
                        coapplicantTuples.Add(Tuple.Create(string.Empty, string.Empty, true));
                    }
                    break;
                default:
                    throw new UnhandledEnumException(coapplicantSource);
            }

            this.SetResult("Applicants", SerializationHelper.JsonNetSerialize(coapplicantTuples));
        }

        private IEnumerable<Tuple<string, string, bool>> GetSecondaryAppApplicants(CPageData dataloan)
        {
            var secondaryAppApplicants = new List<Tuple<string, string, bool>>(dataloan.nApps);
            var sourceSsn = dataloan.sHmdaCoApplicantSourceSsn;

            for (var i = 1; i < dataloan.nApps; i++)
            {
                var secondaryApp = dataloan.GetAppData(i);
                if (secondaryApp.aBIsValidHmdaApplication)
                {
                    secondaryAppApplicants.Add(Tuple.Create(secondaryApp.aBNm, secondaryApp.aBSsn, secondaryApp.aBSsn == sourceSsn));
                }

                if (secondaryApp.aCIsValidHmdaApplication)
                {
                    secondaryAppApplicants.Add(Tuple.Create(secondaryApp.aCNm, secondaryApp.aCSsn, secondaryApp.aCSsn == sourceSsn));
                }
            }

            if (secondaryAppApplicants.Count > 0 && !secondaryAppApplicants.Any(tuple => tuple.Item3))
            {
                secondaryAppApplicants[0] = Tuple.Create(secondaryAppApplicants[0].Item1, secondaryAppApplicants[0].Item2, true);
            }

            return secondaryAppApplicants;
        }


        private void UpdateLei()
        {
            var dataLoan = this.ConstructPageDataClass(this.sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sLegalEntityIdentifier = dataLoan.Branch.LegalEntityIdentifier;
            dataLoan.Save();

            SetResult("sLegalEntityIdentifier", dataLoan.sLegalEntityIdentifier);
        }
    }
	/// <summary>
	/// Summary description for HMDAService.
	/// </summary>
	public partial class HMDAService : BaseSimpleServiceXmlPage
    {
        protected override void Initialize() 
        {
            AddBackgroundItem("", new HMDAServiceItem());
        }
	}
}
