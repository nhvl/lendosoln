﻿namespace LendersOfficeApp.newlos.Status
{
    using System;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.DocMagicLib;
    using LendersOffice.Security;

    public partial class AlternateLenderPopup : LendersOffice.Common.BaseServicePage
    {
        DocMagicAlternateLender altLender;
        Guid id;            
       
        protected void Page_Load(object sender, EventArgs e)
        {
            sLenderZipcode.SmartZipcode(sLenderCity, sLenderState);
            sBeneficiaryZipcode.SmartZipcode(sBeneficiaryCity, sBeneficiaryState);
            sLossPayeeZipcode.SmartZipcode(sLossPayeeCity, sLossPayeeState);
            sMakePaymentsToZipcode.SmartZipcode(sMakePaymentsToCity, sMakePaymentsToState);
            sWhenRecordedMailToZipcode.SmartZipcode(sWhenRecordedMailToCity, sWhenRecordedMailToState);
            
            id = RequestHelper.GetGuid("lenderID", Guid.Empty);
            if (id == Guid.Empty)
            {
                altLender = new DocMagicAlternateLender();
            }
            else
            {
                altLender = new DocMagicAlternateLender(BrokerUserPrincipal.CurrentPrincipal.BrokerId, id);
            }

            if (!IsPostBack)
            {
                DocMagicAlternateLenderInternalId.Text = altLender.DocMagicAlternateLenderInternalId;

                sLenderName.Text = altLender.LenderName;
                sLenderAddress.Text = altLender.LenderAddress;
                sLenderCity.Text = altLender.LenderCity;
                sLenderState.Value = altLender.LenderState;
                sLenderZipcode.Text = altLender.LenderZip;
                sLenderNonPersonEntity.Checked = altLender.LenderNonPersonEntityIndicator;

                sBeneficiaryName.Text = altLender.BeneficiaryName;
                sBeneficiaryAddress.Text = altLender.BeneficiaryAddress;
                sBeneficiaryCity.Text = altLender.BeneficiaryCity;
                sBeneficiaryState.Value = altLender.BeneficiaryState;
                sBeneficiaryZipcode.Text = altLender.BeneficiaryZip;
                sBeneficiaryNonPersonEntity.Checked = altLender.BeneficiaryNonPersonEntityIndicator;

                sLossPayeeName.Text = altLender.LossPayeeName;
                sLossPayeeAddress.Text = altLender.LossPayeeAddress;
                sLossPayeeCity.Text = altLender.LossPayeeCity;                    
                sLossPayeeState.Value = altLender.LossPayeeState;
                sLossPayeeZipcode.Text = altLender.LossPayeeZip;

                sMakePaymentsToName.Text = altLender.MakePaymentsToName;
                sMakePaymentsToAddress.Text = altLender.MakePaymentsToAddress;
                sMakePaymentsToCity.Text = altLender.MakePaymentsToCity;
                sMakePaymentsToState.Value = altLender.MakePaymentsToState;
                sMakePaymentsToZipcode.Text = altLender.MakePaymentsToZip;

                sWhenRecordedMailToName.Text = altLender.WhenRecodedMailToName;
                sWhenRecordedMailToAddress.Text = altLender.WhenRecodedMailToAddress;
                sWhenRecordedMailToCity.Text = altLender.WhenRecodedMailToCity;
                sWhenRecordedMailToState.Value = altLender.WhenRecodedMailToState;
                sWhenRecordedMailToZipcode.Text = altLender.WhenRecodedMailToZip;
            }
        }

        protected void OnSaveBtn_Click(object sender, EventArgs e)
        {
            altLender.DocMagicAlternateLenderInternalId = DocMagicAlternateLenderInternalId.Text;

            altLender.LenderName = sLenderName.Text;
            altLender.LenderAddress = sLenderAddress.Text;
            altLender.LenderCity = sLenderCity.Text;
            altLender.LenderState = sLenderState.Value;
            altLender.LenderZip = sLenderZipcode.Text;
            altLender.LenderNonPersonEntityIndicator = sLenderNonPersonEntity.Checked;

            altLender.BeneficiaryName = sBeneficiaryName.Text;
            altLender.BeneficiaryAddress = sBeneficiaryAddress.Text;
            altLender.BeneficiaryCity = sBeneficiaryCity.Text;
            altLender.BeneficiaryState = sBeneficiaryState.Value;
            altLender.BeneficiaryZip = sBeneficiaryZipcode.Text;
            altLender.BeneficiaryNonPersonEntityIndicator = sBeneficiaryNonPersonEntity.Checked;

            altLender.LossPayeeName = sLossPayeeName.Text;
            altLender.LossPayeeAddress = sLossPayeeAddress.Text;
            altLender.LossPayeeCity = sLossPayeeCity.Text;
            altLender.LossPayeeState = sLossPayeeState.Value;
            altLender.LossPayeeZip = sLossPayeeZipcode.Text;

            altLender.MakePaymentsToName = sMakePaymentsToName.Text;
            altLender.MakePaymentsToAddress = sMakePaymentsToAddress.Text;
            altLender.MakePaymentsToCity = sMakePaymentsToCity.Text;
            altLender.MakePaymentsToState = sMakePaymentsToState.Value;
            altLender.MakePaymentsToZip = sMakePaymentsToZipcode.Text;

            altLender.WhenRecodedMailToName = sWhenRecordedMailToName.Text;
            altLender.WhenRecodedMailToAddress = sWhenRecordedMailToAddress.Text;
            altLender.WhenRecodedMailToCity = sWhenRecordedMailToCity.Text;
            altLender.WhenRecodedMailToState = sWhenRecordedMailToState.Value;
            altLender.WhenRecodedMailToZip = sWhenRecordedMailToZipcode.Text;

            altLender.Save(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            this.ClientScript.RegisterStartupScript(typeof(AlternateLenderPopup), "Close", "onClosePopup();", true);
        }
    }
}
