﻿#region Generated Code
namespace LendersOfficeApp.newlos.Status
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.CreditReportAuthorization;

    /// <summary>
    /// Provides a control containing the credit report authorization data.
    /// </summary>
    public partial class CreditReportAuthorization : BaseLoanUserControl
    {
        /// <summary>
        /// Binds the data from the <paramref name="serviceItem"/> to the loan and application object.
        /// </summary>
        /// <param name="loan">The loan object to bind data to.</param>
        /// <param name="application">The application object to bind data to.</param>
        /// <param name="serviceItem">The service page binding data from this control.</param>
        /// <param name="controlIdPrefix">The prefix of items in this control, provided by the page.</param>
        public static void BindDataToLoan(CPageData loan, CAppData application, AbstractBackgroundServiceItem serviceItem, string controlIdPrefix)
        {
            var appId = serviceItem.GetGuid(controlIdPrefix + nameof(CurrentApplicationID));
            if (application.aAppId != appId)
            {
                application = loan.GetAppData(appId);
            }

            application.aBCreditAuthorizationD_rep = serviceItem.GetString(controlIdPrefix + nameof(aBCreditAuthorizationD));
            application.aCCreditAuthorizationD_rep = serviceItem.GetString(controlIdPrefix + nameof(aCCreditAuthorizationD));

            // Default to the application's current value when the
            // broker has not enabled verbal credit authorization
            // automation and no value is sent.
            application.aBCreditReportAuthorizationProvidedVerbally = serviceItem.GetBool(
                controlIdPrefix + nameof(aBCreditReportAuthorizationProvidedVerbally),
                application.aBCreditReportAuthorizationProvidedVerbally);

            application.aCCreditReportAuthorizationProvidedVerbally = serviceItem.GetBool(
                controlIdPrefix + nameof(aCCreditReportAuthorizationProvidedVerbally),
                application.aCCreditReportAuthorizationProvidedVerbally);
        }

        /// <summary>
        /// Loads the data from  the loan and application object into the <paramref name="serviceItem"/>.
        /// </summary>
        /// <param name="loan">The loan object to load data from.</param>
        /// <param name="application">The application object to load data from.</param>
        /// <param name="serviceItem">The service page loading data from this control.</param>
        /// <param name="controlIdPrefix">The prefix of items in this control, provided by the page.</param>
        public static void LoadDataFromLoan(CPageData loan, CAppData application, AbstractBackgroundServiceItem serviceItem, string controlIdPrefix)
        {
            var appId = serviceItem.GetGuid(controlIdPrefix + nameof(CurrentApplicationID));
            if (application.aAppId != appId)
            {
                application = loan.GetAppData(appId);
            }

            serviceItem.SetResult(controlIdPrefix + nameof(aBCreditAuthorizationD), application.aBCreditAuthorizationD_rep);
            serviceItem.SetResult(controlIdPrefix + nameof(aCCreditAuthorizationD), application.aCCreditAuthorizationD_rep);
            serviceItem.SetResult(controlIdPrefix + nameof(sAllBorrowersHaveAuthorizedCredit), loan.sAllBorrowersHaveAuthorizedCredit);
            serviceItem.SetResult(controlIdPrefix + nameof(aBCreditReportAuthorizationProvidedVerbally), application.aBCreditReportAuthorizationProvidedVerbally);
            serviceItem.SetResult(controlIdPrefix + nameof(aCCreditReportAuthorizationProvidedVerbally), application.aCCreditReportAuthorizationProvidedVerbally);
        }

        /// <summary>
        /// Binds the specified loan and application object into this instance of the control.
        /// </summary>
        /// <remarks>
        /// This should be called during one of the data loading events.
        /// </remarks>
        /// <param name="loan">The loan object to load data from.</param>
        /// <param name="application">The application object to load data from.</param>
        public void BindDataToControl(CPageData loan, CAppData application)
        {
            this.CurrentApplicationID.Value = application.aAppId.ToString();
            this.aBCreditAuthorizationD.Text = application.aBCreditAuthorizationD_rep;
            this.aCCreditAuthorizationD.Text = application.aCCreditAuthorizationD_rep;
            this.sAllBorrowersHaveAuthorizedCredit.Checked = loan.sAllBorrowersHaveAuthorizedCredit;
            this.aBCreditReportAuthorizationProvidedVerbally.Checked = application.aBCreditReportAuthorizationProvidedVerbally;
            this.aCCreditReportAuthorizationProvidedVerbally.Checked = application.aCCreditReportAuthorizationProvidedVerbally;

            var associations = DocumentAssociation.RetrieveForApplication(loan.sBrokerId, application.aAppId);
            var borrowerDocAssociation = associations.Item1;
            var coborrowerDocAssociation = associations.Item2;
            this.BorrowerCreditReportAuthorizationDocumentId.Value = borrowerDocAssociation?.DocumentId.ToString();
            this.BorrowerCreditReportAuthorizationDocumentDescription.Text = borrowerDocAssociation?.DocumentDescription;
            this.CoBorrowerCreditReportAuthorizationDocumentId.Value = coborrowerDocAssociation?.DocumentId.ToString();
            this.CoBorrowerCreditReportAuthorizationDocumentDescription.Text = coborrowerDocAssociation?.DocumentDescription;
        }

        /// <summary>
        /// The handler for the control's page load event.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The arguments of the event.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var page = (BaseLoanPage)this.Page;

            if (!page.UseNewFramework)
            {
                throw CBaseException.GenericException("The new framework is required to refresh the calculated bit in this control.");
            }

            page.RegisterJsScript("LQBPopup.js");
            page.RegisterService("CreditReportAuthorization", "/newlos/Status/CreditReportAuthorizationService.aspx");

            // The ML global variable may not be defined on certain pages where 
            // this control is present, such as the Order Credit page. Use a hidden
            // field to store the value instead.
            this.AllowIndicatingVerbalCreditReportAuthorization.Value = this.BrokerUser.BrokerDB.AllowIndicatingVerbalCreditReportAuthorization.ToString();
        }
    }
}
