﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewBrokerRateLock.aspx.cs" Inherits="LendersOfficeApp.newlos.Status.ViewBrokerRateLock" EnableEventValidation="false" EnableViewState="false" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>View Broker Rate Lock</title>
    <style type="text/css">
        .hidden { display: none; }
        body { background-color: gainsboro; }
        .modalbox { line-height: 1em; height: 100px; border:3px inset black; top: 150px;  margin-left: 200px; left: 0; right: 0;  width: 425px; display: none; position: absolute; background-color : whitesmoke;}
        .RateLockTable
        {
            margin-top: 10px;
            border-collapse: collapse;
            table-layout: fixed;
        }

        table.RateLockTable td
        {
            border: 1px solid #EEEEEE;
        }

        td.date
        {
            width:150px;
        }
        td.action
        {
            width: 100px;
        }

        td.by
        {
            width: 150px;
        }

        td.lockExpiration{
            width: 60px;
        }
        td.rate, td.price, td.fee, td.margin, td.trate, td.lockfee {
            width: 50px;
        }

        td.reason {
            width: 200px;
        }


        .RateLockHeader
        {
            font-weight: bold;
            font-size: 11px;
            color: white;
            font-family: Arial, Helvetica, sans-serif;
            background-color: #999999;
        }
        .RateLockAlternatingItem
        {
            font-size: 11px;
            color: black;
            font-family: Arial, Helvetica, sans-serif;
            background-color: #cccccc;
        }
        .RateLockItem
        {
            font-size: 11px;
            color: black;
            font-family: Arial, Helvetica, sans-serif;
            background-color: white;
        }

        img { vertical-align: bottom; }
        .CenteringDiv input { margin: 0 auto; text-align: center; }
	    .descriptionField { width: 279px; }
	    .rateField { width: 50px; }
	    .noteField { width: 300px; }
	    .bigNoteField { width: 400px; height: 200px; }

	    .modalPadding { padding: 5px; }
	    .dayField { width: 30px; }
	    .dateField { width: 60px; }
	    .timeField { width: 80px; }
	    .firstCol { width: 150px; }
	    .secondCol { width: 100px;}
	    .bottomMargin { margin-bottom: 10px; }
	    .padding { padding: 5px; }
	    #PricingTable { margin-left: 183px; }
	    fieldset { width: 810px; border: 1px solid black; }
	    .wrapper { padding: 5px; }
	    img { vertical-align: bottom; }
	    #OriginatorCompLenderDetails { height: auto;  width: 390px; padding-top: 5px; padding-bottom: 5px; }
	    #OriginatorCompLenderDetails  table { margin: 0 auto; }
	    #CloseOrigDetails { display: block; margin: 0 auto; text-align: center; }
	    .moneyField { width: 70px; }
    </style>
    <script type="text/javascript">
        function _init() {
            $('#OrigCompLenderDetailsLink').click(function(e) {
                e.preventDefault();
                Modal.ShowPopup('OriginatorCompLenderDetails', null, e);
            });

            $('#CloseOrigDetails').click(function(e) {
                e.preventDefault();
                Modal.Hide();
            });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="MainRightHeader">View Front-end Rate Lock</div>
        <div class="wrapper">
        <fieldset class="bottomMargin">
            <table cellpadding="0" cellspacing="2" style="table-layout:fixed;" >
                <tbody>
                    <tr>
                        <td class="firstCol"><ml:EncodedLabel AssociatedControlID="sLpTemplateNm" runat="server" CssClass="FieldLabel">Loan Program</ml:EncodedLabel></td>
                        <td colspan="3"><asp:TextBox runat="server" ID="sLpTemplateNm" CssClass="noteField" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            <ml:EncodedLabel AssociatedControlID="sLpTemplateNmSubmitted" runat="server" CssClass="FieldLabel">Registered Loan Program</ml:EncodedLabel>
                        </td>
                        <td colspan="3">
                            <asp:TextBox runat="server" CssClass="noteField" ID="sLpTemplateNmSubmitted" ReadOnly="true" LockedField="LockedField" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td> <ml:EncodedLabel AssociatedControlID="sStatusT" runat="server" CssClass="FieldLabel">Loan Status</ml:EncodedLabel></td>
                        <td colspan="3"><asp:TextBox runat="server" ID="sStatusT" ReadOnly="true" LockedField="LockedField" SkipMe="SkipMe"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel"> Registered </td>
                        <td class="secondCol" colspan="3">
                            <table cellpadding="0" cellspacing="0"  width="500px">
                            <tr>
                            <td>
                                <span class="FieldLabel"> Date </span>
                            </td>
                            <td>
                           <span class="FieldLabel"> &nbsp;Comments </span>
                            </td>
                            </tr>
                            <tr>
                            <td >
                            <ml:DateTextBox runat="server" CssClass="dateField" ID="sSubmitD"> </ml:DateTextBox>
                            </td>
                            <td>
                            &nbsp;<asp:TextBox runat="server"  ID="sSubmitN" CssClass="noteField"></asp:TextBox>
                            </td>
                            </tr>
                          </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>

        <fieldset class="bottomMargin">
            <div >  <!-- Start Rate Lock Info Section -->
                <div class="MainRightHeader">
                    &nbsp;
                </div>
                <table cellpadding="0" cellspacing="2" border="0">
                    <tbody>
                    <tr>
                        <td class="FieldLabel firstCol">
                            Rate Lock Status
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="sRateLockStatusT" LockedField="LockedField"  ReadOnly="true" SkipMe="SkipMe"></asp:TextBox>
                        </td>
                        <td style="width:300px">
                            &nbsp;
                        </td>
                        <td  >
                            <ml:EncodedLabel  AssociatedControlID="sPpmtPenaltyMon" CssClass="FieldLabel" runat="server">Prepayment Penalty</ml:EncodedLabel>
                        </td>
                        <td>
                            <asp:dropdownlist id="sPpmtPenaltyMon" runat="server"></asp:dropdownlist>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            <ml:EncodedLabel AssociatedControlID="sRLckdDays" runat="server">Lock Period</ml:EncodedLabel>
                        </td>
                        <td class="FieldLabel">
                            <asp:TextBox MaxLength="4" CssClass="dayField" runat="server" ID="sRLckdDays" ></asp:TextBox>
                            days
                        </td>

                    </tr>
                </tbody>
                </table>
                <table cellpadding="0" cellspacing="2">
                    <thead>
                        <tr>
                            <td class="firstCol">
                                &nbsp;
                            </td>
                            <td class="FieldLabel">
                                Date
                            </td>
                            <td>
                                <span class="FieldLabel">Time </span>
                            </td>
                            <td class="FieldLabel">
                                Comments
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="FieldLabel"><ml:EncodedLabel AssociatedControlID="sRLckdD" runat="server">Rate Lock</ml:EncodedLabel></td>
                            <td>
                                <asp:TextBox CssClass="dateField" runat="server" ID="sRLckdD" ReadOnly="true" SkipMe="SkipMe"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox CssClass="timeField" runat="server" ID="sRLckdDTime" ReadOnly="true" SkipMe="SkipMe"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox  runat="server" id="sRLckdN" CssClass="noteField" ></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ml:EncodedLabel  runat="server" AssociatedControlID="sRLckdExpiredD" class="FieldLabel">Rate Lock Expiration</ml:EncodedLabel>
                            </td>
                            <td>
                                <asp:TextBox runat="server"  CssClass="dateField" ID="sRLckdExpiredD" ReadOnly="true" SkipMe="SkipMe"></asp:TextBox>
                            </td>
                            <td></td>
                            <td>
                                <asp:TextBox runat="server" ID="sRLckdExpiredN"  CssClass="noteField" ></asp:TextBox>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr />
                <table cellpadding="0" cellspacing="0" id="PricingTable" class="bottomMargin">
                    <thead>
                        <tr>
                            <td align="center" class="FieldLabel">
                                &nbsp;
                            </td>
                            <td align="center" class="FieldLabel">
                                Rate
                            </td>
                            <td align="center" class="FieldLabel">
                                Price
                            </td>
                            <td align="center" class="FieldLabel">
                                Fee
                            </td>
                            <td align="center" class="FieldLabel">
                                Margin
                            </td>
                            <td align="center" class="FieldLabel">
                                Teaser<br />Rate
                            </td>
                        </tr>
                    </thead>
                    <tbody>


                        <tr>
                            <td class="FieldLabel"> Base Price </td>
                            <td><asp:TextBox runat="server" CssClass="rateField"  preset="percent"  ID="sBrokerLockBrokerBaseNoteIR"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sBrokerLockBrokerBaseBrokComp1PcPrice"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sBrokerLockBrokerBaseBrokComp1PcFee"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent"  ID="sBrokerLockBrokerBaseRAdjMarginR"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent"   ID="sBrokerLockBrokerBaseOptionArmTeaserR"></asp:TextBox> </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel"> Total Adjustments </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent"   ID="sBrokerLockTotVisibleAdjNoteIR" ReadOnly="true"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sBrokerLockTotVisibleAdjBrokComp1PcPrice" ReadOnly="true"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sBrokerLockTotVisibleAdjBrokComp1PcFee" ReadOnly="true"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent"   ID="sBrokerLockTotVisibleAdjRAdjMarginR" ReadOnly="true"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" preset="percent" CssClass="rateField"   ID="sBrokerLockTotVisibleAdjOptionArmTeaserR" ReadOnly="true"></asp:TextBox> </td>
                        </tr>
                        <tr>
                            <td runat="server" class="FieldLabel" id="finalPriceLabel"> Final Price </td>
                            <td><asp:TextBox runat="server" CssClass="rateField"  preset="percent" ID="sNoteIR"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sBrokerLockFinalBrokComp1PcPrice"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sBrokComp1Pc"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sRAdjMarginR"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent"  ID="sOptionArmTeaserR" ></asp:TextBox> </td>
                        </tr>
                        <tr runat="server" id="originatorAdjRow" style="display: none;">
                            <td class="FieldLabel">Originator Comp Adjustments</td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ReadOnly="true" ID="sBrokerLockOriginatorCompAdjNoteIR"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ReadOnly="true" ID="sBrokerLockOriginatorCompAdjBrokComp1PcPrice"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ReadOnly="true" ID="sBrokerLockOriginatorCompAdjBrokComp1PcFee"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ReadOnly="true" ID="sBrokerLockOriginatorCompAdjRAdjMarginR"></asp:TextBox> </td>
                            <td colspan="2"><asp:TextBox runat="server" CssClass="rateField" ReadOnly="true" preset="percent"  ID="sBrokerLockOriginatorCompAdjOptionArmTeaserR" ></asp:TextBox> </td>
                        </tr>
                        <tr runat="server" id="originatorPriceRow" style="display: none;">
                            <td class="FieldLabel"> Final Price </td>
                            <td><asp:TextBox runat="server" CssClass="rateField"  preset="percent" ID="sBrokerLockOriginatorPriceNoteIR"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sBrokerLockOriginatorPriceBrokComp1PcPrice"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sBrokerLockOriginatorPriceBrokComp1PcFee"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent" ID="sBrokerLockOriginatorPriceRAdjMarginR"></asp:TextBox> </td>
                            <td><asp:TextBox runat="server" CssClass="rateField" preset="percent"  ID="sBrokerLockOriginatorPriceOptionArmTeaserR" ></asp:TextBox> </td>
                        </tr>
                    </tbody>
                </table>
                <asp:Repeater runat="server" ID="AdjustmentsRepeater" OnItemDataBound="AdjustmentRepeater_BindItem">
                    <HeaderTemplate>
                        <div class="FieldLabel bottomMargin" > Adjustments </div>

                        <table cellpadding="0" cellspacing="0" id="AdjustmentTable">
                            <thead>
                                <tr class="GridHeader">
                                    <th style="width:275px" >Description</th>
                                    <th style="width:56px">Rate</th>
                                    <th style="width:56px">Price</th>
                                    <th style="width:56px" >Fee</th>
                                    <th style="width:56px">Margin</th>
                                    <th style="width:56px">Teaser<br />Rate</th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <FooterTemplate>
                            </tbody>
                        </table>
                    </FooterTemplate>

                    <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" ID="description" CssClass="descriptionField"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="rate" CssClass="rateField"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="price" CssClass="rateField"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="fee" CssClass="rateField"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="margin" CssClass="rateField"></asp:TextBox>
                                </td>

                                <td>
                                    <asp:TextBox runat="server" ID="trate" CssClass="rateField"></asp:TextBox>
                                </td>
                            </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </div> <!-- End Rate Lock Info Section -->
        </fieldset>

        <fieldset>
            <table>
                <tr>
                    <td class="FieldLabel">Originator Compensation</td>
                    <td class="FieldLabel">Amount</td>
                    <td class="FieldLabel" colspan="2">Net Points</td>
                </tr>
                <tr>
                    <td class="FieldLabel"> <ml:EncodedLabel runat="server" ID="sOriginatorCompensationPaymentSourceT"></ml:EncodedLabel> &nbsp;&nbsp;&nbsp;<a href="#" id="OrigCompLenderDetailsLink"  visible="false" runat="server" >details</a> </td>
                    <td><asp:TextBox runat="server" ReadOnly="true" ID="sOriginatorCompensationAmount"></asp:TextBox></td>
                    <td><asp:TextBox runat="server" ReadOnly="true" ID="sOriginatorCompNetPoints" preset="percent" class="rateField" ></asp:TextBox></td>
                    <td class="FieldLabel"><asp:CheckBox runat="server" ReadOnly="true" ID="sOriginatorCompensationLenderFeeOptionT_InAdditionToLenderFees" Visible="false" Text="Add lender paid originator compensation to fees"/> </td>
                </tr>
            </table>
        </fieldset>

        <div>
            <div class="FieldLabel">Rate Lock History</div>
            <div id="RateLockHistory">
                <ml:PassthroughLiteral runat="server" ID="sRateLockHistoryXmlContent"></ml:PassthroughLiteral>
            </div>
        </div>

        <asp:Panel runat="server" ID="OriginatorCompLenderDetails" Visible="false" CssClass="modalbox">
            <table cellspacing="0">
                <tr>
                    <td>
                        <table class="OriginatorCompensation_InnerInfoTable" cellspacing="0">
                            <tr>
                                <td class="FormTableHeader" colspan="4">
                                  <ml:EncodedLiteral runat="server" ID="OriginatorPlanDetails"></ml:EncodedLiteral>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" colspan="2">
                                    Originator compensation set
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="sOriginatorCompensationPlanAppliedD"  CssClass="dateField" ReadOnly="true"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" colspan="2">
                                    Effective date of compensation plan
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="sOriginatorCompensationEffectiveD" CssClass="dateField" ReadOnly="true"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                            <td class="FieldLabel" colspan="3">
                                <asp:CheckBox id="sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo" runat="server" Enabled="false" Text="Compensation only paid for the 1st lien of a combo" />                            </td>
                            <td>
                            </td>
                        </tr>

                            <tr>
                                <td class="FieldLabel" colspan="3">

                                    <asp:TextBox runat="server" ID="sOriginatorCompensationPercent" CssClass="rateField" ReadOnly="true"></asp:TextBox>
                                    of the
                                    <asp:DropDownList runat="server" ID="sOriginatorCompensationBaseT" Enabled="false"></asp:DropDownList>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="sOriginatorCompensationAdjBaseAmount" CssClass="moneyField" ReadOnly="true"></asp:TextBox>

                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" style="width: 140px">
                                 &nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" ID="sOriginatorCompensationHasMinAmount" Enabled="false" />
                                    Minimum
                                </td>
                                <td>

                                 <asp:TextBox runat="server" ID="sOriginatorCompensationMinAmount" CssClass="moneyField" ReadOnly="true"></asp:TextBox>

                                </td>
                                <td colspan="2" rowspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                  &nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" ID="sOriginatorCompensationHasMaxAmount" CssClass="moneyField" Enabled="false" />
                                    Maximum
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="sOriginatorCompensationMaxAmount" CssClass="moneyField" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" colspan="3">
                                    Fixed amount
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="sOriginatorCompensationFixedAmount" CssClass="moneyField" ReadOnly="true"></asp:TextBox>


                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <hr style="height: 1px; color: black;" />
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" colspan="3" >
                                    Total lender-paid compensation
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="sOriginatorCompensationTotalAmount" CssClass="moneyField" ReadOnly="true"></asp:TextBox>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <a href="#" id="CloseOrigDetails"  >[close]</a>
        </asp:Panel>

    </div>
    </form>
</body>
</html>
