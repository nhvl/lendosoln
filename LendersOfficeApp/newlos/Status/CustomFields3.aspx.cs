///
/// Author: Matthew Flynn
/// 
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MeridianLink.CommonControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Status
{
    public partial class CustomFields3 : LendersOfficeApp.newlos.BaseLoanPage
    {

        protected void PageLoad(object sender, System.EventArgs e)
        {

        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Custom Fields 41-60";
            this.PageID = "StatusCustomFields3";
            // this.PDFPrintClass = typeof(LendersOffice.Pdf._____);
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(CustomFields3));
            dataLoan.InitLoad();

            sCustomField41Desc.Text = dataLoan.sCustomField41Desc;
            sCustomField42Desc.Text = dataLoan.sCustomField42Desc;
            sCustomField43Desc.Text = dataLoan.sCustomField43Desc;
            sCustomField44Desc.Text = dataLoan.sCustomField44Desc;
            sCustomField45Desc.Text = dataLoan.sCustomField45Desc;
            sCustomField46Desc.Text = dataLoan.sCustomField46Desc;
            sCustomField47Desc.Text = dataLoan.sCustomField47Desc;
            sCustomField48Desc.Text = dataLoan.sCustomField48Desc;
            sCustomField49Desc.Text = dataLoan.sCustomField49Desc;
            sCustomField50Desc.Text = dataLoan.sCustomField50Desc;
            sCustomField51Desc.Text = dataLoan.sCustomField51Desc;
            sCustomField52Desc.Text = dataLoan.sCustomField52Desc;
            sCustomField53Desc.Text = dataLoan.sCustomField53Desc;
            sCustomField54Desc.Text = dataLoan.sCustomField54Desc;
            sCustomField55Desc.Text = dataLoan.sCustomField55Desc;
            sCustomField56Desc.Text = dataLoan.sCustomField56Desc;
            sCustomField57Desc.Text = dataLoan.sCustomField57Desc;
            sCustomField58Desc.Text = dataLoan.sCustomField58Desc;
            sCustomField59Desc.Text = dataLoan.sCustomField59Desc;
            sCustomField60Desc.Text = dataLoan.sCustomField60Desc;



            UpdateTextBox("sCustomField41Desc", sCustomField41Desc, dataLoan);
            UpdateTextBox("sCustomField42Desc", sCustomField42Desc, dataLoan);
            UpdateTextBox("sCustomField43Desc", sCustomField43Desc, dataLoan);
            UpdateTextBox("sCustomField44Desc", sCustomField44Desc, dataLoan);
            UpdateTextBox("sCustomField45Desc", sCustomField45Desc, dataLoan);
            UpdateTextBox("sCustomField46Desc", sCustomField46Desc, dataLoan);
            UpdateTextBox("sCustomField47Desc", sCustomField47Desc, dataLoan);
            UpdateTextBox("sCustomField48Desc", sCustomField48Desc, dataLoan);
            UpdateTextBox("sCustomField49Desc", sCustomField49Desc, dataLoan);
            UpdateTextBox("sCustomField50Desc", sCustomField50Desc, dataLoan);
            UpdateTextBox("sCustomField51Desc", sCustomField51Desc, dataLoan);
            UpdateTextBox("sCustomField52Desc", sCustomField52Desc, dataLoan);
            UpdateTextBox("sCustomField53Desc", sCustomField53Desc, dataLoan);
            UpdateTextBox("sCustomField54Desc", sCustomField54Desc, dataLoan);
            UpdateTextBox("sCustomField55Desc", sCustomField55Desc, dataLoan);
            UpdateTextBox("sCustomField56Desc", sCustomField56Desc, dataLoan);
            UpdateTextBox("sCustomField57Desc", sCustomField57Desc, dataLoan);
            UpdateTextBox("sCustomField58Desc", sCustomField58Desc, dataLoan);
            UpdateTextBox("sCustomField59Desc", sCustomField59Desc, dataLoan);
            UpdateTextBox("sCustomField60Desc", sCustomField60Desc, dataLoan);
            
    
            sCustomField41D.Text = dataLoan.sCustomField41D_rep;
            sCustomField42D.Text = dataLoan.sCustomField42D_rep;
            sCustomField43D.Text = dataLoan.sCustomField43D_rep;
            sCustomField44D.Text = dataLoan.sCustomField44D_rep;
            sCustomField45D.Text = dataLoan.sCustomField45D_rep;
            sCustomField46D.Text = dataLoan.sCustomField46D_rep;
            sCustomField47D.Text = dataLoan.sCustomField47D_rep;
            sCustomField48D.Text = dataLoan.sCustomField48D_rep;
            sCustomField49D.Text = dataLoan.sCustomField49D_rep;
            sCustomField50D.Text = dataLoan.sCustomField50D_rep;
            sCustomField51D.Text = dataLoan.sCustomField51D_rep;
            sCustomField52D.Text = dataLoan.sCustomField52D_rep;
            sCustomField53D.Text = dataLoan.sCustomField53D_rep;
            sCustomField54D.Text = dataLoan.sCustomField54D_rep;
            sCustomField55D.Text = dataLoan.sCustomField55D_rep;
            sCustomField56D.Text = dataLoan.sCustomField56D_rep;
            sCustomField57D.Text = dataLoan.sCustomField57D_rep;
            sCustomField58D.Text = dataLoan.sCustomField58D_rep;
            sCustomField59D.Text = dataLoan.sCustomField59D_rep;
            sCustomField60D.Text = dataLoan.sCustomField60D_rep;
            sCustomField41Money.Text = dataLoan.sCustomField41Money_rep;
            sCustomField42Money.Text = dataLoan.sCustomField42Money_rep;
            sCustomField43Money.Text = dataLoan.sCustomField43Money_rep;
            sCustomField44Money.Text = dataLoan.sCustomField44Money_rep;
            sCustomField45Money.Text = dataLoan.sCustomField45Money_rep;
            sCustomField46Money.Text = dataLoan.sCustomField46Money_rep;
            sCustomField47Money.Text = dataLoan.sCustomField47Money_rep;
            sCustomField48Money.Text = dataLoan.sCustomField48Money_rep;
            sCustomField49Money.Text = dataLoan.sCustomField49Money_rep;
            sCustomField50Money.Text = dataLoan.sCustomField50Money_rep;
            sCustomField51Money.Text = dataLoan.sCustomField51Money_rep;
            sCustomField52Money.Text = dataLoan.sCustomField52Money_rep;
            sCustomField53Money.Text = dataLoan.sCustomField53Money_rep;
            sCustomField54Money.Text = dataLoan.sCustomField54Money_rep;
            sCustomField55Money.Text = dataLoan.sCustomField55Money_rep;
            sCustomField56Money.Text = dataLoan.sCustomField56Money_rep;
            sCustomField57Money.Text = dataLoan.sCustomField57Money_rep;
            sCustomField58Money.Text = dataLoan.sCustomField58Money_rep;
            sCustomField59Money.Text = dataLoan.sCustomField59Money_rep;
            sCustomField60Money.Text = dataLoan.sCustomField60Money_rep;
            sCustomField41Pc.Text = dataLoan.sCustomField41Pc_rep;
            sCustomField42Pc.Text = dataLoan.sCustomField42Pc_rep;
            sCustomField43Pc.Text = dataLoan.sCustomField43Pc_rep;
            sCustomField44Pc.Text = dataLoan.sCustomField44Pc_rep;
            sCustomField45Pc.Text = dataLoan.sCustomField45Pc_rep;
            sCustomField46Pc.Text = dataLoan.sCustomField46Pc_rep;
            sCustomField47Pc.Text = dataLoan.sCustomField47Pc_rep;
            sCustomField48Pc.Text = dataLoan.sCustomField48Pc_rep;
            sCustomField49Pc.Text = dataLoan.sCustomField49Pc_rep;
            sCustomField50Pc.Text = dataLoan.sCustomField50Pc_rep;
            sCustomField51Pc.Text = dataLoan.sCustomField51Pc_rep;
            sCustomField52Pc.Text = dataLoan.sCustomField52Pc_rep;
            sCustomField53Pc.Text = dataLoan.sCustomField53Pc_rep;
            sCustomField54Pc.Text = dataLoan.sCustomField54Pc_rep;
            sCustomField55Pc.Text = dataLoan.sCustomField55Pc_rep;
            sCustomField56Pc.Text = dataLoan.sCustomField56Pc_rep;
            sCustomField57Pc.Text = dataLoan.sCustomField57Pc_rep;
            sCustomField58Pc.Text = dataLoan.sCustomField58Pc_rep;
            sCustomField59Pc.Text = dataLoan.sCustomField59Pc_rep;
            sCustomField60Pc.Text = dataLoan.sCustomField60Pc_rep;
            sCustomField41Bit.Checked = dataLoan.sCustomField41Bit;
            sCustomField42Bit.Checked = dataLoan.sCustomField42Bit;
            sCustomField43Bit.Checked = dataLoan.sCustomField43Bit;
            sCustomField44Bit.Checked = dataLoan.sCustomField44Bit;
            sCustomField45Bit.Checked = dataLoan.sCustomField45Bit;
            sCustomField46Bit.Checked = dataLoan.sCustomField46Bit;
            sCustomField47Bit.Checked = dataLoan.sCustomField47Bit;
            sCustomField48Bit.Checked = dataLoan.sCustomField48Bit;
            sCustomField49Bit.Checked = dataLoan.sCustomField49Bit;
            sCustomField50Bit.Checked = dataLoan.sCustomField50Bit;
            sCustomField51Bit.Checked = dataLoan.sCustomField51Bit;
            sCustomField52Bit.Checked = dataLoan.sCustomField52Bit;
            sCustomField53Bit.Checked = dataLoan.sCustomField53Bit;
            sCustomField54Bit.Checked = dataLoan.sCustomField54Bit;
            sCustomField55Bit.Checked = dataLoan.sCustomField55Bit;
            sCustomField56Bit.Checked = dataLoan.sCustomField56Bit;
            sCustomField57Bit.Checked = dataLoan.sCustomField57Bit;
            sCustomField58Bit.Checked = dataLoan.sCustomField58Bit;
            sCustomField59Bit.Checked = dataLoan.sCustomField59Bit;
            sCustomField60Bit.Checked = dataLoan.sCustomField60Bit;
            sCustomField41Notes.Text = dataLoan.sCustomField41Notes;
            sCustomField42Notes.Text = dataLoan.sCustomField42Notes;
            sCustomField43Notes.Text = dataLoan.sCustomField43Notes;
            sCustomField44Notes.Text = dataLoan.sCustomField44Notes;
            sCustomField45Notes.Text = dataLoan.sCustomField45Notes;
            sCustomField46Notes.Text = dataLoan.sCustomField46Notes;
            sCustomField47Notes.Text = dataLoan.sCustomField47Notes;
            sCustomField48Notes.Text = dataLoan.sCustomField48Notes;
            sCustomField49Notes.Text = dataLoan.sCustomField49Notes;
            sCustomField50Notes.Text = dataLoan.sCustomField50Notes;
            sCustomField51Notes.Text = dataLoan.sCustomField51Notes;
            sCustomField52Notes.Text = dataLoan.sCustomField52Notes;
            sCustomField53Notes.Text = dataLoan.sCustomField53Notes;
            sCustomField54Notes.Text = dataLoan.sCustomField54Notes;
            sCustomField55Notes.Text = dataLoan.sCustomField55Notes;
            sCustomField56Notes.Text = dataLoan.sCustomField56Notes;
            sCustomField57Notes.Text = dataLoan.sCustomField57Notes;
            sCustomField58Notes.Text = dataLoan.sCustomField58Notes;
            sCustomField59Notes.Text = dataLoan.sCustomField59Notes;
            sCustomField60Notes.Text = dataLoan.sCustomField60Notes;
            

        }

        private void UpdateTextBox(string id, TextBox tb, CPageData dataLoan)
        {
            if (dataLoan.IsCustomFieldGloballyDefined(id))
            {
                tb.ReadOnly = true;
                tb.BackColor = Color.Gainsboro;
                tb.ForeColor = Color.Black;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion
    }
}
