﻿<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ContactFieldMapper.ascx.cs" Inherits="LendersOfficeApp.newlos.Status.ContactFieldMapper" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="DataAccess"%>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

    <td class="FieldLabel" nowrap>
        <asp:RadioButton ID="m_rbUseOfficialContact" runat="server" GroupName="PreparerFlow" />
        Same as <a href="#" onclick="linkMe('../Status/Agents.aspx')">Official Contact</a>:
    </td>
    <td nowrap>
        <asp:DropDownList ID="m_officialContactList" runat="server"/>
    </td>
</tr>
<tr>
    <td class="FieldLabel" nowrap>
        <asp:RadioButton ID="m_rbManualOverride" runat="server" GroupName="PreparerFlow" Text="Set Manually" CssClass="FieldLabel"/>
    </td>
    <td nowrap>
        <asp:HyperLink ID="m_linkPickFromContact" runat="server" Text="Pick from Contacts" href="#" />
        <asp:HyperLink ID="m_linkAddToContact" runat="server" Text="Add to Contacts" href="#" />
    </td>
