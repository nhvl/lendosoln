﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AgentRecord.ascx.cs" Inherits="LendersOfficeApp.newlos.Status.AgentRecord1" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="LendersOffice.Common" %>

<style type="text/css">
    .alignRight { text-align: right; }
	.narrow { width: 110px; }
	.verticalAlignHack { padding-bottom: .15em; width: 90px; }
	.padRight { padding-right: 5px; }
</style>

<script type="text/javascript">
    function _initControl() {
        addTrimmingToAspRegExValidator(<%= AspxTools.JsGetElementById(EmailValidator) %>);

        var oAgentRoleT = <%= AspxTools.JsGetElementById(AgentRoleT) %>;
        if (!oAgentRoleT.disabled)
            oAgentRoleT.focus();
        
        document.getElementById('AffiliateFields').style.display = ( <%= AspxTools.JsBool(m_showAffilates) %> ? 'inline' : 'none');
        document.getElementById('IsLenderAffiliate_Row').style.display = ( <%= AspxTools.JsBool(m_showAffilates) %> ? 'none' : 'inline');
        document.getElementById('OverrideLicenses_Row').style.display = ( <%= AspxTools.JsBool(m_allowToggleOverride) %> ? 'inline' : 'none');

        $("[name='linkPickFromContact']").click(onPickFromContact);

        refreshUI();

        $(".ShowForPicker").hide();

        <% if (IsFeeContactPicker) { %>
        $(".HideForPicker").hide();
        $(".ShowForPicker").show();
        $(".ExpandedOnly").hide();
        <% } %>
    }
    
    function ToggleExpanded() {
        $(".ExpandedOnly").toggle();

        if($("#toggleExpanded").text() == "show more") {
            $("#toggleExpanded").text("show less");
        }
        else {
            $("#toggleExpanded").text("show more");
        }
    }
      
    function CalculateRelationships()
    {
        var args = new Object();
        args["LoanID"] = <%= AspxTools.JsString(LoanID) %>;
        args["EmployeeId"] = <%= AspxTools.JsGetElementById(EmployeeId) %>.value;
        args["AgentSourceT"] = <%= AspxTools.JsGetElementById(AgentSourceT) %>.value;
        args["BrokerLevelAgentID"] = <%= AspxTools.JsGetElementById(BrokerLevelAgentID) %>.value;
      
        var result = gService.agents.call("CalculateRelationships", args);
        if (!result.error) {
            var isLenderCB = <%= AspxTools.JsGetElementById(IsLender) %>;
            var isOriginatorCB = <%= AspxTools.JsGetElementById(IsOriginator) %>;

            // OPM 227085, ML, 12/29/2015
            // We want the "IsLender" or "IsOriginator" checkboxes to be checked if either:
            // 1. The contact selected from the rolodex is a Lender or Broker, or
            // 2. The relationship calculated lists the contact as a Lender or Broker
            isLenderCB.checked = isLenderCB.checked || result.value["IsLender"] == "True";
            isOriginatorCB.checked = isOriginatorCB.checked || result.value["IsOriginator"] == "True";

            <%= AspxTools.JsGetElementById(IsLenderAffiliate) %>.checked = result.value["IsLenderAffiliate"] == "True";
            <%= AspxTools.JsGetElementById(IsOriginatorAffiliate) %>.checked = result.value["IsOriginatorAffiliate"] == "True";
        }
    }

    function onPickFromContact() {
        setCompanyAddress();

        // Ensure the county is bound correctly in case the state has changed.
        var state = <%= AspxTools.JsGetElementById(State) %>;
        var countyDropdown = <%= AspxTools.JsGetElementById(CountyDropdown) %>;

        UpdateCounties(state, countyDropdown, null, function() {
            var selectedCounty = <%= AspxTools.JsGetElementById(County) %>.value;
            if (selectedCounty !== '') {
                countyDropdown.value = selectedCounty;
            }
            else {
                setCountyByZip();
            }

            onCountyDropdownChanged();
        });
    }

    function onCountyDropdownChanged() {
        // Cache the selected county in the hidden county field.
        var selectedCounty = <%= AspxTools.JsGetElementById(CountyDropdown) %>.value;
        <%= AspxTools.JsGetElementById(County) %>.value = selectedCounty;
    }
      
    function setCompanyAddress()
    {
        var LicenseNumOfCompany = $("#LicenseNumOfCompany").val();
        //get the license num
        if(!!LicenseNumOfCompany)
        {
            var licenseNum = $('[id*="CompanyLendingLicenseNumber"]').filter(function() { return $(this).val() === LicenseNumOfCompany; }).first();
            var licenseNumId = licenseNum.attr("id");
            
            if( typeof licenseNumId !== "undefined" )
            {
                var licenseIndex = licenseNumId.replace("CompanyLendingLicenseNumber", "");

                $("#CompanyName").val($("#CompanyDisplayName" + licenseIndex).val());
                $("#StreetAddr").val($("#CompanyStreet" + licenseIndex).val());
                $("#City").val($("#CompanyCity" + licenseIndex).val());
                $("#State").val($("#CompanyAddrState" + licenseIndex).val());
                $("#Zip").val($("#CompanyZip" + licenseIndex).val());
                $("#PhoneOfCompany").val($("#CompanyPhone" + licenseIndex).val());
            }
        }

        if ( <%= AspxTools.JsBool(m_showAffilates) %> )
        {
            CalculateRelationships();
        }

        setLicenseUI();
    }

    function setCountyByZip()
    {
        var zip = <%= AspxTools.JsGetElementById(Zip) %>.value;
        if (zip == '') {
            return;
        }

        var args = { "Zip": zip };

        var result = gService.agents.call("GetCountyByZip", args);

        if (result && result.value) {
            <%= AspxTools.JsGetElementById(County) %>.value = result.value["County"];
        }
        else {
            <%= AspxTools.JsGetElementById(County) %>.value = '';
        }
    }

    function setLicenseUI()
    { 
        var bToggle = <%= AspxTools.JsGetElementById(OverrideLicenses) %>.checked;
        <%= AspxTools.JsGetElementById(LicenseNumOfCompany) %>.readOnly = !bToggle;
        <%= AspxTools.JsGetElementById(LicenseNum) %>.readOnly = !bToggle;
        <%= AspxTools.JsGetElementById(LicenseNumOfCompany) %>.style.backgroundColor =  bToggle ? "" : gReadonlyBackgroundColor;
        <%= AspxTools.JsGetElementById(LicenseNum) %>.style.backgroundColor =  bToggle ? "" : gReadonlyBackgroundColor;
        
    }
    function resetAgentFields() {
        <%= AspxTools.JsGetElementById(AgentRoleT) %>.value = '0';
        
        <%= AspxTools.JsGetElementById(OtherAgentRoleTDesc) %>.value = '';
        <%= AspxTools.JsGetElementById(AgentName) %>.value = '';
        <%= AspxTools.JsGetElementById(CompanyName) %>.value = '';
        <%= AspxTools.JsGetElementById(DepartmentName) %>.value = '';
        <%= AspxTools.JsGetElementById(StreetAddr) %>.value = '';
        <%= AspxTools.JsGetElementById(City) %>.value = '';
        <%= AspxTools.JsGetElementById(State) %>.value = '';
        <%= AspxTools.JsGetElementById(Zip) %>.value = '';
        <%= AspxTools.JsGetElementById(County) %>.value = '';
        <%= AspxTools.JsGetElementById(Phone) %>.value = '';
        <%= AspxTools.JsGetElementById(FaxNum) %>.value = '';
        
        <%= AspxTools.JsGetElementById(CellPhone) %>.value = '';
        <%= AspxTools.JsGetElementById(PagerNum) %>.value = '';
        <%= AspxTools.JsGetElementById(EmailAddr) %>.value = '';
        <%= AspxTools.JsGetElementById(LicenseNum) %>.value = '';
        <%= AspxTools.JsGetElementById(LicenseNumOfCompany) %>.value = '';
        <%= AspxTools.JsGetElementById(CaseNum) %>.value = '';
        
        <%= AspxTools.JsGetElementById(InvestorSoldDate) %>.value = '';
        <%= AspxTools.JsGetElementById(InvestorBasisPoints) %>.value = '';


        <%= AspxTools.JsGetElementById(Notes) %>.value = '';
        <%= AspxTools.JsGetElementById(IsListedInGFEProviderForm) %>.checked = false;
        <%= AspxTools.JsGetElementById(IsLenderAssociation) %>.checked = false;
        <%= AspxTools.JsGetElementById(IsLenderAffiliate) %>.checked = false;
        <%= AspxTools.JsGetElementById(IsLenderAffiliate_Relation) %>.checked = false;
        <%= AspxTools.JsGetElementById(IsLenderRelative) %>.checked = false;
        <%= AspxTools.JsGetElementById(HasLenderRelationship) %>.checked = false;
        <%= AspxTools.JsGetElementById(HasLenderAccountLast12Months) %>.checked = false;
        <%= AspxTools.JsGetElementById(IsUsedRepeatlyByLenderLast12Months) %>.checked = false;
        
        <%= AspxTools.JsGetElementById(PhoneOfCompany) %>.value = '';
        <%= AspxTools.JsGetElementById(FaxOfCompany) %>.value = '';
        <%= AspxTools.JsGetElementById(IsNotifyWhenLoanStatusChange) %>.checked = false;
        <%= AspxTools.JsGetElementById(CompanyLoanOriginatorIdentifier) %>.value = '';
        <%= AspxTools.JsGetElementById(LoanOriginatorIdentifier) %>.value = '';
        <%= AspxTools.JsGetElementById(EmployeeId) %>.value = '';

        <%= AspxTools.JsGetElementById(IsLender) %>.checked = false;
        <%= AspxTools.JsGetElementById(IsOriginator) %>.checked = false;
        <%= AspxTools.JsGetElementById(IsLenderAffiliate) %>.checked = false;
        <%= AspxTools.JsGetElementById(IsOriginatorAffiliate) %>.checked = false; 
        <%= AspxTools.JsGetElementById(OverrideLicenses) %>.checked = false; 
        changeInvestorDisplay();
        setLicenseUI();
        f_resetCommisionFields(); 
    }
  
    function f_resetCommisionFields() {
        var fieldsToReset = [
            <%= AspxTools.JsGetElementById(CommissionPointOfLoanAmount) %>, 
            <%= AspxTools.JsGetElementById(CommissionPointOfGrossProfit) %>, 
            <%= AspxTools.JsGetElementById(Commission) %>,
            <%= AspxTools.JsGetElementById(CommissionMinBase) %> 
        ];
    
        for( var i = 0; i < fieldsToReset.length; i++){
            if( fieldsToReset[i] ) {
                fieldsToReset[i].value = ''; 
            }
        }
    }
    
    function refreshUI() {
        var isUseOriginatorCompensationAmount = document.getElementById('IsUseOriginatorCompensationAmount');
        if( isUseOriginatorCompensationAmount != null )
        {
            var row = <%= AspxTools.JsGetElementById(CommissionInfoRow) %> ;
            if (row != null)
            {
                row.style.display = isUseOriginatorCompensationAmount.value === 'True' ? 'none' : '';
            }
        }
     
        changeInvestorDisplay();
        setLicenseUI();
    
        if (typeof(Page_ClientValidate) == 'function' || typeof(Page_ClientValidate) == 'object')
            Page_ClientValidate();
    }
    
    function changeInvestorDisplay() {
      var d = "";
      var otherDisplay = "none";
      
      if (<%= AspxTools.JsGetElementById(AgentRoleT) %>.value != 10) {
        // Agent Role is not Investor.
        d = "none";
      } 
      if (<%= AspxTools.JsGetElementById(AgentRoleT) %>.value == 0) {
        // Agent role is other. Display other description edit textfield.
        otherDisplay = "";
      } 

      document.getElementById('InvestorTable').style.display = d;
      <%= AspxTools.JsGetElementById(OtherAgentRoleTDesc) %>.style.display = otherDisplay;     

      ToggleEnableChildInputCtrls("tblLenderRelOptions", <%= AspxTools.JsGetElementById(IsListedInGFEProviderForm) %>.checked);
    }
    
    function ToggleEnableChildInputCtrls(parentId, isEnabled)
    {
        var parent = document.getElementById(parentId);
        var childCtrls = parent.getElementsByTagName("input");        
        for(var i=0; i <childCtrls.length; i++)
        {            
            childCtrls[i].disabled = !isEnabled;
            if(!isEnabled && (childCtrls[i].getAttribute("type") == "checkbox"))
            childCtrls[i].checked = false;
        }
    }
   
	function updatePrepareByFields() 
	{
		document.getElementById("IsApplyToFormsUponSave").value = "True";
		updateDirtyBit();
		var saved = saveMe();
		
		if ( saved ) 
		{
			alert("Done.");
		}
	}
 
    function f_calculateCommission() {
      var args = new Object();
      args["LoanID"] = <%= AspxTools.JsString(LoanID) %>;
      args["RecordID"] = <%= AspxTools.JsString(RecordID) %>;
      args["CommissionPointOfLoanAmount"] = <%= AspxTools.JsGetElementById(CommissionPointOfLoanAmount) %>.value;
      args["CommissionPointOfGrossProfit"] = <%= AspxTools.JsGetElementById(CommissionPointOfGrossProfit) %>.value;
      args["CommissionMinBase"] = <%= AspxTools.JsGetElementById(CommissionMinBase) %>.value;
      
      var result = gService.agents.call("CalculateCommission", args);
      if (!result.error) {
        <%= AspxTools.JsGetElementById(Commission) %>.value = result.value["Commission"];
      }

    }
    
    function f_sendEmail() {
      var email = <%= AspxTools.JsGetElementById(EmailAddr) %>.value;
      email = email.replace(/&/i, "%26");
      window.open('mailto:' + email);
    }
</script>

<table cellpadding="0" cellspacing="0" border="0">
    <tr>
		<td noWrap>
            <table id="Table4" cellspacing="0" cellpadding="0" border="0">
                <tr height="15">
                    <td class="FieldLabel" nowrap></td>
                </tr>
                <tr class="HideForPicker">
                    <td class="FieldLabel" nowrap></td>
                    <td nowrap><UC:CFM ID="CFM" runat="server" /></td>
                </tr>
                <tr class="ShowForPicker">
                    <td><a id="toggleExpanded" class="toggleExpanded" href="javascript:ToggleExpanded();">show more</a></td>
                </tr>
                <tr>
                    <td class="FieldLabel" style="width: 128px" nowrap>Contact Type</td>
                    <td style="width: 300px" nowrap>
                        <asp:DropDownList ID="AgentRoleT" TabIndex="1" runat="server" onchange="changeInvestorDisplay();" Width="200px"></asp:DropDownList>
                        <asp:TextBox ID="OtherAgentRoleTDesc" TabIndex="1" runat="server" Width="74px"></asp:TextBox></td>
                    <td class="FieldLabel ExpandedOnly" nowrap>Phone</td>
                    <td style="width: 122px" class="ExpandedOnly" nowrap><ml:PhoneTextBox ID="Phone" TabIndex="3" runat="server" Width="120" preset="phone" CssClass="mask" DESIGNTIMEDRAGDROP="464"></ml:PhoneTextBox></td>
                </tr>
                <tr>
                    <td class="FieldLabel" style="width: 128px" nowrap>Contact Name</td>
                    <td style="width: 234px" nowrap><asp:TextBox ID="AgentName" TabIndex="2" runat="server" Width="226px"></asp:TextBox></td>
                    <td class="FieldLabel ExpandedOnly" nowrap>Fax</td>
                    <td style="width: 122px" class="ExpandedOnly" nowrap><ml:PhoneTextBox ID="FaxNum" TabIndex="3" runat="server" Width="120" preset="phone" CssClass="mask"></ml:PhoneTextBox></td>
                </tr>
                <tr>
                    <td class="FieldLabel" style="width: 128px" nowrap>Company Name</td>
                    <td style="width: 234px" nowrap><asp:TextBox ID="CompanyName" TabIndex="2" runat="server" Width="226px"></asp:TextBox></td>
                    <td class="FieldLabel ExpandedOnly" nowrap>Cell phone</td>
                    <td style="width: 122px" class="ExpandedOnly" nowrap><ml:PhoneTextBox ID="CellPhone" TabIndex="3" runat="server" Width="120" preset="phone" CssClass="mask"></ml:PhoneTextBox></td>
                </tr>
                <tr class="ExpandedOnly">
                    <td class="FieldLabel" style="width: 128px" nowrap>Branch Name</td>
                    <td style="width: 234px" nowrap><asp:TextBox ID="BranchName" TabIndex="4" runat="server" Width="226px"></asp:TextBox></td>
                    <td class="FieldLabel" nowrap>Pager</td>
                    <td style="width: 122px" nowrap><ml:PhoneTextBox ID="PagerNum" TabIndex="3" runat="server" Width="120" preset="phone" CssClass="mask"></ml:PhoneTextBox></td>
                </tr>
                <tr class="ExpandedOnly">
                    <td class="FieldLabel" style="width: 128px" nowrap>Department Name</td>
                    <td style="width: 234px" nowrap><asp:TextBox ID="DepartmentName" TabIndex="4" runat="server" Width="226px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="FieldLabel" style="width: 128px" nowrap>Company Address</td>
                    <td style="width: 234px" nowrap><asp:TextBox ID="StreetAddr" TabIndex="5" runat="server" Width="226" MaxLength="60"></asp:TextBox></td>
                    <td class="FieldLabel ExpandedOnly" nowrap>Employee ID</td>
                    <td style="width: 122px" class="ExpandedOnly" nowrap><asp:TextBox ID="EmployeeIDInCompany" TabIndex="3" runat="server" Width="120" MaxLength="30"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="FieldLabel" style="width: 128px" nowrap></td>
                    <td style="width: 234px" nowrap>
                        <asp:TextBox ID="City" TabIndex="5" runat="server" Width="125px"></asp:TextBox><ml:StateDropDownList ID="State" TabIndex="5" runat="server"></ml:StateDropDownList>
                        <ml:ZipcodeTextBox ID="Zip" TabIndex="5" runat="server" Width="50px" preset="zipcode" CssClass="mask" onchange="onCountyDropdownChanged();"></ml:ZipcodeTextBox>
                    </td>
                    <td class="FieldLabel ExpandedOnly" nowrap>Company ID</td>
                    <td style="width: 122px" class="ExpandedOnly" nowrap><asp:TextBox ID="CompanyId" TabIndex="3" runat="server" Width="120" MaxLength="36"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap>Company County</td>
                    <td>
                        <asp:DropDownList runat="server" Width="100" ID="CountyDropdown" onchange="onCountyDropdownChanged();"></asp:DropDownList>
                        <asp:HiddenField runat="server" ID="County" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" style="width: 128px" nowrap>Company Phone</td>
                    <td style="width: 234px" nowrap><ml:PhoneTextBox ID="PhoneOfCompany" TabIndex="5" runat="server" Width="120" preset="phone" CssClass="mask"></ml:PhoneTextBox></td>
                    <td class="FieldLabel ExpandedOnly">Company Fax</td>
                    <td class="ExpandedOnly"><ml:PhoneTextBox ID="FaxOfCompany" TabIndex="5" runat="server" Width="120" preset="phone" CssClass="mask" designtimedragdrop="592"></ml:PhoneTextBox></td>
                </tr>
                <tr>
                    <td class="FieldLabel" style="width: 128px" nowrap>Email</td>
                    <td style="width: 234px" nowrap>
                      <asp:TextBox ID="EmailAddr" TabIndex="5" runat="server"></asp:TextBox>
                      <a onclick="f_sendEmail(); return false;" tabindex="5" href="#">send email</a> <br>
                      <asp:RegularExpressionValidator ID="EmailValidator" runat="server" ControlToValidate="EmailAddr" Display="Dynamic" Font-Bold="True"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr class="ExpandedOnly">
                    <td>&nbsp;</td>
                    <td class="FieldLabel" nowrap><asp:CheckBox ID="IsNotifyWhenLoanStatusChange" runat="server" Text="Send email when loan status changes"></asp:CheckBox></td>
                </tr>
                <tr class="ExpandedOnly" id="OverrideLicenses_Row">
                    <td>&nbsp;</td>
                    <td class="FieldLabel" nowrap><asp:CheckBox ID="OverrideLicenses" onclick="setLicenseUI();" runat="server" Text="Override agent and company licenses"></asp:CheckBox></td>
                </tr>
                <tr class="ExpandedOnly">
                    <td class="FieldLabel" style="width: 128px" nowrap>Agent License #</td>
                    <td style="width: 234px" class="FieldLabel" nowrap>
                        <asp:TextBox ID="LicenseNum" TabIndex="5" runat="server" DESIGNTIMEDRAGDROP="233"></asp:TextBox>
                        <span style="position:relative; top:8px;">&nbsp; for state: &nbsp;<asp:TextBox ID="sSpState" style="text-align:center" TabIndex="5" runat="server" MaxLength="2" Width="35"></asp:TextBox></span>
                    </td>
                    <td class="FieldLabel" nowrap>Case Number</td>
                    <td style="width: 122px" nowrap><asp:TextBox ID="CaseNum" TabIndex="7" runat="server" Width="120px"></asp:TextBox></td>
                </tr>
                <tr class="ExpandedOnly">
                    <td class="FieldLabel" style="width: 128px" nowrap>Company License #</td>
                    <td style="width: 234px" nowrap><asp:TextBox ID="LicenseNumOfCompany" TabIndex="5" runat="server"></asp:TextBox></td>
                    <td class="FieldLabel" nowrap>CHUMS ID</td>
                    <td><asp:TextBox MaxLength="8" ID="ChumsID" TabIndex="5" style="width:120px;" runat="server"></asp:TextBox></td>
                </tr>
                <tr class="ExpandedOnly">
                    <%--OPM 55321: changed the label on these next two fields to their current values, added mask.js compat to the text fields to make it harder to submit non-numbers. --%>
                    <td class="FieldLabel" nowrap>Loan Originator NMLS ID</td>
                    <td><asp:TextBox ID="LoanOriginatorIdentifier" TabIndex="5" runat="server" CssClass="mask" preset="nmlsID" /></td>
                    <td class="FieldLabel" style="width: 213px" valign="top" nowrap colspan="2" rowspan="3">
                        <table class="InsetBorder" id="InvestorTable" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td class="FieldLabel" nowrap>Investor Sold Date</td>
                                <td nowrap><ml:DateTextBox ID="InvestorSoldDate" TabIndex="7" runat="server" Width="75" preset="date" CssClass="mask"></ml:DateTextBox></td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" nowrap>Basis Point</td>
                                <td nowrap><ml:PercentTextBox ID="InvestorBasisPoints" TabIndex="7" runat="server" Width="75px" preset="percent" CssClass="mask"></ml:PercentTextBox></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="ExpandedOnly">
                    <td class="FieldLabel" style="width: 200px;">Loan Origination Company NMLS ID</td>
                    <td><asp:TextBox ID="CompanyLoanOriginatorIdentifier" TabIndex="5" runat="server" CssClass="mask" preset="nmlsID" /></td>
                </tr>
                <tr class="ExpandedOnly">
                    <td class="FieldLabel">Tax ID</td>
                    <td><asp:TextBox ID="TaxID" TabIndex="5" runat="server" /></td>
                </tr>
                <tr class="ExpandedOnly">
                    <td class="padRight">
                        <div style="width: 50%; float: left;" class="FieldLabel">Pay To</div>
                        <div style="width: 50%; float: right;" class="alignRight FieldLabel">Bank Name</div>
                    </td>
                    <td colspan="3" nowrap>
                        <asp:TextBox ID="PayToBankName" class="narrow" runat="server"></asp:TextBox>
                        <span>
                            <label class="FieldLabel verticalAlignHack">Bank City/State</label>
                            <asp:TextBox ID="PayToBankCityState" class="narrow" runat="server"></asp:TextBox>
                        </span>
                        <span>
                            <label class="FieldLabel verticalAlignHack" style="width: 75px;">ABA Number</label>
                            <asp:TextBox ID="PayToABANumber" class="narrow" runat="server"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr class="ExpandedOnly">
                    <td class="FieldLabel alignRight padRight">Account Number</td>
                    <td colspan="3">
                        <asp:TextBox ID="PayToAccountNumber" class="narrow" runat="server"></asp:TextBox>
                        <span>
                            <label class="FieldLabel verticalAlignHack">Account Name</label>
                            <asp:TextBox ID="PayToAccountName" class="narrow" runat="server"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr class="ExpandedOnly">
                    <td class="padRight">
                        <div style="width: 50%; float: left;" class="FieldLabel">Further Credit To</div>
                        <div style="width: 50%; float: right;" class="alignRight FieldLabel">Account Number</div>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="FurtherCreditToAccountNumber" class="narrow" runat="server"></asp:TextBox>
                        <span>
                            <label class="FieldLabel verticalAlignHack">Account Name</label>
                            <asp:TextBox ID="FurtherCreditToAccountName" class="narrow" runat="server"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <asp:PlaceHolder runat="server" ID="CommisionInfo">
                <tr id="CommissionInfoRow" runat="server" class="ExpandedOnly">
                    <td class="FieldLabel" style="width: 128px" nowrap>Commission</td>
                    <td class="FieldLabel" style="width: 450px" nowrap colspan="3">Loan Amt x
                        <ml:PercentTextBox ID="CommissionPointOfLoanAmount" TabIndex="8" runat="server" Width="70" preset="percent" CssClass="mask" onchange="f_calculateCommission();"></ml:PercentTextBox>&nbsp;+ Gross Profit&nbsp;x
                        <ml:PercentTextBox ID="CommissionPointOfGrossProfit" TabIndex="8" runat="server" Width="90px" preset="percent" CssClass="mask" onchange="f_calculateCommission();"></ml:PercentTextBox>&nbsp;+&nbsp;<ml:MoneyTextBox ID="CommissionMinBase" TabIndex="8" runat="server" Width="87px" preset="money" CssClass="mask" onchange="f_calculateCommission();"></ml:MoneyTextBox>
                    </td>
                </tr>
                <tr class="ExpandedOnly">
                    <td class="FieldLabel" style="width: 128px" nowrap>Commission Total =</td>
                    <td style="width: 234px" nowrap><ml:MoneyTextBox ID="Commission" TabIndex="8" runat="server" Width="90" preset="money" CssClass="mask" ReadOnly="True"></ml:MoneyTextBox></td>
                </tr>
                </asp:PlaceHolder>
            </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>        
        <td nowrap>          
            <table class="InsetBorder" id="LenderRelationshipTable" cellspacing="0" cellpadding="0" border="0">
                <tbody id="AffiliateFields">
                <tr><td class="FieldLabel" nowrap="nowrap" colspan="2"><input id="IsLender" tabindex="8" type="checkbox" runat="server">This company is the lender</td></tr>
                <tr><td class="FieldLabel" nowrap="nowrap" colspan="2"><input id="IsOriginator" tabindex="8" type="checkbox" runat="server">This company is the originator</td></tr>
                <tr><td class="FieldLabel" nowrap="nowrap" colspan="2"><input id="IsLenderAffiliate" tabindex="8" type="checkbox" runat="server">This company is an affiliate of the lender</td></tr>
                <tr><td class="FieldLabel" nowrap="nowrap" colspan="2"><input id="IsOriginatorAffiliate" tabindex="8" type="checkbox" runat="server">This company is an affiliate of the originator</td></tr>
                </tbody>
                <tr class="ExpandedOnly"><td class="FieldLabel" nowrap="nowrap" colspan="2"><input id="IsListedInGFEProviderForm" onclick="refreshUI();" tabindex="8" type="checkbox" name="IsListedInGFEProviderForm" runat="server">The service provider has a relationship with Lender</td></tr>
                <tr class="ExpandedOnly">
                    <td "nowrap" style="padding-left:2em">
                        <table id="tblLenderRelOptions" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                              <td nowrap>Item Number
                                <asp:TextBox ID="ProviderItemNumber" TabIndex="9" runat="server" /></td>
                            </tr>
                            <tr>
                              <td nowrap="nowrap"><asp:CheckBox ID="IsLenderAssociation" TabIndex="9" runat="server" Text="The provider is an associate of Lender" /></td>
                            </tr>

                            <tr id="IsLenderAffiliate_Row">
                              <td nowrap="nowrap"><asp:CheckBox ID="IsLenderAffiliate_Relation" TabIndex="9" runat="server" Text="The provider is an affiliate of Lender" /></td>
                            </tr>                    
                            
                            <tr>
                              <td nowrap="nowrap"><asp:CheckBox ID="IsLenderRelative" TabIndex="9" runat="server" Text="The provider is a relative of Lender" /></td>
                            </tr>
                            <tr>
                              <td nowrap="nowrap"><asp:CheckBox ID="HasLenderRelationship" TabIndex="9" runat="server" Text="The provider has an employment, franchise or other business relationship with Lender"></asp:CheckBox></td>
                            </tr>
                            <tr>
                              <td nowrap="nowrap"><asp:CheckBox ID="HasLenderAccountLast12Months" TabIndex="9" runat="server" Text="Within the last 12 months, the provider has maintained an account with Lender or had an outstanding loan or credit arrangement with Lender"></asp:CheckBox></td>
                            </tr>
                            <tr>
                              <td nowrap="nowrap"><asp:CheckBox ID="IsUsedRepeatlyByLenderLast12Months" TabIndex="9" runat="server" Text="Within the last 12 months, Lender has repeatedly used or required borrowers to use the services of this provider"></asp:CheckBox></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="ExpandedOnly">
      <td class="FieldLabel" nowrap>Notes</td>
    </tr>
    <tr class="ExpandedOnly">
        <td nowrap>
            <asp:TextBox ID="Notes" TabIndex="9" runat="server" Width="376px" Height="69px" TextMode="MultiLine"/>
        </td>
    </tr>
    <tr height="70" class="HideForPicker">
        <td nowrap><input type="hidden" id="IsApplyToFormsUponSave" name="IsApplyToFormsUponSave" value="false" /> <input type="button" id="CopyToPrepForms" onclick="updatePrepareByFields()" value="Copy info to 'Prepared By' fields on appropriate forms" style="width: 306px" />&nbsp; <a target="_blank" href="https://support.lendingqb.com/index.php?/Knowledgebase/Article/View/334">Which forms will be updated?</a> </td>
    </tr>
</table>

<input type="hidden" id="EmployeeId" runat="server" />
<input type="hidden" id="AgentSourceT" runat="server" />
<input type="hidden" id="BrokerLevelAgentID" runat="server" />
<input type="hidden" id="ShouldMatchBrokerContact" runat="server" />
