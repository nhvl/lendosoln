﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SettlementServiceProviderList.aspx.cs" Inherits="LendersOfficeApp.newlos.Status.SettlementServiceProviderList" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc" TagName="CFM" Src="~/newlos/Status/ContactFieldMapper.ascx" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Settlement Service Provider List</title>
    <style type="text/css">
        table.NoOuterBorders{
            table-layout:fixed;
            border-collapse:collapse;
            border-spacing:0;
            border-style:hidden;
        }
        table.NoOuterBorders td, table.NoOuterBorders th{
            border:1px solid gray;
        }
        table#ServiceProvidersFromFeesTable
        {
            margin:5px;
        }
        table#ServiceProvidersFromFeesTable td, table#ServiceProvidersFromFeesTable th
        {
            text-align:left;
            vertical-align:top;
            padding: 5px;
        }
        #ServiceProvidersFromFeesTable td
        {
            background-color:white;            
        }
        #ServiceProvidersFromFeesTable th
        {
            background-color:rgb(8,83,147);
            color:white;
        }
        #DataCompleteSettlementServiceProvidersListContainer {
            margin: 5px 0;
        }
        #sHasDataCompleteSettlementServiceProvidersList {
            margin-left: 15px;
        }
        .row
        {
            padding-left: 10px;
            padding-bottom: 4px;
            display: block;
        }
        label
        {
            display: inline-block;
            font-weight: bold;
            width: 125px;
        }
        .align
        {
            width: 365px;
        }
        .wide
        {
            width: 245px;
        }
        .Zip
        {
            width: 50px;
        }
        .addremove
        {
            padding: 5px 0px 5px 10px;
        }
        .cfm
        {
            padding-left: 138px;
            padding-bottom: 3px;
            padding-top: 3px;
        }
        .FormTableSubheader
        {
            padding: 3px 0px 3px 5px;
        }
        .AddFirst
        {
            display: inline-block;
            padding-top: 3px;
            padding-bottom: 3px;
        }
        .Action
        {
            color: Blue;
            cursor: pointer;
            text-decoration: underline;
        }
        .Action.Disabled
        {
            cursor: auto;
        }
        div.BlueFormTableSubheader
        {
            color: white;
            background-color: rgb(8,83,147);
        }
    </style>
    <script id="AvailableSettlementServiceProvidersSectionTemplate" type="text/x-jquery-templ">
        <div class="FormTableSubheader BlueFormTableSubheader">
            {{if IsTitleFee}} Title - {{/if}}
            ${FeeDescription}
        </div>
        <div class="SSPListContainer Misc" data-feeTypeid="${FeeTypeId}" data-feeDescription="${FeeDescription}">
            <span class="Action AddFirst" href="#">add provider</span>
            <input type="hidden" class="FeeTotalAmount" value="${FeeTotalAmount}" />
        </div>
    </script>
    <script id="SSPTemplate" type="text/x-jquery-templ">
        <div class="SSPContainer">
            <div class="cfm">
                <span class="Action PickFromContacts">Pick from Contacts</span>
                <span class="Action AddToContacts">Add to Contacts</span>
            </div>
            <div class="row ServiceTContainer">
                <span class="align">
                    <label>Service</label>
                    <select class="ServiceT wide"></select>
                </span>
                <img src="../../images/require_icon.gif" alt="Required" />
            </div>
            <div class="row">
                <span class="align">
                    <label>Company Name</label>
                    <input type="text" class="CompanyName wide" value="${CompanyName}" />
                    <input type="hidden" class="AgentId" value="${AgentId}" />
                    <input type="hidden" class="Id" value="${Id}" />
                </span>
                <img src="../../images/require_icon.gif" alt="Required" />
            </div>
            <div class="row">
                <span class="align">
                    <label>Company Address</label>
                    <input type="text" class="StreetAddress wide" value="${StreetAddress}" />
                </span>
                <img src="../../images/require_icon.gif" alt="Required" />
            </div>
            <div class="row">
                <span class="align">
                    <label>&nbsp;</label>
                    <input type="text" class="City" value="${City}" />
                    <select class="State"></select>
                    <input type="text" class="Zip" preset="zipcode" value="${Zip}" />
                </span>
                <img src="../../images/require_icon.gif" alt="Required" />
            </div>
            <div class="row">
                <span class="align">
                    <label>Contact Name</label>
                    <input type="text" class="ContactName notrequired wide" value="${ContactName}" />
                </span>
            </div>
            {{if IsAvailableSSP}}
            <div class="row">
                <label>Email</label>
                <input type="text" class="Email notrequired" preset="email" value="${Email}" />
            </div>
            {{/if}}
            <div class="row">
                <label>Phone Number</label>
                <input type="text" class="PhoneNumber" preset="phone" value="${Phone}" />
                <img src="../../images/require_icon.gif" alt="Required" />
            </div>
            {{if IsAvailableSSP}}
            <div class="row">
                <label>Fax Number</label>
                <input type="text" class="Fax notrequired" preset="phone" value="${Fax}" />    
            </div>
            {{/if}}
            {{if IsAvailableSSP}}
            <div class="row">
                <label>Estimated Cost Amount</label>
                <input type="text" class="money EstimatedCostAmount" preset="money" value="${EstimatedCostAmount}" />
                <input type="hidden" class="IsAvailableSSP" value="${IsAvailableSSP}" />
            </div>
            {{/if}}
            <div class="addremove">
                <span class="Action Remove">remove provider</span> 
                <span class="Action Add">add another provider</span>
            </div>
        </div>
    </script>
    <script id="SSPFromFirstFeeRowTemplate" type="text/x-jquery-templ">
        <tr data-agentID="${Contact.ID}">
            {{tmpl "#SSPFromAfterFirstFeeCellTemplate"}}
            <td rowspan="${NumFees}" style="border-top:lightgrey">
                ${Contact.CompanyName}
            </td>
            <td rowspan="${NumFees}">
                ${Contact.Name}<br/>
                ${Contact.StreetAddr}<br/>
                ${Contact.City}, ${Contact.State} ${Contact.Zip}<br/>
                ${Contact.Email}<br/>
                ${Contact.Phone}<br/>
                F: ${Contact.Fax}
            </td>
            <td>
                <a href="#" class="AddProviderToList">Add provider to list</a>
            </td>
        </tr>
    </script>
    <script id="SSPFromAfterFirstFeeRowTemplate" type="text/x-jquery-templ">
        <tr data-agentID="${Contact.ID}">
            {{tmpl "#SSPFromAfterFirstFeeCellTemplate"}}
            <td>
                <a href="#" class="AddProviderToList">Add provider to list</a>
            </td>
        </tr>
    </script>
    <script id="SSPFromAfterFirstFeeCellTemplate" type="text/x-jquery-templ">
        <td class="FeeDescription" data-type="${Fee.FeeTypeId}">
            {{if Fee.IsTitleFee}} Title - {{/if}} ${Fee.Description}
        </td>
    </script>
</head>
<body class="EditBackground">
    <form id="SSPLForm" runat="server">
        <asp:HiddenField ID="TitleProviders" runat="server" />
        <asp:HiddenField ID="EscrowProviders" runat="server" />
        <asp:HiddenField ID="SurveyProviders" runat="server" />
        <asp:HiddenField ID="PestInspectionProviders" runat="server" />
        <asp:HiddenField ID="MiscProviders" runat="server" />
        <asp:HiddenField ID="SettlementServiceProvidersFromFees" runat="server" />
        <asp:HiddenField ID="AvailableSettlementServiceProviders" runat="server" />

        <div class="MainRightHeader">Settlement Service Provider List</div>

        <asp:PlaceHolder runat="server" ID="ServiceProvidersFromFeesSection">
        <div class="FormTableSubheader">
            Providers Currently Assigned to Fees
        </div>
        
        <table id="ServiceProvidersFromFeesTable" class="NoOuterBorders">
            <thead>
                <tr>
                    <th style="min-width:200px">
                        Service
                    </th>
                    <th style="min-width:150px">
                        Provider We Identified
                    </th>
                    <th style="min-width:200px">
                        Contact Information
                    </th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="ServiceProvidersFromFeesTableBody">
                
            </tbody>
        </table>

        <div class="FormTableSubheader">
            <ml:EncodedLiteral runat="server" ID="ServiceProvidersFromFeesSectionHeaderText"></ml:EncodedLiteral>
        </div>

        <div id="DataCompleteSettlementServiceProvidersListContainer" runat="server">
            <input type="checkbox" runat="server" id="sHasDataCompleteSettlementServiceProvidersList" disabled="disabled" /> All section C fees have at least one suggested provider.
        </div>

        <div id="AvailableSettlementServiceProvidersContainer">
        </div>
        </asp:PlaceHolder>

        <asp:PlaceHolder runat="server" ID="CategorizedServiceProviders">
        <div class="FormTableSubheader">Title Insurance</div>
        <div class="SSPListContainer Title">
            <span class="Action AddFirst" href="#">add provider</span>
        </div>
        
        <div class="FormTableSubheader">Escrow / Settlement Company</div>
        <div class="SSPListContainer Escrow">
            <span class="Action AddFirst" href="#">add provider</span>
        </div>
        
        <div class="FormTableSubheader">Survey</div>
        <div class="SSPListContainer Survey">
            <span class="Action AddFirst" href="#">add provider</span>
        </div>
        
        <div class="FormTableSubheader">Pest Inspection</div>
        <div class="SSPListContainer PestInspection">
            <span class="Action AddFirst" href="#">add provider</span>
        </div>
        </asp:PlaceHolder>
        
        <asp:PlaceHolder runat="server" ID="MiscServiceProvidersPlaceHolder">
        <div class="FormTableSubheader"><ml:EncodedLiteral runat="server" ID="MiscText"></ml:EncodedLiteral></div>
        <div class="SSPListContainer Misc">
            <span class="Action AddFirst" href="#">add provider</span>
        </div>
        </asp:PlaceHolder>
    </form>
    <script type="text/javascript">
        $(window).on("load", function() {
            var oldRefreshCalculation = refreshCalculation;
            refreshCalculation = function () {
                var sspMissingRole = false;
                $availableSettlementServiceProvidersContainer.find('.ServiceT').each(function (index, element) {
                    if ($(this).val().length === 0) {
                        sspMissingRole = true;
                    }
                });

                // If an SSP has not had a role selected, do not
                // perform the background calculation as we cannot
                // properly deserialize the collection.
                if (sspMissingRole) {
                    return;
                }

                populateHiddenInputs();
                oldRefreshCalculation();
            }

            /* Existing SSPs stored on loan file */
            var titleProviders = jsonParse($('#TitleProviders').val()) || [];
            var escrowProviders = jsonParse($('#EscrowProviders').val()) || [];
            var surveyProviders = jsonParse($('#SurveyProviders').val()) || [];
            var pestInspectionProviders = jsonParse($('#PestInspectionProviders').val()) || [];
            var miscProviders = jsonParse($('#MiscProviders').val()) || [];
            var availableSettlementServiceProviders = jsonParse($('#AvailableSettlementServiceProviders').val()) || [];
            function jsonParse(json) {
                if (!json) return null;
                try {
                    return JSON.parse(json);
                } catch (e) {
                    return null;
                }
            }

            /* Cached SSP list containers */
            var $titleContainer = $('.SSPListContainer.Title');
            var $escrowContainer = $('.SSPListContainer.Escrow');
            var $surveyContainer = $('.SSPListContainer.Survey');
            var $pestInspectionContainer = $('.SSPListContainer.PestInspection');
            var $miscContainer = $('.SSPListContainer.Misc');

            /* Cached SSPFromFee container */
            var $sspFromFeeContainer = $('#ServiceProvidersFromFeesTableBody');

            var $availableSettlementServiceProvidersContainer = $('#AvailableSettlementServiceProvidersContainer');

            /* Template for each SSP display */
            var $sspTemplate = $('#SSPTemplate').template();

            var $availableSettlementServiceProvidersSectionTemplate = $('#AvailableSettlementServiceProvidersSectionTemplate').template();

            /* SSPs from fees */
            var sspsFromFees = jsonParse($('#SettlementServiceProvidersFromFees').val()) || [];

            /* Templates related to the service provider table. */
            var $sspFromFirstFeeRowTemplate = $('#SSPFromFirstFeeRowTemplate').template();
            var $sspFromAfterFirstFeeRowTemplate = $('#SSPFromAfterFirstFeeRowTemplate').template();

            var allowReadingFromRolodex = <%= AspxTools.JsBool(AllowReadingFromRolodex) %>;
            var allowWritingToRolodex = <%= AspxTools.JsBool(AllowWritingToRolodex) %>;
            
            var serviceTOptions = DDLConfig['ServiceT'];
            var stateOptions = DDLConfig['State'];

            var rolodex = new cRolodex();
            
            /* Perform Initial Load */
            var i;
            for (i = 0; i < titleProviders.length; i++) {
                addSSP($titleContainer, titleProviders[i], true);
            }
            for (i = 0; i < escrowProviders.length; i++) {
                addSSP($escrowContainer, escrowProviders[i], true);
            }
            for (i = 0; i < surveyProviders.length; i++) {
                addSSP($surveyContainer, surveyProviders[i], true);
            }
            for (i = 0; i < pestInspectionProviders.length; i++) {
                addSSP($pestInspectionContainer, pestInspectionProviders[i], true);
            }
            for (i = 0; i < miscProviders.length; i++) {
                addSSP($miscContainer, miscProviders[i], true);
            }

            var populateAvailableSettlementServiceProviders = function() {
                var i, j, serviceProvidersForFee, $feeSectionWithHeaderAndSspListContainer, $providersListContainer;

                for (i = 0; i < availableSettlementServiceProviders.length; i++) {
                    serviceProvidersForFee = availableSettlementServiceProviders[i] || {};

                    $feeSectionWithHeaderAndSspListContainer = $.tmpl(
                        $availableSettlementServiceProvidersSectionTemplate,
                        serviceProvidersForFee);
                    $providersListContainer = $feeSectionWithHeaderAndSspListContainer.filter('.SSPListContainer');

                    $providersListContainer.data('feeTypeId', serviceProvidersForFee.FeeTypeId);
                    $providersListContainer.data('feeDescription', serviceProvidersForFee.FeeDescription);

                    for (j = 0; j < serviceProvidersForFee.SettlementServiceProviders.length; j++) {
                        addSSP($providersListContainer,
                            serviceProvidersForFee.SettlementServiceProviders[j],
                            true);
                    }

                    $availableSettlementServiceProvidersContainer.append($feeSectionWithHeaderAndSspListContainer);
                }
            };

            populateAvailableSettlementServiceProviders();

            for (i = 0; i < sspsFromFees.length; i++)
            {
                var sspFromFee = sspsFromFees[i];
                var fees = sspFromFee.Fees;
                if(fees.length > 0)
                {
                    addSSPFromFee($sspFromFirstFeeRowTemplate, {NumFees: sspFromFee.Fees.length, Fee: sspFromFee.Fees[0], Contact: sspFromFee.Contact});
                }
                for (var j = 1; j < fees.length; j++)
                {
                    addSSPFromFee($sspFromAfterFirstFeeRowTemplate, {Fee: sspFromFee.Fees[j], Contact: sspFromFee.Contact});
                }
            }
            
            if (isReadOnly()) {
                var $action = $('.Action');
                setDisabledAttr($action, true);
                $action.addClass('Disabled');

                if (typeof formatReadonlyField === 'function') {
                    $('.SSPContainer input, .SSPContainer select').each(function(index, el) {
                        formatReadonlyField(this);
                    });
                }
                if (typeof disableDDL === 'function') {
                    $('.SSPContainer select').each(function(index, el) {
                        disableDDL(this, true);
                    });
                }
            }
            else {
                /* Bind Events */
                $('.SSPListContainer').on('click', '.AddFirst, .Add', function() {
                    if (hasDisabledAttr(this)) {
                        return;
                    }

                    var $listContainer = $(this).closest('.SSPListContainer');
                    addSSP($listContainer);
                });

                $('.SSPListContainer').on('click', '.Remove', function() {
                    if (hasDisabledAttr(this)) {
                        return;
                    }

                    var $this = $(this);
                    var $listContainer = $this.closest('.SSPListContainer');
                    removeSSP($this, $listContainer);
                });

                $('.SSPListContainer').on('change', '.CompanyName, .ContactName', function () {
                    var $listContainer = $(this).closest('.SSPContainer');
                    $listContainer.find('.AgentId').val(<%=AspxTools.JsString(Guid.Empty) %>);
                });

                if (ML.sIsInTRID2015Mode) {
                    $('.SSPListContainer').on('change', '.ServiceT, .CompanyName, .StreetAddress, .City, .State, .Zip, .PhoneNumber', refreshCalculation);
                }

                $('#ServiceProvidersFromFeesTable').on('click', '.AddProviderToList', function () {
                    var $this, feeTypeId, contact, sspConfig, $providerListContainer;

                    $this = $(this);
                    feeTypeId = $this.closest('tr').find('td.FeeDescription').data('type');

                    contact = getContactForFeeByFeeTypeId(feeTypeId);

                    $providerListContainer = getProviderListContainerByFeeTypeId(feeTypeId);

                    var feeTotal = $providerListContainer.find('input.FeeTotalAmount').val();
                    var isAvailableSSP = true;
                    if(feeTotal == null)
                    {
                        feeTotal = 0;
                        isAvailableSSP = false;
                    }

                    sspConfig = createProviderFromContact(contact, feeTotal, isAvailableSSP);

                    addSSP($providerListContainer, sspConfig);

                    function getContactForFeeByFeeTypeId(feeTypeId) {
                        var i, j, feesForContact, contact, fee;

                        for (i = 0; i < sspsFromFees.length && !contact; i++) {
                            feesForContact = sspsFromFees[i];

                            for (j = 0; j < feesForContact.Fees.length && !contact; j++) {
                                fee = feesForContact.Fees[j];

                                if (fee.FeeTypeId === feeTypeId) {
                                    contact = feesForContact.Contact;
                                }
                            }
                        }

                        return contact;
                    }

                    function createProviderFromContact(contact, feeTotal, isAvailableSSP) {
                        return {
                            'ServiceT': contact.AgentRoleT,
                            'CompanyName': contact.CompanyName,
                            'StreetAddress': contact.StreetAddr,
                            'City': contact.City,
                            'State': contact.State,
                            'Zip': contact.Zip,
                            'ContactName': contact.Name,
                            'Phone': contact.Phone,
                            'AgentId': contact.ID,
                            'EstimatedCostAmount': feeTotal,
                            'IsAvailableSSP': isAvailableSSP,
                            'Email': contact.Email,
                            'Fax': contact.Fax,
                            'Id': '00000000-0000-0000-0000-000000000000'
                        };
                    }

                    function getProviderListContainerByFeeTypeId(feeTypeId) {
                        var $providerListContainer;

                        $availableSettlementServiceProvidersContainer
                            .find('div.SSPListContainer')
                            .filter(function() {
                                var $this = $(this);
                                if ($this.data('feeTypeId') === feeTypeId) {
                                    $providerListContainer = $this;
                                    return false;
                                }
                            })

                        return $providerListContainer;
                    }
                });

                if (allowReadingFromRolodex) {
                    $('.SSPListContainer').on('click', '.PickFromContacts', function() {
                        if (hasDisabledAttr(this)) {
                            return;
                        }

                        var $entryContainer = $(this).closest('.SSPContainer');
                        pickFromContacts($entryContainer);
                    });
                }

                if (allowWritingToRolodex) {
                    $('.SSPListContainer').on('click', '.AddToContacts', function() {
                        if (hasDisabledAttr(this)) {
                            return;
                        }

                        var $entryContainer = $(this).closest('.SSPContainer');
                        addToContacts($entryContainer);
                    });
                }
                
                if (typeof smartZipcode === 'function') {
                    $('.SSPListContainer').on('blur', '[preset=zipcode]', function(event) {
                        var $entryContainer = $(this).closest('.SSPContainer');
                        var city = $entryContainer.find('.City')[0];
                        var state = $entryContainer.find('.State')[0];
                        smartZipcode(this, city, state, null, event);
                    });
                }
            }
            
            /* Functions */
            function addSSP($listContainer, sspConfig, bInitLoad) {
                var feeTotalAmount = $listContainer.find('input.FeeTotalAmount').val();

                if (sspConfig == null && feeTotalAmount != null)
                {
                    sspConfig = {
                        'EstimatedCostAmount': feeTotalAmount,
                        'IsAvailableSSP': true
                    };
                }

                var $newSSP = $.tmpl($sspTemplate, sspConfig || {});

                $('.AddFirst, .Add', $listContainer).hide();
                $listContainer.append($newSSP);

                var $serviceT = $newSSP.find('.ServiceT');
                bindDDL($serviceT, serviceTOptions);
                setServiceT($serviceT, $listContainer, sspConfig);
                if (!$listContainer.hasClass('Misc')) {
                    $serviceT.parents('.ServiceTContainer')
                        .hide();
                }
                
                var $state = $newSSP.find('.State');
                bindDDL($state, stateOptions);
                if (sspConfig !== undefined) {
                    $state.val(sspConfig.State);
                }

                var $agentId = $newSSP.find('.AgentId');
                if (sspConfig === undefined || !sspConfig.AgentId) {
                    $agentId.val(<%=AspxTools.JsString(Guid.Empty) %>);
                }

                var $id = $newSSP.find('.Id');
                if(sspConfig === undefined || !sspConfig.Id) {
                    $id.val(<%=AspxTools.JsString(Guid.Empty)%>);
                }
                
                if (typeof _initDynamicInput === 'function') {
                    $newSSP.find(':input').each(function(index, el) {
                        _initDynamicInput(this);
                    });
                }
                
                if (!allowReadingFromRolodex) {
                    var $pickFromContacts = $('.PickFromContacts', $newSSP);
                    setDisabledAttr($pickFromContacts, true);
                    $pickFromContacts.addClass('Disabled');
                }

                if (!allowWritingToRolodex) {
                    var $addToContacts = $('.AddToContacts', $newSSP);
                    setDisabledAttr($addToContacts, true);
                    $addToContacts.addClass('Disabled');
                }
                
                if (bInitLoad !== true) {
                    updateDirtyBit();
                }
            }

            function addSSPFromFee($template, data)
            {
                var $newSSPFromFee = $.tmpl($template, data || {});
                $sspFromFeeContainer.append($newSSPFromFee);
            }

            function removeSSP($ssp, $listContainer) {
                $ssp.closest('.SSPContainer').remove();
                if ($listContainer.children('.SSPContainer').length === 0) {
                    $('.AddFirst', $listContainer).show();
                }
                else {
                    $('.Add', $listContainer).filter(':last').show();
                }

                updateDirtyBit();

                if (ML.sIsInTRID2015Mode) {
                    refreshCalculation();
                }
            }

            function pickFromContacts($entryContainer) {
                var $serviceT = $entryContainer.find('.ServiceT');
                var type = $serviceT.val();
                rolodex.chooseFromRolodex(type, ML.sLId, false, false, function(args){
                    if (args.OK) {
                        $entryContainer.find('.CompanyName').val(args['AgentCompanyName']);
                        $entryContainer.find('.StreetAddress').val(args['CompanyStreetAddr']);
                        $entryContainer.find('.City').val(args['CompanyCity']);
                        $entryContainer.find('.State').val(args['CompanyState']);
                        $entryContainer.find('.Zip').val(args['CompanyZip']);
                        $entryContainer.find('.ContactName').val(args['AgentName']);
                        $entryContainer.find('.PhoneNumber').val(args['AgentPhone']);
                        $entryContainer.find('.Email').val(args['AgentEmail']);
                        $entryContainer.find('.Fax').val(args['AgentFax']);

                        if (args['RecordId']){
                            $entryContainer.find('.AgentId').val(args['RecordId']);
                        } else {
                            $entryContainer.find('.AgentId').val(<%=AspxTools.JsString(Guid.Empty) %>);
                        }
                        
                        if ($entryContainer.closest('.SSPListContainer').hasClass('Misc')) {
                            $serviceT.val(args['AgentType']);
                        }
                        
                        updateDirtyBit();

                        if (ML.sIsInTRID2015Mode) {
                            refreshCalculation();
                        }
                    }
                });
            }

            function addToContacts($entryContainer) {
                var type = $entryContainer.find('.ServiceT').val();
                var args = {
                    'AgentType': type,
                    'AgentCompanyName': $entryContainer.find('.CompanyName').val(),
                    'CompanyStreetAddr': $entryContainer.find('.StreetAddress').val(),
                    'CompanyCity': $entryContainer.find('.City').val(),
                    'CompanyState': $entryContainer.find('.State').val(),
                    'CompanyZip': $entryContainer.find('.Zip').val(),
                    'AgentName': $entryContainer.find('.ContactName').val(),
                    'AgentPhone': $entryContainer.find('.PhoneNumber').val(),
                    'AgentEmail': $entryContainer.find('.Email').val(),
                    'AgentFax': $entryContainer.find('.Fax').val()
                };

                rolodex.addToRolodex(args);
                return false;
            }

            function bindDDL($ddl, options) {
                var i;
                for (i = 0; i < options.length; i++) {
                    var item = options[i];
                    var $option = $('<option>').val(item.Key).text(item.Value);
                    $ddl.append($option);
                }
            }

            function setServiceT($serviceT, $listContainer, sspConfig) {
                var agentT = null;
                if ($listContainer.hasClass('Title')) {
                    agentT = <%= AspxTools.JsString(E_AgentRoleT.Title) %>;
                } 
                else if ($listContainer.hasClass('Escrow')) {
                    agentT = <%= AspxTools.JsString(E_AgentRoleT.Escrow) %>;
                } 
                else if ($listContainer.hasClass('Survey')) {
                    agentT = <%= AspxTools.JsString(E_AgentRoleT.Surveyor) %>;
                } 
                else if ($listContainer.hasClass('PestInspection')) {
                    agentT = <%= AspxTools.JsString(E_AgentRoleT.PestInspection) %>;
                } 
                else if ($listContainer.hasClass('Misc') && sspConfig !== undefined) {
                    agentT = sspConfig.ServiceT;
                }
                
                if (agentT != null) {
                    $serviceT.val(agentT);
                }
            }
            
            function extractSSPEntries($listContainer) {
                var entries = [];
                $listContainer.find('.SSPContainer')
                    .each(function(index, el) {
                        var $this = $(this);
                        var esp = $this.find('.EstimatedCostAmount').val();
                        if(esp == null)
                        {
                            esp = 0;
                        }
                        
                        var entry = {
                            'ServiceT': $this.find('.ServiceT').val(),
                            'CompanyName': $this.find('.CompanyName').val(),
                            'StreetAddress': $this.find('.StreetAddress').val(),
                            'City': $this.find('.City').val(),
                            'State': $this.find('.State').val(),
                            'Zip': $this.find('.Zip').val(),
                            'ContactName': $this.find('.ContactName').val(),
                            'Phone': $this.find('.PhoneNumber').val(),
                            'AgentId': $this.find('.AgentId').val(),
                            'EstimatedCostAmount': esp,
                            'Email': $this.find('.Email').val(),
                            'Fax': $this.find('.Fax').val(),
                            'Id': $this.find('.Id').val()
                        };
                        entries.push(entry);
                    });
                    
                return entries;
            }
            
            /* Page is valid if every input has a valid value. OPM 149136 - Contact Name no longer required. */
            function pageIsValid() {
                var isValid = true;
                $('#SSPLForm input[type="text"], #SSPLForm select').each(function (index, element) {
                    var $this = $(this);
                    if ($this.val().length === 0 && !$this.hasClass('notrequired')) {
                        isValid = false;
                    }
                });
                return isValid;
            }

            function extractAvailableSettlementServiceProviders() {
                var $sspListContainers, availableSettlementServiceProvidersForFee;

                $sspListContainers = $availableSettlementServiceProvidersContainer
                    .find('.SSPListContainer');

                availableSettlementServiceProvidersForFee = [];

                $sspListContainers.each(function() {
                    var $this = $(this);

                    availableSettlementServiceProvidersForFee.push({
                        FeeTypeId: $this.data('feeTypeId'),
                        FeeDescription: $this.data('feeDescription'),
                        SettlementServiceProviders: extractSSPEntries($this)
                    });
                });

                return availableSettlementServiceProvidersForFee;
            }
            
            var oldSaveMe = saveMe;
            saveMe = function (bRefresh) {
                if (!pageIsValid()) {
                    alert('Please fill out all of the required fields for each provider.');
                    return;
                }
                
                populateHiddenInputs();

                return oldSaveMe(bRefresh);
            }

            function populateHiddenInputs () {
                titleProviders = extractSSPEntries($titleContainer);
                escrowProviders = extractSSPEntries($escrowContainer);
                surveyProviders = extractSSPEntries($surveyContainer);
                pestInspectionProviders = extractSSPEntries($pestInspectionContainer);
                miscProviders = extractSSPEntries($miscContainer);
                availableSettlementServiceProviders = extractAvailableSettlementServiceProviders();


                $('#TitleProviders').val(JSON.stringify(titleProviders));
                $('#EscrowProviders').val(JSON.stringify(escrowProviders));
                $('#SurveyProviders').val(JSON.stringify(surveyProviders));
                $('#PestInspectionProviders').val(JSON.stringify(pestInspectionProviders));
                $('#MiscProviders').val(JSON.stringify(miscProviders));
                $('#AvailableSettlementServiceProviders').val(JSON.stringify(availableSettlementServiceProviders));
            }
        });
    </script>
</body>
</html>
