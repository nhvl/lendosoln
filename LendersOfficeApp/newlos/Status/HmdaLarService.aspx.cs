﻿#region Generated Code
namespace LendersOfficeApp.newlos.Status
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Provides an implementation for data loading and binding for
    /// the HMDA LAR page.
    /// </summary>
    public partial class HmdaLarServiceItem : AbstractBackgroundServiceItem
    {
        /// <summary>
        /// Constructs a loan for the service.
        /// </summary>
        /// <param name="loanId">
        /// The id for the loan.
        /// </param>
        /// <returns>
        /// The loan the for the service.
        /// </returns>
        protected override CPageData ConstructPageDataClass(Guid loanId)
        {
            return CPageData.CreateUsingSmartDependency(loanId, typeof(HmdaLarServiceItem));
        }

        /// <summary>
        /// Binds data to the specified <see cref="dataLoan"/> and <see cref="dataApp"/>.
        /// </summary>
        /// <param name="dataLoan">
        /// The loan to bind data.
        /// </param>
        /// <param name="dataApp">
        /// The app to bind data.
        /// </param>
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sUniversalLoanIdentifier = this.GetString(nameof(dataLoan.sUniversalLoanIdentifier));
            dataLoan.sUniversalLoanIdentifierLckd = this.GetBool(nameof(dataLoan.sUniversalLoanIdentifierLckd));
            dataLoan.sHmdaApplicationDate_rep = this.GetString("sHmdaApplicationDate");
            dataLoan.sHmdaApplicationDateLckd = this.GetBool(nameof(dataLoan.sHmdaApplicationDateLckd));
            dataLoan.sHmdaHoepaStatusT = this.GetEnum<sHmdaHoepaStatusT>(nameof(dataLoan.sHmdaHoepaStatusT));
            dataLoan.sHmdaOtherNonAmortFeatureT = this.GetEnum<sHmdaOtherNonAmortFeatureT>(nameof(dataLoan.sHmdaOtherNonAmortFeatureT));
            dataLoan.sHmdaManufacturedTypeT = this.GetEnum<sHmdaManufacturedTypeT>(nameof(dataLoan.sHmdaManufacturedTypeT));
            dataLoan.sHmdaManufacturedInterestT = this.GetEnum<sHmdaManufacturedInterestT>(nameof(dataLoan.sHmdaManufacturedInterestT));
            dataLoan.sHmdaMultifamilyUnits_rep = this.GetString("sHmdaMultifamilyUnits");
            dataLoan.sHmdaReverseMortgageT = this.GetEnum<sHmdaReverseMortgageT>(nameof(dataLoan.sHmdaReverseMortgageT));
            dataLoan.sHmdaBusinessPurposeT = this.GetEnum<sHmdaBusinessPurposeT>(nameof(dataLoan.sHmdaBusinessPurposeT));
            dataLoan.sHmdaLoanPurposeTLckd = this.GetBool("sHmdaLoanPurposeTLckd");
            dataLoan.sHmdaLoanPurposeT = this.GetEnum<sHmdaLoanPurposeT>("sHmdaLoanPurposeT");
            dataLoan.sHmdaTotalUnitsLckd = this.GetBool(nameof(dataLoan.sHmdaTotalUnitsLckd));
            dataLoan.sHmdaTotalUnits_rep = this.GetString("sHmdaTotalUnits");
            dataLoan.sHmdaLoanAmount_rep = this.GetString("sHmdaLoanAmount");
            dataLoan.sHmdaLoanAmountLckd = this.GetBool("sHmdaLoanAmountLckd");
        }

        /// <summary>
        /// Loads data from the specified <see cref="dataLoan"/> and <see cref="dataApp"/>.
        /// </summary>
        /// <param name="dataLoan">
        /// The loan to load data.
        /// </param>
        /// <param name="dataApp">
        /// The app to load data.
        /// </param>
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            this.SetResult(nameof(dataLoan.sUniversalLoanIdentifier), dataLoan.sUniversalLoanIdentifier);
            this.SetResult(nameof(dataLoan.sUniversalLoanIdentifierLckd), dataLoan.sUniversalLoanIdentifierLckd);
            this.SetResult("sHmdaApplicationDate", dataLoan.sHmdaApplicationDate_rep);
            this.SetResult(nameof(dataLoan.sHmdaApplicationDateLckd), dataLoan.sHmdaApplicationDateLckd);
            this.SetResult(nameof(dataLoan.sHmdaHoepaStatusT), dataLoan.sHmdaHoepaStatusT);
            this.SetResult(nameof(dataLoan.sHmdaOtherNonAmortFeatureT), dataLoan.sHmdaOtherNonAmortFeatureT);
            this.SetResult(nameof(dataLoan.sHmdaManufacturedTypeT), dataLoan.sHmdaManufacturedTypeT);
            this.SetResult(nameof(dataLoan.sHmdaManufacturedInterestT), dataLoan.sHmdaManufacturedInterestT);
            this.SetResult("sHmdaMultifamilyUnits", dataLoan.sHmdaMultifamilyUnits_rep);
            this.SetResult("sHmdaMultifamilyUnitsNotApplicable", dataLoan.sHmdaPropT != E_sHmdaPropT.MultiFamiliy || LosConvert.IsNotApplicableHmdaString(dataLoan.sHmdaMultifamilyUnits_rep));
            this.SetResult(nameof(dataLoan.sHmdaReverseMortgageT), dataLoan.sHmdaReverseMortgageT);
            this.SetResult(nameof(dataLoan.sHmdaBusinessPurposeT), dataLoan.sHmdaBusinessPurposeT);
            this.SetResult("sHmdaLoanPurposeTLckd", dataLoan.sHmdaLoanPurposeTLckd);
            this.SetResult("sHmdaLoanPurposeT", dataLoan.sHmdaLoanPurposeT);
            this.SetResult(nameof(dataLoan.sHmda2018AprRateSpread), dataLoan.sHmda2018AprRateSpread);
            this.SetResult("sHmdaTotalLoanCosts", dataLoan.sHmdaTotalLoanCosts_rep);
            this.SetResult("sHmdaOriginationCharge", dataLoan.sHmdaOriginationCharge_rep);
            this.SetResult("sHmdaDiscountPoints", dataLoan.sHmdaDiscountPoints_rep);
            this.SetResult("sHmdaLenderCredits", dataLoan.sHmdaLenderCredits_rep);
            this.SetResult("sHmdaPrepaymentTerm", dataLoan.sHmdaPrepaymentTerm_rep);
            this.SetResult("sHmdaTerm", dataLoan.sHmdaTerm_rep);
            this.SetResult("sHmdaDebtRatio", dataLoan.sHmdaDebtRatio_rep);
            this.SetResult("sHmdaTotalUnitsLckd", dataLoan.sHmdaTotalUnitsLckd);
            this.SetResult("sHmdaTotalUnits", dataLoan.sHmdaTotalUnits_rep);
            this.SetResult("sHmdaLoanAmount", dataLoan.sHmdaLoanAmount_rep);
            this.SetResult("sHmdaLoanAmountLckd", dataLoan.sHmdaLoanAmountLckd);
        }
    }

    /// <summary>
    /// Provides data loading and binding for the HMDA LAR page.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed.")]
    public partial class HmdaLarService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Initializes the service.
        /// </summary>
        protected override void Initialize()
        {
            this.AddBackgroundItem(string.Empty, new HmdaLarServiceItem());
        }
    }
}