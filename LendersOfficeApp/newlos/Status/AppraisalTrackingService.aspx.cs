﻿#region Generated Code
namespace LendersOfficeApp.newlos.Status
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using DataAccess;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Appraisal;
    using LendersOffice.ObjLib.Conversions.LQBAppraisal;
    using LendersOffice.ObjLib.Edocs.AutoSaveDocType;
    using LendersOffice.Security;
    using LendersOfficeApp.newlos.Services;

    /// <summary>
    /// Service Item for Tolerance Cure Page.
    /// </summary>
    public partial class AppraisalTrackingServiceItem : AbstractBackgroundServiceItem
    {
        /// <summary>
        /// Triggers a process by name.
        /// </summary>
        /// <param name="methodName">The name of the method to invoke.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "GetNewUniqueGuid":
                    GetNewUniqueGuid();
                    break;
                case "GetExistingOrders":
                    GetExistingOrders();
                    break;
                case "SaveOrderInfo":
                    SaveOrderInfo();
                    break;
                case "AssocateEdocsToManualOrder":
                    this.AssocateEdocsToManualOrder();
                    break;
                case "DeleteEdocsForManualOrder":
                    this.DeleteEdocsForManualOrder();
                    break;
                default:
                    throw new ArgumentException($"Unknown method {methodName}");
            }
        }

        /// <summary>
        /// Constructs CPageData object.
        /// </summary>
        /// <param name="loanId">The Loan Id.</param>
        /// <returns>CPageData object.</returns>
        protected override DataAccess.CPageData ConstructPageDataClass(Guid loanId)
        {
            return CPageData.CreateUsingSmartDependency(loanId, typeof(AppraisalTrackingServiceItem));
        }

        /// <summary>
        /// Associates the edocs to a manual order.
        /// </summary>
        protected void AssocateEdocsToManualOrder()
        {
            Guid appraisalOrderId = this.GetGuid("AppraisalOrderId");
            Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            var manualAppraisalOrder = ManualAppraisalOrder.GetOrderById(appraisalOrderId, brokerId);
            if (manualAppraisalOrder == null)
            {
                this.SetResult("Error", "Unable to find appraisal order.");
                return;
            }

            List<Guid> edocs = SerializationHelper.JsonNetDeserialize<List<Guid>>(this.GetString("EDocsToAssociate", null));

            foreach (var edoc in edocs.CoalesceWithEmpty())
            {
                manualAppraisalOrder.AddEdocRecord(edoc);
            }

            manualAppraisalOrder.Save();
            this.SetResult("manualOrder", SerializationHelper.JsonNetSerialize(manualAppraisalOrder.ToView(PrincipalFactory.CurrentPrincipal)));
        }

        /// <summary>
        /// Deletes the edoc associates for a manual appraisal order.
        /// </summary>
        protected void DeleteEdocsForManualOrder()
        {
            Guid appraisalOrderId = this.GetGuid("AppraisalOrderId");
            Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            var manualAppraisalOrder = ManualAppraisalOrder.GetOrderById(appraisalOrderId, brokerId);
            if (manualAppraisalOrder == null)
            {
                this.SetResult("Error", "Unable to find appraisal order.");
                return;
            }

            manualAppraisalOrder.RemoveEdocAssocations();
            this.SetResult("manualOrder", SerializationHelper.JsonNetSerialize(manualAppraisalOrder.ToView(PrincipalFactory.CurrentPrincipal)));
        }

        /// <summary>
        /// Creates a new GUID and places it on the page.
        /// </summary>
        protected void GetNewUniqueGuid()
        {
            string newGuid = Guid.NewGuid().ToString();
            SetResult("newGuid", newGuid);
        }

        /// <summary>
        /// Gets existing appraisal orders for loan.
        /// </summary>
        protected void GetExistingOrders()
        {
            Guid loanId = GetGuid("LoanId");
            Guid brokerId = GetGuid("BrokerId");

            var loader = new AppraisalOrderViewLoader(loanId, brokerId, PrincipalFactory.CurrentPrincipal);
            var orderViews = loader.Retrieve();

            orderViews.Sort((lhs, rhs) => lhs.OrderedDate.CompareToWithTime(rhs.OrderedDate, false));
            this.SetResult("orders", SerializationHelper.JsonNetSerialize(orderViews));
        }

        /// <summary>
        /// Saves order info to DB.
        /// </summary>
        protected void SaveOrderInfo()
        {
            string ordersJson = GetString("Orders");
            List<AppraisalOrderView> orders = SerializationHelper.JsonNetDeserialize<List<AppraisalOrderView>>(ordersJson);
            if (orders.Count == 0)
            {
                return;
            }

            var result = AppraisalOrderView.Save(orders, PrincipalFactory.CurrentPrincipal);
            if (result.HasError)
            {
                throw result.Error;
            }

            Dictionary<int, KeyValuePair<Guid, string>> indexToIds = orders.ToDictionary((order) => order.Index, (order) => new KeyValuePair<Guid, string>(order.AppraisalOrderId, order.LoadingId));
            SetResult("indexToIds", SerializationHelper.JsonNetSerialize(indexToIds));
        }

        /// <summary>
        /// Load data from loan to page.
        /// </summary>
        /// <param name="dataLoan">The Loan data.</param>
        /// <param name="dataApp">The App data.</param>
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            this.SetResult("sIntentToProceedD", dataLoan.sIntentToProceedD_rep);
            this.SetResult("sApprWaiverReceivedD", dataLoan.sApprWaiverReceivedD_rep);
            this.SetResult("sApprECopyConsentReceivedD", dataLoan.sApprECopyConsentReceivedD_rep);
            this.SetResult("sApprDeliveryDueDLckd", dataLoan.sApprDeliveryDueDLckd);
            this.SetResult("sApprDeliveryDueD", dataLoan.sApprDeliveryDueD_rep);
            this.SetResult("sApprRprtExpD", dataLoan.sApprRprtExpD_rep);
        }

        /// <summary>
        /// Bind data from page to loan.
        /// </summary>
        /// <param name="dataLoan">The Loan data.</param>
        /// <param name="dataApp">The App data.</param>
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sIntentToProceedD_rep = this.GetString("sIntentToProceedD");
            dataLoan.sApprWaiverReceivedD_rep = this.GetString("sApprWaiverReceivedD");
            dataLoan.sApprECopyConsentReceivedD_rep = this.GetString("sApprECopyConsentReceivedD");
            dataLoan.sApprDeliveryDueDLckd = this.GetBool("sApprDeliveryDueDLckd");
            dataLoan.sApprDeliveryDueD_rep = this.GetString("sApprDeliveryDueD");
            dataLoan.sApprRprtExpD_rep = this.GetString("sApprRprtExpD");
        }
    }

    /// <summary>
    /// Tolerance Cure Service Page.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed.")]
    public partial class AppraisalTrackingService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Initialize function for AppraisalTrackingService.
        /// </summary>
        protected override void Initialize()
        {
            this.AddBackgroundItem(string.Empty, new AppraisalTrackingServiceItem());
        }
    }
}