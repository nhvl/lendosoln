﻿#region Generated Code
namespace LendersOfficeApp.newlos.Status
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.ObjLib.Conversions.LQBAppraisal;
    using LendersOffice.ObjLib.UI.DataContainers;

    /// <summary>
    /// Appraisal Tracking Page.
    /// </summary>
    public partial class AppraisalTracking : BaseLoanPage
    {
        /// <summary>
        /// OnInit function.
        /// </summary>
        /// <param name="e">System event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.PageTitle = "Appraisal Tracking";
            this.PageID = "AppraisalTracking";

            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);

            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("LQBPopup.js");
            RegisterJsScript("widgets/jquery.docmetadatatooltip.js");

            Tools.Bind_CuRiskT(this.OvervaluationRiskT);
            Tools.Bind_CuRiskT(this.PropertyEligibilityRiskT);
            Tools.Bind_CuRiskT(this.AppraisalQualityRiskT);

            base.OnInit(e);
        }

        /// <summary>
        /// Page_Loan function.
        /// </summary>
        /// <param name="sender">Control that calls Page_Load.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack && Request["__EVENTTARGET"] == "ViewDoc")
            {
                ViewDoc(Request["__EVENTARGUMENT"]);
                return;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(AppraisalTracking));
            dataLoan.InitLoad();

            this.PopulatePropertyInfo(dataLoan);

            sIntentToProceedD.ReadOnly = dataLoan.sUseInitialLeSignedDateAsIntentToProceedDate;
            sIntentToProceedD.Text = dataLoan.sIntentToProceedD_rep;
            sApprWaiverReceivedD.Text = dataLoan.sApprWaiverReceivedD_rep;
            sApprECopyConsentReceivedD.Text = dataLoan.sApprECopyConsentReceivedD_rep;
            sApprDeliveryDueDLckd.Checked = dataLoan.sApprDeliveryDueDLckd;
            sApprDeliveryDueD.Text = dataLoan.sApprDeliveryDueD_rep;
            sApprRprtExpD.Text = dataLoan.sApprRprtExpD_rep;

            Tools.Bind_sSpAppraisalFormTShort(AppraisalFormType);

            this.RegisterJsGlobalVariables("BrokerId", this.BrokerID);
            this.RegisterJsGlobalVariables("CanViewEDocs", this.BrokerUser.HasPermission(LendersOffice.Security.Permission.CanViewEDocs));
            var appraisalDeliveryEnabledForVendors = this.Broker.AllActiveDocumentVendors.Any(vendor => vendor.EnableAppraisalDelivery);
            this.RegisterJsGlobalVariables("CanDeliverAppraisals", this.BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowAppraisalDelivery) && appraisalDeliveryEnabledForVendors);
        }

        /// <summary>
        /// Populate property selector picker.
        /// </summary>
        /// <param name="dataLoan">The data loan.</param>
        private void PopulatePropertyInfo(CPageData dataLoan)
        {
            var propertyInfoDetails = Tools.PopulatePropertyInfoDetails(dataLoan, this.PropertySelector, includeSubjPropInDDL: false);
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("PropertyInfoDetails", propertyInfoDetails);
        }

        /// <summary>
        /// Gets a non-EDocs appraisal order document from FileDB.
        /// </summary>
        /// <param name="fileDbKey">A FileDB key.</param>
        private void ViewDoc(string fileDbKey)
        {
            // Although we could just grab the doc from the filedb key, 
            // we need to make sure the requested filedb key actually belongs to a document on this loan.
            var orders = LQBAppraisalOrder.GetOrdersForLoan(LoanID);

            LQBAppraisalOrder.AppraisalDocument doc = default(LQBAppraisalOrder.AppraisalDocument);
            foreach (var order in orders)
            {
                doc = order.UploadedFiles.FirstOrDefault(
                    a => a.FileDBKey.Equals(fileDbKey, StringComparison.OrdinalIgnoreCase));

                if (doc != default(LQBAppraisalOrder.AppraisalDocument))
                {
                    break;
                }
            }

            if (doc == default(LQBAppraisalOrder.AppraisalDocument))
            {
                throw new CBaseException(ErrorMessages.Generic, $"FileDbKey did not belong to any appraisal order document on the loan. FileDbKey=[{fileDbKey}]. LoanId=[{this.LoanID}]");
            }

            string path = TempFileUtils.NewTempFilePath();
            BinaryFileHelper.WriteAllBytes(path, FileDBTools.ReadData(E_FileDB.Normal, doc.FileDBKey));
            RequestHelper.SendFileToClient(this.Context, path, "application/pdf", doc.DocumentName);
            Response.End();
        }
    }
}