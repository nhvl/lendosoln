﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataAccess;

namespace LendersOfficeApp.newlos.Status
{
    public partial class OfficialContactList : BaseLoanUserControl
    {
        protected string m_sortExpression;

        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(OfficialContactList));
            dataLoan.InitLoad();
            BindDataGrid(dataLoan.sAgentDataSet);
            IsPageReadOnly.Value = IsReadOnly.ToString().ToLower();
        }
        protected string sourcePage
        {
            get
            {
                string source = "";
                if (this.Page is LendersOfficeApp.los.Lead.LeadAssignment)
                    source = "leadAssignment";
                return source;
            }
        }
        protected string displayYesNo(object b)
        {
            string ret = "No";
            if (null != b)
            {
                if (b.ToString() == "True") ret = "Yes";
            }
            return ret;
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            //RegisterJsScript("utilities.js");
            //this.RegisterService("loanedit", "/newlos/Status/AgentsService.aspx");

        }

        private void BindDataGrid(DataSet ds)
        {
            m_agentsDG.DataSource = ds.Tables[0].DefaultView;
            m_agentsDG.DataBind();
            m_sortExpression = ((DataView)m_agentsDG.DataSource).Sort;
        }
    }
}