﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Status
{
    public class LendingStaffNotesServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(LendingStaffNotesServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            if (!BrokerUser.HasPermission(Permission.CanAccessLendingStaffNotes))
            {
                throw new AccessDenied(ErrorMessages.LendingStaffNotesAccessDenied);
            }

            dataLoan.sLendingStaffNotes = GetString("sLendingStaffNotes");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }

    public partial class LendingStaffNotesService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new LendingStaffNotesServiceItem());
        }
        //private BrokerUserPrincipal BrokerUser
        //{
        //    get { return BrokerUserPrincipal.CurrentPrincipal; }
        //}

        //protected void Page_Load(object sender, EventArgs e)
        //{

        //    if (!BrokerUser.HasPermission(Permission.CanAccessLendingStaffNotes))
        //    {
        //        throw new AccessDenied(ErrorMessages.LendingStaffNotesAccessDenied);
        //    }

        //}

        //protected override void Process(string methodName)
        //{
        //    switch (methodName)
        //    {
        //        case "SaveData":
        //            SaveData();
        //            break;

        //    }
        //}

        //private void SaveData()
        //{
        //    Guid loanID = GetGuid("LoanID");
        //    CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanID, typeof(LendingStaffNotesService));
        //    int sFileVersion = GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck);
        //    dataLoan.InitSave(sFileVersion);
        //    dataLoan.sLendingStaffNotes = GetString("sLendingStaffNotes");
        //    dataLoan.Save();
        //}
    }
}
