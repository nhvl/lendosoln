﻿using System;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Status
{
    public partial class LendingStaffNotes : LendersOfficeApp.newlos.BaseLoanPage
    {       

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageTitle = "Lending Staff Notes";
            this.PageID = "LendingStaffNotes";

            if (!BrokerUser.HasPermission(Permission.CanAccessLendingStaffNotes))
            {
                throw new AccessDenied(ErrorMessages.LendingStaffNotesAccessDenied);
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(LendingStaffNotes));
            dataLoan.InitLoad();
            sLendingStaffNotes.Text = dataLoan.sLendingStaffNotes;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}
