﻿#region Auto Generated Code
namespace LendersOfficeApp.newlos.Status
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocMagicAppraisalDelivery;
    using LendersOffice.Security;

    /// <summary>
    /// DeliverAppraisal service page.
    /// </summary>
    public partial class DeliverAppraisalService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Chooses the method to run.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "CheckDmCredentials":
                    this.CheckDmCredentials();
                    break;
                case "AssignWebsheetNum":
                    this.AssignWebsheetNum();
                    break;
                case "DeliverAppraisal":
                    this.DeliverAppraisal();
                    break;
                default:
                    throw new UnhandledCaseException(methodName);
            }
        }

        /// <summary>
        /// Checks for DM credentials for the selected DM vendor.
        /// </summary>
        private void CheckDmCredentials()
        {
            var vendorId = this.GetGuid("VendorId");
            var loanId = this.GetGuid("LoanId");

            var credentials = DocMagicAppraisalDeliveryRequestProvider.LoadSavedCredentials(PrincipalFactory.CurrentPrincipal, vendorId, loanId);
            this.SetResult("HasCredentials", credentials != null);
        }

        /// <summary>
        /// Sets the websheet number. 
        /// </summary>
        private void AssignWebsheetNum()
        {
            Guid loanId = this.GetGuid("LoanId");
            string websheetNum = this.GetString("sDocMagicFileId");

            var fieldInfo = CFieldDbInfoList.TheOnlyInstance.Get("sDocMagicFileId");

            if (fieldInfo?.m_charMaxLen < websheetNum.Length)
            {
                throw new InvalidOperationException("LendingQB cannot save websheet numbers that long.");
            }

            var dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(DeliverAppraisalService));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sDocMagicFileId = websheetNum;

            dataLoan.Save();
        }

        /// <summary>
        /// Delivers the appraisal.
        /// </summary>
        private void DeliverAppraisal()
        {
            var selectedDocIds = SerializationHelper.JsonNetDeserialize<Guid[]>(this.GetString("SelectedDocIds"));
            var targetVendorId = this.GetGuid("TargetVendorId");
            var websheetNumber = this.GetString("WebsheetNumber");
            var sendConfirmationEmail = this.GetBool("SendConfirmationEmail", false);
            var clickSign = this.GetBool("ClickSign", false);
            var borrowerMobileService = this.GetBool("BorrowerMobileService", false);
            var disableSigningInvitationIndicator = this.GetBool("DisableSigningInvitationIndicator", false);
            var enablePreviewIndicator = this.GetBool("EnablePreviewIndicator", false);
            var appraisalOrderId = this.GetGuid("AppraisalOrderId");
            var loanId = this.GetGuid("LoanId");

            var principal = PrincipalFactory.CurrentPrincipal;

            var customerId = this.GetString("CustomerId", null);
            var username = this.GetString("Username", null);
            var password = this.GetString("Password", null);

            var request = new DocMagicAppraisalDeliveryRequestData(
                loanId: loanId,
                appraisalOrderId: appraisalOrderId,
                vendorId: targetVendorId,
                inputCredentials: new DocMagicCredentials(username, password, customerId),
                ePortalOptions: new EPortalOptions(
                    enableElectronicSignature: clickSign,
                    enableMobileApplication: borrowerMobileService,
                    disableSigningInvitation: disableSigningInvitationIndicator,
                    previewDocumentsPriorToSigning: enablePreviewIndicator,
                    sendConfirmationEmail: sendConfirmationEmail),
                websheetNumber: websheetNumber,
                documents: selectedDocIds);

            var result = DocMagicAppraisalDeliveryRequestProvider.DeliverDocuments(request, principal);

            if (result.Success)
            {
                this.SetResult("Message", "Appraisal successfully delivered.");
            }
            else
            {
                this.SetResult("Errors", SerializationHelper.JsonNetAnonymousSerialize(result.ErrorMessages));
            }
        }
    }
}