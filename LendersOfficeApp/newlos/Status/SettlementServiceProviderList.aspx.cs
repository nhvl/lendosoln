﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Rolodex;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Status
{
    public partial class SettlementServiceProviderList : BaseLoanPage
    {
        protected bool AllowReadingFromRolodex
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowReadingFromRolodex); }
        }

        protected bool AllowWritingToRolodex
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowWritingToRolodex); }
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.EnableJqueryMigrate = false;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }

        private void PageInit(object sender, EventArgs e)
        {
            this.PageTitle = "Settlement Service Provider List";
            this.PageID = "SettlementServiceProviderList";

            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("mask.js");
            this.RegisterJsScript("LQBPopup.js");
            
            var ddls = new Dictionary<string, object>();
            var agentKeyValuePairs = Tools.ExtractDDLEntries((ddl) => RolodexDB.PopulateAgentTypeDropDownList(ddl));
            agentKeyValuePairs.Insert(0, new KeyValuePair<string,string>("", "-- Select a Service Type --"));
            ddls.Add("ServiceT", agentKeyValuePairs);
            ddls.Add("State", Tools.ExtractDDLEntries(Tools.Bind_StatesWithoutMilitaryMailCodes));
            this.RegisterJsStruct("DDLConfig", ddls);
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SettlementServiceProviderList));
            dataLoan.InitLoad();

            ServiceProvidersFromFeesSection.Visible = (false == dataLoan.sIsLegacyClosingCostVersion);
            CategorizedServiceProviders.Visible = dataLoan.sIsLegacyClosingCostVersion;
            if (false == dataLoan.sIsLegacyClosingCostVersion)
            {
                SettlementServiceProvidersFromFees.Value = ObsoleteSerializationHelper
                    .JavascriptJsonSerialize(dataLoan.sAssignedContactsFromFees.Select(feesForContact => new SlimFeesForContact(feesForContact)));

                var modelGenerator = new AvailableSettlementServiceProvidersViewModelGenerator(
                    dataLoan.sAvailableSettlementServiceProviders,
                    dataLoan.sClosingCostSet,
                    dataLoan.m_convertLos);

                var viewModel = modelGenerator.GenerateViewModel();
                this.AvailableSettlementServiceProviders.Value = SerializationHelper.JsonNetSerialize(viewModel);

                MiscServiceProvidersPlaceHolder.Visible = dataLoan.sAllSettlementServiceProviders.Any();
                MiscText.Text = "Add more providers";
                MiscProviders.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(dataLoan.sAllSettlementServiceProviders);
            }
            else
            {
                MiscText.Text = "Miscellaneous";
                MiscProviders.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(dataLoan.sMiscSettlementServiceProviders);
                TitleProviders.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(dataLoan.sTitleSettlementServiceProviders);
                EscrowProviders.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(dataLoan.sEscrowSettlementServiceProviders);
                SurveyProviders.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(dataLoan.sSurveySettlementServiceProviders);
                PestInspectionProviders.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(dataLoan.sPestInspectionSettlementServiceProviders);
            }

            this.RegisterJsGlobalVariables(nameof(dataLoan.sIsInTRID2015Mode), dataLoan.sIsInTRID2015Mode);
            if (dataLoan.sIsInTRID2015Mode)
            {
                ServiceProvidersFromFeesSectionHeaderText.Text = "Available Providers for Section C Fees";
                this.sHasDataCompleteSettlementServiceProvidersList.Checked = dataLoan.sHasDataCompleteSettlementServiceProvidersList;
            }
            else
            {
                ServiceProvidersFromFeesSectionHeaderText.Text = "Available Providers for GFE Fees in Boxes B4/B5/B6";
                this.DataCompleteSettlementServiceProvidersListContainer.Visible = false;
            }
            
            base.LoadData();
        }
    }

    public  class SlimFeesForContact
    {
        public List<SlimFee> Fees { get; private set; }
        public SlimContact Contact { get; private set; }

        public SlimFeesForContact(FeesForContact feesForContact)
        {
            this.Contact = SlimContact.GetFromAgent(feesForContact.Contact);
            this.Fees = new List<SlimFee>(feesForContact.Fees.Count());
            
            foreach (var fee in feesForContact.Fees)
            {
                this.Fees.Add(new SlimFee(fee));
            }
        }
    }

    public class SlimContact
    {
        public Guid ID { get; private set; }

        public E_AgentRoleT AgentRoleT { get; private set; }
        public string City { get; private set; }
        public string CompanyName { get; private set; }
        public string Name { get; private set; }
        public string Phone { get; private set; }
        public string State { get; private set; }
        public string StreetAddr { get; private set; }
        public string Zip { get; private set; }
        public string Email { get; private set; }
        public string Fax { get; private set; }

        private SlimContact()
        {
        }

        public static SlimContact GetFromAgent(CAgentFields agent)
        {
            return new SlimContact()
            {
                ID = agent.RecordId,
                AgentRoleT = agent.AgentRoleT,
                City = agent.City,
                CompanyName = agent.CompanyName,
                Name = agent.AgentName,
                Phone = agent.Phone,
                State = agent.State,
                StreetAddr = agent.StreetAddr,
                Zip = agent.Zip,
                Email = agent.EmailAddr,
                Fax = agent.FaxNum
            };
        }
    }

    public class SlimFee
    {
        public Guid FeeTypeId { get; private set; }
        public string Description { get; private set; }
        public string BeneficiaryType { get; private set; }
        public bool IsTitleFee { get; private set; }

        public SlimFee(BorrowerClosingCostFee fee)
        {
            this.FeeTypeId = fee.ClosingCostFeeTypeId;
            this.Description = fee.Description;
            this.BeneficiaryType = fee.BeneficiaryType;
            this.IsTitleFee = fee.IsTitleFee;
        }
    }

    public class AvailableSettlementServiceProvidersForFeeViewModel
    {
        public string FeeTotalAmount { get; set; }
        public Guid FeeTypeId { get; set; }
        public string FeeDescription { get; set; }
        public bool IsTitleFee { get; set; }
        public IEnumerable<SettlementServiceProviderViewModel> SettlementServiceProviders { get; set; }
    }

    public class SettlementServiceProviderViewModel
    {
        public E_AgentRoleT ServiceT { get; set; }
        public string CompanyName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string ContactName { get; set; }
        public string Phone { get; set; }
        public Guid AgentId { get; set; }
        public string EstimatedCostAmount { get; set; }
        public string ServiceTDesc { get; set; }
        public bool IsAvailableSSP { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public Guid Id { get; set; }
    }

    public class AvailableSettlementServiceProvidersViewModelGenerator
    {
        private AvailableSettlementServiceProviders providers;
        private BorrowerClosingCostSet closingCostSet;
        private LosConvert converter;

        public AvailableSettlementServiceProvidersViewModelGenerator(
            AvailableSettlementServiceProviders providers,
            BorrowerClosingCostSet closingCostSet,
            LosConvert losConverter)
        {
            this.providers = providers;
            this.closingCostSet = closingCostSet;
            this.converter = losConverter;
        }

        public List<AvailableSettlementServiceProvidersForFeeViewModel> GenerateViewModel()
        {
            var viewModels = new List<AvailableSettlementServiceProvidersForFeeViewModel>();

            foreach (var feeTypeId in this.providers.GetFeeTypeIds())
            {
                BorrowerClosingCostFee fee = (BorrowerClosingCostFee)this.closingCostSet.FindFeeByTypeId(feeTypeId);

                var feeSection = new AvailableSettlementServiceProvidersForFeeViewModel
                {
                    FeeTotalAmount = fee.TotalAmount_rep,
                    FeeTypeId = feeTypeId,
                    FeeDescription = fee.Description,
                    IsTitleFee = fee.IsTitleFee,
                    SettlementServiceProviders = this.GenerateServiceProviderViewModel(providers.GetProvidersForFeeTypeId(feeTypeId))
                };

                viewModels.Add(feeSection);
            }

            return viewModels;
        }

        private IEnumerable<SettlementServiceProviderViewModel> GenerateServiceProviderViewModel(IEnumerable<SettlementServiceProvider> providers)
        {
            List<SettlementServiceProviderViewModel> modelList = new List<SettlementServiceProviderViewModel>();

            foreach (var provider in providers)
            {
                SettlementServiceProviderViewModel model = new SettlementServiceProviderViewModel();
                model.AgentId = provider.AgentId;
                model.City = provider.City;
                model.CompanyName = provider.CompanyName;
                model.ContactName = provider.ContactName;
                model.EstimatedCostAmount = provider.GetEstimatedCostAmount_rep(this.converter);
                model.Phone = provider.Phone;
                model.ServiceT = provider.ServiceT;
                model.ServiceTDesc = provider.ServiceTDesc;
                model.State = provider.State;
                model.StreetAddress = provider.StreetAddress;
                model.Zip = provider.Zip;
                model.IsAvailableSSP = true;
                model.Email = provider.Email;
                model.Fax = provider.Fax;
                model.Id = provider.Id;

                modelList.Add(model);
            }

            return modelList;
        }
    }
}
