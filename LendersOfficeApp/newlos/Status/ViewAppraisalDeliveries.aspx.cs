﻿#region Auto Generated Code
namespace LendersOfficeApp.newlos.Status
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using DataAccess;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Views the appraisal deliveries for an appraisal order.
    /// </summary>
    public partial class ViewAppraisalDeliveries : BaseLoanPage
    {
        /// <summary>
        /// The available generic docs.
        /// </summary>
        private Lazy<HashSet<Guid>> genericDocs;

        /// <summary>
        /// The EDoc repository for the user.
        /// </summary>
        private EDocumentRepository userRepo;

        /// <summary>
        /// Initialize function.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.userRepo = EDocumentRepository.GetUserRepository(PrincipalFactory.CurrentPrincipal);
            this.genericDocs = new Lazy<HashSet<Guid>>(() =>
            {
                return new HashSet<Guid>(this.userRepo.GetGenericDocumentsByLoanId(this.LoanID).Select(doc => doc.DocumentId));
            });

            this.UseNewFramework = true;
            this.EnableJquery = true;
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterCSS("jquery-ui-1.11.custom.css");
            this.RegisterJsScript("widgets/jquery.docmetadatatooltip.js");

            base.OnInit(e);
        }

        /// <summary>
        /// Gets the JQuery version.
        /// </summary>
        /// <returns>The jquery version.</returns>
        protected override E_JqueryVersion GetJQueryVersion()
        {
            return E_JqueryVersion._1_11_2;
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        protected override void LoadData()
        {
            var records = LendersOffice.Integration.DocMagicAppraisalDelivery.DocMagicAppraisalDeliveryRecord.RetrieveAllForLoan(this.BrokerID, this.LoanID);

            List<AppraisalDeliveryViewModel> viewModels = new List<AppraisalDeliveryViewModel>(records.Count);
            foreach (var record in records.OrderByDescending(r => r.SentDate))
            {
                viewModels.Add(new AppraisalDeliveryViewModel(record.SentDate, record.SentByName, record.DocumentIds, record.WebsheetNumber));
            }

            this.DeliveriesRepeater.DataSource = viewModels;
            this.DeliveriesRepeater.DataBind();
        }

        /// <summary>
        /// Populates the repeater.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event parameters.</param>
        protected void DeliveriesRepeater_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            HtmlGenericControl sentDate = (HtmlGenericControl)e.Item.FindControl("SentDate");
            HtmlGenericControl sentBy = (HtmlGenericControl)e.Item.FindControl("SentBy");
            HtmlGenericControl websheetNumber = (HtmlGenericControl)e.Item.FindControl("WebsheetNumber");
            var deliveryDocumentsRepeater = (System.Web.UI.WebControls.Repeater)e.Item.FindControl("DeliveryDocumentsRepeater");

            var deliveryView = (AppraisalDeliveryViewModel)e.Item.DataItem;

            sentDate.InnerText = deliveryView.SentDate;
            sentBy.InnerText = deliveryView.SentBy;
            websheetNumber.InnerText = deliveryView.WebsheetNumber;
            deliveryDocumentsRepeater.ItemDataBound += DeliveryDocumentsRepeater_ItemDataBound;
            deliveryDocumentsRepeater.DataSource = deliveryView.EDocIds;
            deliveryDocumentsRepeater.DataBind();
        }

        /// <summary>
        /// Populates the documents for the delivery.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event parameters.</param>
        protected void DeliveryDocumentsRepeater_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            HtmlGenericControl edocDiv = (HtmlGenericControl)e.Item.FindControl("EDocDiv");
            HtmlGenericControl unavailableDiv = (HtmlGenericControl)e.Item.FindControl("UnavailableDiv");
            HtmlInputHidden edocId = (HtmlInputHidden)e.Item.FindControl("EDocId");
            HtmlImage fileInfo = (HtmlImage)e.Item.FindControl("FileInfo");
            HtmlAnchor fileLink = (HtmlAnchor)e.Item.FindControl("FileLink");
            HtmlAnchor xmlLink = (HtmlAnchor)e.Item.FindControl("XmlLink");

            var edocumentId = (Guid)e.Item.DataItem;
            edocId.Value = edocumentId.ToString();
            try
            {
                var edoc = this.userRepo.GetDocumentById(edocumentId);
                Tools.ApplyDocMetaDataTooltip(fileInfo, edoc);

                if (!this.genericDocs.Value.Contains(edocumentId))
                {
                    xmlLink.Visible = false;
                }

                unavailableDiv.Visible = false;
            }
            catch (CBaseException exc) when (exc is AccessDenied || exc is NotFoundException)
            {
                edocDiv.Visible = false;
            }
        }

        /// <summary>
        /// Viewmodel for the appraisal delivery records.
        /// </summary>
        private class AppraisalDeliveryViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="AppraisalDeliveryViewModel" /> class.
            /// </summary>
            /// <param name="sentDate">The date the appraisal was delivered.</param>
            /// <param name="sentBy">The person who delivered the appraisal.</param>
            /// <param name="edocIds">The edoc ids.</param>
            /// <param name="websheetNumber">The websheet number.</param>
            public AppraisalDeliveryViewModel(DateTime sentDate, string sentBy, IEnumerable<Guid> edocIds, string websheetNumber)
            {
                this.SentDate = sentDate.ToString();
                this.SentBy = sentBy;
                this.EDocIds = edocIds;
                this.WebsheetNumber = websheetNumber;
            }

            /// <summary>
            /// Gets or sets the sent date.
            /// </summary>
            public string SentDate { get; set; }

            /// <summary>
            /// Gets or sets the sent by.
            /// </summary>
            public string SentBy { get; set; }

            /// <summary>
            /// Gets or sets the edoc id.
            /// </summary>
            public IEnumerable<Guid> EDocIds { get; set; }

            /// <summary>
            /// Gets or sets the websheet number.
            /// </summary>
            public string WebsheetNumber { get; set; }
        }
    }
}