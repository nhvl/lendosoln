using System;
using System.Collections;
using System.Text;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Rolodex;
using LendersOffice.Security;

// 5/20/2009 dd - I add the ability for user to lock the field based on the Official Contact Information.
//                The new feature should only enable for fill out Preparer Information.
namespace LendersOfficeApp.newlos.Status
{



    /// <summary>
	///		Maps Rolodex fields onto javascript. Provides Pick From Contacts and Save Contact ability.
	///		
	///
	///		Pages can use this control multiple times, but in order for them to work correctly 
	///		the Name attribute has to be provided and be unique for each control. 
	///		Setting the Name attribute will generate javascript specific to a particular control.
	///		
	///		
	///		Attributes : 
	///			Type			- string with an int representing the type of the agent being searched for/added   
	///			
	///							Other 					= 0
	///							Appraiser 				= 1
	///							Escrow 					= 2
	///							CreditReport 			= 3
	///							Title 					= 4
	///							MortgageInsurance		= 5
	///							ListingAgent 			= 6
	///							SellingAgent 			= 7
	///							Builder 				= 8
	///							Underwriter 			= 9
	///							Investor 				= 10
	///							Servicing 				= 11
	///							BuyerAttorney 			= 12
	///							SellerAttorney 			= 13
	///							HomeOwnerInsurance 		= 14
	///							HazardInsurance 		= 15
	///							MarketingLead 			= 16
	///							Seller 					= 17
	///							Surveyor 				= 18
	///							LoanOfficer 			= 19
	///							Bank 					= 20
	///							Lender 					= 21
	///							Processor 				= 22
	///							CallCenterAgent 		= 23
	///							LoanOpener 				= 24
	///							Manager 				= 25
	///							Broker 					= 26
	///							BrokerRep 				= 27
	///							ECOA 					= 28
	///							Realtor 				= 29
	///							Mortgagee 				= 30
	///							HomeOwnerAssociation 	= 31
	///							BuyerAgent 				= 32
	///							ClosingAgent 			= 33
	///							FairHousingLending 		= 34
	///							HomeInspection			= 35
	///							
	///			TypeDDL			- Same as 'Type' only this works dynamically for a dropdownlist
	///			DisablePickFrom - if true, will disable the rendering of the "Pick from contacts" link
	///			DisableAddTo	- if true, will disable the rendering of the "Add to contacts" link
	///			TabIndex		- Same as the HTML tabindex attribute for the links that are generated
	///			Name            - Important when using more than one control on the same page
	///			AgentNameField	
	///			CompanyNameField
	///			DepartmentNameField
	///			StreetAddressField
	///			CityField
	///			StateField
	///			ZipField
	///			PhoneField
	///			FaxField
	///			AgentTitleField
	///			EmailField
	///			AgentLicenseField
	///			CompanyLicenseField
	///			PagerNumField
	///			CellPhoneField
	///			CompanyPhoneField
	///			CompanyFaxField
	///			StatusLoanChangeField
	///			ComPointLoanAmtField
	///			ComPointGrossField
	///			ComMinBaseField
	///			NotesField
	///		
	/// </summary>
	public partial  class ContactFieldMapper : System.Web.UI.UserControl
	{
        public class FieldIdHolder
        {
            public string AgentName { get; set; }
            public string AgentCompanyName { get; set; }
            public string AgentDepartmentName { get; set; }
            public string AgentStreetAddr { get; set; }
            public string AgentCity { get; set; }
            public string AgentState { get; set; }
            public string AgentZip { get; set; }
            public string AgentCounty { get; set; }
            public string AgentPhone { get; set; }
            public string AgentFax { get; set; }
            public string AgentTitle { get; set; }
            public string AgentEmail { get; set; }
            public string AgentLicenseNumber { get; set; }
            public string CompanyLicenseNumber { get; set; }
            public string AgentPager { get; set; }
            public string AgentAltPhone { get; set; }
            public string EmployeeIDInCompany { get; set; }
            
            public string CompanyStreetAddr { get; set; }
            public string CompanyCity { get; set; }
            public string CompanyState { get; set; }
            public string CompanyZip { get; set; }           

            public string PhoneOfCompany { get; set; }
            public string FaxOfCompany { get; set; }
            public string AgentNote { get; set; }
            public string CommissionPointOfLoanAmount { get; set; }
            public string CommissionPointOfGrossProfit { get; set; }
            public string CommissionMinBase { get; set; }
            public string IsNotifyWhenLoanStatusChange { get; set; }
            public string TaxID { get; set; }
            public string LoanOriginatorIdentifier { get; set; }
            public string CompanyLoanOriginatorIdentifier { get; set; }

            public string AgentLicensesPanel { get; set; }
            public string CompanyLicensesPanel { get; set; }
            public string EmployeeId { get; set; }
            public string CompanyId { get; set; }

            public string BranchName { get; set; }
            public string PayToBankName { get; set; }
            public string PayToBankCityState { get; set; }
            public string PayToABANumber { get; set; }
            public string PayToAccountNumber { get; set; }
            public string PayToAccountName { get; set; }
            public string FurtherCreditToAccountNumber { get; set; }
            public string FurtherCreditToAccountName { get; set; }

            public string AgentSourceT { get; set; }
            public string BrokerLevelAgentID { get; set; }
            public string ShouldMatchBrokerContact { get; set; }

            public string ChumsId { get; set; }

            public string RecordId { get; set; }

            public string OverrideLicenses { get; set; }

            // OPM 227085, 12/29/2015, ml
            public string IsLender { get; set; }
            public string IsOriginator { get; set; }
        }
        public class ContactFieldMapperInfo
        {
            public Guid LoanId { get; set; }
            public string OfficialContactListClientId { get; set; }
            public string UseOfficialContactClientId { get; set; }
            public string ManualOverrideClientId { get; set; }
            public string PickFromContactClientId { get; set; }
            public string AddToContactClientId { get; set; }
            public string AgentRoleTDDLClientId { get; set; }
            public string DefaultAgentRoleT { get; set; }
            public string ClientID { get; set; }
            public FieldIdHolder FieldIdHolder { get; set; }
        }
		#region{ Private members }
		private Hashtable m_Fields = new  Hashtable();
		private string    m_sStatusLoanChangeField;
		private string    m_sTabIndex;
		private string    m_sType;
		private string    m_sName;
		private string	  m_sTypeD;

        private FieldIdHolder m_fieldIdHolder = new FieldIdHolder();
        private bool x_isLocked = true;

        private bool m_disableAddTo = false;
        private bool m_disableLinks = false;
		#endregion

		#region {Public Members Accessors} 

        /// <summary>
        /// If this setting turn on then we will display the official contact drop down and populate the information
        /// from Official Contact List.
        /// </summary>
        public bool IsAllowLockableFeature
        {
            get;
            set;
        }
        private E_AgentRoleT m_agentRoleT;

        public E_AgentRoleT AgentRoleT
        {
            get { return m_agentRoleT; }
            set
            {
                m_agentRoleT = value;
                Tools.SetDropDownListValue(m_officialContactList, value);
            }
        }
        /// <summary>
        /// Use this property to indicate whether information is from official contact or manual input from user. 
        /// This will always return true if IsAllowLockableFeature does not turn on.
        /// </summary>
        public bool IsLocked
        {
            get { return !IsAllowLockableFeature || x_isLocked; }
            set { x_isLocked = value; }
        }
        public Guid LoanID
        {
            get { return RequestHelper.LoanID; }
        }
		public string TypeDDL  
		{
			get { return m_sTypeD; } 
			set { m_sTypeD = value; }
		}
		
		public string Type 
		{
			get { return m_sType == null  ? null : m_sType ;} 
			set { 
                m_sType = value.TrimWhitespaceAndBOM().Length > 0 ? value.TrimWhitespaceAndBOM() : null ;

            }  
		}

		public string AgentNameField
		{
			get{ return m_Fields["AgentName"].ToString();   }
			set
            { 
                m_Fields.Add("AgentName",value);
                m_fieldIdHolder.AgentName = value;
            }
		}
		public string CompanyNameField
		{
			get	{ return m_Fields["AgentCompanyName"].ToString();  }
			set 
            { 
                m_Fields.Add("AgentCompanyName",value);
                m_fieldIdHolder.AgentCompanyName = value;
            }
		}
		public string DepartmentNameField
		{
			get {return m_Fields["AgentDepartmentName"].ToString();}
            set
            {
                m_Fields.Add("AgentDepartmentName", value);
                m_fieldIdHolder.AgentDepartmentName = value;
            }
		}
		public string StreetAddressField
		{
			get { return m_Fields["AgentStreetAddr"].ToString(); }
            set
            {
                m_Fields.Add("AgentStreetAddr", value);
                m_fieldIdHolder.AgentStreetAddr = value;
            }
		}

		public string CityField
		{
			get { return  m_Fields["AgentCity"].ToString(); }
            set
            {
                m_Fields.Add("AgentCity", value);
                m_fieldIdHolder.AgentCity = value;
            }
		}

		public string StateField
		{
			get	{ return  m_Fields["AgentState"].ToString(); }
			set 
            { 
                m_Fields.Add("AgentState",value);
                m_fieldIdHolder.AgentState = value;
            }
		}

		public string ZipField
		{
			get{ return m_Fields["AgentZip"].ToString();}
			set
            { 
                m_Fields.Add("AgentZip",value);
                m_fieldIdHolder.AgentZip = value;
            }
		}

        public string CountyField
        {
            get { return m_Fields["AgentCounty"].ToString(); }
            set
            {
                m_Fields.Add("AgentCounty", value);
                m_fieldIdHolder.AgentCounty = value;
            }
        }

		public string PhoneField
		{
			get{ return  m_Fields["AgentPhone"].ToString(); }
            set
            {
                m_Fields.Add("AgentPhone", value);
                m_fieldIdHolder.AgentPhone = value;
            }
		}

		public string FaxField
		{
			get{ return  m_Fields["AgentFax"].ToString(); }
			set 
            { 
                m_Fields.Add("AgentFax",value);
                m_fieldIdHolder.AgentFax = value;
            }
		}

        public string EmployeeIDInCompanyField
        {
            get { return m_Fields["EmployeeIDInCompany"].ToString(); }
            set
            {
                m_Fields.Add("EmployeeIDInCompany", value);
                m_fieldIdHolder.EmployeeIDInCompany = value;
            }
        }

		public string AgentTitleField
		{
			get{ return  m_Fields["AgentTitle"].ToString(); }
			set { 
                m_Fields.Add("AgentTitle",value);
                m_fieldIdHolder.AgentTitle = value;
            }
		}
		public string EmailField
		{
			get{ return  m_Fields["AgentEmail"].ToString(); }
			set { 
                m_Fields.Add("AgentEmail",value);
                m_fieldIdHolder.AgentEmail = value;
            }
		}

		public string AgentLicenseField
		{
			get{ return  m_Fields["AgentLicenseNumber"].ToString(); }
			set { 
                m_Fields.Add("AgentLicenseNumber",value);
                m_fieldIdHolder.AgentLicenseNumber = value;
            }
		}

		public string CompanyLicenseField
		{
			get{ return  m_Fields["CompanyLicenseNumber"].ToString(); }
			set { 
                m_Fields.Add("CompanyLicenseNumber",value);
                m_fieldIdHolder.CompanyLicenseNumber = value;
            }
		}

        public string AgentLicensesPanel {
            get { return m_Fields["AgentLicensesPanel"].ToString(); }
            set
            {
                m_Fields.Add("AgentLicensesPanel", value);
                m_fieldIdHolder.AgentLicensesPanel = value;
            }
        }

        public string CompanyLicensesPanel
        {
            get { return m_Fields["CompanyLicensesPanel"].ToString(); }
            set
            {
                m_Fields.Add("CompanyLicensesPanel", value);
                m_fieldIdHolder.CompanyLicensesPanel = value;
            }
        }

		public string PagerNumField
		{
			get{ return  m_Fields["AgentPager"].ToString(); }
			set { 
                m_Fields.Add("AgentPager",value);
                m_fieldIdHolder.AgentPager = value; 
            }
		}

		public string CellPhoneField
		{
			get{ return  m_Fields["AgentAltPhone"].ToString(); }
			set { 
                m_Fields.Add("AgentAltPhone",value);
                m_fieldIdHolder.AgentAltPhone = value;
            }
		}
        public string CompanyStreetAddr
		{
			get{ return  m_Fields["CompanyStreetAddr"].ToString(); }
			set { 
                m_Fields.Add("CompanyStreetAddr",value);
                m_fieldIdHolder.CompanyStreetAddr = value;
            }
		}
        public string CompanyCity
		{
			get{ return  m_Fields["CompanyCity"].ToString(); }
			set { 
                m_Fields.Add("CompanyCity",value);
                m_fieldIdHolder.CompanyCity = value;
            }
		}
        public string CompanyState
		{
			get{ return  m_Fields["CompanyState"].ToString(); }
			set { 
                m_Fields.Add("CompanyState",value);
                m_fieldIdHolder.CompanyState = value;
            }
		}
        public string CompanyZip
		{
			get{ return  m_Fields["CompanyZip"].ToString(); }
			set { 
                m_Fields.Add("CompanyZip",value);
                m_fieldIdHolder.CompanyZip = value;
            }
		}
		public string CompanyPhoneField
		{
			get{ return  m_Fields["PhoneOfCompany"].ToString(); }
			set { 
                m_Fields.Add("PhoneOfCompany",value);
                m_fieldIdHolder.PhoneOfCompany = value;
            }
		}

		public string CompanyFaxField
		{
			get{ return  m_Fields["FaxOfCompany"].ToString(); }
			set 
            { 
                m_Fields.Add("FaxOfCompany",value);
                m_fieldIdHolder.FaxOfCompany = value;
            }
		}

		public string StatusLoanChangeField
		{
			get{ return  m_sStatusLoanChangeField; }
			set { 
                m_sStatusLoanChangeField = value;

                m_fieldIdHolder.IsNotifyWhenLoanStatusChange = value;
            }
		}

		public string ComPointLoanAmtField
		{
			get{ return  m_Fields["CommissionPointOfLoanAmount"].ToString(); }
			set { 
                m_Fields.Add("CommissionPointOfLoanAmount",value);
                m_fieldIdHolder.CommissionPointOfLoanAmount = value;
            }
		}

		public string ComPointGrossField
		{
			get{ return  m_Fields["CommissionPointOfGrossProfit"].ToString(); }
			set { 
                m_Fields.Add("CommissionPointOfGrossProfit",value);
                m_fieldIdHolder.CommissionPointOfGrossProfit = value;
            }
		}

		public string ComMinBaseField
		{
			get{ return  m_Fields["CommissionMinBase"].ToString(); }
			set { 
                m_Fields.Add("CommissionMinBase",value);
                m_fieldIdHolder.CommissionMinBase = value;
            }
		}

        public string TaxIdField
        {
            get { return m_Fields["TaxID"].ToString(); }
            set
            {
                m_Fields.Add("TaxID", value);
                m_fieldIdHolder.TaxID = value;
            }
        }

        public string ChumsIdField
        {
            get { return m_Fields["ChumsId"].ToString(); }
            set
            {
                m_Fields.Add("ChumsId", value);
                m_fieldIdHolder.ChumsId = value;
            }
        }
        public string LoanOriginatorIdentifierField
        {
            get { return m_Fields["LoanOriginatorIdentifier"].ToString(); }
            set
            {
                m_Fields.Add("LoanOriginatorIdentifier", value);
                m_fieldIdHolder.LoanOriginatorIdentifier = value;
            }
        }
        public string CompanyLoanOriginatorIdentifierField
        {
            get
            {
                return m_Fields["CompanyLoanOriginatorIdentifier"].ToString();
            }
            set
            {
                m_Fields.Add("CompanyLoanOriginatorIdentifier", value);
                m_fieldIdHolder.CompanyLoanOriginatorIdentifier = value;
            }
        }

		public string TabIndex 
		{
			get{ return  m_sTabIndex == null ? "0" : m_sTabIndex; }
			set { m_sTabIndex = value; }
		}

		public string NotesField
		{
			get{ return  m_Fields["AgentNote"].ToString(); }
			set { 
                m_Fields.Add("AgentNote",value);
                m_fieldIdHolder.AgentNote = value;
            }
		}
        public string EmployeeIdField
        {
            get { return m_Fields["EmployeeId"].ToString(); }
            set
            {
                m_Fields.Add("EmployeeId", value);
                m_fieldIdHolder.EmployeeId = value;
            }
        }
        public string CompanyIdField
        {
            get { return m_Fields["CompanyId"].ToString(); }
            set
            {
                m_Fields.Add("CompanyId", value);
                m_fieldIdHolder.CompanyId = value;
            }
        }
        public string BranchNameField
        {
            get { return m_Fields["BranchName"].ToString(); }
            set
            {
                m_Fields.Add("BranchName", value);
                m_fieldIdHolder.BranchName = value;
            }
        }
        public string PayToBankNameField 
        {
            get { return m_Fields["PayToBankName"].ToString(); }
            set
            {
                m_Fields.Add("PayToBankName", value);
                m_fieldIdHolder.PayToBankName = value;
            }
        }
        public string PayToBankCityStateField
        {
            get { return m_Fields["PayToBankCityState"].ToString(); }
            set
            {
                m_Fields.Add("PayToBankCityState", value);
                m_fieldIdHolder.PayToBankCityState = value;
            }
        }
        public string PayToABANumberField
        {
            get { return m_Fields["PayToABANumber"].ToString(); }
            set
            {
                m_Fields.Add("PayToABANumber", value);
                m_fieldIdHolder.PayToABANumber = value;
            }
        }
        public string PayToAccountNumberField
        {
            get { return m_Fields["PayToAccountNumber"].ToString(); }
            set
            {
                m_Fields.Add("PayToAccountNumber", value);
                m_fieldIdHolder.PayToAccountNumber = value;
            }
        }
        public string PayToAccountNameField
        {
            get { return m_Fields["PayToAccountName"].ToString(); }
            set
            {
                m_Fields.Add("PayToAccountName", value);
                m_fieldIdHolder.PayToAccountName = value;
            }
        }
        public string FurtherCreditToAccountNumberField
        {
            get { return m_Fields["FurtherCreditToAccountNumber"].ToString(); }
            set
            {
                m_Fields.Add("FurtherCreditToAccountNumber", value);
                m_fieldIdHolder.FurtherCreditToAccountNumber = value;
            }
        }
        public string FurtherCreditToAccountNameField
        {
            get { return m_Fields["FurtherCreditToAccountName"].ToString(); }
            set
            {
                m_Fields.Add("FurtherCreditToAccountName", value);
                m_fieldIdHolder.FurtherCreditToAccountName = value;
            }
        }
        public string AgentSourceTField
        {
            get { return m_Fields["AgentSourceT"].ToString(); }
            set
            {
                m_Fields.Add("AgentSourceT", value);
                m_fieldIdHolder.AgentSourceT = value;
            }
        }
        public string BrokerLevelAgentIDField
        {
            get { return m_Fields["BrokerLevelAgentID"].ToString(); }
            set
            {
                m_Fields.Add("BrokerLevelAgentID", value);
                m_fieldIdHolder.BrokerLevelAgentID = value;
            }
        }
        public string ShouldMatchBrokerContactField
        {
            get { return m_Fields["ShouldMatchBrokerContact"].ToString(); }
            set
            {
                m_Fields.Add("ShouldMatchBrokerContact", value);
                m_fieldIdHolder.ShouldMatchBrokerContact = value;
            }
        }

        public string RecordIdField
        {
            get { return m_Fields["RecordId"].ToString(); }
            set
            {
                m_Fields.Add("RecordId", value);
                m_fieldIdHolder.RecordId = value;
            }
        }

        public string IsLenderField
        {
            get { return m_Fields["IsLender"].ToString(); }
            set
            {
                m_Fields.Add("IsLender", value);
                m_fieldIdHolder.IsLender = value;
            }
        }

        public string IsOriginatorField
        {
            get { return m_Fields["IsOriginator"].ToString(); }
            set
            {
                m_Fields.Add("IsOriginator", value);
                m_fieldIdHolder.IsOriginator = value;
            }
        }

        public string OverrideLicensesField
        {
            get { return m_Fields["OverrideLicenses"].ToString(); }
            set
            {
                m_Fields.Add("OverrideLicenses", value);
                m_fieldIdHolder.OverrideLicenses = value;
            }
        }

		public string Name 
		{
			get{ return m_sName == null ? String.Empty :m_sName ;}
			set{ m_sName = "_"+value; }
		}

        public bool RestrictOfficialDropDownTypes { get; set; }

        public bool DisableAddTo
        {
            get { return m_disableAddTo; }
            set { m_disableAddTo = value; m_linkAddToContact.Visible = !value; }
        }
        
        public bool DisableLinks
        {
            get { return m_disableLinks; }
            set
            {
                m_disableLinks = value;
                if (m_disableLinks)
                {
                    m_linkAddToContact.Attributes.Add("disabled", "disabled");
                    m_linkAddToContact.Style.Add("cursor", "default");

                    m_linkPickFromContact.Attributes.Add("disabled", "disabled");
                    m_linkPickFromContact.Style.Add("cursor", "default");
                }
            }
        }

		#endregion
		

		override protected void OnInit(EventArgs e)
		{
            this.Init += new EventHandler(ContactFieldMapper_Init);
            this.Load += new EventHandler(ContactFieldMapper_Load);
            this.PreRender += new EventHandler(ContactFieldMapper_PreRender);
			base.OnInit(e);
		}

        void ContactFieldMapper_Load(object sender, EventArgs e)
        {
            // 5/20/2009 dd - Only register the FieldHolderId object after the values are set.
            ContactFieldMapperInfo info = new ContactFieldMapperInfo();
            info.LoanId = this.LoanID;
            info.AddToContactClientId = m_linkAddToContact.ClientID;
            info.PickFromContactClientId = m_linkPickFromContact.ClientID;
            info.ManualOverrideClientId = m_rbManualOverride.ClientID;
            info.UseOfficialContactClientId = m_rbUseOfficialContact.ClientID;
            info.OfficialContactListClientId = m_officialContactList.ClientID;
            info.FieldIdHolder = m_fieldIdHolder;
            info.DefaultAgentRoleT = Type;
            info.AgentRoleTDDLClientId = TypeDDL;
            info.ClientID = this.ClientID;

            ((BasePage)this.Page).RegisterJsObject(JsObjectVarName, info); // This call will ensure that JsObjectVarName is a valid JS variable name, as it is used in dynamic js in other parts of this control
        }

        void ContactFieldMapper_PreRender(object sender, EventArgs e)
        {
            m_officialContactList.Visible = IsAllowLockableFeature;
            if (RestrictOfficialDropDownTypes )
            {
                var item = m_officialContactList.SelectedItem; // get selected type from DDL 
                if ( !string.IsNullOrEmpty(m_sType) )  //look for the one set by type 
                {
                    item = m_officialContactList.Items.FindByValue(m_sType) ?? item;  //if we dont find it go ahead and leave it to the selected
                }
                m_officialContactList.Items.Clear();
                m_officialContactList.Items.Add(item);
            }
            m_rbManualOverride.Visible = IsAllowLockableFeature;
            m_rbUseOfficialContact.Visible = IsAllowLockableFeature;
            if (IsAllowLockableFeature)
            {
                m_rbManualOverride.Checked = IsLocked;
                m_rbUseOfficialContact.Checked = !m_rbManualOverride.Checked;

                // 5/21/2009 dd - Set the two hidden variables so the javascript can retrieve the selected agent role and lock method.
                Page.ClientScript.RegisterHiddenField(this.ClientID + "_AgentRoleT", m_officialContactList.SelectedValue);
                Page.ClientScript.RegisterHiddenField(this.ClientID + "_IsLocked", IsLocked.ToString());
            }

            AbstractUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
            bool allowPickFromContact = principal.HasPermission(Permission.AllowReadingFromRolodex);
            bool allowWriteToContact = principal.HasPermission(Permission.AllowWritingToRolodex);

            m_linkPickFromContact.Visible = allowPickFromContact;
            m_linkAddToContact.Visible = (allowWriteToContact && !DisableAddTo);

            if (m_linkPickFromContact.Visible)
            {
                m_linkPickFromContact.Attributes.Add("onclick", "if (!this.disabled && !isReadOnly() && !hasDisabledAttr(this)) PickFromContact_OnClick(" + JsObjectVarName + "); return false;");
                m_linkPickFromContact.Attributes.Add("onmouseover", "if (this.disabled || hasDisabledAttr(this)) {this.style.cursor='default';} else{this.style.cursor='hand';}");
            }
            if (m_linkAddToContact.Visible)
            {
                m_linkAddToContact.Attributes.Add("onclick", "if (!this.disabled && !isReadOnly() && !hasDisabledAttr(this)) AddToContact_OnClick(" + JsObjectVarName + "); return false;");
                m_linkAddToContact.Attributes.Add("onmouseover", "if (this.disabled || hasDisabledAttr(this)) {this.style.cursor='default';} else{this.style.cursor='hand';}");
            }



        }
        private string JsObjectVarName
        {
            get { return "m_" + this.ClientID; }
        }

        private void ContactFieldMapper_Init(object sender, EventArgs e)
        {
            string onchangeScript = string.Format("return OfficialContactList_OnChange({0});", JsObjectVarName);
            m_officialContactList.Attributes.Add("onchange", onchangeScript);

            m_rbUseOfficialContact.Attributes.Add("onclick", "return UseOfficialContact_OnClick(" + JsObjectVarName + ");");
            m_rbManualOverride.Attributes.Add("onclick", "return ManualOverride_OnClick(" + JsObjectVarName + ");");
           
            RolodexDB.PopulateAgentTypeDropDownList(m_officialContactList);
  
            ((BasePage)this.Page).RegisterJsScript("ContactFieldMapper.js");
            ((BasePage)this.Page).AddInitScriptFunctionWithArgs("ContactFieldMapper_OnInit", JsObjectVarName);
        }
	}
}
