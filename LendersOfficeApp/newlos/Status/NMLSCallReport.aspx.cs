﻿using System;
using System.Collections.Generic;
namespace LendersOfficeApp.newlos.Status
{
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Migration;
    using LendersOffice.NmlsCallReport;

    public partial class NMLSCallReport : BaseLoanPage
    {
        protected void PageInit(object sender, System.EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            Tools.Bind_sHmdaActionTaken(this.sHmdaActionTaken);
            Tools.Bind_sHmdaActionTakenT(this.sHmdaActionTakenT);
            Tools.Bind_sLT(sLT);
            Tools.Bind_sHmdaPropT(sHmdaPropT);
            Tools.Bind_sLPurposeT(sLPurposeT);
            Tools.Bind_sLienPosT(sLienPosT);
            Tools.Bind_sFinMethT(sFinMethT);
            Tools.Bind_sMortgageLoanT(sMortgageLoanT);
            Tools.Bind_sJumboT(sJumboT);
            Tools.Bind_sDocumentationT(sDocumentationT);
            Tools.Bind_sNMLSLoanPurposeT(sNMLSLoanPurposeT);
            Tools.Bind_sOccT(sOccT);
            Tools.Bind_sLoanSaleDispositionT(sLoanSaleDispositionT);
            Tools.Bind_PurchaseAdviceServicingStatus(sPurchaseAdviceSummaryServicingStatus);
            BindTriState(sIsBranchActAsLenderForFileTri);
            BindTriState(sIsBranchActAsOriginatorForFileTri);

            Bind_ServicingIntent();
            Bind_PeriodForDaysDelinquent();
            Bind_QMStatusT();

            if(false == BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowAccountantRead))
            {
                rowLoanSaleDisposition.Visible = false;
                rowServicing.Visible = false;
                rowDaysOfInterest.Visible = false;
            }

            this.PageTitle = "NMLS Call Report";
            this.PageID = "NMLSCallReport";
        }

        public void Bind_ServicingIntent()
        {
            List<ListItem> list = new List<ListItem>();
            sNMLSServicingIntentT.Items.Add(createIntListItem("sell", (int)E_ServicingStatus.Released));
            sNMLSServicingIntentT.Items.Add(createIntListItem("retain", (int)E_ServicingStatus.Retained));
        }

        public void Bind_PeriodForDaysDelinquent()
        {
            sNMLSPeriodForDaysDelinquentT.Items.Add(createIntListItem("Q1 (January-March)", (int)E_McrQuarterT.Q1));
            sNMLSPeriodForDaysDelinquentT.Items.Add(createIntListItem("Q2 (April-June)", (int)E_McrQuarterT.Q2));
            sNMLSPeriodForDaysDelinquentT.Items.Add(createIntListItem("Q3 (July-September)", (int)E_McrQuarterT.Q3));
            sNMLSPeriodForDaysDelinquentT.Items.Add(createIntListItem("Q4 (October-December)", (int)E_McrQuarterT.Q4));
        }

        public void Bind_QMStatusT()
        {
            sQMStatusT.Items.Add(createIntListItem("", (int)E_sQMStatusT.Blank));
            sQMStatusT.Items.Add(createIntListItem("Eligible", (int)E_sQMStatusT.Eligible));
            sQMStatusT.Items.Add(createIntListItem("Ineligible", (int)E_sQMStatusT.Ineligible));
            sQMStatusT.Items.Add(createIntListItem("Provisionally Eligible", (int)E_sQMStatusT.ProvisionallyEligible));
        }

        public ListItem createIntListItem(String desc, int val)
        {
            return new ListItem(desc, "" + val);
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(NMLSCallReport));
            dataLoan.InitLoad();
            dataLoan.ByPassFieldSecurityCheck = true;
            CAppData dataApp = null;
            BindDataObject(dataLoan, dataApp);
        }

        private void BindDataObject(CPageData dataLoan, CAppData dataApp)
        {
            // ** Application data
            sBranchChannelT.Text = Tools.GetDescription(dataLoan.sBranchChannelT);
            sOpenedD.Text = dataLoan.sOpenedD_rep;
            sClosedD.Text = dataLoan.sClosedD_rep;
            sHmdaExcludedFromReport.Checked = dataLoan.sHmdaExcludedFromReport;

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
            {
                Tools.SetDropDownListValue(this.sHmdaActionTakenT, dataLoan.sHmdaActionTakenT);
            }
            else
            {
                sHmdaActionTaken.Text = dataLoan.sHmdaActionTaken;
            }

            sHmdaActionD.Text = dataLoan.sHmdaActionD_rep;

            sNMLSApplicationAmount.Text = dataLoan.sNMLSApplicationAmount_rep;
            sNMLSApplicationAmountLckd.Checked = dataLoan.sNMLSApplicationAmountLckd;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sNMLSNetChangeApplicationAmount.Text = dataLoan.sNMLSNetChangeApplicationAmount_rep;

            // ** Closed loan data
            Tools.SetDropDownListValue(sLT, dataLoan.sLT);

            sGseSpT.Value = dataLoan.sGseSpT.ToString("D");
            Tools.SetDropDownListValue(sHmdaPropT, dataLoan.sHmdaPropT);
            sHmdaPropTLckd.Checked = dataLoan.sHmdaPropTLckd;

            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            sHmdaReportAsHomeImprov.Checked = dataLoan.sHmdaReportAsHomeImprov;
            sHmdaReportAsHoepaLoan.Checked = dataLoan.sHmdaReportAsHoepaLoan;

            // lien position
            Tools.SetDropDownListValue(sLienPosT, dataLoan.sLienPosT);

            sLenderFeesCollected.Text = dataLoan.sLenderFeesCollected_rep;
            sBrokerFeesCollected.Text = dataLoan.sBrokerFeesCollected_rep;
            sLenderFeesCollectedLckd.Checked = dataLoan.sLenderFeesCollectedLckd;
            sBrokerFeesCollectedLckd.Checked = dataLoan.sBrokerFeesCollectedLckd;

            // ** Expanded Call Report Data
            sLpTemplateNm.Text = dataLoan.sLpTemplateNm;
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
            Tools.SetDropDownListValue(sMortgageLoanT, dataLoan.sMortgageLoanT);
            Tools.SetDropDownListValue(sJumboT, dataLoan.sJumboT);
            Tools.SetDropDownListValue(sDocumentationT, dataLoan.sDocumentationT);
            sIOnlyMon.Text = dataLoan.sIOnlyMon_rep;
            sIsOptionArm.Checked = dataLoan.sIsOptionArm;
            sHasPrepaymentPenalty.Checked = dataLoan.sHasPrepaymentPenalty;
            Tools.SetDropDownListValue(sNMLSLoanPurposeT, dataLoan.sNMLSLoanPurposeT);
            Tools.SetDropDownListValue(sOccT, dataLoan.sOccT);
            sHasPrivateMortgageInsurance.Checked = dataLoan.sHasPrivateMortgageInsurance;
            sHasPiggybackFinancing.Checked = dataLoan.sHasPiggybackFinancing;
            sCreditScoreLpeQual.Text = dataLoan.sCreditScoreLpeQual_rep;
            sNMLSLtvR.Text = dataLoan.sNMLSLtvR_rep;
            sNMLSCLtvR.Text = dataLoan.sNMLSCLtvR_rep;
            Tools.SetDropDownListValue(sLoanSaleDispositionT, dataLoan.sLoanSaleDispositionT);
            Tools.SetDropDownListValue(sPurchaseAdviceSummaryServicingStatus, dataLoan.sPurchaseAdviceSummaryServicingStatus);
            sDisbursementDaysOfInterest.Text = dataLoan.sDisbursementDaysOfInterest_rep;
            sDisbursementDaysOfInterestLckd.Checked = dataLoan.sDisbursementDaysOfInterestLckd;


            sIsBranchActAsOriginatorForFileLabel.Text = dataLoan.sIsBranchActAsOriginatorForFileLabel;
            sIsBranchActAsLenderForFileLabel.Text = dataLoan.sIsBranchActAsLenderForFileLabel;
            Tools.Set_ListControl(sIsBranchActAsLenderForFileTri, dataLoan.sIsBranchActAsLenderForFileTri);
            Tools.Set_ListControl(sIsBranchActAsOriginatorForFileTri, dataLoan.sIsBranchActAsOriginatorForFileTri);

            sServicingByUsStartD.Text = dataLoan.sServicingByUsStartD_rep;
            sServicingByUsStartDLckd.Checked = dataLoan.sServicingByUsStartDLckd;

            sServicingByUsEndD.Text = dataLoan.sServicingByUsEndD_rep;
            sServicingByUsEndDLckd.Checked = dataLoan.sServicingByUsEndDLckd;

            sGLServTransEffD.Text = dataLoan.sGLServTransEffD_rep;

            sNMLSServicingTransferInD.Text = dataLoan.sNMLSServicingTransferInD_rep;
            sNMLSServicingTransferInDLckd.Checked = dataLoan.sNMLSServicingTransferInDLckd;

            sNMLSServicingTransferOutD.Text = dataLoan.sNMLSServicingTransferOutD_rep;
            sNMLSServicingTransferOutDLckd.Checked = dataLoan.sNMLSServicingTransferOutDLckd;

            sNMLSPeriodForDaysDelinquentLckd.Checked = dataLoan.sNMLSPeriodForDaysDelinquentLckd;
            sNMLSPeriodForDaysDelinquentT.SelectedValue = "" + (int)dataLoan.sNMLSPeriodForDaysDelinquentT;

            sNMLSDaysDelinquentAsOfPeriodEnd.Text = dataLoan.sNMLSDaysDelinquentAsOfPeriodEnd_rep;

            sNMLSServicingIntentT.SelectedValue = "" + (int)dataLoan.sNMLSServicingIntentT;
            sNMLSServicingIntentLckd.Checked = dataLoan.sNMLSServicingIntentLckd;

            sQMStatusT.SelectedValue = "" + (int)dataLoan.sQMStatusT;

            sNmlsApplicationDate.Text = dataLoan.sNmlsApplicationDate_rep;
            sNmlsApplicationDateLckd.Checked = dataLoan.sNmlsApplicationDateLckd;

        }
        private void BindTriState(RadioButtonList rbl)
        {
            rbl.Items.Add(new ListItem("Yes", E_TriState.Yes.ToString("D")));
            rbl.Items.Add(new ListItem("No", E_TriState.No.ToString("D")));
            rbl.Items.Add(new ListItem("Not Sure", E_TriState.Blank.ToString("D")));
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
