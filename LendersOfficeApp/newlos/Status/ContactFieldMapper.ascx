<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ContactFieldMapper.ascx.cs" Inherits="LendersOfficeApp.newlos.Status.ContactFieldMapper" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="DataAccess"%>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<table>
  <tr>
  <td class="FieldLabel"><asp:RadioButton ID="m_rbUseOfficialContact" runat="server" GroupName="PreparerFlow" Text="Populate from Official Contact"/></td>
  <td><asp:DropDownList ID="m_officialContactList" runat="server"/></td>
  </tr>
  <tr>
    <td><asp:RadioButton ID="m_rbManualOverride" runat="server" GroupName="PreparerFlow" Text="Set Manually" CssClass="FieldLabel"/></td>
    <td><asp:HyperLink ID="m_linkPickFromContact" name="linkPickFromContact" runat="server" Text="Pick from Contacts" href="#" />
<asp:HyperLink ID="m_linkAddToContact" runat="server" Text="Add to Contacts" href="#" /></td>
  </tr>
</table>    
