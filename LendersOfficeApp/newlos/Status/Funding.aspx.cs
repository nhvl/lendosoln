using System;
using DataAccess;
using LendersOffice.Admin;

namespace LendersOfficeApp.newlos.Status
{
	public partial class Funding : LendersOfficeApp.newlos.BaseLoanPage
	{

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Funding";
            this.PageID = "Funding";

            // 12/10/2004 dd - Bind custom field choices to ComboBox
            MeridianLink.CommonControls.ComboBox[] items = {
                                                               sInternalFundingSrcDesc,
                                                               sExternalFundingSrcDesc,
                                                               sWarehouseFunderDesc
                                                           };

            foreach (MeridianLink.CommonControls.ComboBox cb in items) 
            {
                FieldOptions options = Broker.FieldChoices[cb.ID];
                // 12/13/2004 dd - Allow blank to be one of the valid option.
                cb.Items.Add(" ");
                if (null != options) 
                {
                    foreach (string s in options.Options) 
                        cb.Items.Add(s);
                }
            }

            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Funding));
            dataLoan.InitLoad();

            sInternalFundingSrcDesc.Text = dataLoan.sInternalFundingSrcDesc;
            sExternalFundingSrcDesc.Text = dataLoan.sExternalFundingSrcDesc;
            sWarehouseFunderDesc.Text    = dataLoan.sWarehouseFunderDesc;
            sIsTexasHomeEquity.Checked   = dataLoan.sIsTexasHomeEquity;
            sIsDocDrawExternal.Checked   = dataLoan.sIsDocDrawExternal;

        }



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            this.UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
