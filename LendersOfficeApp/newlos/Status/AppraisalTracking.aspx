﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AppraisalTracking.aspx.cs" Inherits="LendersOfficeApp.newlos.Status.AppraisalTracking" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.ObjLib.Appraisal" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title>Appraisal Tracking</title>

    <style>
        body {
            background-color: gainsboro;
        }

        .SubHeader {
            width: 1100px;
            padding: 5px;
            margin: 10px;
            border: 1px solid black;
            border-collapse: collapse;
            background-color: #999999;
            color: white;
            font-weight: bold;
        }

        .IndentedTable {
            width: 1100px;
            margin-left: 50px;
            border-collapse: collapse;
        }

        .OrderInfoTable {
            width: 1100px;
            border-collapse: collapse;
            margin-top: 10px;
            table-layout: fixed;
        }

        .TopRow td:nth-child(2) {
            width: 280px;
        }

        .TopRow td:nth-child(3) {
            width: 125px;
        }

        .TopRow td:nth-child(4) {
            width: 135px;
        }

        .TopRow td:nth-child(5) {
            width: 85px;
        }

        .TopRow td:nth-child(6) {
            width: 70px;
        }

        .TopRow td:nth-child(7) {
            width: 370px;
        }

        #OrderInfoForCloning, #DocLinkTemplate, #RestoreRowTemplate, #dialog-restore {
            display: none;
        }

        .InternalTable {
            width: 1050px;
            margin: 10px;
        }

        .PadCell {
            width: 100px;
        }

        .RemoveCell {
            padding: 10px;
            vertical-align: top;
        }

        .BorderedCell {
            border: 1px solid black;
            border-collapse: collapse;
            background-color: white;
            vertical-align: top;
            padding: 0;
        }

        .CellHeader {
            background-color: lightgray;
            color: black;
            padding: 5px;
            white-space: nowrap;
        }

        .CellBody {
            padding: 5px;
        }

        .ValuationCell {
            white-space: nowrap;
        }

        .ProgressBarCell {
            text-align: center;
            width: 400px;
            height: 75px;
            padding: 5px;
        }

        .ProductName {
            width: 270px;
        }

        .ViewLink {
            white-space: nowrap;
        }

        .ProgressBar {
            fill: white;
        }

        .DatesTable {
            width: 600px;
        }

        .NotesAndDocsTable {
            width: 450px;
        }

        .UploadedDocumentsCell {
            width: 350px;
        }

        .UploadedDocuments {
            height: 35px;
            overflow-y: scroll;
            overflow-x: hidden;
        }

        .Notes {
            height: 35px;
            width: 345px;
        }

        .ButtonsDiv {
            margin: 10px;
        }

        #dialog-confirm {
            text-align: center;
            display: none;
        }

        .RestoreTable {
            width: 500px;
            border-collapse: collapse;
        }

        .RestoreTable th {
            border-top: 1px solid black;
        }

        .RestoreTable td, .RestoreTable th {
            text-align: left;
            padding: 5px;
            border-left: 1px solid black;
        }

        .RestoreTable th:first-child, .RestoreTable td:first-child {
            width: 20px;
        }

        .RestoreTable th:last-child, .RestoreTable td:last-child {
            border-right: 1px solid black;
        }

         .RestoreTable tr:last-child td {
            border-bottom: 1px solid black;
         }

        .headerRow {
            white-space: nowrap;
        }

        .LQBDialogBox .ui-dialog-buttonpane div.ui-dialog-buttonset { 
            float: none;
        }
        .CuRiskScore, .OvervaluationRiskT, .PropertyEligibilityRiskT, .AppraisalQualityRiskT {
            margin-left: 8px;
        }
        .CuRiskScore {
            width: 40px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="DocLinkTemplate"><a runat="server" class="UploadedDocLink"></a></div>

        <div id="dialog-confirm" title="Delete record?">
            <p>Are you sure you would like to remove this record?</p>
        </div>

        <div id="dialog-restore" title="Deleted Records">
            <table class="RestoreTable">
                <thead>
                    <tr class="GridHeader headerRow">
                        <th>&nbsp;</th>
                        <th>Product Name</th>
                        <th>Valuation Description</th>
                        <th>File #</th>
                        <th>Deleted By</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="RestoreRow GridAutoItem" id="RestoreRowTemplate">
                        <td>
                            <input type="checkbox" class="RestoreBox"/>
                            <input type="hidden" class="RestoreIndex" value="-1" />
                        </td>
                        <td>
                            <span class="RestoreProductName" />
                        </td>
                        <td>
                            <span class="RestoreValuation" />
                        </td>
                        <td>
                            <span class="RestoreOrderNumber" />
                        </td>
                        <td>
                            <span class="RestoreDeletedBy" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="MainRightHeader" runat="server" id="PageHeader">Appraisal Tracking</div>
        <div class="SubHeader">Important Dates</div>
        <table class="IndentedTable">
            <tr>
                <td>
                    Intent to Proceed Received Date
                </td>
                <td>
                    <ml:DateTextBox ID="sIntentToProceedD" runat="server" preset="date" TabIndex="1"></ml:DateTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    Borrower Consent to Receive Electronic Copy Date
                </td>
                <td>
                    <ml:DateTextBox ID="sApprECopyConsentReceivedD" runat="server" CssClass="sApprECopyConsentReceivedD" preset="date" TabIndex="2"></ml:DateTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Appraisal Waiver Form Received Date
                </td>
                <td>
                    <ml:DateTextBox ID="sApprWaiverReceivedD" runat="server" preset="date" onchange="refreshCalculation();" TabIndex="3"></ml:DateTextBox>
                </td>
                <td class="PadCell">
                    &nbsp;
                </td>
                <td>
                    Appraisal Delivery Due Date
                </td>
                <td>
                    <ml:DateTextBox ID="sApprDeliveryDueD" runat="server" preset="date" TabIndex="4"></ml:DateTextBox>
                    <asp:CheckBox ID="sApprDeliveryDueDLckd" runat="server" Text="Lock" onclick="refreshCalculation();" TabIndex="5"></asp:CheckBox>
                </td>
            </tr>
            <tr>
                <td>
                    Appraisal Expiration Date
                </td>
                <td>
                    <ml:DateTextBox ID="sApprRprtExpD" runat="server" preset="date" TabIndex="6"></ml:DateTextBox>
                </td>
            </tr>
        </table>
        <div class="SubHeader">Order Information</div>

        <table id="OrderInfoForCloning" class="OrderInfoTable">
            <tr class="TopRow" >
                <td class="RemoveCell" rowspan="5">
                    <input type="button" class="RemoveBtn" value="-" TabIndex="7"/>
                    <input type="hidden" class="Index" value="-1" />
                    <input type="hidden" class="AppraisalOrderId" value=<%= AspxTools.JsString(Guid.Empty) %> />
                    <input type="hidden" class="LoadingId" />
                    <input type="hidden" class="VendorId" value=<%= AspxTools.JsString(Guid.Empty) %> />
                    <input type="hidden" class="AppraisalOrderType" value="<%= AspxTools.JsNumeric(AppraisalOrderType.Manual) %>" />
                    <input type="hidden" class="IsDirty" value="false" />
                    <input type="hidden" class="IsValid" value="true" />
                </td>
                <td class="BorderedCell">
                    <div class="CellHeader">Product Name</div>
                    <div class="CellBody">
                        <input type="text" class="ProductName" maxlength="100"  TabIndex="8"/>
                        <span class="ProductNameSpan" />
                    </div>
                </td>
                <td class="BorderedCell">
                    <div class="CellHeader">Valuation Method</div>
                    <div class="CellBody ValuationCell">
                        <select class="ValuationMethod"  TabIndex="9">
                            <option value="0"></option>
                            <option value="1">AUS Findings</option>
                            <option value="2">Appraisal</option>
                            <option value="3">Desk Review</option>
                            <option value="4">AVM</option>
                        </select>
                        <img class="ValuationMethodReqIcon" src="../../images/require_icon_red.gif">
                    </div>
                </td>
                <td class="BorderedCell">
                    <div class="CellHeader">File #</div>
                    <div class="CellBody">
                        <input type="text" class="OrderNumber" maxlength="50" TabIndex="10"/>
                        <span class="OrderNumberSpan"></span>
                        <div class="VendorName"></div>
                    </div>
                </td>
                <td class="BorderedCell StatusCell">
                    <div class="CellHeader">Status</div>
                    <div class="CellBody">
                        <span class="Status">SAMPLE TEXT</span>
                        <br />
                        <a class="ViewLink">view order</a>
                    </div>
                </td>
                <td class="BorderedCell StatusDateCell">
                    <div class="CellHeader">Status Date</div>
                    <div class="CellBody">
                        <span class="StatusDate">SAMPLE TEXT</span>
                    </div>
                </td>
                <td class="ProgressBarCell">
                    <img class="ProgressBar" src="../../images/ApprTracking_Empty.svg">
                </td>
            </tr>
            <tr class="PropertyInfoSection">
                <td class="BorderedCell" colspan="6">
                    <input type="hidden" class="AppIdAssociatedWithProperty" />
                    <input type="hidden" class="LinkedReoId" />
                    <div class="CellBody">
                        <div>
                            <label>
                                <input type="radio" class="IsSubjectProperty" disabled="disabled" />Subject Property
                            </label>
                            <label>
                                <input type="radio" class="IsReo" disabled="disabled" />REO
                            </label>
                        </div>
                        <div class="Padding5 PropertySelectorSection">
                            <span>Select Property: </span><asp:DropDownList CssClass="PropertySelector" runat="server" ID="PropertySelector"></asp:DropDownList>
                        </div>
                        <div class="Padding5 PropertyAddressSection">
                            <div>
                                <span>Property Address</span>
                            </div>
                            <div>
                                <table>
                                    <tr>
                                        <td>Street</td>
                                        <td>Zip Code</td>
                                        <td>City</td>
                                        <td>State</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="text" class="InputLarge PropertyAddress PropertyDetail" disabled="disabled" />
                                        </td>
                                        <td>
                                            <ml:ZipcodeTextBox runat="server" CssClass="PropertyZip PropertyDetail" ID="PropertyZip" Enabled="false"></ml:ZipcodeTextBox>
                                        </td>
                                        <td>
                                            <input type="text" class="PropertyCity PropertyDetail" disabled="disabled" />
                                        </td>
                                        <td>
                                            <ml:StateDropDownList runat="server" CssClass="PropertyState" ID="PropertyState" Enabled="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="BorderedCell" colspan="6">
                    <table class="InternalTable">
                        <tr>
                            <td>
                                <table class="DatesTable">
                                    <tr>
                                        <td>
                                            Ordered
                                        </td>
                                        <td>
                                            <ml:DateTextBox ID="OrderedDate" CssClass="OrderedDate" runat="server" preset="date" TabIndex="11"></ml:DateTextBox>
                                        </td>
                                        <td>
                                            Received
                                        </td>
                                        <td>
                                            <ml:DateTextBox ID="ReceivedDate" CssClass="ReceivedDate" runat="server" preset="date" TabIndex="14"></ml:DateTextBox>
                                        </td>
                                        <td>
                                            Method of Delivery
                                        </td>
                                        <td>
                                            <select class="DeliveryMethod" TabIndex="17">
                                                <option value="0"></option>
                                                <option value="1">Fax</option>
                                                <option value="2">In Person</option>
                                                <option value="3">Mail</option>
                                                <option value="4">Overnight</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Needed
                                        </td>
                                        <td>
                                            <ml:DateTextBox ID="NeededDate" CssClass="NeededDate" runat="server" preset="date" TabIndex="12"></ml:DateTextBox>
                                        </td>
                                        <td>
                                            Sent to Borrower
                                        </td>
                                        <td>
                                            <ml:DateTextBox ID="SentToBorrowerDate" CssClass="SentToBorrowerDate" runat="server" preset="date" TabIndex="15"></ml:DateTextBox>
                                        </td>
                                        <td>
                                            Borrower Received
                                        </td>
                                        <td>
                                            <ml:DateTextBox ID="BorrowerReceivedDate" CssClass="BorrowerReceivedDate" runat="server" preset="date" TabIndex="18"></ml:DateTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Expiration
                                        </td>
                                        <td>
                                            <ml:DateTextBox ID="ExpirationDate" CssClass="ExpirationDate" runat="server" preset="date" TabIndex="13"></ml:DateTextBox>
                                        </td>
                                        <td>
                                            <a id="SendToBorrowerLink" class="SendToBorrowerLink" runat="server" TabIndex="16">send to borrower</a>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Revision
                                        </td>
                                        <td>
                                            <ml:DateTextBox ID="RevisionDate" CssClass="RevisionDate" runat="server" preset="date" TabIndex="19"></ml:DateTextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table class="NotesAndDocsTable">
                                    <tr>
                                        <td>
                                            Notes
                                        </td>
                                        <td>
                                            <textarea class="Notes" rows="1" cols="50"  TabIndex="20"></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Documents
                                        </td>
                                        <td class="UploadedDocumentsCell">
                                            <div class="UploadedDocuments BorderedCell"  TabIndex="21"></div>
                                            <div>
                                                <div class="AssociateDocumentsDiv InlineBlock">
                                                    <input type="button" class="AssociateDocumentsLink" value="Associate EDocs" />
                                                    <input type="button" class="RemoveDocumentsLink" value="Remove Assocations" />
                                                </div>
                                                <div class="DeliveryAppraisalDiv InlineBlock">
                                                    <input type="button" class="DeliverAppraisalLink" value="Deliver Appraisal" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="BorderedCell" colspan="6">
                    <table class="InternalTable">
                        <tr>
                            <td>
                                Valuation Effective
                            </td>
                            <td>
                                <ml:DateTextBox ID="ValuationEffectiveDate" CssClass="ValuationEffectiveDate" runat="server" preset="date" TabIndex="22"></ml:DateTextBox>
                            </td>
                            <td>
                                FHA Doc File ID
                            </td>
                            <td>
                                <input type="text" class="FhaDocFileId" maxlength="50" TabIndex="23"/>
                            </td>
                            <td>
                                Submitted to FHA
                            </td>
                            <td>
                                <ml:DateTextBox ID="SubmittedToFhaDate" CssClass="SubmittedToFhaDate" runat="server" preset="date" TabIndex="24"></ml:DateTextBox>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Appraisal Form Type
                            </td>
                            <td>
                                <asp:DropDownList ID="AppraisalFormType" CssClass="AppraisalFormType" runat="server" TabIndex="25"/>
                            </td>

                            <td>
                                UCDP Appraisal File ID
                            </td>
                            <td>
                                <input type="text" class="UcdpAppraisalId" maxlength="50" TabIndex="26"/>
                            </td>

                            <td>
                                Submitted to UCDP
                            </td>
                            <td>
                                <ml:DateTextBox ID="SubmittedToUcdpDate" CssClass="SubmittedToUcdpDate" runat="server" preset="date" TabIndex="27"></ml:DateTextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="BorderedCell" colspan="6">
                    <table class="InternalTable">
                        <tr>
                            <td>CU Risk Score<asp:TextBox ID="CuRiskScore" CssClass="CuRiskScore" runat="server" preset="curiskscore"/></td>
                            <td>Overvaluation Risk<asp:DropDownList ID="OvervaluationRiskT" CssClass="OvervaluationRiskT" runat="server"/></td>
                            <td>Property Eligibilitiy Risk<asp:DropDownList ID="PropertyEligibilityRiskT" CssClass="PropertyEligibilityRiskT" runat="server"/></td>
                            <td>Appraisal Quality Risk<asp:DropDownList ID="AppraisalQualityRiskT" CssClass="AppraisalQualityRiskT" runat="server"/></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <div class="ButtonsDiv">
            <input type="button" id="AddBtn" class="AddBtn" value="Add" TabIndex="10000"/>
            <input type="button" class="RestoreBtn" value="Restore Deleted Records" TabIndex="10001"/>
            <input type="button" id="ViewAppraisalDeliveries" value="View Appraisal Deliveries"/>
        </div>
    </form>

    <script>
        var emptyGuid = <%= AspxTools.JsString(Guid.Empty) %>;
        var manualOrderType = <%= AspxTools.JsString(AppraisalOrderType.Manual) %>;
        var firstInit = true;
        function _init() {
            lockField(<%= AspxTools.JsGetElementById(sApprDeliveryDueDLckd) %>, 'sApprDeliveryDueD');

            if (firstInit) {
                $(".sApprECopyConsentReceivedD").change();
                populateOrders();
                SetRestoreBtn();
                firstInit = false;
            }
        }

        var oldSaveMe = window.saveMe;
        window.saveMe = function (bRefreshScreen) {
            if (validate() && SaveOrderInfo()) {
                return oldSaveMe(bRefreshScreen);
            }

            return false;
        }

        function validate() {
            if ($(".IsValid[value='true']").parents(".OrderInfoTable").not("#OrderInfoForCloning").find(".ValuationMethod option:selected[value='0']").length > 0) {
                alert("Valuation Method must be set for all orders before saving.");
                return false;
            }

            return true;
        }

        function GetAllOrderInfo() {
            var out = [];
            var orders = $(".OrderInfoTable").not("#OrderInfoForCloning");

            orders.each(function() {
                var order = $(this);

                // Don't save orders that aren't dirty.
                if (order.find(".IsDirty").val() == "false") {
                    return true; // continue
                }

                var appIdAssociatedWithProperty = order.find('.AppIdAssociatedWithProperty').val();
                var linkedReoId = order.find('.LinkedReoId').val();
                var orderObj = {
                    LoanId: ML.sLId,
                    BrokerId: ML.BrokerId,
                    AppraisalOrderId: order.find(".AppraisalOrderId").val(),
                    LoadingId: order.find(".LoadingId").val(),
                    VendorId: order.find(".VendorId").val(),
                    AppraisalOrderType: order.find(".AppraisalOrderType").val(),
                    IsValid: order.find(".IsValid").val(),
                    ProductName: order.find(".ProductName").val(),
                    ValuationMethod: order.find(".ValuationMethod").val(),
                    OrderNumber: order.find(".OrderNumber").val(),
                    OrderedDate: order.find(".OrderedDate").val(),
                    ReceivedDate: order.find(".ReceivedDate").val(),
                    DeliveryMethod: order.find(".DeliveryMethod").val(),
                    Notes: order.find(".Notes").val(),
                    NeededDate: order.find(".NeededDate").val(),
                    SentToBorrowerDate: order.find(".SentToBorrowerDate").val(),
                    BorrowerReceivedDate: order.find(".BorrowerReceivedDate").val(),
                    ExpirationDate: order.find(".ExpirationDate").val(),
                    RevisionDate: order.find(".RevisionDate").val(),
                    ValuationEffectiveDate: order.find(".ValuationEffectiveDate").val(),
                    FhaDocFileId: order.find(".FhaDocFileId").val(),
                    SubmittedToFhaDate: order.find(".SubmittedToFhaDate").val(),
                    AppraisalFormType: order.find(".AppraisalFormType").val(),
                    UcdpAppraisalId: order.find(".UcdpAppraisalId").val(),
                    SubmittedToUcdpDate: order.find(".SubmittedToUcdpDate").val(),
                    CuRiskScore: order.find(".CuRiskScore").val(),
                    OvervaluationRiskT: order.find(".OvervaluationRiskT").val(),
                    PropertyEligibilityRiskT: order.find(".PropertyEligibilityRiskT").val(),
                    AppraisalQualityRiskT: order.find(".AppraisalQualityRiskT").val(),
                    Index: order.find(".Index").val(),
                    AppIdAssociatedWithProperty: appIdAssociatedWithProperty == '' ? null : appIdAssociatedWithProperty,
                    LinkedReoId: !order.find('.IsSubjectProperty').prop('checked') && linkedReoId != '' && linkedReoId != emptyGuid ? linkedReoId : null,
                    PropertyAddress: order.find('.PropertyAddress').val(),
                    PropertyCity: order.find('.PropertyCity').val(),
                    PropertyState: order.find('.PropertyState').val(),
                    PropertyZip: order.find('.PropertyZip').val()
                }

                out.push(orderObj);
            });

            return out;
        }

        function SaveOrderInfo() {
            orders = GetAllOrderInfo();

            // Don't save if there is nothing to save.
            if (orders.length == 0) {
                return true;
            }

            var args = {
                Orders: JSON.stringify(orders)
            };

            var result = gService.loanedit.call("SaveOrderInfo", args);
            if (result.error) {
                alert("Failed to save order info:\n" + result.UserMessage);
                return;
            }

            // Set IDs for new orders.
            indexToIds = $.parseJSON(result.value["indexToIds"]);
            for (var i=0; i < orders.length; i++)
            {
                var order = orders[i];
                if(indexToIds[order.Index] != undefined) {
                    var isManual = order.AppraisalOrderType == manualOrderType;
                    var isNew = order.AppraisalOrderId == <%= AspxTools.JsString(Guid.Empty) %>;
                    var orderRow = $(".Index[value='" + order.Index + "']").parents(".OrderInfoTable");
                    var showDeliveryAppraisal = true;
                    if (isManual && isNew) {
                        orderRow.find(".AppraisalOrderId").val(indexToIds[order.Index].Key);
                        orderRow.find(".LoadingId").val(indexToIds[order.Index].Value);
                        orderRow.find('.AssociateDocumentsDiv').show();
                        showDeliveryAppraisal = false;
                    }
                    
                    var loadingId = indexToIds[order.Index].Value;
                    orderRow.find('.DeliverAppraisalLink').toggle(showDeliveryAppraisal && typeof(loadingId) === 'string' && loadingId != '');
                }
            }

            // Clear individual dirty bits.
            $(".IsDirty").val("false")

            return true;
        }

        function updateDirtyBit_callback(event) {
            var src = retrieveEventTarget(event);
            var orderInfo = $(src).parents(".OrderInfoTable");

            if (orderInfo.length > 0)
            {
                orderInfo.find(".IsDirty").val("true");
            }
        }

        $(document).on('change', ".sApprECopyConsentReceivedD", function () {
            if ($j(this).val() == "") {
                $(".DeliveryMethod option[value='5']").remove();
            } else {
                if ($(".DeliveryMethod option[value='5']").length == 0) {
                    $(".DeliveryMethod").append($('<option>', {
                        value: 5,
                        text: 'Email'
                    }));
                }
            }
        });

        $("#AddBtn").click(function () {
            var newOrder = AddBlankOrderInfo();
            var numOrders = $(".OrderInfoTable").not("#OrderInfoForCloning").length;
            InitializeOrderInfo(newOrder, null);
            
            $(newOrder).find('.IsSubjectProperty').prop('checked', true);
            SetSpReoPickerGrouping(newOrder, numOrders + 1);
            
            $(newOrder).find(".IsDirty").val("true");
            $(newOrder).find(".AssociateDocumentsDiv").hide();
            $(newOrder).find('.IsSubjectProperty').prop('checked', true);
            updateDirtyBit();
        });
        
        $(document).on('change', '.PropertySelector', function() {
            PropertySelectorChange($(this));
        });

        function PropertySelectorChange(propertySelector) {
            var propertySection = propertySelector.closest('.PropertyInfoSection');
            var selectedProperty = propertySelector.val();
            propertySection.find('.LinkedReoId').val(selectedProperty);
            
            var propertyDetails = PropertyInfoDetails[selectedProperty];
            if(propertyDetails) {
                propertySection.find('.AppIdAssociatedWithProperty').val(propertyDetails.AppId);
                propertySection.find('.PropertyAddress').val(propertyDetails.Address);
                propertySection.find('.PropertyCity').val(propertyDetails.City);
                propertySection.find('.PropertyState').val(propertyDetails.State);
                propertySection.find('.PropertyZip').val(propertyDetails.Zip);
            }
            else {
                propertySection.find('.PropertyDetail').val('');
            }
        }

        function populateOrders()
        {
            var orders;
            var args = {
                LoanId: ML.sLId,
                BrokerId: ML.BrokerId
            };

            var result = gService.loanedit.call("GetExistingOrders", args);
            if (!result.error) {

                orders = $.parseJSON(result.value["orders"]);
            }

            for (var i=0; i < orders.length; i++) {
                // Add new blank row.
                var newOrder = AddBlankOrderInfo();

                // Populate values.
                var order = orders[i]
                newOrder.find(".LoadingId").val(order.LoadingId);
                newOrder.find(".AppraisalOrderId").val(order.AppraisalOrderId);
                newOrder.find(".VendorId").val(order.VendorId);
                newOrder.find(".AppraisalOrderType").val(order.AppraisalOrderType);
                newOrder.find(".IsValid").val(order.IsValid);
                newOrder.find(".ProductName").val(order.ProductName);
                newOrder.find(".ProductNameSpan").text(order.ProductName);
                newOrder.find(".ValuationMethod").val(order.ValuationMethod);
                newOrder.find(".OrderNumber").val(order.OrderNumber);
                newOrder.find(".OrderNumberSpan").text(order.OrderNumber);
                newOrder.find(".VendorName").text(order.VendorName);
                newOrder.find(".Status").text(order.Status);
                newOrder.find(".StatusDate").text(order.StatusDate);
                newOrder.find(".OrderedDate").val(order.OrderedDate);
                newOrder.find(".ReceivedDate").val(order.ReceivedDate);
                newOrder.find(".DeliveryMethod").val(order.DeliveryMethod);
                newOrder.find(".Notes").val(order.Notes);
                newOrder.find(".NeededDate").val(order.NeededDate);
                newOrder.find(".SentToBorrowerDate").val(order.SentToBorrowerDate);
                newOrder.find(".BorrowerReceivedDate").val(order.BorrowerReceivedDate);
                newOrder.find(".ExpirationDate").val(order.ExpirationDate);
                newOrder.find(".RevisionDate").val(order.RevisionDate);
                newOrder.find(".ValuationEffectiveDate").val(order.ValuationEffectiveDate);
                newOrder.find(".FhaDocFileId").val(order.FhaDocFileId);
                newOrder.find(".SubmittedToFhaDate").val(order.SubmittedToFhaDate);
                newOrder.find(".AppraisalFormType").val(order.AppraisalFormType);
                newOrder.find(".UcdpAppraisalId").val(order.UcdpAppraisalId);
                newOrder.find(".SubmittedToUcdpDate").val(order.SubmittedToUcdpDate);
                newOrder.find(".CuRiskScore").val(order.CuRiskScore);
                newOrder.find(".OvervaluationRiskT").val(order.OvervaluationRiskT);
                newOrder.find(".PropertyEligibilityRiskT").val(order.PropertyEligibilityRiskT);
                newOrder.find(".AppraisalQualityRiskT").val(order.AppraisalQualityRiskT);
                
                newOrder.find('.LinkedReoId').val(order.LinkedReoId);
                newOrder.find('.AppIdAssociatedWithProperty').val(order.AppIdAssociatedWithProperty);
                newOrder.find(".IsSubjectProperty").prop('checked', !order.HasLinkedReo);
                newOrder.find(".IsReo").prop('checked', order.HasLinkedReo);
                newOrder.find(".PropertyAddress").val(order.PropertyAddress);
                newOrder.find(".PropertyCity").val(order.PropertyCity);
                newOrder.find(".PropertyState").val(order.PropertyState);
                newOrder.find(".PropertyZip").val(order.PropertyZip);
                newOrder.find('.PropertySelector').val(order.LinkedReoId ? order.LinkedReoId : '');
                SetSpReoPickerGrouping(newOrder, i);

                if (order.ViewOrderUrl == "ERROR")
                {
                    newOrder.find(".ViewLink").hide();
                } else {
                    newOrder.find(".ViewLink").on("click", function () {
                        LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(order.ViewOrderUrl);
                    });
                }

                // Populate docs.
                PopulateOrderDocuments(newOrder, order.UploadedDocuments);

                // Populate Restore table.
                if (order.IsValid == false)
                {
                    AddRestoreTableRow(
                        newOrder.find(".Index").val(),
                        order.ProductName,
                        newOrder.find(".ValuationMethod option[value='" + order.ValuationMethod + "']" ).text(),
                        order.OrderNumber,
                        order.DeletedByUserName ? order.DeletedByUserName + " on " + order.DeletedDate : ""
                    );
                }

                InitializeOrderInfo(newOrder, order);
                newOrder.find(".IsDirty").val("false");
            }
        }

        function SetSpReoPickerGrouping(order, index) {
            var name = 'SpReoPicker_' + index;
            order.find('.IsSubjectProperty').prop('name', name);
            order.find('.IsReo').prop('name', name);

            if(order.find(".AppraisalOrderType").val() == manualOrderType) {
                order.find("[name^='SpReoPicker']").change(function() {
                    var isReo = order.find('.IsReo').prop('checked')
                    order.find('.PropertySelector').prop('disabled', !isReo).change();
                    order.find('.PropertySelectorSection').toggle(isReo);
                    if(!isReo) {
                        var subjectPropertyDetails = PropertyInfoDetails[emptyGuid];
                        if(subjectPropertyDetails) {
                            order.find('.PropertyAddress').val(subjectPropertyDetails.Address);
                            order.find('.PropertyCity').val(subjectPropertyDetails.City);
                            order.find('.PropertyState').val(subjectPropertyDetails.State);
                            order.find('.PropertyZip').val(subjectPropertyDetails.Zip);
                        }
                    }
                }).change();
            }
        }

        function PopulateOrderDocuments(order, documents, shouldClear) {
            var documentsList = order.find(".UploadedDocuments");
            if(shouldClear) {
                documentsList.empty();
            }

            for (var j=0; j < documents.length; j++) {
                var docClone = $("#DocLinkTemplate").first().clone();
                docClone.removeAttr("id");   // Clear id from clone.

                var docLink = docClone.find(".UploadedDocLink");

                var doc = documents[j];
                docLink.text(doc.FileName);
                docLink.attr("href", doc.ViewUrl);
                if (doc.EdocMetaData) {
                    docLink.data("docmetadata", JSON.parse(doc.EdocMetaData));
                    docLink.docmetadatatooltip();
                }
                else {
                    docLink.prop("title", doc.Title);
                }

                if (order.find(".AppraisalOrderType").val() == <%= AspxTools.JsString(AppraisalOrderType.Gdms) %>)
                {
                    docLink.attr("target","_blank");
                }

                docLink.css("display", "block");
                documentsList.append(docLink);
            }
        }

        function AddRestoreTableRow(index, productName, valuation, orderNumber, deletedBy)
        {
            var restoreClone = $("#RestoreRowTemplate").first().clone();
            restoreClone.removeAttr("id");   // Clear id from clone.

            restoreClone.find(".RestoreIndex").val(index);
            restoreClone.find(".RestoreProductName").text(productName);
            restoreClone.find(".RestoreValuation").text(valuation);
            restoreClone.find(".RestoreOrderNumber").text(orderNumber);
            restoreClone.find(".RestoreDeletedBy").text(deletedBy);

            restoreClone.css("display", "table-row");
            $(".RestoreTable tbody").append(restoreClone);
        }

        function AddBlankOrderInfo() {
            var index = $(".OrderInfoTable").not("#OrderInfoForCloning").length;
            var clone = $("#OrderInfoForCloning").first().clone();
            clone.removeAttr("id");   // Clear id from clone.
            clone.find(".CuRiskScore,.OvervaluationRiskT,.PropertyEligibilityRiskT,.AppraisalQualityRiskT").removeAttr("id");
            clone.find(".Index").val(index);    // Set index (used for restore feature)

            // Fix calendar control logic.
            cloneCalendar(clone, ".OrderedDate", index);
            cloneCalendar(clone, ".ReceivedDate", index);
            cloneCalendar(clone, ".NeededDate", index);
            cloneCalendar(clone, ".SentToBorrowerDate", index);
            cloneCalendar(clone, ".BorrowerReceivedDate", index);
            cloneCalendar(clone, ".ExpirationDate", index);
            cloneCalendar(clone, ".RevisionDate", index);
            cloneCalendar(clone, ".ValuationEffectiveDate", index);
            cloneCalendar(clone, ".SubmittedToFhaDate", index);
            cloneCalendar(clone, ".SubmittedToUcdpDate", index);

            // Wire up inputs.
            clone.find(':input').not("[preset='date']").each(function () {
                $this = $(this);
                $this.removeAttr(gInputInitializedAttr);
                $this.find('option').filter(':selected').removeAttr('selected');
                $this.find('option').first().attr('selected', 'selected');
                $this.not("[id$='ManualID']").prop('checked', false);
                _initDynamicInput(this);
            });

            clone.css("display", "block");
            clone.insertAfter(".OrderInfoTable:last");

            // Reset tab indexes.
            clone.find("[tabindex]").filter("[tabindex!='-1']").each(function()
            {
                var $this = $(this);
                var tabIndex = $this.attr('tabindex')
                $this.attr('tabindex', index * 21 + tabIndex);
            });

            return clone;
        }

        function cloneCalendar(clone, calendarSelector, index) {
            var createdCalendar = $(clone).find(calendarSelector);
            var newID = createdCalendar.prop('id') + index;
            createdCalendar.prop('id', newID);

            createdCalendar.siblings('a').attr('onclick', '');

            // Rebind handlers. I hope that's all of them.
            createdCalendar.siblings('a').bind('click', function () {
                return displayCalendar(newID);
            });

            createdCalendar.removeAttr(gInputInitializedAttr);
            _initMask(createdCalendar[0]);
            _initDynamicInput(createdCalendar[0]);

            createdCalendar.change(function() {
                $(this).parents(".OrderInfoTable").find(".IsDirty").val("true");
            });
        }

        function InitializeOrderInfo(orderInfo, orderProperties)
        {
            // Type specific UI changes.
            if (orderInfo.find(".AppraisalOrderType").val() == manualOrderType) {
                orderInfo.find(".StatusCell").css('visibility','hidden').css("border","none");
                orderInfo.find(".StatusDateCell").css('visibility','hidden').css("border","none");

                orderInfo.find(".ProductNameSpan").hide();
                orderInfo.find(".OrderNumberSpan").hide();
                orderInfo.find(".VendorName").hide();
                orderInfo.find(".AssociateDocumentsDiv").show();
                orderInfo.find(".IsSubjectProperty").prop('disabled', false);
                orderInfo.find('.IsReo').prop('disabled', false);
            } else {
                orderInfo.find(".RemoveCell").css('visibility','hidden').css("border","none")                

                orderInfo.find(".ProductName").hide();
                orderInfo.find(".OrderNumber").hide();
                
                orderInfo.find(".OrderedDate").prop('readonly', true);
                orderInfo.find(".NeededDate").prop('readonly', true);
                orderInfo.find(".AssociateDocumentsDiv").hide();
                orderInfo.find(".IsSubjectProperty").prop('disabled', true);
                orderInfo.find('.IsReo').prop('disabled', true);
                orderInfo.find('.PropertySelectorSection').hide();
            }

            // Disable send to borrower link
            orderInfo.find(".ReceivedDate").change();
            
            // Hide invalid orders.
            if (orderInfo.find(".IsValid").val() == "false") {
                orderInfo.hide();
            }

            // Set valuation method required icon.
            orderInfo.find(".ValuationMethod").change();

            // Set progress bar.
            orderInfo.find(".OrderedDate").change();

            ToggleDeliverAppraisalLink(orderInfo, orderProperties);
        }

        function ToggleDeliverAppraisalLink(orderInfo, orderProperties)
        {
            var loadingId = orderInfo.find('.LoadingId').val();
            var hasDocs = orderProperties != null && orderProperties.UploadedDocuments.length > 0;
            orderInfo.find('.DeliverAppraisalLink').toggle(ML.CanDeliverAppraisals && typeof(loadingId) === 'string' && loadingId != '' && hasDocs);
        }
        $(document).on("click", ".RemoveBtn", function () {
            var $this = $(this);
            var orderInfo = $this.parents(".OrderInfoTable");

            $('#dialog-confirm').dialog({
                modal: true,
                buttons: {
                    "Yes": function() {
                        $(this).dialog("close");
                        orderInfo.find(".IsValid").val("false")
                        orderInfo.find(".IsDirty").val("true")
                        orderInfo.hide();
                        updateDirtyBit();

                        // Don't add new blank orders to restore table.
                        if ( orderInfo.find(".AppraisalOrderId").val() != <%= AspxTools.JsString(Guid.Empty) %>
                            || orderInfo.find("input[type='text']").filter(function () { return $.trim($(this).val()).length > 0 }).length > 0
                            || orderInfo.find("select").filter(function () { return $(this).find(":selected").val() != 0 }).length > 0) {

                            AddRestoreTableRow(
                                orderInfo.find(".Index").val(),
                                orderInfo.find(".ProductName").val(),
                                orderInfo.find(".ValuationMethod option:selected" ).text(),
                                orderInfo.find(".OrderNumber").val(),
                                <%= AspxTools.JsString(BrokerUser.DisplayName + " on " + DateTime.Now.ToShortDateString()) %>
                            );

                            SetRestoreBtn();
                        }
                    },
                    "No": function() {
                        $(this).dialog("close");
                    }
                },
                closeOnEscape: false,
                width: "350",
                draggable: false,
                dialogClass: "LQBDialogBox",
                resizable: false
            });
        });

        $('#ViewAppraisalDeliveries').click(function() {
            var lqbPopupSettings = {
                'width': 500,
                'height': 400,
                'hideCloseButton': true
            };
            
            LQBPopup.Show(ML.VirtualRoot + '/newlos/Status/ViewAppraisalDeliveries.aspx?loanid=' + ML.sLId, lqbPopupSettings);
        });

        $(document).on('click', '.DeliverAppraisalLink', function() {
            var orderRow = $(this).closest(".OrderInfoTable");
            var appraisalId = orderRow.find(".AppraisalOrderId").val();

            var lqbPopupSettings = {
                'width': 1000,
                'height': 600,
                'hideCloseButton': true
            };
            var appraisalType = orderRow.find(".AppraisalOrderType").val();
            var loadingId = orderRow.find(".LoadingId").val();
            LQBPopup.Show(ML.VirtualRoot + '/newlos/Status/DeliverAppraisal.aspx?loanid=' + ML.sLId +"&aot="+encodeURIComponent(appraisalType)+"&aid="+encodeURIComponent(loadingId), lqbPopupSettings);
        });

        $(document).on('click', '.AssociateDocumentsLink', function() {
            var orderRow = $(this).closest(".OrderInfoTable");
            var appraisalId = orderRow.find(".AppraisalOrderId").val();
            var lqbPopupSettings = {
                'width': 700,
                'height': 500,
                'hideCloseButton': true,
                'onReturn': function(result) {
                    if(result.DocIds) {
                        var data = {
                            "AppraisalOrderId": appraisalId,
                            "EDocsToAssociate": JSON.stringify(result.DocIds)
                        };

                        gService.loanedit.callAsyncSimple("AssocateEdocsToManualOrder", data, 
                        function (results) {
                            if(results.value.Error) {
                                alert(results.value.Error);
                            }
                            else {
                                var parsedOrder = JSON.parse(results.value.manualOrder);
                                PopulateOrderDocuments(orderRow, parsedOrder.UploadedDocuments, true);
                                ToggleDeliverAppraisalLink(orderRow, parsedOrder);
                            }
                        },
                        function (results) {
                            alert(results.UserMessage || "Unable to associate edocs");
                        });
                    }
                },
            };

            LQBPopup.Show(ML.VirtualRoot + '/newlos/Services/OrderAppraisalDocumentPicker.aspx?loanid=' + ML.sLId, lqbPopupSettings);
        });

         $(document).on('click','.RemoveDocumentsLink', function() {
            var orderRow = $(this).closest(".OrderInfoTable");
            var appraisalId = orderRow.find(".AppraisalOrderId").val();
            var orderNum = orderRow.find('.OrderNumber').val();

            if(confirm("Delete edoc associations for File # [" + orderNum + "]?"))
            {
                var data = {
                    "AppraisalOrderId": appraisalId,
                };

                gService.loanedit.callAsyncSimple("DeleteEdocsForManualOrder", data, 
                function (results) {
                    if(results.value.Error) {
                        alert(results.value.Error);
                    }
                    else {
                        var parsedOrder = JSON.parse(results.value.manualOrder);
                        PopulateOrderDocuments(orderRow, parsedOrder.UploadedDocuments, true);
                        ToggleDeliverAppraisalLink(orderRow, parsedOrder);
                    }
                }, 
                function (results) {
                    alert(results.UserMessage || "Unable to remove edoc assocations");
                });
            }
        });

        $(".RestoreBtn").click(function () {
            $('#dialog-restore').dialog({
                modal: true,
                buttons: {
                    "Restore selected records": function() {
                        $(this).dialog("close");
                        
                        var checkedBoxes = $(".RestoreBox:checked");

                        if (checkedBoxes.length == 0) {
                            return;
                        }

                        checkedBoxes.each(function () {
                            var row = $(this).parents(".RestoreRow");
                            var index = row.find(".RestoreIndex").val();

                            var orderInfo = $(".Index[value='" + index + "']").parents(".OrderInfoTable");
                            orderInfo.find(".IsValid").val("true");
                            orderInfo.find(".IsDirty").val("true");
                            orderInfo.show();

                            row.remove();
                        });

                        SetRestoreBtn();
                        updateDirtyBit();
                    },
                    "Cancel": function() {
                        $(this).dialog("close");
                        $(".RestoreBox:checked").removeAttr("checked");
                    }
                },
                closeOnEscape: false,
                width: "550",
                draggable: false,
                dialogClass: "LQBDialogBox",
                resizable: false
            });

            SetDialogRestoreBtn();
        });
        function SetDialogRestoreBtn() {
            if ($(".RestoreBox:checked").length == 0)
            {
                $("#dialog-restore").parents(".ui-dialog").find("span:contains('Restore')").parent().prop("disabled", true).addClass('ui-state-disabled');
            }
            else
            {
                $("#dialog-restore").parents(".ui-dialog").find("span:contains('Restore')").parent().prop("disabled", false).removeClass('ui-state-disabled');
            }
        }

        $(document).on("click", ".RestoreBox", SetDialogRestoreBtn);

        function SetRestoreBtn()
        {
            var invalidOrders = $(".IsValid[value='false']").parents(".OrderInfoTable").not("#OrderInfoForCloning");
            $(".RestoreBtn").prop('disabled', invalidOrders.length == 0);
        }

        function IsDate(value) {
            // Try mm?/dd?/yy(yy)?(offset)?
            var dateGroups = value.match(/^(\d{1,2})[-/.](\d{1,2})[-/.](\d{4}|\d{2})(?:([-+]\d+)([dbwmy]?))?$/); 
            if (dateGroups === null) {
                // Try mmddyy(yy)?(offset)?
                dateGroups = value.match(/^(\d{2})(\d{2})(\d{4}|\d{2})(?:([-+]\d+)([dbwmy]?))?$/);
            }
            if (dateGroups === null) {
                // Try dd?(offset)? or mm?/dd?(offset)?
                // Note that ( *?) is a hack to produce an always empty group 3.
                dateGroups = value.match(/^(\d{1,2})(?:[-/.](\d{1,2}))?( *?)(?:([-+]\d+)([dbwmy]?))?$/);
                if (dateGroups != null) {
                    missingYear = true;
                    // If no day, assume 1.
                    if (dateGroups[2] === "" || typeof(dateGroups[2]) === 'undefined') {
                        dateGroups[2] = 1;
                    }
                    // Set the year to be the same as the start of the date window
                    dateGroups[3] = windowStartDate.getFullYear();
                }
            }
            if (dateGroups === null) {
                return false;
            }

            return true;
        }

        $(document).on("change", ".OrderedDate,.ReceivedDate,.SentToBorrowerDate", function (){
            var $this = $(this);
            var orderInfo = $this.parents(".OrderInfoTable");
            var orderedDateSet = IsDate(orderInfo.find(".OrderedDate").val());
            var receivedDateSet = IsDate(orderInfo.find(".ReceivedDate").val());
            var sentToBorrowerDateSet = IsDate(orderInfo.find(".SentToBorrowerDate").val());
            var progressImg = orderInfo.find(".ProgressBar");

            if (orderedDateSet != "" && receivedDateSet != "" && sentToBorrowerDateSet !="")
            {
                progressImg.attr("src", "../../images/ApprTracking_Sent.svg");
            }
            else if (orderedDateSet != "" && receivedDateSet != "")
            {
                progressImg.attr("src", "../../images/ApprTracking_Completed.svg");
            }
            else if (orderedDateSet != "")
            {
                progressImg.attr("src", "../../images/ApprTracking_Ordered.svg");
            }
            else
            {
                progressImg.attr("src", "../../images/ApprTracking_Empty.svg");
            }
        });

        $(document).on("change", ".ReceivedDate", function () {
            var $this = $(this);
            var orderInfo = $this.parents(".OrderInfoTable");
            var sendLink = orderInfo.find(".SendToBorrowerLink");
            var isDisabled =  !ML.CanViewEDocs || $this.val() == "";
            setDisabledAttr(sendLink, isDisabled);
            sendLink.toggleClass("DisabledLink", isDisabled);
        });

        $(document).on("change", ".ValuationMethod", function () {
            var $this = $(this);
            var orderInfo = $this.parents(".OrderInfoTable");
            var reqIcon = orderInfo.find(".ValuationMethodReqIcon");

            if ($this.val() == "0")
            {
                reqIcon.attr("src", "../../images/require_icon_red.gif");
            } else {
                reqIcon.attr("src", "../../images/require_icon.gif");
            }
        });

        $(document).on("click", ".SendToBorrowerLink" , function () {
            f_load(VRoot + "/newlos/ElectronicDocs/ElectronicDocs.aspx?pg=0&loanid=" + ML.sLId);
        });

        function doAfterDateFormat(e)
        {
            $(e).change();
        }

        function openDoc(docid) {
            window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + docid, 'pdfdoc');
        }

        function openFileDbDoc(fileDbKey) {
            __doPostBack("ViewDoc", fileDbKey);
        }
    </script>
</body>
</html>
