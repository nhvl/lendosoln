///
/// Author: Matthew Flynn
/// 
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MeridianLink.CommonControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Status
{
	public partial class CustomFields2 : LendersOfficeApp.newlos.BaseLoanPage
	{

        protected void PageLoad(object sender, System.EventArgs e)
        {

        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "Custom Fields 21-40";
            this.PageID = "StatusCustomFields2";
            // this.PDFPrintClass = typeof(LendersOffice.Pdf._____);
        }

		protected override void LoadData()
		{
			CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(CustomFields2));
			dataLoan.InitLoad();

			sCustomField21Desc.Text = dataLoan.sCustomField21Desc;
			sCustomField22Desc.Text = dataLoan.sCustomField22Desc;
			sCustomField23Desc.Text = dataLoan.sCustomField23Desc;
			sCustomField24Desc.Text = dataLoan.sCustomField24Desc;
			sCustomField25Desc.Text = dataLoan.sCustomField25Desc;
			sCustomField26Desc.Text = dataLoan.sCustomField26Desc;
			sCustomField27Desc.Text = dataLoan.sCustomField27Desc;
			sCustomField28Desc.Text = dataLoan.sCustomField28Desc;
			sCustomField29Desc.Text = dataLoan.sCustomField29Desc;
			sCustomField30Desc.Text = dataLoan.sCustomField30Desc;
			sCustomField31Desc.Text = dataLoan.sCustomField31Desc;
			sCustomField32Desc.Text = dataLoan.sCustomField32Desc;
			sCustomField33Desc.Text = dataLoan.sCustomField33Desc;
			sCustomField34Desc.Text = dataLoan.sCustomField34Desc;
			sCustomField35Desc.Text = dataLoan.sCustomField35Desc;
			sCustomField36Desc.Text = dataLoan.sCustomField36Desc;
			sCustomField37Desc.Text = dataLoan.sCustomField37Desc;
			sCustomField38Desc.Text = dataLoan.sCustomField38Desc;
			sCustomField39Desc.Text = dataLoan.sCustomField39Desc;
			sCustomField40Desc.Text = dataLoan.sCustomField40Desc;



            UpdateTextBox("sCustomField21Desc", sCustomField21Desc, dataLoan);
            UpdateTextBox("sCustomField22Desc", sCustomField22Desc, dataLoan);
            UpdateTextBox("sCustomField23Desc", sCustomField23Desc, dataLoan);
            UpdateTextBox("sCustomField24Desc", sCustomField24Desc, dataLoan);
            UpdateTextBox("sCustomField25Desc", sCustomField25Desc, dataLoan);
            UpdateTextBox("sCustomField26Desc", sCustomField26Desc, dataLoan);
            UpdateTextBox("sCustomField27Desc", sCustomField27Desc, dataLoan);
            UpdateTextBox("sCustomField28Desc", sCustomField28Desc, dataLoan);
            UpdateTextBox("sCustomField29Desc", sCustomField29Desc, dataLoan);
            UpdateTextBox("sCustomField30Desc", sCustomField30Desc, dataLoan);
            UpdateTextBox("sCustomField31Desc", sCustomField31Desc, dataLoan);
            UpdateTextBox("sCustomField32Desc", sCustomField32Desc, dataLoan);
            UpdateTextBox("sCustomField33Desc", sCustomField33Desc, dataLoan);
            UpdateTextBox("sCustomField34Desc", sCustomField34Desc, dataLoan);
            UpdateTextBox("sCustomField35Desc", sCustomField35Desc, dataLoan);
            UpdateTextBox("sCustomField36Desc", sCustomField36Desc, dataLoan);
            UpdateTextBox("sCustomField37Desc", sCustomField37Desc, dataLoan);
            UpdateTextBox("sCustomField38Desc", sCustomField38Desc, dataLoan);
            UpdateTextBox("sCustomField39Desc", sCustomField39Desc, dataLoan);
            UpdateTextBox("sCustomField40Desc", sCustomField40Desc, dataLoan);
            
    
			sCustomField21D.Text = dataLoan.sCustomField21D_rep;
			sCustomField22D.Text = dataLoan.sCustomField22D_rep;
			sCustomField23D.Text = dataLoan.sCustomField23D_rep;
			sCustomField24D.Text = dataLoan.sCustomField24D_rep;
			sCustomField25D.Text = dataLoan.sCustomField25D_rep;
			sCustomField26D.Text = dataLoan.sCustomField26D_rep;
			sCustomField27D.Text = dataLoan.sCustomField27D_rep;
			sCustomField28D.Text = dataLoan.sCustomField28D_rep;
			sCustomField29D.Text = dataLoan.sCustomField29D_rep;
			sCustomField30D.Text = dataLoan.sCustomField30D_rep;
			sCustomField31D.Text = dataLoan.sCustomField31D_rep;
			sCustomField32D.Text = dataLoan.sCustomField32D_rep;
			sCustomField33D.Text = dataLoan.sCustomField33D_rep;
			sCustomField34D.Text = dataLoan.sCustomField34D_rep;
			sCustomField35D.Text = dataLoan.sCustomField35D_rep;
			sCustomField36D.Text = dataLoan.sCustomField36D_rep;
			sCustomField37D.Text = dataLoan.sCustomField37D_rep;
			sCustomField38D.Text = dataLoan.sCustomField38D_rep;
			sCustomField39D.Text = dataLoan.sCustomField39D_rep;
			sCustomField40D.Text = dataLoan.sCustomField40D_rep;
			sCustomField21Money.Text = dataLoan.sCustomField21Money_rep;
			sCustomField22Money.Text = dataLoan.sCustomField22Money_rep;
			sCustomField23Money.Text = dataLoan.sCustomField23Money_rep;
			sCustomField24Money.Text = dataLoan.sCustomField24Money_rep;
			sCustomField25Money.Text = dataLoan.sCustomField25Money_rep;
			sCustomField26Money.Text = dataLoan.sCustomField26Money_rep;
			sCustomField27Money.Text = dataLoan.sCustomField27Money_rep;
			sCustomField28Money.Text = dataLoan.sCustomField28Money_rep;
			sCustomField29Money.Text = dataLoan.sCustomField29Money_rep;
			sCustomField30Money.Text = dataLoan.sCustomField30Money_rep;
			sCustomField31Money.Text = dataLoan.sCustomField31Money_rep;
			sCustomField32Money.Text = dataLoan.sCustomField32Money_rep;
			sCustomField33Money.Text = dataLoan.sCustomField33Money_rep;
			sCustomField34Money.Text = dataLoan.sCustomField34Money_rep;
			sCustomField35Money.Text = dataLoan.sCustomField35Money_rep;
			sCustomField36Money.Text = dataLoan.sCustomField36Money_rep;
			sCustomField37Money.Text = dataLoan.sCustomField37Money_rep;
			sCustomField38Money.Text = dataLoan.sCustomField38Money_rep;
			sCustomField39Money.Text = dataLoan.sCustomField39Money_rep;
			sCustomField40Money.Text = dataLoan.sCustomField40Money_rep;
			sCustomField21Pc.Text = dataLoan.sCustomField21Pc_rep;
			sCustomField22Pc.Text = dataLoan.sCustomField22Pc_rep;
			sCustomField23Pc.Text = dataLoan.sCustomField23Pc_rep;
			sCustomField24Pc.Text = dataLoan.sCustomField24Pc_rep;
			sCustomField25Pc.Text = dataLoan.sCustomField25Pc_rep;
			sCustomField26Pc.Text = dataLoan.sCustomField26Pc_rep;
			sCustomField27Pc.Text = dataLoan.sCustomField27Pc_rep;
			sCustomField28Pc.Text = dataLoan.sCustomField28Pc_rep;
			sCustomField29Pc.Text = dataLoan.sCustomField29Pc_rep;
			sCustomField30Pc.Text = dataLoan.sCustomField30Pc_rep;
			sCustomField31Pc.Text = dataLoan.sCustomField31Pc_rep;
			sCustomField32Pc.Text = dataLoan.sCustomField32Pc_rep;
			sCustomField33Pc.Text = dataLoan.sCustomField33Pc_rep;
			sCustomField34Pc.Text = dataLoan.sCustomField34Pc_rep;
			sCustomField35Pc.Text = dataLoan.sCustomField35Pc_rep;
			sCustomField36Pc.Text = dataLoan.sCustomField36Pc_rep;
			sCustomField37Pc.Text = dataLoan.sCustomField37Pc_rep;
			sCustomField38Pc.Text = dataLoan.sCustomField38Pc_rep;
			sCustomField39Pc.Text = dataLoan.sCustomField39Pc_rep;
			sCustomField40Pc.Text = dataLoan.sCustomField40Pc_rep;
			sCustomField21Bit.Checked = dataLoan.sCustomField21Bit;
			sCustomField22Bit.Checked = dataLoan.sCustomField22Bit;
			sCustomField23Bit.Checked = dataLoan.sCustomField23Bit;
			sCustomField24Bit.Checked = dataLoan.sCustomField24Bit;
			sCustomField25Bit.Checked = dataLoan.sCustomField25Bit;
			sCustomField26Bit.Checked = dataLoan.sCustomField26Bit;
			sCustomField27Bit.Checked = dataLoan.sCustomField27Bit;
			sCustomField28Bit.Checked = dataLoan.sCustomField28Bit;
			sCustomField29Bit.Checked = dataLoan.sCustomField29Bit;
			sCustomField30Bit.Checked = dataLoan.sCustomField30Bit;
			sCustomField31Bit.Checked = dataLoan.sCustomField31Bit;
			sCustomField32Bit.Checked = dataLoan.sCustomField32Bit;
			sCustomField33Bit.Checked = dataLoan.sCustomField33Bit;
			sCustomField34Bit.Checked = dataLoan.sCustomField34Bit;
			sCustomField35Bit.Checked = dataLoan.sCustomField35Bit;
			sCustomField36Bit.Checked = dataLoan.sCustomField36Bit;
			sCustomField37Bit.Checked = dataLoan.sCustomField37Bit;
			sCustomField38Bit.Checked = dataLoan.sCustomField38Bit;
			sCustomField39Bit.Checked = dataLoan.sCustomField39Bit;
			sCustomField40Bit.Checked = dataLoan.sCustomField40Bit;
			sCustomField21Notes.Text = dataLoan.sCustomField21Notes;
			sCustomField22Notes.Text = dataLoan.sCustomField22Notes;
			sCustomField23Notes.Text = dataLoan.sCustomField23Notes;
			sCustomField24Notes.Text = dataLoan.sCustomField24Notes;
			sCustomField25Notes.Text = dataLoan.sCustomField25Notes;
			sCustomField26Notes.Text = dataLoan.sCustomField26Notes;
			sCustomField27Notes.Text = dataLoan.sCustomField27Notes;
			sCustomField28Notes.Text = dataLoan.sCustomField28Notes;
			sCustomField29Notes.Text = dataLoan.sCustomField29Notes;
			sCustomField30Notes.Text = dataLoan.sCustomField30Notes;
			sCustomField31Notes.Text = dataLoan.sCustomField31Notes;
			sCustomField32Notes.Text = dataLoan.sCustomField32Notes;
			sCustomField33Notes.Text = dataLoan.sCustomField33Notes;
			sCustomField34Notes.Text = dataLoan.sCustomField34Notes;
			sCustomField35Notes.Text = dataLoan.sCustomField35Notes;
			sCustomField36Notes.Text = dataLoan.sCustomField36Notes;
			sCustomField37Notes.Text = dataLoan.sCustomField37Notes;
			sCustomField38Notes.Text = dataLoan.sCustomField38Notes;
			sCustomField39Notes.Text = dataLoan.sCustomField39Notes;
			sCustomField40Notes.Text = dataLoan.sCustomField40Notes;
            

		}

        private void UpdateTextBox(string id, TextBox tb, CPageData dataLoan)
        {
            if (dataLoan.IsCustomFieldGloballyDefined(id))
            {
                tb.ReadOnly = true;
                tb.BackColor = Color.Gainsboro;
                tb.ForeColor = Color.Black;
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
		}
		#endregion
	}
}
