﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Constants;
using System.Xml.Serialization;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Status
{
    public class SettlementServiceProviderListServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(SettlementServiceProviderListServiceItem));
        }

        private AvailableSettlementServiceProviders GetAvailableSettlementServiceProviders(string json, LosConvert converter)
        {
            var providers =
                SerializationHelper.JsonNetDeserialize<List<AvailableSettlementServiceProvidersForFeeViewModel>>(
                    json);

            var providersFromModel = new AvailableSettlementServiceProvidersViewModelParser(providers, converter);

            return providersFromModel.GetAvailableSettlementServiceProviders();
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var titleProviders = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<TitleInsuranceProvider>>(GetString("TitleProviders"));
            var escrowProviders = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<EscrowProvider>>(GetString("EscrowProviders"));
            var surveyProviders = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<SurveyProvider>>(GetString("SurveyProviders"));
            var pestInspectionProviders = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<PestInspectionProvider>>(GetString("PestInspectionProviders"));
            var miscProviders = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<SettlementServiceProvider>>(GetString("MiscProviders"));

            dataLoan.sTitleSettlementServiceProviders = titleProviders;
            dataLoan.sEscrowSettlementServiceProviders = escrowProviders;
            dataLoan.sSurveySettlementServiceProviders = surveyProviders;
            dataLoan.sPestInspectionSettlementServiceProviders = pestInspectionProviders;
            dataLoan.sMiscSettlementServiceProviders = miscProviders;

            if (!dataLoan.sIsLegacyClosingCostVersion)
            {
                var availableSettlementServiceProvidersJson = GetString("AvailableSettlementServiceProviders");
                var availableSettlementServiceProviders = this.GetAvailableSettlementServiceProviders(
                    availableSettlementServiceProvidersJson,
                    dataApp.m_convertLos);

                dataLoan.sAvailableSettlementServiceProviders = availableSettlementServiceProviders;
            }

        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            if (dataLoan.sIsInTRID2015Mode)
            {
                this.SetResult(nameof(dataLoan.sHasDataCompleteSettlementServiceProvidersList), dataLoan.sHasDataCompleteSettlementServiceProvidersList);
            }
        }

        private class AvailableSettlementServiceProvidersViewModelParser
        {
            private IEnumerable<AvailableSettlementServiceProvidersForFeeViewModel> viewModels;
            private LosConvert converter;

            public AvailableSettlementServiceProvidersViewModelParser(
                IEnumerable<AvailableSettlementServiceProvidersForFeeViewModel> viewModels,
                LosConvert losConverter)
            {
                this.viewModels = viewModels;
                this.converter = losConverter;
            }

            public AvailableSettlementServiceProviders GetAvailableSettlementServiceProviders()
            {
                var providers = new AvailableSettlementServiceProviders();

                foreach (var model in this.viewModels)
                {
                    providers.AddFeeType(model.FeeTypeId);
                    providers.AddProvidersForFeeType(model.FeeTypeId, this.GetServiceProvidersFromModel(model.SettlementServiceProviders));
                }

                return providers;
            }

            private IEnumerable<SettlementServiceProvider> GetServiceProvidersFromModel(IEnumerable<SettlementServiceProviderViewModel> modelList)
            {
                List<SettlementServiceProvider> ssProviders = new List<SettlementServiceProvider>();

                foreach (SettlementServiceProviderViewModel model in modelList)
                {
                    SettlementServiceProvider provider = new SettlementServiceProvider();
                    provider.AgentId = model.AgentId;
                    provider.City = model.City;
                    provider.CompanyName = model.CompanyName;
                    provider.ContactName = model.ContactName;
                    provider.SetEstimatedCostAmount_rep(this.converter, model.EstimatedCostAmount);
                    provider.EstimatedCostAmountPopulated = true; // Whatever is saved onto the page is what we'll use from now on.
                    provider.Phone = model.Phone;
                    provider.ServiceT = model.ServiceT;
                    provider.State = model.State;
                    provider.StreetAddress = model.StreetAddress;
                    provider.Zip = model.Zip;
                    provider.Email = model.Email;
                    provider.Fax = model.Fax;
                    provider.Id = model.Id;

                    ssProviders.Add(provider);
                }

                return ssProviders;
            }
        }
    }

    public partial class SettlementServiceProviderListService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new SettlementServiceProviderListServiceItem());
        }
    }
}
