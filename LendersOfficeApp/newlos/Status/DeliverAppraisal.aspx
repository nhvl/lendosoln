﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeliverAppraisal.aspx.cs" Inherits="LendersOfficeApp.newlos.Status.DeliverAppraisal" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Deliver Appraisal</title>
    <style type="text/css">
        body  {
            background-color: gainsboro; 
        }
        a[data-sort] {
            color: white;
        }
        .LabelLength {
            width: 140px;
        }
        Width7Pc {
            width: 7%;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        jQuery(function ($) {
            $('img[data-docmetadata]').docmetadatatooltip();

            function RowComparer(sortTargetClass) {
                return function (a, b) {
                    var sortTargetA = $(a).find('.' + sortTargetClass);
                    var sortTargetB = $(b).find('.' + sortTargetClass);

                    var sortValueA = sortTargetA.text();
                    var sortValueB = sortTargetB.text();

                    if (sortValueA < sortValueB) {
                        return -1;
                    }
                    else if (sortValueA > sortValueB) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                }
            }

            function CheckAllFieldsValid() {
                var targetChosen = $('#TargetVendor').val() !== '';
                var documentChosen = $('.DocPicker:checked').length > 0;
                var inputsOk = $('.RequiredInput:visible').filter(function () {
                    return $(this).val() === ''
                }).length === 0;

                return documentChosen && inputsOk && targetChosen;
            }
            function ToggleDeliverButton() {
                var fieldsValid = CheckAllFieldsValid();

                $('#DeliverBtn').prop('disabled', !fieldsValid);
                $('#DeliverWarning').toggle(!fieldsValid);
            }

            $('a[data-sort="true"]').click(function () {
                var link = $(this);
                var table = $(this).closest('table').eq(0);
                var rows = table.find('tr:gt(0)').toArray().sort(RowComparer(link.data('sorttarget')));
                table.find('.SortImg').hide();

                link.data('asc', !link.data('asc'));
                if (!link.data('asc')) {
                    rows = rows.reverse();
                    link.siblings('img').prop('src', '../../images/Tri_Desc.gif').show();
                }
                else {
                    link.siblings('img').prop('src', '../../images/Tri_Asc.gif').show();
                }

                for (var i = 0; i < rows.length; i++) {
                    table.append(rows[i]);
                }
            });

            $('.RequiredInput').change(function () {
                $(this).siblings('.RequiredImg').toggle(this.value === '');
                ToggleDeliverButton();
            });

            $('#CancelBtn').click(function () {
                parent.LQBPopup.Return()
            });

            $('#AssignWebsheetNumBtn').click(function () {
                var loanId = ML.sLId;
                var sDocMagicFileId = $('#sDocMagicFileId').val();

                var data = {
                    LoanId: loanId,
                    "sDocMagicFileId": sDocMagicFileId
                }

                gService.appraisal.callAsyncSimple("AssignWebsheetNum", data,
                    function (results) {
                        if (results.value.Error) {
                            alert(results.value.Error);
                        }
                        ToggleDeliverButton();
                    },
                    function (results) {
                        alert(results.UserMessage || "Unable to auto retrieve DM credentials");
                        ToggleDeliverButton();
                    });
            });

            $('.FileLink').click(function () {
                var docId = $(this).closest('.DocRow').find('.EDocId').val();
                window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?docid=" + encodeURIComponent(docId), '_self');
            });

            $('.XmlLink').click(function () {
                var docId = $(this).closest('.DocRow').find('.EDocId').val();
                window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?cmd=xml&docid=" + encodeURIComponent(docId), 'xmldoc');
            });

            $('#TargetVendor').change(function () {
                var vendorId = $(this).val();
                var loanId = ML.sLId;
                var data = {
                    VendorId: vendorId,
                    LoanId: loanId
                };
                
                $(this).siblings('.RequiredImg').toggle(this.value === '');
                if (vendorId == '') {
                    $('#CredentialsSection').toggle(!hasCredentials);
                    ToggleDeliverButton();
                    return;
                }

                gService.appraisal.callAsyncSimple("CheckDmCredentials", data,
                    function (results) {
                        if (results.value.Error) {
                            alert(results.value.Error);
                        }
                        else {
                            var hasCredentials = results.value.HasCredentials.toLowerCase() === 'true';
                            $('#CredentialsSection').toggle(!hasCredentials);
                        }
                        ToggleDeliverButton();
                    },
                    function (results) {
                        alert(results.UserMessage || "Unable to auto retrieve DM credentials");
                        ToggleDeliverButton();
                    });
            });

            $('.DocPicker').click(function () {
                ToggleDeliverButton();
            });

            $('#DeliverBtn').click(function () {
                var fieldsValid = CheckAllFieldsValid();
                if(!fieldsValid) {
                    alert("Please fill out all required fields.");
                    return;
                }

                var selectedDocIds = $('.DocPicker:checked').map(function() { return $(this).closest('.DocRow').find('.EDocId').val(); }).get();
                var targetVendorId = $('#TargetVendor').val();
                var websheetNumber = $('#sDocMagicFileId').val();
                var sendConfirmationEmail = $('#SendConfirmationEmail').is(':checked');
                var clickSign = $('#ClickSign').is(':checked');
                var borrowerMobileService = $('#BorrowerMobileService').is(':checked');
                var disableSigningInvitationIndicator = $('#DisableSigningInvitationIndicator').is(':checked');
                var enablePreviewIndicator = $('#EnablePreviewIndicator').is(':checked');
                var customerId = $('#CustomerId').val();
                var username = $('#Username').val();
                var password = $('#Password').val();

                var data = {
                    SelectedDocIds: JSON.stringify(selectedDocIds),
                    TargetVendorId: targetVendorId,
                    WebsheetNumber: websheetNumber,
                    SendConfirmationEmail: sendConfirmationEmail,
                    ClickSign: clickSign,
                    BorrowerMobileService: borrowerMobileService,
                    DisableSigningInvitationIndicator: disableSigningInvitationIndicator,
                    EnablePreviewIndicator: enablePreviewIndicator,
                    CustomerId: customerId,
                    Username: username,
                    Password: password,
                    AppraisalOrderId: ML.AppraisalOrderId,
                    LoanId: ML.sLId
                };

                var loadingPopup = SimplePopups.CreateLoadingPopup("Performing Request...");
                LQBPopup.ShowElement($('<div>').append(loadingPopup), {
                    width: 350,
                    height: 200,
                    hideCloseButton: true,
                    elementClasses: 'FullWidthHeight'
                });

                gService.appraisal.callAsyncSimple("DeliverAppraisal", data,
                    function (results) {
                        LQBPopup.Return(null);
                        if (results.value.Errors) {
                            var errors = JSON.parse(results.value.Errors);
                            var errorPopup = SimplePopups.CreateErrorPopup("Something went wrong", errors);
                            LQBPopup.ShowElement($j('<div>').append(errorPopup), {
                                width: 350,
                                height: 200,
                                hideCloseButton: true,
                                elementClasses: 'FullWidthHeight',
                            });
                        }
                        else {
                            var successPopup = SimplePopups.CreateAlertPopup(results.value.Message, '', ML.VirtualRoot + '/images/success.png');
                            LQBPopup.ShowElement($j('<div>').append(successPopup), {
                                width: 350,
                                height: 200,
                                hideCloseButton: true,
                                elementClasses: 'FullWidthHeight',
                                onReturn: function () {
                                    parent.LQBPopup.Return();
                                }
                            });
                        }
                    },
                    function (results) {
                        LQBPopup.Return(null);
                        alert(results.UserMessage);
                    });
            });

            ToggleDeliverButton();
            $('#sDocMagicFileIdImg').toggle($('#sDocMagicFileId').val() === '')
        });
    </script>
    <h4 class="page-header">DocMagic Appraisal Delivery</h4>
    <form id="form1" runat="server">
        <div>
            <div class="Padding5">
                <div>
                    <div>
                        <h3>Select a Document</h3>
                    </div>
                </div>
                <div>
                    <table id="DocumentTable" class="Table">
                        <thead>
                            <tr class="LoanFormHeader">
                                <td></td>
                                <td><a data-sort="true" data-sorttarget="Folder" data-asc="false">Folder</a><img alt="Sort Image" class="SortImage align-middle Hidden" /></td>
                                <td><a data-sort="true" data-sorttarget="DocType" data-asc="false">Doc Type</a><img alt="Sort Image" class="SortImage align-middle Hidden" /></td>
                                <td><a data-sort="true" data-sorttarget="Description" data-asc="false">Description</a><img alt="Sort Image" class="SortImage align-middle Hidden" /></td>
                                <td><a data-sort="true" data-sorttarget="Comments" data-asc="false">Comments</a><img alt="Sort Image" class="SortImage align-middle Hidden" /></td>
                                <td><a data-sort="true" data-sorttarget="LastModified" data-asc="false">Last Modified</a><img alt="Sort Image" class="SortImage align-middle Hidden" /></td>
                                <td class="Width7Pc">Document</td>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater runat="server" ID="DocRepeater" OnItemDataBound="DocRepeater_ItemDataBound">
                                <ItemTemplate>
                                    <tr class="ReverseGridAutoItem DocRow">
                                        <td>
                                            <input type="checkbox" name="DocPicker" class="DocPicker" />
                                            <input type="hidden" runat="server" class="EDocId" id="EDocId" />
                                        </td>
                                        <td>
                                            <span runat="server" class="Folder" id="Folder"></span>
                                        </td>
                                        <td>
                                            <span runat="server" class="DocType" id="DocType"></span>
                                        </td>
                                        <td>
                                            <span runat="server" class="Description" id="Description"></span>
                                        </td>
                                        <td>
                                            <span runat="server" class="Comments" id="Comments"></span>
                                        </td>
                                        <td>
                                            <span runat="server" class="LastModified" id="LastModified"></span>
                                        </td>
                                        <td>
                                            <img runat="server" id="FileInfo" visible="false" class="FloatRight" />
                                            <div>
                                                <a runat="server" id="FileLink" class="FileLink">view pdf</a>
                                            </div>
                                            <div>
                                                <a runat="server" id="XmlLink" class="XmlLink">view xml</a>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
                <hr />
                <div id="ConfigurationSection">
                    <h3>Configuration</h3>
                    <div>
                        <span class="FieldLabel InlineBlock LabelLength">Target Vendor</span>
                        <asp:DropDownList runat="server" ID="TargetVendor"></asp:DropDownList>
                        <img alt="Required" class="RequiredImg align-middle" id="TargetVendorImg" src="../../images/require_icon_red.gif" />
                    </div>
                    <div class="PaddingTop">
                        <span class="FieldLabel InlineBlock LabelLength">Websheet Number</span>
                        <input type="text" class="RequiredInput" id="sDocMagicFileId" runat="server" />
                        <input type="button" id="AssignWebsheetNumBtn" value="Assign" />
                        <img alt="Required" class="RequiredImg" id="sDocMagicFileIdImg" runat="server" src="../../images/require_icon_red.gif" />
                    </div>
                    <div class="PaddingTop">
                        <span class="FieldLabel InlineBlock LabelLength">Send Confirmation Email</span>
                        <input type="checkbox" id="SendConfirmationEmail" />
                    </div>
                    <div class="PaddingTop">
                        <span class="FieldLabel InlineBlock LabelLength">EPortal Options</span>
                        <ul class="UndecoratedList">
                            <li>
                                <label>
                                    <input type="checkbox" id="ClickSign" />Click Sign
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="checkbox" id="BorrowerMobileService" />Borrower Mobile Service
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="checkbox" id="DisableSigningInvitationIndicator" />Disable Signing Invitation
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="checkbox" id="EnablePreviewIndicator" />Enable Preview
                                </label>
                            </li>
                        </ul>
                    </div>
                    <hr />
                </div>
                <div id="CredentialsSection">
                    <div class="CssTable FullWidthHeight">
                        <div class="CssTableCell Padding5 Width50Pc">
                            <h3>DocMagic Credentials</h3>
                            <div class="LeftPadding20">
                                <table>
                                    <tr id="CustomerIdRow">
                                        <td class="FieldLabel">Customer ID</td>
                                        <td>
                                            <input type="text" class="RequiredInput InputLarge Credential" id="CustomerId" runat="server" />
                                            <img alt="Required" class="RequiredImg" id="CustomerIdImg" runat="server" src="../../images/require_icon_red.gif" />
                                        </td>
                                    </tr>
                                    <tr id="UsernameRow">
                                        <td class="FieldLabel">Username</td>
                                        <td>
                                            <input type="text" class="RequiredInput InputLarge Credential" id="Username" />
                                            <img alt="Required" class="RequiredImg" id="UsernameImg" runat="server" src="../../images/require_icon_red.gif" />
                                        </td>
                                    </tr>
                                    <tr id="PasswordRow">
                                        <td class="FieldLabel">Password</td>
                                        <td>
                                            <input type="password" class="RequiredInput Inputlarge Credential" id="Password" />
                                            <img alt="Required" class="RequiredImg" id="PasswordImg" runat="server" src="../../images/require_icon_red.gif" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="align-center">
            <input type="button" id="DeliverBtn" value="Deliver" />
            <img src="~/images/warning25x25.png" runat="server" class="WarningIcon align-middle" alt="Warning" id="DeliverWarning" title="Please fill out all required fields."/>
            <input type="button" id="CancelBtn" value="Cancel" />
        </div>
    </form>
</body>
</html>
