﻿namespace LendersOfficeApp.newlos.Status
{
    using System;
    using System.Drawing;
    using System.Web.UI;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Migration;
    using LendersOffice.Rolodex;
    using LendersOffice.Security;

    public partial class AgentRecord1 : BaseLoanUserControl
    {
        private CPageData m_dataLoan;
        protected bool m_showAffilates;
        protected bool m_allowToggleOverride;

        protected string AgentLicensesPanel { get; set; }
        protected string CompanyLicensesPanel { get; set; }

        protected Guid RecordID
        {
            get { return RequestHelper.GetGuid("recordid", Guid.Empty); }
        }

        protected bool IsFeeContactPicker
        {
            get
            {
                return RequestHelper.GetBool("isFeeContactPicker");
            }
        }

        protected void Page_Init(object sender, System.EventArgs e)
        {
            ((BaseServicePage)Page).RegisterService("agents", "/newlos/Status/AgentRecordService.aspx");

            RolodexDB.PopulateAgentTypeDropDownList(AgentRoleT);

            Zip.SmartZipcode(City, State, CountyDropdown);
            State.Attributes.Add("onchange", "javascript:UpdateCounties(this,document.getElementById('" + this.CountyDropdown.ClientID + "'), event);");

            m_dataLoan = new CPeopleData(LoanID);
            m_dataLoan.InitLoad();

            EmailValidator.ValidationExpression = ConstApp.EmailValidationExpression;
            EmailValidator.Text = "(" + ErrorMessages.InvalidEmailAddress + ")";

            CFM.IsAllowLockableFeature = false;
            CFM.TypeDDL = AgentRoleT.ClientID;
            CFM.PhoneField = Phone.ClientID;
            CFM.AgentNameField = AgentName.ClientID;
            CFM.FaxField = FaxNum.ClientID;
            CFM.CompanyNameField = CompanyName.ClientID;
            CFM.CellPhoneField = CellPhone.ClientID;
            CFM.DepartmentNameField = DepartmentName.ClientID;
            CFM.PagerNumField = PagerNum.ClientID;
            CFM.StreetAddressField = StreetAddr.ClientID;
            CFM.CityField = City.ClientID;
            CFM.StateField = State.ClientID;
            CFM.ZipField = Zip.ClientID;
            CFM.CountyField = County.ClientID;
            CFM.CompanyStreetAddr = StreetAddr.ClientID;
            CFM.CompanyCity = City.ClientID;
            CFM.CompanyState = State.ClientID;
            CFM.CompanyZip = Zip.ClientID;
            CFM.CompanyPhoneField = PhoneOfCompany.ClientID;
            CFM.CompanyFaxField = FaxOfCompany.ClientID;
            CFM.EmailField = EmailAddr.ClientID;
            CFM.EmployeeIdField = EmployeeId.ClientID;
            CFM.EmployeeIDInCompanyField = EmployeeIDInCompany.ClientID;
            CFM.CompanyIdField = CompanyId.ClientID;
            if (!string.IsNullOrEmpty(m_dataLoan.sSpState))
            {
                CFM.AgentLicenseField = LicenseNum.ClientID;
                CFM.CompanyLicenseField = LicenseNumOfCompany.ClientID;
            }

            CFM.AgentLicensesPanel = AgentLicensesPanel;
            CFM.CompanyLicensesPanel = CompanyLicensesPanel;

            CFM.StatusLoanChangeField = IsNotifyWhenLoanStatusChange.ClientID;
            CFM.ComPointLoanAmtField = CommissionPointOfLoanAmount.ClientID;
            CFM.ComPointGrossField = CommissionPointOfGrossProfit.ClientID;
            CFM.ComMinBaseField = CommissionMinBase.ClientID;
            CFM.TaxIdField = TaxID.ClientID;
            CFM.ChumsIdField = ChumsID.ClientID;

            // OPM 109299
            CFM.BranchNameField = BranchName.ClientID;
            CFM.PayToBankNameField = PayToBankName.ClientID;
            CFM.PayToBankCityStateField = PayToBankCityState.ClientID;
            CFM.PayToABANumberField = PayToABANumber.ClientID;
            CFM.PayToAccountNumberField = PayToAccountNumber.ClientID;
            CFM.PayToAccountNameField = PayToAccountName.ClientID;
            CFM.FurtherCreditToAccountNumberField = FurtherCreditToAccountNumber.ClientID;
            CFM.FurtherCreditToAccountNameField = FurtherCreditToAccountName.ClientID;

            CFM.LoanOriginatorIdentifierField = LoanOriginatorIdentifier.ClientID;
            CFM.CompanyLoanOriginatorIdentifierField = CompanyLoanOriginatorIdentifier.ClientID;

            CFM.AgentSourceTField = AgentSourceT.ClientID;
            CFM.BrokerLevelAgentIDField = BrokerLevelAgentID.ClientID;
            CFM.ShouldMatchBrokerContactField = ShouldMatchBrokerContact.ClientID;
            CFM.NotesField = Notes.ClientID;
            CFM.OverrideLicensesField = OverrideLicenses.ClientID;

            // OPM 227085, 12/29/2015, ML
            CFM.IsLenderField = IsLender.ClientID;
            CFM.IsOriginatorField = IsOriginator.ClientID;

            if (RecordID == Guid.Empty)
            {
                LicenseNum.Attributes.Add("readonly", "readonly");
                LicenseNumOfCompany.Attributes.Add("readonly", "readonly");
                LicenseNum.BackColor = Color.LightGray;
                LicenseNumOfCompany.BackColor = Color.LightGray;
            }

            sSpState.Attributes.Add("readonly", "readonly");
            sSpState.BackColor = Color.LightGray;

            m_showAffilates = m_dataLoan.BrokerDB.IsEnableGfe2015 && m_dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy;
            m_allowToggleOverride = BrokerUser.HasPermission(Permission.AllowOverridingContactLicenses);

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.m_dataLoan.sLoanVersionT, LoanVersionT.V18_ConsolidateTaxIdFields))
            {
                this.TaxID.Attributes.Add("preset", "employerIdentificationNumber");
            }
        }

        public void BindSingleRecord(CAgentFields field)
        {
            AgentName.Text = field.AgentName;
            Tools.SetDropDownListValue(AgentRoleT, field.AgentRoleT);
            CaseNum.Text = field.CaseNum;
            CellPhone.Text = field.CellPhone;
            City.Text = field.City;
            CompanyName.Text = field.CompanyName;
            DepartmentName.Text = field.DepartmentName;
            EmailAddr.Text = field.EmailAddr;
            FaxNum.Text = field.FaxNum;
            Notes.Text = field.Notes;
            LicenseNum.Text = field.LicenseNumOfAgent;
            LicenseNumOfCompany.Text = field.LicenseNumOfCompany;
            PagerNum.Text = field.PagerNum;
            Phone.Text = field.Phone;
            State.Value = field.State;
            StreetAddr.Text = field.StreetAddr;
            Zip.Text = field.Zip;
            Tools.Bind_sSpCounty(field.State, this.CountyDropdown, true);
            this.County.Value = field.County;
            Tools.SetDropDownListCaseInsensitive(this.CountyDropdown, field.County);
            EmployeeIDInCompany.Text = field.EmployeeIDInCompany;
            CompanyId.Text = field.CompanyId;
            OtherAgentRoleTDesc.Text = field.OtherAgentRoleTDesc;
            InvestorBasisPoints.Text = field.InvestorBasisPoints_rep;
            InvestorSoldDate.Text = field.InvestorSoldDate_rep;
            CommissionMinBase.Text = field.CommissionMinBase_rep;
            CommissionPointOfLoanAmount.Text = field.CommissionPointOfLoanAmount_rep;
            CommissionPointOfGrossProfit.Text = field.CommissionPointOfGrossProfit_rep;
            Commission.Text = field.Commission_rep;
            ProviderItemNumber.Text = field.ProviderItemNumber;
            IsLenderAssociation.Checked = field.IsLenderAssociation;
            IsLenderAffiliate.Checked = field.IsLenderAffiliate;
            IsLenderAffiliate_Relation.Checked = field.IsLenderAffiliate;
            IsLenderRelative.Checked = field.IsLenderRelative;
            HasLenderRelationship.Checked = field.HasLenderRelationship;
            HasLenderAccountLast12Months.Checked = field.HasLenderAccountLast12Months;
            IsUsedRepeatlyByLenderLast12Months.Checked = field.IsUsedRepeatlyByLenderLast12Months;
            //08-06-07 av OPM 3866
            //IsApplyToFormsUponSave.Checked = field.IsApplyToFormsUponSave;
            IsListedInGFEProviderForm.Checked = field.IsListedInGFEProviderForm;
            PhoneOfCompany.Text = field.PhoneOfCompany;
            FaxOfCompany.Text = field.FaxOfCompany;
            IsNotifyWhenLoanStatusChange.Checked = field.IsNotifyWhenLoanStatusChange;
            IsLender.Checked = field.IsLender;
            IsOriginator.Checked = field.IsOriginator;
            IsOriginatorAffiliate.Checked = field.IsOriginatorAffiliate;
            OverrideLicenses.Checked = field.OverrideLicenses;

            if (!field.OverrideLicenses)
            {
                LicenseNum.Attributes.Add("readonly", "readonly");
                LicenseNumOfCompany.Attributes.Add("readonly", "readonly");
                LicenseNum.BackColor = Color.LightGray;
                LicenseNumOfCompany.BackColor = Color.LightGray;
            }

            sSpState.Text = m_dataLoan.sSpState;
            TaxID.Text = field.TaxId;
            ChumsID.Text = field.ChumsId;
            EmployeeId.Value = field.EmployeeId.ToString();
            AgentSourceT.Value = ((int)field.AgentSourceT).ToString();
            BrokerLevelAgentID.Value = field.BrokerLevelAgentID.ToString();


            LoanOriginatorIdentifier.Text = field.LoanOriginatorIdentifier;
            CompanyLoanOriginatorIdentifier.Text = field.CompanyLoanOriginatorIdentifier;

            //av opm 48610 11/08/10  Agents record: Hide the commission information unless user has Finance read permission
            CommisionInfo.Visible = BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowAccountantRead);

            string isUseOriginatorCompensationAmount = "";
            if (CommisionInfo.Visible)
            {
                CommissionInfoRow.Visible = field.IsUseOriginatorCompensationAmount == false;
                isUseOriginatorCompensationAmount = field.IsUseOriginatorCompensationAmount.ToString();
            }

            BranchName.Text = field.BranchName;
            PayToBankName.Text = field.PayToBankName;
            PayToBankCityState.Text = field.PayToBankCityState;
            PayToABANumber.Text = field.PayToABANumber.Value;
            PayToAccountNumber.Text = field.PayToAccountNumber.Value;
            PayToAccountName.Text = field.PayToAccountName;
            FurtherCreditToAccountNumber.Text = field.FurtherCreditToAccountNumber.Value;
            FurtherCreditToAccountName.Text = field.FurtherCreditToAccountName;

            Page.ClientScript.RegisterHiddenField("IsUseOriginatorCompensationAmount", isUseOriginatorCompensationAmount);
        }
    }
}