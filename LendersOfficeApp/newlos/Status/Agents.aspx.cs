using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Rolodex;
using LendersOffice.Security;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos.Status
{

    public partial class Agents : BaseLoanPage
    {
        public class AgentRoleItem
        {
            public string ShortDesc { get; set; }
            public string LongDesc { get; set; }
            public string Show { get; set; }

        }

        protected int tabIndex
        {
            get { return RequestHelper.GetInt("tab", 0);}
        }

        protected string GetAgentCompanyName(string recordID)
        {

            return loanData.GetAgentFields(new Guid(recordID)).CompanyName;
        }

        protected string m_sortExpression;

        protected string displayYesNo(object b)
        {
            string ret = "No";
            if (null != b)
            {
                if (b.ToString() == "True") ret = "Yes";
            }
            return ret;
        }

        protected bool IsPickerDialog
        {
            get
            {
                return !string.IsNullOrEmpty(Request.QueryString["picker"]) && Request.QueryString["picker"].ToUpper() == "Y";
            }
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
            RegisterJsScript("utilities.js");
            IsAlwaysSave = true;
            this.PageTitle = "Agent list";
            this.PageID = "StatusAgents";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CAgentListPDF);
            this.RegisterService("loanedit", "/newlos/Status/AgentsService.aspx");

            List<AgentRoleItem> roleItemList = new List<AgentRoleItem>()
            {
                new AgentRoleItem() { LongDesc = "Test", ShortDesc="Blah", Show="Simple"},
                new AgentRoleItem() { LongDesc="CC", ShortDesc="CC", Show="Full"}
            };

            RegisterJsObjectArray("AgentRoleItemList", roleItemList);
        }

        protected override void SaveData()
        {
            LoadData();
        }

        private CPageData loanData;
        protected override void LoadData()
        {
            // 8/17/2005 kb - Updated assignment loading to use admin
            // helpers and an init table to help with maintenance.
            // Just add a new link object to the table to get the
            // role to load.
            loanData = CPageData.CreateUsingSmartDependency(LoanID, typeof(Agents));
            loanData.InitLoad();

            //LoanAssignmentContactTable lTable = new LoanAssignmentContactTable();
            Dictionary<E_RoleT, HtmlGenericControl> teamNameSpans = new Dictionary<E_RoleT, HtmlGenericControl>();
            teamNameSpans.Add(E_RoleT.LoanOfficer, m_agentTeam);
            teamNameSpans.Add(E_RoleT.Processor, m_processorTeam);
            teamNameSpans.Add(E_RoleT.Manager, m_managerTeam);
            teamNameSpans.Add(E_RoleT.CallCenterAgent, m_telemarketerTeam);
            teamNameSpans.Add(E_RoleT.RealEstateAgent, m_realEstateTeam);
            teamNameSpans.Add(E_RoleT.LoanOpener, m_loanOpenerTeam);
            teamNameSpans.Add(E_RoleT.LenderAccountExecutive, m_lenderAcctExecTeam);
            teamNameSpans.Add(E_RoleT.LockDesk, m_lockDeskTeam);
            teamNameSpans.Add(E_RoleT.Underwriter, m_underwriterTeam);
            teamNameSpans.Add(E_RoleT.Closer, m_closerTeam);
            teamNameSpans.Add(E_RoleT.Shipper, m_shipperTeam);
            teamNameSpans.Add(E_RoleT.DocDrawer, m_docDrawerTeam);
            teamNameSpans.Add(E_RoleT.Funder, m_funderTeam);
            teamNameSpans.Add(E_RoleT.PostCloser, m_postCloserTeam);
            teamNameSpans.Add(E_RoleT.Insuring, m_insuringTeam);
            teamNameSpans.Add(E_RoleT.CollateralAgent, m_collateralAgentTeam);
            teamNameSpans.Add(E_RoleT.CreditAuditor, m_creditAuditorTeam);
            teamNameSpans.Add(E_RoleT.DisclosureDesk, m_disclosureDeskTeam);
            teamNameSpans.Add(E_RoleT.JuniorProcessor, m_juniorProcessorTeam);
            teamNameSpans.Add(E_RoleT.JuniorUnderwriter, m_juniorUnderwriterTeam);
            teamNameSpans.Add(E_RoleT.LegalAuditor, m_legalAuditorTeam);
            teamNameSpans.Add(E_RoleT.LoanOfficerAssistant, m_loanOfficerAssistantTeam);
            teamNameSpans.Add(E_RoleT.Purchaser, m_purchaserTeam);
            teamNameSpans.Add(E_RoleT.QCCompliance, m_qcComplianceTeam);
            teamNameSpans.Add(E_RoleT.Secondary, m_secondaryTeam);
            teamNameSpans.Add(E_RoleT.Servicing, m_servicingTeam);
            if (BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
            {
                teamNameSpans.Add(E_RoleT.Pml_BrokerProcessor, m_brokerprocessorTeam);
            }

            foreach(Team team in Team.ListTeamsOnLoan(LoanID))
            {
                Role role = Role.Get(team.RoleId);

                if (role.RoleT == E_RoleT.Administrator || role.RoleT == E_RoleT.Accountant)
                {
                    continue;
                }

                HtmlGenericControl span = teamNameSpans[role.RoleT];
                if (span != null)
                {
                    span.InnerText = team.Name;
                }
            }

            //LoanAssignmentContactTable lTable = new LoanAssignmentContactTable();
            Dictionary<E_RoleT, HtmlAnchor> rLinks = new Dictionary<E_RoleT, HtmlAnchor>();

            rLinks.Add( E_RoleT.LoanOfficer, m_agentLink);
            rLinks.Add( E_RoleT.Processor, m_processorLink);
            rLinks.Add( E_RoleT.Manager, m_managerLink);
            rLinks.Add( E_RoleT.CallCenterAgent, m_telemarketerLink);
            rLinks.Add( E_RoleT.RealEstateAgent, m_realEstateLink);
            rLinks.Add( E_RoleT.LoanOpener, m_loanOpenerLink);
            rLinks.Add( E_RoleT.LenderAccountExecutive, m_lenderAcctExecLink);
            rLinks.Add( E_RoleT.LockDesk, m_lockDeskLink);
            rLinks.Add( E_RoleT.Underwriter, m_underwriterLink);
            rLinks.Add( E_RoleT.Closer, m_closerLink);
            rLinks.Add( E_RoleT.Shipper, m_shipperLink);
            rLinks.Add( E_RoleT.DocDrawer, m_docDrawerLink);
            rLinks.Add( E_RoleT.Funder, m_funderLink);
            rLinks.Add( E_RoleT.PostCloser, m_postCloserLink);
            rLinks.Add( E_RoleT.Insuring, m_insuringLink);
            rLinks.Add( E_RoleT.CollateralAgent, m_collateralAgentLink);
            rLinks.Add( E_RoleT.CreditAuditor, m_creditAuditorLink);
            rLinks.Add( E_RoleT.DisclosureDesk, m_disclosureDeskLink);
            rLinks.Add( E_RoleT.JuniorProcessor, m_juniorProcessorLink);
            rLinks.Add( E_RoleT.JuniorUnderwriter, m_juniorUnderwriterLink);
            rLinks.Add( E_RoleT.LegalAuditor, m_legalAuditorLink);
            rLinks.Add( E_RoleT.LoanOfficerAssistant, m_loanOfficerAssistantLink);
            rLinks.Add( E_RoleT.Purchaser, m_purchaserLink);
            rLinks.Add( E_RoleT.QCCompliance, m_qcComplianceLink);
            rLinks.Add( E_RoleT.Secondary, m_secondaryLink);
            rLinks.Add( E_RoleT.Servicing, m_servicingLink);


            if (BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
            {
                rLinks.Add(E_RoleT.Pml_LoanOfficer, m_brokerAgentLink);
                rLinks.Add(E_RoleT.Pml_BrokerProcessor, m_brokerprocessorLink);
                rLinks.Add(E_RoleT.Pml_Secondary, m_externalSecondaryLink);
                rLinks.Add(E_RoleT.Pml_PostCloser, m_externalPostCloserLink);
            }

            LoanAssignmentContactTable loanAssignmentContactTable = new LoanAssignmentContactTable(BrokerUser.BrokerId, LoanID);
            //lTable.Retrieve( LoanID );

            foreach (EmployeeLoanAssignment lA in loanAssignmentContactTable.Items)
            {
                HtmlAnchor hLink;

                if (false == rLinks.TryGetValue(lA.Role.RoleT, out hLink))
                {
                    Tools.LogError("Didn't find role '" + lA.RoleDesc + "' in agent list ui for loan " + LoanID + ".");
                    continue;
                }

                // The LoanAssignmentContactTable will return E_RoleT.LoanOfficer
                // for a PML Loan Officer instead of E_RoleT.Pml_LoanOfficer.
                if (BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) &&
                    lA.RoleT == E_RoleT.LoanOfficer && lA.IsPmlUser)
                {
                    hLink = rLinks[E_RoleT.Pml_LoanOfficer];
                }

                hLink.HRef = "mailto:" + AspxTools.HtmlString(lA.Email) + "?subject=" + Uri.EscapeDataString(loanData.sLNm); // opm 132146
                hLink.InnerText = lA.EmployeeName;
            }

            var lo = loanAssignmentContactTable.FindByRoleT(E_RoleT.LoanOfficer);
            var bp = loanAssignmentContactTable.FindByRoleT(E_RoleT.Pml_BrokerProcessor);
            var externalSecondary = loanAssignmentContactTable.FindByRoleT(E_RoleT.Pml_Secondary);
            var externalPostCloser = loanAssignmentContactTable.FindByRoleT(E_RoleT.Pml_PostCloser);

            BrokerProcessorSettings.Visible = BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan);

            if (null != lo && false == lo.IsPmlUser)
            {
                m_brokerprocessorLink.InnerText = "(LO is required to be a PML user)";
                m_brokerprocessorLink.Style.Add("color", "gray");
                bp_assign_link.Visible = false;

                if (externalSecondary == null)
                {
                    m_externalSecondaryLink.InnerText = "(LO is required to be a PML User)";
                    m_externalSecondaryLink.Style.Add("color", "gray");
                    m_externalSecondaryAssignLink.Visible = false;
                }
            }

            if (lo == null && externalSecondary != null)
            {
                m_agentLink.InnerText = "(Secondary (External) must not be assigned)";
                m_agentLink.Style.Add("color", "gray");
                m_agentAssignLink.Visible = false;
            }

            if (externalSecondary == null && externalPostCloser == null)
            {
                m_externalPostCloserLink.InnerText = "(Secondary (External) must be assigned)";
                m_externalPostCloserLink.Style.Add("color", "gray");
                m_externalPostCloserAssignLink.Visible = false;
            }

            // The originating company depends on the currently assigned loan officer.
            PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerByLoanId(BrokerUser.BrokerId, RequestHelper.LoanID);

            if (pmlBroker != null)
            {
                OriginatingCompanyNameRow.Visible = true;
                OriginatingCompanyName.Text = pmlBroker.NmlsName;
                OriginatingCompanyName1.Text = pmlBroker.NmlsName;
                OriginatingCompanyID.Text = pmlBroker.CompanyId;
                OriginatingCompanyIndexNumber.Text = pmlBroker.IndexNumber.ToString();

                var incorrectRoles = new List<string>();

                if (lo != null && lo.PmlBrokerId != pmlBroker.PmlBrokerId)
                {
                    incorrectRoles.Add(lo.RoleModifiableDesc);
                }

                if (bp != null && bp.PmlBrokerId != pmlBroker.PmlBrokerId)
                {
                    incorrectRoles.Add(bp.RoleModifiableDesc);
                }

                if (externalSecondary != null &&
                    externalSecondary.PmlBrokerId != pmlBroker.PmlBrokerId)
                {
                    incorrectRoles.Add(externalSecondary.RoleModifiableDesc);
                }

                if (externalPostCloser != null &&
                    externalPostCloser.PmlBrokerId != pmlBroker.PmlBrokerId)
                {
                    incorrectRoles.Add(externalPostCloser.RoleModifiableDesc);
                }

                string formattedRoles = "";
                if (incorrectRoles.Count == 1)
                {
                    formattedRoles = incorrectRoles.First();
                }
                else if (incorrectRoles.Count == 2)
                {
                    formattedRoles = string.Format(
                        "{0} and {1}",
                        incorrectRoles[0],
                        incorrectRoles[1]);
                }
                else if (incorrectRoles.Count > 2)
                {
                    formattedRoles = string.Format(
                        "{0}, and {1}",
                        string.Join(", ", incorrectRoles.Take(incorrectRoles.Count - 1).ToArray()),
                        incorrectRoles.Last());
                }

                OCRoles.Text = OCRoles1.Text = OCRoles11.Text = formattedRoles;

                mOCError.Visible = incorrectRoles.Any();

                sPmlBrokerStatusT.InnerText = loanData.sPmlBrokerStatusT_rep;
            }

            sPmlCompanyTierDesc.Text = loanData.sPmlCompanyTierDesc;
            trPmlCompanyTierDesc.Visible = loanData.sPmlCompanyTierId != -1;

            sFileVersion = loanData.sFileVersion;
            DataSet ds = loanData.sAgentDataSet;
            AddRoleDescriptionColumn(ds);
            BindDataGrid(ds);
            IsPageReadOnly.Value = IsReadOnly.ToString().ToLower();

            if (!Broker.EnableTeamsUI)
            {
                tab1.Style.Add("display", "none");
            }
        }

        /// <summary>
        /// Injects an AgentRoleDescription column into the agent dataset. This is needed so the Type column
        /// in the UI has a string to sort on. If we just sort on AgentRoleT (the stored integer enum value)
        /// the results will not be alphabetical.
        /// </summary>
        /// <param name="dataset">The agent dataset.</param>
        /// <remarks>This replicates functionality from <see cref="CAgentFields"/> since this file works
        /// directly on the agent dataset.</remarks>
        public static void AddRoleDescriptionColumn(DataSet dataset)
        {
            var table = dataset.Tables["AgentXmlContent"];
            table.Columns.Add("AgentRoleDescription", typeof(string));

            foreach (DataRow row in table.Rows)
            {
                row["AgentRoleDescription"] = RolodexDB.GetAgentType(row["AgentRoleT"].ToString(), row["OtherAgentRoleTDesc"].ToString());
            }
        }

        private void BindDataGrid(DataSet ds)
        {
            m_agentsDG.DataSource = ds.Tables[0].DefaultView;
            m_agentsDG.DataBind();

            m_sortExpression = ((DataView) m_agentsDG.DataSource ).Sort;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
