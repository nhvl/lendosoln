﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EDocs;
using System.Web.UI.HtmlControls;
using LendersOffice.ObjLib.DocMagicLib;
using LendersOffice.Security;
using CommonProjectLib.Common.Lib;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos.Status
{
    public partial class AlternateLender : LendersOffice.Common.BaseServicePage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
            BindLender();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(Page.IsPostBack)
            {
                if (m_hiddenCmd.Value == "deleteLender")
                    DeleteLenderClick();
            }  
            BindLender();
        }

        protected void BindLender()
        {
            m_dgLenders.DataSource = DocMagicAlternateLender.GetAlternateLenderListByBroker(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            m_dgLenders.DataBind();
        }

        protected void OnDataBound_m_dgLenders(object sender, DataGridItemEventArgs args)
        {
            Tuple<string, Guid> lender = (Tuple<string, Guid>)args.Item.DataItem;            
            if (lender == null)
                return;

            Literal lenderName = (Literal)args.Item.FindControl("LenderName");            
            HtmlAnchor editAnchor = (HtmlAnchor)args.Item.FindControl("EditLender");
            HtmlAnchor deleteAnchor = (HtmlAnchor)args.Item.FindControl("DeleteLender");
            lenderName.Text = lender.Item1;             
            editAnchor.Attributes.Add("onclick", "onEditLender('" + AspxTools.JsStringUnquoted(lender.Item2.ToString()) + "')");
            deleteAnchor.Attributes.Add("onclick", "onDeleteLender('" + AspxTools.JsStringUnquoted(lender.Item2.ToString()) + "')");                    

        }

        protected void DeleteLenderClick()
        {
            DocMagicAlternateLender.DeleteAlternateLenderId(BrokerUserPrincipal.CurrentPrincipal.BrokerId, new Guid(m_hiddenId.Value));                        
        }
    }
}
