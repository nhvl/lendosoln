﻿<%-- Include in page: RegisterService("agentsService", "/newlos/Status/AgentsService.aspx");   --%>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfficialContactList.ascx.cs" Inherits="LendersOfficeApp.newlos.Status.OfficialContactList" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:HiddenField runat="server" ID="IsPageReadOnly" />
<table width="100%">
    <tr>
        <td valign="top" colspan="2">
            <p class="FormTableSubheader" align="left">
                OFFICIAL CONTACT LIST FOR THIS LOAN</p>
        </td>
    </tr>
    <tr>
        <td valign="top" colspan="2" style="height: 20px">
            <input onclick="displayPeopleRecord(<%= AspxTools.JsString(Guid.Empty)%>);" type="button" value="Add People..."
                accesskey="A" id="btnAddPeople">&nbsp;&nbsp;&nbsp;
                <input onclick="OfficialContactsPage.confirmDelete();" type="button" value="Delete checked" id="btnDelete" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td valign="top" align="left" colspan="2">
            <ml:CommonDataGrid ID="m_agentsDG" runat="server">
                <AlternatingItemStyle CssClass="GridAlternatingItem"></AlternatingItemStyle>
                <ItemStyle CssClass="GridItem"></ItemStyle>
                <HeaderStyle CssClass="GridHeader"></HeaderStyle>
                <Columns>
                    <asp:TemplateColumn HeaderText="Select">
                        <ItemTemplate>
                            <input type="checkbox" name="del" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "RecordId").ToString()) %>"
                                onclick='highlightRowByCheckbox(this);'>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Type" SortExpression="AgentRoleT">
                        <ItemTemplate>
                            <%# AspxTools.HtmlString(LendersOffice.Rolodex.RolodexDB.GetAgentType(Eval("AgentRoleT").ToString(), Eval("OtherAgentRoleTDesc").ToString()))%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Agent Name" SortExpression="AgentName">
                        <ItemTemplate>
                            <%# AspxTools.HtmlString(Eval("AgentName").ToString())%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Company Name" SortExpression="CompanyName">
                        <ItemTemplate>
                            <%# AspxTools.HtmlString(Eval("CompanyName").ToString())%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="Phone" HeaderText="Phone"></asp:BoundColumn>
                    <asp:HyperLinkColumn DataTextField="EmailAddr" DataNavigateUrlField="EmailAddr" DataNavigateUrlFormatString="mailto:{0}" HeaderText="Email" />
                    <asp:TemplateColumn HeaderText="Lender Relationship?">
                        <ItemTemplate>
                            <%# AspxTools.HtmlString(displayYesNo(Eval("IsListedInGFEProviderForm")))%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            <a href="#" onclick="displayPeopleRecord(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "RecordId").ToString())%>)">
                                edit</a>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </ml:CommonDataGrid>
        </td>
    </tr>
</table>
<script type="text/javascript">
<!--
    var OfficialContactsPage = (function(){ 
        var m_isReadOnly = false;
        var m_AgentsServiceFound = false;
        function init(){
            var IsPageReadOnly = document.getElementById('<%= AspxTools.ClientId(IsPageReadOnly) %>');
            
            m_isReadOnly = IsPageReadOnly.value == 'true'; 
            document.getElementById("btnAddPeople").disabled = m_isReadOnly;
            document.getElementById("btnDelete").disabled = m_isReadOnly;
            if (gService.agentsService && typeof (gService.agentsService.call) === 'function')
                m_AgentsServiceFound = true;
        }
        function confirmDelete(){
           if(m_isReadOnly || !m_AgentsServiceFound){
                return false;
            }
        
            var collection = document.getElementsByName('del');
            var length = collection.length;

            var isChecked = false;
            var checkList = [];
            for (var i = 0; i < length; i++) {
                if (collection[i].checked == true) {
                    isChecked = true;
                    checkList.push(collection[i].value);
                    //break;
                }
            }
        
            if (!isChecked) {  
                alert('No agent selected.');
                return false;
            }

            var bConfirm = confirm('Do you want to delete these agents?');
            if (bConfirm == false) {
              return false;
            }
            
            length = checkList.length;
            var args = {};
            args.LoanID = ML.sLId;
            args.Count = length + '';
            args.sFileVersion = document.getElementById("sFileVersion").value;
            for (var i = 0; i < length; i++) {
              args["id" + i] = checkList[i];
            }
            var result = gService.agentsService.call("DeleteOfficialContact", args);
            if (!result.error)
            {
              location.href = location.href;
            } else {
                if (result.ErrorType === 'VersionMismatchException')
                {
                  f_displayVersionMismatch();
                }
                else if (result.ErrorType === 'LoanFieldWritePermissionDenied') 
                {
                  f_displayFieldWriteDenied(result.UserMessage);
                }
                else
                {
                  var errMsg = result.UserMessage || 'Unable to delete agent. Please try again';
                  alert(errMsg);
                }

            }
            
        }
        return { 
            init : init,
            confirmDelete : confirmDelete
        }
    })();
    function _init() {
        OfficialContactsPage.init();
    }
    function displayPeopleRecord(id) {
      linkMe(gVirtualRoot + "/newlos/Status/AgentRecord.aspx?recordid=" + id + "&orderby=" + <%=AspxTools.JsString(m_sortExpression)%> + '&source=' + <%=AspxTools.JsString(sourcePage)%>);
    }
-->
</script>