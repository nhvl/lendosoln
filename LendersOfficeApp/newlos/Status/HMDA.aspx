<%@ Page language="c#" Codebehind="HMDA.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Status.HMDA" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<%@ Register TagPrefix="HRED" TagName="HmdaRaceEthnicityData" Src="~/newlos/HmdaraceEthnicitydata.ascx" %>
<%@ Register TagPrefix="RELIED" TagName="HmdaReliedOn" Src="~/newlos/HmdaReliedOn.ascx" %>

<!DOCTYPE HTML>
<HTML>
<HEAD runat="server">
    <title>HMDA</title>
    <style>
    .WarningLabelStyle {
        BORDER-RIGHT: black 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: black 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: bold; PADDING-BOTTOM: 5px; MARGIN: 5px; BORDER-LEFT: black 1px solid; COLOR: red; PADDING-TOP: 5px; BORDER-BOTTOM: black 1px solid; BACKGROUND-COLOR: white
    }
    #NCTable tr { padding-left: 5px; }
    .hidden { display: none; }
    body { padding-bottom: 30px; }
    .RadioButtonList { padding-left: 5px; }
    .RadioButtonList label { padding-right: 5px; }
    #HmdaCoApplicantCell {
        padding-left: 25px;
    }
    #sHmdaCoApplicantSourceSsn {
        min-width: 70px;
    }
    .layout-table {
        width: 800px;
    }
    .HmdaDenialReason {
        width: 224px;
    }
     #AvailableReasons, #SelectedReasons {
        height: 175px; 
        width: 310px;
        overflow: auto; 
        border: 1px solid black; 
        background-color: White;
    }
    #AvailableReasons a.up, #AvailableReasons a.down {
        display:none; 
        height: 15px;
    }
    a.up, a.down {
        text-decoration: none;
    }
    a.up img, a.down img {
        height: 9px;
    }
    #AvailableReasons ul, #SelectedReasons  ul {
        list-style: none;
        padding: 0 0 0 10px; 
    }

    .sLegalEntityIdentifier {
        width: 200px;
    }

    .sUniversalLoanIdentifier {
        width: 325px;
    }
</style>
</HEAD>
<body class=EditBackground MS_POSITIONING="FlowLayout">
<script type="text/javascript">
    function doAfterDateFormat(e) {
        if(this.event.type == "keyup" && e.id == "sHmdaActionD") {
            refreshCalculation();
        }
    }
    function _init() {
    lockField(document.getElementById('sUniversalLoanIdentifierLckd'), 'sUniversalLoanIdentifier');
    f_displayWarning('RejectWarningLabel', false);
    f_lockHmdaPropT();
    f_lockHmdaLienT();
    lockHmdaCoApplicantSourceT();
    lockHmdaCoApplicant();
    PopulateHmdaCalculation();
    bindDenialReasons();

    <%= AspxTools.JsGetElementById(sHmdaAprRateSpread) %>.readOnly = !<%= AspxTools.JsGetElementById(sHmdaAprRateSpreadLckd) %>.checked;
    var sHmdaPurchaser2004T = <%= AspxTools.JsGetElementById(sHmdaPurchaser2004T) %>;

    if (sHmdaPurchaser2004T != null) {
        sHmdaPurchaser2004T.disabled = !<%= AspxTools.JsGetElementById(sHmdaPurchaser2004TLckd) %>.checked;
    }

    <%= AspxTools.JsGetElementById(sHmdaPurchaser2015T) %>.disabled = !<%= AspxTools.JsGetElementById(sHmdaPurchaser2015TLckd) %>.checked;
    lockField(<%= AspxTools.JsGetElementById(sAppSubmittedDLckd) %>, 'sAppSubmittedD');  
    lockField(<%= AspxTools.JsGetElementById(sAppReceivedByLenderDLckd) %>, 'sAppReceivedByLenderD');
    LockHmdaPreapprovalT();
    onHmdaCoApplicantSourceTChange(<%=AspxTools.JsGetElementById(this.sHmdaCoApplicantSourceT)%>);

    if (ML.LoanVersionTCurrent < <%=AspxTools.JsNumeric(LendersOffice.Migration.LoanVersionT.V22_HmdaDataPointImprovement) %>) {
        $(".sHmdaActionTakenT").hide();
        $(".postV22").hide();
    } else {
        var combo = $(".sHmdaActionTaken");
        combo.hide();
        combo.next(".combobox_img").hide();

        $(".preV22").hide();
    }
}

    function update2004TVisibility(data) {
        var sHmdaActionDText = $("#sHmdaActionD").val();
        var sHmdaActionDate = new Date(sHmdaActionDText);
        var sHmdaPurchaser2015TDdl = $("#sHmdaPurchaser2015T");
        var today = Date.now();
        var hmdaDataCollectionRequirementChangeDate = new Date(2018, 0, 1, 0, 0, 0, 0);

        var show2004Row = sHmdaActionDate < hmdaDataCollectionRequirementChangeDate || (sHmdaActionDText == "" &&
                    today <= hmdaDataCollectionRequirementChangeDate);

        $(".hmda-purchaser-2004-row").toggle(show2004Row);

        if(data) {
            var lifeInsCreditUnionOption = sHmdaPurchaser2015TDdl.find("option[value='"+ML.LifeInsCreditUnionVal+"']");

            if(data["sHmdaPurchaser2015TLckd"] == "True"){
                //remove the temporary option
                sHmdaPurchaser2015TDdl.find("option.temporary").remove();
            }
            else if(lifeInsCreditUnionOption.length == 0 && data["sHmdaPurchaser2015T"] == ML.LifeInsCreditUnionVal) {
                sHmdaPurchaser2015TDdl.append($("<option class='temporary' value='" + ML.LifeInsCreditUnionVal + "'>" + ML.LifeInsCreditUnionDescription + "</option>"));
                sHmdaPurchaser2015TDdl.val(ML.LifeInsCreditUnionVal);
            }
        }
    }


    function LockHmdaPreapprovalT()
    {
        var sHmdaPreapprovalTLckd = $("#sHmdaPreapprovalTLckd").is(":checked");
        $("#sHmdaPreapprovalT").prop("disabled", !sHmdaPreapprovalTLckd);
    }
function PopulateHmdaCalculation()
{
    var calcMessage = $("#sHmdaAprRateSpreadCalcMessage_Hidden").val();
    $("#sHmdaAprRateSpreadCalcMessage").text(calcMessage);
}

function f_displayWarning(id, bVisible, o) {
    document.getElementById(id).style.display = bVisible ? "" : "none";
    if (typeof o != 'undefined') {
        try { o.focus(); } catch (e) { }
    }
}
function f_lockHmdaPropT() {
  var bLocked = <%= AspxTools.JsGetElementById(sHmdaPropTLckd) %>.checked;
  disableDDL(<%= AspxTools.JsGetElementById(sHmdaPropT) %>, !bLocked);
  var bShowWarning = <%= AspxTools.JsGetElementById(sGseSpT) %>.value  == <%= AspxTools.JsString(E_sGseSpT.Modular.ToString("D")) %>;
  f_displayWarning('sHmdaPropTWarning', bShowWarning);
}

function f_lockHmdaLienT() {
  var bLocked = <%= AspxTools.JsGetElementById(sHmdaLienTLckd) %>.checked;
  disableDDL(<%= AspxTools.JsGetElementById(sHmdaLienT) %>, !bLocked);
  }

function lockHmdaCoApplicantSourceT() {
    var locked = <%=AspxTools.JsGetElementById(this.sHmdaCoApplicantSourceTLckd)%>.checked;
    disableDDL(<%=AspxTools.JsGetElementById(this.sHmdaCoApplicantSourceT)%>, !locked);
}

function onHmdaCoApplicantSourceTChange(dropdown) {
    var coapplicantDropdown = $(<%=AspxTools.JsGetElementById(this.sHmdaCoApplicantSourceSsn)%>);
    var coapplicantSource = $(dropdown).val();

    var args = {
        loanid: <%=AspxTools.JsString(this.LoanID)%>,
        sHmdaCoApplicantSourceT: coapplicantSource
    };

    var selectedSsn = $("#sHmdaCoApplicantSourceSsn").val();

    var result = gService.loanedit.call('GetCoapplicantsForSourceSsn', args);

    if (result.error) {
        alert('Unable to determine co-applicants. Please try again.');
        return;
    }

    coapplicantDropdown.find('option').remove();

    var coapplicants = JSON.parse(result.value.Applicants);
    for (var i = 0; i < coapplicants.length; i++) {
        var coapplicant = coapplicants[i];
        var newOption = $('<option></option>').text(coapplicant.Item1).val(coapplicant.Item2).prop('selected', selectedSsn ? coapplicant.Item2 === selectedSsn : coapplicant.Item3);
        coapplicantDropdown.append(newOption);
    }

    lockHmdaCoApplicant();
}

function lockHmdaCoApplicant() {
    var checkboxValue = <%=AspxTools.JsGetElementById(this.sHmdaCoApplicantSourceSsnLckd)%>.checked;
    var isApplicantOnSecondary = <%=AspxTools.JsGetElementById(this.sHmdaCoApplicantSourceT)%>.value === <%=AspxTools.JsString(DataAccess.sHmdaCoApplicantSourceT.ApplicantOnSecondaryApp)%>;
    var locked = checkboxValue && isApplicantOnSecondary; 
    disableDDL(<%=AspxTools.JsGetElementById(this.sHmdaCoApplicantSourceSsn)%>, !locked);
    $('#sHmdaCoApplicantSourceSsnLckdSpan').toggle(isApplicantOnSecondary);

    if (!isApplicantOnSecondary) {
        $('#sHmdaCoApplicantSourceSsnLckd').prop('checked', false);
    }
}

function f_onclick_sHmdaReportAsHoepaLoan() {
    // If sHmdaReportAsHoepaLoan isn't true, disable related radio buttons.
    var bChecked = $(this).prop('checked');
    var $waiveRescSpan = $('#sNCBorrWaiveRescT');

    if (bChecked) {
        var bNA = $waiveRescSpan.find('input')
            .prop('disabled', false) // enable all inputs
            .last()
            .prop('checked');        // check if last input checked
        $waiveRescSpan.parent()
            .find('.validator')
            .toggle(bNA);            // show validator if N/A
    }
    else {
        $waiveRescSpan.find('input')
            .prop('disabled', true) // disable all inputs
            .last()
            .prop('checked', true)  // check last input
            .end()                  // back to all inputs
            .end()                  // back to span
            .parent()               // td
            .find('.validator')
            .hide();                // hide validator
            
    }
}
function f_onclick_NCRadioButton() {
    var $this = $(this);
    $this.closest('td')
        .find('.validator')
        .toggle($this.val() === '0');
}

function f_onChangeDenialReason(event) {
    if($('#sHmdaDenialReason1').val()) {

        var results = gService.loanedit.call('SetDeniedBy');
        if(!results.error)
        {
            var $sHmdaLoanDenied = $('#sHmdaLoanDenied');
            $sHmdaLoanDenied.prop('checked', (results.value.sHmdaLoanDenied == "True"));
            var $sRejectD = $('#sRejectD');
            if(!$sRejectD.val()) {
                $sRejectD.val(results.value.sRejectD);
            }
            var $sHmdaLoanDeniedBy = $('#sHmdaLoanDeniedBy');
            $sHmdaLoanDeniedBy.val(results.value.sHmdaLoanDeniedBy);
            updateDirtyBit(event);
        }
    }
}

$(function() {
    $(".hmda-purchaser-2015").change(refreshCalculation);
    registerPostRefreshCalculationCallback(update2004TVisibility);
    update2004TVisibility();

    if ($('#NCCallReportPanel').hasClass('hidden')) {
        return;
    }

    
    // Cycle through the spans that contain the radio buttons.
    // Attach event, show validator when non-disabled selection is N/A
    $('.RadioButtonList').each(function(index) {
        var $thisSpan = $(this);
        var $validator = $thisSpan.parent().find('.validator');
        var bDisabled = $thisSpan.find('input:disabled').length > 0;
        var bInvalidSelection = false;
        
        $thisSpan.find('input').each(function(index) {
            var $thisInput = $(this);
            $thisInput.click(f_onclick_NCRadioButton);
            if ($thisInput.prop('checked') === true && $thisInput.val() === '0') {
                bInvalidSelection = true;
            }
        });
        $validator.toggle(!bDisabled && bInvalidSelection);
    });
    
    // Conditional NC Call Report Behavior
    var $hoepaCB = $('#sHmdaReportAsHoepaLoan');
    $hoepaCB.click(f_onclick_sHmdaReportAsHoepaLoan);
    f_onclick_sHmdaReportAsHoepaLoan.call($hoepaCB.get(0));
});

// OPM 451705 - HMDA Denial Reasons scripts.
$(document).on('click', '#AvailableReasons ul a.pick', function() {
    moveLI(this, $('#SelectedReasons ul'), 'remove');
    bindDenialReasons();
    updateDirtyBit();
});

$(document).on('click', '#SelectedReasons ul a.pick', function() {
    moveLI(this, $('#AvailableReasons ul'), 'include');
    bindDenialReasons();
    updateDirtyBit();
});

$(document).on('click', '#SelectedReasons ul a.up', function() {
    var $this = $(this), $lis = $('#SelectedReasons ul').children('li'), $li = $this.parents('li'),
    index = $lis.index($li), $removedNode = $li.remove();
    if (index == 0) {
        $('#SelectedReasons ul').append($removedNode);
    }
    else {
        $lis.eq(index - 1).before($removedNode);
    }
    
    bindDenialReasons();
    updateDirtyBit();
});

$(document).on('click', '#SelectedReasons ul a.down', function() {
    var $this = $(this), $lis = $('#SelectedReasons ul').children('li'), $li = $this.parents('li'),
    index = $lis.index($li), $removedNode = $li.remove();
    if (index + 1 == $lis.length) {
        $('#SelectedReasons ul').prepend($removedNode);
    }
    else {
        $lis.eq(index + 1).after($removedNode);
    }
    
    bindDenialReasons();
    updateDirtyBit();
});

$(document).on('click', '.sHmdaActionTakenT', function () {
    bindDenialReasons();
});

function moveLI(anchor, $dest, anchorText) {
    var $this = $(anchor), $li = $this.parents('li');
    $this.text(anchorText);
    $li.appendTo($dest);
}

function bindDenialReasons()
{
    // Clear text boxes.
    var denialReasons = $('.HmdaDenialReason').not('.OtherReason');
    denialReasons.val('');

    // Set 1st reason to NA if Action Taken is not a denial.
    var actionTaken = $('.sHmdaActionTakenT').val();
    var isDenialAction = actionTaken == <%= AspxTools.JsNumeric(LendersOffice.ObjLib.Hmda.HmdaActionTaken.ApplicationDenied) %>
        || actionTaken == <%= AspxTools.JsNumeric(LendersOffice.ObjLib.Hmda.HmdaActionTaken.PreapprovalRequestDenied) %>;

    if (!isDenialAction) {
        denialReasons[0].value = <%= AspxTools.JsString(Tools.MapHmdaDenialReasonToString(LendersOffice.ObjLib.Hmda.HmdaDenialReason.NotApplicable)) %>;
    }

    // Bind selected reasons.
    var selectedArray = [];
    var isOtherInFirst4 = false;
    var selectedReasonsList = $('#SelectedReasons li span.reason');
    for (var i=0; i < selectedReasonsList.length; i++) {
        var txt = $(selectedReasonsList[i]).text();

        // Only bind denial reasons if HMDA Action Taken is a denial.
        if (isDenialAction && i < 4) {
            if (txt ==  <%= AspxTools.JsString(Tools.MapHmdaDenialReasonToString(LendersOffice.ObjLib.Hmda.HmdaDenialReason.Other)) %>)
            {
                isOtherInFirst4 = true;
            }

            denialReasons[i].value = txt;
        }

        selectedArray.push(txt);
    }

    if (isOtherInFirst4)
    {
        $('.OtherReason').prop('disabled', false);
    } else {
        var otherDesc = $('.OtherReason');
        otherDesc.val('');
        otherDesc.prop('disabled', true);
    }

    $('#sHmdaDenialReasonsList').val(JSON.stringify(selectedArray));

    $("#btnUpdateLei").click(function () {
        var args = {
            loanid: <%=AspxTools.JsString(this.LoanID)%>,
        };

        var result = gService.loanedit.call('UpdateLei', args);

        if (result.error) {
            alert('Failed to update LEI:\n' + result.UserMessage);
            return;
        }

        $('.sLegalEntityIdentifier').val(result.value.sLegalEntityIdentifier);

        refreshCalculation();
    });
}

var oldSaveMe = window.saveMe;
window.saveMe = function(bRefreshScreen)
{
    var resultSave = oldSaveMe(bRefreshScreen);
    if(resultSave)
    {
        window.parent.treeview.location = window.parent.treeview.location;
    }
    return resultSave;
}
</script>

<form id=HMDA method=post runat="server">
<input type="hidden" id="sGseSpT" runat="server" value="" />
<input type="hidden" id="sSpAddr" runat="server" value="" />
<input type="hidden" id="sSpCity" runat="server" value="" />
<input type="hidden" id="sSpCounty" runat="server" value="" />
<input type="hidden" id="sSpState" runat="server" value="" />
<input type="hidden" id="sSpZip" runat="server" value="" />
<input type="text" class="hidden" id="sFannieFips" runat="server" value="" />
<input type="checkbox" class="hidden" id="sFannieFipsLckd" runat="server" value="" />
<input type="hidden" id="sHmdaDenialReasonsList" runat="server" value="" />
<div id="LoanHeaderPopup" class="hidden">
	<div id="LoanHeaderPopupText">&nbsp;</div>
</div> 
<TABLE class=FormTable id=Table2 cellSpacing=0 cellPadding=0 width="100%" 
border=0>
  <TR style="PADDING-LEFT: 5px">
    <TD class=MainRightHeader>HMDA - State Call Report</TD></TR>
  <TR>
    <TD>
      <TABLE class="InsetBorder layout-table" id=Table12 cellSpacing=0 cellPadding=0 
      border=0>
        <TR>
          <TD class=FieldLabel>Loan Information</TD></TR>
        <TR>
          <TD>
            <TABLE id=Table13 cellSpacing=0 cellPadding=0 border=0>
              <TR>
                <TD class=FieldLabel noWrap colSpan=2><asp:checkbox id=sHmdaExcludedFromReport runat="server" Text="Exclude loan from HMDA report"></asp:checkbox></TD></TR>
              <TR>
                <TD class=FieldLabel noWrap colSpan=2>
                    <asp:checkbox id=sHmdaReportAsHomeImprov runat="server" Text="Report the purpose of this loan as home improvement (1-4 family)"></asp:checkbox>
                </TD>
              </TR>
                <tr>
                    <td class="FieldLabel" nowrap colspan="2">
                        <asp:CheckBox ID="sHmdaLenderHasPreapprovalProgram" runat="server" Text="Lender has preapproval program as defined by the HMDA" onchange="refreshCalculation();" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap colspan="2">
                        <asp:CheckBox ID="sHmdaBorrowerRequestedPreapproval" runat="server" Text="Borrower requested preapproval" onchange="refreshCalculation();"/>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap colspan="2">
                        <asp:CheckBox ID="sReportFhaGseAusRunsAsTotalRun" runat="server" Text="For HMDA Reporting, report DU and LPA runs on FHA loans as TOTAL Scorecard runs" onchange="refreshCalculation();"/>
                    </td>
                </tr>
              <tr>
                <td class=FieldLabel noWrap>Preapproval</TD>
                <td>
                    <asp:dropdownlist id=sHmdaPreapprovalT runat="server"></asp:dropdownlist>
                    <asp:CheckBox ID="sHmdaPreapprovalTLckd" runat="server" Text="Locked" onchange="refreshCalculation();" />
                </TD>
              </TR>
              <tr>
                <td class=FieldLabel noWrap>APR Rate Spread</TD>
                <td class="FieldLabel"><asp:textbox id=sHmdaAprRateSpread runat="server" Width="75px" /><asp:CheckBox ID="sHmdaAprRateSpreadLckd" runat="server" onclick="refreshCalculation();" Text="Locked"/></TD>
              </TR>
                <tr>
                    <td colspan="2">
                        <span id="sHmdaAprRateSpreadCalcMessage"></span>
                        <asp:HiddenField runat="server" ID="sHmdaAprRateSpreadCalcMessage_Hidden" />
                    </td>
                </tr>
              <tr>
                <td class=FieldLabel noWrap><asp:checkbox id=sHmdaReportAsHoepaLoan runat="server" Text="HOEPA loan" /></TD>
                <td ></TD>
              </TR>
              <tr>
                  <td class="FieldLabel">Co-Applicant Source</td>
                  <td>
                      <asp:DropDownList ID="sHmdaCoApplicantSourceT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                      <asp:CheckBox ID="sHmdaCoApplicantSourceTLckd" runat="server" Text="Locked" onchange="refreshCalculation();" />
                  </td>
              </tr>
              <tr>
                  <td id="HmdaCoApplicantCell" class="FieldLabel">HMDA Co-Applicant</td>
                  <td>
                      <asp:DropDownList ID="sHmdaCoApplicantSourceSsn" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                      <span id="sHmdaCoApplicantSourceSsnLckdSpan"><asp:CheckBox ID="sHmdaCoApplicantSourceSsnLckd" runat="server" Text="Locked" onchange="refreshCalculation();" /></span>
                  </td>
              </tr>
              <tr>
                  <td class="FieldLabel" colspan="2">
                      <asp:checkbox id="sHmdaIsSecuredByDwelling" runat="server" Text="Is secured by dwelling" />
                  </td>
              </tr>
                <tr>
                    <td class="FieldLabel">Legal Entity Identifier</td>
                    <td>
                        <asp:TextBox ID="sLegalEntityIdentifier" runat="server" MaxLength="20" CssClass="sLegalEntityIdentifier" ReadOnly="true"></asp:TextBox>
                        <input type="button" id="btnUpdateLei" value="Update LEI" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">Universal Loan Identifier (ULI)</td>
                    <td>
                        <input id="sUniversalLoanIdentifier" type="text" class="sUniversalLoanIdentifier" maxlength="45" runat="server" /> 
                        <input id="sUniversalLoanIdentifierLckd" type="checkbox" class="sUniversalLoanIdentifierLckd" runat="server" onclick="refreshCalculation();" /> 
                        <label class="vertical-align-label">Lock</label>
                    </td>
                </tr>

            </TABLE></TD></TR></TABLE></TD></TR>
  <TR>
    <TD></TD></TR>
  <TR>
    <TD>
      <TABLE class="InsetBorder layout-table" id=Table8 cellSpacing=0 cellPadding=0 
      border=0>
        <TR>
          <TD class=FieldLabel noWrap>Property 
          Information</TD></TR>
        <TR>
          <TD noWrap>
            <TABLE cellSpacing=0 cellPadding=0 border=0>
              <TR>
                <TD class=FieldLabel noWrap>MSA number 
</TD>
                <TD width=8></TD>
                <TD noWrap><asp:textbox id=sHmdaMsaNum runat="server" Width="225px"></asp:textbox>&nbsp;&nbsp;<asp:Button ID="bLookupGeoCode" runat="server" Text="Import Geocodes" OnClientClick="return importGeoCodes();"/></TD></TR>
              <TR>
                <TD class=FieldLabel noWrap>County 
code</TD>
                <TD></TD>
                <TD noWrap><asp:textbox id=sHmdaCountyCode runat="server" Width="224px"></asp:textbox></TD></TR>
              <tr>
                <td class=FieldLabel noWrap>State code</TD>
                <TD></TD>
                <td noWrap><asp:textbox id=sHmdaStateCode runat="server" Width="224px"></asp:textbox></TD></TR>
              <TR>
                <TD class=FieldLabel noWrap>Census 
                tract</TD>
                <TD></TD>
                <TD noWrap><asp:textbox id=sHmdaCensusTract runat="server" Width="224px" DESIGNTIMEDRAGDROP="247"></asp:textbox></TD></TR>
              <tr>
                <td class=FieldLabel noWrap>Lien status</TD>
                <TD></TD>
                <td noWrap class="FieldLabel"><asp:dropdownlist id=sHmdaLienT runat="server"></asp:dropdownlist>&nbsp;<asp:CheckBox ID="sHmdaLienTLckd" runat="server" onclick="refreshCalculation();" Text="Locked"/></TD></TR>
              <tr>
                <td class=FieldLabel noWrap>Property type</TD>
                <TD></TD>
                <td noWrap class="FieldLabel"><asp:dropdownlist id=sHmdaPropT runat="server"></asp:dropdownlist>&nbsp;<asp:CheckBox ID="sHmdaPropTLckd" runat="server" onclick="refreshCalculation();" Text="Locked"/></TD></TR>
                <tr id="sHmdaPropTWarning"><td colspan="3" class="WarningLabelStyle">
                    WARNING:&nbsp;Modular properties that meet HUD standards may fall under the Manufactured category. See the HMDA LAR guide for more info.
                </td></tr>
                </TABLE></TD></TR></TABLE></TD></TR>
  <TR>
    <TD>
      <TABLE class="InsetBorder layout-table" id=Table9 cellSpacing=0 cellPadding=0 
      border=0>
        <TR>
          <TD class=FieldLabel noWrap>Origination 
            Information</TD></TR>
        <TR>
          <TD noWrap>
            <TABLE cellSpacing=0 cellPadding=0 border=0>
              <tr runat="server" class="hmda-purchaser-2004-row" id="HmdaPurchaser2004TRow">
                <td class=FieldLabel>Type of purchaser (2004)</TD>
                <TD width=8></TD>
                <td class="FieldLabel"><asp:dropdownlist id=sHmdaPurchaser2004T runat="server"></asp:dropdownlist><asp:CheckBox ID="sHmdaPurchaser2004TLckd" runat="server" onclick="refreshCalculation();" Text="Locked"/></TD></tr>
              <tr>
                <td class=FieldLabel>Type of purchaser (2015)</TD>
                <TD width=8></TD>
                <td class="FieldLabel"><asp:dropdownlist CssClass="hmda-purchaser-2015" id=sHmdaPurchaser2015T runat="server"></asp:dropdownlist><asp:CheckBox ID="sHmdaPurchaser2015TLckd" runat="server" onclick="refreshCalculation();" Text="Locked"/></TD>
              </TR>
              <TR>
                <TD class=FieldLabel>Action taken</TD>
                <TD></TD>
                <TD>
                    <ml:combobox id=sHmdaActionTaken runat="server" Width="230px" onchange="refreshCalculation();" CssClass="sHmdaActionTaken"></ml:combobox>
                    <asp:DropDownList ID="sHmdaActionTakenT" runat="server" CssClass="sHmdaActionTakenT"></asp:DropDownList>
                </TD>
              </TR>
              <TR>
                <TD class=FieldLabel>Action date</TD>
                <TD></TD>
                <TD><ml:datetextbox id=sHmdaActionD runat="server" CssClass="mask" onchange="refreshCalculation();" preset="date" width="75"></ml:datetextbox></TD></TR>
                <td class="FieldLabel"></td>
                
                
                </TABLE></TD></TR></TABLE></TD></TR>
    <TR>
    <TD>
        <RELIED:HmdaReliedOn runat="server" id="HmdaReliedOn"></RELIED:HmdaReliedOn>
    </TD>
    </TR>

    <tr>
        <td>
            <table class="InsetBorder layout-table" id="Table10" cellspacing="0" cellpadding="0"
                border="0">
                <tr>
                    <td class="FieldLabel">Denial Reason(s)</td>
                </tr>
                <tr class="preV22">
                    <td>
                        <table id="Table6" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td class="FieldLabel" nowrap>Denial Reason #1&nbsp; </td>
                                <td nowrap>
                                    <ml:ComboBox ID="sHmdaDenialReason1" runat="server" Width="224px" onchange="f_onChangeDenialReason(event);"></ml:ComboBox></td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" nowrap>Denial Reason #2</td>
                                <td nowrap>
                                    <ml:ComboBox ID="sHmdaDenialReason2" runat="server" Width="224px"></ml:ComboBox></td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" nowrap>Denial Reason #3</td>
                                <td nowrap>
                                    <ml:ComboBox ID="sHmdaDenialReason3" runat="server" Width="224px"></ml:ComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" nowrap>Denial Reason #4</td>
                                <td nowrap>
                                    <ml:ComboBox ID="sHmdaDenialReason4" runat="server" Width="224px"></ml:ComboBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="postV22">
                    <td colspan="2">
                        <table>
                            <tr id="trFields">
                                <td nowrap>
                                    Available Reasons
                                    <div id="AvailableReasons" runat="server">
                                        <asp:Repeater runat="server" ID="availableRepeater" OnItemDataBound="FieldRepeater_OnItemDataBound">
                                            <HeaderTemplate><ul></HeaderTemplate>
                                            <FooterTemplate></ul></FooterTemplate>
                                            <ItemTemplate>
                                                <li ID="listItem" runat="server">
                                                    <a class="up"><asp:Image runat="server" ImageUrl="~/images/up-blue.gif" /> </a>
                                                    <a class="down"><asp:Image runat="server" ImageUrl="~/images/down-blue.gif" /> </a>
                                                    <a runat="server" class="pick">include</a>
                                                    <span class="reason" ><ml:EncodedLiteral runat="server" ID="description"></ml:EncodedLiteral></span>
                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </td>
                                <td nowrap>
                                    Selected Reasons
                                    <div id="SelectedReasons" runat="server">
                                        <asp:Repeater runat="server" ID="selectedRepeater" OnItemDataBound="FieldRepeater_OnItemDataBound">
                                            <HeaderTemplate><ul></HeaderTemplate>
                                            <FooterTemplate></ul></FooterTemplate>
                                            <ItemTemplate>
                                                <li ID="listItem" runat="server">
                                                    <a class="up"><asp:Image runat="server" ImageUrl="~/images/up-blue.gif" /> </a>
                                                    <a class="down"><asp:Image runat="server" ImageUrl="~/images/down-blue.gif" /> </a>
                                                    <a class="pick">remove</a>
                                                    <span class="reason" ><ml:EncodedLiteral runat="server" ID="description"></ml:EncodedLiteral></span>
                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="postV22">
                    <td>Denial Reason #1</td>
                    <td><input type="text" id="sHmdaDenialReason1_2" runat="server" class="HmdaDenialReason" data-id="sHmdaDenialReason1" disabled="disabled" /></td>
                </tr>
                <tr class="postV22">
                    <td>Denial Reason #2</td>
                    <td><input type="text" id="sHmdaDenialReason2_2" runat="server" class="HmdaDenialReason" data-id="sHmdaDenialReason2" disabled="disabled" /></td>
                </tr>
                <tr class="postV22">
                    <td>Denial Reason #3</td>
                    <td><input type="text" id="sHmdaDenialReason3_2" runat="server" class="HmdaDenialReason" data-id="sHmdaDenialReason3" disabled="disabled" /></td>
                </tr>
                <tr class="postV22">
                    <td>Denial Reason #4</td>
                    <td><input type="text" id="sHmdaDenialReason4_2" runat="server" class="HmdaDenialReason" data-id="sHmdaDenialReason4" disabled="disabled" /></td>
                </tr>
                <tr class="postV22">
                    <td>Other Denial Reason Description</td>
                    <td><input type="text" id="sHmdaDenialReasonOtherDescription" runat="server" class="HmdaDenialReason OtherReason" maxlength="255" disabled="disabled" /></td>
                </tr>
            </table>
        </td>
    </tr>
  <TR>
    <TD>
      <TABLE class="InsetBorder layout-table" id=Table11 cellSpacing=0 cellPadding=0 
      border=0>
        <TR>
          <TD class=FieldLabel>Denial / Counter Offer 
            Tracking</TD></TR>
        <TR>
          <TD>
            <TABLE id=Table1 cellSpacing=0 cellPadding=0 border=0 
            >
              <TR>
                <TD class=WarningLabelStyle id=RejectWarningLabel
                colSpan=4>WARNING: &nbsp;Modifying the denied date 
                  could remove your permission to edit this loan. &nbsp;Please make sure you complete the Credit Denial form before you enter a denied date here.</TD></TR>
              <TR>
                <TD noWrap><asp:checkbox id=sHmdaLoanDenied runat="server" Text="Loan was denied" DESIGNTIMEDRAGDROP="85"></asp:checkbox></TD>
                <TD noWrap>Denied date&nbsp; &nbsp; </TD>
                <TD noWrap><ml:datetextbox id=sRejectD onblur="f_displayWarning('RejectWarningLabel', false);" onfocus="f_displayWarning('RejectWarningLabel', true);" runat="server" CssClass="mask" preset="date" width="75" HelperAlign="AbsMiddle"></ml:datetextbox></TD>
                <TD noWrap>&nbsp;by <asp:textbox id=sHmdaLoanDeniedBy runat="server" Width="159" DESIGNTIMEDRAGDROP="114"></asp:textbox></TD></TR>
              <TR>
                <TD noWrap><asp:checkbox id=sHmdaDeniedFormDone runat="server" Text="Credit denial form completed"></asp:checkbox>&nbsp;&nbsp;</TD>
                <TD noWrap>Mailed date </TD>
                <TD noWrap><ml:datetextbox id=sHmdaDeniedFormDoneD runat="server" CssClass="mask" preset="date" width="75" HelperAlign="AbsMiddle"></ml:datetextbox></TD>
                <TD noWrap>&nbsp;by <asp:textbox id=sHmdaDeniedFormDoneBy runat="server" Width="159px" DESIGNTIMEDRAGDROP="118"></asp:textbox></TD></TR>
              <TR>
                <TD noWrap><asp:checkbox id=sHmdaCounterOfferMade runat="server" Text="Counter offer made"></asp:checkbox></TD>
                <TD noWrap>Made date </TD>
                <TD noWrap><ml:datetextbox id=sHmdaCounterOfferMadeD runat="server" CssClass="mask" preset="date" width="75" HelperAlign="AbsMiddle"></ml:datetextbox></TD>
                <TD noWrap>&nbsp;by <asp:textbox id=sHmdaCounterOfferMadeBy runat="server" Width="159px" DESIGNTIMEDRAGDROP="120"></asp:textbox></TD></TR>
              <TR>
                <TD class=FieldLabel noWrap colSpan=4 
                  >Counter offer details:</TD></TR>
              <TR>
                <TD noWrap colSpan=4><asp:textbox id=sHmdaCounterOfferDetails runat="server" Width="490px" TextMode="MultiLine" Height="83px"></asp:textbox></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
  <TR>
    <TD>
      <TABLE class=InsetBorder id=Table3 cellSpacing=0 cellPadding=0 width=800
      border=0>
        <TR>
          <TD class=FieldLabel noWrap>Information for 
            Government Tracking</TD>
        </TR>
        <tr>
          <td>
              <HRED:HmdaRaceEthnicityData runat="server" ID="HREData" />
          </td>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
<div id="NCCallReportPanel" runat="server">
    <span class="MainRightHeader" style="width: 100%">State-Specific Reporting - NC</span>
    <table class="FormTable" id="NCTable" cellspacing="0" cellpadding="0">
        <tr>
            <td></td>
            <td class="FieldLabel">Date</td>
        </tr>
        <tr>
            <td class="FieldLabel">App Submitted</td>
            <td><ml:DateTextBox ID="sAppSubmittedD" runat="server" onchange="refreshCalculation();"></ml:DateTextBox></td>
            <td><asp:CheckBox ID="sAppSubmittedDLckd" runat="server" onclick="refreshCalculation();" />Lock</td>
        </tr>
        <tr>
            <td class="FieldLabel">App Received by Lender</td>
            <td><ml:DateTextBox ID="sAppReceivedByLenderD" runat="server"></ml:DateTextBox></td>
            <td><asp:CheckBox ID="sAppReceivedByLenderDLckd" runat="server" onclick="refreshCalculation();" />Lock</td>
        </tr>
        <tr>
            <td class="FieldLabel">Initial GFE Disclosure</td>
            <td><ml:DateTextBox ID="sGfeInitialDisclosureD" runat="server"></ml:DateTextBox></td>
        </tr>
        <tr>
            <td class="FieldLabel">Initial TIL Disclosure</td>
            <td><ml:DateTextBox ID="sTilInitialDisclosureD" runat="server"></ml:DateTextBox></td>
        </tr>
        <tr>
            <td class="FieldLabel">CHARM Booklet Provided</td>
            <td><ml:DateTextBox ID="sCHARMBookletProvidedD" runat="server"></ml:DateTextBox></td>
        </tr>
        <tr>
            <td class="FieldLabel">HUD Special Info Booklet Provided</td>
            <td><ml:DateTextBox ID="sHUDSpecialInfoBookletProvidedD" runat="server"></ml:DateTextBox></td>
        </tr>
        <tr>
            <td class="FieldLabel">Affiliated Business Disclosure Provided</td>
            <td><ml:DateTextBox ID="sAffiliatedBusinessDisclosureProvidedD" runat="server"></ml:DateTextBox></td>
        </tr>
        <tr><td>&nbsp</td></tr>
        <tr>
            <td class="FieldLabel">Loan Originator NMLS ID</td>
            <td><asp:TextBox ID="sApp1003InterviewerLoanOriginatorIdentifier" runat="server" ReadOnly="true"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="FieldLabel">Loan Origination Company NMLS ID</td>
            <td><asp:TextBox ID="sApp1003InterviewerLoanOriginationCompanyIdentifier" runat="server" ReadOnly="true"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="FieldLabel">Branch Manager NMLS ID</td>
            <td><asp:TextBox ID="sBranchManagerNMLSIdentifier" MaxLength="10" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="FieldLabel">Lender NMLS ID</td>
            <td><asp:TextBox ID="LenderNMLSId" runat="server"></asp:TextBox></td>
        </tr>
        <tr><td>&nbsp</td></tr>
        <tr>
            <td class="FieldLabel">Prepayment Penalty Percentage</td>
            <td><ml:PercentTextBox ID="sPpmtPenaltyPc" runat="server"></ml:PercentTextBox></td>
        </tr>
        <tr>
            <td class="FieldLabel">Undiscounted Interest Rate</td>
            <td><ml:PercentTextBox ID="sUndiscountedIR" runat="server"></ml:PercentTextBox></td>
        </tr>
        <tr>
            <td class="FieldLabel">Yield Spread Premium Amount</td>
            <td><ml:MoneyTextBox ID="sYSPAmt" runat="server"></ml:MoneyTextBox></td>
        </tr>
        <tr><td>&nbsp</td></tr>
        <tr>
            <td class="FieldLabel" colspan="2">Did Borrower waive 3 day rescission disclosure rule for HOEPA Provisions?</td>
            <td>
                <asp:RadioButtonList ID="sNCBorrWaiveRescT" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="RadioButtonList"></asp:RadioButtonList>
                <img id="sNCBorrWaiveRescTValidator" alt="" class="validator" src="../../images/require_icon.gif" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" colspan="2">Did borrower receive counseling for a high cost loan?</td>
            <td>
                <asp:RadioButtonList ID="sNCBorrRecvCounselT" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="RadioButtonList"></asp:RadioButtonList>
                <img id="sNCBorrRecvCounselTValidator" alt="" class="validator" src="../../images/require_icon.gif" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" colspan="2">Were home improvement contractor payments made from loan proceeds?</td>
            <td>
                <asp:RadioButtonList ID="sNCHomeImprovPmtsMadeT" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="RadioButtonList"></asp:RadioButtonList>
                <img id="sNCHomeImprovPmtsMadeTValidator" alt="" class="validator" src="../../images/require_icon.gif" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" colspan="2">Do the note terms allow the interest rate to rise after a default?</td>
            <td>
                <asp:RadioButtonList ID="sNCAllowIRToRiseT" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="RadioButtonList"></asp:RadioButtonList>
                <img id="sNCAllowIRToRiseTValidator" alt="" class="validator" src="../../images/require_icon.gif" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" colspan="2">Is the size of the lot 25 or more acres?</td>
            <td>
                <asp:RadioButtonList ID="sNC25OrMoreAcresT" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="RadioButtonList"></asp:RadioButtonList>
                <img id="sNC25OrMoreAcresTValidator" alt="" class="validator" src="../../images/require_icon.gif" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" colspan="2">Is this loan a refinance of a previous loan with the same lender?</td>
            <td>
                <asp:RadioButtonList ID="sNCRefiWithSameLenderT" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="RadioButtonList"></asp:RadioButtonList>
                <img id="sNCRefiWithSameLenderTValidator" alt="" class="validator" src="../../images/require_icon.gif" />
            </td>
        </tr>
        <tr><td>&nbsp</td></tr>
        <tr id="RefinancePurposeRow" runat="server">
            <td class="FieldLabel">Purpose of Refinance</td>
            <td><ml:ComboBox ID="sRefPurpose" runat="server"></ml:ComboBox></td>
        </tr>
    </table>
</div>
</FORM>
	
  </body>
</HTML>
