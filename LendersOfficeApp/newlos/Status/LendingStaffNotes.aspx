﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LendingStaffNotes.aspx.cs" Inherits="LendersOfficeApp.newlos.Status.LendingStaffNotes" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %> 
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Lending Staff Notes</title>
    <style type="text/css">
        body { background-color: gainsboro; }
        .bigNoteField { width: 600px; height: 400px; }
        .wrapper { padding: 5px; }
        span
            {
                font-weight: bold;
                font-size: 11px;
                color: black;
                font-family: Arial, Helvetica, sans-serif;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="MainRightHeader">Lending Staff Notes</div>
    <div class="wrapper">
        <table cellpadding="2" border="0">
            <tr>
                <td>
                    <span>Lending Staff Notes</span>&nbsp;&nbsp;
                    <input type="button" class="ButtonStyle"  value="Date &amp; Time Stamp" id="DateAndAmpTimeStamp" onclick="f_WriteDateAndTimeStamp()" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="sLendingStaffNotes" TextMode="MultiLine" CssClass="bigNoteField" runat="server"  ></asp:TextBox>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
    <script type="text/javascript" >
            function createTimeStampString(currentTime, userName){
                var hours = currentTime.getHours();
                var minutes = currentTime.getMinutes(); 

                if (minutes < 10) {
                    minutes = '0' + minutes;
                }

                var month = currentTime.getMonth() + 1; 
                var day = currentTime.getDate(); 
                var year = currentTime.getFullYear();
                var pm = hours > 11 ? 'PM' : 'AM';
                hours = hours % 12;
                if (hours == 0)
                    hours = 12;
                    
                var date = [month,'/',day,'/',year,' ',hours,':',minutes, ' ', pm];
                
                var carriageReturn = (getBrowserVersion() > 9 )? '\n' : '\r';
                
                var stampString =  [date.join(''), ' [', userName, '] - ', carriageReturn + carriageReturn].join('');
                return stampString;
            }

            function f_WriteDateAndTimeStamp(){
                var stampString = createTimeStampString(new Date(),  <%= AspxTools.JsString(BrokerUser.FirstName + " " + BrokerUser.LastName) %> );
                var $note = $('#sLendingStaffNotes');
                $note.val(stampString + $note.val());
                updateDirtyBit();
            }
    </script>
</body>
</html>
