﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AlternateLenderPopup.aspx.cs" Inherits="LendersOfficeApp.newlos.Status.AlternateLenderPopup" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Alternate Lender List</title>
    <style>
        .FullWidth {
            width: 34em;
        }
        .TitleWidth 
        {
        	width: 15em;
        }
    </style>
</head>
<script language="javascript">    
    function _init() {
        resize(330, 850);
        checkAllFields();
    }

    function unCheckedAll() {
        document.getElementById("sBeneficiaryState").disabled = false;
        document.getElementById("sBeneficiaryNonPersonEntity").disabled = false;
        document.getElementById("sLossPayeeState").disabled = false;
        document.getElementById("sMakePaymentsToState").disabled = false;
        document.getElementById("sWhenRecordedMailToState").disabled = false;
    }

    function checkAllFields() {
        var flag = true;
        var fields = new Array("sLender", "sBeneficiary", "sLossPayee", "sMakePaymentsTo", "sWhenRecordedMailTo");
        for (var i = 0; i < fields.length; i++) {
            flag = flag & (document.getElementById(fields[i] + "Name").value != "")
                        & (document.getElementById(fields[i] + "Address").value != "")
                        & (document.getElementById(fields[i] + "City").value != "")
                        & (document.getElementById(fields[i] + "State").value != "")
                        & (document.getElementById(fields[i] + "Zipcode").value != "");
        }

        if (flag == false)
            document.getElementById("Save_Btn").disabled = true;
        else
            document.getElementById("Save_Btn").disabled = false;
    }

    function updateValues() {
        var types = ["sBeneficiary", "sLossPayee", "sMakePaymentsTo", "sWhenRecordedMailTo"];
        for (var i = 0; i < 4; i++) {
            //alert(types[i] + document.getElementById(types[i].checked));
            if (document.getElementById(types[i]).checked == true) {
                document.getElementById(types[i] + "Name").value = document.getElementById("sLenderName").value;            
                document.getElementById(types[i] + "Address").value = document.getElementById("sLenderAddress").value;
                document.getElementById(types[i] + "City").value = document.getElementById("sLenderCity").value;
                document.getElementById(types[i] + "State").value = document.getElementById("sLenderState").value;
                document.getElementById(types[i] + "Zipcode").value = document.getElementById("sLenderZipcode").value;
            }
        }
    }

    function copyFromLender(obj) {
        if ((document.getElementById(obj.id)).checked == true) {
            document.getElementById(obj.id + "Name").value = document.getElementById("sLenderName").value;            
            document.getElementById(obj.id + "Address").value = document.getElementById("sLenderAddress").value;
            document.getElementById(obj.id + "City").value = document.getElementById("sLenderCity").value;
            document.getElementById(obj.id + "State").value = document.getElementById("sLenderState").value;
            document.getElementById(obj.id + "Zipcode").value = document.getElementById("sLenderZipcode").value;
            if (obj.id == "sBeneficiary") {
                document.getElementById(obj.id + "NonPersonEntity").checked = document.getElementById("sLenderNonPersonEntity").checked;
                document.getElementById(obj.id + "NonPersonEntity").disabled = true;
            }

            document.getElementById(obj.id + "Name").readOnly = true;
            document.getElementById(obj.id + "Address").readOnly = true;
            document.getElementById(obj.id + "City").readOnly = true;
            document.getElementById(obj.id + "State").disabled = true;
            document.getElementById(obj.id + "Zipcode").readOnly = true;
        }
        else {
            document.getElementById(obj.id + "Name").readOnly = false;
            document.getElementById(obj.id + "Address").readOnly = false;
            document.getElementById(obj.id + "City").readOnly = false;
            document.getElementById(obj.id + "State").disabled = false;
            document.getElementById(obj.id + "Zipcode").readOnly = false;
            
            if (obj.id == "sBeneficiary")
                document.getElementById(obj.id + "NonPersonEntity").disabled = false;
        }
        checkAllFields();
    }
</script>
<body bgcolor="gainsboro">    
    <h4 class="page-header">Alternate Lender</h4>
    <form id="FloodCertification" runat="server">
    	<table cellSpacing="0" cellPadding="0">
    	    <tr>
			    <td colspan="2" style="font-weight: bold; text-align: right; color: red;">
			        *All Fields are Required
			    </td>
			</tr>
            <tr>
                <td colspan="2">
                    <table class="InsetBorder FieldLabel" cellSpacing="0" cellPadding="0" width="99%" border="0">
                        <tr>
                            <td>
                                Alternate Lender ID
                                <asp:TextBox ID="DocMagicAlternateLenderInternalId" Width="100" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
			<tr>
				<td class="MainRightHeader" noWrap colspan="2">
				    Lender
				</td>
			</tr>
			<tr>
			    <td>
			        <table ID="sLender" class="InsetBorder FieldLabel" cellSpacing="0" cellPadding="0" width="99%" border="0">			            
			            <tr>
			                <td>Name </td>
			                <td>
                                 <asp:TextBox ID="sLenderName" Width="235" runat="server" onblur="checkAllFields();" onchange="updateValues();" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
			                <td>Address </td>
			                <td>
                                 <asp:TextBox ID="sLenderAddress" Width="235" runat="server" onblur="checkAllFields();" onchange="updateValues();" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
			                <td>City </td>
			                <td>
                                 <asp:TextBox ID="sLenderCity" Width="145" runat="server" onblur="checkAllFields();" onchange="updateValues();"></asp:TextBox>
                                 <ml:StateDropDownList ID="sLenderState" runat="server" IncludeTerritories="false" onblur="checkAllFields();" onchange="updateValues();">                                    
                                 </ml:StateDropDownList>
                                 <ml:zipcodetextbox id="sLenderZipcode" runat="server" preset="zipcode" width="40" onblur="checkAllFields();" onchange="updateValues();"></ml:zipcodetextbox>
                            </td>
                        </tr>
                         <tr>
			                <td colspan="2"><asp:CheckBox ID="sLenderNonPersonEntity" Width="200" runat="server" Text="Non-Person Entity"></asp:CheckBox> </td>			    			                
                        </tr>  
                   </table>
                </td>
            </tr>                  		
		</table>
		
		<table cellSpacing="0" cellPadding="0">
			<tr>
				<td class="MainRightHeader" noWrap colspan="2">
				    Beneficiary
				</td>
			</tr>
			<tr>
			    <td>
			        <table class="InsetBorder FieldLabel" cellSpacing="0" cellPadding="0" width="99%" border="0">
			            <tr>
			                <td colspan="2"><asp:CheckBox ID="sBeneficiary" Width="200" runat="server" onclick="copyFromLender(this);" Text="Copy from Lender"></asp:CheckBox> </td>			    			                
                        </tr>  
			            <tr>
			                <td>Name </td>
			                <td>
                                 <asp:TextBox ID="sBeneficiaryName" Width="235" runat="server" onblur="checkAllFields();"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
			                <td>Address </td>
			                <td>
                                 <asp:TextBox ID="sBeneficiaryAddress" Width="235" runat="server" onblur="checkAllFields();"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
			                <td>City </td>
			                <td>
                                 <asp:TextBox ID="sBeneficiaryCity" Width="145" runat="server" onblur="checkAllFields();"></asp:TextBox>
                                 <ml:StateDropDownList ID="sBeneficiaryState" runat="server" IncludeTerritories="false" onblur="checkAllFields();">                                    
                                 </ml:StateDropDownList>
                                 <ml:zipcodetextbox id="sBeneficiaryZipcode" runat="server" preset="zipcode" width="40" onblur="checkAllFields();"></ml:zipcodetextbox>
                            </td>
                        </tr>
                        <tr>
			                <td colspan="2"><asp:CheckBox ID="sBeneficiaryNonPersonEntity" Width="200" runat="server" Text="Non-Person Entity"></asp:CheckBox> </td>			    			                
                        </tr>  
                   </table>
                </td>
            </tr>                  		
		</table>
		
		<table cellSpacing="0" cellPadding="0">
			<tr>
				<td class="MainRightHeader" noWrap colspan="2">
				    Loss Payee
				</td>
			</tr>
			<tr>
			    <td>
			        <table class="InsetBorder FieldLabel" cellSpacing="0" cellPadding="0" width="99%" border="0">
			            <tr>
			                <td colspan="2"><asp:CheckBox ID="sLossPayee" Width="200" runat="server" onclick="copyFromLender(this);" Text="Copy from Lender"></asp:CheckBox> </td>			    			                
                        </tr>  
			            <tr>
			                <td>Name </td>
			                <td>
                                 <asp:TextBox ID="sLossPayeeName" Width="235" runat="server" onblur="checkAllFields();"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
			                <td>Address </td>
			                <td>
                                 <asp:TextBox ID="sLossPayeeAddress" Width="235" runat="server" onblur="checkAllFields();"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
			                <td>City </td>
			                <td>
                                 <asp:TextBox ID="sLossPayeeCity" Width="145" runat="server" onblur="checkAllFields();"></asp:TextBox>
                                 <ml:StateDropDownList ID="sLossPayeeState" runat="server" IncludeTerritories="false" onblur="checkAllFields();">                                    
                                 </ml:StateDropDownList>                                 
                                 <ml:zipcodetextbox id="sLossPayeeZipcode" runat="server" preset="zipcode" width="40" onblur="checkAllFields();"></ml:zipcodetextbox>
                            </td>
                        </tr>
                   </table>
                </td>
            </tr>                  		
		</table>
		
		<table cellSpacing="0" cellPadding="0">
			<tr>
				<td class="MainRightHeader" noWrap colspan="2">
				    Make Payments To
				</td>
			</tr>
			<tr>
			    <td>
			        <table class="InsetBorder FieldLabel" cellSpacing="0" cellPadding="0" width="99%" border="0">
			            <tr>
			                <td colspan="2"><asp:CheckBox ID="sMakePaymentsTo" Width="200" runat="server" onclick="copyFromLender(this);" Text="Copy from Lender"></asp:CheckBox> </td>			    			                
                        </tr>  
			            <tr>
			                <td>Name </td>
			                <td>
                                 <asp:TextBox ID="sMakePaymentsToName" Width="235" runat="server" onblur="checkAllFields();"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
			                <td>Address </td>
			                <td>
                                 <asp:TextBox ID="sMakePaymentsToAddress" Width="235" runat="server" onblur="checkAllFields();"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
			                <td>City </td>
			                <td>
                                 <asp:TextBox ID="sMakePaymentsToCity" Width="145" runat="server" onblur="checkAllFields();"></asp:TextBox>
                                 <ml:StateDropDownList ID="sMakePaymentsToState" runat="server" IncludeTerritories="false" onblur="checkAllFields();">                                    
                                 </ml:StateDropDownList>
                                 <ml:zipcodetextbox id="sMakePaymentsToZipcode" runat="server" preset="zipcode" width="40" onblur="checkAllFields();"></ml:zipcodetextbox>
                            </td>
                        </tr>
                   </table>
                </td>
            </tr>                  		
		</table>
		
		<table cellSpacing="0" cellPadding="0">
			<tr>
				<td class="MainRightHeader" noWrap colspan="2">
				    When Recorded Mail To
				</td>
			</tr>
			<tr>
			    <td>
			        <table class="InsetBorder FieldLabel" cellSpacing="0" cellPadding="0" width="99%" border="0">
			            <tr>
			                <td colspan="2"><asp:CheckBox ID="sWhenRecordedMailTo" Width="200" runat="server" onclick="copyFromLender(this);" Text="Copy from Lender"></asp:CheckBox> </td>			    			                
                        </tr>  
			            <tr>
			                <td>Name </td>
			                <td>
                                 <asp:TextBox ID="sWhenRecordedMailToName" Width="235" runat="server" onblur="checkAllFields();" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
			                <td>Address </td>
			                <td>
                                 <asp:TextBox ID="sWhenRecordedMailToAddress" Width="235" runat="server" onblur="checkAllFields();"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
			                <td>City </td>
			                <td>
                                 <asp:TextBox ID="sWhenRecordedMailToCity" Width="145" runat="server" onblur="checkAllFields();"></asp:TextBox>
                                 <ml:StateDropDownList ID="sWhenRecordedMailToState" runat="server" IncludeTerritories="false" onblur="checkAllFields();">
                                 </ml:StateDropDownList>
                                 <ml:zipcodetextbox id="sWhenRecordedMailToZipcode" runat="server" preset="zipcode" width="40" onblur="checkAllFields();"></ml:zipcodetextbox>
                            </td>
                        </tr>
                   </table>
                </td>
            </tr>                  		
		</table>
		<table cellspacing="2" cellpadding="3" width="100%" border="0">
		    <tr>
                <td  style="font-weight: bold; text-align: right; color: red;">
                    *All Fields are Required
			    </td>
			</tr>
		    <tr align = "center">
		        <td>
		            <asp:Button ID="Save_Btn" runat="server" Text="Save" OnClientClick="unCheckedAll();" OnClick="OnSaveBtn_Click"/>		        
		            <input type="button" value="Cancel" onclick="onClosePopup()" /> 
		        </td>
		    </tr>
		</table>
    </form>
</body>
</html>
