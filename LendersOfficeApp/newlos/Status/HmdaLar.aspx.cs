﻿#region Generated Code
namespace LendersOfficeApp.newlos.Status
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;

    /// <summary>
    /// Provides a means of displaying data for 2018 HMDA regulations.
    /// </summary>
    public partial class HmdaLar : BaseLoanPage
    {
        /// <summary>
        /// Overrides the initialization function for the page.
        /// </summary>
        /// <param name="e">
        /// The arguments for the page initialization.
        /// </param>
        protected override void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;
            base.OnInit(e);
        }

        /// <summary>
        /// Initializes data for the page.
        /// </summary>
        /// <param name="sender">
        /// The sender for the page initialization.
        /// </param>
        /// <param name="e">
        /// The arguments for the page initialization.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            this.PageID = "StatusHMDALar";

            this.RegisterJsScript("HmdaLar.js");
            this.RegisterCSS("HmdaLar.css");

            Tools.Bind_sHmdaLoanTypeT(this.sHmdaLoanTypeT);
            Tools.Bind_sHmdaLoanPurposeT(this.sHmdaLoanPurposeT);
            Tools.Bind_sHmdaPreapprovalT(this.sHmdaPreapprovalT);
            Tools.Bind_sHmdaActionTaken(this.sHmdaActionTaken);
            Tools.Bind_sHmdaActionTakenT(this.sHmdaActionTakenT);
            Tools.Bind_sHmdaConstructionMethT(this.sHmdaConstructionMethT);
            Tools.Bind_sOccT(this.sOccT);
            Tools.Bind_sHmdaHoepaStatusT(this.sHmdaHoepaStatusT);
            Tools.Bind_sHmdaBalloonPaymentT(this.sHmdaBalloonPaymentT);
            Tools.Bind_sHmdaInterestOnlyPaymentT(this.sHmdaInterestOnlyPaymentT);
            Tools.Bind_sHmdaNegativeAmortizationT(this.sHmdaNegativeAmortizationT);
            Tools.Bind_sHmdaOtherNonAmortFeatureT(this.sHmdaOtherNonAmortFeatureT);
            Tools.Bind_sHmdaManufacturedTypeT(this.sHmdaManufacturedTypeT);
            Tools.Bind_sHmdaManufacturedInterestT(this.sHmdaManufacturedInterestT);
            Tools.Bind_sHmdaSubmissionApplicationT(this.sHmdaSubmissionApplicationT);
            Tools.Bind_sHmdaInitiallyPayableToInstitutionT(this.sHmdaInitiallyPayableToInstitutionT);
            Tools.Bind_sHmdaReverseMortgageT(this.sHmdaReverseMortgageT);
            Tools.Bind_sHmdaBusinessPurposeT(this.sHmdaBusinessPurposeT);
            Tools.BindHmdaCreditModel(this.sHmdaBCreditScoreModelT);
            Tools.BindHmdaCreditModel(this.sHmdaCCreditScoreModelT);
            Tools.BindComboBox_CreditModelName(this.sHmdaBCreditScoreModelOtherDescription);
            Tools.BindComboBox_CreditModelName(this.sHmdaCCreditScoreModelOtherDescription);
            Tools.Bind_sHmdaPurchaser2015T(this.sHmdaPurchaser2015T);
        }

        /// <summary>
        /// Loads data for the page.
        /// </summary>
        /// <param name="sender">
        /// The sender for the page load.
        /// </param>
        /// <param name="e">
        /// The arguments for the page load.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var dataloan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(HmdaLar));
            dataloan.InitLoad();

            if (dataloan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
            {
                this.TotalLoanCostNum.InnerText = "74";
                this.sHmdaTotalLoanCostsLabel.InnerText = "Total Points and Fees";
            }

            this.LoadLarData(dataloan);
        }

        /// <summary>
        /// Gets the forced compatibility mode for the page.
        /// </summary>
        /// <returns>
        /// The forced compatibility mode <see cref="E_XUAComaptibleValue.Edge"/>.
        /// </returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        /// <summary>
        /// Loads the LAR data for the page.
        /// </summary>
        /// <param name="dataloan">
        /// The loan to load data.
        /// </param>
        private void LoadLarData(CPageData dataloan)
        {
            this.SetNotApplicableCheckboxes(dataloan);

            this.sLegalEntityIdentifier.Value = dataloan.sLegalEntityIdentifier;
            this.sUniversalLoanIdentifier.Value = dataloan.sUniversalLoanIdentifier;
            this.sUniversalLoanIdentifierLckd.Checked = dataloan.sUniversalLoanIdentifierLckd;
            this.sHmdaApplicationDate.Text = dataloan.sHmdaApplicationDate_rep;
            this.sHmdaApplicationDateLckd.Checked = dataloan.sHmdaApplicationDateLckd;
            this.sHmdaPreapprovalTLckd.Checked = dataloan.sHmdaPreapprovalTLckd;
            this.sHmdaLoanAmount.Value = dataloan.sHmdaLoanAmount_rep;
            this.sHmdaLoanAmountLckd.Checked = dataloan.sHmdaLoanAmountLckd;

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataloan.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
            {
                Tools.SetDropDownListValue(this.sHmdaActionTakenT, dataloan.sHmdaActionTakenT);
            }
            else
            {
                this.sHmdaActionTaken.Text = dataloan.sHmdaActionTaken;
            }

            this.sHmdaActionD.Text = dataloan.sHmdaActionD_rep;
            this.sHmdaSpAddr.Value = dataloan.sHmdaSpAddr;
            this.sHmdaSpCity.Value = dataloan.sHmdaSpCity;
            this.sHmdaSpState.Value = dataloan.sHmdaSpState;
            this.sHmdaSpZip.Value = dataloan.sHmdaSpZip;
            this.sHmda2018CountyCode.Value = dataloan.sHmda2018CountyCode;
            this.sHmda2018CensusTract.Value = dataloan.sHmda2018CensusTract;
            this.sHmdaBAge.Value = dataloan.sHmdaBAge_rep;
            this.sHmdaCAge.Value = dataloan.sHmdaCAge_rep;
            this.sHmdaIncome.Value = dataloan.sHmdaIncome_rep;
            this.sHmda2018AprRateSpread.Value = dataloan.sHmda2018AprRateSpread;
            this.sHmdaBCreditScore.Value = dataloan.sHmdaBCreditScore_rep;
            this.sHmdaCCreditScore.Value = dataloan.sHmdaCCreditScore_rep;
            this.sHmdaBCreditScoreModelOtherDescription.Text = dataloan.sHmdaBCreditScoreModelOtherDescription;
            this.sHmdaCCreditScoreModelOtherDescription.Text = dataloan.sHmdaCCreditScoreModelOtherDescription;
            this.sHmdaTotalLoanCosts.Value = dataloan.sHmdaTotalLoanCosts_rep;
            this.sHmdaOriginationCharge.Value = dataloan.sHmdaOriginationCharge_rep;
            this.sHmdaDiscountPoints.Value = dataloan.sHmdaDiscountPoints_rep;
            this.sHmdaLenderCredits.Value = dataloan.sHmdaLenderCredits_rep;
            this.sHmdaInterestRate.Value = dataloan.sHmdaInterestRate_rep;
            this.sHmdaPrepaymentTerm.Value = dataloan.sHmdaPrepaymentTerm_rep;
            this.sHmdaDebtRatio.Value = dataloan.sHmdaDebtRatio_rep;
            this.sHmdaCombinedRatio.Value = dataloan.sHmdaCombinedRatio_rep;
            this.sHmdaTerm.Value = dataloan.sHmdaTerm_rep;
            this.sHmdaIntroductoryPeriod.Value = dataloan.sHmdaIntroductoryPeriod_rep;           
            this.sHmdaPropertyValue.Value = dataloan.sHmdaPropertyValue_rep;
            this.sHmdaTotalUnits.Value = dataloan.sHmdaTotalUnits_rep;
            this.sHmdaTotalUnitsLckd.Checked = dataloan.sHmdaTotalUnitsLckd;
            this.sHmdaMultifamilyUnits.Value = dataloan.sHmdaMultifamilyUnits_rep;
            this.sHmdaApp1003InterviewerLoanOriginatorIdentifier.Value = dataloan.sHmdaApp1003InterviewerLoanOriginatorIdentifier;
            this.sIsLineOfCredit.Checked = dataloan.sIsLineOfCredit;
            this.sHmdaPurchaser2015TLckd.Checked = dataloan.sHmdaPurchaser2015TLckd;
            this.sHmdaLoanPurposeTLckd.Checked = dataloan.sHmdaLoanPurposeTLckd;

            // Set text field values.
            this.sHmdaDenialReason1.Value = dataloan.sHmdaDenialReason1;
            this.sHmdaDenialReason2.Value = dataloan.sHmdaDenialReason2;
            this.sHmdaDenialReason3.Value = dataloan.sHmdaDenialReason3;
            this.sHmdaDenialReason4.Value = dataloan.sHmdaDenialReason4;
            this.sHmdaDenialReasonOtherDescription.Value = dataloan.sHmdaDenialReasonOtherDescription;

            Tools.SetDropDownListValue(this.sHmdaLoanTypeT, dataloan.sHmdaLoanTypeT);
            Tools.SetDropDownListValue(this.sHmdaLoanPurposeT, dataloan.sHmdaLoanPurposeT);
            Tools.SetDropDownListValue(this.sHmdaPreapprovalT, dataloan.sHmdaPreapprovalT);
            Tools.SetDropDownListValue(this.sHmdaConstructionMethT, dataloan.sHmdaConstructionMethT);
            Tools.SetDropDownListValue(this.sOccT, dataloan.sOccT);
            Tools.SetDropDownListValue(this.sHmdaBCreditScoreModelT, dataloan.sHmdaBCreditScoreModelT);
            Tools.SetDropDownListValue(this.sHmdaCCreditScoreModelT, dataloan.sHmdaCCreditScoreModelT);
            Tools.SetDropDownListValue(this.sHmdaHoepaStatusT, dataloan.sHmdaHoepaStatusT);
            Tools.SetDropDownListValue(this.sHmdaBalloonPaymentT, dataloan.sHmdaBalloonPaymentT);
            Tools.SetDropDownListValue(this.sHmdaInterestOnlyPaymentT, dataloan.sHmdaInterestOnlyPaymentT);
            Tools.SetDropDownListValue(this.sHmdaNegativeAmortizationT, dataloan.sHmdaNegativeAmortizationT);
            Tools.SetDropDownListValue(this.sHmdaOtherNonAmortFeatureT, dataloan.sHmdaOtherNonAmortFeatureT);
            Tools.SetDropDownListValue(this.sHmdaManufacturedTypeT, dataloan.sHmdaManufacturedTypeT);
            Tools.SetDropDownListValue(this.sHmdaManufacturedInterestT, dataloan.sHmdaManufacturedInterestT);
            Tools.SetDropDownListValue(this.sHmdaSubmissionApplicationT, dataloan.sHmdaSubmissionApplicationT);
            Tools.SetDropDownListValue(this.sHmdaInitiallyPayableToInstitutionT, dataloan.sHmdaInitiallyPayableToInstitutionT);
            Tools.SetDropDownListValue(this.sHmdaReverseMortgageT, dataloan.sHmdaReverseMortgageT);
            Tools.SetDropDownListValue(this.sHmdaBusinessPurposeT, dataloan.sHmdaBusinessPurposeT);
            Tools.SetDropDownListValue(this.sHmdaPurchaser2015T, dataloan.sHmdaPurchaser2015T);

            Tools.Bind_sHmdaLienT(sHmdaLienT, !LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataloan.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement));
            Tools.SetDropDownListValue(this.sHmdaLienT, dataloan.sHmdaLienT);

            sHmdaBEthnicity1T.Value = dataloan.sHmdaBEthnicity1T.GetDescription();
            sHmdaBEthnicity2T.Value = dataloan.sHmdaBEthnicity2T.GetDescription();
            sHmdaBEthnicity3T.Value = dataloan.sHmdaBEthnicity3T.GetDescription();
            sHmdaBEthnicity4T.Value = dataloan.sHmdaBEthnicity4T.GetDescription();
            sHmdaBEthnicity5T.Value = dataloan.sHmdaBEthnicity5T.GetDescription();

            sHmdaCEthnicity1T.Value = dataloan.sHmdaCEthnicity1T.GetDescription();
            sHmdaCEthnicity2T.Value = dataloan.sHmdaCEthnicity2T.GetDescription();
            sHmdaCEthnicity3T.Value = dataloan.sHmdaCEthnicity3T.GetDescription();
            sHmdaCEthnicity4T.Value = dataloan.sHmdaCEthnicity4T.GetDescription();
            sHmdaCEthnicity5T.Value = dataloan.sHmdaCEthnicity5T.GetDescription();

            sHmdaBEthnicityOtherDescription.Value = dataloan.sHmdaBEthnicityOtherDescription;
            sHmdaBEthnicityCollectedByObservationOrSurname.Value = dataloan.sHmdaBEthnicityCollectedByObservationOrSurname.GetDescription();

            sHmdaCEthnicityOtherDescription.Value = dataloan.sHmdaCEthnicityOtherDescription;
            sHmdaCEthnicityCollectedByObservationOrSurname.Value = dataloan.sHmdaCEthnicityCollectedByObservationOrSurname.GetDescription();

            sHmdaBRace1T.Value = dataloan.sHmdaBRace1T.GetDescription();
            sHmdaBRace2T.Value = dataloan.sHmdaBRace2T.GetDescription();
            sHmdaBRace3T.Value = dataloan.sHmdaBRace3T.GetDescription();
            sHmdaBRace4T.Value = dataloan.sHmdaBRace4T.GetDescription();
            sHmdaBRace5T.Value = dataloan.sHmdaBRace5T.GetDescription();

            sHmdaCRace1T.Value = dataloan.sHmdaCRace1T.GetDescription();
            sHmdaCRace2T.Value = dataloan.sHmdaCRace2T.GetDescription();
            sHmdaCRace3T.Value = dataloan.sHmdaCRace3T.GetDescription();
            sHmdaCRace4T.Value = dataloan.sHmdaCRace4T.GetDescription();
            sHmdaCRace5T.Value = dataloan.sHmdaCRace5T.GetDescription();

            sHmdaBEnrolledOrPrincipalTribe.Value = dataloan.sHmdaBEnrolledOrPrincipalTribe;
            sHmdaBOtherAsianDescription.Value = dataloan.sHmdaBOtherAsianDescription;
            sHmdaBOtherPacificIslanderDescription.Value = dataloan.sHmdaBOtherPacificIslanderDescription;

            sHmdaCEnrolledOrPrincipalTribe.Value = dataloan.sHmdaCEnrolledOrPrincipalTribe;
            sHmdaCOtherAsianDescription.Value = dataloan.sHmdaCOtherAsianDescription;
            sHmdaCOtherPacificIslanderDescription.Value = dataloan.sHmdaCOtherPacificIslanderDescription;

            sHmdaBRaceCollectedByObservationOrSurname.Value = dataloan.sHmdaBRaceCollectedByObservationOrSurname.GetDescription();
            sHmdaCRaceCollectedByObservationOrSurname.Value = dataloan.sHmdaCRaceCollectedByObservationOrSurname.GetDescription();

            sHmdaBSexT.Value = dataloan.sHmdaBSexT.GetDescription();
            sHmdaBSexCollectedByObservationOrSurname.Value = dataloan.sHmdaBSexCollectedByObservationOrSurname.GetDescription();
            sHmdaCSexT.Value = dataloan.sHmdaCSexT.GetDescription();
            sHmdaCSexCollectedByObservationOrSurname.Value = dataloan.sHmdaCSexCollectedByObservationOrSurname.GetDescription();

            sHmdaAutomatedUnderwritingSystem1T.Value = dataloan.sHmdaAutomatedUnderwritingSystem1T.GetDescription();
            sHmdaAutomatedUnderwritingSystem2T.Value = dataloan.sHmdaAutomatedUnderwritingSystem2T.GetDescription();
            sHmdaAutomatedUnderwritingSystem3T.Value = dataloan.sHmdaAutomatedUnderwritingSystem3T.GetDescription();
            sHmdaAutomatedUnderwritingSystem4T.Value = dataloan.sHmdaAutomatedUnderwritingSystem4T.GetDescription();
            sHmdaAutomatedUnderwritingSystem5T.Value = dataloan.sHmdaAutomatedUnderwritingSystem5T.GetDescription();
            sHmdaAutomatedUnderwritingSystemOtherDescription.Value = dataloan.sHmdaAutomatedUnderwritingSystemOtherDescription;

            sHmdaAutomatedUnderwritingSystemResult1T.Value = dataloan.sHmdaAutomatedUnderwritingSystemResult1T.GetDescription();
            sHmdaAutomatedUnderwritingSystemResult2T.Value = dataloan.sHmdaAutomatedUnderwritingSystemResult2T.GetDescription();
            sHmdaAutomatedUnderwritingSystemResult3T.Value = dataloan.sHmdaAutomatedUnderwritingSystemResult3T.GetDescription();
            sHmdaAutomatedUnderwritingSystemResult4T.Value = dataloan.sHmdaAutomatedUnderwritingSystemResult4T.GetDescription();
            sHmdaAutomatedUnderwritingSystemResult5T.Value = dataloan.sHmdaAutomatedUnderwritingSystemResult5T.GetDescription();
            sHmdaAutomatedUnderwritingSystemResultOtherDescription.Value = dataloan.sHmdaAutomatedUnderwritingSystemResultOtherDescription;
        }

        /// <summary>
        /// Sets the "NA" checkboxes on the page using the specified <paramref name="dataloan"/>.
        /// </summary>
        /// <param name="dataloan">
        /// The loan to load data.
        /// </param>
        private void SetNotApplicableCheckboxes(CPageData dataloan)
        {
            this.sHmdaMultifamilyUnitsNotApplicable.Checked = dataloan.sHmdaPropT != E_sHmdaPropT.MultiFamiliy || LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaMultifamilyUnits_rep);
            this.sHmdaMultifamilyUnitsNotApplicable.Disabled = dataloan.sHmdaPropT != E_sHmdaPropT.MultiFamiliy;
            this.sHmdaSpAddrNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaSpAddr);
            this.sHmdaSpCityNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaSpCity);
            this.sHmdaSpStateNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaSpState);
            this.sHmdaSpZipNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaSpZip);
            this.sHmda2018CountyCodeNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmda2018CountyCode);
            this.sHmda2018CensusTractNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmda2018CensusTract);
            this.sHmdaBAgeNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaBAge_rep, preserveNotApplicableCode: true);
            this.sHmdaCAgeNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaCAge_rep, preserveNotApplicableCode: true);
            this.sHmdaIncomeNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaIncome_rep);
            this.sHmda2018AprRateSpreadNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmda2018AprRateSpread);
            this.sHmdaBCreditScoreNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaBCreditScore_rep, preserveNotApplicableCode: true);
            this.sHmdaCCreditScoreNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaCCreditScore_rep, preserveNotApplicableCode: true);
            this.sHmdaTotalLoanCostsNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaTotalLoanCosts_rep);
            this.sHmdaOriginationChargeNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaOriginationCharge_rep);
            this.sHmdaDiscountPointsNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaDiscountPoints_rep);
            this.sHmdaLenderCreditsNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaLenderCredits_rep);
            this.sHmdaInterestRateNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaInterestRate_rep);
            this.sHmdaPrepaymentTermNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaPrepaymentTerm_rep);
            this.sHmdaDebtRatioNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaDebtRatio_rep);
            this.sHmdaCombinedRatioNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaCombinedRatio_rep);
            this.sHmdaTermNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaTerm_rep);
            this.sHmdaIntroductoryPeriodNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaIntroductoryPeriod_rep);
            this.sHmdaPropertyValueNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaPropertyValue_rep);
            this.sHmdaApp1003InterviewerLoanOriginatorIdentifierNotApplicable.Checked = LosConvert.IsNotApplicableHmdaString(dataloan.sHmdaApp1003InterviewerLoanOriginatorIdentifier);
        }
    }
}
