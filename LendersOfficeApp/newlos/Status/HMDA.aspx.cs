namespace LendersOfficeApp.newlos.Status
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.Hmda;

    public partial class HMDA : BaseLoanPage
	{
        /// <summary>
        /// Saves data to the loan from this page's controls.
        /// </summary>
        /// <param name="dataLoan">The loan to bind to.</param>
        /// <param name="dataApp">The app to bind to.</param>
        /// <param name="serviceItem">The service item calling this method.</param>
        internal static void BindDataFromControls(CPageData dataLoan, CAppData dataApp, AbstractBackgroundServiceItem serviceItem)
        {
            HmdaRaceEthnicityData.BindHmdaRaceEthnicityData(dataApp, serviceItem, $"{nameof(HREData)}_");
            HmdaReliedOn.BindHmdaReliedOnData(dataLoan, dataApp, serviceItem, $"{nameof(HmdaReliedOn)}_");
        }

        /// <summary>
        /// Loads the data from the loan to this page's controls.
        /// </summary>
        /// <param name="dataLoan">The loan to load from.</param>
        /// <param name="dataApp">The app to load from.</param>
        /// <param name="serviceItem">The service item.</param>
        internal static void LoadDataForControls(CPageData dataLoan, CAppData dataApp, AbstractBackgroundServiceItem serviceItem)
        {
            HmdaRaceEthnicityData.LoadHmdaRaceEthnicityData(dataApp, serviceItem, $"{nameof(HREData)}_");
            HmdaReliedOn.LoadHmdaReliedOnData(dataLoan, dataApp, serviceItem, $"{nameof(HmdaReliedOn)}_");
        }

        #region Protected Member Variables
        #endregion

        private void BindDenialReason(MeridianLink.CommonControls.ComboBox cb) 
        {
            cb.Items.Add("1 Debt-to-income ratio");
            cb.Items.Add("2 Employment history");
            cb.Items.Add("3 Credit history");
            cb.Items.Add("4 Collateral");
            cb.Items.Add("5 Insufficient cash");
            cb.Items.Add("6 Unverifiable information");
            cb.Items.Add("7 Credit app incomplete");
            cb.Items.Add("8 Mortgage ins denied");
            cb.Items.Add("9 Other");

        }

        private void BindRefinancePurpose()
        {
            sRefPurpose.Items.Add("No Cash-Out Rate/Term");
            sRefPurpose.Items.Add("Limited Cash-Out Rate/Term");
            sRefPurpose.Items.Add("Cash-Out/Home Improvement");
            sRefPurpose.Items.Add("Cash-Out/Debt Consolidation");
            sRefPurpose.Items.Add("Cash-Out/Other");
        }

        private void BindNCRadioButtonList(RadioButtonList rbl)
        {
            rbl.Items.Add(Tools.CreateEnumListItem("Yes", E_NCCallRprtResponseT.YES));
            rbl.Items.Add(Tools.CreateEnumListItem("No", E_NCCallRprtResponseT.NO));
            rbl.Items.Add(Tools.CreateEnumListItem("N/A", E_NCCallRprtResponseT.NA));
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            BindDenialReason(sHmdaDenialReason1);
            BindDenialReason(sHmdaDenialReason2);
            BindDenialReason(sHmdaDenialReason3);
            BindDenialReason(sHmdaDenialReason4);

            RegisterJsGlobalVariables("LifeInsCreditUnionVal", (int)HmdaPurchaser2015T.LifeInsCreditUnion);
            RegisterJsGlobalVariables("LifeInsCreditUnionDescription", Tools.GetHmdaPurchaser2015TDescription(HmdaPurchaser2015T.LifeInsCreditUnion));

            Tools.Bind_sHmdaActionTaken(this.sHmdaActionTaken);
            Tools.Bind_sHmdaActionTakenT(this.sHmdaActionTakenT);
            Tools.Bind_sHmdaPreapprovalT(sHmdaPreapprovalT);
            Tools.Bind_sHmdaPropT(sHmdaPropT);
            Tools.Bind_sHmdaPurchaser2004T(sHmdaPurchaser2004T);
            Tools.Bind_sHmdaCoApplicantSourceT(this.sHmdaCoApplicantSourceT);            

            BindRefinancePurpose();
            BindNCRadioButtonList(sNCBorrWaiveRescT);
            BindNCRadioButtonList(sNCBorrRecvCounselT);
            BindNCRadioButtonList(sNCHomeImprovPmtsMadeT);
            BindNCRadioButtonList(sNCAllowIRToRiseT);
            BindNCRadioButtonList(sNC25OrMoreAcresT);
            BindNCRadioButtonList(sNCRefiWithSameLenderT);

            this.PageTitle = "HMDA";
            this.PageID = "StatusHMDA";

            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterCSS("jquery-ui-1.11.custom.css");

            this.RegisterJsScript("geocode.js");
            this.RegisterService("geocode", "/newlos/GeoCodeService.aspx");
        }

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(HMDA));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            var dateToCompare = dataLoan.sHmdaActionD.IsValid ? dataLoan.sHmdaActionD.DateTimeForComputation : DateTime.Today;
            var isAtLeastV22 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);
            var includeNotApplicable = (isAtLeastV22 && (dateToCompare < new DateTime(2018, 1, 1) && dataLoan.sHmdaActionTakenT == HmdaActionTaken.PurchasedLoan))
                || (!isAtLeastV22 && string.Equals(dataLoan.sHmdaActionTaken, ConstApp.HmdaPurchasedActionTaken, StringComparison.OrdinalIgnoreCase));
            Tools.Bind_sHmdaLienT(sHmdaLienT, includeNotApplicable);

            Tools.Bind_sHmdaPurchaser2015T(sHmdaPurchaser2015T, dataLoan.sHmdaPurchaser2015T);

            sHmdaCountyCode.Text = dataLoan.sHmdaCountyCode;
            sHmdaCensusTract.Text = dataLoan.sHmdaCensusTract;
            sHmdaLoanDenied.Checked = dataLoan.sHmdaLoanDenied;
            sHmdaDeniedFormDone.Checked = dataLoan.sHmdaDeniedFormDone;
            sHmdaCounterOfferMade.Checked = dataLoan.sHmdaCounterOfferMade;
            sHmdaLoanDeniedBy.Text = dataLoan.sHmdaLoanDeniedBy;
            sHmdaDeniedFormDoneBy.Text = dataLoan.sHmdaDeniedFormDoneBy;
            sHmdaCounterOfferMadeBy.Text = dataLoan.sHmdaCounterOfferMadeBy;
            sHmdaCounterOfferDetails.Text = dataLoan.sHmdaCounterOfferDetails;
            sHmdaExcludedFromReport.Checked = dataLoan.sHmdaExcludedFromReport;
            sHmdaReportAsHomeImprov.Checked = dataLoan.sHmdaReportAsHomeImprov;

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement))
            {
                Tools.SetDropDownListValue(this.sHmdaActionTakenT, dataLoan.sHmdaActionTakenT);
                BindDenialReasons(dataLoan);
            }
            else
            {
                sHmdaActionTaken.Text = dataLoan.sHmdaActionTaken;
                sHmdaDenialReason1.Text = dataLoan.sHmdaDenialReason1;
                sHmdaDenialReason2.Text = dataLoan.sHmdaDenialReason2;
                sHmdaDenialReason3.Text = dataLoan.sHmdaDenialReason3;
                sHmdaDenialReason4.Text = dataLoan.sHmdaDenialReason4;
            }

            sHmdaActionD.Text = dataLoan.sHmdaActionD_rep;
            sRejectD.Text = dataLoan.sRejectD_rep;
            sHmdaDeniedFormDoneD.Text = dataLoan.sHmdaDeniedFormDoneD_rep;
            sHmdaCounterOfferMadeD.Text = dataLoan.sHmdaCounterOfferMadeD_rep;
            sHmdaMsaNum.Text = dataLoan.sHmdaMsaNum;
            sHmdaStateCode.Text = dataLoan.sHmdaStateCode;

            sFannieFips.Value = dataLoan.sFannieFips;
            sFannieFipsLckd.Checked = dataLoan.sFannieFipsLckd;

            // New fields for 2004 HMDA
            sHmdaAprRateSpreadLckd.Checked = dataLoan.sHmdaAprRateSpreadLckd;
            sHmdaAprRateSpread.Text = dataLoan.sHmdaAprRateSpread;
            Tools.SetDropDownListValue(sHmdaPreapprovalT, dataLoan.sHmdaPreapprovalT);
            sHmdaReportAsHoepaLoan.Checked = dataLoan.sHmdaReportAsHoepaLoan;
            Tools.SetDropDownListValue(sHmdaLienT, dataLoan.sHmdaLienT);
            Tools.SetDropDownListValue(sHmdaPropT, dataLoan.sHmdaPropT);
            sHmdaLienTLckd.Checked = dataLoan.sHmdaLienTLckd;

            Tools.SetDropDownListValue(this.sHmdaCoApplicantSourceSsn, dataLoan.sHmdaCoApplicantSourceSsn);
            Tools.SetDropDownListValue(this.sHmdaCoApplicantSourceT, dataLoan.sHmdaCoApplicantSourceT);

            this.sHmdaCoApplicantSourceTLckd.Checked = dataLoan.sHmdaCoApplicantSourceTLckd;
            this.sHmdaCoApplicantSourceSsnLckd.Checked = dataLoan.sHmdaCoApplicantSourceSsnLckd;
            this.sHmdaIsSecuredByDwelling.Checked = dataLoan.sHmdaIsSecuredByDwelling;

            sHmdaAprRateSpreadCalcMessage_Hidden.Value = dataLoan.sHmdaAprRateSpreadCalcMessage;

            sGseSpT.Value = dataLoan.sGseSpT.ToString("D");
            sSpAddr.Value = dataLoan.sSpAddr;
            sSpCity.Value = dataLoan.sSpCity;
            sSpCounty.Value = dataLoan.sSpCounty;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Value = dataLoan.sSpZip;

            Tools.SetDropDownListValue(sHmdaPurchaser2004T, dataLoan.sHmdaPurchaser2004T);
            sHmdaPurchaser2004TLckd.Checked = dataLoan.sHmdaPurchaser2004TLckd;

            Tools.SetDropDownListValue(sHmdaPurchaser2015T, dataLoan.sHmdaPurchaser2015T);
            sHmdaPurchaser2015TLckd.Checked = dataLoan.sHmdaPurchaser2015TLckd;


            sHmdaPropTLckd.Checked = dataLoan.sHmdaPropTLckd;

            // OPM 81487 NC specific fields - GF
            if (dataLoan.sSpState != "NC" && !dataLoan.IsTemplate)
            {
                NCCallReportPanel.Attributes.Add("class", "hidden");
            }

            sAppSubmittedD.Text = dataLoan.sAppSubmittedD_rep;
            sAppSubmittedDLckd.Checked = dataLoan.sAppSubmittedDLckd;
            sAppReceivedByLenderD.Text = dataLoan.sAppReceivedByLenderD_rep;            
            sAppReceivedByLenderDLckd.Checked = dataLoan.sAppReceivedByLenderDLckd;

            sGfeInitialDisclosureD.Text = dataLoan.sGfeInitialDisclosureD_rep;
            if (dataLoan.BrokerDB.IsProtectDisclosureDates)
            {
                sGfeInitialDisclosureD.ReadOnly = true;
                sTilInitialDisclosureD.ReadOnly = true;
            }
            sTilInitialDisclosureD.Text = dataLoan.sTilInitialDisclosureD_rep;
            sCHARMBookletProvidedD.Text = dataLoan.sCHARMBookletProvidedD_rep;
            sHUDSpecialInfoBookletProvidedD.Text = dataLoan.sHUDSpecialInfoBookletProvidedD_rep;
            sAffiliatedBusinessDisclosureProvidedD.Text = dataLoan.sAffiliatedBusinessDisclosureProvidedD_rep;
            sApp1003InterviewerLoanOriginatorIdentifier.Text = dataLoan.sApp1003InterviewerLoanOriginatorIdentifier;
            sApp1003InterviewerLoanOriginationCompanyIdentifier.Text = dataLoan.sApp1003InterviewerLoanOriginationCompanyIdentifier;
            sBranchManagerNMLSIdentifier.Text = dataLoan.sBranchManagerNMLSIdentifier;
            CAgentFields lender = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            LenderNMLSId.Text = lender.CompanyLoanOriginatorIdentifier;
            sPpmtPenaltyPc.Text = dataLoan.sPpmtPenaltyPc_rep;
            sUndiscountedIR.Text = dataLoan.sUndiscountedIR_rep;
            sYSPAmt.Text = dataLoan.sYSPAmt_rep;

            // Conditional behavior taken care of client side for this question
            sNCBorrWaiveRescT.SelectedValue = dataLoan.sNCBorrWaiveRescT.ToString("d");
            sNCBorrRecvCounselT.SelectedValue = dataLoan.sNCBorrRecvCounselT.ToString("d");
            sNCHomeImprovPmtsMadeT.SelectedValue = dataLoan.sNCHomeImprovPmtsMadeT.ToString("d");
            sNCAllowIRToRiseT.SelectedValue = dataLoan.sNCAllowIRToRiseT.ToString("d");
            sNC25OrMoreAcresT.SelectedValue = dataLoan.sNC25OrMoreAcresT.ToString("d");
            sNCRefiWithSameLenderT.SelectedValue = dataLoan.sNCRefiWithSameLenderT.ToString("d");

            if (!dataLoan.sIsRefinancing)
            {
                sNCRefiWithSameLenderT.Enabled = false;
                RefinancePurposeRow.Attributes.Add("class", "hidden");
            }

            sRefPurpose.Text = dataLoan.sRefPurpose;

            if (dataLoan.sLT == E_sLT.Conventional && dataLoan.sRefPurpose.ToLower() != "no cash-out rate/term")
                sRefPurpose.Items.Remove("No Cash-Out Rate/Term");

            // End NC specific

            sHmdaLenderHasPreapprovalProgram.Checked = dataLoan.sHmdaLenderHasPreapprovalProgram;
            sHmdaBorrowerRequestedPreapproval.Checked = dataLoan.sHmdaBorrowerRequestedPreapproval;
            sHmdaPreapprovalTLckd.Checked = dataLoan.sHmdaPreapprovalTLckd;
            sReportFhaGseAusRunsAsTotalRun.Checked = dataLoan.sReportFhaGseAusRunsAsTotalRun;

            this.HREData.ShouldLoad = true;
            this.HREData.RaceEthnicityData = dataApp.ConstructHmdaRaceEthnicityData();

            this.sLegalEntityIdentifier.Text = dataLoan.sLegalEntityIdentifier;
            this.sUniversalLoanIdentifier.Value = dataLoan.sUniversalLoanIdentifier;
            this.sUniversalLoanIdentifierLckd.Checked = dataLoan.sUniversalLoanIdentifierLckd;
        }

        private void BindDenialReasons(CPageData dataLoan)
        {
            // Set text field values.
            this.sHmdaDenialReason1_2.Value = dataLoan.sHmdaDenialReason1;
            this.sHmdaDenialReason2_2.Value = dataLoan.sHmdaDenialReason2;
            this.sHmdaDenialReason3_2.Value = dataLoan.sHmdaDenialReason3;
            this.sHmdaDenialReason4_2.Value = dataLoan.sHmdaDenialReason4;
            this.sHmdaDenialReasonOtherDescription.Value = dataLoan.sHmdaDenialReasonOtherDescription;

            // Build selected reason source lists.
            IEnumerable<string> selectedList = dataLoan.sHmdaDenialReasonsList.Where(hdr => hdr != HmdaDenialReason.Blank).Select(hdr => Tools.MapHmdaDenialReasonToString(hdr));
            this.sHmdaDenialReasonsList.Value = SerializationHelper.JsonNetAnonymousSerialize(selectedList);

            this.selectedRepeater.DataSource = selectedList;
            this.selectedRepeater.DataBind();

            // Build available reasons source list.
            IEnumerable<string> availableList = new List<string>()
            {
                Tools.MapHmdaDenialReasonToString(HmdaDenialReason.DebtToIncomeRatio),
                Tools.MapHmdaDenialReasonToString(HmdaDenialReason.EmploymentHistory),
                Tools.MapHmdaDenialReasonToString(HmdaDenialReason.CreditHistory),
                Tools.MapHmdaDenialReasonToString(HmdaDenialReason.Collateral),
                Tools.MapHmdaDenialReasonToString(HmdaDenialReason.InsufficientCash),
                Tools.MapHmdaDenialReasonToString(HmdaDenialReason.UnverifiableInformation),
                Tools.MapHmdaDenialReasonToString(HmdaDenialReason.CreditApplicationIncomplete),
                Tools.MapHmdaDenialReasonToString(HmdaDenialReason.MortgageInsuranceDenied),
                Tools.MapHmdaDenialReasonToString(HmdaDenialReason.Other),
            };

            availableList = availableList.Except(selectedList);

            this.availableRepeater.DataSource = availableList;
            this.availableRepeater.DataBind();
        }

        protected void FieldRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            string text = (string)args.Item.DataItem;
            HtmlGenericControl listItem = args.Item.FindControl("listItem") as HtmlGenericControl;
            Literal description = args.Item.FindControl("description") as Literal;

            description.Text = text;

            if (text == Tools.MapHmdaDenialReasonToString(HmdaDenialReason.Other))
            {
                listItem.Attributes.Add("class", "Other");
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion

	}
}
