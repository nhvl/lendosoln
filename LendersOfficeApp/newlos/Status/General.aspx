<%@ Page Language="c#" CodeBehind="General.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Status.General" ValidateRequest="true" %>
<%@ Register TagPrefix="uc" TagName="RespaDates" Src="~/newlos/Disclosure/RespaDates.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CreditReportAuthorization" Src="~/newlos/Status/CreditReportAuthorization.ascx" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE HTML>
<html>
<head runat="server">
    <title>General</title>
    <style type="text/css">
        .WarningLabelStyle
        {
            border-right: black 1px solid;
            width: 200px;
            display: none;
            position: absolute;
            z-index: 999;
            padding-right: 3px;
            border-top: black 1px solid;
            padding-left: 3px;
            font-weight: bold;
            padding-bottom: 3px;
            margin: 3px;
            border-left: black 1px solid;
            color: red;
            padding-top: 3px;
            border-bottom: black 1px solid;
            background-color: white;
        }

        .table {
            display: table;
        }

        .tableRow {
            display: table-row;
        }

        .tableCell {
            display: table-cell;
        }

        .hidden {
            display: none;
        }

        .centerChildren {
            text-align: center;
        }

        select:disabled {
            background-color: lightgray;
        }

        #TransferTo {
            width: 125px;
        }

        .blank-row {
            height:15px;
        }

        fieldset.RespaDates {
            width: 400px;
        }

        .time {
            width: 75px;
        }

        body[hide-status-events] .touch-textbox, body[hide-status-events] .touch-column {
            display: none;
        }

        .touch-textbox {
            width: 5em;
        }
        .displayNone {
            display: none;
        }
        .LoanFormHeader {
            padding-left: 5px;
        }
        .bg-subheader td {
            padding: 2px 5px;
        }
        td {
            padding-bottom: 4px;
        }
        .w-184 {
            width: 184px;
        }
    </style>
</head>
<body id="body" runat="server" class="RightBackground" ms_positioning="FlowLayout">

    <script type="text/javascript">
function _init() {
    lockField(<%= AspxTools.JsGetElementById(sAppSubmittedDLckd) %>, 'sAppSubmittedD');
    lockField(<%= AspxTools.JsGetElementById(sAppReceivedByLenderDLckd) %>, 'sAppReceivedByLenderD');
    lockField(<%= AspxTools.JsGetElementById(sLenderCaseNumLckd) %>, 'sLenderCaseNum');
    lockField(<%= AspxTools.JsGetElementById(sDocMagicApplicationDLckd) %>, 'sDocMagicApplicationD');
    lockField(<%= AspxTools.JsGetElementById(sDocMagicGFEDLckd) %>, 'sDocMagicGFED');
    lockField(<%= AspxTools.JsGetElementById(sDocMagicEstAvailableThroughDLckd) %>, 'sDocMagicEstAvailableThroughD');      
    lockField(<%= AspxTools.JsGetElementById(sDocMagicDocumentDLckd) %>, 'sDocMagicDocumentD');
    lockField(<%= AspxTools.JsGetElementById(sDocMagicClosingDLckd) %>, 'sDocMagicClosingD');
    lockField(<%= AspxTools.JsGetElementById(sDocMagicSigningDLckd) %>, 'sDocMagicSigningD');
    lockField(<%= AspxTools.JsGetElementById(sDocMagicCancelDLckd) %>, 'sDocMagicCancelD');
    lockField(<%= AspxTools.JsGetElementById(sDocMagicDisbursementDLckd) %>, 'sDocMagicDisbursementD');
    lockField(<%= AspxTools.JsGetElementById(sDocExpirationDLckd) %>, 'sDocExpirationD');
    lockField(<%= AspxTools.JsGetElementById(sDocMagicRateLockDLckd) %>, 'sDocMagicRateLockD');
    lockField(<%= AspxTools.JsGetElementById(sDocMagicPreZSentDLckd) %>, 'sDocMagicPreZSentD');
    lockField(<%= AspxTools.JsGetElementById(sDocMagicReDiscSendDLckd) %>, 'sDocMagicReDiscSendD');
    lockField(<%= AspxTools.JsGetElementById(sDocMagicReDiscReceivedDLckd) %>, 'sDocMagicReDiscReceivedD');
    lockField(<%= AspxTools.JsGetElementById(sEstCloseDLckd) %>, 'sEstCloseD');
    lockField(<%= AspxTools.JsGetElementById(sCreditDecisionDLckd) %>, 'sCreditDecisionD');
    var b = <%= AspxTools.JsGetElementById(sDocMagicRedisclosureMethodTLckd) %>.checked;
    disableDDL(<%= AspxTools.JsGetElementById(sDocMagicRedisclosureMethodT) %>, !b);
    var isReadOnly = document.getElementById('_ReadOnly').value == 'True';
    var btn = document.getElementById("btnChangeStatus");
    if ( isReadOnly == false ) {
        btn.disabled = !<%= AspxTools.JsGetElementById(sStatusLckd) %>.checked;  
    } else {
        btn.disabled = true;
    }
    
    showStatuses();
}

function showStatuses()
{

    var statuses;
    
    document.getElementById("Lead_NewRow").style.display= "";
    document.getElementById("CorrespondentProcessRow").style.display= "none";
    
    switch(getBranchChannelEnum())
    {
        case <%= AspxTools.JsNumeric(E_BranchChannelT.Retail) %>:
             statuses = $.parseJSON(document.getElementById("retailArray").value);
        break;
        case <%= AspxTools.JsNumeric(E_BranchChannelT.Wholesale) %>:
             statuses = $.parseJSON(document.getElementById("wholesaleArray").value);
             document.getElementById("Lead_NewRow").style.display = (document.getElementById("sLeadD").value == "" ? "none" : "");
        break;
        case <%= AspxTools.JsNumeric(E_BranchChannelT.Broker) %>:
             statuses = $.parseJSON(document.getElementById("brokerArray").value);

        break;
        case <%= AspxTools.JsNumeric(E_BranchChannelT.Correspondent) %>:
             switch(parseInt(<%= AspxTools.JsGetElementById(sCorrespondentProcessT) %>.value))
             {
                case <%= AspxTools.JsNumeric(E_sCorrespondentProcessT.Blank) %>:
                    statuses = $.parseJSON(document.getElementById("correspondentArray_Blank").value);
                    break;
                case <%= AspxTools.JsNumeric(E_sCorrespondentProcessT.Delegated) %>:
                    statuses = $.parseJSON(document.getElementById("correspondentArray_Delegated").value);
                    break;
                case <%= AspxTools.JsNumeric(E_sCorrespondentProcessT.PriorApproved) %>:
                    statuses = $.parseJSON(document.getElementById("correspondentArray_PriorApproved").value);
                    break;
                case <%= AspxTools.JsNumeric(E_sCorrespondentProcessT.MiniCorrespondent) %>:
                    statuses = $.parseJSON(document.getElementById("correspondentArray_MiniCorrespondent").value);
                    break;
                case <%= AspxTools.JsNumeric(E_sCorrespondentProcessT.Bulk) %>:
                    statuses = $.parseJSON(document.getElementById("correspondentArray_Bulk").value);
                    break;
                case <%= AspxTools.JsNumeric(E_sCorrespondentProcessT.MiniBulk) %>:
                    statuses = $.parseJSON(document.getElementById("correspondentArray_MiniBulk").value);
                    break;
             }
             
             document.getElementById("Lead_NewRow").style.display = (document.getElementById("sLeadD").value == "" ? "none" : "");
             document.getElementById("CorrespondentProcessRow").style.display= "";
        break;
        case <%= AspxTools.JsNumeric(E_BranchChannelT.Blank) %>:
             statuses = $.parseJSON(document.getElementById("channelNotSpecifiedArray").value);
        break;
        
    }
    iterateStatuses(statuses);
    
    //these rows will always display, unless the loan is using a broker or correspondent channel AND the statuses aren't configured AND the values are null
    document.getElementById("Additional_Conditions_Sent").style.display = "none";
    if(getBranchChannelEnum() != <%= AspxTools.JsNumeric(E_BranchChannelT.Broker) %> && getBranchChannelEnum() != <%= AspxTools.JsNumeric(E_BranchChannelT.Correspondent) %>)
    {
        document.getElementById("Loan_InvestorConditions").style.display = "";
        document.getElementById("Loan_InvestorConditionsSent").style.display = "";
        document.getElementById("Additional_Conditions_Sent").style.display = "";
    }
    else{
        if(document.getElementById("Loan_InvestorConditionsSent").style.display === "")
            document.getElementById("Additional_Conditions_Sent").style.display = "";
    }
}
function getBranchChannelEnum()
{
    var str = <%= AspxTools.JQuery(sBranchChannelT) %>.val();
    if (str == 'Wholesale')
    {
        return <%= AspxTools.JsNumeric(E_BranchChannelT.Wholesale) %>;
    }
    else if (str == 'Correspondent')
    {
        return <%= AspxTools.JsNumeric(E_BranchChannelT.Correspondent) %>;
    }
    else if (str == 'Retail')
    {
        return <%= AspxTools.JsNumeric(E_BranchChannelT.Retail) %>;
    }
    else if (str == 'Broker')
    {
        return <%= AspxTools.JsNumeric(E_BranchChannelT.Broker) %>;
    }
    else if (str == '')
    {
    return <%= AspxTools.JsNumeric(E_BranchChannelT.Blank) %>;
    }
    else
    {
        alert('Error: Unhandle channel=[' + str + ']');
        return <%= AspxTools.JsNumeric(E_BranchChannelT.Blank) %>;
    }
}
function iterateStatuses(statuses)
{
    allStatuses = $.parseJSON(document.getElementById("masterStatusArray").value);
    for(var i = 0; i < allStatuses.length; i++)
    {
        var row = document.getElementById(allStatuses[i]);
            if(row != null)
            {
                //try to get the date text box and check if its empty
                var name = allStatuses[i];
                name = "s" + name.slice(5) + "D";
                var dateTextBox = document.getElementById(name);
                if(dateTextBox == null)
                {
                    //switch on the inconsistent names
                    switch(allStatuses[i])
                    {
                        case "Loan_Open":
                                    dateTextBox = document.getElementById("sOpenedD");
                                    break;
                                case "Loan_Registered":
                                    dateTextBox = document.getElementById("sSubmitD");
                                    break;
                                case "Loan_Preapproval":
                                    dateTextBox = document.getElementById("sPreapprovD");
                                    break;
                                case "Loan_Approved":
                                    dateTextBox = document.getElementById("sApprovD");
                                    break;
                                case "Loan_Funded":
                                    dateTextBox = document.getElementById("sFundD");
                                    break;
                                case "Loan_Rejected":
                                    dateTextBox = document.getElementById("sRejectD");
                                    break;
                                case "Loan_LoanPurchased":
                                    dateTextBox = document.getElementById("sLPurchaseD");
                                    break;
                                case "Loan_Shipped":
                                    dateTextBox = document.getElementById("sShippedToInvestorD");
                                    break;
                                    case "Loan_InvestorConditions":
                                    dateTextBox = document.getElementById("sSuspendedByInvestorD");
                                    break;
                                    case "Loan_InvestorConditionsSent":
                                    dateTextBox = document.getElementById("sCondSentToInvestorD");
                                    break;
                    }
                }
                
                if(dateTextBox != null && dateTextBox.value != "")
                    row.style.display = "";
                    else
                    row.style.display = "none";
                
            }
    }
    for(var i = 0; i < statuses.length; i++)
    {
        var row = document.getElementById(statuses[i]);
        if(row != null)
        {   
            row.style.display = "";
        }
    }
    
            
}

function doAfterDateFormat(e) {
    onchange_dateWithTime(e);
}

function onchange_dateWithTime(e) {
    refreshCalculation();
}

function f_displayWarning(id, bVisible, o) {
   var obj = <%= AspxTools.JsGetElementById(sClosedN) %>;
   var rid = document.getElementById(id);
 
   if( bVisible ) 
   {
   var pos = findPos(obj);
     rid.style.top = (pos[1]-3) + "px";
     rid.style.left = (obj.offsetWidth + pos[0]) + "px";
     rid.style.display = "block";
    }
    else 
    {
     rid.style.display = "none";
        
    }

    if (typeof o != 'undefined') {
        try { o.focus(); } catch (e) { }
    }
}
function onDateStampClick() {
    var user = <%= AspxTools.JsString(BrokerUser.FirstName + " " + BrokerUser.LastName) %>;
                
    var timeStamp = getTimeStamp(user);

    var notes = <%= AspxTools.JsGetElementById(sTrNotes) %>;

    notes.value = timeStamp + notes.value;

    updateDirtyBit();
}

function findPos(obj) {
	var curleft = curtop = 0;
	if (obj.offsetParent) {
	do {
			curleft += obj.offsetLeft;
			curtop += obj.offsetTop;
		} while (obj = obj.offsetParent);
    }   
    return [curleft,curtop];
}
  
function f_validate_sOpenedD(e) {
  var o = <%= AspxTools.JsGetElementById(sOpenedD) %>;
  if (o.value == "") {
      alert(<%=AspxTools.JsString(JsMessages.LoanOpenedRequired) %>);
      o.focus();
      o.select();
  }
  else
  {
     date_onblur(null, e);
  }
}

function displayStatusLink() {
    var branchChannel = getBranchChannelEnum();
    var url = '/los/View/LoanStatusList.aspx?channel='+ branchChannel;
    
    if (branchChannel == <%= AspxTools.JsNumeric(E_BranchChannelT.Correspondent) %>)
    {
        url += "&process=" + <%= AspxTools.JsGetElementById(sCorrespondentProcessT) %>.value
    }
    
    showModal(url, null, null, null, function(arg){

        if (arg.OK) {
            updateDirtyBit();    
            <%= AspxTools.JsGetElementById(sStatus) %>.value = arg.Status;
            <%= AspxTools.JsGetElementById(sStatusT) %>.value = arg.StatusCode;
            
            var today = new Date();
            var dateString = (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear();
            var status = parseInt(arg.StatusCode);
            var field;
            if (status == 0) { //sOpenedD
                field = <%= AspxTools.JsGetElementById(sOpenedD) %>;
            } else if (status == 1) { //sPreQualD
                field = <%= AspxTools.JsGetElementById(sPreQualD) %>;
            } else if (status == 2) { // sPreApprovD
                field = <%= AspxTools.JsGetElementById(sPreApprovD) %>;
            } else if (status == 3) { // sSubmitD
                field = <%= AspxTools.JsGetElementById(sSubmitD) %>;
            } else if (status == 4) { // sApprovD
                field = <%= AspxTools.JsGetElementById(sApprovD) %>;
            } else if (status == 5) { // sDocsD
                field = <%= AspxTools.JsGetElementById(sDocsD) %>;    
            } else if (status == 6) { // sFundD
                field = <%= AspxTools.JsGetElementById(sFundD) %>;
            } else if (status == 7) { // sOnHoldD
                field = <%= AspxTools.JsGetElementById(sOnHoldD) %>;
            } else if (status == 8) { // sSuspendedD
                field = <%= AspxTools.JsGetElementById(sSuspendedD) %>;
            } else if (status == 9) { // sCanceledD
                field = <%= AspxTools.JsGetElementById(sCanceledD) %>;
            } else if (status == 10) { // sRejectD
                field = <%= AspxTools.JsGetElementById(sRejectD) %>;
            } else if (status == 11) { // sClosed
                field = <%= AspxTools.JsGetElementById(sClosedD) %>;
            } else if (status == 12) { //sLeadD
                field = <%= AspxTools.JsGetElementById(sLeadD) %>;
            } else if (status == 13) { // sUnderwritingD
                field = <%= AspxTools.JsGetElementById(sUnderwritingD) %>;
            } else if (status == 19) { //sRecordedD
                field = <%= AspxTools.JsGetElementById(sRecordedD) %>;
            } else if (status == 20) { //sShippedToInvestorD
                field = <%= AspxTools.JsGetElementById(sShippedToInvestorD) %>;
            } else if (status == 21) {
                field = <%= AspxTools.JsGetElementById(sClearToCloseD) %>;
            } else if (status == 22) {
                field = <%= AspxTools.JsGetElementById(sProcessingD) %>;
            } else if (status == 23) {
                field = <%= AspxTools.JsGetElementById(sFinalUnderwritingD) %>;
            } else if (status == 24) {
                field = <%= AspxTools.JsGetElementById(sDocsBackD) %>;
            } else if (status == 25) {
                field = <%= AspxTools.JsGetElementById(sFundingConditionsD) %>;
            } else if (status == 26) {
                field = <%= AspxTools.JsGetElementById(sFinalDocsD) %>;
            } else if (status == 27) {
                field = <%= AspxTools.JsGetElementById(sLPurchaseD) %>;
            } else if (status == 28) {
                field = <%= AspxTools.JsGetElementById(sLoanSubmittedD) %>;
            }
            else if (status == 29) { field = <%= AspxTools.JsGetElementById( sPreProcessingD) %>; }    
            else if (status == 30) { field = <%= AspxTools.JsGetElementById( sDocumentCheckD) %>; }
            else if (status == 31) { field = <%= AspxTools.JsGetElementById( sDocumentCheckFailedD) %>; }
            else if (status == 32) { field = <%= AspxTools.JsGetElementById( sPreUnderwritingD) %>; }
            else if (status == 33) { field = <%= AspxTools.JsGetElementById( sConditionReviewD) %>; }
            else if (status == 34) { field = <%= AspxTools.JsGetElementById( sPreDocQCD) %>; }
            else if (status == 35) { field = <%= AspxTools.JsGetElementById( sDocsOrderedD) %>; }
            else if (status == 36) { field = <%= AspxTools.JsGetElementById( sDocsDrawnD) %>; }
            else if (status == 37) { field = <%= AspxTools.JsGetElementById( sSuspendedByInvestorD) %>; }       
            else if (status == 38) { field = <%= AspxTools.JsGetElementById( sCondSentToInvestorD) %>; }  
            else if (status == 39) { field = <%= AspxTools.JsGetElementById( sReadyForSaleD) %>; }
            else if (status == 40) { field = <%= AspxTools.JsGetElementById( sSubmittedForPurchaseReviewD) %>; }
            else if (status == 41) { field = <%= AspxTools.JsGetElementById( sInPurchaseReviewD) %>; }
            else if (status == 42) { field = <%= AspxTools.JsGetElementById( sPrePurchaseConditionsD) %>; }
            else if (status == 43) { field = <%= AspxTools.JsGetElementById( sSubmittedForFinalPurchaseD) %>; }
            else if (status == 44) { field = <%= AspxTools.JsGetElementById( sInFinalPurchaseReviewD) %>; }
            else if (status == 45) { field = <%= AspxTools.JsGetElementById( sClearToCloseD) %>; }
            else if (status == 46) { field = <%= AspxTools.JsGetElementById( sPurchasedD) %>; }       
            else if (status == 47) { field = <%= AspxTools.JsGetElementById( sCounterOfferD) %>; }
            else if (status == 48) { field = <%= AspxTools.JsGetElementById( sWithdrawnD) %>; }
            else if (status == 49) { field = <%= AspxTools.JsGetElementById( sArchivedD) %>; }
            
            if( !!field && field.value.length == 0) {
                field.value = dateString;
            }
            if(field != null)
            document.getElementById("sStatusD").value = field.value;
        }
        return arg;
    },{hideCloseButton:true});

        }


            function _postSaveMe(results) {
                if (results["RefreshPage"]) {
                    window.location = window.location;
                }
            }
var GeneralPageObject = {};
GeneralPageObject.saveMe = window.saveMe;
function GetWhatStatusWouldBeIfWerentLocked(){
  var clientID = null;
  if (document.getElementById("_ClientID") != null)
    clientID = document.getElementById("_ClientID").value;
  // Since recalculate does not always required every field to be pass to server.
  // Add skipMe attribute to input that does not required in calculation.    
  var args = getAllFormValues(clientID, true); // GetStatusesObject();
  
  args.sStatusLckd = false;  
    
  var method = "CalculateData";
  if (clientID != null && clientID != "")
    method = clientID + "_CalculateData";
    
  var result = gService.loanedit.call(method, args);
  return result;
}
function onEditAddLender() {
    showModal('/newlos/Status/AlternateLender.aspx',null, null, null, null,{hideCloseButton:true});
    var method = "GetAlternateLenderList";
    var $lenderId = $('#sAlternateLender').val();
    var args = {
        'id': $lenderId
    };

    var result = gService.loanedit.call(method, args);
    $('#sAlternateLender option').remove();
    var ddl = $('#sAlternateLender');
    for(var i = 0; result.value['Name' + i] != null; i++)
    {
        ddl.append($('<option></option>').val(result.value['Id' + i]).text(result.value['Name' + i]));
    }
    $('#sAlternateLender').val($lenderId);
}
function GetFurthestDownNewStatus(){
    var args = new Object();
    var clientID = null;
    if (document.getElementById("_ClientID") != null)
        clientID = document.getElementById("_ClientID").value;
      // Since recalculate does not always required every field to be pass to server.
      // Add skipMe attribute to input that does not required in calculation.    
    args = GetStatusesObject(); //getAllFormValues(clientID, true);
    
    var method = "GetStatusOfFurthestDownNewStatus";
    if (clientID != null && clientID != "")
    method = clientID + "GetStatusOfFurthestDownNewStatus";
    
    var result = gService.loanedit.call(method, args);
    return result;
}
function GetStatusesObject()
{
    var theTable = document.getElementById('Table2');
    var inputs = theTable.getElementsByTagName('input');
    var input;
    var retObj = {};
    var numInputs = inputs.length;
    for(var i = 0; i < numInputs; i++)
    {
        input = inputs[i];
        if(input.getAttribute('preset') == 'date')
        {
            retObj[input.id] = input.value;
        }
    }
    // add in info for calculations.
    retObj['sStatusLckd'] = document.getElementById('sStatusLckd').checked;
    retObj['loanid'] = document.getElementById('loanid').value;
    retObj['applicationid'] = document.getElementById('applicationid').value;
    retObj['sBranchChannelT'] = getBranchChannelEnum() + '';
    retObj['sCorrespondentProcessT'] = <%= AspxTools.JsGetElementById(sCorrespondentProcessT) %>.value;
    return retObj;
}

function SelectTransferToWithModal()
{
    if (<%= AspxTools.JsBool(IsDocMagicEnabled) %>)
    {
        var queryString = "?code=" + ML.DocMagicPlanCode + '&vendorId=' + ML.DocMagicVendorId;
        showModal('/newlos/Services/TransferToPicker.aspx' + queryString, null, null, null, function(result){
            if (result && result.description && result.code)
            {
                document.getElementById("TransferTo").value = result.description + ' (' + result.code + ')';
                document.getElementById("TransferToInvestorName").value = result.description;
                document.getElementById("TransferToInvestorCode").value = result.code;
                updateDirtyBit();
            }
        },{hideCloseButton:true});
    }
}

window.saveMe = function(bRefreshScreen){
    var statusLockedObj = <%= AspxTools.JsGetElementById(sStatusLckd) %>;
    var currStatus = <%= AspxTools.JsGetElementById(sStatus) %>.value;
    var isLocked = statusLockedObj.checked;
    var result = GetFurthestDownNewStatus();
    if(result.error)
    {
        alert('unable to save. unable to get status (by new and furthest down) from server.');
        return;
    }
    var resultFurthestDown = result.value;
    var newLoanStatusFurthestDown = result.value.sStatus;
    if('Loan Other' == newLoanStatusFurthestDown)  // then there were no dates added/overwritten with new vals.
    {
        return GeneralPageObject.saveMe(bRefreshScreen);  
    }
    result = GetWhatStatusWouldBeIfWerentLocked();
    if(result.error)
    {
        alert('unable to save.  unable to get (latest new) status from server');
        return false;
    }
    var newLoanStatusByLatest = result.value.sStatus;
    
    
    var newLoanStatusesMatch = (newLoanStatusFurthestDown == newLoanStatusByLatest);

    if(!newLoanStatusesMatch && !('Loan Other' == newLoanStatusFurthestDown))
    {
        var arg = confirm('Do you want to change the loan status to '+newLoanStatusFurthestDown+'?');
        if(arg == true)
        {
//            statusLockedObj.checked = false; //! 109028
//            GeneralPageObject.saveMe(true);
            
            <%= AspxTools.JsGetElementById(sStatus) %>.value = resultFurthestDown.sStatus;
            <%= AspxTools.JsGetElementById(sStatusT) %>.value = resultFurthestDown.sStatusT;
            
            statusLockedObj.checked = true; 
            return GeneralPageObject.saveMe(bRefreshScreen);
        }
        else{
            // don't do anything to the loanStatus.
        }
        
    }
    else{ // if new loan statuses by both methods would match
        if(isLocked){
            // C
            statusLockedObj.checked = false;
            refreshCalculation();
        }
        else{ // if not locked.
            // B
            
        }
        // rely on current behavior to save page.
        
    }
    return GeneralPageObject.saveMe(bRefreshScreen);  
}
</script>

<form method="post" runat="server">
    <table class="FormTable" id="Table4" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td class="MainRightHeader" nowrap>
                Tracking: General
            </td>
        </tr>
        <tr><td height="10"></td></tr>
        <tr>
            <td nowrap>
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td class="FieldLabel" style="width: 127px" nowrap>
                            Origination Branch
                        </td>
                        <td>
                            <asp:TextBox ID="sBranchName" runat="server" Width="228px" ReadOnly="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            Branch Code
                        </td>
                        <td>
                            <asp:TextBox ID="sBranchCode" runat="server" Width="228px" ReadOnly="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            Alternate Lender
                        </td>
                        <td>
                            <asp:DropDownList ID="sAlternateLender" runat="server" Width="125px" ReadOnly="True">
                            </asp:DropDownList>
                            <input type="button" value="Edit/Add New" onclick="onEditAddLender();" id="sAlternateLenderBtn" />
                        </td>
                    </tr>
                    <tbody id="TransferToRow" runat="server">
                        <tr>
                            <td class="FieldLabel" nowrap>
                                Transfer To
                            </td>
                            <td>
                                <asp:TextBox ID="TransferTo" runat="server" ReadOnly="true" />
                                <input type="button" ID="TransferToButton" Value="Select" style="width: 88px;" onclick="SelectTransferToWithModal()"/>
                                <asp:HiddenField ID="TransferToInvestorName" runat="server" />
                                <asp:HiddenField ID="TransferToInvestorCode" runat="server" />
                            </td>
                        </tr>
                    </tbody>
                    <tr>
                        <td class="FieldLabel" nowrap><ml:EncodedLiteral ID="sIsBranchActAsOriginatorForFileLabel" runat="server"/></td>
                        <td onclick="refreshCalculation();"><asp:RadioButtonList ID="sIsBranchActAsOriginatorForFileTri" runat="server" RepeatDirection="Horizontal"/></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap><ml:EncodedLiteral ID="sIsBranchActAsLenderForFileLabel" runat="server" /></td>
                        <td onclick="refreshCalculation();"><asp:RadioButtonList ID="sIsBranchActAsLenderForFileTri" runat="server" RepeatDirection="Horizontal"/></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            Channel
                        </td>
                        <td><asp:TextBox ID="sBranchChannelT" runat="server" ReadOnly="true" />
                        </td>
                    </tr>
                    <tr ID="CorrespondentProcessRow">
                        <td class="FieldLabel" nowrap>
                            Correspondent Process Type
                        </td>
                        <td><asp:DropDownList ID="sCorrespondentProcessT" runat="server" onchange="refreshCalculation();" />
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            File Status&nbsp;&nbsp;<asp:CheckBox ID="sStatusLckd" runat="server" Text="Lock" onclick="refreshCalculation();"></asp:CheckBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sStatus" style="width: 228px" runat="server" ReadOnly="True"></asp:TextBox><input type="button" value="Change Status" style="width: 98px" onclick="displayStatusLink();" id="btnChangeStatus">&nbsp;&nbsp;
                            <input id="sStatusT" type="hidden" name="sStatusT" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            Loan Status Date
                        </td>
                        <td>
                            <input ID="sStatusD" runat="server" style="width: 75px;" ReadOnly/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" nowrap>
                <label class="d-inline-block w-184">Number of days since opened</label><asp:TextBox ID="daySinceOpened" runat="server" ReadOnly="True" Width="62px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td nowrap>
                <table id="Table2" runat="server" cellspacing="0" cellpadding="0" border="0">
                    <tr class="bg-subheader">
                        <td  nowrap>
                            Event
                        </td>
                        <td  style="width: 58px">
                            Date
                        </td>
                        <td >
                            Time
                        </td>
                        <td >
                            Comments
                        </td>
                        <td class="FormTableSubHeader touch-column">
                            Touches
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            <% if (Broker.HasLenderDefaultFeatures) { %>
                            <a href="javascript:linkMe('../LockDesk/BrokerRateLock.aspx');" title="Go to Rate Lock page" tabindex=-1>Rate Lock</a>
                            <% } else { %>
                            Rate Lock
                            <% } %>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sRLckdD" runat="server" CssClass="mask" Width="75" preset="date" onchange="refreshCalculation();"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sRLckdDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td style="height: 21px">
                            <asp:TextBox ID="sRLckdN" runat="server" Width="135" MaxLength="36" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            <% if (Broker.HasLenderDefaultFeatures) { %>
                            <a href="javascript:linkMe('../LockDesk/BrokerRateLock.aspx');" title="Go to Rate Lock page" tabindex=-1>Rate Lock Expiration</a>
                            <% } else { %>
                            Rate Lock Expiration
                            <% } %>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sRLckdExpiredD" runat="server" CssClass="mask" Width="75" preset="date" onchange="refreshCalculation();"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td style="height: 21px">
                            <asp:TextBox ID="sRLckdExpiredN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr  class="FieldLabel" nowrap>
                        <td>
                            Application Submitted
                        </td>
                        <td>
                            <ml:DateTextBox ID="sAppSubmittedD" runat="server" CssClass="mask" Width="75" preset="date" onchange="onDateAddTime(this);"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sAppSubmittedDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr  class="FieldLabel" nowrap>
                        <td>
                            App Received by Lender
                        </td>
                        <td>
                            <ml:DateTextBox ID="sAppReceivedByLenderD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sAppReceivedByLenderDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel" runat="server" id="UsdaAppDateRow">
                        <td>
                            App Submitted to USDA
                        </td>
                        <td>
                            <ml:DateTextBox ID="sAppSubmitedToUsdaD" runat="server" width="75" preset="date"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Intent to Proceed
                        </td>
                        <td>
                            <ml:DateTextBox ID="sIntentToProceedD" runat="server" width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td style="height: 21px">
                            <asp:TextBox ID="sIntentToProceedN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Estimated Closing
                        </td>
                        <td>
                            <ml:DateTextBox ID="sEstCloseD" runat="server" CssClass="mask" Width="75" preset="date" onchange="date_onblur(null, this);"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sEstCloseDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                        <td>
                            <asp:TextBox ID="sEstCloseN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="height: 21px; width: 127px;" nowrap>
                            &nbsp;
                        </td>
                        <td nowrap>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr id="Lead_NewRow">
                        <td class="FieldLabel" nowrap >
                            Lead New
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sLeadD" runat="server" CssClass="mask" Width="75" preset="date" ></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sLeadDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td style="height: 21px">
                            <asp:TextBox ID="sLeadN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesLeadNew" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_Open'>
                        <td class="FieldLabel" nowrap>
                            Loan Open
                        </td>
                        <td nowrap> 
                            <ml:DateTextBox ID="sOpenedD" runat="server" CssClass="mask" Width="75" preset="date" onchange="f_validate_sOpenedD(this);"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sOpenedDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sOpenedN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesLoanOpen" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_Prequal'>
                        <td class="FieldLabel" nowrap>
                            Pre-qual
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sPreQualD" runat="server" CssClass="mask" Width="75" preset="date" ></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPreQualDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPreQualN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesPrequal" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_Registered'>
                        <td class="FieldLabel" nowrap>
                            Registered
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sSubmitD" runat="server" CssClass="mask" Width="75" preset="date" ></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sSubmitDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sSubmitN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesRegistered" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_PreProcessing'>
                        <td class="FieldLabel" nowrap>
                        Pre-Processing
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPreProcessingD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPreProcessingDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPreProcessingN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesPreProcessing" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_Processing'>
                        <td class="FieldLabel" nowrap>
                            Processing
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sProcessingD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sProcessingDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sProcessingN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesProcessing" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_DocumentCheck'>
                        <td class="FieldLabel" nowrap>
                        Document Check
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocumentCheckD" runat="server" CssClass="mask" Width="75" preset="date" ></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sDocumentCheckDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sDocumentCheckN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesDocumentCheck" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_DocumentCheckFailed'>
                        <td class="FieldLabel" nowrap>
                        Document Check Failed
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocumentCheckFailedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sDocumentCheckFailedDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sDocumentCheckFailedN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesDocumentCheckFailed" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_LoanSubmitted'>
                        <td class="FieldLabel" nowrap>
                            Submitted
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sLoanSubmittedD" runat="server" CssClass="mask" Width="75" preset="date" ></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sLoanSubmittedDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sLoanSubmittedN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesSubmitted" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_PreUnderwriting'>
                        <td class="FieldLabel" nowrap>
                            Pre-Underwriting
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sPreUnderwritingD" runat="server" CssClass="mask" Width="75" preset="date" ></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPreUnderwritingDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPreUnderwritingN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesPreUnderwriting" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    
                    <tr style="display:none" id='Loan_Underwriting'>
                        <td class="FieldLabel" nowrap>
                            In Underwriting
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sUnderwritingD" runat="server"  preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sUnderwritingDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sUnderwritingN" runat="server" Width="135px"  MaxLength="21"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesUnderwriting" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_Preapproval'>
                        <td class="FieldLabel" nowrap>
                            Pre-approved
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sPreApprovD" runat="server" CssClass="mask" Width="75" preset="date" ></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPreApprovDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPreApprovN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesPreapproval" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_Approved'>
                        <td class="FieldLabel" nowrap>
                            Approved
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sApprovD" runat="server" CssClass="mask" Width="75" preset="date" ></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sApprovDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sApprovN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesApproved" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_ConditionReview'>
                        <td class="FieldLabel" nowrap>
                            Condition Review
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sConditionReviewD" runat="server" CssClass="mask" Width="75" preset="date" ></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sConditionReviewDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sConditionReviewN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesConditionReview" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_FinalUnderwriting'>
                        <td class="FieldLabel" nowrap>
                            Final Underwriting
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sFinalUnderwritingD" runat="server" CssClass="mask" Width="75" preset="date" ></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFinalUnderwritingDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFinalUnderwritingN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesFinalUnderwriting" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_PreDocQC'>
                        <td class="FieldLabel" nowrap>
                            Pre-Doc QC
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sPreDocQCD" runat="server" CssClass="mask" Width="75" preset="date" ></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPreDocQCDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPreDocQCN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesPreDocQC" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_ClearToClose'>
                        <td class="FieldLabel" nowrap>
                            Clear to Close
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sClearToCloseD" runat="server"  preset="date" Width="75" CssClass="mask" />
                        </td>                        
                        <td>
                            <asp:TextBox ID="sClearToCloseDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sClearToCloseN" runat="server" Width="135px"  />
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesClearToClose" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_DocsOrdered'>
                        <td class="FieldLabel" nowrap>
                            Docs Ordered
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sDocsOrderedD" runat="server"  preset="date" Width="75" CssClass="mask" />
                        </td>
                        <td>
                            <asp:TextBox ID="sDocsOrderedDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sDocsOrderedN" runat="server" Width="135px"  />
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesDocsOrdered" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_DocsDrawn'>
                        <td class="FieldLabel" nowrap>
                            Docs Drawn
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sDocsDrawnD" runat="server"  preset="date" Width="75" CssClass="mask" />
                        </td>
                        <td>
                            <asp:TextBox ID="sDocsDrawnDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sDocsDrawnN" runat="server" Width="135px"  />
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesDocsDrawn" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_Docs'>
                        <td class="FieldLabel" nowrap>
                            Docs Out
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sDocsD" runat="server" CssClass="mask" Width="75" preset="date" ></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sDocsDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sDocsN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesLoanDocs" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_DocsBack'>
                        <td class="FieldLabel" nowrap>
                            Docs Back
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sDocsBackD" runat="server"  preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sDocsBackDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sDocsBackN" runat="server" Width="135px"  MaxLength="36"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesDocsBack" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_FundingConditions'>
                        <td class="FieldLabel" nowrap>
                            Funding Conditions
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sFundingConditionsD" runat="server"  preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFundingConditionsDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFundingConditionsN" runat="server" Width="135px"  MaxLength="36"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesFundingConditions" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_Funded'>
                        <td class="FieldLabel" nowrap>
                            Funded
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sFundD" runat="server" CssClass="mask" Width="75" preset="date" ></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFundDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFundN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesFunded" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_Recorded'>
                        <td class="FieldLabel" nowrap>
                            Recorded
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sRecordedD" runat="server" CssClass="mask" Width="75" preset="date" ></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sRecordedDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sRecordedN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesRecorded" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_FinalDocs'>
                        <td class="FieldLabel" nowrap>
                            Final Docs
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sFinalDocsD" runat="server"  preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFinalDocsDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFinalDocsN" runat="server" Width="135px"  MaxLength="36"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesFinalDocs" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_Closed'>
                        <td class="FieldLabel" nowrap>
                            Loan Closed
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sClosedD" runat="server" onblur="f_displayWarning('CloseWarningLabel', false);" onfocus="f_displayWarning('CloseWarningLabel', true);" onchange="f_displayWarning('CloseWarningLabel', true, this); ;" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sClosedDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sClosedN" runat="server" Width="135px"  MaxLength="21"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesClosed" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_SubmittedForPurchaseReview'>
                        <td class="FieldLabel" nowrap>
                            Submitted for Purchase Review
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sSubmittedForPurchaseReviewD" runat="server" onblur="f_displayWarning('CloseWarningLabel', false);" onfocus="f_displayWarning('CloseWarningLabel', true);" onchange="f_displayWarning('CloseWarningLabel', true, this); ;" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sSubmittedForPurchaseReviewDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sSubmittedForPurchaseReviewN" runat="server" Width="135px"  MaxLength="21"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesSubmittedForPurchaseReview" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_InPurchaseReview'>
                        <td class="FieldLabel" nowrap>
                            In Purchase Review
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sInPurchaseReviewD" runat="server" onblur="f_displayWarning('CloseWarningLabel', false);" onfocus="f_displayWarning('CloseWarningLabel', true);" onchange="f_displayWarning('CloseWarningLabel', true, this); ;" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sInPurchaseReviewDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sInPurchaseReviewN" runat="server" Width="135px"  MaxLength="21"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesInPurchaseReview" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_PrePurchaseConditions'>
                        <td class="FieldLabel" nowrap>
                            Pre-Purchase Conditions
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sPrePurchaseConditionsD" runat="server" onblur="f_displayWarning('CloseWarningLabel', false);" onfocus="f_displayWarning('CloseWarningLabel', true);" onchange="f_displayWarning('CloseWarningLabel', true, this); ;" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPrePurchaseConditionsDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPrePurchaseConditionsN" runat="server" Width="135px"  MaxLength="21"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesPrePurchaseConditions" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_SubmittedForFinalPurchaseReview'>
                        <td class="FieldLabel" nowrap>
                            Submitted for Final Purchase Review
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sSubmittedForFinalPurchaseD" runat="server" onblur="f_displayWarning('CloseWarningLabel', false);" onfocus="f_displayWarning('CloseWarningLabel', true);" onchange="f_displayWarning('CloseWarningLabel', true, this); ;" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sSubmittedForFinalPurchaseDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sSubmittedForFinalPurchaseN" runat="server" Width="135px"  MaxLength="21"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesSubmittedForFinalPurchaseReview" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_InFinalPurchaseReview'>
                        <td class="FieldLabel" nowrap>
                            In Final Purchase Review
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sInFinalPurchaseReviewD" runat="server" onblur="f_displayWarning('CloseWarningLabel', false);" onfocus="f_displayWarning('CloseWarningLabel', true);" onchange="f_displayWarning('CloseWarningLabel', true, this); ;" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sInFinalPurchaseReviewDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sInFinalPurchaseReviewN" runat="server" Width="135px"  MaxLength="21"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesInFinalPurchaseReview" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_ClearToPurchase'>
                        <td class="FieldLabel" nowrap>
                            Clear to Purchase
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sClearToPurchaseD" runat="server" onblur="f_displayWarning('CloseWarningLabel', false);" onfocus="f_displayWarning('CloseWarningLabel', true);" onchange="f_displayWarning('CloseWarningLabel', true, this); ;" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sClearToPurchaseDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sClearToPurchaseN" runat="server" Width="135px"  MaxLength="21"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesClearToPurchase" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_Purchased'>
                        <td class="FieldLabel" nowrap>
                            Loan Purchased
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sPurchasedD" runat="server" onblur="f_displayWarning('CloseWarningLabel', false);" onfocus="f_displayWarning('CloseWarningLabel', true);" onchange="f_displayWarning('CloseWarningLabel', true, this); ;" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPurchasedDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPurchasedN" runat="server" Width="135px"  MaxLength="21"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesPurchased" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_ReadyForSale'>
                        <td class="FieldLabel" nowrap>
                            Ready for Sale
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sReadyForSaleD" runat="server" onblur="f_displayWarning('CloseWarningLabel', false);" onfocus="f_displayWarning('CloseWarningLabel', true);" onchange="f_displayWarning('CloseWarningLabel', true, this); ;" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sReadyForSaleDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sReadyForSaleN" runat="server" Width="135px"  MaxLength="21"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesReadyForSale" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id="Loan_Shipped">
                        <td class="FieldLabel" nowrap>
                            Loan Shipped
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sShippedToInvestorD" runat="server" onchange="refreshCalculation();" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sShippedToInvestorDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sShippedToInvestorN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesShipped" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr id="Loan_InvestorConditions">
                        <td class="FieldLabel" nowrap>
                            Investor Conditions
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sSuspendedByInvestorD" runat="server" onchange="refreshCalculation();" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sSuspendedByInvestorDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>    
                        </td>
                        <td>
                            <asp:TextBox ID="sSuspendedByInvestorN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesInvestorConditions" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr id="Loan_InvestorConditionsSent">
                        <td class="FieldLabel" nowrap>
                            Investor Conditions Sent
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sCondSentToInvestorD" runat="server" onchange="refreshCalculation();" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sCondSentToInvestorDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sCondSentToInvestorN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesInvestorConditionsSent" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr id="Additional_Conditions_Sent">
                        <td class="FieldLabel" nowrap>
                            Additional Conditions Sent
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sAdditionalCondSentD" runat="server" onchange="refreshCalculation();" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                    </tr>                    
                    <tr id='Loan_LoanPurchased' style="display:none;">
                        <td class="FieldLabel" nowrap >
                            Loan Sold
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sLPurchaseD" runat="server" onchange="refreshCalculation();" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sLPurchaseDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sLPurchasedN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesLoanPurchased" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            &nbsp;
                        </td>
                        <td nowrap>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_CounterOffer'>
                        <td class="FieldLabel" nowrap>
                            Counter Offer Approved
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sCounterOfferD" runat="server" CssClass="mask" Width="75" preset="date" onchange="refreshCalculation();"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sCounterOfferDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sCounterOfferN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesCounterOffer" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_OnHold'>
                        <td class="FieldLabel" nowrap>
                            Loan On-hold
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sOnHoldD" runat="server" CssClass="mask" Width="75" preset="date" onchange="refreshCalculation();"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sOnHoldDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sOnHoldN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesOnHold" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_Canceled'>
                        <td class="FieldLabel" nowrap>
                            Loan Canceled
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sCanceledD" runat="server" CssClass="mask" Width="75" preset="date" onchange="refreshCalculation();"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sCanceledDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sCanceledN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesCanceled" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_Suspended'>
                        <td class="FieldLabel" nowrap>
                            Loan Suspended
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sSuspendedD" runat="server" CssClass="mask" Width="75" preset="date" onchange="refreshCalculation();"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sSuspendedDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sSuspendedN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesSuspended" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_Rejected'>
                        <td class="FieldLabel" nowrap>
                            Loan Denied
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sRejectD" runat="server" CssClass="mask" Width="75" preset="date" onchange="refreshCalculation();"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sRejectDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sRejectN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesRejected" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_Withdrawn'>
                        <td class="FieldLabel" nowrap>
                            Loan Withdrawn
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sWithdrawnD" runat="server" CssClass="mask" Width="75" preset="date" onchange="refreshCalculation();"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sWithdrawnDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sWithdrawnN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesWithdrawn" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>
                    <tr style="display:none" id='Loan_Archived'>
                        <td class="FieldLabel" nowrap>
                            Loan Archived
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sArchivedD" runat="server" CssClass="mask" Width="75" preset="date" onchange="refreshCalculation();"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sArchivedDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sArchivedN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNumTouchesArchived" runat="server" readonly class="touch-textbox"/>
                        </td>
                    </tr>

                    <tr>
                        <td class="FieldLabel" nowrap>
                            &nbsp;
                        </td>
                        <td nowrap>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr id="QC_Comp_Date">
                        <td class="FieldLabel" nowrap>
                            QC Completed Date
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sQCCompDate" runat="server" onchange="date_onblur(null, this); ;" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sQCCompDateTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            Credit Decision
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sCreditDecisionD" runat="server" onchange="date_onblur(null, this);" preset="date" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sCreditDecisionDTime" runat="server" CssClass="time" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td class="displayNone">
                            <asp:CheckBox ID="sCreditDecisionDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            &nbsp;
                        </td>
                        <td nowrap>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            "Good-bye" Letter Date
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sGoodByLetterD" runat="server" onchange="refreshCalculation();" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            Servicing Start Date
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sServicingStartD" runat="server" onchange="refreshCalculation();" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            Scheduled Funding
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sSchedFundD" runat="server"  preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sSchedFundDTime" runat="server" Width="75" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sSchedFundN" runat="server" Width="135px"  MaxLength="36"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td nowrap>
                <table id="Table3" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="FieldLabel">
                            Lender Case Num
                        </td>
                        <td class="FieldLabel" colspan="3">
                            <asp:TextBox ID="sLenderCaseNum" runat="server" MaxLength="21"></asp:TextBox><asp:CheckBox ID="sLenderCaseNumLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Agency Case Num
                        </td>
                        <td style="width: 73px" colspan="3">
                            <asp:TextBox ID="sAgencyCaseNum" runat="server" MaxLength="21"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            Case Assignment Date
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sCaseAssignmentD" runat="server" onchange="refreshCalculation();" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            MERS MIN
                        </td>
                        <td style="width: 73px" colspan="3">
                            <asp:TextBox ID="sMersMin" runat="server" MaxLength="18" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 149px">
                        </td>
                        <td  style="width: 73px" colspan="3">
                        </td>
                        <td >
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <uc:RespaDates runat="server" ID="RespaDates"></uc:RespaDates>
                        </td>
                    </tr>
                    <tr class="bg-subheader">
                        <td  style="width: 149px">
                            Custom Events
                        </td>
                        <td  style="width: 130px" colspan="2">
                            Event Description
                        </td>
                        <td  style="width: 74px">
                            Date
                        </td>
                        <td >
                            Comments
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px">
                            Custom Event #1
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="sU1LStatDesc" runat="server" Width="200px" MaxLength="21"></asp:TextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU1LStatD" runat="server" onchange="refreshCalculation();" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="sU1LStatN" runat="server" Width="135px"  MaxLength="21"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px">
                            Custom Event #2
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="sU2LStatDesc" runat="server" Width="200px" MaxLength="21"></asp:TextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU2LStatD" runat="server" onchange="refreshCalculation();" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="sU2LStatN" runat="server" Width="135px"  MaxLength="21"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px">
                            Custom Event #3
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="sU3LStatDesc" runat="server" Width="200px" MaxLength="21"></asp:TextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU3LStatD" runat="server" onchange="refreshCalculation();" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="sU3LStatN" runat="server" Width="135px"  MaxLength="21"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px">
                            Custom Event #4
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="sU4LStatDesc" runat="server" Width="200px" MaxLength="21"></asp:TextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU4LStatD" runat="server" onchange="refreshCalculation();" preset="date" Width="75" CssClass="mask"></ml:DateTextBox>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="sU4LStatN" runat="server" Width="135px"  MaxLength="21"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="bg-subheader">
                        <td  colspan="5">
                            Disclosure/Document Dates
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Application Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicApplicationD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicApplicationDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            GFE Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicGFED" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicGFEDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Est. Available Through
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicEstAvailableThroughD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicEstAvailableThroughDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Document Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicDocumentD" runat="server" CssClass="mask" Width="75" preset="date" onchange="onDateAddTime(this);" ></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicDocumentDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Closing Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicClosingD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicClosingDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Signing Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicSigningD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicSigningDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Cancel Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicCancelD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicCancelDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Disbursement Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicDisbursementD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicDisbursementDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Document Expiration Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocExpirationD" runat="server" CssClass="mask" Width="75" preset="date" onchange="onDateAddTime(this);"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocExpirationDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Rate Lock Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicRateLockD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicRateLockDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Pre-Z Send Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicPreZSentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicPreZSentDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Re-Disc Send Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicReDiscSendD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicReDiscSendDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Re-Disc Method
                        </td>
                        <td>
                            <asp:DropDownList ID="sDocMagicRedisclosureMethodT" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicRedisclosureMethodTLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Re-Disc Received Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sDocMagicReDiscReceivedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="sDocMagicReDiscReceivedDLckd" runat="server" onclick="refreshCalculation();" />Lock
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Homeowner Counsel Disc Date
                        </td>
                        <td colspan="2">
                            <ml:DateTextBox ID="sHomeownerCounselingOrganizationDisclosureD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Disclosures Mailed Date
                        </td>
                        <td colspan="2">
                            <ml:DateTextBox ID="sDisclosuresMailedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            E-Sign Documents on File
                        </td>
                        <td colspan=2>
                            <asp:DropDownList ID="sHasESignedDocumentsT" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="bg-subheader">
                        <td  colspan="5">
                            Credit Report Authorization
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <uc1:CreditReportAuthorization runat="server" ID="CreditReportAuthorizationCtrl"></uc1:CreditReportAuthorization>
                        </td>
                    </tr>
                    <tr class="bg-subheader">
                        <td  colspan="5">
                            Purchase Contract Dates
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Purchase Contract Date
                        </td>
                        <td colspan="2">
                            <ml:DateTextBox ID="sPurchaseContractDate" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td>
                            Financing Contingency Exp. Date
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFinancingContingencyExpD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            Extension Date
                        </td>
                        <td colspan="2">
                            <ml:DateTextBox ID="sFinancingContingencyExtensionExpD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>    
    <table class="FormTable" id="Table1" cellspacing="0" cellpadding="0" border="0">                
        <tr>
            <td nowrap>
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr class="bg-subheader">
                        <td  style="width: 160px">
                            Other Documents
                        </td>
                        <td  style="width: 100px">
                            Ordered
                        </td>
                        <td  style="width: 100px">
                            Due
                        </td>
                        <td  style="width: 100px">
                            Document Date
                        </td>
                        <td  style="width: 100px">
                            Received
                        </td>
                        <td >
                            Comments
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px">
                            Preliminary Title Report
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sPrelimRprtOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sPrelimRprtDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPrelimRprtDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPrelimRprtRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPrelimRprtN" runat="server" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            Closing Services
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sClosingServOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sClosingServDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sClosingServDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sClosingServRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sClosingServN" runat="server" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px">
                            Appraisal Report
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sApprRprtOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sApprRprtDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sSpValuationEffectiveD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sApprRprtRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sApprRprtN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="TILGFERow" runat="server">
                        <td class="FieldLabel" style="width: 149px">
                            TIL Disclosure / GFE
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sTilGfeOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sTilGfeDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sTilGfeDocumentD" runat="server" CssClass="mask" ReadOnly="true" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sTilGfeRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sTilGfeN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            Flood Certificate
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFloodCertOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFloodCertDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFloodCertificationDeterminationD" runat="server" CssClass="mask" ReadOnly="true" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFloodCertRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFloodCertN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            USPS Check  
                        </td>
                        <td>
                            <ml:DateTextBox ID="sUSPSCheckOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sUSPSCheckDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sUSPSCheckDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sUSPSCheckRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sUSPSCheckN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            Payoff/Demand Statement  
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPayDemStmntOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPayDemStmntDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPayDemStmntDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPayDemStmntRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPayDemStmntN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            CAIVRS  
                        </td>
                        <td>
                            <ml:DateTextBox ID="sCAIVRSOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sCAIVRSDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sCAIVRSDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sCAIVRSRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sCAIVRSN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            Fraud Services  
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFraudServOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFraudServDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFraudServDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFraudServRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFraudServN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            AVM
                        </td>
                        <td>
                            <ml:DateTextBox ID="sAVMOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sAVMDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sAVMDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sAVMRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sAVMN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            HOA Certification
                        </td>
                        <td>
                            <ml:DateTextBox ID="sHOACertOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sHOACertDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sHOACertDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sHOACertRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sHOACertN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            Estimated HUD
                        </td>
                        <td>
                            <ml:DateTextBox ID="sEstHUDOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sEstHUDDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sEstHUDDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sEstHUDRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sEstHUDN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            CPL/ICL
                        </td>
                        <td>
                            <ml:DateTextBox ID="sCPLAndICLOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sCPLAndICLDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sCPLAndICLDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sCPLAndICLRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sCPLAndICLN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            Wire Instructions
                        </td>
                        <td>
                            <ml:DateTextBox ID="sWireInstructOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sWireInstructDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sWireInstructDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sWireInstructRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sWireInstructN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px;">
                            Insurance(s)
                        </td>
                        <td>
                            <ml:DateTextBox ID="sInsurancesOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sInsurancesDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sInsurancesDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sInsurancesRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sInsurancesN" runat="server" CssClass="mask" Width="135" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 149px">
                            <asp:TextBox ID="sU1DocStatDesc" runat="server" Width="131" MaxLength="21"></asp:TextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU1DocStatOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU1DocStatDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU1DocDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU1DocStatRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sU1DocStatN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 149px">
                            <asp:TextBox ID="sU2DocStatDesc" runat="server" Width="131px" MaxLength="21"></asp:TextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU2DocStatOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU2DocStatDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU2DocDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU2DocStatRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sU2DocStatN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 149px">
                            <asp:TextBox ID="sU3DocStatDesc" runat="server" Width="131px" MaxLength="21"></asp:TextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU3DocStatOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU3DocStatDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU3DocDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU3DocStatRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sU3DocStatN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 149px">
                            <asp:TextBox ID="sU4DocStatDesc" runat="server" Width="131px" MaxLength="21"></asp:TextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU4DocStatOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU4DocStatDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU4DocDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU4DocStatRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sU4DocStatN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 149px">
                            <asp:TextBox ID="sU5DocStatDesc" runat="server" Width="131px" MaxLength="21"></asp:TextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU5DocStatOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU5DocStatDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sU5DocDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sU5DocStatRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sU5DocStatN" runat="server" Width="135px" MaxLength="21" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Loan Package
                        </td>
                        <td>
                            <ml:DateTextBox ID="sLoanPackageOrderedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td>
                            <ml:DateTextBox ID="sLoanPackageDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sLoanPackageReceivedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sLoanPackageN" Width="135px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Mortgage / Deed of Trust
                        </td>
                        <td>
                            <ml:DateTextBox ID="sMortgageDOTOrderedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td>
                            <ml:DateTextBox ID="sMortgageDOTDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sMortgageDOTReceivedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sMortgageDOTN" Width="135px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" style="width: 149px">
                            Note
                        </td>
                        <td>
                            <ml:DateTextBox ID="sNoteOrderedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td>
                            <ml:DateTextBox ID="sDocumentNoteD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sNoteReceivedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sNoteN" Width="135px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="HUD1SettlementRow" runat="server">
                        <td class="FieldLabel">
                            Final HUD-1 Settlement Stmt
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFinalHUD1SttlmtStmtOrderedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td>
                            <ml:DateTextBox ID="sFinalHUD1SttlmtStmtDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFinalHUD1SttlmtStmtReceivedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFinalHUD1SttlmtStmtN" Width="135px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Title Insurance Policy
                        </td>
                        <td>
                            <ml:DateTextBox ID="sTitleInsPolicyOrderedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td>
                            <ml:DateTextBox ID="sTitleInsPolicyDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sTitleInsPolicyReceivedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sTitleInsPolicyN" Width="135px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Mortgage Ins. Certificate
                        </td>
                        <td>
                            <ml:DateTextBox ID="sMiCertOrderedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td>
                            <ml:DateTextBox ID="sMiCertIssuedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sMiCertReceivedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sMiCertN" Width="135px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Final Hazard Ins Policy
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFinalHazInsPolicyOrderedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td>
                            <ml:DateTextBox ID="sFinalHazInsPolicyDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFinalHazInsPolicyReceivedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFinalHazInsPolicyN" Width="135px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            Final Flood Ins Policy
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFinalFloodInsPolicyOrderedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td></td>
                        <td>
                            <ml:DateTextBox ID="sFinalFloodInsPolicyDocumentD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFinalFloodInsPolicyReceivedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sFinalFloodInsPolicyN" Width="135px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            &nbsp;
                        </td>
                        <td colspan="3">
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table id="Table5" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td class="FieldLabel" style="width: 149px" nowrap>
                Lead Source
            </td>
            <td nowrap>
                <asp:DropDownList ID="sLeadSrcDesc" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <table id="Table6" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td class="FieldLabel">
                &nbsp;
            </td>
            <td colspan="3">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="LoanFormHeader" colspan="5" rowspan="1">
                For Borrower
                <ml:EncodedLiteral ID="aBNm" runat="server"></ml:EncodedLiteral>
            </td>
        </tr>
        <tr class="bg-subheader">
            <td  style="width: 170px">
                Report Type
            </td>
            <td  style="width: 73px">
                Ordered
            </td>
            <td  nowrap>
                Due
            </td>
            <td  style="width: 74px">
                Received
            </td>
            <td >
                Comments
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="width: 170px">
                Credit Report
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aCrOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aCrDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aCrRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td>
                <asp:TextBox ID="aCrN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="width: 170px">
                LQI Credit Report
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aLqiCrOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aLqiCrDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aLqiCrRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td>
                <asp:TextBox ID="aLqiCrN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="width: 170px">
                Business Credit Report
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aBusCrOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aBusCrDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aBusCrRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td>
                <asp:TextBox ID="aBusCrN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 170px">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td class="FieldLabel" width="98px">
                            Pre-Funding VOE
                        </td>
                        <td>
                            <asp:DropDownList ID="aBPreFundVoeTypeT" Width="71px" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aBPreFundVoeOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aBPreFundVoeDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aBPreFundVoeRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td>
                <asp:TextBox ID="aBPreFundVoeN" runat="server" Width="135" MaxLength="36" ></asp:TextBox>
            </td>
        </tr> 
        <tr>
            <td class="FieldLabel" style="width: 170px">
                <asp:TextBox ID="aU1DocStatDesc" runat="server" Width="169" MaxLength="21"></asp:TextBox>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aU1DocStatOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aU1DocStatDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aU1DocStatRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td>
                <asp:TextBox ID="aU1DocStatN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="width: 170px">
                <asp:TextBox ID="aU2DocStatDesc" runat="server" Width="169px" MaxLength="21"></asp:TextBox>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aU2DocStatOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aU2DocStatDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aU2DocStatRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td>
                <asp:TextBox ID="aU2DocStatN" runat="server" Width="135px" MaxLength="36" ></asp:TextBox>
            </td>
        </tr>
        <tr class="bg-subheader">
            <td  style="width: 149px">
                Service
            </td>
            <td  style="width: 73px">
                Ordered
            </td>
            <td  nowrap>
                Deactivated
            </td>
            <td  style="width: 74px">
                File Number
            </td>
            <td ></td>
        </tr>
        <tr>
            <td class="FieldLabel" style="width: 149px">
                UDN
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aUDNOrderedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aUDNDeactivatedD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td nowrap>
                <asp:TextBox ID="aUdnOrderId" runat="server" Width="96"></asp:TextBox>
            </td>
            <td></td>
        </tr>
    </table>
    <table id="Table7" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td class="FieldLabel">
                &nbsp;
            </td>
            <td colspan="3">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="LoanFormHeader" colspan="5" rowspan="1">
                For Co-Borrower <ml:EncodedLiteral ID="aCNm" runat="server"></ml:EncodedLiteral>
            </td> 
        </tr>
        <tr class="bg-subheader">
            <td  style="width: 170px">
                Report Type
            </td>
            <td  style="width: 73px">
                Ordered
            </td>
            <td  nowrap>
                Due
            </td>
            <td  style="width: 74px">
                Received
            </td>
            <td >
                Comments
            </td>
        </tr>
        <tr>
            <td style="width: 170px">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td class="FieldLabel" width="98px">
                            Pre-Funding VOE
                        </td>
                        <td>
                            <asp:DropDownList ID="aCPreFundVoeTypeT" Width="71px" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aCPreFundVoeOd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aCPreFundVoeDueD" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td nowrap>
                <ml:DateTextBox ID="aCPreFundVoeRd" runat="server" CssClass="mask" Width="75" preset="date"></ml:DateTextBox>
            </td>
            <td>
                <asp:TextBox ID="aCPreFundVoeN" runat="server" Width="135" MaxLength="36" ></asp:TextBox>
            </td>
        </tr> 
    </table>
    </TD> </TR> </TABLE>
    <div>
        <span class="FieldLabel">Notes</span>
        <input type="button" value="Date &amp; time stamp" onclick="onDateStampClick();">
        <div>
            <asp:TextBox ID="sTrNotes" runat="server" Width="578px" Height="90px" TextMode="MultiLine">
            </asp:TextBox>
        </div>
    </div>
    </form>
    <div class="WarningLabelStyle" id="CloseWarningLabel">
        WARNING: &nbsp;Modifying the closed date could remove your permission to edit this loan.
    </div>

</body>
</html>
