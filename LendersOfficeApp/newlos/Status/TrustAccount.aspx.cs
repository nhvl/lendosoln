using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos.Status
{
	public partial class TrustAccount : BaseLoanPage
	{
        #region Protected member variables
        #endregion

        protected override void LoadData() 
        {
            CPageData dataLoan =  CPageData.CreateUsingSmartDependency( LoanID , typeof(TrustAccount));
            dataLoan.InitLoad();

            if(dataLoan.IsTemplate)
                ClientScript.RegisterHiddenField("_IsTemplate", "True");
            else
                ClientScript.RegisterHiddenField("_IsTemplate", "False");

            sTrust1PayableChkNum.Text	= dataLoan.sTrust1PayableChkNum.Value;
            sTrust1ReceivableChkNum.Text = dataLoan.sTrust1ReceivableChkNum.Value;
            sTrust1ReceivableN.Text		= dataLoan.sTrust1ReceivableN;
            //TrustModifiedBy.Text			= dataLoan.sTrust1ModifiedByLoginNm;
            sTrust1Desc.Text				= dataLoan.sTrust1Desc;
            sTrust1D.Text				= dataLoan.sTrust1D_rep;
            sTrust1PayableAmt.Text		= dataLoan.sTrust1PayableAmt_rep;
            sTrust1ReceivableAmt.Text	= dataLoan.sTrust1ReceivableAmt_rep;

            sTrust2PayableChkNum.Text	= dataLoan.sTrust2PayableChkNum.Value;
            sTrust2ReceivableChkNum.Text = dataLoan.sTrust2ReceivableChkNum.Value;
            sTrust2ReceivableN.Text		= dataLoan.sTrust2ReceivableN;
            //TrustModifiedBy.Text			= dataLoan.sTrust2ModifiedByLoginNm;
            sTrust2Desc.Text				= dataLoan.sTrust2Desc;
            sTrust2D.Text				= dataLoan.sTrust2D_rep;
            sTrust2PayableAmt.Text		= dataLoan.sTrust2PayableAmt_rep;
            sTrust2ReceivableAmt.Text	= dataLoan.sTrust2ReceivableAmt_rep;

            sTrust3PayableChkNum.Text	= dataLoan.sTrust3PayableChkNum.Value;
            sTrust3ReceivableChkNum.Text = dataLoan.sTrust3ReceivableChkNum.Value;
            sTrust3ReceivableN.Text		= dataLoan.sTrust3ReceivableN;
            //TrustModifiedBy.Text			= dataLoan.sTrust3ModifiedByLoginNm;
            sTrust3Desc.Text				= dataLoan.sTrust3Desc;
            sTrust3D.Text				= dataLoan.sTrust3D_rep;
            sTrust3PayableAmt.Text		= dataLoan.sTrust3PayableAmt_rep;
            sTrust3ReceivableAmt.Text	= dataLoan.sTrust3ReceivableAmt_rep;

            sTrust4PayableChkNum.Text	= dataLoan.sTrust4PayableChkNum.Value;
            sTrust4ReceivableChkNum.Text = dataLoan.sTrust4ReceivableChkNum.Value;
            sTrust4ReceivableN.Text		= dataLoan.sTrust4ReceivableN;
            //TrustModifiedBy.Text			= dataLoan.sTrust4ModifiedByLoginNm;
            sTrust4Desc.Text				= dataLoan.sTrust4Desc;
            sTrust4D.Text				= dataLoan.sTrust4D_rep;
            sTrust4PayableAmt.Text		= dataLoan.sTrust4PayableAmt_rep;
            sTrust4ReceivableAmt.Text	= dataLoan.sTrust4ReceivableAmt_rep;

            sTrust5PayableChkNum.Text	= dataLoan.sTrust5PayableChkNum.Value;
            sTrust5ReceivableChkNum.Text = dataLoan.sTrust5ReceivableChkNum.Value;
            sTrust5ReceivableN.Text		= dataLoan.sTrust5ReceivableN;
            //TrustModifiedBy.Text			= dataLoan.sTrust5ModifiedByLoginNm;
            sTrust5Desc.Text				= dataLoan.sTrust5Desc;
            sTrust5D.Text				= dataLoan.sTrust5D_rep;
            sTrust5PayableAmt.Text		= dataLoan.sTrust5PayableAmt_rep;
            sTrust5ReceivableAmt.Text	= dataLoan.sTrust5ReceivableAmt_rep;

            sTrust6PayableChkNum.Text	= dataLoan.sTrust6PayableChkNum.Value;
            sTrust6ReceivableChkNum.Text = dataLoan.sTrust6ReceivableChkNum.Value;
            sTrust6ReceivableN.Text		= dataLoan.sTrust6ReceivableN;
            //TrustModifiedBy.Text			= dataLoan.sTrust6ModifiedByLoginNm;
            sTrust6Desc.Text				= dataLoan.sTrust6Desc;
            sTrust6D.Text				= dataLoan.sTrust6D_rep;
            sTrust6PayableAmt.Text		= dataLoan.sTrust6PayableAmt_rep;
            sTrust6ReceivableAmt.Text	= dataLoan.sTrust6ReceivableAmt_rep;

            sTrust7PayableChkNum.Text	= dataLoan.sTrust7PayableChkNum.Value;
            sTrust7ReceivableChkNum.Text = dataLoan.sTrust7ReceivableChkNum.Value;
            sTrust7ReceivableN.Text		= dataLoan.sTrust7ReceivableN;
            //TrustModifiedBy.Text			= dataLoan.sTrust7ModifiedByLoginNm;
            sTrust7Desc.Text				= dataLoan.sTrust7Desc;
            sTrust7D.Text				= dataLoan.sTrust7D_rep;
            sTrust7PayableAmt.Text		= dataLoan.sTrust7PayableAmt_rep;
            sTrust7ReceivableAmt.Text	= dataLoan.sTrust7ReceivableAmt_rep;

            sTrust8PayableChkNum.Text	= dataLoan.sTrust8PayableChkNum.Value;
            sTrust8ReceivableChkNum.Text = dataLoan.sTrust8ReceivableChkNum.Value;
            sTrust8ReceivableN.Text		= dataLoan.sTrust8ReceivableN;
            //TrustModifiedBy.Text			= dataLoan.sTrust8ModifiedByLoginNm;
            sTrust8Desc.Text				= dataLoan.sTrust8Desc;
            sTrust8D.Text				= dataLoan.sTrust8D_rep;
            sTrust8PayableAmt.Text		= dataLoan.sTrust8PayableAmt_rep;
            sTrust8ReceivableAmt.Text	= dataLoan.sTrust8ReceivableAmt_rep;

            sTrust9PayableChkNum.Text	= dataLoan.sTrust9PayableChkNum.Value;
            sTrust9ReceivableChkNum.Text = dataLoan.sTrust9ReceivableChkNum.Value;
            sTrust9ReceivableN.Text		= dataLoan.sTrust9ReceivableN;
            //TrustModifiedBy.Text			= dataLoan.sTrust9ModifiedByLoginNm;
            sTrust9Desc.Text				= dataLoan.sTrust9Desc;
            sTrust9D.Text				= dataLoan.sTrust9D_rep;
            sTrust9PayableAmt.Text		= dataLoan.sTrust9PayableAmt_rep;
            sTrust9ReceivableAmt.Text	= dataLoan.sTrust9ReceivableAmt_rep;

            sTrust10PayableChkNum.Text	= dataLoan.sTrust10PayableChkNum.Value;
            sTrust10ReceivableChkNum.Text = dataLoan.sTrust10ReceivableChkNum.Value;
            sTrust10ReceivableN.Text		= dataLoan.sTrust10ReceivableN;
            //TrustModifiedBy.Text			= dataLoan.sTrust10ModifiedByLoginNm;
            sTrust10Desc.Text				= dataLoan.sTrust10Desc;
            sTrust10D.Text				= dataLoan.sTrust10D_rep;
            sTrust10PayableAmt.Text		= dataLoan.sTrust10PayableAmt_rep;
            sTrust10ReceivableAmt.Text	= dataLoan.sTrust10ReceivableAmt_rep;

            sTrust11PayableChkNum.Text	= dataLoan.sTrust11PayableChkNum.Value;
            sTrust11ReceivableChkNum.Text = dataLoan.sTrust11ReceivableChkNum.Value;
            sTrust11ReceivableN.Text		= dataLoan.sTrust11ReceivableN;
            //TrustModifiedBy.Text			= dataLoan.sTrust11ModifiedByLoginNm;
            sTrust11Desc.Text				= dataLoan.sTrust11Desc;
            sTrust11D.Text				= dataLoan.sTrust11D_rep;
            sTrust11PayableAmt.Text		= dataLoan.sTrust11PayableAmt_rep;
            sTrust11ReceivableAmt.Text	= dataLoan.sTrust11ReceivableAmt_rep;

            sTrust12PayableChkNum.Text	= dataLoan.sTrust12PayableChkNum.Value;
            sTrust12ReceivableChkNum.Text = dataLoan.sTrust12ReceivableChkNum.Value;
            sTrust12ReceivableN.Text		= dataLoan.sTrust12ReceivableN;
            //TrustModifiedBy.Text			= dataLoan.sTrust12ModifiedByLoginNm;
            sTrust12Desc.Text				= dataLoan.sTrust12Desc;
            sTrust12D.Text				= dataLoan.sTrust12D_rep;
            sTrust12PayableAmt.Text		= dataLoan.sTrust12PayableAmt_rep;
            sTrust12ReceivableAmt.Text	= dataLoan.sTrust12ReceivableAmt_rep;

            sTrust13PayableChkNum.Text	= dataLoan.sTrust13PayableChkNum.Value;
            sTrust13ReceivableChkNum.Text = dataLoan.sTrust13ReceivableChkNum.Value;
            sTrust13ReceivableN.Text		= dataLoan.sTrust13ReceivableN;
            //TrustModifiedBy.Text			= dataLoan.sTrust13ModifiedByLoginNm;
            sTrust13Desc.Text				= dataLoan.sTrust13Desc;
            sTrust13D.Text				= dataLoan.sTrust13D_rep;
            sTrust13PayableAmt.Text		= dataLoan.sTrust13PayableAmt_rep;
            sTrust13ReceivableAmt.Text	= dataLoan.sTrust13ReceivableAmt_rep;

            sTrust14PayableChkNum.Text	= dataLoan.sTrust14PayableChkNum.Value;
            sTrust14ReceivableChkNum.Text = dataLoan.sTrust14ReceivableChkNum.Value;
            sTrust14ReceivableN.Text		= dataLoan.sTrust14ReceivableN;
            //TrustModifiedBy.Text			= dataLoan.sTrust14ModifiedByLoginNm;
            sTrust14Desc.Text				= dataLoan.sTrust14Desc;
            sTrust14D.Text				= dataLoan.sTrust14D_rep;
            sTrust14PayableAmt.Text		= dataLoan.sTrust14PayableAmt_rep;
            sTrust14ReceivableAmt.Text	= dataLoan.sTrust14ReceivableAmt_rep;

            sTrust15PayableChkNum.Text	= dataLoan.sTrust15PayableChkNum.Value;
            sTrust15ReceivableChkNum.Text = dataLoan.sTrust15ReceivableChkNum.Value;
            sTrust15ReceivableN.Text		= dataLoan.sTrust15ReceivableN;
            //TrustModifiedBy.Text			= dataLoan.sTrust15ModifiedByLoginNm;
            sTrust15Desc.Text				= dataLoan.sTrust15Desc;
            sTrust15D.Text				= dataLoan.sTrust15D_rep;
            sTrust15PayableAmt.Text		= dataLoan.sTrust15PayableAmt_rep;
            sTrust15ReceivableAmt.Text	= dataLoan.sTrust15ReceivableAmt_rep;

            sTrust16PayableChkNum.Text	= dataLoan.sTrust16PayableChkNum.Value;
            sTrust16ReceivableChkNum.Text = dataLoan.sTrust16ReceivableChkNum.Value;
            sTrust16ReceivableN.Text		= dataLoan.sTrust16ReceivableN;
            //TrustModifiedBy.Text			= dataLoan.sTrust16ModifiedByLoginNm;
            sTrust16Desc.Text				= dataLoan.sTrust16Desc;
            sTrust16D.Text				= dataLoan.sTrust16D_rep;
            sTrust16PayableAmt.Text		= dataLoan.sTrust16PayableAmt_rep;
            sTrust16ReceivableAmt.Text	= dataLoan.sTrust16ReceivableAmt_rep;

            sTrustPayableTotal.Text		= dataLoan.sTrustPayableTotal_rep;
            sTrustReceivableTotal.Text  = dataLoan.sTrustReceivableTotal_rep;
            sTrustBalance.Text			= dataLoan.sTrustBalance_rep;

            UpdateTextBox("sTrust1Desc", sTrust1Desc, dataLoan);
            UpdateTextBox("sTrust2Desc", sTrust2Desc, dataLoan);
            UpdateTextBox("sTrust3Desc", sTrust3Desc, dataLoan);
            UpdateTextBox("sTrust4Desc", sTrust4Desc, dataLoan);
            UpdateTextBox("sTrust5Desc", sTrust5Desc, dataLoan);
            UpdateTextBox("sTrust6Desc", sTrust6Desc, dataLoan);
            UpdateTextBox("sTrust7Desc", sTrust7Desc, dataLoan);
            UpdateTextBox("sTrust8Desc", sTrust8Desc, dataLoan);
            UpdateTextBox("sTrust9Desc", sTrust9Desc, dataLoan);
            UpdateTextBox("sTrust10Desc", sTrust10Desc, dataLoan);
            UpdateTextBox("sTrust11Desc", sTrust11Desc, dataLoan);
            UpdateTextBox("sTrust12Desc", sTrust12Desc, dataLoan);
            UpdateTextBox("sTrust13Desc", sTrust13Desc, dataLoan);
            UpdateTextBox("sTrust14Desc", sTrust14Desc, dataLoan);
            UpdateTextBox("sTrust15Desc", sTrust15Desc, dataLoan);
            UpdateTextBox("sTrust16Desc", sTrust16Desc, dataLoan);
            		
            dataLoan.DoneLoad();
            
            if (!BrokerUser.IsInRole(ConstApp.ROLE_ACCOUNTANT) && BrokerUser.IsOnlyAccountantCanModifyTrustAccount) 
            {
                // 2/23/2004 dd - Disable ability to edit trust account.

                AddInitScriptFunction("lockDown");
            }

        }
        protected override void SaveData() 
        {

        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "Trust Accounts";
            this.PageID = "StatusTrustAccounts";
        }

        private void UpdateTextBox(string id, TextBox tb, CPageData dataLoan)
        {
            if (dataLoan.IsCustomFieldGloballyDefined(id))
            {
                tb.ReadOnly = true;
                tb.BackColor = Color.Gainsboro;
                tb.ForeColor = Color.Black;
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
            IsAppSpecific = false;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
