<%@ Page language="c#" Codebehind="CustomFields3.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Status.CustomFields3" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head id="Head1" runat="server">
    <title>CustomFields</title>
  </head>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro">
	
    <form id="CustomFields" method="post" runat="server">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
		<td nowrap class="MainRightHeader">Custom Fields</td>
    </tr>
    <tr>
		<td>
		
			<table class="FormTable" cellSpacing="0" cellpadding="2" border="1" rules="all" style="border-collapse:collapse;">
			<tr class="GridHeader">
				<td></td>
				<td style="white-space: nowrap">Item Description</td>
				<td>Date</td>
				<td>Amount</td>
				<td>Percentage</td>
				<td>Checked?</td>
				<td>Notes</td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel"  style="white-space: nowrap">Field Set 41</td>
				<td style="white-space: nowrap"><asp:TextBox id="sCustomField41Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td style="white-space: nowrap"><ml:DateTextBox id="sCustomField41D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td style="white-space: nowrap"><ml:MoneyTextBox id="sCustomField41Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td style="white-space: nowrap"><ml:percenttextbox id="sCustomField41Pc" runat="server" width="70" preset="percent" /></td>
				<td style="white-space: nowrap"><asp:CheckBox id="sCustomField41Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField41Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 42</td>
				<td><asp:TextBox id="sCustomField42Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField42D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField42Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField42Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField42Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField42Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 43</td>
				<td><asp:TextBox id="sCustomField43Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField43D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField43Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField43Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField43Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField43Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 44</td>
				<td><asp:TextBox id="sCustomField44Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField44D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField44Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField44Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField44Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField44Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 45</td>
				<td><asp:TextBox id="sCustomField45Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField45D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField45Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField45Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField45Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField45Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 46</td>
				<td><asp:TextBox id="sCustomField46Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField46D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField46Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField46Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField46Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField46Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 47</td>
				<td><asp:TextBox id="sCustomField47Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField47D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField47Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField47Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField47Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField47Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 48</td>
				<td><asp:TextBox id="sCustomField48Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField48D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField48Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField48Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField48Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField48Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 49</td>
				<td><asp:TextBox id="sCustomField49Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField49D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField49Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField49Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField49Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField49Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel" style="white-space: nowrap">Field Set 50</td>
				<td><asp:TextBox id="sCustomField50Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField50D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField50Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField50Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField50Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField50Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 51</td>
				<td><asp:TextBox id="sCustomField51Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField51D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField51Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField51Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField51Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField51Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 52</td>
				<td><asp:TextBox id="sCustomField52Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField52D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField52Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField52Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField52Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField52Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 53</td>
				<td><asp:TextBox id="sCustomField53Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField53D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField53Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField53Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField53Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField53Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 54</td>
				<td><asp:TextBox id="sCustomField54Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField54D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField54Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField54Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField54Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField54Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 55</td>
				<td><asp:TextBox id="sCustomField55Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField55D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField55Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField55Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField55Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField55Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 56</td>
				<td><asp:TextBox id="sCustomField56Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField56D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField56Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField56Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField56Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField56Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 57</td>
				<td><asp:TextBox id="sCustomField57Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField57D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField57Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField57Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField57Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField57Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 58</td>
				<td><asp:TextBox id="sCustomField58Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField58D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField58Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField58Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField58Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField58Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridItem">
				<td class="FieldLabel">Field Set 59</td>
				<td><asp:TextBox id="sCustomField59Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField59D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField59Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField59Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField59Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField59Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			<tr valign="top" class="GridAlternatingItem">
				<td class="FieldLabel">Field Set 60</td>
				<td><asp:TextBox id="sCustomField60Desc" runat="server" MaxLength="50" Width="150px" /></td>
				<td><ml:DateTextBox id="sCustomField60D" runat="server" CssClass="mask" width="75" preset="date" /></td>
				<td><ml:MoneyTextBox id="sCustomField60Money" runat="server" width="100" preset="money" CssClass="mask" /></td>
				<td><ml:percenttextbox id="sCustomField60Pc" runat="server" width="70" preset="percent" /></td>
				<td><asp:CheckBox id="sCustomField60Bit" runat="server" /></td>
				<td><asp:TextBox id="sCustomField60Notes" runat="server" MaxLength="200" Width="220px" Rows="3" TextMode="MultiLine" style="FONT: 11px arial;" /></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
    
    

     </form>
	
  </body>
</html>
