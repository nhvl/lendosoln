namespace LendersOfficeApp.newlos.Status
{
    using System;
    using System.Collections.Generic;
    using CommonProjectLib.Common.Lib;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.ObjLib.DocMagicLib;
    using LendersOffice.ObjLib.StatusEvents;
    using LendersOffice.Security;

    public class GeneralServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        private int m_InitialClosingCostCount;

        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private E_sStatusT m_currentStatus = E_sStatusT.Loan_Other;
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(GeneralServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            this.m_InitialClosingCostCount = dataLoan.sClosingCostArchive.Count;

            m_currentStatus = dataLoan.sStatusT;

            dataApp.aBusCrDueD_rep = GetString("aBusCrDueD");
            dataApp.aBusCrN = GetString("aBusCrN");
            dataApp.aBusCrOd_rep = GetString("aBusCrOd");
            dataApp.aBusCrRd_rep = GetString("aBusCrRd");
            dataApp.aCrDueD_rep = GetString("aCrDueD");
            dataApp.aCrN = GetString("aCrN");
            dataApp.aCrOd_rep = GetString("aCrOd");
            dataApp.aCrRd_rep = GetString("aCrRd");
            dataApp.aLqiCrOd_rep = GetString("aLqiCrOd");
            dataApp.aLqiCrDueD_rep = GetString("aLqiCrDueD");
            dataApp.aLqiCrRd_rep = GetString("aLqiCrRd");
            dataApp.aLqiCrN = GetString("aLqiCrN");
            dataApp.aU1DocStatDesc = GetString("aU1DocStatDesc");
            dataApp.aU1DocStatDueD_rep = GetString("aU1DocStatDueD");
            dataApp.aU1DocStatN = GetString("aU1DocStatN");
            dataApp.aU1DocStatOd_rep = GetString("aU1DocStatOd");
            dataApp.aU1DocStatRd_rep = GetString("aU1DocStatRd");
            dataApp.aU2DocStatDesc = GetString("aU2DocStatDesc");
            dataApp.aU2DocStatDueD_rep = GetString("aU2DocStatDueD");
            dataApp.aU2DocStatN = GetString("aU2DocStatN");
            dataApp.aU2DocStatOd_rep = GetString("aU2DocStatOd");
            dataApp.aU2DocStatRd_rep = GetString("aU2DocStatRd");
            dataApp.aUDNOrderedD_rep = GetString("aUDNOrderedD");
            dataApp.aUDNDeactivatedD_rep = GetString("aUDNDeactivatedD");
            dataApp.aUdnOrderId = GetString("aUdnOrderId");
            dataLoan.sAgencyCaseNum = GetString("sAgencyCaseNum");
            dataLoan.sApprRprtDueD_rep = GetString("sApprRprtDueD");
            dataLoan.sApprRprtN = GetString("sApprRprtN");
            dataLoan.sApprRprtOd_rep = GetString("sApprRprtOd");
            dataLoan.sApprRprtRd_rep = GetString("sApprRprtRd");
            dataLoan.sDocumentNoteD_rep = GetString("sDocumentNoteD");

            dataLoan.sAppSubmittedD_rep = GetString("sAppSubmittedD");
            dataLoan.sAppSubmittedDLckd = GetBool("sAppSubmittedDLckd");
            dataLoan.sAppReceivedByLenderD_rep = GetString("sAppReceivedByLenderD");
            dataLoan.sAppReceivedByLenderDLckd = GetBool("sAppReceivedByLenderDLckd");
            
            dataLoan.sLenderCaseNum = GetString("sLenderCaseNum");
            dataLoan.sLenderCaseNumLckd = GetBool("sLenderCaseNumLckd");

            dataLoan.sLeadD_rep = GetString("sLeadD");
            dataLoan.sLeadN = GetString("sLeadN");

            dataLoan.sOpenedD_rep = GetString("sOpenedD");
            dataLoan.sOpenedN = GetString("sOpenedN");

            dataLoan.sPreQualD_rep = GetString("sPreQualD");
            dataLoan.sPreQualN = GetString("sPreQualN");

            dataLoan.sSubmitD_rep = GetString("sSubmitD");
            dataLoan.sSubmitN = GetString("sSubmitN");

            dataLoan.sProcessingD_rep = GetString("sProcessingD");
            dataLoan.sProcessingN = GetString("sProcessingN");

            dataLoan.sPreApprovD_rep = GetString("sPreApprovD");
            dataLoan.sPreApprovN = GetString("sPreApprovN");

            dataLoan.sEstCloseD_rep = GetString("sEstCloseD");
            dataLoan.sEstCloseDLckd = GetBool("sEstCloseDLckd");
            dataLoan.sEstCloseN = GetString("sEstCloseN");

            dataLoan.sLoanSubmittedD_rep = GetString("sLoanSubmittedD");
            dataLoan.sLoanSubmittedN = GetString("sLoanSubmittedN");

            dataLoan.sUnderwritingD_rep = GetString("sUnderwritingD");
            dataLoan.sUnderwritingN = GetString("sUnderwritingN");

            dataLoan.sApprovD_rep = GetString("sApprovD");
            dataLoan.sApprovN = GetString("sApprovN");

            dataLoan.sFinalUnderwritingD_rep = GetString("sFinalUnderwritingD");
            dataLoan.sFinalUnderwritingN = GetString("sFinalUnderwritingN");

            dataLoan.sClearToCloseD_rep = GetString("sClearToCloseD");
            dataLoan.sClearToCloseN = GetString("sClearToCloseN");

            dataLoan.sDocsD_rep = GetString("sDocsD");
            dataLoan.sDocsN = GetString("sDocsN");

            dataLoan.sDocsBackD_rep = GetString("sDocsBackD");
            dataLoan.sDocsBackN = GetString("sDocsBackN");

            var sSchedFundDString = GetString("sSchedFundD");
            dataLoan.sSchedFundD_rep = sSchedFundDString;
            dataLoan.sSchedFundN = GetString("sSchedFundN");

            dataLoan.sFundingConditionsD_rep = GetString("sFundingConditionsD");
            dataLoan.sFundingConditionsN = GetString("sFundingConditionsN");

            dataLoan.sFundD_rep = GetString("sFundD");
            dataLoan.sFundN = GetString("sFundN");

            dataLoan.sRecordedD_rep = GetString("sRecordedD");
            dataLoan.sRecordedN = GetString("sRecordedN");

            dataLoan.sFinalDocsD_rep = GetString("sFinalDocsD");
            dataLoan.sFinalDocsN = GetString("sFinalDocsN");

            dataLoan.sClosedD_rep = GetString("sClosedD");
            dataLoan.sClosedN = GetString("sClosedN");

            dataLoan.sOnHoldD_rep = GetString("sOnHoldD");
            dataLoan.sOnHoldN = GetString("sOnHoldN");

            dataLoan.sCanceledD_rep = GetString("sCanceledD");
            dataLoan.sCanceledN = GetString("sCanceledN");

            dataLoan.sRejectD_rep = GetString("sRejectD");
            dataLoan.sRejectN = GetString("sRejectN");

            dataLoan.sSuspendedD_rep = GetString("sSuspendedD");
            dataLoan.sSuspendedN = GetString("sSuspendedN");

            dataLoan.sPreProcessingD_rep = GetString("sPreProcessingD");
            dataLoan.sPreProcessingN = GetString("sPreProcessingN");

            dataLoan.sDocumentCheckD_rep = GetString("sDocumentCheckD");
            dataLoan.sDocumentCheckN = GetString("sDocumentCheckN");

            dataLoan.sDocumentCheckFailedD_rep = GetString("sDocumentCheckFailedD");
            dataLoan.sDocumentCheckFailedN = GetString("sDocumentCheckFailedN");

            dataLoan.sPreUnderwritingD_rep = GetString("sPreUnderwritingD");
            dataLoan.sPreUnderwritingN = GetString("sPreUnderwritingN");

            dataLoan.sConditionReviewD_rep = GetString("sConditionReviewD");
            dataLoan.sConditionReviewN = GetString("sConditionReviewN");

            dataLoan.sPreDocQCD_rep = GetString("sPreDocQCD");
            dataLoan.sPreDocQCN = GetString("sPreDocQCN");

            dataLoan.sDocsOrderedD_rep = GetString("sDocsOrderedD");
            dataLoan.sDocsOrderedN = GetString("sDocsOrderedN");

            dataLoan.sDocsDrawnD_rep = GetString("sDocsDrawnD");
            dataLoan.sDocsDrawnN = GetString("sDocsDrawnN");

            dataLoan.sReadyForSaleD_rep = GetString("sReadyForSaleD");
            dataLoan.sReadyForSaleN = GetString("sReadyForSaleN");

            dataLoan.sSubmittedForPurchaseReviewD_rep = GetString("sSubmittedForPurchaseReviewD");
            dataLoan.sSubmittedForPurchaseReviewN = GetString("sSubmittedForPurchaseReviewN");

            dataLoan.sInPurchaseReviewD_rep = GetString("sInPurchaseReviewD");
            dataLoan.sInPurchaseReviewN = GetString("sInPurchaseReviewN");

            dataLoan.sPrePurchaseConditionsD_rep = GetString("sPrePurchaseConditionsD");
            dataLoan.sPrePurchaseConditionsN = GetString("sPrePurchaseConditionsN");

            dataLoan.sSubmittedForFinalPurchaseD_rep = GetString("sSubmittedForFinalPurchaseD");
            dataLoan.sSubmittedForFinalPurchaseN = GetString("sSubmittedForFinalPurchaseN");

            dataLoan.sInFinalPurchaseReviewD_rep = GetString("sInFinalPurchaseReviewD");
            dataLoan.sInFinalPurchaseReviewN = GetString("sInFinalPurchaseReviewN");

            dataLoan.sClearToPurchaseD_rep = GetString("sClearToPurchaseD");
            dataLoan.sClearToPurchaseN = GetString("sClearToPurchaseN");

            dataLoan.sPurchasedD_rep = GetString("sPurchasedD");
            dataLoan.sPurchasedN = GetString("sPurchasedN");

            dataLoan.sCounterOfferD_rep = GetString("sCounterOfferD");
            dataLoan.sCounterOfferN = GetString("sCounterOfferN");

            dataLoan.sWithdrawnD_rep = GetString("sWithdrawnD");
            dataLoan.sWithdrawnN = GetString("sWithdrawnN");

            dataLoan.sArchivedD_rep = GetString("sArchivedD");
            dataLoan.sArchivedN = GetString("sArchivedN");

            dataLoan.sPrelimRprtDueD_rep = GetString("sPrelimRprtDueD");
            dataLoan.sPrelimRprtN = GetString("sPrelimRprtN");
            dataLoan.sPrelimRprtOd_rep = GetString("sPrelimRprtOd");
            dataLoan.sPrelimRprtRd_rep = GetString("sPrelimRprtRd");

            dataLoan.sClosingServOd_rep = GetString("sClosingServOd");
            dataLoan.sClosingServDueD_rep = GetString("sClosingServDueD");
            dataLoan.sClosingServDocumentD_rep = GetString("sClosingServDocumentD");
            dataLoan.sClosingServRd_rep = GetString("sClosingServRd");
            dataLoan.sClosingServN = GetString("sClosingServN");

            dataLoan.sRLckdD_rep = GetString("sRLckdD");
            dataLoan.Set_sRLckdExpiredD_Manually(GetString("sRLckdExpiredD"));
            dataLoan.sRLckdExpiredN = GetString("sRLckdExpiredN");
            dataLoan.sRLckdN = GetString("sRLckdN");
            
            dataLoan.sStatusLckd = GetBool("sStatusLckd");
            dataLoan.sStatusT = (E_sStatusT)GetInt("sStatusT");
            
            dataLoan.sTilGfeDueD_rep = GetString("sTilGfeDueD");
            dataLoan.sTilGfeN = GetString("sTilGfeN");
            dataLoan.sTilGfeOd_rep = GetString("sTilGfeOd");
            dataLoan.sTilGfeRd_rep = GetString("sTilGfeRd");

            dataLoan.sFloodCertOd_rep = GetString("sFloodCertOd");
            dataLoan.sFloodCertDueD_rep = GetString("sFloodCertDueD");
            dataLoan.sFloodCertRd_rep = GetString("sFloodCertRd");
            dataLoan.sFloodCertN = GetString("sFloodCertN");

            dataLoan.sUSPSCheckOd_rep = GetString("sUSPSCheckOd");
            dataLoan.sUSPSCheckDueD_rep = GetString("sUSPSCheckDueD");
            dataLoan.sUSPSCheckDocumentD_rep = GetString("sUSPSCheckDocumentD");
            dataLoan.sUSPSCheckRd_rep = GetString("sUSPSCheckRd");
            dataLoan.sUSPSCheckN = GetString("sUSPSCheckN");

            dataLoan.sPayDemStmntOd_rep = GetString("sPayDemStmntOd");
            dataLoan.sPayDemStmntDueD_rep = GetString("sPayDemStmntDueD");
            dataLoan.sPayDemStmntDocumentD_rep = GetString("sPayDemStmntDocumentD");
            dataLoan.sPayDemStmntRd_rep = GetString("sPayDemStmntRd");
            dataLoan.sPayDemStmntN = GetString("sPayDemStmntN");

            dataLoan.sCAIVRSOd_rep = GetString("sCAIVRSOd");
            dataLoan.sCAIVRSDueD_rep = GetString("sCAIVRSDueD");
            dataLoan.sCAIVRSDocumentD_rep = GetString("sCAIVRSDocumentD");
            dataLoan.sCAIVRSRd_rep = GetString("sCAIVRSRd");
            dataLoan.sCAIVRSN = GetString("sCAIVRSN");

            dataLoan.sFraudServOd_rep = GetString("sFraudServOd");
            dataLoan.sFraudServDueD_rep = GetString("sFraudServDueD");
            dataLoan.sFraudServDocumentD_rep = GetString("sFraudServDocumentD");
            dataLoan.sFraudServRd_rep = GetString("sFraudServRd");
            dataLoan.sFraudServN = GetString("sFraudServN");

            dataLoan.sAVMOd_rep = GetString("sAVMOd");
            dataLoan.sAVMDueD_rep = GetString("sAVMDueD");
            dataLoan.sAVMDocumentD_rep = GetString("sAVMDocumentD");
            dataLoan.sAVMRd_rep = GetString("sAVMRd");
            dataLoan.sAVMN = GetString("sAVMN");

            dataLoan.sHOACertOd_rep = GetString("sHOACertOd");
            dataLoan.sHOACertDueD_rep = GetString("sHOACertDueD");
            dataLoan.sHOACertDocumentD_rep = GetString("sHOACertDocumentD");
            dataLoan.sHOACertRd_rep = GetString("sHOACertRd");
            dataLoan.sHOACertN = GetString("sHOACertN");

            dataLoan.sEstHUDOd_rep = GetString("sEstHUDOd");
            dataLoan.sEstHUDDueD_rep = GetString("sEstHUDDueD");
            dataLoan.sEstHUDDocumentD_rep = GetString("sEstHUDDocumentD");
            dataLoan.sEstHUDRd_rep = GetString("sEstHUDRd");
            dataLoan.sEstHUDN = GetString("sEstHUDN");

            dataLoan.sCPLAndICLOd_rep = GetString("sCPLAndICLOd");
            dataLoan.sCPLAndICLDueD_rep = GetString("sCPLAndICLDueD");
            dataLoan.sCPLAndICLDocumentD_rep = GetString("sCPLAndICLDocumentD");
            dataLoan.sCPLAndICLRd_rep = GetString("sCPLAndICLRd");
            dataLoan.sCPLAndICLN = GetString("sCPLAndICLN");

            dataLoan.sWireInstructOd_rep = GetString("sWireInstructOd");
            dataLoan.sWireInstructDueD_rep = GetString("sWireInstructDueD");
            dataLoan.sWireInstructDocumentD_rep = GetString("sWireInstructDocumentD");
            dataLoan.sWireInstructRd_rep = GetString("sWireInstructRd");
            dataLoan.sWireInstructN = GetString("sWireInstructN");

            dataLoan.sInsurancesOd_rep = GetString("sInsurancesOd");
            dataLoan.sInsurancesDueD_rep = GetString("sInsurancesDueD");
            dataLoan.sInsurancesDocumentD_rep = GetString("sInsurancesDocumentD");
            dataLoan.sInsurancesRd_rep = GetString("sInsurancesRd");
            dataLoan.sInsurancesN = GetString("sInsurancesN");


            dataLoan.sTrNotes = GetString("sTrNotes");
            dataLoan.sU1DocStatDesc = GetString("sU1DocStatDesc");
            dataLoan.sU1DocStatDueD_rep = GetString("sU1DocStatDueD");
            dataLoan.sU1DocStatN = GetString("sU1DocStatN");
            dataLoan.sU1DocStatOd_rep = GetString("sU1DocStatOd");
            dataLoan.sU1DocStatRd_rep = GetString("sU1DocStatRd");
            dataLoan.sU1LStatD_rep = GetString("sU1LStatD");
            dataLoan.sU1LStatDesc = GetString("sU1LStatDesc");
            dataLoan.sU1LStatN = GetString("sU1LStatN");
            dataLoan.sU2DocStatDesc = GetString("sU2DocStatDesc");
            dataLoan.sU2DocStatDueD_rep = GetString("sU2DocStatDueD");
            dataLoan.sU2DocStatN = GetString("sU2DocStatN");
            dataLoan.sU2DocStatOd_rep = GetString("sU2DocStatOd");
            dataLoan.sU2DocStatRd_rep = GetString("sU2DocStatRd");
            dataLoan.sU2LStatD_rep = GetString("sU2LStatD");
            dataLoan.sU2LStatDesc = GetString("sU2LStatDesc");
            dataLoan.sU2LStatN = GetString("sU2LStatN");
            dataLoan.sU3DocStatDesc = GetString("sU3DocStatDesc");
            dataLoan.sU3DocStatDueD_rep = GetString("sU3DocStatDueD");
            dataLoan.sU3DocStatN = GetString("sU3DocStatN");
            dataLoan.sU3DocStatOd_rep = GetString("sU3DocStatOd");
            dataLoan.sU3DocStatRd_rep = GetString("sU3DocStatRd");
            dataLoan.sU3LStatD_rep = GetString("sU3LStatD");
            dataLoan.sU3LStatDesc = GetString("sU3LStatDesc");
            dataLoan.sU3LStatN = GetString("sU3LStatN");
            dataLoan.sU4DocStatDesc = GetString("sU4DocStatDesc");
            dataLoan.sU4DocStatDueD_rep = GetString("sU4DocStatDueD");
            dataLoan.sU4DocStatN = GetString("sU4DocStatN");
            dataLoan.sU4DocStatOd_rep = GetString("sU4DocStatOd");
            dataLoan.sU4DocStatRd_rep = GetString("sU4DocStatRd");
            dataLoan.sU4LStatD_rep = GetString("sU4LStatD");
            dataLoan.sU4LStatDesc = GetString("sU4LStatDesc");
            dataLoan.sU4LStatN = GetString("sU4LStatN");
            dataLoan.sU5DocStatDesc = GetString("sU5DocStatDesc");
            dataLoan.sU5DocStatDueD_rep = GetString("sU5DocStatDueD");
            dataLoan.sU5DocStatN = GetString("sU5DocStatN");
            dataLoan.sU5DocStatOd_rep = GetString("sU5DocStatOd");
            dataLoan.sU5DocStatRd_rep = GetString("sU5DocStatRd");
            dataLoan.sShippedToInvestorD_rep = GetString("sShippedToInvestorD");
            dataLoan.sShippedToInvestorN = GetString("sShippedToInvestorN");
            dataLoan.sSuspendedByInvestorD_rep = GetString("sSuspendedByInvestorD");
            dataLoan.sSuspendedByInvestorN = GetString("sSuspendedByInvestorN");
            dataLoan.sCondSentToInvestorD_rep = GetString("sCondSentToInvestorD");
            dataLoan.sCondSentToInvestorN = GetString("sCondSentToInvestorN");
            dataLoan.sAdditionalCondSentD_rep = GetString("sAdditionalCondSentD");
            dataLoan.sLPurchaseD_rep = GetString("sLPurchaseD");
            dataLoan.sGoodByLetterD_rep = GetString("sGoodByLetterD");
            dataLoan.sQCCompDate_rep = GetString("sQCCompDate");
            dataLoan.sCreditDecisionDLckd = GetBool("sCreditDecisionDLckd");
            dataLoan.sCreditDecisionD_rep= GetString("sCreditDecisionD");

            dataLoan.sLeadSrcDesc = GetString("sLeadSrcDesc");
            
            dataLoan.sServicingStartD_rep = GetString("sServicingStartD");

            dataLoan.sCaseAssignmentD_rep = GetString("sCaseAssignmentD");
            
            dataLoan.sLPurchasedN = GetString("sLPurchasedN");
            dataLoan.sIsBranchActAsLenderForFileTri = (E_TriState)GetInt("sIsBranchActAsLenderForFileTri");
            dataLoan.sIsBranchActAsOriginatorForFileTri = (E_TriState)GetInt("sIsBranchActAsOriginatorForFileTri");

            dataLoan.sDocMagicApplicationD_rep = GetString("sDocMagicApplicationD");
            dataLoan.sDocMagicApplicationDLckd = GetBool("sDocMagicApplicationDLckd");
            dataLoan.sDocMagicGFED_rep = GetString("sDocMagicGFED");
            dataLoan.sDocMagicGFEDLckd = GetBool("sDocMagicGFEDLckd");
            dataLoan.sDocMagicEstAvailableThroughD_rep = GetString("sDocMagicEstAvailableThroughD");
            dataLoan.sDocMagicEstAvailableThroughDLckd = GetBool("sDocMagicEstAvailableThroughDLckd");
            dataLoan.sDocMagicDocumentD_rep = GetString("sDocMagicDocumentD");
            dataLoan.sDocMagicDocumentDLckd = GetBool("sDocMagicDocumentDLckd");
            dataLoan.sDocMagicClosingD_rep = GetString("sDocMagicClosingD");
            dataLoan.sDocMagicClosingDLckd = GetBool("sDocMagicClosingDLckd");
            dataLoan.sDocMagicSigningD_rep = GetString("sDocMagicSigningD");
            dataLoan.sDocMagicSigningDLckd = GetBool("sDocMagicSigningDLckd");
            dataLoan.sDocMagicCancelD_rep = GetString("sDocMagicCancelD");
            dataLoan.sDocMagicCancelDLckd = GetBool("sDocMagicCancelDLckd");
            dataLoan.sDocMagicDisbursementD_rep = GetString("sDocMagicDisbursementD");
            dataLoan.sDocMagicDisbursementDLckd = GetBool("sDocMagicDisbursementDLckd");
            dataLoan.sDocExpirationD_rep = GetString("sDocExpirationD");
            dataLoan.sDocExpirationDLckd = GetBool("sDocExpirationDLckd");
            dataLoan.sDocMagicRateLockD_rep = GetString("sDocMagicRateLockD");
            dataLoan.sDocMagicRateLockDLckd = GetBool("sDocMagicRateLockDLckd");
            dataLoan.sDocMagicPreZSentD_rep = GetString("sDocMagicPreZSentD");
            dataLoan.sDocMagicPreZSentDLckd = GetBool("sDocMagicPreZSentDLckd");
            dataLoan.sDocMagicReDiscSendD_rep = GetString("sDocMagicReDiscSendD");
            dataLoan.sDocMagicReDiscSendDLckd = GetBool("sDocMagicReDiscSendDLckd");
            dataLoan.sDocMagicRedisclosureMethodT = (E_RedisclosureMethodT)GetInt("sDocMagicRedisclosureMethodT");
            dataLoan.sHasESignedDocumentsT = (E_sHasESignedDocumentsT)GetInt("sHasESignedDocumentsT");
            dataLoan.sDocMagicRedisclosureMethodTLckd = GetBool("sDocMagicRedisclosureMethodTLckd");
            dataLoan.sDocMagicReDiscReceivedD_rep = GetString("sDocMagicReDiscReceivedD");
            dataLoan.sDocMagicReDiscReceivedDLckd = GetBool("sDocMagicReDiscReceivedDLckd");
            dataLoan.sDocMagicAlternateLenderId = GetGuid("sAlternateLender");
            dataLoan.sHomeownerCounselingOrganizationDisclosureD_rep = GetString("sHomeownerCounselingOrganizationDisclosureD");
            dataLoan.sDisclosuresMailedD_rep = GetString("sDisclosuresMailedD");

            // OPM 175355, 12/24/2015, ML
            dataLoan.sPurchaseContractDate_rep = GetString("sPurchaseContractDate");
            dataLoan.sFinancingContingencyExpD_rep = GetString("sFinancingContingencyExpD");
            dataLoan.sFinancingContingencyExtensionExpD_rep = GetString("sFinancingContingencyExtensionExpD");

            // OPM 80416
            dataLoan.sPrelimRprtDocumentD_rep = GetString("sPrelimRprtDocumentD");
            dataLoan.sSpValuationEffectiveD_rep = GetString("sSpValuationEffectiveD");
            //dataLoan.sAppSubmittedN = GetString("sAppSubmittedN");
            dataLoan.sU1DocDocumentD_rep = GetString("sU1DocDocumentD");
            dataLoan.sU2DocDocumentD_rep = GetString("sU2DocDocumentD");
            dataLoan.sU3DocDocumentD_rep = GetString("sU3DocDocumentD");
            dataLoan.sU4DocDocumentD_rep = GetString("sU4DocDocumentD");
            dataLoan.sU5DocDocumentD_rep = GetString("sU5DocDocumentD");

            dataLoan.sLoanPackageOrderedD_rep = GetString("sLoanPackageOrderedD");
            dataLoan.sLoanPackageDocumentD_rep = GetString("sLoanPackageDocumentD");
            dataLoan.sLoanPackageReceivedD_rep = GetString("sLoanPackageReceivedD");
            dataLoan.sLoanPackageN = GetString("sLoanPackageN");

            dataLoan.sMortgageDOTOrderedD_rep = GetString("sMortgageDOTOrderedD");
            dataLoan.sMortgageDOTDocumentD_rep = GetString("sMortgageDOTDocumentD");
            dataLoan.sMortgageDOTReceivedD_rep = GetString("sMortgageDOTReceivedD");
            dataLoan.sMortgageDOTN = GetString("sMortgageDOTN");

            dataLoan.sNoteOrderedD_rep = GetString("sNoteOrderedD");
            dataLoan.sNoteReceivedD_rep = GetString("sNoteReceivedD");
            dataLoan.sNoteN = GetString("sNoteN");

            dataLoan.sFinalHUD1SttlmtStmtOrderedD_rep = GetString("sFinalHUD1SttlmtStmtOrderedD");
            dataLoan.sFinalHUD1SttlmtStmtDocumentD_rep = GetString("sFinalHUD1SttlmtStmtDocumentD");
            dataLoan.sFinalHUD1SttlmtStmtReceivedD_rep = GetString("sFinalHUD1SttlmtStmtReceivedD");
            dataLoan.sFinalHUD1SttlmtStmtN = GetString("sFinalHUD1SttlmtStmtN");

            dataLoan.sTitleInsPolicyOrderedD_rep = GetString("sTitleInsPolicyOrderedD");
            dataLoan.sTitleInsPolicyDocumentD_rep = GetString("sTitleInsPolicyDocumentD");
            dataLoan.sTitleInsPolicyReceivedD_rep = GetString("sTitleInsPolicyReceivedD");
            dataLoan.sTitleInsPolicyN = GetString("sTitleInsPolicyN");

            dataLoan.sMiCertOrderedD_rep = GetString("sMiCertOrderedD");
            dataLoan.sMiCertIssuedD_rep = GetString("sMiCertIssuedD");
            dataLoan.sMiCertReceivedD_rep = GetString("sMiCertReceivedD");
            dataLoan.sMiCertN = GetString("sMiCertN");

            dataLoan.sFinalHazInsPolicyOrderedD_rep = GetString("sFinalHazInsPolicyOrderedD");
            dataLoan.sFinalHazInsPolicyDocumentD_rep = GetString("sFinalHazInsPolicyDocumentD");
            dataLoan.sFinalHazInsPolicyReceivedD_rep = GetString("sFinalHazInsPolicyReceivedD");
            dataLoan.sFinalHazInsPolicyN = GetString("sFinalHazInsPolicyN");

            dataLoan.sFinalFloodInsPolicyOrderedD_rep = GetString("sFinalFloodInsPolicyOrderedD");
            dataLoan.sFinalFloodInsPolicyDocumentD_rep = GetString("sFinalFloodInsPolicyDocumentD");
            dataLoan.sFinalFloodInsPolicyReceivedD_rep = GetString("sFinalFloodInsPolicyReceivedD");
            dataLoan.sFinalFloodInsPolicyN = GetString("sFinalFloodInsPolicyN");

            dataLoan.sCorrespondentProcessT = (E_sCorrespondentProcessT)GetInt("sCorrespondentProcessT");


            dataApp.aBPreFundVoeTypeT = (E_aPreFundVoeTypeT)GetInt("aBPreFundVoeTypeT");
            dataApp.aBPreFundVoeOd_rep = GetString("aBPreFundVoeOd");
            dataApp.aBPreFundVoeDueD_rep = GetString("aBPreFundVoeDueD");
            dataApp.aBPreFundVoeRd_rep = GetString("aBPreFundVoeRd");
            dataApp.aBPreFundVoeN = GetString("aBPreFundVoeN");

            dataApp.aCPreFundVoeTypeT = (E_aPreFundVoeTypeT)GetInt("aCPreFundVoeTypeT");
            dataApp.aCPreFundVoeOd_rep = GetString("aCPreFundVoeOd");
            dataApp.aCPreFundVoeDueD_rep = GetString("aCPreFundVoeDueD");
            dataApp.aCPreFundVoeRd_rep = GetString("aCPreFundVoeRd");
            dataApp.aCPreFundVoeN = GetString("aCPreFundVoeN");

            if (dataLoan.sLT == E_sLT.UsdaRural)
            {
                dataLoan.sAppSubmitedToUsdaD_rep = GetString("sAppSubmitedToUsdaD"); 
            }

            dataLoan.sIntentToProceedD_rep = GetString("sIntentToProceedD");
            dataLoan.sIntentToProceedN = GetString("sIntentToProceedN");

            if (DocumentVendorBrokerSettings.GetDocMagicSettings(dataLoan.BrokerDB.BrokerID).BrokerId != Guid.Empty)
            {
                dataLoan.sDocMagicTransferToInvestorCode = GetString("TransferToInvestorCode");
                dataLoan.sDocMagicTransferToInvestorNm = GetString("TransferToInvestorName");
            }

            General.BindDataFromControls(dataLoan, dataApp, this);
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            // Compute days since opened.
            DateTime dt = DateTime.Now;
            DateTime dt1 = DateTime.Now;
            // Set dt to closing date if possible.
            if (dataLoan.sClosedD_rep.TrimWhitespaceAndBOM() != "")
            {
                //dt = DateTime.Parse(dataLoan.sClosedD_rep);
                if (DateTime.TryParse(dataLoan.sClosedD_rep, out dt) == false)
                {
                    dt = DateTime.Now;
                }
            }

            if (DateTime.TryParse(dataLoan.sOpenedD_rep, out dt1) == false)
            {
                dt1 = DateTime.Now;
            }
            TimeSpan ts = dt.Subtract(dt1);

            SetResult("daySinceOpened", ts.Days.ToString());
            SetResult("sBranchChannelT", Tools.GetDescription(dataLoan.sBranchChannelT));
            SetResult("sCorrespondentProcessT", dataLoan.sCorrespondentProcessT);
            SetResult("sStatusT", dataLoan.sStatusT);
            SetResult("sStatus", dataLoan.sStatusT_rep);
            SetResult("sStatusD", dataLoan.sStatusD_rep);
            SetResult("sLenderCaseNum", dataLoan.sLenderCaseNum);
            SetResult("sDocMagicApplicationD", dataLoan.sDocMagicApplicationD_rep);
            SetResult("sDocMagicApplicationDLckd", dataLoan.sDocMagicApplicationDLckd);
            SetResult("sDocMagicGFED", dataLoan.sDocMagicGFED_rep);
            SetResult("sDocMagicGFEDLckd", dataLoan.sDocMagicGFEDLckd);
            SetResult("sDocMagicEstAvailableThroughD", dataLoan.sDocMagicEstAvailableThroughD_rep);
            SetResult("sDocMagicEstAvailableThroughDLckd", dataLoan.sDocMagicEstAvailableThroughDLckd);
            SetResult("sDocMagicDocumentD", dataLoan.sDocMagicDocumentD_rep);
            SetResult("sDocMagicDocumentDLckd", dataLoan.sDocMagicDocumentDLckd);
            SetResult("sDocMagicClosingD", dataLoan.sDocMagicClosingD_rep);
            SetResult("sDocMagicClosingDLckd", dataLoan.sDocMagicClosingDLckd);
            SetResult("sDocMagicSigningD", dataLoan.sDocMagicSigningD_rep);
            SetResult("sDocMagicSigningDLckd", dataLoan.sDocMagicSigningDLckd);
            SetResult("sDocMagicCancelD", dataLoan.sDocMagicCancelD_rep);
            SetResult("sDocMagicCancelDLckd", dataLoan.sDocMagicCancelDLckd);
            SetResult("sDocMagicDisbursementD", dataLoan.sDocMagicDisbursementD_rep);
            SetResult("sDocMagicDisbursementDLckd", dataLoan.sDocMagicDisbursementDLckd);
            SetResult("sDocExpirationD", dataLoan.sDocExpirationD_rep);
            SetResult("sDocExpirationDLckd", dataLoan.sDocExpirationDLckd);
            SetResult("sDocMagicRateLockD", dataLoan.sDocMagicRateLockD_rep);
            SetResult("sDocMagicRateLockDLckd", dataLoan.sDocMagicRateLockDLckd);
            SetResult("sDocMagicPreZSentD", dataLoan.sDocMagicPreZSentD_rep);
            SetResult("sDocMagicPreZSentDLckd", dataLoan.sDocMagicPreZSentDLckd);
            SetResult("sDocMagicReDiscSendD", dataLoan.sDocMagicReDiscSendD_rep);
            SetResult("sDocMagicReDiscSendDLckd", dataLoan.sDocMagicReDiscSendDLckd);
            SetResult("sDocMagicRedisclosureMethodT", dataLoan.sDocMagicRedisclosureMethodT);
            SetResult("sDocMagicRedisclosureMethodTLckd", dataLoan.sDocMagicRedisclosureMethodTLckd);
            SetResult("sHasESignedDocumentsT", dataLoan.sHasESignedDocumentsT);
            SetResult("sDocMagicReDiscReceivedD", dataLoan.sDocMagicReDiscReceivedD_rep);
            SetResult("sDocMagicReDiscReceivedDLckd", dataLoan.sDocMagicReDiscReceivedDLckd);
            SetResult("sHomeownerCounselingOrganizationDisclosureD", dataLoan.sHomeownerCounselingOrganizationDisclosureD_rep);
            SetResult("sDisclosuresMailedD", dataLoan.sDisclosuresMailedD_rep);

            // OPM 175355, 12/24/2015, ML
            SetResult("sPurchaseContractDate", dataLoan.sPurchaseContractDate_rep);
            SetResult("sFinancingContingencyExpD", dataLoan.sFinancingContingencyExpD_rep);
            SetResult("sFinancingContingencyExtensionExpD", dataLoan.sFinancingContingencyExtensionExpD_rep);

            SetResult("sLeadDTime", dataLoan.sLeadDTime_rep);
            SetResult("sOpenedDTime", dataLoan.sOpenedDTime_rep);
            SetResult("sPreQualDTime", dataLoan.sPreQualDTime_rep);
            SetResult("sSubmitDTime", dataLoan.sSubmitDTime_rep);
            SetResult("sProcessingDTime", dataLoan.sProcessingDTime_rep);
            SetResult("sPreApprovDTime", dataLoan.sPreApprovDTime_rep);
            SetResult("sLoanSubmittedDTime", dataLoan.sLoanSubmittedDTime_rep);
            SetResult("sUnderwritingDTime", dataLoan.sUnderwritingDTime_rep);
            SetResult("sApprovDTime", dataLoan.sApprovDTime_rep);
            SetResult("sFinalUnderwritingDTime", dataLoan.sFinalUnderwritingDTime_rep);
            SetResult("sClearToCloseDTime", dataLoan.sClearToCloseDTime_rep);
            SetResult("sDocsDTime", dataLoan.sDocsDTime_rep);
            SetResult("sDocsBackDTime", dataLoan.sDocsBackDTime_rep);
            SetResult("sSchedFundDTime", dataLoan.sSchedFundDTime_rep);
            SetResult("sFundingConditionsDTime", dataLoan.sFundingConditionsDTime_rep);
            SetResult("sFundDTime", dataLoan.sFundDTime_rep);
            SetResult("sRecordedDTime", dataLoan.sRecordedDTime_rep);
            SetResult("sFinalDocsDTime", dataLoan.sFinalDocsDTime_rep);
            SetResult("sClosedDTime", dataLoan.sClosedDTime_rep);
            SetResult("sOnHoldDTime", dataLoan.sOnHoldDTime_rep);
            SetResult("sCanceledDTime", dataLoan.sCanceledDTime_rep);
            SetResult("sRejectDTime", dataLoan.sRejectDTime_rep);
            SetResult("sSuspendedDTime", dataLoan.sSuspendedDTime_rep);

            SetResult("sPreProcessingDTime", dataLoan.sPreProcessingDTime_rep);
            SetResult("sDocumentCheckDTime", dataLoan.sDocumentCheckDTime_rep);
            SetResult("sDocumentCheckFailedDTime", dataLoan.sDocumentCheckFailedDTime_rep);
            SetResult("sPreUnderwritingDTime", dataLoan.sPreUnderwritingDTime_rep);
            SetResult("sConditionReviewDTime", dataLoan.sConditionReviewDTime_rep);
            SetResult("sPreDocQCDTime", dataLoan.sPreDocQCDTime_rep);
            SetResult("sDocsOrderedDTime", dataLoan.sDocsOrderedDTime_rep);
            SetResult("sDocsDrawnDTime", dataLoan.sDocsDrawnDTime_rep);
            SetResult("sReadyForSaleDTime", dataLoan.sReadyForSaleDTime_rep);
            SetResult("sSubmittedForPurchaseReviewDTime", dataLoan.sSubmittedForPurchaseReviewDTime_rep);
            SetResult("sInPurchaseReviewDTime", dataLoan.sInPurchaseReviewDTime_rep);
            SetResult("sPrePurchaseConditionsDTime", dataLoan.sPrePurchaseConditionsDTime_rep);
            SetResult("sSubmittedForFinalPurchaseDTime", dataLoan.sSubmittedForFinalPurchaseDTime_rep);
            SetResult("sInFinalPurchaseReviewDTime", dataLoan.sInFinalPurchaseReviewDTime_rep);
            SetResult("sClearToPurchaseDTime", dataLoan.sClearToPurchaseDTime_rep);
            SetResult("sPurchasedDTime", dataLoan.sPurchasedDTime_rep);
            SetResult("sCounterOfferDTime", dataLoan.sCounterOfferDTime_rep);
            SetResult("sWithdrawnDTime", dataLoan.sWithdrawnDTime_rep);
            SetResult("sArchivedDTime", dataLoan.sArchivedDTime_rep);

            SetResult("sShippedToInvestorDTime", dataLoan.sShippedToInvestorDTime_rep);
            SetResult("sSuspendedByInvestorDTime", dataLoan.sSuspendedByInvestorDTime_rep);
            SetResult("sCondSentToInvestorDTime", dataLoan.sCondSentToInvestorDTime_rep);
            SetResult("sLPurchaseDTime", dataLoan.sLPurchaseDTime_rep);

            SetResult("sAppSubmittedD", dataLoan.sAppSubmittedD_rep);
            SetResult("sAppSubmittedDLckd", dataLoan.sAppSubmittedDLckd);
            SetResult("sAppReceivedByLenderD", dataLoan.sAppReceivedByLenderD_rep);
            SetResult("sAppReceivedByLenderDLckd", dataLoan.sAppReceivedByLenderDLckd);
            SetResult("sIsBranchActAsOriginatorForFileTri", dataLoan.sIsBranchActAsOriginatorForFileTri.ToString("D"));
            SetResult("sIsBranchActAsLenderForFileTri", dataLoan.sIsBranchActAsLenderForFileTri.ToString("D"));
            SetResult("sAppSubmitedToUsdaD", dataLoan.sAppSubmitedToUsdaD_rep);

            SetResult("sIntentToProceedD", dataLoan.sIntentToProceedD_rep);
            SetResult("sIntentToProceedN", dataLoan.sIntentToProceedN);

            SetResult("sEstCloseD", dataLoan.sEstCloseD_rep);
            SetResult("sEstCloseDLckd", dataLoan.sEstCloseDLckd);

            SetResult("sQCCompDate", dataLoan.sQCCompDate_rep);
            SetResult("sQCCompDateTime", dataLoan.sQCCompDateTime_rep);

            SetResult("sCreditDecisionDLckd", dataLoan.sCreditDecisionDLckd);
            SetResult("sCreditDecisionD", dataLoan.sCreditDecisionD_rep);
            SetResult("sCreditDecisionDTime", dataLoan.sCreditDecisionDTime_rep);

            if (DocumentVendorBrokerSettings.GetDocMagicSettings(dataLoan.BrokerDB.BrokerID).BrokerId != Guid.Empty)
            {
                SetResult("TransferToInvestorName", dataLoan.sDocMagicTransferToInvestorNm);
                SetResult("TransferToInvestorCode", dataLoan.sDocMagicTransferToInvestorCode);
            }

            General.LoadDataForControls(dataLoan, dataApp, this);

            // A save occured and we're tracking touch events.
            // Update the number of touches field for the corresponding loan status.
            if (dataLoan.sStatusEventMigrationVersion != StatusEventMigrationVersion.NotMigrated && dataLoan.sFileVersion != sFileVersion)
            {
                var loanStatus = dataLoan.sStatusT;
                string fieldId = null;
                if (statusToTouchFieldMap.TryGetValue(loanStatus, out fieldId))
                {
                    var statusEvents = StatusEvent.RetrieveAllLoanEvents(dataLoan.sLId, dataLoan.sBrokerId);
                    SetResult(fieldId, StatusEvent.RetrieveNumTouches(statusEvents, dataLoan.sStatusT));
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Missing status - touch field mapping for loan status " + loanStatus.ToString("G"));
                }
            }
        }

        private static Dictionary<E_sStatusT, string> statusToTouchFieldMap = new Dictionary<E_sStatusT, string>() {
                {E_sStatusT.Lead_New, "sNumTouchesLeadNew"  },
                {E_sStatusT.Loan_Open, "sNumTouchesLoanOpen"},
                {E_sStatusT.Loan_Prequal, "sNumTouchesPrequal"},
                {E_sStatusT.Loan_Registered, "sNumTouchesRegistered"},
                {E_sStatusT.Loan_PreProcessing, "sNumTouchesPreProcessing"},
                {E_sStatusT.Loan_Processing, "sNumTouchesProcessing"},
                {E_sStatusT.Loan_DocumentCheck, "sNumTouchesDocumentCheck"},
                {E_sStatusT.Loan_DocumentCheckFailed, "sNumTouchesDocumentCheckFailed"},
                {E_sStatusT.Loan_LoanSubmitted, "sNumTouchesSubmitted"},
                {E_sStatusT.Loan_PreUnderwriting, "sNumTouchesPreUnderwriting"},
                {E_sStatusT.Loan_Underwriting, "sNumTouchesUnderwriting"},
                {E_sStatusT.Loan_Preapproval, "sNumTouchesPreapproval"},
                {E_sStatusT.Loan_Approved, "sNumTouchesApproved"},
                {E_sStatusT.Loan_ConditionReview, "sNumTouchesConditionReview"},
                {E_sStatusT.Loan_FinalUnderwriting, "sNumTouchesFinalUnderwriting"},
                {E_sStatusT.Loan_PreDocQC, "sNumTouchesPreDocQC"},
                {E_sStatusT.Loan_ClearToClose, "sNumTouchesClearToClose"},
                {E_sStatusT.Loan_DocsOrdered, "sNumTouchesDocsOrdered"},
                {E_sStatusT.Loan_DocsDrawn, "sNumTouchesDocsDrawn"},
                {E_sStatusT.Loan_Docs, "sNumTouchesLoanDocs"},
                {E_sStatusT.Loan_DocsBack, "sNumTouchesDocsBack"},
                {E_sStatusT.Loan_FundingConditions, "sNumTouchesFundingConditions"},
                {E_sStatusT.Loan_Funded, "sNumTouchesFunded"},
                {E_sStatusT.Loan_Recorded, "sNumTouchesRecorded"},
                {E_sStatusT.Loan_FinalDocs, "sNumTouchesFinalDocs"},
                {E_sStatusT.Loan_Closed, "sNumTouchesClosed"},
                {E_sStatusT.Loan_SubmittedForPurchaseReview, "sNumTouchesSubmittedForPurchaseReview"},
                {E_sStatusT.Loan_InPurchaseReview, "sNumTouchesInPurchaseReview"},
                {E_sStatusT.Loan_PrePurchaseConditions, "sNumTouchesPrePurchaseConditions"},
                {E_sStatusT.Loan_SubmittedForFinalPurchaseReview, "sNumTouchesSubmittedForFinalPurchaseReview"},
                {E_sStatusT.Loan_InFinalPurchaseReview, "sNumTouchesInFinalPurchaseReview"},
                {E_sStatusT.Loan_ClearToPurchase, "sNumTouchesClearToPurchase"},
                {E_sStatusT.Loan_Purchased, "sNumTouchesPurchased"},
                {E_sStatusT.Loan_ReadyForSale, "sNumTouchesReadyForSale"},
                {E_sStatusT.Loan_Shipped, "sNumTouchesShipped"},
                {E_sStatusT.Loan_LoanPurchased, "sNumTouchesLoanPurchased"},
                {E_sStatusT.Loan_CounterOffer, "sNumTouchesCounterOffer"},
                {E_sStatusT.Loan_OnHold, "sNumTouchesOnHold"},
                {E_sStatusT.Lead_Canceled, "sNumTouchesCanceled"},
                {E_sStatusT.Loan_Suspended, "sNumTouchesSuspended"},
                {E_sStatusT.Loan_Rejected, "sNumTouchesRejected"},
                {E_sStatusT.Loan_Withdrawn, "sNumTouchesWithdrawn"},
                {E_sStatusT.Loan_Archived, "sNumTouchesArchived"},
                {E_sStatusT.Loan_InvestorConditions, "sNumTouchesInvestorConditions"},
                {E_sStatusT.Loan_InvestorConditionsSent, "sNumTouchesInvestorConditionsSent"},
                {E_sStatusT.Loan_Canceled, "sNumTouchesCanceled" },
            };

        protected override void AfterSaveAndLoadPageDataCallback(CPageData dataLoan)
        {
            if (this.m_InitialClosingCostCount != dataLoan.sClosingCostArchive.Count)
            {
                SetResult("RefreshPage", true);
            }
        }

        protected override void Process(string method)
        {
            switch (method)
            {
                case "GetStatusOfFurthestDownNewStatus":
                    GetStatusOfFurthestDownNewStatus();
                    break;
                case "GetAlternateLenderList":
                    GetAlternateLenderList();
                    break;
                default:
                    throw new ArgumentException("unknown method " + method);
            }
        }



        protected void GetAlternateLenderList()
        {
            List<Tuple<string, Guid>> list = DocMagicAlternateLender.GetAlternateLenderListByBroker(BrokerUser.BrokerId);
            Guid currentId = GetGuid("id");
            if (!list.Exists(n => n.Item2.Equals(currentId)))
            {
                DocMagicAlternateLender altLender = new DocMagicAlternateLender(BrokerUser.BrokerId, currentId);
                list.Add(new Tuple<string,Guid>(altLender.LenderName, altLender.DocMagicAlternateLenderId));
            }

            SetResult("Name0", "");
            SetResult("Id0", Guid.Empty);

            for(int i = 1; i <= list.Count; i++)
            {
                SetResult("Name" + i, list[i - 1].Item1);
                SetResult("Id" + i, list[i - 1].Item2);
            }
        }

        protected void GetStatusOfFurthestDownNewStatus()
        {
            CPageData dataLoanOrig = ConstructPageDataClass(sLId);
            CAppData dataAppOrig;
            SetupDataLoan(dataLoanOrig, out dataAppOrig);

            E_sStatusT result = CalculateLoanStatusByFurthestDownNewStatus(dataLoanOrig);
            dataLoanOrig.sStatusLckd = true; // need this to prevent dStatusT from being computed when gotten
            dataLoanOrig.sStatusT = result;

            SetResult("sStatusT", result);
            SetResult("sStatus", dataLoanOrig.sStatusT_rep);

        }
        private void SetupDataLoan(CPageData dataLoan, out CAppData dataApp)
        {
            dataLoan.InitLoad();
            dataApp = null;

            Guid appID = GetGuid("applicationID", Guid.Empty);

            if (dataLoan.nApps != 0)
            {
                if (aAppId == Guid.Empty)
                    dataApp = dataLoan.GetAppData(0);
                else
                    dataApp = dataLoan.GetAppData(aAppId);
            }
            
        }

        private E_sStatusT CalculateLoanStatusByFurthestDownNewStatus(CPageData dataLoanOrig)//, CPageData dataLoanNew)
        {
            // check the list of loan statuses for new dates (in the reverse order as they appear on the page)
            
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sArchivedD, "sArchivedD", E_sStatusT.Loan_Archived)){ return E_sStatusT.Loan_Archived; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sWithdrawnD, "sWithdrawnD", E_sStatusT.Loan_Withdrawn)){ return E_sStatusT.Loan_Withdrawn; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sRejectD, "sRejectD", E_sStatusT.Loan_Rejected)){ return E_sStatusT.Loan_Rejected; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sSuspendedD, "sSuspendedD", E_sStatusT.Loan_Suspended)){ return E_sStatusT.Loan_Suspended; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sCanceledD, "sCanceledD", E_sStatusT.Loan_Canceled)){ return E_sStatusT.Loan_Canceled; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sOnHoldD, "sOnHoldD", E_sStatusT.Loan_OnHold)){ return E_sStatusT.Loan_OnHold; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sCounterOfferD, "sCounterOfferD", E_sStatusT.Loan_CounterOffer)){ return E_sStatusT.Loan_CounterOffer; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sLPurchaseD, "sLPurchaseD", E_sStatusT.Loan_LoanPurchased)) { return E_sStatusT.Loan_LoanPurchased; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sCondSentToInvestorD, "sCondSentToInvestorD", E_sStatusT.Loan_InvestorConditionsSent)){ return E_sStatusT.Loan_InvestorConditionsSent; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sSuspendedByInvestorD, "sSuspendedByInvestorD", E_sStatusT.Loan_InvestorConditions)){ return E_sStatusT.Loan_InvestorConditions; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sShippedToInvestorD, "sShippedToInvestorD", E_sStatusT.Loan_Shipped)){ return E_sStatusT.Loan_Shipped; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sReadyForSaleD, "sReadyForSaleD", E_sStatusT.Loan_ReadyForSale)){ return E_sStatusT.Loan_ReadyForSale; }
            
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sPurchasedD, "sPurchasedD", E_sStatusT.Loan_Purchased)) { return E_sStatusT.Loan_Purchased; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sClearToPurchaseD, "sClearToPurchaseD", E_sStatusT.Loan_ClearToPurchase)) { return E_sStatusT.Loan_ClearToPurchase; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sInFinalPurchaseReviewD, "sInFinalPurchaseReviewD", E_sStatusT.Loan_InFinalPurchaseReview)) { return E_sStatusT.Loan_InFinalPurchaseReview; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sSubmittedForFinalPurchaseD, "sSubmittedForFinalPurchaseD", E_sStatusT.Loan_SubmittedForFinalPurchaseReview)) { return E_sStatusT.Loan_SubmittedForFinalPurchaseReview; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sPrePurchaseConditionsD, "sPrePurchaseConditionsD", E_sStatusT.Loan_PrePurchaseConditions)) { return E_sStatusT.Loan_PrePurchaseConditions; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sInPurchaseReviewD, "sInPurchaseReviewD", E_sStatusT.Loan_InPurchaseReview)) { return E_sStatusT.Loan_InPurchaseReview; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sSubmittedForPurchaseReviewD, "sSubmittedForPurchaseReviewD", E_sStatusT.Loan_SubmittedForPurchaseReview)) { return E_sStatusT.Loan_SubmittedForPurchaseReview; }
            
            
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sClosedD, "sClosedD", E_sStatusT.Loan_Closed)){ return E_sStatusT.Loan_Closed; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sFinalDocsD, "sFinalDocsD", E_sStatusT.Loan_FinalDocs)){ return E_sStatusT.Loan_FinalDocs; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sRecordedD, "sRecordedD", E_sStatusT.Loan_Recorded)){ return E_sStatusT.Loan_Recorded; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sFundD, "sFundD", E_sStatusT.Loan_Funded)){ return E_sStatusT.Loan_Funded; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sFundingConditionsD, "sFundingConditionsD", E_sStatusT.Loan_FundingConditions)){ return E_sStatusT.Loan_FundingConditions; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sDocsBackD, "sDocsBackD", E_sStatusT.Loan_DocsBack)){ return E_sStatusT.Loan_DocsBack; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sDocsD, "sDocsD", E_sStatusT.Loan_Docs)){ return E_sStatusT.Loan_Docs; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sDocsDrawnD, "sDocsDrawnD", E_sStatusT.Loan_DocsDrawn)){ return E_sStatusT.Loan_DocsDrawn; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sDocsOrderedD, "sDocsOrderedD", E_sStatusT.Loan_DocsOrdered)){ return E_sStatusT.Loan_DocsOrdered; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sClearToCloseD, "sClearToCloseD", E_sStatusT.Loan_ClearToClose)){ return E_sStatusT.Loan_ClearToClose; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sPreDocQCD, "sPreDocQCD", E_sStatusT.Loan_PreDocQC)){ return E_sStatusT.Loan_PreDocQC; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sFinalUnderwritingD, "sFinalUnderwritingD", E_sStatusT.Loan_FinalUnderwriting)){ return E_sStatusT.Loan_FinalUnderwriting; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sConditionReviewD, "sConditionReviewD", E_sStatusT.Loan_ConditionReview)){ return E_sStatusT.Loan_ConditionReview; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sApprovD, "sApprovD", E_sStatusT.Loan_Approved)){ return E_sStatusT.Loan_Approved; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sPreApprovD, "sPreApprovD", E_sStatusT.Loan_Preapproval)){ return E_sStatusT.Loan_Preapproval; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sUnderwritingD, "sUnderwritingD", E_sStatusT.Loan_Underwriting)){ return E_sStatusT.Loan_Underwriting; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sPreUnderwritingD, "sPreUnderwritingD", E_sStatusT.Loan_PreUnderwriting)){ return E_sStatusT.Loan_PreUnderwriting; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sLoanSubmittedD, "sLoanSubmittedD", E_sStatusT.Loan_LoanSubmitted)){ return E_sStatusT.Loan_LoanSubmitted; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sDocumentCheckFailedD, "sDocumentCheckFailedD", E_sStatusT.Loan_DocumentCheckFailed)){ return E_sStatusT.Loan_DocumentCheckFailed; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sDocumentCheckD, "sDocumentCheckD", E_sStatusT.Loan_DocumentCheck)){ return E_sStatusT.Loan_DocumentCheck; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sProcessingD, "sProcessingD", E_sStatusT.Loan_Processing)){ return E_sStatusT.Loan_Processing; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sPreProcessingD, "sPreProcessingD", E_sStatusT.Loan_PreProcessing)){ return E_sStatusT.Loan_PreProcessing; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sSubmitD, "sSubmitD", E_sStatusT.Loan_Registered)){ return E_sStatusT.Loan_Registered; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sPreQualD, "sPreQualD", E_sStatusT.Loan_Prequal)){ return E_sStatusT.Loan_Prequal; }
            if (IsNewDateAndAllowedStatus(dataLoanOrig.sOpenedD, "sOpenedD", E_sStatusT.Loan_Open)){ return E_sStatusT.Loan_Open; }

            
            return E_sStatusT.Loan_Other;
        }
        private bool IsNewDateAndAllowedStatus(CDateTime origDate, string newFieldName, E_sStatusT associatedStatus)// CDateTime newDate)
        {
            var broker = BrokerDB.RetrieveById(BrokerUser.BrokerId);
            var channel = (E_BranchChannelT)GetInt("sBranchChannelT");
            var process = (E_sCorrespondentProcessT)GetInt("sCorrespondentProcessT");
            if (!broker.GetEnabledStatusesByChannelAndProcessType(channel, process).Contains(associatedStatus))
            {
                return false;
            }
            CDateTime newDate = CDateTime.Create(GetString(newFieldName, ""), null);
            DateTime dOrig = origDate.GetSafeDateTimeForComputation(DateTime.MinValue);
            DateTime dNew = newDate.GetSafeDateTimeForComputation(DateTime.MinValue);

            // if the new date is an entered value, and the oldDate changed (overwritten in any way.)
            if (DateTime.MinValue != dNew && 0 != dOrig.CompareTo(dNew))
            {
                return true;
            }
            return false;
        }
    }

	public partial class GeneralService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize()
        {
            AddBackgroundItem("", new GeneralServiceItem());
        }
	}
}
