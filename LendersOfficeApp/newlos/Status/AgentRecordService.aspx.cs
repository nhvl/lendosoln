namespace LendersOfficeApp.newlos.Status
{
    using System;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LendersOfficeApp.los.BrokerAdmin;

    public partial class AgentRecordService :  AbstractSingleEditService
	{

        private void SaveData(CAgentFields field) 
        {
            Guid loanID = GetGuid("loanid");
            Guid recordID = GetGuid("recordid");

            CPageData dataLoan = new CPeopleData(LoanID);
            dataLoan.InitLoad();

            var brokerLevelAgentID = GetGuid("ContactInfo_BrokerLevelAgentID", Guid.Empty);
            if (GetBool("ContactInfo_ShouldMatchBrokerContact", false))
            {
                field.PopulateFromRolodex(brokerLevelAgentID, BrokerUserPrincipal.CurrentPrincipal);
            }
            field.BrokerLevelAgentID = brokerLevelAgentID;
            field.AgentSourceT = (E_AgentSourceT)GetInt("ContactInfo_AgentSourceT", 0); // order here is important.

            field.AgentName = GetString("ContactInfo_AgentName");
            field.AgentRoleT = (E_AgentRoleT)GetInt("ContactInfo_AgentRoleT");
            field.CaseNum = GetString("ContactInfo_CaseNum");
            field.CellPhone = GetString("ContactInfo_CellPhone");
            field.City = GetString("ContactInfo_City");
            field.CompanyName = GetString("ContactInfo_CompanyName");
            field.DepartmentName = GetString("ContactInfo_DepartmentName");
            field.EmailAddr = GetString("ContactInfo_EmailAddr");
            field.FaxNum = GetString("ContactInfo_FaxNum");
            field.Notes = GetString("ContactInfo_Notes");
            field.EmployeeIDInCompany = GetString("ContactInfo_EmployeeIDInCompany");
            field.CompanyId = GetString("ContactInfo_CompanyId");
            field.LicenseNumOfAgent = GetString("ContactInfo_LicenseNum");
            field.LicenseNumOfCompany = GetString("ContactInfo_LicenseNumOfCompany");
            field.PagerNum = GetString("ContactInfo_PagerNum");
            field.Phone = GetString("ContactInfo_Phone");
            field.State = GetString("ContactInfo_State");
            field.StreetAddr = GetString("ContactInfo_StreetAddr");
            field.Zip = GetString("ContactInfo_Zip");
            field.County = GetString("ContactInfo_County");
            field.OtherAgentRoleTDesc = GetString("ContactInfo_OtherAgentRoleTDesc");
            field.InvestorBasisPoints_rep = GetString("ContactInfo_InvestorBasisPoints");
            field.InvestorSoldDate_rep = GetString("ContactInfo_InvestorSoldDate");
            field.TaxId = GetString("ContactInfo_TaxID");
            field.ChumsId = GetString("ContactInfo_ChumsID");
            field.IsLenderAssociation = GetBool("ContactInfo_IsLenderAssociation");

            if (dataLoan.BrokerDB.IsEnableGfe2015 && dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy)
            {
                field.IsLenderAffiliate = GetBool("ContactInfo_IsLenderAffiliate");    
            }
            else
            {
                field.IsLenderAffiliate = GetBool("ContactInfo_IsLenderAffiliate_Relation");
            }

            field.IsLenderRelative = GetBool("ContactInfo_IsLenderRelative");
            field.HasLenderRelationship = GetBool("ContactInfo_HasLenderRelationship");
            field.HasLenderAccountLast12Months = GetBool("ContactInfo_HasLenderAccountLast12Months");
            field.IsUsedRepeatlyByLenderLast12Months = GetBool("ContactInfo_IsUsedRepeatlyByLenderLast12Months");
            field.IsApplyToFormsUponSave = GetBool("IsApplyToFormsUponSave");
            field.IsListedInGFEProviderForm = GetBool("ContactInfo_IsListedInGFEProviderForm");
            field.ProviderItemNumber = GetString("ContactInfo_ProviderItemNumber");
            field.PhoneOfCompany = GetString("ContactInfo_PhoneOfCompany");
            field.FaxOfCompany = GetString("ContactInfo_FaxOfCompany");
            field.IsNotifyWhenLoanStatusChange = GetBool("ContactInfo_IsNotifyWhenLoanStatusChange");
            field.CompanyLoanOriginatorIdentifier = GetString("ContactInfo_CompanyLoanOriginatorIdentifier");
            field.LoanOriginatorIdentifier = GetString("ContactInfo_LoanOriginatorIdentifier");
            field.EmployeeId = GetGuid("ContactInfo_EmployeeId", Guid.Empty);
            field.IsLender = GetBool("ContactInfo_IsLender");
            field.IsOriginator = GetBool("ContactInfo_IsOriginator");
            field.IsOriginatorAffiliate = GetBool("ContactInfo_IsOriginatorAffiliate");
            field.OverrideLicenses = GetBool("ContactInfo_OverrideLicenses");

            string prefix = "User";
            string json = GetString(prefix + "LendingLicenseList");
            LicenseInfoList list = LicenseInfoList.JsonDeserialize(json);
            field.LicenseInfoList.Clear();
            field.LicenseInfoList.Add(list);

            prefix = "Company";
            json = GetString(prefix + "LendingLicenseList");
            list = LicenseInfoList.JsonDeserialize(json);
            field.CompanyLicenseInfoList.Clear();
            field.CompanyLicenseInfoList.Add(list);       

            // av opm 48610  Agents record: Hide the commission information unless user has Finance read permission Just in case the user was JUST given the permission (while looking at the page)
            // dont try to save the values if the strings are not there there. If one of them is there the rest should be. 
            if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowAccountantRead) && GetString("ContactInfo_CommissionMinBase", null) != null)
            {
                bool bCopyCommissionToAgent = CommonFunctions.GetEmployeeRoleWithCommissionInLoanFile(field.EmployeeId, LoanID) == E_AgentRoleT.Other;
                if (bCopyCommissionToAgent)
                {
                    field.CommissionMinBase_rep = GetString("ContactInfo_CommissionMinBase");
                    field.CommissionPointOfLoanAmount_rep = GetString("ContactInfo_CommissionPointOfLoanAmount");
                    field.CommissionPointOfGrossProfit_rep = GetString("ContactInfo_CommissionPointOfGrossProfit");
                }
                else
                {
                    // else it retains the old values
                    field.CommissionMinBase = 0;
                    field.CommissionPointOfLoanAmount = 0;
                    field.CommissionPointOfGrossProfit = 0;
                }
            }

            // OPM 109299
            field.BranchName = GetString("ContactInfo_BranchName");
            field.PayToBankName = GetString("ContactInfo_PayToBankName");
            field.PayToBankCityState = GetString("ContactInfo_PayToBankCityState");
            field.PayToABANumber = GetString("ContactInfo_PayToABANumber");
            field.PayToAccountNumber = GetString("ContactInfo_PayToAccountNumber");
            field.PayToAccountName = GetString("ContactInfo_PayToAccountName");
            field.FurtherCreditToAccountNumber = GetString("ContactInfo_FurtherCreditToAccountNumber");
            field.FurtherCreditToAccountName = GetString("ContactInfo_FurtherCreditToAccountName");

            field.Update(false);
        }

        protected override void SaveData() 
        {
            CPageData dataLoan = new CPeopleData(LoanID);
            dataLoan.InitSave(sFileVersion);

            CAgentFields field = dataLoan.GetAgentFields(RecordID);
            SaveData(field);

            dataLoan.Save();

            //SetResult("PrevRecordID", field.RecordId.ToString());
            SetResult("sFileVersion", dataLoan.sFileVersion);
            LoadData(field, field.RecordId);

        }

        protected override void SaveDataAndLoadNext() 
        {

            CPageData dataLoan = new CPeopleData(LoanID);
            dataLoan.InitSave(sFileVersion);

            CAgentFields field = dataLoan.GetAgentFields(RecordID);
            SaveData(field);
            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            Guid prevRecordID = field.RecordId;

            field = dataLoan.GetAgentFields(NextRecordID);
            LoadData(field, prevRecordID);
        }

        protected override void LoadData() 
        {
            Guid loanID = GetGuid("loanid");
            Guid recordID = GetGuid("recordid");

            CPageData dataLoan = new CPeopleData(LoanID);
            dataLoan.InitLoad();

            CAgentFields field = dataLoan.GetAgentFields(RecordID);

            LoadData(field, Guid.Empty);
        }
        private void LoadData(CAgentFields field, Guid prevRecordID) 
        {
            SetResult("ContactInfo_ShouldUpdateCompanyInfo", false);
            if (prevRecordID != Guid.Empty) 
            {
                SetResult("PrevRecordID", prevRecordID);
            }
            SetResult("RecordID", field.RecordId);
            SetResult("ContactInfo_AgentName", field.AgentName);
            SetResult("ContactInfo_AgentRoleT", field.AgentRoleT);
            SetResult("ContactInfo_CaseNum", field.CaseNum);
            SetResult("ContactInfo_CellPhone", field.CellPhone);
            SetResult("ContactInfo_City", field.City);
            SetResult("ContactInfo_CompanyName", field.CompanyName);
            SetResult("ContactInfo_DepartmentName", field.DepartmentName);
            SetResult("ContactInfo_EmailAddr", field.EmailAddr);
            SetResult("ContactInfo_FaxNum", field.FaxNum);
            SetResult("ContactInfo_Notes", field.Notes);
            SetResult("ContactInfo_EmployeeIDInCompany", field.EmployeeIDInCompany);
            SetResult("ContactInfo_CompanyId", field.CompanyId);
            SetResult("ContactInfo_LicenseNum", field.LicenseNumOfAgent);
            SetResult("ContactInfo_LicenseNumOfCompany", field.LicenseNumOfCompany);
            SetResult("ContactInfo_PagerNum", field.PagerNum);
            SetResult("ContactInfo_Phone", field.Phone);
            SetResult("ContactInfo_State", field.State);
            SetResult("ContactInfo_StreetAddr", field.StreetAddr);
            SetResult("ContactInfo_Zip", field.Zip);
            SetResult("ContactInfo_County", field.County);
            SetResult("ContactInfo_OtherAgentRoleTDesc", field.OtherAgentRoleTDesc);
            SetResult("ContactInfo_InvestorBasisPoints", field.InvestorBasisPoints_rep);
            SetResult("ContactInfo_InvestorSoldDate", field.InvestorSoldDate);

            SetResult("ContactInfo_TaxID", field.TaxId);
            SetResult("ContactInfo_ChumsID", field.ChumsId);
            SetResult("ContactInfo_IsLenderAssociation", field.IsLenderAssociation);
            SetResult("ContactInfo_IsLenderAffiliate", field.IsLenderAffiliate);
            SetResult("ContactInfo_IsLenderRelative", field.IsLenderRelative);
            SetResult("ContactInfo_HasLenderRelationship", field.HasLenderRelationship);
            SetResult("ContactInfo_HasLenderAccountLast12Months", field.HasLenderAccountLast12Months);
            SetResult("ContactInfo_IsUsedRepeatlyByLenderLast12Months", field.IsUsedRepeatlyByLenderLast12Months);
            SetResult("IsApplyToFormsUponSave", field.IsApplyToFormsUponSave);
            SetResult("ContactInfo_IsListedInGFEProviderForm", field.IsListedInGFEProviderForm);
            SetResult("ContactInfo_ProviderItemNumber", field.ProviderItemNumber);
            SetResult("ContactInfo_PhoneOfCompany", field.PhoneOfCompany);
            SetResult("ContactInfo_FaxOfCompany", field.FaxOfCompany);
            SetResult("ContactInfo_IsNotifyWhenLoanStatusChange", field.IsNotifyWhenLoanStatusChange);
            SetResult("ContactInfo_CompanyLoanOriginatorIdentifier", field.CompanyLoanOriginatorIdentifier);
            SetResult("ContactInfo_LoanOriginatorIdentifier", field.LoanOriginatorIdentifier);
            SetResult("ContactInfo_EmployeeId", field.EmployeeId);
            SetResult("ContactInfo_AgentSourceT", field.AgentSourceT);
            SetResult("ContactInfo_BrokerLevelAgentID", field.BrokerLevelAgentID);
            SetResult("ContactInfo_IsLender", field.IsLender);
            SetResult("ContactInfo_IsOriginator", field.IsOriginator);
            SetResult("ContactInfo_IsOriginatorAffiliate", field.IsOriginatorAffiliate);
            SetResult("ContactInfo_OverrideLicenses", field.OverrideLicenses);
            
            string prefix = "User";
            string json = LicenseInfoList.JsonSerialize(field.LicenseInfoList);
            SetResult(prefix + "LendingLicenseList", json);

            prefix = "Company";
            json = LicenseInfoList.JsonSerialize(field.CompanyLicenseInfoList);
            SetResult(prefix + "LendingLicenseList", json);


            //opm 48610  Agents record: Hide the commission information unless user has Finance read permission 11/08/10
            if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowAccountantRead))
            {
                SetResult("ContactInfo_CommissionPointOfLoanAmount", field.CommissionPointOfLoanAmount_rep);
                SetResult("ContactInfo_CommissionPointOfGrossProfit", field.CommissionPointOfGrossProfit_rep);
                SetResult("ContactInfo_CommissionMinBase", field.CommissionMinBase_rep);
                SetResult("ContactInfo_Commission", field.Commission_rep);
                SetResult("IsUseOriginatorCompensationAmount", field.IsUseOriginatorCompensationAmount);
            }

            // OPM 109299
            SetResult("ContactInfo_BranchName", field.BranchName);
            SetResult("ContactInfo_PayToBankName", field.PayToBankName);
            SetResult("ContactInfo_PayToBankCityState", field.PayToBankCityState);
            SetResult("ContactInfo_PayToABANumber", field.PayToABANumber.Value);
            SetResult("ContactInfo_PayToAccountNumber", field.PayToAccountNumber.Value);
            SetResult("ContactInfo_PayToAccountName", field.PayToAccountName);
            SetResult("ContactInfo_FurtherCreditToAccountNumber", field.FurtherCreditToAccountNumber.Value);
            SetResult("ContactInfo_FurtherCreditToAccountName", field.FurtherCreditToAccountName);
        }

        protected override void CustomProcess(string methodName) 
        {
            switch (methodName) 
            {
                case "CalculateCommission":
                    CalculateCommission();
                    break;
                case "CalculateRelationships":
                    CalculateRelationships();
                    break;
                case "GetCountyByZip":
                    GetCountyByZip();
                    break;
            }
        }

        /// <summary>
        /// Calculates the county by the zip code.
        /// </summary>
        private void GetCountyByZip()
        {
            var zip = GetString("Zip");
            SetResult("County", Tools.GetCountyFromZipCode(zip));
        }

        private void CalculateCommission() 
        {
            Guid loanID = GetGuid("loanid");
            Guid recordID = GetGuid("recordid");

            CPageData dataLoan = new CPeopleData(LoanID);
            dataLoan.InitLoad();

            CAgentFields field = dataLoan.GetAgentFields(RecordID);

            field.CommissionPointOfLoanAmount_rep = GetString("CommissionPointOfLoanAmount");
            field.CommissionPointOfGrossProfit_rep = GetString("CommissionPointOfGrossProfit");
            field.CommissionMinBase_rep = GetString("CommissionMinBase");

            SetResult("Commission", field.Commission_rep);
            SetResult("IsUseOriginatorCompensationAmount", field.IsUseOriginatorCompensationAmount);

        }

        private void CalculateRelationships()
        {
            // After picking from contact picker, we want the UI in the state
            // that it would be if the system had calculated the affiliate settings.

            Guid loanID = GetGuid("loanid");
            Guid employeeId = GetGuid("EmployeeId", Guid.Empty);
            Guid brokerLevelAgentID = GetGuid("BrokerLevelAgentID", Guid.Empty);

            E_AgentSourceT agentSourceT = (brokerLevelAgentID != Guid.Empty) ? E_AgentSourceT.CorporateList : E_AgentSourceT.NotYetDefined;

            CPageData dataLoan = new CPageData(LoanID,new string[] {"sAgentXmlContent", "sfUpdateAffiliateOfficialContactValues" } );
            dataLoan.InitLoad();
            CAgentFields agent = dataLoan.GetAgentFields(-1);
            agent.EmployeeId = employeeId;
            agent.AgentSourceT = agentSourceT;
            agent.BrokerLevelAgentID = brokerLevelAgentID;

            dataLoan.UpdateAffiliateOfficialContactValues(agent);

            SetResult("IsLender", agent.IsLender);
            SetResult("IsLenderAffiliate", agent.IsLenderAffiliate);
            SetResult("IsOriginator", agent.IsOriginator);
            SetResult("IsOriginatorAffiliate", agent.IsOriginatorAffiliate);
        }

	}
}
