﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos
{
    public partial class TestConfig : BaseLoanPage
    {
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            base.OnInit(e);
        }

        protected void Page_Init(object sender, System.EventArgs e)
        {
            this.PageTitle = "Test Configuration’ ";
            this.PageID = "TestConfig";

            Bind_sTestLoanFileEnvironmentId(sTestLoanFileEnvironmentId);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(TestConfig));
            dataLoan.InitLoad();

            Tools.SetDropDownListValue(sTestLoanFileEnvironmentId, dataLoan.sTestLoanFileEnvironmentId.ToString());
        }

        private void Bind_sTestLoanFileEnvironmentId(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("Production", Guid.Empty.ToString()));
            ddl.Items.Add(new ListItem("Fee Service", ConstApp.TestFeeServiceEnvironmentId.ToString()));
        }
    }
}