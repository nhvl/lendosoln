﻿using System;
using LendersOffice.Conversions;
using DataAccess;

namespace LendersOfficeApp.newlos
{
    public partial class FreddieLoanQualityAdvisorExport : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string cmd = Request.QueryString["cmd"];
            if (Page.IsPostBack == false && string.IsNullOrEmpty(cmd) == false && cmd.Equals("download", StringComparison.OrdinalIgnoreCase))
            {
                DownloadXml();
            }

        }

        private void DownloadXml()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FreddieLoanQualityAdvisorExport));
            dataLoan.InitLoad();

            Response.Clear();

            UlddExporter newExporter = new UlddExporter(LoanID, true);
            Response.ContentType = "text/xml";
            Response.BufferOutput = true;
            Response.AddHeader("Content-Disposition", $"attachment; filename=\"{dataLoan.sLNm}.xml\"");
            Response.Write(newExporter.Export());
            Response.Flush();
            Response.End();

        }
    }
}
