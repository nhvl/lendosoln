<%@ Import Namespace="DataAccess"%>
<%@ Register TagPrefix="cc1" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="AssetRecord.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.AssetRecord" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../newlos/Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!doctype html>
<html>
<head runat="server">
    <title>Borrower Assets</title>
    <style type="text/css">
        .Hidden { display: none; }
        .AssetDateLabel { margin-right: 15px; }
    </style>
</head>
<body leftmargin=0 bgcolor="gainsboro" scroll="yes" MS_POSITIONING="FlowLayout">
    <script type="text/javascript">
      <!--
      var oRolodex = null;
      function contains(a, obj) {
          var i = a.length;
          while (i--) {
            if (a[i] == obj) {
              return true;
            }
          }
          return false;
        }

      function _init() {
        oRolodex = new cRolodex();
        parent.parent_initScreen(1);
      }

      function lowerFrameForCalendar() {
          window.scrollTo(0, document.body.scrollHeight);
      }

      function getSpecialValues(args) {
        args["CashDeposit1_Desc"] = parent.getCashDeposit1_Desc();
        args["CashDeposit1_Val"] = parent.getCashDeposit1_Val();
        args["CashDeposit2_Desc"] = parent.getCashDeposit2_Desc();
        args["CashDeposit2_Val"] = parent.getCashDeposit2_Val();
        args["LifeInsurance_FaceVal"] = parent.getLifeInsurance_FaceVal();
        args["LifeInsurance_Val"] = parent.getLifeInsurance_Val();
        args["Retirement_Val"] = parent.getRetirement_Val();
        args["Business_Val"] = parent.getBusiness_Val();
        args["aAsstLiaCompletedNotJointly"] = parent.getAsstLiaCompletedNotJointly();
        args["sLienToIncludeCashDepositInTridDisclosures"] = parent.getLienToIncludeCashDepositInTridDisclosures();
      }

      function setSpecialValues(args) {
        parent.updateTotal(args["aAsstLiqTot"], args["aReTotVal"], args["aAsstNonReSolidTot"], args["aAsstValTot"]);
        parent.setCashDeposit1_Desc(args["CashDeposit1_Desc"]);
        parent.setCashDeposit1_Val(args["CashDeposit1_Val"]);

        parent.setCashDeposit2_Desc(args["CashDeposit2_Desc"]);
        parent.setCashDeposit2_Val(args["CashDeposit2_Val"]);

        parent.setLifeInsurance_FaceVal(args["LifeInsurance_FaceVal"]);
        parent.setLifeInsurance_Val(args["LifeInsurance_Val"]);

        parent.setRetirement_Val(args["Retirement_Val"]);
        parent.setBusiness_Val(args["Business_Val"]);

        parent.setLienToIncludeCashDepositInTridDisclosures(args["sLienToIncludeCashDepositInTridDisclosures"]);
      }

      function refreshCalculation() {
        invokeMethod("CalculateData", document.getElementById("RecordID").value);
      }

      function updateDirtyBit_callback() {
          parent.parent_disableSaveBtn(false);
      }
      function refreshUI(isFocus) {
        changeAssetType();
        if (document.getElementById("MainEdit").style.display != "none" && isFocus)
          <%= AspxTools.JsGetElementById(ComNm) %>.focus();
      }

        function updateListIFrame(row) {
            var ListIFrame = retrieveFrame(parent, "ListIFrame");
        if (ListIFrame) {
          var AssetT = "";
          var OwnerT = getDropDownValue(<%= AspxTools.JsGetElementById(OwnerT) %>).charAt(0);

          var assetTDDL = <%= AspxTools.JsGetElementById(AssetT) %>;
          if ((assetTDDL.value == <%= AspxTools.JsString(E_AssetT.OtherIlliquidAsset.ToString("D")) %> || assetTDDL.value == <%= AspxTools.JsString(E_AssetT.OtherLiquidAsset.ToString("D")) %>) && <%= AspxTools.JsGetElementById(OtherTypeDesc)%>.value != "") {
            // If assetT is other liquid then get description from OtherTypeDesc
            AssetT = <%= AspxTools.JsGetElementById(OtherTypeDesc) %>.value;
          } else {
            AssetT = getDropDownValue(<%= AspxTools.JsGetElementById(AssetT) %>);
          }

          var Desc = <%= AspxTools.JsGetElementById(Desc) %>.value;
          var Val = <%= AspxTools.JsGetElementById(Val) %>.value;

          callFrameMethod(parent, "ListIFrame", "list_updateRow", [row, document.getElementById("RecordID").value, OwnerT, AssetT, Desc, Val]);
        }
      }
      function getDropDownValue(ddl) {
        return ddl.options[ddl.selectedIndex].innerText;
      }

      function populateAccountName() {
        var value = document.getElementById("BorrowerName").value;
        if (document.getElementById("OwnerT").selectedIndex == 1)
          value = document.getElementById("CoborrowerName").value;
        else if (document.getElementById("OwnerT").selectedIndex == 2)
          value = document.getElementById("BorrowerName").value + " & " + document.getElementById("CoborrowerName").value;
        document.getElementById("AccNm").value = value;
      }
      function changeAssetType() {
        var assetT = document.getElementById('AssetT').value;
        <%-- If asset type is auto then the only fields allow to be edit are Desc and Val --%>
        if (assetT == <%= AspxTools.JsString(E_AssetT.Auto.ToString("D")) %> || assetT == <%= AspxTools.JsString(E_AssetT.OtherIlliquidAsset.ToString("D")) %>) {
          <%= AspxTools.JsGetElementById(AccNm) %>.readOnly = true;
          <%= AspxTools.JsGetElementById(AccNum) %>.readOnly = true;
          <%= AspxTools.JsGetElementById(ComNm) %>.readOnly = true;
          <%= AspxTools.JsGetElementById(DepartmentName) %>.readOnly = true;
          <%= AspxTools.JsGetElementById(StAddr) %>.readOnly = true;
          <%= AspxTools.JsGetElementById(City) %>.readOnly = true;
          <%= AspxTools.JsGetElementById(State) %>.disabled = true;
          <%= AspxTools.JsGetElementById(State) %>.style.backgroundColor = gReadonlyBackgroundColor;
          <%= AspxTools.JsGetElementById(Zip) %>.readOnly = true;
          <%= AspxTools.JsGetElementById(VerifSentD) %>.readOnly = true;
          <%= AspxTools.JsGetElementById(VerifReorderedD) %>.readOnly = true;
          <%= AspxTools.JsGetElementById(VerifRecvD) %>.readOnly = true;
            <%= AspxTools.JsGetElementById(VerifExpD) %>.readOnly = true;
            <%= AspxTools.JsGetElementById(PhoneNumber) %>.readOnly = true;

          <%= AspxTools.JsGetElementById(AccNm) %>.value = '';
          <%= AspxTools.JsGetElementById(AccNum) %>.value = '';
          <%= AspxTools.JsGetElementById(ComNm) %>.value = '';
          <%= AspxTools.JsGetElementById(DepartmentName) %>.value = '';
          <%= AspxTools.JsGetElementById(StAddr) %>.value = '';
          <%= AspxTools.JsGetElementById(City) %>.value = '';
          <%= AspxTools.JsGetElementById(State) %>.value = '';
          <%= AspxTools.JsGetElementById(Zip) %>.value = '';
          <%= AspxTools.JsGetElementById(VerifSentD) %>.value = '';
          <%= AspxTools.JsGetElementById(VerifReorderedD) %>.value = '';
          <%= AspxTools.JsGetElementById(VerifRecvD) %>.value = '';
          <%= AspxTools.JsGetElementById(VerifExpD) %>.value = '';
            <%= AspxTools.JsGetElementById(GiftSource) %>.cssClass = 'hidden';
            <%= AspxTools.JsGetElementById(PhoneNumber) %>.value = '';

        } else {
          if (<%= AspxTools.JsGetElementById(AccNm) %>.readOnly) {
            // If it was readonly. Then populate the borrower name.
            populateAccountName();
          }
          <%= AspxTools.JsGetElementById(AccNm) %>.readOnly = false;
          <%= AspxTools.JsGetElementById(AccNum) %>.readOnly = false;
          <%= AspxTools.JsGetElementById(ComNm) %>.readOnly = false;
          <%= AspxTools.JsGetElementById(DepartmentName) %>.readOnly = false;
          <%= AspxTools.JsGetElementById(StAddr) %>.readOnly = false;
          <%= AspxTools.JsGetElementById(City) %>.readOnly = false;
          <%= AspxTools.JsGetElementById(State) %>.disabled = false;
          <%= AspxTools.JsGetElementById(State) %>.style.backgroundColor = '';
          <%= AspxTools.JsGetElementById(PhoneNumber) %>.readOnly = false;
          <%= AspxTools.JsGetElementById(Zip) %>.readOnly = false;
          <%= AspxTools.JsGetElementById(VerifSentD) %>.readOnly = false;
          <%= AspxTools.JsGetElementById(VerifReorderedD) %>.readOnly = false;
          <%= AspxTools.JsGetElementById(VerifRecvD) %>.readOnly = false;
          <%= AspxTools.JsGetElementById(VerifExpD) %>.readOnly = false;



        }
        <%= AspxTools.JsGetElementById(GiftSource) %>.className  = <%= AspxTools.JsString(E_AssetT.GiftFunds.ToString("D")) %> == assetT ? "" : "hidden";
        document.getElementById('giftfundsourcelabel').className  = <%= AspxTools.JsString(E_AssetT.GiftFunds.ToString("D")) %> == assetT ? "" : "hidden";
        if (assetT == <%= AspxTools.JsString(E_AssetT.OtherIlliquidAsset.ToString("D")) %> || assetT == <%= AspxTools.JsString(E_AssetT.OtherLiquidAsset.ToString("D")) %>) {
          document.getElementById("OtherTypeDesc").readOnly = false;
        } else {
          document.getElementById("OtherTypeDesc").readOnly = true;
        }

        parent.parent_setVODStatus(contains(VOAAssets, assetT));
      }



//-->
        </script>
        <form id="AssetRecord" method="post" runat="server">
          <div id="MainEdit">
            <table class="FormTable" id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
                <TR>
                    <td>
                        <TABLE class="InsetBorder" id="Table1" cellSpacing="0" cellPadding="0" width="99%" border="0">
                            <TR>
                                <TD noWrap>
                                    <TABLE id="Table4" cellSpacing="0" cellPadding="0" width="99%" border="0">
                                        <TR>
                                            <TD class="FieldLabel" > Owner</TD>
                                            <TD class=FieldLabel><asp:dropdownlist id="OwnerT" runat="server" onchange="populateAccountName();" >
<asp:ListItem Value="0" Selected="True">Borrower</asp:ListItem>
<asp:ListItem Value="1">Coborrower</asp:ListItem>
<asp:ListItem Value="2">Joint</asp:ListItem>
                                                </asp:dropdownlist>&nbsp;&nbsp;Type&nbsp;<asp:dropdownlist id="AssetT" runat="server" onchange="changeAssetType();"></asp:dropdownlist><asp:textbox id="OtherTypeDesc" runat="server" Width="139px"></asp:textbox></TD>
                                        </TR>
                                        <tr height=5><td nowrap class=FieldLable></td></tr>
                                        <tr>
											<td class=FieldLabel nowrap></td>
											<td nowrap>
												<UC:CFM runat=server Type="20" CompanyNameField="ComNm" DepartmentNameField="DepartmentName" StreetAddressField="StAddr" CityField="City" StateField="State" ZipField="Zip" PhoneField="PhoneNumber"></UC:CFM>
											</td>
                                        </tr>
                                        <TR>
                                            <TD class="FieldLabel" >Company Name</TD>
                                            <TD><asp:textbox id="ComNm" runat="server" Width="270px"></asp:textbox></TD>
                                        </TR>
                                        <TR>
                                            <TD class="FieldLabel" >Department</TD>
                                            <TD><asp:textbox id="DepartmentName" runat="server" Width="270px"></asp:textbox></TD>
                                        </TR>
                                        <TR>
                                            <TD class="FieldLabel" >Address</TD>
                                            <TD><asp:textbox id="StAddr" runat="server" Width="270px"></asp:textbox></TD>
                                        </TR>
                                        <TR>
                                            <TD class="FieldLabel" >City</TD>
                                            <TD><asp:textbox id="City" runat="server" Width="154px"></asp:textbox><cc1:statedropdownlist id="State" runat="server"></cc1:statedropdownlist><cc1:zipcodetextbox id="Zip" runat="server" width="70px" preset="zipcode" CssClass="mask"></cc1:zipcodetextbox></TD>
                                        </TR>
                                        <tr>
                                            <td class="FieldLabel">Phone Number</td>
                                            <td>
                                                <ml:PhoneTextBox ID="PhoneNumber" runat="server" preset="phone"></ml:PhoneTextBox>
                                            </td>
                                        </tr>
                                    </TABLE>
                                </TD>
                            </TR>
                        </TABLE>
                    </td>
                </TR>
                <tr>
                    <td>
                        <TABLE class="InsetBorder" id="Table3" cellSpacing="0" cellPadding="0" width="99%" border="0">
                            <TR>
                                <TD>
                                    <TABLE id="Table6" cellSpacing="0" cellPadding="0" width="99%" border="0">
                                        <TR>
                                            <TD class="FieldLabel" >Description</TD>
                                            <TD class=FieldLabel><asp:textbox id="Desc" runat="server" Width="266px"></asp:textbox>&nbsp;Value&nbsp;
                                            <cc1:moneytextbox id="Val" runat="server" width="90" preset="money" CssClass="mask"  onchange="refreshCalculation();"></cc1:moneytextbox>
                                            &nbsp;&nbsp;<span id="giftfundsourcelabel">Source </span> <asp:DropDownList runat="server" ID="GiftSource" CssClass="Hidden"   > </asp:DropDownList></TD>
                                        </TR>
              <TR>
                <TD class=FieldLabel noWrap>Account Holder Name&nbsp;</TD>
                <TD class=FieldLabel width="90%"><asp:textbox id="AccNm" runat="server" Width="267px"></asp:textbox>&nbsp;Account
                                                Number&nbsp;<asp:textbox id="AccNum" runat="server" Width="152px"></asp:textbox></TD></TR>
                                    </TABLE>
                                </TD>
                            </TR>
                        </TABLE>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="InsetBorder" id="Table7" cellSpacing="0" cellPadding="0" width="99%" border="0">
                            <tr onclick="lowerFrameForCalendar();">
                                <td class="FieldLabel">
                                    <label class="AssetDateLabel">Verif. Sent</label>
                                    <cc1:datetextbox id="VerifSentD" runat="server" width="65px" preset="date" CssClass="mask"></cc1:datetextbox>
                                </td>
                                <td class="FieldLabel">
                                    <label class="AssetDateLabel">Re-order</label>
                                    <cc1:datetextbox id="VerifReorderedD" runat="server" width="65px" preset="date" CssClass="mask"></cc1:datetextbox>
                                </td>
                                <td class="FieldLabel">
                                    <label class="AssetDateLabel">Received</label>
                                    <cc1:datetextbox id="VerifRecvD" runat="server" width="65px" preset="date" CssClass="mask"></cc1:datetextbox>
                                </td>
                                <td class="FieldLabel">
                                    <label class="AssetDateLabel">Expected</label>
                                    <cc1:datetextbox id="VerifExpD" runat="server" width="65px" preset="date" CssClass="mask"></cc1:datetextbox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </div>
        </form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
    </body>
</html>
