﻿#region Generated Code
namespace LendersOfficeApp.newlos
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// The QualifyingBorrowerPageService is used to load, calculate, and save the data for the qualifying borrower user control.
    /// </summary>
    public partial class QualifyingBorrowerPageService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Determines what method to call.
        /// </summary>
        /// <param name="methodName">The name of the method to call.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "LoadData":
                    Refresh(false);
                    break;
                case "CalculateData":
                    Refresh(true);
                    break;
                case "SaveData":
                    Save();
                    break;
                default:
                    throw new UnhandledCaseException("Attempted to call the method " + methodName + " in QualifyingBorrowerPageService");
            }
        }

        /// <summary>
        /// Loads the loan for the appropriate purpose.
        /// </summary>
        /// <param name="isSave">A value indicating whether the loan is to be saved afterwards.</param>
        /// <returns>The loan file.</returns>
        private CPageData GetLoan(bool isSave)
        {
            Guid loanId = GetGuid("loanid");
            var dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(QualifyingBorrowerPageService));
            if (isSave)
            {
                dataLoan.InitSave();
            }
            else
            {
                dataLoan.InitLoad();
            }

            return dataLoan;
        }

        /// <summary>
        /// Updates the loan with the data sent to the service page.
        /// </summary>
        /// <param name="dataLoan">The loan to be updated.</param>
        private void UpdateLoan(CPageData dataLoan)
        {
            QualifyingBorrower.Bind_QualifyingBorrower(this, dataLoan);
        }

        /// <summary>
        /// Updates the service page result with the loan data.
        /// </summary>
        /// <param name="dataLoan">The loan to retrieve the results from.</param>
        private void BindLoan(CPageData dataLoan)
        {
            QualifyingBorrower.Load_QualifyingBorrower(this, dataLoan);
        }

        /// <summary>
        /// The calculation method.
        /// </summary>
        /// <param name="isLoaded">A value indicating whether the page has been loaded already.</param>
        private void Refresh(bool isLoaded)
        {
            var dataLoan = GetLoan(false);
            if (isLoaded)
            {
                UpdateLoan(dataLoan);
            }

            BindLoan(dataLoan);
        }

        /// <summary>
        /// Updates and saves the loan.
        /// </summary>
        private void Save()
        {
            var dataLoan = GetLoan(true);
            UpdateLoan(dataLoan);
            dataLoan.Save();
            BindLoan(dataLoan);
        }
    }
}