namespace LendersOfficeApp.newlos
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Pdf.Async;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    public partial class PrintListService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "HidePassword":
                    HidePassword();
                    break;
                case "BatchPrint":
                    BatchPrint();
                    break;
                case "BatchEPrint":
                    BatchEPrint();
                    break;
                case "PrintToEDocs":
                    PrintToEDocs();
                    break;
                case "PrintToEDocs_Poll":
                    PrintToEDocs_Poll();
                    break;
                case "GetNewPrintId":
                    GetNewPrintId();
                    break;
            }
        }
        private string GetPassword() 
        {
            string option = GetString("PdfPasswordOption", "");
            string password = "";

            if (option == "sSpZip") 
            {
                CPageData dataLoan = new CPdfEncryptionPasswordData(GetGuid("loanid"));
                dataLoan.InitLoad();
                password = dataLoan.sSpZip;
            } 
            else if (option == "Other") 
            {
                password = GetString("PdfOtherPassword", "");
            }
            return password;
        }
        private void HidePassword() 
        {
            string id = Guid.NewGuid().ToString();
            string passwordKey = "PrintPassword" + id;
            string password = GetPassword();
			
			//01-30-08 20080 av
			if ( password != null && password != String.Empty )
				AutoExpiredTextCache.AddToCache(password, new TimeSpan(0, 5, 0), passwordKey);

            SetResult("ID", id);
        }

        private PdfPrintList GetPrintList() 
        {
            int count = GetInt("count");
            Guid loanID = GetGuid("loanid");
            string printOptions = GetString("printoptions", "");
            PdfPrintList printList = new PdfPrintList();
            printList.sLId = loanID;
            printList.PrintOptions = printOptions;
            for (int i = 0; i < count; i++) 
            {
                var item = new PdfPrintItem();
                item.Type = GetString("type_" + i, "");
                item.ID = GetString("item_" + i, "");
                item.ApplicationID = GetGuid("appid_" + i, Guid.Empty);
                item.RecordID = GetGuid("recordid_" + i, Guid.Empty);
                item.PageSize = GetString("pagesize_" + i);
                item.BorrType = GetString("borrtype_" + i, "");
                item.IsAddUli = GetString("isadduli" + i, "");
                printList.ItemList.Add(item);
            }
            return printList;

        }

        private string StorePrintListToCache(PdfPrintList list, string password) 
        {
            string id = Guid.NewGuid().ToString();
            string key = "Print" + id;
            string passwordKey = "PrintPassword" + id;

            string xml = SerializationHelper.XmlSerialize(list);

//            Cache.Add(key, list, null, DateTime.Now.AddMinutes(5), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.Normal, null);
            AutoExpiredTextCache.AddToCache(xml, new TimeSpan(0, 10, 0), key);
			//01-30-08 20080 av
			
			if( password != null && password !=  String.Empty ) 
			{
				AutoExpiredTextCache.AddToCache(password, new TimeSpan(0, 10, 0), passwordKey);
			}
            return id;
        }
        private void BatchPrint() 
        {
            PdfPrintList list = GetPrintList();
            string password = GetPassword();
            string id = StorePrintListToCache(list, password);
            SetResult("PrintID", id);
        }

        private void BatchEPrint() 
        {
            PdfPrintList list = GetPrintList();
            PdfPrintList letterSize = new PdfPrintList();
            PdfPrintList legalSize = new PdfPrintList();

            letterSize.PrintOptions = list.PrintOptions;
            letterSize.sLId = list.sLId;

            legalSize.PrintOptions = list.PrintOptions;
            legalSize.sLId = list.sLId;

            foreach (PdfPrintItem item in list.ItemList) 
            {
                if (item.PageSize == "0")
                    letterSize.ItemList.Add(item);
                else
                    legalSize.ItemList.Add(item);
            }
            if (legalSize.ItemList.Count> 0)
            {
                string id = StorePrintListToCache(legalSize, "");
                SetResult("LegalSizeID", id);
                
            }
            if (letterSize.ItemList.Count > 0) 
            {
                string id = StorePrintListToCache(letterSize, "");
                SetResult("LetterSizeID", id);

            }
        }

        private void PrintToEDocs()
        {
            var currentUser = PrincipalFactory.CurrentPrincipal;
            var repository = EDocs.EDocumentRepository.GetUserRepository();
            if (!repository.CanCreateDocument)
            {
                this.SetResult("ErrorMessage", ErrorMessages.EDocs.InsufficientCreatePermission);
                this.SetResult("Success", false);
                return;
            }

            PdfPrintList printList = this.GetPrintList();
            string password = this.GetPassword();

            if (ConstStage.UseSynchronousPdfGenerationFallback)
            {
                var generationHelper = new AsyncPdfGenerationHelper();
                var result = generationHelper.GenerateBatchPdf(printList, password);

                switch (result.Status)
                {
                    case AsyncPdfGenerationStatus.Complete:
                        this.UploadPrintedDocumentToEDocs(repository, result, printList);
                        break;
                    case AsyncPdfGenerationStatus.Processing:
                    case AsyncPdfGenerationStatus.Error:
                        this.SetResult("Error", true);
                        this.SetResult("ErrorMessage", ErrorMessages.Generic);
                        break;
                    default:
                        throw new UnhandledEnumException(result.Status);
                }
            }
            else
            {
                var jobHelper = new AsyncPdfJobHelper();
                var jobId = jobHelper.CreateBatchPdfJob(currentUser, printList, password);

                // Save the print list after adding the initial job so we 
                // can query the batch PDF list later for e-sign tags.
                var serializedPrintList = SerializationHelper.XmlSerialize(printList);
                AutoExpiredTextCache.AddToCacheByUser(currentUser, serializedPrintList, TimeSpan.FromMinutes(30), jobId);

                this.SetResult("JobId", jobId);
            }
        }

        private void PrintToEDocs_Poll()
        {
            var repository = EDocs.EDocumentRepository.GetUserRepository();
            if (!repository.CanCreateDocument)
            {
                this.SetResult("Error", true);
                this.SetResult("ErrorMessage", ErrorMessages.EDocs.InsufficientCreatePermission);
                return;
            }

            var jobHelper = new AsyncPdfJobHelper();
            var jobId = this.GetGuid("JobId");

            var result = jobHelper.GetJobStatus(jobId);
            if (result.Status == AsyncPdfGenerationStatus.Complete)
            {
                var serializedPrintList = AutoExpiredTextCache.GetFromCacheByUser(PrincipalFactory.CurrentPrincipal, jobId.ToString());
                var deserializedPrintList = SerializationHelper.XmlDeserialize<PdfPrintList>(serializedPrintList);
                this.UploadPrintedDocumentToEDocs(repository, result, deserializedPrintList);
            }
            else if (result.Status == AsyncPdfGenerationStatus.Error)
            {
                this.SetResult("Error", true);
                this.SetResult("ErrorMessage", ErrorMessages.Generic);
            }
            else
            {
                this.SetResult("Complete", false);
            }
        }

        private void UploadPrintedDocumentToEDocs(EDocs.EDocumentRepository repository, AsyncPdfGenerationResult result, PdfPrintList printList)
        {
            var document = repository.CreateDocument(EDocs.E_EDocumentSource.UserUpload); // The order's a little funky, but this will check the parameters before generating
            document.LoanId = printList.sLId;
            document.AppId = this.GetGuid("EDoc_aAppId");
            document.DocumentTypeId = this.GetInt("EDoc_DocTypeId");
            document.PublicDescription = this.GetString("EDoc_Description");
            document.InternalDescription = this.GetString("EDoc_InternalComments");

            var batchPdfHelper = new LendersOffice.Pdf.BatchPdfHelper();
            var batchPdf = batchPdfHelper.CreateGenericBatchPdfInstance(printList);

            if (batchPdf.HasPdfFormLayout)
            {
                foreach (var annotation in EDocs.PdfFormToEDocConversion.CreateESignTagAnnotations(batchPdf.PdfFormLayout))
                {
                    document.ESignTags.Add(annotation);
                }
            }

            var filePath = FileDBTools.CreateCopy(E_FileDB.Normal, result.FileDbKey.ToString());
            document.UpdatePDFContentOnSave(filePath);
            repository.Save(document);

            // Cleanup temp file.
            var localFilePath = LocalFilePath.Create(filePath);
            LendersOffice.Drivers.FileSystem.FileOperationHelper.Delete(localFilePath.Value);
            this.SetResult("Complete", true);
        }

        private void GetNewPrintId()
        {
            SetResult("PrintId", BrokerUserPrincipal.CurrentPrincipal.GenerateByPassTicket().ToString());
        }
	}
}
