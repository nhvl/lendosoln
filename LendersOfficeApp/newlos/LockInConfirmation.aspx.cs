using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos
{
	public partial class LockInConfirmation : BaseLoanPage
	{

        protected override void LoadData() 
        {
            CPageData dataLoan = new CLockInConfirmationData(LoanID);
            dataLoan.InitLoad();

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.LockInConfirmation, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            LockInConfirmationCompanyName.Text = preparer.CompanyName;
            LockInConfirmationStreetAddr.Text = preparer.StreetAddr;
            LockInConfirmationCity.Text = preparer.City;
            LockInConfirmationState.Value = preparer.State;
            LockInConfirmationZip.Text = preparer.Zip;
            LockInConfirmationPhoneOfCompany.Text = preparer.PhoneOfCompany;
            LockInConfirmationPreparedDate.Text = preparer.PrepareDate_rep;
			LockInConfirmationPreparedDate.ToolTip = "Hint:  Enter 't' for today's date.";
            
            Tools.SetDropDownListValue(sLT, dataLoan.sLT);

            if (Tools.ShouldBeReadonly_sLT(dataLoan.sLPurposeT))
            {
                Tools.ReadonlifyDropDown(sLT);
            }

            sLAmt.Text = dataLoan.sLAmtCalc_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sTerm.Text = dataLoan.sTerm_rep;
            sTerm.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sDue.Text = dataLoan.sDue_rep;
            sDue.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sLpTemplateNm.Text = dataLoan.sLpTemplateNm;
			// 5/29/07 mf. OPM 12061. There is now a corporate setting for editing
			// the loan program name.
            if (! BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowEditingLoanProgramName))
				sLpTemplateNm.ReadOnly = true;

            sRLckdExpiredD.Text = dataLoan.sRLckdExpiredD_rep;
            sRLckdExpiredD.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly || Broker.HasLenderDefaultFeatures;
			sRLckdExpiredD.IsDisplayCalendarHelper = !sRLckdExpiredD.ReadOnly;
			if (sRLckdExpiredD.IsDisplayCalendarHelper) sRLckdExpiredD.ToolTip = "Hint:  Enter 't' for today's date.";

            sLOrigFPc.Text = dataLoan.sLOrigFPc_rep;
            sLOrigFPc.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sLDiscntPc.Text = dataLoan.sLDiscntPc_rep;
            sLOrigFMb.Text = dataLoan.sLOrigFMb_rep;
            sLDiscntFMb.Text = dataLoan.sLDiscntFMb_rep;
            sLOrigF.Text = dataLoan.sLOrigF_rep;
            sLDiscnt.Text = dataLoan.sLDiscnt_rep;

        }


        protected void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Lock-In Confirmation";
            this.PageID = "LockInConfirmation";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CLockInConfirmationPDF);
            Tools.Bind_sLT(sLT);
            LockInConfirmationZip.SmartZipcode(LockInConfirmationCity, LockInConfirmationState);
            
			this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }

        protected HtmlControl GenerateExpirationDateLink()
        {
            if (Broker.HasLenderDefaultFeatures)
            {
                var anchor = new HtmlAnchor();
                anchor.InnerText = "Expiration Date";
                anchor.HRef = "javascript:linkMe('LockDesk/BrokerRateLock.aspx');";
                anchor.Title = "Go to Rate Lock page";
                anchor.Attributes.Add("tabindex", "-1");
                return anchor;
            }

            var span = new HtmlGenericControl("span");
            span.InnerText = "Expiration Date";
            return span;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
            IsAppSpecific = false;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion
	}
}
