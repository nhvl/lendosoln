using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Globalization;

using DataAccess;

namespace LendersOfficeApp.newlos
{
	public partial class BorrowerAssetList : BaseListEditPage
	{
    
        private NumberFormatInfo m_formatMoney;

        protected string displayMoneyString(string value) 
        {
            if (value == null || value == "") 
            {
                return "";
            }
            try 
            {
                return decimal.Parse(value).ToString("C", m_formatMoney);
            } 
            catch 
            {
                // If value is invalid then display value AS IS.
                return value;
            }
        }
        protected string displayOwnerType(object type) 
        {
			try
			{
				E_AssetOwnerT ownerT = (E_AssetOwnerT) int.Parse( type.ToString() );
				return CAssetFields.DisplayStringForOwnerT( ownerT );
			}
			catch
			{ return "?"; }
        }
        protected string displayAssetType(object type, string other) 
        {
            try 
            {
                E_AssetRegularT assetType = (E_AssetRegularT) int.Parse(type.ToString());

                return CAssetRegular.DisplayStringForAssetT( assetType, other );
            } 
            catch {return "???";}

        }
        protected override void LoadData() 
        {
            m_formatMoney = new NumberFormatInfo();
            m_formatMoney.CurrencyDecimalDigits = 2;
            m_formatMoney.CurrencyNegativePattern = 0; // Parenthesis
            m_formatMoney.CurrencySymbol = "$";

            CPageData dataLoan = CPageData.CreateUsingSmartDependency( LoanID, typeof(BorrowerAssetList) );
            dataLoan.InitLoad();
			
            CAppData dataApp = dataLoan.GetAppData( ApplicationID ); 

            IAssetCollection recordList = dataApp.aAssetCollection;

            m_dg.DataSource = recordList.SortedView;
            m_dg.DataBind();
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.m_dg.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DataGrid_ItemCreated);

        }
		#endregion


	}
}
