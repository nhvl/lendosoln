﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Conversions;

namespace LendersOfficeApp.newlos
{
    public partial class ExportUlddMismo30 : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ExportUlddMismo30));
            dataLoan.InitLoad();
            Response.Clear();
            UlddExporter newExporter = new UlddExporter(LoanID);
            List<UlddExportError> exportErrors;
            if (newExporter.Verify(out exportErrors))
            {
                Response.ContentType = "text/xml";
                Response.BufferOutput = true;
                Response.AddHeader("Content-Disposition", $"attachment; filename=\"{dataLoan.sLNm}.xml\"");
                Response.Write(newExporter.Export());    
            }
            else
            {
                var errors = (from error in exportErrors
                              from subError in error.ErrorList
                              select subError).ToArray();

                string key = AutoExpiredTextCache.AddToCache(ObsoleteSerializationHelper.JavascriptJsonSerialize(errors), TimeSpan.FromHours(1.0));
                string url = "ErrorInExportULDD.aspx?key=" + key;
                //Calculate num to adjust the height of popup window - based on how many errors are in there.
                int num = errors.Length + 1;
                Response.Write("<script type='text/javascript'>window.open('" + url + "','Errors','toolbar=no,menubar=no,width=510,height=" + (15 + num * 45 + 15) + ",resizable=no,scrollbars=yes');</script>");
                Response.Write("<script type='text/javascript'>history.back(-1);</script>");   
            }
            Response.Flush();
            Response.End();
            
        }
    }
}
