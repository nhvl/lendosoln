using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Admin;

namespace LendersOfficeApp.newlos
{
	public partial class DebtConsolidateSummary : LendersOfficeApp.newlos.BaseLoanPage
	{

        protected void PageLoad(object sender, System.EventArgs e)
        {
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterJsScript("loanframework2.js");
            this.DisplayCopyRight = false;
            Tools.Bind_sLPurposeT(sLPurposeT);
        }
        protected override void LoadData() 
        {

            
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(DebtConsolidateSummary));
            dataLoan.InitLoad();
            


            sMonthlyPmt.Text = dataLoan.sMonthlyPmt_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;

            sQualBottomR.Text = dataLoan.sQualBottomR_rep;
            sTransNetCash.Text = dataLoan.sTransNetCash_rep;
            sLAmtCalc.Text = dataLoan.sLAmtCalc_rep;
            
            sTerm.Text = dataLoan.sTerm_rep;
            sTerm.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;

            sDue.Text = dataLoan.sDue_rep;
            sDue.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sLAmtLckd.Checked = dataLoan.sLAmtLckd;

            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            if (BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan)) 
            {

                sProdCashoutAmt.Text = dataLoan.sProdCashoutAmt_rep;
            }

            sIOnlyMon.Text = dataLoan.sIOnlyMon_rep;
            sTransNetCashLckd.Checked = dataLoan.sTransNetCashLckd;
            
            sTransmTotMonPmt.Text = dataLoan.sTransmTotMonPmt_rep;
			
			sCurrentTotMonPmt.Text = dataLoan.sCurrentTotMonPmt_rep;
			sMonSavingPmt.Text = dataLoan.sMonSavingPmt_rep;

            this.RegisterJsGlobalVariables("IsRenovationLoan", dataLoan.sIsRenovationLoan);
            this.RegisterJsGlobalVariables("IsStandAloneSecond", dataLoan.sIsStandAlone2ndLien);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
