namespace LendersOfficeApp.newlos
{
    using System;
    using System.Web.UI.WebControls;
    using DataAccess;
    using DataAccess.Core.Construction;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Migration;
    using LendersOffice.Security;
    using LendersOfficeApp.newlos.Forms;

    public partial class LoanInfoUserControl : LendersOfficeApp.newlos.BaseLoanUserControl, IAutoLoadUserControl
    {
		protected bool m_IsRateLocked = false;
        internal static void BindDataFromControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem item)
        {
            QualRateCalculationPopup.BindData(loan, item);
        }

        internal static void LoadDataForControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem item)
        {
            QualRateCalculationPopup.LoadData(loan, item);
        }

        protected bool m_showCreditLineFields
        {
            get {
                // 10/22/2013 dd - Only show the Is Credit Line check box for broker with HELOC feature and Loan TEmplate
                //return broker.IsEnableHELOC && m_isTemplate;
                // 5/11/2014 dd - OPM 179885 - Allow the user to change the product type on a 2nd lien from HELOC to CLOSED-END or vice-versa
                return this.BrokerDB.IsEnableHELOC;
                //return BrokerUser.BrokerId == new Guid("e9f006c9-cb83-4531-8925-08fccf4add63")
                //    || BrokerUser.BrokerId == new Guid("D116B2B3-D3CA-4181-BAF6-B663815DB67D"); // David W Test (Dev)
            }
        }

        //ir OPM 169478 - Only show if broker bit is enabled
        protected bool m_showCRMID => this.BrokerDB.ShowCRMID && !m_isTemplate; //Loan Templates should not have visible CRM ID

        protected string ClosingCostPageUrl => this.BrokerDB.AreAutomatedClosingCostPagesVisible ?
            $"/los/Template/ClosingCost/ClosingCostTemplatePicker.aspx?sLId={LoanID:N}" :
            $"/los/View/ClosingCostList.aspx?loanid={LoanID:N}";

        protected string m_lineAmtDisplay => this.BrokerDB.IsEnableHELOC && sIsLineOfCredit.Checked ? string.Empty : "none";

        protected bool m_isInterestOnly;
		protected bool m_isPmlEnabled;
        protected bool m_cannotModifyLoanName;
        protected bool m_isTemplate;

        private void SetIsPMLEnable()
        {
            m_isPmlEnabled = BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
        }

		protected void PageInit(object sender, System.EventArgs e)
		{

            SetIsPMLEnable();
            Tools.Bind_sLienPosT(sLienPosT);
            Tools.Bind_sLPurposeT(sLPurposeT);
            Tools.Bind_sLT(sLT);
            Tools.Bind_sFinMethT(sFinMethT);
            Tools.Bind_aOccT(aOccT);
            Tools.Bind_sProRealETxT(sProRealETxT);
            Tools.Bind_PercentBaseT(sProHazInsT);
            Tools.Bind_sGseSpT(sGseSpT);
            Tools.Bind_QualTermCalculationType(sQualTermCalculationType);
            Tools.Bind_TriState(this.sHomeIsMhAdvantageTri);

            m_cannotModifyLoanName = !BrokerUser.HasPermission(Permission.CanModifyLoanName);
            BindLeadSource();
		}

        private void BindLeadSource()
        {
            BrokerDB Broker = this.BrokerDB;

            // 5/5/14 gf - opm 172380, always add a blank option for the default
            // value. We do not want the UI to display misleading data before the
            // page is saved because there are now pricing rules and fee service
            // rules which depend on this field.
            sLeadSrcDesc.Items.Add(new ListItem("", ""));

            foreach (LeadSource desc in Broker.LeadSources)
            {
                sLeadSrcDesc.Items.Add(new ListItem(desc.Name, desc.Name));
            }
        }

        public void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(LoanInfoUserControl));
            dataLoan.ByPassFieldSecurityCheck = true;
            dataLoan.InitLoad();
            m_isTemplate = dataLoan.IsTemplate;

            ((BasePage)Page).RegisterJsGlobalVariables("IsConstructionAndLotPurchase", 
                dataLoan.sConstructionPurposeT == ConstructionPurpose.ConstructionAndLotPurchase);

            ((BasePage)Page).RegisterJsGlobalVariables("AreConstructionLoanCalcsMigrated",
                LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges));

            ((BasePage)Page).RegisterJsGlobalVariables("IsConstructionPurposeRefi", dataLoan.sConstructionPurposeT.EqualsOneOf(ConstructionPurpose.ConstructionAndLotRefinance, ConstructionPurpose.ConstructionOnOwnedLot));

            this.QualRatePopup.LoadData(dataLoan);

            bool isRealEstateTaxVisible = !dataLoan.sRealEstateTaxExpenseInDisbMode;
            bool isHazInsVisible = !dataLoan.sHazardExpenseInDisbursementMode;

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            Tools.SetDropDownListValue(aOccT, dataApp.aOccT);
            Tools.SetDropDownListValue(sLT, dataLoan.sLT);
            Tools.SetDropDownListValue(sLienPosT, dataLoan.sLienPosT);
            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
            Tools.SetDropDownListValue(sProRealETxT, dataLoan.sProRealETxT);
            Tools.SetDropDownListValue(sProHazInsT, dataLoan.sProHazInsT);
            Tools.SetDropDownListValue(sGseSpT, dataLoan.sGseSpT);
            Tools.SetDropDownListValue(this.sHomeIsMhAdvantageTri, dataLoan.sHomeIsMhAdvantageTri);
			sFinMethT.Enabled = ! ( dataLoan.sIsRateLocked || IsReadOnly );

            // OPM 235326, ML, 1/14/2016
            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                MortgagePILink.HRef = "javascript:linkMe('Forms/LoanTerms.aspx');";
                MortgagePILink.Title = "Go to Loan Terms";
            }
            else
            {
                MortgagePILink.HRef = "javascript:linkMe('Forms/TruthInLending.aspx');";
                MortgagePILink.Title = "Go to Truth-In-Lending";
            }

            // 7/15/2005 dd - If current description no longer in description, just add to the drop downlist instead of clear out.
            if (dataLoan.sLeadSrcDesc.Length > 0 && sLeadSrcDesc.Items.FindByText(dataLoan.sLeadSrcDesc) == null)
                sLeadSrcDesc.Items.Add(new ListItem(dataLoan.sLeadSrcDesc, dataLoan.sLeadSrcDesc));

            Tools.SetDropDownListValue(sLeadSrcDesc, dataLoan.sLeadSrcDesc);


            sLNm.Text                    = dataLoan.sLNm;

            // 5/25/2005 dd - Disable sLNm either cannotModifyLoanName or Loan in ReadOnly.

            // 5/25/2005 kb - Per case 1058, we are enforcing locking
            // the loan name.  We shall put all permission controlled
            // visibility/access control to ui elements here.
            sLNm.ReadOnly = m_cannotModifyLoanName || IsReadOnly;
            if (m_cannotModifyLoanName)
                sLNm.ToolTip = "You do not have permission to modify the loan number.";

            sLRefNm.Text = dataLoan.sLRefNm;

            sCrmNowLeadId.Text           = dataLoan.sCrmNowLeadId;

            sLpTemplateNm.Text           = dataLoan.sLpTemplateNm;

			// 5/29/07 mf. OPM 12061. There is now a corporate setting for editing
			// the loan program name.
			if (! ( (LoanInfo) this.Parent.Page).CanEditLoanProgramName )
				sLpTemplateNm.ReadOnly = true;

            if (!this.BrokerDB.ShowLoanProductIdentifier)
            {
                this.sLoanProductIdentifierRow.Visible = false;
            }
            else
            {
                this.sLoanProductIdentifier.Text = dataLoan.sLoanProductIdentifier;
            }

            sCcTemplateNm.Text           = dataLoan.sCcTemplateNm;

            sIsStudentLoanCashoutRefi.Checked = dataLoan.sIsStudentLoanCashoutRefi;

            sFinMethDesc.Text            = dataLoan.sFinMethDesc;
            sPurchPrice.Text             = dataLoan.sPurchPrice_rep;
            sEquityCalc.Text             = dataLoan.sEquityCalc_rep;
            sDownPmtPc.Text              = dataLoan.sDownPmtPc_rep;

            sNoteIR.Text                 = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly             = dataLoan.sIsRateLocked || IsReadOnly;
            sQualIR.Text                 = dataLoan.sQualIR_rep;
            this.sQualIRLckd.Checked     = dataLoan.sQualIRLckd;

            sTerm.Text                   = dataLoan.sTerm_rep;
            sTerm.ReadOnly               = dataLoan.sIsRateLocked || IsReadOnly;
            sDue.Text                    = dataLoan.sDue_rep;
            sDue.ReadOnly                = dataLoan.sIsRateLocked || IsReadOnly;
            sApprVal.Text                = dataLoan.sApprVal_rep;
            sSpState.Value               = dataLoan.sSpState;
            sProRealETxMb.Text           = dataLoan.sProRealETxMb_rep;
            sProRealETxR.Text            = dataLoan.sProRealETxR_rep;
            sProRealETxBaseAmt.Text      = dataLoan.sProRealETxBaseAmt_rep;
            sProRealETxBaseAmtHolder.Visible = isRealEstateTaxVisible;
            sProRealETxTHolder.Visible = isRealEstateTaxVisible;
            sProRealETxMbHolder.Visible = isRealEstateTaxVisible;
            sProRealETxRHolder.Visible = isRealEstateTaxVisible;
            sProRealETx.Text = dataLoan.sProRealETx_rep;

            sLAmtCalc.Text               = dataLoan.sLAmtCalc_rep;
            sLAmtLckd.Checked            = dataLoan.sLAmtLckd;
            sFfUfmipFinanced.Text        = dataLoan.sFfUfmipFinanced_rep;
            sFinalLAmt.Text              = dataLoan.sFinalLAmt_rep;
            sMonthlyPmt.Text             = dataLoan.sMonthlyPmt_rep;
            sLTotI.Text                  = dataLoan.sLTotI_rep;
            sLiaMonLTot.Text             = dataLoan.sLiaMonLTot_rep;
            sPresLTotPersistentHExp.Text = dataLoan.sPresLTotPersistentHExp_rep;
            sProThisMPmt.Text            = dataLoan.sProThisMPmt_rep;
            sProOFinPmt.Text             = dataLoan.sProOFinPmt_rep;

            sProHazInsBaseAmt.Text       = dataLoan.sProHazInsBaseAmt_rep;
            sProHazInsR.Text             = dataLoan.sProHazInsR_rep;
            sProHazInsMb.Text            = dataLoan.sProHazInsMb_rep;
            sProHazIns.Text              = dataLoan.sProHazIns_rep;
            sProHazInsBaseAmtHolder.Visible = isHazInsVisible;
            sProHazInsMbHolder.Visible = isHazInsVisible;
            sProHazInsRHolder.Visible = isHazInsVisible;
            sProHazInsTHolder.Visible = isHazInsVisible;

            sProMIns.Text                = dataLoan.sProMIns_rep;
            sProMInsLckd.Checked = dataLoan.sProMInsLckd;
            sProHoAssocDues.Text         = dataLoan.sProHoAssocDues_rep;
            sProHoAssocDues.ReadOnly = dataLoan.sHoaDuesExpenseInDisbMode;
            sProOHExp.Text               = dataLoan.sProOHExp_rep;
            sProOHExpLckd.Checked = dataLoan.sProOHExpLckd;
            sLtvR.Text                   = dataLoan.sLtvR_rep;
            sCltvR.Text                  = dataLoan.sCltvR_rep;
            sQualTopR.Text               = dataLoan.sQualTopR_rep;
            sQualBottomR.Text            = dataLoan.sQualBottomR_rep;

			sOptionArmTeaserR.Text       = dataLoan.sOptionArmTeaserR_rep;
			sOptionArmTeaserR.ReadOnly   = dataLoan.sIsRateLocked || IsReadOnly;
			sIsOptionArm.Checked         = dataLoan.sIsOptionArm;
			sIsOptionArm.Enabled		 = !(dataLoan.sIsRateLocked || IsReadOnly);
			sIsQRateIOnly.Checked        = dataLoan.sIsQRateIOnly;
            sOriginalAppraisedValue.Text = dataLoan.sOriginalAppraisedValue_rep;
            sFHAPurposeIsStreamlineRefiWithoutAppr.Value = dataLoan.sFHAPurposeIsStreamlineRefiWithoutAppr ? "1" : "0";

            m_IsRateLocked = dataLoan.sIsRateLocked;
            sIsEmployeeLoan.Checked = dataLoan.sIsEmployeeLoan;

            sIsNewConstruction.Checked = dataLoan.sIsNewConstruction;

            sHighPricedMortgageTLckd.Checked = dataLoan.sHighPricedMortgageTLckd;

            Tools.Bind_sHighPricedMortgageT(sHighPricedMortgageT);
            Tools.SetDropDownListValue(sHighPricedMortgageT, dataLoan.sHighPricedMortgageT);

            sIsLineOfCredit.Checked = dataLoan.sIsLineOfCredit;
            sCreditLineAmt.Text = dataLoan.sCreditLineAmt_rep;

            // 6/4/2004 dd - Kevin Bagley requested display Interest Only indicator when sIOnlyMon is not zero.
            m_isInterestOnly = dataLoan.sIOnlyMon_rep != "0";
            sIsIOnlyForSubFin.Visible = dataLoan.sIsIOnlyForSubFin;
            sConstructionPeriodMon.Text = dataLoan.sConstructionPeriodMon_rep;
            sConstructionPeriodIR.Text = dataLoan.sConstructionPeriodIR_rep;
            sConstructionImprovementAmt.Text = dataLoan.sConstructionImprovementAmt_rep;
            sLandCost.Text = dataLoan.sLandCost_rep;

            Tools.SetDropDownListValue(this.sQualTermCalculationType, dataLoan.sQualTermCalculationType);
            this.sQualTerm.Value = dataLoan.sQualTerm_rep;

            #region OPM 3297. Add details of other loan next to "Other Financing"
            if (dataLoan.sLienPosT == E_sLienPosT.First)
            {
                // 12/7/2005 dd - Only display the 2nd loan information if current loan is first lien.
                if (dataLoan.sConcurSubFin_rep != "$0.00" && dataLoan.sSubFinIR_rep != "0.000%" && dataLoan.sSubFinPmtLckd == false)
                {
                    OtherFinancingDescription.Text = string.Format("({0} @ {1})", dataLoan.sConcurSubFin_rep, dataLoan.sSubFinIR_rep);
                }
            }

            #endregion

            LoanValueEvaluator evaluator = new LoanValueEvaluator(BrokerUser.ConnectionInfo, BrokerUser.BrokerId, LoanID, WorkflowOperations.UseOldSecurityModelForPml, WorkflowOperations.RunPmlToStep3, WorkflowOperations.RunPmlForAllLoans, WorkflowOperations.RunPmlForRegisteredLoans);
            evaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(BrokerUser));
            bool useOldEngine = LendingQBExecutingEngine.CanPerform(WorkflowOperations.UseOldSecurityModelForPml, evaluator);

            btnRunLPE.Disabled = useOldEngine
                ? IsReadOnly
                : !LendingQBExecutingEngine.CanPerformAny(evaluator, WorkflowOperations.RunPmlToStep3, WorkflowOperations.RunPmlForRegisteredLoans, WorkflowOperations.RunPmlForAllLoans);

            sDownOrEquityLabel  .InnerText = dataLoan.sIsRefinancing ? "Equity"     : "Down Payment"    ;
            sDownOrEquityPcLabel.InnerText = dataLoan.sIsRefinancing ? "Equity (%)" : "Down Payment (%)";

            this.Page.ClientScript.RegisterHiddenField("IsEnableRenovationCheckboxInPML", this.BrokerDB.IsEnableRenovationCheckboxInPML.ToString());
            this.Page.ClientScript.RegisterHiddenField("EnableRenovationLoanSupport", this.BrokerDB.EnableRenovationLoanSupport.ToString());

            sIsRenovationLoan.Checked = dataLoan.sIsRenovationLoan;

            if (dataLoan.sIsRenovationLoan)
            {
                sTotalRenovationCostsLckd.Checked = dataLoan.sTotalRenovationCostsLckd;
                sFHASpAsIsVal.Text = dataLoan.sFHASpAsIsVal_rep;
                sTotalRenovationCosts.Text = dataLoan.sTotalRenovationCosts_rep;
                sInducementPurchPrice.Text = dataLoan.sInducementPurchPrice_rep;
            }
        }

        public void SaveData()
        {

        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion

	}
}
