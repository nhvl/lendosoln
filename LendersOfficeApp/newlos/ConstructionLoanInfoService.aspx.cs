﻿namespace LendersOfficeApp.newlos
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using DataAccess.Core.Construction;
    using LendersOffice.Migration;
    using LendersOffice.Security;

    public partial class ConstructionLoanInfoService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch(methodName)
            {
                case "SaveData":
                    Save();
                    break;
                case "calculateTotal":
                    calculateTotal();
                    break;
                case "CalculateData":
                    CalculateData();
                    break;
                case "GetAprDebugText":
                    GetAprDebugText();
                    break;
            }
        }

        public void BindData(CPageData dataLoan)
        {

            dataLoan.sBuildingStatusT = this.GetEnum<E_sBuildingStatusT>("sBuildingStatusT");
            dataLoan.sConstructionAmortT = this.GetEnum<E_sFinMethT>("sConstructionAmortT");
            dataLoan.sConstructionIntCalcT = this.GetEnum<ConstructionIntCalcType>("sConstructionIntCalcT");
            dataLoan.sConstructionMethodT = this.GetEnum<ConstructionMethod>("sConstructionMethodT");
            dataLoan.sConstructionPhaseIntAccrualT = this.GetEnum<ConstructionPhaseIntAccrual>("sConstructionPhaseIntAccrualT");
            dataLoan.sConstructionPurposeT = this.GetEnum<ConstructionPurpose>("sConstructionPurposeT");
            if (dataLoan.sConstructionPurposeT != ConstructionPurpose.ConstructionAndLotPurchase)
            {
                dataLoan.sLotOwnerT = LotOwnerType.Borrower;
            }
            else
            {
                if(dataLoan.sHasLienLiabilities)
                {
                    throw new CBaseException("There are existing Liens on the loan. You must delete them before changing the purpose to 'Construction and Lot Purchase'", "There are existing Liens on the loan. You must delete them before changing the purpose to 'Construction and Lot Purchase'");
                }
                dataLoan.sLotOwnerT = this.GetEnum<LotOwnerType>("sLotOwnerT");
            }

            dataLoan.sConstructionPeriodMon = this.GetInt("sConstructionPeriodMon");
            dataLoan.sConstructionPeriodIR_rep = this.GetString("sConstructionPeriodIR");

            dataLoan.sLotImprovC_rep = this.GetString("sLotImprovC");

            if (dataLoan.sConstructionPurposeT == ConstructionPurpose.ConstructionAndLotPurchase)
            {
                dataLoan.sLandCost_rep = this.GetString("sLandCost");
            }
            else
            {
                dataLoan.sLotVal_rep = this.GetString("sLotVal");
            }

            dataLoan.sSubsequentlyPaidFinanceChargeAmt_rep = this.GetString("sSubsequentlyPaidFinanceChargeAmt");
            dataLoan.sConstructionInitialAdvanceAmt_rep = this.GetString("sConstructionInitialAdvanceAmt");
            dataLoan.sConstructionLoanDLckd = this.GetBool("sConstructionLoanDLckd");
            dataLoan.sConstructionLoanD_rep = this.GetString("sConstructionLoanD");
            dataLoan.sConstructionIntAccrualDLckd = this.GetBool("sConstructionIntAccrualDLckd");
            dataLoan.sConstructionIntAccrualD_rep = this.GetString("sConstructionIntAccrualD");
            dataLoan.sConstructionFirstPaymentDLckd = this.GetBool("sConstructionFirstPaymentDLckd");
            dataLoan.sConstructionFirstPaymentD_rep = this.GetString("sConstructionFirstPaymentD");
            dataLoan.sIsIntReserveRequired = this.GetBool("sIsIntReserveRequired");
            dataLoan.sIntReserveAmt_rep = this.GetString("sIntReserveAmt");

            //Arm Section
            if (dataLoan.sConstructionAmortT == E_sFinMethT.ARM)
            {
                dataLoan.sConstructionRAdj1stCapR_rep = this.GetString("sConstructionRAdj1stCapR");
                dataLoan.sConstructionRAdj1stCapMon_rep = this.GetString("sConstructionRAdj1stCapMon");
                dataLoan.sConstructionRAdjCapR_rep = this.GetString("sConstructionRAdjCapR");
                dataLoan.sConstructionRAdjCapMon_rep = this.GetString("sConstructionRAdjCapMon");
                dataLoan.sConstructionRAdjLifeCapR_rep = this.GetString("sConstructionRAdjLifeCapR");
                dataLoan.sConstructionRAdjMarginR_rep = this.GetString("sConstructionRAdjMarginR");
                dataLoan.sConstructionRAdjIndexR_rep = this.GetString("sConstructionRAdjIndexR");
                dataLoan.sConstructionRAdjFloorR_rep = this.GetString("sConstructionRAdjFloorR");
                dataLoan.sConstructionRAdjRoundToR_rep = this.GetString("sConstructionRAdjRoundToR");
                dataLoan.sConstructionRAdjRoundT = this.GetEnum<E_sRAdjRoundT>("sConstructionRAdjRoundT");
                dataLoan.sConstructionArmIndexNameVstr = this.GetString("sConstructionArmIndexNameVstr");

                // From RateFloorPopup
                dataLoan.sConstructionRAdjFloorCalcT = GetEnum<RateAdjFloorCalcT>("sRAdjFloorCalcT");
                dataLoan.sConstructionRAdjFloorAddR_rep = GetString("sRAdjFloorAddR");
                dataLoan.sConstructionRAdjFloorR_rep = GetString("sRAdjFloorR");
            }

        }

        public void LoadData(CPageData dataLoan)
        {
            SetResult("sBuildingStatusT", dataLoan.sBuildingStatusT);
            SetResult("sConstructionIntCalcT", dataLoan.sConstructionIntCalcT);
            SetResult("sConstructionMethodT", dataLoan.sConstructionMethodT);
            SetResult("sConstructionPhaseIntAccrualT", dataLoan.sConstructionPhaseIntAccrualT);
            SetResult("sConstructionPurposeT", dataLoan.sConstructionPurposeT);
            SetResult("sLotOwnerT", dataLoan.sLotOwnerT);
            SetResult("sConstructionRAdjRoundT", dataLoan.sConstructionRAdjRoundT);
            SetResult("sConstructionAmortT", dataLoan.sConstructionAmortT);
            SetResult("sConstructionPeriodMon", dataLoan.sConstructionPeriodMon);
            SetResult("sConstructionPeriodIR", dataLoan.sConstructionPeriodIR_rep);
            SetResult("sLotImprovC", dataLoan.sLotImprovC_rep);
            SetResult("sLandCost", dataLoan.sLandCost_rep);
            SetResult("sLotVal", dataLoan.sLotVal_rep);
            SetResult("sSubsequentlyPaidFinanceChargeAmt", dataLoan.sSubsequentlyPaidFinanceChargeAmt_rep);
            SetResult("sConstructionInitialAdvanceAmt", dataLoan.sConstructionInitialAdvanceAmt_rep);
            SetResult("sConstructionLoanDLckd", dataLoan.sConstructionLoanDLckd);
            SetResult("sConstructionLoanD", dataLoan.sConstructionLoanD_rep);
            SetResult("sConstructionIntAccrualDLckd", dataLoan.sConstructionIntAccrualDLckd);
            SetResult("sConstructionIntAccrualD", dataLoan.sConstructionIntAccrualD_rep);
            SetResult("sConstructionFirstPaymentDLckd", dataLoan.sConstructionFirstPaymentDLckd);
            SetResult("sConstructionFirstPaymentD", dataLoan.sConstructionFirstPaymentD_rep);
            SetResult("sIsIntReserveRequired", dataLoan.sIsIntReserveRequired);
            SetResult("sIntReserveAmt", dataLoan.sIntReserveAmt_rep);
            this.SetResult("sConstructionIntAmount", dataLoan.sConstructionIntAmount_rep);
            SetResult("sApr", dataLoan.sApr_rep);

            //Arm Section
            if (dataLoan.sConstructionAmortT == E_sFinMethT.ARM)
            {
                SetResult("sConstructionRAdj1stCapR", dataLoan.sConstructionRAdj1stCapR_rep);
                SetResult("sConstructionRAdj1stCapMon", dataLoan.sConstructionRAdj1stCapMon_rep);
                SetResult("sConstructionRAdjCapR", dataLoan.sConstructionRAdjCapR_rep);
                SetResult("sConstructionRAdjCapMon", dataLoan.sConstructionRAdjCapMon_rep);
                SetResult("sConstructionRAdjLifeCapR", dataLoan.sConstructionRAdjLifeCapR_rep);
                SetResult("sConstructionRAdjMarginR", dataLoan.sConstructionRAdjMarginR_rep);
                SetResult("sConstructionRAdjIndexR", dataLoan.sConstructionRAdjIndexR_rep);
                SetResult("sConstructionRAdjFloorR", dataLoan.sConstructionRAdjFloorR_rep);
                SetResult("sConstructionRAdjRoundToR", dataLoan.sConstructionRAdjRoundToR_rep);
                SetResult("sConstructionArmIndexNameVstr", dataLoan.sConstructionArmIndexNameVstr);
            }

            // For RateFloorPopup
            SetResult("sRAdjFloorCalcT", dataLoan.sConstructionRAdjFloorCalcT);
            SetResult("sRAdjFloorCalcT_hidden", Tools.GetsRAdjFloorCalcTLabel(dataLoan.sConstructionRAdjFloorCalcT));
            SetResult("sRAdjFloorBaseR", dataLoan.sConstructionRAdjFloorBaseR_rep);
            SetResult("sRAdjFloorAddR", dataLoan.sConstructionRAdjFloorAddR_rep);
            SetResult("sRAdjFloorTotalR", dataLoan.sConstructionRAdjFloorTotalR_rep);
            SetResult("sRAdjFloorLifeCapR", dataLoan.sConstructionRAdjFloorLifeCapR_rep);
            SetResult("sIsRAdjFloorRReadOnly", dataLoan.sIsConstructionRAdjFloorRReadOnly.ToString());
            SetResult("sRAdjFloorR", dataLoan.sConstructionRAdjFloorR_rep);
            SetResult("sRAdjFloorR_readonly", dataLoan.sConstructionRAdjFloorR_rep);
            SetResult("sNoteIR", dataLoan.sConstructionPeriodIR_rep);
            SetResult("sNoteIR_popup", dataLoan.sConstructionPeriodIR_rep);
            SetResult("sRAdjLifeCapR", dataLoan.sConstructionRAdjLifeCapR_rep);
            SetResult("sRAdjLifeCapR_popup", dataLoan.sConstructionRAdjLifeCapR_rep);

            if (dataLoan.sConstructionPhaseIntAccrualT == ConstructionPhaseIntAccrual.Monthly_360_360)
            {
                if (dataLoan.sConstructionIntAccrualD.GetSafeDateTimeForComputation(DateTime.MinValue) != dataLoan.sConstructionLoanD.GetSafeDateTimeForComputation(DateTime.MinValue))
                {
                    SetResult("dateWarning", "If Interest Accrual is set to Monthly (360/360), then the Interest Accrual Date must be equal to the Construction Loan Date or else the APR will not calculate.");
                }
                else
                {
                    SetResult("dateWarning", string.Empty);
                }
            }
        }

        public void CalculateData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(GetGuid("loanid"), typeof(ConstructionLoanInfoService));
            dataLoan.InitLoad();
            BindData(dataLoan);
            LoadData(dataLoan);
        }

        public void calculateTotal()
        {
            decimal percent = GetRate("percent");
            decimal fixedAmount = GetMoney("fixedAmount");
            int basis = GetInt("basis");
            decimal basisValue = 0;


            CPageData dataLoan = CreatePageData(GetGuid("LoanID"));
            dataLoan.InitLoad();

            switch((E_PercentBaseT)basis)
            {
                case E_PercentBaseT.LoanAmount:
                    basisValue = dataLoan.sLAmtCalc;
                    break;
                case E_PercentBaseT.SalesPrice:
                    basisValue = dataLoan.sPurchPrice;
                    break;
                case E_PercentBaseT.AppraisalValue:
                    basisValue = dataLoan.sApprVal;
                    break;
                case E_PercentBaseT.OriginalCost:
                    basisValue = dataLoan.sLotOrigC;
                    break;
                case E_PercentBaseT.TotalLoanAmount:
                    basisValue = dataLoan.sFinalLAmt;
                    break;

            }

            decimal total = basisValue * percent / 100 + fixedAmount;

            SetResult("total", total);
        }



        protected BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private CPageData CreatePageData(Guid loanID)
        {
            return CPageData.CreateUsingSmartDependency(loanID, typeof(ConstructionLoanInfoService));
        }

        private void Save()
        {
            Guid loanID = GetGuid("LoanID");


            string serializedDrawSchedules = GetString("Draws");
            List<DrawSchedule> drawSchedules = LendersOffice.Common.ObsoleteSerializationHelper.JsonDeserialize<List<DrawSchedule>>(serializedDrawSchedules);

            CPageData pageData = CreatePageData(loanID);
            pageData.InitSave(GetInt("sFileVersion"));
            BindData(pageData);
            LoadData(pageData);
            pageData.sDrawSchedules = drawSchedules;
            pageData.Save();

        }

        private void GetAprDebugText()
        {
            Guid loanId = GetGuid("loanid");
            CPageData dataLoan = CreatePageData(loanId);
            dataLoan.InitLoad();

            // Make sure we get debug results with up to date info from the page
            // (ie. the page might be dirty, and we don't want to force a save at this point).
            BindData(dataLoan);

            SetResult("sAprDebugResult", dataLoan.sAprDebugResult);
        }
    }
}
