﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StyleEditor.aspx.cs" Inherits="LendersOfficeApp.newlos.Prototype.StyleEditor" EnableViewState="false" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" ng-app="StyleEditor">
<head runat="server">
    <meta charset='utf-8' />
    <title></title>


    <script src="../../inc/jquery-2.1.4.min.js"></script>
    <script src="../../inc/bootstrap-3.3.4.min.js"></script>
    <script src="../../inc/ThirdParty/ractive-legacy-0.6.1.min.js"></script>
    <link rel="stylesheet" href="../../css/bootstrap.min.css" />
    <link rel="stylesheet" href="../../css/Prototype/SASS/MasterSCSS.css" />

    <style>
        body
        {
            background-color: #EEE;
        }

        .children-header
        {
            font-size: 13px;
            font-weight: 600;
        }

        .friendly-description
        {
            font-weight: bold;
            font-size: 16px;
        }

        table .header
        {
            font-size: 14px;
        }

        input
        {
            min-width: 100px;
        }
    </style>
    <script>
        
        
    </script>

</head>
<body>
    <form id="Form1" runat="server">

        <script id="MainTemplate" type="text/ractive">
                    
            <h6>Variables</h6>
            {{#each variables: key}}
                <div>
                    <span>{{key}}</span>
                    <input type="text" value="{{this}}" />
                </div>
            {{/each}}
                <br/>
                <div>
                    <input id="varKey" placeholder="$variable name" type="text" />
                    <input id="varValue" placeholder="variable value" type="text" />
                    <button on-click='addVariable'>Add</button>
                </div>
            <br/>

        {{#each elements}}
            {{>ElementTemplate}}
        {{/each}}
        </script>

        <script id="ElementTemplate" type="text/ractive">
        <div>
            <span class='friendly-description'>
                {{friendlyDescription}}
            </span>
            <div style="margin-left: 50px;">
                {{#each states: state}}
                        {{state}}
                            <div style='display: table; width: 80%; margin-left: 50px;'>
                                {{#each this: key}}
                                    {{#if key == 'mixin'}}
                                    <div style='display: table-cell'>
                                        <div>
                                            {{key}}
                                        </div>
                                        <div>
                                            <input type='text' style='width: 100%;' class='form-control' value='{{this}}'/>
                                        </div>
                                    </div>
                                    {{/if}}
                                {{/each}}
                            </div>
                            <div style='display: table; margin-left: 50px;'>
                                {{#each this: key}}
                                    {{#if key != 'mixin'}}
                                    <div style='display: table-cell'>
                                        <div>
                                            {{key}}
                                        </div>
                                        <div>
                                            <input type='text' class='form-control' value='{{this}}'/>
                                        </div>
                                    </div>
                                    {{/if}}
                                {{/each}}
                            </div>
                {{/each}}
                
        {{#if children.length > 0}}
        <span class='children-header'>
            Children:
        </span>
        <div style="margin-left: 50px;">
                {{#each children}}
                    {{>ElementTemplate}}
                {{/each}}
        </div>

        {{/if}}

            </div>
        </div>
        </script>

        <script>

            var ractive;
            $(window).ready(function () {
                var JSON = $("#elements").val();
                var dynamicInfo = $("#dynamicInfo").val();
                var variables = $("#variables").val();
                ractive = new Ractive({
                    el: 'container',
                    append: false,
                    template: '#MainTemplate',
                    partials: '#ElementTemplate',
                    data: { elements: $.parseJSON(JSON), DynamicInfo: dynamicInfo, variables: $.parseJSON(variables) }
                });

                ractive.on('addVariable', function () {
                    var key = $("#varKey").val();
                    $("#varKey").val("");
                    var value = $("#varValue").val();
                    $("#varValue").val("");

                    if (key.indexOf("$") != 0) {
                        key = "$" + key;
                    }

                    ractive.data.variables[key] = value;
                    ractive.update();
                    return false;
                });


                $(".save").click(GenerateSASS);
                $(".clear").click(ClearSass);
            });

            function GenerateSASS() {
                var cssJSON = JSON.stringify(ractive.data.elements);
                var variablesJson = JSON.stringify(ractive.data.variables);
                
                var args = {
                    cssJSON: cssJSON,
                    variablesJson: variablesJson
                };
                var result = gService.main.call("GenerateSass", args);
            }

            function ClearSass() {
                var args = {};
                var result = gService.main.call("ClearSass", args);
            }
        </script>

        <h1>Style Editor</h1>
        <div>
            <button class="save" onclick="return false;">Save</button>
            <button class="clear" onclick="return false;">Clear SASS</button>
        </div>

        <div id="container"></div>

        <div>
            <button class="save" onclick="return false;">Save</button>
            <button class="clear" onclick="return false;">Clear SASS</button>
        </div>

    </form>
</body>
</html>
