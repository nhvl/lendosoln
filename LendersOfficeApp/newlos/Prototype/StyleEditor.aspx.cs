﻿namespace LendersOfficeApp.newlos.Prototype
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.ObjLib.UI.Themes;

    public partial class StyleEditor : BaseServicePage
    {
        [DataContract]
        public class CSSElement
        {


            [DataMember]
            public string friendlyDescription;
            [DataMember]
            public string className;

            public CSSElement parent = null;

            [DataMember]
            public List<CSSElement> children = new List<CSSElement>();

            public static Dictionary<string, string> PropsList = new Dictionary<string, string>(){
                {"mixin", ""},
                {"background-color", ""},
                {"border", ""},
                {"border-color", ""},
                {"border-spacing", ""},
                {"box-shadow", ""},
                {"padding", ""},
                {"margin", ""},
                {"color", ""},
                {"font-size", ""},
            };

            [DataMember]
            public Dictionary<string, Dictionary<string, string>> states = new Dictionary<string, Dictionary<string, string>>() { };

            public CSSElement(string description, string className, string validStates, List<CSSElement> children)
            {

                this.friendlyDescription = description;
                this.className = className;
                this.children = children;
                states.Add("default", PropsList.ToDictionary(p => p.Key, p => p.Value));
                if (validStates != null && !String.IsNullOrEmpty(validStates.Trim()))
                {
                    string[] validStatesArr = validStates.Split(' ');
                    foreach (string val in validStatesArr)
                    {
                        states.Add(val, PropsList.ToDictionary(p => p.Key, p => p.Value));
                    }
                }
            }

            public void Add(CSSElement child)
            {
                children.Add(child);
            }
        }

        public List<CSSElement> elements = new List<CSSElement>();

        protected void Page_Load(object sender, EventArgs e)
        {

            Response.AddHeader("X-UA-Compatible", "IE=edge");
            this.RegisterService("main", "/newlos/Prototype/StyleEditorService.aspx");

            List<CSSElement> children;
            // Standalone elements are elements that do not have parent values in LeTheme.scss.  
            // We need to know which elements they are so we can check for them within the correct scope.
            IEnumerable<CSSElement> standaloneEl = new List<CSSElement>();

            CSSElement logo = new CSSElement("Logo area", ".header-logo", "", null);

            children = new List<CSSElement>() { 
                new CSSElement("Title", ".page-header", "", null),
                new CSSElement("Icon", ".glyphicon", "", null),
                new CSSElement("Flat button", ".btn-flat", "hover active", null),
                new CSSElement("Applicant ddl", ".applicants-ddl-btn", "", null)};
            standaloneEl = standaloneEl.Concat(children);
            CSSElement header = new CSSElement("Header", ".header-main", "", children);

            CSSElement loanSummary = new CSSElement("Loan summary", ".loan-summary", "", null);

            children = new List<CSSElement>() { 
                new CSSElement("Folder", ".folder", "", null),
                new CSSElement("Page", "a:not(.folder)", "", null)};
            CSSElement navigation = new CSSElement("Navigation", ".sidebar-navigation", "", children);

            CSSElement content = new CSSElement("Content", ".content", "", null);

            children = new List<CSSElement>() { 
                new CSSElement("Title", ".panel-heading", "", null),
                new CSSElement("Content", ".panel-body", "", null)};
            CSSElement section = new CSSElement("Section", ".section", "", children);

            CSSElement buttons = new CSSElement("Buttons", "button", "hover active", null);
            CSSElement btnDefault = new CSSElement("Default/Secondary Buttons", ".btn-default", "hover active", null);
            CSSElement btnPrimary = new CSSElement("Primary Buttons", ".btn-primary", "hover active", null);

            CSSElement input = new CSSElement("Input", "input", "focus disabled", null);

            elements.Add(logo);
            elements.Add(header);
            elements.Add(loanSummary);
            elements.Add(navigation);
            elements.Add(content);
            elements.Add(section);
            elements.Add(buttons);
            elements.Add(btnDefault);
            elements.Add(btnPrimary);
            elements.Add(input);

            standaloneEl = standaloneEl.Concat(elements);

            //Take in the values from LETheme.scss;

            string filePath = Tools.GetServerMapPath(Tools.VRoot + "/css/Prototype/SASS/LoanEditorTheme.scss");

            var fileLines = TextFileHelper.ReadLines(filePath);

            SetProperties(standaloneEl, fileLines);

            //Take in the values from the current Custom CSS
            filePath = Tools.GetServerMapPath(Tools.VRoot + "/css/Prototype/SASS/CustomSCSS.scss");

            if (FileOperationHelper.Exists(filePath))
            {
                fileLines = TextFileHelper.ReadLines(filePath);

                SetProperties(elements, fileLines);
            }

            //Take in the values from the current Custom CSS
            filePath = Tools.GetServerMapPath(Tools.VRoot + "/css/Prototype/SASS/MasterVariables.scss");

            Dictionary<string, string> masterVariableDictionary = null;
            if (FileOperationHelper.Exists(filePath))
            {
                fileLines = TextFileHelper.ReadLines(filePath);

                masterVariableDictionary = SassUtilities.CreateVariableDictionary(fileLines, removeDefaultKeyword: false);
            }

            //Take in the values from the current Custom CSS
            filePath = Tools.GetServerMapPath(Tools.VRoot + "/css/Prototype/SASS/CustomVariables.scss");

            Dictionary<string, string> customVariableDictionary = null;
            if (FileOperationHelper.Exists(filePath))
            {
                fileLines = TextFileHelper.ReadLines(filePath);

                customVariableDictionary = SassUtilities.CreateVariableDictionary(fileLines, removeDefaultKeyword: false);
            }

            var variables = masterVariableDictionary ?? new Dictionary<string, string>();
            if (customVariableDictionary != null)
            {
                foreach (var kvp in customVariableDictionary)
                {
                    if (!variables.ContainsKey(kvp.Key))
                    {
                        variables.Add(kvp.Key, kvp.Value);
                    }
                    else
                    {
                        variables[kvp.Key] = kvp.Value;
                    }
                }
            }

            ClientScript.RegisterHiddenField("elements", SerializationHelper.JsonNetSerialize(elements));
            ClientScript.RegisterHiddenField("variables", SerializationHelper.JsonNetSerialize(variables));
        }

        public void SetProperties( IEnumerable<CSSElement> standalone, IEnumerable<string> lines)
        {
            string selector = "";
            IEnumerable<CSSElement> scope = standalone;
            CSSElement el = null;
            bool recentlySet = false;
            bool ignoreEl = false;
            string state = "default";

            List<string> pseudoSelectors = new List<string>(){""};

            foreach (string line in lines)
            {
                string lineT = line.Trim().Replace("!important", "");
                if (lineT.Contains("{"))
                {
                    if (!String.IsNullOrEmpty(selector))
                    {
                        recentlySet = false;
                        // Found the beginning of a selector's properties
                        //A selector may be many combined into one.
                        string[] selectors = selector.Split(',');

                        if (scope != null)
                        {
                            foreach (string possibleSelector in selectors)
                            {
                                CSSElement possibleEl = scope.FirstOrDefault(p => p.className.Equals(possibleSelector));
                                if (possibleEl != null)
                                {
                                    possibleEl.parent = el;
                                    el = possibleEl;
                                    recentlySet = true;
                                    ignoreEl = false;
                                    scope = el.children;
                                }
                                else if (!recentlySet)
                                {
                                    ignoreEl = true;
                                }
                            }
                        }
                        else
                        {
                            ignoreEl = true;
                        }
                    }
                }
                else
                {
                    selector = "";
                    if (lineT.Contains("}"))
                    {
                        // Found the end of a selector's properties
                        if (state.Equals("default"))
                        {
                            // If we finished reading a configurable child, switch back to the parent.
                            if (el != null)
                            {
                                el = el.parent;
                                scope = el != null ? el.children : standalone;
                            }
                        }
                        else if (!state.Equals("default"))
                        {
                            state = "default";
                        }

                        // Stop ignoring code.
                        ignoreEl = false;
                    }
                    if (!ignoreEl)
                    {
                        if (el != null)
                        {
                            if (lineT.Contains("&:"))
                            {
                                // We found a state selector.
                                string[] possibleSelectors = lineT.Split(',');

                                foreach (string possibleSelector in possibleSelectors)
                                {
                                    if (possibleSelector.Contains("&:"))
                                    {
                                        state = possibleSelector.Replace("&:", "");
                                    }
                                }
                            }
                            else if (lineT.Contains("@") && !lineT.Contains("@import"))
                            {
                                var mixinVal = el.states[state]["mixin"];
                                if(string.IsNullOrEmpty(mixinVal) || !mixinVal.Contains(lineT.Trim()))
                                {
                                    el.states[state]["mixin"] += " " + lineT.Trim();
                                }
                            }
                            else if (lineT.Contains(":"))
                            {
                                if (!lineT.Contains(";"))
                                {
                                    //If it does not contain a semi-colon, then it is a selector
                                    selector = lineT;
                                    continue;
                                }

                                // We found a property, but it could also stand for a pseudo selector...  (Pseudo's are ignored since it's not in the list of properties though.
                                string[] property = lineT.Split(':');
                                string propertyName = property[0];
                                string propertyVal = property[1];


                                if (CSSElement.PropsList.Keys.Contains(propertyName))
                                {
                                    Tools.LogError(propertyName + ", " + state);
                                    el.states[state][propertyName] = propertyVal.Replace(";", "").Trim();
                                }
                            }
                            else
                            {
                                selector = lineT;
                            }
                        }
                        else
                        {
                            // We found a selector
                            selector = lineT;
                        }
                    }
                }

            }
        }
    }
}