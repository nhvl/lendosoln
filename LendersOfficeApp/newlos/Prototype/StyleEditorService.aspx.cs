﻿namespace LendersOfficeApp.newlos.Prototype
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;
    using NSass;

    public partial class StyleEditorService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch(methodName)
            {
                case "GenerateSass":
                    GenerateSass();
                    break;
                case "ClearSass":
                    ClearSass();
                    break;
                default:
                    throw new Exception("Invalid method name");
            }
        }

        
        public void GenerateSass()
        {
            string cssFolder = Tools.GetServerMapPath(Tools.VRoot + "/css/Prototype/SASS/");
            string SassFile = cssFolder + "CustomSCSS.scss";
            string CssFile = cssFolder + "CustomSCSS.css";
            string variablesFile = cssFolder + "CustomVariables.scss";

            Dictionary<string, string> variables = SerializationHelper.JsonNetDeserialize<Dictionary<string, string>>(GetString("variablesJSON"));
            GenerateVariablesSass(variables, variablesFile);

            List<StyleEditor.CSSElement> elements;
            var cssJSON = GetString("cssJSON");
            elements = SerializationHelper.JsonNetDeserialize<List<StyleEditor.CSSElement>>(cssJSON);

            StringBuilder sb = new StringBuilder();

            sb.AppendLine();
            sb.AppendLine("@import \"MasterVariables\";");
            sb.AppendLine("@import \"CustomVariables\";");
            sb.AppendLine("@import \"MasterSCSS\";");

            foreach (StyleEditor.CSSElement element in elements)
            {
                GenerateElementSass(element, sb);
            }

            ISassCompiler compiler = new SassCompiler();
            TextFileHelper.WriteString(SassFile, sb.ToString(), false);

            string sass = compiler.CompileFile(SassFile);

            TextFileHelper.WriteString(CssFile, sass, false);
            SetResult("Success", true);


            string LETheme = compiler.CompileFile(cssFolder + "LoanEditorTheme.scss", OutputStyle.Nested, false, null);
            TextFileHelper.WriteString(cssFolder + "LoanEditorTheme.css", LETheme, false);

            string DarkTheme = compiler.CompileFile(cssFolder + "DarkTheme.scss", OutputStyle.Nested, false, null);
            TextFileHelper.WriteString(cssFolder + "DarkTheme.css", DarkTheme, false);
        }

        public void GenerateVariablesSass(Dictionary<string, string> variables, string directory)
        {
            StringBuilder sb = new StringBuilder();

            foreach (string key in variables.Keys)
            {
                string value = "";
                if (variables.TryGetValue(key, out value))
                {
                    sb.AppendLine(key + ": " + value + ";");
                }
            }

            TextFileHelper.WriteString(directory, sb.ToString(), false);
        }

        private void ClearSass()
        {
            string cssFolder = Tools.GetServerMapPath(Tools.VRoot + "/css/Prototype/SASS/");
            string SassFile = cssFolder + "CustomSCSS.scss";
            string CssFile = cssFolder + "CustomSCSS.css";
            string variablesFile = cssFolder + "CustomVariables.scss";
            TextFileHelper.WriteString(SassFile, string.Empty, false);
            TextFileHelper.WriteString(CssFile, string.Empty, false);
            TextFileHelper.WriteString(variablesFile, string.Empty, false);

            SassCompiler compiler = new SassCompiler();

            string LETheme = compiler.CompileFile(cssFolder + "LoanEditorTheme.scss", OutputStyle.Nested, false, null);
            TextFileHelper.WriteString(cssFolder + "LoanEditorTheme.css", LETheme, false);

            string DarkTheme = compiler.CompileFile(cssFolder + "DarkTheme.scss", OutputStyle.Nested, false, null);
            TextFileHelper.WriteString(cssFolder + "DarkTheme.css", DarkTheme, false);
        }

        public void GenerateElementSass(StyleEditor.CSSElement element, StringBuilder sb)
        {
            sb.AppendLine();
            sb.AppendLine(element.className);
            sb.AppendLine("{");

            foreach (string state in element.states.Keys)
            {
                if (!state.Equals("default"))
                {
                    sb.AppendLine("&:" + state);
                    sb.AppendLine("{");
                }

                foreach (string property in element.states[state].Keys)
                {
                    var propVal = element.states[state][property];
                    if (!String.IsNullOrEmpty(propVal))
                    {
                        if (property.Equals("mixin"))
                        {
                            sb.AppendLine(propVal);
                        }
                        else
                        {
                            sb.AppendLine(property + ": " + propVal.Replace("!important", "") + " !important;");
                        }
                    }
                }

                if (!state.Equals("default"))
                {
                    sb.AppendLine("}");
                }
            }
            if (element.children != null)
            {
                foreach (StyleEditor.CSSElement child in element.children)
                {
                    GenerateElementSass(child, sb);
                }
            }

            sb.AppendLine("}");
        }
    }
}