﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos
{
    public class GovernmentMonitoringDataServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(GovernmentMonitoringDataServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            GovernmentMonitoringData.BindDataFromControls(dataLoan, dataApp, this);
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            GovernmentMonitoringData.LoadDataForControls(dataLoan, dataApp, this);
        }
    }

    public partial class GovernmentMonitoringDataService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new GovernmentMonitoringDataServiceItem());
        }
    }
}
