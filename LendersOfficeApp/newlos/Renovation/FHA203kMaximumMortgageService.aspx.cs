﻿namespace LendersOfficeApp.newlos.Renovation
{
    using System;
    using DataAccess;
    using LendersOffice.ObjLib.Renovation;
    using LendersOffice.Common;
    using Worksheets;

    /// <summary>
    /// Service page for FHA203kMaximumMortgage, renovation loan.
    /// </summary>
    public class FHA203kMaximumMortgageServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        /// <summary>
        /// Function that creates the page data class for the service page.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <returns>CPageData object with the loan file fields loaded.</returns>
        protected override CPageData ConstructPageDataClass(Guid loanId)
        {
            return CPageData.CreateUsingSmartDependency(loanId, typeof(FHA203kMaximumMortgageServiceItem));
        }

        /// <summary>
        /// Service Page function that binds the data from the UI.
        /// </summary>
        /// <param name="dataLoan">The data loan object.</param>
        /// <param name="dataApp">The data app object.</param>
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            bool isPurchase = dataLoan.sLPurposeT == E_sLPurposeT.Purchase || dataLoan.sLPurposeT == E_sLPurposeT.Construct || dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm;

            if (!isPurchase)
            {
                dataLoan.sFHAPropAcquisitionT = this.GetEnum<FHAPropAcquisitionT>("sFHAPropAcquisitionT");
            }

            dataLoan.sFHA203kType = this.GetEnum<E_sFHA203kType>("sFHA203kType");
            dataLoan.sLPurposeT = this.GetEnum<E_sLPurposeT>("sLPurposeT");
            dataLoan.sProdSpT = this.GetEnum<E_sProdSpT>("sProdSpT");

            // Step 1                        
            dataLoan.sCostConstrRepairsRehab_rep = this.GetString("sCostConstrRepairsRehab");
            dataLoan.sArchitectEngineerFee_rep = this.GetString("sArchitectEngineerFee");
            dataLoan.sConsultantFee_rep = this.GetString("sConsultantFee");
            dataLoan.sRenovationConstrInspectFee_rep = this.GetString("sRenovationConstrInspectFee");
            dataLoan.sTitleUpdateFee_rep = this.GetString("sTitleUpdateFee");
            dataLoan.sPermitFee_rep = this.GetString("sPermitFee");
            dataLoan.sFeasibilityStudyFee_rep = this.GetString("sFeasibilityStudyFee");
            dataLoan.sContingencyRsrvPcRenovationHardCost_rep = this.GetString("sContingencyRsrvPcRenovationHardCost");
            dataLoan.sRenoMtgPmntRsrvMnth_rep = this.GetString("sRenoMtgPmntRsrvMnth");
            dataLoan.sFinanceOriginationFeeTgtVal_rep = this.GetString("sFinanceOriginationFeeTgtVal");
            dataLoan.sFinanceMortgagePmntRsrvTgtValLckd = this.GetBool("sFinanceMortgagePmntRsrvTgtValLckd");
            dataLoan.sDiscntPntOnRepairCostFeeTgtVal_rep = this.GetString("sDiscntPntOnRepairCostFeeTgtVal");
            dataLoan.sTotalRenovationCosts_rep = this.GetString("sTotalRenovationCosts");
            dataLoan.sTotalRenovationCostsLckd = this.GetBool("sTotalRenovationCostsLckd");
            dataLoan.sFinanceMortgagePmntRsrvTgtVal_rep = this.GetString("sFinanceMortgagePmntRsrvTgtVal");

            // Step 2 - Purchase
            if (isPurchase)
            {
                dataLoan.sPurchPrice_rep = this.GetString("sPurchPrice");
                dataLoan.sInducementPurchPrice_rep = this.GetString("sInducementPurchPrice");
                dataLoan.sFHASpAsIsVal_rep = this.GetString("sFHASpAsIsVal");
                dataLoan.sApprVal_rep = this.GetString("sApprVal");
            }

            // Step 2 - Refinance
            if (!isPurchase)
            {
                dataLoan.sRefPdOffAmt1003_rep = this.GetString("sRefPdOffAmt1003");
                dataLoan.sRefPdOffAmt1003Lckd = this.GetBool("sRefPdOffAmt1003Lckd");
                dataLoan.sFeeNewLoan_rep = this.GetString("sFeeNewLoan");
                dataLoan.sFHASpAsIsVal_rep = this.GetString("sFHASpAsIsVal_2");
                dataLoan.sApprVal_rep = this.GetString("sApprVal_2");
            }

            // Step 3 - Purchase
            if (isPurchase)
            {
                dataLoan.sFHA203kLeadPaintCredit_rep = this.GetString("sFHA203kLeadPaintCredit");
            }            

            // Step 4
            dataLoan.sEnergyImprovementAmount_rep = this.GetString("sEnergyImprovementAmount");
            dataLoan.sSolarEnergySystemCost_rep = this.GetString("sSolarEnergySystemCost");

            dataLoan.sLAmtCalc_rep = this.GetString("sLAmtCalc");

            // Step 6
            dataLoan.sBorrOwnFundsContingencyRsrv_rep = this.GetString("sBorrOwnFundsContingencyRsrv");
            dataLoan.s203kConsultantFeeInitDraw_rep = this.GetString("s203kConsultantFeeInitDraw");
            dataLoan.s203kConsultantFeeInitDrawLckd = this.GetBool("s203kConsultantFeeInitDrawLckd");
            dataLoan.sArchitectEngineerFeeInitDraw_rep = this.GetString("sArchitectEngineerFeeInitDraw");
            dataLoan.sArchitectEngineerFeeInitDrawLckd = this.GetBool("sArchitectEngineerFeeInitDrawLckd");
            dataLoan.sPermitFeeInitDraw_rep = this.GetString("sPermitFeeInitDraw");
            dataLoan.sPermitFeeInitDrawLckd = this.GetBool("sPermitFeeInitDrawLckd");
            dataLoan.sOriginationFeeInitDraw_rep = this.GetString("sOriginationFeeInitDraw");
            dataLoan.sOriginationFeeInitDrawLckd = this.GetBool("sOriginationFeeInitDrawLckd");
            dataLoan.sDiscntPointsOnRepairCostFeeInitDraw_rep = this.GetString("sDiscntPointsOnRepairCostFeeInitDraw");
            dataLoan.sDiscntPointsOnRepairCostFeeInitDrawLckd = this.GetBool("sDiscntPointsOnRepairCostFeeInitDrawLckd");
            dataLoan.sMatCostOrderedAndPrepaidInitDraw_rep = this.GetString("sMatCostOrderedAndPrepaidInitDraw");
            dataLoan.s50PcMatCostOrderedButNotPaidInitDraw_rep = this.GetString("s50PcMatCostOrderedButNotPaidInitDraw");

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }

        /// <summary>
        /// Service Page function that binds data to the UI.
        /// </summary>
        /// <param name="dataLoan">The data loan object.</param>
        /// <param name="dataApp">The data app object.</param>
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            this.SetResult("sFHA203kType", dataLoan.sFHA203kType);
            this.SetResult("sLPurposeT", dataLoan.sLPurposeT);
            this.SetResult("sProdSpT", dataLoan.sProdSpT);
            this.SetResult("sOccT", dataLoan.sOccT);
            this.SetResult("sCreditScoreType2", dataLoan.sCreditScoreType2);

            bool isPurchase = dataLoan.sLPurposeT == E_sLPurposeT.Purchase || dataLoan.sLPurposeT == E_sLPurposeT.Construct || dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm;

            if (!isPurchase)
            {
                this.SetResult("sFHAPropAcquisitionT", dataLoan.sFHAPropAcquisitionT);
            }

            this.SetResult("sRenoFeesToReduceUrlaLineF", dataLoan.sRenoFeesToReduceUrlaLineF_rep);
            this.SetResult("sDiscntPntOnRepairCostFee_Explanation", dataLoan.sDiscntPntOnRepairCostFee_rep);
            this.SetResult("sRenoFeesInClosingCosts", dataLoan.sRenoFeesInClosingCosts_rep);

            // Step 1
            this.SetResult("sRepairImprovementCostFee", dataLoan.sRepairImprovementCostFee_rep); // A
            this.SetResult("sCostConstrRepairsRehab", dataLoan.sCostConstrRepairsRehab_rep); // A.1
            this.SetResult("sArchitectEngineerFee", dataLoan.sArchitectEngineerFee_rep); // A.2 
            this.SetResult("sConsultantFee", dataLoan.sConsultantFee_rep); // A.3
            this.SetResult("sRenovationConstrInspectFee", dataLoan.sRenovationConstrInspectFee_rep); // A.4
            this.SetResult("sTitleUpdateFee", dataLoan.sTitleUpdateFee_rep); // A.5
            this.SetResult("sPermitFee", dataLoan.sPermitFee_rep); // A.6
            this.SetResult("sFeasibilityStudyFee", dataLoan.sFeasibilityStudyFee_rep); // A.7
            this.SetResult("sContingencyRsrvPcRenovationHardCost", dataLoan.sContingencyRsrvPcRenovationHardCost_rep); // B as percentage
            this.SetResult("sFinanceContingencyRsrv", dataLoan.sFinanceContingencyRsrv_rep); // B
            this.SetResult("sFinanceMortgagePmntRsrv", dataLoan.sFinanceMortgagePmntRsrv_rep); // C
            this.SetResult("sRenoMtgPmntRsrvMnth", dataLoan.sRenoMtgPmntRsrvMnth_rep); // C.Target month
            this.SetResult("sMonthlyPmt", dataLoan.sMonthlyPmt_rep); // C.Target what month is multiplied by
            this.SetResult("sFinanceMortgagePmntRsrvTgtVal", dataLoan.sFinanceMortgagePmntRsrvTgtVal_rep); // C.Target
            this.SetResult("sFinanceMortgagePmntRsrvTgtValLckd", dataLoan.sFinanceMortgagePmntRsrvTgtValLckd); // C.Target Lckd box
            this.SetResult("sMaxFinanceMortgagePmntRsrv", dataLoan.sMaxFinanceMortgagePmntRsrv_rep); // C Max
            this.SetResult("sFinanceMortgageFee", dataLoan.sFinanceMortgageFee_rep); // D
            this.SetResult("sFinanceOriginationFee", dataLoan.sFinanceOriginationFee_rep); // D.1
            this.SetResult("sFinanceOriginationFeeTgtVal", dataLoan.sFinanceOriginationFeeTgtVal_rep); // D.1 Target
            this.SetResult("sMaxFinanceOriginationFee", dataLoan.sMaxFinanceOriginationFee_rep); // D.1 Max
            this.SetResult("sDiscntPntOnRepairCostFee", dataLoan.sDiscntPntOnRepairCostFee_rep); // D.2
            this.SetResult("sDiscntPntOnRepairCostFeeTgtVal", dataLoan.sDiscntPntOnRepairCostFeeTgtVal_rep); // D.2 Target
            this.SetResult("sMaxDiscntPntOnRepairCostFee", dataLoan.sMaxDiscntPntOnRepairCostFee_rep); // D.2 Max
            this.SetResult("sTotalRenovationCosts", dataLoan.sTotalRenovationCosts_rep); // E
            this.SetResult("sTotalRenovationCostsLckd", dataLoan.sTotalRenovationCostsLckd); // E Lckd box

            // Step 2 - Purchase
            if (isPurchase)
            {
                this.SetResult("sPurchPrice", dataLoan.sPurchPrice_rep); // A
                this.SetResult("sInducementPurchPrice", dataLoan.sInducementPurchPrice_rep); // B
                this.SetResult("sPurchPriceLessInducement", dataLoan.sPurchPriceLessInducement_rep); // C
                this.SetResult("sFHASpAsIsVal", dataLoan.sFHASpAsIsVal_rep); // D
                this.SetResult("s203kFHASpAdjAsIsValue", dataLoan.s203kFHASpAdjAsIsValue_rep); // E
                this.SetResult("sApprVal", dataLoan.sApprVal_rep); // F
            }

            // Step 2 - Refinance
            if (!isPurchase)
            {
                this.SetResult("sRefPdOffAmt1003", dataLoan.sRefPdOffAmt1003_rep); // A
                this.SetResult("sRefPdOffAmt1003Lckd", dataLoan.sRefPdOffAmt1003Lckd); // A Lckd box
                this.SetResult("sTotalRenovationCosts_2", dataLoan.sTotalRenovationCosts_rep); // B
                this.SetResult("sFeeNewLoan", dataLoan.sFeeNewLoan_rep); // C
                this.SetResult("sFHA203kSum2AExistingDebtOnTheSubjectPropAnd2BTotalRehabilitationCostAnd2CFeeAssociatedWithTheNewLoan", dataLoan.sFHA203kSum2AExistingDebtOnTheSubjectPropAnd2BTotalRehabilitationCostAnd2CFeeAssociatedWithTheNewLoan_rep); // D
                this.SetResult("sFHASpAsIsVal_2", dataLoan.sFHASpAsIsVal_rep); // E
                this.SetResult("s203kFHASpAdjAsIsValue_2", dataLoan.s203kFHASpAdjAsIsValue_rep); // F
                this.SetResult("sApprVal_2", dataLoan.sApprVal_rep); // G
            }

            // Step 3 - Purchase
            if (isPurchase)
            {
                this.SetResult("sTotalRehabCostPlusAdjAsIsValue", dataLoan.sTotalRehabCostPlusAdjAsIsValue_rep); // A
                this.SetResult("sFHAAdjPropTypeAfterImproveValue", dataLoan.sFHAAdjPropTypeAfterImproveValue_rep); // B
                this.SetResult("sFHA203kLeadPaintCredit", dataLoan.sFHA203kLeadPaintCredit_rep); // C
                this.SetResult("sFHA203ProductLtvMaxMortgageAndMinAppraisedValue", dataLoan.sFHA203ProductLtvMaxMortgageAndMinAppraisedValue_rep); // D
                this.SetResult("sNationWideMortgageLimit", dataLoan.sNationWideMortgageLimit_rep); // E
                this.SetResult("s203kInitMaxBaseLoanAmount", dataLoan.s203kInitMaxBaseLoanAmount_rep); // F
                this.SetResult("sLtv203KMaxMortgageEligibility", dataLoan.sLtv203KMaxMortgageEligibility_rep); //G
            }            

            // Step 3 - Refinance
            if (!isPurchase)
            {
                this.SetResult("sFHA203kSum2AExistingDebtOnTheSubjectPropAnd2BTotalRehabilitationCostAnd2CFeeAssociatedWithTheNewLoan_2", dataLoan.sFHA203kSum2AExistingDebtOnTheSubjectPropAnd2BTotalRehabilitationCostAnd2CFeeAssociatedWithTheNewLoan_rep); // A
                this.SetResult("sTotalRehabCostPlusAdjAsIsValue_2", dataLoan.sTotalRehabCostPlusAdjAsIsValue_rep); // B
                this.SetResult("sFHAAdjPropTypeAfterImproveValue_2", dataLoan.sFHAAdjPropTypeAfterImproveValue_rep); // C
                this.SetResult("sFHA203ProductLtvMaxMortgageAndMinAppraisedValue_2", dataLoan.sFHA203ProductLtvMaxMortgageAndMinAppraisedValue_rep); // D
                this.SetResult("sNationWideMortgageLimit_2", dataLoan.sNationWideMortgageLimit_rep); // E
                this.SetResult("s203kInitMaxBaseLoanAmount_2", dataLoan.s203kInitMaxBaseLoanAmount_rep); // F
                this.SetResult("sLtv203KMaxMortgageEligibility_2", dataLoan.sLtv203KMaxMortgageEligibility_rep); // G
            }

            // Step 4
            this.SetResult("sEnergyImprovementAmount", dataLoan.sEnergyImprovementAmount_rep); // A
            this.SetResult("s203kIntermediateMaxBaseLoanAmount", dataLoan.s203kIntermediateMaxBaseLoanAmount_rep); // B
            this.SetResult("sSolarEnergySystemCost", dataLoan.sSolarEnergySystemCost_rep); // C
            this.SetResult("sFHASpAfterImproveValBy20Pc", dataLoan.sFHASpAfterImproveValBy20Pc_rep); // D
            this.SetResult("sAdjSolarEnergyAmount", dataLoan.sAdjSolarEnergyAmount_rep); // E
            this.SetResult("sNationWideMortgageLimitBy120Pc", dataLoan.sNationWideMortgageLimitBy120Pc_rep); // F
            this.SetResult("s203kFinalMaxBaseLoanAmount", dataLoan.s203kFinalMaxBaseLoanAmount_rep); // G

            this.SetResult("sLAmtCalc", dataLoan.sLAmtCalc_rep);

            // Step 5
            this.SetResult("sFHAMipLtv", dataLoan.sFHAMipLtv_rep);
            this.SetResult("sLtvR", dataLoan.sLtvR_rep);

            // Step 6
            this.SetResult("sTotalRenovationCosts_3", dataLoan.sTotalRenovationCosts_rep); // A
            this.SetResult("sEnergyImprovementAmount_2", dataLoan.sEnergyImprovementAmount_rep); // B
            this.SetResult("sAdjSolarEnergyAmount_2", dataLoan.sAdjSolarEnergyAmount_rep); // C
            this.SetResult("sBorrOwnFundsContingencyRsrv", dataLoan.sBorrOwnFundsContingencyRsrv_rep); // D
            this.SetResult("s203KRehabEscrowAmtTot", dataLoan.s203KRehabEscrowAmtTot_rep); // E
            this.SetResult("s203KRehabEscrowInitDrawAmt", dataLoan.s203KRehabEscrowInitDrawAmt_rep); // F
            this.SetResult("s203kConsultantFeeInitDraw", dataLoan.s203kConsultantFeeInitDraw_rep); // F.1
            this.SetResult("s203kConsultantFeeInitDrawLckd", dataLoan.s203kConsultantFeeInitDrawLckd); // F.1
            this.SetResult("sArchitectEngineerFeeInitDraw", dataLoan.sArchitectEngineerFeeInitDraw_rep); // F.2
            this.SetResult("sArchitectEngineerFeeInitDrawLckd", dataLoan.sArchitectEngineerFeeInitDrawLckd); // F.2
            this.SetResult("sPermitFeeInitDraw", dataLoan.sPermitFeeInitDraw_rep); // F.3
            this.SetResult("sPermitFeeInitDrawLckd", dataLoan.sPermitFeeInitDrawLckd); // F.3
            this.SetResult("sOriginationFeeInitDraw", dataLoan.sOriginationFeeInitDraw_rep); // F.4
            this.SetResult("sOriginationFeeInitDrawLckd", dataLoan.sOriginationFeeInitDrawLckd); // F.4
            this.SetResult("sDiscntPointsOnRepairCostFeeInitDraw", dataLoan.sDiscntPointsOnRepairCostFeeInitDraw_rep); // F.5
            this.SetResult("sDiscntPointsOnRepairCostFeeInitDrawLckd", dataLoan.sDiscntPointsOnRepairCostFeeInitDrawLckd); // F.5
            this.SetResult("sMatCostOrderedAndPrepaidInitDraw", dataLoan.sMatCostOrderedAndPrepaidInitDraw_rep); // F.6
            this.SetResult("s50PcMatCostOrderedButNotPaidInitDraw", dataLoan.s50PcMatCostOrderedButNotPaidInitDraw_rep); // F.7
            this.SetResult("sTotalRehabEscrowAmount", dataLoan.sTotalRehabEscrowAmount_rep); // G

            MaxFinanceableOriginationFeePopup.LoadData(dataLoan, this);
            MaxFinanceableDiscountPointsPopup.LoadData(dataLoan, this);
        }
    }

    public partial class FHA203kMaximumMortgageService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new FHA203kMaximumMortgageServiceItem());
        }
    }
}