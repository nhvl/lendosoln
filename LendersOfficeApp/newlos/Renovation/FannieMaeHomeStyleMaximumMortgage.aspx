﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FannieMaeHomeStyleMaximumMortgage.aspx.cs" Inherits="LendersOfficeApp.newlos.Renovation.FannieMaeHomeStyleMaximumMortgage" %>
<%@ Register TagPrefix="ML" TagName="MaxOrigFeePopup" Src="~/newlos/Renovation/MaxFinanceableOriginationFeePopup.ascx" %>
<%@ Register TagPrefix="ML" TagName="MaxDiscountPointsPopup" Src="~/newlos/Renovation/MaxFinanceableDiscountPointsPopup.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>HomeStyle Calculator</title>
        <style type="text/css">
            body {
                background-color: gainsboro;
            }
            h1 {
                margin: 0px;
            }
            .width20 {
                width: 20px;
            }
            .width40 {
                width: 40px;
            }
            .width90Important {
                width: 90px !important;
            }
            .width120 {
                width: 120px;
            }
            .width150 {
                width: 150px;
            }
            .width240 {
                width: 240px;
            }
            th {
                background-color: gray;
                color: white;
            }
            .width960 {
                width: 960px;
            }
            .width860 {
                width: 860px;
            }
            .alignLeft {
                text-align: left;
            }
            .alignCenter {
                text-align: center;
            }
            .alignRight {
                text-align: right;
                float: right;
            }
            .solidBorder {
                border: 1px solid;
                margin: 10px;
                border-collapse: collapse;
            }
            .stepTable td, .stepTable th {
                border: .5px solid;
                border-color: grey;
                border-width: 1px;
                padding: 4px;
            }
            .padLeft15 {
                padding-left: 15px;
            }
            .padLeft30 {
                padding-left: 30px;
            }
            .float-left {
                float: left;
            }
            .clear-both {
                clear: both;
            }
            .reno-reduction-explanation th, .reno-reduction-explanation td {
                padding: 4px;
            }
            .btn-copy-file {
                width: 172px;
                text-align: center;
            }
        </style>
    </head>
    <body>
    <script type="text/javascript">
        $(document).ready(function () {
            registerPostRefreshCalculationCallback(postRefreshCalculationMaxOrigFeeHandler);
            registerPostRefreshCalculationCallback(postRefreshCalculationMaxDiscountPointsHandler);
        });

        function consolidateDuplicateLckdFieldDataBeforeServiceCall(isChecked) {
            $("#sRefPdOffAmt1003Lckd").prop('checked', isChecked);
            $("#sRefPdOffAmt1003Lckd_2").prop('checked', isChecked);
        }

        function consolidateDuplicateFieldDataBeforeServiceCall(fieldId, value) {
            $("#" + fieldId).val(value);
            $("#" + fieldId + "_2").val(value);
        }

        function _init() {
            $("#purchOrRefiLabel").text(ML.isPurchase ? "Purchase" : "Refinance");
            $(".isPurchase").toggle(ML.isPurchase);
            $(".isRefinance").toggle(!ML.isPurchase);
            $("#sPurchPrice").prop('readonly', !ML.isPurchase);
            $("#sPurchPrice_2").prop('readonly', !ML.isPurchase);

            lockField(document.getElementById("sRefPdOffAmt1003Lckd"), "sRefPdOffAmt1003");
            lockField(document.getElementById("sRefPdOffAmt1003Lckd_2"), "sRefPdOffAmt1003_2");
            lockField(document.getElementById("sFinanceMortgagePmntRsrvTgtValLckd"), "sFinanceMortgagePmntRsrvTgtVal");
            lockField(document.getElementById("sTotEstPp1003Lckd"), "sTotEstPp1003");
            lockField(document.getElementById("sTotEstCc1003Lckd"), "sTotEstCcNoDiscnt1003");
            lockField(document.getElementById("sLDiscnt1003Lckd"), "sLDiscnt1003");
            lockField(document.getElementById("sTotCcPbsLocked"), "sTotCcPbs");
            lockField(document.getElementById("sTransNetCashLckd"), "sTransNetCash");
            lockField(document.getElementById("sArchitectEngineerFeeLckd"), "sArchitectEngineerFee");
            lockField(document.getElementById("sConsultantFeeLckd"), "sConsultantFee");
            lockField(document.getElementById("sRenovationConstrInspectFeeLckd"), "sRenovationConstrInspectFee");
            lockField(document.getElementById("sTitleUpdateFeeLckd"), "sTitleUpdateFee");
            lockField(document.getElementById("sPermitFeeLckd"), "sPermitFee");
            lockField(document.getElementById("sTotalRenovationCostsLckd"), "sTotalRenovationCosts");
            lockField(document.getElementById("sAltCostLckd"), "sAltCost");

            checkMonth(document.getElementById('sRenoMtgPmntRsrvMnth'));
        }

        function copyToFileBaseLoanAmount() {
            $("#sLAmtCalc").val($("#sRenovationMaxMtgAmt").val());
            updateDirtyBit();
            refreshCalculation();
        }

        function checkMonth(month) {
            if (month.value > 6) {
                $("#sRenoMtgPmntRsrvMnth").val(6);
            }
        }

        function displayMaxOrigFeePopup() {
            $('.max-orig-fee-popup').dialog({
                dialogClass: "MaxOrigFeePopup",
                width: 525,
                height: 450,
                modal: true,
                title: "Maximum Financeable Origination Fee Calculation",
                resizable: false,
                draggable: false,
                buttons: [
                    {
                        text: 'Close',
                        click: function () { $(this).dialog('close'); }
                    }
                ]
            });
        }

        function displayMaxDiscountPointsPopup() {
            $('.max-discount-points-popup').dialog({
                dialogClass: "MaxDiscountPointsPopup",
                width: 525,
                height: 300,
                modal: true,
                title: "Maximum Financeable Discount Points Calculation",
                resizable: false,
                draggable: false,
                buttons: [
                    {
                        text: 'Close',
                        click: function () { $(this).dialog('close'); }
                    }
                ]
            });
        }
    </script>
        <form id="form1" runat="server">
        <ML:MaxOrigFeePopup runat="server" id="MaxFinanceableOriginationFeePopup"></ML:MaxOrigFeePopup>
        <ML:MaxDiscountPointsPopup runat="server" id="MaxFinanceableDiscountPointsPopup"></ML:MaxDiscountPointsPopup>
        <h1 class="MainRightHeader">HomeStyle Calculator</h1>   
            <table class="reno-reduction-explanation solidBorder float-left">
                <tr>
                    <td>
                        <label class="FieldLabel">
                            URLA: Renovation Costs not included in Line F
                        </label>
                    </td>
                    <td>
                        <ML:MoneyTextBox ID="sRenoFeesToReduceUrlaLineF" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="FieldLabel">
                            URLA: Renovation Costs not included in Line H
                        </label>
                    </td>
                    <td>
                        <ML:MoneyTextBox ID="sDiscntPntOnRepairCostFee_Explanation" data-field-id="sDiscntPntOnRepairCostFee" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="FieldLabel">
                            LE/CD: Renovation Costs already included in Closing Cost Details
                        </label>
                    </td>
                    <td>
                        <ML:MoneyTextBox ID="sRenoFeesInClosingCosts" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                    </td>
                </tr>
            </table>         
            <div class="stepTable clear-both">
                <table class="width960 solidBorder">
                    <thead>
                        <tr>
                            <th class="alignLeft" colspan="4">
                                A. Loan Parameters
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    1.
                                </label>
                            </td>
                            <td>
                                Applicable LTV Percentage
                            </td>
                            <td class="width120">
                                <ML:PercentTextBox id="sRenovationLoanTgtLtv" runat="server" onchange="refreshCalculation();" class="width90Important"></ML:PercentTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    2.
                                </label>
                            </td>
                            <td>
                                Renovation Costs Cannot Exceed 75% of:
                                <br />
                                <span class="padLeft15">
                                    For purchase - the lesser of: Purchase Price plus Renovation Costs or "As Completed" Appraised
                                </span>
                                <br />
                                <span class="padLeft15">
                                    For refinance - the "As Completed" appraised value
                                </span>
                                <br />
                                <span class="padLeft15">
                                    For manufactured homes - eligible renovation funds capped at the lesser of $50,000 or 50% of the "as-completed" appraised value
                                </span>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sHomeStyleMaxRenovCost" runat="server" readonly="true" class="width90Important"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    3.
                                </label>
                            </td>
                            <td>
                                Occupancy
                            </td>
                            <td>
                                <asp:DropDownList ID="sOccT" runat="server" disabled="true"></asp:DropDownList>
                            </td>
                        </tr>                        
                    </tbody>
                </table>
                <table class="width960 solidBorder">
                    <thead>
                        <tr>
                            <th class="alignLeft" colspan="4">
                                B. Property Information
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    1.
                                </label>
                            </td>
                            <td>
                                Purchase Price <i>(Purchase Transaction)</i>
                            </td>
                            <td class="width120">
                                <ML:MoneyTextBox id="sPurchPrice" runat="server" onchange="consolidateDuplicateFieldDataBeforeServiceCall('sPurchPrice', this.value); refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    2.
                                </label>
                            </td>
                            <td>
                                First Mortgage Pay-Off and Eligible Liens <i>(Refinance Transaction)</i>
                            </td>
                            <td class="width120">
                                <ML:MoneyTextBox id="sRefPdOffAmt1003" runat="server" onchange="consolidateDuplicateFieldDataBeforeServiceCall('sRefPdOffAmt1003', this.value); refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox ID="sRefPdOffAmt1003Lckd" runat="server" class="alignCheckBox" onclick="consolidateDuplicateLckdFieldDataBeforeServiceCall(this.checked); refreshCalculation();" />
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    3.
                                </label>
                            </td>
                            <td>
                                Estimated "As Completed" Value <i>(after improvements)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sApprVal" runat="server" onchange="consolidateDuplicateFieldDataBeforeServiceCall('sApprVal', this.value); refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>                        
                    </tbody>
                </table>
                <table class="width960 solidBorder">
                    <thead>
                        <tr>
                            <th class="alignLeft" colspan="4">
                                C. Alterations, Improvements, and Repairs
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    1.
                                </label>
                            </td>
                            <td>
                                Alterations, Improvements, and Repairs.
                            </td>
                            <td class="width120">
                            </td>
                            <td class="width120">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                a. Hard Costs <i>(Labor/Materials)</i>
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sCostConstrRepairsRehab" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                b. Contingency Reserve <i>(if applicable and financed)</i>
                            </td>
                            <td>
                                <ML:PercentTextBox id="sContingencyRsrvPcRenovationHardCost" runat="server" class="width90Important" onchange="refreshCalculation();"></ML:PercentTextBox>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFinanceContingencyRsrv" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                c. Architect/Engineer Fees
                            </td>
                            <td>
                            </td>
                            <td>
                                <input type="checkbox" id="sArchitectEngineerFeeLckd" runat="server" class="Hidden" />
                                <ML:MoneyTextBox id="sArchitectEngineerFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                d. Consultant Fees
                            </td>
                            <td>
                            </td>
                            <td>
                                <input type="checkbox" id="sConsultantFeeLckd" runat="server" class="Hidden" />
                                <ML:MoneyTextBox id="sConsultantFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                e. Inspections
                            </td>
                            <td>
                            </td>
                            <td>
                                <input type="checkbox" id="sRenovationConstrInspectFeeLckd" runat="server" class="Hidden" />
                                <ML:MoneyTextBox id="sRenovationConstrInspectFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                f. Title Updates
                            </td>
                            <td>
                            </td>
                            <td>
                                <input type="checkbox" id="sTitleUpdateFeeLckd" runat="server" class="Hidden" />
                                <ML:MoneyTextBox id="sTitleUpdateFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                g. Permits
                            </td>
                            <td>
                            </td>
                            <td>
                                <input type="checkbox" id="sPermitFeeLckd" runat="server" class="Hidden" />
                                <ML:MoneyTextBox id="sPermitFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                h. Payment Reserve <i>(Months not occupied x Monthly Payment - Not to exceed 6 months)</i>
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFinanceMortgagePmntRsrv" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <span class="padLeft15">
                                    Target Value
                                </span>                     
                                <span class="alignRight">
                                    <asp:TextBox ID="sRenoMtgPmntRsrvMnth" runat="server" class="width20" maxlength="1" onchange="refreshCalculation();"></asp:TextBox>
                                    months x 
                                    <ML:MoneyTextBox ID="sMonthlyPmt" runat="server" readonly="true"></ML:MoneyTextBox>
                                    =
                                </span>                                
                            </td>                            
                            <td>
                                <ML:MoneyTextBox id="sFinanceMortgagePmntRsrvTgtVal" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox ID="sFinanceMortgagePmntRsrvTgtValLckd" class="alignCheckBox" runat="server" onchange="refreshCalculation();"/>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <span class="padLeft15">
                                    Maximum Financeable Mortgage Payment Reserves
                                </span>                                
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sMaxFinanceMortgagePmntRsrv" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                i. Other&nbsp;
                                <span>
                                    <asp:TextBox id="sOtherRenovationCostDescription" runat="server" class="width240"></asp:TextBox>
                                </span>
                            </td>
                            <td>
                            </td>
                            <td class="width120">
                                <ML:MoneyTextBox id="sOtherRenovationCost" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <span class="padLeft15">
                                    Financeable Origination Fee
                                </span>
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFinanceOriginationFee" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <span class="padLeft30">
                                    Target Value
                                </span>
                            </td>
                            <td>
                                <span>
                                    <ML:MoneyTextBox id="sFinanceOriginationFeeTgtVal" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                </span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <span class="padLeft30">
                                    Maximum Financeable Origination Fee
                                </span>  
                                <span class="alignRight">
                                    <a onclick="displayMaxOrigFeePopup();">explain</a>
                                </span>                              
                            </td>
                            <td>
                                <span>
                                    <ML:MoneyTextBox id="sMaxFinanceOriginationFee" runat="server" readonly="true"></ML:MoneyTextBox>
                                </span>
                            </td>
                            <td>
                            </td>                            
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <span class="padLeft15">
                                    Discount Points
                                </span>
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sDiscntPntOnRepairCostFee" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td> 
                                <span class="padLeft30">
                                    Target Value
                                </span>                               
                            </td>
                            <td>
                                <span>
                                    <ML:MoneyTextBox id="sDiscntPntOnRepairCostFeeTgtVal" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                </span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td> 
                                <span class="padLeft30">
                                    Maximum Financeable Discount Points
                                </span>     
                                <span class="alignRight">
                                    <a onclick="displayMaxDiscountPointsPopup();">explain</a>
                                </span>                          
                            </td>
                            <td>
                                <span>
                                    <ML:MoneyTextBox id="sMaxDiscntPntOnRepairCostFee" runat="server" readonly="true"></ML:MoneyTextBox>
                                </span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <span class="padLeft15">
                                    Feasibility Study
                                </span>
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFeasibilityStudyFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>                                
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    2.
                                </label>
                            </td>
                            <td>
                                Total of Alterations, Improvements, and Repairs <i>(Total of C1a to C1i)</i>
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sTotalRenovationCosts" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox id="sTotalRenovationCostsLckd" runat="server" onclick="refreshCalculation();"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="width960 solidBorder">
                    <thead>
                        <tr>
                            <th class="alignLeft" colspan="4">
                                D. Loan Amount
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    1.
                                </label>
                            </td>
                            <td>
                                Total of Purchase Price and Improvement Costs <i>(B1 + C2)</i>
                            </td>
                            <td class="width120">
                                <ML:MoneyTextBox id="sRenovationPurchPricePlusTotalRenovationCosts" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    2.
                                </label>
                            </td>
                            <td>
                                Estimated "As Completed" Value <i>(B3)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox data-field-id="sApprVal" id="sApprVal_2" runat="server" onchange="consolidateDuplicateFieldDataBeforeServiceCall('sApprVal', this.value); refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    3.
                                </label>
                            </td>
                            <td>
                                Total of Financed Mortgage Insurance <i>(E7)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFfUfmipFinanced" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    4.
                                </label>
                            </td>
                            <td>
                                Purchase Mortgage Amount <i>((Lesser of D1 or D2) x A1)</i>
                                <span class="isPurchase">
                                    <input class="alignRight btn-copy-file" type="button" value="Copy to File Base Loan Amount" onclick="copyToFileBaseLoanAmount();"/>
                                </span>
                            </td>
                            <td class="isPurchase">
                                <ML:MoneyTextBox id="sRenovationMaxMtgAmt" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                            </td>
                            <td class="alignCenter isRefinance">
                                <label class="FieldLabel">
                                    N/A
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    5.
                                </label>
                            </td>
                            <td>
                                Refinance Mortgage Amount <i>(B3 x A1)</i>
                                <span class="isRefinance">
                                    <input class="alignRight" type="button" value="Copy to File Base Loan Amount" onclick="copyToFileBaseLoanAmount();"/>
                                </span>
                            </td>
                            <td class="alignCenter isPurchase">
                                <label class="FieldLabel">
                                    N/A
                                </label>
                            </td>
                            <td class="isRefinance">
                                <ML:MoneyTextBox data-field-id="sRenovationMaxMtgAmt" id="sRenovationMaxMtgAmt_2" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="width960 solidBorder">
                    <thead>
                        <tr>
                            <th class="alignLeft" colspan="4">
                                E. Details of Transactions <i>(from Form 1003)</i>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="2">
                            </td>
                            <td class="alignCenter width120">
                                <label class="FieldLabel" id="purchOrRefiLabel">
                                </label>
                            </td>                            
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    1.
                                </label>
                            </td>
                            <td>
                                Purchase Price <i>(B1)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox data-field-id="sPurchPrice" id="sPurchPrice_2" runat="server" onchange="consolidateDuplicateFieldDataBeforeServiceCall('sPurchPrice', this.value); refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    2.
                                </label>
                            </td>
                            <td>
                                Alterations, Improvements, and Repairs <i>(C2)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sAltCost" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox id="sAltCostLckd" runat="server" onclick="refreshCalculation();"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    3.
                                </label>
                            </td>
                            <td>
                                Land <i>(if acquired separately)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sLandCost" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    4.
                                </label>
                            </td>
                            <td>
                                Refinance <i>(include debts to be paid off) (B2)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox data-field-id="sRefPdOffAmt1003" id="sRefPdOffAmt1003_2" runat="server" onchange="consolidateDuplicateFieldDataBeforeServiceCall('sRefPdOffAmt1003', this.value); refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox data-field-id="sRefPdOffAmt1003Lckd" ID="sRefPdOffAmt1003Lckd_2" runat="server" onclick="consolidateDuplicateLckdFieldDataBeforeServiceCall(this.checked); refreshCalculation();"></asp:CheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    5.
                                </label>
                            </td>
                            <td>
                                Estimated Prepaid Items
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sTotEstPp1003" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox id="sTotEstPp1003Lckd" runat="server" onclick="refreshCalculation();"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    6.
                                </label>
                            </td>
                            <td>
                                Estimated Closing Costs
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sTotEstCcNoDiscnt1003" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox id="sTotEstCc1003Lckd" runat="server" onclick="refreshCalculation();"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    7.
                                </label>
                            </td>
                            <td>
                                Financed Mortgage Insurance
                            </td>
                            <td>
                                <ML:MoneyTextBox data-field-id="sFfUfmipFinanced" id="sFfUfmipFinanced_2" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    8.
                                </label>
                            </td>
                            <td>
                                Discount <i>(if borrower will pay)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sLDiscnt1003" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox id="sLDiscnt1003Lckd" runat="server" onclick="refreshCalculation();"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    9.
                                </label>
                            </td>
                            <td>
                                Total Costs <i>(Total of E1 to E8)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sTotTransC" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    10.
                                </label>
                            </td>
                            <td>
                                Subordinate Financing
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sONewFinBal" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    11.
                                </label>
                            </td>
                            <td>
                                Borrower Closing Costs paid by Seller
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sTotCcPbs" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox id="sTotCcPbsLocked" runat="server" onclick="refreshCalculation();"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    12.
                                </label>
                            </td>
                            <td>
                                Other Credits
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sRenovationOtherCredits" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    13a.
                                </label>
                            </td>
                            <td>
                                Total Loan Amount*
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFinalLAmt" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    13b.
                                </label>
                            </td>
                            <td>
                                Base Loan Amount <i>(D4 or D5)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sLAmtCalc" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    14.
                                </label>
                            </td>
                            <td>
                                Total Funds Available to Borrower <i>(E10 + E11 + E12 + E13a)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sRenovationTtlFundsAvailableToBorrower" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter">
                                <label class="FieldLabel">
                                    15.
                                </label>
                            </td>
                            <td>
                                Cash from borrower** <i>(E9 - E15)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sTransNetCash" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox id="sTransNetCashLckd" runat="server" onclick="refreshCalculation();"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                *Includes Financed MI when applicable
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                **No cash back to the borrower is permitted with HomeStyle Renovation; standard limited cash-out refinance cash back guidelines per the Selling Guide do not apply to HomeStyle Renovation.
                            </td>
                            <td>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </body>
</html>
