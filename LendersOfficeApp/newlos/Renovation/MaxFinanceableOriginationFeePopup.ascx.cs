﻿#region Generated code
namespace LendersOfficeApp.newlos.Renovation
#endregion
{
    using System;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Provides a breakdown of the maximum financeable origination fee calculation
    /// for renovation loan files.
    /// </summary>
    public partial class MaxFinanceableOriginationFeePopup : System.Web.UI.UserControl
    {
        /// <summary>
        /// Loads the control data for the service item.
        /// </summary>
        /// <param name="loan">
        /// The loan to load.
        /// </param>
        /// <param name="service">
        /// The service item to send the data through.
        /// </param>
        public static void LoadData(CPageData loan, AbstractBackgroundServiceItem service)
        {
            Func<string, string> keyGenerator = key => nameof(MaxFinanceableOriginationFeePopup) + "_" + key;

            var data = new MaxFinanceableOriginationFeePopupData();
            data.PopulateFrom(loan);

            service.SetResult(keyGenerator(nameof(data.OriginationFeeAmount)), data.OriginationFeeAmount);
            service.SetResult(keyGenerator(nameof(data.SupplementalOriginationFeeAmount)), data.SupplementalOriginationFeeAmount);
            service.SetResult(keyGenerator(nameof(data.SumOriginationFees)), data.SumOriginationFees);
            service.SetResult(keyGenerator(nameof(data.RepairCostsAndFees)), data.RepairCostsAndFees);
            service.SetResult(keyGenerator(nameof(data.ContingencyReserves)), data.ContingencyReserves);
            service.SetResult(keyGenerator(nameof(data.MortgagePaymentReserves)), data.MortgagePaymentReserves);
            service.SetResult(keyGenerator(nameof(data.SumRepairCostsFeesAndReserves)), data.SumRepairCostsFeesAndReserves);
            service.SetResult(keyGenerator(nameof(data.PercentageOfRepairCostReservesSum)), data.PercentageOfRepairCostReservesSum);
            service.SetResult(keyGenerator(nameof(data.GreaterOfRepairCostReserveSumPercentageAndFixedAmount)), data.GreaterOfRepairCostReserveSumPercentageAndFixedAmount);
            service.SetResult(keyGenerator(nameof(data.MaxFinanceableOrigFeeAmount)), data.MaxFinanceableOrigFeeAmount);
        }

        /// <summary>
        /// Loads data for the popup.
        /// </summary>
        /// <param name="loan">
        /// The loan to load data.
        /// </param>
        public void LoadData(CPageData loan)
        {
            var data = new MaxFinanceableOriginationFeePopupData();
            data.PopulateFrom(loan);

            this.OriginationFeeAmount.Text = data.OriginationFeeAmount;
            this.SupplementalOriginationFeeAmount.Text = data.SupplementalOriginationFeeAmount;
            this.SumOriginationFees.Text = data.SumOriginationFees;
            this.RepairCostsAndFees.Text = data.RepairCostsAndFees;
            this.ContingencyReserves.Text = data.ContingencyReserves;
            this.MortgagePaymentReserves.Text = data.MortgagePaymentReserves;
            this.SumRepairCostsFeesAndReserves.Text = data.SumRepairCostsFeesAndReserves;
            this.PercentageOfRepairCostReservesSum.Text = data.PercentageOfRepairCostReservesSum;
            this.GreaterOfRepairCostReserveSumPercentageAndFixedAmount.Text = data.GreaterOfRepairCostReserveSumPercentageAndFixedAmount;
            this.MaxFinanceableOrigFeeAmount.Text = data.MaxFinanceableOrigFeeAmount;
        }

        /// <summary>
        /// Initializes the control.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="args">
        /// The parameter is not used.
        /// </param>
        protected void Page_Init(object sender, EventArgs args)
        {
            var basePage = this.Page as BasePage;
            if (basePage == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Max financeable origination fee popup is expected to be hosted on a BasePage.");
            }
            else
            {
                basePage.RegisterJsGlobalVariables("MaxOrigCompFeeClientId", this.ClientID);
                basePage.RegisterJsGlobalVariables("MaxOrigCompFeeResultPrefix", nameof(MaxFinanceableOriginationFeePopup));

                basePage.RegisterJsScript("MaxFinanceableOriginationFeePopup.js");
                basePage.RegisterCSS("MaxFinanceableOriginationFeePopup.css");
            }
        }

        /// <summary>
        /// Provides a container for max financeable origination fee data.
        /// </summary>
        private class MaxFinanceableOriginationFeePopupData
        {
            /// <summary>
            /// Gets the origination fee amount.
            /// </summary>
            public string OriginationFeeAmount { get; private set; }

            /// <summary>
            /// Gets the supplemental origination fee amount.
            /// </summary>
            public string SupplementalOriginationFeeAmount { get; private set; }

            /// <summary>
            /// Gets the sum of the origination fees.
            /// </summary>
            public string SumOriginationFees { get; private set; }

            /// <summary>
            /// Gets the repair costs and fees.
            /// </summary>
            public string RepairCostsAndFees { get; private set; }

            /// <summary>
            /// Gets the contingency reserves.
            /// </summary>
            public string ContingencyReserves { get; private set; }

            /// <summary>
            /// Gets the mortgage payment reserves.
            /// </summary>
            public string MortgagePaymentReserves { get; private set; }

            /// <summary>
            /// Gets the sum of the repair costs, contingency reserves, and mortgage payment reserves.
            /// </summary>
            public string SumRepairCostsFeesAndReserves { get; private set; }

            /// <summary>
            /// Gets the 1.500% percentage of the repair costs and reserves sum.
            /// </summary>
            public string PercentageOfRepairCostReservesSum { get; private set; }

            /// <summary>
            /// Gets the max of the sum of the repair costs and reserves and the $350 
            /// fixed amount.
            /// </summary>
            public string GreaterOfRepairCostReserveSumPercentageAndFixedAmount { get; private set; }

            /// <summary>
            /// Gets the maximum financeable origination fee amount.
            /// </summary>
            public string MaxFinanceableOrigFeeAmount { get; private set; }

            /// <summary>
            /// Populates data from the loan.
            /// </summary>
            /// <param name="loan">
            /// The loan to populate data.
            /// </param>
            public void PopulateFrom(CPageData loan)
            {
                Func<decimal, string> moneyToRep = value => loan.m_convertLos.ToMoneyString(value, FormatDirection.ToRep);

                var originationFee = loan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId);
                var originationFeeAmount = originationFee == null ? 0M : ((BorrowerClosingCostFee)originationFee).TotalAmount;
                this.OriginationFeeAmount = moneyToRep(originationFeeAmount);

                var supplementalOriginationFees = loan.sClosingCostSet.GetFees(fee => fee.MismoFeeT == E_ClosingCostFeeMismoFeeT._203KSupplementalOriginationFee);
                var supplementalOriginationFeesAmount = !supplementalOriginationFees.Any() ? 0M : Tools.SumMoney(supplementalOriginationFees
                    .Cast<BorrowerClosingCostFee>()
                    .Select(fee => fee.TotalAmount));

                this.SupplementalOriginationFeeAmount = moneyToRep(supplementalOriginationFeesAmount);

                var originationFeeSum = Tools.SumMoney(new[] { originationFeeAmount, supplementalOriginationFeesAmount });
                this.SumOriginationFees = moneyToRep(originationFeeSum);

                var repairCost = loan.sRepairImprovementCostFee;
                var financeContingencyReserves = loan.sFinanceContingencyRsrv;
                var financeMortgageReserves = loan.sFinanceMortgagePmntRsrv;

                this.RepairCostsAndFees = moneyToRep(repairCost);
                this.ContingencyReserves = moneyToRep(financeContingencyReserves);
                this.MortgagePaymentReserves = moneyToRep(financeMortgageReserves);

                var feeReserveSum = Tools.SumMoney(new[] { repairCost, financeContingencyReserves, financeMortgageReserves });
                this.SumRepairCostsFeesAndReserves = moneyToRep(feeReserveSum);

                var percentageOfRepairCostsAndReserves = .015M * feeReserveSum;
                this.PercentageOfRepairCostReservesSum = moneyToRep(percentageOfRepairCostsAndReserves);

                var greaterOfRepairCostsReservePercentageAndFixedAmount = Math.Max(350.0M, percentageOfRepairCostsAndReserves);
                this.GreaterOfRepairCostReserveSumPercentageAndFixedAmount = moneyToRep(greaterOfRepairCostsReservePercentageAndFixedAmount);

                var maxFinanceableOriginationFeeAmount = Math.Min(originationFeeSum, greaterOfRepairCostsReservePercentageAndFixedAmount);
                this.MaxFinanceableOrigFeeAmount = moneyToRep(maxFinanceableOriginationFeeAmount);
            }
        }
    }
}