﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FHA203kMaximumMortgage.aspx.cs" Inherits="LendersOfficeApp.newlos.Renovation.FHA203kMaximumMortgage" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ML" TagName="MaxOrigFeePopup" Src="~/newlos/Renovation/MaxFinanceableOriginationFeePopup.ascx" %>
<%@ Register TagPrefix="ML" TagName="MaxDiscountPointsPopup" Src="~/newlos/Renovation/MaxFinanceableDiscountPointsPopup.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>203(k) Maximum Mortgage Calculator</title>
        <style type="text/css">
            body {
                background-color: gainsboro;
            }
            h1 {
                margin: 0px;
            }
            .width20 {
                width: 20px;
            }
            .width40 {
                width: 40px;
            }
            .width90Important {
                width: 90px !important;
            }
            .width120 {
                width: 120px;
            }
            .width150 {
                width: 150px;
            }
            th {
                background-color: gray;
                color: white;
            }
            .width960 {
                width: 960px;
            }
            .width840 {
                width: 840px;
            }
            .alignLeft {
                text-align: left;
            }
            .alignCenter {
                text-align: center;
            }
            .alignRight {
                text-align: right;
                float: right;
            }
            .solidBorder {
                border: 1px solid;
                margin: 10px;
                border-collapse: collapse;
            }
            .stepTable td, .stepTable th {
                border: .5px solid;
                border-color: grey;
                border-width: 1px;
                padding: 4px;
            }
            .alignCheckBox {
                margin: 0px;
                padding: 0px;
                vertical-align: top;
                line-height: normal;
            }
            input[type="text"] input[type="checkbox"] {
                vertical-align: top;
            }
            .float-left {
                float: left;
            }
            .clear-both {
                clear: both;
            }
            .reno-reduction-explanation {
                margin-left: 60px;
                margin-top: 15px;
            }
            .reno-reduction-explanation th, .reno-reduction-explanation td {
                padding: 4px;
            }
        </style>
    </head>
    <body>
    <script type="text/javascript">
        $(document).ready(function () {
            registerPostRefreshCalculationCallback(postRefreshCalculationMaxOrigFeeHandler);
            registerPostRefreshCalculationCallback(postRefreshCalculationMaxDiscountPointsHandler);
        });

        function consolidateDuplicateFieldDataBeforeServiceCall(value) {
            $("#sTotalRenovationCosts_2").val(value);
            $("#sTotalRenovationCosts_3").val(value);
        }

        function _init() {
            var loanPurpose = $("#sLPurposeT").val();
            var isPurchase = loanPurpose == 0 || loanPurpose == 3 || loanPurpose == 4; // loan purpose is purchase, construction, or construct perm
            $(".purchase").toggle(isPurchase);
            $(".refinance").toggle(!isPurchase);

            var isFha203kTypeLimited = $("#sFHA203kType").val() == 1;
            $("#sArchitectEngineerFee").prop('readonly', isFha203kTypeLimited);
            $("#sConsultantFee").prop('readonly', isFha203kTypeLimited);
            $("#sFeasibilityStudyFee").prop('readonly', isFha203kTypeLimited);
            $("#sRenoMtgPmntRsrvMnth").prop('readonly', isFha203kTypeLimited);
            $("#sFinanceMortgagePmntRsrvTgtValLckd").prop('disabled', isFha203kTypeLimited);

            lockField(document.getElementById("sFinanceMortgagePmntRsrvTgtValLckd"), "sFinanceMortgagePmntRsrvTgtVal");
            lockField(document.getElementById("sRefPdOffAmt1003Lckd"), "sRefPdOffAmt1003");
            lockField(document.getElementById("sArchitectEngineerFeeLckd"), "sArchitectEngineerFee");
            lockField(document.getElementById("sConsultantFeeLckd"), "sConsultantFee");
            lockField(document.getElementById("sRenovationConstrInspectFeeLckd"), "sRenovationConstrInspectFee");
            lockField(document.getElementById("sTitleUpdateFeeLckd"), "sTitleUpdateFee");
            lockField(document.getElementById("sPermitFeeLckd"), "sPermitFee");
            lockField(document.getElementById("s203kConsultantFeeInitDrawLckd"), "s203kConsultantFeeInitDraw");
            lockField(document.getElementById("sArchitectEngineerFeeInitDrawLckd"), "sArchitectEngineerFeeInitDraw");
            lockField(document.getElementById("sPermitFeeInitDrawLckd"), "sPermitFeeInitDraw");
            lockField(document.getElementById("sOriginationFeeInitDrawLckd"), "sOriginationFeeInitDraw");
            lockField(document.getElementById("sDiscntPointsOnRepairCostFeeInitDrawLckd"), "sDiscntPointsOnRepairCostFeeInitDraw");
            lockField(document.getElementById("sTotalRenovationCostsLckd"), "sTotalRenovationCosts");           

            checkMonth(document.getElementById('sRenoMtgPmntRsrvMnth'));
        }

        function copyToFileBaseLoanAmount() {
            $("#sLAmtCalc").val($("#s203kFinalMaxBaseLoanAmount").val());
            updateDirtyBit();
            refreshCalculation();
        }

        function checkMonth(month) {
            if (month.value > 6) {
                $("#sRenoMtgPmntRsrvMnth").val(6);
            }
        }

        function displayMaxOrigFeePopup() {
            $('.max-orig-fee-popup').dialog({
                dialogClass: "MaxOrigFeePopup",
                width: 525,
                height: 415,
                modal: true,
                title: "Maximum Financeable Origination Fee Calculation",
                resizable: false,
                draggable: false,
                buttons: [
                    {
                        text: 'Close',
                        click: function () { $(this).dialog('close'); }
                    }
                ]
            });
        }

        function displayMaxDiscountPointsPopup() {
            $('.max-discount-points-popup').dialog({
                dialogClass: "MaxDiscountPointsPopup",
                width: 525,
                height: 300,
                modal: true,
                title: "Maximum Financeable Discount Points Calculation",
                resizable: false,
                draggable: false,
                buttons: [
                    {
                        text: 'Close',
                        click: function () { $(this).dialog('close'); }
                    }
                ]
            });
        }
    </script>
        <form id="form1" runat="server">
        <ML:MaxOrigFeePopup runat="server" id="MaxFinanceableOriginationFeePopup"></ML:MaxOrigFeePopup>
        <ML:MaxDiscountPointsPopup runat="server" id="MaxFinanceableDiscountPointsPopup"></ML:MaxDiscountPointsPopup>
        <h1 class="MainRightHeader">203(k) Maximum Mortgage Calculator</h1>
            <div>
                <table class="float-left">
                    <tr>
                        <td class="width150">
                            <label class="FieldLabel">
                                203k Type
                            </label>
                        </td>
                        <td>
                            <asp:DropDownList ID="sFHA203kType" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="FieldLabel">
                                Loan Purpose
                            </label>
                        </td>
                        <td>
                            <asp:DropDownList ID="sLPurposeT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="FieldLabel">
                                Property Type
                            </label>
                        </td>
                        <td>
                            <asp:DropDownList ID="sProdSpT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="FieldLabel">
                                Residence Type
                            </label>
                        </td>
                        <td>
                            <asp:DropDownList ID="sOccT" runat="server" disabled="true"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="refinance">
                        <td>
                            <label class="FieldLabel">
                                Property Acquisition
                            </label>
                        </td>
                        <td>
                            <asp:DropDownList ID="sFHAPropAcquisitionT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="FieldLabel">
                                Decisioning Credit Score
                            </label>
                        </td>
                        <td>
                            <asp:TextBox id="sCreditScoreType2" runat="server" ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <table class="reno-reduction-explanation solidBorder float-left">
                    <tr>
                        <td>
                            <label class="FieldLabel">
                                URLA: Renovation Costs not included in Line F
                            </label>
                        </td>
                        <td>
                            <ML:MoneyTextBox ID="sRenoFeesToReduceUrlaLineF" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="FieldLabel">
                                URLA: Renovation Costs not included in Line H
                            </label>
                        </td>
                        <td>
                            <ML:MoneyTextBox ID="sDiscntPntOnRepairCostFee_Explanation" data-field-id="sDiscntPntOnRepairCostFee" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="FieldLabel">
                                LE/CD: Renovation Costs already included in Closing Cost Details
                            </label>
                        </td>
                        <td>
                            <ML:MoneyTextBox ID="sRenoFeesInClosingCosts" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="stepTable clear-both">
                <table class="width960 solidBorder">
                    <thead>
                        <tr>
                            <th class="alignLeft" colspan="4">
                                Step 1: Establishing Financeable Repair and Improvement Costs, Fees and Reserves
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    A
                                </label>
                            </td>
                            <td>
                                Repair and Improvement Costs and Fees Total <i>(sum of Step A1 thru Step A7)</i>
                            </td>
                            <td class="width120">
                            </td>
                            <td class="width120">
                                <ML:MoneyTextBox id="sRepairImprovementCostFee" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;1. Costs of Construction, Repairs, and Rehabilitation
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sCostConstrRepairsRehab" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;2. Architectural or Engineering Professional Fees
                            </td>
                            <td>
                                <input type="checkbox" id="sArchitectEngineerFeeLckd" runat="server" class="Hidden" />
                                <ML:MoneyTextBox id="sArchitectEngineerFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;3. 203k Consultant Fees
                            </td>
                            <td>
                                <input type="checkbox" id="sConsultantFeeLckd" runat="server" class="Hidden" />
                                <ML:MoneyTextBox id="sConsultantFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;4. Draw Inspection Fees during Construction Period
                            </td>
                            <td>
                                <input type="checkbox" id="sRenovationConstrInspectFeeLckd" runat="server" class="Hidden" />
                                <ML:MoneyTextBox id="sRenovationConstrInspectFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;5. Title Update Fees
                            </td>
                            <td>
                                <input type="checkbox" id="sTitleUpdateFeeLckd" runat="server" class="Hidden" />
                                <ML:MoneyTextBox id="sTitleUpdateFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;6. Permit Fees
                            </td>
                            <td>
                                <input type="checkbox" id="sPermitFeeLckd" runat="server" class="Hidden" />
                                <ML:MoneyTextBox id="sPermitFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;7. Feasibility Study <i>(when necessary)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFeasibilityStudyFee" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>                                
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    B
                                </label>
                            </td>
                            <td>
                                Financeable Contingency Reserves
                            </td>
                            <td>
                                <ML:PercentTextBox id="sContingencyRsrvPcRenovationHardCost" runat="server" class="width90Important" onchange="refreshCalculation();"></ML:PercentTextBox>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFinanceContingencyRsrv" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    C
                                </label>
                            </td>
                            <td>
                                Financeable Mortgage Payment Reserves
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFinanceMortgagePmntRsrv" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Target Value                                
                                <span class="alignRight">
                                    <asp:TextBox ID="sRenoMtgPmntRsrvMnth" runat="server" class="width20" maxlength="1" onchange="refreshCalculation();"></asp:TextBox>
                                    months x 
                                    <ML:MoneyTextBox ID="sMonthlyPmt" runat="server" readonly="true"></ML:MoneyTextBox>
                                    =
                                </span>                                
                            </td>                            
                            <td>
                                <ML:MoneyTextBox id="sFinanceMortgagePmntRsrvTgtVal" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox ID="sFinanceMortgagePmntRsrvTgtValLckd" class="alignCheckBox" runat="server" onchange="refreshCalculation();"/>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Maximum Financeable Mortgage Payment Reserves
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sMaxFinanceMortgagePmntRsrv" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    D
                                </label>
                            </td>
                            <td>
                                Financeable Mortgage Fees Total <i>(sum of Step D1 and Step D2)</i>
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFinanceMortgageFee" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;1. Financeable Origination Fee
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFinanceOriginationFee" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Target Value
                                <span class="alignRight">
                                    <ML:MoneyTextBox id="sFinanceOriginationFeeTgtVal" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                </span>
                            </td>
                            <td>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Maximum Financeable Origination Fee
                                <span class="alignRight">
                                    <a onclick="displayMaxOrigFeePopup();">explain</a>
                                    <ML:MoneyTextBox id="sMaxFinanceOriginationFee" runat="server" readonly="true"></ML:MoneyTextBox>
                                </span>
                            </td>
                            <td>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;2. Discount Points on Repair Costs and Fees
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sDiscntPntOnRepairCostFee" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                            <td>                              
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Target Value
                                <span class="alignRight">
                                    <ML:MoneyTextBox id="sDiscntPntOnRepairCostFeeTgtVal" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                </span>
                            </td>
                            <td>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Maximum Financeable Discount Points
                                <span class="alignRight">
                                    <a onclick="displayMaxDiscountPointsPopup();">explain</a>
                                    <ML:MoneyTextBox id="sMaxDiscntPntOnRepairCostFee" runat="server" readonly="true"></ML:MoneyTextBox>
                                </span>
                            </td>
                            <td>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    E
                                </label>
                            </td>
                            <td>
                                Total Rehabilitation Cost <i>(Step 1 Total) (sum of Steps 1A, 1B, 1C and 1D)</i>
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sTotalRenovationCosts" runat="server" onchange="consolidateDuplicateFieldDataBeforeServiceCall(this.value); refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox ID="sTotalRenovationCostsLckd" runat="server" class="alignCheckBox" onclick="refreshCalculation();" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="width960 solidBorder purchase">
                    <thead>
                        <tr>
                            <th class="alignLeft" colspan="4">
                                Step 2: Establishing Value
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    A
                                </label>
                            </td>
                            <td>
                                Purchase Price
                            </td>
                            <td class="width120">
                                <ML:MoneyTextBox id="sPurchPrice" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    B
                                </label>
                            </td>
                            <td>
                                Inducements to Purchase
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sInducementPurchPrice" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    C
                                </label>
                            </td>
                            <td>
                                Purchase Price Minus Inducements to Purchase
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sPurchPriceLessInducement" runat="server" ReadOnly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    D
                                </label>
                            </td>
                            <td>
                                As-Is Property Value <i>(when an As-Is Appraisal is performed)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFHASpAsIsVal" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    E
                                </label>
                            </td>
                            <td>
                                Adjusted As-Is Value = Lesser of Step 2C and 2D
                            </td>
                            <td>
                                <ML:MoneyTextBox id="s203kFHASpAdjAsIsValue" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    F
                                </label>
                            </td>
                            <td>
                                After-Improved Value <i>(subject to repairs and improvements)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sApprVal" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="width960 solidBorder refinance">
                    <thead>
                        <tr>
                            <th class="alignLeft" colspan="4">
                                Step 2: Establishing Value
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    A
                                </label>
                            </td>
                            <td>
                                Existing Debt on the Subject Property
                            </td>
                            <td class="width120">
                                <ML:MoneyTextBox id="sRefPdOffAmt1003" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox ID="sRefPdOffAmt1003Lckd" runat="server" class="alignCheckBox" onclick="refreshCalculation();" />
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    B
                                </label>
                            </td>
                            <td>
                                Total Rehabilitation Cost <i>(Step 1E)</i>
                            </td>
                            <td class="width120">
                                <ML:MoneyTextBox data-field-id="sTotalRenovationCosts" id="sTotalRenovationCosts_2" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    C
                                </label>
                            </td>
                            <td>
                                Fees Associated with the New Loan
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFeeNewLoan" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    D
                                </label>
                            </td>
                            <td>
                                Sum of Step 2A, Step 2B, and Step 2C
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFHA203kSum2AExistingDebtOnTheSubjectPropAnd2BTotalRehabilitationCostAnd2CFeeAssociatedWithTheNewLoan" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    E
                                </label>
                            </td>
                            <td>
                                As-Is Property Value <i>(when an As-Is Appraisal is performed)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox data-field-id="sFHASpAsIsVal" id="sFHASpAsIsVal_2" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    F
                                </label>
                            </td>
                            <td>
                                Adjusted As-Is Value
                            </td>
                            <td>
                                <ML:MoneyTextBox data-field-id="s203kFHASpAdjAsIsValue" id="s203kFHASpAdjAsIsValue_2" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    G
                                </label>
                            </td>
                            <td>
                                After-Improved Value <i>(subject to repairs and improvements)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox data-field-id="sApprVal" id="sApprVal_2" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="width960 solidBorder purchase">
                    <thead>
                        <tr>
                            <th class="alignLeft" colspan="5">
                                Step 3: Calculating Maximum Mortgage
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    A
                                </label>
                            </td>
                            <td colspan="2">
                                Total Rehabilitation Costs plus Adjusted As-Is Value
                            </td>
                            <td class="width120">
                            </td>
                            <td class="width120">
                                <ML:MoneyTextBox id="sTotalRehabCostPlusAdjAsIsValue" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    B
                                </label>
                            </td>
                            <td colspan="2">
                                After-Improved Value x 110% <i>(100% for condos)</i>
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFHAAdjPropTypeAfterImproveValue" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    C
                                </label>
                            </td>
                            <td colspan="2">
                                Less Lead-Based Paint Credit
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFHA203kLeadPaintCredit" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    D
                                </label>
                            </td>
                            <td colspan="2">
                                Lesser of Step 3A or Step 3B x Step 3G (Appropriate LTV Factor) Minus Step 3C
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFHA203ProductLtvMaxMortgageAndMinAppraisedValue" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    E
                                </label>
                            </td>
                            <td colspan="2">
                                Nationwide Mortgage Limit
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sNationWideMortgageLimit" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    F
                                </label>
                            </td>
                            <td colspan="2">
                                Initial Base Loan Amount
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="s203kInitMaxBaseLoanAmount" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    G
                                </label>
                            </td>
                            <td colspan="2">
                                LTV Factor for Maximum Mortgage Eligibility
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:PercentTextBox id="sLtv203KMaxMortgageEligibility" runat="server" readonly="true"></ML:PercentTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                Basis
                            </td>
                            <td>
                                Criteria
                            </td>
                            <td colspan="2">
                                Maximum LTV Factor
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                MDCS
                            </td>
                            <td>
                                At or Above 580
                            </td>
                            <td colspan="2">
                                96.5%
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                MDCS
                            </td>
                            <td>
                                Between 500 and 579
                            </td>
                            <td colspan="2">
                                90%
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                Secondary Residence
                            </td>
                            <td>
                                With HOC Approval
                            </td>
                            <td colspan="2">
                                85%
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                No Credit Score
                            </td>
                            <td>
                                Manual Underwriting Required
                            </td>
                            <td colspan="2">
                                96.5%
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="width960 solidBorder refinance">
                    <thead>
                        <tr>
                            <th class="alignLeft" colspan="5">
                                Step 3: Calculating Maximum Mortgage
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    A
                                </label>
                            </td>
                            <td colspan="2">
                                Step 2D <i>(sum of 2A, 2B and 2C)</i>
                            </td>
                            <td class="width120">
                            </td>
                            <td class="width120">
                                <ML:MoneyTextBox data-field-id="sFHA203kSum2AExistingDebtOnTheSubjectPropAnd2BTotalRehabilitationCostAnd2CFeeAssociatedWithTheNewLoan" id="sFHA203kSum2AExistingDebtOnTheSubjectPropAnd2BTotalRehabilitationCostAnd2CFeeAssociatedWithTheNewLoan_2" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    B
                                </label>
                            </td>
                            <td colspan="2">
                                Total Rehabilitation Costs plus Adjusted As-Is Value <i>(sum of Step 2F + Step 1E)</i>
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox data-field-id="sTotalRehabCostPlusAdjAsIsValue" id="sTotalRehabCostPlusAdjAsIsValue_2" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    C
                                </label>
                            </td>
                            <td colspan="2">
                                After-Improved Value <i>(Step 2G) X 110% (100% for condos)</i>
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox data-field-id="sFHAAdjPropTypeAfterImproveValue" id="sFHAAdjPropTypeAfterImproveValue_2" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    D
                                </label>
                            </td>
                            <td colspan="2">
                                Lesser of Step 3B or Step 3C x Step 3G <i>(Appropriate LTV Factor)</i>
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox data-field-id="sFHA203ProductLtvMaxMortgageAndMinAppraisedValue" id="sFHA203ProductLtvMaxMortgageAndMinAppraisedValue_2" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    E
                                </label>
                            </td>
                            <td colspan="2">
                                Nationwide Mortgage Limit
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox data-field-id="sNationWideMortgageLimit" id="sNationWideMortgageLimit_2" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    F
                                </label>
                            </td>
                            <td colspan="2">
                                <i>Initial</i> Maximum Base Loan Amount = Lesser of Step 3A, 3D, or 3E
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox data-field-id="s203kInitMaxBaseLoanAmount" id="s203kInitMaxBaseLoanAmount_2" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    G
                                </label>
                            </td>
                            <td colspan="2">
                                LTV Factor for Maximum Mortgage Eligibility
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:PercentTextBox data-field-id="sLtv203KMaxMortgageEligibility" id="sLtv203KMaxMortgageEligibility_2" runat="server" readonly="true"></ML:PercentTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                Basis
                            </td>
                            <td>
                                Criteria
                            </td>
                            <td colspan="2">
                                Maximum LTV Factor
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                MDCS
                            </td>
                            <td>
                                At or Above 580
                            </td>
                            <td colspan="2">
                                97.5%
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                MDCS
                            </td>
                            <td>
                                Between 500 and 579
                            </td>
                            <td colspan="2">
                                90%
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                Secondary Residence
                            </td>
                            <td>
                                With HOC Approval
                            </td>
                            <td colspan="2">
                                85%
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                No Credit Score
                            </td>
                            <td>
                                Manual Underwriting Required
                            </td>
                            <td colspan="2">
                                97.5%
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="width960 solidBorder">
                    <thead>
                        <tr>
                            <th class="alignLeft" colspan="5">
                                Step 4: Additions to Base Mortgage Amount
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    A
                                </label>
                            </td>
                            <td>
                                Energy Efficient Improvement Amount <i>(EEM)</i>
                            </td>
                            <td class="width120">
                            </td>
                            <td class="width120">
                                <ML:MoneyTextBox id="sEnergyImprovementAmount" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    B
                                </label>
                            </td>
                            <td>
                                Intermediate Base Loan Amount = Step 3F + Step 4A
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="s203kIntermediateMaxBaseLoanAmount" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    C
                                </label>
                            </td>
                            <td>
                                Solar Energy System Cost and Installation
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sSolarEnergySystemCost" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    D
                                </label>
                            </td>
                            <td>
                                After-Improved Value x 20%
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sFHASpAfterImproveValBy20Pc" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    E
                                </label>
                            </td>
                            <td>
                                Solar Energy Amount to Be Added to Base Loan Amount = Lesser of Step 4C or Step 4D
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sAdjSolarEnergyAmount" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    F
                                </label>
                            </td>
                            <td>
                                National Mortgage Limit x 120%
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sNationWideMortgageLimitBy120Pc" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    G
                                </label>
                            </td>
                            <td>
                                Final Base Loan Amount = Lesser of (sum of step 4B and Step 4E) or Step 4F
                                <input class="alignRight" type="button" value="Copy to File Base Loan Amount" onclick="copyToFileBaseLoanAmount();"/>
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="s203kFinalMaxBaseLoanAmount" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </div>
                <table class="width960">
                    <tr>
                        <td class="width840 alignRight">
                            <label class="FieldLabel">
                                Loan Amount&nbsp;&nbsp;
                            </label>
                        </td>
                        <td class="width120">
                            <ML:MoneyTextBox class="width120" id="sLAmtCalc" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                        </td>
                    </tr>                    
                </table>                
                <div class="stepTable">
                <table class="width960 solidBorder">
                    <thead>
                        <tr>
                            <th class="alignLeft" colspan="5">
                                Step 5: Calculating the LTV
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    A
                                </label>
                            </td>
                            <td>
                                MIP LTV = Loan Amount Divided by the After-Improved Value
                            </td>
                            <td class="width120">
                            </td>
                            <td class="width120">
                                <ML:PercentTextBox id="sFHAMipLtv" runat="server" ReadOnly="true"></ML:PercentTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    B
                                </label>
                            </td>
                            <td>
                                Case LTV = Loan Amount divided by the lesser of 
                                <span class="purchase">Step 3A or Step 3B</span>
                                <span class="refinance">Step 3B or Step 3C</span>
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:PercentTextBox id="sLtvR" runat="server" ReadOnly="true"></ML:PercentTextBox>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="width960 solidBorder">
                    <thead>
                        <tr>
                            <th class="alignLeft" colspan="5">
                                Step 6: Establishing the Rehabilitation Escrow Amount
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    A
                                </label>
                            </td>
                            <td>
                                Total Rehabilitation Cost <i>(Step 1 Total)</i>
                            </td>
                            <td class="width120">
                                <ML:MoneyTextBox data-field-id="sTotalRenovationCosts" id="sTotalRenovationCosts_3" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                            <td class="width120">
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    B
                                </label>
                            </td>
                            <td>
                                Cost of Energy Efficient Improvement Amount <i>(Step 4A)</i>
                            </td>
                            <td class="width120">
                                <ML:MoneyTextBox data-field-id="sEnergyImprovementAmount" id="sEnergyImprovementAmount_2" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    C
                                </label>
                            </td>
                            <td>
                                Cost of Financed Solar Energy Systems Improvement <i>(Step 4E)</i>
                            </td>
                            <td class="width120">
                                <ML:MoneyTextBox data-field-id="sAdjSolarEnergyAmount" id="sAdjSolarEnergyAmount_2" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    D
                                </label>
                            </td>
                            <td>
                                Borrower's Own Funds for Contingency Reserves
                            </td>
                            <td class="width120">
                                <ML:MoneyTextBox id="sBorrOwnFundsContingencyRsrv" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    E
                                </label>
                            </td>
                            <td>
                                Rehabilitation Escrow Amount Total
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="s203KRehabEscrowAmtTot" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    F
                                </label>
                            </td>
                            <td>
                                Initial Draw at Closing Total
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="s203KRehabEscrowInitDrawAmt" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;1. 203k Consultant Fees
                            </td>
                            <td>
                                <ML:MoneyTextBox id="s203kConsultantFeeInitDraw" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox ID="s203kConsultantFeeInitDrawLckd" class="alignCheckBox" runat="server" onchange="refreshCalculation();"/>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;2. Architectural or Engineering Professional Fees
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sArchitectEngineerFeeInitDraw" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox ID="sArchitectEngineerFeeInitDrawLckd" class="alignCheckBox" runat="server" onchange="refreshCalculation();"/>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;3. Permit Fees
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sPermitFeeInitDraw" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox ID="sPermitFeeInitDrawLckd" class="alignCheckBox" runat="server" onchange="refreshCalculation();"/>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;4. Origination Fees
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sOriginationFeeInitDraw" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox ID="sOriginationFeeInitDrawLckd" class="alignCheckBox" runat="server" onchange="refreshCalculation();"/>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;5. Discount Points on Repair Costs and Fees
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sDiscntPointsOnRepairCostFeeInitDraw" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                                <asp:CheckBox ID="sDiscntPointsOnRepairCostFeeInitDrawLckd" class="alignCheckBox" runat="server" onchange="refreshCalculation();"/>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;6. Material Cost for Items Ordered and Prepaid by Borrower or Contractor <i>(under contract for delivery)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sMatCostOrderedAndPrepaidInitDraw" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;7. Up to 50% of Material Cost for Items Ordered but Not Yet Paid for <i>(under contract for delivery)</i>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="s50PcMatCostOrderedButNotPaidInitDraw" runat="server" onchange="refreshCalculation();"></ML:MoneyTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="alignCenter width40">
                                <label class="FieldLabel">
                                    G
                                </label>
                            </td>
                            <td>
                                Rehabilitation Escrow Amount Balance <i>(for future draws)</i> = Step 6E Minus Step 6F
                            </td>
                            <td>
                            </td>
                            <td>
                                <ML:MoneyTextBox id="sTotalRehabEscrowAmount" runat="server" readonly="true"></ML:MoneyTextBox>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </body>
</html>
