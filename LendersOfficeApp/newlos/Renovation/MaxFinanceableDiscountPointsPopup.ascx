﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MaxFinanceableDiscountPointsPopup.ascx.cs" Inherits="LendersOfficeApp.newlos.Renovation.MaxFinanceableDiscountPointsPopup" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<div id="MaxDiscountPointsPopup" class="max-discount-points-popup" runat="server">
    <table class="max-discount-points-table">
        <tr>
            <td>
                1. Discount Point Percentage
            </td>
            <td>
                <ml:MoneyTextBox runat="server" ID="DiscountPointPercentage" ReadOnly="true"></ml:MoneyTextBox>
            </td>
        </tr>
        <tr>
            <td>
                2. Repair Costs and Fees
            </td>
            <td>
                <ml:MoneyTextBox runat="server" ID="RepairCostsAndFees" ReadOnly="true"></ml:MoneyTextBox>
            </td>
        </tr>
        <tr>
            <td>
                3. Contingency Reserves
            </td>
            <td>
                <ml:MoneyTextBox runat="server" ID="ContingencyReserves" ReadOnly="true"></ml:MoneyTextBox>
            </td>
        </tr>
        <tr>
            <td>
                4. Mortgage Payment Reserves
            </td>
            <td>
                <ml:MoneyTextBox runat="server" ID="MortgagePaymentReserves" ReadOnly="true"></ml:MoneyTextBox>
            </td>
        </tr>
        <tr>
            <td>
                5. Step 2 + Step 3 + Step 4
            </td>
            <td>
                <ml:MoneyTextBox runat="server" ID="SumRepairCostsFeesAndReserves" ReadOnly="true"></ml:MoneyTextBox>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Maximum Financeable Discount Points (Step 1 x Step 5)</strong>
            </td>
            <td>
                <ml:MoneyTextBox runat="server" ID="MaxFinanceableDiscountPointsAmount" ReadOnly="true"></ml:MoneyTextBox>
            </td>
        </tr>
    </table>
</div>
