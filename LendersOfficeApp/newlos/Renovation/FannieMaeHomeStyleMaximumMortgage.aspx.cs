﻿#region Auto-generated code
namespace LendersOfficeApp.newlos.Renovation
#endregion
{
    using DataAccess;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    /// <summary>
    /// Page for Fannie Mae HomeStyle maximum mortgage renovation loan.
    /// </summary>
    public partial class FannieMaeHomeStyleMaximumMortgage : BaseLoanPage
    {
        /// <summary>
        /// Load function of the page.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Object event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageTitle = "Fannie Mae HomeStyle Maximum Mortgage";
            this.PageID = "FannieMaeHomeStyleMaximumMortgage";

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FannieMaeHomeStyleMaximumMortgage));
            dataLoan.InitLoad();

            this.MaxFinanceableOriginationFeePopup.LoadData(dataLoan);
            this.MaxFinanceableDiscountPointsPopup.LoadData(dataLoan);

            bool isPurchase = dataLoan.sLPurposeT == E_sLPurposeT.Purchase || dataLoan.sLPurposeT == E_sLPurposeT.Construct || dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm;

            this.RegisterJsGlobalVariables("isPurchase", isPurchase);

            this.sRenoFeesToReduceUrlaLineF.Text = dataLoan.sRenoFeesToReduceUrlaLineF_rep;
            this.sDiscntPntOnRepairCostFee_Explanation.Text = dataLoan.sDiscntPntOnRepairCostFee_rep;
            this.sRenoFeesInClosingCosts.Text = dataLoan.sRenoFeesInClosingCosts_rep;

            // Section A
            this.sRenovationLoanTgtLtv.Text = dataLoan.sRenovationLoanTgtLtv_rep; // A.1
            this.sHomeStyleMaxRenovCost.Text = dataLoan.sHomeStyleMaxRenovCost_rep; // A.2
            Tools.SetDropDownListValue(this.sOccT, dataLoan.sOccT); // A.3

            // Section B
            this.sPurchPrice.Text = dataLoan.sPurchPrice_rep; // B.1
            this.sRefPdOffAmt1003.Text = dataLoan.sRefPdOffAmt1003_rep; // B.2
            this.sRefPdOffAmt1003Lckd.Checked = dataLoan.sRefPdOffAmt1003Lckd; // B.2 Lckd checkbox
            this.sApprVal.Text = dataLoan.sApprVal_rep; // B.3

            // Section C
            this.sCostConstrRepairsRehab.Text = dataLoan.sCostConstrRepairsRehab_rep; // C.1.a
            this.sContingencyRsrvPcRenovationHardCost.Text = dataLoan.sContingencyRsrvPcRenovationHardCost_rep; // C.1.b percentage
            this.sFinanceContingencyRsrv.Text = dataLoan.sFinanceContingencyRsrv_rep; // C.1.b
            this.sArchitectEngineerFeeLckd.Checked = !Tools.GetRenovationFeePaymentsMatchingMismoType(dataLoan.sClosingCostSet, E_ClosingCostFeeMismoFeeT._203KArchitecturalAndEngineeringFee).Any(); // C.1.c
            this.sArchitectEngineerFee.Text = dataLoan.sArchitectEngineerFee_rep; // C.1.c
            this.sConsultantFeeLckd.Checked = !Tools.GetRenovationFeePaymentsMatchingMismoType(dataLoan.sClosingCostSet, E_ClosingCostFeeMismoFeeT._203KConsultantFee).Any(); // C.1.d
            this.sConsultantFee.Text = dataLoan.sConsultantFee_rep; // C.1.d
            this.sRenovationConstrInspectFeeLckd.Checked = !Tools.GetRenovationFeePaymentsMatchingMismoType(dataLoan.sClosingCostSet, E_ClosingCostFeeMismoFeeT._203KInspectionFee).Any(); // C.1.e
            this.sRenovationConstrInspectFee.Text = dataLoan.sRenovationConstrInspectFee_rep; // C.1.e
            this.sTitleUpdateFeeLckd.Checked = !Tools.GetRenovationFeePaymentsMatchingMismoType(dataLoan.sClosingCostSet, E_ClosingCostFeeMismoFeeT._203KTitleUpdate).Any(); // C.1.f
            this.sTitleUpdateFee.Text = dataLoan.sTitleUpdateFee_rep; // C.1.f
            this.sPermitFeeLckd.Checked = !Tools.GetRenovationFeePaymentsMatchingMismoType(dataLoan.sClosingCostSet, E_ClosingCostFeeMismoFeeT._203KPermits).Any(); // C.1.g
            this.sPermitFee.Text = dataLoan.sPermitFee_rep; // C.1.g
            this.sFinanceMortgagePmntRsrv.Text = dataLoan.sFinanceMortgagePmntRsrv_rep; // C.1.h
            this.sRenoMtgPmntRsrvMnth.Text = dataLoan.sRenoMtgPmntRsrvMnth_rep; // C.1.h months
            this.sMonthlyPmt.Text = dataLoan.sMonthlyPmt_rep; // C.1.h months multiplier
            this.sFinanceMortgagePmntRsrvTgtVal.Text = dataLoan.sFinanceMortgagePmntRsrvTgtVal_rep; // C.1.h Target
            this.sFinanceMortgagePmntRsrvTgtValLckd.Checked = dataLoan.sFinanceMortgagePmntRsrvTgtValLckd; // C.1.h Target Locked box
            this.sMaxFinanceMortgagePmntRsrv.Text = dataLoan.sMaxFinanceMortgagePmntRsrv_rep; // C.1.h Max value
            this.sOtherRenovationCost.Text = dataLoan.sOtherRenovationCost_rep; // C.1.i
            this.sOtherRenovationCostDescription.Text = dataLoan.sOtherRenovationCostDescription; // C.1.i Description
            this.sFinanceOriginationFee.Text = dataLoan.sFinanceOriginationFee_rep; // C.1.i financeable origination fee
            this.sFinanceOriginationFeeTgtVal.Text = dataLoan.sFinanceOriginationFeeTgtVal_rep; // C.1.i financeable origination fee Target
            this.sMaxFinanceOriginationFee.Text = dataLoan.sMaxFinanceOriginationFee_rep; // C.1.i financeable origination fee Max value
            this.sDiscntPntOnRepairCostFee.Text = dataLoan.sDiscntPntOnRepairCostFee_rep; // C.1.i discount points
            this.sDiscntPntOnRepairCostFeeTgtVal.Text = dataLoan.sDiscntPntOnRepairCostFeeTgtVal_rep; // C.1.i discount points Target
            this.sMaxDiscntPntOnRepairCostFee.Text = dataLoan.sMaxDiscntPntOnRepairCostFee_rep; // C.1.i. discount points Max value
            this.sFeasibilityStudyFee.Text = dataLoan.sFeasibilityStudyFee_rep; // C.1.i feasibility study
            this.sTotalRenovationCosts.Text = dataLoan.sTotalRenovationCosts_rep; // C.2
            this.sTotalRenovationCostsLckd.Checked = dataLoan.sTotalRenovationCostsLckd; // C.2 Lckd checkbox

            // Section D
            this.sRenovationPurchPricePlusTotalRenovationCosts.Text = dataLoan.sRenovationPurchPricePlusTotalRenovationCosts_rep; // D.1
            this.sApprVal_2.Text = dataLoan.sApprVal_rep; // D.2
            this.sFfUfmipFinanced.Text = dataLoan.sFfUfmipFinanced_rep; // D.3
            this.sRenovationMaxMtgAmt.Text = dataLoan.sRenovationMaxMtgAmt_rep; // D.4
            this.sRenovationMaxMtgAmt_2.Text = dataLoan.sRenovationMaxMtgAmt_rep; // D.5

            // Section E
            this.sPurchPrice_2.Text = dataLoan.sPurchPrice_rep; // E.1
            this.sAltCost.Text = dataLoan.sAltCost_rep; // E.2
            this.sAltCostLckd.Checked = dataLoan.sAltCostLckd; // E.2 Lckd checkbox
            this.sLandCost.Text = dataLoan.sLandCost_rep; // E.3
            this.sRefPdOffAmt1003_2.Text = dataLoan.sRefPdOffAmt1003_rep; // E.4
            this.sRefPdOffAmt1003Lckd_2.Checked = dataLoan.sRefPdOffAmt1003Lckd; // E.4 Lckd checkbox
            this.sTotEstPp1003.Text = dataLoan.sTotEstPp1003_rep; // E.5
            this.sTotEstPp1003Lckd.Checked = dataLoan.sTotEstPp1003Lckd; // E.5 Lckd checkbox
            this.sTotEstCcNoDiscnt1003.Text = dataLoan.sTotEstCcNoDiscnt1003_rep; // E.6
            this.sTotEstCc1003Lckd.Checked = dataLoan.sTotEstCc1003Lckd; // E.6 Lckd checkbox
            this.sFfUfmipFinanced_2.Text = dataLoan.sFfUfmipFinanced_rep; // E.7
            this.sLDiscnt1003.Text = dataLoan.sLDiscnt1003_rep; // E.8
            this.sLDiscnt1003Lckd.Checked = dataLoan.sLDiscnt1003Lckd; // E.8 Lckd checkbox
            this.sTotTransC.Text = dataLoan.sTotTransC_rep; // E.9
            this.sONewFinBal.Text = dataLoan.sONewFinBal_rep; // E.10
            this.sTotCcPbs.Text = dataLoan.sTotCcPbs_rep; // E.11
            this.sTotCcPbsLocked.Checked = dataLoan.sTotCcPbsLocked; // E.11 Locked checkbox
            this.sRenovationOtherCredits.Text = dataLoan.sRenovationOtherCredits_rep; // E.12
            this.sFinalLAmt.Text = dataLoan.sFinalLAmt_rep; // E.13a
            this.sLAmtCalc.Text = dataLoan.sLAmtCalc_rep; // E.13b
            this.sRenovationTtlFundsAvailableToBorrower.Text = dataLoan.sRenovationTtlFundsAvailableToBorrower_rep; // E.14
            this.sTransNetCash.Text = dataLoan.sTransNetCash_rep; // E.15
            this.sTransNetCashLckd.Checked = dataLoan.sTransNetCashLckd; // E.15 Lckd checkbox
        }

        /// <summary>
        /// The code that runs when the page is initialized.
        /// </summary>
        protected void Page_Init()
        {
            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("loanframework2.js");
            this.RegisterService("loanedit", "/newlos/Renovation/FannieMaeHomeStyleMaximumMortgageService.aspx");            

            Tools.Bind_sOccT(this.sOccT);
        }
    }
}