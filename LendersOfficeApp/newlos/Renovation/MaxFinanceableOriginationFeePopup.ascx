﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MaxFinanceableOriginationFeePopup.ascx.cs" Inherits="LendersOfficeApp.newlos.Renovation.MaxFinanceableOriginationFeePopup" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<div id="MaxOrigFeePopup" class="max-orig-fee-popup" runat="server">
    <table class="max-orig-fee-table">
        <tr>
            <td>
                1. Origination Fee
            </td>
            <td>
                <ml:MoneyTextBox runat="server" ID="OriginationFeeAmount" ReadOnly="true"></ml:MoneyTextBox>
            </td>
        </tr>
        <tr>
            <td>
                2. Supplemental Origination Fee
            </td>
            <td>
                <ml:MoneyTextBox runat="server" ID="SupplementalOriginationFeeAmount" ReadOnly="true"></ml:MoneyTextBox>
            </td>
        </tr>
        <tr>
            <td>
                3. Step 1 + Step 2
            </td>
            <td>
                <ml:MoneyTextBox runat="server" ID="SumOriginationFees" ReadOnly="true"></ml:MoneyTextBox>
            </td>
        </tr>
        <tr>
            <td>
                4. Repair Costs and Fees
            </td>
            <td>
                <ml:MoneyTextBox runat="server" ID="RepairCostsAndFees" ReadOnly="true"></ml:MoneyTextBox>
            </td>
        </tr>
        <tr>
            <td>
                5. Contingency Reserves
            </td>
            <td>
                <ml:MoneyTextBox runat="server" ID="ContingencyReserves" ReadOnly="true"></ml:MoneyTextBox>
            </td>
        </tr>
        <tr>
            <td>
                6. Mortgage Payment Reserves
            </td>
            <td>
                <ml:MoneyTextBox runat="server" ID="MortgagePaymentReserves" ReadOnly="true"></ml:MoneyTextBox>
            </td>
        </tr>
        <tr>
            <td>
                7. Step 4 + Step 5 + Step 6
            </td>
            <td>
                <ml:MoneyTextBox runat="server" ID="SumRepairCostsFeesAndReserves" ReadOnly="true"></ml:MoneyTextBox>
            </td>
        </tr>
        <tr>
            <td>
                8. 1.500% x Step 7
            </td>
            <td>
                <ml:MoneyTextBox runat="server" ID="PercentageOfRepairCostReservesSum" ReadOnly="true"></ml:MoneyTextBox>
            </td>
        </tr>
        <tr>
            <td>
                9. Greater of $350.00 and Step 8
            </td>
            <td>
                <ml:MoneyTextBox runat="server" ID="GreaterOfRepairCostReserveSumPercentageAndFixedAmount" ReadOnly="true"></ml:MoneyTextBox>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Maximum Financeable Origination Fee (Lesser of Step 3 and Step 9)</strong>
            </td>
            <td>
                <ml:MoneyTextBox runat="server" ID="MaxFinanceableOrigFeeAmount" ReadOnly="true"></ml:MoneyTextBox>
            </td>
        </tr>
    </table>
</div>