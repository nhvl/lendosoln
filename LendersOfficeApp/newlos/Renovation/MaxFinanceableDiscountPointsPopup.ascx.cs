﻿#region Generated code
namespace LendersOfficeApp.newlos.Renovation
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Provides a breakdown of the maximum financeable discount point calculation
    /// for renovation loan files.
    /// </summary>
    public partial class MaxFinanceableDiscountPointsPopup : System.Web.UI.UserControl
    {
        /// <summary>
        /// Loads the control data for the service item.
        /// </summary>
        /// <param name="loan">
        /// The loan to load.
        /// </param>
        /// <param name="service">
        /// The service item to send the data through.
        /// </param>
        public static void LoadData(CPageData loan, AbstractBackgroundServiceItem service)
        {
            Func<string, string> keyGenerator = key => nameof(MaxFinanceableDiscountPointsPopup) + "_" + key;

            var data = new MaxFinanceableOriginationFeePopupData();
            data.PopulateFrom(loan);

            service.SetResult(keyGenerator(nameof(data.DiscountPointPercentage)), data.DiscountPointPercentage);
            service.SetResult(keyGenerator(nameof(data.RepairCostsAndFees)), data.RepairCostsAndFees);
            service.SetResult(keyGenerator(nameof(data.ContingencyReserves)), data.ContingencyReserves);
            service.SetResult(keyGenerator(nameof(data.MortgagePaymentReserves)), data.MortgagePaymentReserves);
            service.SetResult(keyGenerator(nameof(data.SumRepairCostsFeesAndReserves)), data.SumRepairCostsFeesAndReserves);
            service.SetResult(keyGenerator(nameof(data.MaxFinanceableDiscountPointsAmount)), data.MaxFinanceableDiscountPointsAmount);
        }

        /// <summary>
        /// Loads data for the popup.
        /// </summary>
        /// <param name="loan">
        /// The loan to load data.
        /// </param>
        public void LoadData(CPageData loan)
        {
            var data = new MaxFinanceableOriginationFeePopupData();
            data.PopulateFrom(loan);

            this.DiscountPointPercentage.Text = data.DiscountPointPercentage;
            this.RepairCostsAndFees.Text = data.RepairCostsAndFees;
            this.ContingencyReserves.Text = data.ContingencyReserves;
            this.MortgagePaymentReserves.Text = data.MortgagePaymentReserves;
            this.SumRepairCostsFeesAndReserves.Text = data.SumRepairCostsFeesAndReserves;
            this.MaxFinanceableDiscountPointsAmount.Text = data.MaxFinanceableDiscountPointsAmount;
        }

        /// <summary>
        /// Initializes the control.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="args">
        /// The parameter is not used.
        /// </param>
        protected void Page_Init(object sender, EventArgs args)
        {
            var basePage = this.Page as BasePage;
            if (basePage == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Max financeable origination fee popup is expected to be hosted on a BasePage.");
            }
            else
            {
                basePage.RegisterJsGlobalVariables("MaxDiscountPointsClientId", this.ClientID);
                basePage.RegisterJsGlobalVariables("MaxDiscountPointsResultPrefix", nameof(MaxFinanceableDiscountPointsPopup));

                basePage.RegisterJsScript("MaxFinanceableDiscountPointsPopup.js");

                basePage.RegisterCSS("MaxFinanceableDiscountPointsPopup.css");
            }
        }

        /// <summary>
        /// Provides a container for max financeable discount points data.
        /// </summary>
        private class MaxFinanceableOriginationFeePopupData
        {
            /// <summary>
            /// Gets the discount point percentage.
            /// </summary>
            public string DiscountPointPercentage { get; private set; }

            /// <summary>
            /// Gets the repair costs and fees.
            /// </summary>
            public string RepairCostsAndFees { get; private set; }

            /// <summary>
            /// Gets the contingency reserves.
            /// </summary>
            public string ContingencyReserves { get; private set; }

            /// <summary>
            /// Gets the mortgage payment reserves.
            /// </summary>
            public string MortgagePaymentReserves { get; private set; }

            /// <summary>
            /// Gets the sum of the repair costs, contingency reserves, and mortgage payment reserves.
            /// </summary>
            public string SumRepairCostsFeesAndReserves { get; private set; }

            /// <summary>
            /// Gets the maximum financeable discount points amount.
            /// </summary>
            public string MaxFinanceableDiscountPointsAmount { get; private set; }

            /// <summary>
            /// Populates data from the loan.
            /// </summary>
            /// <param name="loan">
            /// The loan to populate data.
            /// </param>
            public void PopulateFrom(CPageData loan)
            {
                Func<decimal, string> moneyToRep = value => loan.m_convertLos.ToMoneyString(value, FormatDirection.ToRep);

                var discountPointPercentage = loan.sGfeDiscountPointFPc;
                this.DiscountPointPercentage = loan.m_convertLos.ToRateString(discountPointPercentage);

                var repairCost = loan.sRepairImprovementCostFee;
                var financeContingencyReserves = loan.sFinanceContingencyRsrv;
                var financeMortgageReserves = loan.sFinanceMortgagePmntRsrv;

                this.RepairCostsAndFees = moneyToRep(repairCost);
                this.ContingencyReserves = moneyToRep(financeContingencyReserves);
                this.MortgagePaymentReserves = moneyToRep(financeMortgageReserves);

                var feeReserveSum = Tools.SumMoney(new[] { repairCost, financeContingencyReserves, financeMortgageReserves });
                this.SumRepairCostsFeesAndReserves = moneyToRep(feeReserveSum);

                var maxFinanceableDiscountPointAmount = (discountPointPercentage / 100M) * feeReserveSum;
                this.MaxFinanceableDiscountPointsAmount = moneyToRep(maxFinanceableDiscountPointAmount);
            }
        }
    }
}