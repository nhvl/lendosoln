﻿#region Auto-generated code
namespace LendersOfficeApp.newlos.Renovation
#endregion
{
    using DataAccess;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    /// <summary>
    /// Page for FHA 203k maximum mortgage renovation loan.
    /// </summary>
    public partial class FHA203kMaximumMortgage : BaseLoanPage
    {
        /// <summary>
        /// Load function of the page.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Object event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageTitle = "FHA 203(k) Maximum Mortgage";
            this.PageID = "FHA203kMaximumMortgage";

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHA203kMaximumMortgage));
            dataLoan.InitLoad();

            Tools.SetDropDownListValue(this.sFHA203kType, dataLoan.sFHA203kType);
            Tools.SetDropDownListValue(this.sLPurposeT, dataLoan.sLPurposeT);
            Tools.SetDropDownListValue(this.sProdSpT, dataLoan.sProdSpT);
            Tools.SetDropDownListValue(this.sOccT, dataLoan.sOccT);

            this.MaxFinanceableOriginationFeePopup.LoadData(dataLoan);
            this.MaxFinanceableDiscountPointsPopup.LoadData(dataLoan);

            bool isPurchase = dataLoan.sLPurposeT == E_sLPurposeT.Purchase || dataLoan.sLPurposeT == E_sLPurposeT.Construct || dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm;

            if (!isPurchase)
            {
                Tools.SetDropDownListValue(this.sFHAPropAcquisitionT, dataLoan.sFHAPropAcquisitionT);
            }

            this.sCreditScoreType2.Text = dataLoan.sCreditScoreType2_rep;

            this.sRenoFeesToReduceUrlaLineF.Text = dataLoan.sRenoFeesToReduceUrlaLineF_rep;
            this.sDiscntPntOnRepairCostFee_Explanation.Text = dataLoan.sDiscntPntOnRepairCostFee_rep;
            this.sRenoFeesInClosingCosts.Text = dataLoan.sRenoFeesInClosingCosts_rep;

            // Step 1
            this.sRepairImprovementCostFee.Text = dataLoan.sRepairImprovementCostFee_rep; // A
            this.sCostConstrRepairsRehab.Text = dataLoan.sCostConstrRepairsRehab_rep; // A.1
            this.sArchitectEngineerFee.Text = dataLoan.sArchitectEngineerFee_rep; // A.2
            this.sArchitectEngineerFeeLckd.Checked = !Tools.GetRenovationFeePaymentsMatchingMismoType(dataLoan.sClosingCostSet, E_ClosingCostFeeMismoFeeT._203KArchitecturalAndEngineeringFee).Any(); // A.2
            this.sConsultantFee.Text = dataLoan.sConsultantFee_rep; // A.3
            this.sConsultantFeeLckd.Checked = !Tools.GetRenovationFeePaymentsMatchingMismoType(dataLoan.sClosingCostSet, E_ClosingCostFeeMismoFeeT._203KConsultantFee).Any(); // A.3
            this.sRenovationConstrInspectFee.Text = dataLoan.sRenovationConstrInspectFee_rep; // A.4
            this.sRenovationConstrInspectFeeLckd.Checked = !Tools.GetRenovationFeePaymentsMatchingMismoType(dataLoan.sClosingCostSet, E_ClosingCostFeeMismoFeeT._203KInspectionFee).Any(); // A.4
            this.sTitleUpdateFee.Text = dataLoan.sTitleUpdateFee_rep; // A.5
            this.sTitleUpdateFeeLckd.Checked = !Tools.GetRenovationFeePaymentsMatchingMismoType(dataLoan.sClosingCostSet, E_ClosingCostFeeMismoFeeT._203KTitleUpdate).Any(); // A.5
            this.sPermitFee.Text = dataLoan.sPermitFee_rep; // A.6
            this.sPermitFeeLckd.Checked = !Tools.GetRenovationFeePaymentsMatchingMismoType(dataLoan.sClosingCostSet, E_ClosingCostFeeMismoFeeT._203KPermits).Any(); // A.6
            this.sFeasibilityStudyFee.Text = dataLoan.sFeasibilityStudyFee_rep; // A.7
            this.sContingencyRsrvPcRenovationHardCost.Text = dataLoan.sContingencyRsrvPcRenovationHardCost_rep; // B as percentage
            this.sFinanceContingencyRsrv.Text = dataLoan.sFinanceContingencyRsrv_rep; // B
            this.sFinanceMortgagePmntRsrv.Text = dataLoan.sFinanceMortgagePmntRsrv_rep; // C
            this.sRenoMtgPmntRsrvMnth.Text = dataLoan.sRenoMtgPmntRsrvMnth_rep; // C.Target month
            this.sMonthlyPmt.Text = dataLoan.sMonthlyPmt_rep; // C.Target what month is multiplied by
            this.sFinanceMortgagePmntRsrvTgtVal.Text = dataLoan.sFinanceMortgagePmntRsrvTgtVal_rep; // C.Target
            this.sFinanceMortgagePmntRsrvTgtValLckd.Checked = dataLoan.sFinanceMortgagePmntRsrvTgtValLckd; // C.Target Lckd box
            this.sMaxFinanceMortgagePmntRsrv.Text = dataLoan.sMaxFinanceMortgagePmntRsrv_rep; // C Max
            this.sFinanceMortgageFee.Text = dataLoan.sFinanceMortgageFee_rep; // D
            this.sFinanceOriginationFee.Text = dataLoan.sFinanceOriginationFee_rep; // D.1
            this.sFinanceOriginationFeeTgtVal.Text = dataLoan.sFinanceOriginationFeeTgtVal_rep; // D.1 Target
            this.sMaxFinanceOriginationFee.Text = dataLoan.sMaxFinanceOriginationFee_rep; // D.1 Max
            this.sDiscntPntOnRepairCostFee.Text = dataLoan.sDiscntPntOnRepairCostFee_rep; // D.2
            this.sDiscntPntOnRepairCostFeeTgtVal.Text = dataLoan.sDiscntPntOnRepairCostFeeTgtVal_rep; // D.2 Target
            this.sMaxDiscntPntOnRepairCostFee.Text = dataLoan.sMaxDiscntPntOnRepairCostFee_rep; // D.2 Max
            this.sTotalRenovationCosts.Text = dataLoan.sTotalRenovationCosts_rep; // E
            this.sTotalRenovationCostsLckd.Checked = dataLoan.sTotalRenovationCostsLckd; // E Lckd box

            // Step 2 - Purchase
            if (isPurchase)
            {
                this.sPurchPrice.Text = dataLoan.sPurchPrice_rep; // A
                this.sInducementPurchPrice.Text = dataLoan.sInducementPurchPrice_rep; // B
                this.sPurchPriceLessInducement.Text = dataLoan.sPurchPriceLessInducement_rep; // C
                this.sFHASpAsIsVal.Text = dataLoan.sFHASpAsIsVal_rep; // D
                this.s203kFHASpAdjAsIsValue.Text = dataLoan.s203kFHASpAdjAsIsValue_rep; // E
                this.sApprVal.Text = dataLoan.sApprVal_rep; // F
            }
            else // Step 2 - Refinance
            {
                this.sRefPdOffAmt1003.Text = dataLoan.sRefPdOffAmt1003_rep; // A
                this.sRefPdOffAmt1003Lckd.Checked = dataLoan.sRefPdOffAmt1003Lckd; // A Lckd box
                this.sTotalRenovationCosts_2.Text = dataLoan.sTotalRenovationCosts_rep; // B
                this.sFeeNewLoan.Text = dataLoan.sFeeNewLoan_rep; // C
                this.sFHA203kSum2AExistingDebtOnTheSubjectPropAnd2BTotalRehabilitationCostAnd2CFeeAssociatedWithTheNewLoan.Text = dataLoan.sFHA203kSum2AExistingDebtOnTheSubjectPropAnd2BTotalRehabilitationCostAnd2CFeeAssociatedWithTheNewLoan_rep; // D
                this.sFHASpAsIsVal_2.Text = dataLoan.sFHASpAsIsVal_rep; // E
                this.s203kFHASpAdjAsIsValue_2.Text = dataLoan.s203kFHASpAdjAsIsValue_rep; // F
                this.sApprVal_2.Text = dataLoan.sApprVal_rep; // G
            }            

            // Step 3 - Purchase
            if (isPurchase)
            {
                this.sTotalRehabCostPlusAdjAsIsValue.Text = dataLoan.sTotalRehabCostPlusAdjAsIsValue_rep; // A
                this.sFHAAdjPropTypeAfterImproveValue.Text = dataLoan.sFHAAdjPropTypeAfterImproveValue_rep; // B
                this.sFHA203kLeadPaintCredit.Text = dataLoan.sFHA203kLeadPaintCredit_rep; // C
                this.sFHA203ProductLtvMaxMortgageAndMinAppraisedValue.Text = dataLoan.sFHA203ProductLtvMaxMortgageAndMinAppraisedValue_rep; // D
                this.sNationWideMortgageLimit.Text = dataLoan.sNationWideMortgageLimit_rep; // E
                this.s203kInitMaxBaseLoanAmount.Text = dataLoan.s203kInitMaxBaseLoanAmount_rep; // F
                this.sLtv203KMaxMortgageEligibility.Text = dataLoan.sLtv203KMaxMortgageEligibility_rep; // G
            }
            else // Step 3 - Refinance
            {
                this.sFHA203kSum2AExistingDebtOnTheSubjectPropAnd2BTotalRehabilitationCostAnd2CFeeAssociatedWithTheNewLoan_2.Text = dataLoan.sFHA203kSum2AExistingDebtOnTheSubjectPropAnd2BTotalRehabilitationCostAnd2CFeeAssociatedWithTheNewLoan_rep; // A
                this.sTotalRehabCostPlusAdjAsIsValue_2.Text = dataLoan.sTotalRehabCostPlusAdjAsIsValue_rep; // B
                this.sFHAAdjPropTypeAfterImproveValue_2.Text = dataLoan.sFHAAdjPropTypeAfterImproveValue_rep; // C
                this.sFHA203ProductLtvMaxMortgageAndMinAppraisedValue_2.Text = dataLoan.sFHA203ProductLtvMaxMortgageAndMinAppraisedValue_rep; // D
                this.sNationWideMortgageLimit_2.Text = dataLoan.sNationWideMortgageLimit_rep; // E
                this.s203kInitMaxBaseLoanAmount_2.Text = dataLoan.s203kInitMaxBaseLoanAmount_rep; // F
                this.sLtv203KMaxMortgageEligibility_2.Text = dataLoan.sLtv203KMaxMortgageEligibility_rep; // G
            }            

            // Step 4
            this.sEnergyImprovementAmount.Text = dataLoan.sEnergyImprovementAmount_rep; // A
            this.s203kIntermediateMaxBaseLoanAmount.Text = dataLoan.s203kIntermediateMaxBaseLoanAmount_rep;
            this.sSolarEnergySystemCost.Text = dataLoan.sSolarEnergySystemCost_rep; // C
            this.sFHASpAfterImproveValBy20Pc.Text = dataLoan.sFHASpAfterImproveValBy20Pc_rep; // D
            this.sAdjSolarEnergyAmount.Text = dataLoan.sAdjSolarEnergyAmount_rep; // E
            this.sNationWideMortgageLimitBy120Pc.Text = dataLoan.sNationWideMortgageLimitBy120Pc_rep; // F
            this.s203kFinalMaxBaseLoanAmount.Text = dataLoan.s203kFinalMaxBaseLoanAmount_rep; // G

            this.sLAmtCalc.Text = dataLoan.sLAmtCalc_rep;

            // Step 5
            this.sFHAMipLtv.Text = dataLoan.sFHAMipLtv_rep;
            this.sLtvR.Text = dataLoan.sLtvR_rep;

            // Step 6
            this.sTotalRenovationCosts_3.Text = dataLoan.sTotalRenovationCosts_rep; // A
            this.sEnergyImprovementAmount_2.Text = dataLoan.sEnergyImprovementAmount_rep; // B
            this.sAdjSolarEnergyAmount_2.Text = dataLoan.sAdjSolarEnergyAmount_rep; // C
            this.sBorrOwnFundsContingencyRsrv.Text = dataLoan.sBorrOwnFundsContingencyRsrv_rep; // D
            this.s203KRehabEscrowAmtTot.Text = dataLoan.s203KRehabEscrowAmtTot_rep; // E
            this.s203KRehabEscrowInitDrawAmt.Text = dataLoan.s203KRehabEscrowInitDrawAmt_rep; // F
            this.s203kConsultantFeeInitDraw.Text = dataLoan.s203kConsultantFeeInitDraw_rep; // F.1
            this.s203kConsultantFeeInitDrawLckd.Checked = dataLoan.s203kConsultantFeeInitDrawLckd; // F.1
            this.sArchitectEngineerFeeInitDraw.Text = dataLoan.sArchitectEngineerFeeInitDraw_rep; // F.2
            this.sArchitectEngineerFeeInitDrawLckd.Checked = dataLoan.sArchitectEngineerFeeInitDrawLckd; // F.2
            this.sPermitFeeInitDraw.Text = dataLoan.sPermitFeeInitDraw_rep; // F.3
            this.sPermitFeeInitDrawLckd.Checked = dataLoan.sPermitFeeInitDrawLckd; // F.3
            this.sOriginationFeeInitDraw.Text = dataLoan.sOriginationFeeInitDraw_rep; // F.4
            this.sOriginationFeeInitDrawLckd.Checked = dataLoan.sOriginationFeeInitDrawLckd; // F.4
            this.sDiscntPointsOnRepairCostFeeInitDraw.Text = dataLoan.sDiscntPointsOnRepairCostFeeInitDraw_rep; // F.5
            this.sDiscntPointsOnRepairCostFeeInitDrawLckd.Checked = dataLoan.sDiscntPointsOnRepairCostFeeInitDrawLckd; // F.5
            this.sMatCostOrderedAndPrepaidInitDraw.Text = dataLoan.sMatCostOrderedAndPrepaidInitDraw_rep; // F.6
            this.s50PcMatCostOrderedButNotPaidInitDraw.Text = dataLoan.s50PcMatCostOrderedButNotPaidInitDraw_rep; // F.7
            this.sTotalRehabEscrowAmount.Text = dataLoan.sTotalRehabEscrowAmount_rep; // G
        }          

        /// <summary>
        /// The code that runs when the page is initialized.
        /// </summary>
        protected void Page_Init()
        {
            this.RegisterJsScript("loanframework2.js");
            this.RegisterService("loanedit", "/newlos/Renovation/FHA203kMaximumMortgageService.aspx");
            this.EnableJqueryMigrate = false;

            Tools.Bind_sFHA203kType(this.sFHA203kType);
            Tools.Bind_sLPurposeT(this.sLPurposeT);
            Tools.Bind_sProdSpT(this.sProdSpT);
            Tools.Bind_sOccT(this.sOccT);
            Tools.Bind_sFHAPropAcquisitionT(this.sFHAPropAcquisitionT);
        }
    }
}