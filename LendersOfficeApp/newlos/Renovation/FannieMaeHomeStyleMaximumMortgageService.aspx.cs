﻿#region Auto-generated code
namespace LendersOfficeApp.newlos.Renovation
#endregion
{
    using System;
    using DataAccess;

    /// <summary>
    /// Service page for FannieMaeHomeStyleMaximumMortgage, renovation loan.
    /// </summary>
    public class FannieMaeHomeStyleMaximumMortgageServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        /// <summary>
        /// Function that creates the page data class for the service page.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <returns>CPageData object with the loan file fields loaded.</returns>
        protected override CPageData ConstructPageDataClass(Guid loanId)
        {
            return CPageData.CreateUsingSmartDependency(loanId, typeof(FannieMaeHomeStyleMaximumMortgageServiceItem));
        }

        /// <summary>
        /// Service Page function that binds the data from the UI.
        /// </summary>
        /// <param name="dataLoan">The data loan object.</param>
        /// <param name="dataApp">The data app object.</param>
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            // Section A
            dataLoan.sRenovationLoanTgtLtv_rep = this.GetString("sRenovationLoanTgtLtv");

            // Section B
            dataLoan.sPurchPrice_rep = this.GetString("sPurchPrice"); // B.1
            dataLoan.sRefPdOffAmt1003_rep = this.GetString("sRefPdOffAmt1003"); // B.2
            dataLoan.sRefPdOffAmt1003Lckd = this.GetBool("sRefPdOffAmt1003Lckd"); // B.2 Lckd checkbox
            dataLoan.sApprVal_rep = this.GetString("sApprVal"); // B.3

            // Section C
            dataLoan.sCostConstrRepairsRehab_rep = this.GetString("sCostConstrRepairsRehab"); // C.1.a
            dataLoan.sContingencyRsrvPcRenovationHardCost_rep = this.GetString("sContingencyRsrvPcRenovationHardCost"); // C.1.b percentage
            dataLoan.sArchitectEngineerFee_rep = this.GetString("sArchitectEngineerFee"); // C.1.c
            dataLoan.sConsultantFee_rep = this.GetString("sConsultantFee"); // C.1.d
            dataLoan.sRenovationConstrInspectFee_rep = this.GetString("sRenovationConstrInspectFee"); // C.1.e
            dataLoan.sTitleUpdateFee_rep = this.GetString("sTitleUpdateFee"); // C.1.f
            dataLoan.sPermitFee_rep = this.GetString("sPermitFee"); // C.1.g
            dataLoan.sRenoMtgPmntRsrvMnth_rep = this.GetString("sRenoMtgPmntRsrvMnth"); // C.1.h months
            dataLoan.sFinanceMortgagePmntRsrvTgtVal_rep = this.GetString("sFinanceMortgagePmntRsrvTgtVal"); // C.1.h Target
            dataLoan.sFinanceMortgagePmntRsrvTgtValLckd = this.GetBool("sFinanceMortgagePmntRsrvTgtValLckd"); // C.1.h Target Locked box
            dataLoan.sOtherRenovationCost_rep = this.GetString("sOtherRenovationCost"); // C.1.i
            dataLoan.sOtherRenovationCostDescription = this.GetString("sOtherRenovationCostDescription"); // C.1.i Description
            dataLoan.sFinanceOriginationFeeTgtVal_rep = this.GetString("sFinanceOriginationFeeTgtVal"); // C.1.i financeable origination fee Target
            dataLoan.sDiscntPntOnRepairCostFeeTgtVal_rep = this.GetString("sDiscntPntOnRepairCostFeeTgtVal"); // C.1.i discount points Target            
            dataLoan.sFeasibilityStudyFee_rep = this.GetString("sFeasibilityStudyFee"); // C.1.i feasibility study
            dataLoan.sTotalRenovationCosts_rep = this.GetString("sTotalRenovationCosts"); // C.2
            dataLoan.sTotalRenovationCostsLckd = this.GetBool("sTotalRenovationCostsLckd"); // C.2 Lckd checkbox

            // Section D
            dataLoan.sApprVal_rep = this.GetString("sApprVal_2"); // D.2
            
            // Section E
            dataLoan.sPurchPrice_rep = this.GetString("sPurchPrice_2"); // E.1
            dataLoan.sAltCost_rep = this.GetString("sAltCost"); // E.2
            dataLoan.sAltCostLckd = this.GetBool("sAltCostLckd"); // E.2 Lckd checkbox
            dataLoan.sLandCost_rep = this.GetString("sLandCost"); // E.3
            dataLoan.sRefPdOffAmt1003_rep = this.GetString("sRefPdOffAmt1003_2"); // E.4
            dataLoan.sRefPdOffAmt1003Lckd = this.GetBool("sRefPdOffAmt1003Lckd_2"); // E.4 Lckd checkbox
            dataLoan.sTotEstPp1003_rep = this.GetString("sTotEstPp1003"); // E.5
            dataLoan.sTotEstPp1003Lckd = this.GetBool("sTotEstPp1003Lckd"); // E.5 Lckd checkbox
            dataLoan.sTotEstCcNoDiscnt1003_rep = this.GetString("sTotEstCcNoDiscnt1003"); // E.6
            dataLoan.sTotEstCc1003Lckd = this.GetBool("sTotEstCc1003Lckd"); // E.6 Lckd checkbox
            dataLoan.sLDiscnt1003_rep = this.GetString("sLDiscnt1003"); // E.8
            dataLoan.sLDiscnt1003Lckd = this.GetBool("sLDiscnt1003Lckd"); // E.8 Lckd checkbox
            dataLoan.sTotCcPbs_rep = this.GetString("sTotCcPbs"); // E.11
            dataLoan.sTotCcPbsLocked = this.GetBool("sTotCcPbsLocked"); // E.11 Locked checkbox
            dataLoan.sLAmtCalc_rep = this.GetString("sLAmtCalc"); // E.13b
            dataLoan.sTransNetCash_rep = this.GetString("sTransNetCash"); // E.15
            dataLoan.sTransNetCashLckd = this.GetBool("sTransNetCashLckd"); // E.15 Lckd checkbox
        }

        /// <summary>
        /// Service Page function that binds data to the UI.
        /// </summary>
        /// <param name="dataLoan">The data loan object.</param>
        /// <param name="dataApp">The data app object.</param>
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            this.SetResult("sRenoFeesToReduceUrlaLineF", dataLoan.sRenoFeesToReduceUrlaLineF_rep);
            this.SetResult("sDiscntPntOnRepairCostFee_Explanation", dataLoan.sDiscntPntOnRepairCostFee_rep);
            this.SetResult("sRenoFeesInClosingCosts", dataLoan.sRenoFeesInClosingCosts_rep);

            // Section A
            this.SetResult("sRenovationLoanTgtLtv", dataLoan.sRenovationLoanTgtLtv_rep); // A.1
            this.SetResult("sHomeStyleMaxRenovCost", dataLoan.sHomeStyleMaxRenovCost_rep); // A.2
            this.SetResult("sOccT", dataLoan.sOccT); // A.3

            // Section B
            this.SetResult("sPurchPrice", dataLoan.sPurchPrice_rep); // B.1
            this.SetResult("sRefPdOffAmt1003", dataLoan.sRefPdOffAmt1003_rep); // B.2
            this.SetResult("sRefPdOffAmt1003Lckd", dataLoan.sRefPdOffAmt1003Lckd); // B.2 Lckd checkbox
            this.SetResult("sApprVal", dataLoan.sApprVal_rep); // B.3

            // Section C
            this.SetResult("sCostConstrRepairsRehab", dataLoan.sCostConstrRepairsRehab_rep); // C.1.a
            this.SetResult("sContingencyRsrvPcRenovationHardCost", dataLoan.sContingencyRsrvPcRenovationHardCost_rep); // C.1.b percentage
            this.SetResult("sFinanceContingencyRsrv", dataLoan.sFinanceContingencyRsrv_rep); // C.1.b
            this.SetResult("sArchitectEngineerFee", dataLoan.sArchitectEngineerFee_rep); // C.1.c
            this.SetResult("sConsultantFee", dataLoan.sConsultantFee_rep); // C.1.d
            this.SetResult("sRenovationConstrInspectFee", dataLoan.sRenovationConstrInspectFee_rep); // C.1.e
            this.SetResult("sTitleUpdateFee", dataLoan.sTitleUpdateFee_rep); // C.1.f
            this.SetResult("sPermitFee", dataLoan.sPermitFee_rep); // C.1.g
            this.SetResult("sFinanceMortgagePmntRsrv", dataLoan.sFinanceMortgagePmntRsrv_rep); // C.1.h
            this.SetResult("sRenoMtgPmntRsrvMnth", dataLoan.sRenoMtgPmntRsrvMnth_rep); // C.1.h months
            this.SetResult("sMonthlyPmt", dataLoan.sMonthlyPmt_rep); // C.1.h months multiplier
            this.SetResult("sFinanceMortgagePmntRsrvTgtVal", dataLoan.sFinanceMortgagePmntRsrvTgtVal_rep); // C.1.h Target
            this.SetResult("sFinanceMortgagePmntRsrvTgtValLckd", dataLoan.sFinanceMortgagePmntRsrvTgtValLckd); // C.1.h Target Locked box
            this.SetResult("sMaxFinanceMortgagePmntRsrv", dataLoan.sMaxFinanceMortgagePmntRsrv_rep); // C.1.h Max value
            this.SetResult("sOtherRenovationCost", dataLoan.sOtherRenovationCost_rep); // C.1.i
            this.SetResult("sOtherRenovationCostDescription", dataLoan.sOtherRenovationCostDescription); // C.1.i Description
            this.SetResult("sFinanceOriginationFee", dataLoan.sFinanceOriginationFee_rep); // C.1.i financeable origination fee
            this.SetResult("sFinanceOriginationFeeTgtVal", dataLoan.sFinanceOriginationFeeTgtVal_rep); // C.1.i financeable origination fee Target
            this.SetResult("sMaxFinanceOriginationFee", dataLoan.sMaxFinanceOriginationFee_rep); // C.1.i financeable origination fee Max value
            this.SetResult("sDiscntPntOnRepairCostFee", dataLoan.sDiscntPntOnRepairCostFee_rep); // C.1.i discount points
            this.SetResult("sDiscntPntOnRepairCostFeeTgtVal", dataLoan.sDiscntPntOnRepairCostFeeTgtVal_rep); // C.1.i discount points Target
            this.SetResult("sMaxDiscntPntOnRepairCostFee", dataLoan.sMaxDiscntPntOnRepairCostFee_rep); // C.1.i. discount points Max value
            this.SetResult("sFeasibilityStudyFee", dataLoan.sFeasibilityStudyFee_rep); // C.1.i feasibility study
            this.SetResult("sTotalRenovationCosts", dataLoan.sTotalRenovationCosts_rep); // C.2
            this.SetResult("sTotalRenovationCostsLckd", dataLoan.sTotalRenovationCostsLckd); // C.2 Lckd checkbox

            // Section D
            this.SetResult("sRenovationPurchPricePlusTotalRenovationCosts", dataLoan.sRenovationPurchPricePlusTotalRenovationCosts_rep); // D.1
            this.SetResult("sApprVal_2", dataLoan.sApprVal_rep); // D.2
            this.SetResult("sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep); // D.3
            this.SetResult("sRenovationMaxMtgAmt", dataLoan.sRenovationMaxMtgAmt_rep); // D.4
            this.SetResult("sRenovationMaxMtgAmt_2", dataLoan.sRenovationMaxMtgAmt_rep); // D.5

            // Section E
            this.SetResult("sPurchPrice_2", dataLoan.sPurchPrice_rep); // E.1
            this.SetResult("sAltCost", dataLoan.sAltCost_rep); // E.2
            this.SetResult("sAltCostLckd", dataLoan.sAltCostLckd); // E.2 Lckd checkbox
            this.SetResult("sLandCost", dataLoan.sLandCost_rep); // E.3
            this.SetResult("sRefPdOffAmt1003_2", dataLoan.sRefPdOffAmt1003_rep); // E.4
            this.SetResult("sRefPdOffAmt1003Lckd_2", dataLoan.sRefPdOffAmt1003Lckd); // E.4 Lckd checkbox
            this.SetResult("sTotEstPp1003", dataLoan.sTotEstPp1003_rep); // E.5
            this.SetResult("sTotEstPp1003Lckd", dataLoan.sTotEstPp1003Lckd); // E.5 Lckd checkbox
            this.SetResult("sTotEstCcNoDiscnt1003", dataLoan.sTotEstCcNoDiscnt1003_rep); // E.6
            this.SetResult("sTotEstCc1003Lckd", dataLoan.sTotEstCc1003Lckd); // E.6 Lckd checkbox
            this.SetResult("sFfUfmipFinanced_2", dataLoan.sFfUfmipFinanced_rep); // E.7
            this.SetResult("sLDiscnt1003", dataLoan.sLDiscnt1003_rep); // E.8
            this.SetResult("sLDiscnt1003Lckd", dataLoan.sLDiscnt1003Lckd); // E.8 Lckd checkbox
            this.SetResult("sTotTransC", dataLoan.sTotTransC_rep); // E.9
            this.SetResult("sONewFinBal", dataLoan.sONewFinBal_rep); // E.10
            this.SetResult("sTotCcPbs", dataLoan.sTotCcPbs_rep); // E.11
            this.SetResult("sTotCcPbsLocked", dataLoan.sTotCcPbsLocked); // E.11 Locked checkbox
            this.SetResult("sRenovationOtherCredits", dataLoan.sRenovationOtherCredits_rep); // E.12
            this.SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep); // E.13a
            this.SetResult("sLAmtCalc", dataLoan.sLAmtCalc_rep); // E.13b
            this.SetResult("sRenovationTtlFundsAvailableToBorrower", dataLoan.sRenovationTtlFundsAvailableToBorrower_rep); // E.14
            this.SetResult("sTransNetCash", dataLoan.sTransNetCash_rep); // E.15
            this.SetResult("sTransNetCashLckd", dataLoan.sTransNetCashLckd); // E.15 Lckd checkbox

            MaxFinanceableOriginationFeePopup.LoadData(dataLoan, this);
            MaxFinanceableDiscountPointsPopup.LoadData(dataLoan, this);
        }
    }

    /// <summary>
    /// This service does all the loading, saving, and assigning.
    /// </summary>
    public partial class FannieMaeHomeStyleMaximumMortgageService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Initialize the service.
        /// </summary>
        protected override void Initialize()
        {
            this.AddBackgroundItem(string.Empty, new FannieMaeHomeStyleMaximumMortgageServiceItem());
        }
    }
}