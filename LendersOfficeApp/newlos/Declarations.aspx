﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Declarations.aspx.cs" Inherits="LendersOfficeApp.newlos.Declarations" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.Admin" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import namespace="DataAccess"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Declarations</title>
    <style type="text/css">
        .dateCol { width: 100px; }
        .docCol { width: 170px; }
        .methodCol { width: 153px; }
        .trackingCol { width: 133px; }
        .commentsCol { width: 133px; }
        .tdHeight { height: 40px; }
    </style>
</head>
<body bgcolor="gainsboro">
<script>
<!--
  function _init() {
     	
    setForeignNational();
  }

function validateYesNo(control, event) {
    updateDirtyBit(event);

    if (event != null && !(31 < event.keyCode && event.keyCode < 127)) { // if it's not a letter, ignore it
        return;
    }
    
    var value = '';
    if (control.value.length > 0)
        value = control.value.charAt(0);
    if (value == 'y' || value == 'Y') {
        control.value = 'Y';
    } else if (value == 'n' || value == 'N') {
        control.value = 'N';
    } else {
        control.value = '';
    }
    
    if (event != null && (retrieveEventTarget(event).id == <%= AspxTools.JsGetClientIdString(aBDecCitizen) %> || retrieveEventTarget(event).id == <%= AspxTools.JsGetClientIdString(aBDecResidency)%> ))
		setForeignNational();
  }

  function setForeignNational()
  {
	<% if ( BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) && ! IsReadOnly ) { %>
    
    var aBDecForeignNational = <%= AspxTools.JsGetElementById(aBDecForeignNational) %>;
	  var saBDecCitizen = <%= AspxTools.JsGetElementById(aBDecCitizen) %>.value;
	  var saBDecResidency = <%= AspxTools.JsGetElementById(aBDecResidency) %>.value;
	
//	if (saBDecCitizen == 'N' && saBDecResidency == 'N')
//		aBDecForeignNational.readOnly = false;
//	else

    if (saBDecCitizen != 'N' || saBDecResidency != 'N')
	{
		//aBDecForeignNational.readOnly = true;
		aBDecForeignNational.value = 'N';
	}
	
	<% } %>
  }
  //-->
  
</script>
    <form id="form1" runat="server">
    <div>
     <table id="Table5" cellspacing="0" cellpadding="0" width="600" border="0">
        <tr>
            <td class="MainRightHeader" nowrap colspan = "4">
                Declarations 
            </td>
        </tr>
        <tr>
          <td class="FieldLabel" colspan="2">If you answer "Yes" to any questions a through i, please provide the explanation below</td>
          <td class="FieldLabel">Borr&nbsp;&nbsp;</td>
          <td class="FieldLabel" nowrap>Co-borr</td>
        </tr>
        <tr>
          <td class="FieldLabel" colspan="2"></td>
          <td class="FieldLabel" align="left">Y/N</td>
          <td class="FieldLabel" nowrap align="left">Y/N</td>
        </tr>
        <tr valign="top">
          <td>a.</td>
          <td>Are there any outstanding judgments against you?</td>
          <td>
            <asp:TextBox ID="aBDecJudgment" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aCDecJudgment" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>b.</td>
          <td>Have you been declared bankrupt within the past 7 years?</td>
          <td>
            <asp:TextBox ID="aBDecBankrupt" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aCDecBankrupt" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>c.</td>
          <td>Have you had property foreclosed upon or given title or deed in lieu thereof in the last 7 years?</td>
          <td valign="top">
            <asp:TextBox ID="aBDecForeclosure" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="3"></asp:TextBox></td>
          <td valign="top">
            <asp:TextBox ID="aCDecForeclosure" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="3"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>d.</td>
          <td>Are you a party to a lawsuit?</td>
          <td>
            <asp:TextBox ID="aBDecLawsuit" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aCDecLawsuit" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>e.</td>
          <td>Have you directly or indirectly been obligated on any loan which resulted in foreclosure, transfer of title in lieu of foreclosure, or judgment? (This would include such loans as home mortgage loans, SBA loans, home improvement loans, educational loans, manufactured (mobile) home loans, any mortgage, financial obligation, bond, or loan guarantee.&nbsp; If "Yes", provide details, including if any, and reasons for the action.)</td>
          <td valign="top">
            <asp:TextBox ID="aBDecObligated" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td valign="top">
            <asp:TextBox ID="aCDecObligated" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>f.</td>
          <td>Are you presently delinquent or in default on any Federal debt or any other loan, mortgage, financial obligation bond, or loan guarantee? If "Yes", give details as described in the preceding question.</td>
          <td valign="top">
            <asp:TextBox ID="aBDecDelinquent" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td valign="top">
            <asp:TextBox ID="aCDecDelinquent" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>g.</td>
          <td>Are you obligated to pay alimony, child support, or separate maintenance?</td>
          <td>
            <asp:TextBox ID="aBDecAlimony" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aCDecAlimony" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>h.</td>
          <td>Is any part of the down payment borrowed?</td>
          <td>
            <asp:TextBox ID="aBDecBorrowing" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aCDecBorrowing" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>i.</td>
          <td>Are you a co-maker or endorser on a note?</td>
          <td>
            <asp:TextBox ID="aBDecEndorser" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aCDecEndorser" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>j.</td>
          <td>Are you a U.S. citizen?</td>
          <td>
            <asp:TextBox ID="aBDecCitizen" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aCDecCitizen" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td>k.</td>
          <td>Are you a permanent resident alien?</td>
          <td>
            <asp:TextBox ID="aBDecResidency" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>
            <asp:TextBox ID="aCDecResidency" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <%--<tr valign="top" style=<%= AspxTools.HtmlAttribute(getDisplayStyle) %>>--%>
        <tr valign="top" style='display:none;'>
          <td>-</td>
          <td>Are you a non-resident alien (foreign national)? (For PriceMyLoan)</td>
          <td>
            <asp:TextBox ID="aBDecForeignNational" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td>&nbsp;</td>
        </tr>
        <tr valign="top">
          <td>l.</td>
          <td>Do you intend to occupy the property as your primary residence?&nbsp; If "Yes," complete question m below</td>
          <td valign="top">
            <asp:TextBox ID="aBDecOcc" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td valign="top">
            <asp:TextBox ID="aCDecOcc" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td style="height: 14px; padding-right: 2px">m.</td>
          <td>Have you had an ownership interest in a property in the last three years?</td>
          <td style="height: 14px">
            <asp:TextBox ID="aBDecPastOwnership" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="5" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
          <td style="height: 14px">
            <asp:TextBox ID="aCDecPastOwnership" onblur="unhighlightRow(this);" onkeyup="validateYesNo(this, event);" onfocus="highlightRow(this);" TabIndex="6" runat="server" Width="23px" MaxLength="1"></asp:TextBox></td>
        </tr>
        <tr valign="top">
          <td style="height: 15px" colspan="2">
            <p>
              &nbsp;&nbsp; (1)&nbsp;Type of property: (PR - Principal Residence, SH - Second Home, IP - Investment Property)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
          </td>
          <td style="height: 15px">
            <asp:DropDownList ID="aBDecPastOwnedPropT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" TabIndex="5" runat="server">
            </asp:DropDownList>
          </td>
          <td style="height: 15px">
            <asp:DropDownList ID="aCDecPastOwnedPropT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" TabIndex="6" runat="server">
            </asp:DropDownList>
          </td>
        </tr>
        <tr valign="top">
          <td colspan="2">&nbsp;&nbsp; (2) Title held: (S - Solely by Yourself, SP - Jointly w/ Spouse, O - Jointly w/ Another Person)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td>
            <asp:DropDownList ID="aBDecPastOwnedPropTitleT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" TabIndex="5" runat="server">
            </asp:DropDownList>
          </td>
          <td>
            <asp:DropDownList ID="aCDecPastOwnedPropTitleT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" TabIndex="6" runat="server">
            </asp:DropDownList>
          </td>
        </tr>
         <tr>
            <td colspan="2"></td>
            <td colspan="2" class="tdHeight">
                <input type="button" value="Explanations" onclick="addExplanations();" />
            </td>
        </tr>    
      </table>
    </div>
    </form>
</body>
</html>
