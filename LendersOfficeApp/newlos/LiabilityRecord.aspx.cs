using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using LendersOffice.Constants;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Admin;

namespace LendersOfficeApp.newlos
{

    public partial class LiabilityRecord : BaseSingleEditPage2
    {
        #region Protected member variables

        protected string m_borrowerName;
        protected System.Web.UI.WebControls.Literal PmlAuditTrailXmlContent;
        protected bool m_isFHA = false;
        #endregion

        public override bool IsReadOnly
        {
                // 7/13/2005 dd - OPM 2351: Protect liabilities from being edited by AE.
            get { return base.IsReadOnly || BrokerUser.IsAccountExecutiveOnly; }
        }

        private void Bind_PayoffTiming(DropDownList ddl)
        {
            ddl.Items.Add(Tools.CreateEnumListItem("", E_Timing.Blank));
            ddl.Items.Add(Tools.CreateEnumListItem("Before closing", E_Timing.Before_Closing));
            ddl.Items.Add(Tools.CreateEnumListItem("At closing", E_Timing.At_Closing));
        }

        protected bool HasPmlEnabled
        {
            get { return BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan); }
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterCSS("LiabilityPmlAuditHistoryTable.css");

            Tools.Bind_DebtT(DebtT);
            this.Bind_PayoffTiming(this.PayoffTiming);
            ComZip.SmartZipcode(ComCity, ComState);

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(LiabilityRecord));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            ClientScript.RegisterHiddenField("BorrowerName", dataApp.aBNm);
            ClientScript.RegisterHiddenField("CoborrowerName", dataApp.aCNm);

			var recoll = dataApp.aReCollection;
			var subcoll = recoll.GetSubcollection( true, E_ReoGroupT.All );

            MatchedReRecordId.Items.Add(new ListItem("<-- Select a matched REO -->", Guid.Empty.ToString()));

			foreach( var item in subcoll )
			{
                var reField = (IRealEstateOwned)item;
                string addr = string.Format(@"{0}, {1}, {2} {3}", reField.Addr, reField.City, reField.State, reField.Zip);
                MatchedReRecordId.Items.Add(new ListItem(addr, reField.RecordId.ToString()));
            }
            Tools.BindPredefineLiabilityDescription(Desc);
            m_isFHA = dataLoan.sLT == E_sLT.FHA;

            this.PageTitle = "Liability record";
            this.PageID = "LiabilityRecordEdit";

			VerifSentD.ToolTip = VerifReorderedD.ToolTip = VerifRecvD.ToolTip = VerifExpD.ToolTip = "Hint:  Enter 't' for today's date.";
        }


        protected void PageLoad(object sender, System.EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            bool isReadOnly = IsReadOnly;
            VerifSentD.IsDisplayCalendarHelper = !isReadOnly;
            VerifReorderedD.IsDisplayCalendarHelper = !isReadOnly;
            VerifRecvD.IsDisplayCalendarHelper = !isReadOnly;
            VerifExpD.IsDisplayCalendarHelper = !isReadOnly;
            m_CFM.DisableLinks = isReadOnly;

            #region OPM 2758 - Only underwriter can edit "excl from underwriting"

            //// 7/31/2013 EM - Case 130395 - Now Underwriters, or if the company allows it, all roles can edit it.

            ExcFromUnderwriting.Enabled = BrokerUser.HasRole(E_RoleT.Underwriter) || BrokerUser.HasRole(E_RoleT.JuniorUnderwriter) || Broker.NonUWCanExcludeLiabilities;
            #endregion
        }
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.Init += new System.EventHandler(this.PageInit);
            this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion


    }
}
