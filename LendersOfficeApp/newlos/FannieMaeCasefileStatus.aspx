﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="FannieMaeCasefileStatus.aspx.cs" Inherits="LendersOfficeApp.newlos.FannieMaeCasefileStatus" EnableViewState="false" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.Common" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title></title>
</head>
<body class="RightBackground" style="MARGIN-LEFT: 0px" >

  <script language="javascript" type="text/javascript">
    var g_panelList = ["MainPanel", "StatusPanel", "ErrorPanel"];
    function _init() {
      resize(400, 200);
    }
    function f_close() {
      var ret = window.dialogArguments || {};
      ret.OK = false;
      onClosePopup(ret);
    }
    function f_getStatus() {
      f_displayWaitPanel();
      
      var args = new Object();
      args["LoanID"] = <%=AspxTools.JsString(LoanID)%>;
      args["FannieMaeMORNETUserID"] = <%= AspxTools.JsGetElementById(FannieMaeMORNETUserID) %>.value;
      args["FannieMaeMORNETPassword"] = <%= AspxTools.JsGetElementById(FannieMaeMORNETPassword) %>.value;
      args["sDuCaseId"] = <%= AspxTools.JsGetElementById(sDuCaseId) %>.value;
      args["RememberLastLogin"] = <%= AspxTools.JsGetElementById(m_rememberLoginCB) %>.checked ? 'True' : 'False';
      args["IsDo"] = '<%=AspxTools.JsBool(m_isDo)%>';

      var result = gService.loanedit.call("CasefileStatus", args);
      if (!result.error) {
        switch (result.value["Status"]) {
          case "ERROR":
            f_displayError(result.value["ErrorMessage"]);
            break;
          case "DONE":
            f_displayComplete(result.value);
            break;
        }
      } else {
        var errMsg = <%=AspxTools.JsString(ErrorMessages.Generic)%>;
        if (null != result.UserMessage)
          errMsg = result.UserMessage;

        f_displayError(errMsg);
      }
    }
    function f_displayWaitPanel() {
      document.getElementById("ErrorPanel").style.display = "none";
      document.getElementById("StatusPanel").style.display = "none";
      document.getElementById("MainPanel").style.display = "none";
      document.getElementById("WaitPanel").style.display = "";    
    }
    function f_displayError(errMsg) {
      resize(550, 500);    
      document.getElementById("ErrorPanel").style.display = "";
      document.getElementById("StatusPanel").style.display = "none";
      document.getElementById("MainPanel").style.display = "";
      document.getElementById("WaitPanel").style.display = "none";    

      <%= AspxTools.JsGetElementById(ErrorMessage) %>.value = errMsg;
    }
    function f_displayComplete(obj) {
      resize(550, 300);
      document.getElementById("MainPanel").style.display = "none";
      document.getElementById("ErrorPanel").style.display = "none";
      document.getElementById("StatusPanel").style.display = "";
      document.getElementById("WaitPanel").style.display = "none";    
      
      document.getElementById("LastUnderwritingDate").innerText = obj.LastUnderwritingDate;
      document.getElementById("UnderwritingRecommendation").innerText = obj.UnderwritingRecommendation;
      document.getElementById("UnderwritingStatus").innerText = obj.UnderwritingStatus;
      document.getElementById("UnderwritingSubmissionType").innerText = obj.UnderwritingSubmissionType;
      document.getElementById("OriginatorInstitutionName").innerText = obj.OriginatorInstitutionName;
      document.getElementById("OriginatorInstitutionIdentifier").innerText = obj.OriginatorInstitutionIdentifier;
      document.getElementById("LenderInstitutionName").innerText = obj.LenderInstitutionName;
      document.getElementById("LenderInstitutionIdentifier").innerText = obj.LenderInstitutionIdentifier;
      
    }
  </script>
  <h4 class="page-header"><ml:EncodedLiteral ID="m_headerLabel" runat="server"></ml:EncodedLiteral></h4>
  <form id="form1" runat="server">
  <div>
    <table width="100%">
      <tbody id="WaitPanel" style="display: none">
        <tr>
          <td style="font-weight: bold; font-size: 16px" valign="middle" align="center" colspan="2" height="50">Please Wait ...<br /><img height="10" src="../images/status.gif" /></td>
        </tr>
      </tbody>
      <tbody id="MainPanel">
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">
            <ml:EncodedLiteral ID="m_loginLabel" runat="server"></ml:EncodedLiteral>
          </td>
          <td>
            <asp:TextBox ID="FannieMaeMORNETUserID" runat="server" Width="155px"></asp:TextBox>
          </td>
        </tr>
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">
            <ml:EncodedLiteral ID="m_passwordLabel" runat="server"></ml:EncodedLiteral>
          </td>
          <td>
            <asp:TextBox ID="FannieMaeMORNETPassword" runat="server" Width="155px" TextMode="Password"></asp:TextBox>
          </td>
        </tr>
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">
            <ml:EncodedLiteral ID="m_caseIdLabel" runat="server"></ml:EncodedLiteral>
          </td>
          <td>
            <asp:TextBox ID="sDuCaseId" runat="server" Width="155px" ></asp:TextBox>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>
            <asp:CheckBox ID="m_rememberLoginCB" runat="server" Text="Remember login name" />
          </td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td><input type="button" class="ButtonStyle" onclick="f_getStatus();" value="Get Status" /> &nbsp;<input type="button" class="ButtonStyle" onclick="f_close();" value="Cancel" /> </td>
        </tr>        
        <tr style="padding-top:6px">
          <td></td>
          <td><a href="#" onclick="window.open('https://profile-manager.efanniemae.com/integration/service/password/UserSelfService', 'FannieMae', 'width=740,height=420,menubar=no,status=yes,resizable=yes');">Forgot your User ID or Password?</a></td>
        </tr>
      </tbody>
      <tbody id="StatusPanel" style="display:none">
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">Last Underwriting Date</td>
          <td>
            <div id="LastUnderwritingDate">
            </div>
          </td>
        </tr>
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">Underwriting Recommendation</td>
          <td>
            <div id="UnderwritingRecommendation">
            </div>
          </td>
        </tr>
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">Underwriting Status</td>
          <td>
            <div id="UnderwritingStatus">
            </div>
          </td>
        </tr>
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">Underwriting Submission Type</td>
          <td>
            <div id="UnderwritingSubmissionType">
            </div>
          </td>
        </tr>
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">Originator Institution Name</td>
          <td>
            <div id="OriginatorInstitutionName">
            </div>
          </td>
        </tr>
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">Originator Institution Identifier </td>
          <td>
            <div id="OriginatorInstitutionIdentifier">
            </div>
          </td>
        </tr>
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">Lender Institution Name</td>
          <td>
            <div id="LenderInstitutionName">
            </div>
          </td>
        </tr>
        <tr>
          <td class="FieldLabel" style="padding-left: 5px">Lender Institution Identifier</td>
          <td>
            <div id="LenderInstitutionIdentifier">
            </div>
          </td>
        </tr>
        <tr><td colspan="2" align="center"><input type="button" class="ButtonStyle" value="Close" onclick="f_close();" /></td></tr>
      </tbody>
      <tbody id="ErrorPanel" style="display: none">
        <tr>
          <td class="FieldLabel" style="padding-left: 5px; padding-top: 10px" colspan="2">Error Message</td>
        </tr>
        <tr>
          <td style="padding-left: 5px" colspan="2">
            <asp:TextBox ID="ErrorMessage" runat="server" Width="520px" TextMode="MultiLine" ReadOnly="true" Height="201px" ForeColor="Red"></asp:TextBox></td>
        </tr>
      </tbody>
    </table>
  </div>
  </form>
  <uc1:cModalDlg ID="CModalDlg1" runat="server"></uc1:cModalDlg>
</body>
</html>
