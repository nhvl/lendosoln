<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="PropertyInfo.ascx.cs" Inherits="LendersOfficeApp.newlos.PropertyInfoUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" EnableViewState="False" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<table>
<tr>
            <td class="MainRightHeader" nowrap colspan = "4">
                Subject Property Description
            </td>
        </tr>
<tr><td>
<table class="InsetBorder" id="Table4" cellspacing="0" cellpadding="0" border="0">

    <tr>
        <td class="FieldLabel" nowrap>
            Street
        </td>
        <td nowrap>
            <asp:TextBox ID="sSpAddr" Width="359px" MaxLength="60" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td nowrap>
        </td>
        <td nowrap>
            <asp:TextBox ID="sSpCity" MaxLength="72" runat="server" Width="258px"></asp:TextBox><ml:StateDropDownList ID="sSpState" runat="server" IncludeTerritories="false"></ml:StateDropDownList>
            <ml:ZipcodeTextBox ID="sSpZip" Width="50" runat="server" preset="zipcode"></ml:ZipcodeTextBox>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel" nowrap>
            County
        </td>
        <td class="FieldLabel" nowrap>
            <asp:DropDownList ID="sSpCounty" runat="server">
            </asp:DropDownList>
            &nbsp;No Units<asp:TextBox ID="sUnitsNum" Width="37px" MaxLength="3" runat="server"></asp:TextBox>&nbsp; Year built<asp:TextBox ID="sYrBuilt" Width="41px" MaxLength="4" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel" nowrap>
            Property Type
        </td>
        <td>
            <asp:DropDownList ID="sGseSpT" runat="server" onchange="refreshCalculation();" />
        </td>
    </tr>
    <tr id="HomeIsMhAdvantageRow">
        <td class="FieldLabel">Is home MH Advantage?</td>
        <td>
            <asp:DropDownList runat="server" ID="sHomeIsMhAdvantageTri"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel" nowrap>
            New Construction
        </td>
        <td>
            <input type="checkbox" id="sIsNewConstruction" runat="server"/>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel" nowrap>
            Is Mixed Use
        </td>
        <td>
            <input type="checkbox" id="sSpIsMixedUse" runat="server"/>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel" nowrap colspan="2">
            Planned Unit Development Indicator
            <input type="checkbox" id="sSpIsInPud" runat="server"/>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel" nowrap colspan="2">
            Legal Description
        </td>
    </tr>
    <tr>
        <td nowrap colspan="2">
            <asp:TextBox ID="sSpLegalDesc" Width="401px" runat="server" TextMode="MultiLine" Columns="20" Rows="3" Height="92px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel" nowrap colspan="2">
            Mineral Rights / Short Legal Description
        </td>
    </tr>
    <tr>
        <td nowrap colspan="2">
            <asp:TextBox ID="sSubjPropertyMineralAbbrLegalDesc" Width="401px" runat="server" TextMode="MultiLine" Columns="20" Rows="3" Height="92px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel" colspan="2">
            Project Name
            <asp:TextBox ID="sProjNm" Width="320px" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel" colspan="2">
            NFIP Flood Zone
            <asp:TextBox ID="sNfipFloodZoneId" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>                                                
        <td colspan="2">
            <label class="FieldLabel" style="float:left;margin-top:5px;">Building is in Special Flood Hazard Area (Zones containing letters "A" or "V")</label>
            <asp:RadioButtonList runat="server" ID="sFloodCertificationIsInSpecialArea" RepeatDirection="Horizontal" style="float:left">
                <asp:ListItem Value="True">Yes</asp:ListItem>
                <asp:ListItem Value="False">No</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel" colspan="2">
        Assessors Parcel ID
        <asp:TextBox ID="sAssessorsParcelId" Width="280px" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel" colspan="2">
            <asp:CheckBox ID="sHasMultipleParcels" runat="server" Text="Multiple Parcels" />            
        </td>
    </tr>
</table>
</td></tr></table>

<script language="javascript">
    function f_initControl() {
        onPropertyTypeChange();
        <%= AspxTools.JsGetElementById(sSpAddr) %>.focus();
    }

    function onPropertyTypeChange() {
        var manufacturedTypes = [
            <%=AspxTools.JsString(DataAccess.E_sGseSpT.ManufacturedHomeCondominium)%>,
            <%=AspxTools.JsString(DataAccess.E_sGseSpT.ManufacturedHomeMultiwide)%>,
            <%=AspxTools.JsString(DataAccess.E_sGseSpT.ManufacturedHousing)%>,
            <%=AspxTools.JsString(DataAccess.E_sGseSpT.ManufacturedHousingSingleWide)%>
        ];

        var propertyType = <%=AspxTools.JQuery(this.sGseSpT)%>.val();
        $('#HomeIsMhAdvantageRow').toggle(manufacturedTypes.indexOf(propertyType) !== -1);
    }
</script>

