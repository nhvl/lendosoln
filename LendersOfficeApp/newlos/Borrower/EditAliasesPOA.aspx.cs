﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Borrower
{
    public partial class EditAliasesPOA : BasePage
    {
        protected void PageInit(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            string borrMode = RequestHelper.GetSafeQueryString("borrmode");
            if (borrMode == "B")
            {
                BorrowerType.Text = "Borrower";
            }
            else if (borrMode == "C")
            {
                BorrowerType.Text = "Co-borrower";
            }
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
