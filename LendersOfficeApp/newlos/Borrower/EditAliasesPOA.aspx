﻿<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="RateLockConfirmation.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Borrower.EditAliasesPOA" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>Edit Aliases/POA</title>
    <style type="text/css">
        label
        {
            width: 60px;	
        }
        input.data
        {
            width: 300px;	
        }
        #Buttons
        {
            text-align: center;	
        }
    </style>
</head>

<script type="text/javascript">
<!--

function _init() {
    resizeForIE6And7(400, 400);

    var args = getModalArgs();
    if (args && args.data){
        var data = JSON.parse(args.data);
        var aliasObj = eval(data.alias);
        for (var i = 0; i < aliasObj.length; i++) {
            addAlias(aliasObj[i]);
        }
        $("#POA").val(data.powerOfAttorney);
    }
}

function addAliasFromTextbox() {
    addAlias($("#newAlias").val());
    $("#newAlias").val('');
}

function addAlias(alias) {
    $('#aliasContainer')
        .append($('<tr>')
            .append($('<td>')
                .append($('<input>')
                    .attr('type', 'text')
                    .attr('class', 'aliasTextbox')
                    .attr('style', 'width:300px')
                    .val(alias)
                )
                .append($('<a>')
                    .attr('href', '#')
                    .click(function(){$(this).closest('tr').remove();})
                    .text('delete')
                )
            )
        );
}

function cancel_onClick() {
    var results = window.dialogArguments || {};
    results.OK = false;
    onClosePopup(results);
}

function ok_onClick() {
    var results = window.dialogArguments || {};
    results.OK = true;
    
    var valid = true;
    if (typeof(Page_ClientValidate) == 'function')
        valid = Page_ClientValidate();
    if (!valid)
    {
        results.OK = false;
        onClosePopup(results);
    }
    
    // JSON it
    var aliasPOAData = {
        alias : JSON.stringify(getAliases()),
        powerOfAttorney : $('#POA').val()
    };
    results.data = JSON.stringify(aliasPOAData);
    
    onClosePopup(results);

}

function getAliases() {
    var result = [];
    $('.aliasTextbox').each(function(idx, obj) {
        result.push($(obj).val());
    });
    return result;
}
//-->
</script>
<h4 class="page-header">Edit Aliases/POA</h4>
<body bgcolor="gainsboro">
<form id="EditAliasesPOA" method="post" runat="server">

<div class="InsetBorder">
    <div class="FormTableSubHeader">
        <ml:EncodedLiteral ID="BorrowerType" runat="server"></ml:EncodedLiteral> Aliases
    </div>
    <div style="overflow:auto; height:270px" style="border:groove 1px">
        <table id="aliasContainer">
        </table>
    </div>
    <div>
        <input type="text" id="newAlias" style="width:300px" />
        <input type="button" onclick="addAliasFromTextbox();" value="add" />
    </div>
</div>

<div class="InsetBorder">
    <div class="FormTableSubHeader">
        Power of Attorney
    </div>
    <div>
        <label>POA Name</label>
        <input class="data" type="text" id="POA" />
    </div>
</div>

<div id="Buttons">
    <input type="button" value="OK" onclick="ok_onClick()" />
    <input type="button" value="Cancel" onclick="cancel_onClick()" />
</div>
</form>
</body>

</html>
