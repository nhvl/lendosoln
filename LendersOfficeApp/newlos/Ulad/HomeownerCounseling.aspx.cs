﻿#region Generated code
namespace LendersOfficeApp.newlos.Ulad
#endregion
{
    using System;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// Homeowner Counseling Page.
    /// </summary>
    public partial class HomeownerCounseling : BaseLoanPage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="args">
        /// The parameter is not used.
        /// </param>
        protected override void OnInit(EventArgs args)
        {
            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;

            this.RegisterCSS("UladEditor.css");

            this.RegisterJsScript("Ulad.HomeownerCounseling.js");
            this.RegisterJsScript("angular-1.5.5.min.js");
            this.RegisterJsScript("mask.js");

            this.PageTitle = "Homeowner Counseling";
            this.PageID = "HomeownerCounseling";

            Tools.Bind_CounselingType(this.CounselingType);
            Tools.Bind_CounselingFormatType(this.CounselingFormat);

            var counselingType = Enum.GetValues(typeof(CounselingType)).Cast<CounselingType>();
            this.RegisterJsObjectWithJsonNetAnonymousSerializer(
                "CounselingTypeByValue",
                counselingType.ToDictionary(e => e.ToString("D"), e => EnumUtilities.GetDescription(e)));

            var counselingFormatType = Enum.GetValues(typeof(CounselingFormatType)).Cast<CounselingFormatType>();
            this.RegisterJsObjectWithJsonNetAnonymousSerializer(
                "CounselingFormatTypeByValue",
                counselingFormatType.ToDictionary(e => e.ToString("D"), e => EnumUtilities.GetDescription(e)));

            base.OnInit(args);
        }
    }
}