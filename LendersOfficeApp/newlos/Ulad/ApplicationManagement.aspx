﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApplicationManagement.aspx.cs" Inherits="LendersOfficeApp.newlos.Ulad.ApplicationManagement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Borrower/Application Management</title>
    <style type="text/css">
        body {
            background-color: gainsboro;
        }

        table {
            width: 60%;
            table-layout: fixed;
        }

        .consumer-row:nth-of-type(even) {
            background-color: #FFF;
        }

        .consumer-row:nth-of-type(odd) {
            background-color: #CCC;
        }

        .grippy {
            cursor: move;
            display: inline-flex;
        }

        .grippy + span {
            display: inline-flex;
            vertical-align: top;
            margin-left: 5px;
        }

        .grippy-placeholder {
            height: 26px;
        }

        .ui-sortable-helper {
            display: table;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <script type="text/javascript">
            function saveMe() {
                angular.element('#ApplicationManagementControllerDiv').scope().saveData();
                return true;
            }

            function saveAndRedirect(postbackHandler) {
                // Used for the save confirmation dialog when navigating away from the page.
                // This will allow the async save to complete before redirecting the user.
                angular.element('#ApplicationManagementControllerDiv').scope().saveData(postbackHandler);
            }
        </script>
        <div ng-app="ApplicationManagement">
            <div id="ApplicationManagementControllerDiv" ng-controller="ApplicationManagementController">
                <h4 class="page-header">Borrower/Application Management</h4>

                <div ng-if="vm.loan != null">
                    <label class="FieldLabel">Target GSE Application Version:</label>

                    <select ng-model="vm.loan.sGseTargetApplicationT" ng-disabled="!vm.loan.sGseTargetApplicationTLckd" NotEditable="true">
                        <option value="0">&nbsp;</option>
                        <option value="1">Legacy</option>
                        <option value="2">ULAD 2019</option>
                    </select>

                    <input type="checkbox" ng-model="vm.loan.sGseTargetApplicationTLckd" ng-change="onTargetAppLockChanged();" /> Lock
                </div>

                <div class="Tabs">
                    <ul class="tabnav">
                        <li ng-class="{'selected': legacyMode }">
                            <a ng-click="changeAppMode();">Legacy Applications</a>
                        </li>
                        <li ng-class="{'selected': !legacyMode }">
                            <a ng-click="changeAppMode();">ULAD Applications</a>
                        </li>
                    </ul>
                </div>

                <table class="InsetBorder" ng-if="vm.loan != null">
                    <tbody ng-if="!legacyMode">
                        <tr>
                            <td colspan="4">
                                <input id="sSyncUladAndLegacyApps" type="checkbox" ng-model="vm.loan.sSyncUladAndLegacyApps" onchange="updateDirtyBit();" ng-disabled="!vm.loan.sCanSyncUladAndLegacyApps" />
                                <label for="sSyncUladAndLegacyApps">Restrict to two borrowers per application</label>
                            </td>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr class="GridHeader">
                            <th>
                                Primary Application
                            </th>
                            <th>
                                Position
                            </th>
                            <th>
                                Name
                            </th>
                            <th>&nbsp;</th>
                        </tr>
                    </tbody>
                    <tbody class="application-row" ng-if="legacyMode" ng-repeat="application in vm.loan.LegacyApplications" data-app-id="{{application['Id']}}">
                        <tr class="LoanProgramHeader">
                            <td class="align-center">
                                <input type="radio" name="PrimaryApplication" ng-model="application['IsPrimary']" ng-change="setPrimaryApplication(application['Id'])" ng-value="true" />
                            </td>
                            <td>
                                <a ng-click="swapBorrowerAndCoborrower(application['Id']);" ng-disabled="getConsumersInApplication(application['Id']).length != 2">swap borr & coborr</a>
                            </td>
                            <td>
                                <span ng-bind="getAppName(application['Id']);">&nbsp;</span>
                            </td>
                            <td>
                                <a ng-click="deleteApplication(application['Id']);" ng-disabled="vm.loan.LegacyApplications.length < 2">delete application</a>
                            </td>
                        </tr>
                        <tr class="consumer-row" ng-repeat="consumer in getConsumersInApplication(application['Id'])" data-consumer-id="{{consumer['Id']}}">
                            <td>&nbsp;</td>
                            <td>
                                <span ng-bind="$index === 0 ? 'Borrower' : 'Coborrower'">&nbsp;</span>
                            </td>
                            <td>
                                <img src="../../images/grip_small.png" alt="Drag and drop" class="grippy" />
                                <span ng-bind="getConsumerName(consumer);">&nbsp;</span>
                            </td>
                            <td>
                                <a ng-if="$index === 1" ng-click="deleteConsumer(consumer['Id'], getConsumerName(consumer));">delete</a>
                            </td>
                        </tr>
                    </tbody>
                    <tbody class="application-row" ng-if="!legacyMode" ng-repeat="application in vm.loan.UladApplications" data-app-id="{{application['Id']}}">
                        <tr class="LoanProgramHeader">
                            <td class="align-center">
                                <input type="radio" name="PrimaryApplication" ng-model="application['IsPrimary']" ng-change="setPrimaryApplication(application['Id'], {{vm.loan.UladApplications}})" ng-value="true" />
                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <span ng-bind="getAppName(application['Id']);">&nbsp;</span>
                            </td>
                            <td>
                                <a ng-click="deleteApplication(application['Id']);" ng-disabled="vm.loan.UladApplications.length < 2">delete application</a>
                            </td>
                        </tr>
                        <tr class="consumer-row" ng-repeat="consumer in getConsumersInApplication(application['Id']) as consumers" data-consumer-id="{{consumer['Id']}}">
                            <td>&nbsp;</td>
                            <td>
                                <span ng-bind="$index === 0 ? 'Primary' : 'Additional'">&nbsp;</span>
                            </td>
                            <td>
                                <img src="../../images/grip_small.png" alt="Drag and drop" class="grippy" />
                                <span ng-bind="getConsumerName(consumer);">&nbsp;</span>
                            </td>
                            <td>
                                <a ng-disabled="vm.loan.UladApplications.length < 2 && consumers.length == 1" ng-click="deleteConsumer(consumer['Id'], getConsumerName(consumer));">delete</a>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="3">
                                <input ng-if="!vm.loan.sSyncUladAndLegacyApps || consumers.length < 2" type="button" ng-click="addNewConsumer(application['Id']);" value="Add new borrower to application" />
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div>
                    <input type="button" ng-click="addNewApplication();" value="Add new application" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
