﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditAssociations.aspx.cs" Inherits="LendersOfficeApp.newlos.Ulad.EditAssociations" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit Associations</title>
    <style type="text/css">
        table {
            width: 100%;
        }

        form {
            min-width: 400px;
            min-height: 225px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div ng-app="EditAssociations">
            <div ng-controller="EditAssociationsController">
                <div class="FieldLabel align-center loading-div" ng-if="!vm.loan || !vm.loan.EmploymentRecords">
                    Loading employment records...
                </div>
                <table id="EditAssociationsTable" class="FormTable" ng-if="vm.loan && vm.loan.EmploymentRecords">
                    <tr>
                        <th class="GridHeader">Linked</th>
                        <th class="GridHeader align-left">Name</th>
                        <th class="GridHeader RightAlign">Monthly Income</th>
                        <th class="GridHeader RightAlign">From</th>
                        <th class="GridHeader RightAlign">To</th>
                    </tr>
                    <tr ng-repeat="employmentRecord in vm.loan.EmploymentRecords" class="GridAutoItem">
                        <td class="align-center">
                            <input type="checkbox" ng-model="employmentRecord.IsSelected" />
                        </td>
                        <td class="align-left" ng-bind="employmentRecord.EmployerName"></td>
                        <td class="RightAlign" ng-bind="employmentRecord.MonthlyIncome"></td>
                        <td class="RightAlign" ng-bind="employmentRecord.EmploymentStartDate"></td>
                        <td class="RightAlign" ng-bind="getEmploymentToDate(employmentRecord);"></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="align-center">
                            <input type="button" ng-click="onEditAssociations();" value="OK" />
                            <input type="button" onclick="parent.LQBPopup.Hide();" value="Cancel" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
