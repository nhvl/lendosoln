﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Liabilities.aspx.cs" Inherits="LendersOfficeApp.newlos.Ulad.Liabilities" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Liabilities</title>
    <style type="text/css">
        .liability-data-table {
            width: 825px;
        }

        .liability-account-table {
            width: 710px;
            display: inline-flex;
            vertical-align: top;
        }

        .lates-table {
            width: 100px;
            display: inline-flex;
            vertical-align: top;
        }

        .with-right-margin label {
            margin-right: 20px;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        function onApplicationFilterChange() {
            var newApplicationFilter = $('#ApplicationFilter').val();
            angular.element('#LiabilitiesControllerDiv').scope().onApplicationFilterChange(newApplicationFilter);
        }

        function saveMe() {
            angular.element('#LiabilitiesControllerDiv').scope().saveData();
            return true;
        }

        function saveAndRedirect(postbackHandler) {
            // Used for the save confirmation dialog when navigating away from the page.
            // This will allow the async save to complete before redirecting the user.
            angular.element('#LiabilitiesControllerDiv').scope().saveData(postbackHandler);
        }

        function format_money_callback(input) {
            angular.element('#LiabilitiesControllerDiv').scope().onFormatField(input);
        }

        function format_percent_callback(input) {
            angular.element('#LiabilitiesControllerDiv').scope().onFormatField(input);
        }

        function smartZipcode_callback() {
            angular.element('#LiabilitiesControllerDiv').scope().onSmartZipcode();
        }

        function doAfterDateFormat(input) {
            angular.element('#LiabilitiesControllerDiv').scope().onDateFormat(input);
        }
    </script>
    <form id="form1" runat="server">
        <div ng-app="Liabilities">
            <div id="LiabilitiesControllerDiv" ng-controller="LiabilitiesController">
                <h4 class="page-header">Liabilities</h4>

                <table class="FormTable page-width header-table">
                    <tr>
                        <td>
                            Show

                            <select id="LiabilityFilter" ng-model="liabilityFilterModel" ng-change="onLiabilityFilterChange('{{liabilityFilterModel}}');" NotForEdit="true">
                                <option value="0">All</option>
                                <option value="1">Selected Legacy Application</option>
                                <option value="2">Selected ULAD Application</option>
                                <option value="3">Selected Borrower</option>
                            </select>

                            <label class="app-label" ng-bind="applicationLabelsByValue[liabilityFilterModel]" ng-if="liabilityFilterModel !== '0'">&nbsp;</label>

                            <select id="ApplicationFilter" onchange="onApplicationFilterChange()" NotForEdit="true" ng-if="liabilityFilterModel !== '0'">
                                <option ng-repeat="appKeyValuePair in selectedApplications" value="{{appKeyValuePair.key}}" ng-bind="getAppName(appKeyValuePair.values)"></option>
                            </select>
                        </td>
                        <td class="RightAlign">
                            Enter Alimony, Child Support, and Job Expenses as ordinary liabilities below.
                        </td>
                    </tr>
                    <tr>
	                    <td colspan="2">
		                    Balance
		                    <input type="text" ng-model="vm.liabilityTotals.balance" ng-disabled="true" class="small" />

		                    Payment
		                    <input type="text" ng-model="vm.liabilityTotals.payment" ng-disabled="true" class="small" />

		                    Paid Off
		                    <input type="text" ng-model="vm.liabilityTotals.paidOff" ng-disabled="true" class="small" />
	                    </td>
                    </tr>
                    <tr ng-show="displayDebtsToBePaidOffRow">
                        <td colspan="2">
                            Debts to be paid off with:
                            <select ng-model="vm.loan.sLienToPayoffTotDebt" ng-disabled="!lienToPayoffEnabled" NotEditable="false" ng-change="onLienToPayoffTotDebtChanged('{{vm.loan.sLienToPayoffTotDebt}}')">
                                <option value="0">This Lien Transaction</option>
                                <option value="1">Other Lien Transaction</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="button" value="Order Credit..." ng-click="orderCredit();" />
                            <input type="button" value="View Credit" ng-click="viewCredit();" ng-if="liabilityFilterModel == '1' || liabilityFilterModel == '3'" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            View:

                            <input type="radio" name="ViewLiabilities" ng-model="viewLiabilities" ng-value="true" ng-change="onChangeLiabilitiesView({{viewLiabilities}})" DoesntDirty="true" />
                            Liability Details

                            <input type="radio" name="ViewLiabilities" ng-model="viewLiabilities" ng-value="false" ng-change="onChangeLiabilitiesView({{viewLiabilities}})" DoesntDirty="true" />
                            Debt Consolidation
                        </td>
	                    <td class="assets-liabilities-completed RightAlign" ng-if="liabilityFilterModel == '1'">
		                    Assets and Liabilities Completed:
		                    <input type="radio" name="AssetLiaCompletedNotJointly" ng-model="vm.AssetLiaCompletedNotJointly" ng-value="false" ng-change="updateDirtyBit();" /> Jointly
		                    <input type="radio" name="AssetLiaCompletedNotJointly" ng-model="vm.AssetLiaCompletedNotJointly" ng-value="true" ng-change="updateDirtyBit();" /> Not Jointly
	                    </td>
                    </tr>
                </table>

                <div class="ng-cloak" ng-show="!viewLiabilities">
                    <div class="listing-container">
                        <table class="FormTable page-width">
                            <tr>
                                <th class="GridHeader">Owner</th>
                                <th class="GridHeader">Debt Type</th>
                                <th class="GridHeader">Company</th>
                                <th class="GridHeader">Balance</th>
                                <th class="GridHeader">Payment</th>
                                <th class="GridHeader">Pd Off</th>
                                <th class="GridHeader">Used in Ratio</th>
                            </tr>
                            <tr class="GridItem" ng-if="vm.loan && vm.loan.Liabilities" ng-repeat="liability in vm.loan.Liabilities | filter:liabilityMatchesFilter track by $index">
                                <td class="entity-cell">
                                    <div ng-if="liability.Owners.length > 0" ng-bind="getConsumerName(liability.Owners[0].ConsumerId)"></div>
                                    <div ng-if="liability.Owners.length == 2" ng-bind="getConsumerName(liability.Owners[1].ConsumerId)"></div>
                                    <div ng-if="liability.Owners.length > 2">
                                        <span ng-show="!liability.ShowAllConsumers" ng-click="liability.ShowAllConsumers = !liability.ShowAllConsumers">
                                            &#x25ba; and <span ng-bind="liability.Owners.length - 1"></span> others
                                        </span>
                                        <div ng-show="liability.ShowAllConsumers" ng-click="liability.ShowAllConsumers = !liability.ShowAllConsumers">
                                            <div class="additional-consumer-name" ng-repeat="consumerId in liability.Owners.slice(1)">
                                                <span ng-if="$index == 0">&#x25bc;</span>
                                                <span ng-bind="getConsumerName(consumerId.ConsumerId)"></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="entity-cell" ng-bind="getFriendlyDebtTypeName(liability.DebtType);"></td>
                                <td class="entity-cell" ng-bind="liability.CompanyName"></td>
                                <td class="entity-cell" ng-bind="liability.Bal"></td>
                                <td class="entity-cell" ng-bind="liability.Pmt"></td>
                                <td class="entity-cell">
                                    <input type="checkbox" ng-model="liability.WillBePdOff" />
                                </td>
                                <td class="entity-cell">
                                    <input type="checkbox" ng-model="liability.UsedInRatioData" ng-if="!isMortgageDebtType(liability.DebtType)" />
                                    <span ng-if="isMortgageDebtType(liability.DebtType)">See REO</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <table>
                            <tr>
                                <td>Perform Calculations</td>
                                <td>
                                    <input type="radio" name="CalculationType" ng-model="useAutoCalc" ng-value="true" DoesntDirty="true" />
                                    <label class="FieldLabel">Automatically</label>
                                </td>
                                <td colspan="2">
                                    <input type="radio" name="CalculationType" ng-model="useAutoCalc" ng-value="false" DoesntDirty="true" />
                                    <label class="FieldLabel">Manually</label>

                                    <input type="button" ng-click="refreshCalculation(true);" value="Recalculate" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="FieldLabel">Loan Purpose</label>
                                </td>
                                <td>
                                    <select runat="server" id="sLPurposeT" ng-model="vm.loan.sLPurposeT" ng-change="onLoanPurposeChange();"></select>
                                </td>
                                <td>
                                    <label class="FieldLabel">PITI</label>
                                </td>
                                <td>
                                    <input type="text" ng-value="vm.loan.sMonthlyPmt" class="small" preset="money" disabled="disabled"  />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="FieldLabel">Loan Amount</label>
                                </td>
                                <td>
                                    <input id="sLAmtCalc" type="text" ng-model="vm.loan.sLAmtCalc" class="small" preset="money" ng-disabled="!vm.loan.sLAmtLckd" ng-blur="refreshCalculation();"  />
                                    <input type="checkbox" ng-model="vm.loan.sLAmtLckd" ng-disabled="vm.loan.sLPurposeT != purchaseType || vm.loan.sIsRenovationLoan" class="small" ng-click="refreshCalculation();" />
                                </td>
                                <td>
                                    <label class="FieldLabel">Bottom Ratio</label>
                                </td>
                                <td>
                                    <input type="text" ng-value="vm.loan.sQualBottomR" class="small" preset="percent" disabled="disabled"  />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="FieldLabel">Note Rate</label>
                                </td>
                                <td>
                                    <input id="sNoteIR" type="text" ng-model="vm.loan.sNoteIR" class="small" preset="percent" ng-blur="refreshCalculation();"  />
                                </td>
                                <td>
                                    <label class="FieldLabel">Cash from/to Borrower</label>
                                </td>
                                <td>
                                    <input id="sTransNetCash" type="text" ng-model="vm.loan.sTransNetCash" class="small" preset="money" ng-disabled="!vm.loan.sTransNetCashLckd" ng-blur="refreshCalculation();"  />
                                    <input type="checkbox" ng-model="vm.loan.sTransNetCashLckd" class="small" ng-click="refreshCalculation();" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="FieldLabel">Term/Due (mths)</label>
                                </td>
                                <td>
                                    <input type="text" ng-model="vm.loan.sTerm" class="micro" ng-blur="refreshCalculation();"  />
                                    /
                                    <input type="text" ng-model="vm.loan.sDue" class="micro" ng-blur="refreshCalculation();"  />
                                </td>
                                <td>
                                    <label class="FieldLabel">Total All Monthly Payments</label>
                                </td>
                                <td>
                                    <input type="text" ng-value="vm.loan.sTransmTotMonPmt" class="small" preset="money" disabled="disabled"  />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="FieldLabel">Interest Only</label>
                                </td>
                                <td>
                                    <input type="text" ng-model="vm.loan.sIOnlyMon" class="micro" ng-blur="refreshCalculation();"  />
                                    mths
                                </td>
                                <td>
                                    <label class="FieldLabel">Current Total Payment</label>
                                </td>
                                <td>
                                    <input type="text" ng-value="vm.loan.sCurrentTotMonPmt" class="small" preset="money" disabled="disabled"  />
                                </td>
                            </tr>
                            <tr>
                                <td ng-if="IsPmlEnabled">
                                    <label class="FieldLabel">Cashout Amount</label>
                                </td>
                                <td ng-if="IsPmlEnabled">
                                    <input 
                                        id="sProdCashoutAmt" 
                                        type="text" 
                                        ng-model="vm.loan.sProdCashoutAmt" 
                                        class="small" 
                                        preset="money" 
                                        ng-blur="refreshCalculation();" 
                                        ng-disabled="!vm.loan.sIsStandAlone2ndLien && vm.loan.sLPurposeT != refinCashoutType"  />
                                </td>
                                <td>
                                    <label class="FieldLabel">Monthly Savings</label>
                                </td>
                                <td>
                                    <input type="text" ng-value="vm.loan.sMonSavingPmt" class="small" preset="money" disabled="disabled"  />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="ng-cloak" ng-show="viewLiabilities">
                    <div class="listing-container">
                        <table class="FormTable page-width">
                            <tr>
                                <th class="GridHeader"><a ng-click="orderBy('Owner')">Owner</a></th>
                                <th class="GridHeader" ng-if="IsPmlEnabled"><a ng-click="orderBy('PmlAuditTrail')">Altered in PML</a></th>
                                <th class="GridHeader"><a ng-click="orderBy('DebtType')">Debt Type</a></th>
                                <th class="GridHeader"><a ng-click="orderBy('CompanyName')">Company</a></th>
                                <th class="GridHeader"><a ng-click="orderBy('Bal')">Balance</a></th>
                                <th class="GridHeader"><a ng-click="orderBy('Pmt')">Payment</a></th>
                                <th class="GridHeader"><a ng-click="orderBy('WillBePdOff')">Pd Off</a></th>
                                <th class="GridHeader"><a ng-click="orderBy('UsedInRatioData')">Used in Ratio</a></th>
                            </tr>
                            <tr 
                                ng-if="vm.loan && vm.loan.Liabilities" 
                                ng-repeat="liability in vm.loan.Liabilities | filter:liabilityMatchesFilter track by $index" 
                                ng-class="{selected: liability.Id === selectedLiability.Id, GridAutoItem: liability.Id !== selectedLiability.Id}" 
                                ng-click="selectLiability(liability);">
                                <td class="entity-cell">
                                    <div ng-if="liability.Owners.length > 0" ng-bind="getConsumerName(liability.Owners[0].ConsumerId)"></div>
                                    <div ng-if="liability.Owners.length == 2" ng-bind="getConsumerName(liability.Owners[1].ConsumerId)"></div>
                                    <div ng-if="liability.Owners.length > 2">
                                        <span ng-show="!liability.ShowAllConsumers" ng-click="liability.ShowAllConsumers = !liability.ShowAllConsumers">
                                            &#x25ba; and <span ng-bind="liability.Owners.length - 1"></span> others
                                        </span>
                                        <div ng-show="liability.ShowAllConsumers" ng-click="liability.ShowAllConsumers = !liability.ShowAllConsumers">
                                            <div class="additional-consumer-name" ng-repeat="consumerId in liability.Owners.slice(1)">
                                                <span ng-if="$index == 0">&#x25bc;</span>
                                                <span ng-bind="getConsumerName(consumerId.ConsumerId)"></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="entity-cell" ng-if="IsPmlEnabled" ng-bind="getFriendlyAlteredInPmlStatus(liability.PmlAuditTrail);"></td>
                                <td class="entity-cell" ng-bind="getFriendlyDebtTypeName(liability.DebtType);"></td>
                                <td class="entity-cell" ng-bind="liability.CompanyName"></td>
                                <td class="entity-cell" ng-bind="liability.Bal"></td>
                                <td class="entity-cell" ng-bind="liability.Pmt"></td>
                                <td class="entity-cell" ng-bind="liability.WillBePdOff ? 'Yes' : 'No'"></td>
                                <td class="entity-cell" ng-bind="getFriendlyUsedInRatioType(liability.DebtType, liability.UsedInRatioData)"></td>
                            </tr>
                        </table>
                    </div>

                    <table class="FormTable page-width">
                        <tr>
                            <td colspan="4">
                                <input type="button" value="<< Prev" ng-click="moveToPreviousRecord();" ng-disabled="selectedRecordIndex === 0" />
                                <input type="button" value="Next >>" ng-click="moveToNextRecord();" ng-disabled="vm.loan.Liabilities.length === 0 || selectedRecordIndex === vm.loan.Liabilities.length - 1" />
                                <input type="button" value="Insert" ng-click="insertRecord();" />
                                <input type="button" value="Add" ng-click="addRecord();" />
                                <input type="button" value="Save" ng-click="saveData();" ng-disabled="!isDirty();" />
                                <input type="button" value="Move Up" ng-click="moveRecordUp();" ng-if="liabilityFilterModel == 0" ng-disabled="selectedRecordIndex === 0" />
                                <input type="button" value="Move Down" ng-click="moveRecordDown();" ng-if="liabilityFilterModel == 0" ng-disabled="vm.loan.Liabilities.length === 0 || selectedRecordIndex === vm.loan.Liabilities.length - 1" />
                                <input type="button" value="Delete" ng-click="deleteRecord();" ng-disabled="vm.loan.Liabilities.length === 0" />
                                <input type="button" value="Edit VOM" ng-click="navigateToVom();" ng-disabled="!selectedLiability.RequiresVom" />
                                <input type="button" value="Edit VOL" ng-click="navigateToVol();" ng-disabled="!selectedLiability.RequiresVol" />
                            </td>
                        </tr>
                    </table>

                    <table class="FormTable InsetBorder liability-data-table left-padded-table" ng-if="selectedLiability != null">
                        <tr>
                            <td><label class="FieldLabel">Owner</label></td>
                            <td>
                                <div ng-repeat="consumerAssociation in selectedLiability.Owners">
                                    {{getConsumerName(consumerAssociation.ConsumerId)}}
                                </div>

                                <a ng-click="editLiabilityOwners(selectedLiability);">edit</a>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="FieldLabel">Debt Type</label></td>
                            <td>
                                <select id="DebtType" runat="server" ng-model="selectedLiability.DebtType" ng-change="onDebtTypeChange();"></select>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <a ng-click="pickFromContacts();">Pick from Contacts</a>
                                <a ng-click="addToContacts();">Add to Contacts</a>
                            </td>
                        </tr>
                        <tr>
	                        <td><label class="FieldLabel">Company Name</label></td>
	                        <td>
		                        <input type="text" ng-model="selectedLiability.CompanyName" />
	                        </td>
                        </tr>
                        <tr>
	                        <td><label class="FieldLabel">Company Address</label></td>
	                        <td>
		                        <input type="text" ng-model="selectedLiability.CompanyAddress" />
	                        </td>
                            <td><label class="FieldLabel">Phone</label></td>
	                        <td>
		                        <input type="text" class="medium" ng-model="selectedLiability.CompanyPhone" preset="phone" />
	                        </td>
                        </tr>
                        <tr>
	                        <td><label class="FieldLabel">Company City</label></td>
	                        <td>
		                        <input id="City" ng-model="selectedLiability.CompanyCity" type="text" />
		                        <select id="State" runat="server" ng-model="selectedLiability.CompanyState"></select>

		                        <input
			                        type="text"
			                        id="Zip" 
			                        ng-model="selectedLiability.CompanyZip" 
			                        ng-disabled="selectedLiability.AssetType == '0' || selectedLiability.AssetType == '9'"
			                        preset="zipcode" 
			                        mask="#####" 
			                        maxlength="5" 
			                        onfocus="this.oldValue = this.value;" 
			                        city="City" 
			                        state="State" 
			                        county=""
			                        onblur="smartZipcodeAuto(event);" />
	                        </td>
                            <td><label class="FieldLabel">Fax</label></td>
	                        <td>
		                        <input type="text" class="medium" ng-model="selectedLiability.CompanyFax" preset="phone" />
	                        </td>
                        </tr>
                        <tr>
                            <td><label class="FieldLabel">Description</label></td>
                            <td>
                                <ml:ComboBox ID="Description" runat="server" ng-model="selectedLiability.Desc"></ml:ComboBox>
                            </td>
                        </tr>
                        <tr ng-show="isMortgageDebtType(selectedLiability.DebtType)">
                            <td><label class="FieldLabel">Mortgage Type</label></td>
                            <td>
                                <select runat="server" id="MortgageType" ng-model="selectedLiability.MortgageType"></select>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="FieldLabel">Property Address</label></td>
                            <td>
                                <select 
                                    id="LinkedReoId"
                                    ng-disabled="!isMortgageDebtType(selectedLiability.DebtType)" 
                                    ng-model="selectedLiability.LinkedReoId" 
                                    ng-options="reo.Id as getFullRealPropertyAddress(reo) for reo in vm.loan.RealProperties"
                                    ng-change="onLinkedReoIdChanged();"
                                    NotEditable="false">
                                    <option value="">&lt;-- Select a matched REO --&gt;</option>
                                </select>
                                <input type="button" ng-click="addBalPmtToRealProperty();" ng-disabled="selectedLiability.LinkedReoId == null || !isMortgageDebtType(selectedLiability.DebtType)" value="Add Bal / Pmt Info to REO" />
                            </td>
                        </tr>
                    </table>

                    <table class="FormTable InsetBorder liability-account-table left-padded-table" ng-if="selectedLiability != null">
                        <tr>
                            <td colspan="2">
                                <label class="FieldLabel">Account Holder Name</label>
                                <input type="text" ng-model="selectedLiability.AccountName" class="medium" />
                            </td>
                            <td colspan="2">
                                <label class="FieldLabel">Acc. Number</label>
                                <input type="text" ng-model="selectedLiability.AccountNum" class="medium" />
                            </td>
                            <td colspan="2">
                                <label class="FieldLabel">Max Bal.</label>
                                <input id="MaximumBalance" type="text" ng-model="selectedLiability.MaximumBalance" class="small" preset="money" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="FieldLabel">Bal.</label>
                                <input id="Bal" type="text" ng-model="selectedLiability.Bal" class="small" preset="money" ng-blur="refreshCalculation();"  />
                            </td>
                            <td>
                                <label class="FieldLabel">Pmt.</label>
                                <input id="Pmt" type="text" ng-model="selectedLiability.Pmt" class="small" preset="money" ng-blur="refreshCalculation();" />
                            </td>
                            <td>
                                <label class="FieldLabel">Mos. Left</label>
                                <input type="text" ng-model="selectedLiability.RemainMon" class="micro" />
                            </td>
                            <td>
                                <label class="FieldLabel">Rate</label>
                                <input id="Rate" type="text" ng-model="selectedLiability.Rate" class="small" preset="percent" preset="percent" />
                            </td>
                            <td>
                                <label class="FieldLabel">Term</label>
                                <input type="text" ng-model="selectedLiability.OriginalTermAsString" class="micro" />
                            </td>
                            <td>
                                <label class="FieldLabel">Due In</label>
                                <input type="text" ng-model="selectedLiability.DueInMonAsString" class="micro" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" ng-model="selectedLiability.WillBePdOff" ng-click="onWillBePdOffChanged();" />
                                <label class="FieldLabel">Will be paid off</label>
                            </td>

                            <td colspan="2">
                                <label class="FieldLabel">Payoff Amt.</label>
                                <input id="PayoffAmt" type="text" ng-model="selectedLiability.PayoffAmt" class="small" preset="money" ng-show="!selectedLiability.PayoffAmtLocked" disabled="disabled" />
                                <input id="PayoffAmtData" type="text" ng-model="selectedLiability.PayoffAmtData" class="small" preset="money" ng-show="selectedLiability.PayoffAmtLocked" ng-blur="refreshCalculation();" />

                                <input type="checkbox" ng-model="selectedLiability.PayoffAmtLocked" ng-click="onPayoffAmtLockedClick();" />
                                <label class="FieldLabel">Lock</label>
                            </td>

                            <td colspan="2">
                                <label class="FieldLabel">Payoff</label>
                                <select ng-model="selectedLiability.PayoffTiming" disabled="disabled" ng-show="!selectedLiability.PayoffTimingLockedData" NotEditable="true">
                                    <option value="0">&nbsp;</option>
                                    <option value="1">Before closing</option>
                                    <option value="2">At closing</option>
                                </select>

                                <select ng-model="selectedLiability.PayoffTimingData" ng-show="selectedLiability.PayoffTimingLockedData" NotEditable="false" ng-change="refreshCalculation();">
                                    <option value="0" ng-disabled="selectedLiability.WillBePdOff">&nbsp;</option>
                                    <option value="1">Before closing</option>
                                    <option value="2">At closing</option>
                                </select>

                                <input type="checkbox" ng-model="selectedLiability.PayoffTimingLockedData" ng-disabled="!selectedLiability.WillBePdOff" ng-click="onPayoffTimingLockedDataClicked();" />
                                <label class="FieldLabel">Lock</label>
                            </td>
                        </tr>
                    </table>

                    <table class="FormTable InsetBorder lates-table left-padded-table" ng-if="selectedLiability != null">
                        <tr>
                            <td><label class="FieldLabel">Late 30</label></td>
                            <td>
                                <input type="text" ng-model="selectedLiability.Late30AsString" class="micro" />
                            </td>
                        </tr>
                        <tr>
                            <td><label class="FieldLabel">Late 60</label></td>
                            <td>
                                <input type="text" ng-model="selectedLiability.Late60AsString" class="micro" />
                            </td>
                        </tr>
                        <tr>
                            <td><label class="FieldLabel">Late 90&#43;</label></td>
                            <td>
                                <input type="text" ng-model="selectedLiability.Late90PlusAsString" class="micro" />
                            </td>
                        </tr>
                    </table>

                    <table class="FormTable InsetBorder liability-data-table left-padded-table with-spacer with-right-margin" ng-if="selectedLiability != null">
                        <tr>
                            <td colspan="2">
                                <input type="checkbox" ng-model="selectedLiability.UsedInRatioData" ng-disabled="isMortgageDebtType(selectedLiability.DebtType)" ng-click="refreshCalculation();" />
                                <label class="FieldLabel">Debt should be included in ratios</label>

                                <input type="checkbox" ng-model="selectedLiability.IsPiggyBack" />
                                <label class="FieldLabel">Debt will be resubordinated</label>

                                <input type="checkbox" ng-model="selectedLiability.ExcludeFromUw" ng-click="onExcludeFromUwChanged();" />
                                <label class="FieldLabel">Excl. from underwriting</label>
                            </td>
                        </tr>
                    </table>

                    <table class="FormTable InsetBorder liability-data-table left-padded-table with-spacer with-right-margin" ng-if="selectedLiability != null">
                        <tr>
                            <td colspan="2">
                                <input type="checkbox" ng-model="selectedLiability.IncludeInReposession" />
                                <label class="FieldLabel">Incl. in repossession</label>

                                <input type="checkbox" ng-model="selectedLiability.IncludeInBk" />
                                <label class="FieldLabel">Incl. in bankruptcy</label>

                                <input type="checkbox" ng-model="selectedLiability.IncludeInFc" />
                                <label class="FieldLabel">Incl. in foreclosure</label>
                            </td>
                        </tr>
                    </table>

                    <table class="FormTable InsetBorder liability-data-table left-padded-table with-spacer" ng-if="selectedLiability != null">
                        <tr>
                            <td colspan="2">
                                <label class="FieldLabel">Verif. Sent</label>
                                <input type="text" id="VerifSentDate" ng-model="selectedLiability.VerifSentDate" preset="date" />
                                <a onclick="return displayCalendar('VerifSentDate');">
                                    <img class="date-picker" src="../../images/pdate.gif" title="Open calendar" alt="Open calendar"/>
                                </a>
                            </td>
                            <td>
                                <label class="FieldLabel">Re-order</label>
                                <input type="text" id="VerifReorderedDate" ng-model="selectedLiability.VerifReorderedDate" preset="date" />
                                <a onclick="return displayCalendar('VerifReorderedDate');">
                                    <img class="date-picker" src="../../images/pdate.gif" title="Open calendar" alt="Open calendar"/>
                                </a>
                            </td>
                            <td>
                                <label class="FieldLabel">Received</label>
                                <input type="text" id="VerifRecvDate" ng-model="selectedLiability.VerifRecvDate" preset="date" />
                                <a onclick="return displayCalendar('VerifRecvDate');">
                                    <img class="date-picker" src="../../images/pdate.gif" title="Open calendar" alt="Open calendar"/>
                                </a>
                            </td>
                            <td>
                                <label class="FieldLabel">Expected</label>
                                <input type="text" id="VerifExpiresDate" ng-model="selectedLiability.VerifExpiresDate" preset="date" />
                                <a onclick="return displayCalendar('VerifExpiresDate');">
                                    <img class="date-picker" src="../../images/pdate.gif" title="Open calendar" alt="Open calendar"/>
                                </a>
                            </td>
                        </tr>
                    </table>

                    <table class="FormTable InsetBorder liability-data-table left-padded-table with-spacer" ng-if="selectedLiability != null && vm.loan.sLT == '1'">
                        <tr>
                            <td class="LoanFormHeader" colspan="3"><label class="FieldLabel">Additional Information required for HUD-56001 (FHA Only)</label></td>
                        </tr>
                        <tr>
                            <td><label class="FieldLabel">Original Debt Amount</label></td>
                            <td>
                                <input id="OriginalDebtAmt" type="text" ng-model="selectedLiability.OriginalDebtAmt" preset="money" class="small" ng-blur="refreshCalculation();"  />
                            </td>
                            <td>
                                <input type="checkbox" ng-model="selectedLiability.IsMtgFhaInsured" />
                                <label class="FieldLabel">Mortgage is FHA insured</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" ng-model="selectedLiability.IsForAuto" />
                                <label class="FieldLabel">Is Debt for Auto?</label>
                            </td>
                            <td><label class="FieldLabel">Auto Year Make</label></td>
                            <td>
                                <input type="text" ng-model="selectedLiability.AutoYearMakeAsString" class="mini" />
                            </td>
                        </tr>
                    </table>

                    <table class="FormTable InsetBorder liability-data-table left-padded-table with-spacer" ng-if="selectedLiability != null && IsPmlEnabled">
                        <tr>
                            <td>
                                <label class="FieldLabel">PML Audit History</label>
                            </td>
                        </tr>
                        <tbody ng-if="selectedLiability.PmlAuditTrail == null || selectedLiability.PmlAuditTrail.length === 0">
                            <tr>
                                <td class="align-center">
                                    None
                                </td>
                            </tr>
                        </tbody>
                        <tbody ng-if="selectedLiability.PmlAuditTrail != null && selectedLiability.PmlAuditTrail.length !== 0">
                            <tr class="AuditTableHeader">
                                <th>
                                    <label class="FieldLabel">Date</label>
                                </th>
                                <th>
                                    <label class="FieldLabel">User Name</label>
                                </th>
                                <th>
                                    <label class="FieldLabel">Login Name</label>
                                </th>
                                <th>
                                    <label class="FieldLabel">Action</label>
                                </th>
                                <th>
                                    <label class="FieldLabel">Field</label>
                                </th>
                                <th>
                                    <label class="FieldLabel">Value</label>
                                </th>
                            </tr>
                            <tr ng-repeat="auditItem in selectedLiability.PmlAuditTrail track by $index" class="audit-item">
                                <td ng-bind="auditItem.EventDate"></td>
                                <td ng-bind="auditItem.UserName"></td>
                                <td ng-bind="auditItem.LoginId"></td>
                                <td ng-bind="auditItem.Action"></td>
                                <td ng-bind="auditItem.Field"></td>
                                <td ng-bind="auditItem.Value"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
