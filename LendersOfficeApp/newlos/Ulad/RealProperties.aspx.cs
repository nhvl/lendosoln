﻿#region Generated code
namespace LendersOfficeApp.newlos.Ulad
#endregion
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Provides a means for managing real properties on
    /// legacy and ULAD applications.
    /// </summary>
    public partial class RealProperties : BaseLoanPage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="args">
        /// The parameter is not used.
        /// </param>
        protected override void OnInit(EventArgs args)
        {
            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;

            this.RegisterCSS("UladEditor.css");

            this.RegisterJsScript("Ulad.RealProperties.js");
            this.RegisterJsScript("angular-1.5.5.min.js");
            this.RegisterJsScript("mask.js");

            Tools.Bind_StatesWithoutMilitaryMailCodes(this.State);
            Tools.Bind_ReoTypeT(this.Type);

            var reoStatusByEnumConstantValue = new Dictionary<string, string>
            {
                [E_ReoStatusT.Sale.ToString("D")] = "S",
                [E_ReoStatusT.PendingSale.ToString("D")] = "PS",
                [E_ReoStatusT.Rental.ToString("D")] = "R",
                [E_ReoStatusT.Residence.ToString("D")] = string.Empty
            };
            this.RegisterJsObjectWithJsonNetSerializer("ReoStatusByEnumConstantValue", reoStatusByEnumConstantValue);

            base.OnInit(args);
        }
    }
}