﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RealProperties.aspx.cs" Inherits="LendersOfficeApp.newlos.Ulad.RealProperties" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Real Properties</title>
    <style type="text/css">
        .address-table {
            width: 625px;
            display: inline-flex;
            vertical-align: top;
        }

        .type-status-table {
            width: 175px;
            display: inline-flex;
            vertical-align: top;
        }

        .linked-liabilities-table {
            width: 400px;
            display: inline-flex;
            vertical-align: top;
        }

        .linked-liabilities-table th {
            text-align: left;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        function onApplicationFilterChange() {
            var newApplicationFilter = $('#ApplicationFilter').val();
            angular.element('#RealPropertiesControllerDiv').scope().onApplicationFilterChange(newApplicationFilter);
        }

        function saveMe() {
            angular.element('#RealPropertiesControllerDiv').scope().saveData();
            return true;
        }

        function saveAndRedirect(postbackHandler) {
            // Used for the save confirmation dialog when navigating away from the page.
            // This will allow the async save to complete before redirecting the user.
            angular.element('#RealPropertiesControllerDiv').scope().saveData(postbackHandler);
        }

        function format_money_callback(input) {
            angular.element('#RealPropertiesControllerDiv').scope().onFormatMoney(input);
        }

        function smartZipcode_callback() {
            angular.element('#RealPropertiesControllerDiv').scope().onSmartZipcode();
        }
    </script>
    <form id="form1" runat="server">
        <div ng-app="RealProperties">
            <div id="RealPropertiesControllerDiv" ng-controller="RealPropertiesController">
                <h4 class="page-header">REO</h4>

                <table class="FormTable page-width header-table">
                    <tr>
                        <td colspan="4">
                            Show

                            <select id="RealPropertyFilter" ng-model="realPropertyFilterModel" ng-change="onRealPropertyFilterChange('{{realPropertyFilterModel}}');" NotForEdit="true">
                                <option value="0">All</option>
                                <option value="1">Selected Legacy Application</option>
                                <option value="2">Selected ULAD Application</option>
                                <option value="3">Selected Borrower</option>
                            </select>

                            <label class="app-label" ng-bind="applicationLabelsByValue[realPropertyFilterModel]" ng-if="realPropertyFilterModel !== '0'">&nbsp;</label>

                            <select id="ApplicationFilter" onchange="onApplicationFilterChange()" NotForEdit="true" ng-if="realPropertyFilterModel !== '0'">
                                <option ng-repeat="appKeyValuePair in selectedApplications" value="{{appKeyValuePair.key}}">
                                    {{getAppName(appKeyValuePair.values)}}
                                </option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>Market Value</td>
                        <td>Mtg Amount</td>
                        <td>Net Rental Income (Rental)</td>
                        <td>Net Rental Income (Retained)</td>
                    </tr>
                    <tr>
                        <td>Total</td>
                        <td>
                            <input type="text" ng-model="vm.reoTotals.marketValue" ng-disabled="true" class="small" />
                        </td>
                        <td>
                            <input type="text" ng-model="vm.reoTotals.mortgageAmount" ng-disabled="true" class="small" />
                        </td>
                        <td>
                            <input type="text" ng-model="vm.reoTotals.rentalNetRentalIncome" ng-disabled="true" class="small" />
                        </td>
                        <td>
                            <input type="text" ng-model="vm.reoTotals.retainedNetRentalIncome" ng-disabled="true" class="small" />
                        </td>
                    </tr>
                </table>

                <div class="listing-container">
                    <table class="FormTable page-width">
                        <tr>
                            <th class="GridHeader"><a ng-click="orderBy('Owner')">Owner</a></th>
                            <th class="GridHeader"><a ng-click="orderBy('Address')">Address</a></th>
                            <th class="GridHeader">Is Subj Prop?</th>
                            <th class="GridHeader"><a ng-click="orderBy('Status')">Status</a></th>
                            <th class="GridHeader"><a ng-click="orderBy('NetRentInc')">Cash Flow</a></th>
                        </tr>
                        <tr 
                            ng-if="vm.loan && vm.loan.RealProperties" 
                            ng-repeat="realProperty in vm.loan.RealProperties | filter:realPropertyMatchesFilter track by $index" 
                            ng-class="{selected: realProperty.Id === selectedRealProperty.Id, GridAutoItem: realProperty.Id !== selectedRealProperty.Id}" 
                            ng-click="selectRealProperty(realProperty);">
                            <td class="entity-cell">
                                <div ng-if="realProperty.Owners.length > 0">
                                    {{getConsumerName(realProperty.Owners[0].ConsumerId)}}
                                </div>
                                <div ng-if="realProperty.Owners.length == 2">
                                    {{getConsumerName(realProperty.Owners[1].ConsumerId)}}
                                </div>
                                <div ng-if="realProperty.Owners.length > 2">
                                    <span ng-show="!realProperty.ShowAllConsumers" ng-click="realProperty.ShowAllConsumers = !realProperty.ShowAllConsumers">
                                        &#x25ba; and {{realProperty.Owners.length - 1}} others
                                    </span>
                                    <div ng-show="realProperty.ShowAllConsumers" ng-click="realProperty.ShowAllConsumers = !realProperty.ShowAllConsumers">
                                        <div class="additional-consumer-name" ng-repeat="consumerId in realProperty.Owners.slice(1)">
                                            <span ng-if="$index == 0">&#x25bc;</span> {{getConsumerName(consumerId.ConsumerId)}}
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="entity-cell" ng-bind="getFullRealPropertyAddress(realProperty);"></td>
                            <td class="entity-cell" ng-bind="realProperty.IsSubjectProp ? 'Yes' : 'No'"></td>
                            <td class="entity-cell" ng-bind="getFriendlyReoStatusName(realProperty.Status);"></td>
                            <td class="entity-cell" ng-bind="realProperty.NetRentIncLocked ? realProperty.NetRentIncData : realProperty.NetRentInc"></td>
                        </tr>
                    </table>
                </div>

                <table class="FormTable page-width">
                    <tr>
                        <td colspan="4">
                            <input type="button" value="<< Prev" ng-click="moveToPreviousRecord();" ng-disabled="selectedRecordIndex === 0" />
                            <input type="button" value="Next >>" ng-click="moveToNextRecord();" ng-disabled="vm.loan.RealProperties.length === 0 || selectedRecordIndex === vm.loan.RealProperties.length - 1" />
                            <input type="button" value="Insert" ng-click="insertRecord();" />
                            <input type="button" value="Add" ng-click="addRecord();" />
                            <input type="button" value="Save" ng-click="saveData();" ng-disabled="!isDirty();" />
                            <input type="button" value="Move Up" ng-click="moveRecordUp();" ng-if="realPropertyFilterModel == 0" ng-disabled="selectedRecordIndex === 0" />
                            <input type="button" value="Move Down" ng-click="moveRecordDown();" ng-if="realPropertyFilterModel == 0" ng-disabled="vm.loan.RealProperties.length === 0 || selectedRecordIndex === vm.loan.RealProperties.length - 1" />
                            <input type="button" value="Delete" ng-click="deleteRecord();" ng-disabled="vm.loan.RealProperties.length === 0" />
                        </td>
                    </tr>
                </table>

                <table class="FormTable page-width left-padded-table" ng-if="selectedRealProperty != null">
                    <tr>
                        <td><label class="FieldLabel">Owner</label></td>
                        <td>
                            <div ng-repeat="consumerAssociation in selectedRealProperty.Owners">
                                {{getConsumerName(consumerAssociation.ConsumerId)}}
                            </div>

                            <a ng-click="editRealPropertyOwners(selectedRealProperty);">edit</a>
                        </td>
                    </tr>
                </table>

                <table class="InsetBorder FormTable address-table left-padded-table" ng-if="selectedRealProperty != null">
                    <tr>
                        <td>
                            <input type="button" ng-click="copyFromBorrowerPresentAddress();" value="Copy From Borrower Present Address" />
                        </td>
                        <td>
                            <input type="checkbox" id="IsPrimaryResidence" ng-model="selectedRealProperty.IsPrimaryResidence" ng-change="updateCashFlowCalculationCheckbox();" /> 
                            <label for="IsPrimaryResidence" class="FieldLabel">Is Primary Residence</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="button" ng-click="copyFromSubjectProperty();" value="Copy From Subject Property" />
                        </td>
                        <td>
                            <input type="checkbox" id="IsSubjectProp" ng-model="selectedRealProperty.IsSubjectProp" ng-change="updateCashFlowCalculationCheckbox();" /> 
                            <label for="IsSubjectProp" class="FieldLabel">Is Subject Property</label>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="FieldLabel">Property Address</label></td>
                        <td>
                            <input type="text" ng-model="selectedRealProperty.StreetAddress" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input id="City" ng-model="selectedRealProperty.City" type="text" />
                            <select id="State" runat="server" ng-model="selectedRealProperty.State"></select>
                            <input
                                type="text"
                                id="Zip" 
                                ng-model="selectedRealProperty.Zip" 
                                preset="zipcode" 
                                mask="#####" 
                                maxlength="5" 
                                onfocus="this.oldValue = this.value;" 
                                city="City" 
                                state="State" 
                                county=""
                                onblur="smartZipcodeAuto(event);" />
                        </td>
                    </tr>
                </table>

                <table class="InsetBorder FormTable type-status-table left-padded-table" ng-if="selectedRealProperty != null">
                    <tr>
                        <td><label class="FieldLabel">Type</label></td>
                        <td>
                            <select runat="server" id="Type" ng-model="selectedRealProperty.Type"></select>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="FieldLabel">Status</label></td>
                        <td>
                            <select id="Status" ng-model="selectedRealProperty.Status" ng-change="onStatusChange();">
                                <option value="1">Sold</option>
                                <option value="2">Pending Sale</option>
                                <option value="3">Rental</option>
                                <option value="0">Retained</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="FieldLabel">Occ. Rate</label></td>
                        <td>
                            <input 
                                type="text" 
                                class="mini" 
                                preset="percent" 
                                ng-model="selectedRealProperty.OccR" 
                                ng-disabled="selectedRealProperty.Status != '3'"
                                ng-blur="refreshCalculation();"/>
                        </td>
                    </tr>
                    <tr ng-if="selectedRealProperty.Status == '0' || selectedRealProperty.Status == '2'">
                        <td colspan="2">
                            <input type="checkbox" ng-model="selectedRealProperty.IsForceCalcNetRentalInc" ng-disabled="selectedRealProperty.IsSubjectProp" ng-change="refreshCalculation();" />
                            <label class="FieldLabel">Calculate Cash Flow</label>
                        </td>
                    </tr>
                </table>

                <table class="InsetBorder FormTable linked-liabilities-table left-padded-table" ng-if="selectedRealProperty != null">
                    <tr ng-if="getLinkedLiabilities(selectedRealProperty['Id']).length === 0">
                        <th colspan="3">
                            No Linked Liabilities
                        </th>
                    </tr>
                    <tbody ng-if="getLinkedLiabilities(selectedRealProperty['Id']).length !== 0">
                        <tr>
                            <th colspan="4">
                                Linked Liabilities
                            </th>
                        </tr>
                        <tr>
                            <th>
                                <!-- View link column -->
                            </th>
                            <th>
                                Creditor Name
                            </th>
                            <th>
                                Balance
                            </th>
                            <th>
                                Payment
                            </th>
                        </tr>
                        <tr ng-repeat="linkedLiability in getLinkedLiabilities(selectedRealProperty['Id'])">
                            <td>
                                <a ng-click="goToLinkedLiability(linkedLiability['Id']);">view</a>
                            </td>
                            <td>
                                <input type="text" disabled="disabled" class="medium" ng-value="linkedLiability.CompanyName" />
                            </td>
                            <td>
                                <input type="text" disabled="disabled" class="small" ng-value="linkedLiability.Bal" />
                            </td>
                            <td>
                                <input type="text" disabled="disabled" class="small" ng-value="linkedLiability.Pmt" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                    
                <table class="InsetBorder FormTable" ng-if="selectedRealProperty != null">
                    <tr>
                        <td><label class="FieldLabel">Market Value</label></td>
                        <td><label class="FieldLabel">Mtg Amount</label></td>
                        <td><label class="FieldLabel">Gross Rent</label></td>
                        <td><label class="FieldLabel">Mtg Payment</label></td>
                        <td><label class="FieldLabel">Ins/Maint/Taxes</label></td>
                        <td><label class="FieldLabel">Cash Flow</label></td>
                    </tr>
                    <tr>
                        <td>
                            <input id="MarketValue" type="text" class="small" preset="money" ng-model="selectedRealProperty.MarketValue" ng-blur="refreshCalculation();" />
                        </td>
                        <td>
                            <input id="MtgAmt" type="text" class="small" preset="money" ng-model="selectedRealProperty.MtgAmt" ng-blur="refreshCalculation();" />
                        </td>
                        <td>
                            <input id="GrossRentInc" type="text" class="small" preset="money" ng-model="selectedRealProperty.GrossRentInc" ng-blur="refreshCalculation();" />
                        </td>
                        <td>
                            <input id="MtgPmt" type="text" class="small" preset="money" ng-model="selectedRealProperty.MtgPmt" ng-blur="refreshCalculation();" />
                        </td>
                        <td>
                            <input id="HExp" type="text" class="small" preset="money" ng-model="selectedRealProperty.HExp" ng-blur="refreshCalculation();" />
                        </td>
                        <td>
                            <input type="checkbox" ng-model="selectedRealProperty.NetRentIncLocked" ng-click="onNetRentIncLockedClick();" /> Lock
                            <input type="text" id="NetRentIncData" class="small" preset="money" ng-show="selectedRealProperty.NetRentIncLocked" ng-model="selectedRealProperty.NetRentIncData" ng-blur="refreshCalculation();" />
                            <input type="text" id="NetRentInc" class="small" preset="money" ng-show="!selectedRealProperty.NetRentIncLocked" ng-model="selectedRealProperty.NetRentInc" disabled="disabled" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
