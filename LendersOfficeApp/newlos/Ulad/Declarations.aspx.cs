﻿#region Generated code
namespace LendersOfficeApp.newlos.Ulad
#endregion
{
    using System;

    /// <summary>
    /// Provides a means for managing declarations
    /// on ULAD applications.
    /// </summary>
    public partial class Declarations : BaseLoanPage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="args">
        /// The parameter is not used.
        /// </param>
        protected override void OnInit(EventArgs args)
        {
            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;

            this.RegisterCSS("UladEditor.css");
            this.RegisterCSS("LiabilityPmlAuditHistoryTable.css");

            this.RegisterJsScript("Ulad.Declarations.js");
            this.RegisterJsScript("angular-1.5.5.min.js");
            this.RegisterJsScript("mask.js");

            DataAccess.Tools.Bind_aDecPastOwnedPropT(this.DecOwnershipIntOtherPropLastThreeYearsPropTypeUlad, includeFhaSecondaryResidence: true);
            DataAccess.Tools.Bind_aDecPastOwnedPropTitleT(this.DecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad);

            base.OnInit(args);
        }
    }
}