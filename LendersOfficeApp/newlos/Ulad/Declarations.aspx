﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Declarations.aspx.cs" Inherits="LendersOfficeApp.newlos.Ulad.Declarations" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Declarations</title>
    <style type="text/css">
        .single-level-indent {
            padding-left: 15px;
        }

        .double-level-indent {
            padding-left: 30px;
        }

        div.column-container {
            display: flex;
        }

        div.column {
            display: inline-flex;
            vertical-align: top;
            flex-wrap: nowrap;
        }

        div.right-border {
            border-right: 1px solid #000;
        }

        div.table {
            display: table;
        }

        div.table > div {
            display: table-row;
        }

        div.table > div > div {
            display: table-cell;
        }

        div.table > div > div:last-child {
            padding-left: 10px;
        }

        div.table > div > div:nth-last-child(2) {
            border-right: 1px solid #000;
            padding-right: 10px;
        }

        div.table > div.border-row > div {
            border-bottom: 1px solid #000;
            padding-bottom: 5px;
        }

        div.table > div.border-row + div > div {
            padding-top: 5px;
        }

        div.table > div.borrower-picker-row {
            background-color: #777;
            border: 1px solid #000;
            border-left: none;
            color: #FFF;
            cursor: pointer;
            display: flex;
            margin-bottom: 3px;
            min-height: 30px;
            width: 250px;
        }

        div.table > div.borrower-picker-row:first-child {
            margin-top: 3px;
        }

        div.table > div.borrower-picker-row > div {
            border: none;
            display: inline-flex;
            min-height: 30px;
            line-height: 30px;
        }

        div.table > div.borrower-picker-row > div.borrower-picker-row-border {
            background-color: darkred;
            padding: 0;
            margin: 0 0 0 auto;
            width: 4px;
        }

        div.table > div.borrower-picker-row.selected {
            background-color: gainsboro;
            border-left: none;
            color: #000;
            font-weight: bold;
            padding-left: 10px;
            width: 240px;
        }

        input[type=text].explanation-input {
            width: 550px;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        function saveMe() {
            angular.element('#DeclarationsControllerDiv').scope().saveData();
            return true;
        }

        function saveAndRedirect(postbackHandler) {
            // Used for the save confirmation dialog when navigating away from the page.
            // This will allow the async save to complete before redirecting the user.
            angular.element('#DeclarationsControllerDiv').scope().saveData(postbackHandler);
        }

        function format_money_callback(input) {
            angular.element('#DeclarationsControllerDiv').scope().onFormatMoney(input);
        }
    </script>
    <form id="form1" runat="server">
        <div ng-app="Declarations">
            <div id="DeclarationsControllerDiv" ng-controller="DeclarationsController">
                <h4 class="page-header">Declarations</h4>
                <div class="column-container">
                    <div class="column right-border">
                        <div class="table">
                            <div>
                                <div>
                                    <span class="FieldLabel">A.</span> Will you occupy the property as your primary residence?

                                    <div class="double-level-indent" ng-show="selectedConsumer.DecIsPrimaryResidenceUlad == triStateTrueValue">
                                        Explanation: <input class="explanation-input" type="text" maxlength="512" ng-model="selectedConsumer.DecIsPrimaryResidenceExplanationUlad" />
                                    </div>
                                </div>
                                <div>
                                    <tri-state-checkbox-list ng-model="selectedConsumer.DecIsPrimaryResidenceUlad"></tri-state-checkbox-list>
                                </div>
                            </div>
                            <div>
                                <div class="single-level-indent">
                                    If YES, have you had an ownership interest in another property in the last three years?

                                    <div ng-show="selectedConsumer.DecHasOwnershipIntOtherPropLastThreeYearsUlad == triStateTrueValue" class="double-level-indent">
                                        Explanation: <input class="explanation-input" type="text" maxlength="512" ng-model="selectedConsumer.DecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad" />
                                    </div>
                                </div>
                                <div>
                                    <tri-state-checkbox-list ng-model="selectedConsumer.DecHasOwnershipIntOtherPropLastThreeYearsUlad"></tri-state-checkbox-list>
                                </div>
                            </div>
                            <div>
                                <div class="double-level-indent">
                                    If YES, complete (1) and (2) below:
                                </div>
                                <div>&nbsp;</div>
                            </div>
                            <div>
                                <div class="double-level-indent">
                                    (1) What type of property did you own: primary residence (PR), FHA secondary residence (SR), second home (SH), or investment property (IP)?
                                </div>
                                <div>
                                    <select runat="server" id="DecOwnershipIntOtherPropLastThreeYearsPropTypeUlad" ng-model="selectedConsumer.DecOwnershipIntOtherPropLastThreeYearsPropTypeUlad"></select>
                                </div>
                            </div>
                            <div class="border-row">
                                <div class="double-level-indent">
                                    (2) How did you hold title to the property: by yourself (S), jointly with your spouse (SP), or jointly with another person (O)?
                                </div>
                                <div>
                                    <select runat="server" id="DecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad" ng-model="selectedConsumer.DecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad"></select>
                                </div>
                            </div>

                            <div class="border-row">
                                <div>
                                    <span class="FieldLabel">B.</span> If this is a Purchase Transaction: Do you have a family relationship or business affiliation with the seller of the property? 

                                    <div class="double-level-indent" ng-show="selectedConsumer.DecIsFamilyOrBusAffiliateOfSellerUlad == triStateTrueValue">
                                        Explanation: <input class="explanation-input" type="text" maxlength="512" ng-model="selectedConsumer.DecIsFamilyOrBusAffiliateOfSellerExplanationUlad" />
                                    </div>
                                </div>
                                <div>
                                    <tri-state-checkbox-list ng-model="selectedConsumer.DecIsFamilyOrBusAffiliateOfSellerUlad"></tri-state-checkbox-list>
                                </div>
                            </div>

                            <div>
                                <div>
                                    <span class="FieldLabel">C.</span> 
                                    Are you borrowing any money for this real estate transaction (e.g., money for your closing costs or down payment) or <br />
                                    <span class="single-level-indent">
                                        obtaining any money from another party, such as the seller or realtor, that you have not disclosed on this loan application? 
                                    </span>

                                    <div class="double-level-indent" ng-show="selectedConsumer.DecIsBorrowingFromOthersForTransUlad == triStateTrueValue">
                                        Explanation: <input class="explanation-input" type="text" maxlength="512" ng-model="selectedConsumer.DecIsBorrowingFromOthersForTransExplanationUlad" />
                                    </div>
                                </div>
                                <div>
                                    <tri-state-checkbox-list ng-model="selectedConsumer.DecIsBorrowingFromOthersForTransUlad"></tri-state-checkbox-list>
                                </div>
                            </div>
                            <div class="border-row">
                                <div class="double-level-indent">
                                    If YES, what is the amount of this money? 
                                </div>
                                <div>
                                    <input id="DecAmtBorrowedFromOthersForTransUlad" type="text" preset="money" ng-model="selectedConsumer.DecAmtBorrowedFromOthersForTransUlad" />
                                </div>
                            </div>

                            <div>
                                <div>
                                    <span class="FieldLabel">D.</span> 
                                    1. Have you or will you be applying for a mortgage loan on another property (not the property securing this loan) on or <br />
                                    <span class="double-level-indent">
                                        before closing this transaction that is not disclosed on this loan application? 
                                    </span>
                                    <div class="double-level-indent" ng-show="selectedConsumer.DecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad == triStateTrueValue">
                                        Explanation: <input class="explanation-input" type="text" maxlength="512" ng-model="selectedConsumer.DecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad" />
                                    </div>
                                </div>
                                <div>
                                    <tri-state-checkbox-list ng-model="selectedConsumer.DecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad"></tri-state-checkbox-list>
                                </div>
                            </div>
                            <div class="border-row">
                                <div class="single-level-indent">
                                    2. Have you or will you be applying for any new credit (e.g., installment loan, credit card, etc.) on or before closing this loan that <br />
                                    <span class="single-level-indent">
                                        is not disclosed on this application?
                                    </span>
                                    <div class="double-level-indent" ng-show="selectedConsumer.DecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad == triStateTrueValue">
                                        Explanation: <input class="explanation-input" type="text" maxlength="512" ng-model="selectedConsumer.DecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad" />
                                    </div>
                                </div>
                                <div>
                                    <tri-state-checkbox-list ng-model="selectedConsumer.DecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad"></tri-state-checkbox-list>
                                </div>
                            </div>

                            <div class="border-row">
                                <div>
                                    <span class="FieldLabel">E.</span> 
                                    Will this property be subject to a lien that could take priority over the first mortgage lien, such as a clean energy lien paid <br />
                                    <span class="single-level-indent">
                                        through your property taxes (e.g.,the Property Assessed Clean Energy Program)? 
                                    </span>
                                    <div class="double-level-indent" ng-show="selectedConsumer.DecPropSubjectToHigherPriorityLienThanFirstMtgUlad == triStateTrueValue">
                                        Explanation: <input class="explanation-input" type="text" maxlength="512" ng-model="selectedConsumer.DecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad" />
                                    </div>
                                </div>
                                <div>
                                    <tri-state-checkbox-list ng-model="selectedConsumer.DecPropSubjectToHigherPriorityLienThanFirstMtgUlad"></tri-state-checkbox-list>
                                </div>
                            </div>

                            <div class="border-row">
                                <div>
                                    <span class="FieldLabel">F.</span> 
                                    Are you a co-signer or guarantor on any debt or loan that is not disclosed on this application?
                                    <div class="double-level-indent" ng-show="selectedConsumer.DecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad == triStateTrueValue">
                                        Explanation: <input class="explanation-input" type="text" maxlength="512" ng-model="selectedConsumer.DecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad" />
                                    </div>
                                </div>
                                <div>
                                    <tri-state-checkbox-list ng-model="selectedConsumer.DecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad"></tri-state-checkbox-list>
                                </div>
                            </div>

                            <div class="border-row">
                                <div>
                                    <span class="FieldLabel">G.</span> 
                                    Are there any outstanding judgments against you?
                                    <div class="double-level-indent" ng-show="selectedConsumer.DecHasOutstandingJudgmentsUlad == triStateTrueValue">
                                        Explanation: <input class="explanation-input" type="text" maxlength="512" ng-model="selectedConsumer.DecHasOutstandingJudgmentsExplanationUlad" />
                                    </div>
                                </div>
                                <div>
                                    <tri-state-checkbox-list ng-model="selectedConsumer.DecHasOutstandingJudgmentsUlad"></tri-state-checkbox-list>
                                </div>
                            </div>

                            <div class="border-row">
                                <div>
                                    <span class="FieldLabel">H.</span> 
                                    Are you currently delinquent or in default on a federal debt?
                                    <div class="double-level-indent" ng-show="selectedConsumer.DecIsDelinquentOrDefaultFedDebtUlad == triStateTrueValue">
                                        Explanation: <input class="explanation-input" type="text" maxlength="512" ng-model="selectedConsumer.DecIsDelinquentOrDefaultFedDebtExplanationUlad" />
                                    </div>
                                </div>
                                <div>
                                    <tri-state-checkbox-list ng-model="selectedConsumer.DecIsDelinquentOrDefaultFedDebtUlad"></tri-state-checkbox-list>
                                </div>
                            </div>

                            <div class="border-row">
                                <div>
                                    <span class="FieldLabel">I.</span> 
                                    Are you a party to a lawsuit in which you potentially have any personal financial liability? 
                                    <div class="double-level-indent" ng-show="selectedConsumer.DecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad == triStateTrueValue">
                                        Explanation: <input class="explanation-input" type="text" maxlength="512" ng-model="selectedConsumer.DecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad" />
                                    </div>
                                </div>
                                <div>
                                    <tri-state-checkbox-list ng-model="selectedConsumer.DecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad"></tri-state-checkbox-list>
                                </div>
                            </div>

                            <div class="border-row">
                                <div>
                                    <span class="FieldLabel">J.</span> 
                                    Have you conveyed title to any property in lieu of foreclosure in the past 7 years? 
                                    <div class="double-level-indent" ng-show="selectedConsumer.DecHasConveyedTitleInLieuOfFcLast7YearsUlad == triStateTrueValue">
                                        Explanation: <input class="explanation-input" type="text" maxlength="512" ng-model="selectedConsumer.DecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad" />
                                    </div>
                                </div>
                                <div>
                                    <tri-state-checkbox-list ng-model="selectedConsumer.DecHasConveyedTitleInLieuOfFcLast7YearsUlad"></tri-state-checkbox-list>
                                </div>
                            </div>

                            <div class="border-row">
                                <div>
                                    <span class="FieldLabel">K.</span> 
                                    Within the past 7 years, have you completed a pre-foreclosure sale or short sale, whereby the property was sold to a <br />
                                    <span class="single-level-indent">
                                        third party and the Lender agreed to accept less than the outstanding mortgage balance due? 
                                    </span>
                                    <div class="double-level-indent" ng-show="selectedConsumer.DecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad == triStateTrueValue">
                                        Explanation: <input class="explanation-input" type="text" maxlength="512" ng-model="selectedConsumer.DecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad" />
                                    </div>
                                </div>
                                <div>
                                    <tri-state-checkbox-list ng-model="selectedConsumer.DecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad"></tri-state-checkbox-list>
                                </div>
                            </div>

                            <div class="border-row">
                                <div>
                                    <span class="FieldLabel">L.</span> 
                                    Have you had property foreclosed upon in the last 7 years?
                                    <div class="double-level-indent" ng-show="selectedConsumer.DecHasForeclosedLast7YearsUlad == triStateTrueValue">
                                        Explanation: <input class="explanation-input" type="text" maxlength="512" ng-model="selectedConsumer.DecHasForeclosedLast7YearsExplanationUlad" />
                                    </div>
                                </div>
                                <div>
                                    <tri-state-checkbox-list ng-model="selectedConsumer.DecHasForeclosedLast7YearsUlad"></tri-state-checkbox-list>
                                </div>
                            </div>

                            <div>
                                <div>
                                    <span class="FieldLabel">M.</span> 
                                    Have you declared bankruptcy within the past 7 years?
                                    <div class="double-level-indent" ng-show="selectedConsumer.DecHasBankruptcyLast7YearsUlad == triStateTrueValue">
                                        Explanation: <input class="explanation-input" type="text" maxlength="512" ng-model="selectedConsumer.DecHasBankruptcyLast7YearsExplanationUlad" />
                                    </div>
                                </div>
                                <div>
                                    <tri-state-checkbox-list ng-model="selectedConsumer.DecHasBankruptcyLast7YearsUlad"></tri-state-checkbox-list>
                                </div>
                            </div>
                            <div class="border-row">
                                <div class="single-level-indent">
                                    If YES, identify the type(s) of bankruptcy: 
                                    <input type="checkbox" ng-model="selectedConsumer.DecHasBankruptcyChapter7Ulad" /> Chapter 7
                                    <input type="checkbox" ng-model="selectedConsumer.DecHasBankruptcyChapter11Ulad" /> Chapter 11
                                    <input type="checkbox" ng-model="selectedConsumer.DecHasBankruptcyChapter12Ulad" /> Chapter 12
                                    <input type="checkbox" ng-model="selectedConsumer.DecHasBankruptcyChapter13Ulad" /> Chapter 13
                                </div>
                                <div>&nbsp;</div>
                            </div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="table" ng-if="vm.loan.Consumers != null">
                            <div ng-repeat="consumer in vm.loan.Consumers" ng-class="['borrower-picker-row', { 'selected': selectedConsumer.Id === consumer.Id}]" ng-click="setSelectedConsumer(consumer.Id)">
                                <div ng-bind="getConsumerName(consumer)"></div>
                                <div class="borrower-picker-row-border" ng-if="selectedConsumer.Id === consumer.Id"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
