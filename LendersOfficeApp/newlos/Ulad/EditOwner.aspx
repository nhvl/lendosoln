﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditOwner.aspx.cs" Inherits="LendersOfficeApp.newlos.Ulad.EditOwner" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Select Owners</title>
    <style type="text/css">
        table {
            width: 100%;
        }

        form {
            min-width: 400px;
            min-height: 225px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <h4 class="page-header">
            <!-- 
                The popup's title will be updated based on the type of ownership, 
                e.g. "Select Owners" for primary association.
            -->
        </h4>
        <div ng-app="Owners">
            <div ng-controller="OwnersController">
                <div class="FieldLabel align-center loading-div" ng-if="!vm.loan || !vm.loan.Consumers">
                    Loading consumers...
                </div>
                <table id="OwnerTable" class="FormTable" ng-if="vm.loan && vm.loan.Consumers">
                    <tr>
                        <th ng-bind="getOwnerLabel()"></th>
                        <th>Name</th>
                        <th ng-if="needsPrimary()">Primary Owner</th>
                    </tr>
                    <tr ng-repeat="consumer in vm.loan.Consumers" class="GridAutoItem">
                        <td class="align-center">
                            <input class="owner-checkbox" type="checkbox" ng-model="consumer.IsSelected" ng-click="setOwnerSelectedState(consumer);" />
                        </td>
                        <td class="align-center" ng-bind="getConsumerName(consumer)"></td>
                        <td class="align-center" ng-if="needsPrimary()">
                            <input type="radio" name="PrimaryOwner" ng-disabled="!consumer.IsSelected" ng-model="consumer.IsPrimary" ng-value="true" ng-click="setPrimaryOwnerState(consumer);" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="align-center">
                            <input type="button" ng-click="onEditOwner();" value="OK" />
                            <input type="button" onclick="parent.LQBPopup.Hide();" value="Cancel" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
