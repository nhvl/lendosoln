﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IncomeSources.aspx.cs" Inherits="LendersOfficeApp.newlos.Ulad.IncomeSources" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Income</title>
    <style type="text/css">
        .legend-cell {
            border-bottom: 1px solid #000;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        function onApplicationFilterChange() {
            var newApplicationFilter = $('#ApplicationFilter').val();
            angular.element('#IncomeSourcesControllerDiv').scope().onApplicationFilterChange(newApplicationFilter);
        }

        function saveMe() {
            angular.element('#IncomeSourcesControllerDiv').scope().saveData();
            return true;
        }

        function saveAndRedirect(postbackHandler) {
            // Used for the save confirmation dialog when navigating away from the page.
            // This will allow the async save to complete before redirecting the user.
            angular.element('#IncomeSourcesControllerDiv').scope().saveData(postbackHandler);
        }

        function format_money_callback(input) {
            angular.element('#IncomeSourcesControllerDiv').scope().onFormatMoney(input);
        }        
    </script>
    <form id="form1" runat="server">
        <div ng-app="IncomeSources">
            <div id="IncomeSourcesControllerDiv" ng-controller="IncomeSourcesController">
                <h4 class="page-header">Income</h4>

                <table class="FormTable page-width header-table">
                    <tr>
                        <td colspan="2">
                            Show

                            <select id="IncomeSourceFilter" ng-model="incomeSourceFilterModel" ng-change="onIncomeSourceFilterChange('{{incomeSourceFilterModel}}');" NotForEdit="true">
                                <option value="0">All</option>
                                <option value="1">Selected Legacy Application</option>
                                <option value="2">Selected ULAD Application</option>
                                <option value="3">Selected Borrower</option>
                            </select>

                            <label class="app-label" ng-bind="applicationLabelsByValue[incomeSourceFilterModel]" ng-if="incomeSourceFilterModel !== '0'">&nbsp;</label>

                            <select id="ApplicationFilter" onchange="onApplicationFilterChange()" NotForEdit="true" ng-if="incomeSourceFilterModel !== '0'">
                                <option ng-repeat="appKeyValuePair in selectedApplications" value="{{appKeyValuePair.key}}">
                                    {{getAppName(appKeyValuePair.values)}}
                                </option>
                            </select>
                        </td>
                        <td>
                            Subtotal Income
                        </td>
                        <td>
                            <input type="text" ng-model="vm.incomeSourceSubtotals.monthlyAmount" ng-disabled="true" class="small" />
                        </td>
                        <td>
                            Total Loan Income
                        </td>
                        <td>
                            <input type="text" ng-model="vm.incomeSourceTotals.monthlyAmount" ng-disabled="true" class="small" />
                        </td>
                    </tr>
                </table>

                <div class="listing-container">
                    <table class="FormTable page-width">
                        <tr>
                            <th class="GridHeader"><a ng-click="orderBy('Owner')">Owner</a></th>
                            <th class="GridHeader"><a ng-click="orderBy('IncomeType')">Income Type</a></th>
                            <th class="GridHeader"><a ng-click="orderBy('Description')">Description</a></th>
                            <th class="GridHeader"><a ng-click="orderBy('MonthlyAmountData')">Monthly Amount</a></th>
                        </tr>
                        <tr 
                            ng-if="vm.loan && vm.loan.IncomeSources" 
                            ng-repeat="incomeSource in vm.loan.IncomeSources | filter:incomeSourceMatchesFilter track by $index" 
                            ng-class="{selected: incomeSource.Id === selectedIncomeSource.Id, GridAutoItem: incomeSource.Id !== selectedIncomeSource.Id}" 
                            ng-click="selectIncomeSource(incomeSource);">
                            <td class="entity-cell">
                                <div ng-if="incomeSource.Owners.length > 0">
                                    {{getConsumerName(incomeSource.Owners[0].ConsumerId)}}
                                </div>
                                <div ng-if="incomeSource.Owners.length == 2">
                                    {{getConsumerName(incomeSource.Owners[1].ConsumerId)}}
                                </div>
                                <div ng-if="incomeSource.Owners.length > 2">
                                    <span ng-show="!incomeSource.ShowAllConsumers" ng-click="incomeSource.ShowAllConsumers = !incomeSource.ShowAllConsumers">
                                        &#x25ba; and {{incomeSource.Owners.length - 1}} others
                                    </span>
                                    <div ng-show="incomeSource.ShowAllConsumers" ng-click="incomeSource.ShowAllConsumers = !incomeSource.ShowAllConsumers">
                                        <div class="additional-consumer-name" ng-repeat="consumerId in incomeSource.Owners.slice(1)">
                                            <span ng-if="$index == 0">&#x25bc;</span> {{getConsumerName(consumerId.ConsumerId)}}
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="entity-cell" ng-bind="getFriendlyIncomeSourceTypeName(incomeSource.IncomeType);"></td>
                            <td class="entity-cell" ng-bind="incomeSource.Description"></td>
                            <td class="entity-cell" ng-bind="incomeSource.MonthlyAmountData"></td>
                        </tr>
                    </table>
                </div>

                <table class="FormTable page-width">
                    <tr>
                        <td colspan="4">
                            <input type="button" value="<< Prev" ng-click="moveToPreviousRecord();" ng-disabled="selectedRecordIndex === 0" />
                            <input type="button" value="Next >>" ng-click="moveToNextRecord();" ng-disabled="vm.loan.IncomeSources.length === 0 || selectedRecordIndex === vm.loan.IncomeSources.length - 1" />
                            <input type="button" value="Insert" ng-click="insertRecord();" />
                            <input type="button" value="Add" ng-click="addRecord();" />
                            <input type="button" value="Save" ng-click="saveData();" ng-disabled="!isDirty();" />
                            <input type="button" value="Move Up" ng-click="moveRecordUp();" ng-if="incomeSourceFilterModel == 0" ng-disabled="selectedRecordIndex === 0" />
                            <input type="button" value="Move Down" ng-click="moveRecordDown();" ng-if="incomeSourceFilterModel == 0" ng-disabled="vm.loan.IncomeSources.length === 0 || selectedRecordIndex === vm.loan.IncomeSources.length - 1" />
                            <input type="button" value="Delete" ng-click="deleteRecord();" ng-disabled="vm.loan.IncomeSources.length === 0" />
                        </td>
                    </tr>
                </table>

                <div class="InsetBorder">
                    <table class="FormTable inline-flex-display" ng-if="selectedIncomeSource != null">
                        <tr>
                            <td><label class="FieldLabel">Owner</label></td>
                            <td>
                                <div ng-repeat="consumerAssociation in selectedIncomeSource.Owners">
                                    {{getConsumerName(consumerAssociation.ConsumerId)}}
                                </div>

                                <a ng-click="editIncomeSourceOwners(selectedIncomeSource);">edit</a>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="FieldLabel">Income Type</label></td>
                            <td>
                                <select id="IncomeType" runat="server" ng-model="selectedIncomeSource.IncomeType" ng-change="updateDirtyBit();"></select>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="FieldLabel">Description</label></td>
                            <td>
                                <input type="text" ng-model="selectedIncomeSource.Description" ng-disabled="selectedIncomeSource.IncomeType == ''" />
                            </td>
                        </tr>
                        <tr>
                            <td><label class="FieldLabel">Monthly Amount</label></td>
                            <td>
                                <input type="text" id="SelectedIncomeSourceMonthlyAmountData" ng-model="selectedIncomeSource.MonthlyAmountData" ng-disabled="selectedIncomeSource.IncomeType == ''" preset="money" ng-blur="refreshCalculation();"/>
                            </td>
                        </tr>                    
                    </table>    
                
                    <table class="FormTable inline-flex-display align-top" ng-if="selectedIncomeSource != null">
                        <tr>
                            <td class="legend-cell"><label class="FieldLabel">Employment Records</label></td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tbody ng-if="getEmploymentRecords(selectedIncomeSource['Id']).length === 0">
                                        <tr>
                                            <td>No employment records associated with this income source.</td>
                                        </tr>
                                    </tbody>
                                    <tbody ng-if="getEmploymentRecords(selectedIncomeSource['Id']).length !== 0">
                                        <tr>
                                            <th class="GridHeader align-left">Name</th>
                                            <th class="GridHeader RightAlign">Monthly Income</th>
                                            <th class="GridHeader RightAlign">From</th>
                                            <th class="GridHeader RightAlign">To</th>
                                        </tr>
                                        <tr ng-repeat="employmentRecord in getEmploymentRecords(selectedIncomeSource['Id'])" class="GridAutoItem">
                                            <td class="align-left" ng-bind="employmentRecord.EmployerName"></td>
                                            <td class="RightAlign" ng-bind="employmentRecord.MonthlyIncome"></td>
                                            <td class="RightAlign" ng-bind="employmentRecord.EmploymentStartDate"></td>
                                            <td class="RightAlign" ng-bind="getEmploymentToDate(employmentRecord);"></td>
                                        </tr>
                                    </tbody>
                                    <tbody>
                                        <tr>
                                            <td colspan="4">
                                                <a ng-click="editEmploymentRecords(selectedIncomeSource['Id']);">edit</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
