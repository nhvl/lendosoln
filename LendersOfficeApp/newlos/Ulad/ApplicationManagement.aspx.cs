﻿#region Generated code
namespace LendersOfficeApp.newlos.Ulad
#endregion
{
    /// <summary>
    /// Provides a means for managing applications on a ULAD file.
    /// </summary>
    public partial class ApplicationManagement : BaseLoanPage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="args">
        /// The parameter is not used.
        /// </param>
        protected override void OnInit(System.EventArgs args)
        {
            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;

            this.RegisterCSS("Tabs.css");
            this.RegisterCSS("jquery-ui-1.11.custom.css");

            this.RegisterJsScript("Ulad.ApplicationManagement.js");
            this.RegisterJsScript("angular-1.5.5.min.js");
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");

            base.OnInit(args);
        }
    }
}