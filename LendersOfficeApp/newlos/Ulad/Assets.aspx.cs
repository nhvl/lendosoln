﻿#region Generated code
namespace LendersOfficeApp.newlos.Ulad
#endregion
{
    using System;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// Provides a means for managing assets on
    /// legacy and ULAD applications.
    /// </summary>
    public partial class Assets : BaseLoanPage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="args">
        /// The parameter is not used.
        /// </param>
        protected override void OnInit(EventArgs args)
        {
            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;

            this.RegisterCSS("UladEditor.css");

            this.RegisterJsScript("Ulad.Assets.js");
            this.RegisterJsScript("angular-1.5.5.min.js");
            this.RegisterJsScript("mask.js");

            Tools.Bind_StatesWithoutMilitaryMailCodes(this.State);
            Tools.Bind_AssetT(this.AssetType, useUladAssets: true);
            Tools.Bind_ResponsibleLien(sLienToIncludeCashDepositInTridDisclosures);

            var assetTypes = Enum.GetValues(typeof(E_AssetT)).Cast<E_AssetT>();
            this.RegisterJsObjectWithJsonNetAnonymousSerializer(
                "AssetDescriptionsByValue",
                assetTypes.ToDictionary(e => e.ToString("D"), e => EnumUtilities.GetDescription(e)));

            base.OnInit(args);
        }
    }
}