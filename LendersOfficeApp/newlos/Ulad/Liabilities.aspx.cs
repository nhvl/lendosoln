﻿#region Generated code
namespace LendersOfficeApp.newlos.Ulad
#endregion
{
    using System;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// Provides a means for managing liabilities on
    /// legacy and ULAD applications.
    /// </summary>
    public partial class Liabilities : BaseLoanPage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="args">
        /// The parameter is not used.
        /// </param>
        protected override void OnInit(EventArgs args)
        {
            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;

            this.RegisterCSS("UladEditor.css");
            this.RegisterCSS("LiabilityPmlAuditHistoryTable.css");

            this.RegisterJsScript("Ulad.Liabilities.js");
            this.RegisterJsScript("angular-1.5.5.min.js");
            this.RegisterJsScript("mask.js");

            this.RegisterJsGlobalVariables("IsPmlEnabled", this.BrokerUser.BrokerDB.HasFeatures(LendersOffice.Admin.E_BrokerFeatureT.PriceMyLoan));

            Tools.Bind_DebtT(this.DebtType, useUladDebtTypes: true);
            Tools.BindPredefineLiabilityDescription(this.Description);
            Tools.Bind_sLPurposeT(this.sLPurposeT);
            Tools.Bind_sLT(this.MortgageType);
            Tools.Bind_StatesWithoutMilitaryMailCodes(this.State);

            var debtTypes = Enum.GetValues(typeof(E_DebtT)).Cast<E_DebtT>().ToDictionary(e => e.ToString("D"), e => EnumUtilities.GetDescription(e));

            // The "Other" liability is renamed only for ULAD files.
            // To avoid renaming for non-ULAD files, set the description
            // here instead of using a description attribute.
            debtTypes[E_DebtT.Other.ToString("D")] = "Other Debt Liability";
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("DebtTypeDescriptionsByValue", debtTypes);

            base.OnInit(args);
        }
    }
}