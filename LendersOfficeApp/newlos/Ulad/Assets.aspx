﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Assets.aspx.cs" Inherits="LendersOfficeApp.newlos.Ulad.Assets" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Assets</title>
</head>
<body>
    <script type="text/javascript">
        function onApplicationFilterChange() {
            var newApplicationFilter = $('#ApplicationFilter').val();
            angular.element('#AssetsControllerDiv').scope().onApplicationFilterChange(newApplicationFilter);
        }

        function saveMe() {
            angular.element('#AssetsControllerDiv').scope().saveData();
            return true;
        }

        function saveAndRedirect(postbackHandler) {
            // Used for the save confirmation dialog when navigating away from the page.
            // This will allow the async save to complete before redirecting the user.
            angular.element('#AssetsControllerDiv').scope().saveData(postbackHandler);
        }

        function format_money_callback(input) {
            angular.element('#AssetsControllerDiv').scope().onFormatMoney(input);
        }

        function smartZipcode_callback() {
            angular.element('#AssetsControllerDiv').scope().onSmartZipcode();
        }

        function doAfterDateFormat(input) {
            angular.element('#AssetsControllerDiv').scope().onDateFormat(input);
        }
    </script>
    <form id="form1" runat="server">
        <div ng-app="Assets">
            <div id="AssetsControllerDiv" ng-controller="AssetsController">
                <h4 class="page-header">Assets</h4>

                <table class="FormTable page-width header-table">
                    <tr>
                        <td>
                            Show

                            <select id="AssetFilter" ng-model="assetFilterModel" ng-change="onAssetFilterChange('{{assetFilterModel}}');" NotForEdit="true">
                                <option value="0">All</option>
                                <option value="1">Selected Legacy Application</option>
                                <option value="2">Selected ULAD Application</option>
                                <option value="3">Selected Borrower</option>
                            </select>

                            <label class="app-label" ng-bind="applicationLabelsByValue[assetFilterModel]" ng-if="assetFilterModel !== '0'">&nbsp;</label>

                            <select id="ApplicationFilter" onchange="onApplicationFilterChange()" NotForEdit="true" ng-if="assetFilterModel !== '0'">
                                <option ng-repeat="appKeyValuePair in selectedApplications" value="{{appKeyValuePair.key}}">
                                    {{getAppName(appKeyValuePair.values)}}
                                </option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <a href="RealProperties.aspx">REO</a> &nbsp;
                            <input type="text" ng-model="vm.assetTotals.subtotalReo" ng-disabled="true" class="small" />

                            Subtotal Liquid
                            <input type="text" ng-model="vm.assetTotals.subtotalLiquid" ng-disabled="true" class="small" />

                            Subtotal Other
                            <input type="text" ng-model="vm.assetTotals.subtotalIlliquid" ng-disabled="true" class="small" />

                            Total
                            <input type="text" ng-model="vm.assetTotals.total" ng-disabled="true" class="small" />
                        </td>
                    </tr>
                    <tr ng-if="vm.loan.sTridTargetRegulationVersionT == '2'">
                        <td>
                            Include Cash Deposit In Disclosures For:
                            <select 
                                    id="sLienToIncludeCashDepositInTridDisclosures" 
                                    runat="server" 
                                    ng-model="vm.loan.sLienToIncludeCashDepositInTridDisclosures" 
                                    ng-disabled="vm.loan.sLienPosT == '0' && (vm.loan.sIsOFinNew == false || parseMoneyFloat(vm.loan.sConcurSubFin) <= 0)">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            Enter Cash Deposit, Life Insurance, Retirement Funds, and Business assets as ordinary assets below.
                        </td>
                        <td class="assets-liabilities-completed" ng-if="assetFilterModel == '1'">
                            Assets and Liabilities Completed:
                            <input type="radio" name="AssetLiaCompletedNotJointly" ng-model="vm.AssetLiaCompletedNotJointly" ng-value="false" ng-change="updateDirtyBit();" /> Jointly
                            <input type="radio" name="AssetLiaCompletedNotJointly" ng-model="vm.AssetLiaCompletedNotJointly" ng-value="true" ng-change="updateDirtyBit();" /> Not Jointly
                        </td>
                    </tr>
                </table>

                <div class="listing-container">
                    <table class="FormTable page-width asset-listing">
                        <tr>
                            <th class="GridHeader"><a ng-click="orderBy('Owner')">Owner</a></th>
                            <th class="GridHeader"><a ng-click="orderBy('AssetType')">Asset Type</a></th>
                            <th class="GridHeader"><a ng-click="orderBy('Desc')">Description</a></th>
                            <th class="GridHeader"><a ng-click="orderBy('Value')">Market Value</a></th>
                        </tr>
                        <tr 
                            ng-if="vm.loan && vm.loan.Assets" 
                            ng-repeat="asset in vm.loan.Assets | filter:assetMatchesFilter track by $index" 
                            ng-class="['asset-row', {selected: asset.Id === selectedAsset.Id, GridAutoItem: asset.Id !== selectedAsset.Id}]" 
                            ng-click="selectAsset(asset);">
                            <td class="entity-cell">
                                <div ng-if="asset.Owners.length > 0">
                                    {{getConsumerName(asset.Owners[0].ConsumerId)}}
                                </div>
                                <div ng-if="asset.Owners.length == 2">
                                    {{getConsumerName(asset.Owners[1].ConsumerId)}}
                                </div>
                                <div ng-if="asset.Owners.length > 2" class="additional-consumers">
                                    <span ng-show="!asset.ShowAllConsumers" ng-click="asset.ShowAllConsumers = !asset.ShowAllConsumers">
                                        &#x25ba; and {{asset.Owners.length - 1}} others
                                    </span>
                                    <div ng-show="asset.ShowAllConsumers" ng-click="asset.ShowAllConsumers = !asset.ShowAllConsumers">
                                        <div class="additional-consumer-name" ng-repeat="consumerId in asset.Owners.slice(1)">
                                            <span ng-if="$index == 0">&#x25bc;</span> {{getConsumerName(consumerId.ConsumerId)}}
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="entity-cell" ng-bind="getFriendlyAssetTypeName(asset.AssetType)"></td>
                            <td class="entity-cell" ng-bind="asset.Desc"></td>
                            <td class="entity-cell" ng-bind="asset.Value"></td>
                        </tr>
                    </table>
                </div>

                <table class="FormTable page-width">
                    <tr>
                        <td colspan="4">
                            <input type="button" value="<< Prev" ng-click="moveToPreviousRecord();" ng-disabled="selectedRecordIndex === 0" />
                            <input type="button" value="Next >>" ng-click="moveToNextRecord();" ng-disabled="vm.loan.Assets.length === 0 || selectedRecordIndex === vm.loan.Assets.length - 1" />
                            <input type="button" value="Insert" ng-click="insertRecord();" />
                            <input type="button" value="Add" ng-click="addRecord();" />
                            <input type="button" value="Save" ng-click="saveData();" ng-disabled="!isDirty();" />
                            <input type="button" value="Move Up" ng-click="moveRecordUp();" ng-if="assetFilterModel == 0" ng-disabled="selectedRecordIndex === 0" />
                            <input type="button" value="Move Down" ng-click="moveRecordDown();" ng-if="assetFilterModel == 0" ng-disabled="vm.loan.Assets.length === 0 || selectedRecordIndex === vm.loan.Assets.length - 1" />
                            <input type="button" value="Delete" ng-click="deleteRecord();" ng-disabled="vm.loan.Assets.length === 0" />
                            <input type="button" value="Edit VOD" ng-click="navigateToVod();" ng-disabled="!selectedAsset.RequiresVod" />
                        </td>
                    </tr>
                </table>

                <table class="FormTable InsetBorder page-width left-padded-table" ng-if="selectedAsset != null">
                    <tr>
                        <td><label class="FieldLabel">Owner</label></td>
                        <td>
                            <div ng-repeat="consumerAssociation in selectedAsset.Owners">
                                {{getConsumerName(consumerAssociation.ConsumerId)}}
                            </div>

                            <a ng-click="editAssetOwners(selectedAsset);">edit</a>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="FieldLabel">Type</label></td>
                        <td>
                            <select id="AssetType" runat="server" ng-model="selectedAsset.AssetType" ng-change="onAssetTypeChange();"></select>
                            <input type="text" class="medium" ng-model="selectedAsset.OtherTypeDesc" ng-disabled="selectedAsset.AssetType != '9' && selectedAsset.AssetType != '11'" />
                        </td>
                    </tr>
                    <tr >
                        <td><label class="FieldLabel">Is Credited At Closing</label></td>
                        <td><input ng-disabled="!selectedAsset.IsCreditAtClosingUserDependent" type="checkbox" ng-model="selectedAsset.IsCreditAtClosing" ng-change="refreshCalculation();" /></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <a ng-click="pickFromContacts();" ng-disabled="selectedAsset.AssetType == '0' || selectedAsset.AssetType == '9'">Pick from Contacts</a>
                            <a ng-click="addToContacts();" ng-disabled="selectedAsset.AssetType == '0' || selectedAsset.AssetType == '9'">Add to Contacts</a>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="FieldLabel">Company Name</label></td>
                        <td>
                            <!-- Disable all inputs but description and value when asset type is "Auto" or "Other Illiquid Asset" -->
                            <input type="text" ng-model="selectedAsset.CompanyName" ng-disabled="selectedAsset.AssetType == '0' || selectedAsset.AssetType == '9'" />
                        </td>
                    </tr>
                    <tr>
                        <td><label class="FieldLabel">Department</label></td>
                        <td>
                            <input type="text" ng-model="selectedAsset.DepartmentName" ng-disabled="selectedAsset.AssetType == '0' || selectedAsset.AssetType == '9'" />
                        </td>
                    </tr>
                    <tr>
                        <td><label class="FieldLabel">Address</label></td>
                        <td>
                            <input type="text" ng-model="selectedAsset.StreetAddress" ng-disabled="selectedAsset.AssetType == '0' || selectedAsset.AssetType == '9'" />
                        </td>
                    </tr>
                    <tr>
                        <td><label class="FieldLabel">City</label></td>
                        <td>
                            <input id="City" ng-model="selectedAsset.City" type="text" ng-disabled="selectedAsset.AssetType == '0' || selectedAsset.AssetType == '9'" />
                            <select 
                                id="State" 
                                runat="server" 
                                ng-model="selectedAsset.State" 
                                ng-disabled="selectedAsset.AssetType == '0' || selectedAsset.AssetType == '9'"
                                ng-style="{backgroundColor: (selectedAsset.AssetType == '0' || selectedAsset.AssetType == '9') ? 'lightgrey' : ''}"></select>

                            <input
                                type="text"
                                id="Zip" 
                                ng-model="selectedAsset.Zip" 
                                ng-disabled="selectedAsset.AssetType == '0' || selectedAsset.AssetType == '9'"
                                preset="zipcode" 
                                mask="#####" 
                                maxlength="5" 
                                onfocus="this.oldValue = this.value;" 
                                city="City" 
                                state="State" 
                                county=""
                                onblur="smartZipcodeAuto(event);" />
                        </td>
                    </tr>
                    <tr>
                        <td><label class="FieldLabel">Phone Number</label></td>
                        <td>
                            <input type="text" class="medium" ng-model="selectedAsset.Phone" preset="phone" ng-disabled="selectedAsset.AssetType == '0' || selectedAsset.AssetType == '9'" />
                        </td>
                    </tr>
                </table>

                <table class="FormTable InsetBorder page-width left-padded-table with-spacer" ng-if="selectedAsset != null">
                    <tr>
                        <td><label class="FieldLabel">Description</label></td>
                        <td>
                            <input type="text" ng-model="selectedAsset.Desc" />
                        </td>
                        <td><label class="FieldLabel">Value</label></td>
                        <td>
                            <input type="text" id="SelectedAssetValue" ng-model="selectedAsset.Value" preset="money" ng-blur="refreshCalculation();" />
                        </td>
                        <td>
                            <label class="FieldLabel">Source</label>
                        </td>
                        <td>
                            <select 
                                id="GiftSource" 
                                ng-model="selectedAsset.GiftSource" 
                                ng-disabled="isGiftSourceDisabled()"
                                ng-change="onGiftSourceChange()"
                                ng-style="{backgroundColor: isGiftSourceDisabled() ? 'lightgrey' : ''}"
                                runat="server">
                                <option value="6">Community Nonprofit</option>
                                <option value="3">Employer</option>
                                <option value="7">Federal Agency</option>
                                <option value="2" ng-if="selectedAsset.AssetType == '2'">Government</option>
                                <option value="8">Local Agency</option>
                                <option value="4" ng-if="selectedAsset.AssetType == '4'">Nonprofit</option>
                                <option value="5">Other</option>
                                <option value="1">Relative</option>
                                <option value="9">Religious Nonprofit</option>
                                <option value="10">State Agency</option>
                                <option value="11">Unmarried Partner</option>
                                <option value="12">Seller</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="FieldLabel">Account Holder Name</label></td>
                        <td>
                            <input type="text" ng-model="selectedAsset.AccountName" ng-disabled="selectedAsset.AssetType == '0' || selectedAsset.AssetType == '9'" />
                        </td>
                        <td><label class="FieldLabel">Account Number</label></td>
                        <td>
                            <input type="text" ng-model="selectedAsset.AccountNum" ng-disabled="selectedAsset.AssetType == '0' || selectedAsset.AssetType == '9'" />
                        </td>
                    </tr>
                </table>

                <table class="FormTable InsetBorder page-width left-padded-table with-spacer" ng-if="selectedAsset != null">
                    <tr>
                        <td colspan="2">
                            <label class="FieldLabel">Verif. Sent</label>
                            <input type="text" id="VerifSentDate" ng-model="selectedAsset.VerifSentDate" preset="date" ng-disabled="selectedAsset.AssetType == '0' || selectedAsset.AssetType == '9'" />
                            <a onclick="return displayCalendar('VerifSentDate');">
                                <img class="date-picker" src="../../images/pdate.gif" title="Open calendar" alt="Open calendar"/>
                            </a>
                        </td>
                        <td>
                            <label class="FieldLabel">Re-order</label>
                            <input type="text" id="VerifReorderedDate" ng-model="selectedAsset.VerifReorderedDate" preset="date" ng-disabled="selectedAsset.AssetType == '0' || selectedAsset.AssetType == '9'" />
                            <a onclick="return displayCalendar('VerifReorderedDate');">
                                <img class="date-picker" src="../../images/pdate.gif" title="Open calendar" alt="Open calendar"/>
                            </a>
                        </td>
                        <td>
                            <label class="FieldLabel">Received</label>
                            <input type="text" id="VerifRecvDate" ng-model="selectedAsset.VerifRecvDate" preset="date" ng-disabled="selectedAsset.AssetType == '0' || selectedAsset.AssetType == '9'" />
                            <a onclick="return displayCalendar('VerifRecvDate');">
                                <img class="date-picker" src="../../images/pdate.gif" title="Open calendar" alt="Open calendar"/>
                            </a>
                        </td>
                        <td>
                            <label class="FieldLabel">Expected</label>
                            <input type="text" id="VerifExpiresDate" ng-model="selectedAsset.VerifExpiresDate" preset="date" ng-disabled="selectedAsset.AssetType == '0' || selectedAsset.AssetType == '9'" />
                            <a onclick="return displayCalendar('VerifExpiresDate');">
                                <img class="date-picker" src="../../images/pdate.gif" title="Open calendar" alt="Open calendar"/>
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
