﻿#region Generated code
namespace LendersOfficeApp.newlos.Ulad
#endregion
{
    using System;

    /// <summary>
    /// Provides a means for managing owners on ULAD
    /// assets, liabilities, and REOs.
    /// </summary>
    public partial class EditOwner : BaseLoanPage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected override void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;
            this.DisplayCopyRight = false;

            this.RegisterJsScript("Ulad.Owners.js");
            this.RegisterJsScript("angular-1.5.5.min.js");

            base.OnInit(e);
        }
    }
}