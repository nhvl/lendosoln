﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HomeownerCounseling.aspx.cs" Inherits="LendersOfficeApp.newlos.Ulad.HomeownerCounseling" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Homeowner Counseling</title>

    <style type="text/css">
        #HousingCounselingHudAgencyID {
            width: 70px;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        function saveMe() {
            angular.element('#HomeownerCounselingControllerDiv').scope().saveData();
            return true;
        }

        function saveAndRedirect(postbackHandler) {
            // Used for the save confirmation dialog when navigating away from the page.
            // This will allow the async save to complete before redirecting the user.
            angular.element('#HomeownerCounselingControllerDiv').scope().saveData(postbackHandler);
        }

        function doAfterDateFormat(input) {
            angular.element('#HomeownerCounselingControllerDiv').scope().onDateFormat(input);
        }
    </script>
    <form id="form1" runat="server">
        <div ng-app="HomeownerCounseling">
            <div id="HomeownerCounselingControllerDiv" ng-controller="HomeownerCounselingController">
                <h4 class="page-header">Homeowner Counseling</h4>

                <div class="listing-container">
                    <table class="FormTable page-width asset-listing">
                        <tr>
                            <th class="GridHeader"><a ng-click="orderBy('Attendees')">Attendee(s)</a></th>
                            <th class="GridHeader"><a ng-click="orderBy('CompletedDate')">Completed Date</a></th>
                            <th class="GridHeader"><a ng-click="orderBy('CounselingType')">Counseling Type</a></th>
                            <th class="GridHeader"><a ng-click="orderBy('CounselingFormat')">Counseling Format</a></th>
                            <th class="GridHeader"><a ng-click="orderBy('HousingCounselingAgency')">Counseling Agency</a></th>
                        </tr>
                        <tr 
                            ng-if="vm.loan && vm.loan.CounselingEvents" 
                            ng-repeat="event in vm.loan.CounselingEvents" 
                            ng-class="['asset-row', {selected: event.Id === selectedEvent.Id, GridAutoItem: event.Id !== selectedEvent.Id}]" 
                            ng-click="selectEvent(event);">

                            <td class="entity-cell">
                                <div ng-if="event.Owners.length > 0" ng-bind="getConsumerName(event.Owners[0].ConsumerId)"></div>
                                <div ng-if="event.Owners.length == 2" ng-bind="getConsumerName(event.Owners[1].ConsumerId)"></div>
                                <div ng-if="event.Owners.length > 2" class="additional-consumers">
                                    <span ng-show="!event.ShowAllConsumers" ng-click="event.ShowAllConsumers = !event.ShowAllConsumers">
                                        &#x25ba; and <span ng-bind="event.Owners.length - 1"></span> others
                                    </span>
                                    <div ng-show="event.ShowAllConsumers" ng-click="event.ShowAllConsumers = !event.ShowAllConsumers">
                                        <div class="additional-consumer-name" ng-repeat="owner in event.Owners.slice(1)">
                                            <span ng-if="$index == 0">&#x25bc;</span> <span ng-bind="getConsumerName(owner.ConsumerId)"></span>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="entity-cell" ng-bind="event.CompletedDate"></td>
                            <td class="entity-cell" ng-bind="getFriendlyEventCounselingTypeName(event.CounselingType)"></td>
                            <td class="entity-cell" ng-bind="getFriendlyEventCounselingFormatTypeName(event.CounselingFormat)"></td>
                            <td class="entity-cell" ng-bind="event.HousingCounselingAgency"></td>
                        </tr>
                    </table>
                </div>

                <table class="FormTable page-width">
                    <tr>
                        <td colspan="4">
                            <input type="button" value="<< Prev" ng-click="moveToPreviousRecord();" ng-disabled="selectedRecordIndex === 0" />
                            <input type="button" value="Next >>" ng-click="moveToNextRecord();" ng-disabled="vm.loan.CounselingEvents.length === 0 || selectedRecordIndex === vm.loan.CounselingEvents.length - 1" />
                            <input type="button" value="Insert" ng-click="insertRecord();" />
                            <input type="button" value="Add" ng-click="addRecord();" />
                            <input type="button" value="Save" ng-click="saveData();" ng-disabled="!isDirty();" />
                            <input type="button" value="Move Up" ng-click="moveRecordUp();" ng-disabled="selectedRecordIndex === 0" />
                            <input type="button" value="Move Down" ng-click="moveRecordDown();" ng-disabled="vm.loan.CounselingEvents.length === 0 || selectedRecordIndex === vm.loan.CounselingEvents.length - 1" />
                            <input type="button" value="Delete" ng-click="deleteRecord();" ng-disabled="vm.loan.CounselingEvents.length === 0" />
                        </td>
                    </tr>
                </table>

                <table class="FormTable InsetBorder page-width left-padded-table" ng-if="selectedEvent != null">
                    <tr>
                        <td>
                            <label class="FieldLabel">Attendee(s)</label>
                        </td>
                        <td>
                            <div ng-repeat="owner in selectedEvent.Owners" ng-bind="getConsumerName(owner.ConsumerId)"></div>
                            <a ng-click="editEventAttendees(selectedEvent);">edit</a>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="FieldLabel">Completed Date</label>
                        </td>
                        <td>
                            <input type="text" id="CompletedDate" ng-model="selectedEvent.CompletedDate" preset="date" />
                            <a onclick="return displayCalendar('CompletedDate');">
                                <img class="date-picker" src="../../images/pdate.gif" title="Open calendar" alt="Open calendar"/>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="FieldLabel">Counseling Type</label>
                        </td>
                        <td>
                            <select id="CounselingType" runat="server" ng-model="selectedEvent.CounselingType" ng-change="updateDirtyBit();"></select>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="FieldLabel">Counseling Format</label>
                        </td>
                        <td>
                            <select id="CounselingFormat" runat="server" ng-model="selectedEvent.CounselingFormat" ng-change="updateDirtyBit();"></select>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="FieldLabel">Counseling Agency</label>
                        </td>
                        <td>
                            <input type="text" id="HousingCounselingAgency" maxlength="100" ng-model="selectedEvent.HousingCounselingAgency" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="FieldLabel">Counseling Agency ID</label>
                        </td>
                        <td>
                            <input type="text" id="HousingCounselingHudAgencyID" maxlength="100" ng-model="selectedEvent.HousingCounselingHudAgencyID" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
