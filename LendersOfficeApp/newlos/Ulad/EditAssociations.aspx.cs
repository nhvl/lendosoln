﻿#region Generated Code
namespace LendersOfficeApp.newlos.Ulad
#endregion
{
    using System;

    /// <summary>
    /// Provides a means for users to edit associations between entities.
    /// </summary>
    public partial class EditAssociations : BaseLoanPage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="args">
        /// The parameter is not used.
        /// </param>
        protected override void OnInit(EventArgs args)
        {
            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;
            this.DisplayCopyRight = false;

            this.RegisterJsScript("Ulad.EditAssociations.js");
            this.RegisterJsScript("angular-1.5.5.min.js");

            base.OnInit(args);
        }
    }
}