<!DOCTYPE>
<%@ Page language="c#" Codebehind="LoanApp.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.LoanApp" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="LendersOffice.Common"%>
<html>
  <head>
  <meta runat="server" id="CompatTag" http-equiv="X-UA-Compatible"  />
    <title>LendingQB</title>
    <script type="text/javascript">
<!--

       

    function f_insertWindowToOpenList(sLNm) {
      window.name = sLNm;
      var winOpener = window.opener;
      if (null != winOpener && !winOpener.closed) {
        if (typeof(winOpener.f_insertWindowToOpenList) != "undefined")
          winOpener.f_insertWindowToOpenList(<%= AspxTools.JsString(RequestHelper.LoanID) %>, sLNm, this);
      }
    }
    function f_update_sLNm(sLNm) {
      var winOpener = window.opener;
      if (null != winOpener && !winOpener.closed) {
        if (typeof(winOpener.f_insertWindowToOpenList) != "undefined")
          winOpener.f_update_sLNm_WindowToOpenList(<%= AspxTools.JsString(RequestHelper.LoanID) %>, sLNm);
      }
    }
    function f_isLoanWindowOpen(sLNm) {
      var winOpener = window.opener;
      if (null != winOpener && !winOpener.closed) {
        if (typeof(winOpener.f_isLoanWindowOpen) != "undefined")
          return winOpener.f_isLoanWindowOpen(sLNm);
      }
    }
    
    function _init() {
        // OPM 68620: required for nonmodal task dialogs to refresh the window
        self.name = 'LoanWindow_' + <%=AspxTools.JsString(RequestHelper.LoanID)%>.replace(/-/g, '_'); // TODO: prepend m_serverID
        

        if(<%=AspxTools.JsBool(IsResize) %>) {
            resizeToScreen();    
        }
    }

    function navigateToHighUrl() {
        var hasHighUrl = <%= AspxTools.JsBool(false == string.IsNullOrEmpty(RequestHelper.GetSafeQueryString("highURL"))) %>;
        if (hasHighUrl) {
            changeBodyURLWhenDocumentReady();
            return true;
        }

        return false;
    }

    <% if(false == string.IsNullOrEmpty(RequestHelper.GetSafeQueryString("highURL"))){ %>
    function changeBodyURLWhenDocumentReady()
    {
        if(document.readyState != "complete")
        {
            window.setTimeout(changeBodyURLWhenDocumentReady, 200);
        }
        else{
            changeBodyURLTo(<%=AspxTools.JsString(highUrl) %>);
            
        }
    }
    <%} %>    
    function changeBodyURLTo(url)
    {
        if (!redirectToUladIfUladPage(url, true)) {
            document.getElementsByName("body")[0].src = url;
        }
    }

    function resizeSummary(newSize)
    {
        newSize = newSize || '40';
        var style = document.getElementById('ResizeSummaryStyle') || document.createElement("style");
        style.id = "ResizeSummaryStyle";
        style.innerHTML = '#info { flex: 0 0 '+newSize+'px; -ms-flex: 0 0 ' + newSize + 'px; -webkit-flex: 0 0 ' + newSize + 'px; }';
        var head = document.getElementsByTagName("head")[0];
        head.appendChild(style);
    }
    
    <% if (RequestHelper.GetBool("isnew")) { %>
      var sLId = <%= AspxTools.JsString(RequestHelper.LoanID) %>;
      var sLNm = <%= AspxTools.JsString(m_sLNm) %>;
      if (!f_isLoanWindowOpen(sLId)) {
        f_insertWindowToOpenList(sLNm);
      }
    <% } %>
    
//-->
    </script>

    <style>
        iframe {width: 100%; border: none; min-height: 0; min-width: 0;}

        iframe, div {
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }
        body {margin: 0; padding: 0;}
        iframe[name='body'] {
            border: 0 solid gainsboro;
            border-left-width: 4px;
        }

        iframe[name="frmCode"] {
            height: 0;
        }

        iframe[name="header"] { -ms-flex: 0 0 23px; -webkit-flex: 0 0 23px; flex: 0 0 23px; }
        iframe[name="info"] { -ms-flex: 0 0 40px; -webkit-flex: 0 0 40px; flex: 0 0 40px; }

        .main-container {
            height: 100%;
            width: 100%;
            display: flex;
            -ms-flex-direction: column;
            -webkit-flex-direction: column;
            flex-direction: column;
        }

        .no-pointers {
            pointer-events: none;
        }

        #CalculationOverlay {
            position: absolute;
            left: 0;
            top: 0; 
            height: 100%;
            width: 100%;
            background-color: black;
            opacity: 0;
            z-index: 9999998;
        }

        #CalculationOverlay.darkened {
            opacity: .3;
        }

        form {
            margin-bottom: 0;
        }

        div#toast-container > div {
            z-index: 9999999;
            background-color:#be0000;
        }

    </style>
  </head>
<body>
    <form runat="server" id="form1">
    <div class="main-container">
    <iframe id="frmCode" name="frmCode" src=<%= AspxTools.SafeUrl("LoanAppCode.aspx?loanid=" + RequestHelper.LoanID) %> scrolling="no" tabindex="-1" noresize ></iframe>
    <iframe id="header" name="header" src=<%= AspxTools.SafeUrl(HeaderSrc) %> scrolling="no" tabindex="-1" noresize ></iframe>
    <iframe id="info" name="info" src=<%= AspxTools.SafeUrl("LeftSummaryFrame.aspx?loanid=" + RequestHelper.LoanID + "&body_url=" + RequestHelper.GetSafeQueryString("body_url")) %> scrolling="no" tabindex="-1" noresize ></iframe>
    <div class="body-container">
        <iframe id="treeview" name="treeview" src=<%= AspxTools.SafeUrl(TreeViewSrc) %> frameborder="yes" tabindex="-1" ></iframe>
        <div onmousedown="triggerTreeviewResize();" class="DragBar"></div>
        <iframe id="body" name="body" src="Loading.aspx" ></iframe>
    </div>
        <div id='CalculationOverlay' class="Hidden"></div>
    </div>
    </form>
</body>
</html>
