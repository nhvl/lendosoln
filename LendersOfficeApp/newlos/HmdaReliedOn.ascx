﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HmdaReliedOn.ascx.cs" Inherits="LendersOfficeApp.newlos.HmdaReliedOn" %>
<style type="text/css">
    .width230 {
        width: 230px;
    }
    .width100 {
        width: 100px;
    }
    .width150 {
        width: 150px !important; 
    }
    .width165 {
        width: 165px;
    }
    .width200 {
        width: 200px;
    }
    .width252 {
        width: 252px;
    }
    .width50 {
        width: 50px;
    }
    .alignRight {
        text-align: right;
    }
    .noWrap {
        white-space: nowrap;
    }
    .tableFormat {
        border-spacing: 0px;
        border-collapse: separate;
        padding: 0px;
    }
</style>
<TABLE class="InsetBorder layout-table Main tableFormat">
    <TR>
        <TD class="FieldLabel noWrap">
            <span id="reliedOnHeader">Relied on Information</span>
        </TD>
    </TR>
    <TR>
        <TD class="noWrap">
            <TABLE class="tableFormat">
                <thead id="reliedOnWithoutCreditInfo">
                <TR>
                    <TD class="FieldLabel width230">Total Annual Income Relied On</TD>
                    <TD class="FieldLabel width100">
                        <asp:CheckBox ID="sHmdaIsIncomeReliedOn" runat="server" onclick="refreshCalculation();" Text="Relied On"/>
                    </TD>                            
                    <TD class="width165">
                        <ml:MoneyTextBox id="sReliedOnTotalIncome" runat="server" class="width150">
                        </ml:MoneyTextBox>
                    </TD>                            
                    <TD class="FieldLabel">
                        <asp:CheckBox ID="sReliedOnTotalIncomeLckd" runat="server" onclick="refreshCalculation(); " Text="Locked"/>
                    </TD>
                </TR>
                <TR>
                    <TD class=FieldLabel>Debt-to-Income Ratio Relied On</TD>                            
                    <TD class="FieldLabel">
                        <asp:CheckBox ID="sHmdaIsDebtRatioReliedOn" runat="server" onclick="refreshCalculation();" Text="Relied On"/>
                    </TD>                            
                    <TD>
                        <asp:TextBox id="sReliedOnDebtRatio" runat="server" class="width150 alignRight">
                        </asp:TextBox>
                    </TD>                            
                    <TD class="FieldLabel">
                        <asp:CheckBox ID="sReliedOnDebtRatioLckd" runat="server" onclick="refreshCalculation(); " Text="Locked"/>
                    </TD>
                </TR>
                <TR>
                    <TD class=FieldLabel>Combined Loan-to-Value Ratio Relied On</TD>                            
                    <TD class="FieldLabel">
                        <asp:CheckBox ID="sHmdaIsCombinedRatioReliedOn" runat="server" onclick="refreshCalculation();" Text="Relied On"/>
                    </TD>                            
                    <TD>
                        <asp:TextBox id="sReliedOnCombinedLTVRatio" runat="server" class="width150 alignRight">
                        </asp:TextBox>
                    </TD>                            
                    <TD class="FieldLabel">
                        <asp:CheckBox ID="sReliedOnCombinedLTVRatioLckd" runat="server" onclick="refreshCalculation(); " Text="Locked"/>
                    </TD>
                </TR>
                <TR>
                    <TD class=FieldLabel>Property Value Relied On</TD>
                    <TD class="FieldLabel">
                        <asp:CheckBox ID="sHmdaIsPropertyValueReliedOn" runat="server" onclick="refreshCalculation();" Text="Relied On"/>
                    </TD>                            
                    <TD>
                        <ml:MoneyTextBox id="sReliedOnPropertyValue" runat="server" class="width150">
                        </ml:MoneyTextBox>
                    </TD>                            
                    <TD class="FieldLabel">
                        <asp:CheckBox ID="sReliedOnPropertyValueLckd" runat="server" onclick="refreshCalculation(); " Text="Locked"/>
                    </TD>
                </TR>
                </thead>
                <TR>
                    <TD class="FieldLabel width230">Credit Score Mode</TD>
                    <TD class="FieldLabel width100">
                        <asp:CheckBox ID="sHmdaIsCreditScoreReliedOn" runat="server" onclick="refreshCalculation();" Text="Relied On"/>
                    </TD>                            
                    <TD>
                        <asp:DropDownList runat="server" ID="sReliedOnCreditScoreModeT" onchange="updateModeDdlUI(); refreshCalculation();">
                        </asp:DropDownList>
                    </TD>
                </TR>
            </TABLE>
            <div id="singleScore">
                <table class="tableFormat">
                    <TR>
                        <TD class="FieldLabel width230">Credit Score Relied On Source</TD>
                        <TD class="width100"></TD>
                        <TD>
                            <asp:DropDownList runat="server" ID="sReliedOnCreditScoreSourceT" onchange="updateSingleSourceDdlUI(); refreshCalculation(); updateSingleSourceDdlUI();">
                            </asp:DropDownList>
                        </TD>
                    </TR>
                    <TR>
                        <TD class="FieldLabel">HMDA Report Credit Score As</TD>
                        <TD></TD>
                        <TD>
                            <asp:RadioButtonList id="sReliedOnCreditScoreSingleHmdaReportAsT" runat="server" RepeatDirection="Horizontal" class="FieldLabel">
                                <asp:ListItem Value="1" Text="Borrower">
                                </asp:ListItem>
                                <asp:ListItem Value="2" Text="Coborrower">
                                </asp:ListItem>
                            </asp:RadioButtonList>
                        </TD>
                    </TR>
                    <TR>
                        <TD class="FieldLabel">Credit Score Relied On</TD>
                        <TD></TD>
                        <TD>
                            <asp:TextBox ID="sReliedOnCreditScore" runat="server">
                            </asp:TextBox>
                        </TD>
                    </TR>
                    <tbody id="sReliedOnCreditScoreModelLegacySection" runat="server">
                        <tr>
                            <td class="FieldLabel">Credit Score Relied On Model</td>
                            <td></td>
                            <td colspan="2" class="FieldLabel">
                                <ml:ComboBox ID="sReliedOnCreditScoreModelName" MaxLength="100" runat="server" class="width200"/>
                                <asp:CheckBox ID="sReliedOnCreditScoreModelNameLckd" runat="server" onclick="refreshCalculation();" Text="Locked"/>
                            </td>
                        </tr>
                    </tbody>
                    <tbody id="sReliedOnCreditScoreModelLoanVersion26Section" runat="server">
                        <tr>
                            <td class="FieldLabel">Credit Score Relied On Model</td>
                            <td></td>
                            <td colspan="2" class="FieldLabel">
                                <asp:DropDownList ID="sReliedOnCreditScoreModelT" runat="server"></asp:DropDownList>
                                <asp:CheckBox ID="sReliedOnCreditScoreModelTLckd" runat="server" onclick="refreshCalculation();" Text="Locked"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">
                                <asp:TextBox ID="sReliedOnCreditScoreModelTOtherDescription" class="width252" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>         
            <div id="applicantIndividualScore">
                <table>
                <asp:Repeater runat="server" ID="ApplicantCreditScores" OnItemDataBound="ApplicantCreditScores_ItemDataBound">
                    <ItemTemplate>
                        <tr runat="server" id="AppCreditScoreBorrowerRow">
                            <td class="FieldLabel width200">
                                <ml:EncodedLiteral runat="server" id="AppCreditScoreBorrowerLabel"></ml:EncodedLiteral>
                            </td>
                            <td>
                                <asp:CheckBox ID="aBDecisionCreditSourceTLckd" runat="server" class="applicantCreditInfo"/>
                            </td>
                            <td>
                                <asp:DropDownList ID="aBDecisionCreditSourceT" runat="server" class="applicantCreditInfo"/>
                            </td>
                            <td>
                                <asp:TextBox ID="aBDecisionCreditScore" runat="server" class="width50 applicantCreditInfoText"/>
                            </td>
                            <td>
                                <asp:TextBox ID="aBDecisionCreditModelName" runat="server" class="width200 applicantCreditInfoText"></asp:TextBox>
                                <asp:CheckBox ID="aBIsDecisionCreditModelAValidHmdaModel" class="hidden" runat="server" />
                            </td>
                        </tr>
                        <tr runat="server" id="AppCreditScoreCoBorrowerRow">
                            <td class="FieldLabel width200">
                                <ml:EncodedLiteral runat="server" id="AppCreditScoreCoBorrowerLabel"></ml:EncodedLiteral>
                            </td>
                            <td>
                                <asp:CheckBox ID="aCDecisionCreditSourceTLckd" runat="server" class="applicantCreditInfo"/>
                            </td>
                            <td>
                                <asp:DropDownList ID="aCDecisionCreditSourceT" runat="server" class="applicantCreditInfo"/>
                            </td>
                            <td>
                                <asp:TextBox ID="aCDecisionCreditScore" runat="server" class="width50 applicantCreditInfoText"/>
                            </td>
                            <td>
                                <asp:TextBox ID="aCDecisionCreditModelName" runat="server" class="width200 applicantCreditInfoText"></asp:TextBox>
                                <asp:CheckBox ID="aCIsDecisionCreditModelAValidHmdaModel" class="hidden" runat="server" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                </table>
            </div>               
        </TD>
    </TR>
</TABLE>
<input type="hidden" runat="server" id="HmdaReportAs" />