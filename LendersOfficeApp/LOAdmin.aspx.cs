using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp
{

    public partial class LOAdminLoginPage : MinimalPage
    {
        protected LoginUserControl m_LoginUserControl1;
        protected System.Web.UI.WebControls.Button m_exportButton;
        protected System.Web.UI.WebControls.TextBox m_Employees;

        /// <summary>
        /// Enable/Disable the top row of "become other user" controls individually, because wrapping them in a panel
        /// and disabling it doesn't completely disable the child controls in this case
        /// </summary>
        /// 
        private void EnableUserLogin(bool enableIt)
        {
            m_BecomeUser.Enabled = enableIt;
            m_TypeDdl.Enabled = enableIt;
            m_BecomeUserButton.Enabled = enableIt;
            m_LoginType.Enabled = enableIt;
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {            
            m_LoginUserControl1.IsInternalLogin = true;
            m_ErrMsg.Text = "";

            if (RequestHelper.GetBool("requireinternalpwchange"))
            {
                m_ErrMsg.ForeColor = System.Drawing.Color.Red;
                m_ErrMsg.Text = "Internal user password must be updated. Please update the password.";
            }

            m_BrokersPanel.Visible = false;
            EnableUserLogin(true);
        }

        protected void LoginAsOtherUserByEmployeeID(Guid initialUserId)
        {
            Guid employeeId = Guid.Empty;
            try
            {
                employeeId = new Guid(m_BecomeUser.Text.TrimWhitespaceAndBOM());
            }
            catch
            {
                m_ErrMsg.ForeColor = System.Drawing.Color.Red;
                m_ErrMsg.Text = "Invalid employee id.";
                return;
            }
            string type = m_TypeDdl.SelectedItem.Value;

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters = {
											new SqlParameter("@EmployeeId", employeeId),
											new SqlParameter("@Type", type[0])
										};

                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetActiveUserByEmployeeId", parameters))
                {
                    if (reader.Read())
                    {
                        if (type.StartsWith("B"))
                        {
                            Guid userId = (Guid)reader["UserId"];
                            Guid brokerId = (Guid)reader["BrokerId"];
                            if (!RequestHelper.BecomeUser(brokerId, userId, true, IsUsingNewUI, initialUserId, isRetailBUser: type == "BTpo"))
                            {
                                m_ErrMsg.ForeColor = System.Drawing.Color.Red;
                                m_ErrMsg.Text = "Unable to become user.";
                                break;
                            }
                        }
                        else if (type.Equals("P"))
                        {
                            string[] val = ((String)reader["ValueField"]).Split(',');
                            RequestHelper.BecomePmlUser(new Guid(val[0]), new Guid(val[1]), initialUserId);
                        }
                    }

                }
                m_ErrMsg.ForeColor = System.Drawing.Color.Red;
                m_ErrMsg.Text = "Invalid or inactive employee id.";
            }
        }

        protected void LoginAsOtherUserByUserID(Guid initialUserId)
        {
            Guid userId = Guid.Empty;
            userId = new Guid(m_BecomeUser.Text.TrimWhitespaceAndBOM());
            string type = m_TypeDdl.SelectedItem.Value;

            if (type.StartsWith("B"))
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@UserId", userId)
                                            };

                Guid brokerId = Guid.Empty;
                using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "ALL_USER_SHARED_DB_RetrieveByUserId", parameters))
                {
                    if (reader.Read())
                    {
                        brokerId = (Guid)reader["BrokerId"];
                    }
                }
                if (brokerId == Guid.Empty)
                {
                    m_ErrMsg.ForeColor = System.Drawing.Color.Red;
                    m_ErrMsg.Text = "Unable to become user (you may have entered a 'P' user's ID)";

                }
                else if (!RequestHelper.BecomeUser(brokerId, userId, true, IsUsingNewUI, initialUserId, isRetailBUser: type == "BTpo"))
                {
                    m_ErrMsg.ForeColor = System.Drawing.Color.Red;
                    m_ErrMsg.Text = "Unable to become user (you may have entered a 'P' user's ID)";
                }
            }
            else if (type.Equals("P"))
            {
                Guid brokerPmlSiteId = Guid.Empty;

                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@UserId", userId)
                                                };

                    using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "RetrieveBrokerPmlSiteIdByUserID", parameters))
                    {
                        if (reader.Read())
                        {
                            brokerPmlSiteId = (Guid)reader["BrokerPmlSiteId"];
                            break;
                        }
                    }
                }

                if (brokerPmlSiteId == Guid.Empty)
                {
                    m_ErrMsg.ForeColor = System.Drawing.Color.Red;
                    m_ErrMsg.Text = "Invalid or inactive user id.";
                }
                else
                {
                    RequestHelper.BecomePmlUser(userId, brokerPmlSiteId, initialUserId);
                }
            }
        }

        protected void LoginAsOtherUserByLoginName(Guid initialUserId)
        {
            string loginName = m_BecomeUser.Text.TrimWhitespaceAndBOM();
            string type = m_TypeDdl.SelectedItem.Value;

            if (type.StartsWith("B"))
            {
                Guid userId = Guid.Empty;
                Guid brokerId = Guid.Empty;

                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    SqlParameter[] parameters = {
											new SqlParameter("@LoginName", loginName),
											new SqlParameter("@Type", type[0])
										};
                    using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetUserByLoginName", parameters))
                    {
                        if (reader.Read())
                        {
                            userId = (Guid)reader["UserId"];
                            brokerId = (Guid)reader["BrokerId"];
                            break;
                        }

                    }

                }
                if (userId != Guid.Empty)
                {
                    if (!RequestHelper.BecomeUser(brokerId, userId, true, IsUsingNewUI, initialUserId, isRetailBUser: type == "BTpo"))
                    {
                        m_ErrMsg.ForeColor = System.Drawing.Color.Red;
                        m_ErrMsg.Text = "Unable to become user.";
                    }
                }
                else
                {
                    m_ErrMsg.ForeColor = System.Drawing.Color.Red;
                    m_ErrMsg.Text = "Invalid or inactive user login.";
                }

            }
            else if (type.Equals("P"))
            {
                List<KeyValuePair<string, string>> brokerList = new List<KeyValuePair<string, string>>();

                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    SqlParameter[] parameters = {
											new SqlParameter("@LoginName", loginName),
											new SqlParameter("@Type", type)
										};
                    using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetUserByLoginName", parameters))
                    {
                        while (reader.Read())
                        {
                            brokerList.Add(new KeyValuePair<string, string>((string)reader["ValueField"], (string)reader["BrokerNm"]));
                        }
                    }
                }

                m_Brokers.DataSource = brokerList;
                m_Brokers.DataTextField = "Value";
                m_Brokers.DataValueField = "Key";
                m_Brokers.DataBind();

                if (m_Brokers.Items.Count == 0)
                {
                    m_ErrMsg.ForeColor = System.Drawing.Color.Red;
                    m_ErrMsg.Text = "Invalid or inactive user login.";
                }
                else if (m_Brokers.Items.Count == 1)
                {
                    string[] val = m_Brokers.Items[0].Value.Split(',');
                    RequestHelper.BecomePmlUser(new Guid(val[0]), new Guid(val[1]), initialUserId);
                }
                else
                {
                    m_ErrMsg.ForeColor = System.Drawing.Color.Green;
                    m_ErrMsg.Text = "Multiple Brokers contain this login name:";
                    m_Brokers.Items.Insert(0, "<-- Select Broker -->");
                    m_BrokersPanel.Visible = true;
                    EnableUserLogin(false);
                }
            }
        }

        /// <summary>
        /// Handle UI event.
        /// </summary>
        /// 
        protected void SelectClick(object sender, System.EventArgs a)
        {
            if (m_Brokers.SelectedIndex == 0)
            {
                return;
            }
            string[] val = m_Brokers.Items[m_Brokers.SelectedIndex].Value.Split(',');
            RequestHelper.BecomePmlUser(new Guid(val[0]), new Guid(val[1]), PrincipalFactory.CurrentPrincipal.UserId);
        }

        public bool IsUsingNewUI { get; set; }
        /// <summary>
        /// Handle UI event.
        /// </summary>
        /// 
        protected void BecomeClick(object sender, System.EventArgs a)
        {
            AbstractUserPrincipal principal = m_LoginUserControl1.GetInternalPrincipal();
            if (principal == null)
            {
                m_ErrMsg.Text = "";
                return;
            }
            else
            {
                m_LoginUserControl1.ClearErrorMessages();
            }

            if (((System.Web.UI.WebControls.Button)sender).ID.Equals("m_BecomeUserButton2016UI"))
            {
                IsUsingNewUI = true;
            }

            RequestHelper.StoreToCookie("DisabledIECheck", "True", true);
            InternalUserPermissions m_Permissions = new InternalUserPermissions();
            m_Permissions.Opts = principal.Permissions;
            if (!m_Permissions.Can(E_InternalUserPermissions.BecomeUser))
            {
                m_ErrMsg.ForeColor = System.Drawing.Color.Red;
                m_ErrMsg.Text = "You do not have permission to become users.";
                return;
            }

            string loginType = m_LoginType.SelectedItem.Value;

            if (loginType.Equals("LoginName"))
            {
                LoginAsOtherUserByLoginName(principal.UserId);
            }
            else if (loginType.Equals("UserID"))
            {
                LoginAsOtherUserByUserID(principal.UserId);
            }
            else if (loginType.Equals("EmployeeID"))
            {
                LoginAsOtherUserByEmployeeID(principal.UserId);
            }
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion
    }
}
