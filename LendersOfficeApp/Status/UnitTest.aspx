﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UnitTest.aspx.cs" Inherits="LendersOfficeApp.Status.UnitTest" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Unit Test</title>
</head>
<body>
<style>
    .popover { max-width: 600px; }
</style>
<div class="container">
    <!--[if lt IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please upgrade your browser to improve your experience.</p>
    <![endif]-->
    <div class="page-header">
        <h3>Unit Test</h3>
    </div>
    <div ng-app="app">
        <test-center></test-center>
    </div>
</div>
<form runat="server"></form>
</body>
</html>
