﻿using System;
using System.Web.Services;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendersOfficeApp.LOAdmin.Test;

namespace LendersOfficeApp.Status
{
    public partial class UnitTest : BasePage
    {
        [WebMethod]
        public static LUnit.WebShim.TestClassData[] GetTestManager()
        {
            ThrowIfHasNoPermission();

            return TestCenter.GetTestManager();
        }

        [WebMethod]
        public static LUnit.WebShim.TestClassResult[] RunTestMethod(int fixtureId, int methodId)
        {
            ThrowIfHasNoPermission();

            return TestCenter.RunTestMethod(fixtureId, methodId);
        }

        [WebMethod]
        public static LUnit.WebShim.TestClassResult[] RunTestFixture(int fixtureId)
        {
            ThrowIfHasNoPermission();

            return TestCenter.RunTestFixture(fixtureId);
        }

        static void ThrowIfHasNoPermission()
        {
            if (!HasPermission()) throw new UnauthorizedAccessException("No Permission.");
        }
        static bool HasPermission()
        {
            var principal = PrincipalFactory.CurrentPrincipal;

            return ConstStage.AcceptedNonLoadminSmoketesters.Contains(principal.LoginNm);
        }

        void RegisterResources()
        {
            RegisterCSS("newdesign.main.css");
            RegisterCSS("newdesign.lendingqb.css");

            this.EnableJqueryMigrate = false;
            RegisterJsScript("bootstrap-3.3.4.min.js");

            RegisterJsScript("LOAdmin.Test.TestCenter.js");
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HasPermission()) { RequestHelper.Signout(); return; }
            RegisterResources();
        }

        protected override void OnInit(EventArgs e)
        {
            this.m_loadDefaultStylesheet = false;
            this.m_loadLOdefaultScripts= false;

            base.OnInit(e);
        }
    }
}
