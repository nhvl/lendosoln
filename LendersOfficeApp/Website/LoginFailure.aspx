<%@ Page language="c#" AutoEventWireup="false" Inherits="LendersOfficeApp.Website.LoginFailure" CodeBehind="LoginFailure.aspx.cs" ValidateRequest="true" enableViewState="False"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import NameSpace="DataAccess" %>
<HTML>
	<HEAD>
		<title>LendingQB</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="styles/style.css" type="text/css" rel="stylesheet">
		<style type="text/css" media="screen">
			#errorMsg
			{ 	font-style: normal;
				color: #FF0000;
			}
		</style>
	</HEAD>
	<body>
		<form id="failure" method="post" runat="server">
			<table width="100%">
				<tr>
					<td><a href=<%= AspxTools.SafeUrl(Tools.VRoot + "/") %>><img height="51" src="images/header_logo_lqb.png" width="185" border="0"></a></td>
				</tr>
				<tr>
					<td align="center"><h3>Login Failure</h3>
					</td>
				</tr>
				<tr>
					<td align="center">
						<%	string m_errMsg1="";
							string m_errMsg2="";
							switch( Request.QueryString["showError"] ){
								case "0": //PrincipalFactory.E_LoginProblem.IsDisabled
									m_errMsg1="Your account is disabled.";
									m_errMsg2="Please contact your account administrator for more information.";
									break;
								case "1": //PrincipalFactory.E_LoginProblem.IsLocked
									m_errMsg1="Your account is locked.";
									m_errMsg2="Please contact your account administrator for more information.";
									break;
								case "2": //PrincipalFactory.E_LoginProblem.NeedsToWait
									m_errMsg1="Please wait before attempting to log in again.";
									break;
								case "3": //PrincipalFactory.E_LoginProblem.InvalidAndWait
									m_errMsg1="Please ensure the login and password are correct.";
									m_errMsg2="Due to the number of failed login attempts, you must wait "+Request.QueryString["time"]+" minute(s) before attempting to log in again.";
									break;									
							}
						%>
						<div id="errorMsg">
							<%=AspxTools.HtmlString(m_errMsg1) %>
							<br />
							<%=AspxTools.HtmlString(m_errMsg2) %>
						</div>
					</td>
				</tr>
				<tr>
					<td align="center"><a href="index.aspx">Click To Try Again</a></td>
				</tr>
				<tr>
					<td><br>
						<table bgcolor="mistyrose" cellspacing="0" cellpadding="2" width="450" align="center" style="border:1px solid;border-color:#ffcccc">
							<tr>
								<td align="center" class="body">
									<b><u>SECURITY NOTICE</u><br>
									</b>The use of this system may be monitored for computer security 
									purposes.&nbsp; Any unauthorized access to this system is prohibited and is 
									subject to criminal and civil penalties under Federal Laws including, but not 
									limited to, the Computer Fraud and Abuse Act and the National Information 
									Infrastructure Protection Act.
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<hr>
                    <a href=<%= AspxTools.SafeUrl(Tools.VRoot + "/Website/legal/privacy.htm") %>>Privacy</a>
                    |
                    <a href=<%= AspxTools.SafeUrl(Tools.VRoot + "/website/legal/term.htm") %>>Terms</a>
                    |
                    <a href="http://www.lendingqb.com/about.php">About Us</a>
                    &nbsp;&nbsp;
                    &copy;<ml:EncodedLiteral runat="server" ID="Year"></ml:EncodedLiteral> <a href="http://www.lendingqb.com">LendingQB</a>, All Rights Reserved.
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
