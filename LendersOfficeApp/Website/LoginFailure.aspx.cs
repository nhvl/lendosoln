﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;

namespace LendersOfficeApp.Website
{
    public partial class LoginFailure : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Year.Text = DateTime.Now.Year.ToString();
        }
    }
}
