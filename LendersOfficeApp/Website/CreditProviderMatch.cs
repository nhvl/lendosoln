using System;
using System.Collections;
using System.Text;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.Register
{
	class CCreditProviderMatch
	{
		public CCreditProviderMatch()
		{
			m_providers = new ArrayList( 100 ); // ArrayList of ArrayList of Names
			
			
			ArrayList nameList = new ArrayList( 20 );
			nameList.Add( "https://apluscreditco.meridianlink.com" );
			nameList.Add( "A+ CREDIT COMPANY" );
			nameList.Add( "A Plus Credit" );
			nameList.Add( "A Plus Credit Company" );
			nameList.Add( "A+ Credit" );
			nameList.Add( "A Plus" );
			nameList.Add( "A" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://able.meridianlink.com" );
			nameList.Add( "ABLE SCREENING SERVICES" );
			nameList.Add( "ABLE SCREENING SERVICE" );
			nameList.Add( "ABLE SCREENING" );
			nameList.Add( "ABLE" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://advancedcredit.meridianlink.com" );
			nameList.Add( "ADVANCED CREDIT SERVICES" );
			nameList.Add( "Advanced Credit Service" );
			nameList.Add( "Advanced Credit" );
			nameList.Add( "Advanced" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://acs.meridianlink.com" );
			nameList.Add( "ADVANCED CREDIT SOLUTIONS" );
			nameList.Add( "Advanced Credit Solution" );
			nameList.Add( "Advanced Credit" );
			nameList.Add( "Advanced" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.advcredit.com" );
			nameList.Add( "ADVANTAGE CREDIT" );
			nameList.Add( "ADVCREDIT" );
			nameList.Add( "Advantage" );
			m_providers.Add( nameList );
			
			nameList = new ArrayList( 20 );
			nameList.Add( "https://advantageplus.meridianlink.com" );
			nameList.Add( "ADVANTAGE PLUS" );
			nameList.Add( "Advantage +" );
			nameList.Add( "Advantage" );
			nameList.Add( "AdvantagePlus" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://alliance.meridianlink.com" );
			nameList.Add( "ALLIANCE CREDIT SERVICES" );
			nameList.Add( "Alliance Credit Service" );
			nameList.Add( "Alliance Credit" );
			nameList.Add( "Alliance" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://aminfcb.meridianlink.com" );
			nameList.Add( "AMI NORTH FLORIDA CREDIT" );
			nameList.Add( "AMI North Florida" );
			nameList.Add( "AMI Florida Credit" );
			nameList.Add( "AMI Florida" );
			nameList.Add( "AMI" );
			nameList.Add( "AMINFCB" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://bri.meridianlink.com" );
			nameList.Add( "BACKGROUND RESEARCH" );
			nameList.Add( "BACKGROUND" );
			m_providers.Add( nameList );
	
			nameList = new ArrayList( 20 );
			nameList.Add( "https://birchwood.meridianlink.com" );
			nameList.Add( "BCS INFORMATION SERVICES" );
			nameList.Add( "BCS INFORMATION SERVICE" );
			nameList.Add( "BCS INFORMATION" );
			nameList.Add( "BCS INFO SERVICE" );
			nameList.Add( "BCS INFO SERVICES" );
			nameList.Add( "BCS INFO" );
			nameList.Add( "BCS" );
			nameList.Add( "Birchwood" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://calcoast.meridianlink.com" );
			nameList.Add( "CAL COAST CREDIT REPORTS" );
			nameList.Add( "CALIFORNIA COAST CREDIT REPORTS" );
			nameList.Add( "CAL COAST CREDIT" );
			nameList.Add( "CALIFORNIA COAST" );
			nameList.Add( "CAL COAST" );
			nameList.Add( "CALIFORNIA COAST" );
			nameList.Add( "CAL COAST CREDIT REPORT" );
			nameList.Add( "CAL" );
			nameList.Add( "CALIFORNIA" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://calmayacredit.meridianlink.com" );
			nameList.Add( "CALMAYA CREDIT" );
			nameList.Add( "CALMAYA" );
			nameList.Add( "CALMAVACREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://cmr.meridianlink.com" );
			nameList.Add( "CAPITAL MORTGAGE REPORTS" );
			nameList.Add( "CAPITAL MORTGAGE REPORT" );
			nameList.Add( "CAPITAL MORT REPORTS" );	
			nameList.Add( "CAPITAL MORT REPORT" );	
			nameList.Add( "CAPITAL MORTGAGE" );
			nameList.Add( "CAPITAL" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://cbf.meridianlink.com" );
			nameList.Add( "CBF BUSINESS SOLUTIONS" );
			nameList.Add( "CBF BUSINESS SOLUTION" );
			nameList.Add( "CBF BUSINESS" );
			nameList.Add( "CBF" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://cbr.meridianlink.com" );
			nameList.Add( "CBRESOURCE" );
			nameList.Add( "CB RESOURCE" );
			nameList.Add( "CBRESOURCES" );
			nameList.Add( "CBRESOURCES" );
			nameList.Add( "CBR" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://cby.meridianlink.com" );
			nameList.Add( "CBY SYSTEMS INC." );
			nameList.Add( "CBY SYSTEMS" );
			nameList.Add( "CBY SYSTEMS INCORPORATION" );
			nameList.Add( "CBY SYSTEM" );
			nameList.Add( "CBY SYSTEM INC" );
			nameList.Add( "CBY" );
			nameList.Add( "CBY INC" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://cds.meridianlink.com" );
			nameList.Add( "CDS MORTGAGE REPORTS, INC." );
			nameList.Add( "CDS" );
			nameList.Add( "CDS MORT" );
			nameList.Add( "CDS MORTGAGE" );
			nameList.Add( "CDS MORTGAGE REPORT" );
			nameList.Add( "CDS MORTGAGE REPORTS" );
			nameList.Add( "CDS MORTGAGE REPORTS INC" );
			//nameList.Add( "CDS MORTGAGE REPORTS INC." );
			nameList.Add( "CDS MORTGAGE REPORT INC" );
			//nameList.Add( "CDS MORTGAGE REPORT INC." );
			nameList.Add( "CDS MORTGAGE REPORT, INC" );
			//nameList.Add( "CDS MORTGAGE REPORT, INC." );
			nameList.Add( "CDS MORTGAGE REPORT INCORPORATION" );
			nameList.Add( "CDS MORTGAGE REPORTS INCORPORATION" );
			//nameList.Add( "CDS MORTGAGE REPORTS, INCORPORATION" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://cis.meridianlink.com" );
			nameList.Add( "CIS INFORMATION SERVICES" );
			nameList.Add( "CIS INFORMATION SERVICE" );
			nameList.Add( "CIS INFO SERVICES" );
			nameList.Add( "CIS INFO SERVICE" );
			nameList.Add( "CIS" );
			nameList.Add( "CIS INFO" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.ciscocredit.com" );
			nameList.Add( "CISCO CREDIT" );
			nameList.Add( "CISCO" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.coastalcreditcorp.com" );
			nameList.Add( "COASTAL CREDIT" );
			nameList.Add( "COASTAL" );
			//nameList.Add( "COASTAL CREDIT CORP." );
			nameList.Add( "COASTAL CREDIT CORP" );
			nameList.Add( "COASTAL CREDIT CORPORATION" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credamerica.meridianlink.com" );
			nameList.Add( "CREDIT AMERICA" );
			nameList.Add( "CREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.credamerica.net" );
			nameList.Add( "CREDIT AMERICA" );
			nameList.Add( "CREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://cba.meridianlink.com" );
			nameList.Add( "CREDIT BUREAU ASSOCIATES" );
			nameList.Add( "CREDIT BUREAU ASSOCIATE" );
			nameList.Add( "CBA" );
			nameList.Add( "CREDIT BUREAU" );
			nameList.Add( "CREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://cbetinc.meridianlink.com" );
			nameList.Add( "CREDIT BUREAU OF EAST TENNESSEE" );
			nameList.Add( "CREDIT BUREAU EAST TENNESSEE" );
			nameList.Add( "CBETINC" );
			nameList.Add( "CREDIT BUREAU EAST OF TENNESSEE" );
			nameList.Add( "CREDIT BUREAU TENNESSEE" );
			nameList.Add( "CREDIT BUREAU" );
			nameList.Add( "CREDIT" );
			nameList.Add( "CBETINC" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.cbfayetteville-springdalemw.com" );
			nameList.Add( "CREDIT BUREAU OF FAYETTEVILLE-SPRINGDALE" );
			nameList.Add( "CREDIT BUREAU OF FAYETTEVILLE SPRINGDALE" );
			nameList.Add( "CREDIT BUREAU FAYETTEVILLE SPRINGDALE" );
			nameList.Add( "CREDIT BUREAU FAYETTEVILLE" );
			nameList.Add( "CREDIT BUREAU OF FAYETTEVILLE" );
			nameList.Add( "CREDIT BUREAU" );
			nameList.Add( "CREDIT" );
			nameList.Add( "cbfayetteville-springdalemw" );
			nameList.Add( "cbfayetteville" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://cbs.meridianlink.com" );
			nameList.Add( "CREDIT BUREAU SERVICES" );
			nameList.Add( "CBS" );
			nameList.Add( "CREDIT BUREAU SERVICE" );
			nameList.Add( "CREDIT BUREAU" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://cci.meridianlink.com" );
			nameList.Add( "CREDIT COMMUNICATIONS" );
			nameList.Add( "CREDIT COMMUNICATION" );
			nameList.Add( "CREDIT COMM" );
			nameList.Add( "CCI" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://cdi.meridianlink.com" );
			nameList.Add( "CREDIT DATA INFORMATION" );
			nameList.Add( "CDI" );
			nameList.Add( "CREDIT DATA" );
			nameList.Add( "CREDIT DATA INFO" );
			nameList.Add( "CREDIT DATA INFO.");
			nameList.Add( "CREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://creditlink.meridianlink.com" );
			nameList.Add( "CREDIT LINK" );
			nameList.Add( "CREDIT LINKS" );
			nameList.Add( "CREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://cm.meridianlink.com" );
			nameList.Add( "CREDIT MASTERS" );
			nameList.Add( "CM" );
			nameList.Add( "CREDIT MASTER" );
			nameList.Add( "CREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://creditonline.meridianlink.com" );
			nameList.Add( "CREDIT ONLINE" );
			nameList.Add( "CREDITONLINE" );
			nameList.Add( "CREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.creditplus.com" );
			nameList.Add( "CREDIT PLUS" );
			nameList.Add( "Credit +" );
			nameList.Add( "Credit+" );
			nameList.Add( "CREDIT" );
			nameList.Add( "CREDITPLUS" );
			m_providers.Add( nameList );
			
			nameList = new ArrayList( 20 );
			nameList.Add( "https://creditresources.meridianlink.com" );
			nameList.Add( "CREDIT RESOURCES / RMCR" );
			nameList.Add( "CREDIT RESOURCES RMCR" );
			nameList.Add( "RMCR" );
			//nameList.Add( "CREDIT RESOURCES (RMCR)" );
			nameList.Add( "CREDIT RESOURCE RMCR" );
			//nameList.Add( "CREDIT RESOURCE / RMCR" );
			nameList.Add( "CREDIT RESOURCES" );
			nameList.Add( "CREDIT RESOURCE" );
			nameList.Add( "CREDIT" );
			nameList.Add( "CREDITRESOURCES" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://cri.meridianlink.com" );
			nameList.Add( "CREDIT RESOURCES, INC / NJ" );
			nameList.Add( "CRI" );
			nameList.Add( "CREDIT RESOURCES INC NJ" );
			nameList.Add( "CREDIT RESOURCES INC" );
			nameList.Add( "CREDIT RESOURCES INCORPORATION NJ" );
			nameList.Add( "CREDIT RESOURCES INCORPORATION" );
			//nameList.Add( "CREDIT RESOURCES INC." );
			nameList.Add( "CREDIT RESOURCES" );
			nameList.Add( "CREDIT RESOURCE" );
			nameList.Add( "CREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.creditsearchinc.com" );
			nameList.Add( "CREDIT SEARCH, INC." );
			nameList.Add( "CreditSearchInc" );
			nameList.Add( "CREDIT SEARCH INC" );
			//nameList.Add( "CREDIT SEARCH, INC" );
			//nameList.Add( "CREDIT SEARCH, INC" );
			//nameList.Add( "CREDIT SEARCH, INCORPORATION" );
			nameList.Add( "CREDIT SEARCH INCORPORATION" );
			nameList.Add( "CREDIT SEARCH" );
			nameList.Add( "CREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.csc4credit.com" );
			nameList.Add( "CREDIT SERVICE COMPANY" );
			nameList.Add( "CREDIT SERVICE" );
			//nameList.Add( "CREDIT SERVICE CO." );
			nameList.Add( "CREDIT SERVICE CO" );
			nameList.Add( "CSC" );
			nameList.Add( "CREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.credittechnologies.com" );
			nameList.Add( "CREDIT TECHNOLOGIES, INC OF MICHIGAN" );
			nameList.Add( "CREDIT TECHNOLOGIES INC OF MICHIGAN" );
			nameList.Add( "CREDIT TECHNOLOGIES INCORPORATION OF MICHIGAN" );
			nameList.Add( "CREDIT TECHNOLOGIES" );
			//nameList.Add( "CREDIT TECHNOLOGIES, INC. OF MICHIGAN" );
			nameList.Add( "CREDIT TECHNOLOGIES INCORPORATION" );
			nameList.Add( "CREDIT TECHNOLOGIES MICHIGAN" );
			nameList.Add( "CREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://ctinetwork.meridianlink.com" );
			nameList.Add( "CREDIT TECHNOLOGY INC OF CALIFORNIA" );
			nameList.Add( "CTINETWORK" );
			//nameList.Add( "CREDIT TECHNOLOGY, INC OF CALIFORNIA" );
			//nameList.Add( "CREDIT TECHNOLOGY, INC. OF CALIFORNIA" );
			nameList.Add( "CREDIT TECHNOLOGY INC OF CAL" );
			nameList.Add( "CREDIT TECHNOLOGY INCOPORATION OF CAL" );
			nameList.Add( "CREDIT TECHNOLOGY INCOPORATION OF CALIFORNIA" );
			nameList.Add( "CREDIT TECH" );
			nameList.Add( "CREDIT TECHNOLOGY CALIFORNIA" );
			nameList.Add( "CREDIT TECHNOLOGY CAL" );
			nameList.Add( "CREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://cvi.meridianlink.com" );
			nameList.Add( "CREDIT VERIFIERS" );
			nameList.Add( "CREDIT VERIFIER" );
			nameList.Add( "CVI" );
			nameList.Add( "CREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://crs.meridianlink.com" );
			nameList.Add( "CRS CREDIT SERVICES" );
			nameList.Add( "CRS" );			
			nameList.Add( "CRS CREDIT SERVICE" );
			nameList.Add( "CRS CREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.datafaxcredit.com" );
			nameList.Add( "DATAFAX CREDIT SERVICES" );
			nameList.Add( "DATAFAX" );
			nameList.Add( "DATAFAX CREDIT" );
			nameList.Add( "DATAFAX CREDIT SERVICE" );
			nameList.Add( "DATAFAXCREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.dcicredit.com" );
			nameList.Add( "DCI CREDIT" );
			nameList.Add( "DCI" );
			nameList.Add( "DCICREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://dpcredit.meridianlink.com" );
			nameList.Add( "DP CREDIT" );
			nameList.Add( "DP" );
			nameList.Add( "DPCREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://ecr.meridianlink.com" );
			nameList.Add( "EASTERN CREDIT RESEARCH, INC." );
			nameList.Add( "EASTERN CREDIT RESEARCH" );
			nameList.Add( "EASTERN CREDIT RESEARCH INC" );
			//nameList.Add( "EASTERN CREDIT RESEARCH INC." );
			nameList.Add( "EASTERN CREDIT RESEARCH INCORPORATION" );
			//nameList.Add( "EASTERN CREDIT RESEARCH, INCORPORATION" );
			//nameList.Add( "EASTERN CREDIT RESEARCH, INC" );
			nameList.Add( "ECR" );
			nameList.Add( "EASTERN" );
			nameList.Add( "EASTERN CREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://factualdatareports.meridianlink.com" );
			nameList.Add( "FACTUAL DATA REPORTS" );
			nameList.Add( "FACTUAL DATA" );
			nameList.Add( "FACTUAL" );
			nameList.Add( "FACTUAL DATA REPORT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.farwest-credit.com" );
			nameList.Add( "FARWEST CREDIT" );
			nameList.Add( "FARWEST" );
			nameList.Add( "FARWESTCREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://fdr.meridianlink.com" );
			nameList.Add( "FINANCIAL DATA REPORTS" );
			nameList.Add( "FINANCIAL DATA REPORT" );
			nameList.Add( "FINANCIAL DATA" );
			nameList.Add( "FDR" );
			nameList.Add( "FINANCIAL" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.financialresearch.com" );
			nameList.Add( "FINANCIAL RESEARCH" );
			nameList.Add( "FINANCIAL" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.firstcheck.com" );
			nameList.Add( "FIRST CHECK" );
			nameList.Add( "1st CHECK" );
			nameList.Add( "1st" );
			nameList.Add( "FIRST" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://firstsource.meridianlink.com" );
			nameList.Add( "FIRST SOURCE CREDIT REPORTING" );
			nameList.Add( "FIRST SOURCE CREDIT" );
			nameList.Add( "FIRST SOURCE" );
			nameList.Add( "1st SOURCE CREDIT REPORTING" );
			nameList.Add( "1st SOURCE CREDIT" );
			nameList.Add( "1st SOURCE" );
			nameList.Add( "1st" );
			nameList.Add( "first" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://innovativecredit.meridianlink.com" );
			nameList.Add( "INNOVATIVE CREDIT SERVICES" );
			nameList.Add( "INNOVATIVE" );
			nameList.Add( "INNOVATIVE CREDIT SERVICE" );
			nameList.Add( "INNOVATIVE CREDIT" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://iscsite.meridianlink.com" );
			nameList.Add( "ISC" );
			nameList.Add( "ISC SITE" );
			nameList.Add( "ISCSITE" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.kewaneecreditbureau.com" );
			nameList.Add( "KEWANEE CREDIT BUREAU" );
			nameList.Add( "KEWANEE CREDIT" );
			nameList.Add( "KEWANEE" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://mcb.meridianlink.com" );
			nameList.Add( "MERCHANTS CREDIT BUREAU" );
			nameList.Add( "MCB" );
			nameList.Add( "MERCHANTS CREDIT" );
			nameList.Add( "MERCHANTS" );
			nameList.Add( "MERCHANT CREDIT" );
			nameList.Add( "MERCHANT CREDIT BUREAU" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://mcbsav.meridianlink.com" );
			nameList.Add( "MERCHANTS CREDIT BUREAU OF SAVANNA" );
			nameList.Add( "MERCHANT CREDIT BUREAU OF SAVANNA" );
			nameList.Add( "MCBSAV" );
			nameList.Add( "MERCHANTS CREDIT BUREAU" );
			nameList.Add( "MERCHANTS" );
			nameList.Add( "SAVANNA" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://midwestcredit.meridianlink.com" );
			nameList.Add( "MIDWEST MORTGAGE CREDIT SERVICES" );
			nameList.Add( "MIDWEST MORTGAGE CREDIT SERVICE" );
			nameList.Add( "MIDWESTCREDIT" );
			nameList.Add( "MIDWEST MORT" );
			nameList.Add( "MIDWEST" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://mcd.meridianlink.com" );
			nameList.Add( "MIDWESTERN CREDIT DATA" );
			nameList.Add( "MIDWESTERN CREDIT" );
			nameList.Add( "MCD" );
			nameList.Add( "MIDWESTERN" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.mcs-mclhost.com" );
			nameList.Add( "MORTGAGE CREDIT SERVICES" );
			nameList.Add( "MORTGAGE CREDIT SERVICE" );
			nameList.Add( "MORTGAGE CREDIT" );
			nameList.Add( "MCS" );
			nameList.Add( "MORTGAGE" );
			m_providers.Add( nameList );
			
			nameList = new ArrayList( 20 );
			nameList.Add( "https://ncs.meridianlink.com" );
			nameList.Add( "NATIONWIDE COMMERCIAL SERVICES" );
			nameList.Add( "NCS" );
			nameList.Add( "NATIONWIDE COMMERCIAL SERVICE" );
			nameList.Add( "NATIONWIDE COMMERCIAL" );
			nameList.Add( "NATIONWIDE" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://login.ncacredit.com" );
			nameList.Add( "NATIONWIDE CREDIT ACCESS" );
			nameList.Add( "NATIONWIDE CREDIT" );
			nameList.Add( "NCA CREDIT" );
			nameList.Add( "NCACREDIT" );
			nameList.Add( "NCA" );
			nameList.Add( "NATIONWIDE" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://ncd.meridianlink.com" );
			nameList.Add( "NATIONWIDE CREDIT DATA" );
			nameList.Add( "NATIONWIDE CREDIT" );
			nameList.Add( "NCD" );
			nameList.Add( "NATIONWIDE" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.nccredit.com" );
			nameList.Add( "NORTHERN CALIFORNIA CREDIT" );
			nameList.Add( "NORTHERN CALIFORNIA" );
			nameList.Add( "NC" );
			nameList.Add( "NORTHERN CAL CREDIT" );
			nameList.Add( "NORTHERN CAL" );
			nameList.Add( "NCCREDIT" );
			nameList.Add( "NORTHERN" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://premium.meridianlink.com" );
			nameList.Add( "PREMIUM CREDIT BUREAU" );
			nameList.Add( "PREMIUM CREDIT" );
			nameList.Add( "PREMIUM" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://order.professionalcreditservices.com" );
			nameList.Add( "PROFESSIONAL CREDIT SERVICES" );
			nameList.Add( "PROFESSIONAL CREDIT SERVICE" );
			nameList.Add( "PROFESSIONAL CREDIT" );
			nameList.Add( "PROFESSIONAL" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://reports.qccredit.com" );
			nameList.Add( "Q.C. CREDIT AGENCY" );
			nameList.Add( "QCCREDIT" );
			//nameList.Add( "Q.C. CREDIT" );
			nameList.Add( "QC" );
			nameList.Add( "QC CREDIT AGENCY" );
			nameList.Add( "QC CREDIT" );
			nameList.Add( "Q" );
			nameList.Add( "Q C" );
			nameList.Add( "Q C CREDIT AGENCY" );
			m_providers.Add( nameList );
			
			nameList = new ArrayList( 20 );
			nameList.Add( "https://qmr.meridianlink.com" );
			nameList.Add( "QUALITY MORTGAGE REPORTING" );
			nameList.Add( "QMR" );
			nameList.Add( "QUALITY MORTGAGE" );
			nameList.Add( "QUALITY" );
			m_providers.Add( nameList );
			
			nameList = new ArrayList( 20 );
			nameList.Add( "https://royal.meridianlink.com" );
			nameList.Add( "ROYAL MORTGAGE CREDIT REPORTING" );
			nameList.Add( "ROYAL MORTGAGE CREDIT" );
			nameList.Add( "ROYAL MORTGAGE" );
			nameList.Add( "ROYAL" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://svc1stinfo.meridianlink.com" );
			nameList.Add( "SERVICE 1ST INFOSYSTEM" );
			nameList.Add( "SERVICE" );
			nameList.Add( "SERVICE FIRST INFOSYSTEM" );
			nameList.Add( "SERVICE 1ST" );
			nameList.Add( "SERVICE FIRST" );
			nameList.Add( "SVC1STINFO" );
			nameList.Add( "SVCFIRSTINFO" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://sccb.meridianlink.com" );
			nameList.Add( "SHASTA CASCADE CREDIT BUREAUS, INC." );
			nameList.Add( "SHASTA CASCADE CREDIT BUREAUS" );
			nameList.Add( "SHASTA CASCADE CREDIT BUREAUS INCORPORATION" );
			nameList.Add( "SHASTA CASCADE CREDIT BUREAUS INC" );
			//nameList.Add( "SHASTA CASCADE CREDIT BUREAUS INC." );
			nameList.Add( "SHASTA CASCADE CREDIT" );
			nameList.Add( "SHASTA CASCADE");
			nameList.Add( "SCCB" );
			nameList.Add( "SHASTA" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://swcsi.meridianlink.com" );
			nameList.Add( "SOUTHWEST CREDIT SERVICES INC." );
			nameList.Add( "SOUTHWEST CREDIT SERVICES INC" );
			nameList.Add( "SOUTHWEST CREDIT SERVICE INC" );
			nameList.Add( "SOUTHWEST" );
			nameList.Add( "SOUTHWEST CREDIT SERVICE INCORPORATION" );
			//nameList.Add( "SOUTHWEST CREDIT SERVICE INC." );
			nameList.Add( "SWCSI" );
			nameList.Add( "SOUTHWEST CREDIT" );
			nameList.Add( "SOUTHWEST CREDIT SERVICES INCORPORATION" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://unicred.meridianlink.com" );
			nameList.Add( "UNICRED" );
			nameList.Add( "UNI");
			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://unitedoneresources.meridianlink.com" );
			nameList.Add( "UNITED ONE RESOURCES" );
			nameList.Add( "UNITED ONE RESOURCE" );
			nameList.Add( "UNITED ONE" );
			nameList.Add( "UNITEDONERESOURCES" );
			nameList.Add( "UNITED" );
			nameList.Add( "UNITED 1");
			nameList.Add( "UNITED 1 RESOURCES" );
			nameList.Add( "UNITED 1 RESOURCE" );

			m_providers.Add( nameList );

			nameList = new ArrayList( 20 );
			nameList.Add( "https://vip.meridianlink.com" );
			nameList.Add( "VIP CREDIT REPORTING" );
			nameList.Add( "VIP" );
			nameList.Add( "VIP CREDIT" );
			m_providers.Add( nameList );

			
			nameList = new ArrayList( 20 );
			nameList.Add( "https://credit.westerndataresearch.com" );
			nameList.Add( "WESTERN DATA RESEARCH" );
			nameList.Add( "WESTERN DATA" );
			nameList.Add( "WESTERN" );
			m_providers.Add( nameList );

			nameList = new ArrayList( 10 );
			nameList.Add( "HTTPS://CREDIT.MERIDIANLINK.COM" );
			nameList.Add( "FOR TEST AND DEMO ONLY" );
			nameList.Add( "TEST" );
			nameList.Add( "DEMO" );
			m_providers.Add( nameList );
        }

		private ArrayList m_providers; // ArrayList of ArrayList
		private ArrayList Providers
		{
			get {return m_providers; }
		}
		/// <summary>
		/// return a list of providers with their exact names
		/// </summary>
		/// <param name="candidate"></param>
		/// <returns></returns>
		public ArrayList Match( string candidate )
		{
			ArrayList results = new ArrayList( 100 );

			
		
			char[] splitChars = new char[]{',', '.', '/', '(', ')', '-',' '};

			string[] words = candidate.Split( splitChars );
			
			for( int iLastWord = ( words.Length - 1 ); iLastWord >= 0 ; --iLastWord )
			{
				StringBuilder sBuilder = new StringBuilder( candidate.Length * 2 );
				for( int i = 0; i <= iLastWord; ++i )
				{
					sBuilder.Append( words[i] + " ");
				}
				string cand = sBuilder.ToString().TrimWhitespaceAndBOM().ToLower();

				int nProviders = Providers.Count;
				for( int i = 0; i < nProviders; ++i )
				{
					ArrayList names = (ArrayList) ( Providers[i] );
					int nNames = names.Count;
				
					for( int j = 1; j < nNames; ++j )
					{
						string storedName = names[j].ToString();
						if( cand == storedName.ToLower() )
						{
							results.Add( names[1] );
							break;
						}
					}
				}
				if( results.Count > 0 )
					break;
			}

			return results;
		}
	
		public string GetUrl( string providerExactName )
		{
			int nProviders = Providers.Count;
			for( int i = 0; i < nProviders; ++i )
			{
				ArrayList providerNameSet = (ArrayList)(Providers[i]);
				if( providerExactName == providerNameSet[1].ToString() )
				{
					return providerNameSet[0].ToString();
				}
			}
			throw new CBaseException( ErrorMessages.Generic, "Programming error:  Must pass in one of the names returned by CCreditProviderMatch.Match()" );
		}

	}
}