﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

using DataAccess;
using LendersOffice.Admin;
using LendersOffice.AntiXss;
using LendersOffice.Common;
using LendersOfficeApp.los.admin;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace LendersOfficeApp.Website.PasswordReset
{
    public partial class RequestPasswordReset : LendersOffice.Common.BasePage
    {
        private void DisplaySuccessful()
        {
            divResetForm.Visible = false;
            divSubmitMessage.Visible = true;
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            string cmd = RequestHelper.GetSafeQueryString("cmd");

            if (!Page.IsPostBack && cmd == "success")
            {
                DisplaySuccessful();
            }
        }

        protected override void OnInit(EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            base.OnInit(e);
        }

        protected void btnSendInstructions_OnClick(object sender, EventArgs e)
        {
            string login = txtLogin.Text.TrimWhitespaceAndBOM().ToLower();
            string email = txtEmail.Text.TrimWhitespaceAndBOM().ToLower();

            string fullName = string.Empty;
            Guid brokerId;

            Guid guidPasswordReset = BrokerUserDB.CreatePasswordResetRequest_Broker(login, email, out fullName, out brokerId);

            if (guidPasswordReset == Guid.Empty)
            {
                // OPM 246520. Unsuccessful attempt to create a request.  
                // Update the failure counts and do appropriate locking.
                BrokerUserDB.IncrementPasswordResetFailureCount( login, "B", Guid.Empty, email);
            }
            else
            {
                SendPasswordResetEmail(brokerId, guidPasswordReset, email, fullName);
            }
            
            Response.Redirect("RequestPasswordReset.aspx?cmd=success");
        }

        private void SendPasswordResetEmail(Guid brokerId, Guid guidRequest, string sRecipientEmail, string sRecipientName)
        {
            string sUrl = Request.Url.Scheme + "://" + Request.Url.Host + Tools.VRoot + "/website/PasswordReset/ResetPassword.aspx?g=" + guidRequest;
            string sSubject = "Reset your LendingQB password";
            string sBody = @"Dear " + sRecipientName + @",<br /><br />To reset your password, click on the link below to complete the process.  If you are having trouble accessing the link, copy and paste the link into the address bar of Internet Explorer.
<br /><br /><a href='" + sUrl + @"'>" + sUrl + @"</a> <br /><br />Thank you";

            var cbe = new LendersOffice.Email.CBaseEmail(brokerId)
            {
                DisclaimerType = E_DisclaimerType.NORMAL,
                From = ConstStage.DefaultDoNotReplyAddress,
                To = sRecipientEmail,
                Subject = sSubject,
                Message = sBody,
                IsHtmlEmail = true,
                BccToSupport = false
            };
            cbe.Send();
        }        
    }
}
