﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="LendersOfficeApp.Website.PasswordReset.ResetPassword" %>
<%@Import Namespace="LendersOffice.AntiXss"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Change Password</title>
    
    <style type="text/css">       
        td.qaCell 
        {       
            text-align:left;    
            padding-left:1em;
            color:Black;
            font: 10pt Verdana, Sans-Serif ;
            white-space:nowrap;
        }
        ul.passwordRules li
        {
            font: 10pt Verdana, Sans-Serif ;            
        }
        td.qaCellLabel
        {
            text-align:right;
            padding-left:1em;
            color:Black;
            font: 10pt Verdana, Sans-Serif ;
            white-space:nowrap;
        }
        
        .ErrorMessage
        {
            color:Red;
        }
    </style>
    
    <script type="text/javascript">
        <!--

        function VerifyPassword() {
            HideAllRuleIcons();
            var password = document.getElementById("txtPassword").value;
            VerifyPasswordOnServer(password);
        }

        function HideAllRuleIcons()
        {
            var numRules = 7;
            for (var i = 1; i <= numRules; i++) {                
                var imgElement = document.getElementById("imgRule" + i);
                if (imgElement != null) imgElement.style.display = "none";
            }
        }
        
        function VerifyPasswordRetype() {
            var password = document.getElementById("txtPassword").value;
            var password2 = document.getElementById("txtVerifyPass").value;

            if (password != password2) {
                SetImageIcon("imgVerifyRetypePass", "F")
            }
            else {
                SetImageIcon("imgVerifyRetypePass", "T")
            }
        }
        
        function SetImageIcon(imgElementName, success) {
           var imgElement = document.getElementById(imgElementName);
           imgElement.style.display = "";
           if(success == "T")
           {
                imgElement.setAttribute("src", "../../images/pass.png");
           }
           else
           {
            imgElement.setAttribute("src", "../../images/fail.png");
           }
        }
        function ReceivePasswordVerification(arg, context) {           
            var numRules = 7;
            var idx = parseInt(arg.substring(0, 1));
            if (idx == 0) {
                for (var i = 1; i <= numRules; i++) {
                    SetImageIcon("imgRule" + i, "T");
                }
            }
            else {
                if (idx > 1) {
                    for (var i = 1; i < idx; i++) {
                        SetImageIcon("imgRule" + i, "T");
                    }
                }
                SetImageIcon("imgRule" + idx, "F");
            }
        }       
        //-->
    </script>
</head>

<body>   
    <form id="form1" runat="server">
    <center>
    <table style="width:30em">
        <tr>
        <td colspan="2" style="text-align:left">
            <br />
            <span style="font-size:14pt"> <b>Reset Your Password</b> </span>(continued)
            <br />
            <br />
            <br />
    <% if (!QuestionsVerified)
       {%>    
        
            <b>Security Questions</b>            
        </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
                
            <tr><td class="qaCellLabel">Question 1:</td> <td class="qaCell"><ml:EncodedLabel ID="lblQuestion1" runat="server"></ml:EncodedLabel></td></tr>
            <tr><td class="qaCellLabel">Answer 1:</td><td class="qaCell"><asp:TextBox ID="txtBoxAnswer1" runat="server" Width="250px"> </asp:TextBox></td><td class="qaCell"><asp:RequiredFieldValidator ID="rfvAnswer1" runat="server" ControlToValidate="txtBoxAnswer1" ErrorMessage="Answer 1 cannot be blank"><img runat="server" src="../images/error_pointer.gif" alt="ERR" /></asp:RequiredFieldValidator></td></tr>
            
            <tr><td class="qaCellLabel">Question 2:</td><td class="qaCell"><ml:EncodedLabel ID="lblQuestion2" runat="server"></ml:EncodedLabel></td></tr>
            <tr><td class="qaCellLabel">Answer 2:</td><td class="qaCell"><asp:TextBox ID="txtBoxAnswer2" runat="server" Width="250px"> </asp:TextBox></td><td class="qaCell"><asp:RequiredFieldValidator ID="rfvAnswer2" runat="server" ControlToValidate="txtBoxAnswer2" ErrorMessage="Answer 2 cannot be blank"><img runat="server" src="../images/error_pointer.gif" alt="ERR" /></asp:RequiredFieldValidator></td></tr>
            
            <tr><td class="qaCellLabel">Question 3:</td><td class="qaCell"><ml:EncodedLabel ID="lblQuestion3" runat="server"></ml:EncodedLabel></td></tr>
            <tr><td class="qaCellLabel">Answer 3:</td><td class="qaCell"><asp:TextBox ID="txtBoxAnswer3" runat="server" Width="250px"> </asp:TextBox></td><td class="qaCell"><asp:RequiredFieldValidator ID="rfvAnswer3" runat="server" ControlToValidate="txtBoxAnswer3" ErrorMessage="Answer 3 cannot be blank"><img runat="server" src="../images/error_pointer.gif" alt="ERR" /></asp:RequiredFieldValidator></td></tr>
        </table>
    
        <table>
        <tr>
            <td><asp:ValidationSummary ID="validationSummary" runat="server" /></td> <td class="ErrorMessage"><ml:EncodedLiteral ID="lblSecurityQuestionsMsg" runat="server" /> </td>
        </tr>
         <tr>
            <td colspan="3" style="text-align:center">
            <asp:Button ID="btnVerifyAnswers" runat="server" Text="Verify Answers" OnClick="btnVerifyAnswers_OnClick" />
            <input type="button" value="Cancel" onclick="javascript:window.location=<%=AspxTools.JsString("../../website/index.aspx")%>" />
            </td>
        </tr>
        </table>    
    <%} %>
    
    <%if (QuestionsVerified && !PasswordChanged)
      {%>     
        </td>
        </tr>     
        <tr>
            <td align="left" style="background-color:whitesmoke; border:1px solid black; padding:0.5em">
            <b>Your password must follow these guidelines:</b>
                <ul class="passwordRules">
                    <li>Password must contain at least one numeric and one alphabetic character.<img id="imgRule1" src="" style="display:none" visible="false" alt="" /></li>
                    <li>Password must have a minimum of 6 characters.<img id="imgRule2" src="" style="display:none" visible="false" alt="" /></li>
                    <li>Passwords can only contain safe characters. Not Allowed: < > \" ' % ; ) ( & + - =<img id="imgRule3" src="" style="display:none" alt="" /></li>
                    <li>Password cannot contain 3 identical characters in a row (Example: AAAsample1, sample1111).<img id="imgRule4" src="" style="display:none" alt="" /></li>
                    <li>Password cannot contain 3 consecutive alphabetical or numeric characters in a row (Example: sampleABC, MNOsample, sampleOPQsample, sample123sample).<img id="imgRule5" src="" style="display:none" alt="" /></li>
                    <li>Password cannot be the same as the previous password.<img id="imgRule6" src="" style="display:none" alt="" /></li>
                    <li>Password cannot contain first name, last name, or login name.<img id="imgRule7" src="" style="display:none"  alt="" /></li>                    
                </ul>
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>        
            <td style="text-align:left;">
            <table style="text-align:left">
                <tr>
                    <td>
                        <strong>New Password:</strong>
                        </td><td>
                        <asp:textbox id="txtPassword" TextMode="Password" runat="server" autocomplete="off" ></asp:textbox>
                    </td>
                    <td>
                        <img id="imgPasswordCheck" style="display:none" alt="ERR" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><strong>Retype New Password:</strong>
                    </td><td>
                        <asp:textbox id="txtVerifyPass" TextMode="Password" runat="server" autocomplete="off"></asp:textbox></td>
                    <td><img id="imgVerifyRetypePass" style="display:none" alt="ERR" /> </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="ErrorMessage" colspan="3"><ml:PassthroughLiteral ID="lblPasswordError" runat="server" /></td>            
                </tr> 
                <tr>
                    <td colspan="2" style="text-align:right; padding-top:0.25em">
                        <asp:Button id="btnChangePass" text="Change Password" OnClick="btnChangePass_OnClick" Width="105px" runat="server" />
                        <input type="button" value="Cancel" onclick='javascript:window.location=<%=AspxTools.SafeUrl("../../website/index.aspx")%>' />
                    </td>
                    <td>&nbsp;</td>
                </tr>
              </table>
              </td>
        </tr>        
    </table>    
    <%} %>
    
     <%if (PasswordChanged)
       {%>
      <b>Successfully Changed Password </b>
      <br/>
      Your password may now be used to <a href='#' onclick='javascript:window.location=<%=AspxTools.SafeUrl("../../website/index.aspx")%>'>sign in</a>      
      <% }%>
    </center>   
    </form>
</body>
</html>
