﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

using LendersOfficeApp.los.admin;
using LendersOffice.Common;
using DataAccess;
using CommonLib;
using LendersOffice.Admin;
using System.Data.SqlClient;
using LendersOffice.Security;
using LendersOffice.ObjLib.Security;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOfficeApp.Website.PasswordReset
{
    public partial class ResetPassword : LendersOffice.Common.BasePage, ICallbackEventHandler
    {
        private const int MAX_PASS_RESET_ATTEMPTS = 3;
        private const int PASS_RESET_LIFE_IN_HRS = 24;
        private const int COOKIE_EXPIRE_MINUTES = 240;

        protected Guid m_guidUserId;
        protected Guid m_guidBrokerId;
        protected Guid m_guidEmployeeId;
        protected Guid m_guidPasswordReset;
        protected Guid m_guidLenderSiteId;
        protected bool m_bQuestionsVerified;

        private StrongPasswordStatus m_ePasswordCheckStatus = StrongPasswordStatus.OK;
        
        protected bool allRulesVerified = false;

        private bool m_questionsVerified = false;
        protected bool QuestionsVerified
        {
            get
            {
                return m_questionsVerified;
            }
            set
            {
                m_questionsVerified = value;
            }
        }
        private bool m_passwordChanged;
        protected bool PasswordChanged
        {
            get
            {
                return m_passwordChanged;
            }
            set
            {
                m_passwordChanged = value;
            }
        }

        private string GetEncryptVariables()
        {
            // 7/19/2010 dd - Base on Citi security policy we cannot perform postback with 200 OK response. Therefore, after each
            // submission from user we have to perform redirect. With redirection we cannot use ViewState to store state variables.
            // This method is use to generate an encrypt value that can be safely put in query string.

            string data = Guid.NewGuid() + "|" + QuestionsVerified + "|" + PasswordChanged + "|" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
            string encryptData = EncryptionHelper.Encrypt(data);

            string cacheId = AutoExpiredTextCache.AddToCache(encryptData, TimeSpan.FromMinutes(COOKIE_EXPIRE_MINUTES));
            return cacheId;
        }

        private void DecryptAndSetVariables(string cacheId)
        {
            if (string.IsNullOrEmpty(cacheId) == false)
            {
                string data = AutoExpiredTextCache.GetFromCache(cacheId);

                if (string.IsNullOrEmpty(data) == false)
                {
                    string d = EncryptionHelper.Decrypt(data);
                    string[] parts = d.Split('|');

                    DateTime dt = DateTime.Parse(parts[3]);
                    if (dt.AddMinutes(COOKIE_EXPIRE_MINUTES).CompareTo(DateTime.Now) > 0)
                    {
                        QuestionsVerified = bool.Parse(parts[1]);
                        PasswordChanged = bool.Parse(parts[2]);
                    }
                    AutoExpiredTextCache.ExpireCache(cacheId);

                }

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string encryptData = RequestHelper.GetSafeQueryString("data");
            DecryptAndSetVariables(encryptData);

            if (!Page.IsPostBack)
            {
                ViewState["guidPasswordReset"] = m_guidPasswordReset = RequestHelper.GetGuid("g");

                lblPasswordError.Visible = false;
                lblSecurityQuestionsMsg.Visible = false;

                if (QuestionsVerified == false || PasswordChanged == false)
                {
                    bool foundRequest = false;

                    foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                    {
                        SqlParameter[] parameters = {
                                                    new SqlParameter("@RequestID", m_guidPasswordReset)
                                                };

                        using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "RetrievePasswordResetRequest", parameters))
                        {
                            if (reader.Read())
                            {
                                ViewState["UserId"] = m_guidUserId = SafeConvert.ToGuid(reader["UserId"]);
                                ViewState["EmployeeId"] = m_guidEmployeeId = SafeConvert.ToGuid(reader["EmployeeId"]);
                                ViewState["BrokerId"] = m_guidBrokerId = SafeConvert.ToGuid(reader["BrokerId"]);

                                DateTime dtTimeOfRequest = SafeConvert.ToDateTime(reader["TimeOfPasswordChangeRequest"]);

                                if (DateTime.Now.AddHours(-PASS_RESET_LIFE_IN_HRS) <= dtTimeOfRequest)
                                {
                                    foundRequest = true;
                                }

                                break;
                            }
                        }
                    }

                    if (foundRequest == false)
                    {
                        Response.Write("<center><span style='color:red'><b>The Url has expired.</b></font></center>");
                        Response.End();
                    }

                    var userQuestionAnswer = BrokerUserDB.GetSecurityQuestionAnswer(m_guidBrokerId, m_guidUserId);

                    if (userQuestionAnswer.HasMissingQuestionAnswer)
                    {
                        Response.Write("<center><span style='color:red'><b>Password reset cannot be completed due to missing answers to security questions for your account. Please contact your administrator who can complete the password reset for you.</b></font></center>");
                        Response.End();
                    }
                    else
                    {
                        var questionList = SecurityQuestion.ListAllSecurityQuestions(true);
                        if (userQuestionAnswer.SecurityQuestion1Id.HasValue)
                        {
                            lblQuestion1.Text = questionList.First(o=> o.QuestionId == userQuestionAnswer.SecurityQuestion1Id.Value).Question;
                            lblQuestion2.Text = questionList.First(o => o.QuestionId == userQuestionAnswer.SecurityQuestion2Id.Value).Question;
                            lblQuestion3.Text = questionList.First(o => o.QuestionId == userQuestionAnswer.SecurityQuestion3Id.Value).Question;
                        }
                    }
                }
            }
            else
            {
                m_guidUserId = SafeConvert.ToGuid(ViewState["UserId"]);
                m_guidBrokerId = SafeConvert.ToGuid(ViewState["BrokerId"]);
                m_guidEmployeeId = SafeConvert.ToGuid(ViewState["EmployeeId"]);
                m_guidPasswordReset = SafeConvert.ToGuid(ViewState["guidPasswordReset"]); 
            }

            txtPassword.Attributes.Add("onblur", "VerifyPassword()");
            txtVerifyPass.Attributes.Add("onblur", "VerifyPasswordRetype()");

            //Register client scripts for ICallbackEventHandler
            ClientScriptManager cm = Page.ClientScript;
            String cbReference = cm.GetCallbackEventReference(this, "arg",
                "ReceivePasswordVerification", "");
            String callbackScript = "function VerifyPasswordOnServer(arg, context) {" +
                cbReference + "; }";
            cm.RegisterClientScriptBlock(this.GetType(),
                "VerifyPasswordOnServer", callbackScript, true);

            if (Page.IsPostBack == false)
            {
                // 6/8/2010 dd - Add this block to sastify Citi security requirement. Citi does not
                // allow status 200 to return after POST back. Therefore after each POSTBACK we have
                // to perform a redirect.
                string pwdmsg = RequestHelper.GetSafeQueryString("pwdmsg");
                string securitymsg = RequestHelper.GetSafeQueryString("securitymsg");
                //string changepwd = RequestHelper.GetSafeQueryString("changepwd");
                if (!string.IsNullOrEmpty(pwdmsg))
                {
                    string str = AutoExpiredTextCache.GetFromCache(pwdmsg);
                    if (string.IsNullOrEmpty(str))
                    {
                        str = ErrorMessages.Generic;
                    }

                    //QuestionsVerified = true;
                    //PasswordChanged = false;
                    lblPasswordError.Text = str;
                    lblPasswordError.Visible = true;
                }
                else if (!string.IsNullOrEmpty(securitymsg))
                {
                    string str = AutoExpiredTextCache.GetFromCache(securitymsg);
                    if (string.IsNullOrEmpty(str))
                    {
                        str = ErrorMessages.Generic;
                    }
                    lblSecurityQuestionsMsg.Text = str;
                    lblSecurityQuestionsMsg.Visible = true;
                }

            }
        }

        //Ajax query call to validate the password
        public void RaiseCallbackEvent(String eventArgument)
        {
            string sPassword = SafeConvert.ToString(eventArgument);
            EmployeeDB employeeDb = new EmployeeDB(m_guidEmployeeId, m_guidBrokerId);
            employeeDb.Retrieve();
            m_ePasswordCheckStatus = employeeDb.IsStrongPassword(sPassword);            
        }

        //Sends the sends the status of the password check to the client
        public string GetCallbackResult()
        {
            return ((int)m_ePasswordCheckStatus).ToString();            
        }


        protected void btnVerifyAnswers_OnClick(object sender, EventArgs e)
        {
            if (AllUserDB.IsUserLocked(m_guidBrokerId, m_guidUserId, m_guidLenderSiteId))
            {
                RedirectToSecurityErrorMessage("User Account is Locked. Please contact the administrator.");
                return;
            }

            string sAnswer1 = txtBoxAnswer1.Text.TrimWhitespaceAndBOM().ToLower();
            string sAnswer2 = txtBoxAnswer2.Text.TrimWhitespaceAndBOM().ToLower();
            string sAnswer3 = txtBoxAnswer3.Text.TrimWhitespaceAndBOM().ToLower();

            int nAttempts;
            int nRet = BrokerUserDB.MatchSecurityQuestionsAnswers(m_guidBrokerId, m_guidUserId, sAnswer1, sAnswer2, sAnswer3, out nAttempts);
            if (nRet == 1)
            {
                //Answers match. User can now change the password
                QuestionsVerified = true;
                RedirectToChangePassword();
            }
            else
            {
                var employeeDB = EmployeeDB.CreateEmployeeDBWithOnlyUserNamesByBrokerAndUserId(m_guidBrokerId, m_guidUserId);

                AbstractUserPrincipal principal = null;

                if (employeeDB != null)
                {
                    principal = SystemUserPrincipal.SecurityLogPrincipal(m_guidBrokerId, employeeDB.LoginName, employeeDB.FirstName, employeeDB.LastName, InitialPrincipalTypeT.NonInternal, "B", m_guidUserId);
                }

                if (nRet == -1)
                {
                    SecurityEventLogHelper.CreateFailedAuthenticationLog(principal, LoginSource.Website);
                    RedirectToSecurityErrorMessage("User account has been disabled.");
                    return;
                }
                else
                {
                    if (nAttempts >= MAX_PASS_RESET_ATTEMPTS)
                    {
                        //Lock the user
                        AllUserDB.LockAccount(m_guidBrokerId, m_guidUserId, principal);
                        RedirectToSecurityErrorMessage("You have incorrectly answered all of the security questions.  This account is now locked for security purposes.  Please contact your administrator for further assistance.");
                    }
                    else
                    {
                        SecurityEventLogHelper.CreateFailedAuthenticationLog(principal, LoginSource.Website);
                        RedirectToSecurityErrorMessage(string.Format("One or more of the answers were incorrect.  You have {0} tries remaining before this account is locked.", MAX_PASS_RESET_ATTEMPTS - nAttempts));
                    }
                }
            }                                   
        }        

        protected void btnChangePass_OnClick(object sender, EventArgs e)
        {
            string sNewPassword = txtPassword.Text;
            string sNewPassword2 = txtVerifyPass.Text;

            if (sNewPassword != sNewPassword2)
            {
                RedirectToPasswordErrorMessage("Passwords do not match");
                return;
            }
                
            //Change password
            EmployeeDB employeeDb = new EmployeeDB(m_guidEmployeeId, m_guidBrokerId);
            if (employeeDb.Retrieve())
            {
                StrongPasswordStatus sPasswordStatus = employeeDb.IsStrongPassword(sNewPassword);
                if (sPasswordStatus != StrongPasswordStatus.OK)
                {
                    RedirectToPasswordErrorMessage(Utilities.SafeHtmlString(EmployeeDB.GetStrongPasswordUserMessage(sPasswordStatus)));
                    return;
                }

                employeeDb.ResetPasswordExpirationD();
                employeeDb.Password = sNewPassword;                
                employeeDb.Save(PrincipalFactory.CurrentPrincipal);
                //Invalidate the request after the password is changed
                AllUserDB.ExpirePasswordResetRequest(m_guidBrokerId, m_guidPasswordReset);

                PasswordChanged = true;
                RedirectToPasswordChangeSuccessfully();
            }
            else
            {
                RedirectToPasswordErrorMessage("Failed to change the password. Please contact the administrator");
            }
        }

        private void RedirectToPasswordErrorMessage(string sMessage)
        {
            string data = GetEncryptVariables();
            string cacheId = AutoExpiredTextCache.AddToCache(sMessage, TimeSpan.FromMinutes(COOKIE_EXPIRE_MINUTES));
            Response.Redirect("ResetPassword.aspx?g=" + RequestHelper.GetGuid("g") + "&pwdmsg=" + cacheId + "&data=" + data);

        }

        private void RedirectToSecurityErrorMessage(string sMessage)
        {
            // 7/16/2010 dd - We cannot pass the error msg in the query string because malicious user can manipulate the query string.
            // Found during Citi regression.
            string data = GetEncryptVariables();
            string cacheId = AutoExpiredTextCache.AddToCache(sMessage, TimeSpan.FromMinutes(COOKIE_EXPIRE_MINUTES));
            Response.Redirect("ResetPassword.aspx?g=" + RequestHelper.GetGuid("g") + "&securitymsg=" + cacheId + "&data=" + data);
        }
        private void RedirectToChangePassword()
        {
            string data = GetEncryptVariables();
            Response.Redirect("ResetPassword.aspx?g=" + RequestHelper.GetGuid("g") + "&data=" + data);
        }
        private void RedirectToPasswordChangeSuccessfully()
        {
            string data = GetEncryptVariables();
            Response.Redirect("ResetPassword.aspx?g=" + RequestHelper.GetGuid("g") + "&data=" + data);

        }
    }
}
