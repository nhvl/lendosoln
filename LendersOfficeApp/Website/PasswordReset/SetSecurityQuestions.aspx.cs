﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.Website.PasswordReset
{
    public partial class SetSecurityQuestions : LendersOffice.Common.BasePage
    {
        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LogOffPanel.Visible = false;

            if (!Page.IsPostBack)
            {
                lblStatus.Visible = false;
            }

            SecurityQuestions.SetUserInfo(BrokerUser.BrokerId, BrokerUser.UserId);
        }

        protected void btnSubmit_Click(object sender, System.EventArgs e)
        {
            bool bRet = SecurityQuestions.Save();
            if (!bRet)
            {
                DisplayMessage(SecurityQuestions.StatusMessage);
            }
            else
            {
                DisplayMessage("Your security answers have been saved.");
                RequestHelper.DoNextPostLoginTask(PostLoginTask.SetSecurityQuestions);
            }
        }

        protected void btnContinue_Click(object sender, System.EventArgs e)
        {
            // Don't ask to set security questions again (for this session only).
            RequestHelper.StoreToCookie("secquest", "f", false);

            RequestHelper.DoNextPostLoginTask(PostLoginTask.SetSecurityQuestions);
        }
        
        private void DisplayMessage(string sError)
        {
            lblStatus.Visible = true;
            lblStatus.Text = sError;
        }
    }
}
