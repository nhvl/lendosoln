﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

using CommonLib;
using LendersOffice.Admin;
using LendersOfficeApp.los.admin;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.Website.PasswordReset
{
    public partial class SetUserSecurityQuestions : System.Web.UI.UserControl
    {
        public enum Mode { Create, Edit };

        private Mode m_eMode = Mode.Create;

        private int m_nQuestionId1, m_nQuestionId2, m_nQuestionId3;
        private string m_sAnswer1 = string.Empty;
        private string m_sAnswer2 = string.Empty;
        private string m_sAnswer3 = string.Empty;

        public string StatusMessage = string.Empty;

        public Guid UserId { get; private set; }
        public Guid BrokerId { get; private set; }

        public void SetUserInfo(Guid brokerId, Guid userId)
        {
            this.UserId = userId;
            this.BrokerId = brokerId;
        }

        public Mode EditMode
        { 
            get{ return m_eMode; }
            set { m_eMode = value; }
        }

        public int QuestionId1
        {
            get{return m_nQuestionId1;}
            set{ViewState["Question1"] = m_nQuestionId1 = value;}
        }
        public int QuestionId2
        {
            get{return m_nQuestionId2;}
            set{ViewState["Question2"] = m_nQuestionId2 = value;}
        }
        public int QuestionId3
        {
            get{return m_nQuestionId3;}
            set{ViewState["Question3"] = m_nQuestionId3 = value;}
        }

        public string Answer1
        {
            get { return m_sAnswer1; }
            set { ViewState["SecurityQuestion1Answer"] = m_sAnswer1 = value; }
        }
        public string Answer2
        {
            get { return m_sAnswer2; }
            set { ViewState["SecurityQuestion2Answer"] = m_sAnswer2 = value; }
        }
        public string Answer3
        {
            get { return m_sAnswer3; }
            set { ViewState["SecurityQuestion3Answer"] = m_sAnswer3= value; }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                ViewState["Mode"] = m_eMode;

                if (m_eMode == Mode.Create || !BrokerUserDB.AreSecurityAnswersSetForTheUser(this.BrokerId, this.UserId))
                {
                    IEnumerable<SecurityQuestion> questionList = SecurityQuestion.ListAllSecurityQuestions(false);
                    BindDropdownToQuestionsDatatable(questionList, dropDownQuestion1);
                    BindDropdownToQuestionsDatatable(questionList, dropDownQuestion2);
                    BindDropdownToQuestionsDatatable(questionList, dropDownQuestion3);

                    QuestionId1 = QuestionId2 = QuestionId3 = 1;
                }
                else if(m_eMode == Mode.Edit)
                {
                    IEnumerable<SecurityQuestion> questionList = SecurityQuestion.ListAllSecurityQuestions(true);
                    //Load the existing security questions and answers

                    UserSecurityQuestionAnswerItem questionAnswer = BrokerUserDB.GetSecurityQuestionAnswer(this.BrokerId, this.UserId);

                    QuestionId1 = questionAnswer.SecurityQuestion1Id.HasValue ? questionAnswer.SecurityQuestion1Id.Value : 0;
                    QuestionId2 = questionAnswer.SecurityQuestion2Id.HasValue ? questionAnswer.SecurityQuestion2Id.Value : 0;
                    QuestionId3 = questionAnswer.SecurityQuestion3Id.HasValue ? questionAnswer.SecurityQuestion3Id.Value : 0;

                    BindDropdownToQuestionsDatatable(questionList, dropDownQuestion1, QuestionId1, QuestionId2, QuestionId3);
                    BindDropdownToQuestionsDatatable(questionList, dropDownQuestion2, QuestionId1, QuestionId2, QuestionId3);
                    BindDropdownToQuestionsDatatable(questionList, dropDownQuestion3, QuestionId1, QuestionId2, QuestionId3);

                    dropDownQuestion1.SelectedValue = QuestionId1.ToString();
                    dropDownQuestion2.SelectedValue = QuestionId2.ToString();
                    dropDownQuestion3.SelectedValue = QuestionId3.ToString();

                    Answer1 = questionAnswer.SecurityQuestion1Answer;
                    Answer2 = questionAnswer.SecurityQuestion2Answer;
                    Answer3 = questionAnswer.SecurityQuestion3Answer;
                }
            }
            else
            {
                m_eMode = (Mode)SafeConvert.ToInt(ViewState["Mode"]);

                if (m_eMode == Mode.Edit)
                {
                    m_nQuestionId1 = SafeConvert.ToInt(ViewState["Question1"]);
                    m_nQuestionId2 = SafeConvert.ToInt(ViewState["Question2"]);
                    m_nQuestionId3 = SafeConvert.ToInt(ViewState["Question3"]);

                    m_sAnswer1 = SafeConvert.ToString(ViewState["SecurityQuestion1Answer"]);
                    m_sAnswer2 = SafeConvert.ToString(ViewState["SecurityQuestion2Answer"]);
                    m_sAnswer3 = SafeConvert.ToString(ViewState["SecurityQuestion3Answer"]);
                }
            }
            Page.ClientScript.RegisterHiddenField("Answer1Modified", "0");
            Page.ClientScript.RegisterHiddenField("Answer2Modified", "0");
            Page.ClientScript.RegisterHiddenField("Answer3Modified", "0");

        }

        private void BindDropdownToQuestionsDatatable(IEnumerable<SecurityQuestion> questionList, DropDownList ddlist, params int[] arrUserQuestions)
        {
            //if arrUserQuestions is specified, dt may contain obsolete questions. The list should display a question which is either not obsolete or present in users selection
            if (arrUserQuestions.Length > 0)
            {
                List<SecurityQuestion> list = new List<SecurityQuestion>();

                foreach (var question in questionList)
                {
                    if (question.IsObsolete == false || arrUserQuestions.Contains(question.QuestionId))
                    {
                        list.Add(question);
                    }
                }

                questionList = list;
            }

            ddlist.DataSource = questionList;
            ddlist.DataTextField = "Question";
            ddlist.DataValueField = "QuestionId";
            ddlist.DataBind();
        }

        private bool ValidateAnswer(string sAnswer)
        {
            //Alpha numeric check
            Regex re = new Regex("^\\w[\\w\\s]*$");
            if (!re.Match(sAnswer).Success)
            {
                StatusMessage = "Each answer must contain only letters and/or numbers. Do not use any punctuation marks or symbols ( ! @ . $, etc)";
                return false;
            }

            //size check
            if(sAnswer.Length > 20)
            {
                StatusMessage = "Each answer must be less than 20 characters in length.";
                return false;
            }

            return true;
        }

        /// <summary>
        /// Saves the security questions and answers for the user
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
            if (this.UserId != Guid.Empty)
            {
                int nQuestion1 = SafeConvert.ToInt(dropDownQuestion1.SelectedItem.Value);
                int nQuestion2 = SafeConvert.ToInt(dropDownQuestion2.SelectedItem.Value);
                int nQuestion3 = SafeConvert.ToInt(dropDownQuestion3.SelectedItem.Value);

                string sAnswer1 = Request.Form["Answer1Modified"] == "1" ? txtBoxAnswer1.Text.TrimWhitespaceAndBOM().ToLower() : m_sAnswer1;
                string sAnswer2 = Request.Form["Answer2Modified"] == "1" ? txtBoxAnswer2.Text.TrimWhitespaceAndBOM().ToLower() : m_sAnswer2;
                string sAnswer3 = Request.Form["Answer3Modified"] == "1" ? txtBoxAnswer3.Text.TrimWhitespaceAndBOM().ToLower() : m_sAnswer3;

                // If in edit mode, short circut save and return true (save sucessful, nothing saved)
                // NOTE: Allows for saving profile if answers have not yet been chosen
                if (m_eMode == Mode.Edit)
                {
                    bool bQuestionsChanged = (nQuestion1 != m_nQuestionId1) || (nQuestion2 != m_nQuestionId2) || (nQuestion3 != m_nQuestionId3);
                    bool bAnswersChanged = (sAnswer1 != m_sAnswer1) || (sAnswer2 != m_sAnswer2) || (sAnswer3 != m_sAnswer3);
                    //If the questions and answers have not been changed, don't do anything
                    if (!(bQuestionsChanged || bAnswersChanged))
                    {
                        return true;
                    }
                }

                //Validate uniqueness of questions
                if ((nQuestion1 == nQuestion2) || (nQuestion1 == nQuestion3) || (nQuestion2 == nQuestion3))
                {
                    StatusMessage = "Please select three different questions.";
                    return false;
                }

                // Validate Answers
                if (!ValidateAnswer(sAnswer1) || !ValidateAnswer(sAnswer2) || !ValidateAnswer(sAnswer3))
                {
                    return false;
                }

                // 01/05/11 m.p. OPM 61229
                // Validate uniqueness of answers
                if ((sAnswer1 == sAnswer2) || (sAnswer1 == sAnswer3) || (sAnswer2 == sAnswer3))
                {
                    StatusMessage = "Each answer must be unique.";
                    return false;
                }

                bool bRet = BrokerUserDB.SaveSecurityQuestionsAnswers(this.BrokerId, this.UserId, nQuestion1, sAnswer1, nQuestion2, sAnswer2, nQuestion3, sAnswer3);
                Answer1 = sAnswer1; Answer2 = sAnswer2; Answer3 = sAnswer3;
                QuestionId1 = nQuestion1; QuestionId2 = nQuestion2; QuestionId3 = nQuestion3;
                StatusMessage = "Security Information has been updated.";

                txtBoxAnswer1.Text = "";
                txtBoxAnswer2.Text = "";
                txtBoxAnswer3.Text = "";
                return bRet;
            }
            else
            {
                StatusMessage = "Invalid User";
                return false;
            }            
        }
    }
}