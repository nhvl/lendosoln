﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RequestPasswordReset.aspx.cs" Inherits="LendersOfficeApp.Website.PasswordReset.RequestPasswordReset" %>
<%@Import Namespace="LendersOffice.AntiXss"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Reset Password</title>     
    <style type="text/css">
        body
        {
            font-size: 12px;    
            font-family: Verdana, Helvetica, sans-serif;    
        }
        
        .error
        {
            margin-left: 4px;
            color: red;
        }
        
        .label
        {
            width:50px;
        }

    </style>
    <script type="text/javascript">
    <!--
        function OnInit() {
            var loginBox = document.getElementById("txtLogin");
            if (loginBox != null) {
                loginBox.focus();
            }
        }

        jQuery(function($) {

            var elementIsValid =
                {
                    email: false,
                    login: false
                };
            var formWasValid = false;
            function validateForm() {
                var valid = true;
                $.each(elementIsValid, function(idx, val) {
                    return valid = valid && val;
                });
                $('#btnSendInstructions').prop('disabled', !valid);
                if (valid && !formWasValid) {
                    $('#btnSendInstructions').focus();
                }

                formWasValid = valid;
            }

            function makeValidator(element, regex, errorMessage, which) {
                var errorField = $('<span class="error">' + errorMessage + '<img src="../images/error_pointer.gif" alt="ERR" /></span>');
                $(element).after(errorField);
                errorField.hide();
                return function() {
                    if (regex.test($(element).val())) {
                        errorField.hide();
                        elementIsValid[which] = true;
                    }
                    else {
                        errorField.show();
                        elementIsValid[which] = false;
                    }

                    validateForm();
                }
            }

            $('#txtLogin').blur(makeValidator($('#txtLogin'), /.+/, "Login is required", 'login'));
            $('#txtEmail').blur(makeValidator($('#txtEmail'), /.+/, "Email is required", 'email'));
            $('#txtEmail').blur(makeValidator($('#txtEmail'), /^([\w._%+-]+@[\w.-]+\.[A-Za-z]{2,6}|)$/, "Invalid email address", 'email'));
            validateForm();
        });
    //-->
    </script>
</head>

<body onload="OnInit();">
    <form id="form1" runat="server">    	
    <center>
    <div id="divResetForm" runat="server" style="overflow:auto; min-width:500px">
    <table id="main" style="text-align:left">
        <tr>
            <td align="left" colspan="2" style="font-size:20px">Reset Your Password</td>
        </tr>
        <tr>
            <td><br /><br /></td>
        </tr>
        <tr>
            <td align="left" colspan="2"><b>Receive Instructions By Email</b></td>
        </tr>
        <tr>
            <td colspan="2">Please enter your login and e-mail address registered with LendingQB.</td>
        </tr>
        <tr><td colspan="2">
            <asp:ValidationSummary ID="errSummary" Runat="server" CssClass="ErrorMsg" DisplayMode="BulletList"/>
        </td></tr>
        <tr>
            <td class="label" align="right" style="font-size:12px"><strong>Login:</strong></td>
            <td align="left"><asp:textbox id="txtLogin" Width="200px" runat="server" autocomplete="off"></asp:textbox>
            </td>
        </tr>
        
        <tr>
            <td class="label" align="right" style="FONT-SIZE:12px"><strong>E-mail:</strong></td>
            <td align="left"><asp:textbox id="txtEmail" Width="200px" runat="server" autocomplete="off"></asp:textbox>
            </td>
        </tr>       
        <tr>
        <td></td>
        <td>
            <asp:Button id="btnSendInstructions" text="Submit" OnClick="btnSendInstructions_OnClick" runat="server" />
            <input type="button" value="Cancel" onclick='javascript:window.location=<%=AspxTools.SafeUrl("../../website/index.aspx")%>' />
        </td></tr>
      </table>
      </div>
      
      <div id="divSubmitMessage" runat="server" style=" width:50%; text-align:left" Visible="False">
          <br />
          <br />          
          <label style="font-size:14px"> <b>Password Reset Instructions Sent</b></label>
          <br/>
          <br />If an account exists with that login/email combination, you will receive an email shortly. If you do not receive an email, try again or contact your administrator.
          <br />
          <br />
      </div>
    </center>
    </form>
</body>
</html>
