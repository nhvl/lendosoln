﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetSecurityQuestions.aspx.cs"
    Inherits="LendersOfficeApp.Website.PasswordReset.SetSecurityQuestions" %>

<%@ Register Src="~/Website/PasswordReset/SetUserSecurityQuestions.ascx"TagName="SecurityQuestions" TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Set Security Information</title>
</head>
<body>

    <script type="text/javascript">
    function f_logout()
    {
      self.location = gVirtualRoot + "/logout.aspx";
    }
    </script>

    <form id="form1" runat="server">
    <table width="100%" id="LogOffPanel" runat="server">
        <tr>
            <td align="right">
                <input type="button" value="Log Off" onclick="f_logout();" value="LOG OFF" style="font-family: Verdana, Arial, Helvetica, sans-serif;
                    font-size: 11px; font-weight: bold; color: Black; margin-top: 5; margin-right: 15;" />
            </td>
        </tr>
    </table>
    <div style="width: 50em; margin: 2em">
        <div style="font: 10pt Verdana, Sans-Serif">
            <b>Please choose three different questions and provide a different answer for each one.
                These questions will be asked if you forget your password.</b>
        </div>
        <br />
        <table>
            <tr>
                <td>
                    <uc:SecurityQuestions ID="SecurityQuestions" runat="server" EditMode="Create" />
                </td>
            </tr>
        </table>
        <div style="color: Red">
            <ml:EncodedLiteral ID="lblStatus" runat="server" EnableViewState="false" />
        </div>
        <br />
        <div width="100%" style="text-align: center">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                Width="150px" />
            <asp:Button ID="btnContinue" runat="server" Text="Not Now" OnClick="btnContinue_Click"
                Width="150px" />
        </div>
    </div>
    </form>
</body>
</html>
