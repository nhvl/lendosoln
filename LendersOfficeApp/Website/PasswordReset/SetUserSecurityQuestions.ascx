﻿<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SetUserSecurityQuestions.ascx.cs"
    Inherits="LendersOfficeApp.Website.PasswordReset.SetUserSecurityQuestions" %>

<script type="text/javascript">
  function f_changeAnswer(link, i)
  {
      link.style.display = 'none';
      document.getElementById('Answer' + i + 'Modified').value = '1';
      var id = <%=AspxTools.JsString(ClientID) %> + '_txtBoxAnswer' + i;
      var tb = document.getElementById(id);
      tb.style.display = '';
  }
</script>

<table>
    <tr>
        <td nowrap="nowrap" class="FieldLabel" style="padding-right: 1em">
            Question 1
        </td>
        <td>
            <asp:DropDownList ID="dropDownQuestion1" runat="server" />
        </td>
    </tr>
    <tr>
        <td nowrap="nowrap" class="FieldLabel">
            Answer 1
        </td>
        <td>
            <asp:TextBox ID="txtBoxAnswer1" runat="server" Style="display: none" /><a href="#"
                onclick="f_changeAnswer(this, 1);">change answer</a>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td nowrap="nowrap" class="FieldLabel">
            Question 2
        </td>
        <td>
            <asp:DropDownList ID="dropDownQuestion2" runat="server" />
        </td>
    </tr>
    <tr>
        <td nowrap="nowrap" class="FieldLabel">
            Answer 2
        </td>
        <td>
            <asp:TextBox ID="txtBoxAnswer2" runat="server" Style="display: none" /><a href="#"
                onclick="f_changeAnswer(this, 2);">change answer</a>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td nowrap="nowrap" class="FieldLabel">
            Question 3
        </td>
        <td>
            <asp:DropDownList ID="dropDownQuestion3" runat="server" />
        </td>
    </tr>
    <tr>
        <td nowrap="nowrap" class="FieldLabel">
            Answer 3
        </td>
        <td>
            <asp:TextBox ID="txtBoxAnswer3" runat="server" Style="display: none" /><a href="#"
                onclick="f_changeAnswer(this, 3);">change answer</a>
        </td>
    </tr>
</table>
