<%@ Page language="c#" AutoEventWireup="true" Codebehind="index.aspx.cs" Inherits="LendersOfficeApp.Website.index" ValidateRequest="false" enableViewState="False"%>
<%@ Register TagPrefix="uc1" TagName="LoginUserControl" Src="../LoginUserControl.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import NameSpace="DataAccess" %>
<!DOCTYPE HTML>
<HTML>
    <HEAD runat="server">
        <title>LendingQB</title>
        <link href="styles/style.css" type="text/css" rel="stylesheet" />
        <link rel="icon" href="/favicon.ico?v=2" />
        <style type="text/css">
            .container { text-align: center; margin: 0px auto; }
            #SecurityNoticeContainer, #GeneralUserNotification, .IE7Warning  { text-align: center; margin: 0px auto; }
            .bg-grey {
                background-color: #fafafa;
            }
            .d-flex {
                display: flex;
            }
            .d-flex-center {
                align-items: center;
            }
            .login {
                height: calc(100vh - 24px);
            }
            .login-padding-left {
                padding-left: 50px;
            }
            .login-padding-right {
                padding-right: 50px;
            }
            .box-shadow {
                width: 500px;
                height: 520px;
                background: #fff;
                box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 2px 1px -1px rgba(0, 0, 0, 0.12);
                border-radius: 4px;
                margin: 0 auto;
                line-height: 14px;
            }
            .logo {
                margin-top: 40px;
                margin-bottom: 40px;
                text-align: center;
            }
            .login-content {
                padding-left: 14px;
                padding-right: 14px;
            }
            .text-grey {
                color: #808080;
            }
        </style>
    </HEAD>
    <body onload="_init();" id="LP_f6d1d39dee3c435d9d14f1dc245e316d" width="100%" class="bg-grey">
        <form id="index" method="post" runat="server">
            <div class="login d-flex d-flex-center">
                <div class="box-shadow">
                    <div class="logo">
                        <a href=<%= AspxTools.SafeUrl(Tools.VRoot + "/") %>>
                            <img height="51" src=<%= AspxTools.SafeUrl(Tools.VRoot + "/website/images/header_logo_lqb.png")%>  alt="logo" width="185" border="0" />
                        </a>
                    </div>
                    <div class="container login-padding-left login-padding-right">
                        <div id="dvGeneralUserNotification" runat="server">
                            <div class="text-grey" id="GeneralUserNotification">
                                <ml:PassthroughLiteral id="m_GeneralUserNotification" runat="server" EnableViewState="False"></ml:PassthroughLiteral>
                            </div>

                            <br />
                        </div>
                        <div class="login-content">
                            <uc1:loginusercontrol id="LoginUserControl1" runat="server"></uc1:loginusercontrol>
                        </div>
                        <br />

                        <div class="text-grey" id="SecurityNoticeContainer">
                            <p><strong>SECURITY NOTICE</strong></p>
                            The use of this system may be monitored for computer security
                            purposes.&nbsp; Any unauthorized access to this system is prohibited and is
                            subject to criminal and civil penalties under Federal Laws including, but not
                            limited to, the Computer Fraud and Abuse Act and the National Information
                            Infrastructure Protection Act.
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <a href=<%= AspxTools.SafeUrl(Tools.VRoot + "/Website/legal/privacy.htm") %>>Privacy</a>
                |
                <a href=<%= AspxTools.SafeUrl(Tools.VRoot + "/website/legal/term.htm") %>>Terms</a>
                |
                <a href="http://www.lendingqb.com/about.php">About Us</a>
                &nbsp;&nbsp;
                &copy;<ml:EncodedLiteral runat="server" ID="Year"></ml:EncodedLiteral> <a href="http://www.lendingqb.com">LendingQB</a>, All Rights Reserved.
            </div>
        </form>
    </body>
</HTML>
