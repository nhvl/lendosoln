﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using LendersOffice.Constants;
using LendersOffice.Common;
using DataAccess;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Reflection;
using LendersOfficeApp.LOAdmin.Test;
using System.Threading;

namespace LendersOfficeApp.Website
{
    public partial class index : BasePage
    {
        /// <summary>
        /// Sets the render mode in IE.
        /// </summary>
        /// <returns>X-UA-Comaptible meta key value.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }
        
        protected override bool EnableIEOnlyCheck
        {
            get
            {
                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ConstSite.LoginRedirectUrl))
            {
                Response.Redirect(ConstSite.LoginRedirectUrl, true);
                Response.Headers.Add("IsLogin", "true");
                return;
            }
            //if (!Page.IsPostBack)
            //{
            //    RequestHelper.ClearAuthenticationCookies();
            //}

            // OPM 194062 - Add Release Notification Warning
            Response.Headers.Add("IsLogin", "true");
            m_GeneralUserNotification.Text = ConstStage.GeneralUserNotification.TrimWhitespaceAndBOM();
            dvGeneralUserNotification.Visible = !string.IsNullOrEmpty(m_GeneralUserNotification.Text);

            Year.Text = DateTime.Today.Year.ToString();

            // SK - redirecting edocs site login to secure per opm 85042
            if (false == string.IsNullOrEmpty(ConstSite.EDocsSite))
            {
                Uri edocsSite = new Uri(ConstSite.EDocsSite);
                if (string.Compare(edocsSite.Host, Request.Url.Host, true) == 0)
                {
                    Response.Redirect(ConstSite.LOSite);
                }
            }

            Guid cx = RequestHelper.GetGuid("cx", Guid.Empty);

            if (cx == new Guid("e0865c2f-ea5f-4234-aa7a-bf8ee5a308db"))
            {
                Tools.LogInfo("Precompiling assemblies started.");
                HttpContext.Current.Server.ScriptTimeout = 600;
                Stopwatch el = Stopwatch.StartNew();
                var assemblies = new Assembly[] { 
                        Assembly.GetAssembly(typeof(CPageData)),
                        Assembly.GetExecutingAssembly(), 
                    };
                Tools.RunJITOn(assemblies);
                Tools.LogInfo("Precompile assemblies took " + el.ElapsedMilliseconds + "ms.");
                el.Stop();
            }
            else if (cx == new Guid("a70a799f-24de-41d6-949d-59678479232c"))
            {
                var runner = LUnit.WebRunnerFactory.Create(LUnit.LUnitType.Current);
                runner.RunFixture(int.MaxValue);
                ////string host = HttpContext.Current.Request.Url.Host;
                ////ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
                ////    {
                ////        TestCenter.TestManager.RunAutomatedTestAndEmail(host);
                ////    }), null);
            }
        }
    }
}
