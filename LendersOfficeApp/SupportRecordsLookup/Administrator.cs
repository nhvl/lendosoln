using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;

namespace LendersOfficeApp.SupportRecordsLookup
{
	[ XmlRoot ]
	public class Administrator
	{
		private string m_sName;
		private string m_sBranch;
		private string m_sEmail;

		public string Name 
		{
			set { m_sName = value; }
			get { return m_sName; } 
		}
		public string Branch
		{
			set { m_sBranch = value; }
			get { return m_sBranch; }
		}
		public string Email
		{
			set { m_sEmail = value; }
			get { return m_sEmail; }
		}


			#region ( Constructors )

		/// <summary>
		/// Construct default.
		/// </summary>

		public Administrator()
		{
			// Initialize members.

			m_sName = "";
			m_sBranch = "";
			m_sEmail = "";
		}

			#endregion
	}
}
