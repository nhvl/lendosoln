using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using LendersOffice.Common;

namespace LendersOfficeApp.SupportRecordsLookup
{

	[ XmlRoot ] 
	public class Administrators 
	{
		private ArrayList administrators = new ArrayList();

		[ XmlArray ] 
		[ XmlArrayItem( typeof(Administrator) ) ]
		public IList Entries 
		{
			get { return administrators; } 
		}

			#region ( Serialization Operators )

		/// <summary>
		/// Translate xml document back into Administrators.
		/// </summary>
		/// <returns>
		/// Administrators representation.
		/// </returns>

		public static Administrators ToObject( string sXml )
		{
			// Translate xml document back into Administrators.

			if(sXml.TrimWhitespaceAndBOM().Equals("")) { return new Administrators(); }

			//If the xml data doesn't have a header, give it one (for saving space in the database)
			if(sXml.IndexOf("<?xml") < 0)
			{
				string header = "<?xml version='1.0'?> <Administrators xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>";
				sXml = sXml.Insert(0, header);
			}

			return SerializationHelper.XmlDeserialize(sXml, typeof( Administrators )) as Administrators;
		}
			
		/// <summary>
		/// Translate Administrators into xml document.
		/// </summary>
		/// <returns>
		/// Xml document representation.
		/// </returns>

		public override String ToString()
		{
			// Translate Administrators into xml document.
			string serialized = SerializationHelper.XmlSerialize(this);

			//save space in the database by not storing the header (it will get added back upon deserialization)
			int index = serialized.IndexOf("<Entries>");
			serialized = serialized.Substring(index);

			return serialized;
		}

			#endregion
	}
}
