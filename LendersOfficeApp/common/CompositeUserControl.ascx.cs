using System;
using System.Reflection;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Serialization;

namespace LendersOffice.Common
{
	/// <summary>
	///	Summary description for CompositeUserControl.
	/// </summary>

	[ ParseChildren( true )
	]
	public partial  class CompositeUserControl : System.Web.UI.UserControl
	{
		private CompositeTree m_Tree = null;
		private ITemplate m_Template = null;

		private Int32 m_Indent = 16;

		public TreeBindingHandler OnTreeBinding;
		public TreeCommandHandler OnTreeCommand;

		[ PersistenceMode( PersistenceMode.InnerProperty )
		]
		[ TemplateContainer( typeof( BoundItem ) )
		]
		public ITemplate ItemTemplate
		{
			set { m_Template = value; }
			get { return m_Template; }
		}

		public String OnTreeItemBinding
		{
			// Access member.

			set
			{
				Object oTarget = null;
				
				if( Parent != null )
				{
					oTarget = Parent;
				}
				else
				{
					oTarget = Page;
				}

				try
				{
					OnTreeBinding += TreeBindingHandler.CreateDelegate
						( typeof( TreeBindingHandler )
						, oTarget
						, value
						, true
						) as TreeBindingHandler;
				}
				catch( Exception e )
				{
					throw new MissingMethodException( value , e );
				}
			}
		}

		public String OnTreeItemCommand
		{
			// Access member.

			set
			{
				Object oTarget = null;
				
				if( Parent != null )
				{
					oTarget = Parent;
				}
				else
				{
					oTarget = Page;
				}

				try
				{
					OnTreeCommand += TreeCommandHandler.CreateDelegate
						( typeof( TreeCommandHandler )
						, oTarget
						, value
						, true
						) as TreeCommandHandler;
				}
				catch( Exception e )
				{
					throw new MissingMethodException( value , e );
				}
			}
		}

		public CompositeTree DataSource
		{
			set { m_Tree = value; }
		}

		public CompositeTree Tree
		{
			get { return m_Tree; }
		}

		public int Indent
		{
			// Access member.

			set
			{
				if( value > 0 )
				{
					m_Indent = value;
				}
				else
				{
					m_Indent = 0;
				}
			}
		}

		/// <summary>
		/// Render out this tree in a way that makes it easier to
		/// view the content in a text editor.
		/// </summary>

		protected override void Render( HtmlTextWriter writer )
		{
			// Write out our tree in a html-friendly manner (at
			// least make a new line with each node).  Keep in
			// mind that we don't nest nodes within each other.
			// Each child lies below the parent with the right
			// amount of indentation to reflect the nesting.

			string nL = writer.NewLine;

			try
			{
				foreach( Control cN in Controls )
				{
					cN.RenderControl( writer );

					writer.Write( nL );
				}
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Sit on each click event that may bubble up from a
		/// bound link button.
		/// </summary>

		private void CaptureCommands( Object sender , EventArgs a )
		{
			// Sit on the click events that link buttons contained
			// within a bound item generates.

			try
			{
				if( sender is LinkButton == true )
				{
					LinkButton lB = sender as LinkButton;

					if( OnTreeCommand != null )
					{
						TreeCommandArgs tCmdArgs = new TreeCommandArgs();

						tCmdArgs.CommandName     = lB.CommandName;
						tCmdArgs.CommandArgument = lB.CommandArgument;

						if( lB.Parent != null )
						{
							// Search for the containing bound item and
							// pull out the associated key.

							Control cP = lB.Parent;

							while( cP != null )
							{
								BoundItem cB = cP as BoundItem;

								if( cB != null )
								{
									tCmdArgs.Key = cB.Key;

									break;
								}

								cP = cP.Parent;
							}
						}

						OnTreeCommand( sender , tCmdArgs );
					}
				}
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Write out our tree (including any template binding
		/// and rendering) so that the nested organization has
		/// indentation that matches.
		/// </summary>

		private void BindData( CompositeNode cN , Int32 iLevel )
		{
			// Dump given node, then embed the children.  This
			// should create the desirable indentation and list
			// the nodes in the same order.

			BoundItem oDiv = new BoundItem( cN.Id , cN.Data );

			oDiv.Attributes.Add( "style" , String.Format( "MARGIN-LEFT: {0}px;" , iLevel * m_Indent ) );
			oDiv.Attributes.Add( "key"   , String.Format( "{0}" , cN.Id ) );

			if( m_Template != null )
			{
				m_Template.InstantiateIn( oDiv );
			}

			if( OnTreeBinding != null )
			{
				OnTreeBinding( this , new TreeBindingArgs( oDiv ) );
			}

			Controls.Add( oDiv );

			foreach( CompositeNode cC in cN )
			{
				BindData( cC , iLevel + 1 );
			}
		}

		/// <summary>
		/// Recursively bind all command generating controls
		/// to our click handler so we can redirect to a
		/// more appropriate, server exposed interface.
		/// </summary>

		private void BindCommands( Control cN )
		{
			// Update command handler for any embedded link
			// buttons, etc., so we can process click events.

			LinkButton lB = cN as LinkButton;

			if( lB != null )
			{
				lB.Click += new EventHandler( CaptureCommands );
			}

			foreach( Control cC in cN.Controls )
			{
				BindCommands( cC );
			}
		}

		/// <summary>
		/// Bind our tree to the current data source.
		/// </summary>

		public override void DataBind()
		{
			// Bind our tree to what was passed in to
			// the data source.

			try
			{
				Controls.Clear();

				if( m_Tree != null )
				{
					foreach( CompositeNode cN in m_Tree )
					{
						BindData( cN , 0 );
					}

					BindCommands( this );
				}

				base.DataBind();
			}
			catch
			{
				throw;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///	Required method for Designer support - do not modify
		///	the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

	}

	/// <summary>
	/// Store children instances of self so we can have a tree
	/// of arbitrary organization.
	/// </summary>

	public class CompositeNode : IEnumerable
	{
		/// <summary>
		/// Store children instances of self so we can have
		/// a tree of arbitrary organization.
		/// </summary>

		private ArrayList m_Set = new ArrayList();
		private Object   m_Data = null;
		private Guid       m_Id = Guid.Empty;

		public CompositeNode this[ int iNode ]
		{
			get
			{
				return m_Set[ iNode ] as CompositeNode;
			}
		}

		public Object Data
		{
			set { m_Data = value; }
			get { return m_Data; }
		}

		public Guid Id
		{
			set { m_Id = value; }
			get { return m_Id; }
		}

		/// <summary>
		/// Append new composite node entry as a child.
		/// </summary>

		public void Add( CompositeNode childNode )
		{
			// Append a new child node to our set as long
			// as the id is unique.  We are not checking
			// recursively, though we should at some point.

			foreach( CompositeNode cN in m_Set )
			{
				if( cN.Id == childNode.Id )
				{
					return;
				}
			}

			m_Set.Add( childNode );
		}

		/// <summary>
		/// Walk the set and clear the children.  We descend
		/// into the tree recursively.
		/// </summary>

		public void Clear()
		{
			// Recursively clear the children by walking
			// the set.

			foreach( CompositeNode cN in m_Set )
			{
				cN.Clear();
			}

			m_Set.Clear();
		}

		/// <summary>
		/// Delegate to our set container's implementation.
		/// </summary>

		public IEnumerator GetEnumerator()
		{
			// Return our set's looping enumerator.

			return m_Set.GetEnumerator();
		}

		#region ( Constructors )

		/// <summary>
		/// Construct default.
		/// </summary>

		public CompositeNode( Guid uId , Object oData )
		{
			// Initialize members.

			m_Data = oData;
			m_Id   = uId;
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public CompositeNode()
		{
		}

		#endregion

	}

	/// <summary>
	/// Store children of the root of this tree in top level
	/// root node that lacks an id.
	/// </summary>

	public class CompositeTree : IEnumerable
	{
		/// <summary>
		/// Store children of the root of this tree in
		/// top level root node that lacks an id.
		/// </summary>

		private ArrayList m_Set = new ArrayList();

		public CompositeNode this[ int iNode ]
		{
			get
			{
				return m_Set[ iNode ] as CompositeNode;
			}
		}

		public int Count
		{
			get { return m_Set.Count; }
		}

		/// <summary>
		/// Append new composite node entry as a child.
		/// </summary>

		public void Add( CompositeNode childNode )
		{
			// Append a new child node to our set as long
			// as the id is unique.  We are not checking
			// recursively, though we should at some point.

			foreach( CompositeNode cN in m_Set )
			{
				if( cN.Id == childNode.Id )
				{
					return;
				}
			}

			m_Set.Add( childNode );
		}

		/// <summary>
		/// Walk the set and clear the children.  We descend
		/// into the tree recursively.
		/// </summary>

		public void Clear()
		{
			// Recursively clear the children by walking
			// the set.

			foreach( CompositeNode cN in m_Set )
			{
				cN.Clear();
			}

			m_Set.Clear();
		}

		/// <summary>
		/// Delegate to our set container's implementation.
		/// </summary>

		public IEnumerator GetEnumerator()
		{
			// Return our set's looping enumerator.

			return m_Set.GetEnumerator();
		}

	}

	/// <summary>
	/// Any container that wants to bind to a template
	/// declared within our user control must inherit
	/// from this base interface.
	/// </summary>

	public class BoundItem : HtmlContainerControl , INamingContainer
	{
		/// <summary>
		/// Expose bound item for accessing properties.
		/// </summary>

		private Object m_Item;
		private Guid     m_Id;

		public Object DataItem
		{
			get { return m_Item; }
		}

		public Guid Key
		{
			get { return m_Id; }
		}

		#region ( Constructors )

		/// <summary>
		/// Construct default.
		/// </summary>

		public BoundItem( Guid uId , Object oItem )
			: base( "div" )
		{
			// Initialize members.

			m_Item = oItem;
			m_Id   = uId;
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public BoundItem()
			: base( "div" )
		{
			// Initialize members.

			m_Id   = Guid.Empty;
			m_Item = null;
		}

		#endregion

	}

	/// <summary>
	/// Contain binding details for clients of the tree.
	/// </summary>

	public class TreeBindingArgs : EventArgs
	{
		/// <summary>
		/// Contain binding details for clients of the
		/// tree.
		/// </summary>

		private BoundItem m_Container = null;

		public BoundItem Container
		{
			get { return m_Container; }
		}

		#region ( Constructors )

		/// <summary>
		/// Construct default.
		/// </summary>

		public TreeBindingArgs( BoundItem biContainer )
		{
			// Initialize members.

			m_Container = biContainer;
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public TreeBindingArgs()
		{
		}

		#endregion

	}

	public delegate void TreeBindingHandler( object sender , TreeBindingArgs a );

	/// <summary>
	/// Event handling arguments.
	/// </summary>

	public class TreeCommandArgs : EventArgs
	{
		/// <summary>
		/// Stash away the info from the generating command button.
		/// </summary>

		string     m_CommandName = "";
		string m_CommandArgument = "";
		Guid               m_Key = Guid.Empty;

		public string CommandName
		{
			set { m_CommandName = value; }
			get { return m_CommandName; }
		}

		public string CommandArgument
		{
			set { m_CommandArgument = value; }
			get { return m_CommandArgument; }
		}

		public Guid Key
		{
			set { m_Key = value; }
			get { return m_Key; }
		}

	}

	public delegate void TreeCommandHandler( object sender , TreeCommandArgs a );

}
