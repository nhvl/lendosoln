using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LendersOffice.Common
{
	/// <summary>
	/// Summary description for FormUtils.
	/// </summary>
	abstract class FormUtils
	{
		public static void UCaseWebTextBox(Control ctrl)
		{
			foreach(Control child in ctrl.Controls)
			{
				if (child.GetType() == typeof(TextBox))
				{
					TextBox textbox = (TextBox)child ;
                    if (textbox.Attributes["NoUppercase"] == "True" || textbox.Attributes["NoUppercase"] == "true")
                        continue; // 4/8/2004 dd - Do not convert text to upper case for textbox that explicit ask not to convert.
					textbox.Text = textbox.Text.ToUpper() ;
				}
				else
					UCaseWebTextBox(child) ;
			}
		}
	}
}
