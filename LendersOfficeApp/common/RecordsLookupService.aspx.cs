using System;
using System.Collections;
using System.IO;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Xml;
using System.Xml.Serialization;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.los.admin;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using System.Web.Security;
using LendersOffice.ObjLib.Kayako;

namespace LendersOfficeApp.SupportRecordsLookup
{
    /// <summary>
    /// 
    /// </summary>


    public partial class RecordsLookupService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        private BrokerUserPrincipal CurrentUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "SupportStatus":
                    SupportStatus();
                    break;
            }
        }

        private void SupportStatus()
        {
            EmployeeDB db = EmployeeDB.RetrieveById(CurrentUser.BrokerId, CurrentUser.EmployeeId);

            if (db.HasSupportEmail && db.SupportEmailVerified)
            {
                AuthManager manager = new AuthManager();
                SetResult("redirect", manager.GetAuthUrlFor(CurrentUser));
                return;
            }
            
            SetResult("error", true);

            string userMessage = "There is no support email on file. Please enter a support email on 'My Profile' page."; 

            if (db.HasSupportEmail && db.SupportEmailVerified == false)
            {
                userMessage ="Your support email " + db.SupportEmail + " has not been verified. Visit 'My Profile' to resend the confirmation email.";
                
            }

            SetResult("UserMessage", userMessage);
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}















