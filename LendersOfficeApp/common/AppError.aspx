<%@ Page language="c#" Codebehind="AppError.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.common.AppError" validateRequest=false%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<head runat="server">
  <title>Error Page</title>
  <style type="text/css">
    #ContactUs
    {
        float: right;
        width: 25em;
    }
    #ContactUs .ContactUsRow
    {
    	text-align: left;
    }
    #ContactUs .ContactUsLabel
    {
    	width: 25em;
    }
    #ErrorTitle 
    {
        color:Red;
        font-weight:bold;
        font-size:medium;
        text-align:center;
    }
  </style>
</head>
<body class="EditBackground" onload="init();">
<script type="text/javascript">
<!--
var g_sDisplayKey = <%= AspxTools.JsString(DISPLAY_KEY) %>;
var possiblePipelineLoadError = <%= AspxTools.JsBool(possiblePipelineLoadError) %>;
function init() {
  <%-- Only applicable to duplicate login exception --%>
  <% if (m_isDuplicateLoginException) { %>
  if (window.dialogArguments != null) {
    window.dialogArguments.DuplicateLogin = true;
    self.close();
  }
  if (window.opener != null) {
    window.opener.location = window.opener.location;
    self.close();
  }
  <% } %>
  
  if (top.location.pathname.indexOf('/common/ModalDlg/DialogFrame.aspx') > 0) {
    var ModalFrame = retrieveFrame(window, "ModalFrame");
    if (ModalFrame.location.href.indexOf(g_sDisplayKey) < 0) {
      var ch = self.location.href.indexOf('?') > 0 ? '&' : '?';
      ModalFrame.location = self.location + ch + g_sDisplayKey + '=t' + '&possiblePipelineLoadError=' + possiblePipelineLoadError; // Don't display this error in frame.
    }
    return;
    
  }
  
  if (top.location.href.indexOf(g_sDisplayKey) < 0) {
    
    var ch = self.location.href.indexOf('?') > 0 ? '&' : '?';
    top.location = self.location + ch + g_sDisplayKey + '=t' + '&possiblePipelineLoadError=' + possiblePipelineLoadError; // Don't display this error in frame.
  }
}

function f_onCloseClick() {
  self.close();
}
//-->
</script>

  <form id="AppError" method="post" runat="server">
  <div id="content">
  <center>  
  <br />
  <div id="ContactUs">
    <div class="ContactUsRow">
        <span class="ContactUsLabel">Please contact us at <a href="https://support.lendingqb.com">https://support.lendingqb.com</a> or contact your LendingQB system administrator.</span>
    </div>
  </div>
  <div style="clear:both"></div>
  <div id="ErrorTitle">
    System Error    
  </div>
  <br />
  <b>Reference Number </b>: <%= AspxTools.HtmlString(m_errorReferenceNumber) %>
  <br />
  <b>Error </b>: <%= AspxTools.HtmlString(m_errorMessage) %>
  <asp:Panel ID="m_errorMessagePanel" runat="server" Visible="false">
     <br />
     <a href="" id="m_loginUrl" runat="server" visible="false">Click here to login again</a>  
  </asp:Panel>
  <br />
  <a href="" id="loanFindUrl" runat="server" visible="false">Clear here to search for loan</a>
  <br />
  <br />
  <br />
  
  <asp:Button runat="server" ID="CloseBtn" OnClick="m_btnClose_Click" Font-Bold="true" Text=" Close " />
  <br />
  </center>
  </div>
  </form>
</body>
</HTML>