using LendersOffice.Common;
using LendersOffice.Constants;

namespace LendersOfficeApp.common
{
    public partial class Download : BasePage
    {
        public Download()
        {
            Load += new System.EventHandler((obj, eventArgs) => VPrinterGroup.Visible = AllowVPrinterDownload);
        }

        protected bool AllowVPrinterDownload
        {
            get
            {
                var bu = LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal;
                if (bu.HasPermission(LendersOffice.Security.Permission.CanViewEDocs))
                {
                    if (bu.BrokerDB.IsEDocsEnabled)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        protected string DownloadUrlEprintV4_Updated
        {
            get { return "AppDownload.aspx?key=" + ConstAppDavid.DownloadFile_EprintV4_Updated; }
        }

        protected string DownloadUrlVPrinter
        {
            get { return "AppDownload.aspx?key=" + ConstAppDavid.DownloadFile_VPrinter; }
        }

        protected string DownloadBaseRates
        {
            get { return "AppDownload.aspx?key=" + ConstAppDavid.DownloadFile_BaseRates; }
        }

        protected string DownloadRates
        {
            get { return "AppDownload.aspx?key=" + ConstAppDavid.DownloadFile_Rates; }
        }
    }

}
