﻿using LendersOffice.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LendersOfficeApp.common
{
    public partial class PrintView_Frame : LendersOffice.Common.BaseServicePage
    {
        protected string url = "";
        protected bool IsDisplayDetailCert { get; private set; } = false; // opm 458165

        protected void Page_Load(object sender, EventArgs e)
        {
            //get the url from the option
            try
            {
                DataAccess.E_UrlOption urlOption = (DataAccess.E_UrlOption)Int32.Parse(Request["body_url"]);
                url = DataAccess.Tools.UrlOptionToUrl(urlOption);
                if (url.Equals(""))
                { 
                    String msg = @"The body_url has been given a value that does not match a value in the method UrlOptionToUrl in LosUtils.  This could be 
caused by having an Enum value missing from E_UrlOption, or by not including the Enum in UrlOptionToUrl() in LosUtils to return the page's location.";
                    DataAccess.Tools.LogErrorWithCriticalTracking(msg);
                }

                
                url =  url + "?" + Uri.UnescapeDataString(Request["params"]);
                IsDisplayDetailCert = (urlOption == DataAccess.E_UrlOption.Page_Certificate) && BrokerUserPrincipal.CurrentPrincipal?.CanViewDualCertificate == true;
            }
            catch (System.FormatException exc)
            {
                string value = Request["body_url"];
                    String msg = @"The parameter 'body_url' is given the value '" + value + @"', which cannot be parsed into int.  If '" + value + @"' is an url, this can be solved
by adding its pageId, which can be found in LoanNavigationNew.xml.config, as an Enum value for E_urlOption.  Then add the url to the method UrlOptionToUrl() in LosUtils.";

                    DataAccess.Tools.LogErrorWithCriticalTracking(msg, exc);
            }
        }
    }
}
