namespace LendersOfficeApp.common
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using MeridianLink.CommonControls;

    public class MenuItem : Control
	{

		private string m_sLabel ;
		private string m_sURL ;

		public string Label
		{
			get
			{
				return m_sLabel ;
			}
			set
			{
				m_sLabel = value ;
			}
		}
		public string URL
		{
			get 
			{
				return m_sURL ;
			}
			set 
			{
				m_sURL = value;
			}
		}
	}

	public class MenuItemControlBuilder : ControlBuilder 
	{
		public override Type GetChildControlType(string tagName, System.Collections.IDictionary attributes) 
		{
			if (string.Compare(tagName, "menuitem", true) == 0) 
			{
				return typeof(MenuItem);
			}
			return null;
		}
	}

	[	
		ControlBuilderAttribute(typeof(MenuItemControlBuilder)),
		ParseChildrenAttribute(false)
	]
	/// <summary>
	///		Summary description for HeaderNav.
	/// </summary>
	public partial class HeaderNav : System.Web.UI.UserControl
	{
		List<MenuItem> m_rgMenuItems = new List<MenuItem>() ;

		protected void PageLoad(object sender, System.EventArgs e)
		{
			if (0 == m_rgMenuItems.Count) return ;	// do nothing

			DataBind() ;

			foreach (MenuItem item in m_rgMenuItems.GetRange(0, m_rgMenuItems.Count - 1))
			{
				HyperLink link = new HyperLink();
				link.Text = LendersOffice.AntiXss.AspxTools.HtmlString(item.Label);
				link.NavigateUrl = item.URL;
				HeaderNavTable.Rows[0].Cells[0].Controls.Add(link);

				// add spacer
				HeaderNavTable.Rows[0].Cells[0].Controls.Add(new PassthroughLiteral() { Text = "&nbsp;&nbsp;" });

				System.Web.UI.WebControls.Image image = new System.Web.UI.WebControls.Image() ;
				image.ImageUrl = "~/images/nav_arrow.gif" ;
				HeaderNavTable.Rows[0].Cells[0].Controls.Add(image) ;

				// add spacer
				HeaderNavTable.Rows[0].Cells[0].Controls.Add(new PassthroughLiteral() { Text = "&nbsp;&nbsp;"}) ;
			}

			// set the last item
			MenuItem lastItem = (MenuItem)m_rgMenuItems[m_rgMenuItems.Count-1] ;
            HeaderNavTable.Rows[0].Cells[0].Controls.Add(new EncodedLiteral() { Text = lastItem.Label });
        }
        protected override void AddParsedSubObject(Object obj) 
		{
			if (obj is MenuItem)
			{
				m_rgMenuItems.Add((MenuItem)obj);
				Controls.Add((Control)obj) ; // Add to ControlCollection
			}
			else
				base.AddParsedSubObject(obj) ;			
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
