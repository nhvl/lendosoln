﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BaseTabPage.ascx.cs" Inherits="LendersOfficeApp.common.BaseTabPage" %>
<%@ Import Namespace="LendersOffice.AntiXss"    %>

    <asp:Repeater runat="server" ID="Tabs" OnItemDataBound="Tabs_OnRowBound">
        <HeaderTemplate>
            <ul class="tabnav">
        </HeaderTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
        <ItemTemplate>
            <li runat="server" id="tabEntry">
                <a href="#" runat="server" id="tabLink"></a>
            </li>
        </ItemTemplate>    
    </asp:Repeater>
    
