﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditEmployeeGroupControl.ascx.cs" Inherits="LendersOfficeApp.common.Workflow.EditEmployeeGroupControl" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script type="text/javascript">
    <!--       
    var g_iTotal = <%= AspxTools.JsNumeric(m_nEmployeeCount) %>;
    var isEmployeeChecked = false;
        
    function f_selectAll(cb) {
        for (var i = 0; i < g_iTotal; i++) {
            var o = document.getElementById("chk_" + i);
            if(o != null)
            {
                o.checked = cb.checked;
                highlightRowByCheckbox(o);
            }
        }            
        isEmployeeChecked = true;
        //SetEmployeeSelection();
    }
    function onCheckRow(checkBox) {            
        highlightRowByCheckbox(checkBox);
        isEmployeeChecked = true;
            
        if(checkBox.checked == false)
        {
            var cb = document.getElementById('chkBoxHeader')
            if(cb.checked)
                cb.checked = false;
        }
    }
    function SetEmployeeSelection()
    {        
        if(isEmployeeChecked )
        {
        var selectedEmployees = ''          
        for (var i = 0; i < g_iTotal; i++) 
        {
            var o = document.getElementById("chk_" + i);
            if(o != null)
            {
                if(o.checked)
                {
                    selectedEmployees = selectedEmployees + "," + o.value;
                }
            }                
            document.getElementById("selected_employees").value = selectedEmployees;             
        }
        }
    }
    function f_ok() {            
        SetEmployeeSelection();
        if(Page_ClientValidate())
        {
            document.getElementById("command_name").value = "update";            
            document.getElementById("close_after_cmd").value = "true";  
            window.form1.submit();                
        }
    }
    function f_cancel() {
        onClosePopup();
    }
    function f_apply()
    {            
        SetEmployeeSelection();
        if(Page_ClientValidate())
        {
            document.getElementById("command_name").value = "update";
            window.form1.submit();
        }
    }
    function _initControl()
    {    
        resize( 680 , 720 );        
        var isAllChecked = true;
        for (var i = 0; i < g_iTotal; i++) 
        {
            var o = document.getElementById("chk_" + i);
                
            if(o.checked)
            {                    
                highlightRowByCheckbox(o);
            }
            else
            {
                isAllChecked = false;
            }
        }
        if(isAllChecked)
        {
            var cb = document.getElementById('chkBoxHeader')
            cb.checked = true;
            f_selectAll(cb);
            isEmployeeChecked = false;
        }
        document.getElementById("close_after_cmd").value = '';
    }
    -->
</script>     

<div style="width:50em; margin:1em 0 0 1em;">
<div STYLE="BORDER: 1px outset; BACKGROUND-COLOR: gainsboro;">
<div class="FormTableHeader" style="padding:0.2em"  width="100%" border="0"><ml:EncodedLabel ID="lblTitle" runat="server"></ml:EncodedLabel></div>       
<div>
    <table>
        <tr>
            <td>Name</td>
            <td> <asp:TextBox ID="TextBoxName" runat="server" MaxLength="100" Width="25em"></asp:TextBox><img src="../../images/require_icon.gif" />
                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="TextBoxName" ErrorMessage="Group name is required" Display="Dynamic">
                    <img runat="server" src="../../images/error_icon.gif" title="Required Field" />
                </asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td>Description</td>
            <td> <asp:TextBox ID="TextBoxDescription" runat="server" TextMode="MultiLine" MaxLength="200" Height="8em" Width="40em"></asp:TextBox></td>
            <td><img src="../../images/require_icon.gif">
                <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="TextBoxDescription" ErrorMessage="Group description is required" Display="Dynamic">
                    <img runat="server" src="../../images/error_icon.gif" title="Required Field" />
                </asp:RequiredFieldValidator></td>
        </tr>
    </table>
</div>  
<div style="width:50em; margin:1em 0 0em 0;">
<div style="margin-bottom:0.25em"><b>Member Employees</b></div>
    <ml:CommonDataGrid id=grdData runat="server">
        <AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top" />
        <ItemStyle CssClass="GridItem" VerticalAlign="Top" />
        <HeaderStyle CssClass="GridHeader" />
    <columns>      
        <asp:TemplateColumn HeaderText="<input type=checkbox id='chkBoxHeader' onclick=f_selectAll(this);>">
        <ItemTemplate>
            <input type="checkbox" id=<%# AspxTools.JsString("chk_" + m_nCurrentEmployeeIdx++.ToString())%> value="<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "EmployeeId").ToString())%>"  <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "IsInGroup").ToString()) == "'1'" ? "checked" :"" %> onclick="onCheckRow(this);">
        </ItemTemplate>
        </asp:TemplateColumn>          
          
        <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Employee Name" SortExpression="UserLastNm, UserFirstNm">
        <ItemTemplate>
            <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "UserLastNm").ToString()) + ",&nbsp;" + AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "UserFirstNm").ToString())%>
        </ItemTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Login" SortExpression="LoginNm">
                <itemtemplate>  
                <%# AspxTools.HtmlString(Eval("LoginNm").ToString())%>
                </ItemTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Branch" SortExpression="BranchNm">
                <itemtemplate>  
                <%# AspxTools.HtmlString(Eval("BranchNm").ToString())%>
                </ItemTemplate>
        </asp:TemplateColumn>          
    </columns>
    </ml:CommonDataGrid>
</div>    
<div id="divMessage" runat="server" style="text-align:center; font:bold"></div>
</div>
    <table style="text-align:center; width:100%" cellpadding="0" cellspacing="0" style="margin-top:0.5em">
    <tr><td style="text-align:left"><asp:ValidationSummary ID="validationSummary" runat="server" CssClass="ErrorMsg" DisplayMode="BulletList"/></td></tr>
    <tr>
        <td><input type="button" id="ButtonOK" style="width:6em" onclick="f_ok()" value="OK" />
        <input type="button" id="ButtonCancel" style="width:6em" onclick="f_cancel()" value="Cancel" />
        <input type="button" id="ButtonApply" style="width:6em" onclick="f_apply()" value="Apply" /></td>
    </tr>
    </table>    
</div>
<uc1:cModalDlg runat="server" ID="Cmodaldlg2"></uc1:cModalDlg>
    
<input type="hidden" id="command_name" runat="server" enableviewstate="false" />
<input type="hidden" id="close_after_cmd" runat="server" enableviewstate="false" />
<input type="hidden" id="update_employees" runat="server" enableviewstate="false" />
<input type="hidden" id="selected_employees" runat="server" value="none" enableviewstate="false" />
