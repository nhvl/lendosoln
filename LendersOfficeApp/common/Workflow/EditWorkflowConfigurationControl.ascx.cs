﻿namespace LendersOfficeApp.common.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Services;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using ConfigSystem;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Security;
    using LendersOfficeApp.common.ModalDlg;

    public partial class EditWorkflowConfigurationControl : UserControl
    {
        private enum ConditionsColumns { BatchCheckbox = 0, Edit = 2, Delete = 3, Copy = 4, EvaluateAll = 6, Notes = 7 }
        private enum ConstraintsColumns { BatchCheckbox = 0, Edit = 2, Delete = 3, Copy = 4, Notes = 6 }
        private enum RestraintColumns { BatchCheckbox = 0, Edit = 2, Delete = 3, Copy = 4, Notes = 6 }
        private enum FieldProtectionColumns { BatchCheckbox = 0, Edit = 2, Delete = 3, Copy = 4, Operations = 5, Notes = 7 }
        private enum CustomVarColumns { BatchCheckbox = 0, Edit = 2, Delete = 3, Copy = 4 }
        private enum TriggerColumns { Notes = 6 };

        protected string m_errorMessage = string.Empty;

        private bool IsInternal => PrincipalFactory.CurrentPrincipal is InternalUserPrincipal;

        protected Guid m_brokerId
        {
            get
            {
                if (this.IsInternal)
                {
                    return RequestHelper.GetGuid("brokerid", Guid.Empty);
                }

                return PrincipalFactory.CurrentPrincipal.BrokerId;
            }
        }

        protected bool isGlobalConfig
        {
            get
            {
                return m_brokerId == ConstAppDavid.SystemBrokerGuid;
            }
        }

        protected Guid cacheId
        {
            get
            {
                return new Guid(ViewState["cacheId"].ToString());
            }
            set
            {
                ViewState["cacheId"] = value;
            }
        }

        protected WorkflowSystemConfigModel m_data { get; set; }

        protected FilterSet m_filters { get; set; }

        protected bool inDraftMode
        {
            get
            {
                if (IsInternal)
                {
                    InternalUserPermissions permissions = new InternalUserPermissions(PrincipalFactory.CurrentPrincipal.Permissions);
                    if (!permissions.Can(E_InternalUserPermissions.EditBroker))
                    {
                        return false;
                    }
                }

                return Convert.ToBoolean(m_inDraftMode.Value);
            }
            set
            {
                if (IsInternal)
                {
                    InternalUserPermissions permissions = new InternalUserPermissions(PrincipalFactory.CurrentPrincipal.Permissions);
                    if (!permissions.Can(E_InternalUserPermissions.EditBroker))
                    {
                        m_inDraftMode.Value = false.ToString();
                        return;
                    }
                }

                m_inDraftMode.Value = value.ToString();
            }
        }

        protected bool canSave
        {
            get
            {
                return m_data.CanSave;
            }
        }

        private string SearchFilter
        {
            get
            {
                return this.ViewState["SearchFilter"] == null ? string.Empty : this.ViewState["SearchFilter"].ToString();
            }
            set
            {
                this.ViewState["SearchFilter"] = value;
            }
        }

        private WorkflowViewModel m_dataSource;

        private WorkflowViewModel DataSource
        {
            get
            {
                if (m_dataSource == null)
                {
                    if (this.IsPostBack)
                    {
                        var retrievedModel = SerializationHelper.JsonNetDeserialize<WorkflowViewModel>(this.DataSourceJson.Value);

                        m_dataSource = new WorkflowViewModel(false, retrievedModel.AvailableOperations, retrievedModel.SelectedOperations);
                    }
                    else
                    {
                        Func<Filter, bool> predicate = filter => string.Equals(filter.FilterType, "operation")
                            && !string.Equals(filter.Name, "OperationsAll")
                            && WorkflowOperations.Exists(filter.Name);

                        IEnumerable<WorkflowOperation> selectedOperations = m_filters.Where(predicate).Select(filter => WorkflowOperations.Get(filter.Name));
                        if (!IsInternal)
                        {
                            selectedOperations = selectedOperations.Where(op => !WorkflowOperations.OperationsExcludedForNonInternalUsers.Contains(op));
                        }

                        m_dataSource = new WorkflowViewModel(false, WorkflowOperations.GetForDisplay(this.IsInternal), selectedOperations);
                    }
                }

                return m_dataSource;
            }
            set
            {
                m_dataSource = value;
            }
        }

        protected void AppendError(string err)
        {
            m_errorMsg.Text += AspxTools.HtmlString(err) + "<br />";
            m_saveErrorMsg.Text += AspxTools.HtmlString(err) + "<br />";
        }

        protected void ClearError()
        {
            m_errorMsg.Text = string.Empty;
            m_saveErrorMsg.Text = string.Empty;
        }

        protected void OnConstraintValidateClick(object sender, EventArgs e)
        {
            var conflicts = m_data.CheckForConflictsByConstraints();
            string serializedData = ObsoleteSerializationHelper.JsonSerialize(conflicts);
            Page.ClientScript.RegisterClientScriptBlock(typeof(EditWorkflowConfigurationControl), "ConstraintValidationData", string.Format("var ConstraintValidationData = {0};", serializedData), true);
            if (conflicts.Count == 0)
            {
                populateFiltersChecklist();
            }
            bindData();
        }

        protected void OnConditionValidateClick(object sender, EventArgs e)
        {
            var conflicts = m_data.CheckForConflictsByConditions();
            string serializedData = ObsoleteSerializationHelper.JsonSerialize(conflicts);
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "ConditionValidationData", string.Format("var ConditionValidationData = {0};", serializedData), true);
            // If there are no conflicts, show the constraints and privileges as usual
            // otherwise, do not filter the results related to the conflict
            if (conflicts.Count == 0)
            {
                populateFiltersChecklist();
            }
            bindData();
        }

        // OPM 220722 - TODO: FIX VALIDATION FOR RESTRAINTS.
        protected void OnRestraintValidateClick(object sender, EventArgs e)
        {
            var conflicts = m_data.CheckForConflictsByConditions();
            string serializedData = ObsoleteSerializationHelper.JsonSerialize(conflicts);
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "RestraintValidationData", string.Format("var RestraintValidationData = {0};", serializedData), true);
            // If there are no conflicts, show the constraints and privileges as usual
            // otherwise, do not filter the results related to the conflict
            if (conflicts.Count == 0)
            {
                populateFiltersChecklist();
            }
            bindData();
        }

        protected void OnConditionsBound(object sender, DataGridItemEventArgs e)
        {
            if (!inDraftMode || !canSave)
            {
                e.Item.Cells[(int)ConditionsColumns.BatchCheckbox].Visible = false;
                e.Item.Cells[(int)ConditionsColumns.Edit].Visible = false;
                e.Item.Cells[(int)ConditionsColumns.Delete].Visible = false;
                e.Item.Cells[(int)ConditionsColumns.Copy].Visible = false;
            }
            e.Item.Cells[(int)ConditionsColumns.EvaluateAll].CssClass += " center";
            e.Item.Cells[(int)ConditionsColumns.Notes].Visible = false;
        }

        protected void OnConstraintsBound(object sender, DataGridItemEventArgs e)
        {
            if (!inDraftMode || !canSave)
            {
                e.Item.Cells[(int)ConstraintsColumns.BatchCheckbox].Visible = false;
                e.Item.Cells[(int)ConstraintsColumns.Edit].Visible = false;
                e.Item.Cells[(int)ConstraintsColumns.Delete].Visible = false;
                e.Item.Cells[(int)ConstraintsColumns.Copy].Visible = false;
            }
            e.Item.Cells[(int)ConstraintsColumns.Notes].Visible = false;
        }

        protected void OnRestraintsBound(object sender, DataGridItemEventArgs e)
        {
            if (!inDraftMode || !canSave)
            {
                e.Item.Cells[(int)RestraintColumns.BatchCheckbox].Visible = false;
                e.Item.Cells[(int)RestraintColumns.Edit].Visible = false;
                e.Item.Cells[(int)RestraintColumns.Delete].Visible = false;
                e.Item.Cells[(int)RestraintColumns.Copy].Visible = false;
            }
            e.Item.Cells[(int)RestraintColumns.Notes].Visible = false;
        }

        protected void OnFieldProtectionRulesBound(object sender, DataGridItemEventArgs e)
        {
            if (!inDraftMode || !canSave)
            {
                e.Item.Cells[(int)FieldProtectionColumns.BatchCheckbox].Visible = false;
                e.Item.Cells[(int)FieldProtectionColumns.Edit].Visible = false;
                e.Item.Cells[(int)FieldProtectionColumns.Delete].Visible = false;
                e.Item.Cells[(int)FieldProtectionColumns.Copy].Visible = false;
            }
            e.Item.Cells[(int)FieldProtectionColumns.Notes].Visible = false;
        }

        protected void OnImportXMLClicked(object sender, EventArgs e)
        {
            if (IsInternal)
            {
                Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);
            }

            if (m_inputFile.HasFile)
            {
                try
                {
                    m_data.ImportIntoDraft(m_inputFile.PostedFile.InputStream);
                }
                catch (ConfigValidationException ex)
                {
                    m_errorMessage = ex.ToString();
                    Tools.LogError(ex);
                    return;
                }
                catch (CBaseException exc)
                {
                    m_errorMessage = "Unable to import workflow";
                    Tools.LogError(exc);
                    return;
                }
                bindData();
            }
        }

        /// <summary>
        /// This should probably be a webmethod from outside
        /// </summary>
        protected void exportXML()
        {
            BrokerDB brokerDB = BrokerDB.RetrieveById(this.m_brokerId);
            Response.AddHeader("Content-Disposition", $"attachment; filename=\"{brokerDB.CustomerCode}_WorkflowConfig.xml\"");
            Response.ContentType = "text/xml";
            m_data.ExportDraftXml(Response.Output);
            Response.Flush();
            Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        protected void OnExportConditions(object sender, EventArgs e)
        {
            Response.AddHeader("Content-Disposition", "attachment; filename=\"privileges.csv\"");
            Response.ContentType = "text/csv";
            m_data.ExportDataTable(WorkflowSystemConfigModel.TableType.ConditionsExport, Response.Output, getRolesForFilter(), getOperationsForFilter());
            Response.Flush();
            Response.End();
        }

        protected void OnExportConstraints(object sender, EventArgs e)
        {
            Response.AddHeader("Content-Disposition", "attachment; filename=\"constraints.csv\"");
            Response.ContentType = "text/csv";
            m_data.ExportDataTable(WorkflowSystemConfigModel.TableType.Constraints, Response.Output, getRolesForFilter(), getOperationsForFilter());
            Response.Flush();
            Response.End();
        }

        protected void OnExportRestraints(object sender, EventArgs e)
        {
            Response.AddHeader("Content-Disposition", "attachment; filename=\"constraints.csv\"");
            Response.ContentType = "text/csv";
            m_data.ExportDataTable(WorkflowSystemConfigModel.TableType.Restraints, Response.Output, getRolesForFilter(), getOperationsForFilter());
            Response.Flush();
            Response.End();
        }

        protected void OnExportFieldProtectionRules(object sender, EventArgs e)
        {
            var operations = getOperationsForFilter();
            if (operations.Exists((op) => op == WorkflowOperations.WriteLoan || op == WorkflowOperations.WriteField))
            {
                Response.AddHeader("Content-Disposition", "attachment; filename=\"fieldProtectionRules.csv\"");
                Response.ContentType = "text/csv";
                m_data.ExportDataTable(WorkflowSystemConfigModel.TableType.FieldProtectionRulesExport, Response.Output, getRolesForFilter(), new[] { WorkflowOperations.ProtectField });
                Response.Flush();
                Response.End();
            }
        }

        protected void OnExportTriggers(object sender, EventArgs e)
        {
            Response.AddHeader("Content-Disposition", "attachment; filename=\"triggers.csv\"");
            Response.ContentType = "text/csv";

            foreach (var table in m_data.GetCustomVariableDataTables())
            {
                Response.Output.Write(table.ToCSV());
                Response.Output.WriteLine();
            }

            Response.Flush();
            Response.End();
        }

        protected void OnExportExcel_BrokerReadWriteAccess(object sender, EventArgs e)
        {
            BrokerDB brokerDB = BrokerDB.RetrieveById(this.m_brokerId);
            string tempFilePath = TempFileUtils.NewTempFilePath() + ".xlsx";
            this.m_data.ExportReadWriteAccessToExcel(tempFilePath, brokerDB);
            RequestHelper.SendFileToClient(HttpContext.Current, tempFilePath, "application/octet-stream", brokerDB.CustomerCode + " Read-Write " + DateTime.Now.ToString("yyyy-MM-dd") + ".xlsx");
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            FileOperationHelper.Delete(tempFilePath);
        }

        protected void publish()
        {
            if (IsInternal)
            {
                Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);
            }

            if (!inDraftMode && !isGlobalConfig)
            {
                BrokerDB brokerDB = BrokerDB.RetrieveById(m_brokerId);
                brokerDB.IsPmlSubmissionAllowed = m_isPmlSubmissionAllowed.Checked;
                brokerDB.IsRateLockedAtSubmission = m_isRateLockedAtSubmission.Checked;
                brokerDB.PmlSubmitRLockDefaultT = int.Parse(m_pmlSubmitRLockDefaultT.SelectedItem.Value);
                brokerDB.Save();
            }

            try
            {
                if (inDraftMode)
                {
                    WorkflowSystemConfigModel.Publish(m_brokerId, (AbstractUserPrincipal)Page.User);
                    cModalDlg.CloseDialog(Page);
                }
            }
            catch (ConfigValidationException exc)
            {
                foreach (string s in exc.ConditionErrors)
                    AppendError(s);
                foreach (string s in exc.ConstraintErrors)
                    AppendError(s);
                foreach (string s in exc.CustomVariableErrors)
                    AppendError(s);
            }
            catch (CBaseException exc)
            {
                AppendError(exc.UserMessage);
            }
        }

        private new BasePage Page
        {
            get { return (BasePage)base.Page; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.ClientIDMode = ClientIDMode.Static;
            Page.RegisterCSS("Workflow/EditWorkflowConfiguration.css");
            Page.RegisterJsScript("IEPolyfill.js");
            Page.RegisterJsScript("LOAdmin.Broker.WorkflowOperations.js");

            Page.EnableJqueryMigrate = false;
            Page.RegisterJsScript("LQBPopup.js");
            Page.RegisterJsScript("jquery.tmpl.js");
            Page.RegisterJsScript("utilities.js");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ClearError();

            // We add here because these are asp buttons
            m_BtnConditionValidate.Attributes.Add("onclick",
                "javascript:" +
                "document.getElementById('m_hiddenCmd').value = 'conditionValidate';"
                );

            m_btnConstraint.Attributes.Add("onclick",
                "javascript:" +
                "document.getElementById('m_hiddenCmd').value = 'constraintValidate';"
                );

            m_BtnRestraintValidate.Attributes.Add("onclick",
                "javascript:" +
                "document.getElementById('m_hiddenCmd').value = 'restraintValidate';"
                );

            bool deferBindData = false;

            Page.ClientScript.GetPostBackEventReference(this, string.Empty);
            m_hiddenNotes.Value = string.Empty;
            m_data = new WorkflowSystemConfigModel(m_brokerId, inDraftMode);
            m_filters = loadFilters();

            Page.RegisterJsGlobalVariables("IsInternal", this.IsInternal);
            Page.RegisterJsGlobalVariables("canSave", this.canSave);

            if (!Page.IsPostBack)
            {                
                if (!isGlobalConfig)
                {
                    BrokerDB brokerDB = BrokerDB.RetrieveById(m_brokerId);
                    m_isPmlSubmissionAllowed.Checked = brokerDB.IsPmlSubmissionAllowed;
                    m_isRateLockedAtSubmission.Checked = brokerDB.IsRateLockedAtSubmission;
                    if (brokerDB.PmlSubmitRLockDefaultT != 2)
                        m_pmlSubmitRLockDefaultT.SelectedIndex = 0;
                    else
                        m_pmlSubmitRLockDefaultT.SelectedIndex = 1;
                }
                else
                {
                    // Hide stuff for global configs
                    //inDraftMode = true;
                }

                // The default rolesList.
                m_rolesList.DataSource = (m_data.GetParameterFromFields("Role") as SecurityParameter.EnumeratedParmeter).EnumMapping.OrderBy(enumMap => enumMap.FriendlyValue);
                m_rolesList.DataTextField = "FriendlyValue";
                m_rolesList.DataValueField = "RepValue";
                m_rolesList.DataBind();
            }

            if (!inDraftMode)
                m_openDraft.Disabled = !m_data.HasDraft();

            switch (m_hiddenCmd.Value)
            {
            case "filter":
                saveFilters();
                break;
            case "getNotesOfCondition":
                populateFiltersChecklist();
                loadNotes(m_data.GetCondition(new Guid(m_hiddenId.Value)).Notes);
                break;
            case "deleteCondition":
                populateFiltersChecklist();
                m_data.RemoveCondition(new Guid(m_hiddenId.Value));
                saveToDraft();
                break;
            case "copyCondition":
                populateFiltersChecklist();
                m_data.CopyCondition(new Guid(m_hiddenId.Value));
                saveToDraft();
                break;
            case "getNotesOfConstraint":
                populateFiltersChecklist();
                loadNotes(m_data.GetConstraint(new Guid(m_hiddenId.Value)).Notes);
                break;
            case "deleteConstraint":
                populateFiltersChecklist();
                m_data.RemoveConstraint(new Guid(m_hiddenId.Value));
                saveToDraft();
                break;
            case "copyConstraint":
                populateFiltersChecklist();
                m_data.CopyConstraint(new Guid(m_hiddenId.Value));
                saveToDraft();
                break;
            case "getNotesOfRestraint":
                populateFiltersChecklist();
                loadNotes(m_data.GetRestraint(new Guid(m_hiddenId.Value)).Notes);
                break;
            case "deleteRestraint":
                populateFiltersChecklist();
                m_data.RemoveRestraint(new Guid(m_hiddenId.Value));
                saveToDraft();
                break;
            case "copyRestraint":
                populateFiltersChecklist();
                m_data.CopyRestraint(new Guid(m_hiddenId.Value));
                saveToDraft();
                break;
            case "getNotesOfFieldProtectionRule":
                populateFiltersChecklist();
                loadNotes(m_data.GetCondition(new Guid(m_hiddenId.Value)).Notes);
                break;
            case "deleteFieldProtectionRule":
                populateFiltersChecklist();
                m_data.RemoveCondition(new Guid(m_hiddenId.Value));
                saveToDraft();
                break;
            case "copyFieldProtectionRule":
                populateFiltersChecklist();
                m_data.CopyCondition(new Guid(m_hiddenId.Value));
                saveToDraft();
                break;
            case "getNotesOfCustomVariable":
                populateFiltersChecklist();
                loadNotes(m_data.GetCustomVariable(new Guid(m_hiddenId.Value)).Notes);
                break;
            case "DeleteCustomVar":
                populateFiltersChecklist();
                m_data.RemoveCustomVariable(new Guid(m_hiddenId.Value));
                saveToDraft();
                break;
            case "CopyCustomVar":
                populateFiltersChecklist();
                m_data.CopyCustomVariable(new Guid(m_hiddenId.Value));
                saveToDraft();
                break;
            case "createDraft":
                populateFiltersChecklist();
                saveToDraft(true);
                break;
            case "publish":
                populateFiltersChecklist();
                publish();
                break;
            case "exportXML":
                exportXML();
                break;
            case "conditionValidate":
            case "constraintValidate":
            case "restraintValidate":
                m_rolesAll.Checked = true;
                deferBindData = true; // we'll BindData during the onValidate functions
                break;
            case "batchCopy":
                BatchCopy();
                saveToDraft();
                break;
            case "batchDelete":
                BatchDelete();
                saveToDraft();
                break;
            case "batchDisable":
                BatchDisable();
                saveToDraft();
                break;

            case "deleteParameters":
                BatchDeleteParameters();
                break;

            default:
                populateFiltersChecklist();
                break;
            }

            // We want searches to be persistent across page loads until either a new
            // search is performed or a search is cleared.
            if (this.SearchFilter != string.Empty)
            {
                FilterAndBindData();
            }
            else if (!deferBindData)
            {
                bindData();
            }
        }

        protected void bindData()
        {
            var roles = getRolesForFilter();
            var operations = getOperationsForFilter();
            DataTable conditions = m_data.GenerateDataTable(WorkflowSystemConfigModel.TableType.Conditions, roles, operations);
            m_conditionsGrid.DataSource = conditions;
            m_conditionsGrid.DataBind();
            DataTable constraints = m_data.GenerateDataTable(WorkflowSystemConfigModel.TableType.Constraints, roles, operations);
            m_constraintsGrid.DataSource = constraints;
            m_constraintsGrid.DataBind();

            DataTable restraints = m_data.GenerateDataTable(WorkflowSystemConfigModel.TableType.Restraints, roles, operations);
            m_restraintsGrid.DataSource = restraints;
            m_restraintsGrid.DataBind();

            // Task Backend Spec: 4.3.1
            if (operations.Exists((op) => op == WorkflowOperations.WriteLoan || op == WorkflowOperations.WriteField))
            {
                DataTable fieldProtectionRules = m_data.GenerateDataTable(WorkflowSystemConfigModel.TableType.FieldProtectionRules, roles, new[] { WorkflowOperations.ProtectField });
                m_fieldProtectionRulesGrid.DataSource = fieldProtectionRules;
                m_fieldProtectionRulesGrid.DataBind();
                m_fieldProtectionRules.Visible = true;
            }
            else
            {
                m_fieldProtectionRules.Visible = false;
            }

            m_customVariableSet.DataSource = m_data.GetCustomVariableDataTables();
            m_customVariableSet.DataBind();
        }

        protected FilterSet loadFilters()
        {
            ConfigSystem.DataAccess.IConfigRepository repository = ConfigHandler.GetRepository(m_brokerId);
            return repository.LoadFilters(m_brokerId);
        }

        protected void saveFilters()
        {
            // We don't care about the old filters anymore, so clear them
            m_filters.RemoveAll();

            // Figure out what is selected
            if (m_rolesAll.Checked)
            {
                m_filters.Add(new Filter(Filter.ROLESALL, Filter.TYPE_ROLE));
            }
            foreach (ListItem roleCB in m_rolesList.Items)
            {
                if (roleCB.Selected)
                {
                    Filter newFilter = new Filter(roleCB.Text, Filter.TYPE_ROLE);
                    m_filters.Add(newFilter);
                }
            }

            foreach (var operation in this.DataSource.RetrieveSelectedOperations())
            {
                m_filters.Add(new Filter(operation.Id, "operation"));
            }

            // Now save it
            ConfigSystem.DataAccess.IConfigRepository repository = ConfigHandler.GetRepository(m_brokerId);
            repository.SaveFilters(m_brokerId, m_filters);

        }

        /// <summary>
        /// This will set the role checkboxes according to m_filters.
        /// </summary>
        protected void populateFiltersChecklist()
        {

            this.DataSourceJson.Value = SerializationHelper.JsonNetSerialize(this.DataSource);

            foreach (ListItem roleCB in m_rolesList.Items)
            {
                roleCB.Selected = false;
            }

            IEnumerator<Filter> filtersEnumerator = m_filters.GetEnumerator();
            filtersEnumerator.MoveNext();
            if (filtersEnumerator.Current == null)
            {
                m_rolesAll.Checked = true;
                return;
            }

            // Assuming the rolesList and the filters are in the same order
            // Then we can step through them in the same order

            // m_rolesList does not contain m_rolesAll, hence we have the separation here
            if (filtersEnumerator.Current.Name == Filter.ROLESALL)
            {
                m_rolesAll.Checked = true;
                filtersEnumerator.MoveNext();
            }
            else
            {
                m_rolesAll.Checked = false;
            }
            foreach (ListItem roleCB in m_rolesList.Items)
            {
                if (filtersEnumerator.Current == null)
                {
                    break;
                }
                if (filtersEnumerator.Current.Name == roleCB.Text)
                {
                    roleCB.Selected = true;
                    filtersEnumerator.MoveNext();
                }
            }
        }

        protected List<string> getRolesForFilter()
        {
            List<string> roles = new List<string>();
            foreach (ListItem roleCB in m_rolesList.Items)
            {
                if (roleCB.Selected || m_rolesAll.Checked)
                    roles.Add(roleCB.Value);
            }
            return roles;
        }

        protected List<WorkflowOperation> getOperationsForFilter()
        {
            return this.DataSource.RetrieveSelectedOperations();
        }

        protected void loadNotes(string notes)
        {
            m_hiddenNotes.Value = String.IsNullOrEmpty(notes) ? " " : notes;
        }

        protected void saveToDraft(bool forceSaveOnValidationError = false)
        {
            if (IsInternal)
            {
                Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);
            }

            inDraftMode = true;
            m_data.SwitchToDraft();
            try
            {
                m_data.Save(forceSaveOnValidationError);
            }
            catch (ConfigValidationException exc)
            {
                m_data = new WorkflowSystemConfigModel(m_brokerId, inDraftMode);
                foreach (string s in exc.ConditionErrors)
                    AppendError(s);
                foreach (string s in exc.ConstraintErrors)
                    AppendError(s);
                foreach (string s in exc.CustomVariableErrors)
                    AppendError(s);
            }
            catch (CBaseException exc)
            {
                m_data = new WorkflowSystemConfigModel(m_brokerId, inDraftMode);
                AppendError(exc.UserMessage);
            }
        }

        protected void OnCustomVariableBound(object sender, RepeaterItemEventArgs args)
        {
            DataTable customVar = args.Item.DataItem as DataTable;
            DataGrid CustomVariableGrid = args.Item.FindControl("m_customVariableGrid") as DataGrid;

            CustomVariableGrid.DataSource = customVar;
            CustomVariableGrid.DataBind();
            if (!inDraftMode)
            {

                CustomVariableGrid.Columns[(int)CustomVarColumns.BatchCheckbox].Visible = false;
                CustomVariableGrid.Columns[(int)CustomVarColumns.Edit].Visible = false;
                CustomVariableGrid.Columns[(int)CustomVarColumns.Delete].Visible = false;
                CustomVariableGrid.Columns[(int)CustomVarColumns.Copy].Visible = false;
            }
        }

        protected void OnCustomVariableGridBinding(object sender, DataGridItemEventArgs args)
        {
            // The trigger data grid is set up to have both a link that displays trigger
            // notes and a column that displays trigger notes. We need the column so that
            // the notes link will work, but displaying both the column and the link is
            // redundant. As such, we'll hide the column from view but use its contents
            // when the user clicks the note link.
            var triggerDataGrid = args.Item;

            triggerDataGrid.Cells[(int)TriggerColumns.Notes].Visible = false;
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            FilterAndBindData();
        }

        private void FilterAndBindData()
        {
            var oldRolesFilterChecked = m_rolesAll.Checked;
            var oldData = new WorkflowSystemConfigModel(this.m_brokerId, this.m_data.ExportXml(), this.inDraftMode);

            // For the purposes of searching, we want to display any applicable search
            // results regardless of any filters in place.
            m_rolesAll.Checked = true;

            var enteredSearchText = this.SearchTextbox.Text.Trim();

            this.SearchFilter = enteredSearchText;

            this.m_data.Filter(enteredSearchText);

            bindData();

            this.m_data = oldData;
            m_rolesAll.Checked = oldRolesFilterChecked;
        }

        protected void ClearSearchButton_Click(object sender, EventArgs e)
        {
            if (this.SearchFilter == string.Empty)
            {
                // Nothing to do.
                return;
            }

            this.SearchFilter = string.Empty;

            this.SearchTextbox.Text = string.Empty;

            bindData();
        }

        private void BatchCopy()
        {
            BatchOperationUsing(
                conditionAction: (id) => this.m_data.CopyCondition(id),
                constraintAction: (id) => this.m_data.CopyConstraint(id),
                restraintAction: (id) => this.m_data.CopyRestraint(id),
                triggerAction: (id) => this.m_data.CopyCustomVariable(id));
        }

        private void BatchDelete()
        {
            BatchOperationUsing(
                conditionAction: (id) => this.m_data.RemoveCondition(id),
                constraintAction: (id) => this.m_data.RemoveConstraint(id),
                restraintAction: (id) => this.m_data.RemoveRestraint(id),
                triggerAction: (id) => this.m_data.RemoveCustomVariable(id));
        }

        private void BatchDisable()
        {

            Action<ConfigSystem.ParameterSet> addAlwaysFalse = (ParameterSet parameterSet) =>
            {
                if(parameterSet != null)
                {
                    parameterSet.RemoveParameter("AlwaysTrue");
                    var alwaysFalse = new ConfigSystem.Parameter("AlwaysTrue", E_FunctionT.Equal, E_ParameterValueType.Bool, false, false);
                    alwaysFalse.AddValue("False", "False");
                    parameterSet.AddParameter(alwaysFalse);
                }

            };

            BatchOperationUsing(
                conditionAction: (id) => addAlwaysFalse( this.m_data.GetCondition(id)?.ParameterSet ?? null) ,
                constraintAction: (id) => addAlwaysFalse(this.m_data.GetConstraint(id)?.ParameterSet ?? null),
                restraintAction: (id) => addAlwaysFalse(this.m_data.GetRestraint(id)?.ParameterSet ?? null),
                triggerAction: (id) => addAlwaysFalse(this.m_data.GetCustomVariable(id)?.ParameterSet ?? null));
        }


        private void BatchOperationUsing(Action<Guid> conditionAction, Action<Guid> constraintAction,
            Action<Guid> restraintAction, Action<Guid> triggerAction)
        {
            var conditionList = MakeListOfGuidsForBatch(this.m_ConditionAndFieldIds.Value);

            foreach (var condition in conditionList)
            {
                conditionAction(condition);
            }

            var constraintList = MakeListOfGuidsForBatch(this.m_ConstraintIds.Value);

            foreach (var constraint in constraintList)
            {
                constraintAction(constraint);
            }

            var restraintList = MakeListOfGuidsForBatch(this.m_RestraintIds.Value);

            foreach (var restraint in restraintList)
            {
                restraintAction(restraint);
            }

            var triggerList = MakeListOfGuidsForBatch(this.m_TriggerIds.Value);

            foreach (var trigger in triggerList)
            {
                triggerAction(trigger);
            }

            this.m_ConditionAndFieldIds.Value = string.Empty;
            this.m_ConstraintIds.Value = string.Empty;
            this.m_RestraintIds.Value = string.Empty;
            this.m_TriggerIds.Value = string.Empty;
        }

        private static readonly string[] Separator = new string[] { ";" };
        private static IEnumerable<Guid> MakeListOfGuidsForBatch(string idList)
        {
            return idList.Split(Separator, StringSplitOptions.RemoveEmptyEntries).Select(ruleId => new Guid(ruleId));
        }

        public static IEnumerable<object> GetParameterNamesFromRules(Guid brokerId, bool draftMode, string conditionIds, string constraintIds,
                                                                     string restraintIds, string triggerIds)
        {

            var wf = new WorkflowSystemConfigModel(brokerId, draftMode);
            var nameDict = new Dictionary<string, string>();

            Tuple<string, Func<Guid, AbstractConditionGroup>>[] entries = {
                Tuple.Create<string, Func<Guid, AbstractConditionGroup>>(conditionIds, id => wf.GetCondition(id)),
                Tuple.Create<string, Func<Guid, AbstractConditionGroup>>(constraintIds, id => wf.GetConstraint(id)),
                Tuple.Create<string, Func<Guid, AbstractConditionGroup>>(restraintIds, id => wf.GetRestraint(id)),
            };

            foreach (var entry in entries)
            {
                IEnumerable<Guid> itemIds = MakeListOfGuidsForBatch(entry.Item1);
                foreach (var itemId in itemIds)
                {
                    AbstractConditionGroup configItem = entry.Item2(itemId);
                    if (configItem == null)
                    {
                        continue;
                    }

                    foreach (var p in configItem.ParameterSet)
                    {
                        if (!nameDict.ContainsKey(p.Name))
                        {
                            nameDict.Add(p.Name, wf.GetFriendlyName(p.Name));
                        }
                    }
                }
            }

            IEnumerable<Guid> ids = MakeListOfGuidsForBatch(triggerIds);
            foreach (var itemId in ids)
            {
                var customVariable = wf.GetCustomVariable(itemId);
                if (customVariable == null)
                {
                    continue;
                }

                foreach (var p in customVariable.ParameterSet)
                {
                    if (!nameDict.ContainsKey(p.Name))
                    {
                        nameDict.Add(p.Name, p.Name);
                    }
                }
            }

            return nameDict.Select(kv => new { key = kv.Key, desc = kv.Value }).ToList().OrderBy(o => o.key, StringComparer.OrdinalIgnoreCase);
        }

        private static bool BatchDeleteParameters(WorkflowSystemConfigModel wf, string conditionIds, string constraintIds,
                                                 string restraintIds, string triggerIds, string removedParameterNames, out string errMsg)
        {
            List<string>[] lists = { new List<string>(), new List<string>(), new List<string>(), new List<string>(), new List<string>() };
            string[] labels = { "privilege rule(s)", "constrain rule(s)", "restraint rule(s)", "trigger(s)" };

            Tuple<string, Func<Guid, AbstractConditionGroup>, List<string>>[] entries = {
                Tuple.Create<string, Func<Guid, AbstractConditionGroup>, List<string>>(conditionIds, id => wf.GetCondition(id), lists[0]),
                Tuple.Create<string, Func<Guid, AbstractConditionGroup>, List<string>>(constraintIds, id => wf.GetConstraint(id), lists[1]),
                Tuple.Create<string, Func<Guid, AbstractConditionGroup>, List<string>>(restraintIds, id => wf.GetRestraint(id), lists[2]),
            };

            var removednames = new List<string>(removedParameterNames.Split(Separator, StringSplitOptions.RemoveEmptyEntries));

            bool err = false;
            foreach (var entry in entries)
            {
                List<string> errItems = entry.Item3;
                IEnumerable<Guid> itemIds = MakeListOfGuidsForBatch(entry.Item1);
                foreach (var itemId in itemIds)
                {
                    AbstractConditionGroup configItem = entry.Item2(itemId);
                    if (configItem == null)
                    {
                        continue;
                    }

                    foreach (string name in removednames)
                    {
                        configItem.ParameterSet.RemoveParameter(name);
                    }

                    if (configItem.ParameterSet.ParameterCount == 0)
                    {
                        errItems.Add(configItem.Id.ToString());
                        err = true;
                    }
                }
            }

            IEnumerable<Guid> ids = MakeListOfGuidsForBatch(triggerIds);
            foreach (var itemId in ids)
            {
                var customVariable = wf.GetCustomVariable(itemId);
                if (customVariable == null)
                {
                    continue;
                }

                foreach (string name in removednames)
                {
                    customVariable.ParameterSet.RemoveParameter(name);
                }

                if (customVariable.ParameterSet.ParameterCount == 0)
                {
                    lists[3].Add($"{customVariable.Name} ({customVariable.Id})");
                    err = true;
                }
            }


            if (err)
            {
                StringBuilder sb = new StringBuilder("Batch Delete operation blocked. The following rule(s) would have no parameters specified in the logic.\r\n\r\n");

                for (int i = 0; i < 4; i++)
                {
                    var errItems = lists[i];
                    if (errItems.Count != 0)
                    {
                        sb.AppendLine($"   - {errItems.Count} {labels[i]}: " + string.Join(", ", errItems));
                    }
                }

                sb.Append("\r\nPress Ctrl + C to copy this message onto your clipboard.");
                errMsg = sb.ToString();
                return false;
            }

            return wf.ValidateBeforeSave(out errMsg);
        }

        private void BatchDeleteParameters()
        {
            string errMsg;
            bool isValid = BatchDeleteParameters(m_data, this.m_ConditionAndFieldIds.Value, this.m_RestraintIds.Value, this.m_RestraintIds.Value,
                                                 this.m_TriggerIds.Value, this.m_paraNames.Value, out errMsg);
            if (!isValid)
            {
                this.m_errorMessage = errMsg;
                m_data = new WorkflowSystemConfigModel(m_brokerId, inDraftMode);
            }
            else
            {
                saveToDraft();
            }
        }

        public static object DeleteParametersValidate(Guid brokerId, bool draftMode, string conditionIds, string constraintIds,
                                                      string restraintIds, string triggerIds, string removedParameterNames)
        {
            var wf = new WorkflowSystemConfigModel(brokerId, draftMode);
            string errMsg;
            BatchDeleteParameters(wf, conditionIds, constraintIds, restraintIds, triggerIds, removedParameterNames, out errMsg);
            return new {ErrorMsg = errMsg};
        }
    }
}
