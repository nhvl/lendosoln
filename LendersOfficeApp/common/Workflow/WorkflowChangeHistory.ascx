﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WorkflowChangeHistory.ascx.cs" Inherits="LendersOfficeApp.common.Workflow.WorkflowChangeHistory" %>

<style type="text/css">
    body {
        background-color: gainsboro;
        width: 100%;
    }
    h3 {
        text-align: center;
    }
    table {         
        margin: 5px auto;
        width: 100%;
        border-collapse: collapse;
    }
    th, td {
        text-align:center;
        border: 1px solid black;
        padding: 3px;
    }
    #auditDiv {
        margin: 10px;
    }
    .desc {
        text-align: left;
    }
</style>
<script type="text/javascript">
    $(function () {
        if (audits.length != 0) {
            $("#auditTemplate").tmpl({ audits: audits, currentConfigId: ML.currentConfigId }).appendTo("#auditDiv");
            $("#noAudits").hide();
        }
    });

    function errorDownloadingXml(err) {
        alert(err);
    }

    function downloadXML(configId) {
        var url;
        if (ML.isInternal) {
            url = VRoot + "/LOAdmin/Broker/Workflow/WorkflowChangeHistoryHolder.aspx?cmd=download&brokerid="+ ML.brokerId + "&configid=" + configId;
        }
        else {
            url = VRoot + "/los/Workflow/WorkflowChangeHistoryHolder.aspx?cmd=download&configid=" + configId;
        }

        fetchFileViaFrame(url, null);
    }

    function revert(configId, auditDate) {
        var data = {
            ConfigId: configId,
            BrokerId: ML.brokerId,
            AuditDate: auditDate
        };
        gService.workflowAudits.callAsyncSimple("Revert", data, function (result) {
            if (!result.error) {
                alert("Reverted successfully");
                window.location.reload();
            }
            else {
                alert(result.UserMessage);
            }
        });
    }
</script>

<script id="auditTemplate" type="text/x-jquery-tmpl">
    <table class="FormTable Table">
        <tr class="FormTableHeader">
            <th>Description</th>
            <th>Time Stamp</th>
            <th>User</th>
            <th>Configuration XML</th>
            <th>Revert</th>
        </tr>
        {{each audits}}
        <tr class="GridAutoItem">
            <td class="desc">${AuditDescription}</td>
            <td>${AuditDateFormatted}</td>
            <td>${UserDescription}</td>
            <td>
                {{if IsReleaseConfigAudit}}
                    <input type="button" value="Download" onclick="downloadXML('${ReleaseConfigId}');"/>
                {{else}}
                    N/A
                {{/if}}
            </td>
            <td>
                {{if IsReleaseConfigAudit && ReleaseConfigId != currentConfigId}}
                    <input type="button" value="Revert" onclick="revert('${ReleaseConfigId}', '${AuditDateFormatted}');"/>
                {{else}}
                    N/A
                {{/if}}
            </td>
        </tr>
        {{/each}}
    </table>
</script>

<h3>Workflow Change History</h3>        
<div id="auditDiv"></div>
<h3 id="noAudits">There are no audits at this time.</h3>