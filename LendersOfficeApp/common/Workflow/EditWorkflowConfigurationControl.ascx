﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditWorkflowConfigurationControl.ascx.cs" Inherits="LendersOfficeApp.common.Workflow.EditWorkflowConfigurationControl"
%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<script id="tmplParaName" type="text/x-jquery-tmpl">
    <li><input type="checkbox" class="batch" value='${key}' ${disabled} /> ${desc}</li>
</script>

<script type="text/javascript">
    function _initControl()
    {
        if(<%= AspxTools.JsString(m_errorMessage) %>.length > 0)
        {
            alert(<%= AspxTools.JsString(m_errorMessage) %>);
        }
    }
</script>

<input type="hidden" value="" runat="server" id="m_hiddenCmd" />
<input type="hidden" value="" runat="server" id="m_hiddenId" />
<input type="hidden" value="" runat="server" id="m_ConditionAndFieldIds" />
<input type="hidden" value="" runat="server" id="m_ConstraintIds" />
<input type="hidden" value="" runat="server" id="m_RestraintIds" />
<input type="hidden" value="" runat="server" id="m_TriggerIds" />
<input type="hidden" value="" runat="server" id="m_hiddenNotes" />
<input type="hidden" value="False" runat="server" id="m_inDraftMode" />
<input type="hidden" value="false" runat="server" id="m_customVarMode" />
<input type="hidden" value="false" runat="server" id="m_paraNames" />
<script type="text/javascript">
    var g_brokerId = <%= AspxTools.JsString(m_brokerId) %>;
    var isDraft = document.getElementById("m_inDraftMode");
    var conflictMode = false;
    var openedWindow = null;

    function show(url, windowType, postbackFunc)
    {
        if(openedWindow == null || openedWindow.closed)
        {
            var windowUrl = url + '?brokerId=' + g_brokerId;

            if (windowType === 'NonModal'){
                openWindowFn = showNonModal(windowUrl);
            } else {
                openedWindow = showModal(windowUrl, null, null, null, postbackFunc)
            }
        }
    }

    function showNonModal(url)
    {
        openedWindow = window.open(VRoot + url, "modalMimic", "resizable=yes,scrollbars=yes");
    }

    function onAdd(type) {
        var url;
        if (ML.IsInternal) {
            url = '/LOAdmin/Broker/Workflow/EditWorkflowConfigItem.aspx?brokerId=' + g_brokerId;
        }
        else{
            url = '/los/Workflow/EditWorkflowConfigItem.aspx';
        }

        url += (ML.IsInternal ? '&' : '?') + 'isDraft=' + isDraft.value + '&action=add&type=' + type;

        showNonModal(url);
    }

    function onAddCustomVar(type) {
        var url;
        if (ML.IsInternal) {
            url = '/LOAdmin/Broker/Workflow/EditWorkflowConfigCustomVariable.aspx?brokerId=' + g_brokerId;
        }
        else{
            url = '/los/Workflow/EditWorkflowConfigCustomVariable.aspx';
        }

        url += (ML.IsInternal ? '&' : '?')
            + 'isDraft=' + isDraft.value
            + '&action=add&type=' + type
            + '&isGlobal=' + <%= AspxTools.JsString(isGlobalConfig.ToString()) %>;

        showNonModal(url);
    }
    function onAddFieldProtectionRule(type) {
        var url;
        if (ML.IsInternal) {
            url = '/LOAdmin/Broker/Workflow/EditWorkflowConfigFieldProtectionRule.aspx?brokerId=' + g_brokerId;
        }
        else{
            url = '/los/Workflow/EditWorkflowConfigFieldProtectionRule.aspx';
        }

        url += (ML.IsInternal ? '&' : '?')
            + 'isDraft=' + isDraft.value
            + '&action=add'
            + '&isGlobal=' + <%= AspxTools.JsString(isGlobalConfig.ToString()) %>;

        showNonModal(url);
    }

    function onNotes(type, id) {
        document.getElementById("m_hiddenId").value = id;
        document.getElementById("m_hiddenCmd").value = "getNotesOf" + type;
        performPostback();
    }

    function onEdit(type, id) {
        var url;
        if (ML.IsInternal) {
            url = '/LOAdmin/Broker/Workflow/EditWorkflowConfigItem.aspx?brokerId=' + g_brokerId;
        }
        else{
            url = '/los/Workflow/EditWorkflowConfigItem.aspx';
        }

        url += (ML.IsInternal ? '&' : '?') + 'isDraft=' + isDraft.value + '&action=edit&type=' + type + '&itemId=' + id;

        showNonModal(url);
    }
            
    function onEditFieldProtectionRule(type, id) {
        var url;
        if (ML.IsInternal) {
            url = '/LOAdmin/Broker/Workflow/EditWorkflowConfigFieldProtectionRule.aspx?brokerId=' + g_brokerId;
        }
        else{
            url = '/los/Workflow/EditWorkflowConfigFieldProtectionRule.aspx';
        }

        url += (ML.IsInternal ? '&' : '?')
            + 'isDraft=' + isDraft.value
            + '&action=edit'
            + '&isGlobal=' + <%= AspxTools.JsString(isGlobalConfig.ToString()) %>
            + '&itemId=' + id;

        showNonModal(url);
    }
            
    function onEditCustomVar(type, id) {
        var url;
        if (ML.IsInternal) {
            url = '/LOAdmin/Broker/Workflow/EditWorkflowConfigCustomVariable.aspx?brokerId=' + g_brokerId;
        }
        else{
            url = '/los/Workflow/EditWorkflowConfigCustomVariable.aspx';
        }

        url += (ML.IsInternal ? '&' : '?')
            + 'isDraft=' + isDraft.value
            + '&action=edit&type=' + type 
            + '&itemId=' + id
            + '&isGlobal=' + <%= AspxTools.JsString(isGlobalConfig.ToString()) %>;

        showNonModal(url);
    }

    function onDelete(type, id, tableId) {
        var rows = document.getElementById(tableId).tBodies[0].rows;
        var selectedRow;

        for(var i = 0; i < rows.length; i++) {
            var row = rows[i];

            // The second cell in a row contains the hidden input with the
            // ID of the workflow rule.
            var inputs = row.cells[1].getElementsByTagName('input');
            if (inputs.length == 0) {
                continue;
            }
            if (id == inputs[0].value) {
                selectedRow = row;
            }
        }

        var originalClass = selectedRow.className;
        selectedRow.className = 'GridSelectedItem';
        if (confirm("Delete selected item?") == false) {
            selectedRow.className = originalClass;
            return;
        }

        document.getElementById("m_hiddenId").value = id;
        document.getElementById("m_hiddenCmd").value = "delete" + type;
        performPostback();
    }

    function onCustomVarOperation(op, name, id) {
        if (confirm(op + " Trigger " + name + "?") == false)
            return;

        document.getElementById("m_hiddenId").value = id;
        document.getElementById("m_hiddenCmd").value = op + "CustomVar";
        performPostback();
    }

    function onCopy(type, id, tableId) {
        var rows = document.getElementById(tableId).tBodies[0].rows;
        var selectedRow;

        for( var i = 0; i < rows.length; i++) {
            var row = rows[i];

            // The second cell in a row contains the hidden input with the
            // ID of the workflow rule.
            var inputs = row.cells[1].getElementsByTagName('input');
            if (inputs.length == 0) {
                continue;
            }
            if (id == inputs[0].value) {
                selectedRow = row;
            }
        }

        var originalClass = selectedRow.className;
        selectedRow.className = 'GridSelectedItem';
        if (confirm("Copy selected item?") == false) {
            selectedRow.className = originalClass;
            return;
        }

        document.getElementById("m_hiddenId").value = id;
        document.getElementById("m_hiddenCmd").value = "copy" + type;
        performPostback();
    }

    function onCreateDraft() {
        document.getElementById("m_hiddenCmd").value = "createDraft";
        performPostback();
    }

    function onOpenDraft() {
        document.getElementById('m_inDraftMode').value = "True";
        performPostback();
    }

    function onFilter() {
        document.getElementById("m_hiddenCmd").value = "filter";
        performPostback();
    }

    function onExportXML() {
        document.getElementById("m_hiddenCmd").value = "exportXML";
        performPostback();
    }
     
    function onViewAudit() {
        if (ML.IsInternal) {
            url = '/LOAdmin/Broker/Workflow/WorkflowChangeHistoryHolder.aspx?brokerId=' + g_brokerId;
        }
        else{
            url = '/los/Workflow/WorkflowChangeHistoryHolder.aspx';
        }

        window.open(VRoot + url, '', 'resizable=yes,scrollbars=yes');
    }

    function editCustomVarCallback()
    {
        if (!conflictMode) {
            performPostback();
        }
    }

    function performPostback() {
        __doPostBack('', '');
        document.getElementById("m_hiddenId").value = ""; // Some postbacks will not refresh the page. Clear out stuff.
        document.getElementById("m_hiddenCmd").value = "";
    }

    function setCustomVarMode() {
        customVarMode = document.getElementById('m_customVarMode').value;
        var FPR = document.getElementById('m_fieldProtectionRules');
        if(customVarMode == 'false') {
            document.getElementById('toggleCustomVarsLink').innerText = "Triggers";
            document.getElementById('m_conditions').style.display = '';
            if (hasConstraints) document.getElementById('m_constraints').style.display = '';
            document.getElementById('m_restraints').style.display = '';
            if (FPR) { FPR.style.display = ''; }
            document.getElementById('m_customVariables').style.display = 'none';
        } else {
            document.getElementById('toggleCustomVarsLink').innerText = "Privileges and Constraints";
            document.getElementById('m_conditions').style.display = 'none';
            document.getElementById('m_constraints').style.display = 'none';
            document.getElementById('m_restraints').style.display = 'none';
            if (FPR) { FPR.style.display = 'none'; }
            document.getElementById('m_customVariables').style.display = '';
        }
    }

    function toggleCustomVars() {
        customVarMode = document.getElementById('m_customVarMode').value;
        if(customVarMode == 'false') {
            document.getElementById('m_customVarMode').value = 'true';
        } else {
            document.getElementById('m_customVarMode').value = 'false';
        }

        setCustomVarMode();
    }

    function onSaveClicked() {
        if (isDraft.value == 'True') {
            if (confirm("Do you want to release the draft to production?") == false)
                return;
        }

        document.getElementById("m_hiddenCmd").value = "publish";
        performPostback();
    }

    function BatchSelect(container, selectAll) {
        var selector = "#" + container + " input:checkbox";

        $(selector).prop('checked', selectAll).trigger("change");
    }

    function getCheckedIdsForBatchOperation() {
        var idsForCmd = [];

        $("#m_conditionsContainer .batch:checkbox:checked").each(function (){
            var idInput = $(this).closest('tr').find("input[type='hidden']")[0];

            idsForCmd.push(idInput.value);
        });

        $("#m_fieldProtectionRules .batch:checkbox:checked").each(function (){
            var idInput = $(this).closest('tr').find("input[type='hidden']")[0];

            idsForCmd.push(idInput.value);
        });

        document.getElementById("m_ConditionAndFieldIds").value = idsForCmd.join(";");

        idsForCmd.length = 0;

        $("#m_constraintsContainer .batch:checkbox:checked").each(function (){
            var idInput = $(this).closest('tr').find("input[type='hidden']")[0];

            idsForCmd.push(idInput.value);
        });

        document.getElementById("m_ConstraintIds").value = idsForCmd.join(";");

        idsForCmd.length = 0;

        $("#m_restraintsContainer .batch:checkbox:checked").each(function (){
            var idInput = $(this).closest('tr').find("input[type='hidden']")[0];

            idsForCmd.push(idInput.value);
        });

        document.getElementById("m_RestraintIds").value = idsForCmd.join(";");

        idsForCmd.length = 0;

        $("#m_customVariables .batch:checkbox:checked").each(function (){
            var idInput = $(this).closest('tr').find("input[type='hidden']")[0];

            idsForCmd.push(idInput.value);
        });

        document.getElementById("m_TriggerIds").value = idsForCmd.join(";");
    }

    function onBatchOperation(cmd) {
        if (!confirm(cmd + " selected item(s)?")) {
            return;
        }

        getCheckedIdsForBatchOperation();
        document.getElementById("m_hiddenCmd").value = "batch" + cmd;

        performPostback();
    }

    function getSelectedParamterNames() {
        var names = [];
        $("#parameter-names .batch:checkbox:checked").each(function (){
            var idInput = $(this).closest('tr').find("input[type='hidden']")[0];

            names.push(this.value);
        });

        return names.join(";");;
    }

    function performBatchDeleteParameters() {
        if (false && !confirm("Batch delete selected parameter(s)?")) {
            return;
        }

        $j.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: 'EditWorkflowConfiguration.aspx/DeleteParametersValidate',
            data: JSON.stringify({ 'brokerId': g_brokerId, 'draftMode':isDraft.value
                                    , 'conditionIds':document.getElementById("m_ConditionAndFieldIds").value                                            
                                    , 'constraintIds': document.getElementById("m_ConstraintIds").value
                                    , 'restraintIds':document.getElementById("m_RestraintIds").value
                                    , 'triggerIds':document.getElementById("m_TriggerIds").value
                                    , 'removedParameterNames' : getSelectedParamterNames()
            }),
            dataType: 'json',
            async: false,
            success: function (d) {
                if(d.d.ErrorMsg ) {
                    alert( d.d.ErrorMsg );
                    return;
                }

                document.getElementById("m_paraNames").value = getSelectedParamterNames();
                document.getElementById("m_hiddenCmd").value = "deleteParameters";
                performPostback();                    
            },
            error: function (xhr, status, error) {
                var exception = JSON.parse(xhr.responseText);
                alert("Cannot batch delete parameters. The error message: '" + exception.Message + "'");
            }
        });
    }

    function onBatchDeleteParameters() {
        $('#parameter-names').empty();

        getCheckedIdsForBatchOperation();

        $j.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: 'EditWorkflowConfiguration.aspx/GetParameterNamesFromRules',
            data: JSON.stringify({ 'brokerId': g_brokerId, 'draftMode':isDraft.value
                                    , 'conditionIds':document.getElementById("m_ConditionAndFieldIds").value                                            
                                    , 'constraintIds': document.getElementById("m_ConstraintIds").value
                                    , 'restraintIds':document.getElementById("m_RestraintIds").value
                                    , 'triggerIds':document.getElementById("m_TriggerIds").value
            }),
            dataType: 'json',
            async: false,
            success: function (d) {
                var i, items = d.d, item;
                for(i=0; i < items.length; i++) {
                    item = items[i];
                    item.disabled = item.key.indexOf(';') >= 0 ? "disabled" : "";
                    if( item.desc == '') {
                        item.desc = item.key;
                    } 
                    else if( item.key != item.desc) {
                        item.desc = item.desc + '&nbsp;&nbsp;&nbsp; (' + item.key + ')';
                    }
                }

                $('#tmplParaName').tmpl(d.d).appendTo("#parameter-names");    
                Modal.ShowPopup('BatchDeleteParametersDiv', null, null, 200, 200);
            },
            error: function (xhr, status, error) {
                var exception = JSON.parse(xhr.responseText);
                alert("Cannot retrieve the parameters information. The error message: '" + exception.Message + "'");
            }
        });


    }

    function displayNotes(guid, notes) {
        notes = notes.length === 0 ? "(No notes)" : notes;

        var displayText = "Notes: " + notes + "\n\nRule GUID: " + guid + "\nPress Ctrl+C to copy.";

        alert(displayText);
    }
</script>

<table class="align-left">
    <tr>
        <td style="background-color:#0000FF" class="Header">
            Workflow Configuration
        </td>
    </tr>
    <tr>
        <td>
            <input type="button" id="m_importBtn" style="display:none" value="Import from other account" onclick="showModal('/LOAdmin/Broker/Workflow/ImportWorkflowConfigurationFromOtherAccount.aspx?editingBrokerId=' + g_brokerId, null, null, null, performPostback);" />
            <input type="button" id="m_viewAuditBtn" value="View Audit" runat="server" onclick="onViewAudit();" />
            <input type="button" id="m_createDraft" value="Create draft" onclick="onCreateDraft();" />
            <input type="button" id="m_openDraft" value="Open draft" onclick="onOpenDraft();" runat="server" />
            <input type="button" id="m_exportXMLBtn" value="Export XML" runat="server" onclick="onExportXML()" />
            <asp:Button ID="ExportReadWrite" Text="Export Read/Write Privileges/Roles" runat="server" OnClick="OnExportExcel_BrokerReadWriteAccess" Width="200" />
            <a href="#" id="toggleCustomVarsLink" onclick="toggleCustomVars();">Triggers</a>
            <a href="#" id="branchGroupsLink" onclick="show('/LOAdmin/Broker/BranchGroups.aspx', 'Modal', performPostback);">Branch Groups</a>
			<a href="#" id="pmlBrokerGroupsLink" onclick="show('/LOAdmin/Broker/PmlBrokerGroups.aspx', 'Modal', performPostback);">Originating Company Groups</a>
			<a href="#" id="employeeGroupsLink" onclick="show('/LOAdmin/Broker/EmployeeGroups.aspx', 'Modal', performPostback);">Employee Groups</a>
			<a href="#" id="comparisonToolLink" onclick="show('/LOAdmin/Broker/Workflow/WorkflowConfigComparison.aspx', 'NonModal');">Comparison Tool</a>
			<a href="#" id="displayCompileCodeLink" onclick="show('/LOAdmin/Broker/Workflow/DisplayCompileEngineCode.aspx', 'NonModal');">Display Compile Engine Code</a>
        </td>
    </tr>
    <tr>
        <td>
            <div id="DraftDiv">
                <asp:FileUpload id="m_inputFile" Width="380" runat="server" style="display:none" />
                <asp:Button ID="m_importXMLBtn" Text="Import XML" runat="server" OnClick="OnImportXMLClicked" style="display:none" />
                <input id="BatchCopyButton" type="button" value="Batch Copy" onclick="onBatchOperation('Copy');" style="display: none;" disabled="disabled" />
                <input id="BatchDeleteButton" type="button" value="Batch Delete" onclick="onBatchOperation('Delete');" style="display: none;" disabled="disabled" />
                <input id="BatchDisableButton" type="button" value="Batch Disable" onclick="onBatchOperation('Disable');" style="display: none;" disabled="disabled" />
                <input id="BatchDeleteParameterButton" type="button" value="Delete Parameters" onclick="onBatchDeleteParameters(event);" style="display: none;" disabled="disabled" />
            </div>
                    
            <asp:Panel ID="SearchDiv" runat="server" DefaultButton="SearchButton">
                <asp:TextBox ID="SearchTextbox" runat="server" Width="200px" ></asp:TextBox>
                <asp:Button ID="SearchButton" runat="server" Text="Search" OnClick="SearchButton_Click" />
                <asp:Button ID="ClearSearchButton" runat="server" Text="Clear" OnClick="ClearSearchButton_Click" />
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td align="center" style="color:Red">
            <ml:PassthroughLiteral ID="m_errorMsg" runat="server"></ml:PassthroughLiteral>
        </td>
    </tr>

    <tbody id="m_conditions">
        <tr>
            <td>
                <a href="#" onclick="Modal.ShowPopup('FiltersDiv', null, event);">Filter privileges and restraints</a>
            </td>
        </tr>

        <tr>
            <td style="background-color:#00BB00" class="Header">
                Privileges - All privileges that do not explicitly include Read Loan operation only apply to users that otherwise have Read Loan privilege
            </td>
        </tr>
        <tr class="ErrorCount" id="ConditionValidationTrHelper">
            <td>
                <a href="#" id="ConditionPrevConstraintError">Previous</a>&nbsp; &nbsp;
                <span id="ConditionErrorCount"></span>&nbsp; &nbsp; <a href="#" id="ConditionNextConstraintError">Next</a>
                &nbsp;&nbsp;&nbsp;&nbsp;<a href="#" id="ConditionValidationCancel">Cancel</a>
            </td>
        </tr>
        <tr>
            <td>
                <div id="m_conditionsContainer" style="overflow:scroll; width:1150px; height:600px">
                <asp:DataGrid runat="server" id="m_conditionsGrid" AutoGenerateColumns="true" OnItemDataBound="OnConditionsBound">
                    <headerstyle cssclass="StaticHeader ConditionsHeader" Wrap="true"></headerstyle>
                    <itemstyle cssclass="GridItem" Wrap="true"></itemstyle>
                    <alternatingitemstyle cssclass="GridAlternatingItem" Wrap="true"></alternatingitemstyle>
                    <columns>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <input id="batch" class="batch" runat="server" type="checkbox" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <input type="hidden" runat="server" id="id" value=<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Id").ToString())%> />
                                <a href="#" onclick="displayNotes(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Notes").ToString())%>);return false;">notes</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <a href="#" onclick="onEdit('Condition', <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>);">edit</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <a href="#" onclick="onDelete('Condition', <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>, 'm_conditionsGrid');">delete</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <a href="#" onclick="onCopy('Condition', <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>, 'm_conditionsGrid');">copy</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </columns>
                </asp:DataGrid>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <input id="SelectAllConditionsButton" type="button" style="display:none;" value="Select All" onclick="BatchSelect('m_conditionsContainer', true)" />
                <input id="SelectNoneConditionsButton" type="button" style="display:none;" value="Select None" onclick="BatchSelect('m_conditionsContainer', false)" />
                <input runat="server" type="button" id="m_addPrivilegeBtn" style="display:none" value="Add Privilege" onclick="onAdd('Condition');" />
                <asp:Button runat="server"  ID="m_BtnConditionValidate" OnClick="OnConditionValidateClick" Text="Validate Privileges" />
                <asp:Button ID="m_exportCondition" Text="Export Privileges" runat="server" OnClick="OnExportConditions" />
            </td>
        </tr>
        <tr>
            <td>
                *Exclude: exclude from "Export Read/Write Privileges/Roles"
            </td>
        </tr>
    </tbody>

    <tbody  id="m_constraints" runat="server">
        <tr>
            <td style="background-color:#BB0000" class="Header">
                Constraints
            </td>
        </tr>
            <tr class="ErrorCount" id="ConstraintValidationTrHelper">
            <td>
                <a href="#" id="ConstraintPrevError">Previous</a>&nbsp; &nbsp;
                <span id="ConstraintErrorCount"></span>&nbsp; &nbsp; <a href="#" id="ConstraintNextError">Next</a>
                &nbsp;&nbsp;&nbsp;&nbsp;<a href="#" id="ConstraintValidationCancel">Cancel</a>
            </td>
        </tr>
        <tr>
            <td>
                <div id="m_constraintsContainer" style="overflow:scroll; width:1150px; height:600px">
                <asp:DataGrid runat="server" id="m_constraintsGrid" AutoGenerateColumns="true" OnItemDataBound="OnConstraintsBound">
                    <headerstyle cssclass="StaticHeader ConstraintsHeader"></headerstyle>
                    <itemstyle cssclass="GridItem"></itemstyle>
                    <alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
                    <columns>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <input id="batch" class="batch" runat="server" type="checkbox" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                    <input type="hidden" runat="server" id="id" value=<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Id").ToString())%> />
                                    <a href="#" onclick="displayNotes(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Notes").ToString())%>);return false;">notes</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <a href="#" onclick="onEdit('Constraint', <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>);">edit</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <a href="#" onclick="onDelete('Constraint', <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>, 'm_constraintsGrid');">delete</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <a href="#" onclick="onCopy('Constraint', <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>, 'm_constraintsGrid');">copy</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </columns>
                </asp:DataGrid>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <input id="SelectAllConstraintsButton" type="button" style="display:none;" value="Select All" onclick="BatchSelect('m_constraintsContainer', true)" />
                <input id="SelectNoneConstraintsButton" type="button" style="display:none;" value="Select None" onclick="BatchSelect('m_constraintsContainer', false)" />
                <input type="button" id="m_addConstraintBtn" style="display:none" value="Add Constraints" onclick="onAdd('Constraint')"; />
                <asp:Button runat="server"  ID="m_btnConstraint" OnClick="OnConstraintValidateClick" Text="Validate By Constraints" />
                <asp:Button ID="Button1" Text="Export Constraints" runat="server" OnClick="OnExportConstraints" />
            </td>
        </tr>
    </tbody>

    <tbody id="m_restraints">
        <tr>
            <td class="Header RedHeader">
                Restraints - These rules take precedence in workflow evaluation - If these rules evaluate to true, the user will not be able to execute a given operation
            </td>
        </tr>
        <tr class="ErrorCount" id="RestraintValidationTrHelper">
            <td>
                <a href="#" id="RestraintPrevError">Previous</a>&nbsp; &nbsp;
                <span id="RestraintErrorCount"></span>&nbsp; &nbsp; <a href="#" id="RestraintNextError">Next</a>
                &nbsp;&nbsp;&nbsp;&nbsp;<a href="#" id="RestraintValidationCancel">Cancel</a>
            </td>
        </tr>
        <tr>
            <td>
                <div id="m_restraintsContainer" style="overflow:scroll; width:1150px; height:600px">
                <asp:DataGrid runat="server" id="m_restraintsGrid" AutoGenerateColumns="true" OnItemDataBound="OnRestraintsBound">
                    <headerstyle cssclass="StaticHeader RestraintsHeader" Wrap="true"></headerstyle>
                    <itemstyle cssclass="GridItem" Wrap="true"></itemstyle>
                    <alternatingitemstyle cssclass="GridAlternatingItem" Wrap="true"></alternatingitemstyle>
                    <columns>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <input id="batch" class="batch" runat="server" type="checkbox" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <input type="hidden" runat="server" id="id" value=<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Id").ToString())%> />
                                <a href="#" onclick="displayNotes(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Notes").ToString())%>);return false;">notes</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <a href="#" onclick="onEdit('Restraint', <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>);">edit</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <a href="#" onclick="onDelete('Restraint', <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>, 'm_restraintsGrid');">delete</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <a href="#" onclick="onCopy('Restraint', <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>, 'm_restraintsGrid');">copy</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </columns>
                </asp:DataGrid>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <input id="SelectAllRestraintsButton" type="button" style="display:none;" value="Select All" onclick="BatchSelect('m_restraintsContainer', true)" />
                <input id="SelectNoneRestraintsButton" type="button" style="display:none;" value="Select None" onclick="BatchSelect('m_restraintsContainer', false)" />
                <input runat="server" type="button" id="m_addRestraintBtn" style="display:none" value="Add Restraint" onclick="onAdd('Restraint');" />
                <asp:Button runat="server"  ID="m_BtnRestraintValidate" OnClick="OnRestraintValidateClick" Text="Validate Restraints" />
                <asp:Button ID="m_exportRestraint" Text="Export Restraints" runat="server" OnClick="OnExportRestraints" />
            </td>
        </tr>
    </tbody>

    <tbody id="m_fieldProtectionRules" runat="server">
        <tr>
            <td style="background-color:#00BB00" class="Header">
                Field Protection Rules
            </td>
        </tr>
        <tr>
            <td>
                <div id="m_fieldProtectionRulesContainer" style="overflow:scroll; width:1150px; height:600px">
                <asp:DataGrid runat="server" id="m_fieldProtectionRulesGrid" AutoGenerateColumns="true" OnItemDataBound="OnFieldProtectionRulesBound">
                    <headerstyle cssclass="StaticHeader ProtectionRulesHeader"></headerstyle>
                    <itemstyle cssclass="GridItem"></itemstyle>
                    <alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
                    <columns>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <input id="batch" class="batch" runat="server" type="checkbox" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <input type="hidden" runat="server" id="id" value='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>' />
                                <a href="#" onclick="displayNotes(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Notes").ToString())%>);return false;">notes</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <a href="#" onclick="onEditFieldProtectionRule('FieldProtectionRule', <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>);">edit</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <a href="#" onclick="onDelete('FieldProtectionRule', <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>, 'm_fieldProtectionRulesGrid');">delete</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <a href="#" onclick="onCopy('FieldProtectionRule', <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>, 'm_fieldProtectionRulesGrid');">copy</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </columns>
                </asp:DataGrid>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <input id="SelectAllFieldsButton" type="button" style="display:none;" value="Select All" onclick="BatchSelect('m_fieldProtectionRules', true)" />
                <input id="SelectNoneFieldsButton" type="button" style="display:none;" value="Select None" onclick="BatchSelect('m_fieldProtectionRules', false)" />
                <input type="button" id="m_addFieldProtectionRuleBtn" style="display:none;" value="Add Field Protection Rule" onclick="onAddFieldProtectionRule()" />
                <asp:Button ID="m_exportFieldProtectionRules" Text="Export Field Protection Rules" runat="server" OnClick="OnExportFieldProtectionRules" />
            </td>
        </tr>
    </tbody>

    <tbody id="m_customVariables" style="display:none">
        <tr>
            <td style="background-color:#00BB00" class="Header">
                Triggers
            </td>
        </tr>
        <asp:Repeater runat="server" ID="m_customVariableSet" OnItemDataBound="OnCustomVariableBound">
            <ItemTemplate>
                <tr>
                    <td>
                        <div style="overflow-x:scroll; width:920px;">
                        <asp:DataGrid ID="m_customVariableGrid" runat="server" AutoGenerateColumns="true" OnItemCreated="OnCustomVariableGridBinding">
                            <headerstyle cssclass="GridHeader"></headerstyle>
                            <itemstyle cssclass="GridItem"></itemstyle>
                            <columns>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <input id="batch" class="batch" runat="server" type="checkbox"  />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <input type="hidden" runat="server" id="id" value='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>' />
                                        <a href="#" onclick="displayNotes(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Notes").ToString())%>);return false;">notes</a>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <a href="#" onclick="onEditCustomVar('CustomVariable', <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>);">edit</a>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <a href="#" onclick="onCustomVarOperation('Delete', <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Name").ToString())%>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>);">delete</a>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <a href="#" onclick="onCustomVarOperation('Copy', <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Name").ToString())%>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>);">copy</a>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </columns>
                        </asp:DataGrid>
                        </div>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        <tr>
            <td>
                <input id="SelectAllTriggerButton" type="button" style="display:none;" value="Select All" onclick="BatchSelect('m_customVariables', true)" />
                <input id="SelectNoneTriggerButton" type="button" style="display:none;" value="Select None" onclick="BatchSelect('m_customVariables', false)" />
                <input type="button" id="m_addTriggerBtn" value="Add Trigger" onclick="onAddCustomVar('CustomVariable')" />
                <asp:Button ID="m_exportTriggers" Text="Export Triggers" runat="server" OnClick="OnExportTriggers" />
            </td>
        </tr>
    </tbody>
    <tbody id="m_otherPermissions">
        <tr>
            <td style="background-color:#666666" class="Header">
                Other Permissions
            </td>
        </tr>
        <tr>
            <td>
                <asp:checkbox id="m_isRateLockedAtSubmission" runat="server" Text="Is rate locked at submission" />
                ( Default:&nbsp;
                <asp:radiobuttonlist id="m_pmlSubmitRLockDefaultT" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                    <asp:ListItem Value="1">Register</asp:ListItem>
                    <asp:ListItem Selected="True" Value="2">Lock</asp:ListItem>
                </asp:radiobuttonlist>
                )
            </td>
        </tr>
        <tr id="isPmlSubmissionAllowedRow">
            <td>
                <asp:checkbox id="m_isPmlSubmissionAllowed" Text="Is PML registration allowed (For Citi)?" Checked="True" Runat="server" />
            </td>
        </tr>
    </tbody>
    <tr>
        <td align="center" style="color:Red">
            <ml:PassthroughLiteral ID="m_saveErrorMsg" runat="server"></ml:PassthroughLiteral>
        </td>
    </tr>
    <tr>
        <td align="center">
            <input type="button" value="Save Permissions" id="m_btnSave" onclick="onSaveClicked();" />
            <input type="button" value="Cancel" onclick="window.close();" />
        </td>
    </tr>
</table>

<div id="FiltersDiv" class="modal">
    <table class="align-left">
        <tr>
            <td style="background-color:#666666" class="Header" colspan="3">
                Roles
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="m_rolesAll" runat="server" Text="All" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CheckBoxList ID="m_rolesList" runat="server" RepeatLayout="Table" RepeatColumns="4"></asp:CheckBoxList>
            </td>
        </tr>
        <tr>
            <td style="background-color:#666666" class="Header" colspan="3">
                Operations
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:HiddenField runat="server" id="DataSourceJson" />
                <div ng-app="app">
                    <workflow-operations><p>Loading&hellip;</p></workflow-operations>
                </div>
            </td>
        </tr>
    </table>
    <input type="button" value="OK" onclick="Modal.Hide(); onFilter();" />
</div>

<div id="BatchDeleteParametersDiv" class="modal">
    <table class="align-left">
        <tr>
            <td style="background-color:#666666" class="Header" colspan="3">
                Batch Delete Parameters
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <ul id="parameter-names">
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <input type="button" value="Cancel" onclick="Modal.Hide();" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="button" value="Delete" onclick="performBatchDeleteParameters();" />
</div>

<script type="text/javascript">
    function showRowsById(tableId, toShow) {
        var table = document.getElementById(tableId);
        var rows = table.tBodies[0].rows;

        for( var i = 0; i < rows.length; i++) {
            var row = rows[i];
            var inputs = row.cells[0].getElementsByTagName('input');
            if( inputs.length == 0 ) {
                continue;
            }
            var id = inputs[0].value;
            if( contains(toShow, id) ) {
                row.style.display = '';
            }
            else {
                row.style.display = 'none';
            }
        }
    }

    function resetRows(tableId) {
        var table = document.getElementById(tableId);
        var rows = table.tBodies[0].rows;

        for( var i = 0; i < rows.length; i++) {
            rows[i].style.display = '';
        }

    }


    function contains(a, obj) {
        var i = a.length;
        while (i--) {
        if (a[i] === obj) {
            return true;
        }
        }
        return false;
    }

    function showIfPresent(buttonId) {
        var btnEl = document.getElementById(buttonId);

        if (btnEl !== null) {
            btnEl.style.display = '';
        }
    }
    var hasConstraints;
    $(document).ready(function() {
        var constraints = $('#m_constraints') ;
        hasConstraints = constraints.find('tr.GridItem, tr.GridAlternatingItem').length > 0;
        constraints.toggle(hasConstraints);

        setCustomVarMode();
        conflictMode = false;

        $(".batch:checkbox").on("change", function() {
            var closestRow = $(this).closest("tr")[0];

            if ($(this).is(':checked')){
                closestRow.className = 'GridSelectedItem';

                // So long as at least one checkbox is checked, we want to 
                // enable the batch buttons.
                document.getElementById("BatchCopyButton").disabled = '';
                document.getElementById("BatchDeleteButton").disabled = '';
                document.getElementById("BatchDisableButton").disabled = '';
                document.getElementById("BatchDeleteParameterButton").disabled = '';
            }
            else {
                if(closestRow.rowIndex % 2 === 1) {
                    closestRow.className = 'GridItem';
                }
                else {
                    closestRow.className = 'GridAlternatingItem';
                }

                if ($(".batch:checkbox:checked").size() === 0) {
                    document.getElementById("BatchCopyButton").disabled = 'disabled';
                    document.getElementById("BatchDeleteButton").disabled = 'disabled';
                    document.getElementById("BatchDisableButton").disabled = 'disabled';
                    document.getElementById("BatchDeleteParameterButton").disabled = 'disabled';
                }
            }
        });

        if (document.getElementById('m_inDraftMode').value == "True") {
            document.getElementById("m_openDraft").style.display = 'none';
            document.getElementById("m_createDraft").style.display = 'none';
            document.getElementById("m_otherPermissions").style.display = 'none';
            document.getElementById("m_btnSave").value = 'Release';

            if (ML.canSave) {
                document.getElementById("m_inputFile").style.display = '';
                document.getElementById("m_importXMLBtn").style.display = '';
                document.getElementById("BatchCopyButton").style.display = '';
                document.getElementById("BatchDeleteButton").style.display = '';
                document.getElementById("BatchDisableButton").style.display = '';
                document.getElementById("BatchDeleteParameterButton").style.display = '';
                document.getElementById("m_importBtn").style.display = '';
                document.getElementById("m_addPrivilegeBtn").style.display = '';
                document.getElementById("m_addConstraintBtn").style.display = '';
                document.getElementById("m_addRestraintBtn").style.display = '';

                document.getElementById("SelectAllConditionsButton").style.display = '';
                document.getElementById("SelectNoneConditionsButton").style.display = '';
                document.getElementById("SelectAllConstraintsButton").style.display = '';
                document.getElementById("SelectNoneConstraintsButton").style.display = '';
                document.getElementById("SelectAllRestraintsButton").style.display = '';
                document.getElementById("SelectNoneRestraintsButton").style.display = '';
                document.getElementById("SelectAllTriggerButton").style.display = '';
                document.getElementById("SelectNoneTriggerButton").style.display = '';

                showIfPresent("SelectAllFieldsButton");
                showIfPresent("SelectNoneFieldsButton");
                showIfPresent("m_addFieldProtectionRuleBtn");
            }
        }

        if (g_brokerId == "00000000-0000-0000-0000-000000000000") {
            document.getElementById("m_otherPermissions").style.display = 'none';
            document.getElementById("m_btnSave").style.display = 'none';
        }

        // OPM 468635 - Set visibility for LOS/LOAdmin access.
        $("#m_addTriggerBtn").toggle(document.getElementById('m_inDraftMode').value == "True" && ML.canSave);
        $("#m_importBtn").toggle(document.getElementById('m_inDraftMode').value == "True" && ML.canSave && ML.IsInternal);
        $("#m_viewAuditBtn").toggle(ML.IsInternal);

        $("#branchGroupsLink").toggle(ML.IsInternal);
        $("#pmlBrokerGroupsLink").toggle(ML.IsInternal);
        $("#employeeGroupsLink").toggle(ML.IsInternal);
        $("#comparisonToolLink").toggle(ML.IsInternal);
        $("#displayCompileCodeLink").toggle(ML.IsInternal);
        $("#isPmlSubmissionAllowedRow").toggle(ML.IsInternal);

        document.getElementById("m_hiddenNotes").value = '';
        document.getElementById("m_hiddenCmd").value = '';

        var currentIndex = 0;
        var maxIndex = -1;
        var currentData;

        var validationData;

        var validationFields;
        var conditionValidationFields = [
            '#ConditionValidationTrHelper',
            '#ConditionPrevConstraintError',
            '#ConditionNextConstraintError',
            '#ConditionErrorCount',
            '#ConditionValidationCancel'
        ];

        var constraintValidationFields = [
            '#ConstraintValidationTrHelper',
            '#ConstraintPrevError',
            '#ConstraintNextError',
            '#ConstraintErrorCount',
            '#ConstraintValidationCancel'
        ];

        var restraintValidationFields = [
                '#RestraintValidationTrHelper',
                '#RestraintPrevError',
                '#RestraintNextError',
                '#RestraintErrorCount',
                '#RestraintValidationCancel'
        ];

        if ((typeof (ConditionValidationData) != 'undefined' && ConditionValidationData.length == 0) ||
            (typeof (ConstraintValidationData) != 'undefined' && ConstraintValidationData.length == 0) ||
            (typeof (RestraintValidationData) != 'undefined' && RestraintValidationData.length == 0)) {
            alert("No conflicts detected.");
        }

        if (typeof (ConditionValidationData) != 'undefined' && ConditionValidationData.length > 0) {
            validationFields = conditionValidationFields;
            validationData = ConditionValidationData;
            $(constraintValidationFields[0]).hide();
            conflictMode = true;
        } else if (typeof (ConstraintValidationData) != 'undefined' && ConstraintValidationData.length > 0) {
            validationFields = constraintValidationFields;
            validationData = ConstraintValidationData;
            $(conditionValidationFields[0]).hide();
            conflictMode = true;
        } else if (typeof (RestraintValidationData) != 'undefined' && RestraintValidationData.length > 0) {
            validationFields = restraintValidationFields;
            validationData = RestraintValidationData;
            $(restraintValidationFields[0]).hide();
            conflictMode = true;
        } else {
            $(constraintValidationFields[0]).hide();
            $(conditionValidationFields[0]).hide();
            $(restraintValidationFields[0]).hide();
            return;
        }

        maxIndex = validationData.length;
        setDisabledAttr($(validationFields[1]), true);

        if (maxIndex == 1) {
            setDisabledAttr($(validationFields[2]), true);
        }

        showConstraint(currentIndex);

        $(validationFields[3]).text('Conflict ' + (currentIndex + 1) + ' out of ' + maxIndex + '.');

        $(validationFields[1]).on("click", function() {
            if (this.disabled || currentIndex == 0 || hasDisabledAttr(this)) {
                setDisabledAttr(this, true);
                return;
            }

            currentIndex -= 1;
            showConstraint(currentIndex);
            if (currentIndex == 0) {
                setDisabledAttr(this, true);
            }
            setDisabledAttr($(validationFields[2]), false);

            updateErrorText();
        });
        $(validationFields[4]).on("click", function() {
            conflictMode = false;
            $(validationFields[0]).hide();
            performPostback(); // postback so we can refilter when they press cancel
        });
        $(validationFields[2]).on("click", function() {
            if (this.disabled || currentIndex == maxIndex || hasDisabledAttr(this)) {
                setDisabledAttr(this, true);
                return;
            }

            currentIndex += 1;
            showConstraint(currentIndex);
            if (currentIndex == maxIndex - 1) {
                setDisabledAttr(this, true);
            }
            setDisabledAttr($(validationFields[1]), false);
            updateErrorText();
        });


        function updateErrorText() {
            $(validationFields[3]).text('Conflict ' + (currentIndex + 1) + ' out of ' + maxIndex + '.');
        }

        // OPM 220722 - TODO: ADD VALIDATION FOR RESTRAINTS.
        function showConstraint() {
            var toShow = new Array();
            toShow.push(validationData[currentIndex]['Key']);
            if (typeof (ConditionValidationData) != 'undefined') {
                showRowsById('<%= AspxTools.ClientId(m_conditionsGrid) %>', toShow);
                showRowsById('<%= AspxTools.ClientId(m_constraintsGrid) %>', validationData[currentIndex]['Value']);
            } else {
                showRowsById('<%= AspxTools.ClientId(m_constraintsGrid) %>', toShow);
                showRowsById('<%= AspxTools.ClientId(m_conditionsGrid) %>', validationData[currentIndex]['Value']);
            }
        }
    });
</script>
