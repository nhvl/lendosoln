﻿#region auto-generated code
namespace LendersOfficeApp.common.Workflow
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.Security;
    using LendersOffice.Drivers.Gateways;
    using ConfigSystem.DataAccess;

    /// <summary>
    /// Workflow change history page.
    /// </summary>
    public partial class WorkflowChangeHistory : UserControl
    {
        /// <summary>
        /// Gets the Broker Id.
        /// </summary>
        private Guid BrokerId
        {
            get
            {
                if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
                {
                    return RequestHelper.GetGuid("brokerid", Guid.Empty);
                }

                return PrincipalFactory.CurrentPrincipal.BrokerId;
            }
        }

        /// <summary>
        /// Page load function.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">EventArgs e.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request["cmd"] == "download")
            {
                this.DownloadXML(long.Parse(this.Request["configid"]));
            }

            if (!Page.IsPostBack)
            {
                ((BasePage)this.Page).RegisterJsScript("jquery.tmpl.js");
                ((BaseServicePage)this.Page).RegisterService("workflowAudits", "/common/Workflow/WorkflowChangeHistoryService.aspx");
                ((BasePage)this.Page).RegisterJsGlobalVariables("brokerId", this.BrokerId);
                ((BasePage)this.Page).RegisterJsGlobalVariables("isInternal", PrincipalFactory.CurrentPrincipal is InternalUserPrincipal);

                List<WorkflowChangeAudit> audits = WorkflowChangeAudit.ListAllWorkflowChangeAuditsForBroker(this.BrokerId);

                IConfigRepository configRepository = ConfigHandler.GetRepository(this.BrokerId);

                ((BasePage)this.Page).RegisterJsGlobalVariables("currentConfigId", configRepository.GetActiveReleaseId(this.BrokerId)?.ToString() ?? string.Empty);

                ((BasePage)this.Page).RegisterJsObjectWithJsonNetSerializer("audits", audits);
            }
        }

        /// <summary>
        /// Download the selected workflow configuration XML.
        /// </summary>
        /// <param name="configId">Configuration id.</param>
        private void DownloadXML(long configId)
        {
            try
            {
                string xml = WorkflowChangeAudit.RetrieveWorkflowConfigXMLByConfigId(BrokerId, configId);

                string path = TempFileUtils.NewTempFilePath() + ".xml";
                TextFileHelper.WriteString(path, xml, false);
                RequestHelper.SendFileToClient(HttpContext.Current, path, "application/download", "WorkflowConfiguration.xml");
                Response.End();
            }
            catch (CBaseException e)
            {
                Tools.LogError("Error while downloading XML.", e);

                this.Page.ClientScript.RegisterStartupScript(
                    this.GetType(),
                    Guid.NewGuid().ToString().Replace("-", string.Empty),
                    "<script type='text/javascript'>errorDownloadingXml(\"Error downloading Workflow Configuration XML.\");</script>");
            }
        }        
    }
}