﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditWorkflowConfigItemControl.ascx.cs" Inherits="LendersOfficeApp.common.Workflow.EditWorkflowConfigItemControl" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<input type="hidden" value="" runat="server" id="m_hiddenId" />
<input type="hidden" value="" runat="server" id="m_hiddenAction" />
<input type="hidden" value="" runat="server" id="m_hiddenParameterName" />
<input type="hidden" value="" runat="server" id="m_hiddenVariableName" />
<input type="hidden" value="" runat="server" id="m_hiddenCmd" />
<input type="hidden" value="" runat="server" id="m_hiddenEditorMode" />
<input type="hidden" value="" runat="server" id="m_hiddenDraftMode" />
<input type="hidden" value="" runat="server" id="m_hiddenKey" />
<input type="hidden" value="" runat="server" id="PathableFieldPermissionsEditor_Path" />
<input type="hidden" value="" runat="server" id="PathableFieldPermissionsEditor_FriendlyPath" />
<script type="text/javascript">
        var isCondition = <%= AspxTools.JsBool(isCondition) %>;
    $(document).on("click", ".loanStatusCheckbox", validateButtons);
    function validateButtons() {
        var hasOps = $('.selected-operations .operations .operation-item').length > 0;
        var isAdd = <%= AspxTools.JsBool(isAdd) %>;
        var isRestraint = <%= AspxTools.JsBool(isRestraint) %>;
        var failureMessage = $('#m_failureMessage').val().trim();
        var isLoanStatusChecklistVisible = $("#LoanStatusChecklistSection:visible").length > 0;
        var hasCheckedLoanStatus = $(".loanStatusCheckbox:checked").length > 0;

        $('#m_saveItemBtn').prop('disabled', !hasOps || isAdd || (isRestraint && !failureMessage) || (isLoanStatusChecklistVisible && !hasCheckedLoanStatus));
    }

    function validateOperations(postback, isWriteSelected, isLoanStatusChangeSelected) {
        if (isWriteSelected) {
            $('#m_writableFieldsSection').show();
            $('#pathableFieldPermissionsSection').show();
        } else {
            $('#m_writableFieldsSection').hide();
            $('#pathableFieldPermissionsSection').hide();
        }

        $('#LoanStatusChecklistSection').toggle(isLoanStatusChangeSelected);

        if (postback)
            performPostback();
    }

    function triggerParentPostback()
    {
        if (window && window.opener && typeof (window.opener.performPostback) == "function") {
            window.opener.performPostback();
        }
    }

    function _initControl() {
            
        // Determine what to display based on what operation has been set
        //   from a previous postback
        processHiddenCmd();

        document.getElementById('m_hiddenCmd').value = '';
        resize(1000, 700);


        var isRestraint = <%= AspxTools.JsBool(isRestraint) %>;
        $('#imgDenialMessageRequired').toggle(isRestraint);

        if(isRestraint){
            $('#tdPageHeader').addClass('RedHeader');
        } else {
            $('#tdPageHeader').addClass('GreenHeader');
        }

        var writableFieldsSelectionList = createSelectionList(
            $('#writableFieldsSearchCriteria'),
            $('#m_writableFieldsCategories'),
            $('#m_writableFieldsPage'),
            $('#m_writableFieldsListContainer'));
        $('#m_enableWritableFieldsSearch').on("click", function() {
            defaultCategory(writableFieldsSelectionList);
            displaySearchResults(false, writableFieldsSelectionList);
        });

        $('#writableFieldsSearchCriteria').on("focus", function() {
            defaultCategory(writableFieldsSelectionList);
            $('#m_enableWritableFieldsSearch').prop('checked', true);
        }).on("keyup", function() {
            $('#m_enableWritableFieldsSearch').prop('checked', true);
            displaySearchResults(false, writableFieldsSelectionList);
        });

        $('#m_disableProtectedFieldsSearch').on("click", function() {
            displaySearchResults(true, writableFieldsSelectionList);
        });

        $('#m_writableFieldsCategories').on("change", function() {
            $('#m_disableProtectedFieldsSearch').prop('checked', true);
            jumpToSelected(writableFieldsSelectionList);
        });

        $('#m_writableFieldsPage').on("change", function() {
            $('#m_searchWritableFieldsPage').prop('checked', true);
            jumpToPage(writableFieldsSelectionList);
        });

        //
        var parameterSelectionList = createSelectionList(
            $('#searchCriteria'),
            $('#m_Group'),
            $('#m_Page'),
            $('#m_FieldList'));

        $('#m_enableSearch').on("click", function() {
            defaultCategory(parameterSelectionList);
            displaySearchResults(false, parameterSelectionList);
        });

        $('#searchCriteria').on("focus", function() {
            defaultCategory(parameterSelectionList);
            $('#m_enableSearch').prop('checked', true);
        }).on("keyup", function() {
            $('#m_enableSearch').prop('checked', true);
            displaySearchResults(false, parameterSelectionList);
        });

        $('#m_disableSearch').on("click", function() {
            displaySearchResults(true, parameterSelectionList);
        });

        $('#m_Group').on("change", function() {
            $('#m_disableSearch').prop('checked', true);
            jumpToSelected(parameterSelectionList);
        });

        $('#m_Page').on("change", function() {
            $('#m_searchPage').prop('checked', true);
            jumpToPage(parameterSelectionList);
        });

        $('#m_failureMessage').change(validateButtons);

        $('.workflow-operations .buttons-container button').change(validateButtons);

        if( isCondition ) {
            $('#excludeFromRwExport').show();
        } else {
            $('#excludeFromRwExport').hide();
        }

        validateButtons();
    }
</script>
    
<table class="align-left">
    <tr>
        <td id="tdPageHeader" class="Header" colspan="4">
            <ml:EncodedLiteral ID="m_pageHeader" runat="server"></ml:EncodedLiteral>
        </td>
    </tr>
    <tbody id="m_operationSection" runat="server">
        <tr>
            <td style="background-color:#666666" class="Header" colspan="4">
                Operations
            </td>
        </tr>
        <tr>
            <td style="color:Red" colspan="4">
                Read loan is required for all operations
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:HiddenField runat="server" id="DataSourceJson" />
                <div ng-app="app">
                    <workflow-operations><p>Loading&hellip;</p></workflow-operations>
                </div>
            </td>
        </tr>
    </tbody>
    <tr>
        <td style="background-color:#666666" class="Header" colspan="4">
            Parameters
        </td>
    </tr>
    <asp:Repeater runat="server" ID="m_parameterSet" OnItemDataBound="OnParameterBound">
        <ItemTemplate>
            <tr class="GridItem">
                <td>
                    <ml:EncodedLiteral ID="m_parameterName" runat="server"></ml:EncodedLiteral>
                </td>
                <td id="ParameterValueCell" runat="server">
                    <ml:EncodedLiteral ID="m_parameterValue" runat="server"></ml:EncodedLiteral>                    
                </td>
                <td id="ParameterIncludeIfCell" runat="server">
                    Required for User Checklist: <ml:EncodedLiteral ID="ParameterIncludeIf" runat="server">No</ml:EncodedLiteral>
                </td>
                <td>
                    <a id="m_parameterEdit" runat="server" href="#">edit</a>
                </td>
            </tr>
               
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="GridAlternatingItem">
                <td>
                    <ml:EncodedLiteral ID="m_parameterName" runat="server"></ml:EncodedLiteral>
                </td>
                <td id="ParameterValueCell" runat="server">
                    <ml:EncodedLiteral ID="m_parameterValue" runat="server"></ml:EncodedLiteral>                    
                </td>
                <td id="ParameterIncludeIfCell" runat="server">
                    Required for User Checklist: <ml:EncodedLiteral ID="ParameterIncludeIf" runat="server">No</ml:EncodedLiteral>
                </td>
                <td>
                    <a id="m_parameterEdit" runat="server" href="#">edit</a>
                </td>
            </tr>
        </AlternatingItemTemplate>
    </asp:Repeater>
    <tr>
        <td colspan="4">
            <input type="button" id="m_addParameterBtn" value="Add Parameter" onclick="onAddParameter();" runat="server" />
        </td>
    </tr>
    <tbody id="m_writableFieldsSection" runat="server">
    <tr>
        <td style="background-color:#666666" class="Header" colspan="4">
            Writable Fields
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <div class="FieldList">
                <asp:GridView ID="m_writableFieldsListMainDisplay" CssClass="FieldListItem" runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="String Value">
                            <HeaderTemplate></HeaderTemplate>
                            <HeaderStyle CssClass="FieldListItem" />
                            <ItemStyle CssClass="FieldListItem" />
                            <ItemTemplate>
                                <%# AspxTools.HtmlString(Container.DataItem.ToString())%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <input type="button" id="m_editFieldsBtn" value="Edit Fields" runat="server" onclick="onPickWritableField();"/>
        </td>
    </tr>
    <tr>
        <td>
            <input type="checkbox" id="m_writableFieldsAreClearable" runat="server" />Writable dates and text fields can be cleared
        </td>
    </tr>
    </tbody>

    <tbody id="pathableFieldPermissionsSection" runat="server">
        <tr>
            <td id="pathableFieldPermissionsHeader" class="Header" colspan="4">
                Writable Collections
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div class="FieldList">
                    <asp:GridView ID="PathableFieldPermissionsList" CssClass="FieldListItem" runat="server" AutoGenerateColumns="false" OnRowDataBound="PathableFieldPermissionsList_RowBound" ShowHeader="false">
                        <Columns>
                            <asp:TemplateField>
                                <ItemStyle CssClass="FieldListItem" />
                                <ItemTemplate>
                                    <a id="removeLink" runat="server">remove</a>
                                    <input type="hidden" id="fieldId" runat="server" />
                                    <ml:EncodedLabel ID="friendlyName" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" id="btnEditCollections" value="Add Collections" runat="server" onclick="onPickWritableCollection();"/>
            </td>
        </tr>
    </tbody>


    <tbody id="LoanStatusChecklistSection" runat="server">
    <tr>
            <td id="LoanStatusChecklistHeader" class="Header" colspan="4">
                Allowable Loan Statuses
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div class="FieldList">
                    <asp:Repeater ID="LoanStatusChecklistRepeater" runat="server" OnItemDataBound="LoanStatusChecklistRepeater_DataBound">
                        <ItemTemplate>
                            <input type="checkbox" id="loanStatusCheckbox" runat="server" class="loanStatusCheckbox" />
                            <ml:EncodedLabel ID="loanStatusLabel" runat="server" AssociatedControlID="loanStatusCheckbox" />
                            <br />
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </td>
        </tr>
    </tbody>

    <tr>
        <td style="background-color:#666666" class="Header" colspan="2">
            Save Denial Message
            <img id="imgDenialMessageRequired" src="../../../images/require_icon.gif" />
        </td>
        <td style="background-color:#666666" class="Header" colspan="2">
            Notes
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:TextBox ID="m_failureMessage" Width="400px" Rows="6" runat="server" TextMode="MultiLine"></asp:TextBox>
        </td>
        <td colspan="2">
            <asp:TextBox ID="m_tbNotes" Width="400px" Rows="6" runat="server" TextMode="MultiLine"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="4" id="excludeFromRwExport">
            <asp:CheckBox id="m_chkExcludeRWExport" runat="server" Text="Exclude from read/write export." />
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <ml:PassthroughLabel ID="m_saveErrorMsg" runat="server" CssClass="RedText"></ml:PassthroughLabel>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <ml:EncodedLiteral ID="m_readLoanOperationMsg" runat="server" Text="Read loan is required for all operations"></ml:EncodedLiteral>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <input type="button" value="Save" onclick="onSaveConfigItem();" id="m_saveItemBtn" runat="server" class="saveItemBtn" />
            <input type="button" value="Cancel" onclick="window.close();" />
        </td>
    </tr>
</table>
    
<div id="WritableFieldsPopup" class="modal" runat="server">
    <table class="align-left" id="m_fieldPicker" runat="server" border="0" cellspacing="0" cellpading="2">
        <tr>
            <th class="FormTableHeader" colspan="2">
                Select Writable Fields
            </th>
        </tr>
        <tr>
            <td class="FieldLabel" nowrap>
                <%-- Click/focus/change handlers for these pickers are defined up in the $(document).ready --%>
                <input type="radio" id="m_enableWritableFieldsSearch" name="writableFieldsSearchOption" onclick="defaultCategory('m_writableFieldsCategories', 'm_writableFieldsListContainer'); displayWritableFieldSearchResults(false);" checked />
                Search by Field Name or ID
                <input type="text" id="writableFieldsSearchCriteria" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" colspan="2" nowrap>
                <input type="radio" id="m_disableWritableFieldsSearch" name="writableFieldsSearchOption" />
                Jump to category
                <ASP:DropDownList id="m_writableFieldsCategories" runat="server" class="SearchField">
				</ASP:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" colspan="2" nowrap>
                <input type="radio" id="m_searchWritableFieldsPage" name="writableFieldsSearchOption" />
                Jump to Page 
                <asp:DropDownList id="m_writableFieldsPage" runat="server" class="SearchField"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <div id="m_writableFieldsListContainer" style="height:200px; width:650px; overflow:auto; padding:3px; border:solid 1px black">
                    <asp:Repeater ID="m_writableFieldsList" EnableViewState="false" runat="server" OnItemDataBound="OnWritableFieldsListBound" OnLoad="OnWritableFieldsListLoad">
                        <ItemTemplate>
                            <div id="m_choice" runat="server">
                                <asp:CheckBox ID="m_cb" runat="server"></asp:CheckBox>
                                <ml:EncodedLiteral ID="m_pData" runat="server"></ml:EncodedLiteral>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" id="m_writableFieldsSaveBtn" value="Save" onclick="onSaveWritableFields();" />
                <input type="button" value="Cancel" onclick="Modal.Hide();" />
            </td>
        </tr>
    </table>
</div>
    
<div id="ParameterPickerPopup" class="modal" runat="server">
    <table class="align-left" id="m_parameterPicker" runat="server" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <th class="FormTableHeader" colspan="2">
                Select Parameter
            </th>
        </tr>
        <tr>
            <td class="FieldLabel" nowrap>
                <input type="radio" id="m_enableSearch" name="searchOption" checked />
                Search by Field Name or ID
                <input type="text" id="searchCriteria" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" colspan="2" nowrap>
                <input type="radio" id="m_disableSearch" name="searchOption" />
                Jump to category
                <asp:DropDownList id="m_Group" runat="server" class="SearchField"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" colspan="2" nowrap>
                <input type="radio" id="m_searchPage" name="searchOption" />
                Jump to Page 
                <asp:DropDownList id="m_Page" runat="server" class="SearchField"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="m_FieldList" style="height:200px; width:650px; overflow:auto; padding:3px; border:solid 1px black">
                    <asp:Repeater runat="server" ID="m_parameterList" OnItemDataBound="OnParameterListBound" EnableViewState="false">
                        <ItemTemplate>
                            <div id="m_choice" runat="server">
                                <a id="m_selectLink" runat="server" href="#">select</a>
                                <ml:EncodedLiteral ID="m_pData" runat="server"></ml:EncodedLiteral>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <input type="button" value="Cancel" onclick="Modal.Hide();" />
            </td>
        </tr>
    </table>
        
    <table class="align-left" id="m_parameterEditor" style="display:none" runat="server" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <td>
                Parameter: 
            </td>
            <td>
                <ml:EncodedLiteral ID="m_parameterName" runat="server"></ml:EncodedLiteral>
            </td>
        </tr>
        <tr>
            <td colspan="2"><asp:CheckBox ID="m_IsLookAtNewData" Text="Is Look At New Data? (Check if this is date field and use in Pre-Save Validation)" runat="server"/></td>
        </tr>
        <tr id="RequiredForUserFriendlyChecklistInclusionRow" runat="server">
            <td colspan="2"><asp:CheckBox ID="RequiredForUserFriendlyChecklistInclusion" runat="server" /></td>
        </tr>

        <tbody id="parameterPathSection">
            <tr id="propertyLevelRow" runat="server">
                <td>
                    Property Level:
                </td>
                <td>
                    <asp:RadioButtonList ID="propertyLevel" runat="server" RepeatDirection="Horizontal" onchange="onSelectPathPropertyLevel();">
                        <asp:ListItem Text="Individual" Value="INDIVIDUAL" />
                        <asp:ListItem Text="Subset" Value="SUBSET" />
                        <asp:ListItem Text="Whole" Value="WHOLE" />
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr id="subsetRow" runat="server">
                <td>
                    Subset Description:
                </td>
                <td>
                    <asp:DropDownList id="subsetDropdown" runat="server" />
                </td>
            </tr>
            <tr id="recordRow" runat="server">
                <td>
                    Record Description:
                </td>
                <td>
                    <asp:DropDownList id="recordDropdown" runat="server" />
                </td>
            </tr>
            <tr id="propertyRow" runat="server">
                <td>
                    <ml:EncodedLiteral id="propertyDescription" runat="server" /> Property:
                </td>
                <td>
                    <asp:DropDownList id="propertyDropdown" runat="server" onchange="onSelectPathProperty();" />
                </td>
            </tr>
        </tbody>

        <tbody id="m_parameterOperationsSection">
        <tr>
            <td valign="top">
                Operation:
            </td>
            <td>
                <asp:RadioButton ID="m_operationBetween" CssClass="RadioOption" Text="Between" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
                <asp:RadioButton ID="m_operationEqual" CssClass="RadioOption" Text="Equal" runat="server" GroupName="m_ops" onclick="setValueType('bool');" />
                <asp:RadioButton ID="m_operationEqualInt" CssClass="RadioOption" Text="Equal" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
                <asp:RadioButton ID="m_operationEqualConditionCategory" CssClass="RadioOption" Text="Equal" runat="server" GroupName="m_ops" onclick="setValueType('conditionCategory');" />
                <asp:RadioButton ID="m_operationEqualVar" CssClass="RadioOption" Text="Equal to variable" runat="server" GroupName="m_ops" onclick="setValueType('var');" />
                <asp:RadioButton ID="m_operationIsNonblankStr" CssClass="RadioOption" Text="Is not a blank string" runat="server" GroupName="m_ops" onclick="setValueType('bool');" />
                <asp:RadioButton ID="m_operationGreaterThan" CssClass="RadioOption" Text="Greater Than" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
                <asp:RadioButton ID="m_operationGreaterThanVar" CssClass="RadioOption" Text="Greater than variable" runat="server" GroupName="m_ops" onclick="setValueType('var');" />
                <asp:RadioButton ID="m_operationGreaterThanOrEqualTo" CssClass="RadioOption" Text="Greater than or equal to" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
                <asp:RadioButton ID="m_operationGreaterThanOrEqualToVar" CssClass="RadioOption" Text="Greater than or equal to variable" runat="server" GroupName="m_ops" onclick="setValueType('var');" />
                <asp:RadioButton ID="m_operationLessThan" CssClass="RadioOption" Text="Less than" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
                <asp:RadioButton ID="m_operationLessThanVar" CssClass="RadioOption" Text="Less than variable" runat="server" GroupName="m_ops" onclick="setValueType('var');" />
                <asp:RadioButton ID="m_operationLessThanOrEqualTo" CssClass="RadioOption" Text="Less than or equal to" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
                <asp:RadioButton ID="m_operationLessThanOrEqualToVar" CssClass="RadioOption" Text="Less than or equal to variable" runat="server" GroupName="m_ops" onclick="setValueType('var');" />
                <asp:RadioButton ID="m_operationIsNonzeroValue" CssClass="RadioOption" Text="Is a nonzero value" runat="server" GroupName="m_ops" onclick="setValueType('bool');" />
                <asp:RadioButton ID="m_operationIsAnyValue" CssClass="RadioOption" Text="Is any value" runat="server" GroupName="m_ops" onclick="setValueType('bool');" />
                <asp:RadioButton ID="m_operationNDaysFromToday" CssClass="RadioOption" Text="N days from today" runat="server" GroupName="m_ops" onclick="setValueType('NDaysFromToday');" />
                <asp:RadioButton ID="m_operationDontCare" CssClass="RadioOption" Text="Don't Care" runat="server" GroupName="m_ops" onclick="setValueType('');" />
            </td>
        </tr>
        </tbody>
        <tbody id="m_parameterValuesSection">
        <tr>
            <td valign="top">
                Values:
            </td>
            <td>
                <ml:EncodedLabel style="display:none" id="m_valueDesc" runat="server"></ml:EncodedLabel>
                <asp:DropDownList style="display:none" ID="m_operationNDaysFromTodayOperator" runat="server">
                    <asp:ListItem Value="+" Text="+"></asp:ListItem>
                    <asp:ListItem Value="-" Text="-"></asp:ListItem>
                </asp:DropDownList>
                <asp:RadioButton ID="m_valueTextboxRb" GroupName="m_value" runat="server" style="display:none" />
                <asp:TextBox ID="m_valueTextbox1" runat="server" style="display:none" onkeyup="validateEditor();"></asp:TextBox>
                <asp:TextBox ID="m_valueTextbox2" runat="server" style="display:none" onkeyup="validateEditor();"></asp:TextBox>
                <asp:RadioButton ID="m_valueVarRb" GroupName="m_value" runat="server" style="display:none" />
                <ml:EncodedLabel ID="m_valueVar" runat="server" style="display:none"></ml:EncodedLabel>
                <a href="#" id="m_valueVarEdit" style="display:none" onclick="onPickVariable();">Pick variable</a>
                <asp:RadioButton ID="m_valueBoolRb" GroupName="m_value" runat="server" style="display:none" />
                <span id="m_valueTrueSpan" style="display:none" runat="server"><asp:RadioButton ID="m_valueTrue" Text="True" GroupName="m_valueBool" runat="server" onclick="validateEditor();" /></span>
                <span id="m_valueFalseSpan" style="display:none" runat="server"><asp:RadioButton ID="m_valueFalse" Text="False" GroupName="m_valueBool" runat="server" onclick="validateEditor();" /></span>
                <asp:DropDownList ID="m_valueConditionCategory" runat="server" style="display:none" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CheckBoxList ID="m_valueEnum" runat="server"></asp:CheckBoxList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Repeater ID="m_InAndNotInEnumRepeater" runat="server" OnItemDataBound="OnInAndNotInEnumRepeater">
                    <ItemTemplate>
                        <asp:RadioButtonList ID="radioList" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="InAndNotInList">
                            <asp:ListItem Text="Is" Value="IS" />
                            <asp:ListItem Text="Is Not" Value="IS_NOT" />
                            <asp:ListItem Text="N/A" Value="NA" Selected/>
                        </asp:RadioButtonList>
                        <ml:EncodedLabel ID="name" runat="server" AssociatedControlID="radioList" CssClass="InAndNotInName" />
                        <asp:HiddenField ID="value" runat="server" />
                        <br />
                    </ItemTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ml:PassthroughLabel ID="m_valueErrorMsg" runat="server"></ml:PassthroughLabel>
            </td>
        </tr>
        </tbody>
        <tr>
            <td>
                <input type="button" id="m_editorSaveBtn" value="Save" onclick="onSaveParameter();" />
                <input type="button" value="Cancel" onclick="Modal.Hide();" />
            </td>
        </tr>
    </table>
</div>

<div id="pathableFieldPermissionsEditor" class="modal">
    <table class="align-left" id="pathableFieldPermissionsTable" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <td colspan="2">
                Path: <span id="pathPermissions_pathSoFar" ></span>
            </td>
        </tr>
        <tr id="pathPermissionsCollectionRow">
            <td>
                Collection:
            </td>
            <td>
                <select id="pathPermissionsCollectionDropdown" class="updatePath" />
            </td>
        </tr>
        <tr id="pathPermissionsPropertyLevelRow">
            <td>
                Property Level:
            </td>
            <td>
                <input type="radio" id="pathPermissionsPropertyLevel_individual" name="pathPermissionsPropertyLevel" class="updatePath" value="INDIVIDUAL" /><label for="pathPermissionsPropertyLevel_individual">Individual</label>
                <input type="radio" id="pathPermissionsPropertyLevel_subset" name="pathPermissionsPropertyLevel" class="updatePath" value="SUBSET" /><label for="pathPermissionsPropertyLevel_subset">Subset</label>
                <input type="radio" id="pathPermissionsPropertyLevel_whole" name="pathPermissionsPropertyLevel" class="updatePath" value="WHOLE" /><label for="pathPermissionsPropertyLevel_whole">Whole</label>
            </td>
        </tr>
        <tr id="pathPermissionsSubsetRow">
            <td>
                Subset Description:
            </td>
            <td>
                <select id="pathPermissionsSubsetDropdown" class="updatePath" />
            </td>
        </tr>
        <tr id="pathPermissionsRecordRow">
            <td>
                Record Description:
            </td>
            <td>
                <select id="pathPermissionsRecordDropDown" class="updatePath" />
            </td>
        </tr>
        <tr id="pathPermissionsPropertyRow">
            <td>
                <span id="pathPermissionsPropertyDescription"></span> Property:
            </td>
            <td>
                <select id="pathPermissionsPropertyDropdown" class="updatePath" />
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" id="pathPermissionsSaveBtn" value="Save" onclick="onSaveWritableCollection();" />
                <input type="button" value="Cancel" onclick="Modal.Hide();" />
            </td>
        </tr>
    </table>
</div>
