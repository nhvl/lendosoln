﻿namespace LendersOfficeApp.common.Workflow
{
    using System;
    using System.Linq;
    using System.Web.UI;
    using CommonLib;

    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;

    public partial class BranchGroupsControl : UserControl
    {
        private bool IsInternal => PrincipalFactory.CurrentPrincipal is InternalUserPrincipal;

        protected Guid BrokerId
        {
            get
            {
                if (ViewState["brokerid"] == null)
                {
                    if (this.IsInternal)
                    {
                        ViewState["brokerid"] = RequestHelper.GetGuid("brokerid", Guid.Empty);
                    }
                    else
                    {
                        ViewState["brokerid"] = PrincipalFactory.CurrentPrincipal.BrokerId;
                    }
                }

                return (Guid)ViewState["brokerid"];
            }
            set
            {
                ViewState["brokerid"] = value;
            }
        }

        private new BasePage Page
        {
            get { return (BasePage)base.Page; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.ClientIDMode = ClientIDMode.Static;
            Page.EnableJqueryMigrate = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.RegisterJsScript("LQBPopup.js");
            Page.RegisterJsGlobalVariables("IsInternal", this.IsInternal);
            string sCommand = command_name.Value;

            if (sCommand == "delete")
            {
                Guid groupId = SafeConvert.ToGuid(command_param.Value);
                DeleteGroup(groupId);               
            }

            var groupList = GroupDB.GetAllGroups(BrokerId, GroupType.Branch);
            grdData.DataSource = groupList;
            grdData.DataBind();

            int nCount = groupList.Count();
            divMessage.InnerText = (nCount == 0) ? "No records found" : string.Empty;
        }

        private void DeleteGroup(Guid groupId)
        {
             if (GroupDB.GetGroupMembersCount(BrokerId, groupId) > 0)
             {
                 AlertUser(ErrorMessages.DeleteFailForBranchGroupHavingActiveMembers);
             }
             else
             {
                Group group = GroupDB.GetGroupById(BrokerId, groupId);

                 if (GroupDB.DoesGroupExistInConfig(BrokerId, "BranchGroup", group.Name))
                 {
                     AlertUser(ErrorMessages.GroupExistsInConfig);
                     return;
                 }
                GroupDB.DeleteGroup(BrokerId, groupId, group, PrincipalFactory.CurrentPrincipal);
             }
        }

        private void AlertUser(string message)
        {
            string sScript = @"<script type='text/javascript'>
/*[CDATA[*/
    alert('" + message + @"');
/*]]*/
</script>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Updated", sScript);
        }
    }
}
