﻿#region auto-generated code
namespace LendersOfficeApp.common.Workflow
#endregion
{
    using System;
    using ConfigSystem;
    using ConfigSystem.DataAccess;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.Security;
    using LendersOffice.Drivers.Gateways;
    using System.Web;

    /// <summary>
    /// Service page for workflow change history page.
    /// </summary>
    public partial class WorkflowChangeHistoryService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Process method.
        /// </summary>
        /// <param name="methodName">Method name.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "Revert":
                    this.Revert();
                    break;
                default:
                    throw CBaseException.GenericException($"Unhandled method [{methodName}]");
            }
        }

        /// <summary>
        /// Verifies that the user attempting to access the page is the Workflow Controller.
        /// </summary>
        /// <param name="e">Event Args.</param>
        /// <exception cref="CBaseException">Throws if principal is null or if UserID does not match Workflow Controller ID.</exception>
        protected override void OnInit(EventArgs e)
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            if (principal == null ||
                (!(principal is InternalUserPrincipal) && 
                (!principal.BrokerDB.ExposeClientFacingWorkflowConfiguration || principal.UserId != principal.BrokerDB.WorkflowRulesControllerId)))
            {
                throw new CBaseException(ErrorMessages.GenericAccessDenied, $"Attempt to access workflow page by user who is not the Workflow Controller nor the internal user. UserId=[{principal?.UserId.ToString() ?? "null"}]");
            }

            base.OnInit(e);
        }

        /// <summary>
        /// Revert the current workflow configuration to the selected configuration.
        /// </summary>
        private void Revert()
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            if (principal is InternalUserPrincipal)
            {
                Tools.ValidateInternalUserPermission(principal.Permissions, E_InternalUserPermissions.EditBroker);
            }

            try
            {
                Guid brokerId = GetGuid("BrokerId");
                long configId = GetLong("ConfigId");
                DateTime revertToDate = DateTime.Parse(GetString("AuditDate"));

                IConfigRepository configRepository = ConfigHandler.GetAuditRepository(brokerId, principal, WorkflowAuditType.Revert, $"Workflow rules reverted to version created on {revertToDate.ToString("MM/dd/yyyy h:mm tt")}");

                string xml = WorkflowChangeAudit.RetrieveWorkflowConfigXMLByConfigId(brokerId, configId);

                configRepository.SaveReleaseConfig(brokerId, new SystemConfig(xml), principal.UserId);
            }            
            catch (CommonProjectLib.Common.CBaseException e)
            {
                throw new CBaseException("Error while reverting workflow configuration.\n" + e.UserMessage(), e);
            }
        }        
    }
}