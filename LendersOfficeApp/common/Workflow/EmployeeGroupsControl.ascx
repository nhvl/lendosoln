﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeGroupsControl.ascx.cs" Inherits="LendersOfficeApp.common.Workflow.EmployeeGroupsControl" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script type="text/javascript">
      <!--        
        function f_edit(groupId, name, description) {
            var editUrl;
            if (ML.IsInternal) {
                editUrl = '/LOAdmin/Broker/EditEmployeeGroup.aspx?brokerid=' + <%=AspxTools.JsString(BrokerId)%>;
            } else {
                editUrl = '/los/Workflow/EditEmployeeGroup.aspx';
            }

            if(groupId != null)
            {
                editUrl += (ML.IsInternal ? '&' : '?') + 'groupid=' + groupId;
            }
            
            showModal(editUrl, null, null, null, function() { self.location = self.location.href; }, { hideCloseButton: true });
        }
        function f_delete(groupId) 
        {
            if(window.confirm("Are you sure you want to delete the group? It may be associated with some digital certificates.") == "Y")
            {
             document.getElementById("command_name").value = 'delete';
             document.getElementById("command_param").value = groupId;
             window.form1.submit();
            }
        }

        function _initControl()
        {    
            resize( 680 , 720 );
            document.getElementById("command_name").value = ''; 
        }

        var defaultConfirm = window.confirm;
        window.confirm = function(msg) {
            if(typeof execScript == 'function') {
                execScript('r = msgbox("' + msg + '","3", msg)', "vbscript");
                if (r == 6) return "Y"; //Yes is pressed
                if (r == 7) return "N"; //No is pressed
                return "C"; //Cancel is pressed or the message box is closed
            }
            else {
                return defaultConfirm(msg) ? "Y" : "N";
            }
        }
      -->
      </script>

    <table cellspacing="0" cellpadding="8" border="0" width="100%" height="100%">
				<tr>
					<td height="100%">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
							<tr>
								<td height="100%" style="BORDER-RIGHT: 2px outset; BORDER-TOP: 2px outset; BORDER-LEFT: 2px outset; BORDER-BOTTOM: 2px outset; BACKGROUND-COLOR: gainsboro">
									<div class="FormTableHeader" style="padding:0.2em"  width="100%" border="0">Employee Groups</div>					
      <ml:CommonDataGrid id=grdData runat="server">
          <AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top" />
          <ItemStyle CssClass="GridItem" VerticalAlign="Top" />
          <HeaderStyle CssClass="GridHeader" />
      <columns>           
          <asp:TemplateColumn ItemStyle-Width="30px"  ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
              <a onclick="f_edit(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "GroupId").ToString()) + "," + AspxTools.JsString(DataBinder.Eval(Container.DataItem, "GroupName").ToString()) + "," + AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Description").ToString() ) %>);return true;">edit</a>
            </ItemTemplate>
          </asp:TemplateColumn>
          <asp:BoundColumn DataField="GroupName" HeaderText="Employee Group" SortExpression="GroupName"></asp:BoundColumn> 
          <asp:BoundColumn DataField="Description" HeaderText="Description" SortExpression="Description"></asp:BoundColumn>           
          <asp:TemplateColumn ItemStyle-Width="30px"  ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
              <a onclick="f_delete(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "GroupId").ToString())%>);">delete</a>
            </ItemTemplate>
          </asp:TemplateColumn>
      </columns>
      </ml:CommonDataGrid>
    <div id="divMessage" runat="server" style="text-align:center; font:bold"></div>    
    </td>
	</tr>
	<tr>
	<td>							    
        <table width="100%" style="margin-top:0.5em" cellpadding=0; cellspacing=0;>
        <tr>
        <td><asp:Button ID="ButtonNew" runat="server" OnClientClick="f_edit(); return false;" Text="Add employee group" /></td>
        <td style="text-align:right"><asp:Button ID="ButtonClose" Width="6em" runat="server" OnClientClick="window.close()" Text="Close" /></td>
        </tr>
        </table>
    </td>
    </tr>
	</table>
    </td>
	</tr>				
</table>  
        
<uc1:cModalDlg runat="server" ID="Cmodaldlg1"></uc1:cModalDlg>
<input type="hidden" id="command_name" runat="server" enableviewstate="false" />
<input type="hidden" id="command_param" runat="server" enableviewstate="false" />
