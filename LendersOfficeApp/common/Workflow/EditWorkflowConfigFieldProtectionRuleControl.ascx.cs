﻿namespace LendersOfficeApp.common.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Xml;

    using ConfigSystem;
    using DataAccess;
    using DataAccess.PathDispatch;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Security;
    using LendersOfficeApp.common.Workflow;
    using LendersOfficeApp.LOAdmin.TaskBackendUtilities;
    using LqbGrammar.DataTypes.PathDispatch;
    using MeridianLink.CommonControls;

    /// <summary>
    /// This is in need of a rewrite:
    /// 1. We can probably do this without so many postbacks
    /// 2. This page doesn't need to know the difference between a constraint and condition
    /// 3. Separate pages by functionality; use an iframe or user control for the modal divs (probably user control) 
    /// 4. Inconsistent field naming convention. Use m_ for all of them or don't. Or distinguish between
    ///     hidden inputs, ASP controls such as grids and repeaters, and class variables.
    /// 5. Use tables for tabular data and NOT layout!
    /// 6. Since we're already using jQuery, we should follow the jQuery convention of attaching functions to events
    /// 7. Make use of css classes. No more inline styles.
    /// 8. Confusing nomenclature. Reconsider word choice. Rename ParameterSet (editor list) and ParameterList (picker list).
    ///      The word Operation refers to two different things. Rename them to LoanOperation and FieldOperation.
    /// </summary>
    public partial class EditWorkflowConfigFieldProtectionRuleControl : UserControl
    {
        #region Query string
        private bool IsInternal => PrincipalFactory.CurrentPrincipal is InternalUserPrincipal;

        protected Guid m_brokerId
        {
            get
            {
                if (this.IsInternal)
                {
                    return RequestHelper.GetGuid("brokerid", Guid.Empty);
                }

                return PrincipalFactory.CurrentPrincipal.BrokerId;
            }
        }

        protected string m_type
        {
            get
            {
                return RequestHelper.GetSafeQueryString("type");
            }
        }
        #endregion

        #region Hidden inputs
        protected bool m_inDraftMode
        {
            get
            {
                return Convert.ToBoolean(m_hiddenDraftMode.Value);
            }
            set
            {
                m_hiddenDraftMode.Value = value.ToString();
            }
        }

        protected Guid m_cacheId
        {
            get
            {
                return new Guid(m_hiddenKey.Value);
            }
            set
            {
                m_hiddenKey.Value = value.ToString();
            }
        }

        protected Guid m_itemId
        {
            get
            {
                if (isAdd)
                    return new Guid();
                else
                    return new Guid(m_hiddenId.Value);
            }
        }

        protected bool isAdd
        {
            get
            {
                return m_hiddenAction.Value == "add";
            }
        }
        #endregion

        private List<string> m_protectFields;

        private AbstractConditionGroup m_configItem;

        protected WorkflowSystemConfigModel m_data
        {
            get;
            set;
        }

        protected SecurityParameter.SecurityParameterType filterType
        {
            get;
            set;
        }

        protected const string DONT_CARE_STRING = "Don't Care";

        protected WorkflowSystemConfigModel GetConfig()
        {
            if (m_cacheId != Guid.Empty)
                return new WorkflowSystemConfigModel(m_brokerId, m_inDraftMode, m_cacheId);
            else
                return new WorkflowSystemConfigModel(m_brokerId, m_inDraftMode);
        }

        protected AbstractConditionGroup GetConfigItem()
        {
            if (isAdd)
                return new Condition();
            else
                return m_data.GetCondition(m_itemId);
        }

        private new BasePage Page
        {
            get { return (BasePage)base.Page; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.ClientIDMode = ClientIDMode.Static;
            Page.EnableJqueryMigrate = false;

            Page.ClientScript.GetPostBackEventReference(this, string.Empty);
            m_protectFields = new List<string>();

            Page.RegisterCSS("Workflow/EditWorkflowConfigFieldProtectionRule.css");
            Page.RegisterJsScript("Workflow/EditWorkflowConfig.js");
            Page.RegisterJsScript("utilities.js");

            if (!Page.IsPostBack)
            {
                m_hiddenId.Value = RequestHelper.GetSafeQueryString("itemId");
                m_hiddenAction.Value = RequestHelper.GetSafeQueryString("action");
                m_hiddenDraftMode.Value = RequestHelper.GetSafeQueryString("isDraft");
                m_cacheId = Guid.Empty;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            m_data = GetConfig();
            m_configItem = GetConfigItem();
            m_pageHeader.Text = (isAdd ? "Add " : "Edit ");
            m_pageHeader.Text += "Field Protection Rule";
            m_valueErrorMsg.Text = "";
            m_saveErrorMsg.Text = "";

            ProcessCommands_Load();

            if (!Page.IsPostBack && !isAdd)
            {
                PopulateMainDisplay();
                var protectFieldOp = m_configItem.SysOpSet.Get(WorkflowOperations.ProtectField);
                if (protectFieldOp != null && protectFieldOp.Fields != null)
                {
                    // If any of the fields are clearable, then they should all be clearable.
                    // Task Backend Spec 4.2.1.3.1.2
                    if (protectFieldOp.Fields.Values.Any((field) => field.Updateable))
                    {
                        m_protectedFieldsAreUpdateable.Checked = true;
                    }
                }
            }

            m_parameterSet.DataSource = m_data.GetNewConditionParameters(WorkflowSystemConfigModel.TableType.Conditions, m_itemId, GetOperations().ToArray());
            m_parameterSet.DataBind();

            if (propertyLevelRow.Visible)
            {
                SetRadioButtonVisibility();
            }

            validateButtons();

            Page.RegisterJsObjectWithJsonNetSerializer("PathableSchemaModel", EditWorkflowConfigUtilities.GeneratePathModel(m_brokerId));
        }

        private void ProcessCommands_Load()
        {
            string parameterName = m_hiddenParameterName.Value;
            if (!String.IsNullOrEmpty(m_hiddenCmd.Value))
            {
                switch (m_hiddenCmd.Value)
                {
                    case "saveParameter":
                        SaveParameter(parameterName, m_configItem);
                        break;
                    case "pickParameter":
                        filterType = m_data.GetParameterFromFields(parameterName).Type;
                        if (filterType == SecurityParameter.SecurityParameterType.PathableCollection)
                        {
                            SecurityParameter.PathableCollectionParameter collParameter = m_data.GetParameterFromFields(parameterName) as SecurityParameter.PathableCollectionParameter;
                            filterType = collParameter.FieldParameters[propertyDropdown.SelectedValue].Type;
                        }

                        BindParameterPicker();
                        break;
                    case "editParameter":
                        EditParameter(parameterName);
                        break;
                    case "selectPathProperty":
                        SelectPathProperty(parameterName);
                        break;
                    case "selectPathPropertyLevel":
                        SelectPathPropertyLevel();
                        break;
                    case "addParameter":
                        BindParameterPicker();
                        break;
                    case "pickProtectFields":
                        BindProtectedFields();
                        break;
                    case "saveProtectFields":
                    case "saveWritableCollection":
                        BindProtectedFields();
                        break;
                    case "removeWritableCollection":
                        RemoveWritableCollection();
                        break;
                    case "saveConfigItem":
                        break;
                    default:
                        break;
                }
            }
        }

        // Populate viewstate and postback values happening between Load and PreRender

        protected void Page_PreRender(object sender, EventArgs e)
        {
            ProcessCommands_PreRender();

            // Get protected field names
            m_protectFields = new List<string>();
            List<OperationField> protectCollectionFields = new List<OperationField>();

            var protectFieldOp = m_configItem.SysOpSet.Get(WorkflowOperations.ProtectField);
            if (protectFieldOp != null && protectFieldOp.Fields != null)
            {
                foreach (OperationField field in protectFieldOp.Fields.Values)
                {
                    // Separate out fieldnames and paths.
                    if (field.FriendlyName.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
                    {
                        protectCollectionFields.Add(field);
                    }
                    else
                    {
                        m_protectFields.Add(field.FriendlyName);
                    }
                }
            }

            m_protectedFieldsListMainDisplay.DataSource = m_protectFields;
            m_protectedFieldsListMainDisplay.DataBind();

            PathableFieldPermissionsList.DataSource = protectCollectionFields;
            PathableFieldPermissionsList.DataBind();
        }

        private void ProcessCommands_PreRender()
        {
            if (!String.IsNullOrEmpty(m_hiddenCmd.Value))
            {
                switch (m_hiddenCmd.Value)
                {
                    case "saveParameter":
                        break;
                    case "pickParameter":
                        break;
                    case "editParameter":
                        break;
                    case "addParameter":
                        break;
                    case "pickProtectFields":
                        PickProtectFields();
                        break;
                    case "saveProtectFields":
                    case "saveWritableCollection":
                        SaveProtectFields();
                        break;
                    case "saveConfigItem":
                        SaveConfigItem();
                        break;
                    default:
                        break;
                }
            }
        }

        private void PopulateMainDisplay()
        {
            m_failureMessage.Text = m_configItem.FailureMessage;
            m_tbNotes.Text = m_configItem.Notes;
        }

        private void PickProtectFields()
        {
            
        }

        private void SaveProtectFields()
        {
            EditWorkflowConfigUtilities.UpdateOperationFields(WorkflowOperations.ProtectField, m_protectedFieldsList, PathableFieldPermissionsList, m_protectedFieldsAreUpdateable.Checked, false, PathableFieldPermissionsEditor_Path.Value, PathableFieldPermissionsEditor_FriendlyPath.Value, m_configItem);
            EditWorkflowConfigUtilities.UpdateOperations(GetOperations(), false, m_protectedFieldsAreUpdateable.Checked, m_failureMessage.Text, m_tbNotes.Text, false, m_configItem);
            if (isAdd)
            {
                m_data.AddNew(m_configItem);
            }
            else if (!isAdd)
            {
                m_data.Edit(m_configItem.Id, m_configItem);
            }
            SaveToCache(false);
        }

        private void RemoveWritableCollection()
        {
            EditWorkflowConfigUtilities.RemoveOperationField(m_configItem, WorkflowOperations.ProtectField, PathableFieldPermissionsEditor_Path.Value);

            if (isAdd)
            {
                m_data.AddNew(m_configItem);
            }
            else
            {
                m_data.Edit(m_configItem.Id, m_configItem);
            }

            SaveToCache(false);
        }

        protected void validateButtons()
        {
            m_editFieldsBtn.Disabled = isAdd;
            btnEditCollections.Disabled = isAdd;
            m_saveItemBtn.Disabled = isAdd;
        }

        protected void clearValues()
        {
            m_parameterName.Text = "";
            m_valueTextboxRb.Checked = false;
            m_valueVarRb.Checked = false;
            m_valueBoolRb.Checked = false;
            m_valueTextbox1.Text = "";
            m_valueTextbox2.Text = "";
            m_hiddenVariableName.Value = "";
            m_valueVar.Text = "";
            m_valueTrue.Checked = false;
            m_valueFalse.Checked = false;
            m_hiddenEditorMode.Value = "";
        }

        protected void loadConditionCategoryValues(ConfigSystem.Parameter p)
        {
            m_operationEqualConditionCategory.Checked = true;
            Tools.SetDropDownListValue(m_valueConditionCategory, p.Values.First());
            m_hiddenEditorMode.Value = "conditionCategory";
        }

        protected void loadEnumValues(ConfigSystem.Parameter p)
        {
            foreach (string val in p.Values)
            {
                ListItem item = m_valueEnum.Items.FindByValue(val);

                // For Enums, if value is not found (because it's been removed) DO NOT THROW.
                // Add bad value to the list. Force the user to clean it up before save.
                if (item == null)
                {
                    item = new ListItem(p.ValueDesc[val], val);
                    m_valueEnum.Items.Add(item);
                }

                item.Selected = true;
            }
        }

        protected void LoanInAndNotInEnumValues(ConfigSystem.Parameter p)
        {
            foreach (RepeaterItem item in m_InAndNotInEnumRepeater.Items)
            {
                RadioButtonList radioList = item.FindControl("radioList") as RadioButtonList;
                HiddenField value = item.FindControl("value") as HiddenField;

                if (p.Values.Contains(value.Value))
                {
                    radioList.SelectedValue = "IS";
                }
                else if (p.Exceptions.Contains(value.Value))
                {
                    radioList.SelectedValue = "IS_NOT";
                }
                else
                {
                    radioList.SelectedValue = "NA";
                }
            }
        }

        protected void loadTextboxValues(ConfigSystem.Parameter p)
        {
            // First, check if it's a date offset comparison
            string text = p.Values.ElementAt(0);
            var match = Regex.Match(text, @"^today([+-])([0-9]+)$");
            if (match.Success)
            {
                m_operationNDaysFromToday.Checked = true;
                m_operationNDaysFromTodayOperator.SelectedValue = match.Groups[1].Value;
                m_valueTextbox1.Text = match.Groups[2].Value;
                m_hiddenEditorMode.Value = "NDaysFromToday";
            }
            else // otherwise it's something else
            {
                m_valueTextbox1.Text = p.Values.ElementAt(0);
                m_hiddenEditorMode.Value = "textbox";
            }
            if (p.Values.Count > 1)
                m_valueTextbox2.Text = p.Values.ElementAt(1);

        }

        protected void loadBooleanValues(ConfigSystem.Parameter p)
        {
            bool value;
            if (Boolean.TryParse(p.Values.ElementAt(0), out value))
            {
                m_valueTrue.Checked = value == true;
                m_valueFalse.Checked = value == false;
                m_hiddenEditorMode.Value = "bool";
            }
            else
            {
                loadTextboxValues(p);
            }
        }

        protected void loadVariableValues(ConfigSystem.Parameter p)
        {
            m_valueVar.Text = p.Values.ElementAt(0);
            m_hiddenEditorMode.Value = "var";
        }

        /// <summary>
        /// Called when the user attempts to add or edit a parameter. Populates the edit parameter dialog.
        /// </summary>
        protected void EditParameter(string paramName)
        {
            PrepareParameterEditor(paramName);

            if (isAdd)
                return;

            // Load up the parameter from the parameterSet
            ConfigSystem.Parameter configParameter = m_configItem.ParameterSet.GetParameter(paramName);
            if (configParameter == null)
                return;

            LoadParameterValues(configParameter, m_data.GetParameterWithValues(paramName));
        }

        private void LoadPathDropDowns(string pathString)
        {
            DataPath path = DataPath.Create(pathString);

            // Set initial parameter.
            SecurityParameter.Parameter currentParameter = m_data.GetParameterFromFields(path.Head.Name);
            if (currentParameter == null)
            {
                throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{pathString}]");
            }

            PrepareParameter(currentParameter); // Bind drop down items.

            // Set drop down value.
            foreach (IDataPathElement element in path.Tail.PathList)
            {
                if (element is DataPathBasicElement)
                {
                    // Only Record parameters have pathable elements. So check that parameter is a record parameter, and that the next element
                    // in the path corresponds to a field belonging to that parameter.
                    SecurityParameter.PathableRecordParameter recordParameter = currentParameter as SecurityParameter.PathableRecordParameter;
                    if (recordParameter == null || !recordParameter.FieldParameters.TryGetValue(element.Name, out currentParameter))
                    {
                        throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{pathString}]");
                    }

                    // Set Property Drop Down.
                    propertyDropdown.SelectedValue = element.Name;
                }
                else if (element is DataPathCollectionElement)
                {
                    // Only Record parameters have pathable elements. So check that parameter is a record parameter, and that the next element
                    // in the path corresponds to a field belonging to that parameter.
                    SecurityParameter.PathableRecordParameter recordParameter = currentParameter as SecurityParameter.PathableRecordParameter;
                    if (recordParameter == null || !recordParameter.FieldParameters.TryGetValue(element.Name, out currentParameter))
                    {
                        throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{pathString}]");
                    }

                    // Only Collection parameters correspond to Collection data path nodes. So check that parameter is a collection parameter.
                    SecurityParameter.PathableCollectionParameter collectionParameter = currentParameter as SecurityParameter.PathableCollectionParameter;
                    if (collectionParameter == null)
                    {
                        throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{pathString}]");
                    }
                }
                else if (element is DataPathSelectionElement)
                {
                    // Only Collection parameters can be indexed by a selection parameter. So check that parameter is a collection parameter.
                    SecurityParameter.PathableCollectionParameter collectionParameter = currentParameter as SecurityParameter.PathableCollectionParameter;
                    if (collectionParameter == null)
                    {
                        throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{pathString}]");
                    }

                    // Set Property Level and subset/record drop down.
                    if (string.Equals(element.Name, DataPath.WholeCollectionIdentifier, StringComparison.OrdinalIgnoreCase))
                    {
                        propertyLevel.SelectedValue = "WHOLE";
                    }
                    else if (element.Name.StartsWith(DataPath.SubsetMarker, StringComparison.OrdinalIgnoreCase))
                    {
                        // Verify subset name.
                        string substringName = element.Name.Substring(DataPath.SubsetMarker.Length);
                        if (!collectionParameter.Subsets.ContainsKey(substringName))
                        {
                            throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{pathString}]");
                        }

                        propertyLevel.SelectedValue = "SUBSET";
                        subsetDropdown.SelectedValue = substringName;
                    }
                    else
                    {
                        PathableRecordTypeInfo recordInfo;
                        if (!collectionParameter.Records.TryGetValue(element.Name, out recordInfo))
                        {
                            throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{pathString}]");
                        }

                        propertyLevel.SelectedValue = "INDIVIDUAL";
                        recordDropdown.SelectedValue = element.Name;
                    }

                    SelectPathPropertyLevel();  // Sets dropdown visibility based on property level.
                }
            }

            ClearOperationsSection();
            PrepareParameter(currentParameter);

            // Set Path UI elements readonly.
            propertyLevel.Enabled = false;
            subsetDropdown.Enabled = false;
            recordDropdown.Enabled = false;
            propertyDropdown.Enabled = false;
        }

        private void LoadParameterValues(ConfigSystem.Parameter configParameter, SecurityParameter.Parameter securityParameter)
        {

            m_operationDontCare.Checked = false;

            // Populate the edit parameter dialog
            if (securityParameter.Type == SecurityParameter.SecurityParameterType.Enum && securityParameter.Source == SecurityParameter.E_FieldSource.Condition)
            {
                loadConditionCategoryValues(configParameter);
            }
            else if (securityParameter.Type == SecurityParameter.SecurityParameterType.Enum)
            {
                SecurityParameter.EnumeratedParmeter enumParameter = securityParameter as SecurityParameter.EnumeratedParmeter;
                if (enumParameter.HasNotInOption)
                {
                    LoanInAndNotInEnumValues(configParameter);
                }
                else
                {
                    loadEnumValues(configParameter);
                }
            }
            else
            {
                switch (configParameter.FunctionT)
                {
                    case E_FunctionT.Between:
                        m_operationBetween.Checked = true;
                        loadTextboxValues(configParameter);
                        break;
                    case E_FunctionT.Equal:
                        if (configParameter.ValueType == E_ParameterValueType.Int)
                        {
                            m_operationEqualInt.Checked = true;
                            loadTextboxValues(configParameter);
                        }
                        else
                        {
                            m_operationEqual.Checked = true;
                            loadBooleanValues(configParameter);
                        }
                        break;
                    case E_FunctionT.EqualVar:
                        m_operationEqualVar.Checked = true;
                        loadVariableValues(configParameter);
                        break;
                    case E_FunctionT.IsNonblankStr:
                        m_operationIsNonblankStr.Checked = true;
                        loadBooleanValues(configParameter);
                        break;
                    case E_FunctionT.IsGreaterThan:
                        m_operationGreaterThan.Checked = true;
                        loadTextboxValues(configParameter);
                        break;
                    case E_FunctionT.IsGreaterThanVar:
                        m_operationGreaterThanVar.Checked = true;
                        loadVariableValues(configParameter);
                        break;
                    case E_FunctionT.IsGreaterThanOrEqualTo:
                        m_operationGreaterThanOrEqualTo.Checked = true;
                        loadTextboxValues(configParameter);
                        break;
                    case E_FunctionT.IsGreaterThanOrEqualToVar:
                        m_operationGreaterThanOrEqualToVar.Checked = true;
                        loadVariableValues(configParameter);
                        break;
                    case E_FunctionT.IsLessThan:
                        m_operationLessThan.Checked = true;
                        loadTextboxValues(configParameter);
                        break;
                    case E_FunctionT.IsLessThanVar:
                        m_operationLessThanVar.Checked = true;
                        loadVariableValues(configParameter);
                        break;
                    case E_FunctionT.IsLessThanOrEqualTo:
                        m_operationLessThanOrEqualTo.Checked = true;
                        loadTextboxValues(configParameter);
                        break;
                    case E_FunctionT.IsLessThanOrEqualToVar:
                        m_operationLessThanOrEqualToVar.Checked = true;
                        loadVariableValues(configParameter);
                        break;
                    case E_FunctionT.IsNonzeroValue:
                        m_operationIsNonzeroValue.Checked = true;
                        loadBooleanValues(configParameter);
                        break;
                    case E_FunctionT.IsAnyValue:
                        m_operationIsAnyValue.Checked = true;
                        loadBooleanValues(configParameter);
                        break;
                    default:
                        throw new UnhandledEnumException(configParameter.FunctionT);
                }
            }
        }

        /// <summary>
        /// Determine which parameter options and values are visible in the parameter editor
        /// </summary>
        protected void PrepareParameterEditor(string paramName)
        {
            clearValues();
            m_parameterName.Text = paramName;

            ClearOperationsSection();
            ClearPathSection();

            if (paramName.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
            {
                LoadPathDropDowns(paramName);
            }
            else
            {
                SecurityParameter.Parameter p = m_data.GetParameterFromFields(paramName);
                PrepareParameter(p);
            }
        }

        private void ClearPathSection()
        {
            propertyLevel.Enabled = true;
            subsetDropdown.Enabled = true;
            recordDropdown.Enabled = true;
            propertyDropdown.Enabled = true;

            propertyLevel.SelectedIndex = -1;
            propertyLevelRow.Visible = false;
            subsetRow.Visible = false;
            recordRow.Visible = false;
            propertyRow.Visible = false;
        }

        private void ClearOperationsSection()
        {
            m_operationBetween.Visible = false;
            m_operationBetween.Checked = false;
            m_operationEqual.Visible = false;
            m_operationEqual.Checked = false;
            m_operationEqualVar.Visible = false;
            m_operationEqualVar.Checked = false;
            m_operationIsNonblankStr.Visible = false;
            m_operationIsNonblankStr.Checked = false;
            m_operationGreaterThan.Visible = false;
            m_operationGreaterThan.Checked = false;
            m_operationGreaterThanVar.Visible = false;
            m_operationGreaterThanVar.Checked = false;
            m_operationGreaterThanOrEqualTo.Visible = false;
            m_operationGreaterThanOrEqualTo.Checked = false;
            m_operationGreaterThanOrEqualToVar.Visible = false;
            m_operationGreaterThanOrEqualToVar.Checked = false;
            m_operationLessThan.Visible = false;
            m_operationLessThan.Checked = false;
            m_operationLessThanVar.Visible = false;
            m_operationLessThanVar.Checked = false;
            m_operationLessThanOrEqualTo.Visible = false;
            m_operationLessThanOrEqualTo.Checked = false;
            m_operationLessThanOrEqualToVar.Visible = false;
            m_operationLessThanOrEqualToVar.Checked = false;
            m_operationIsNonzeroValue.Visible = false;
            m_operationIsNonzeroValue.Checked = false;
            m_operationIsAnyValue.Visible = false;
            m_operationIsAnyValue.Checked = false;
            m_operationEqualConditionCategory.Visible = false;
            m_operationEqualConditionCategory.Checked = false;
            m_operationNDaysFromToday.Visible = false;
            m_operationNDaysFromToday.Checked = false;
            m_operationEqualInt.Visible = false;
            m_operationDontCare.Visible = true;
            m_operationDontCare.Checked = true;
            m_InAndNotInEnumRepeater.Visible = false;
        }

        private void PrepareParameter(SecurityParameter.Parameter p)
        {
            switch (p.Type)
            {
                case SecurityParameter.SecurityParameterType.Rate:
                case SecurityParameter.SecurityParameterType.Money:
                    //Float
                    m_operationBetween.Visible = true;
                    m_operationGreaterThan.Visible = true;
                    m_operationGreaterThanOrEqualTo.Visible = true;
                    m_operationLessThan.Visible = true;
                    m_operationLessThanOrEqualTo.Visible = true;
                    m_operationIsNonzeroValue.Visible = true;
                    m_operationIsAnyValue.Visible = true;

                    m_operationEqualVar.Visible = true;
                    m_operationGreaterThanVar.Visible = true;
                    m_operationGreaterThanOrEqualToVar.Visible = true;
                    m_operationLessThanVar.Visible = true;
                    m_operationLessThanOrEqualToVar.Visible = true;
                    break;
                case SecurityParameter.SecurityParameterType.Count:
                    //Int
                    m_operationBetween.Visible = true;
                    m_operationGreaterThan.Visible = true;
                    m_operationGreaterThanOrEqualTo.Visible = true;
                    m_operationLessThan.Visible = true;
                    m_operationLessThanOrEqualTo.Visible = true;
                    m_operationIsNonzeroValue.Visible = true;
                    m_operationIsAnyValue.Visible = true;
                    m_operationEqualInt.Visible = true;
                    m_operationEqualVar.Visible = true;
                    m_operationGreaterThanVar.Visible = true;
                    m_operationGreaterThanOrEqualToVar.Visible = true;
                    m_operationLessThanVar.Visible = true;
                    m_operationLessThanOrEqualToVar.Visible = true;
                    break;
                case SecurityParameter.SecurityParameterType.String:
                    //String
                    m_operationIsNonblankStr.Visible = true;

                    m_operationEqualVar.Visible = true;
                    break;
                case SecurityParameter.SecurityParameterType.Date:
                    //DateTime
                    m_operationBetween.Visible = true;
                    m_operationGreaterThan.Visible = true;
                    m_operationGreaterThanOrEqualTo.Visible = true;
                    m_operationLessThan.Visible = true;
                    m_operationLessThanOrEqualTo.Visible = true;
                    m_operationIsAnyValue.Visible = true;

                    m_operationNDaysFromToday.Visible = true;

                    m_operationEqualVar.Visible = true;
                    m_operationGreaterThanVar.Visible = true;
                    m_operationGreaterThanOrEqualToVar.Visible = true;
                    m_operationLessThanVar.Visible = true;
                    m_operationLessThanOrEqualToVar.Visible = true;
                    break;
                case SecurityParameter.SecurityParameterType.Boolean:
                    //Bool
                    m_operationEqual.Visible = true;

                    m_operationEqualVar.Visible = true;
                    break;
                case SecurityParameter.SecurityParameterType.Custom:
                    //CustomVariable
                    m_operationEqual.Visible = true;
                    break;
                case SecurityParameter.SecurityParameterType.Enum:
                    //Enum
                    SecurityParameter.EnumeratedParmeter enumParameter = p as SecurityParameter.EnumeratedParmeter;

                    List<SecurityParameter.EnumMapping> enumValues = enumParameter.EnumMapping;
                    if (p.Source == SecurityParameter.E_FieldSource.Condition) // This parameter is a condition-related parameter
                        // We're trying to shoehorn condition parameters into the enum parameterType
                    {
                        m_operationEqualConditionCategory.Visible = true;
                        m_valueConditionCategory.DataSource = enumValues;
                        m_valueConditionCategory.DataTextField = "FriendlyValue";
                        m_valueConditionCategory.DataValueField = "RepValue";
                        m_valueConditionCategory.DataBind();
                        m_operationDontCare.Visible = true;
                    }
                    else if (enumParameter.HasNotInOption)
                    {
                        m_valueEnum.Items.Clear();
                        m_InAndNotInEnumRepeater.DataSource = enumValues;
                        m_InAndNotInEnumRepeater.DataBind();
                        m_InAndNotInEnumRepeater.Visible = true;
                        m_operationDontCare.Visible = false;
                    }
                    else
                    {
                        m_valueEnum.DataSource = enumValues;
                        m_valueEnum.DataTextField = "FriendlyValue";
                        m_valueEnum.DataValueField = "RepValue";
                        m_valueEnum.DataBind();
                        m_valueEnum.Items.Add(new ListItem(DONT_CARE_STRING, "-1", true));
                        m_operationDontCare.Visible = false;
                    }
                    break;
                case SecurityParameter.SecurityParameterType.PathableCollection:
                    // Load dropdown lists.
                    SecurityParameter.PathableCollectionParameter collParameter = p as SecurityParameter.PathableCollectionParameter;

                    // Subsets.
                    subsetDropdown.DataSource = collParameter.Subsets.Values.OrderBy(s => s.Name);
                    subsetDropdown.DataValueField = "Name";
                    subsetDropdown.DataTextField = "Name";
                    subsetDropdown.DataBind();

                    // Records.
                    recordDropdown.DataSource = collParameter.Records.Values.OrderBy(r => r.FriendlyDescription);
                    recordDropdown.DataValueField = "TypeId";
                    recordDropdown.DataTextField = "FriendlyDescription";
                    recordDropdown.DataBind();

                    // Properties.
                    propertyDropdown.DataSource = collParameter
                        .FieldParameters
                        .Values
                        .Where(
                            f => f.Type != SecurityParameter.SecurityParameterType.PathableRecord
                            && f.Type != SecurityParameter.SecurityParameterType.PathableCollection)
                        .OrderBy(f => f.FriendlyName);
                    propertyDropdown.DataValueField = "FriendlyName";
                    propertyDropdown.DataTextField = "ID";
                    propertyDropdown.DataBind();

                    // Set property level to default and set visibility.
                    SelectPathPropertyLevel();

                    // Prepare propery parameter
                    PrepareParameter(collParameter.FieldParameters[propertyDropdown.SelectedValue]);
                    break;
                default:
                    throw new UnhandledEnumException(p.Type);
            }

            if (p.Type != SecurityParameter.SecurityParameterType.Enum)
            {
                m_valueEnum.Items.Clear();
            }
        }

        private void SelectPathPropertyLevel()
        {
            propertyLevelRow.Visible = true;
            propertyRow.Visible = true;

            SetRadioButtonVisibility();

            switch (propertyLevel.SelectedValue)
            {
                case "INDIVIDUAL":
                    subsetRow.Visible = false;
                    recordRow.Visible = true;
                    propertyDescription.Text = "Record";
                    break;
                case "SUBSET":
                    subsetRow.Visible = true;
                    recordRow.Visible = false;
                    propertyDescription.Text = "Subset";
                    break;
                case "WHOLE":
                    subsetRow.Visible = false;
                    recordRow.Visible = false;
                    propertyDescription.Text = "Collection";
                    break;
            }
        }

        private void SetRadioButtonVisibility()
        {
            if (recordDropdown.Items.Count <= 0)
            {
                propertyLevel.Items[0].Attributes.Add("hidden", "hidden");
            }

            if (subsetDropdown.Items.Count <= 0)
            {
                propertyLevel.Items[1].Attributes.Add("hidden", "hidden");
            }

            if (propertyLevel.SelectedIndex == -1)
            {
                for (int i = 0; i < propertyLevel.Items.Count; i++)
                {
                    if (propertyLevel.Items[i].Attributes["hidden"] == null)
                    {
                        propertyLevel.SelectedIndex = i;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Get a list of operation names from the selected checkboxes
        /// </summary>
        /// <returns></returns>
        protected List<WorkflowOperation> GetOperations()
        {
            var ops = new List<WorkflowOperation> { WorkflowOperations.ProtectField };
            return ops;
        }

        /// <summary>
        /// Saves the item to the draft
        /// </summary>
        protected void SaveConfigItem()
        {
            EditWorkflowConfigUtilities.UpdateOperations(GetOperations(), false, m_protectedFieldsAreUpdateable.Checked, m_failureMessage.Text, m_tbNotes.Text, false, m_configItem);

            try
            {
                SaveAsDraft(m_data);
            }
            catch (ConfigValidationException exc)
            {
                foreach(string s in exc.ConditionErrors)
                    m_saveErrorMsg.Text += s + "<br />";
                foreach (string s in exc.ConstraintErrors)
                    m_saveErrorMsg.Text += s + "<br />";
                foreach (string s in exc.CustomVariableErrors)
                    m_saveErrorMsg.Text += s + "<br />";
            }
        }

        protected void SaveParameter(string paramName, AbstractConditionGroup conditionGroup)
        {
            // Determine what function we're using
            bool delete = false;
            E_FunctionT function = E_FunctionT.In;

            if (m_operationBetween.Checked)
                function = E_FunctionT.Between;
            else if (m_operationEqualInt.Checked)
                function = E_FunctionT.Equal;
            else if (m_operationEqual.Checked)
                function = E_FunctionT.Equal;
            else if (m_operationEqualVar.Checked)
                function = E_FunctionT.EqualVar;
            else if (m_operationIsNonblankStr.Checked)
                function = E_FunctionT.IsNonblankStr;
            else if (m_operationGreaterThan.Checked)
                function = E_FunctionT.IsGreaterThan;
            else if (m_operationGreaterThanVar.Checked)
                function = E_FunctionT.IsGreaterThanVar;
            else if (m_operationGreaterThanOrEqualTo.Checked)
                function = E_FunctionT.IsGreaterThanOrEqualTo;
            else if (m_operationGreaterThanOrEqualToVar.Checked)
                function = E_FunctionT.IsGreaterThanOrEqualToVar;
            else if (m_operationLessThan.Checked)
                function = E_FunctionT.IsLessThan;
            else if (m_operationLessThanVar.Checked)
                function = E_FunctionT.IsLessThanVar;
            else if (m_operationLessThanOrEqualTo.Checked)
                function = E_FunctionT.IsLessThanOrEqualTo;
            else if (m_operationLessThanOrEqualToVar.Checked)
                function = E_FunctionT.IsLessThanOrEqualToVar;
            else if (m_operationIsNonzeroValue.Checked)
                function = E_FunctionT.IsNonzeroValue;
            else if (m_operationIsAnyValue.Checked)
                function = E_FunctionT.IsAnyValue;
            else if (m_operationNDaysFromToday.Checked) // The selected date is N days from today
                function = E_FunctionT.Equal;

            string nameToUse = paramName;
            SecurityParameter.Parameter valueParameter;
            if (paramName.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
            {
                valueParameter = m_data.GetParameterWithValues(paramName);
            }
            else
            {
                SecurityParameter.Parameter passedInParameter = m_data.GetParameterFromFields(paramName);

                // Check if parameter is path. If it is parse out path name.
                valueParameter = passedInParameter;
                if (passedInParameter.Type == SecurityParameter.SecurityParameterType.PathableCollection)
                {
                    SecurityParameter.PathableCollectionParameter collParameter = (SecurityParameter.PathableCollectionParameter)passedInParameter;
                    StringBuilder pathBuilder = new StringBuilder();
                    pathBuilder.Append(DataPath.PathHeader);
                    pathBuilder.Append(collParameter.Id);
                    pathBuilder.Append("[");

                    // Get selection identifier.
                    switch (propertyLevel.SelectedValue)
                    {
                        case "INDIVIDUAL":
                            pathBuilder.Append(recordDropdown.SelectedValue);
                            break;
                        case "SUBSET":
                            pathBuilder.Append(DataPath.SubsetMarker);
                            pathBuilder.Append(subsetDropdown.SelectedValue);
                            break;
                        case "WHOLE":
                            pathBuilder.Append(DataPath.WholeCollectionIdentifier);
                            break;
                        default:
                            throw new UnhandledCaseException(propertyLevel.SelectedValue);
                    }

                    pathBuilder.Append("].");

                    // Get property field.
                    pathBuilder.Append(propertyDropdown.SelectedValue);

                    nameToUse = pathBuilder.ToString();
                    valueParameter = collParameter.FieldParameters[propertyDropdown.SelectedValue];
                }
            }

            // Get the ParameterValueType
            E_ParameterValueType valueType = SystemConfigValidator.ParamTypeToValueType(valueParameter);
            if (valueParameter.Type == SecurityParameter.SecurityParameterType.Enum) // Handle enums
            {
                SecurityParameter.EnumeratedParmeter enumParameter = valueParameter as SecurityParameter.EnumeratedParmeter;

                function = E_FunctionT.In;
                if (valueParameter.Source == SecurityParameter.E_FieldSource.Condition)
                {
                    delete = m_operationDontCare.Checked;
                }
                else if (enumParameter.HasNotInOption)
                {
                    delete = m_InAndNotInEnumRepeater.Items.Cast<RepeaterItem>().All(ri => ((RadioButtonList)ri.FindControl("radioList")).SelectedValue == "NA");
                }
                else
                {
                    delete = m_valueEnum.Items.FindByText(DONT_CARE_STRING).Selected;
                }
                SecurityParameter.EnumParamType enumType = enumParameter.BaseType;
                if (enumType == SecurityParameter.EnumParamType.Int)
                    valueType = E_ParameterValueType.Int;
                else if (enumType == SecurityParameter.EnumParamType.String)
                    valueType = E_ParameterValueType.String;
            }
            else
            {
                delete = m_operationDontCare.Checked;
            }

            // Create the parameter with the desired values
            ConfigSystem.Parameter parameter = new ConfigSystem.Parameter(nameToUse, function, valueType, false, false, valueParameter.Scope);


            if (function == E_FunctionT.In)
            {
                SecurityParameter.EnumeratedParmeter enumParameter = valueParameter as SecurityParameter.EnumeratedParmeter;
                if (valueParameter.Source == SecurityParameter.E_FieldSource.Condition) // Add the selected checkbox to the parameter
                {
                    parameter.AddValue(m_valueConditionCategory.SelectedItem.Text, m_valueConditionCategory.SelectedItem.Value);
                }
                else if (enumParameter != null && enumParameter.HasNotInOption)
                {
                    int count = 0;
                    foreach (RepeaterItem item in m_InAndNotInEnumRepeater.Items)
                    {
                        RadioButtonList radioList = item.FindControl("radioList") as RadioButtonList;
                        Label name = item.FindControl("name") as Label;
                        HiddenField value = item.FindControl("value") as HiddenField;

                        if (radioList.SelectedValue == "IS")
                        {
                            parameter.AddValue(value.Value, name.Text);
                            count++;
                        }
                        else if (radioList.SelectedValue == "IS_NOT")
                        {
                            parameter.AddExcption(value.Value, name.Text);
                            count++;
                        }
                    }

                    if (count == 0)
                    {
                        delete = true;
                    }
                }
                else  // Add any checked enum values to the parameter
                {
                    int count = 0;
                    foreach (ListItem cb in m_valueEnum.Items)
                    {
                        if (cb.Selected)
                        {
                            parameter.AddValue(cb.Value, cb.Text);
                            count++;
                        }
                    }
                    if ((count >= m_valueEnum.Items.Count - 1 && valueParameter.CategoryName != "Groups") || count == 0)
                        delete = true;
                }
            }
            else if (m_valueTextboxRb.Checked) // Add the text to the parameter
            {
                string text = m_valueTextbox1.Text;
                if (m_operationNDaysFromToday.Checked)
                {
                    text = string.Format("today{0}{1}", m_operationNDaysFromTodayOperator.SelectedValue, m_valueTextbox1.Text);
                }
                parameter.AddValue(text, text);
                if (function == E_FunctionT.Between)
                {
                    try
                    {
                        parameter.AddValue(m_valueTextbox2.Text, m_valueTextbox2.Text);
                    }
                    catch (ArgumentException exc)
                    {
                        m_valueErrorMsg.Text = exc.Message;
                        m_hiddenCmd.Value = "saveParameterFailed";
                        m_data = GetConfig();
                        return;
                    }
                }
            }
            else if (m_valueVarRb.Checked) // Add the picked value to the parameter
                parameter.AddValue(m_hiddenVariableName.Value, m_hiddenVariableName.Value);
            else if (m_valueBoolRb.Checked) // Add the bool to the parameter
                parameter.AddValue(m_valueTrue.Checked.ToString(), m_valueTrue.Checked.ToString());

            // Apply our changes to the condition
            EditWorkflowConfigUtilities.UpdateOperations(GetOperations(), false, m_protectedFieldsAreUpdateable.Checked, m_failureMessage.Text, m_tbNotes.Text, false, conditionGroup);
            if (conditionGroup.ParameterSet.GetParameter(nameToUse) != null)
            {
                if (delete)
                    conditionGroup.ParameterSet.RemoveParameter(parameter.Name);
                else
                    conditionGroup.ParameterSet.ReplaceParameter(parameter.Name, parameter);
            }
            else
            {
                if (!delete)
                    conditionGroup.ParameterSet.AddParameter(parameter);
            }

            if (isAdd && !delete)
            {
                m_data.AddNew(conditionGroup);
            }
            else if(!isAdd)
            {
                m_data.Edit(conditionGroup.Id, conditionGroup);
            }

            // Then save the config information
            SaveToCache(delete);
        }

        private void SaveToCache(bool delete)
        {
            try
            {
                m_cacheId = m_data.SaveToCache();
                if (isAdd && !delete) // Save the ID for use later
                {
                    m_hiddenId.Value = m_configItem.Id.ToString();
                    m_hiddenAction.Value = "edit";
                }
            }
            catch (ConfigValidationException exc)
            {
                foreach (string s in exc.ConditionErrors)
                    m_valueErrorMsg.Text += s + "<br />";
                foreach (string s in exc.ConstraintErrors)
                    m_valueErrorMsg.Text += s + "<br />";
                foreach (string s in exc.CustomVariableErrors)
                    m_valueErrorMsg.Text += s + "<br />";
                m_hiddenCmd.Value = "saveParameterFailed";
                m_data = GetConfig();
            }
            catch (XmlException exc)
            {
                m_valueErrorMsg.Text += exc.Message + "<br />";
                m_hiddenCmd.Value = "saveParameterFailed";
                m_data = GetConfig();
            }

            clearValues();
        }

        protected void SaveAsDraft(WorkflowSystemConfigModel toBeSaved)
        {
            if (!m_inDraftMode)
            {
                toBeSaved.SwitchToDraft();
                toBeSaved.Save();
                m_inDraftMode = true;
            }
            else
            {
                toBeSaved.Save();
            }
        }

        private void BindParameterPicker()
        {
            m_parameterList.DataSource = m_data.GetFieldsForUi();
            m_parameterList.DataBind();
            
            m_Group.DataSource = m_data.GetCategories();
            m_Group.DataBind();
            
            m_Page.DataSource = FieldEnumerator.PageNames;
            m_Page.DataTextField = "Value";
            m_Page.DataValueField = "Key";
            m_Page.DataBind();
            m_Page.Items.Insert(0, "");
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "PageFields", "var PageFields = " + FieldEnumerator.GetFieldPageJson() + ";", true);
        }

        private void BindProtectedFields()
        {
            // OnProtectedFieldsListBound
            // "{FF-FN} ({field.FriendlyName}) <category='{FF-CA}' value='{FF-ID}'>"
            m_protectedFieldsList.DataSource = EditWorkflowConfigUtilities.FilterOutExcludedFieldsForUi(EditWorkflowConfigUtilities.OperationFields.Union(EditWorkflowConfigUtilities.BrokerSpecificOperationFields(this.m_brokerId)));
            m_protectedFieldsList.DataBind();

            // Don't bind unless the user opens up the edit fields dialog
            m_protectedFieldsCategories.DataSource = EditWorkflowConfigUtilities.OperationCategories;
            m_protectedFieldsCategories.DataBind();

            m_protectedFieldsPage.DataSource = FieldEnumerator.PageNames;
            m_protectedFieldsPage.DataTextField = "Value";
            m_protectedFieldsPage.DataValueField = "Key";
            m_protectedFieldsPage.DataBind();
            m_protectedFieldsPage.Items.Insert(0, "");
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "PageFields", "var PageFields = " + FieldEnumerator.GetFieldPageJson() + ";", true);
        }

        protected void OnProtectedFieldsListBound(object sender, RepeaterItemEventArgs args)
        {
            EditWorkflowConfigUtilities.OnOperationFieldsListBound(sender, args);
        }

        protected void OnProtectedFieldsListLoad(object sender, EventArgs e)
        {
            EditWorkflowConfigUtilities.OnOperationFieldsListLoad(sender, e, m_configItem, WorkflowOperations.ProtectField);
        }

        /// <summary>
        /// Bind Enums that offer Is and Is Not options.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void OnInAndNotInEnumRepeater(object sender, RepeaterItemEventArgs args)
        {
            SecurityParameter.EnumMapping map = args.Item.DataItem as SecurityParameter.EnumMapping;

            Label name = args.Item.FindControl("name") as Label;
            HiddenField value = args.Item.FindControl("value") as HiddenField;

            name.Text = map.FriendlyValue;
            value.Value = map.RepValue;
        }

        /// <summary>
        /// Parameters that show up on the main display.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void OnParameterBound(object sender, RepeaterItemEventArgs args)
        {
            string pName = args.Item.DataItem as string;

            EncodedLiteral ParameterName = args.Item.FindControl("m_parameterName") as EncodedLiteral;
            PassthroughLiteral ParameterValue = args.Item.FindControl("m_parameterValue") as PassthroughLiteral;
            LinkButton ParameterEdit = args.Item.FindControl("m_parameterEdit") as LinkButton;

            ParameterSet parameterSet;
            if (m_configItem != null)
                parameterSet = m_configItem.ParameterSet;
            else
                parameterSet = new ParameterSet();

            ConfigSystem.Parameter parameter = parameterSet.GetParameter(pName);
            ParameterEdit.Attributes.Add("onclick", "onEditParameter('" + pName + "'); return false;");
            ParameterName.Text = m_data.GetFriendlyName(pName);

            if (parameter != null)
                ParameterValue.Text = m_data.GetOperandRep(parameter);
        }

        /// <summary>
        /// Parameters for the parameter selection list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void OnParameterListBound(object sender, RepeaterItemEventArgs args)
        {
            EditWorkflowConfigUtilities.OnParameterListBound(sender, args, m_hiddenCmd.Value, m_hiddenParameterName.Value, filterType);
        }

        private void SelectPathProperty(string parameterName)
        {
            ClearOperationsSection();
            SecurityParameter.PathableCollectionParameter collParameter = m_data.GetParameterFromFields(parameterName) as SecurityParameter.PathableCollectionParameter;
            PrepareParameter(collParameter.FieldParameters[propertyDropdown.SelectedValue]);
        }
        protected void PathableFieldPermissionsList_RowBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
            {
                return;
            }

            OperationField field = (OperationField)e.Row.DataItem;

            HtmlAnchor removeLink = (HtmlAnchor)e.Row.FindControl("removeLink");
            HtmlInputHidden fieldId = (HtmlInputHidden)e.Row.FindControl("fieldId");
            EncodedLabel friendlyName = (EncodedLabel)e.Row.FindControl("friendlyName");

            fieldId.Value = field.Id;
            friendlyName.Text = field.FriendlyName;

            removeLink.Attributes.Add("onclick", $"onRemoveWritableCollection({AspxTools.JsString(field.Id)});");
        }
    }
}