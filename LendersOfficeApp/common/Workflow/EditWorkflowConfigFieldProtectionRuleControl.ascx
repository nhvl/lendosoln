﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditWorkflowConfigFieldProtectionRuleControl.ascx.cs" Inherits="LendersOfficeApp.common.Workflow.EditWorkflowConfigFieldProtectionRuleControl" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<input type="hidden" value="" runat="server" id="m_hiddenId" />
<input type="hidden" value="" runat="server" id="m_hiddenAction" />
<input type="hidden" value="" runat="server" id="m_hiddenParameterName" />
<input type="hidden" value="" runat="server" id="m_hiddenVariableName" />
<input type="hidden" value="" runat="server" id="m_hiddenCmd" />
<input type="hidden" value="" runat="server" id="m_hiddenEditorMode" />
<input type="hidden" value="" runat="server" id="m_hiddenDraftMode" />
<input type="hidden" value="" runat="server" id="m_hiddenKey" />
<input type="hidden" value="" runat="server" id="PathableFieldPermissionsEditor_Path" />
<input type="hidden" value="" runat="server" id="PathableFieldPermissionsEditor_FriendlyPath" />
<script type="text/javascript">
    function _initControl() {
        // Determine what to display based on what operation has been set
        //   from a previous postback
        processHiddenCmd();

        document.getElementById('m_hiddenCmd').value = '';
        resize(1000, 700);
            
        var protectedFieldsSelectionList = createSelectionList(
            $('#protectedFieldsSearchCriteria'),
            $('#m_protectedFieldsCategories'),
            $('#m_protectedFieldsPage'),
            $('#m_protectedFieldsListContainer'));
        $('#m_enableProtectedFieldsSearch').on("click", function() {
            defaultCategory(protectedFieldsSelectionList);
            displaySearchResults(false, protectedFieldsSelectionList);
        });

        $('#protectedFieldsSearchCriteria').on("focus", function() {
            defaultCategory(protectedFieldsSelectionList);
            $('#m_enableprotectedFieldsSearch').prop('checked', true);
        }).on("keyup", function() {
            $('#m_enableProtectedFieldsSearch').prop('checked', true);
            displaySearchResults(false, protectedFieldsSelectionList);
        });

        $('#m_disableProtectedFieldsSearch').on("click", function() {
            displaySearchResults(true, protectedFieldsSelectionList);
        });

        $('#m_protectedFieldsCategories').on("change", function() {
            $('#m_disableProtectedFieldsSearch').prop('checked', true);
            jumpToSelected(protectedFieldsSelectionList);
        });

        $('#m_protectedFieldsPage').on("change", function() {
            $('#m_searchProtectedFieldsPage').prop('checked', true);
            jumpToPage(protectedFieldsSelectionList);
        });

        //
        var parameterSelectionList = createSelectionList(
            $('#searchCriteria'),
            $('#m_Group'),
            $('#m_Page'),
            $('#m_FieldList'));
        $('#m_enableSearch').on("click", function() {
            defaultCategory(parameterSelectionList);
            displaySearchResults(false, parameterSelectionList);
        });

        $('#searchCriteria').on("focus", function() {
            defaultCategory(parameterSelectionList);
            $('#m_enableSearch').prop('checked', true);
        }).on("keyup", function() {
            $('#m_enableSearch').prop('checked', true);
            displaySearchResults(false, parameterSelectionList);
        });

        $('#m_disableSearch').on("click", function () {
            displaySearchResults(true, parameterSelectionList);
        });

        $('#m_Group').on("change", function() {
            $('#m_disableSearch').prop('checked', true);
            jumpToSelected(parameterSelectionList);
        });

        $('#m_Page').on("change", function() {
            $('#m_searchPage').prop('checked', true);
            jumpToPage(parameterSelectionList);
        });
    }

    function triggerParentPostback() {
        if (window && window.opener && typeof (window.opener.performPostback) == "function") {
            window.opener.performPostback();
        }
    }
</script>
<table>
    <tr>
        <td style="background-color:#00BB00" class="Header" colspan="3">
            <ml:EncodedLiteral ID="m_pageHeader" runat="server"></ml:EncodedLiteral>
        </td>
    </tr>
    <tr>
        <td style="background-color:#666666" class="Header" colspan="3">
            Parameters
        </td>
    </tr>
    <asp:Repeater runat="server" ID="m_parameterSet" OnItemDataBound="OnParameterBound">
        <ItemTemplate>
            <tr class="GridItem">
                <td>
                    <ml:EncodedLiteral ID="m_parameterName" runat="server"></ml:EncodedLiteral>
                </td>
                <td>
                    <ml:PassthroughLiteral ID="m_parameterValue" runat="server"></ml:PassthroughLiteral>                    
                </td>
                <td>
                    <asp:LinkButton ID="m_parameterEdit" runat="server" Text="edit"></asp:LinkButton>
                </td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="GridAlternatingItem">
                <td>
                    <ml:EncodedLiteral ID="m_parameterName" runat="server"></ml:EncodedLiteral>
                </td>
                <td>
                    <ml:PassthroughLiteral ID="m_parameterValue" runat="server"></ml:PassthroughLiteral>                    
                </td>
                <td>
                    <asp:LinkButton ID="m_parameterEdit" runat="server" Text="edit"></asp:LinkButton>
                </td>
            </tr>
        </AlternatingItemTemplate>
    </asp:Repeater>
    <tr>
        <td colspan="3">
            <input type="button" id="m_addParameterBtn" value="Add Parameter" onclick="onAddParameter();" runat="server" />
        </td>
    </tr>
    <tbody id="m_protectedFieldsSection" runat="server">
    <tr>
        <td style="background-color:#666666" class="Header" colspan="3">
            Protected Fields
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <div class="FieldList">
                <asp:GridView ID="m_protectedFieldsListMainDisplay" CssClass="FieldListItem" runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="String Value">
                            <HeaderTemplate></HeaderTemplate>
                            <HeaderStyle CssClass="FieldListItem" />
                            <ItemStyle CssClass="FieldListItem" />
                            <ItemTemplate>
                                <%# AspxTools.HtmlString(Container.DataItem.ToString())%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <input type="button" id="m_editFieldsBtn" value="Edit Fields" runat="server" onclick="onPickProtectedField();"/>
        </td>
    </tr>
    <tr>
        <td>
            <input type="checkbox" id="m_protectedFieldsAreUpdateable" runat="server" />Protected dates and text fields can be updated but not cleared
        </td>
    </tr>
    </tbody>

    <tbody id="pathableFieldPermissionsSection" runat="server">
        <tr>
            <td id="pathableFieldPermissionsHeader" class="Header" colspan="4">
                Protected Collections
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div class="FieldList">
                    <asp:GridView ID="PathableFieldPermissionsList" CssClass="FieldListItem" runat="server" AutoGenerateColumns="false" OnRowDataBound="PathableFieldPermissionsList_RowBound"  ShowHeader="false">
                        <Columns>
                            <asp:TemplateField>
                                <ItemStyle CssClass="FieldListItem" />
                                <ItemTemplate>
                                    <a id="removeLink" runat="server">remove</a>
                                    <input type="hidden" id="fieldId" runat="server" />
                                    <ml:EncodedLabel ID="friendlyName" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" id="btnEditCollections" value="Add Collections" runat="server" onclick="onPickWritableCollection();"/>
            </td>
        </tr>
    </tbody>

    <tr>
        <td style="background-color:#666666" class="Header" colspan="3">
            Save Denial Message
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:TextBox ID="m_failureMessage" Width="300px" Rows="6" runat="server" TextMode="MultiLine"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="background-color:#666666" class="Header" colspan="3">
            Notes
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:TextBox ID="m_tbNotes" Width="300px" Rows="6" runat="server" TextMode="MultiLine"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:Label ID="m_saveErrorMsg" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <input type="button" value="Save" onclick="onSaveConfigItem();" id="m_saveItemBtn" runat="server" />
            <input type="button" value="Cancel" onclick="window.close();" />
        </td>
    </tr>
</table>
    
<div id="ProtectedFieldsPopup" class="modal" runat="server">
    <table id="m_fieldPicker" runat="server" border="0" cellspacing="0" cellpading="2">
        <tr>
            <th class="FormTableHeader" colspan="2">
                Select Protected Fields
            </th>
        </tr>
        <tr>
            <td class="FieldLabel" nowrap>
                <%-- Click/focus/change handlers for these pickers are defined up in the $(document).ready --%>
                <input type="radio" id="m_enableProtectedFieldsSearch" name="protectedFieldsSearchOption" checked />
                Search by Field Name or ID
                <input type="text" id="protectedFieldsSearchCriteria" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" colspan="2" nowrap>
                <input type="radio" id="m_disableProtectedFieldsSearch" name="protectedFieldsSearchOption" />
                Jump to category
                <ASP:DropDownList id="m_protectedFieldsCategories" runat="server" class="SearchField">
				</ASP:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" colspan="2" nowrap>
                <input type="radio" id="m_searchProtectedFieldsPage" name="protectedFieldsSearchOption" />
                Jump to Page
                <asp:DropDownList id="m_protectedFieldsPage" runat="server" class="SearchField"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <div id="m_protectedFieldsListContainer" style="height:200px; width:650px; overflow:auto; padding:3px; border:solid 1px black">
                    <asp:Repeater ID="m_protectedFieldsList" EnableViewState="false" runat="server" OnItemDataBound="OnProtectedFieldsListBound" OnLoad="OnProtectedFieldsListLoad">
                        <ItemTemplate>
                            <div id="m_choice" runat="server">
                                <asp:CheckBox ID="m_cb" runat="server"></asp:CheckBox>
                                <ml:EncodedLiteral ID="m_pData" runat="server"></ml:EncodedLiteral>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" id="m_protectedFieldsSaveBtn" value="Save" onclick="onSaveProtectedFields();" />
                <input type="button" value="Cancel" onclick="Modal.Hide();" />
            </td>
        </tr>
    </table>
</div>
    
<div id="ParameterPickerPopup" class="modal" runat="server">
    <table id="m_parameterPicker" runat="server" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <th class="FormTableHeader" colspan="2">
                Select Parameter
            </th>
        </tr>
        <tr>
            <td class="FieldLabel" nowrap>
                <%-- Click/focus/change handlers for these pickers are defined up in the $(document).ready --%>
                <input type="radio" id="m_enableSearch" name="searchOption" checked />
                Search by Field Name or ID
                <input type="text" id="searchCriteria" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" colspan="2" nowrap>
                <input type="radio" id="m_disableSearch" name="searchOption" />
                Jump to category
                <asp:DropDownList id="m_Group" runat="server" class="SearchField"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" colspan="2" nowrap>
                <input type="radio" id="m_searchPage" name="searchOption" />
                Jump to Page
                <asp:DropDownList id="m_Page" runat="server" class="SearchField"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="m_FieldList" style="height:200px; width:650px; overflow:auto; padding:3px; border:solid 1px black">
                    <asp:Repeater runat="server" ID="m_parameterList" OnItemDataBound="OnParameterListBound" EnableViewState="false">
                        <ItemTemplate>
                            <div id="m_choice" runat="server">
                                <a id="m_selectLink" runat="server" href="#">select</a>
                                <ml:EncodedLiteral ID="m_pData" runat="server"></ml:EncodedLiteral>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <input type="button" value="Cancel" onclick="Modal.Hide();" />
            </td>
        </tr>
    </table>
        
    <table id="m_parameterEditor" style="display:none" runat="server" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <td>
                Parameter: 
            </td>
            <td>
                <ml:EncodedLiteral ID="m_parameterName" runat="server"></ml:EncodedLiteral>
            </td>
        </tr>

        <tbody id="parameterPathSection">
            <tr id="propertyLevelRow" runat="server">
                <td>
                    Property Level:
                </td>
                <td>
                    <asp:RadioButtonList ID="propertyLevel" runat="server" RepeatDirection="Horizontal" onchange="onSelectPathPropertyLevel();">
                        <asp:ListItem Text="Individual" Value="INDIVIDUAL" />
                        <asp:ListItem Text="Subset" Value="SUBSET" />
                        <asp:ListItem Text="Whole" Value="WHOLE" />
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr id="subsetRow" runat="server">
                <td>
                    Subset Description:
                </td>
                <td>
                    <asp:DropDownList id="subsetDropdown" runat="server" />
                </td>
            </tr>
            <tr id="recordRow" runat="server">
                <td>
                    Record Description:
                </td>
                <td>
                    <asp:DropDownList id="recordDropdown" runat="server" />
                </td>
            </tr>
            <tr id="propertyRow" runat="server">
                <td>
                    <ml:EncodedLiteral id="propertyDescription" runat="server" /> Property:
                </td>
                <td>
                    <asp:DropDownList id="propertyDropdown" runat="server" onchange="onSelectPathProperty();" />
                </td>
            </tr>
        </tbody>

        <tbody id="m_parameterOperationsSection">
        <tr>
            <td valign="top">
                Operation:
            </td>
            <td>
                <asp:RadioButton ID="m_operationBetween" CssClass="RadioOption" Text="Between" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
                <asp:RadioButton ID="m_operationEqualInt" CssClass="RadioOption" Text="Equal" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
                <asp:RadioButton ID="m_operationEqual" CssClass="RadioOption" Text="Equal" runat="server" GroupName="m_ops" onclick="setValueType('bool');" />
                <asp:RadioButton ID="m_operationEqualConditionCategory" CssClass="RadioOption" Text="Equal" runat="server" GroupName="m_ops" onclick="setValueType('conditionCategory');" />
                <asp:RadioButton ID="m_operationEqualVar" CssClass="RadioOption" Text="Equal to variable" runat="server" GroupName="m_ops" onclick="setValueType('var');" />
                <asp:RadioButton ID="m_operationIsNonblankStr" CssClass="RadioOption" Text="Is not a blank string" runat="server" GroupName="m_ops" onclick="setValueType('bool');" />
                <asp:RadioButton ID="m_operationGreaterThan" CssClass="RadioOption" Text="Greater Than" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
                <asp:RadioButton ID="m_operationGreaterThanVar" CssClass="RadioOption" Text="Greater than variable" runat="server" GroupName="m_ops" onclick="setValueType('var');" />
                <asp:RadioButton ID="m_operationGreaterThanOrEqualTo" CssClass="RadioOption" Text="Greater than or equal to" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
                <asp:RadioButton ID="m_operationGreaterThanOrEqualToVar" CssClass="RadioOption" Text="Greater than or equal to variable" runat="server" GroupName="m_ops" onclick="setValueType('var');" />
                <asp:RadioButton ID="m_operationLessThan" CssClass="RadioOption" Text="Less than" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
                <asp:RadioButton ID="m_operationLessThanVar" CssClass="RadioOption" Text="Less than variable" runat="server" GroupName="m_ops" onclick="setValueType('var');" />
                <asp:RadioButton ID="m_operationLessThanOrEqualTo" CssClass="RadioOption" Text="Less than or equal to" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
                <asp:RadioButton ID="m_operationLessThanOrEqualToVar" CssClass="RadioOption" Text="Less than or equal to variable" runat="server" GroupName="m_ops" onclick="setValueType('var');" />
                <asp:RadioButton ID="m_operationIsNonzeroValue" CssClass="RadioOption" Text="Is a nonzero value" runat="server" GroupName="m_ops" onclick="setValueType('bool');" />
                <asp:RadioButton ID="m_operationIsAnyValue" CssClass="RadioOption" Text="Is any value" runat="server" GroupName="m_ops" onclick="setValueType('bool');" />
                <asp:RadioButton ID="m_operationNDaysFromToday" CssClass="RadioOption" Text="N days from today" runat="server" GroupName="m_ops" onclick="setValueType('NDaysFromToday');" />
                <asp:RadioButton ID="m_operationDontCare" CssClass="RadioOption" Text="Don't Care" runat="server" GroupName="m_ops" onclick="setValueType('');" />
            </td>
        </tr>
        </tbody>
        <tbody id="m_parameterValuesSection">
        <tr>
            <td valign="top">
                Values:
            </td>
            <td>
                <asp:Label style="display:none" id="m_valueDesc" runat="server"></asp:Label>
                <asp:DropDownList style="display:none" ID="m_operationNDaysFromTodayOperator" runat="server">
                    <asp:ListItem Value="+" Text="+"></asp:ListItem>
                    <asp:ListItem Value="-" Text="-"></asp:ListItem>
                </asp:DropDownList>
                <asp:RadioButton ID="m_valueTextboxRb" GroupName="m_value" runat="server" style="display:none" />
                <asp:TextBox ID="m_valueTextbox1" runat="server" style="display:none" onkeyup="validateEditor();"></asp:TextBox>
                <asp:TextBox ID="m_valueTextbox2" runat="server" style="display:none" onkeyup="validateEditor();"></asp:TextBox>
                <asp:RadioButton ID="m_valueVarRb" GroupName="m_value" runat="server" style="display:none" />
                <asp:Label ID="m_valueVar" runat="server" style="display:none"></asp:Label>
                <a href="#" id="m_valueVarEdit" style="display:none" onclick="onPickVariable();">Pick variable</a>
                <asp:RadioButton ID="m_valueBoolRb" GroupName="m_value" runat="server" style="display:none" />
                <span id="m_valueTrueSpan" style="display:none" runat="server"><asp:RadioButton ID="m_valueTrue" Text="True" GroupName="m_valueBool" runat="server" onclick="validateEditor();" /></span>
                <span id="m_valueFalseSpan" style="display:none" runat="server"><asp:RadioButton ID="m_valueFalse" Text="False" GroupName="m_valueBool" runat="server" onclick="validateEditor();" /></span>
                <asp:DropDownList ID="m_valueConditionCategory" runat="server" style="display:none" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CheckBoxList ID="m_valueEnum" runat="server"></asp:CheckBoxList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Repeater ID="m_InAndNotInEnumRepeater" runat="server" OnItemDataBound="OnInAndNotInEnumRepeater">
                    <ItemTemplate>
                        <asp:RadioButtonList ID="radioList" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="InAndNotInList">
                            <asp:ListItem Text="Is" Value="IS" />
                            <asp:ListItem Text="Is Not" Value="IS_NOT" />
                            <asp:ListItem Text="N/A" Value="NA" Selected/>
                        </asp:RadioButtonList>
                        <asp:Label ID="name" runat="server" AssociatedControlID="radioList" CssClass="InAndNotInName" />
                        <asp:HiddenField ID="value" runat="server" />
                        <br />
                    </ItemTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="m_valueErrorMsg" runat="server"></asp:Label>
            </td>
        </tr>
        </tbody>
        <tr>
            <td>
                <input type="button" id="m_editorSaveBtn" value="Save" onclick="onSaveParameter();" />
                <input type="button" value="Cancel" onclick="Modal.Hide();" />
            </td>
        </tr>
    </table>
</div>

<div id="pathableFieldPermissionsEditor" class="modal">
    <table id="pathableFieldPermissionsTable" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <td colspan="2">
                Path: <span id="pathPermissions_pathSoFar" ></span>
            </td>
        </tr>
        <tr id="pathPermissionsCollectionRow">
            <td>
                Collection:
            </td>
            <td>
                <select id="pathPermissionsCollectionDropdown" class="updatePath" />
            </td>
        </tr>
        <tr id="pathPermissionsPropertyLevelRow">
            <td>
                Property Level:
            </td>
            <td>
                <input type="radio" id="pathPermissionsPropertyLevel_individual" name="pathPermissionsPropertyLevel" class="updatePath" value="INDIVIDUAL" /><label for="pathPermissionsPropertyLevel_individual">Individual</label>
                <input type="radio" id="pathPermissionsPropertyLevel_subset" name="pathPermissionsPropertyLevel" class="updatePath" value="SUBSET" /><label for="pathPermissionsPropertyLevel_subset">Subset</label>
                <input type="radio" id="pathPermissionsPropertyLevel_whole" name="pathPermissionsPropertyLevel" class="updatePath" value="WHOLE" /><label for="pathPermissionsPropertyLevel_whole">Whole</label>
            </td>
        </tr>
        <tr id="pathPermissionsSubsetRow">
            <td>
                Subset Description:
            </td>
            <td>
                <select id="pathPermissionsSubsetDropdown" class="updatePath" />
            </td>
        </tr>
        <tr id="pathPermissionsRecordRow">
            <td>
                Record Description:
            </td>
            <td>
                <select id="pathPermissionsRecordDropDown" class="updatePath" />
            </td>
        </tr>
        <tr id="pathPermissionsPropertyRow">
            <td>
                <span id="pathPermissionsPropertyDescription"></span> Property:
            </td>
            <td>
                <select id="pathPermissionsPropertyDropdown" class="updatePath" />
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" id="pathPermissionsSaveBtn" value="Save" onclick="onSaveWritableCollection();" />
                <input type="button" value="Cancel" onclick="Modal.Hide();" />
            </td>
        </tr>
    </table>
</div>
