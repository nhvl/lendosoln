﻿namespace LendersOfficeApp.common.Workflow
{
    using System;
    using System.Linq;
    using System.Web.UI;
    using CommonLib;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;

    public partial class EditEmployeeGroupControl : UserControl
    {
        protected int m_nEmployeeCount = 0;
        protected int m_nCurrentEmployeeIdx = 0;

        private bool IsInternal => PrincipalFactory.CurrentPrincipal is InternalUserPrincipal;

        protected Guid BrokerId
        {
            get
            {
                if (ViewState["brokerid"] == null)
                {
                    if (this.IsInternal)
                    {
                        ViewState["brokerid"] = RequestHelper.GetGuid("brokerid", Guid.Empty);
                    }
                    else
                    {
                        ViewState["brokerid"] = PrincipalFactory.CurrentPrincipal.BrokerId;
                    }
                }

                return (Guid)ViewState["brokerid"];
            }
            set
            {
                ViewState["brokerid"] = value;
            }
        }

        protected Guid GroupId
        {
            get
            {
                if (ViewState["groupId"] == null)
                    ViewState["groupId"] = RequestHelper.GetGuid("groupId", Guid.Empty);
                return (Guid)ViewState["groupId"];
            }
            set
            {
                ViewState["groupId"] = value;
            }
        }

        private string Name
        {
            get
            {
                if (ViewState["name"] == null)
                {
                    return string.Empty;
                }

                return SafeConvert.ToString(ViewState["name"], false);  // OPM 328493 - Do not trim here.
            }
            set
            {
                ViewState["name"] = value;
            }
        }

        private string Description
        {
            get
            {
                if (ViewState["description"] == null)
                {
                    return string.Empty;
                }

                return SafeConvert.ToString(ViewState["description"], false);  // OPM 328493 - Do not trim here.
            }
            set
            {
                ViewState["description"] = value;
            }
        }

        private new BasePage Page
        {
            get { return (BasePage)base.Page; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.ClientIDMode = ClientIDMode.Static;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (GroupId != Guid.Empty)
                {
                    Group group = GroupDB.GetGroupById(BrokerId, GroupId);
                    Name = group.Name.Trim();
                    Description = group.Description.Trim();

                    TextBoxName.Text = Name;
                    TextBoxDescription.Text = Description;
                }
            }
            if (Page.IsPostBack)
            {
                bool bCloseClientWnd = close_after_cmd.Value == "true";

                if (command_name.Value == "update")
                {

                    //Save group
                    string sName = TextBoxName.Text.Trim();
                    string sDescription = TextBoxDescription.Text.Trim();

                    if (Name != sName && GroupDB.IfGroupNameExist(BrokerId, GroupType.Employee, sName))
                    {                        
                        AlertUser("Group name already exists. Please use a different name.");
                    }
                    else
                    {

                        //if any attributes of the group change, update the group                    
                        bool bAttributesChanged = (Name != sName) || (sDescription != Description);
                        bool bIsNew = (GroupId == Guid.Empty);

                        if (bAttributesChanged)
                        {

                            if (bIsNew)
                            {
                                GroupId = GroupDB.CreateGroup(BrokerId, sName, GroupType.Employee, sDescription, PrincipalFactory.CurrentPrincipal);
                                AlertUser("The group has been created.", bCloseClientWnd);
                            }
                            else
                            {
                                GroupDB.UpdateGroup(BrokerId, (AbstractUserPrincipal)Page.User, GroupId, "UserGroup", Name, sName, Description, sDescription);
                                AlertUser("Your changes have been saved.", bCloseClientWnd);
                            }
                            Description = sDescription;
                            Name = sName;
                        }

                        string sSelectedEmployees = selected_employees.Value;
                        if (sSelectedEmployees != "none")
                        {
                            GroupDB.UpdateGroupMembership(BrokerId, GroupId, sSelectedEmployees, PrincipalFactory.CurrentPrincipal);
                            if (!bIsNew && !bAttributesChanged)
                            {
                                AlertUser("Your changes have been saved.", bCloseClientWnd);
                            }
                        }

                        if (bCloseClientWnd)
                        {
                            string sScript = @"<script type='text/javascript'>
                            /*[CDATA[*/                                
                                         window.close();
                            /*]]*/
                            </script>";
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "Updated", sScript);
                        }
                    }
                    selected_employees.Value = "none";
                    command_name.Value = string.Empty;
                }
            }

            var list = GroupDB.GetEmployeesWithGroupMembership(BrokerId, GroupId);
            grdData.DataSource = list;
            grdData.DataBind();
            m_nEmployeeCount = list.Count();
                      
            divMessage.InnerText = (m_nEmployeeCount == 0) ? "No Records Found" : string.Empty;
            lblTitle.Text = (GroupId == Guid.Empty) ? "Add Employee Group" : "Edit Employee Group";
        }
        private void AlertUser(string message)
        {
            AlertUser(message, false);
        }
        private void AlertUser(string message, bool bCloseWindow)
        {
            string sScript = @"<script type='text/javascript'>
/*[CDATA[*/
    alert('" + message + @"');" +
             (bCloseWindow ? "onClosePopup();" : string.Empty) + @"/*]]*/
</script>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Updated", sScript);
        }
    }
}
