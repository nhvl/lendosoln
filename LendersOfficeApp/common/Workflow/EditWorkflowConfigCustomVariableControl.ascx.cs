﻿namespace LendersOfficeApp.common.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Xml;

    using ConfigSystem;
    using DataAccess;
    using DataAccess.PathDispatch;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LendersOfficeApp.LOAdmin.TaskBackendUtilities;
    using LqbGrammar.DataTypes.PathDispatch;
    using MeridianLink.CommonControls;

    public partial class EditWorkflowConfigCustomVariableControl : UserControl
    {
        private bool IsInternal => PrincipalFactory.CurrentPrincipal is InternalUserPrincipal;

        protected Guid m_brokerId
        {
            get
            {
                if (this.IsInternal)
                {
                    return RequestHelper.GetGuid("brokerid", ConstAppDavid.SystemBrokerGuid);
                }

                return PrincipalFactory.CurrentPrincipal.BrokerId;
            }
        }

        protected bool m_inDraftMode
        {
            get
            {
                return Convert.ToBoolean(m_hiddenDraftMode.Value);
            }
            set
            {
                m_hiddenDraftMode.Value = value.ToString();
            }
        }

        protected Guid m_cacheId
        {
            get
            {
                return new Guid(m_hiddenKey.Value);
            }
            set
            {
                m_hiddenKey.Value = value.ToString();
            }
        }

        protected Guid m_itemId
        {
            get
            {
                if (isAdd)
                    return new Guid();
                else
                    return new Guid(m_hiddenId.Value);
            }
        }

        protected string m_action
        {
            get
            {
                return m_hiddenAction.Value;
            }
        }

        protected bool isAdd
        {
            get
            {
                return m_action == "add";
            }
        }

        protected string m_type
        {
            get
            {
                return RequestHelper.GetSafeQueryString("type");
            }
        }

        protected bool isCondition
        {
            get
            {
                return m_type == "Condition";
            }
        }

        protected bool isConstraint
        {
            get
            {
                return m_type == "Constraint";
            }
        }

        protected bool isCustomVariable
        {
            get
            {
                return m_type == "CustomVariable";
            }
        }

        protected WorkflowSystemConfigModel data
        {
            get;
            set;
        }

        protected string parameterName
        {
            get
            {
                return m_hiddenParameterName.Value;
            }
        }

        protected SecurityParameter.SecurityParameterType filterType
        {
            get;
            set;
        }

        protected bool isGlobalConfig
        {
            get { return RequestHelper.GetBool("isGlobal"); }
        }

        protected const string DONT_CARE_STRING = "Don't Care";

        private new BasePage Page
        {
            get { return (BasePage)base.Page; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.ClientIDMode = ClientIDMode.Static;
            Page.EnableJqueryMigrate = false;
            Page.RegisterCSS("Workflow/EditWorkflowConfigCustomVariable.css");
            Page.RegisterJsScript("LOAdmin.Broker.WorkflowOperations.js");
            Page.RegisterJsScript("Workflow/EditWorkflowConfig.js");
            Page.RegisterJsScript("utilities.js");
            Page.ClientScript.GetPostBackEventReference(this, string.Empty);

            if (!Page.IsPostBack)
            {
                m_hiddenId.Value = RequestHelper.GetSafeQueryString("itemId");
                m_hiddenAction.Value = RequestHelper.GetSafeQueryString("action");
                m_hiddenDraftMode.Value = RequestHelper.GetSafeQueryString("isDraft");
                m_cacheId = Guid.Empty;

                m_pageHeader.Text = (isAdd ? "Add " : "Edit ");
                m_pageHeader.Text += "Trigger";
            }
        }

        protected WorkflowSystemConfigModel GetConfig()
        {
            if(m_cacheId != Guid.Empty)
                return new WorkflowSystemConfigModel(m_brokerId, m_inDraftMode, m_cacheId);
            else
                return new WorkflowSystemConfigModel(m_brokerId, m_inDraftMode);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            m_valueErrorMsg.Text = "";
            m_saveErrorMsg.Text = "";
            data = GetConfig();
                
            if (!String.IsNullOrEmpty(m_hiddenCmd.Value))
            {
                switch (m_hiddenCmd.Value)
                {
                    case "saveParameter":
                        saveParameter();
                        break;
                    case "selectParameter":
                        PrepareParameterEditor();
                        break;
                    case "pickParameter":
                        filterType = data.GetParameterFromFields(parameterName).Type;
                        if (filterType == SecurityParameter.SecurityParameterType.PathableCollection)
                        {
                            SecurityParameter.PathableCollectionParameter collParameter = data.GetParameterFromFields(parameterName) as SecurityParameter.PathableCollectionParameter;
                            filterType = collParameter.FieldParameters[propertyDropdown.SelectedValue].Type;
                        }
                        break;
                    case "editParameter":
                        editParameter();
                        break;
                    case "selectPathProperty":
                        SelectPathProperty(parameterName);
                        break;
                    case "selectPathPropertyLevel":
                        SelectPathPropertyLevel();
                        break;
                    case "saveConfigItem":
                        saveConfigItem();
                        break;
                    default:
                        break;
                }
            }

            if (!Page.IsPostBack && isAdd != true)
            {
                CustomVariable customVar = data.GetCustomVariable(m_itemId);
                m_customVariableName.Text = customVar.Name;
                m_tbNotes.Text = customVar.Notes;
            }

            if (!isAdd)
            {
                m_parameterSet.DataSource = data.GetNewCustomVariableParameters(m_itemId);
                m_parameterSet.DataBind();
                m_customVariableName.ReadOnly = true;
            }
            
            m_Group.DataSource = data.GetCategories();
            m_Group.DataBind();
            m_parameterList.DataSource = data.GetFieldsForUi();
            m_parameterList.DataBind();
            m_Page.DataSource = FieldEnumerator.PageNames;
            m_Page.DataTextField = "Value";
            m_Page.DataValueField = "Key";
            m_Page.DataBind();
            m_Page.Items.Insert(0, "");

            if (propertyLevelRow.Visible)
            {
                SetRadioButtonVisibility();
            }

            validateButtons();

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "PageFields", "var PageFields = " + FieldEnumerator.GetFieldPageJson() + ";", true);

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
        }

        protected void validateButtons()
        {
        }

        protected void clearValues()
        {
            m_parameterName.Text = "";
            m_valueTextboxRb.Checked = false;
            m_valueVarRb.Checked = false;
            m_valueBoolRb.Checked = false;
            m_valueTextbox1.Text = "";
            m_valueTextbox2.Text = "";
            m_hiddenVariableName.Value = "";
            m_valueVar.Text = "";
            m_valueTrue.Checked = false;
            m_valueFalse.Checked = false;
            m_hiddenEditorMode.Value = "";
        }

        protected void loadConditionCategoryValues(ConfigSystem.Parameter p)
        {
            m_operationEqualConditionCategory.Checked = true;
            Tools.SetDropDownListValue(m_valueConditionCategory, p.Values.First());
            m_hiddenEditorMode.Value = "conditionCategory";
        }

        protected void loadEnumValues(ConfigSystem.Parameter p)
        {
            foreach (string val in p.Values)
            {
                ListItem item = m_valueEnum.Items.FindByValue(val);

                // For Enums, if value is not found (because it's been removed) DO NOT THROW.
                // Add bad value to the list. Force the user to clean it up before save.
                if (item == null)
                {
                    item = new ListItem(p.ValueDesc[val], val);
                    m_valueEnum.Items.Add(item);
                }

                item.Selected = true;
            }
        }

        protected void LoanInAndNotInEnumValues(ConfigSystem.Parameter p)
        {
            foreach (RepeaterItem item in m_InAndNotInEnumRepeater.Items)
            {
                RadioButtonList radioList = item.FindControl("radioList") as RadioButtonList;
                HiddenField value = item.FindControl("value") as HiddenField;

                if (p.Values.Contains(value.Value))
                {
                    radioList.SelectedValue = "IS";
                }
                else if (p.Exceptions.Contains(value.Value))
                {
                    radioList.SelectedValue = "IS_NOT";
                }
                else
                {
                    radioList.SelectedValue = "NA";
                }
            }
        }

        protected void loadTextboxValues(ConfigSystem.Parameter p)
        {
            // First, check if it's a date offset comparison
            string text = p.Values.ElementAt(0);
            var match = Regex.Match(text, @"^today([+-])([0-9]+)$");
            if (match.Success)
            {
                m_operationNDaysFromToday.Checked = true;
                m_operationNDaysFromTodayOperator.SelectedValue = match.Groups[1].Value;
                m_valueTextbox1.Text = match.Groups[2].Value;
                m_hiddenEditorMode.Value = "NDaysFromToday";
            }
            else // otherwise it's something else
            {
                m_valueTextbox1.Text = p.Values.ElementAt(0);
                m_hiddenEditorMode.Value = "textbox";
            }
            if (p.Values.Count > 1)
                m_valueTextbox2.Text = p.Values.ElementAt(1);

        }

        protected void loadBooleanValues(ConfigSystem.Parameter p)
        {
            bool value;
            if (Boolean.TryParse(p.Values.ElementAt(0), out value))
            {
                m_valueTrue.Checked = value == true;
                m_valueFalse.Checked = value == false;
                m_hiddenEditorMode.Value = "bool";
            }
            else
            {
                loadTextboxValues(p);
            }
        }

        protected void loadVariableValues(ConfigSystem.Parameter p)
        {
            m_valueVar.Text = p.Values.ElementAt(0);
            m_hiddenEditorMode.Value = "var";
        }

        protected void editParameter()
        {
            PrepareParameterEditor();

            if (isAdd)
                return;

            ConfigSystem.Parameter configParameter = data.GetCustomVariable(m_itemId).ParameterSet.GetParameter(parameterName);
            if (configParameter == null)
                return;

            LoadParameterValues(configParameter, data.GetParameterWithValues(parameterName));
        }

        private void LoadPathDropDowns(string pathString)
        {
            DataPath path = DataPath.Create(pathString);

            // Set initial parameter.
            SecurityParameter.Parameter currentParameter = data.GetParameterFromFields(path.Head.Name);
            if (currentParameter == null)
            {
                throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{pathString}]");
            }

            PrepareParameter(currentParameter); // Bind drop down items.

            // Set drop down value.
            foreach (IDataPathElement element in path.Tail.PathList)
            {
                if (element is DataPathBasicElement)
                {
                    // Only Record parameters have pathable elements. So check that parameter is a record parameter, and that the next element
                    // in the path corresponds to a field belonging to that parameter.
                    SecurityParameter.PathableRecordParameter recordParameter = currentParameter as SecurityParameter.PathableRecordParameter;
                    if (recordParameter == null || !recordParameter.FieldParameters.TryGetValue(element.Name, out currentParameter))
                    {
                        throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{pathString}]");
                    }

                    // Set Property Drop Down.
                    propertyDropdown.SelectedValue = element.Name;
                }
                else if (element is DataPathCollectionElement)
                {
                    // Only Record parameters have pathable elements. So check that parameter is a record parameter, and that the next element
                    // in the path corresponds to a field belonging to that parameter.
                    SecurityParameter.PathableRecordParameter recordParameter = currentParameter as SecurityParameter.PathableRecordParameter;
                    if (recordParameter == null || !recordParameter.FieldParameters.TryGetValue(element.Name, out currentParameter))
                    {
                        throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{pathString}]");
                    }

                    // Only Collection parameters correspond to Collection data path nodes. So check that parameter is a collection parameter.
                    SecurityParameter.PathableCollectionParameter collectionParameter = currentParameter as SecurityParameter.PathableCollectionParameter;
                    if (collectionParameter == null)
                    {
                        throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{pathString}]");
                    }
                }
                else if (element is DataPathSelectionElement)
                {
                    // Only Collection parameters can be indexed by a selection parameter. So check that parameter is a collection parameter.
                    SecurityParameter.PathableCollectionParameter collectionParameter = currentParameter as SecurityParameter.PathableCollectionParameter;
                    if (collectionParameter == null)
                    {
                        throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{pathString}]");
                    }

                    // Set Property Level and subset/record drop down.
                    if (string.Equals(element.Name, DataPath.WholeCollectionIdentifier, StringComparison.OrdinalIgnoreCase))
                    {
                        propertyLevel.SelectedValue = "WHOLE";
                    }
                    else if (element.Name.StartsWith(DataPath.SubsetMarker, StringComparison.OrdinalIgnoreCase))
                    {
                        // Verify subset name.
                        string substringName = element.Name.Substring(DataPath.SubsetMarker.Length);
                        if (!collectionParameter.Subsets.ContainsKey(substringName))
                        {
                            throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{pathString}]");
                        }

                        propertyLevel.SelectedValue = "SUBSET";
                        subsetDropdown.SelectedValue = substringName;
                    }
                    else
                    {
                        PathableRecordTypeInfo recordInfo;
                        if (!collectionParameter.Records.TryGetValue(element.Name, out recordInfo))
                        {
                            throw new CBaseException(ErrorMessages.Generic, $"Invalid path name. Path=[{pathString}]");
                        }

                        propertyLevel.SelectedValue = "INDIVIDUAL";
                        recordDropdown.SelectedValue = element.Name;
                    }

                    SelectPathPropertyLevel();  // Sets dropdown visibility based on property level.
                }
            }

            ClearOperationsSection();
            PrepareParameter(currentParameter);

            // Set Path UI elements readonly.
            // NOTE: Parameters are identified by Name, and since Path Parameters use the path as thier name,
            // we cannot allow the path to change when editing parameters.
            propertyLevel.Enabled = false;
            subsetDropdown.Enabled = false;
            recordDropdown.Enabled = false;
            propertyDropdown.Enabled = false;
        }

        private void LoadParameterValues(ConfigSystem.Parameter configParameter, SecurityParameter.Parameter securityParameter)
        {
            m_operationDontCare.Checked = false;
            if (securityParameter.Type == SecurityParameter.SecurityParameterType.Enum && securityParameter.Source == SecurityParameter.E_FieldSource.Condition)
            {
                loadConditionCategoryValues(configParameter);
            }
            else if (securityParameter.Type == SecurityParameter.SecurityParameterType.Enum)
            {
                SecurityParameter.EnumeratedParmeter enumParameter = securityParameter as SecurityParameter.EnumeratedParmeter;
                if (enumParameter.HasNotInOption)
                {
                    LoanInAndNotInEnumValues(configParameter);
                }
                else
                {
                    loadEnumValues(configParameter);
                }
            }
            else
            {
                switch (configParameter.FunctionT)
                {
                    case E_FunctionT.Between:
                        m_operationBetween.Checked = true;
                        loadTextboxValues(configParameter);
                        break;
                    case E_FunctionT.Equal:
                        if (configParameter.ValueType == E_ParameterValueType.Bool)
                        {
                            m_operationEqual.Checked = true;
                            loadBooleanValues(configParameter);
                        }
                        else 
                        {
                            m_operationEqualInt.Checked = true;
                            loadTextboxValues(configParameter);
                        }
                        break;
                    case E_FunctionT.EqualVar:
                        m_operationEqualVar.Checked = true;
                        loadVariableValues(configParameter);
                        break;
                    case E_FunctionT.IsNonblankStr:
                        m_operationIsNonblankStr.Checked = true;
                        loadBooleanValues(configParameter);
                        break;
                    case E_FunctionT.IsGreaterThan:
                        m_operationGreaterThan.Checked = true;
                        loadTextboxValues(configParameter);
                        break;
                    case E_FunctionT.IsGreaterThanVar:
                        m_operationGreaterThanVar.Checked = true;
                        loadVariableValues(configParameter);
                        break;
                    case E_FunctionT.IsGreaterThanOrEqualTo:
                        m_operationGreaterThanOrEqualTo.Checked = true;
                        loadTextboxValues(configParameter);
                        break;
                    case E_FunctionT.IsGreaterThanOrEqualToVar:
                        m_operationGreaterThanOrEqualToVar.Checked = true;
                        loadVariableValues(configParameter);
                        break;
                    case E_FunctionT.IsLessThan:
                        m_operationLessThan.Checked = true;
                        loadTextboxValues(configParameter);
                        break;
                    case E_FunctionT.IsLessThanVar:
                        m_operationLessThanVar.Checked = true;
                        loadVariableValues(configParameter);
                        break;
                    case E_FunctionT.IsLessThanOrEqualTo:
                        m_operationLessThanOrEqualTo.Checked = true;
                        loadTextboxValues(configParameter);
                        break;
                    case E_FunctionT.IsLessThanOrEqualToVar:
                        m_operationLessThanOrEqualToVar.Checked = true;
                        loadVariableValues(configParameter);
                        break;
                    case E_FunctionT.IsNonzeroValue:
                        m_operationIsNonzeroValue.Checked = true;
                        loadBooleanValues(configParameter);
                        break;
                    case E_FunctionT.IsAnyValue:
                        m_operationIsAnyValue.Checked = true;
                        loadBooleanValues(configParameter);
                        break;
                    default:
                        throw new UnhandledEnumException(configParameter.FunctionT);
                }
            }
        }

        protected void PrepareParameterEditor()
        {
            clearValues();
            m_parameterName.Text = parameterName;

            ClearOperationsSection();
            ClearPathSection();

            if (parameterName.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
            {
                LoadPathDropDowns(parameterName);
            }
            else
            {
                SecurityParameter.Parameter p = data.GetParameterFromFields(parameterName);
                PrepareParameter(p);
            }
        }

        private void ClearPathSection()
        {
            propertyLevel.Enabled = true;
            subsetDropdown.Enabled = true;
            recordDropdown.Enabled = true;
            propertyDropdown.Enabled = true;

            propertyLevel.SelectedIndex = -1;
            propertyLevelRow.Visible = false;
            subsetRow.Visible = false;
            recordRow.Visible = false;
            propertyRow.Visible = false;
        }

        private void ClearOperationsSection()
        {

            SecurityParameter.Parameter p = data.GetParameterFromFields(parameterName);

            m_operationBetween.Visible = false;
            m_operationBetween.Checked = false;
            m_operationEqual.Visible = false;
            m_operationEqual.Checked = false;
            m_operationEqualVar.Visible = false;
            m_operationEqualVar.Checked = false;
            m_operationIsNonblankStr.Visible = false;
            m_operationIsNonblankStr.Checked = false;
            m_operationEqualInt.Visible = false;
            m_operationEqualInt.Checked = false;
            m_operationGreaterThan.Visible = false;
            m_operationGreaterThan.Checked = false;
            m_operationGreaterThanVar.Visible = false;
            m_operationGreaterThanVar.Checked = false;
            m_operationGreaterThanOrEqualTo.Visible = false;
            m_operationGreaterThanOrEqualTo.Checked = false;
            m_operationGreaterThanOrEqualToVar.Visible = false;
            m_operationGreaterThanOrEqualToVar.Checked = false;
            m_operationLessThan.Visible = false;
            m_operationLessThan.Checked = false;
            m_operationLessThanVar.Visible = false;
            m_operationLessThanVar.Checked = false;
            m_operationLessThanOrEqualTo.Visible = false;
            m_operationLessThanOrEqualTo.Checked = false;
            m_operationLessThanOrEqualToVar.Visible = false;
            m_operationLessThanOrEqualToVar.Checked = false;
            m_operationIsNonzeroValue.Visible = false;
            m_operationIsNonzeroValue.Checked = false;
            m_operationIsAnyValue.Visible = false;
            m_operationIsAnyValue.Checked = false;
            m_operationEqualConditionCategory.Visible = false;
            m_operationEqualConditionCategory.Checked = false;
            m_operationNDaysFromToday.Visible = false;
            m_operationNDaysFromToday.Checked = false;
            m_operationDontCare.Visible = true;
            m_operationDontCare.Checked = true;
            m_InAndNotInEnumRepeater.Visible = false;
        }

        private void PrepareParameter(SecurityParameter.Parameter p)
        {
            switch (p.Type)
            {
                case SecurityParameter.SecurityParameterType.Rate:
                case SecurityParameter.SecurityParameterType.Money:
                    //Float
                    m_operationBetween.Visible = true;
                    m_operationGreaterThan.Visible = true;
                    m_operationGreaterThanOrEqualTo.Visible = true;
                    m_operationLessThan.Visible = true;
                    m_operationLessThanOrEqualTo.Visible = true;
                    m_operationIsNonzeroValue.Visible = true;
                    m_operationIsAnyValue.Visible = true;
                    break;
                case SecurityParameter.SecurityParameterType.Count:
                    //Int
                    m_operationBetween.Visible = true;
                    m_operationGreaterThan.Visible = true;
                    m_operationGreaterThanOrEqualTo.Visible = true;
                    m_operationLessThan.Visible = true;
                    m_operationLessThanOrEqualTo.Visible = true;
                    m_operationIsNonzeroValue.Visible = true;
                    m_operationIsAnyValue.Visible = true;
                    m_operationEqualInt.Visible = true;
                    break;
                case SecurityParameter.SecurityParameterType.String:
                    //String
                    m_operationIsNonblankStr.Visible = true;
                    break;
                case SecurityParameter.SecurityParameterType.Date:
                    //DateTime
                    m_operationBetween.Visible = true;
                    m_operationGreaterThan.Visible = true;
                    m_operationGreaterThanOrEqualTo.Visible = true;
                    m_operationLessThan.Visible = true;
                    m_operationLessThanOrEqualTo.Visible = true;
                    m_operationIsAnyValue.Visible = true;
                    m_operationNDaysFromToday.Visible = true;
                    break;
                case SecurityParameter.SecurityParameterType.Boolean:
                    //Bool
                    m_operationEqual.Visible = true;
                    break;
                case SecurityParameter.SecurityParameterType.Custom:
                    //CustomVariable
                    m_operationEqual.Visible = true;
                    break;
                case SecurityParameter.SecurityParameterType.Enum:
                    //Enum
                    SecurityParameter.EnumeratedParmeter enumParameter = p as SecurityParameter.EnumeratedParmeter;

                    List<SecurityParameter.EnumMapping> enumValues = enumParameter.EnumMapping;
                    if (p.Source == SecurityParameter.E_FieldSource.Condition) // This parameter is a condition-related parameter
                    // We're trying to shoehorn condition parameters into the enum parameterType
                    {
                        m_operationEqualConditionCategory.Visible = true;
                        m_valueConditionCategory.DataSource = enumValues;
                        m_valueConditionCategory.DataTextField = "FriendlyValue";
                        m_valueConditionCategory.DataValueField = "RepValue";
                        m_valueConditionCategory.DataBind();
                        m_operationDontCare.Visible = true;
                    }
                    else if (enumParameter.HasNotInOption)
                    {
                        m_valueEnum.Items.Clear();
                        m_InAndNotInEnumRepeater.DataSource = enumValues;
                        m_InAndNotInEnumRepeater.DataBind();
                        m_InAndNotInEnumRepeater.Visible = true;
                        m_operationDontCare.Visible = false;
                    }
                    else
                    {
                        m_valueEnum.DataSource = enumValues;
                        m_valueEnum.DataTextField = "FriendlyValue";
                        m_valueEnum.DataValueField = "RepValue";
                        m_valueEnum.DataBind();
                        m_valueEnum.Items.Add(new ListItem(DONT_CARE_STRING, "-1", true));
                        m_operationDontCare.Visible = false;
                    }
                    break;
                case SecurityParameter.SecurityParameterType.PathableCollection:
                    // Load dropdown lists.
                    SecurityParameter.PathableCollectionParameter collParameter = p as SecurityParameter.PathableCollectionParameter;

                    // Subsets.
                    subsetDropdown.DataSource = collParameter.Subsets.Values.OrderBy(s => s.Name);
                    subsetDropdown.DataValueField = "Name";
                    subsetDropdown.DataTextField = "Name";
                    subsetDropdown.DataBind();

                    // Records.
                    recordDropdown.DataSource = collParameter.Records.Values.OrderBy(r => r.FriendlyDescription);
                    recordDropdown.DataValueField = "TypeId";
                    recordDropdown.DataTextField = "FriendlyDescription";
                    recordDropdown.DataBind();

                    // Properties.
                    propertyDropdown.DataSource = collParameter
                        .FieldParameters
                        .Values
                        .Where(
                            f => f.Type != SecurityParameter.SecurityParameterType.PathableRecord
                            && f.Type != SecurityParameter.SecurityParameterType.PathableCollection)
                        .OrderBy(f => f.FriendlyName);
                    propertyDropdown.DataValueField = "FriendlyName";
                    propertyDropdown.DataTextField = "ID";
                    propertyDropdown.DataBind();

                    // Set property level to default and set visibility.
                    SelectPathPropertyLevel();

                    // Prepare propery parameter
                    PrepareParameter(collParameter.FieldParameters[propertyDropdown.SelectedValue]);
                    break;
                default:
                    throw new UnhandledEnumException(p.Type);
            }

            if (p.Type != SecurityParameter.SecurityParameterType.Enum)
            {
                m_valueEnum.Items.Clear();
            }
        }

        private void SelectPathPropertyLevel()
        {
            propertyLevelRow.Visible = true;
            propertyRow.Visible = true;

            SetRadioButtonVisibility();

            switch (propertyLevel.SelectedValue)
            {
                case "INDIVIDUAL":
                    subsetRow.Visible = false;
                    recordRow.Visible = true;
                    propertyDescription.Text = "Record";
                    break;
                case "SUBSET":
                    subsetRow.Visible = true;
                    recordRow.Visible = false;
                    propertyDescription.Text = "Subset";
                    break;
                case "WHOLE":
                    subsetRow.Visible = false;
                    recordRow.Visible = false;
                    propertyDescription.Text = "Collection";
                    break;
            }
        }

        private void SetRadioButtonVisibility()
        {
            if (recordDropdown.Items.Count <= 0)
            {
                propertyLevel.Items[0].Attributes.Add("hidden", "hidden");
            }

            if (subsetDropdown.Items.Count <= 0)
            {
                propertyLevel.Items[1].Attributes.Add("hidden", "hidden");
            }

            if (propertyLevel.SelectedIndex == -1)
            {
                for (int i = 0; i < propertyLevel.Items.Count; i++)
                {
                    if (propertyLevel.Items[i].Attributes["hidden"] == null)
                    {
                        propertyLevel.SelectedIndex = i;
                        break;
                    }
                }
            }
        }

        protected void saveConfigItem()
        {
            CustomVariable customVar = data.GetCustomVariable(m_itemId);
            customVar.Notes = m_tbNotes.Text;
            var scope = this.isGlobalConfig ? CustomVariable.E_Scope.System : CustomVariable.E_Scope.Lender;
            customVar.Scope = scope;

            try
            {
                SaveAsDraft(data);
            }
            catch (ConfigValidationException exc)
            {
                foreach(string s in exc.ConditionErrors)
                    m_saveErrorMsg.Text += AspxTools.HtmlString(s) + "<br />";
                foreach (string s in exc.ConstraintErrors)
                    m_saveErrorMsg.Text += AspxTools.HtmlString(s) + "<br />";
                foreach (string s in exc.CustomVariableErrors)
                    m_saveErrorMsg.Text += AspxTools.HtmlString(s) + "<br />";
            }
        }

        protected void saveParameter()
        {
            // Retrieve the thing that we're going to change
            var scope = this.isGlobalConfig ? CustomVariable.E_Scope.System : CustomVariable.E_Scope.Lender;
            CustomVariable customVariable = new CustomVariable("", scope);
            if (isAdd)
            {
                customVariable = new CustomVariable(m_customVariableName.Text, scope);
                customVariable.Notes = m_tbNotes.Text;
            }
            else
            {
                customVariable = data.GetCustomVariable(m_itemId);
            }

            // Determine what function we're using
            bool delete = false;
            E_FunctionT function = E_FunctionT.In;

            if (m_operationBetween.Checked)
                function = E_FunctionT.Between;
            else if (m_operationEqual.Checked)
                function = E_FunctionT.Equal;
            else if (m_operationEqualVar.Checked)
                function = E_FunctionT.EqualVar;
            else if (m_operationIsNonblankStr.Checked)
                function = E_FunctionT.IsNonblankStr;
            else if (m_operationGreaterThan.Checked)
                function = E_FunctionT.IsGreaterThan;
            else if (m_operationGreaterThanVar.Checked)
                function = E_FunctionT.IsGreaterThanVar;
            else if (m_operationGreaterThanOrEqualTo.Checked)
                function = E_FunctionT.IsGreaterThanOrEqualTo;
            else if (m_operationGreaterThanOrEqualToVar.Checked)
                function = E_FunctionT.IsGreaterThanOrEqualToVar;
            else if (m_operationLessThan.Checked)
                function = E_FunctionT.IsLessThan;
            else if (m_operationLessThanVar.Checked)
                function = E_FunctionT.IsLessThanVar;
            else if (m_operationLessThanOrEqualTo.Checked)
                function = E_FunctionT.IsLessThanOrEqualTo;
            else if (m_operationLessThanOrEqualToVar.Checked)
                function = E_FunctionT.IsLessThanOrEqualToVar;
            else if (m_operationIsNonzeroValue.Checked)
                function = E_FunctionT.IsNonzeroValue;
            else if (m_operationIsAnyValue.Checked)
                function = E_FunctionT.IsAnyValue;
            else if (m_operationNDaysFromToday.Checked)
                function = E_FunctionT.Equal;
            else if (m_operationEqualInt.Checked)
            {
                function = E_FunctionT.Equal;
            }

            string nameToUse = parameterName;
            SecurityParameter.Parameter valueParameter;
            if (parameterName.StartsWith(DataPath.PathHeader, StringComparison.OrdinalIgnoreCase))
            {
                valueParameter = data.GetParameterWithValues(parameterName);
            }
            else
            {
                SecurityParameter.Parameter passedInParameter = data.GetParameterFromFields(parameterName);

                // Check if parameter is path. If it is parse out path name.
                valueParameter = passedInParameter;
                if (passedInParameter.Type == SecurityParameter.SecurityParameterType.PathableCollection)
                {
                    SecurityParameter.PathableCollectionParameter collParameter = (SecurityParameter.PathableCollectionParameter)passedInParameter;
                    StringBuilder pathBuilder = new StringBuilder();
                    pathBuilder.Append(DataPath.PathHeader);
                    pathBuilder.Append(collParameter.Id);
                    pathBuilder.Append("[");

                    // Get selection identifier.
                    switch (propertyLevel.SelectedValue)
                    {
                        case "INDIVIDUAL":
                            pathBuilder.Append(recordDropdown.SelectedValue);
                            break;
                        case "SUBSET":
                            pathBuilder.Append(DataPath.SubsetMarker);
                            pathBuilder.Append(subsetDropdown.SelectedValue);
                            break;
                        case "WHOLE":
                            pathBuilder.Append(DataPath.WholeCollectionIdentifier);
                            break;
                        default:
                            throw new UnhandledCaseException(propertyLevel.SelectedValue);
                    }

                    pathBuilder.Append("].");

                    // Get property field.
                    pathBuilder.Append(propertyDropdown.SelectedValue);

                    nameToUse = pathBuilder.ToString();
                    valueParameter = collParameter.FieldParameters[propertyDropdown.SelectedValue];
                }
            }

            // Get the ParameterValueType
            E_ParameterValueType valueType = SystemConfigValidator.ParamTypeToValueType(valueParameter);
            if (valueParameter.Type == SecurityParameter.SecurityParameterType.Enum) // Handle enums
            {
                SecurityParameter.EnumeratedParmeter enumParameter = valueParameter as SecurityParameter.EnumeratedParmeter;

                function = E_FunctionT.In;
                if (enumParameter.Source == SecurityParameter.E_FieldSource.Condition)
                {
                    delete = m_operationDontCare.Checked;
                }
                else if (enumParameter.HasNotInOption)
                {
                    delete = m_InAndNotInEnumRepeater.Items.Cast<RepeaterItem>().All(ri => ((RadioButtonList)ri.FindControl("radioList")).SelectedValue == "NA");
                }
                else
                {
                    delete = m_valueEnum.Items.FindByText(DONT_CARE_STRING).Selected;
                }
                SecurityParameter.EnumParamType enumType = enumParameter.BaseType;
                if (enumType == SecurityParameter.EnumParamType.Int)
                    valueType = E_ParameterValueType.Int;
                else if (enumType == SecurityParameter.EnumParamType.String)
                    valueType = E_ParameterValueType.String;
            }
            else
            {
                delete = m_operationDontCare.Checked;
            }

            // Create the parameter with the desired values
            ConfigSystem.Parameter parameter = new ConfigSystem.Parameter(nameToUse, function, valueType, false, false, valueParameter.Scope);

            if (function == E_FunctionT.In)
            {
                SecurityParameter.EnumeratedParmeter enumParameter = valueParameter as SecurityParameter.EnumeratedParmeter;
                if (valueParameter.Source == SecurityParameter.E_FieldSource.Condition) // Add the selected checkbox to the parameter
                {
                    parameter.AddValue(m_valueConditionCategory.SelectedItem.Text, m_valueConditionCategory.SelectedItem.Value);
                }
                else if (enumParameter != null && enumParameter.HasNotInOption)
                {
                    int count = 0;
                    foreach (RepeaterItem item in m_InAndNotInEnumRepeater.Items)
                    {
                        RadioButtonList radioList = item.FindControl("radioList") as RadioButtonList;
                        Label name = item.FindControl("name") as Label;
                        HiddenField value = item.FindControl("value") as HiddenField;

                        if (radioList.SelectedValue == "IS")
                        {
                            parameter.AddValue(value.Value, name.Text);
                            count++;
                        }
                        else if (radioList.SelectedValue == "IS_NOT")
                        {
                            parameter.AddExcption(value.Value, name.Text);
                            count++;
                        }
                    }

                    if (count == 0)
                    {
                        delete = true;
                    }
                }
                else  // Add any checked enum values to the parameter
                {
                    int count = 0;
                    foreach (ListItem cb in m_valueEnum.Items)
                    {
                        if (cb.Selected)
                        {
                            parameter.AddValue(cb.Value, cb.Text);
                            count++;
                        }
                    }
                    if ((count >= m_valueEnum.Items.Count - 1 && valueParameter.CategoryName != "Groups") || count == 0)
                        delete = true;
                }
            }
            else if (m_valueTextboxRb.Checked) // Add the text to the parameter
            {
                string text = m_valueTextbox1.Text;
                if (m_operationNDaysFromToday.Checked)
                {
                    text = string.Format("today{0}{1}", m_operationNDaysFromTodayOperator.SelectedValue, m_valueTextbox1.Text);
                }
                parameter.AddValue(text, text);
                if (function == E_FunctionT.Between)
                {
                    try
                    {
                        parameter.AddValue(m_valueTextbox2.Text, m_valueTextbox2.Text);
                    }
                    catch (ArgumentException exc)
                    {
                        m_valueErrorMsg.Text = exc.Message;
                        m_hiddenCmd.Value = "saveParameterFailed";
                        data = GetConfig();
                        return;
                    }
                }
            }
            else if (m_valueVarRb.Checked) // Add the picked value to the parameter
                parameter.AddValue(m_hiddenVariableName.Value, m_hiddenVariableName.Value);
            else if (m_valueBoolRb.Checked) // Add the bool to the parameter
                parameter.AddValue(m_valueTrue.Checked.ToString(), m_valueTrue.Checked.ToString());

            if (customVariable.ParameterSet.GetParameter(nameToUse) != null)
            {
                if (delete)
                    customVariable.ParameterSet.RemoveParameter(parameter.Name);
                else
                    customVariable.ParameterSet.ReplaceParameter(parameter.Name, parameter);
            }
            else
            {
                if (!delete)
                    customVariable.ParameterSet.AddParameter(parameter);
            }

            // Apply our changes to the condition, then save the config information
            if (isAdd && !delete)
                data.AddNew(customVariable);
            else if(!isAdd)
                data.Edit(customVariable.Id, customVariable);

            try
            {
                m_cacheId = data.SaveToCache();
                if (isAdd && !delete) // Save the ID for use later
                {
                    m_hiddenId.Value = customVariable.Id.ToString();
                    m_hiddenAction.Value = "edit";
                }
            }
            catch (ConfigValidationException exc)
            {
                foreach (string s in exc.ConditionErrors)
                    m_valueErrorMsg.Text += AspxTools.HtmlString(s) + "<br />";
                foreach (string s in exc.ConstraintErrors)
                    m_valueErrorMsg.Text += AspxTools.HtmlString(s) + "<br />";
                foreach (string s in exc.CustomVariableErrors)
                    m_valueErrorMsg.Text += AspxTools.HtmlString(s) + "<br />";
                m_hiddenCmd.Value = "saveParameterFailed";
                data = GetConfig();
            }
            catch (XmlException exc)
            {
                m_valueErrorMsg.Text += exc.Message + "<br />";
                m_hiddenCmd.Value = "saveParameterFailed";
                data = GetConfig();
            }

            clearValues();
        }

        protected void SaveAsDraft(WorkflowSystemConfigModel toBeSaved)
        {
            if (!m_inDraftMode)
            {
                toBeSaved.SwitchToDraft();
                toBeSaved.Save();
                m_inDraftMode = true;
            }
            else
            {
                toBeSaved.Save();
            }
        }

        /// <summary>
        /// Bind Enums that offer Is and Is Not options.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void OnInAndNotInEnumRepeater(object sender, RepeaterItemEventArgs args)
        {
            SecurityParameter.EnumMapping map = args.Item.DataItem as SecurityParameter.EnumMapping;

            Label name = args.Item.FindControl("name") as Label;
            HiddenField value = args.Item.FindControl("value") as HiddenField;

            name.Text = map.FriendlyValue;
            value.Value = map.RepValue;
        }

        protected void OnParameterBound(object sender, RepeaterItemEventArgs args)
        {
            string pName = args.Item.DataItem as string;

            EncodedLiteral ParameterName = args.Item.FindControl("m_parameterName") as EncodedLiteral;
            EncodedLiteral ParameterValue = args.Item.FindControl("m_parameterValue") as EncodedLiteral;
            HtmlAnchor ParameterEdit = (HtmlAnchor)args.Item.FindControl("m_parameterEdit");

            ParameterSet parameterSet = new ParameterSet();
            CustomVariable varFromConfig = data.GetCustomVariable(m_itemId);
            if (varFromConfig != null)
                parameterSet = varFromConfig.ParameterSet;

            ConfigSystem.Parameter parameter = parameterSet.GetParameter(pName);
            ParameterEdit.Attributes.Add("onclick", "onEditParameter(" + AspxTools.JsString(pName) + "); return false;");
            ParameterName.Text = data.GetFriendlyName(pName);

            if (parameter != null)
                ParameterValue.Text = data.GetOperandRep(parameter);
        }

        protected void OnParameterListBound(object sender, RepeaterItemEventArgs args)
        {
            SecurityParameter.Parameter parameter = args.Item.DataItem as SecurityParameter.Parameter;

            EncodedLiteral ParameterData = args.Item.FindControl("m_parameterData") as EncodedLiteral;
            HtmlAnchor selectLink = (HtmlAnchor)args.Item.FindControl("m_selectLink");
            HtmlGenericControl Div = args.Item.FindControl("m_choiceDiv") as HtmlGenericControl;

            string friendlyName = parameter.FriendlyName;

            
            if (parameter.Id == friendlyName && FieldEnumerator.ListLoanFieldsByName().ContainsKey(parameter.Id))
            {
                string newName = FieldEnumerator.ListLoanFieldsByName()[parameter.Id].FriendlyName;
                if (false == string.IsNullOrEmpty(newName))
                {
                    friendlyName = newName;
                }
            }
            if (m_hiddenCmd.Value == "pickParameter")
            {
                if (parameter.Type == filterType && parameter.Id != parameterName)
                {
                    ParameterData.Text = string.Format("{0} ({1}) ({2})", parameter.Id, friendlyName, parameter.Source);
                    selectLink.Attributes.Add("onclick", "onVariableSelect(" + AspxTools.JsString(parameter.Id) + ");return false;");
                    Div.Attributes.Add("category", parameter.CategoryName);
                    Div.Attributes.Add("fid", parameter.Id);
                }
                else
                {
                    Div.Visible = false;
                }
            }
            else
            {
                ParameterData.Text = string.Format("{0} ({1}) ({2})", parameter.Id, friendlyName, parameter.Source);
                selectLink.Attributes.Add("onclick", "onEditParameter(" + AspxTools.JsString(parameter.Id) + ");return false;");
                Div.Attributes.Add("category", parameter.CategoryName);
                Div.Attributes.Add("fid", parameter.Id);
            }
        }

        private void SelectPathProperty(string parameterName)
        {
            ClearOperationsSection();
            SecurityParameter.PathableCollectionParameter collParameter = data.GetParameterFromFields(parameterName) as SecurityParameter.PathableCollectionParameter;
            PrepareParameter(collParameter.FieldParameters[propertyDropdown.SelectedValue]);
        }
    }
}