﻿namespace LendersOfficeApp.common.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using global::ConfigSystem;
    using DataAccess;
    using DataAccess.PathDispatch;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOfficeApp.LOAdmin.TaskBackendUtilities;

    using MeridianLink.CommonControls;

    /// <summary>
    /// Utilities class for the workflow conditions editor, constraints editor, and field protection editor
    /// </summary>
    public static class EditWorkflowConfigUtilities
    {
        private static readonly HashSet<string> FieldsToExcludeFromUI = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "sSelectedProductCodeFilter",
            "sProductCodesByFileType",
            "sAvailableProductCodeFilter"
        };

        public static IEnumerable<OperationField> FilterOutExcludedFieldsForUi(IEnumerable<OperationField> currentFields)
        {
            return currentFields.Where((field) => !FieldsToExcludeFromUI.Contains(field.Id));
        }

        private static HashSet<string> m_operationCategories;
        public static HashSet<string> OperationCategories
        {
            get
            {
                if (m_operationCategories == null)
                {
                    EnumerateFields();
                    return m_operationCategories;
                }
                else
                {
                    return m_operationCategories;
                }
            }
        }

        private static List<OperationField> m_operationFields;
        public static List<OperationField> OperationFields
        {
            get
            {
                if (m_operationFields == null)
                {
                    EnumerateFields();
                    return m_operationFields;
                }
                else
                {
                    return m_operationFields;
                }
            }
        }

        public static IEnumerable<OperationField> BrokerSpecificOperationFields(Guid brokerId)
        {
            if (brokerId == Guid.Empty)
            {
                return new OperationField[0];
            }

            BrokerDB db = BrokerDB.RetrieveById(brokerId);
            List<OperationField> fields = new List<OperationField>();

            OperationField op = new OperationField("Lead Source - Blank (0)", "sLeadSrcId:0", "Everything_Else", false, false);
            fields.Add(op);

            string prefix = "Lead Source - ";
            foreach (LeadSource source in db.LeadSources)
            {
                string name = string.Concat(prefix, source.Name, "(", source.Id + ")");
                string id = string.Concat("sLeadSrcId:", source.Id);
                if (source.Deleted)
                {
                    name = String.Concat(name, " (deleted)");
                }

                op = new OperationField(name, id, "Everything_Else", false, false);
                fields.Add(op);
            }

            return fields;
        }

        // TODO: Responsiveness ideas:
        // Perhaps we could cache this data somewhere
        // What if we changed this into a webmethod that was called through ajax?
        // Then, what if we split up the fields so that it would come in one chunk at a time?
        /// <summary>
        /// SIDE EFFECTS:
        ///   Populates m_operationCategories
        ///   Populates m_operationFields
        /// </summary>
        private static void EnumerateFields()
        {
            m_operationCategories = new HashSet<string>();
            m_operationFields = new List<OperationField>();

            var seenFields = new Dictionary<string, OperationField>();

            // if the categories field is removed, we don't have to sort this
            var pages = (from p in FieldEnumerator.ListLoanPages()
                             //Skip this page for now, since it's fake
                         where !p.Key.Equals("Everything_Else")
                         orderby p.Key
                         select p.Value)
                        .ToList();

            //Then make sure the fake page goes at the end
            pages.Add(FieldEnumerator.ListLoanPages()["Everything_Else"]);

            string pattern = @"(.*)\s\((.*)\)"; // Match two things: 1: The friendly name, 2: The enumeration value
            var regex = new Regex(pattern);

            foreach (var pageInfo in pages)
            {
                foreach (FieldEnumerator.LoanFieldInfo field in pageInfo.Fields)
                {
                    foreach (var formField in field.FormFieldInfo)
                    {
                        // Display each type per enumerated field with the form <name (field id) : type>
                        // 4.2.1.4
                        string friendlyName;
                        var match = regex.Match(formField.FriendlyName);
                        if (match.Success)
                        {
                            friendlyName = string.Format("{0} ({1}) : {2}", match.Groups[1], field.Name, match.Groups[2]);
                        }
                        else
                        {
                            friendlyName = string.Format("{0} ({1})", formField.FriendlyName, field.Name);
                        }
                        // We are ignoring formField.Category
                        // The writable field's category is the page that it came from
                        string category = pageInfo.Name;
                        m_operationCategories.Add(category);

                        OperationField opField;
                        seenFields.TryGetValue(formField.Id, out opField);
                        if (opField != null)
                        {
                            opField.Categories.Add(category);
                        }
                        else
                        {
                            opField = new OperationField(
                                friendlyName,
                                formField.Id,
                                category,
                                false,
                                false,
                                field.CanWrite);
                            m_operationFields.Add(opField);
                            seenFields.Add(formField.Id, opField);
                        }
                    }
                }
            }
        }

        public static void OnOperationFieldsListBound(object sender, RepeaterItemEventArgs args)
        {
            var opField = args.Item.DataItem as OperationField;

            EncodedLiteral ParameterData = args.Item.FindControl("m_pData") as EncodedLiteral;
            ParameterData.Text = opField.FriendlyName;

            HtmlGenericControl Div = args.Item.FindControl("m_choice") as HtmlGenericControl;
            Div.Attributes.Add("category", opField.Categories_rep);
            Div.Attributes.Add("fid", opField.Id);
        }

        public static void OnOperationFieldsListLoad(object sender, EventArgs e, AbstractConditionGroup configItem, WorkflowOperation operation)
        {
            var repeater = sender as Repeater;
            var fieldSysOp = configItem.SysOpSet.Get(operation);
            if (fieldSysOp == null || fieldSysOp.Fields == null)
            {
                return;
            }
            foreach (RepeaterItem item in repeater.Items)
            {
                // Mark the box as checked if it's already in the fields list
                HtmlGenericControl Div = item.FindControl("m_choice") as HtmlGenericControl;
                CheckBox SelectBox = item.FindControl("m_cb") as CheckBox;
                OperationField matchedField;
                fieldSysOp.Fields.TryGetValue(Div.Attributes["fid"], out matchedField);
                if (matchedField != null)
                {
                    SelectBox.Checked = true;
                }
            }
        }

        /// <summary>
        /// Retrieve the fields that are checked in the fieldsList repeater
        /// </summary>
        /// <param name="fieldsList"></param>
        /// <param name="fieldsAreClearable"></param>
        /// <param name="fieldsAreUpdateable"></param>
        /// <returns></returns>
        public static OperationFieldSet GetFieldsFromFieldsListCheckboxes(Repeater fieldsList, bool fieldsAreClearable, bool fieldsAreUpdateable)
        {
            var fieldSet = new OperationFieldSet();
            // Figure out what is checked
            foreach (RepeaterItem field in fieldsList.Items)
            {
                CheckBox SelectBox = field.FindControl("m_cb") as CheckBox;
                if (!SelectBox.Checked)
                {
                    continue;
                }


                EncodedLiteral ParameterData = field.FindControl("m_pData") as EncodedLiteral;
                string fieldName = ParameterData.Text;

                HtmlGenericControl Div = field.FindControl("m_choice") as HtmlGenericControl;
                string categories = Div.Attributes["category"]; // this is a delimited string of categories
                string fieldId = Div.Attributes["fid"];

                if (!fieldSet.ContainsKey(fieldId))
                {
                    fieldSet.Add(new OperationField(fieldName, fieldId, categories, fieldsAreClearable, fieldsAreUpdateable));
                }
            }

            return fieldSet;
        }

        public static void UpdateOperations(List<WorkflowOperation> operations, bool fieldsAreClearable, bool fieldsAreUpdateable, string failureMessage, 
                                        string notes, bool excludeFromRWExport, AbstractConditionGroup conditionGroup)
        {
            // Keep field write since it has subitems that we care about
            var fieldWriteOp = conditionGroup.SysOpSet.Get(WorkflowOperations.WriteField);
            var fieldProtectOp = conditionGroup.SysOpSet.Get(WorkflowOperations.ProtectField);

            // Throw away the old stuff, use what is currently checked
            conditionGroup.SysOpSet = new SystemOperationSet();
            foreach (var operation in operations)
            {
                var op = new SystemOperation(operation);
                conditionGroup.SysOpSet.Add(op);
            }

            // We need the old operationFields here
            // If the operation is selected in operationNames and there was a previously existing one,
            //   then use the previously existing one
            if (conditionGroup.SysOpSet.Get(WorkflowOperations.WriteField) != null
                && fieldWriteOp != null)
            {
                foreach (var field in fieldWriteOp.Fields.Values)
                {
                    field.Clearable = fieldsAreClearable;
                    field.Updateable = fieldsAreUpdateable;
                }
                conditionGroup.SysOpSet.Replace(WorkflowOperations.WriteField, fieldWriteOp);
            }
            if (conditionGroup.SysOpSet.Get(WorkflowOperations.ProtectField) != null
                && fieldProtectOp != null)
            {
                foreach (var field in fieldProtectOp.Fields.Values)
                {
                    field.Clearable = fieldsAreClearable;
                    field.Updateable = fieldsAreUpdateable;
                }
                conditionGroup.SysOpSet.Replace(WorkflowOperations.ProtectField, fieldProtectOp);
            }

            conditionGroup.FailureMessage = failureMessage;
            conditionGroup.Notes = notes;
            conditionGroup.ExcludeFromRWExport = excludeFromRWExport &&
                                                 conditionGroup.SysOpSet.Any(operation => operation.Name == WorkflowOperations.ReadLoan.Id
                                                                              || operation.Name == WorkflowOperations.WriteLoan.Id);
        }

        /// <summary>
        /// Update the checked fields in fieldsList to the operationName operation in conditionGroup.
        /// </summary>
        /// <param name="operationName">E.G. ConstApp.OPERATION_WRITE_FIELD.</param>
        /// <param name="fieldsList">The repeater holding all the operations.</param>
        /// <param name="conditionGroup">The condition/constraint to save to.</param>
        public static void UpdateOperationFields(WorkflowOperation operation, Repeater fieldsList, GridView collectionsList, bool fieldsAreClearable, bool fieldsAreUpdateable, string newPathFieldId, string newPathFieldFriendlyName, AbstractConditionGroup conditionGroup)
        {
            var fieldOp = conditionGroup.SysOpSet.Get(operation);
            var fields = GetFieldsFromFieldsListCheckboxes(fieldsList, fieldsAreClearable, fieldsAreUpdateable);
            GetFieldsFromCollectionList(fields, collectionsList, fieldsAreClearable, fieldsAreUpdateable);


            // Add new path if applicable.
            if (!string.IsNullOrWhiteSpace(newPathFieldId) && !string.IsNullOrWhiteSpace(newPathFieldFriendlyName))
            {
                fields.Add(new OperationField(newPathFieldFriendlyName, newPathFieldId, null, fieldsAreClearable, fieldsAreUpdateable));
            }

            if (fieldOp != null) // An existing field write operation with writable fields defined
            {
                fieldOp.Fields = fields;
            }
            else
            {
                fieldOp = new SystemOperation(operation);
                fieldOp.Fields = fields;
                conditionGroup.SysOpSet.Add(fieldOp);
            }
        }

        public static void UpdatedLoanStatusChange(AbstractConditionGroup conditionGroup, Repeater LoanStatusChecklistRepeater)
        {
            SystemOperation loanStatusChangeOp = conditionGroup.SysOpSet.Get(WorkflowOperations.LoanStatusChange);
            if (loanStatusChangeOp == null)
            {
                return;
            }

            OperationFieldSet fieldSet = new OperationFieldSet();
            foreach (RepeaterItem item in LoanStatusChecklistRepeater.Items)
            {
                HtmlInputCheckBox loanStatusCheckbox = (HtmlInputCheckBox)item.FindControl("loanStatusCheckbox");
                EncodedLabel loanStatusLabel = (EncodedLabel)item.FindControl("loanStatusLabel");

                if (loanStatusCheckbox.Checked)
                {
                    fieldSet.Add(new OperationField($"Loan Status (sStatusT) : {loanStatusLabel.Text}", $"sStatusT:{loanStatusCheckbox.Value}", string.Empty, true, true));
                }
            }

            loanStatusChangeOp.Fields = fieldSet;
        }

        public static void RemoveOperationField(AbstractConditionGroup conditionGroup, WorkflowOperation operation,  string fieldId)
        {
            var fieldOp = conditionGroup.SysOpSet.Get(operation);
            fieldOp.Fields.Remove(fieldId);
        }

        public static void GetFieldsFromCollectionList(OperationFieldSet fields, GridView collectionsList, bool fieldsAreClearable, bool fieldsAreUpdateable)
        {
            foreach (GridViewRow row in collectionsList.Rows)
            {
                if (row.RowType != DataControlRowType.DataRow)
                {
                    continue;
                }

                HtmlAnchor removeLink = (HtmlAnchor)row.FindControl("removeLink");
                HtmlInputHidden fieldId = (HtmlInputHidden)row.FindControl("fieldId");
                EncodedLabel friendlyName = (EncodedLabel)row.FindControl("friendlyName");

                if (!fields.ContainsKey(fieldId.Value))
                {
                    fields.Add(new OperationField(friendlyName.Text, fieldId.Value, null, fieldsAreClearable, fieldsAreUpdateable));
                }
            }
        }

        public static void OnParameterListBound(object sender, RepeaterItemEventArgs args, string command, string parameterName, SecurityParameter.SecurityParameterType filterType)
        {
            SecurityParameter.Parameter parameter = args.Item.DataItem as SecurityParameter.Parameter;

            EncodedLiteral ParameterData = args.Item.FindControl("m_pData") as EncodedLiteral;
            HtmlAnchor selectLink = (HtmlAnchor)args.Item.FindControl("m_selectLink");
            HtmlGenericControl Div = args.Item.FindControl("m_choice") as HtmlGenericControl;

            string friendlyName = parameter.FriendlyName;

            if (parameter.Id == friendlyName && FieldEnumerator.ListLoanFieldsByName().ContainsKey(parameter.Id))
            {
                string newName = FieldEnumerator.ListLoanFieldsByName()[parameter.Id].FriendlyName;
                if (false == string.IsNullOrEmpty(newName))
                {
                    friendlyName = newName;
                }
            }

            ParameterData.Text = string.Format("{0} ({1}) ({2})", parameter.Id, friendlyName, parameter.Source);

            if (command == "pickParameter")
            {
                string paramName = parameterName;
                if (parameter.Type == filterType && parameter.Id != paramName) // Only show parameters that match filter
                {
                    selectLink.Attributes.Add("onclick", "onVariableSelect(" + AspxTools.JsString(parameter.Id) + ");return false;");
                    Div.Attributes.Add("fid", parameter.Id);
                    Div.Attributes.Add("category", parameter.CategoryName);
                }
                else
                {
                    Div.Visible = false;
                }
            }
            else
            {
                selectLink.Attributes.Add("onclick", "onEditParameter(" + AspxTools.JsString(parameter.Id) + ");return false;");
                Div.Attributes.Add("category", parameter.CategoryName);
                Div.Attributes.Add("fid", parameter.Id);
            }
        }

        public static List<PathableCollectionModel> GeneratePathModel(Guid brokerId)
        {
            List<PathableCollectionModel> list = new List<PathableCollectionModel>();

            IEnumerable<PathableFieldInfo> lffPathableFields = PathSchemaManager.GetSchemaByType(typeof(CPageData)).GetFields().Values.Where(
                f => f.ParameterType == SecurityParameter.SecurityParameterType.PathableRecord
                || f.ParameterType == SecurityParameter.SecurityParameterType.PathableCollection);

            foreach (PathableFieldInfo fieldInfo in lffPathableFields)
            {
                IPathableRecordSchema schema = PathSchemaManager.GetSchemaByType(fieldInfo.FieldType);

                PathableCollectionModel collectionModel = new PathableCollectionModel();
                collectionModel.Name = fieldInfo.Name;
                
                if (fieldInfo.ParameterType == SecurityParameter.SecurityParameterType.PathableCollection)
                {
                    IPathableCollectionSchema collectionSchema = schema as IPathableCollectionSchema;
                    collectionModel.Subsets = collectionSchema.GetSubsets().Values.OrderBy(s => s.Name).Select(s => s.Name).ToList();
                    collectionModel.RecordTypes = collectionSchema.GetRecordTypes(brokerId).Values.OrderBy(r => r.FriendlyDescription)
                        .Select(r => new PathableRecordTypeModel { FriendlyDescription = r.FriendlyDescription, TypeId = r.TypeId }).ToList();

                    // Add "Add and "Remove" properties to field list.
                    collectionModel.Fields.Add("Add");
                    collectionModel.Fields.Add("Remove");

                    // Hack fix to get around UI not supporting nested nested collections.
                    if (fieldInfo.FieldType == typeof(BorrowerClosingCostSet) || fieldInfo.FieldType == typeof(SellerClosingCostSet))
                    {
                        collectionModel.Fields.Add("Payments[ANY].PaidByT");
                        collectionModel.Fields.Add("Payments[ANY].GfeClosingCostFeePaymentTimingT");
                    }
                }

                // NOTE: Add fields last so that Add/Remove properties end up on top of the list.
                collectionModel.Fields.AddRange(schema.GetFields().Values.OrderBy(f => f.Name).Select(f => f.Name));

                list.Add(collectionModel);
            }

            return list;
        }

        public class PathableCollectionModel
        {
            public string Name;
            public List<string> Subsets = new List<string>();
            public List<PathableRecordTypeModel> RecordTypes = new List<PathableRecordTypeModel>();
            public List<string> Fields = new List<string>();
        }

        public class PathableRecordTypeModel
        {
            public string TypeId;
            public string FriendlyDescription;
        }

    }
}
