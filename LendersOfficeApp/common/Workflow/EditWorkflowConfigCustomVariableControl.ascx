﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditWorkflowConfigCustomVariableControl.ascx.cs" Inherits="LendersOfficeApp.common.Workflow.EditWorkflowConfigCustomVariableControl" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<input type="hidden" value="" runat="server" id="m_hiddenId" />
<input type="hidden" value="" runat="server" id="m_hiddenAction" />
<input type="hidden" value="" runat="server" id="m_hiddenParameterName" />
<input type="hidden" value="" runat="server" id="m_hiddenVariableName" />
<input type="hidden" value="" runat="server" id="m_hiddenCmd" />
<input type="hidden" value="" runat="server" id="m_hiddenEditorMode" />
<input type="hidden" value="" runat="server" id="m_hiddenDraftMode" />
<input type="hidden" value="" runat="server" id="m_hiddenKey" />
<script type="text/javascript">
    function _init() {
        var op = document.getElementById('m_hiddenCmd').value;
        if (op == 'selectParameter' || op == 'editParameter' || op == "saveParameterFailed" || op == "selectPathProperty" || op == "selectPathPropertyLevel") {
            Modal.ShowPopup('ParameterPickerPopup', null, null, 100, 100); // utilities.js:Modal
            document.getElementById('m_parameterPicker').style.display = 'none';
            document.getElementById('m_parameterEditor').style.display = '';
            setValueType(document.getElementById("m_hiddenEditorMode").value);
        }
        else if (op == 'pickParameter' || op == 'addParameter') {
            Modal.ShowPopup('ParameterPickerPopup', null, null, 100, 100);
            document.getElementById('m_parameterPicker').style.display = '';
            document.getElementById('m_parameterEditor').style.display = 'none';
        }
        else if (op == 'pickWriteFields') {
            Modal.ShowPopup('WritableFieldsPopup', null, null, 100, 100);
        }
        else if (op == "saveConfigItem") {
            if (window && window.opener && typeof (window.opener.editCustomVarCallback) == "function") {
                window.opener.editCustomVarCallback();
            }

            window.close();
        }

        document.getElementById('m_hiddenCmd').value = '';

        var parameterSelectionList = createSelectionList(
            $('#searchCriteria'),
            $('#m_Group'),
            $('#m_Page'),
            $('#m_FieldList'));

        $('#m_enableSearch').on("click", function() {
            defaultCategory(parameterSelectionList);
            displaySearchResults(false, parameterSelectionList);
        })

        $('#searchCriteria').on("focus", function() {
            defaultCategory(parameterSelectionList);
            $('#m_enableSearch').prop('checked', true);
        }).on("keyup", function() {
            $('#m_enableSearch').prop('checked', true);
            displaySearchResults(false, parameterSelectionList);
        });

        $('#m_disableSearch').on("click", function() {
            displaySearchResults(true, parameterSelectionList);
        });

        $('#m_Group').on("change", function() {
            $('#m_disableSearch').prop('checked', true);
            jumpToSelected(parameterSelectionList);
        });

        $('#m_Page').on("change", function() {
            $('#m_searchPage').prop('checked', true);
            jumpToPage(parameterSelectionList);
        });

        resize(1000, 700);
        validateOperations(false);
        validateButtons();
    }

    function validateButtons() {
        if (<%= AspxTools.JsBool(isCustomVariable) %>) {
            var validName = document.getElementById('m_customVariableName').value != '';
            document.getElementById('m_addParameterBtn').disabled = !validName;
            document.getElementById('m_saveItemBtn').disabled = !validName || <%= AspxTools.JsBool(isAdd) %>;
        }
    }
    
    function validateOperations(postback) {
        if(postback)
            performPostback();
    }

    function onEditParameter(name) {
        document.getElementById('m_hiddenParameterName').value = name;
        document.getElementById('m_hiddenCmd').value = 'editParameter';
        performPostback();
    }

    function onSelectPathProperty() {
        document.getElementById('m_hiddenCmd').value = 'selectPathProperty';
        performPostback();
    }

    function onSelectPathPropertyLevel() {
        document.getElementById('m_hiddenCmd').value = 'selectPathPropertyLevel';
        performPostback();
    }

    function onAddParameter() {
        document.getElementById('m_hiddenCmd').value = 'addParameter';
        performPostback();
    }

    function onParameterSelect(name) {
        document.getElementById('m_hiddenParameterName').value = name;
        document.getElementById('m_hiddenCmd').value = 'selectParameter';
        performPostback();
    }

    function onVariableSelect(name) {
        document.getElementById('m_valueVar').innerText = name + '  ';
        document.getElementById('m_hiddenVariableName').value = name;
            
        document.getElementById('m_parameterPicker').style.display = 'none';
        document.getElementById('m_parameterEditor').style.display = '';
        setValueType('var');
    }

    function onSaveConfigItem() {
        document.getElementById('m_hiddenCmd').value = 'saveConfigItem';
        performPostback();
    }

    function onSaveParameter() {
        document.getElementById('m_hiddenCmd').value = 'saveParameter';
        performPostback();
    }
        
    function onPickWritableField() {
        document.getElementById('m_hiddenCmd').value = 'pickWriteFields';
        performPostback();
    }
        
    function onSaveWritableFields() {
        document.getElementById('m_hiddenCmd').value = 'saveWriteFields';
        performPostback();
    }

    function onPickVariable() {
        document.getElementById('m_hiddenCmd').value = 'pickParameter';
        performPostback();
    }

    function performPostback() {            
        __doPostBack('', '');            
    }

    function setValueType(type) {
        document.getElementById("m_hiddenEditorMode").value = type;
        document.getElementById('m_valueTextbox1').style.display = 'none';
        document.getElementById('m_valueTextbox2').style.display = 'none';
        document.getElementById('m_valueVar').style.display = 'none';
        document.getElementById('m_valueVarEdit').style.display = 'none';
        document.getElementById('m_valueTrueSpan').style.display = 'none';
        document.getElementById('m_valueFalseSpan').style.display = 'none';
        document.getElementById('m_valueConditionCategory').style.display = 'none';
        document.getElementById('m_valueDesc').style.display = 'none';
        document.getElementById('m_operationNDaysFromTodayOperator').style.display = 'none';
        if (type == 'textbox') {
            document.getElementById('m_valueTextboxRb').checked = 'true';
            document.getElementById('m_valueTextbox1').style.display = '';
            document.getElementById('m_valueTextbox2').style.display = document.getElementById('m_operationBetween').checked ? '' : 'none';
        } else if (type == 'var') {
            document.getElementById('m_valueVarRb').checked = 'true';
            document.getElementById('m_valueVar').style.display = '';
            document.getElementById('m_valueVarEdit').style.display = '';
        } else if (type == 'bool') {
            document.getElementById('m_valueBoolRb').checked = 'true';
            document.getElementById('m_valueTrueSpan').style.display = '';
            document.getElementById('m_valueFalseSpan').style.display = '';
        } else if (type == 'conditionCategory') {
            // make the category dropdown visible
            document.getElementById('m_valueConditionCategory').style.display = '';
        } else if (type == 'NDaysFromToday') {
            document.getElementById('m_valueTextboxRb').checked = 'true';
            document.getElementById('m_valueDesc').style.display = '';
            document.getElementById('m_valueDesc').innerHTML = 'Today';
            document.getElementById('m_operationNDaysFromTodayOperator').style.display = '';
            document.getElementById('m_valueTextbox1').style.display = '';
        }
            
        validateEditor();
    }

    function validateEditor() {
        var type = document.getElementById("m_hiddenEditorMode").value;
        var valid = true;
        if (type == 'textbox') {
            valid = document.getElementById('m_valueTextbox1').value != '';
            if (document.getElementById('m_operationBetween').checked)
                valid &= document.getElementById('m_valueTextbox2').value != '';
        }
        else if (type == 'var')
            valid = document.getElementById('m_valueVar').innerText != '';
        else if (type == 'bool')
            valid = document.getElementById('m_valueTrue').checked || document.getElementById('m_valueFalse').checked;
            
        document.getElementById('m_editorSaveBtn').disabled = !valid;            
    }
</script>
<table>
    <tr>
        <td style="background-color:#00BB00" class="Header" colspan="3">
            <ml:EncodedLiteral ID="m_pageHeader" runat="server"></ml:EncodedLiteral>
        </td>
    </tr>
    <tbody id="m_customVarSection" runat="server">
        <tr>
            <td colspan="3">
                Name: <asp:TextBox ID="m_customVariableName" runat="server" onkeyup="validateButtons();"></asp:TextBox>
            </td>
        </tr>
    </tbody>
    <tr>
        <td style="background-color:#666666" class="Header" colspan="3">
            Parameters
        </td>
    </tr>
    <asp:Repeater runat="server" ID="m_parameterSet" OnItemDataBound="OnParameterBound">
        <ItemTemplate>
            <tr class="GridItem">
                <td>
                    <ml:EncodedLiteral ID="m_parameterName" runat="server"></ml:EncodedLiteral>
                </td>
                <td>
                    <ml:EncodedLiteral ID="m_parameterValue" runat="server"></ml:EncodedLiteral>                    
                </td>
                <td>
                    <a id="m_parameterEdit" runat="server" href="#">edit</a>
                </td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="GridAlternatingItem">
                <td>
                    <ml:EncodedLiteral ID="m_parameterName" runat="server"></ml:EncodedLiteral>
                </td>
                <td>
                    <ml:EncodedLiteral ID="m_parameterValue" runat="server"></ml:EncodedLiteral>                    
                </td>
                <td>
                    <a id="m_parameterEdit" runat="server" href="#">edit</a>
                </td>
            </tr>
        </AlternatingItemTemplate>
    </asp:Repeater>
    <tr>
        <td colspan="3">
            <input type="button" id="m_addParameterBtn" value="Add Parameter" onclick="onAddParameter();" runat="server" />
        </td>
    </tr>
    <tr>
        <td style="background-color:#666666" class="Header" colspan="3">
            Notes
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:TextBox ID="m_tbNotes" Width="300px" Rows="6" runat="server" TextMode="MultiLine"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <ml:PassthroughLabel ID="m_saveErrorMsg" runat="server"></ml:PassthroughLabel>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <input type="button" value="Save" onclick="onSaveConfigItem();" id="m_saveItemBtn" runat="server" />
            <input type="button" value="Cancel" onclick="window.close();" />
        </td>
    </tr>
</table>
    
<div id="ParameterPickerPopup" class="modal" runat="server">
    <table id="m_parameterPicker" runat="server" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <th class="FormTableHeader" colspan="2">
                Select Parameter
            </th>
        </tr>
        <tr>
            <td class="FieldLabel" nowrap>
                <input type="radio" id="m_enableSearch" name="searchOption" checked />
                Search by Field Name or ID
                <input type="text" id="searchCriteria" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" colspan="2" nowrap>
                <input type="radio" id="m_disableSearch" name="searchOption" />
                Jump to category
                <ASP:DropDownList id="m_Group" runat="server" style="PADDING-LEFT: 4px; BORDER: 1px groove; FONT: 11px arial;"></ASP:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" colspan="2" nowrap>
                <input type="radio" id="m_searchPage" name="searchOption" />
                Jump to Page 
                <asp:DropDownList id="m_Page" runat="server" style="padding-left: 4px; border: 1px groove; font: 11px arial;"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="m_FieldList" style="height:200px; width:650px; overflow:auto; padding:3px; border:solid 1px black">
                    <asp:Repeater runat="server" ID="m_parameterList" OnItemDataBound="OnParameterListBound" EnableViewState="false">
                        <ItemTemplate>
                            <div id="m_choiceDiv" runat="server">
                                <a id="m_selectLink" runat="server" href="#">select</a>
                                <ml:EncodedLiteral ID="m_parameterData" runat="server"></ml:EncodedLiteral>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <input type="button" value="Cancel" onclick="Modal.Hide();" />
            </td>
        </tr>
    </table>
        
    <table id="m_parameterEditor" style="display:none" runat="server" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <td>
                Parameter: 
            </td>
            <td>
                <ml:EncodedLiteral ID="m_parameterName" runat="server"></ml:EncodedLiteral>
            </td>
        </tr>

        <tbody id="parameterPathSection">
            <tr id="propertyLevelRow" runat="server">
                <td>
                    Property Level:
                </td>
                <td>
                    <asp:RadioButtonList ID="propertyLevel" runat="server" RepeatDirection="Horizontal" onchange="onSelectPathPropertyLevel();">
                        <asp:ListItem Text="Individual" Value="INDIVIDUAL" />
                        <asp:ListItem Text="Subset" Value="SUBSET" />
                        <asp:ListItem Text="Whole" Value="WHOLE" />
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr id="subsetRow" runat="server">
                <td>
                    Subset Description:
                </td>
                <td>
                    <asp:DropDownList id="subsetDropdown" runat="server" />
                </td>
            </tr>
            <tr id="recordRow" runat="server">
                <td>
                    Record Description:
                </td>
                <td>
                    <asp:DropDownList id="recordDropdown" runat="server" />
                </td>
            </tr>
            <tr id="propertyRow" runat="server">
                <td>
                    <ml:EncodedLiteral id="propertyDescription" runat="server" /> Property:
                </td>
                <td>
                    <asp:DropDownList id="propertyDropdown" runat="server" onchange="onSelectPathProperty();" />
                </td>
            </tr>
        </tbody>

        <tr>
            <td valign="top" rowspan="17">
                Operation:
            </td>
            <td>
                <asp:RadioButton ID="m_operationBetween" Text="Between" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
            </td>
        </tr>
        <tr>
        <td>
                <asp:RadioButton ID="m_operationEqualInt" Text="Equal to" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
        </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="m_operationEqual" Text="Equal" runat="server" GroupName="m_ops" onclick="setValueType('bool');" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="m_operationEqualConditionCategory" CssClass="RadioOption" Text="Equal" runat="server" GroupName="m_ops" onclick="setValueType('conditionCategory');" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="m_operationEqualVar" Text="Equal to variable" runat="server" GroupName="m_ops" onclick="setValueType('var');" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="m_operationIsNonblankStr" Text="Is not a blank string" runat="server" GroupName="m_ops" onclick="setValueType('bool');" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="m_operationGreaterThan" Text="Greater Than" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="m_operationGreaterThanVar" Text="Greater than variable" runat="server" GroupName="m_ops" onclick="setValueType('var');" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="m_operationGreaterThanOrEqualTo" Text="Greater than or equal to" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="m_operationGreaterThanOrEqualToVar" Text="Greater than or equal to variable" runat="server" GroupName="m_ops" onclick="setValueType('var');" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="m_operationLessThan" Text="Less than" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="m_operationLessThanVar" Text="Less than variable" runat="server" GroupName="m_ops" onclick="setValueType('var');" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="m_operationLessThanOrEqualTo" Text="Less than or equal to" runat="server" GroupName="m_ops" onclick="setValueType('textbox');" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="m_operationLessThanOrEqualToVar" Text="Less than or equal to variable" runat="server" GroupName="m_ops" onclick="setValueType('var');" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="m_operationIsNonzeroValue" Text="Is a nonzero value" runat="server" GroupName="m_ops" onclick="setValueType('bool');" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="m_operationIsAnyValue" Text="Is any value" runat="server" GroupName="m_ops" onclick="setValueType('bool');" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="m_operationNDaysFromToday" Text="N days from today" runat="server" GroupName="m_ops" onclick="setValueType('NDaysFromToday');" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="m_operationDontCare" Text="Don't Care" runat="server" GroupName="m_ops" onclick="setValueType('');" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                Values:
            </td>
            <td>
                <ml:EncodedLabel style="display:none" id="m_valueDesc" runat="server"></ml:EncodedLabel>
                <asp:DropDownList style="display:none" ID="m_operationNDaysFromTodayOperator" runat="server">
                    <asp:ListItem Value="+" Text="+"></asp:ListItem>
                    <asp:ListItem Value="-" Text="-"></asp:ListItem>
                </asp:DropDownList>
                <asp:RadioButton ID="m_valueTextboxRb" GroupName="m_value" runat="server" style="display:none" />
                <asp:TextBox ID="m_valueTextbox1" runat="server" style="display:none" onkeyup="validateEditor();"></asp:TextBox>
                <asp:TextBox ID="m_valueTextbox2" runat="server" style="display:none" onkeyup="validateEditor();"></asp:TextBox>
                <asp:RadioButton ID="m_valueVarRb" GroupName="m_value" runat="server" style="display:none" />
                <ml:EncodedLabel ID="m_valueVar" runat="server" style="display:none"></ml:EncodedLabel>
                <a href="#" id="m_valueVarEdit" style="display:none" onclick="onPickVariable();">Pick variable</a>
                <asp:RadioButton ID="m_valueBoolRb" GroupName="m_value" runat="server" style="display:none" />
                <span id="m_valueTrueSpan" style="display:none" runat="server"><asp:RadioButton ID="m_valueTrue" Text="True" GroupName="m_valueBool" runat="server" onclick="validateEditor();" /></span>
                <span id="m_valueFalseSpan" style="display:none" runat="server"><asp:RadioButton ID="m_valueFalse" Text="False" GroupName="m_valueBool" runat="server" onclick="validateEditor();" /></span>
                <asp:DropDownList ID="m_valueConditionCategory" runat="server" style="display:none" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CheckBoxList ID="m_valueEnum" runat="server"></asp:CheckBoxList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Repeater ID="m_InAndNotInEnumRepeater" runat="server" OnItemDataBound="OnInAndNotInEnumRepeater">
                    <ItemTemplate>
                        <asp:RadioButtonList ID="radioList" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="InAndNotInList">
                            <asp:ListItem Text="Is" Value="IS" />
                            <asp:ListItem Text="Is Not" Value="IS_NOT" />
                            <asp:ListItem Text="N/A" Value="NA" Selected/>
                        </asp:RadioButtonList>
                        <ml:EncodedLabel ID="name" runat="server" AssociatedControlID="radioList" CssClass="InAndNotInName" />
                        <asp:HiddenField ID="value" runat="server" />
                        <br />
                    </ItemTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ml:PassthroughLabel ID="m_valueErrorMsg" runat="server"></ml:PassthroughLabel>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" id="m_editorSaveBtn" value="Save" onclick="onSaveParameter();" />
                <input type="button" value="Cancel" onclick="Modal.Hide();" />
            </td>
        </tr>
    </table>
</div>
