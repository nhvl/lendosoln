﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportSurvey.aspx.cs" Inherits="LendersOfficeApp.common.ReportSurvey" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body bgcolor="gainsboro">
<script type="text/javascript">
    var bMeasureSpeedTest = false;
    var fileSizeInBytes = 505174;
    var timeInMs, Mbps;
    function submitSurvey() {

        if (bMeasureSpeedTest == false) {
            window.setTimeout(submitSurvey, 200); // Wait until speed test complete.
            return;
        }
    $('#thankyouPanel').show();
    $('#surveyPanel').hide();

    var msg = '';
    if ($('#chkCannotDisplayWebpage').prop('checked')) {
        msg += "\r\nGetting Internet Explorer cannot display the webpage error message. YES";
    }
     if ($('#chkIEDoesnotResponse').prop('checked')) {
        msg += "\r\nInternet Explorer doesn't respond and needs to be shut down. YES";
    }
     if ($('#chkEdocEditorNotDisplayImages').prop('checked')) {
        msg += "\r\nEdoc editor does not display images. YES";

    }
     if ($('#chkSlow').prop('checked')) {
    msg += "\r\nSlowness but eventually the page shows up correctly. YES";
}

if ($('#txtPageExperiencedSlowness').val() != '') {
    msg += "\r\nWhich page were you on when you experienced slowness?\r\n\r\n" + $('#txtPageExperiencedSlowness').val();
}

if ($('#txtOperationSlow').val() != '') {
    msg += "\r\nWhich operation was slow?\r\n\r\n" + $('#txtOperationSlow').val();
}

msg += "r\nWhen did the issue occur?" + $("input[name=rbWhenIssueOccur]:checked").val();

var args = {
    speedTestInMs : timeInMs,
    Mbps : Mbps,
    speedTestSizeInBytes: fileSizeInBytes,
    comment : msg
};

gService.utils.call("MarkSlowness", args);



}

$(function() {
    // Measure speed test right after window load.
    var img = new Image();
    var startTime = new Date();

    img.src = gVirtualRoot + '/images/SpeedTest_Small.png?n=' + Math.random();
    img.onload = function() {
        var endTime = new Date();
        bMeasureSpeedTest = true;
        timeInMs = endTime.getTime() - startTime.getTime();
        Mbps = ((fileSizeInBytes * 8) / (timeInMs / 1000)) / (1024 * 1024);


    };

});
</script>
    <form id="form1" runat="server">
        <table class="FormTable" cellSpacing="0" cellpadding="0" width="100%" border="0">
            <tr><td class="FormTableHeader" style="padding:10px">Survey</td></tr>
            <tbody id="surveyPanel">
            <tr>
                <td style="padding-left:10px;padding-right:10px">
                <br />
                Please provide additional details about your slowness experience.
                    <ul>
                        <li><input type="checkbox" id="chkCannotDisplayWebpage"/>Getting "Internet Explorer cannot display the webpage" error message.</li>
                        <li><input type="checkbox" id="chkIEDoesnotResponse" />Internet Explorer doesn't respond and needs to be shut down.</li>
                        <li><input type="checkbox" id="chkEdocEditorNotDisplayImages" />Edoc editor does not display images.</li>
                        <li><input type="checkbox" id="chkSlow"/>Slowness but eventually the page shows up correctly.</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td style="padding-left:10px;padding-right:10px">
                    <ul>
                        <li>Which page were you on when you experienced slowness?
                        <br />
                        <textarea style="width:400px;height:50px" id="txtPageExperiencedSlowness"></textarea>
                        <br /><br />
                        </li>
                        
                        <li>Which operation was slow? (viewing, saving, uploading edocs, etc...)</li>
                        <br />
                        <textarea style="width:400px;height:50px" id="txtOperationSlow"></textarea>
                        <br /><br />
                        <li>When did the issue occur?
                        <br /><input type="radio" name="rbWhenIssueOccur" value="Still waiting for the page to response."/>Still waiting for the page to response.
                        <br /><input type="radio" name="rbWhenIssueOccur" value="Just happened."/>Just happened.

                        </li>
                    </ul>                
                </td>
            </tr>

            <tr><td align=center><input type="button" value="Submit" onclick="submitSurvey();"/></td></tr>
            </tbody>
            <tbody id="thankyouPanel" style="display:none">
                <tr>
                    <td>These details will help us diagnose the issue further. Thank you for your continued patience and participation.</td>
                </tr>
                <tr><td align="center"><input type="button" value="Close" onclick="self.close();"/></td></tr>
            </tbody>
        </table>
    <div>



    
    </div>
    </form>
</body>
</html>
