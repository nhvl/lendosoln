<%@ Page Language="C#" CodeBehind="PrintView_Menu.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.common.PrintView_Menu" %>
<html>
<head runat="server">
    <link href="../css/stylesheetnew.css" type="text/css" rel="stylesheet">
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
<form name="frm" method="post">

<table border="0" cellspacing="0" cellpadding="5" width="100%">
<tr>
    <td align="right" valign="top">
    <% if (Request["configure"] == "T") { %>
        <input type="button" onclick="callFrameMethod(parent, 'bodyiframe', 'Configure')" value="Configure" class="ButtonStyle">
    <% } %>

    <% if (Request["detailCert"] == "t") { %>
        <input type="button" onclick="parent.showNormalCert();" value="Show Normal Certificate" class="ButtonStyle">
    <% } %>

    <% if (Request["cmd"] == "print") { %>
        <input type="button" onclick="lqbPrintByFrame(retrieveFrame(parent, 'bodyiframe'))" value="Print" class="ButtonStyle">
    <% } %>
        <input type="button" onclick="top.window.close();" value="Close" class="ButtonStyle">
    </td>
</tr>
</table>

</form>
</body>
</html>
