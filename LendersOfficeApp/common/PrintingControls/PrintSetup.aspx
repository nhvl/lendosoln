<%@ Page language="c#" Codebehind="PrintSetup.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.common.PrintingControls.PrintSetup" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../ModalDlg/cModalDlg.ascx" %>
<HTML>
	<HEAD>
		<title>Print Settings</title>
		<script language="JavaScript">
		var g_ePrintCtrl = null;
		function GetConfigure()
		{
				return g_ePrintCtrl.CanConfigure();
		}
		
		function _init()
		{
		    if (!ML.IsIe) {
		        alert("You cannot access E-print from this browser.  Please ensure that you're using IE 11.");
		        onClosePopup();
		    }

		  g_ePrintCtrl = new ePrintCtrl();
		  if (g_ePrintCtrl.ePrintVersion == 0) {
		    // ePrint does not install. Disable print button.
		    document.getElementById('btnPrint').disabled = true;
		    document.getElementById('btnSettings').disabled = true;
		    return;
		  }
		  
		    document.getElementById('btnSettings').disabled = g_ePrintCtrl.ePrintVersion != 3;
		  
			<%
				if (Request["isConfig"] == "yes")
					Response.Write("document.getElementById('btnPrint').value = 'Save';");
			%>

			LoadOptions() ;
		
			var oPrintDlg = getModalArgs() || {};
			
			var printerName = oPrintDlg.PrinterName;
			if (printerName == "")
			  printerName = g_ePrintCtrl.GetDefaultPrinter();
			
			SetSelectOption(document.getElementById("printername"), printerName) ;
			SetSelectOption(document.getElementById("papersize"), oPrintDlg.PaperSize) ;

			if (!GetConfigure())
			  document.getElementById("btnSettings").disabled = true;
		}
		function SetSelectOption(selectOption, value)
		{
			for (var i = 0 ; i < selectOption.options.length ; i++)
			{
				if (value == selectOption[i].value)
				{
					selectOption.selectedIndex = i ;
					return ;
				}
			}
		}
		function LoadOptions()
		{
		  var rgPrinters = g_ePrintCtrl.GetPrinterNames();
			

			for (var i = 0 ; i < rgPrinters.length ; i++)
			{
				var option ;
				option = new Option(rgPrinters[i], rgPrinters[i]) ;
				document.getElementById("printername").options[i] = option ;
			}
				
			document.getElementById("papersize")[0] = new Option("LEGAL", "LEGAL") ;
			document.getElementById("papersize")[1] = new Option("LETTER", "LETTER") ;
			document.getElementById("papersize")[2] = new Option("AUTOMATIC", "AUTOMATIC");
			
		}
		function SaveOptions()
		{
			sDefaultPrinter = GetSetting("DEFAULT_PRINTER") ;
			sDefaultPaperSize = GetSetting("DEFAULT_PAPERSIZE") ;
			sPrintAsImage = GetSetting("PRINT_AS_IMAGE") ;
		}
		function onPrint()
		{
			var oPrintDlg = window.dialogArguments || {};
			oPrintDlg.PrintOK = true ;
			oPrintDlg.PrinterName = document.getElementById("printername").options[document.getElementById("printername").selectedIndex].value ;
			oPrintDlg.PaperSize = document.getElementById("papersize")[document.getElementById("papersize").selectedIndex].value ;
			oPrintDlg.nCopies = document.getElementById("nCopies").value;
			onClosePopup(oPrintDlg) ;
		}
		function onSettings()
		{
			// Popup settings page
			var theHeight = screen.height * .8 ;
			var theWidth = screen.width * .5 ;
			var nLeft = (screen.width - theWidth) / 2 ;
			var nTop = 0 ;

			showModal("/common/PrintingControls/AdvanceSettings.aspx", null, "dialogHeight: " + theHeight + "px; dialogWidth: " + theWidth + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: Yes", null, null, { hideCloseButton: true });
		}

		function SetSetting(sName, sValue)
		{
			setCookie(sName, sValue);
		}
		function GetSetting(sName)
		{
			var value = getCookie(sName);
			return value === null ? "" : value;
		}
        </script>
		<LINK rel="stylesheet" type="text/css" href="../../css/stylesheet.css">
		<script language=javascript src="ePrintCtrl.js"></script>
	</HEAD>
	<body>
		<h4 class="page-header">Print Settings</h4>
		<form name="PrintSetup" id="PrintSetup" method="post" runat="server">
			<table border="0" cellspacing="0" cellpadding="2" width="100%" class="Panel">
				<tr>
					<td>Printer:</td>
					<td>
						<select name="printername" id="printername">
						</select>
					</td>
				</tr>
				<tr>
					<td>Paper Size:</td>
					<td>
						<select name="papersize" id="papersize">
						</select>
					</td>
				</tr>
				<tr>
				  <td>Number Of Copies:</td>
				  <td><input type=text name="nCopies" id="nCopies" value="1" style="width:40px"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="2" width="100%" class="Panel">
				<tr>
					<td align="right">
						<input type="button" onclick="onPrint();" value="Print" id="btnPrint" name="btnPrint">
						<input type="button" onclick="onClosePopup();" value="Cancel" id="btnCancel" name="btnCancel">
						<input type="button" onclick="onSettings();" value="Settings" id="btnSettings" name="btnSettings">
					</td>
				</tr>
			</table>
			<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
		</form>
	</body>
</HTML>
