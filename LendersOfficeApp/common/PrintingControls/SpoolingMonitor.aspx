<%@ Page language="c#" Codebehind="SpoolingMonitor.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.common.PrintingControls.SpoolingMonitor" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Spooling Monitor</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<style> .TableHeader { BACKGROUND-COLOR: #d4d4d4; BORDER-BOTTOM: #808080 1px solid; BORDER-LEFT: #ffffff 1px solid; BORDER-RIGHT: #808080 1px solid; BORDER-TOP: #ffffff 1px solid; FONT-FAMILY: Verdana, Arial, Helvetica; FONT-SIZE: 8pt; FONT-WEIGHT: bold; color:navy; }
	.TableBody { FONT-FAMILY: Verdana, Arial, Helvetica; FONT-SIZE: 10pt; color:navy; }
	input.wizbutton { FONT-FAMILY: Verdana, Arial, Helvetica; FONT-SIZE: 10pt; FONT-WEIGHT: bold; background-color:gainsboro; color:navy; }
	td.optiontext { FONT-FAMILY: Verdana, Arial, Helvetica; FONT-SIZE: 10pt; FONT-WEIGHT: bold; color:navy; }
	select.wizoption { FONT-FAMILY: Verdana, Arial, Helvetica; FONT-SIZE: 8pt; FONT-WEIGHT: bold; color:navy; background-color:whitesmoke; }
		</style>
		<script language="JavaScript">
	var lastError = "No error";
	
	function init()
	{
		setTimeout("checkStatus();", 2000) ;
	}
	function checkStatus()
	{
		var ePrint = new ActiveXObject("ComPDFPrinter.PDFPrinter.1") ;
		if (ePrint.IsClearOfJobs())
			self.close() ;
		else
		{
			setTimeout("checkStatus();", 2000) ;
			var lastError = ePrint.GetLastError();
		}
	}
	function cancelJobs()
	{
		if (confirm("Cancel all print jobs?"))
		{
			var ePrint = new ActiveXObject("ComPDFPrinter.PDFPrinter.1") ;
			ePrint.CancelAllJobs() ;
		}
	}
		</script>
	</HEAD>
	<body link="blue" vlink="blue" alink="blue" style="CURSOR:wait" onload="init();">
		<form id="SpoolingMonitor" method="post" runat="server">
			<table BORDER="0" CELLSPACING="1" CELLPADDING="1" class="field_caption" width="100%">
				<tr>
					<td align="middle" valign="top">
						<b>Printing</b>
					</td>
				</tr>
				<tr>
					<td align="middle" valign="top">
						<img src="../../images/status.gif" WIDTH="150" HEIGHT="10">
					</td>
				</tr>
				<tr>
					<td align="middle" valign="top">
						<input type="button" value="Cancel All Jobs" onclick="cancelJobs();" id="button1" name="button1">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button" onclick="self.close();" value="Close" name="btnClose">
					</td>
				</tr>
			</table>
			<br>
		</form>
	</body>
</HTML>
