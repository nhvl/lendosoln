<%@ Page language="c#" Codebehind="ePrintInstaller.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.common.PrintingControls.ePrintInstaller" %>
<%@ Import namespace="LendersOffice.Constants"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Please install our ePrint Control</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="../ModalDlg/CModalDlg.js"></script>		
<script language=javascript>
<!--
function init() {
  resize(360, 110);
}
//-->
</script>
	</HEAD>
	<body style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; PADDING-TOP: 10px;" bgcolor="gainsboro" onload="init();">
		<form id="ePrintInstaller" method="post" runat="server">
			<font face="verdana" size="2">ePrint must be installed for this feature to work.&nbsp; <a href="../AppDownload.aspx?key=<%=AspxTools.HtmlString(ConstAppDavid.DownloadFile_EprintV4_Updated)%>">Click here to install ePrint</a>.
			<p align=center><input type="button" value="Close" onclick="window.close();"></p>
		</font>
		</form>
	</body>
</HTML>
