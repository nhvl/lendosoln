/* 
print method 1 = ePrint (used in LO)
print method 2 = express print	(not used in LO but may be used in the future)
	
09/09/03-David
Add ability to print out mix size document in batch. For mix size to work the
programmer has to specify the size of document in the code before hand.	
*/

function ePrintCtrl()
{
	this.Print = ePrintCtrl_Print ;
	this.BatchPrint = ePrintCtrl_BatchPrint ;
	this.SessionID = "" ;
	this.ePrintVersion = 0;
	this.CanConfigure = ePrintCtrl_CanConfigure;
	this.GetPrinterNames = ePrintCtrl_GetPrinterNames;
	this.GetDefaultPrinter = ePrintCtrl_GetDefaultPrinter;


  try {
    this.__ePrintImpl = new ActiveXObject("ePrintV4COM.MLPDFPrint");
    this.PrintMethod = 1;
    this.ePrintVersion = 4;
  } 
  catch (e) 
  {
	  try
	  {
		  this.__ePrintImpl = new ActiveXObject("ComPDFPrinter.PDFPrinter.1") ;
		  this.PrintMethod = 1 ;
		  this.ePrintVersion = 3;
	  }
	  catch (e)
	  {
	      try 
	      {
	          this.__ePrintImpl = new ActiveXObject("ePrintDotNet.ePrint");
	          this.PrintMethod = 1;
	          this.ePrintVersion = 5;
	      }
	      catch (e) {
	          try {
	              this.__ePrintImpl = new ActiveXObject("ePrintDotNet.ePrint.1");
	              this.PrintMethod = 1;
	              this.ePrintVersion = 5;
	          }
	          catch (e) {
	              var sURL = gVirtualRoot + "/common/PrintingControls/ePrintInstaller.aspx";
				  showModeless(sURL, "dialogHeight:400px;dialogwidth:550px;status:no;resizable:yes;scroll:no;");
	          }
	      }
	  }
	}

	return this ;
}
function ePrintCtrl_CanConfigure() {
  if (this.ePrintVersion == 4)
    return false;
  else if (this.ePrintVersion == 3)
    return this.__ePrintImpl.CanConfigure();
  else
    return false;
}
function ePrintCtrl_GetPrinterNames() {
    if (this.ePrintVersion == 4) {
        return (new VBArray(this.__ePrintImpl.MLEnumeratePrinters())).toArray();
    } else if (this.ePrintVersion == 3) {
        return (new VBArray(this.__ePrintImpl.GetPrinterNames())).toArray();

    } else if (this.ePrintVersion == 5) {
    return (this.__ePrintImpl.GetAvailablePrinters());
    }
    else {
        return null;
    }
}

function ePrintCtrl_GetDefaultPrinter() {
  if (this.ePrintVersion == 4) {
    return this.__ePrintImpl.MLGetDefaultPrinter();
  } else if (this.ePrintVersion == 3) {
    return this.__ePrintImpl.GetDefaultPrinterName();
  } else if (this.ePrintVersion == 5) {
    return this.__ePrintImpl.GetDefaultPrinterName();
  } 
  else {
    return null;
  }
}

var g_oPrintSetupDialog = null;
function ShowPrintSetup(callback) {
  g_oPrintSetupDialog = new cPrintSetupDialog();
  g_oPrintSetupDialog.Show(callback);
}
function GetPrinterName() {
  if (null != g_oPrintSetupDialog)
    return g_oPrintSetupDialog.PrinterName;
  else
    return "";
}

/**
 09/09/03 - David
 Add rgSizes to support mix size printing in batch. rgSizes contain integer: 0 - Letter, 1 - Legal
*/
function ePrintCtrl_BatchPrint(rgURLs, rgSizes, bShowPrintDlg, getNewAuthId, baseJobName)
{
	if (1 != this.PrintMethod)
		return false ;
		
	// new ePrint method
	var sPrinterName = this.GetDefaultPrinter();

	// init default parameters
	var nCopies = 1 ;
	var bLetterSize = false ;
	var bDontFitToPage = false ;
	var bPrintAsImage = false ;
	var bAutomaticSize = false; // 09/09/03 -dd

	if (bShowPrintDlg)
	{
		var oPrintSetupDialog = new cPrintSetupDialog() ;
		if (!oPrintSetupDialog.Show()) return false ;	// exit if the user canceled the print job
			
		// override default parameters with values from the print dialog box
		sPrinterName = oPrintSetupDialog.PrinterName ;
		bAutomaticSize = oPrintSetupDialog.PaperSize == "AUTOMATIC";
		bLetterSize = oPrintSetupDialog.PaperSize == "LETTER" ;
		bPrintAsImage = oPrintSetupDialog.PrintAsImage ;
		
		// show the spooling monitor (aka progress bar)
		// 8/15/2006 dd - Support new version ePrint. Spooling does not work in new version.
		//var oMonitor = new cSpoolingMonitor() ;
		//oMonitor.Show() ;
	}
	if (null != g_oPrintSetupDialog) {
	  sPrinterName = g_oPrintSetupDialog.PrinterName;
		bAutomaticSize = g_oPrintSetupDialog.PaperSize == "AUTOMATIC";
		bLetterSize = g_oPrintSetupDialog.PaperSize == "LETTER" ;
		bPrintAsImage = g_oPrintSetupDialog.PrintAsImage ;	
		nCopies = g_oPrintSetupDialog.nCopies;
}
	
	
	// print the jobs
for (var i = 0; i < rgURLs.length; i++) {

    var sJobName = "LendersOfficePrinting";
    var sPaperType = "LETTER";
    if (bAutomaticSize && rgSizes[i] == '1') {
        sPaperType = "LEGAL";
    } else if (bAutomaticSize && rgSizes[i] == '0') {
        sPaperType = "LETTER";
    } else {
        sPaperType = bLetterSize ? "LETTER" : "LEGAL";
    }
        var sJobName = (baseJobName || "LendersOfficePrinting") + "_" + sPaperType;
    if (this.ePrintVersion == 4) {

        this.__ePrintImpl.MLPrintURL(rgURLs[i], sJobName, sPrinterName, sPaperType, bDontFitToPage, true /*bRemovePDFAfterPrinting*/, nCopies);

    } else if (this.ePrintVersion == 3) {
        this.__ePrintImpl.PrintURL(rgURLs[i], nCopies, bAutomaticSize ? rgSizes[i] == '0' : bLetterSize, bDontFitToPage, bPrintAsImage, sPrinterName);

    } else if (this.ePrintVersion == 5) {
    this.__ePrintImpl.DefaultPaperSize = sPaperType;
    var authId = getNewAuthId();
    this.__ePrintImpl.PrintToNamedPrinter(rgURLs[i] + '&auth_id=' + authId, sJobName, sPrinterName);

    }

    else {
        return false;
    }
}

return true;
}
function ePrintCtrl_Print(sURL, bShowPrintDlg)
{
	var rgURL = new Array() ;
	rgURL[0] = sURL ;
	this.PrintBatch(rgURL, bShowPrintDlg) ;
}
// todo: I may need to use this to prevent caching from occuring..not sure yet.
function urlCracker()
{
	return escape((new Date().toString())) ;
}
/* ********************************************************************************
	cPrintSetupDialog
	
	This class manages a print dialog box and print settings specific to the MCL system.

	02/13/02-Binh
	Created the class as a replacement for the current print dialog box which is
	dependent on ePrint v1.
	
	04/25/02-Lawrence
	Added isConfig and showConfig to allow AutoPrint users to configure printing
	options without actually printing.  (In other words, the "Print" button is
	relabeled "Save" to reduce anxiety and/or confusion.)
	Also: Allow non-default settings with no dialog.
	

******************************************************************************** */
function cPrintSetupDialog()
{
// public methods
	this.Show = cPrintSetupDialog_Show ;
	this.ShowConfig = cPrintSetupDialog_ShowConfig ;
	this.GetSettings = cPrintSetupDialog_GetSettings ;

// properties
	this.PrintOK = false ;
	this.PrinterName = "" ;
	this.PaperSize = "" ;
	this.PrintAsImage = false ;
	this.isConfig = false;
	this.nCopies = 1;
	
// private methods
	this.SetSetting = cPrintSetupDialog_SetSetting ;
	this.GetSetting = cPrintSetupDialog_GetSetting ;
	
	return this ;
}
function cPrintSetupDialog_Show(callback)
{
	this.PrintOK = false ;
	this.PrinterName = this.GetSetting("PRINTER_NAME") ;
	this.PaperSize = this.GetSetting("PAPER_SIZE") ;
	this.PrintAsImage = this.GetSetting("PRINT_AS_IMAGE") == "1" ;


	if ("" == this.PaperSize)
		this.PaperSize = "AUTOMATIC" ;

	// display the print setup window
	var nLeft = (screen.width - 300) / 2 ;
	var nTop = (screen.height - 30) / 2 ;

	
	var sDlgOptions ;
	sDlgOptions = "" ;
	
	if (this.isConfig)
	{
		sDlgOptions = "?isConfig=yes" ;
	}

	

	showModal("/common/PrintingControls/PrintSetup.aspx", this, "dialogHeight: 200px; dialogWidth: 370px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No", null,
        function (printObject) {
        
            return function(args) {
                if (args.PrintOK) {
                    printObject.SetSetting("PRINTER_NAME", args.PrinterName);
                    printObject.SetSetting("PAPER_SIZE", args.PaperSize);
                    printObject.SetSetting("PRINT_AS_IMAGE", args.PrintAsImage ? "1" : "0");
                }
                if (callback) {
                    callback(args.PrintOK);
                }
            }
        }(this)
	, { hideCloseButton: true });
}
function cPrintSetupDialog_ShowConfig()
{
	this.isConfig = true ;
	return this.Show() ;
}
function cPrintSetupDialog_GetSettings()
{
	// Used to retrieve settings without showing dialogs
	this.PrinterName = this.GetSetting("PRINTER_NAME") ;
	this.PaperSize = this.GetSetting("PAPER_SIZE") ;
	this.PrintAsImage = this.GetSetting("PRINT_AS_IMAGE") == "1" ;

	// set default settings if they are blank ;
	if ("" == this.PrinterName)
	{
		var ePrint = new ActiveXObject("ComPDFPrinter.PDFPrinter.1") ;
		this.PrinterName = ePrint.GetDefaultPrinterName() ;
	}
	if ("" == this.PaperSize)
		this.PaperSize = "LEGAL" ;
		
	return true ;
}

function cPrintSetupDialog_SetSetting(sName, sValue)
{
	setCookie(sName, sValue);
}

function cPrintSetupDialog_GetSetting(sName)
{
	var value = getCookie(sName);
	return value == null ? "" : value;
}

/*
	Display a progress bar so that the user doesn't get impatience and wonder when his document
	will come out. This doesn't actually do any spooling monitoring. It only display an animated 
	progress bar.
*/
function cSpoolingMonitor()
{
	this.Show = cSpoolingMonitor_Show ;
	this.Close = cSpoolingMonitor_Close;
	this.oWnd = null;
	
	return this ;
}
function cSpoolingMonitor_Show()
{
	// show the progress bar centered on the screen
	var nLeft = (screen.width - 300) / 2 ;
	var nTop = (screen.height - 30) / 2 ;
	this.oWnd = window.open(gVirtualRoot + "/common/PrintingControls/SpoolingMonitor.aspx","printdlg","scrollbars=no,resizable=no,toolbar=no,titlebar=no,status=no,height=30,width=300,left=" + nLeft + "px,top=" + nTop + "px,screenX=0,screenY=0") ;
	this.oWnd.focus() ;
	return true ;
}

/**
3/15/04 dd - Add method to close spooling windows explicitly. 
*/
function cSpoolingMonitor_Close() {
    if (null != this.oWnd) this.oWnd.close();
}

