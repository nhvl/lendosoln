<%@ Page language="c#" Codebehind="AdvanceSettings.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.common.PrintingControls.AdvanceSettings" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<HTML>
	<HEAD>
		<TITLE>Settings for Tuning the Performance of ePrint</TITLE>
		<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
		<LINK rel="stylesheet" type="text/css" href="../../css/stylesheet.css">
			<script src="../../inc/jquery-1.6.min.js"></script>
			<script src="../../inc/LQBPopup.js"></script>
			<script src="../ModalDlg/CModalDlg.js"></script>
			<script src="../../inc/common.js"></script>
			<script type="text/javascript">

	var ePrint;
	var version;
	
	var downloadMaxAttempts;
	var downloadSleeptime;
	var downloadTimeout;
	var downloadFlushPeriod;
	var downloadLifetime;

	var printMaxAttempts;
	var printSleeptime;
	var printTimeout;
	var printAcroStartupTime;
	
	var useRawFilename;
	var useWaitForFileSleeptime;
	var useWaitForFileTimeout;
	
	var bypassSet;
	var bypassPrintQueuePeriod;
	var bypassTemplateSize;
	var bypassPageSize;
	var bypassDocumentTime;
	var bypassPageTime;
	
	var logName;
	var logDo;
	var logPerm;
	var VRoot = <%= AspxTools.SafeUrl(DataAccess.Tools.VRoot) %>;
	function SetBypass()
	{
		ePrint.SetPrintQueuePeriod(parseInt(document.getElementById("inputbox1").value));
		ePrint.SetPDFTemplateSize(parseInt(document.getElementById("inputbox2").value));
		ePrint.SetPDFPageSize(parseInt(document.getElementById("inputbox3").value));
		ePrint.SetAcroDocumentSpoolTime(parseInt(document.getElementById("inputbox4").value));
		ePrint.SetAcroPageSpoolTime(parseInt(document.getElementById("inputbox5").value));
	}
	
	function SetNoBypass()
	{
		ePrint.SetFileWaitSleepTime(parseInt(document.getElementById("inputbox2").value));
		ePrint.SetFileWaitTimeout(parseInt(document.getElementById("inputbox3").value));
	}
	
	function CaptureBypass()
	{
		bypassPrintQueuePeriod = parseInt(document.getElementById("inputbox1").value);
		bypassTemplateSize = parseInt(document.getElementById("inputbox2").value);
		bypassPageSize = parseInt(document.getElementById("inputbox3").value);
		bypassDocumentTime = parseInt(document.getElementById("inputbox4").value);
		bypassPageTime = parseInt(document.getElementById("inputbox5").value);
	}
	
	function CaptureNoBypass()
	{
		useWaitForFileSleeptime = parseInt(document.getElementById("inputbox2").value);
		useWaitForFileTimeout = parseInt(document.getElementById("inputbox3").value);
	}
	
	function ShowBypass()
	{
		document.getElementById("usevirtualprinter").checked = false;
		document.getElementById("bypassvirtualprinter").checked = true;
		document.getElementById("description1").value = "Print Queue Periond in seconds";
		document.getElementById("description2").value = "Template size in 1/10 KB";
		document.getElementById("description3").value = "Page size in 1/20 KB";
		document.getElementById("description4").value = "Document base time in milliseconds";
		document.getElementById("description5").value = "Page time in milliseconds";
		 // REMEMBER TO CHANGE THIS IF YOU CHANGE inputChars
		document.getElementById("box1").innerHTML = "<input id=inputbox1 border=0 size=8 >";
		document.getElementById("inputbox1").value = bypassPrintQueuePeriod;
		document.getElementById("inputbox2").value = bypassTemplateSize;
		document.getElementById("inputbox3").value = bypassPageSize;
		document.getElementById("inputbox4").value = bypassDocumentTime;
		document.getElementById("inputbox5").value = bypassPageTime;
		document.getElementById("inputbox4").style.visibility = "visible";
		document.getElementById("inputbox5").style.visibility = "visible";
		document.getElementById("expinputbox1").innerHTML = "<U>Explain</U>";
		document.getElementById("expinputbox2").innerHTML = "<U>Explain</U>";
		document.getElementById("expinputbox3").innerHTML = "<U>Explain</U>";
		document.getElementById("expinputbox4").innerHTML = "<U>Explain</U>";
		document.getElementById("expinputbox5").innerHTML = "<U>Explain</U>";
	}
	
	function ShowNoBypass()
	{
		document.getElementById("usevirtualprinter").checked = true;
		document.getElementById("bypassvirtualprinter").checked = false;
		document.getElementById("description1").value = "Raw File Name";
		document.getElementById("description2").value = "File Wait SleepTime in milliseconds";
		document.getElementById("description3").value = "File Wait Timeout in seconds";
		document.getElementById("description4").value = "";
		document.getElementById("description5").value = "";
		 // REMEMBER TO CHANGE THIS IF YOU CHANGE inputChars
		document.getElementById("box1").innerHTML = "<input id=inputbox1 contentEditable=false border=0 style=\"BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none\" class=\"Panel\" size=8 >";
		document.getElementById("inputbox1").value = useRawFilename;
		document.getElementById("inputbox2").value = useWaitForFileSleeptime;
		document.getElementById("inputbox3").value = useWaitForFileTimeout;
		document.getElementById("inputbox4").value = "";
		document.getElementById("inputbox5").value = "";
		document.getElementById("inputbox4").style.visibility = "hidden";
		document.getElementById("inputbox5").style.visibility = "hidden";
		document.getElementById("expinputbox1").innerHTML = "<U>Explain</U>";
		document.getElementById("expinputbox2").innerHTML = "<U>Explain</U>";
		document.getElementById("expinputbox3").innerHTML = "<U>Explain</U>";
		document.getElementById("expinputbox4").innerHTML = "";
		document.getElementById("expinputbox5").innerHTML = "";
	}
	
	function ShowLogFlags()
	{
		if (logDo)
		{
			document.getElementById("dologging").checked = true;
			document.getElementById("permanentlog").disabled = false;
			if (logPerm)
			{
				document.getElementById("permanentlog").checked = true;
			}
			else
			{
				document.getElementById("permanentlog").checked = false;
			}
		}
		else
		{
			document.getElementById("dologging").checked = false;
			document.getElementById("permanentlog").checked = false;
			document.getElementById("permanentlog").disabled = true;
		}
	}
	
	function ShowAllSettings()
	{
		//ePrint
		document.getElementById("versionSpace").value = version;
		document.getElementById("downloadmaxattempts").value = downloadMaxAttempts;
		document.getElementById("downloadsleeptime").value = downloadSleeptime;
		document.getElementById("downloadtimeout").value = downloadTimeout;
		document.getElementById("downloadflushperiod").value = downloadFlushPeriod;
		document.getElementById("downloadtemplifetime").value = downloadLifetime;
		document.getElementById("downloadtemplifetime").value = downloadLifetime;
		document.getElementById("printmaxattempts").value = printMaxAttempts;
		document.getElementById("printsleeptime").value = printSleeptime;
		document.getElementById("printtimeout").value = printTimeout;
		document.getElementById("printacrostartuptime").value = printAcroStartupTime;
		
		// ML Printer
		if (bypassSet)
		{
			ShowBypass();
		}
		else
		{
			ShowNoBypass();
		}
		
		// Logging
		
		if (!logDo) logPerm = false;
		document.getElementById("logfile").value = logName;
		ShowLogFlags();
	}

	function GetAllSettings()
	{
		//ePrint
		version = ePrint.Version();
		downloadMaxAttempts = ePrint.GetDownloadAttemptsCount();
		downloadSleeptime = ePrint.GetURLSleepTime();
		downloadTimeout = ePrint.GetURLTimeout();
		downloadFlushPeriod = ePrint.GetTempFilePeriod();
		downloadLifetime = ePrint.GetTempFileLifetime();
		printMaxAttempts = ePrint.GetPrintAttemptsCount();
		printSleeptime = ePrint.GetJobSleepTime();
		printTimeout = ePrint.GetJobTimeout();
		printAcroStartupTime = ePrint.GetAcroStartupTime();
		
		//ML Printer
		bypassSet = ePrint.GetVirtualPrintBypass();
		bypassPrintQueuePeriod = ePrint.GetPrintQueuePeriod();
		bypassTemplateSize = ePrint.GetPDFTemplateSize();
		bypassPageSize = ePrint.GetPDFPageSize();
		bypassDocumentTime = ePrint.GetAcroDocumentSpoolTime();
		bypassPageTime = ePrint.GetAcroPageSpoolTime();
		
		useRawFilename = ePrint.GetRawFilename();
		useWaitForFileSleeptime = ePrint.GetFileWaitSleepTime();
		useWaitForFileTimeout = ePrint.GetFileWaitTimeout();
	
		// Logging
		logName = ePrint.GetLoggingFilename();
		logDo = ePrint.GetLoggingFlag();
		logPerm = ePrint.GetPermLoggingFlag();
	}
	
	function OnInit()
	{
		ePrint = new ActiveXObject("ComPDFPrinter.PDFPrinter.1");
		
		GetAllSettings();
		ShowAllSettings();
	}
	
	function OnRadioChange()
	{
		var bypass1 = document.getElementById("bypassvirtualprinter").checked;
		var bypass2 = document.getElementById("usevirtualprinter").checked;
		if (bypass1 && !bypassSet) // The bypass was set by the user
		{
			document.getElementById("usevirtualprinter").checked = false;
			bypassSet = true;
			CaptureNoBypass();
			ShowBypass();
		}
		else if (bypass2 && bypassSet) // The 'use' was set by the user
		{
			document.getElementById("bypassvirtualprinter").checked = false;
			bypassSet = false;
			CaptureBypass();
			ShowNoBypass();
		}
		else
		{
			// CANNOT GET HERE
		}
		
		// Need to redraw the screen
	}
	
	function OnLogChange()
	{
		logDo = !logDo;
		if (!logDo) logPerm = false;
		ShowLogFlags();
	}
	
	function OnPermChange()
	{
		if (!logDo)
		{
			logPerm = false;
		}
		else
		{
			logPerm = !logPerm;
		}
		ShowLogFlags();
	}
	
	function EnsurePermission()
	{
		if ("jem" != document.getElementById("PwdField").value)
		{
			alert("INCORRECT Magic word, changes to numeric values will NOT take effect.\nHowever, changes to logging mode and bypass ARE accepted.");
			onClosePopup();
			return false;
		}
		
		return true;
	}
	
	function OnOK()
	{
		// Logging changes and bypass can be set without permission
		ePrint.SetLoggingFlag(logDo);
		ePrint.SetPermLoggingFlag(logPerm);
		ePrint.SetVirtualPrintBypass(bypassSet);
		
		if (!EnsurePermission()) return;
		
		// ePrint
		ePrint.SetDownloadAttemptsCount(parseInt(document.getElementById("downloadmaxattempts").value));
		ePrint.SetURLSleepTime(parseInt(document.getElementById("downloadsleeptime").value));
		ePrint.SetURLTimeout(parseInt(document.getElementById("downloadtimeout").value));
		ePrint.SetTempFilePeriod(parseInt(document.getElementById("downloadflushperiod").value));
		ePrint.SetTempFileLifetime(parseInt(document.getElementById("downloadtemplifetime").value));
		ePrint.SetPrintAttemptsCount(parseInt(document.getElementById("printmaxattempts").value));
		ePrint.SetJobSleepTime(parseInt(document.getElementById("printsleeptime").value));
		ePrint.SetJobTimeout(parseInt(document.getElementById("printtimeout").value));
		ePrint.SetAcroStartupTime(parseInt(document.getElementById("printacrostartuptime").value));
		
		// ML Printer
		if (bypassSet)
		{
			SetBypass();
		}
		else
		{
			SetNoBypass();
		}
		
		onClosePopup();
	}
	
	function OnRestore()
	{
		if (!EnsurePermission()) return;
		
		ePrint.RestoreDefaultSettings();
		GetAllSettings();
		ShowAllSettings();
	}
	
	var width = 600;
	var height = 250;
	var bigheight = 400;
	var nLeft = (screen.width - width) / 2 ;
	var nTop = (screen.height - height) / 2;
		
	function OnExpEPrint()
	{
		showModal("common/PrintingControls/PrintSettingExplanations/SummaryExp.htm", this, "dialogHeight: " + bigheight + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
	}
		
	function OnExpDownloadAttempts()
	{
		showModal("common/PrintingControls/PrintSettingExplanations/MaxDownload.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
	}
		
	function OnExpDownloadSleeptime()
	{
		showModal("common/PrintingControls/PrintSettingExplanations/DownloadSleeptime.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
	}
		
	function OnExpDownloadTimeout()
	{
		showModal("common/PrintingControls/PrintSettingExplanations/DownloadTimeout.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
	}
		
	function OnExpFlushPeriod()
	{
		showModal("common/PrintingControls/PrintSettingExplanations/FlushPeriod.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
	}
		
	function OnExpTempLifetime()
	{
		showModal("common/PrintingControls/PrintSettingExplanations/TempLifetime.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
	}
		
	function OnExpPrintAttempts()
	{
		showModal("common/PrintingControls/PrintSettingExplanations/MaxPrint.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
	}
		
	function OnExpPrintSleeptime()
	{
		showModal("common/PrintingControls/PrintSettingExplanations/PrintSleeptime.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
	}
		
	function OnExpPrintTimeout()
	{
		showModal("common/PrintingControls/PrintSettingExplanations/PrintTimeout.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
	}
	
	function OnExpAcroStartupTime()
	{
		showModal("common/PrintingControls/PrintSettingExplanations/AcroStartupTime.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
	}
	
	function OnExpUseVirtual()
	{
		showModal("common/PrintingControls/PrintSettingExplanations/AlwaysMLPrinter.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
	}
	
	function OnExpBypassVirtual()
	{
		showModal("common/PrintingControls/PrintSettingExplanations/BypassMLPrinter.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
	}
	
	function OnExp1()
	{
		if (bypassSet)
		{
			showModal("common/PrintingControls/PrintSettingExplanations/PrintQueuePeriod.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
		}
		else
		{
			showModal("common/PrintingControls/PrintSettingExplanations/RawFilename.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
		}
	}
		
	function OnExp2()
	{
		if (bypassSet)
		{
			showModal("common/PrintingControls/PrintSettingExplanations/PDFTemplateSize.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
		}
		else
		{
			showModal("common/PrintingControls/PrintSettingExplanations/FileWaitSleeptime.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
		}
	}
		
	function OnExp3()
	{
		if (bypassSet)
		{
			showModal("common/PrintingControls/PrintSettingExplanations/PDFPageSize.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
		}
		else
		{
			showModal("common/PrintingControls/PrintSettingExplanations/FileWaitTimeout.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
		}
	}
		
	function OnExp4()
	{
		if (bypassSet)
		{
			showModal("common/PrintingControls/PrintSettingExplanations/AcroDocumentTime.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
		}
	}
		
	function OnExp5()
	{
		if (bypassSet)
		{
			showModal("common/PrintingControls/PrintSettingExplanations/AcroPageTime.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
		}
	}
	
	function OnExpLogFilename()
	{
		showModal("common/PrintingControls/PrintSettingExplanations/LogFilename.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
	}
	
	function OnExpDoLog()
	{
		showModal("common/PrintingControls/PrintSettingExplanations/ExtensiveLogging.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
	}
	
	function OnExpDoPerm()
	{
		showModal("common/PrintingControls/PrintSettingExplanations/PermanentLogging.htm", this, "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: " + nTop + "px; dialogLeft: " + nLeft + "px; edge: Sunken; center: Yes; help: No; resizable: Yes; status: No;scroll: No") ;
	}
		
			</script>
	</HEAD>
	<BODY onload="OnInit()">
		<h4 class="page-header">Settings</h4>
		<form name="AdvanceSettings" method="post" runat="server">
			<TABLE border="0" cellspacing="0" cellpadding="2" align="center" cols="4">
				<TR>
					<TD colspan="2" class="Panel" align="left"><H3>ePrint</H3>
					</TD>
					<TD class="Panel"><DIV onClick="OnExpEPrint()" style="COLOR:blue"><U>Explain</U></DIV>
					</TD>
					<TD></TD>
				</TR>
				<TR>
					<TH class="Panel" align="right">
						Version:</TH>
					<TD class="Panel"><input id="versionSpace" contentEditable="false" border="0" style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none" class="Panel"></TD>
					<TD class="Panel"></TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
				</TR>
				<TR>
					<TH class="Panel" align="right">
						Download:</TH>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel">Max download attempts</INPUT></TD>
					<TD class="Panel"><input id="downloadmaxattempts" size="8"></TD>
					<TD class="Panel"><DIV onClick="OnExpDownloadAttempts()" style="COLOR:blue"><U>Explain</U></DIV>
					</TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel">Sleeptime in milliseconds</TD>
					<TD class="Panel"><input id="downloadsleeptime" size="8"></TD>
					<TD class="Panel"><DIV onClick="OnExpDownloadSleeptime()" style="COLOR:blue"><U>Explain</U></DIV>
					</TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel">Timeout in seconds</TD>
					<TD class="Panel"><input id="downloadtimeout" size="8"></TD>
					<TD class="Panel"><DIV onClick="OnExpDownloadTimeout()" style="COLOR:blue"><U>Explain</U></DIV>
					</TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel">Temp file flush period in minutes</TD>
					<TD class="Panel"><input id="downloadflushperiod" size="8"></TD>
					<TD class="Panel"><DIV onClick="OnExpFlushPeriod()" style="COLOR:blue"><U>Explain</U></DIV>
					</TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel">Temp file lifetime in minutes</TD>
					<TD class="Panel"><input id="downloadtemplifetime" size="8"></TD>
					<TD class="Panel"><DIV onClick="OnExpTempLifetime()" style="COLOR:blue"><U>Explain</U></DIV>
					</TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
				</TR>
				<TR>
					<TH class="Panel" align="right">
						Print:</TH>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel">Max Print Attempts</TD>
					<TD class="Panel"><input id="printmaxattempts" size="8"></TD>
					<TD class="Panel"><DIV onClick="OnExpPrintAttempts()" style="COLOR:blue"><U>Explain</U></DIV>
					</TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel">Sleeptime in milliseconds</TD>
					<TD class="Panel"><input id="printsleeptime" size="8"></TD>
					<TD class="Panel"><DIV onClick="OnExpPrintSleeptime()" style="COLOR:blue"><U>Explain</U></DIV>
					</TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel">Timeout in seconds</TD>
					<TD class="Panel"><input id="printtimeout" size="8"></TD>
					<TD class="Panel"><DIV onClick="OnExpPrintTimeout()" style="COLOR:blue"><U>Explain</U></DIV>
					</TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel">Acrobat Startup Time in milliseconds</TD>
					<TD class="Panel"><input id="printacrostartuptime" size="8"></TD>
					<TD class="Panel"><DIV onClick="OnExpAcroStartupTime()" style="COLOR:blue"><U>Explain</U></DIV>
					</TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
				</TR>
				<TR>
					<TD colspan="4" align="left"><H3>ML Printer</H3>
					</TD>
				</TR>
				<TR>
					<TD class="Panel" align="right"><INPUT type="radio" id="usevirtualprinter" name="usevirtualprinter" onClick="OnRadioChange()"></TD>
					<TD class="Panel">Always use ML Printer</TD>
					<TD class="Panel"><DIV onClick="OnExpUseVirtual()" style="COLOR:blue"><U>Explain</U></DIV>
					</TD>
				<TR>
				<TR>
					<TD class="Panel" align="right"><INPUT type="radio" id="bypassvirtualprinter" name="bypassvirtualprinter" onClick="OnRadioChange()"></TD>
					<TD class="Panel">Bypass when possible</TD>
					<TD class="Panel"><DIV onClick="OnExpBypassVirtual()" style="COLOR:blue"><U>Explain</U></DIV>
					</TD>
				<TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel"><input id="description1" contentEditable="false" style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none" class="Panel" size="35"></input></TD>
					<TD class="Panel"><DIV id="box1"></DIV>
					</TD>
					<TD class="Panel"><DIV id="expinputbox1" onClick="OnExp1()" style="COLOR:blue"></DIV>
					</TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel"><input id="description2" contentEditable="false" style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none" class="Panel" size="35"></input></TD>
					<TD class="Panel"><input id="inputbox2" border="0" size="8"></TD>
					<TD class="Panel"><DIV id="expinputbox2" onClick="OnExp2()" style="COLOR:blue"></DIV>
					</TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel"><input id="description3" contentEditable="false" style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none" class="Panel" size="35"></input></TD>
					<TD class="Panel"><input id="inputbox3" border="0" size="8"></TD>
					<TD class="Panel"><DIV id="expinputbox3" onClick="OnExp3()" style="COLOR:blue"></DIV>
					</TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel"><input id="description4" contentEditable="false" style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none" class="Panel" size="35"></input></TD>
					<TD class="Panel"><input id="inputbox4" border="0" size="8"></TD>
					<TD class="Panel"><DIV id="expinputbox4" onClick="OnExp4()" style="COLOR:blue"></DIV>
					</TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel"><input id="description5" contentEditable="false" style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none" class="Panel" size="35"></input></TD>
					<TD class="Panel"><input id="inputbox5" border="0" size="8"></TD>
					<TD class="Panel"><DIV id="expinputbox5" onClick="OnExp5()" style="COLOR:blue"></DIV>
					</TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
				</TR>
				<TR>
					<TD colspan="4" align="left"><H3>Logging</H3>
					</TD>
				</TR>
				<TR>
					<TD class="Panel" align="right">Log Filename:</TD>
					<TD class="Panel"><input id="logfile" contentEditable="false" border="0" style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none" class="Panel"></TD>
					<TD class="Panel"><DIV onClick="OnExpLogFilename()" style="COLOR:blue"><U>Explain</U></DIV>
					</TD>
				</TR>
				<TR>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
					<TD class="Panel"></TD>
				</TR>
				<TR>
					<TD class="Panel" align="right"><INPUT type="checkbox" id="dologging" name="dologging" onClick="OnLogChange()"></TD>
					<TD class="Panel">Extensive logging</TD>
					<TD class="Panel"><DIV onClick="OnExpDoLog()" style="COLOR:blue"><U>Explain</U></DIV>
					</TD>
				<TR>
				<TR>
					<TD class="Panel" align="right"><INPUT type="checkbox" id="permanentlog" name="permanentlog" onClick="OnPermChange()"></TD>
					<TD class="Panel">Always perform logging</TD>
					<TD class="Panel"><DIV onClick="OnExpDoPerm()" style="COLOR:blue"><U>Explain</U></DIV>
					</TD>
				<TR>
				</TR>
			</TABLE>
			<p>&nbsp;</p>
			<table border="0" cellspacing="0" cellpadding="2" width="100%" class="Panel">
				<tr>
					<td align="right">
						Magic word: <input type="password" id="PwdField" size="10"> <input type="button" onclick="OnRestore();" value="Restore Defaults" id="button3" name="button3">
						<input type="button" onclick="OnOK();" value="   OK   " id="button1" name="button1">
						<input type="button" onclick="onClosePopup();" value="Cancel" id="button2" name="button2">
					</td>
				</tr>
			</table>
		</form>
	</BODY>
</HTML>
