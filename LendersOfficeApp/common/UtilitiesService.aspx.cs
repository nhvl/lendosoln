namespace LendersOffice.Common
{
    using System;
    using LendersOffice.Security;

    public partial class UtilitiesService : AbstractUtilitiesService 
    {
        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }
        protected override Guid BrokerID 
        {
            get { return BrokerUser.BrokerId; }
        }
        protected override bool HasPermission(Permission p) 
        {
            return BrokerUser.HasPermission(p); 
        }
    }
}
