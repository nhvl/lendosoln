using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace LendersOfficeApp.common.ModalDlg
{

	/// <summary>
	///		Summary description for cModalDlg.
	/// </summary>
    public partial  class cModalDlg : System.Web.UI.UserControl
    {
        
        #region CModalDlg javascript functions
        private static string s_javascript = @"
<script language=""javascript"" type=""text/javascript"">
<!--
var VRoot = '<%=VRoot%>';
-->
</script>
<script type=""text/javascript"" src=""<%=VRoot%>/common/ModalDlg/CModalDlg.js""></script>
".Replace("<%=VRoot%>", DataAccess.Tools.VRoot);
        #endregion
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            this.Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "cModalDlg", s_javascript);
		}


		/// <summary>
		/// Close a dialog box opened with the cModalDlg class.
		/// Allows you to pass simple return values back to the parent window.
		/// </summary>
		/// <param name="oPage">The page using this method.</param>
		/// <param name="rgArgumentAssignments">
		/// An array of return arguments to return to the client. If you have no
		/// arguments, just pass in NULL.
		/// 
		/// How this work:
		///		
		///	if you want to return to the parent b = 5, c = 6, then you would write:
		///		rgArgumentAssignments[0] = "b = 5" ;
		///		rgArgumentAssignments[1] = "c = 6" ;
		///		
		///	The CloseDialog function will generate client-side javascript of:
		///	
		///		var arg = {};
		///		arg.b = 5 ;
		///		arg.c = 6 ;
		///		onClosePopup(args) ;
		///		
		///	Warning: If you want to do funky stuff with the arg object, then you
		///	are responsible for any javascript errors that occurs.
		/// </param>
		public static void CloseDialog(Page oPage, string[] rgArgumentAssignments)
		{
			HttpResponse oResponse = oPage.Response ;
			oResponse.Clear() ;
			oResponse.Write($"<HTML><script src='{oPage.ResolveUrl("~/inc/jquery-1.12.4.min.js")}' ></script>") ;
			oResponse.Write($"<script src='{oPage.ResolveUrl("~/inc/LQBPopup.js")}' ></script>") ;
			oResponse.Write($"<script src='{oPage.ResolveUrl("~/common/ModalDlg/CModalDlg.js")}' ></script><script language=javascript>") ;
			oResponse.Write("var args = {};") ;
			if (null != rgArgumentAssignments)
			{
				foreach(string sAssignment in rgArgumentAssignments)
					oResponse.Write("args." + sAssignment + ";") ;
			}

			oResponse.Write("onClosePopup(args);") ;
			oResponse.Write("</script></HTML>") ;
			oResponse.End() ;
		}
		public static void CloseDialog(Page oPage)
		{
			CloseDialog(oPage, null) ;
		}
		public static void CloseDialog(Page oPage, string sRetKey, string sRetValue)
		{
			HttpResponse oResponse = oPage.Response ;
			oResponse.Clear() ;
			oResponse.Write($"<HTML><script src='{oPage.ResolveUrl("~/inc/jquery-1.12.4.min.js")}' ></script>") ;
			oResponse.Write($"<script src='{oPage.ResolveUrl("~/inc/LQBPopup.js")}' ></script>") ;
			oResponse.Write($"<script src='{oPage.ResolveUrl("~/common/ModalDlg/CModalDlg.js")}' ></script><script language=javascript>") ;
			oResponse.Write("var arg = {};") ;
			oResponse.Write("arg." + sRetKey + "=" + sRetValue + ";") ;
			oResponse.Write("onClosePopup(arg)") ;
			oResponse.Write("</script></HTML>") ;
			oResponse.End() ;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
