<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="ConfirmPopupPage.aspx.cs" Inherits="LendersOfficeApp.Common.ModalDlg.ConfirmPopupPage" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<HTML>
  <head runat="server">
  </head>
  <style>
    .div-center{
      text-align: center;
    }
  </style>
  <BODY>
      <form id="confirmForm" method="post" runat="server">
        <div>
          <div>
            <p class="div-center" id="message"></p>
          </div>
          <div class="div-center">
              <button onclick="confirm(1)">Yes</button>
              <button onclick="confirm(0)">No</button>
              <button id="CancelButton" onclick="confirm(-1)">Cancel</button>
          </div>
        </div>
      </form>
    
    <script language="javascript" type="text/javascript">
      $(function() { 
          var args = getModalArgs();
          if (args != null){
              if (args.confirmMessage){
                $("#message").text(args.confirmMessage);
              }

              $("#CancelButton").toggle(args.showCancel == null || args.showCancel);
          }
       });

       function confirm(value){
        $("#confirmForm button").prop('disabled', true);
         onClosePopup(value);
       }
    </script>
  </BODY>
</HTML>
