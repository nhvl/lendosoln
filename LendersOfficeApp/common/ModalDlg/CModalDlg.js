// VRoot is declare globally in cModalDlg.ascx
function cModalDlg()
{
  this.Open = cModalDlg_Open;
  this.VRoot = VRoot;
  this.ForceEdge = false;
  return this;
}

function cModalDlg_Open(sURL, oParam, sFeatures) {
    var framingPage = this.ForceEdge ? 'DialogFrameEdge.aspx' : 'DialogFrame.aspx';

    if (!isFullUrl(sURL)) {
        sURL = VRoot + '/common/ModalDlg/' + framingPage + '?url=' + escape(sURL)
    }

    return window.showModalDialog(sURL, oParam, sFeatures);
}

function cGenericArgumentObject() { return this; }

function _resizeViewPort(viewWindow, width, height) {
    var verScrollPadding = 30;
    width = width + verScrollPadding;
    if (viewWindow.outerWidth) {
        viewWindow.resizeTo(
            width + (viewWindow.outerWidth - viewWindow.innerWidth),
            height + (viewWindow.outerHeight - viewWindow.innerHeight)
        );
    } else {
        viewWindow.resizeTo(width, height);
        viewWindow.resizeTo(
            width + (width - document.body.offsetWidth),
            height + (height - document.body.offsetHeight)
        );
    }
}

function resize(w, h) {
    window.hasBeenResized = true;
    w = w < screen.width ? w : screen.width - 10;
    h = h < screen.height ? h : screen.height - 60;

    if (isLqbPopup(window) && parent.LQBPopup.GetCurrentlyDisplayedFilename()) {
        parent.LQBPopup.Resize(w, h);
        return;
    }

    if (window.frameElement == null) {
        if (self.dialogWidth) {
            var l = (screen.width - w) / 2;
            var t = (screen.height - h - 50) / 2;
            self.dialogWidth = w + 'px';
            self.dialogHeight = h + 'px';
            self.dialogLeft = l + 'px';
            self.dialogTop = t + 'px';
            return;
        }
        else {
            _resizeViewPort(window, w, h);
            return;
        }
    }

    try {
        if (window.top.modelessArgs != null) {
            _resizeViewPort(window.top, w, h);
            return;
        }
    }
    catch (e) {

    }

    //move showModalDialog check to bottom to get rid of resize issue in IE 11.
    // Case 464200.
    if (window.showModalDialog) {
        var l = (screen.width - w) / 2;
        var t = (screen.height - h - 50) / 2;
        self.dialogWidth = w + 'px';
        self.dialogHeight = h + 'px';
        self.dialogLeft = l + 'px';
        self.dialogTop = t + 'px';
        return;
    }
}

function resizeForIE6And7(w, h)
{
    if (window.showModalDialog && !isLqbPopup(window)) {
        resize(w, h);
    }
    else {
        resize(w, h);
    }
}

function showModalTopLevel(href, a, feature, forceEdge, callback, lqbPopupSettings) {
    var top = getHighestParentSameDomain();
    if ((!window.showModalDialog || getBrowserVersion() >= 11) && LQBPopup && !top.LQBPopup) {
        alert("LQBPopup is not define in the top level frame.");
    }

    top.showModal(href, a, feature, forceEdge, callback, lqbPopupSettings)
}

function isFullUrl(url) {
    return url != null && (url.indexOf('http://') != -1 || url.indexOf('https://') != -1 || url.indexOf('www.') != -1);
}

function showModal(href, a, feature, forceEdge, callback, lqbPopupSettings) {
    if (!isFullUrl(href)) {
        href = VRoot + href;
    }

    if (window.showModalDialog && getBrowserVersion() < 11)
    {
        var dlg = new cModalDlg();
        if (forceEdge == true) {
            dlg.ForceEdge = true;
        }

        var arg = new cGenericArgumentObject();
        arg.OK = false;

        if (a != null)
            arg = a;

        var sFeature = feature || 'dialogWidth:200px;dialogHeight:200px;center:yes;resizable:yes;scroll:yes;status=yes;help=no;';
        dlg.Open(href, arg, sFeature);
        if (arg.DuplicateLogin != null)
            self.location = VRoot + '/common/AppError.aspx';
        if (typeof callback === "function") {
            var returnArgs = [arg];
            callback.apply( (lqbPopupSettings && lqbPopupSettings.context) || this, returnArgs);
        }

        return arg;
    }
    else if (LQBPopup) {
        var settings = new Object(); // We avoid using json literal because the some pages use IE5 recently.
        settings.onReturn = callback;
        if (lqbPopupSettings) {
            settings = jQuery.extend({}, lqbPopupSettings, settings);
        }
        var featureExpression = /(dialog)?(\w+)\s*:\s*([\d\w]+)(px)*/g;
        feature = feature || "";
        var match = featureExpression.exec(feature);
        while (match != null) {
            var featureName = match[2].charAt(0).toLowerCase() + match[2].slice(1);
            if (settings[featureName] == null)
                settings[featureName] = match[3].replace("px", "");
            var match = featureExpression.exec(feature);
        }

        setTimeout(function () {
            LQBPopup.Show(href, settings, a);
        }, 0);
    }
}


var isProgressWindowShow = false;
var progressWindowInstance = null;
function displayProgressWindow(control, isValidate) {
  if (isValidate == null || isValidate) {
    if (typeof(Page_ClientValidate) == 'function' && !Page_ClientValidate()) return false;
  }
  if (isProgressWindowShow) return false;
  if (control == null || !document.getElementById(control).disabled) {
    isProgressWindowShow = true;
    showModeless(VRoot + '/common/processing.htm', 'status:false;dialogWidth:200px;dialogHeight:50px');
    return true;
  }
  return false;
}

function transferArguments(destinationObject, sourceObject) {
    if (destinationObject && sourceObject) {
        for (var key in sourceObject) {
            destinationObject[key] = sourceObject[key];
        }
    }
}

function onClosePopup(args) {
    if (args == null) args = {};
    if (isJqueryUIDialog(window) && !isLqbPopup(window)) {
        parent.$(".ui-dialog-content").dialog("close");
        return;
    }

    if (isLqbPopup(window)){
        parent.LQBPopup.Return(args);
        return;
    }

    var isIeModalDialog = (window.dialogArguments != null);
    if (isIeModalDialog) {
        transferArguments(window.dialogArguments, args);
        self.close();
        return;
    } 

    var isNonIframeWindow = (window.frameElement == null);
    if (isNonIframeWindow) {
        self.close();
        return;       
    }
    
    var isModelessWindow = (window.top.modelessArgs != null);
    if (isModelessWindow){
        top.window.close();
    }
}

function cModalDlg_CloseDlg(retkey, retval)
{
	var arg = window.dialogArguments || {};
	eval("arg." + retkey + "='" + retval + "';") ;
	self.close() ;
}

function getModalArgs(){
    if (window.dialogArguments){
        return window.dialogArguments;
    }
    
    if (isLqbPopup(window)){
        return  parent.LQBPopup.GetArguments();
    }

    return  window.top.modelessArgs;
}

function isJqueryUIDialog(window){
    return (
        window.frameElement != null &&
        jQuery(window.frameElement).closest(".ui-dialog").length > 0
    );
}
