<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<%@ Import Namespace="LendersOffice.AntiXss" %>
<HTML>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=6" />
    <title>LendingQB</title>
    <style>
        .body-iframe {
            overflow: hidden;
            margin: 0;
        }
    </style>
  </head>
  <BODY scroll=no height='100%' class="body-iframe">
    <IFRAME width='100%' height='100%' NAME="ModalFrame" SRC=<%= AspxTools.SafeUrlFromQueryString("url")%>></IFRAME>
  </BODY>
</HTML>
