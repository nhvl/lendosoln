﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.AntiXss;
using System.Text.RegularExpressions;

namespace LendersOfficeApp.common
{
    public partial class RequestError : LendersOffice.Common.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterJsScript("loanframework.js");
            RegisterJsScript("loanframework2.js");

            try
            {
                string url = Request.QueryString["url"];
                string match = "^https?://" + Request.Url.Host;
                url = Server.UrlDecode(url);
                hField.Value = Regex.IsMatch(url, match, RegexOptions.IgnoreCase | RegexOptions.Singleline) ? AspxTools.HtmlString(url) : "";
            }
            catch { hField.Value = ""; }
        }
    }
}
