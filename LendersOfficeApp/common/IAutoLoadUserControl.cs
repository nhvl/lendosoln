using System;
using DataAccess;

namespace LendersOffice.Common
{
	/// <summary>
	/// Summary description for IAutoLoadUserControl.
	/// </summary>
	public interface IAutoLoadUserControl
	{
        void LoadData();
        void SaveData();
	}
	public class IAutoLoadUserControlSaveDataException : CBaseException
	{
		public IAutoLoadUserControlSaveDataException(string sReason) : base(ErrorMessages.Generic, sReason)
		{
		}
	}
}
