using System;
using System.Web.UI;
using DataAccess;
using LendersOffice.Common;
using LendersOfficeApp.los.admin;
using LendersOffice.Security;

namespace LendersOfficeApp.common
{

	public partial class AppError : LendersOffice.Common.BasePage
	{
        protected const string DISPLAY_KEY = "_DA115D9487E84e1e8E5C424299F5B844";

        protected bool m_isDuplicateLoginException = false;
		private bool m_ExceptionMissing = false; // 01-31-08 av 

        protected string m_errorReferenceNumber = string.Empty;

        protected string m_errorMessage = string.Empty;

        private static string[] PagesThatMayRedirectToPipeline = { Tools.VRoot + "/los/pipeline.aspx", Tools.VRoot + "/los/main.aspx", Tools.VRoot + "/los/loanfind.aspx" };

        protected bool possiblePipelineLoadError = false;

        /// <summary>
        /// 01-30-08 av opm 19618 Retrieve Error From Cache  All the time not just on first load.
        /// </summary>
        /// <returns></returns>
        private ExceptionInfo GetErrorFromCache() 
		{
			ExceptionInfo eI;

            string errorId = RequestHelper.GetSafeQueryString("id");

			if ( string.IsNullOrEmpty(errorId) )
			{	
				if ( RequestHelper.GetSafeQueryString("errmsg") != null && RequestHelper.GetSafeQueryString("errmsg") != String.Empty )
				{
					eI = new ExceptionInfo(new CBaseException( RequestHelper.GetSafeQueryString("errmsg"), "Exception's origin is from the querystring of AppError.")); 
				}
				else 
				{	
					eI = new ExceptionInfo(new CBaseException( ErrorMessages.Generic, "No errmsg or exception id existed in the querystring of AppError." ));
				}
				m_ExceptionMissing = true;
			}
            else if (errorId == "AJAX")
            {
                // 8/12/2009 dd - Error from our background service.
                string msg = RequestHelper.GetSafeQueryString("msg");
                eI = new ExceptionInfo(new CBaseException(ErrorMessages.Generic, msg));

                Tools.LogErrorWithCriticalTracking(eI.ErrorReferenceNumber + "::" + msg);

                m_ExceptionMissing = true;
            }
            else if (errorId == "DUP_LOGIN")
            {
                eI = new ExceptionInfo(new LendersOffice.Security.DuplicationLoginException(""));
                m_ExceptionMissing = true;
            }
            else
            {
                // Retrieve exception from cache.
                // 05/30/06 mf - As per OPM 2582, we no longer use server cache.
                // Instead of caching the entire Exception object, we only cache
                // the data this page needs in a ExceptionInfo object.


                eI = ErrorUtilities.RetrieveExceptionFromCache(errorId);

                if (eI == null)
                {
                    // We were unable to get the exception object from cache.
                    Tools.LogError("Unable to load Exception from cache in AppError.aspx");
                    eI = new ExceptionInfo(new CBaseException(ErrorMessages.Generic, "Failed to retrieve Exception from cache.  System clock may help determine thrower."));
                    m_ExceptionMissing = true;
                }
            }
			return eI; 
		
		}
		private void DisplayErrorMessage() 
		{			

			ExceptionInfo eI = GetErrorFromCache();

			m_isDuplicateLoginException = ( eI.ExceptionType == "LendersOffice.Security.DuplicationLoginException" );

            m_errorReferenceNumber = eI.ErrorReferenceNumber;

			m_errorMessage = eI.UserMessage;

			//OPM 56322 - DuplicationLoginException uses html in UserMessage; render as special case
            if (m_isDuplicateLoginException)
            {
                if (!IsPostBack)
                {
                    RequestHelper.ClearAuthenticationCookies();
                }
                m_errorMessage = "You have been logged out because you logged in on a different computer.";
                m_errorMessagePanel.Visible = true;
                if (!"You have been logged out because you logged in on a different computer.<br><br>".Equals(eI.UserMessage))
                {
                    string loginUrl = LendersOffice.Constants.ConstSite.LoginUrl.Replace("~/", Tools.VRoot + "/");
                    m_loginUrl.HRef = loginUrl;
                    m_loginUrl.Visible = true;
                }
            }
		}
        
        protected void PageLoad(object sender, System.EventArgs e)
        {
            m_isDuplicateLoginException = RequestHelper.GetSafeQueryString("id") == "DUP_LOGIN";
            if (RequestHelper.GetSafeQueryString(DISPLAY_KEY) == "t")
            {
                if (RequestHelper.GetBool(nameof(possiblePipelineLoadError)))
                {
                    BrokerUserPrincipal principal = PrincipalFactory.CurrentPrincipal as BrokerUserPrincipal;
                    if (principal != null && principal.BrokerId != null && principal.BrokerId != Guid.Empty)
                    {
                        loanFindUrl.HRef = Tools.VRoot + "/los/loanfind.aspx";
                        loanFindUrl.Visible = true;
                    }
                }

                DisplayErrorMessage();
            }
            if (!Page.IsPostBack && Request.UrlReferrer != null)
            {
                ViewState["UrlReferrer"] = Request.UrlReferrer.ToString();

                foreach(string page in PagesThatMayRedirectToPipeline)
                {
                    if (Request.UrlReferrer.LocalPath.Equals(page, StringComparison.OrdinalIgnoreCase))
                    {
                        possiblePipelineLoadError = true;
                        loanFindUrl.HRef = Tools.VRoot + "/los/loanfind.aspx";
                        loanFindUrl.Visible = true;
                    }
                }
            }

            DisplayErrorMessage();
            
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion

		// 01-31-08 av 
		private void DeleteFromCache() 
		{	
			if ( ! this.m_ExceptionMissing ) 
			{
				ErrorUtilities.ExpireException( RequestHelper.GetSafeQueryString("id") );
			}

		}

		// 01-31-08 av 
		protected void m_btnClose_Click(object sender, System.EventArgs e)
		{
            this.AddInitScriptFunction("f_onCloseClick");
			if ( m_ExceptionMissing ) 
				DeleteFromCache(); 
		}
	}
}
