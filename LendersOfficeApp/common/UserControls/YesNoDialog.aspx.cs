﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LendersOfficeApp.common.UserControls
{
    public partial class YesNoDialog : LendersOffice.Common.BasePage
    {
        protected int m_width = 300;
        protected int m_height = 150;
        protected void Page_Load(object sender, EventArgs e)
        {
            m_Message.Text = (Request["msg"] != null) ? Request["msg"] : "Confirm?";
            try
            {
                if (Request["width"] != null)
                    m_width = int.Parse(Request["width"]);
                if (Request["height"] != null)
                    m_height = int.Parse(Request["height"]);
            }
            catch{}
        }

        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
    }
}
