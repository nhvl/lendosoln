﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="YesNoDialog.aspx.cs" Inherits="LendersOfficeApp.common.UserControls.YesNoDialog" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Confirm</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro" onload="init();">
	<script language="javascript">
	<!--
		function init() 
		{
			var width = <%= AspxTools.JsNumeric(m_width) %>;
			var height = <%= AspxTools.JsNumeric(m_height) %>;
			resize(width, height);
		}

		function f_choice(choice) 
		{
			var args = window.dialogArguments || {};
			args.choice = choice;
			onClosePopup(args);
		}
	//-->
		</script>
		<form id="ConfirmSendWelcomeEmail" method="post" runat="server">
		<table width="100%">
			<tr>
				<td align="center" style="PADDING-LEFT:20px; COLOR:black">
					<br /><ml:EncodedLabel id="m_Message" name="m_Message" runat="server"></ml:EncodedLabel>
				</td>
			</tr>
			<tr>
				<td align="center">
					<br><input type="button" value="Yes" class="buttonstyle" onclick="f_choice(0);" style="WIDTH: 50px">
					&nbsp;<input type="button" value="No" class="buttonstyle" onclick="f_choice(1);" style="WIDTH: 50px">
				</td>
			</tr>
		</table>
     </form>
	<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
  </body>
</html>
