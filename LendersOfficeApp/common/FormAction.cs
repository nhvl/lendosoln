using System;

namespace LendersOffice.Common
{
	/// <summary>
	/// Summary description for FormAction.
	/// </summary>
    public enum FormAction {Add, Edit};
}
