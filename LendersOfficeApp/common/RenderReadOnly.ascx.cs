using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;

namespace LendersOffice.Common
{

	/// <summary>
	///	Summary description for RenderReadOnly.
	/// </summary>

	public partial  class RenderReadOnly : System.Web.UI.UserControl
	{
		private ArrayList m_Skip = new ArrayList();

		public Control Skip
		{
			// Access member.

			set
			{
				if( m_Skip.Contains( value.ID ) == false )
				{
					m_Skip.Add( value.ID );
				}
			}
		}

		private Boolean m_Enabled = false;
		private Boolean m_SkipOff = false;

		public Boolean Enabled
		{
			// Access member.

			set
			{
				m_Enabled = value;
			}
			get
			{
				return m_Enabled;
			}
		}

		public Boolean SkipOff
		{
			// Access member.

			set
			{
				m_SkipOff = value;
			}
			get
			{
				return m_SkipOff;
			}
		}

		/// <summary>
		/// Restore our state from the last save operation prior
		/// to returning from posting back.
		/// </summary>

		protected override void LoadViewState( object savedState )
		{
			// Load our flags from the streamed out sequence.
			// First delagate to base to initialize.

			base.LoadViewState( savedState );

			try
			{
				// Restore our flags because we assume that the
				// page made this decision up front.

				m_Enabled = ( Boolean   ) ViewState[ "Enabled" ];
				m_SkipOff = ( Boolean   ) ViewState[ "SkipOff" ];
				m_Skip    = ( ArrayList ) ViewState[ "Skip"    ];
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( e );
			}
		}

		/// <summary>
		/// Saves user control state between postbacks so that last
		/// setting is preserved.
		/// </summary>

		protected override object SaveViewState()
		{
			// Commit our flags to the persisted sequence.

			try
			{
				// Commit our flags to the persisted sequence.

				ViewState.Add( "Enabled" , m_Enabled );
				ViewState.Add( "SkipOff" , m_SkipOff );
				ViewState.Add( "Skip"    , m_Skip    );
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( e );
			}

			// Delegate to the base implementation.

			return base.SaveViewState();
		}

		/// <summary>
		/// Render the entire page as readonly.  If you want to
		/// undo this whole-page change for a few controls, then
		/// turn them back on in your pre-render.
		/// </summary>

		protected void ControlPreRender( object sender , System.EventArgs a )
		{
			try
			{
				// Walk the parent's controls and bfs down to all the
				// children, making each editable control readonly.
				// If not enabled, then punt.

				ArrayList aQ = new ArrayList();

				if( m_Enabled == false )
				{
					return;
				}

				foreach( Control pageControl in Page.Controls )
				{
					aQ.Add( pageControl );
				}

				for( int i = 0 ; i < aQ.Count ; ++i )
				{
					Control pageControl = aQ[ i ] as Control;

					if( pageControl == null || m_SkipOff == false && m_Skip.Contains( pageControl.ID ) == true )
					{
						continue;
					}

					foreach( Control childControl in pageControl.Controls )
					{
						aQ.Add( childControl );
					}

					if( pageControl is WebControl == true )
					{
						WebControl wC = pageControl as WebControl;

						if( wC is TextBox == true )
						{
							TextBox tC = wC as TextBox;

							tC.ReadOnly = true;

							continue;
						}

						if( wC is Button == true || wC is CheckBox == true || wC is DropDownList == true )
						{
							wC.Enabled = false;

							continue;
						}
					}

					if( pageControl is HtmlControl == true )
					{
						HtmlControl hC = pageControl as HtmlControl;

						if( hC is HtmlInputButton == true || hC is HtmlInputText == true || hC is HtmlSelect )
						{
							hC.Disabled = true;
						}
					}
				}
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( e );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.PreRender += new System.EventHandler(this.ControlPreRender);
		}
		#endregion

	}

}
