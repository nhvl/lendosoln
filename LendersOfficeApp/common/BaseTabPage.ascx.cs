﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonProjectLib.Common.Lib;
using LendersOffice.Common;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Text.RegularExpressions;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.common
{
    public partial class BaseTabPage : System.Web.UI.UserControl
    {
        private List<Tuple<string, IAutoLoadUserControl>> m_controls = new List<Tuple<string, IAutoLoadUserControl>>();
        private Dictionary<string, string> m_urlParameters = new Dictionary<string,string>(); 
        public void RegisterControl(string tabName, IAutoLoadUserControl control)
        {
            m_controls.Add(Tuple.Create(tabName, control)); 
        }

        private bool m_IsInFrameseSet = true;
        public bool IsInFrameset
        {
            get
            {
                return m_IsInFrameseSet;
            }
            set
            {
                m_IsInFrameseSet = value;
            }
        }

        public int TabIndex
        {
            get
            {
                return RequestHelper.GetInt("pg", 0);
            }
        }

        public void AddToQueryString(string key, string value)
        {
            if (key.Equals("pg", StringComparison.OrdinalIgnoreCase))
            {
                throw new ArgumentException("key cannot be page");
            }
            m_urlParameters[key] = value;
        }

        protected string TabSwitchUrl
        {
            get;
            set;
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            BasePage bp = Page as BasePage;  //maybe should assume bp is never null
            for (int i = 0; i < m_controls.Count; i++)
            {
                bool isEnabled = i == TabIndex;
                Control currentControl = (Control)m_controls[i].Item2;
                currentControl.Visible = isEnabled;
                currentControl.EnableViewState = currentControl.EnableViewState & isEnabled;
                if (isEnabled)
                {
                    IAutoLoadUserControl c1 = currentControl as IAutoLoadUserControl;
                    if (c1 != null)
                    {
                        if (bp != null)
                        {
                            bp.RegisterJsGlobalVariables("_ClientID", currentControl.ClientID);
                        }
                        Page.ClientScript.RegisterHiddenField("_ClientID", currentControl.ClientID);
                        c1.LoadData();
                    }
                }
            }

            #region rebuild the url  this will be used for the tabs
            StringBuilder url = new StringBuilder(Request.Path);
            url.Append("?pg={0}");

            foreach (KeyValuePair<string, string> entries in m_urlParameters)
            {
                url.AppendFormat("&{0}={1}", entries.Key, entries.Value);
            }

            TabSwitchUrl = url.ToString();
  
            #endregion

            Tabs.DataSource = m_controls;
            Tabs.DataBind();

            string script = string.Format(@"
function switchApplicant() {{
  f_load('{0}');
}}
", TabSwitchUrl.Replace("{0}", TabIndex.ToString()));
            Page.ClientScript.RegisterClientScriptBlock(typeof(BaseTabPage), "SwitchApplicant", script, true);


        }

        protected void Tabs_OnRowBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }

            Tuple<string, IAutoLoadUserControl> control = (Tuple<string, IAutoLoadUserControl>)args.Item.DataItem;


            HtmlContainerControl listItem = (HtmlContainerControl)args.Item.FindControl("tabEntry");

            if (args.Item.ItemIndex == TabIndex)
            {
                listItem.Attributes.Add("class", "selected");
            }

            HtmlAnchor anchor = (HtmlAnchor)args.Item.FindControl("tabLink");
            anchor.InnerText = control.Item1;
            //todo set this in ajvascript on init if possible so user doesnt get load errors. 
            //also may not be able to use it everywhere =/
            if (IsInFrameset)
            {
                anchor.HRef = string.Format("javascript:f_load('{0}');", AspxTools.JsStringUnquoted(TabSwitchUrl.Replace("{0}", args.Item.ItemIndex.ToString())));
            }
            else
            {
                anchor.HRef = TabSwitchUrl.Replace("{0}", AspxTools.HtmlString(args.Item.ItemIndex.ToString()));
            }   
        }
    }
}