<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintView_Frame.aspx.cs" Inherits="LendersOfficeApp.common.PrintView_Frame" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" style="height: 100%;">
<head runat="server">
    <title>LENDINGQB</title>
    	<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
    	
    	<style type="text/css">

        body 
        {
            margin: 0;
            overflow: hidden;
        }
    	</style>
</head>

<script type="text/javascript">
    function showNormalCert() {

        <%if(  this.IsDisplayDetailCert ) {%>
        showModal(<%= AspxTools.SafeUrl(url.Substring(DataAccess.Tools.VRoot.Length) + "&detailCert=t")%>, null, 'dialogWidth:780px;dialogHeight:600px;center:yes;resizable:yes;scroll:yes;status=yes;help=no;');
        <%}%>
	}

</script>

<body style="height:100%;">
    <form id="form1" runat="server" style="height: 100%;">
        <iframe name="menu" src=<%= AspxTools.SafeUrl("PrintView_Menu.aspx?cmd=" + Request["menu_param"] + (IsDisplayDetailCert ? "&detailCert=t" : "" )) %> style="border-right: none, 0px, black; display :block; width: 100%; height:10%" ></iframe>
        <iframe name="bodyiframe" src=<%= AspxTools.SafeUrl(url)%> style="display:block; width: 100%; height:93%"></iframe>
    </form>
</body>
</html>
