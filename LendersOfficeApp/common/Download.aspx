<%@ Page language="c#" Codebehind="Download.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.common.Download" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Download</title>
		<LINK href="../css/stylesheet.css" type="text/css" rel="stylesheet">
		<style type="text/css">
		  .SoftwareHeader
		  {
		    padding: 2px 2px 2px 2px;
		    font: bold 14px arial;
		  }
      .SoftwareContainer{
        margin: 0px 3px;
      }
		  .SoftwareGroup
		  {
		    border: solid 2px white;
		    padding: 8px 0px;
		    margin-top: 4px;
		    font: 12px arial;
		    width: 100%;
		    background-color: whitesmoke;
		  }
		  .MessageBorder
		  {
		    border: solid 3px black;
		    padding: 5px 5px 5px 5px;
		    display: none;
		    width: 270px;
		    position: absolute;
		    height: 50px;
		    background-color: whitesmoke;
		  }
          .RequirementSection
          {
              font: bold 12px arial;
          }
		</style>
	</HEAD>
	<body style="DISPLAY: none; BACKGROUND-COLOR: gainsboro" onload="onInit();">
		<script language="javascript">

		<!--

		function onInit() { try
		{
			resize( 500 , 600 );
			
			document.body.style.display = "block";
			var operatingSystem = document.getElementById("operatingSystem");
			var kindOfProcessor = document.getElementById("kindOfProcessor");
			var internetBrowser = document.getElementById("internetBrowser");
			var cpuContainer = document.getElementById("CpuContainer");

			if (window.clientInformation) {
			    operatingSystem.innerText = window.clientInformation.platform;
			    internetBrowser.innerText = window.clientInformation.appVersion;
			    kindOfProcessor.innerText = window.clientInformation.cpuClass;
			    cpuContainer.className = window.clientInformation.cpuClass ? '' : 'Hidden';
			}
			else if (window.navigator) {
			    operatingSystem.innerText = window.navigator.platform;
			    internetBrowser.innerText = window.navigator.userAgent;
			    kindOfProcessor.innerText = window.navigator.oscpu;
			    cpuContainer.className = window.navigator.oscpu ? '' : 'Hidden';
			}
		}
		catch( e )
		{
			alert( window.status = e.message );
		}}


		function f_closeDataTracHelperText() {
		    document.getElementById("DataTracHelperText").style.display = "none";
		}

		function f_showDataTracHelperText(event) {
		    var msg = document.getElementById("DataTracHelperText");
		    msg.style.display = "";
		    msg.style.top = (event.clientY - 110) + "px";
		    msg.style.left = (event.clientX - 160) + "px";
		}


		//-->

        </script>
		<form id="Download" method="post" runat="server">
      <h4 class="page-header">Software Download Page</h4>
			<TABLE cellSpacing="1" cellPadding="2" width="100%" border="0">
        <TR>
					<TD class="FormTable">
                        <DIV class="SoftwareContainer">
              <div class="SoftwareGroup">
                <div class="SoftwareHeader">ePrintDotNet (v1.3)</div>
                <table cellspacing="0" cellpadding="2" border="0">
                  <tr>
                    <td>Install this for:</td>
                    <td>Printing forms in LendingQB</td>
                  </tr>
                  <tr>
                    <td valign="top">Requirements:</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td nowrap width="130px">-</td>
                    <td><a href=<%=AspxTools.SafeUrl(DownloadUrlEprintV4_Updated) %>>Download and install ePrintDotNet</a></td>
                  </tr>
                </table>
              </div>              

              <div class="SoftwareGroup" id="VPrinterGroup" runat="server">
                <div class="SoftwareHeader">LendingQB VPrinter (v3.1.4)</div>
                <table cellspacing="0" cellpadding="2" border="0">
                  <tr>
                    <td nowrap valign="top">Download this for:</td>
                    <td valign="top">Printing documents directly into the EDocs system</td>
                  </tr>
                  <tr class="RequirementSection">
                    <td valign="top">Requirements:</td>
                    <td valign="top">Download and install the latest 32 bit version of Ghostscript from <a href=https://www.ghostscript.com/download/gsdnld.html target="_blank">here</a></td>
                  </tr>                
                  <tr>
                    <td nowrap width="130px">-</td>
                    <td><a href=<%= AspxTools.SafeUrl(DownloadUrlVPrinter) %>>Download the VPrinter Installer</a></td>
                  </tr>
                </table>
              </div>
              <div class="SoftwareGroup">
                <div class="SoftwareHeader">Download Base Rates</div>
                <table cellspacing="0" cellpadding="2" border="0">
                    <tr>
                        <td nowrap="nowrap" valign="top">Download this for:</td>
                        <td valign="top">Downloading the base rates from each investor without built in adjustment</td>
                    </tr>
                    <tr>
                        <td nowrap width="130px">-</td>
                        <td><a href=<%= AspxTools.SafeUrl(DownloadBaseRates) %>>Download the Base Rate Application</a></td>
                    </tr>
                </table>
              </div>
                            <div class="SoftwareGroup">
                <div class="SoftwareHeader">Download Rates</div>
                <table cellspacing="0" cellpadding="2" border="0">
                    <tr>
                        <td nowrap="nowrap" valign="top">Download this for:</td>
                        <td valign="top">Downloading the best execution pricing results for each product type with built-in adjustment</td>
                    </tr>
                    <tr>
                        <td nowrap width="130px">-</td>
                        <td><a href=<%= AspxTools.SafeUrl(DownloadRates) %>>Download the Rate Application</a></td>
                    </tr>
                </table>
              </div>
              <div class="SoftwareGroup">
                <div class="SoftwareHeader">Application Environment</div>
                <table cellspacing="0" cellpadding="2" border="0">
                  <tr>
                    <td nowrap width="130px">Operating system </td>
                    <td id="operatingSystem"></td>
                  </tr>
                  <tr>
                    <td nowrap valign="top">Internet browser </td>
                    <td id="internetBrowser"></td>
                  </tr>
                  <tr id="CpuContainer">
                    <td nowrap>CPU </td>
                    <td id="kindOfProcessor"></td>
                  </tr>
                </table>
              </div>
      
              
						</div>
					</TD>
				</TR>
				<TR>
					<TD style="PADDING-RIGHT: 8px; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; PADDING-TOP: 8px" align="middle">
						<table cellpadding="0" cellspacing="0">
						<tr>
						<td align=center>
							<input onclick="onClosePopup();" type="button" value="Close">
						</td>
						</tr>
						</table>
					</TD>
				</TR>
			</TABLE>
    <div id="DataTracHelperText" class="MessageBorder">
      <table width="100%">
        <tr>
          <td>Please contact our support staff for information about the DataTrac integration.</td>
        </tr>
        <tr>
          <td align="center">[ <a href="#" onclick="f_closeDataTracHelperText();">Close</a> ]</td>
        </tr>
      </table>
    </div>
			<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg></form>
	</body>
</HTML>
