/// Author: David Dao

namespace LendersOffice.Common
{
    using System;
    using System.Web;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.HttpModule;

    /// <summary>
    /// This page will be use to download all executable in our system. For security reason, we do not allow user to pass in the file name.
    /// User must pass in correct key to download the file.
    /// </summary>
    public partial class AppDownload : MinimalPage
	{
        private const int SENDING_BUFFER_LENGTH = 4026;

		protected void PageLoad(object sender, System.EventArgs e)
		{
            PerformanceMonitorItem item = PerformanceMonitorItem.Current;

            if (null != item) 
            {
                item.IsEnabledPageSizeWarning = false;
                item.IsEnabledPageRenderTimeWarning = false;
            }
			string key = RequestHelper.GetSafeQueryString("key");

            switch (key) 
            {
                case ConstAppDavid.DownloadFile_EprintV4_Updated:
                    SendFile("ePrint.msi");
                    break;
                case ConstAppDavid.DownloadFile_VPrinter:
                    SendFile("PDFvPrinter.msi");
                    break;
                case ConstAppDavid.DownloadFile_VPrinterForCitrix:
                    SendFile("PDF VPrinter For Citrix.exe");
                    break;
                case ConstAppDavid.DownloadFile_Rates:
                    SendFile("downloadrates.zip");
                    break;
                case ConstAppDavid.DownloadFile_BaseRates:
                    SendFile("downloadbaserates.zip");
                    break;
                case ConstAppDavid.DownloadFile_BrowserXtInfo:
                    SendFile("BrowserXT Info.pdf");
                    break;
                case ConstAppDavid.DownloadFile_BrowserXtUninstallDirections:
                    SendFile("Uninstalling BrowserXT.pdf");
                    break;
                case ConstAppDavid.DownloadFile_BrowserXtUninstaller:
                    SendFile("BrowserXTUninstaller.zip");
                    break;
            }
		}

        private void SendFile(string fileName) 
        {
            try
            {
                if (ConstStage.UseBufferingWhenSendingAppDownloadFiles)
                {
                    SendFileWithBuffer(fileName);
                }
                else
                {
                    SendFileWithoutBuffer(fileName);
                }
            }
            catch (Exception e)
            {
                Tools.LogError("Sending file " + fileName + " with " + 
                    (ConstStage.UseBufferingWhenSendingAppDownloadFiles ? String.Empty : "out " )
                    + "buffering failed.", e);
                throw; // preserving existing.  have case 182341 to look into fixing this.
            }
        }

        private void SendFileWithoutBuffer(string fileName)
        {
            string fullPath = Tools.GetServerMapPath(ConstAppDavid.DownloadFile_Path + fileName);
            if (FileOperationHelper.Exists(fullPath))
            {
                Response.Clear();
                Response.ContentType = "application/download";
                Response.AddHeader("Content-Disposition", $"attachment; filename=\"{fileName}\"");
                Response.WriteFile(fullPath);
                Response.Flush();
                Response.End();
            }
            else
            {
                Tools.LogWarning("Unable to locate file::" + fileName);
            }
        }
        private void SendFileWithBuffer(string fileName)
        {
            string fullPath = Tools.GetServerMapPath(ConstAppDavid.DownloadFile_Path + fileName);
            if (FileOperationHelper.Exists(fullPath))
            {
                Response.Clear();
                Response.ContentType = "application/download";
                Response.AddHeader("Content-Disposition", $"attachment; filename=\"{fileName}\"");

                Tools.WriteFileToResponseWithBuffer(Response, fullPath);
                
                //Response.End(); // this causes threadabortexceptions.  not sure about iislogging.
                // ^ see http://stackoverflow.com/questions/1087777/is-response-end-considered-harmful
                Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Tools.LogWarning("Unable to locate file::" + fileName);
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
