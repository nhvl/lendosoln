using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Xml;
using DataAccess;
using System.Collections.Generic;
using System.Linq;
using LendersOffice.ObjLib.DynaTree;
using LendersOffice.ObjLib.CustomFavoriteFolder;

namespace LendersOffice.Common.UI
{

    public delegate void SpecialTypeNodeHandler(TreeNode node, string code, Tree tree);
    public delegate void NodeCreateHandler(TreeNode node);
    /// <summary>
    /// 3/2/2004 dd
    /// Due to instability of Microsof TreeView control, I need some control that will allow me construct tree from XML file.
    /// My other requirement is tree must have elegant looks. 
    /// Unhappy with http://www.dynamicdrive.com/dynamicindex1/navigate1.htm, I decide to create a simple Tree control that I can
    /// use.
    /// 
    /// Warning: This tree is very basic because I have no other need for any advance features. 
    /// 
    /// To edit style of the folder node. Define .NodeFolder in the stylesheet.
    /// </summary>
    public class Tree
    {
        private ArrayList m_childs;
        private TreeNode m_previousNode;
        private TreeFolder m_FavoritesFolder;
        public event SpecialTypeNodeHandler SpecialTypeNode;
        public event NodeCreateHandler OnNodeCreate;
        private bool IsUsingFavorites = false;

        private string m_menuTreeStateXmlContent = "";

        /// <summary>
        /// Store customize collapse state of menu. Set this before invoke Load(xml)
        /// </summary>
        public string MenuTreeStateXmlContent 
        {
            get { return m_menuTreeStateXmlContent; }
            set { m_menuTreeStateXmlContent = value; }
        }

        private int m_menuTreeStateWidth = 0;

        public int MenuTreeStateWidth
        {
            get{ return m_menuTreeStateWidth; }
            set{ m_menuTreeStateWidth = value; }
        }

        public Tree()
        {

        }

        public Tree(BasePage p)
        {
            if (null == p) throw new GenericUserErrorMessageException("In order for this tree to work, it needs to belong to LendersOffice.Common.BasePage");

            // 3/2/2004 dd - The tree control depends on tree.js. The only way I could enforce the aspx to include tree.js is
            // required tree control to belong to LendersOffice.Common.BasePage
            p.RegisterJsScript("tree.js");

            m_childs = new ArrayList();
        }

        public void AddChild(TreeNode child) 
        {
            child.PreviousNode = m_previousNode;

            m_childs.Add(child);

            if (null != m_previousNode)
                m_previousNode.NextNode = child;

            m_previousNode = child;
        }


        private Hashtable m_folderSettingsHash = new Hashtable();

        public void DetermineFolderCollapsed(DynaTreeNode node)
        {
            var path = node.customAttributes["path"].ToLower();
            if (m_folderSettingsHash.Contains(path))
            {
                node.isCollapsedAfterRender = (bool)m_folderSettingsHash[path];
            }
        }

        private void LoadSaveFoldersSetting(string xml) 
        {
            if (null == xml || "" == xml)
                return;

            XmlDocument doc = DataAccess.Tools.CreateXmlDoc(xml);

            XmlNode root = doc.DocumentElement;
            if (root.Attributes["width"] != null)
                int.TryParse(root.Attributes["width"].Value, out m_menuTreeStateWidth);

            foreach (XmlNode el in root.ChildNodes) 
            {
                if (el.NodeType == XmlNodeType.Element) 
                {
                    switch (el.Name.ToLower()) 
                    {
                        case "folder":
                            string name = ((XmlElement) el).GetAttribute("path").ToLower();
                            bool collapse = ((XmlElement) el).GetAttribute("collapse").ToLower() == "true";
                            m_folderSettingsHash.Add(name, collapse);
                            break;

                    }
                }
            }

        }

        private void DetermineFavoriteVisibility(CustomFavoriteNode favoriteNode, Dictionary<string, TreeNode> treeDictionary)
        {
            if(favoriteNode is CustomFavoritePageNode)
            {
                var pageNode = favoriteNode as CustomFavoritePageNode;
                TreeNode node;
                if(treeDictionary.TryGetValue(pageNode.PageId, out node))
                {
                    pageNode.IsVisible = node.IsVisible;
                    pageNode.LinkText = node.Name;
                }
            }
            else
            {
                var folderNode = favoriteNode as CustomFavoriteFolderNode;
                foreach (CustomFavoriteNode childNode in folderNode.Children)
                {
                    DetermineFavoriteVisibility(childNode, treeDictionary);
                }
            }
        }

        /// <summary>
        /// Construct tree from XML and JSON
        /// </summary>
        /// <param name="xmlFile"></param>
        public void Load(string xmlFile) 
        {
            LoadSaveFoldersSetting(m_menuTreeStateXmlContent);

            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);

            XmlNode root = doc.DocumentElement;

            // We need to get the favorites folder first, or else any elements that appear before it will not be added to it since it doesn't exist yet.
            XmlNode favoritesRoot = root.SelectSingleNode("folder[@id='Folder_Favorites']");
            m_FavoritesFolder = CreateFolderItem((XmlElement)favoritesRoot);

            foreach (XmlNode el in root.ChildNodes) 
            {
                if (el.NodeType == XmlNodeType.Element) 
                {
                    switch (el.Name.ToLower()) 
                    {
                        case "folder":
                            AddFolder((XmlElement) el, null);
                            break;
                        case "item":
                            AddItem((XmlElement) el, null);
                            break;
                    }
                }
            }

        }

        public TreeFolder CreateFolderItem(XmlElement el)
        {
            string name = el.GetAttribute("name");
            TreeFolder item = new TreeFolder(name);
            item.Attributes["id"] = el.GetAttribute("id");

            return item;
        }

        private void AddFolder(XmlElement el, TreeFolder parent) 
        {
            TreeFolder item = CreateFolderItem(el);

            if (IsUsingFavorites || item.ID != "Folder_Favorites")
            {
                if (item.ID == "Folder_Favorites")
                {
                    item = m_FavoritesFolder;
                }

                if (el.GetAttribute("collapse") != "")
                    item.IsCollapsed = true;

                if (null == parent)
                    AddChild(item);
                else
                    parent.AddChild(item);

                foreach (XmlNode o in el.ChildNodes)
                {
                    if (o.NodeType == XmlNodeType.Element)
                    {
                        switch (o.Name.ToLower())
                        {
                            case "folder":
                                AddFolder((XmlElement)o, item);
                                break;
                            case "item":
                                AddItem((XmlElement)o, item);
                                break;
                        }
                    }
                }

                if (null != OnNodeCreate)
                    OnNodeCreate(item);

                if (el.GetAttribute("special") != "")
                    OnSpecialNode(item, el.GetAttribute("special"), this);

                // IsCollapsed is to be determined after the create handlers since OnNodeCreate
                // and OnSpecialNode may change the folder's path name.
                if (m_folderSettingsHash.Contains(item.FullPathName.ToLower()))
                {
                    item.IsCollapsed = (bool)m_folderSettingsHash[item.FullPathName.ToLower()];
                }
            }
        }

        private void AddItem(XmlElement el, TreeFolder parent) 
        {
            string name = el.GetAttribute("name");
            TreeNode item = new TreeNode(name);
            item.Attributes["class"] = "TreeNodeLink"; // 4/19/2006 dd - Default style for node item.

            foreach (XmlAttribute attr in el.Attributes) 
            {
                if (attr.Name != "name" && attr.Name != "special")
                    item.Attributes[attr.Name] = attr.Value;
            }
            if (null == parent)
            {
                AddChild(item);
            }
            else
            {
                if (m_FavoritesFolder != null && parent == m_FavoritesFolder)
                {
                    //  Some items depend on their parents for their visibility logic.  To be able to safely determine an items logic, add the parent manually for
                    //  favorited items.
                    item.Parent = m_FavoritesFolder;
                    item.IsVisible = false;
                }
                else
                {
                    parent.AddChild(item);
                }
            }

            if (null != OnNodeCreate)
                OnNodeCreate(item);

            if (el.GetAttribute("special") != "")
                OnSpecialNode(item, el.GetAttribute("special"), this);
        }

        /// <summary>
        /// Generate HTML code for this tree to StringBuilder
        /// </summary>
        /// <param name="sb"></param>
        public DynaTreeNode Render(bool filterLandingPagesOnly) 
        {
            var tree = new DynaTreeNode();

            foreach (TreeNode o in m_childs)
            {
                o.IsUsingFavorites = this.IsUsingFavorites;
                var child = o.Render(filterLandingPagesOnly);
                if(child != null)
                {
                    tree.children.Add(child);
                }
            }

            return tree;
        }

        private void OnSpecialNode(TreeNode node, string code, Tree tree) 
        {
            if (SpecialTypeNode != null)
                SpecialTypeNode(node, code, tree);
        }
    }


    public class TreeConstants 
    {
        private static string s_img_p;
        private static string s_img_m;
        private static string s_img_f;
        private static string s_img_i;
        private static string s_img_l;
        private static string s_img_r;
        private static string s_img_t;
        private static string s_img_fp;
        private static string s_img_ip;
        private static string s_img_lp;
        private static string s_img_rp;
        private static string s_img_tp;
        private static string s_img_fm;
        private static string s_img_im;
        private static string s_img_lm;
        private static string s_img_rm;
        private static string s_img_tm;
        private static string s_img_folder;
        private static string s_img_folder_open;
        private static string s_img_empty;

        private static string s_imageRoot;

        static TreeConstants() 
        {
            s_imageRoot = DataAccess.Tools.VRoot + "/images/tree";
            Init();
        }
        private static void Init() 
        {
            string imageFormat = "<img src='{0}/{1}' align=absMiddle>";
            s_img_p = string.Format(imageFormat, s_imageRoot, "p.gif");
            s_img_m = string.Format(imageFormat, s_imageRoot, "m.gif");
            s_img_f = string.Format(imageFormat, s_imageRoot, "f.gif");
            s_img_i = string.Format(imageFormat, s_imageRoot, "i.gif");
            s_img_l = string.Format(imageFormat, s_imageRoot, "l.gif");
            s_img_r = string.Format(imageFormat, s_imageRoot, "r.gif");
            s_img_t = string.Format(imageFormat, s_imageRoot, "t.gif");
            s_img_fp = string.Format(imageFormat, s_imageRoot, "fp.gif");
            s_img_ip = string.Format(imageFormat, s_imageRoot, "ip.gif");
            s_img_lp = string.Format(imageFormat, s_imageRoot, "lp.gif");
            s_img_rp = string.Format(imageFormat, s_imageRoot, "rp.gif");
            s_img_tp = string.Format(imageFormat, s_imageRoot, "tp.gif");
            s_img_fm = string.Format(imageFormat, s_imageRoot, "fm.gif");
            s_img_im = string.Format(imageFormat, s_imageRoot, "im.gif");
            s_img_lm = string.Format(imageFormat, s_imageRoot, "lm.gif");
            s_img_rm = string.Format(imageFormat, s_imageRoot, "rm.gif");
            s_img_tm = string.Format(imageFormat, s_imageRoot, "tm.gif");
            s_img_folder = string.Format(imageFormat, s_imageRoot, "folder.gif");
            s_img_folder_open = string.Format(imageFormat, s_imageRoot, "folderopen.gif");
            s_img_empty = string.Format(imageFormat, s_imageRoot, "white.gif");

        }
        public static string IMG_P 
        {
            get { return s_img_p; }
        }
        public static string IMG_M 
        {
            get { return s_img_m; }
        }
        public static string IMG_F 
        {
            get { return s_img_f; }
        }
        public static string IMG_I 
        {
            get { return s_img_i; }
        }
        public static string IMG_L 
        {
            get { return s_img_l; }
        }
        public static string IMG_R 
        {
            get { return s_img_r; }
        }
        public static string IMG_T 
        {
            get { return s_img_t; }
        }
        public static string IMG_FP 
        {
            get { return s_img_fp; }
        }
        public static string IMG_IP 
        {
            get { return s_img_ip; }
        }
        public static string IMG_LP 
        {
            get { return s_img_lp; }
        }
        public static string IMG_RP 
        {
            get { return s_img_rp; }
        }
        public static string IMG_TP 
        {
            get { return s_img_tp; }
        }
        public static string IMG_FM 
        {
            get { return s_img_fm; }
        }
        public static string IMG_IM 
        {
            get { return s_img_im; }
        }
        public static string IMG_LM 
        {
            get { return s_img_lm; }
        }
        public static string IMG_RM 
        {
            get { return s_img_rm; }
        }
        public static string IMG_TM 
        {
            get { return s_img_tm; }
        }
        public static string IMG_FOLDER 
        {
            get { return s_img_folder; }
        }
        public static string IMG_FOLDER_OPEN 
        {
            get { return s_img_folder_open; }
        }
        public static string IMG_EMPTY 
        {
            get { return s_img_empty; }
        }

    }
    public class TreeNode 
    {
        private TreeNode m_parent;
        private TreeNode m_nextNode;
        private TreeNode m_previousNode;
        private NameValueCollection m_attributes = new NameValueCollection();
        private bool m_isVisible = true;
        private string m_name;
        public bool IsUsingFavorites = false;
        private List<string> excludedLandingPageIds = new List<string>() {
            "Page_QualifiedLoanProgramSearch",
            "Page_QualifiedLoanProgramSearchBeta",
            "Page_RunInternalPricingV2"
        };

        public NameValueCollection Attributes 
        {
            get { return m_attributes; }
        }

        public bool IsVisible
        {
            get { return m_isVisible; }
            set { m_isVisible = value; }
        }

            public TreeNode Parent
        {
            get { return m_parent; }
            set { m_parent = value; }
        }

        public TreeNode NextNode
        {
            get { return m_nextNode; }
            set { m_nextNode = value; }
        }

        public TreeNode PreviousNode
        {
            get { return m_previousNode; }
            set { m_previousNode = value; }
        }

        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        public string ID 
        {
            get { return m_attributes.Get("Id"); }
        }

        public string FullPathName 
        {
            get 
            {
                string prefix = m_parent == null ? "" : m_parent.FullPathName;
                return prefix + "/" + m_name;
            }
        }

        protected int Level 
        {
            get 
            {
                if (null != m_parent)
                    return m_parent.Level + 1;
                else 
                    return 1;
            }
        }


        public TreeNode(string name) 
        {
            m_name = name;    
        }
        public virtual DynaTreeNode Render(bool filterLandingPagesOnly) 
        {
            if (IsVisible) 
            {
                bool isFavChild = Parent != null && Parent.ID.Equals("Folder_Favorites");
                bool isConvLog = this.ID == "Page_ConversationLog";
                var dynaNode = new DynaTreeNode() {
                    title = this.Name,
                    key = this.ID,
                    isFolder = false,
                    addClass = (this.IsUsingFavorites && isFavChild) ? "FavContainer PageContainer" : "PageContainer",
                    onCreate = "initializeNodes"
                };
                
                if(excludedLandingPageIds.Contains(this.ID)) {
                    if(filterLandingPagesOnly)
                    {
                        return null;
                    }
                    else
                    {
                        dynaNode.customAttributes.Add("excludelandingpage", "true");
                    }
                }

                StringBuilder extraAttributes = new StringBuilder();

                foreach (string key in m_attributes.AllKeys)
                {
                    if (key == "onclick")
                    {
                        var calledFunction = m_attributes[key];
                        if (!calledFunction.Contains(".aspx"))
                        {
                            if (filterLandingPagesOnly)
                            {
                                return null;
                            }
                            else
                            {
                                dynaNode.customAttributes.Add("excludelandingpage", "true");
                            }
                        }
                    }

                    dynaNode.customAttributes.Add(key, m_attributes[key]);
                }

                // Add conversation log ending icon 
                return dynaNode;
            }

            return null;
        }
    }

    public class TreeFolder : TreeNode
    {
        private ArrayList m_childs = new ArrayList();
        private TreeNode m_previousChild;
        private bool m_isCollapsed = false;

        public bool IsCollapsed
        {
            get { return m_isCollapsed; }
            set { m_isCollapsed = value; }
        }

        public bool HasVisibleChildren
        {
            get
            {
                var ret = false;
                foreach (object c in m_childs)
                {
                    var child = c as TreeNode;
                    if (child == null) continue;

                    if (child.IsVisible)
                    {
                        ret = true;
                        break;
                    }
                }

                return ret;
            }
        }
        public TreeFolder(string name) : base(name) 
        {
        }
        public void AddChild(TreeNode child) 
        {
            child.Parent = this;
            child.PreviousNode = m_previousChild;

            m_childs.Add(child);

            if (null != m_previousChild)
                m_previousChild.NextNode = child;

            m_previousChild = child;
        }
        public override DynaTreeNode Render(bool filterLandingPagesOnly) 
        {
            if (IsVisible) 
            {
                var dynaNode = new DynaTreeNode() {
                    title = this.Name,
                    key = this.ID,
                    isFolder = true,
                    hideCheckbox = true,
                    expand = true,
                    onCreate = "initializeNodes",
                    isCollapsedAfterRender = this.IsCollapsed
                };

                dynaNode.customAttributes.Add("path", Utilities.SafeHtmlString(this.FullPathName));
                // Folder has tooltip "Star Pages to add" on its expand/collapse tooltip
                
                foreach (TreeNode o in m_childs)
                {
                    o.IsUsingFavorites = this.IsUsingFavorites;
                    var child = o.Render(filterLandingPagesOnly);
                    if(child != null)
                    {
                        dynaNode.children.Add(child);
                    }
                }

                return dynaNode;
            }

            return null;
        }
    }
}
