using System;
using System.Collections;
using System.Threading;

namespace LendersOffice.Monitor
{
	public class PostbackTracker
	{
		/// <summary>
		/// Keep track of recent requests.  We list user ids in
		/// our activity list by request date order (most recent
		/// at the end).  We evict entries of the top of the
		/// </summary>

		static private Hashtable    m_Table = new Hashtable();
		static private Hashtable    m_Users = new Hashtable();
		static private ArrayList m_Activity = new ArrayList();
		static private DateTime m_StartedOn = DateTime.Now;
		static private Int32   m_MaxEntries = 256;
		static private Int32 m_HistoryCount = 10;

		#region ( Tracker Properties )

		public TrackingInfo this[ Guid userId ]
		{
			// Lookup the matching user record and return it.

			get
			{
				TrackingInfo tInfo = new TrackingInfo();

				lock( m_Table )
				{
					if( m_Users.Contains( userId ) == true )
					{
						PostbackUserInfo uInfo = m_Users[ userId ] as PostbackUserInfo;

						tInfo.LoginName = uInfo.LoginName;
						tInfo.UserId    = uInfo.UserId;
					}

					if( m_Table.Contains( userId ) == true )
					{
						ICollection recsQ = m_Table[ userId ] as ICollection;

						tInfo.History = recsQ;
					}
				}

				return tInfo;
			}
		}

		public ICollection Entries
		{
			// Copy entries for display (binding).

			get
			{
				ArrayList aList = new ArrayList();

				lock( m_Table )
				{
					foreach( Guid userId in m_Activity )
					{
						TrackingInfo tInfo = new TrackingInfo();

						if( m_Users.Contains( userId ) == true )
						{
							PostbackUserInfo uInfo = m_Users[ userId ] as PostbackUserInfo;

							tInfo.LoginName = uInfo.LoginName;
							tInfo.UserId    = uInfo.UserId;
						}

						if( m_Table.Contains( userId ) == true )
						{
							ICollection recsQ = m_Table[ userId ] as ICollection;

							tInfo.History = recsQ;
						}

						aList.Add( tInfo );
					}
				}

				return aList;
			}
		}

		public DateTime StartedOn
		{
			// Access member.

			get
			{
				lock( m_Table )
				{
					return m_StartedOn;
				}
			}
		}

		public Int32 HistoryCount
		{
			// Access member.

			set
			{
				if( value < 0 )
				{
					throw new ArgumentException( "Invalid history count value (must be zero or greater)" );
				}

				lock( m_Table )
				{
					m_HistoryCount = value;
				}
			}
			get
			{
				lock( m_Table )
				{
					return m_HistoryCount;
				}
			}
		}

		public Int32 MaxEntries
		{
			// Access member.

			set
			{
				if( value < 0 )
				{
					throw new ArgumentException( "Invalid max entries value (must be zero or greater)" );
				}

				lock( m_Table )
				{
					m_MaxEntries = value;
				}
			}
			get
			{
				lock( m_Table )
				{
					return m_MaxEntries;
				}
			}
		}

		#endregion

		/// <summary>
		/// Remember the details of a client's request in our table.
		/// </summary>

		public void Track( String loginName , String userType , String sOp , String sWhat , String sFrom , DateTime requestDate , Guid userId )
		{
			// Add a new record to our table.  If an entry doesn't
			// already exist, we insert it.

			lock( m_Table )
			{
				if( m_Users.Contains( userId ) == false )
				{
					// Clean out the least active records.  If the
					// max entry count was recently updated to trim
					// our cache, then we enforce the new limit here.

					while( m_Activity.Count >= m_MaxEntries && m_Activity.Count > 0 )
					{
						// Get rid of the least active entry.

						m_Users.Remove( m_Activity[ 0 ] );
						m_Table.Remove( m_Activity[ 0 ] );

						// Take the reference out of our set.

						m_Activity.RemoveAt( 0 );
					}

					// Add a new user info if we have room in our
					// cache of user logs.

					PostbackUserInfo uInfo = new PostbackUserInfo();

					uInfo.LoginName = loginName;
					uInfo.UserId    = userId;

					m_Users.Add( userId , uInfo );
				}
				else
				{
					// Take the user's reference and make it look
					// like the most recently active.  We should
					// check the records date.  We assume that
					// all track requests happen chronologically
					// and serially.

					m_Activity.Remove( userId );
				}

				// Insert a new queue to hold this user who is
				// a new entry in our table.  If the max count
				// is set ot zero, we will punt here.

				Queue qLog = m_Table[ userId ] as Queue;

				if( qLog == null && m_Table.Count < m_MaxEntries )
				{
					m_Table.Add( userId , qLog = new Queue() );
				}

				if( qLog != null )
				{
					// The user must look like the most active entry so
					// we're not too quick to evict him.

					m_Activity.Add( userId );

					// Remove older postback records.  Again, we assume
					// that these records are coming in chronological
					// order.  If they're not ordered in this way, then
					// we need a priority queue that sorts by request
					// date and evicts the oldest.

					while( qLog.Count >= m_HistoryCount && qLog.Count > 0 )
					{
						qLog.Dequeue();
					}

					if( qLog.Count < m_HistoryCount )
					{
						PostbackRecord pRec = new PostbackRecord();

						pRec.Operation   = sOp;
						pRec.RequestedOn = requestDate;
						pRec.What        = sWhat;
						pRec.From        = sFrom;

						qLog.Enqueue( pRec );
					}
				}
			}
		}

		/// <summary>
		/// Nix any reference to the given user in our cache. 
		/// </summary>

		public void Drop( Guid userId )
		{
			// Nix any reference to the given user in our cache.

			lock( m_Table )
			{
				m_Activity.Remove( userId );
				m_Table.Remove( userId );
			}
		}

		/// <summary>
		/// Erase the contents of our shared lookup table.
		/// </summary>

		public void Clear()
		{
			// Empty out the contents of our static table.

			lock( m_Table )
			{
				m_Activity.Clear();
				m_Table.Clear();
			}
		}
	}

	/// <summary>
	/// Track individual application requests from authenticated
	/// users.  We should track the ip addresses too.  Maybe
	/// later.
	/// </summary>

	public class PostbackUserInfo
	{
		/// <summary>
		/// Track individual application requests from authenticated
		/// users.  We should track the ip addresses too.  Maybe
		/// later.
		/// </summary>

		private String m_LoginName = String.Empty;
		private Guid      m_UserId = Guid.Empty;

		#region ( User Info Properties )

		public String LoginName
		{
			set { m_LoginName = value; }
			get { return m_LoginName; }
		}

		public Guid UserId
		{
			set { m_UserId = value; }
			get { return m_UserId; }
		}

		#endregion

	}

	/// <summary>
	/// Track individual application requests from authenticated
	/// users.  We should track the ip addresses too.  Maybe
	/// later.
	/// </summary>

	public class PostbackRecord
	{
		/// <summary>
		/// Track individual application requests from authenticated
		/// users.  We should track the ip addresses too.  Maybe
		/// later.
		/// </summary>

		private String     m_Operation = String.Empty;
		private String          m_What = String.Empty;
		private String          m_From = String.Empty;
		private DateTime m_RequestedOn = DateTime.Now;

		#region ( Record Properties )

		public String Operation
		{
			set { m_Operation = value; }
			get { return m_Operation; }
		}

		public String What
		{
			set { m_What = value; }
			get { return m_What; }
		}

		public String From
		{
			set { m_From = value; }
			get { return m_From; }
		}

		public DateTime RequestedOn
		{
			set { m_RequestedOn = value; }
			get { return m_RequestedOn; }
		}

		#endregion

	}

	/// <summary>
	/// Present individual user's tracking data in a single
	/// row.  Binding will pull this content out.
	/// </summary>

	public class TrackingInfo : ArrayList
	{
		/// <summary>
		/// Present individual user's tracking data in a
		/// single row.  Binding will pull this content
		/// out.
		/// </summary>

		private String    m_LoginName = String.Empty;
		private DateTime m_MostRecent = DateTime.MinValue;
		private DateTime m_FirstEntry = DateTime.MinValue;
		private Double m_AverageDelay = Double.NaN;
		private Int32   m_RecordCount = Int32.MinValue;
		private Guid         m_UserId = Guid.Empty;

		#region ( Tracking Properties )

		public ICollection History
		{
			// Access initializer.

			set
			{
				// Reset the counter and delay values.  We do
				// simple statistics on the records to get an idea
				// of activity.  We assume that the record queue
				// is chronologically ordered oldest to recent.
				// The queue is fifo, but the tracking events may
				// occur out of order -- watch out.

				m_MostRecent = DateTime.MinValue;
				m_FirstEntry = DateTime.MaxValue;

				m_AverageDelay = 0;

				m_RecordCount = 0;

				foreach( PostbackRecord postRec in value )
				{
					if( postRec.RequestedOn > m_MostRecent )
					{
						m_MostRecent = postRec.RequestedOn;
					}

					if( postRec.RequestedOn < m_FirstEntry )
					{
						m_FirstEntry = postRec.RequestedOn;
					}

					++m_RecordCount;
				}

				if( m_RecordCount > 1 )
				{
					DateTime prevDate = m_FirstEntry;

					foreach( PostbackRecord postRec in value )
					{
						m_AverageDelay += postRec.RequestedOn.Subtract( prevDate ).TotalSeconds;

						prevDate = postRec.RequestedOn;
					}

					m_AverageDelay /= ( m_RecordCount - 1 );
				}
				else
				{
					m_AverageDelay = Double.NaN;
				}

				foreach( PostbackRecord postRec in value )
				{
					Add( postRec );
				}
			}
		}

		public Double Age
		{
			get { return DateTime.Now.Subtract( m_MostRecent ).TotalMinutes; }
		}

		public DateTime MostRecent
		{
			get { return m_MostRecent; }
		}

		public DateTime FirstEntry
		{
			get { return m_FirstEntry; }
		}

		public Double AverageDelay
		{
			get { return m_AverageDelay; }
		}

		public int RecordCount
		{
			get { return m_RecordCount; }
		}

		public String LoginName
		{
			set { m_LoginName = value; }
			get { return m_LoginName; }
		}

		public Guid UserId
		{
			set { m_UserId = value; }
			get { return m_UserId; }
		}

		#endregion

	}

}