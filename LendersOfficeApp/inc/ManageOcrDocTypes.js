﻿var app = angular.module('ManageOcrDocTypes', []);
app.controller('ManageOcrDocTypesController', ['$scope', function ($scope) {
    $scope.vm = {};

    $scope.edit = function (id, name) {
        var newDocTypeName = prompt('Enter the new OCR document type name:', name);
        if ($.trim(newDocTypeName) === '') {
            return;
        }

        var args = {
            Id: id,
            NewName: newDocTypeName
        };

        gService.ocr.callAsyncSimple('EditDocType', args, function (result) {
            if (result.error) {
                alert(result.UserMessage);
                return;
            }
            else if (result.value.Result !== "True") {
                alert('Failed to edit doc type.');
                return;
            }

            $scope.vm.DocTypes = angular.fromJson(result.value.ViewModel);
        });
    }

    $scope.delete = function (id, name) {
        if (!confirm('Are you sure you want to delete "' + name + '"?')) {
            return;
        }

        var args = {
            Id: id
        };

        gService.ocr.callAsyncSimple('DeleteDocType', args, function (result) {
            if (result.error) {
                alert(result.UserMessage);
                return;
            }
            else if (result.value.WasDeleted === "False") {
                alert('Could not delete doc type.');
                return;
            }

            $scope.vm.DocTypes = angular.fromJson(result.value.ViewModel);
        });
    }

    $scope.addNewDocType = function () {
        var docTypeName = prompt('Enter the new OCR document type name:', '');
        if ($.trim(docTypeName) === '') {
            return;
        }

        var args = { Name: docTypeName };
        gService.ocr.callAsyncSimple('AddDocType', args, function (result) {
            if (result.error) {
                alert(result.UserMessage);
                return;
            }

            $scope.vm.DocTypes = angular.fromJson(result.value.ViewModel);
        });
    }

    $scope.init = function () {
        gService.ocr.callAsyncSimple('GetViewModel', {}, function (result) {
            if (result.error) {
                alert(result.UserMessage);
                return;
            }

            $scope.vm.DocTypes = angular.fromJson(result.value.ViewModel);

            // Calling the service has to be done once the page is loaded
            // and angular has finished binding, so $apply must be called
            // to populate the table.
            $scope.$apply();
        });
    }
}]);

function _init() {
    angular.element(document.getElementById('ManageOcrDocTypesDiv')).scope().init();
}
