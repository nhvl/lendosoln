﻿// Note the arrangement GFE2010RC must be in same row as the combobox, jquery must be enabled.
// it relies on the utils service.
function FeeTypePropertiesSetter(isArchivePage) {
    this._isArchivePage = isArchivePage;
}
FeeTypePropertiesSetter.prototype.setFeeTypeProperties = function(combobox, lineNumber, option_default) {
    var args = {
        'hfIsArchivePage': this._isArchivePage,
        'Description': combobox.value,
        'LineNumber': lineNumber
    };

    gService.utils.callAsyncSimple("GetFeeTypeProperties", args, function (result) {
        if (!result.error) {
            // if fee type not found (user entered description), just return
            if (result.value.notFound) {
                return;
            }

            var $row = $(combobox).parents("tr").first();
            var $Apr = $row.find("input[data-fsid=Apr]");
            var $Fha = $row.find("input[data-fsid=Fha]");
            var $Page2 = $row.find("input[data-fsid=Page2]");

            // If blank option selected, uncheck boxes, reset radio, and blank textbox
            if (result.value.blank) {
                combobox.value = ""; // Blank textbox so special characters (&zwnj;) aren't saved to DB
                $Apr.prop("checked", false);
                $Fha.prop("checked", false);

                // Select first visible radio button as default
                var $option1 = $Page2.filter("[data-index=1]");
                var $option2 = $Page2.filter("[data-index=2]");
                var $option3 = $Page2.filter("[data-index=3]");

                if ($option1.is(":visible"))
                    $option1.prop('checked', true);

                if ($option2.is(":visible") && (!$option1.prop('checked') || $option2.val() == option_default))
                    $option2.prop('checked', true);

                if ($option3.is(":visible") && ((!$option1.prop('checked') && !$option2.prop('checked')) || $option3.val() == option_default))
                    $option3.prop('checked', true);

                return;
            }

            $Apr.prop("checked", result.value.Apr == "True");
            $Fha.prop("checked", result.value.Fha == "True");
            $Page2.filter("[value=" + result.value.Page2 + "]").prop("checked", true);
        } else {
            var errMsg = 'Unexpected error. Could not auto-populate fee type properties.';

            if (null != result.UserMessage)
                errMsg += " error msg: " + result.UserMessage;

            if (console && console.log) {
                console.log(errMsg);
            }

            return false;
        }
    });    
}
