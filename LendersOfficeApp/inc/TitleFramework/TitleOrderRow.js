﻿var TitleOrderRow = (function () {
    var titleOrderRowTemplate;
    var titleOrderPostTemplateLoadCallback;

    jQuery(function ($) {
        $.get(ML.VirtualRoot + "/newlos/TitleFramework/TitleOrderRow.html", function (data) {
            titleOrderRowTemplate = $.template(data);
            if (typeof (titleOrderPostTemplateLoadCallback) == 'function') {
                titleOrderPostTemplateLoadCallback();
            }
        });
    });

    function _createTitleOrderRow (data) {
        return $.tmpl(titleOrderRowTemplate, data);
    }

    return {
        CreateTitleOrderRow: _createTitleOrderRow,
        PostTemplateLoadCallback: function (callback) { titleOrderPostTemplateLoadCallback = callback; }
    };
})();
