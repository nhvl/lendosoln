﻿function OfficialContactList_OnChange(oContactFieldMapperInfo) {
  if (isReadOnly()) return;
  
  var ddl = document.getElementById(oContactFieldMapperInfo.OfficialContactListClientId);
  
  if (null == ddl) return;
  _SetContactFieldMapperHiddenValue(oContactFieldMapperInfo);
  var agentRoleT = ddl.value;

  var oRolodex = new cRolodex();
  oRolodex.populateFromOfficialContactList(oContactFieldMapperInfo.LoanId, agentRoleT, function (args) {
      var oFieldIdHolder = oContactFieldMapperInfo.FieldIdHolder;
      for (var p in oFieldIdHolder) {
          _SafeSet(oFieldIdHolder[p], args[p]);
      }
  });

  return false;
}

function _SafeSet(sFieldId, sValue) {
  if (null == sFieldId)
      return;

  if (sFieldId.indexOf("LicensesPanel") > 0) //Licenses Panel
  {
      //Get the control prefix
      var i = sFieldId.indexOf("LicensesPanel")
      var prefix = sFieldId.substring(0, i);
      LICENSES.fn.setLicenses(prefix, sValue);
      return;
  }
      
  var o = document.getElementById(sFieldId);
  if (null != o) {
    if (o.type == 'checkbox') {
      if (sValue == true || sValue == '1') {
        o.checked = true;
      } else {
        o.checked = false;
      }
    } else {
      if (null != sValue) {
       o.value = sValue;
      }
    }
  }
}
function _SafeSetReadOnly(sFieldId, bIsReadOnly) {
  if (null == sFieldId)
    return;

  var o = document.getElementById(sFieldId);
  if (null != o) {
    if (o.type == 'select-one') {
      disableDDL(o, bIsReadOnly);
    } else {
      o.readOnly = bIsReadOnly;
    }
  }

}
function _SetContactFieldMapperHiddenValue(oContactFieldMapperInfo) {
  var ddl = document.getElementById(oContactFieldMapperInfo.OfficialContactListClientId);
  var manualOverrideRadioButton = document.getElementById(oContactFieldMapperInfo.ManualOverrideClientId);
  var useOfficialContactRadioButton = document.getElementById(oContactFieldMapperInfo.UseOfficialContactClientId);

  if (null == manualOverrideRadioButton || null == useOfficialContactRadioButton) {
    return;
  }

  document.getElementById(oContactFieldMapperInfo.ClientID + "_AgentRoleT").value = ddl.value;
  document.getElementById(oContactFieldMapperInfo.ClientID + "_IsLocked").value = manualOverrideRadioButton.checked ? 'True' : 'False';
}
function UseOfficialContact_OnClick(oContactFieldMapperInfo) {
  ContactFieldMapper_OnInit(oContactFieldMapperInfo);
  if (typeof (refreshCalculation) != 'undefined') {
    refreshCalculation();
  }

  return true;
}
function ManualOverride_OnClick(oContactFieldMapperInfo) {
  ContactFieldMapper_OnInit(oContactFieldMapperInfo);
  return true;

}

function ContactFieldMapper_OnInit(oContactFieldMapperInfo) {
  if (isReadOnly()) return;

  var ddl = document.getElementById(oContactFieldMapperInfo.OfficialContactListClientId);
  var pickFromContactLink = document.getElementById(oContactFieldMapperInfo.PickFromContactClientId);
  var addToContactLink = document.getElementById(oContactFieldMapperInfo.AddToContactClientId);
  var manualOverrideRadioButton = document.getElementById(oContactFieldMapperInfo.ManualOverrideClientId);
  var useOfficialContactRadioButton = document.getElementById(oContactFieldMapperInfo.UseOfficialContactClientId);

  if (null == manualOverrideRadioButton || null == useOfficialContactRadioButton) {
    return;
  }
  _SetContactFieldMapperHiddenValue(oContactFieldMapperInfo);
  var bIsManualOverride = manualOverrideRadioButton.checked;
  
  if (null != ddl ) {
    disableDDL(ddl, bIsManualOverride);
    if ( ddl.options.length === 1 ) {
        disableDDL(ddl, true);  
    }
  }
  if (null != pickFromContactLink) {
      setDisabledAttr(pickFromContactLink, !bIsManualOverride);
  }
  if (null != addToContactLink) {
      setDisabledAttr(addToContactLink, !bIsManualOverride);
  }

  var oFieldIdHolder = oContactFieldMapperInfo.FieldIdHolder;
  for (var p in oFieldIdHolder) {
    _SafeSetReadOnly(oFieldIdHolder[p], !bIsManualOverride);
  }
  

}

function PickFromContact_OnClick(oContactFieldMapperInfo) 
{
  var rolodex = new cRolodex();
  var agentRoleTDDL = document.getElementById(oContactFieldMapperInfo.AgentRoleTDDLClientId);

  var type = null;
  if (null != agentRoleTDDL) {
    type = agentRoleTDDL.value;
  } else {
    type = oContactFieldMapperInfo.DefaultAgentRoleT;
  }
  var rolodexHandler = function(args){
    if (args.OK) {
      var oFieldIdHolder = oContactFieldMapperInfo.FieldIdHolder;
      for (var p in oFieldIdHolder) {
        _SafeSet(oFieldIdHolder[p], args[p]);
      }
      updateDirtyBit();
    }
  };
  
  rolodex.chooseFromRolodex(type, oContactFieldMapperInfo.LoanId, null, null,  rolodexHandler);
 
  return false;
}

function AddToContact_OnClick(oContactFieldMapperInfo) {
  var rolodex = new cRolodex();

  var agentRoleTDDL = document.getElementById(oContactFieldMapperInfo.AgentRoleTDDLClientId);

  var type = null;
  if (null != agentRoleTDDL) {
    type = agentRoleTDDL.value;
  } else {
    type = oContactFieldMapperInfo.DefaultAgentRoleT;
  }

  var args = new Object();
  
  if (null != type) {
    args.AgentType = type;
  }

  var oFieldIdHolder = oContactFieldMapperInfo.FieldIdHolder;
  for (var p in oFieldIdHolder) {
    var o = document.getElementById(oFieldIdHolder[p]);
    if (null != o) {
      if (o.type == 'checkbox') {
        args[p] = o.checked ? '1' : '0';
      } else {
      args[p] = o.value;
      }
    }
  }

  rolodex.addToRolodex(args);
  return false;
  
}