﻿var AuditPage = (function () {
    var auditPageTemplate;

    jQuery(function ($) {
        $.get(ML.VirtualRoot + "/newlos/Common/AuditPage.html", function (data) {
            auditPageTemplate = $.template(data);
        });

        $('body').on('click', '#AuditCancelBtn', function () {
            LQBPopup.Return({ OK: false });
        });

        $('body').on('click', '#AuditNextBtn', function () {
            LQBPopup.Return({ OK: true });
        });

        $('body').on('click', '.FieldUrl', function () {
            parent.linkMe(gVirtualRoot + $(this).data('url'));
        });
    });

    function _createAuditPage(data) {
        return $.tmpl(auditPageTemplate, data);
    }

    return {
        CreateAuditPage: _createAuditPage
    };
})();

