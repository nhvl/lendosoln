﻿jQuery(function(jq) {
    var $main = jq('#TradeMasterPortlet');
    function $(a) {
        return jq(a, $main);
    }

    $('.search-trades').data('tabNum', 0);
    $('.open-trades').data('tabNum', 1);
    $('.settlement-report').data('tabNum', 2);

    $('.tab').click(function(ev) {
        $this = $(this);
        $('.selected').removeClass('selected');
        $this.parent().addClass('selected');
        var href = QueryStringUtilities.updateQueryString('tabSubPage', $this.data('tabNum'));
        if (href.indexOf('page') == -1)
            href += '&page=' + selectedTabIndex;
        window.location.href = href;
    });

});