﻿function postRefreshCalculationMaxOrigFeeHandler(result) {
    $('#' + ML.MaxOrigCompFeeClientId + '_OriginationFeeAmount').val(result[ML.MaxOrigCompFeeResultPrefix + '_OriginationFeeAmount']);
    $('#' + ML.MaxOrigCompFeeClientId + '_SupplementalOriginationFeeAmount').val(result[ML.MaxOrigCompFeeResultPrefix + '_SupplementalOriginationFeeAmount']);
    $('#' + ML.MaxOrigCompFeeClientId + '_SumOriginationFees').val(result[ML.MaxOrigCompFeeResultPrefix + '_SumOriginationFees']);
    $('#' + ML.MaxOrigCompFeeClientId + '_RepairCostsAndFees').val(result[ML.MaxOrigCompFeeResultPrefix + '_RepairCostsAndFees']);
    $('#' + ML.MaxOrigCompFeeClientId + '_ContingencyReserves').val(result[ML.MaxOrigCompFeeResultPrefix + '_ContingencyReserves']);
    $('#' + ML.MaxOrigCompFeeClientId + '_MortgagePaymentReserves').val(result[ML.MaxOrigCompFeeResultPrefix + '_MortgagePaymentReserves']);
    $('#' + ML.MaxOrigCompFeeClientId + '_SumRepairCostsFeesAndReserves').val(result[ML.MaxOrigCompFeeResultPrefix + '_SumRepairCostsFeesAndReserves']);
    $('#' + ML.MaxOrigCompFeeClientId + '_PercentageOfRepairCostReservesSum').val(result[ML.MaxOrigCompFeeResultPrefix + '_PercentageOfRepairCostReservesSum']);
    $('#' + ML.MaxOrigCompFeeClientId + '_GreaterOfRepairCostReserveSumPercentageAndFixedAmount').val(result[ML.MaxOrigCompFeeResultPrefix + '_GreaterOfRepairCostReserveSumPercentageAndFixedAmount']);
    $('#' + ML.MaxOrigCompFeeClientId + '_MaxFinanceableOrigFeeAmount').val(result[ML.MaxOrigCompFeeResultPrefix + '_MaxFinanceableOrigFeeAmount']);
}
