﻿jQuery(function($) {

    var $table = $('#DisclosuresTable');

    $.tablesorter.defaults.widgetZebra = {
        css: ["GridItem", "GridAlternatingItem"]
    };

    var sorting = [[4, 0]];
    if ($table.find('tbody').children().length !== 0) {
        $table.tablesorter({
            sortList: sorting,
            widgets: ['zebra']
        });
    }
    else {
        $table.hide();
    }

    function getLoanId(elem) {
        return $(elem).closest('tr').find('.LoanId').val();
    }

    function editLoanHelper(loanId) {
        if (typeof editLoan === 'function') {
            editLoan(loanId);
        }
    }

    function viewAudit(loanId) {
        if (typeof ML !== 'object' || !ML.hasOwnProperty('VirtualRoot')) {
            return;
        }

        var url = ML.VirtualRoot + '/newlos/DisclosureAudit.aspx?loanid=' + loanId;
        var name = 'disclosure_audit_' + loanId.replace(/\-/g, '');
        var options = 'width=750px,height=675px,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes';
        var win = window.open(url, name, options);
        win.focus();
    }

    // OPM 173000, 8/13/2015, ML
    function disclosureChange($row, loanId, confirmPrompt, webMethod)
    {
        if (!confirm(confirmPrompt)) {
            return;
        }

        var args = {
            sLId: loanId
        };

        var methodUrl = 'PipelineService.aspx/' + webMethod;

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: methodUrl,
            data: JSON.stringify(args),
            dataType: 'json',
            async: true
        }).then(
            function (response) {
                $row.hide();
                $table.trigger('sorton', [$table[0].config.sortList]);
                if (typeof refreshPipeline === 'function') {
                    refreshPipeline(false); // display the warning.
                }
            },
            function (XMLHttpRequest, textStatus, errorThrown) {
                alert('An unexpected error happened, please try again later.');
            }
        );
    }

    function markDisclosed(loanId, fileVersion) {
        // We need to set this variable at the time that this method is called.
        // If we don't and instead try to set $row in disclosureChange(), jQuery
        // will not be able to find the closest table row.
        var $row = $(this).closest('tr');

        disclosureChange(
            $row,
            loanId,
            'Are you sure you want to mark this loan as disclosed?',
            'MarkLoanDisclosed'
        );
    }

    function clearDisclosed(loanId) {
        // We need to set this variable at the time that this method is called.
        // If we don't and instead try to set $row in disclosureChange(), jQuery
        // will not be able to find the closest table row.
        var $row = $(this).closest('tr');

        disclosureChange(
            $row,
            loanId,
            'Are you sure you want to clear the disclosure/redisclosure requirement for this loan ' +
            'and remove it from the disclosure pipeline? Doing so will not mark the loan as disclosed ' +
            'in the loan\'s audit history.',
            'ClearLoanDisclosure'
        );
    }

    var contextMenuLayout = {
        'EditLoan': {
            Html: '<li><a href="#">edit loan</a></li>',
            Fxn: editLoanHelper
        },
        'ViewAudit': {
            Html: '<li><a href="#">view audit</a></li>',
            Fxn: viewAudit
        },
        'MarkDisclosed': {
            Html: '<li><a href="#" class="disclose">mark disclosed</a></li>',
            Fxn: markDisclosed
        },
        'ClearDisclosed': {
            Html: '<li><a href="#" class="disclose">reviewed/clear</a></li>',
            Fxn: clearDisclosed
        }
    };

    function createContextMenu(getLoanId) {
        var contextMenu = $('<ul class="Context">');

        $.each(contextMenuLayout, function(key, config) {
            var menuEntry = contextMenuLayout[key];

            contextMenu.append($(menuEntry.Html).click(function() {
                menuEntry.Fxn.call(this, getLoanId(this));
                return false;
            }));
        });

        contextMenu.hide();
        return contextMenu;
    }

    var contextMenu = createContextMenu(getLoanId);
    var numDueToday = 0;
    var today = new Date();
    today.setHours(0, 0, 0, 0);

    // Attach the context menu and highlight the due dates that are today.
    var $tableRows = $('tbody tr', $table);
    $tableRows.find('td:first')
        .addClass('Action')
        .append(contextMenu)
        .click(function() {
            $(this).closest('tr').toggleClass('Opened');
            $('.Context', this).toggle();
        }).end()
        .find('td:eq(4) span')
        .each(function() {
            var $this = $(this)
            var dueD = new Date($this.html());
            dueD.setHours(0, 0, 0, 0);

            if (dueD.getTime() <= today.getTime()) {
                $this.addClass('Due');
                numDueToday += 1;
            }
        }).end();

    var numCellsPerRow = $('tbody tr:first', $table).find('td').length;
    if (numCellsPerRow === 9) {
        $tableRows.find('td:eq(7) span')
            .each(function() {
                var $this = $(this);

                if ($this.text() === 'Automatic disclosure failed') {
                    $this.addClass('Failed');
                    $this.append('<a class="ViewAutoDisclosureDetails Action">view details</a>')
                }
            });
    }

    $table.on('click', 'a.ViewAutoDisclosureDetails', function() {
        var $parentRow = $(this).parent()
            .parent()
            .parent();

        var loanId = $parentRow.find('.LoanId')
            .val();

        window.open(ML.VirtualRoot + '/newlos/Services/AutoDisclosureAuditResults.aspx?loanid=' + loanId,
            'auto_disclosure_audit_results',
            'toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes,height=450,width=600');
    });

    if (numDueToday > 0) {
        var msg = numDueToday + (numDueToday > 1 ? ' loans ' : ' loan ') + 'must be disclosed today';
        $('#DisclosuresPipeline .heading').html(msg)
            .addClass('Due');
    }
    else {
        $('#DisclosuresPipeline .heading').html('No loans need disclosure today');
    }
});
